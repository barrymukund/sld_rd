<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#179" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2347" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.807951477051" Y="20.574431640625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.442368682861" Y="21.676697265625" />
                  <Point X="0.378635528564" Y="21.768525390625" />
                  <Point X="0.356748565674" Y="21.790982421875" />
                  <Point X="0.330493225098" Y="21.810224609375" />
                  <Point X="0.302494873047" Y="21.824330078125" />
                  <Point X="0.147759277344" Y="21.872353515625" />
                  <Point X="0.049135734558" Y="21.902962890625" />
                  <Point X="0.02098301506" Y="21.907232421875" />
                  <Point X="-0.008657706261" Y="21.907234375" />
                  <Point X="-0.036824085236" Y="21.90296484375" />
                  <Point X="-0.191559692383" Y="21.854939453125" />
                  <Point X="-0.290183227539" Y="21.82433203125" />
                  <Point X="-0.318183410645" Y="21.810224609375" />
                  <Point X="-0.344439331055" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.466318084717" Y="21.62444921875" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.706921813965" Y="20.90553125" />
                  <Point X="-0.916584899902" Y="20.12305859375" />
                  <Point X="-0.966941650391" Y="20.13283203125" />
                  <Point X="-1.079321533203" Y="20.154646484375" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.244413085938" Y="20.212865234375" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.253475097656" Y="20.6696171875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323644897461" Y="20.868794921875" />
                  <Point X="-1.466105712891" Y="20.99373046875" />
                  <Point X="-1.556905639648" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643027587891" Y="21.109033203125" />
                  <Point X="-1.832104858398" Y="21.12142578125" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.20020703125" Y="20.999927734375" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.422646484375" Y="20.786447265625" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.6369609375" Y="20.80860546875" />
                  <Point X="-2.801724853516" Y="20.910623046875" />
                  <Point X="-3.045924316406" Y="21.0986484375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.878414794922" Y="21.535896484375" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405576171875" Y="22.4148125" />
                  <Point X="-2.4145625" Y="22.4444296875" />
                  <Point X="-2.428781982422" Y="22.473259765625" />
                  <Point X="-2.446807861328" Y="22.4984140625" />
                  <Point X="-2.464151855469" Y="22.5157578125" />
                  <Point X="-2.489304931641" Y="22.533783203125" />
                  <Point X="-2.518134277344" Y="22.548001953125" />
                  <Point X="-2.54775390625" Y="22.556986328125" />
                  <Point X="-2.578689941406" Y="22.555974609375" />
                  <Point X="-2.610217773438" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.141100097656" Y="22.24901953125" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.952699951172" Y="22.035134765625" />
                  <Point X="-4.082860351562" Y="22.206138671875" />
                  <Point X="-4.25794140625" Y="22.49972265625" />
                  <Point X="-4.306142089844" Y="22.580548828125" />
                  <Point X="-3.901450683594" Y="22.891080078125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64034375" />
                  <Point X="-3.045557128906" Y="23.672017578125" />
                  <Point X="-3.052560058594" Y="23.701765625" />
                  <Point X="-3.068645996094" Y="23.72775" />
                  <Point X="-3.089480957031" Y="23.751705078125" />
                  <Point X="-3.112976806641" Y="23.771234375" />
                  <Point X="-3.139459472656" Y="23.7868203125" />
                  <Point X="-3.168722900391" Y="23.79804296875" />
                  <Point X="-3.200607421875" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.86555078125" Y="23.722197265625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.783170410156" Y="23.808046875" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.880399902344" Y="24.331224609375" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.439065917969" Y="24.53677734375" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517491699219" Y="24.785171875" />
                  <Point X="-3.487729736328" Y="24.800529296875" />
                  <Point X="-3.470780029297" Y="24.81229296875" />
                  <Point X="-3.459976806641" Y="24.819791015625" />
                  <Point X="-3.437521240234" Y="24.841673828125" />
                  <Point X="-3.418277832031" Y="24.867927734375" />
                  <Point X="-3.404168945312" Y="24.8959296875" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.3906484375" Y="24.95389453125" />
                  <Point X="-3.390647705078" Y="24.983537109375" />
                  <Point X="-3.394916748047" Y="25.011697265625" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.418276123047" Y="25.0695078125" />
                  <Point X="-3.437520263672" Y="25.095763671875" />
                  <Point X="-3.459976806641" Y="25.1176484375" />
                  <Point X="-3.476926513672" Y="25.129412109375" />
                  <Point X="-3.48479296875" Y="25.1343203125" />
                  <Point X="-3.511164306641" Y="25.1490390625" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-4.110453125" Y="25.312609375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.857295898438" Y="25.75526171875" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.731242675781" Y="26.321076171875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.415245605469" Y="26.385310546875" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703138671875" Y="26.305263671875" />
                  <Point X="-3.665623535156" Y="26.317091796875" />
                  <Point X="-3.641712646484" Y="26.324630859375" />
                  <Point X="-3.622784179688" Y="26.332958984375" />
                  <Point X="-3.604040039063" Y="26.343779296875" />
                  <Point X="-3.587355224609" Y="26.35601171875" />
                  <Point X="-3.573714355469" Y="26.37156640625" />
                  <Point X="-3.561299804688" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.536298339844" Y="26.443771484375" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.550213378906" Y="26.62426953125" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-3.933435302734" Y="26.9488046875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.208632324219" Y="27.5152578125" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.834151855469" Y="28.05114453125" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.605610351562" Y="28.0750078125" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187730224609" Y="27.836341796875" />
                  <Point X="-3.167088867188" Y="27.82983203125" />
                  <Point X="-3.146793945312" Y="27.825794921875" />
                  <Point X="-3.094545898438" Y="27.821224609375" />
                  <Point X="-3.061244628906" Y="27.818310546875" />
                  <Point X="-3.040561767578" Y="27.81876171875" />
                  <Point X="-3.019102783203" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.908991943359" Y="27.897314453125" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848007080078" Y="28.0883671875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.016604003906" Y="28.4358125" />
                  <Point X="-3.183332519531" Y="28.72459375" />
                  <Point X="-2.922648681641" Y="28.924458984375" />
                  <Point X="-2.700620605469" Y="29.0946875" />
                  <Point X="-2.311607421875" Y="29.3108125" />
                  <Point X="-2.167036621094" Y="29.3911328125" />
                  <Point X="-2.165327636719" Y="29.38890625" />
                  <Point X="-2.043195678711" Y="29.2297421875" />
                  <Point X="-2.028891967773" Y="29.21480078125" />
                  <Point X="-2.01231237793" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.936963012695" Y="29.159123046875" />
                  <Point X="-1.899898803711" Y="29.139828125" />
                  <Point X="-1.880625732422" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.839269287109" Y="29.123580078125" />
                  <Point X="-1.818622436523" Y="29.12493359375" />
                  <Point X="-1.797307128906" Y="29.128693359375" />
                  <Point X="-1.777452636719" Y="29.134482421875" />
                  <Point X="-1.716883544922" Y="29.159572265625" />
                  <Point X="-1.678278686523" Y="29.1755625" />
                  <Point X="-1.660147094727" Y="29.185509765625" />
                  <Point X="-1.642417236328" Y="29.197923828125" />
                  <Point X="-1.626864868164" Y="29.2115625" />
                  <Point X="-1.614633300781" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.575766235352" Y="29.328447265625" />
                  <Point X="-1.563201171875" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.574420898438" Y="29.557603515625" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.237057861328" Y="29.729224609375" />
                  <Point X="-0.949623718262" Y="29.80980859375" />
                  <Point X="-0.478031585693" Y="29.86500390625" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.251924758911" Y="29.72677734375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907154" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.221438339233" Y="29.56705078125" />
                  <Point X="0.307419616699" Y="29.8879375" />
                  <Point X="0.59307208252" Y="29.858021484375" />
                  <Point X="0.844030639648" Y="29.831740234375" />
                  <Point X="1.234215576172" Y="29.737537109375" />
                  <Point X="1.481038696289" Y="29.6779453125" />
                  <Point X="1.734416503906" Y="29.586044921875" />
                  <Point X="1.894645996094" Y="29.527927734375" />
                  <Point X="2.140220458984" Y="29.41308203125" />
                  <Point X="2.294555908203" Y="29.340904296875" />
                  <Point X="2.531836914062" Y="29.2026640625" />
                  <Point X="2.680977294922" Y="29.1157734375" />
                  <Point X="2.904724853516" Y="28.95665625" />
                  <Point X="2.943259033203" Y="28.92925390625" />
                  <Point X="2.673808349609" Y="28.46255078125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142075683594" Y="27.5399296875" />
                  <Point X="2.133076660156" Y="27.516056640625" />
                  <Point X="2.119964355469" Y="27.4670234375" />
                  <Point X="2.111606933594" Y="27.43576953125" />
                  <Point X="2.108618896484" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.112840820312" Y="27.33855078125" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.166306884766" Y="27.2088046875" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.194469238281" Y="27.170322265625" />
                  <Point X="2.221596923828" Y="27.14559375" />
                  <Point X="2.26026171875" Y="27.119357421875" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304944580078" Y="27.092275390625" />
                  <Point X="2.327031982422" Y="27.0840078125" />
                  <Point X="2.348965087891" Y="27.078662109375" />
                  <Point X="2.391365478516" Y="27.07355078125" />
                  <Point X="2.418389892578" Y="27.070291015625" />
                  <Point X="2.436466308594" Y="27.06984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.522241699219" Y="27.087283203125" />
                  <Point X="2.553494384766" Y="27.095640625" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.169464599609" Y="27.445546875" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.034806152344" Y="27.812408203125" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.2480078125" Y="27.483333984375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.923611328125" Y="27.200076171875" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221423339844" Y="26.660240234375" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.168684570312" Y="26.59558984375" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.13660546875" Y="26.55091015625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.108484375" Y="26.470080078125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.108530517578" Y="26.31946875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282714844" Y="26.23573828125" />
                  <Point X="3.165633056641" Y="26.191126953125" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198893798828" Y="26.14544921875" />
                  <Point X="3.216137695313" Y="26.129359375" />
                  <Point X="3.234345947266" Y="26.116033203125" />
                  <Point X="3.27687890625" Y="26.09208984375" />
                  <Point X="3.303988037109" Y="26.076830078125" />
                  <Point X="3.320530029297" Y="26.06949609375" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.413627441406" Y="26.051837890625" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203369141" Y="26.046984375" />
                  <Point X="4.040048583984" Y="26.119634765625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.807940429688" Y="26.0888828125" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.885243652344" Y="25.680341796875" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.510575195312" Y="25.542341796875" />
                  <Point X="3.716580078125" Y="25.32958984375" />
                  <Point X="3.704788085938" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.625048339844" Y="25.282412109375" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574313232422" Y="25.25109765625" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.513630859375" Y="25.18237890625" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300537109" Y="25.135568359375" />
                  <Point X="3.470526611328" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.452381103516" Y="25.033599609375" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443483154297" Y="24.978125" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.456478759766" Y="24.882443359375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526855469" Y="24.8233359375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.525923828125" Y="24.73939453125" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.560001708984" Y="24.698759765625" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.645534912109" Y="24.6431875" />
                  <Point X="3.681545654297" Y="24.62237109375" />
                  <Point X="3.692698730469" Y="24.616865234375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.222646972656" Y="24.472248046875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.876237792969" Y="24.19198828125" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.804662597656" Y="23.83058984375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.345193359375" Y="23.87533203125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.263771240234" Y="23.970388671875" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163973876953" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.045372314453" Y="23.825427734375" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.960151367188" Y="23.590240234375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.037817626953" Y="23.33655078125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.58026171875" Y="22.8787265625" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.18461328125" Y="22.346984375" />
                  <Point X="4.124813964844" Y="22.250220703125" />
                  <Point X="4.028979980469" Y="22.1140546875" />
                  <Point X="3.621061279297" Y="22.34956640625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.62225" Y="22.8640078125" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.3351953125" Y="22.80712109375" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.146829833984" Y="22.599921875" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.119510009766" Y="22.304767578125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.453605957031" Y="21.6512109375" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.852814453125" Y="20.939044921875" />
                  <Point X="2.781861572266" Y="20.88836328125" />
                  <Point X="2.701763916016" Y="20.83651953125" />
                  <Point X="2.38436328125" Y="21.2501640625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923583984" Y="22.099443359375" />
                  <Point X="1.591761352539" Y="22.183125" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986816406" Y="22.24883203125" />
                  <Point X="1.448366943359" Y="22.2565625" />
                  <Point X="1.417100463867" Y="22.258880859375" />
                  <Point X="1.274745849609" Y="22.24578125" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156363891602" Y="22.2306015625" />
                  <Point X="1.128977661133" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="0.994672485352" Y="22.113138671875" />
                  <Point X="0.924611328125" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.842758056641" Y="21.82298046875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.905266296387" Y="21.027259765625" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.975713867188" Y="20.129923828125" />
                  <Point X="0.929315307617" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05841003418" Y="20.247361328125" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.160300415039" Y="20.688150390625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575317383" Y="20.905138671875" />
                  <Point X="-1.250209594727" Y="20.9290625" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.403467651367" Y="21.065154296875" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735839844" Y="21.15403125" />
                  <Point X="-1.53301940918" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531860352" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.62145715332" Y="21.201552734375" />
                  <Point X="-1.636814331055" Y="21.203830078125" />
                  <Point X="-1.825891601562" Y="21.21622265625" />
                  <Point X="-1.946403442383" Y="21.22412109375" />
                  <Point X="-1.961927856445" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.252986328125" Y="21.07891796875" />
                  <Point X="-2.353403320312" Y="21.011822265625" />
                  <Point X="-2.359686279297" Y="21.007240234375" />
                  <Point X="-2.380448242188" Y="20.98986328125" />
                  <Point X="-2.402761962891" Y="20.967224609375" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.498015380859" Y="20.844279296875" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.586950195313" Y="20.889376953125" />
                  <Point X="-2.747600341797" Y="20.98884765625" />
                  <Point X="-2.980862304688" Y="21.168451171875" />
                  <Point X="-2.796142333984" Y="21.488396484375" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.38076953125" />
                  <Point X="-2.310626953125" Y="22.41170703125" />
                  <Point X="-2.314668701172" Y="22.44239453125" />
                  <Point X="-2.323655029297" Y="22.47201171875" />
                  <Point X="-2.329362060547" Y="22.486451171875" />
                  <Point X="-2.343581542969" Y="22.51528125" />
                  <Point X="-2.351562255859" Y="22.528595703125" />
                  <Point X="-2.369588134766" Y="22.55375" />
                  <Point X="-2.379633300781" Y="22.56558984375" />
                  <Point X="-2.396977294922" Y="22.58293359375" />
                  <Point X="-2.408814453125" Y="22.5929765625" />
                  <Point X="-2.433967529297" Y="22.611001953125" />
                  <Point X="-2.447283447266" Y="22.618984375" />
                  <Point X="-2.476112792969" Y="22.633203125" />
                  <Point X="-2.490559082031" Y="22.638912109375" />
                  <Point X="-2.520178710938" Y="22.647896484375" />
                  <Point X="-2.550859130859" Y="22.651935546875" />
                  <Point X="-2.581795166016" Y="22.650923828125" />
                  <Point X="-2.597224121094" Y="22.6491484375" />
                  <Point X="-2.628751953125" Y="22.642876953125" />
                  <Point X="-2.643682617188" Y="22.63861328125" />
                  <Point X="-2.6726484375" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.188600097656" Y="22.331291015625" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.877106689453" Y="22.092673828125" />
                  <Point X="-4.004019775391" Y="22.259412109375" />
                  <Point X="-4.176348632812" Y="22.548380859375" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.843618164062" Y="22.8157109375" />
                  <Point X="-3.048122070312" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748535156" Y="23.631626953125" />
                  <Point X="-2.948577880859" Y="23.646955078125" />
                  <Point X="-2.950787353516" Y="23.67862890625" />
                  <Point X="-2.953084960938" Y="23.693787109375" />
                  <Point X="-2.960087890625" Y="23.72353515625" />
                  <Point X="-2.971785400391" Y="23.75176953125" />
                  <Point X="-2.987871337891" Y="23.77775390625" />
                  <Point X="-2.996965087891" Y="23.79009375" />
                  <Point X="-3.017800048828" Y="23.814048828125" />
                  <Point X="-3.028756347656" Y="23.824763671875" />
                  <Point X="-3.052252197266" Y="23.84429296875" />
                  <Point X="-3.064791748047" Y="23.853107421875" />
                  <Point X="-3.091274414062" Y="23.868693359375" />
                  <Point X="-3.105442382812" Y="23.875521484375" />
                  <Point X="-3.134705810547" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891138671875" />
                  <Point X="-3.181685791016" Y="23.897619140625" />
                  <Point X="-3.197297851562" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-3.877950683594" Y="23.816384765625" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.691125488281" Y="23.83155859375" />
                  <Point X="-4.740762695312" Y="24.025884765625" />
                  <Point X="-4.786356933594" Y="24.34467578125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.414478027344" Y="24.445013671875" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.500472412109" Y="24.690287109375" />
                  <Point X="-3.473928710938" Y="24.700748046875" />
                  <Point X="-3.444166748047" Y="24.71610546875" />
                  <Point X="-3.433563720703" Y="24.722484375" />
                  <Point X="-3.416614013672" Y="24.734248046875" />
                  <Point X="-3.393675048828" Y="24.75175390625" />
                  <Point X="-3.371219482422" Y="24.77363671875" />
                  <Point X="-3.360899658203" Y="24.78551171875" />
                  <Point X="-3.34165625" Y="24.811765625" />
                  <Point X="-3.333438476563" Y="24.825181640625" />
                  <Point X="-3.319329589844" Y="24.85318359375" />
                  <Point X="-3.313438476562" Y="24.86776953125" />
                  <Point X="-3.3041875" Y="24.897576171875" />
                  <Point X="-3.300991455078" Y="24.911494140625" />
                  <Point X="-3.296721923828" Y="24.93965234375" />
                  <Point X="-3.2956484375" Y="24.953892578125" />
                  <Point X="-3.295647705078" Y="24.98353515625" />
                  <Point X="-3.296720947266" Y="24.997775390625" />
                  <Point X="-3.300989990234" Y="25.025935546875" />
                  <Point X="-3.304185791016" Y="25.03985546875" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.319327636719" Y="25.084251953125" />
                  <Point X="-3.333436035156" Y="25.11225390625" />
                  <Point X="-3.341653564453" Y="25.12566796875" />
                  <Point X="-3.360897705078" Y="25.151923828125" />
                  <Point X="-3.371216796875" Y="25.163798828125" />
                  <Point X="-3.393673339844" Y="25.18568359375" />
                  <Point X="-3.405810791016" Y="25.195693359375" />
                  <Point X="-3.422760498047" Y="25.20745703125" />
                  <Point X="-3.438493408203" Y="25.2172734375" />
                  <Point X="-3.464864746094" Y="25.2319921875" />
                  <Point X="-3.475443115234" Y="25.237068359375" />
                  <Point X="-3.497155029297" Y="25.24587890625" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-4.085865478516" Y="25.404373046875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.763319335938" Y="25.74135546875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.639549804688" Y="26.296228515625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.427645996094" Y="26.291123046875" />
                  <Point X="-3.778066650391" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703613281" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704888916016" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.674572509766" Y="26.21466015625" />
                  <Point X="-3.637057373047" Y="26.22648828125" />
                  <Point X="-3.613146484375" Y="26.23402734375" />
                  <Point X="-3.603454101562" Y="26.23767578125" />
                  <Point X="-3.584525634766" Y="26.24600390625" />
                  <Point X="-3.575289550781" Y="26.25068359375" />
                  <Point X="-3.556545410156" Y="26.26150390625" />
                  <Point X="-3.547869873047" Y="26.2671640625" />
                  <Point X="-3.531185058594" Y="26.279396484375" />
                  <Point X="-3.515929931641" Y="26.293375" />
                  <Point X="-3.5022890625" Y="26.3089296875" />
                  <Point X="-3.495894042969" Y="26.317078125" />
                  <Point X="-3.483479492188" Y="26.33480859375" />
                  <Point X="-3.478011230469" Y="26.3436015625" />
                  <Point X="-3.468062988281" Y="26.361734375" />
                  <Point X="-3.463583007813" Y="26.37107421875" />
                  <Point X="-3.448529785156" Y="26.407416015625" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.447784423828" Y="26.63324609375" />
                  <Point X="-3.465947509766" Y="26.66813671875" />
                  <Point X="-3.477524169922" Y="26.690375" />
                  <Point X="-3.482802246094" Y="26.6992890625" />
                  <Point X="-3.494293945312" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-3.875603027344" Y="27.024173828125" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.1265859375" Y="27.467369140625" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.759171142578" Y="27.992810546875" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.653110351562" Y="27.992736328125" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244926025391" Y="27.757720703125" />
                  <Point X="-3.225999267578" Y="27.749390625" />
                  <Point X="-3.216303466797" Y="27.745740234375" />
                  <Point X="-3.195662109375" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165328369141" Y="27.73262109375" />
                  <Point X="-3.155072265625" Y="27.73115625" />
                  <Point X="-3.10282421875" Y="27.7265859375" />
                  <Point X="-3.069522949219" Y="27.723671875" />
                  <Point X="-3.059172851562" Y="27.723333984375" />
                  <Point X="-3.038489990234" Y="27.72378515625" />
                  <Point X="-3.028157226562" Y="27.72457421875" />
                  <Point X="-3.006698242188" Y="27.727400390625" />
                  <Point X="-2.996521240234" Y="27.729310546875" />
                  <Point X="-2.976432373047" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902749267578" Y="27.77319140625" />
                  <Point X="-2.886616699219" Y="27.786138671875" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.841817138672" Y="27.830138671875" />
                  <Point X="-2.8181796875" Y="27.853775390625" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753368652344" Y="28.096646484375" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.934331787109" Y="28.4833125" />
                  <Point X="-3.059387451172" Y="28.6999140625" />
                  <Point X="-2.864846191406" Y="28.849068359375" />
                  <Point X="-2.648375488281" Y="29.01503515625" />
                  <Point X="-2.265470214844" Y="29.227767578125" />
                  <Point X="-2.1925234375" Y="29.268294921875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111819335938" Y="29.164046875" />
                  <Point X="-2.097515625" Y="29.14910546875" />
                  <Point X="-2.089957275391" Y="29.14202734375" />
                  <Point X="-2.073377685547" Y="29.128115234375" />
                  <Point X="-2.065095214844" Y="29.12190234375" />
                  <Point X="-2.047897827148" Y="29.11041015625" />
                  <Point X="-2.038982666016" Y="29.105130859375" />
                  <Point X="-1.980830810547" Y="29.074857421875" />
                  <Point X="-1.943766479492" Y="29.0555625" />
                  <Point X="-1.934335205078" Y="29.0512890625" />
                  <Point X="-1.915062133789" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313720703" Y="29.034966796875" />
                  <Point X="-1.874174926758" Y="29.032833984375" />
                  <Point X="-1.853724975586" Y="29.029685546875" />
                  <Point X="-1.83305480957" Y="29.028783203125" />
                  <Point X="-1.812407958984" Y="29.03013671875" />
                  <Point X="-1.802120239258" Y="29.031376953125" />
                  <Point X="-1.780805053711" Y="29.03513671875" />
                  <Point X="-1.77071484375" Y="29.037490234375" />
                  <Point X="-1.750860351562" Y="29.043279296875" />
                  <Point X="-1.741096069336" Y="29.04671484375" />
                  <Point X="-1.680526977539" Y="29.0718046875" />
                  <Point X="-1.64192199707" Y="29.087794921875" />
                  <Point X="-1.632584960938" Y="29.0922734375" />
                  <Point X="-1.614453369141" Y="29.102220703125" />
                  <Point X="-1.605658813477" Y="29.107689453125" />
                  <Point X="-1.587928955078" Y="29.120103515625" />
                  <Point X="-1.579780395508" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252807617" Y="29.155388671875" />
                  <Point X="-1.538021240234" Y="29.1720703125" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153918457" Y="29.19948828125" />
                  <Point X="-1.516856445312" Y="29.208728515625" />
                  <Point X="-1.508525512695" Y="29.227662109375" />
                  <Point X="-1.504877441406" Y="29.23735546875" />
                  <Point X="-1.485163208008" Y="29.299880859375" />
                  <Point X="-1.472598144531" Y="29.339732421875" />
                  <Point X="-1.470026977539" Y="29.349763671875" />
                  <Point X="-1.465991577148" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.211411743164" Y="29.637751953125" />
                  <Point X="-0.931163818359" Y="29.7163203125" />
                  <Point X="-0.46698815918" Y="29.7706484375" />
                  <Point X="-0.365222595215" Y="29.78255859375" />
                  <Point X="-0.343687774658" Y="29.702189453125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.31320123291" Y="29.542462890625" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.583176940918" Y="29.7635390625" />
                  <Point X="0.827851867676" Y="29.737916015625" />
                  <Point X="1.211920166016" Y="29.645189453125" />
                  <Point X="1.453622314453" Y="29.586833984375" />
                  <Point X="1.702024658203" Y="29.49673828125" />
                  <Point X="1.858249511719" Y="29.44007421875" />
                  <Point X="2.099976074219" Y="29.32702734375" />
                  <Point X="2.250426269531" Y="29.256666015625" />
                  <Point X="2.484013916016" Y="29.120578125" />
                  <Point X="2.629434326172" Y="29.03585546875" />
                  <Point X="2.817778320312" Y="28.9019140625" />
                  <Point X="2.591535888672" Y="28.51005078125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062371826172" Y="27.5931015625" />
                  <Point X="2.053181640625" Y="27.573439453125" />
                  <Point X="2.044182617188" Y="27.54956640625" />
                  <Point X="2.041301513672" Y="27.540599609375" />
                  <Point X="2.028189208984" Y="27.49156640625" />
                  <Point X="2.019831787109" Y="27.4603125" />
                  <Point X="2.017912231445" Y="27.45146484375" />
                  <Point X="2.013646484375" Y="27.42021875" />
                  <Point X="2.012755615234" Y="27.38323828125" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.018524047852" Y="27.327177734375" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040735717773" Y="27.234216796875" />
                  <Point X="2.045319580078" Y="27.22388671875" />
                  <Point X="2.055681396484" Y="27.20384375" />
                  <Point X="2.061459228516" Y="27.194130859375" />
                  <Point X="2.087695068359" Y="27.15546484375" />
                  <Point X="2.104416992188" Y="27.130822265625" />
                  <Point X="2.109806884766" Y="27.123634765625" />
                  <Point X="2.130470458984" Y="27.100115234375" />
                  <Point X="2.157598144531" Y="27.07538671875" />
                  <Point X="2.168254882812" Y="27.066982421875" />
                  <Point X="2.206919677734" Y="27.04074609375" />
                  <Point X="2.231563476562" Y="27.0240234375" />
                  <Point X="2.241279052734" Y="27.018244140625" />
                  <Point X="2.261318115234" Y="27.007884765625" />
                  <Point X="2.271641601562" Y="27.0033046875" />
                  <Point X="2.293729003906" Y="26.995037109375" />
                  <Point X="2.304536376953" Y="26.991708984375" />
                  <Point X="2.326469482422" Y="26.98636328125" />
                  <Point X="2.337595214844" Y="26.984345703125" />
                  <Point X="2.379995605469" Y="26.979234375" />
                  <Point X="2.407020019531" Y="26.975974609375" />
                  <Point X="2.416040039062" Y="26.9753203125" />
                  <Point X="2.447575439453" Y="26.97549609375" />
                  <Point X="2.484317138672" Y="26.979822265625" />
                  <Point X="2.497751708984" Y="26.98239453125" />
                  <Point X="2.546785400391" Y="26.9955078125" />
                  <Point X="2.578038085938" Y="27.003865234375" />
                  <Point X="2.583998291016" Y="27.005671875" />
                  <Point X="2.604414794922" Y="27.0130703125" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.216964599609" Y="27.363275390625" />
                  <Point X="3.940403076172" Y="27.780951171875" />
                  <Point X="3.957693603516" Y="27.756921875" />
                  <Point X="4.043957275391" Y="27.63703515625" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.865779052734" Y="27.2754453125" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.152115478516" Y="26.725212890625" />
                  <Point X="3.134666503906" Y="26.706599609375" />
                  <Point X="3.128577636719" Y="26.699421875" />
                  <Point X="3.093287841797" Y="26.653384765625" />
                  <Point X="3.070795410156" Y="26.624041015625" />
                  <Point X="3.065636230469" Y="26.6166015625" />
                  <Point X="3.049738769531" Y="26.58937109375" />
                  <Point X="3.034763183594" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.016994628906" Y="26.495666015625" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.015490478516" Y="26.300271484375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682617188" Y="26.228611328125" />
                  <Point X="3.050285888672" Y="26.19537109375" />
                  <Point X="3.056918701172" Y="26.1835234375" />
                  <Point X="3.086269042969" Y="26.138912109375" />
                  <Point X="3.104976318359" Y="26.110478515625" />
                  <Point X="3.111740478516" Y="26.101421875" />
                  <Point X="3.126293945313" Y="26.084177734375" />
                  <Point X="3.134083251953" Y="26.075990234375" />
                  <Point X="3.151327148438" Y="26.059900390625" />
                  <Point X="3.160030761719" Y="26.052697265625" />
                  <Point X="3.178239013672" Y="26.03937109375" />
                  <Point X="3.187743652344" Y="26.033248046875" />
                  <Point X="3.230276611328" Y="26.0093046875" />
                  <Point X="3.257385742188" Y="25.994044921875" />
                  <Point X="3.265483886719" Y="25.989982421875" />
                  <Point X="3.294692871094" Y="25.978076171875" />
                  <Point X="3.330282958984" Y="25.968017578125" />
                  <Point X="3.343674072266" Y="25.965255859375" />
                  <Point X="3.401181396484" Y="25.95765625" />
                  <Point X="3.437834716797" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603027344" Y="25.952796875" />
                  <Point X="4.052448242188" Y="26.025447265625" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.715636230469" Y="26.066412109375" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.485987304688" Y="25.63410546875" />
                  <Point X="3.6919921875" Y="25.421353515625" />
                  <Point X="3.686022705078" Y="25.419541015625" />
                  <Point X="3.665625732422" Y="25.41213671875" />
                  <Point X="3.642385253906" Y="25.40162109375" />
                  <Point X="3.6340078125" Y="25.397318359375" />
                  <Point X="3.577508544922" Y="25.364662109375" />
                  <Point X="3.541497802734" Y="25.34384765625" />
                  <Point X="3.533880615234" Y="25.3389453125" />
                  <Point X="3.50877734375" Y="25.319873046875" />
                  <Point X="3.481994384766" Y="25.2943515625" />
                  <Point X="3.472795410156" Y="25.284224609375" />
                  <Point X="3.438895996094" Y="25.24102734375" />
                  <Point X="3.417289550781" Y="25.21349609375" />
                  <Point X="3.410854248047" Y="25.20420703125" />
                  <Point X="3.399130371094" Y="25.184927734375" />
                  <Point X="3.393841796875" Y="25.1749375" />
                  <Point X="3.384067871094" Y="25.15347265625" />
                  <Point X="3.380004394531" Y="25.14292578125" />
                  <Point X="3.373158691406" Y="25.12142578125" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.359076660156" Y="25.05146875" />
                  <Point X="3.351874267578" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584472656" Y="24.97373828125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.363174560547" Y="24.86457421875" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380005126953" Y="24.79451171875" />
                  <Point X="3.384068603516" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.762501953125" />
                  <Point X="3.399130126953" Y="24.75251171875" />
                  <Point X="3.410853515625" Y="24.733232421875" />
                  <Point X="3.417289550781" Y="24.723943359375" />
                  <Point X="3.451188964844" Y="24.68074609375" />
                  <Point X="3.472795410156" Y="24.65321484375" />
                  <Point X="3.478715820312" Y="24.646369140625" />
                  <Point X="3.501144287109" Y="24.624189453125" />
                  <Point X="3.530178222656" Y="24.6012734375" />
                  <Point X="3.541495849609" Y="24.59359375" />
                  <Point X="3.597995117188" Y="24.5609375" />
                  <Point X="3.634005859375" Y="24.54012109375" />
                  <Point X="3.639492675781" Y="24.537185546875" />
                  <Point X="3.659139648438" Y="24.527990234375" />
                  <Point X="3.683020996094" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.198059082031" Y="24.380484375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.782299316406" Y="24.206150390625" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="4.357593261719" Y="23.96951953125" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481933594" Y="24.087322265625" />
                  <Point X="3.243593994141" Y="24.063220703125" />
                  <Point X="3.172917724609" Y="24.047859375" />
                  <Point X="3.157874023438" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114675537109" Y="24.024609375" />
                  <Point X="3.086848876953" Y="24.007716796875" />
                  <Point X="3.074124023438" Y="23.99846875" />
                  <Point X="3.050374023438" Y="23.977998046875" />
                  <Point X="3.039348876953" Y="23.966775390625" />
                  <Point X="2.972323974609" Y="23.8861640625" />
                  <Point X="2.929604736328" Y="23.834787109375" />
                  <Point X="2.921325683594" Y="23.82315234375" />
                  <Point X="2.906605957031" Y="23.798771484375" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.865551025391" Y="23.5989453125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.957907714844" Y="23.28517578125" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.522429199219" Y="22.803357421875" />
                  <Point X="4.087170166016" Y="22.370015625" />
                  <Point X="4.045500976562" Y="22.30258984375" />
                  <Point X="4.001272460938" Y="22.239748046875" />
                  <Point X="3.668561279297" Y="22.431837890625" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815026123047" Y="22.920484375" />
                  <Point X="2.78312109375" Y="22.930671875" />
                  <Point X="2.771107910156" Y="22.933662109375" />
                  <Point X="2.639133544922" Y="22.95749609375" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444847167969" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589355469" Y="22.948890625" />
                  <Point X="2.290951171875" Y="22.891189453125" />
                  <Point X="2.221071289062" Y="22.854412109375" />
                  <Point X="2.208968261719" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.06276171875" Y="22.644166015625" />
                  <Point X="2.025984619141" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.026022338867" Y="22.287884765625" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.371333496094" Y="21.6037109375" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.72375390625" Y="20.963916015625" />
                  <Point X="2.459731933594" Y="21.30799609375" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808834350586" Y="22.15037109375" />
                  <Point X="1.783251098633" Y="22.17199609375" />
                  <Point X="1.773298217773" Y="22.179353515625" />
                  <Point X="1.643135864258" Y="22.26303515625" />
                  <Point X="1.560174682617" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.323755859375" />
                  <Point X="1.517466674805" Y="22.336126953125" />
                  <Point X="1.502548095703" Y="22.34111328125" />
                  <Point X="1.470928344727" Y="22.34884375" />
                  <Point X="1.455391723633" Y="22.351302734375" />
                  <Point X="1.424125244141" Y="22.35362109375" />
                  <Point X="1.408395263672" Y="22.35348046875" />
                  <Point X="1.266040649414" Y="22.340380859375" />
                  <Point X="1.17530847168" Y="22.332033203125" />
                  <Point X="1.161225341797" Y="22.32966015625" />
                  <Point X="1.133575561523" Y="22.322828125" />
                  <Point X="1.120008911133" Y="22.318369140625" />
                  <Point X="1.092622680664" Y="22.307025390625" />
                  <Point X="1.079876953125" Y="22.3005859375" />
                  <Point X="1.055494384766" Y="22.285865234375" />
                  <Point X="1.043857666016" Y="22.277583984375" />
                  <Point X="0.933934875488" Y="22.186185546875" />
                  <Point X="0.863873718262" Y="22.127931640625" />
                  <Point X="0.852653930664" Y="22.11691015625" />
                  <Point X="0.832183776855" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.749925598145" Y="21.843158203125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.811079040527" Y="21.014859375" />
                  <Point X="0.833091003418" Y="20.847662109375" />
                  <Point X="0.655065124512" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407287598" Y="21.58679296875" />
                  <Point X="0.520413024902" Y="21.730865234375" />
                  <Point X="0.456679840088" Y="21.822693359375" />
                  <Point X="0.446668609619" Y="21.83483203125" />
                  <Point X="0.424781646729" Y="21.8572890625" />
                  <Point X="0.412905792236" Y="21.867607421875" />
                  <Point X="0.386650512695" Y="21.886849609375" />
                  <Point X="0.373235961914" Y="21.89506640625" />
                  <Point X="0.345237640381" Y="21.909171875" />
                  <Point X="0.330653869629" Y="21.915060546875" />
                  <Point X="0.175918243408" Y="21.963083984375" />
                  <Point X="0.077294761658" Y="21.993693359375" />
                  <Point X="0.063380245209" Y="21.996888671875" />
                  <Point X="0.035227500916" Y="22.001158203125" />
                  <Point X="0.020989276886" Y="22.002232421875" />
                  <Point X="-0.008651480675" Y="22.002234375" />
                  <Point X="-0.02289535141" Y="22.001162109375" />
                  <Point X="-0.051061767578" Y="21.996892578125" />
                  <Point X="-0.064984169006" Y="21.9936953125" />
                  <Point X="-0.219719772339" Y="21.945669921875" />
                  <Point X="-0.318343261719" Y="21.9150625" />
                  <Point X="-0.332928527832" Y="21.909171875" />
                  <Point X="-0.360928619385" Y="21.895064453125" />
                  <Point X="-0.37434362793" Y="21.88684765625" />
                  <Point X="-0.400599487305" Y="21.867603515625" />
                  <Point X="-0.4124765625" Y="21.857283203125" />
                  <Point X="-0.434360839844" Y="21.834826171875" />
                  <Point X="-0.44436819458" Y="21.822689453125" />
                  <Point X="-0.544362731934" Y="21.678615234375" />
                  <Point X="-0.60809576416" Y="21.5867890625" />
                  <Point X="-0.612469482422" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.798684753418" Y="20.930119140625" />
                  <Point X="-0.985425598145" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.394156148918" Y="29.783335709319" />
                  <Point X="0.377184295036" Y="29.781251827693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.341236420272" Y="29.69304085562" />
                  <Point X="-1.478055268247" Y="29.553457052545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814852734489" Y="29.739277321433" />
                  <Point X="0.350665591641" Y="29.682282306922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.31640702083" Y="29.60037608911" />
                  <Point X="-1.465654758957" Y="29.459266210215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.082524285055" Y="29.676429821817" />
                  <Point X="0.324146888247" Y="29.583312786152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.291577621387" Y="29.507711322599" />
                  <Point X="-1.467324649127" Y="29.363347740065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.345315492646" Y="29.612983091432" />
                  <Point X="0.297628148026" Y="29.484343260859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.266748221945" Y="29.415046556088" />
                  <Point X="-1.496458417209" Y="29.264057129725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.129617310515" Y="29.186314993029" />
                  <Point X="-2.399792234429" Y="29.153141683629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.569518564502" Y="29.544798333744" />
                  <Point X="0.271109381922" Y="29.385373732389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241918822502" Y="29.322381789578" />
                  <Point X="-1.545136184713" Y="29.162366817998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.033696864987" Y="29.102379109396" />
                  <Point X="-2.620946691844" Y="29.030273897265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.766667573014" Y="29.473291754768" />
                  <Point X="0.244590615818" Y="29.286404203919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212794671411" Y="29.230244352263" />
                  <Point X="-1.76203201521" Y="29.04002192527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.848184797595" Y="29.029443693714" />
                  <Point X="-2.77858404784" Y="28.915205030309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.945049666257" Y="29.399480888342" />
                  <Point X="0.198891000961" Y="29.185079563356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.149426551427" Y="29.142311545632" />
                  <Point X="-2.927227950911" Y="28.80124042052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.107152416864" Y="29.323671169977" />
                  <Point X="-3.053664961314" Y="28.690002474298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266185730341" Y="29.247484572123" />
                  <Point X="-3.002062576112" Y="28.600625017088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401875674313" Y="29.168431768894" />
                  <Point X="-2.950460190909" Y="28.511247559878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.537565151168" Y="29.08937890831" />
                  <Point X="-2.898857850138" Y="28.421870097212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666499302099" Y="29.009496597999" />
                  <Point X="-2.847255529568" Y="28.332492632066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.78127219631" Y="28.927875504" />
                  <Point X="-2.795653208998" Y="28.243115166921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.777219697708" Y="28.83166448632" />
                  <Point X="-2.760047006632" Y="28.151773625425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723644012985" Y="28.033458790113" />
                  <Point X="-3.727959054202" Y="28.032928969671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717743236764" Y="28.72864826176" />
                  <Point X="-2.749926396576" Y="28.057302846668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.586936655008" Y="27.954530909615" />
                  <Point X="-3.810288009655" Y="27.927106811608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658266775819" Y="28.6256320372" />
                  <Point X="-2.757049607635" Y="27.960714792908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.450229560596" Y="27.875602996757" />
                  <Point X="-3.892616583022" Y="27.821284700459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.598790314875" Y="28.52261581264" />
                  <Point X="-2.814432214617" Y="27.857955661287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.313522466184" Y="27.796675083898" />
                  <Point X="-3.974945156389" Y="27.71546258931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.539313913038" Y="28.419599595338" />
                  <Point X="-2.947718135377" Y="27.745876774614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103995184228" Y="27.726688365791" />
                  <Point X="-4.042483376957" Y="27.611456505134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479837519413" Y="28.316583379044" />
                  <Point X="-4.102663494779" Y="27.508353882374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420361125788" Y="28.21356716275" />
                  <Point X="-4.162842994476" Y="27.40525133551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.360884732163" Y="28.110550946456" />
                  <Point X="-4.223022086341" Y="27.302148838721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.301408338537" Y="28.007534730163" />
                  <Point X="-4.128289879987" Y="27.218067057663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.241931944912" Y="27.904518513869" />
                  <Point X="-4.02076007234" Y="27.13555662446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.182455551287" Y="27.801502297575" />
                  <Point X="-3.913230264693" Y="27.053046191257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.122979157662" Y="27.698486081281" />
                  <Point X="-3.805700508042" Y="26.970535751793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063646744729" Y="27.595487543593" />
                  <Point X="-3.69817077884" Y="26.888025308959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973949574123" Y="27.734329804273" />
                  <Point X="3.828778709572" Y="27.716505063413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029254840822" Y="27.495551315354" />
                  <Point X="-3.590641049639" Y="26.805514866124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.037228873388" Y="27.646386091829" />
                  <Point X="3.618217502284" Y="27.594937964615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013107742247" Y="27.397855267527" />
                  <Point X="-3.497827977281" Y="26.721197445041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.092138527718" Y="27.557414716207" />
                  <Point X="3.407656294996" Y="27.473370865816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021419864996" Y="27.303162434451" />
                  <Point X="-3.447090232702" Y="26.631713823312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111633677039" Y="27.464094986137" />
                  <Point X="3.197095179096" Y="27.351803778238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.051883789442" Y="27.211189500619" />
                  <Point X="-3.42173183975" Y="26.539114009037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.963134271726" Y="27.350148118443" />
                  <Point X="2.986534940266" Y="27.230236798351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.110636892164" Y="27.122690041119" />
                  <Point X="-3.4350879987" Y="26.441760645506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.540704904324" Y="26.306007959222" />
                  <Point X="-4.640210542157" Y="26.293790203174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.814635240555" Y="27.236201296687" />
                  <Point X="2.775974701435" Y="27.108669818464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.209349857065" Y="27.039097035752" />
                  <Point X="-3.479913081974" Y="26.340543383921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.164527466868" Y="26.256483307284" />
                  <Point X="-4.667039779652" Y="26.19478255361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666136921583" Y="27.122254562379" />
                  <Point X="-3.642009059058" Y="26.224927067132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.788347563196" Y="26.206958958158" />
                  <Point X="-4.693869017147" Y="26.095774904046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51763860261" Y="27.00830782807" />
                  <Point X="-4.720698254642" Y="25.996767254482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369140283638" Y="26.894361093762" />
                  <Point X="-4.740039560147" Y="25.89867900736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220641964665" Y="26.780414359454" />
                  <Point X="-4.754464738848" Y="25.801194384709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.106555375473" Y="26.670692854276" />
                  <Point X="-4.768889926091" Y="25.703709761009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.039734331048" Y="26.566774828259" />
                  <Point X="-4.783315126912" Y="25.606225135641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.009060335421" Y="26.467295101756" />
                  <Point X="-4.57667002489" Y="25.535884530337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002483850822" Y="26.370774177564" />
                  <Point X="-4.331712857717" Y="25.47024805513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020242401869" Y="26.277241220037" />
                  <Point X="-4.086755690543" Y="25.404611579923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.05560511938" Y="26.185869782361" />
                  <Point X="-3.841795881524" Y="25.338975429094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.115084580763" Y="26.097459508491" />
                  <Point X="-3.596836062869" Y="25.273339279447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220673304871" Y="26.014710740198" />
                  <Point X="-3.412406431056" Y="25.200270957389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707093055791" Y="26.101506703214" />
                  <Point X="3.565064985542" Y="25.96128328807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48919090913" Y="25.951967122913" />
                  <Point X="-3.334575513798" Y="25.11411395897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729717654202" Y="26.008571221177" />
                  <Point X="-3.300482301182" Y="25.022586645692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752342874324" Y="25.915635815476" />
                  <Point X="-3.298625001379" Y="24.927101260015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767085572224" Y="25.821732557746" />
                  <Point X="-3.332401036672" Y="24.827240650933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781707925459" Y="25.727814523548" />
                  <Point X="-3.440619236816" Y="24.718239693328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.222054058424" Y="25.563384235808" />
                  <Point X="-4.015358896559" Y="24.551957103155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.629942527303" Y="25.394968648036" />
                  <Point X="-4.67469618766" Y="24.375287229962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.469091840781" Y="25.279505233701" />
                  <Point X="-4.775279557063" Y="24.267223711697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.393655989581" Y="25.174529442417" />
                  <Point X="-4.761826643224" Y="24.173162088397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.363607342283" Y="25.075126499034" />
                  <Point X="-4.748373729385" Y="24.079100465097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.348834091846" Y="24.977599138547" />
                  <Point X="-4.730468946264" Y="23.985585462612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.359605762952" Y="24.883208300036" />
                  <Point X="-4.706764090194" Y="23.892782619537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.38166531866" Y="24.790203439478" />
                  <Point X="-4.683059186944" Y="23.799979782255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435240968731" Y="24.701068268729" />
                  <Point X="-3.15405874807" Y="23.892003996344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.512922688821" Y="24.614892951202" />
                  <Point X="-3.016992371375" Y="23.813120197802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.643665222886" Y="24.53523268242" />
                  <Point X="-2.960426389452" Y="23.724352193636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872757704354" Y="24.467648268744" />
                  <Point X="-2.948893411479" Y="23.630054831854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.117716565543" Y="24.402012001536" />
                  <Point X="-2.971508224521" Y="23.531564648546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362675727776" Y="24.336375771291" />
                  <Point X="-3.047703226542" Y="23.426495645261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.607635036936" Y="24.270739559087" />
                  <Point X="-3.196110305857" Y="23.312560113774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780812648051" Y="24.196289662608" />
                  <Point X="3.670563996175" Y="24.059968269394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.058248676092" Y="23.984785401683" />
                  <Point X="-3.344608956502" Y="23.198613338742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766110088939" Y="24.098770981924" />
                  <Point X="4.046742066975" Y="24.01044369522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.965246971158" Y="23.877652794761" />
                  <Point X="-3.493107607147" Y="23.084666563709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746014200349" Y="24.000590083649" />
                  <Point X="4.422920406041" Y="23.960919153985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89489457018" Y="23.77330117268" />
                  <Point X="-3.641606257792" Y="22.970719788676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.872534934658" Y="23.674842321232" />
                  <Point X="-3.790104908437" Y="22.856773013643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.863626850593" Y="23.578035112623" />
                  <Point X="-3.938603181445" Y="22.742826284979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86212511868" Y="23.482137289711" />
                  <Point X="-4.087101241697" Y="22.628879582437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.891540441434" Y="23.390035603781" />
                  <Point X="-4.161805131509" Y="22.52399366471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.947601879974" Y="23.301205649477" />
                  <Point X="-2.480078455175" Y="22.634770302804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.712356237321" Y="22.606250177316" />
                  <Point X="-4.108620050724" Y="22.434810538082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005831143507" Y="23.212641870613" />
                  <Point X="-2.368846074412" Y="22.552714488416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.922916758096" Y="22.48468316281" />
                  <Point X="-4.055434969939" Y="22.345627411455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098886137938" Y="23.128354153825" />
                  <Point X="-2.320887250295" Y="22.462889658158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.133477278871" Y="22.363116148305" />
                  <Point X="-4.001802612803" Y="22.256499203457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.206415641591" Y="23.045843683297" />
                  <Point X="2.577515180255" Y="22.9686244163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.395657619586" Y="22.946295115566" />
                  <Point X="-2.313092263266" Y="22.368133328799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.344036950526" Y="22.241549238058" />
                  <Point X="-3.935176877832" Y="22.168966381652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313945145244" Y="22.963333212768" />
                  <Point X="2.847132067144" Y="22.90601577395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179129222813" Y="22.823995338027" />
                  <Point X="-2.345806019212" Y="22.268403151221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.554596321058" Y="22.119982364785" />
                  <Point X="-3.868551165045" Y="22.081433557123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421474648896" Y="22.88082274224" />
                  <Point X="2.983944970498" Y="22.827100852795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.102050323575" Y="22.71881780581" />
                  <Point X="-2.405282357173" Y="22.165386941761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.765155691589" Y="21.998415491512" />
                  <Point X="-3.801925602837" Y="21.993900714105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529004154321" Y="22.798312271929" />
                  <Point X="3.120652242818" Y="22.748172961781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.048196956618" Y="22.616492010377" />
                  <Point X="-2.464758695135" Y="22.062370732302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636533686952" Y="22.715801804958" />
                  <Point X="3.257359515139" Y="22.669245070766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006560465239" Y="22.515666258647" />
                  <Point X="-2.524235033097" Y="21.959354522843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744063219584" Y="22.633291337988" />
                  <Point X="3.394066787459" Y="22.590317179752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002266213207" Y="22.419425557378" />
                  <Point X="1.450440399408" Y="22.351669867136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.095058899631" Y="22.308034505733" />
                  <Point X="-2.583711371059" Y="21.856338313383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.851592752216" Y="22.550780871018" />
                  <Point X="3.53077405978" Y="22.511389288737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019176964596" Y="22.325788503143" />
                  <Point X="1.621480399645" Y="22.276957505043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.942897817728" Y="22.193638040686" />
                  <Point X="-2.643187709021" Y="21.753322103924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959122284847" Y="22.468270404047" />
                  <Point X="3.6674813321" Y="22.432461397723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.036087514023" Y="22.232151424111" />
                  <Point X="1.746483799812" Y="22.196592559225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825135593461" Y="22.08346522427" />
                  <Point X="0.082030859159" Y="21.992223435764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.169059326947" Y="21.961393437516" />
                  <Point X="-2.702664046982" Y="21.650305894465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.066651817479" Y="22.385759937077" />
                  <Point X="3.804189086985" Y="22.35353356596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063139752523" Y="22.139759587918" />
                  <Point X="1.84230293084" Y="22.112644235736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.780153543521" Y="21.982228689601" />
                  <Point X="0.303003566758" Y="21.923642039219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.435939835159" Y="21.832911198083" />
                  <Point X="-2.762140384944" Y="21.547289685005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.033837147675" Y="22.286017368835" />
                  <Point X="3.940896845711" Y="22.274605734669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113532893908" Y="22.050233654237" />
                  <Point X="1.909422759319" Y="22.025172080985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758779115071" Y="21.883890806371" />
                  <Point X="0.43732308566" Y="21.84442096895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.510016279287" Y="21.728102320999" />
                  <Point X="-2.821616687833" Y="21.444273479852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.16513512239" Y="21.960856177784" />
                  <Point X="1.976542587799" Y="21.937699926235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.737404852138" Y="21.785552943463" />
                  <Point X="0.502477261789" Y="21.756707462438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.582635265149" Y="21.623472397288" />
                  <Point X="-2.881092943907" Y="21.341257280448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216737350872" Y="21.871478701331" />
                  <Point X="2.043662416279" Y="21.850227771484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724736134255" Y="21.688283987082" />
                  <Point X="0.563690978667" Y="21.668510128368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.639810932107" Y="21.520738674708" />
                  <Point X="-2.940569199981" Y="21.238241081043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268339579353" Y="21.782101224878" />
                  <Point X="2.110782244758" Y="21.762755616733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734858465343" Y="21.593813419641" />
                  <Point X="0.62434592338" Y="21.580244185702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.666968561762" Y="21.421690703657" />
                  <Point X="-2.946282730011" Y="21.141826114348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319941807835" Y="21.692723748425" />
                  <Point X="2.177902073238" Y="21.675283461982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747258898432" Y="21.499622567955" />
                  <Point X="0.661229287197" Y="21.489059459915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.69348736589" Y="21.322721170517" />
                  <Point X="-1.653089776533" Y="21.204896809885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.157054475499" Y="21.143017725612" />
                  <Point X="-2.839070750135" Y="21.059276656802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371544036045" Y="21.603346271939" />
                  <Point X="2.245021901717" Y="21.587811307231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759659331522" Y="21.405431716268" />
                  <Point X="0.686058708961" Y="21.396394696145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720006170019" Y="21.223751637378" />
                  <Point X="-1.478164729847" Y="21.130661471515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.332549439473" Y="21.025756220101" />
                  <Point X="-2.728659604115" Y="20.977120007467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.423146197787" Y="21.513968787292" />
                  <Point X="2.312141730197" Y="21.50033915248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772059764612" Y="21.311240864581" />
                  <Point X="0.710888130725" Y="21.303729932375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.746524974147" Y="21.124782104239" />
                  <Point X="-1.382428141398" Y="21.046703013071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442573210747" Y="20.916533566238" />
                  <Point X="-2.599658888878" Y="20.897245870225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.474748359529" Y="21.424591302644" />
                  <Point X="2.379261558677" Y="21.412866997729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784460197702" Y="21.217050012895" />
                  <Point X="0.735717552488" Y="21.211065168605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.773043778275" Y="21.0258125711" />
                  <Point X="-1.286692415356" Y="20.962744448738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52635052127" Y="21.335213817997" />
                  <Point X="2.446381387156" Y="21.325394842979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796860630792" Y="21.122859161208" />
                  <Point X="0.760546974252" Y="21.118400404835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.799562583186" Y="20.926843037865" />
                  <Point X="-1.212376438114" Y="20.876155869953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.577952683012" Y="21.245836333349" />
                  <Point X="2.513501162491" Y="21.237922681702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809261063882" Y="21.028668309522" />
                  <Point X="0.785376396016" Y="21.025735641065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.826081410958" Y="20.827873501823" />
                  <Point X="-1.179462334424" Y="20.784483780303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629554844754" Y="21.156458848702" />
                  <Point X="2.580620924629" Y="21.150450518806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.821661532267" Y="20.934477462169" />
                  <Point X="0.81020581778" Y="20.933070877295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.85260023873" Y="20.72890396578" />
                  <Point X="-1.160877633496" Y="20.691052261228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.681157006496" Y="21.067081364054" />
                  <Point X="2.647740686768" Y="21.062978355909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.879119066501" Y="20.629934429738" />
                  <Point X="-1.142293096176" Y="20.597620722063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.732759168238" Y="20.977703879407" />
                  <Point X="2.714860448907" Y="20.975506193013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.905637894273" Y="20.530964893696" />
                  <Point X="-1.1237085641" Y="20.504189182255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932156722045" Y="20.431995357654" />
                  <Point X="-1.122826293978" Y="20.408584077986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.958675549817" Y="20.333025821611" />
                  <Point X="-1.135634433683" Y="20.311298002758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.985194377589" Y="20.234056285569" />
                  <Point X="-0.98805897009" Y="20.233704557837" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.716188598633" Y="20.54984375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.364324401855" Y="21.622529296875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.733599609375" />
                  <Point X="0.119600318909" Y="21.781623046875" />
                  <Point X="0.02097677803" Y="21.812232421875" />
                  <Point X="-0.008663995743" Y="21.812234375" />
                  <Point X="-0.163399581909" Y="21.764208984375" />
                  <Point X="-0.262023132324" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.388273406982" Y="21.570283203125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.615158813477" Y="20.880943359375" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.985042541504" Y="20.039572265625" />
                  <Point X="-1.100231201172" Y="20.061931640625" />
                  <Point X="-1.279380126953" Y="20.108025390625" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.338600341797" Y="20.225265625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.346649658203" Y="20.651083984375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.528743652344" Y="20.922306640625" />
                  <Point X="-1.619543701172" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.838318115234" Y="21.02662890625" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878295898" Y="21.026208984375" />
                  <Point X="-2.147427734375" Y="20.9209375" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.347277587891" Y="20.728615234375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.686971923828" Y="20.727833984375" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.103881835938" Y="21.023376953125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.960687255859" Y="21.583396484375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513982421875" Y="22.43123828125" />
                  <Point X="-2.531326416016" Y="22.44858203125" />
                  <Point X="-2.560155761719" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.093600097656" Y="22.166748046875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.028293212891" Y="21.977595703125" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.339534179688" Y="22.451064453125" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-3.959282958984" Y="22.96644921875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326904297" Y="23.66540625" />
                  <Point X="-3.161161865234" Y="23.689361328125" />
                  <Point X="-3.18764453125" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-3.853150878906" Y="23.628009765625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.875215332031" Y="23.78453515625" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.974442871094" Y="24.3177734375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.463653808594" Y="24.628541015625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541895751953" Y="24.87857421875" />
                  <Point X="-3.524946044922" Y="24.890337890625" />
                  <Point X="-3.514142822266" Y="24.8978359375" />
                  <Point X="-3.494899414062" Y="24.92408984375" />
                  <Point X="-3.4856484375" Y="24.953896484375" />
                  <Point X="-3.485647705078" Y="24.9835390625" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.514142822266" Y="25.039603515625" />
                  <Point X="-3.531092529297" Y="25.0513671875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.135040527344" Y="25.220845703125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.951272460938" Y="25.76916796875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.822935546875" Y="26.345923828125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.402845214844" Y="26.479498046875" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704833984" Y="26.3958671875" />
                  <Point X="-3.694189697266" Y="26.4076953125" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534667969" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.624066894531" Y="26.480126953125" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.634479248047" Y="26.58040234375" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.991267578125" Y="26.873435546875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.290678710937" Y="27.563146484375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.909132568359" Y="28.109478515625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.558110351562" Y="28.157279296875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515625" Y="27.92043359375" />
                  <Point X="-3.086267578125" Y="27.91586328125" />
                  <Point X="-3.052966308594" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.976166748047" Y="27.964490234375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942645507812" Y="28.080087890625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.098876220703" Y="28.3883125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.980450927734" Y="28.9998515625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.357744628906" Y="29.393857421875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.089958496094" Y="29.44673828125" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247070312" Y="29.273662109375" />
                  <Point X="-1.893095214844" Y="29.243388671875" />
                  <Point X="-1.856031005859" Y="29.22409375" />
                  <Point X="-1.835124633789" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.753240234375" Y="29.24733984375" />
                  <Point X="-1.714635375977" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.666369262695" Y="29.357013671875" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.668608154297" Y="29.545203125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.262703979492" Y="29.820697265625" />
                  <Point X="-0.968083312988" Y="29.903296875" />
                  <Point X="-0.489074951172" Y="29.959359375" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.16016178894" Y="29.751365234375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282123566" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594043732" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.129675430298" Y="29.591638671875" />
                  <Point X="0.236648406982" Y="29.9908671875" />
                  <Point X="0.602967041016" Y="29.95250390625" />
                  <Point X="0.860209960938" Y="29.925564453125" />
                  <Point X="1.256510864258" Y="29.829884765625" />
                  <Point X="1.508455322266" Y="29.769056640625" />
                  <Point X="1.766808349609" Y="29.6753515625" />
                  <Point X="1.931043457031" Y="29.61578125" />
                  <Point X="2.18046484375" Y="29.49913671875" />
                  <Point X="2.338683837891" Y="29.425142578125" />
                  <Point X="2.579659667969" Y="29.28475" />
                  <Point X="2.732521240234" Y="29.19569140625" />
                  <Point X="2.959781494141" Y="29.034076171875" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.756080810547" Y="28.41505078125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851806641" Y="27.491513671875" />
                  <Point X="2.211739501953" Y="27.44248046875" />
                  <Point X="2.203382080078" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.207157470703" Y="27.349923828125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682861328" Y="27.300810546875" />
                  <Point X="2.244918701172" Y="27.26214453125" />
                  <Point X="2.261640625" Y="27.237501953125" />
                  <Point X="2.274938964844" Y="27.224205078125" />
                  <Point X="2.313603759766" Y="27.19796875" />
                  <Point X="2.338247558594" Y="27.18124609375" />
                  <Point X="2.360334960938" Y="27.172978515625" />
                  <Point X="2.402735351562" Y="27.1678671875" />
                  <Point X="2.429759765625" Y="27.164607421875" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.497697998047" Y="27.17905859375" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.121964599609" Y="27.527818359375" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.111918457031" Y="27.86789453125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.329284667969" Y="27.532517578125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.981443603516" Y="27.12470703125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.244081298828" Y="26.537794921875" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.199974121094" Y="26.444494140625" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.201570556641" Y="26.338666015625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.244997070312" Y="26.243341796875" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280948242188" Y="26.198818359375" />
                  <Point X="3.323481201172" Y="26.174875" />
                  <Point X="3.350590332031" Y="26.159615234375" />
                  <Point X="3.368566162109" Y="26.153619140625" />
                  <Point X="3.426073486328" Y="26.14601953125" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.027648925781" Y="26.213822265625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.900244628906" Y="26.111353515625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.979112792969" Y="25.69495703125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.535163085938" Y="25.450578125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087402344" Y="25.232818359375" />
                  <Point X="3.672588134766" Y="25.200162109375" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.588365722656" Y="25.12373046875" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.545685546875" Y="25.01573046875" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.549782958984" Y="24.9003125" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759277344" Y="24.841240234375" />
                  <Point X="3.600658691406" Y="24.79804296875" />
                  <Point X="3.622265136719" Y="24.77051171875" />
                  <Point X="3.636575439453" Y="24.75809375" />
                  <Point X="3.693074707031" Y="24.7254375" />
                  <Point X="3.729085449219" Y="24.70462109375" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.247234863281" Y="24.56401171875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.970176269531" Y="24.177826171875" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.897281738281" Y="23.809455078125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.332793457031" Y="23.78114453125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.283948486328" Y="23.877556640625" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.118420654297" Y="23.76469140625" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.054751708984" Y="23.58153515625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.117727539062" Y="23.38792578125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.638094238281" Y="22.954095703125" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.265426757813" Y="22.29704296875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.098350585938" Y="22.04755859375" />
                  <Point X="4.056687744141" Y="21.988361328125" />
                  <Point X="3.573561279297" Y="22.267294921875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.605366455078" Y="22.77051953125" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.379439453125" Y="22.723052734375" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.230897949219" Y="22.555677734375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.212997558594" Y="22.321650390625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.535878417969" Y="21.6987109375" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.908031494141" Y="20.861740234375" />
                  <Point X="2.835295654297" Y="20.80978515625" />
                  <Point X="2.717027832031" Y="20.733234375" />
                  <Point X="2.679774902344" Y="20.70912109375" />
                  <Point X="2.308994628906" Y="21.19233203125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.54038684082" Y="22.10321484375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805664062" Y="22.16428125" />
                  <Point X="1.283451049805" Y="22.151181640625" />
                  <Point X="1.19271887207" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.05541003418" Y="22.040091796875" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.935590454102" Y="21.802802734375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.999453491211" Y="21.03966015625" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="1.063155029297" Y="20.0518359375" />
                  <Point X="0.994361572266" Y="20.036755859375" />
                  <Point X="0.885088317871" Y="20.016904296875" />
                  <Point X="0.860200500488" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#178" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.118021454921" Y="4.795586562198" Z="1.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="-0.501043024775" Y="5.040299717792" Z="1.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.55" />
                  <Point X="-1.282357687583" Y="4.900121759192" Z="1.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.55" />
                  <Point X="-1.724336386163" Y="4.56995757033" Z="1.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.55" />
                  <Point X="-1.720211036827" Y="4.403329113849" Z="1.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.55" />
                  <Point X="-1.778529953687" Y="4.324813330738" Z="1.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.55" />
                  <Point X="-1.876163249083" Y="4.319019117196" Z="1.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.55" />
                  <Point X="-2.056446899308" Y="4.508456521585" Z="1.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.55" />
                  <Point X="-2.388183621896" Y="4.46884544635" Z="1.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.55" />
                  <Point X="-3.017028955026" Y="4.070812093102" Z="1.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.55" />
                  <Point X="-3.148333406096" Y="3.39459305617" Z="1.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.55" />
                  <Point X="-2.998611174216" Y="3.107011645774" Z="1.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.55" />
                  <Point X="-3.017677487723" Y="3.031126080164" Z="1.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.55" />
                  <Point X="-3.088064819832" Y="2.996953524957" Z="1.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.55" />
                  <Point X="-3.539266292127" Y="3.23186054265" Z="1.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.55" />
                  <Point X="-3.954751861485" Y="3.171462395404" Z="1.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.55" />
                  <Point X="-4.340016845627" Y="2.619632240479" Z="1.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.55" />
                  <Point X="-4.027861517212" Y="1.865048931163" Z="1.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.55" />
                  <Point X="-3.684985641435" Y="1.588595583434" Z="1.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.55" />
                  <Point X="-3.676416802243" Y="1.530541524281" Z="1.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.55" />
                  <Point X="-3.71538088746" Y="1.486661014917" Z="1.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.55" />
                  <Point X="-4.402475017386" Y="1.56035125137" Z="1.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.55" />
                  <Point X="-4.877351399051" Y="1.390282691317" Z="1.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.55" />
                  <Point X="-5.006889904506" Y="0.807766176404" Z="1.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.55" />
                  <Point X="-4.154137206738" Y="0.203830183435" Z="1.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.55" />
                  <Point X="-3.565757546443" Y="0.041571001793" Z="1.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.55" />
                  <Point X="-3.545206734462" Y="0.018204184908" Z="1.55" />
                  <Point X="-3.539556741714" Y="0" Z="1.55" />
                  <Point X="-3.543157866673" Y="-0.011602766153" Z="1.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.55" />
                  <Point X="-3.559611048681" Y="-0.037304981341" Z="1.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.55" />
                  <Point X="-4.482750582034" Y="-0.291881873321" Z="1.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.55" />
                  <Point X="-5.030094754285" Y="-0.658024069008" Z="1.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.55" />
                  <Point X="-4.929815897462" Y="-1.196560254025" Z="1.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.55" />
                  <Point X="-3.85278082431" Y="-1.390281202411" Z="1.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.55" />
                  <Point X="-3.208850178109" Y="-1.312930565919" Z="1.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.55" />
                  <Point X="-3.195675387082" Y="-1.334029375552" Z="1.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.55" />
                  <Point X="-3.995876832783" Y="-1.962602717623" Z="1.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.55" />
                  <Point X="-4.38863388554" Y="-2.543263560259" Z="1.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.55" />
                  <Point X="-4.074235197089" Y="-3.021406532522" Z="1.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.55" />
                  <Point X="-3.074755964804" Y="-2.845272573507" Z="1.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.55" />
                  <Point X="-2.566086345136" Y="-2.562244123995" Z="1.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.55" />
                  <Point X="-3.010144439661" Y="-3.360322417688" Z="1.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.55" />
                  <Point X="-3.14054178508" Y="-3.984959453628" Z="1.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.55" />
                  <Point X="-2.719449454302" Y="-4.283397243072" Z="1.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.55" />
                  <Point X="-2.313765938117" Y="-4.270541259882" Z="1.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.55" />
                  <Point X="-2.125805349915" Y="-4.089355634877" Z="1.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.55" />
                  <Point X="-1.847744157858" Y="-3.99198313957" Z="1.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.55" />
                  <Point X="-1.56786684341" Y="-4.084005510939" Z="1.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.55" />
                  <Point X="-1.401844838139" Y="-4.327390250207" Z="1.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.55" />
                  <Point X="-1.394328550946" Y="-4.736927212299" Z="1.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.55" />
                  <Point X="-1.297994838808" Y="-4.909118563218" Z="1.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.55" />
                  <Point X="-1.000726345569" Y="-4.978230696624" Z="1.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="-0.573018567347" Y="-4.100718473139" Z="1.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="-0.35335351798" Y="-3.426945598979" Z="1.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="-0.154735568874" Y="-3.252263578741" Z="1.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.55" />
                  <Point X="0.098623510487" Y="-3.234848466547" Z="1.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.55" />
                  <Point X="0.317092341452" Y="-3.374700156789" Z="1.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.55" />
                  <Point X="0.661736293054" Y="-4.431817556677" Z="1.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.55" />
                  <Point X="0.887868717649" Y="-5.00101005716" Z="1.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.55" />
                  <Point X="1.067705040214" Y="-4.965724299135" Z="1.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.55" />
                  <Point X="1.042869833251" Y="-3.922533267899" Z="1.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.55" />
                  <Point X="0.978293714694" Y="-3.176536780252" Z="1.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.55" />
                  <Point X="1.081221163383" Y="-2.967072466998" Z="1.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.55" />
                  <Point X="1.281875960835" Y="-2.867326179368" Z="1.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.55" />
                  <Point X="1.507191690448" Y="-2.907562997413" Z="1.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.55" />
                  <Point X="2.263170898313" Y="-3.806825698108" Z="1.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.55" />
                  <Point X="2.738041433093" Y="-4.277460674389" Z="1.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.55" />
                  <Point X="2.93093747506" Y="-4.147667598255" Z="1.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.55" />
                  <Point X="2.573023641388" Y="-3.245008378544" Z="1.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.55" />
                  <Point X="2.256045572873" Y="-2.638182442426" Z="1.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.55" />
                  <Point X="2.26898839744" Y="-2.43632833393" Z="1.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.55" />
                  <Point X="2.396570044443" Y="-2.289912912527" Z="1.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.55" />
                  <Point X="2.590324361087" Y="-2.247402436618" Z="1.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.55" />
                  <Point X="3.542405218563" Y="-2.744725858887" Z="1.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.55" />
                  <Point X="4.133083261921" Y="-2.949939030082" Z="1.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.55" />
                  <Point X="4.301804487935" Y="-2.697961302153" Z="1.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.55" />
                  <Point X="3.662376219975" Y="-1.974955456041" Z="1.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.55" />
                  <Point X="3.153629477095" Y="-1.553754787591" Z="1.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.55" />
                  <Point X="3.098385217061" Y="-1.391765537903" Z="1.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.55" />
                  <Point X="3.150710848212" Y="-1.235994066061" Z="1.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.55" />
                  <Point X="3.288411945016" Y="-1.140022371516" Z="1.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.55" />
                  <Point X="4.320111075638" Y="-1.237147511655" Z="1.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.55" />
                  <Point X="4.939872922308" Y="-1.170389692535" Z="1.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.55" />
                  <Point X="5.0134616596" Y="-0.79834708072" Z="1.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.55" />
                  <Point X="4.254019898193" Y="-0.356410927814" Z="1.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.55" />
                  <Point X="3.711941914116" Y="-0.199995757355" Z="1.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.55" />
                  <Point X="3.633836243364" Y="-0.139806485476" Z="1.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.55" />
                  <Point X="3.5927345666" Y="-0.05900355159" Z="1.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.55" />
                  <Point X="3.588636883824" Y="0.037606979612" Z="1.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.55" />
                  <Point X="3.621543195037" Y="0.124142253099" Z="1.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.55" />
                  <Point X="3.691453500237" Y="0.188153086887" Z="1.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.55" />
                  <Point X="4.541947919114" Y="0.433561004955" Z="1.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.55" />
                  <Point X="5.022361765904" Y="0.733928586454" Z="1.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.55" />
                  <Point X="4.942669791251" Y="1.154460949136" Z="1.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.55" />
                  <Point X="4.014966667806" Y="1.294675911422" Z="1.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.55" />
                  <Point X="3.42646802802" Y="1.226868274631" Z="1.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.55" />
                  <Point X="3.341851485414" Y="1.249728673451" Z="1.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.55" />
                  <Point X="3.280611486515" Y="1.302104948285" Z="1.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.55" />
                  <Point X="3.244383108912" Y="1.380050150967" Z="1.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.55" />
                  <Point X="3.241970528283" Y="1.462308695524" Z="1.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.55" />
                  <Point X="3.277608492453" Y="1.538656949862" Z="1.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.55" />
                  <Point X="4.005725716726" Y="2.116320365526" Z="1.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.55" />
                  <Point X="4.365905962201" Y="2.58968546636" Z="1.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.55" />
                  <Point X="4.146347771355" Y="2.928378884458" Z="1.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.55" />
                  <Point X="3.090808647623" Y="2.602399260608" Z="1.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.55" />
                  <Point X="2.478625386154" Y="2.258641433115" Z="1.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.55" />
                  <Point X="2.402567070001" Y="2.248787839295" Z="1.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.55" />
                  <Point X="2.335522998684" Y="2.270622343624" Z="1.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.55" />
                  <Point X="2.280136256611" Y="2.321501861698" Z="1.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.55" />
                  <Point X="2.250641863464" Y="2.387191373832" Z="1.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.55" />
                  <Point X="2.253886509543" Y="2.460844189727" Z="1.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.55" />
                  <Point X="2.793226092252" Y="3.421330434332" Z="1.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.55" />
                  <Point X="2.982602554955" Y="4.106105384685" Z="1.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.55" />
                  <Point X="2.598673966999" Y="4.359232747806" Z="1.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.55" />
                  <Point X="2.195490571222" Y="4.575707326365" Z="1.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.55" />
                  <Point X="1.777701298091" Y="4.753635544382" Z="1.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.55" />
                  <Point X="1.262089428417" Y="4.909768926881" Z="1.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.55" />
                  <Point X="0.602018932247" Y="5.033513438341" Z="1.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.55" />
                  <Point X="0.075223106632" Y="4.635860943753" Z="1.55" />
                  <Point X="0" Y="4.355124473572" Z="1.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>