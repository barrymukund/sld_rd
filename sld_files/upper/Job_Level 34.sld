<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#183" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2481" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.823722473145" Y="20.51557421875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557720153809" Y="21.502861328125" />
                  <Point X="0.54236315918" Y="21.532623046875" />
                  <Point X="0.435922729492" Y="21.685982421875" />
                  <Point X="0.378635375977" Y="21.7685234375" />
                  <Point X="0.356752502441" Y="21.7909765625" />
                  <Point X="0.330497314453" Y="21.810220703125" />
                  <Point X="0.302495025635" Y="21.824330078125" />
                  <Point X="0.137784515381" Y="21.87544921875" />
                  <Point X="0.049135883331" Y="21.902962890625" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008657706261" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.201534286499" Y="21.85184375" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318183410645" Y="21.810224609375" />
                  <Point X="-0.344439331055" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.47276385498" Y="21.615162109375" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.691150817871" Y="20.964388671875" />
                  <Point X="-0.916584838867" Y="20.12305859375" />
                  <Point X="-0.978742004395" Y="20.135123046875" />
                  <Point X="-1.079353271484" Y="20.15465234375" />
                  <Point X="-1.24641796875" Y="20.19763671875" />
                  <Point X="-1.246311523438" Y="20.1984453125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.255858154297" Y="20.681595703125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868796875" />
                  <Point X="-1.47528918457" Y="21.00178515625" />
                  <Point X="-1.556905639648" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.844293579102" Y="21.122224609375" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.210363037109" Y="20.993142578125" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.413792236328" Y="20.797986328125" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.654258544922" Y="20.81931640625" />
                  <Point X="-2.801712890625" Y="20.910615234375" />
                  <Point X="-3.062044677734" Y="21.1110625" />
                  <Point X="-3.104721435547" Y="21.143921875" />
                  <Point X="-2.907723144531" Y="21.4851328125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575195312" Y="22.4148046875" />
                  <Point X="-2.414559326172" Y="22.444421875" />
                  <Point X="-2.428776367188" Y="22.473251953125" />
                  <Point X="-2.446806396484" Y="22.4984140625" />
                  <Point X="-2.464155029297" Y="22.51576171875" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.090336181641" Y="22.278328125" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.966365966797" Y="22.05308984375" />
                  <Point X="-4.082862060547" Y="22.206142578125" />
                  <Point X="-4.269498535156" Y="22.519103515625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.952730957031" Y="22.85173046875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64034375" />
                  <Point X="-3.045556884766" Y="23.672015625" />
                  <Point X="-3.05255859375" Y="23.70176171875" />
                  <Point X="-3.068641845703" Y="23.727744140625" />
                  <Point X="-3.089475830078" Y="23.751701171875" />
                  <Point X="-3.112975585938" Y="23.771234375" />
                  <Point X="-3.139458251953" Y="23.7868203125" />
                  <Point X="-3.168722412109" Y="23.798044921875" />
                  <Point X="-3.200608398438" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.801466796875" Y="23.730634765625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.788515136719" Y="23.828970703125" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.883457519531" Y="24.352603515625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.497481933594" Y="24.521125" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517485351562" Y="24.78517578125" />
                  <Point X="-3.487729736328" Y="24.800529296875" />
                  <Point X="-3.4696875" Y="24.81305078125" />
                  <Point X="-3.459976806641" Y="24.819791015625" />
                  <Point X="-3.437520263672" Y="24.84167578125" />
                  <Point X="-3.418278076172" Y="24.867927734375" />
                  <Point X="-3.404168457031" Y="24.895931640625" />
                  <Point X="-3.398154296875" Y="24.915310546875" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.3906484375" Y="24.95389453125" />
                  <Point X="-3.390647705078" Y="24.983537109375" />
                  <Point X="-3.394916748047" Y="25.011697265625" />
                  <Point X="-3.400930908203" Y="25.031076171875" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.418276367188" Y="25.0695078125" />
                  <Point X="-3.437519287109" Y="25.09576171875" />
                  <Point X="-3.459976806641" Y="25.1176484375" />
                  <Point X="-3.478019042969" Y="25.130169921875" />
                  <Point X="-3.486252929688" Y="25.13528125" />
                  <Point X="-3.511532958984" Y="25.149244140625" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-4.052036621094" Y="25.29695703125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.853851074219" Y="25.778541015625" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.725087890625" Y="26.3437890625" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.457119140625" Y="26.39082421875" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744985839844" Y="26.299341796875" />
                  <Point X="-3.723422607422" Y="26.301228515625" />
                  <Point X="-3.703138427734" Y="26.305263671875" />
                  <Point X="-3.663205078125" Y="26.317853515625" />
                  <Point X="-3.641712402344" Y="26.324630859375" />
                  <Point X="-3.622774658203" Y="26.332962890625" />
                  <Point X="-3.60403125" Y="26.34378515625" />
                  <Point X="-3.587351806641" Y="26.356015625" />
                  <Point X="-3.573714599609" Y="26.37156640625" />
                  <Point X="-3.56130078125" Y="26.389294921875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.535328125" Y="26.44611328125" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.515804443359" Y="26.52875" />
                  <Point X="-3.518951660156" Y="26.5491953125" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532050292969" Y="26.589376953125" />
                  <Point X="-3.551384277344" Y="26.626517578125" />
                  <Point X="-3.561790039062" Y="26.646505859375" />
                  <Point X="-3.573281494141" Y="26.663705078125" />
                  <Point X="-3.587193603516" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-3.899927734375" Y="26.92309375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.195247558594" Y="27.538189453125" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.817847167969" Y="28.0721015625" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.631328369141" Y="28.08985546875" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187729980469" Y="27.836341796875" />
                  <Point X="-3.167088378906" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.091177978516" Y="27.8209296875" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040561523438" Y="27.81876171875" />
                  <Point X="-3.019102783203" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.906601074219" Y="27.899705078125" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848301757812" Y="28.091736328125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.001755859375" Y="28.41009375" />
                  <Point X="-3.183333007812" Y="28.72459375" />
                  <Point X="-2.899336425781" Y="28.94233203125" />
                  <Point X="-2.700621337891" Y="29.094685546875" />
                  <Point X="-2.285928466797" Y="29.325080078125" />
                  <Point X="-2.167037353516" Y="29.3911328125" />
                  <Point X="-2.043195922852" Y="29.2297421875" />
                  <Point X="-2.028892700195" Y="29.21480078125" />
                  <Point X="-2.012313476562" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.933214355469" Y="29.157171875" />
                  <Point X="-1.899898803711" Y="29.139828125" />
                  <Point X="-1.880625732422" Y="29.13233203125" />
                  <Point X="-1.859719116211" Y="29.126728515625" />
                  <Point X="-1.839269287109" Y="29.123580078125" />
                  <Point X="-1.818622802734" Y="29.12493359375" />
                  <Point X="-1.797307373047" Y="29.128693359375" />
                  <Point X="-1.777452880859" Y="29.134482421875" />
                  <Point X="-1.712979370117" Y="29.161189453125" />
                  <Point X="-1.678279052734" Y="29.1755625" />
                  <Point X="-1.660147094727" Y="29.185509765625" />
                  <Point X="-1.642417236328" Y="29.197923828125" />
                  <Point X="-1.626864868164" Y="29.2115625" />
                  <Point X="-1.614633300781" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.574495239258" Y="29.332478515625" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.572732666016" Y="29.54478125" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.206878540039" Y="29.737685546875" />
                  <Point X="-0.949623840332" Y="29.80980859375" />
                  <Point X="-0.446900909424" Y="29.868646484375" />
                  <Point X="-0.294711456299" Y="29.886458984375" />
                  <Point X="-0.259532806396" Y="29.755169921875" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425933838" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.213830307007" Y="29.538658203125" />
                  <Point X="0.307419525146" Y="29.8879375" />
                  <Point X="0.619423217773" Y="29.85526171875" />
                  <Point X="0.844045227051" Y="29.83173828125" />
                  <Point X="1.259970581055" Y="29.7313203125" />
                  <Point X="1.481023681641" Y="29.677951171875" />
                  <Point X="1.751239135742" Y="29.579943359375" />
                  <Point X="1.894645751953" Y="29.527927734375" />
                  <Point X="2.156430175781" Y="29.4055" />
                  <Point X="2.294559082031" Y="29.340900390625" />
                  <Point X="2.547496337891" Y="29.193541015625" />
                  <Point X="2.680988525391" Y="29.115767578125" />
                  <Point X="2.919493408203" Y="28.94615625" />
                  <Point X="2.943260742188" Y="28.92925390625" />
                  <Point X="2.707730712891" Y="28.5213046875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.119119140625" Y="27.463859375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.113170410156" Y="27.335818359375" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442138672" Y="27.289603515625" />
                  <Point X="2.129708740234" Y="27.267513671875" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.167997802734" Y="27.2063125" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.194470214844" Y="27.170322265625" />
                  <Point X="2.221597167969" Y="27.14559375" />
                  <Point X="2.262754394531" Y="27.117666015625" />
                  <Point X="2.284905761719" Y="27.102634765625" />
                  <Point X="2.3049453125" Y="27.092275390625" />
                  <Point X="2.327033203125" Y="27.0840078125" />
                  <Point X="2.348964599609" Y="27.078662109375" />
                  <Point X="2.394098144531" Y="27.073220703125" />
                  <Point X="2.418389404297" Y="27.070291015625" />
                  <Point X="2.436467285156" Y="27.06984375" />
                  <Point X="2.473208251953" Y="27.074169921875" />
                  <Point X="2.525402832031" Y="27.08812890625" />
                  <Point X="2.553494628906" Y="27.095640625" />
                  <Point X="2.565284667969" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.110709472656" Y="27.411625" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.044094238281" Y="27.7995" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.256241210938" Y="27.469728515625" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.9682734375" Y="27.23434765625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221421630859" Y="26.66023828125" />
                  <Point X="3.203974121094" Y="26.641626953125" />
                  <Point X="3.166409423828" Y="26.59262109375" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.136607421875" Y="26.5509140625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.107636962891" Y="26.46705078125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.109226074219" Y="26.31609765625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.235740234375" />
                  <Point X="3.167525390625" Y="26.18825390625" />
                  <Point X="3.184340576172" Y="26.1626953125" />
                  <Point X="3.198893066406" Y="26.145451171875" />
                  <Point X="3.21613671875" Y="26.129361328125" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.279620849609" Y="26.090548828125" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320522216797" Y="26.0695" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.417334472656" Y="26.05134765625" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462698974609" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="3.984235351562" Y="26.112287109375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.8119296875" Y="26.07249609375" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.887838378906" Y="25.663677734375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.561758789062" Y="25.5560546875" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704787597656" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.62140625" Y="25.280306640625" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574310302734" Y="25.251095703125" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.511445556641" Y="25.179595703125" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300537109" Y="25.13556640625" />
                  <Point X="3.470527099609" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.45165234375" Y="25.029796875" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.45720703125" Y="24.878638671875" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526855469" Y="24.8233359375" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.528109130859" Y="24.736611328125" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559997802734" Y="24.698765625" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.649177001953" Y="24.64108203125" />
                  <Point X="3.681545654297" Y="24.62237109375" />
                  <Point X="3.692698486328" Y="24.616865234375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.171463867187" Y="24.485962890625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.874010253906" Y="24.17721484375" />
                  <Point X="4.855021484375" Y="24.051267578125" />
                  <Point X="4.801338378906" Y="23.816021484375" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="4.404551757812" Y="23.867517578125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.256623046875" Y="23.968833984375" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163975830078" Y="23.943404296875" />
                  <Point X="3.136148193359" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.041051757813" Y="23.820232421875" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.959531982422" Y="23.58351171875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.041773681641" Y="23.3303984375" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.532762939453" Y="22.915173828125" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.178334472656" Y="22.33682421875" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.028979980469" Y="22.1140546875" />
                  <Point X="3.673927978516" Y="22.31904296875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786128417969" Y="22.82998828125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.613742675781" Y="22.865544921875" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474610839844" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.328127685547" Y="22.80340234375" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242384277344" Y="22.753451171875" />
                  <Point X="2.221425048828" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.143110595703" Y="22.59285546875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.121046386719" Y="22.296259765625" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.423083251953" Y="21.704078125" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.845363769531" Y="20.93372265625" />
                  <Point X="2.781833251953" Y="20.88834375" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="2.424705566406" Y="21.197587890625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.583370849609" Y="22.18851953125" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448366333008" Y="22.2565625" />
                  <Point X="1.417100463867" Y="22.258880859375" />
                  <Point X="1.265569335938" Y="22.2449375" />
                  <Point X="1.184013549805" Y="22.23743359375" />
                  <Point X="1.15636328125" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.2045390625" />
                  <Point X="0.987586608887" Y="22.10725" />
                  <Point X="0.92461151123" Y="22.05488671875" />
                  <Point X="0.904140441895" Y="22.031134765625" />
                  <Point X="0.887248596191" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.840639404297" Y="21.813232421875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.896616210938" Y="21.092962890625" />
                  <Point X="1.022065551758" Y="20.140083984375" />
                  <Point X="0.975708374023" Y="20.129921875" />
                  <Point X="0.929315734863" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058445068359" Y="20.2473671875" />
                  <Point X="-1.141246337891" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.16268359375" Y="20.70012890625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573242188" Y="20.905138671875" />
                  <Point X="-1.250208251953" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94022265625" />
                  <Point X="-1.412651245117" Y="21.0732109375" />
                  <Point X="-1.494267822266" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.533023681641" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621457275391" Y="21.201552734375" />
                  <Point X="-1.636814575195" Y="21.203830078125" />
                  <Point X="-1.838080444336" Y="21.217021484375" />
                  <Point X="-1.946403686523" Y="21.22412109375" />
                  <Point X="-1.961928100586" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.263142089844" Y="21.0721328125" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.489160888672" Y="20.855818359375" />
                  <Point X="-2.503200195312" Y="20.837521484375" />
                  <Point X="-2.604247558594" Y="20.900087890625" />
                  <Point X="-2.747599365234" Y="20.988845703125" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.825450683594" Y="21.4376328125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665771484" Y="22.442380859375" />
                  <Point X="-2.323649902344" Y="22.471998046875" />
                  <Point X="-2.329355957031" Y="22.4864375" />
                  <Point X="-2.343572998047" Y="22.515267578125" />
                  <Point X="-2.3515546875" Y="22.5285859375" />
                  <Point X="-2.369584716797" Y="22.553748046875" />
                  <Point X="-2.379633056641" Y="22.565591796875" />
                  <Point X="-2.396981689453" Y="22.582939453125" />
                  <Point X="-2.408822021484" Y="22.592984375" />
                  <Point X="-2.433978027344" Y="22.611009765625" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.137836181641" Y="22.360599609375" />
                  <Point X="-3.793088134766" Y="21.9822890625" />
                  <Point X="-3.890772460938" Y="22.110626953125" />
                  <Point X="-4.004015380859" Y="22.25940625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.894898681641" Y="22.776361328125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748535156" Y="23.631626953125" />
                  <Point X="-2.948577880859" Y="23.646955078125" />
                  <Point X="-2.950787109375" Y="23.678626953125" />
                  <Point X="-2.953083984375" Y="23.69378125" />
                  <Point X="-2.960085693359" Y="23.72352734375" />
                  <Point X="-2.971781982422" Y="23.751763671875" />
                  <Point X="-2.987865234375" Y="23.77774609375" />
                  <Point X="-2.99695703125" Y="23.790083984375" />
                  <Point X="-3.017791015625" Y="23.814041015625" />
                  <Point X="-3.02875" Y="23.8247578125" />
                  <Point X="-3.052249755859" Y="23.844291015625" />
                  <Point X="-3.064790527344" Y="23.853107421875" />
                  <Point X="-3.091273193359" Y="23.868693359375" />
                  <Point X="-3.105436767578" Y="23.87551953125" />
                  <Point X="-3.134700927734" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891142578125" />
                  <Point X="-3.1816875" Y="23.897623046875" />
                  <Point X="-3.1973046875" Y="23.89946875" />
                  <Point X="-3.228625488281" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.813866699219" Y="23.824822265625" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.696470214844" Y="23.852482421875" />
                  <Point X="-4.740762695312" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.472893554688" Y="24.429361328125" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.500464355469" Y="24.6902890625" />
                  <Point X="-3.473923828125" Y="24.700751953125" />
                  <Point X="-3.444168212891" Y="24.71610546875" />
                  <Point X="-3.433564941406" Y="24.722482421875" />
                  <Point X="-3.415522705078" Y="24.73500390625" />
                  <Point X="-3.393673339844" Y="24.751755859375" />
                  <Point X="-3.371216796875" Y="24.773640625" />
                  <Point X="-3.360898925781" Y="24.785513671875" />
                  <Point X="-3.341656738281" Y="24.811765625" />
                  <Point X="-3.333438476563" Y="24.825181640625" />
                  <Point X="-3.319328857422" Y="24.853185546875" />
                  <Point X="-3.3134375" Y="24.8677734375" />
                  <Point X="-3.307423339844" Y="24.88715234375" />
                  <Point X="-3.300990966797" Y="24.911498046875" />
                  <Point X="-3.296721923828" Y="24.939654296875" />
                  <Point X="-3.2956484375" Y="24.953892578125" />
                  <Point X="-3.295647705078" Y="24.98353515625" />
                  <Point X="-3.296720947266" Y="24.997775390625" />
                  <Point X="-3.300989990234" Y="25.025935546875" />
                  <Point X="-3.304185791016" Y="25.03985546875" />
                  <Point X="-3.310199951172" Y="25.059234375" />
                  <Point X="-3.319328125" Y="25.084251953125" />
                  <Point X="-3.333436767578" Y="25.11225390625" />
                  <Point X="-3.341654052734" Y="25.12566796875" />
                  <Point X="-3.360896972656" Y="25.151921875" />
                  <Point X="-3.371214355469" Y="25.163796875" />
                  <Point X="-3.393671875" Y="25.18568359375" />
                  <Point X="-3.405812011719" Y="25.1956953125" />
                  <Point X="-3.423854248047" Y="25.208216796875" />
                  <Point X="-3.440322021484" Y="25.218439453125" />
                  <Point X="-3.465602050781" Y="25.23240234375" />
                  <Point X="-3.476008300781" Y="25.2373515625" />
                  <Point X="-3.4973515625" Y="25.24595703125" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-4.027448974609" Y="25.388720703125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.759874511719" Y="25.764634765625" />
                  <Point X="-4.731331542969" Y="25.957525390625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.469519042969" Y="26.29663671875" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747057861328" Y="26.204365234375" />
                  <Point X="-3.736705322266" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704887451172" Y="26.2080546875" />
                  <Point X="-3.684603271484" Y="26.21208984375" />
                  <Point X="-3.674573730469" Y="26.21466015625" />
                  <Point X="-3.634640380859" Y="26.22725" />
                  <Point X="-3.613147705078" Y="26.23402734375" />
                  <Point X="-3.603454345703" Y="26.23767578125" />
                  <Point X="-3.584516601562" Y="26.2460078125" />
                  <Point X="-3.575272216797" Y="26.25069140625" />
                  <Point X="-3.556528808594" Y="26.261513671875" />
                  <Point X="-3.547854980469" Y="26.267173828125" />
                  <Point X="-3.531175537109" Y="26.279404296875" />
                  <Point X="-3.515926025391" Y="26.29337890625" />
                  <Point X="-3.502288818359" Y="26.3089296875" />
                  <Point X="-3.495895507812" Y="26.317076171875" />
                  <Point X="-3.483481689453" Y="26.3348046875" />
                  <Point X="-3.478011962891" Y="26.343599609375" />
                  <Point X="-3.468062744141" Y="26.361734375" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.447559814453" Y="26.4097578125" />
                  <Point X="-3.438935791016" Y="26.430578125" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.4210078125" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057617188" Y="26.5636484375" />
                  <Point X="-3.427189208984" Y="26.57378515625" />
                  <Point X="-3.432791503906" Y="26.59469140625" />
                  <Point X="-3.436014160156" Y="26.60453515625" />
                  <Point X="-3.443510498047" Y="26.623810546875" />
                  <Point X="-3.447784179688" Y="26.6332421875" />
                  <Point X="-3.467118164062" Y="26.6703828125" />
                  <Point X="-3.477523925781" Y="26.69037109375" />
                  <Point X="-3.482799072266" Y="26.699283203125" />
                  <Point X="-3.494290527344" Y="26.716482421875" />
                  <Point X="-3.500506835938" Y="26.72476953125" />
                  <Point X="-3.514418945312" Y="26.741349609375" />
                  <Point X="-3.521498779297" Y="26.748908203125" />
                  <Point X="-3.536441162109" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-3.842095458984" Y="26.998462890625" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.113201171875" Y="27.49030078125" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.742866455078" Y="28.013767578125" />
                  <Point X="-3.726337402344" Y="28.035013671875" />
                  <Point X="-3.678828613281" Y="28.007583984375" />
                  <Point X="-3.254157470703" Y="27.762400390625" />
                  <Point X="-3.244925537109" Y="27.757720703125" />
                  <Point X="-3.225998291016" Y="27.749390625" />
                  <Point X="-3.216302978516" Y="27.745740234375" />
                  <Point X="-3.195661376953" Y="27.73923046875" />
                  <Point X="-3.185623535156" Y="27.736658203125" />
                  <Point X="-3.165329345703" Y="27.73262109375" />
                  <Point X="-3.155072998047" Y="27.73115625" />
                  <Point X="-3.099456787109" Y="27.726291015625" />
                  <Point X="-3.069523681641" Y="27.723671875" />
                  <Point X="-3.059173095703" Y="27.723333984375" />
                  <Point X="-3.038489746094" Y="27.72378515625" />
                  <Point X="-3.028156982422" Y="27.72457421875" />
                  <Point X="-3.006698242188" Y="27.727400390625" />
                  <Point X="-2.996521240234" Y="27.729310546875" />
                  <Point X="-2.976432373047" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902749755859" Y="27.77319140625" />
                  <Point X="-2.886616943359" Y="27.786138671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.839426025391" Y="27.832529296875" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753663330078" Y="28.100015625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.919483642578" Y="28.45759375" />
                  <Point X="-3.059387207031" Y="28.699912109375" />
                  <Point X="-2.841534179688" Y="28.866939453125" />
                  <Point X="-2.648368652344" Y="29.0150390625" />
                  <Point X="-2.239791015625" Y="29.24203515625" />
                  <Point X="-2.192524169922" Y="29.268294921875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111820556641" Y="29.164048828125" />
                  <Point X="-2.097517333984" Y="29.149107421875" />
                  <Point X="-2.089958740234" Y="29.14202734375" />
                  <Point X="-2.073379638672" Y="29.128115234375" />
                  <Point X="-2.065093994141" Y="29.121900390625" />
                  <Point X="-2.047895507812" Y="29.110408203125" />
                  <Point X="-2.038982299805" Y="29.105130859375" />
                  <Point X="-1.977081787109" Y="29.07290625" />
                  <Point X="-1.943766235352" Y="29.0555625" />
                  <Point X="-1.934335205078" Y="29.0512890625" />
                  <Point X="-1.915062133789" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313476562" Y="29.034966796875" />
                  <Point X="-1.874174926758" Y="29.032833984375" />
                  <Point X="-1.853725097656" Y="29.029685546875" />
                  <Point X="-1.8330546875" Y="29.028783203125" />
                  <Point X="-1.812408325195" Y="29.03013671875" />
                  <Point X="-1.802120727539" Y="29.031376953125" />
                  <Point X="-1.780805297852" Y="29.03513671875" />
                  <Point X="-1.770715087891" Y="29.037490234375" />
                  <Point X="-1.750860595703" Y="29.043279296875" />
                  <Point X="-1.741096679688" Y="29.04671484375" />
                  <Point X="-1.676623168945" Y="29.073421875" />
                  <Point X="-1.641922729492" Y="29.087794921875" />
                  <Point X="-1.63258605957" Y="29.0922734375" />
                  <Point X="-1.614454101562" Y="29.102220703125" />
                  <Point X="-1.605658813477" Y="29.107689453125" />
                  <Point X="-1.587928955078" Y="29.120103515625" />
                  <Point X="-1.579780395508" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252807617" Y="29.155388671875" />
                  <Point X="-1.538021240234" Y="29.1720703125" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153918457" Y="29.19948828125" />
                  <Point X="-1.516856445312" Y="29.208728515625" />
                  <Point X="-1.508525512695" Y="29.227662109375" />
                  <Point X="-1.504877319336" Y="29.23735546875" />
                  <Point X="-1.483892089844" Y="29.303912109375" />
                  <Point X="-1.47259777832" Y="29.339732421875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.478545288086" Y="29.557181640625" />
                  <Point X="-1.479265869141" Y="29.562654296875" />
                  <Point X="-1.181232543945" Y="29.646212890625" />
                  <Point X="-0.93116394043" Y="29.7163203125" />
                  <Point X="-0.43585760498" Y="29.774291015625" />
                  <Point X="-0.365222686768" Y="29.78255859375" />
                  <Point X="-0.351295776367" Y="29.73058203125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.305593353271" Y="29.5140703125" />
                  <Point X="0.378190673828" Y="29.7850078125" />
                  <Point X="0.609528076172" Y="29.760779296875" />
                  <Point X="0.82786529541" Y="29.7379140625" />
                  <Point X="1.237675048828" Y="29.63897265625" />
                  <Point X="1.453608520508" Y="29.58683984375" />
                  <Point X="1.718847167969" Y="29.49063671875" />
                  <Point X="1.858247436523" Y="29.44007421875" />
                  <Point X="2.116185546875" Y="29.3194453125" />
                  <Point X="2.250433837891" Y="29.25666015625" />
                  <Point X="2.499674072266" Y="29.111455078125" />
                  <Point X="2.629443847656" Y="29.035849609375" />
                  <Point X="2.817780517578" Y="28.901916015625" />
                  <Point X="2.625458251953" Y="28.5688046875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.02734387207" Y="27.488400390625" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.018853637695" Y="27.3244453125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023800537109" Y="27.28903515625" />
                  <Point X="2.029143310547" Y="27.267111328125" />
                  <Point X="2.032468261719" Y="27.256306640625" />
                  <Point X="2.040734863281" Y="27.234216796875" />
                  <Point X="2.045319335938" Y="27.223884765625" />
                  <Point X="2.055681640625" Y="27.203841796875" />
                  <Point X="2.061458984375" Y="27.194130859375" />
                  <Point X="2.089385986328" Y="27.15297265625" />
                  <Point X="2.104416748047" Y="27.130822265625" />
                  <Point X="2.109809814453" Y="27.123630859375" />
                  <Point X="2.130470458984" Y="27.100115234375" />
                  <Point X="2.157597412109" Y="27.07538671875" />
                  <Point X="2.168255126953" Y="27.066982421875" />
                  <Point X="2.209412353516" Y="27.0390546875" />
                  <Point X="2.231563720703" Y="27.0240234375" />
                  <Point X="2.241280273438" Y="27.018244140625" />
                  <Point X="2.261319824219" Y="27.007884765625" />
                  <Point X="2.271642822266" Y="27.0033046875" />
                  <Point X="2.293730712891" Y="26.995037109375" />
                  <Point X="2.304535888672" Y="26.9917109375" />
                  <Point X="2.326467285156" Y="26.986365234375" />
                  <Point X="2.337593505859" Y="26.984345703125" />
                  <Point X="2.382727050781" Y="26.978904296875" />
                  <Point X="2.407018310547" Y="26.975974609375" />
                  <Point X="2.416039794922" Y="26.9753203125" />
                  <Point X="2.447576660156" Y="26.97549609375" />
                  <Point X="2.484317626953" Y="26.979822265625" />
                  <Point X="2.497752685547" Y="26.98239453125" />
                  <Point X="2.549947265625" Y="26.996353515625" />
                  <Point X="2.5780390625" Y="27.003865234375" />
                  <Point X="2.584003173828" Y="27.005671875" />
                  <Point X="2.604410644531" Y="27.0130703125" />
                  <Point X="2.627659912109" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.158209472656" Y="27.329353515625" />
                  <Point X="3.940403076172" Y="27.780951171875" />
                  <Point X="3.966981689453" Y="27.744013671875" />
                  <Point X="4.043959228516" Y="27.637033203125" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="3.91044140625" Y="27.309716796875" />
                  <Point X="3.172951904297" Y="26.7438203125" />
                  <Point X="3.168135986328" Y="26.7398671875" />
                  <Point X="3.152114501953" Y="26.7252109375" />
                  <Point X="3.134666992188" Y="26.706599609375" />
                  <Point X="3.128576660156" Y="26.699421875" />
                  <Point X="3.091011962891" Y="26.650416015625" />
                  <Point X="3.070794433594" Y="26.624041015625" />
                  <Point X="3.06563671875" Y="26.616603515625" />
                  <Point X="3.049740966797" Y="26.589375" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.016147460938" Y="26.49263671875" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.016186035156" Y="26.296900390625" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034684814453" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.19537109375" />
                  <Point X="3.056919433594" Y="26.183525390625" />
                  <Point X="3.088161865234" Y="26.1360390625" />
                  <Point X="3.104977050781" Y="26.11048046875" />
                  <Point X="3.111738525391" Y="26.10142578125" />
                  <Point X="3.126291015625" Y="26.084181640625" />
                  <Point X="3.13408203125" Y="26.0759921875" />
                  <Point X="3.151325683594" Y="26.05990234375" />
                  <Point X="3.160032226562" Y="26.052697265625" />
                  <Point X="3.178241699219" Y="26.03937109375" />
                  <Point X="3.187744628906" Y="26.03325" />
                  <Point X="3.233019287109" Y="26.007763671875" />
                  <Point X="3.25738671875" Y="25.994046875" />
                  <Point X="3.265477050781" Y="25.98998828125" />
                  <Point X="3.294680908203" Y="25.97808203125" />
                  <Point X="3.330278808594" Y="25.96801953125" />
                  <Point X="3.343673583984" Y="25.965255859375" />
                  <Point X="3.404887939453" Y="25.957166015625" />
                  <Point X="3.437834228516" Y="25.9528125" />
                  <Point X="3.444033935547" Y="25.95219921875" />
                  <Point X="3.465709228516" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="3.996635253906" Y="26.018099609375" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.719625488281" Y="26.050025390625" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.537170410156" Y="25.647818359375" />
                  <Point X="3.691991210938" Y="25.421353515625" />
                  <Point X="3.686022216797" Y="25.419541015625" />
                  <Point X="3.665624511719" Y="25.41213671875" />
                  <Point X="3.642384521484" Y="25.40162109375" />
                  <Point X="3.634007568359" Y="25.397318359375" />
                  <Point X="3.573866210938" Y="25.362556640625" />
                  <Point X="3.541497558594" Y="25.34384765625" />
                  <Point X="3.533881103516" Y="25.3389453125" />
                  <Point X="3.508773193359" Y="25.319869140625" />
                  <Point X="3.481993164062" Y="25.294349609375" />
                  <Point X="3.472796142578" Y="25.2842265625" />
                  <Point X="3.436711425781" Y="25.23824609375" />
                  <Point X="3.417290283203" Y="25.213498046875" />
                  <Point X="3.410852050781" Y="25.204205078125" />
                  <Point X="3.399128173828" Y="25.184923828125" />
                  <Point X="3.393842529297" Y="25.174935546875" />
                  <Point X="3.384069091797" Y="25.15347265625" />
                  <Point X="3.380005859375" Y="25.1429296875" />
                  <Point X="3.373159423828" Y="25.1214296875" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.358347900391" Y="25.047666015625" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.00496484375" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280029297" Y="24.937056640625" />
                  <Point X="3.351874267578" Y="24.923576171875" />
                  <Point X="3.363902587891" Y="24.86076953125" />
                  <Point X="3.370376464844" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380005126953" Y="24.79451171875" />
                  <Point X="3.384068359375" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.76250390625" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417290283203" Y="24.72394140625" />
                  <Point X="3.453375" Y="24.6779609375" />
                  <Point X="3.472796142578" Y="24.653212890625" />
                  <Point X="3.478720214844" Y="24.64636328125" />
                  <Point X="3.501135986328" Y="24.62419921875" />
                  <Point X="3.530173828125" Y="24.60127734375" />
                  <Point X="3.541495605469" Y="24.59359375" />
                  <Point X="3.601636962891" Y="24.55883203125" />
                  <Point X="3.634005615234" Y="24.54012109375" />
                  <Point X="3.639491943359" Y="24.537185546875" />
                  <Point X="3.659139648438" Y="24.527990234375" />
                  <Point X="3.683021240234" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.146876464844" Y="24.39419921875" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.780071777344" Y="24.191376953125" />
                  <Point X="4.761612304688" Y="24.068939453125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.416951660156" Y="23.961705078125" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481201172" Y="24.087322265625" />
                  <Point X="3.236445068359" Y="24.061666015625" />
                  <Point X="3.172916992188" Y="24.047859375" />
                  <Point X="3.157877197266" Y="24.0432578125" />
                  <Point X="3.128758056641" Y="24.031634765625" />
                  <Point X="3.114678710938" Y="24.02461328125" />
                  <Point X="3.086851074219" Y="24.007720703125" />
                  <Point X="3.074123046875" Y="23.99846875" />
                  <Point X="3.050372070312" Y="23.97799609375" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.968003662109" Y="23.88096875" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.864931640625" Y="23.592216796875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.42333203125" />
                  <Point X="2.889158203125" Y="23.39451953125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.961864013672" Y="23.2790234375" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001742675781" Y="23.217646484375" />
                  <Point X="3.019788818359" Y="23.195556640625" />
                  <Point X="3.043485595703" Y="23.1718828125" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.474930419922" Y="22.8398046875" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045499023438" Y="22.302583984375" />
                  <Point X="4.001273925781" Y="22.239748046875" />
                  <Point X="3.721427734375" Y="22.40131640625" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841198730469" Y="22.909109375" />
                  <Point X="2.815020996094" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108398438" Y="22.933662109375" />
                  <Point X="2.630626708984" Y="22.959033203125" />
                  <Point X="2.555018066406" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.40058984375" Y="22.948890625" />
                  <Point X="2.283884033203" Y="22.887470703125" />
                  <Point X="2.221071777344" Y="22.854412109375" />
                  <Point X="2.208970214844" Y="22.846830078125" />
                  <Point X="2.1860390625" Y="22.8299375" />
                  <Point X="2.175209472656" Y="22.820626953125" />
                  <Point X="2.154250244141" Y="22.79966796875" />
                  <Point X="2.144938476562" Y="22.788837890625" />
                  <Point X="2.128045410156" Y="22.76590625" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.059042724609" Y="22.637099609375" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.02755871582" Y="22.279376953125" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.340810791016" Y="21.656578125" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.963916015625" />
                  <Point X="2.50007421875" Y="21.255419921875" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657714844" Y="22.12984765625" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251831055" Y="22.17199609375" />
                  <Point X="1.773298339844" Y="22.179353515625" />
                  <Point X="1.634745483398" Y="22.2684296875" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546279663086" Y="22.323755859375" />
                  <Point X="1.517465942383" Y="22.336126953125" />
                  <Point X="1.502547485352" Y="22.34111328125" />
                  <Point X="1.470927368164" Y="22.34884375" />
                  <Point X="1.455391235352" Y="22.351302734375" />
                  <Point X="1.424125244141" Y="22.35362109375" />
                  <Point X="1.408395629883" Y="22.35348046875" />
                  <Point X="1.256864501953" Y="22.339537109375" />
                  <Point X="1.17530871582" Y="22.332033203125" />
                  <Point X="1.161225463867" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120008422852" Y="22.318369140625" />
                  <Point X="1.092622070312" Y="22.307025390625" />
                  <Point X="1.079880126953" Y="22.300587890625" />
                  <Point X="1.055498413086" Y="22.285869140625" />
                  <Point X="1.043858032227" Y="22.277587890625" />
                  <Point X="0.92684942627" Y="22.180298828125" />
                  <Point X="0.863874328613" Y="22.127935546875" />
                  <Point X="0.852650512695" Y="22.116908203125" />
                  <Point X="0.832179321289" Y="22.09315625" />
                  <Point X="0.822932312012" Y="22.080431640625" />
                  <Point X="0.806040405273" Y="22.05260546875" />
                  <Point X="0.799018859863" Y="22.038529296875" />
                  <Point X="0.78739465332" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.747807006836" Y="21.83341015625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.802429016113" Y="21.0805625" />
                  <Point X="0.833091064453" Y="20.847662109375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143676758" Y="21.546423828125" />
                  <Point X="0.626786621094" Y="21.576185546875" />
                  <Point X="0.620407409668" Y="21.586791015625" />
                  <Point X="0.51396697998" Y="21.740150390625" />
                  <Point X="0.4566796875" Y="21.82269140625" />
                  <Point X="0.446668884277" Y="21.834828125" />
                  <Point X="0.424786102295" Y="21.85728125" />
                  <Point X="0.412913665771" Y="21.86759765625" />
                  <Point X="0.386658538818" Y="21.886841796875" />
                  <Point X="0.373244720459" Y="21.895060546875" />
                  <Point X="0.345242401123" Y="21.909169921875" />
                  <Point X="0.330654022217" Y="21.915060546875" />
                  <Point X="0.16594342041" Y="21.9661796875" />
                  <Point X="0.077294914246" Y="21.993693359375" />
                  <Point X="0.063380393982" Y="21.996888671875" />
                  <Point X="0.035227645874" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008651480675" Y="22.002234375" />
                  <Point X="-0.022895498276" Y="22.001162109375" />
                  <Point X="-0.051061767578" Y="21.996892578125" />
                  <Point X="-0.06498387146" Y="21.9936953125" />
                  <Point X="-0.22969430542" Y="21.94257421875" />
                  <Point X="-0.318343109131" Y="21.9150625" />
                  <Point X="-0.332928222656" Y="21.909171875" />
                  <Point X="-0.360928466797" Y="21.895064453125" />
                  <Point X="-0.37434362793" Y="21.88684765625" />
                  <Point X="-0.400599487305" Y="21.867603515625" />
                  <Point X="-0.4124765625" Y="21.857283203125" />
                  <Point X="-0.434360839844" Y="21.834826171875" />
                  <Point X="-0.44436819458" Y="21.822689453125" />
                  <Point X="-0.550808532715" Y="21.669328125" />
                  <Point X="-0.60809576416" Y="21.5867890625" />
                  <Point X="-0.612469482422" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.782913757324" Y="20.9889765625" />
                  <Point X="-0.985425476074" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.932770503312" Y="21.251749160053" />
                  <Point X="-2.471791034333" Y="20.878455346442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.968425836883" Y="22.212648471831" />
                  <Point X="-3.729378605077" Y="22.019071840335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884678457797" Y="21.33504714823" />
                  <Point X="-2.413939113941" Y="20.953849943779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12431230426" Y="22.461125002864" />
                  <Point X="-3.641252984933" Y="22.069951278987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836586412281" Y="21.418345136408" />
                  <Point X="-2.343085690764" Y="21.018716131753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.135065452132" Y="22.592074889077" />
                  <Point X="-3.553127364788" Y="22.120830717638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.788494372655" Y="21.501643129354" />
                  <Point X="-2.260375824493" Y="21.073981161419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057555271229" Y="22.651550540931" />
                  <Point X="-3.465001744644" Y="22.17171015629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.740402334803" Y="21.584941123738" />
                  <Point X="-2.177665702669" Y="21.129245984142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.138712470203" Y="20.287918245254" />
                  <Point X="-1.102696411159" Y="20.258753015702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980045090325" Y="22.711026192785" />
                  <Point X="-3.3768761245" Y="22.222589594942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.692310296951" Y="21.668239118122" />
                  <Point X="-2.094915951703" Y="21.184478715819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.124169239134" Y="20.398383527704" />
                  <Point X="-0.973791910945" Y="20.276610368382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902534909421" Y="22.770501844639" />
                  <Point X="-3.288750504356" Y="22.273469033594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.644218259099" Y="21.751537112505" />
                  <Point X="-1.98927428659" Y="21.22117394093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127517304883" Y="20.523336896649" />
                  <Point X="-0.946877178887" Y="20.377057406863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825024705839" Y="22.829977478128" />
                  <Point X="-3.200624884212" Y="22.324348472245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.596126221247" Y="21.834835106889" />
                  <Point X="-1.832759249368" Y="21.216672721592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.156501599557" Y="20.669050074449" />
                  <Point X="-0.919962446829" Y="20.477504445345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.747514499778" Y="22.88945310961" />
                  <Point X="-3.112499229126" Y="22.375227882602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.548034183396" Y="21.918133101273" />
                  <Point X="-1.668508663062" Y="21.205907378119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.18640505293" Y="20.815507572288" />
                  <Point X="-0.893047714771" Y="20.577951483826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.67317382019" Y="23.761279406218" />
                  <Point X="-4.620503172447" Y="23.718627556658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.670004293717" Y="22.948928741092" />
                  <Point X="-3.024373487451" Y="22.426107222839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.499942145544" Y="22.001431095656" />
                  <Point X="-0.866132982712" Y="20.678398522308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.712541617934" Y="23.915400979013" />
                  <Point X="-4.490656586603" Y="23.735722023436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.592494087655" Y="23.008404372574" />
                  <Point X="-2.936247745775" Y="22.476986563077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.451850107692" Y="22.08472909004" />
                  <Point X="-0.839218250654" Y="20.778845560789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746361301765" Y="24.065029777747" />
                  <Point X="-4.360810000759" Y="23.752816490215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514983881594" Y="23.067880004056" />
                  <Point X="-2.848122004099" Y="22.527865903314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.40375806984" Y="22.168027084424" />
                  <Point X="-0.812303518596" Y="20.87929259927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76613475003" Y="24.203284159193" />
                  <Point X="-4.230963414915" Y="23.769910956994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437473675533" Y="23.127355635538" />
                  <Point X="-2.759996262423" Y="22.578745243552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355666031989" Y="22.251325078807" />
                  <Point X="-0.785388786538" Y="20.979739637752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.785908198296" Y="24.34153854064" />
                  <Point X="-4.101116829072" Y="23.787005423772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359963469472" Y="23.18683126702" />
                  <Point X="-2.670504315138" Y="22.6285182523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.317865888284" Y="22.342957284743" />
                  <Point X="-0.758474106548" Y="21.080186718397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676147703" Y="24.374898402834" />
                  <Point X="-3.971270243228" Y="23.804099890551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.282453263411" Y="23.246306898502" />
                  <Point X="-2.548002159313" Y="22.651560121241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322799501052" Y="22.469194604348" />
                  <Point X="-0.73155943183" Y="21.180633803312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.562723045149" Y="24.405291084695" />
                  <Point X="-3.841423657384" Y="23.82119435733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.20494305735" Y="23.305782529984" />
                  <Point X="-0.704644757113" Y="21.281080888228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.449298252127" Y="24.435683657098" />
                  <Point X="-3.711577095533" Y="23.838288843537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.127432851289" Y="23.365258161466" />
                  <Point X="-0.677730082396" Y="21.381527973143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.335872944495" Y="24.466075812776" />
                  <Point X="-3.581730540144" Y="23.855383334978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.049922645228" Y="23.424733792948" />
                  <Point X="-0.650815407679" Y="21.481975058058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222447636862" Y="24.496467968455" />
                  <Point X="-3.451883984756" Y="23.872477826419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.98858327776" Y="23.497304311326" />
                  <Point X="-0.61497978477" Y="21.575198101566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.109022329229" Y="24.526860124134" />
                  <Point X="-3.322037429368" Y="23.889572317861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953331061303" Y="23.590999788064" />
                  <Point X="-0.561407021833" Y="21.654057892286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.995597021596" Y="24.557252279812" />
                  <Point X="-3.180800018193" Y="23.897442676161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95868524704" Y="23.717577680944" />
                  <Point X="-0.507091622186" Y="21.732316307655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882171713964" Y="24.587644435491" />
                  <Point X="-0.452776346242" Y="21.810574823197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.768746406331" Y="24.618036591169" />
                  <Point X="-0.385715825935" Y="21.878512443355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818075326938" Y="20.903701588457" />
                  <Point X="0.826624754893" Y="20.896778398205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.655321098698" Y="24.648428746848" />
                  <Point X="-0.290547427523" Y="21.923688752615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.776244031769" Y="21.059818062132" />
                  <Point X="0.808610714386" Y="21.033608039341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.773024366655" Y="25.675769165849" />
                  <Point X="-4.611957763108" Y="25.545340002016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.541895791065" Y="24.678820902527" />
                  <Point X="-0.181416274999" Y="21.957558246537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7344127366" Y="21.215934535807" />
                  <Point X="0.790596770708" Y="21.170437602066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.756871214255" Y="25.78493075971" />
                  <Point X="-4.386349520535" Y="25.484888208183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440044510138" Y="24.718585520231" />
                  <Point X="-0.072286449499" Y="21.991428815062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692581441432" Y="21.372051009483" />
                  <Point X="0.772582877618" Y="21.307267123827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740718041947" Y="25.894092337449" />
                  <Point X="-4.160741277962" Y="25.42443641435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.365375730981" Y="24.780362093851" />
                  <Point X="0.075310953445" Y="21.994148953576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.648676865827" Y="21.529846392551" />
                  <Point X="0.754568984527" Y="21.444096645587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719952193199" Y="25.999518643457" />
                  <Point X="-3.935133612167" Y="25.363985087583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.315637158531" Y="24.862326750808" />
                  <Point X="0.319757178993" Y="21.918442461913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.480650529911" Y="21.788153595293" />
                  <Point X="0.736555091436" Y="21.580926167347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692787796657" Y="26.099763507626" />
                  <Point X="-3.709526779164" Y="25.303534435197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295648079487" Y="24.968382072519" />
                  <Point X="0.724950506945" Y="21.71256553334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665623400115" Y="26.200008371794" />
                  <Point X="-3.477667228073" Y="25.23802043154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.345641316241" Y="25.13110795617" />
                  <Point X="0.744644411922" Y="21.818859882298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.638459003573" Y="26.300253235963" />
                  <Point X="0.767237766748" Y="21.922806303063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.485660728978" Y="26.298761791657" />
                  <Point X="0.793221678011" Y="22.024007105363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.305397637586" Y="26.275029777232" />
                  <Point X="0.843115985803" Y="22.105845650325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125134523608" Y="26.251297744519" />
                  <Point X="0.914653413262" Y="22.170157942553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.94487140963" Y="26.227565711806" />
                  <Point X="0.989134227251" Y="22.232086727365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.765768796825" Y="26.204773434413" />
                  <Point X="1.065911899657" Y="22.292155552905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640346859032" Y="26.225450910536" />
                  <Point X="1.168947293458" Y="22.33096129491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.504905542629" Y="21.249123635717" />
                  <Point X="2.638526922276" Y="21.140919175986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.544212328202" Y="26.26984486119" />
                  <Point X="1.303961828355" Y="22.343870839062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.257171634412" Y="21.571976757832" />
                  <Point X="2.505981337708" Y="21.370494632799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480129911581" Y="26.340194102162" />
                  <Point X="1.444767953354" Y="22.352090446021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009437763081" Y="21.894829850077" />
                  <Point X="2.37343575314" Y="21.600070089612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.43940111424" Y="26.429454731144" />
                  <Point X="2.240889753146" Y="21.829645882831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421653686765" Y="26.537325306504" />
                  <Point X="2.108343617512" Y="22.059221785889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.220503717359" Y="27.306463464956" />
                  <Point X="-3.80507825423" Y="26.970058557932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.497930986132" Y="26.721335604386" />
                  <Point X="2.034432372706" Y="22.241316090566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.172053477822" Y="27.389471393334" />
                  <Point X="2.008574082869" Y="22.384497879562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123603238284" Y="27.472479321712" />
                  <Point X="2.00548973479" Y="22.509237694149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.075152529138" Y="27.555486869808" />
                  <Point X="2.040965791784" Y="22.602751908395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026701691604" Y="27.638494313938" />
                  <Point X="2.086075908682" Y="22.688464614754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97334034614" Y="27.717525307152" />
                  <Point X="2.133035294759" Y="22.772679812461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.914994920657" Y="27.792520271945" />
                  <Point X="2.200305853997" Y="22.840447346446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.856649495174" Y="27.867515236739" />
                  <Point X="2.289526373266" Y="22.890440153268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798304069691" Y="27.942510201533" />
                  <Point X="2.381020616961" Y="22.938591734354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739958668171" Y="28.017505185732" />
                  <Point X="2.489424278583" Y="22.973050338793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323196637283" Y="27.802260106237" />
                  <Point X="2.665458879917" Y="22.952742488102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.075878408631" Y="27.724227912316" />
                  <Point X="2.940823658416" Y="22.851998645929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950165460835" Y="27.744669733185" />
                  <Point X="3.466746984125" Y="22.548356492846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869978079962" Y="27.80197743125" />
                  <Point X="3.99266609803" Y="22.244717750413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803877453632" Y="27.870692358224" />
                  <Point X="4.05439235961" Y="22.316974968116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.758213880096" Y="27.955956884235" />
                  <Point X="2.901637855733" Y="23.37269931831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751262065732" Y="28.072569574721" />
                  <Point X="2.859399258812" Y="23.529145618441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.78334694747" Y="28.22079355842" />
                  <Point X="2.8696153113" Y="23.643114981014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.914803018014" Y="28.449486744173" />
                  <Point X="2.88652818867" Y="23.751661361724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047350140071" Y="28.67906344602" />
                  <Point X="2.93170861841" Y="23.837317129867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988887853469" Y="28.753963778545" />
                  <Point X="2.992450851241" Y="23.91037119834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.911346717693" Y="28.813414363638" />
                  <Point X="3.054986134085" Y="23.981973283542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833805584634" Y="28.872864950931" />
                  <Point X="3.139380058818" Y="24.035874589554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756264476109" Y="28.932315558091" />
                  <Point X="3.253822426884" Y="24.065443145933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.678723367584" Y="28.991766165251" />
                  <Point X="3.374670070055" Y="24.089824812803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.593885970844" Y="29.045308354713" />
                  <Point X="3.541516135222" Y="24.076957691989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.504354916977" Y="29.095049695576" />
                  <Point X="3.72177913222" Y="24.053225754005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.41482386311" Y="29.144791036439" />
                  <Point X="3.902042129217" Y="24.029493816021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325292809243" Y="29.194532377303" />
                  <Point X="4.082305126214" Y="24.005761878037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.235761719258" Y="29.244273688918" />
                  <Point X="4.262568123211" Y="23.982029940052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.819771957205" Y="29.029653980395" />
                  <Point X="3.370444304748" Y="24.826699722637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.784697777828" Y="24.49124387444" />
                  <Point X="4.442831096424" Y="23.958298021328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.707215349912" Y="29.060749595739" />
                  <Point X="3.34892473801" Y="24.966368082942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010304608385" Y="24.430793224036" />
                  <Point X="4.623093927758" Y="23.934566217495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.610504249271" Y="29.104676649367" />
                  <Point X="3.363871292699" Y="25.076506760363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.235911709628" Y="24.370342354433" />
                  <Point X="4.73767077379" Y="23.964025875764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.539791153231" Y="29.169656472015" />
                  <Point X="3.393690538988" Y="25.174601769596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.461519226082" Y="24.3098911486" />
                  <Point X="4.761215743812" Y="24.067201693738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.498278977608" Y="29.258282733772" />
                  <Point X="3.448112563758" Y="25.252773841644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.687126742536" Y="24.249439942767" />
                  <Point X="4.777760419243" Y="24.176046238499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468676044239" Y="29.356552909754" />
                  <Point X="3.512611774445" Y="25.322785569435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468171947302" Y="29.478386858863" />
                  <Point X="3.597550167932" Y="25.376245973344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.436190720113" Y="29.574731130484" />
                  <Point X="3.692601536325" Y="25.421517051646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.324057448202" Y="29.60616955606" />
                  <Point X="3.806026814567" Y="25.451909231125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.211924176292" Y="29.637607981637" />
                  <Point X="3.060804271706" Y="26.17762070627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313935055983" Y="25.972639438852" />
                  <Point X="3.919452092809" Y="25.482301410604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.099789869112" Y="29.66904556887" />
                  <Point X="3.006501578779" Y="26.343836318722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.490373447274" Y="25.952004605502" />
                  <Point X="4.032877371051" Y="25.512693590082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.987655171788" Y="29.700482840169" />
                  <Point X="2.033258392045" Y="27.254195270515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371550434466" Y="26.980251776006" />
                  <Point X="3.008369418425" Y="26.46456593076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.620809182142" Y="25.968621988808" />
                  <Point X="4.146302649293" Y="25.543085769561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.865715720995" Y="29.72398037866" />
                  <Point X="-0.15810179744" Y="29.150965921699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124452031612" Y="29.123716878611" />
                  <Point X="2.012987307274" Y="27.392852630058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.514372083284" Y="26.986839243958" />
                  <Point X="3.037985905182" Y="26.562825131425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75065589854" Y="25.985716349866" />
                  <Point X="4.259727927535" Y="25.57347794904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733822134761" Y="29.739417217207" />
                  <Point X="-0.248317998287" Y="29.34626371944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.064222597973" Y="29.093173334863" />
                  <Point X="2.030656130419" Y="27.500786857949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622718036873" Y="27.02134457944" />
                  <Point X="3.087091892972" Y="26.645302045338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.880502614938" Y="26.002810710924" />
                  <Point X="4.373153205777" Y="25.603870128519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.601928548527" Y="29.754854055754" />
                  <Point X="-0.290149276311" Y="29.502380179232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.158021033895" Y="29.139459017875" />
                  <Point X="2.063954647812" Y="27.596064408996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.711678152742" Y="27.071548256778" />
                  <Point X="3.146614331489" Y="26.71934388377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010349263838" Y="26.019905126641" />
                  <Point X="4.486578484019" Y="25.634262307998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.470034962294" Y="29.770290894301" />
                  <Point X="-0.331980554335" Y="29.658496639024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.217144108429" Y="29.213824254884" />
                  <Point X="2.111972743141" Y="27.679422280854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799803796433" Y="27.122427676361" />
                  <Point X="3.221418958824" Y="26.781010449705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.140195341156" Y="26.037000005215" />
                  <Point X="4.600003221105" Y="25.664654925696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.250631422768" Y="29.308948921178" />
                  <Point X="2.160064858118" Y="27.762720212782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887929440125" Y="27.173307095945" />
                  <Point X="3.298929079533" Y="26.840486150303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.270041418475" Y="26.054094883789" />
                  <Point X="4.713427522466" Y="25.695047896237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.277546058128" Y="29.409396037963" />
                  <Point X="2.208156973096" Y="27.846018144711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.976055083817" Y="27.224186515528" />
                  <Point X="3.376439200242" Y="26.899961850902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.399887495794" Y="26.071189762363" />
                  <Point X="4.775618474508" Y="25.766928815024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.304460693489" Y="29.509843154749" />
                  <Point X="2.256249088073" Y="27.929316076639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.064180727509" Y="27.275065935111" />
                  <Point X="3.453949320951" Y="26.959437551501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529733573113" Y="26.088284640938" />
                  <Point X="4.753840166643" Y="25.906806699763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331375350691" Y="29.610290253847" />
                  <Point X="2.30434120305" Y="28.012614008568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.1523063712" Y="27.325945354694" />
                  <Point X="3.53145944166" Y="27.018913252099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.659579650431" Y="26.105379519512" />
                  <Point X="4.717584829192" Y="26.058407851909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.358290008853" Y="29.710737352168" />
                  <Point X="2.352433318028" Y="28.095911940496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.240432206139" Y="27.376824619409" />
                  <Point X="3.608969562369" Y="27.078388952698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.423373827887" Y="29.780275673455" />
                  <Point X="2.400525433005" Y="28.179209872425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.328558054809" Y="27.427703873004" />
                  <Point X="3.686479683078" Y="27.137864653296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.596754256651" Y="29.762117129334" />
                  <Point X="2.448617547983" Y="28.262507804353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.416683903478" Y="27.4785831266" />
                  <Point X="3.763989803788" Y="27.197340353895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770132868823" Y="29.743960056259" />
                  <Point X="2.49670966296" Y="28.345805736282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.504809752148" Y="27.529462380196" />
                  <Point X="3.841499924497" Y="27.256816054494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.971328196529" Y="29.703277451089" />
                  <Point X="2.544801777938" Y="28.429103668211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592935600817" Y="27.580341633791" />
                  <Point X="3.919010057529" Y="27.316291745113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.186410163271" Y="29.651349667353" />
                  <Point X="2.592893892915" Y="28.512401600139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.681061449486" Y="27.631220887387" />
                  <Point X="3.996520289713" Y="27.375767355441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.401491392892" Y="29.599422480527" />
                  <Point X="2.640986020242" Y="28.595699522067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.769187298156" Y="27.682100140982" />
                  <Point X="4.074030521898" Y="27.435242965769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.660777653602" Y="29.511698765336" />
                  <Point X="2.689078173468" Y="28.678997423022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857313146825" Y="27.732979394578" />
                  <Point X="4.109457040899" Y="27.528797295089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.957508022765" Y="29.393653408984" />
                  <Point X="2.737170326694" Y="28.762295323978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347394329643" Y="29.200171861672" />
                  <Point X="2.78526247992" Y="28.845593224933" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.731959533691" Y="20.490986328125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318969727" Y="21.478455078125" />
                  <Point X="0.357878479004" Y="21.631814453125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.109625595093" Y="21.78471875" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008663995743" Y="21.812234375" />
                  <Point X="-0.173374313354" Y="21.76111328125" />
                  <Point X="-0.262023132324" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.394719329834" Y="21.56099609375" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.599387878418" Y="20.93980078125" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.996843261719" Y="20.04186328125" />
                  <Point X="-1.100258056641" Y="20.0619375" />
                  <Point X="-1.291021240234" Y="20.111017578125" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.340498779297" Y="20.210845703125" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.349032714844" Y="20.6630625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.537927001953" Y="20.930359375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.850506713867" Y="21.027427734375" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.157583984375" Y="20.91415234375" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.338423583984" Y="20.740154296875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.704269775391" Y="20.738544921875" />
                  <Point X="-2.855836914062" Y="20.832390625" />
                  <Point X="-3.120001953125" Y="21.0357890625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.989995605469" Y="21.5326328125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979736328" Y="22.431236328125" />
                  <Point X="-2.531328369141" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.042836181641" Y="22.196056640625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.041959228516" Y="21.99555078125" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.351091308594" Y="22.4704453125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.010563232422" Y="22.927099609375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.161160644531" Y="23.689361328125" />
                  <Point X="-3.187643310547" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.789066894531" Y="23.636447265625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.880560058594" Y="23.805458984375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.977500488281" Y="24.33915234375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.522069824219" Y="24.612888671875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.54189453125" Y="24.878576171875" />
                  <Point X="-3.523852294922" Y="24.89109765625" />
                  <Point X="-3.514141601562" Y="24.897837890625" />
                  <Point X="-3.494899414062" Y="24.92408984375" />
                  <Point X="-3.488885253906" Y="24.94346875" />
                  <Point X="-3.4856484375" Y="24.953896484375" />
                  <Point X="-3.485647705078" Y="24.9835390625" />
                  <Point X="-3.491661865234" Y="25.00291796875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.514141601562" Y="25.0396015625" />
                  <Point X="-3.532183837891" Y="25.052123046875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.076624267578" Y="25.205193359375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.947827636719" Y="25.792447265625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.816780761719" Y="26.36863671875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.444719238281" Y="26.48501171875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731703125" Y="26.3958671875" />
                  <Point X="-3.691769775391" Y="26.40845703125" />
                  <Point X="-3.670277099609" Y="26.415234375" />
                  <Point X="-3.651533691406" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.623096435547" Y="26.48246875" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.61631640625" Y="26.54551171875" />
                  <Point X="-3.635650390625" Y="26.58265234375" />
                  <Point X="-3.646056152344" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.957760009766" Y="26.847724609375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.277293945313" Y="27.586078125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.892827880859" Y="28.130435546875" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.583828125" Y="28.172126953125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515380859" Y="27.92043359375" />
                  <Point X="-3.082899169922" Y="27.915568359375" />
                  <Point X="-3.052966064453" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.973776123047" Y="27.966880859375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942940185547" Y="28.08345703125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.084028076172" Y="28.36259375" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-2.957138427734" Y="29.017724609375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.332065917969" Y="29.408125" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.097831542969" Y="29.456998046875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247436523" Y="29.273662109375" />
                  <Point X="-1.889346923828" Y="29.2414375" />
                  <Point X="-1.85603137207" Y="29.22409375" />
                  <Point X="-1.835124755859" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.749335693359" Y="29.24895703125" />
                  <Point X="-1.714635375977" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.665098388672" Y="29.361044921875" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.666919921875" Y="29.532380859375" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.232524536133" Y="29.829158203125" />
                  <Point X="-0.968083435059" Y="29.903296875" />
                  <Point X="-0.457944061279" Y="29.963001953125" />
                  <Point X="-0.224200134277" Y="29.990359375" />
                  <Point X="-0.167769882202" Y="29.7797578125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282114029" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594032288" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.122067337036" Y="29.56324609375" />
                  <Point X="0.236648376465" Y="29.9908671875" />
                  <Point X="0.62931817627" Y="29.949744140625" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="1.282266113281" Y="29.823666015625" />
                  <Point X="1.508455078125" Y="29.769056640625" />
                  <Point X="1.783630981445" Y="29.66925" />
                  <Point X="1.931045410156" Y="29.61578125" />
                  <Point X="2.196675048828" Y="29.4915546875" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.595318847656" Y="29.275626953125" />
                  <Point X="2.732533691406" Y="29.195685546875" />
                  <Point X="2.974550048828" Y="29.023576171875" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.790003173828" Y="28.4738046875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.21089453125" Y="27.439318359375" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.207487060547" Y="27.34719140625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682617188" Y="27.300810546875" />
                  <Point X="2.246609619141" Y="27.25965234375" />
                  <Point X="2.261640380859" Y="27.237501953125" />
                  <Point X="2.274939208984" Y="27.224205078125" />
                  <Point X="2.316096435547" Y="27.19627734375" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360335693359" Y="27.172978515625" />
                  <Point X="2.405469238281" Y="27.167537109375" />
                  <Point X="2.429760498047" Y="27.164607421875" />
                  <Point X="2.448663818359" Y="27.1659453125" />
                  <Point X="2.500858398438" Y="27.179904296875" />
                  <Point X="2.528950195312" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.063209472656" Y="27.493896484375" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.121206542969" Y="27.854986328125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.337518066406" Y="27.518912109375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.026105712891" Y="27.158978515625" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.241806884766" Y="26.534826171875" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.199126464844" Y="26.44146484375" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.202266113281" Y="26.335294921875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.246888916016" Y="26.24046875" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.326222412109" Y="26.173333984375" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.368566650391" Y="26.153619140625" />
                  <Point X="3.429781005859" Y="26.145529296875" />
                  <Point X="3.462727294922" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.971835449219" Y="26.206474609375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.904233886719" Y="26.094966796875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.981707519531" Y="25.67829296875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.586346679688" Y="25.464291015625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087646484" Y="25.232818359375" />
                  <Point X="3.668946289062" Y="25.198056640625" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.622264404297" Y="25.16692578125" />
                  <Point X="3.5861796875" Y="25.1209453125" />
                  <Point X="3.566758544922" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.544956787109" Y="25.011927734375" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.550511474609" Y="24.8965078125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758544922" Y="24.8412421875" />
                  <Point X="3.602843261719" Y="24.79526171875" />
                  <Point X="3.622264404297" Y="24.770513671875" />
                  <Point X="3.636575683594" Y="24.75809375" />
                  <Point X="3.696717041016" Y="24.72333203125" />
                  <Point X="3.729085693359" Y="24.70462109375" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.196051269531" Y="24.5777265625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.967948730469" Y="24.163052734375" />
                  <Point X="4.948430664062" Y="24.033595703125" />
                  <Point X="4.893957519531" Y="23.79488671875" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="4.392151855469" Y="23.773330078125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394837158203" Y="23.901658203125" />
                  <Point X="3.276801025391" Y="23.876001953125" />
                  <Point X="3.213272949219" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.114099853516" Y="23.75949609375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.054132324219" Y="23.574806640625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.121683349609" Y="23.3817734375" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.590595458984" Y="22.99054296875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.259147949219" Y="22.2868828125" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.091476074219" Y="22.037791015625" />
                  <Point X="4.056687744141" Y="21.988361328125" />
                  <Point X="3.626427978516" Y="22.236771484375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.746685546875" />
                  <Point X="2.596858642578" Y="22.772056640625" />
                  <Point X="2.52125" Y="22.7857109375" />
                  <Point X="2.489077148438" Y="22.78075390625" />
                  <Point X="2.372371337891" Y="22.719333984375" />
                  <Point X="2.309559082031" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.227178466797" Y="22.548611328125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.214533935547" Y="22.313142578125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.505355712891" Y="21.751578125" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.900581298828" Y="20.85641796875" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.709342041016" Y="20.7282578125" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.349336914062" Y="21.139755859375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.53199621582" Y="22.108609375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805297852" Y="22.16428125" />
                  <Point X="1.274274169922" Y="22.150337890625" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.048323730469" Y="22.034201171875" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.933471801758" Y="21.7930546875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.990803527832" Y="21.10536328125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.056107299805" Y="20.0502890625" />
                  <Point X="0.994372619629" Y="20.0367578125" />
                  <Point X="0.877987854004" Y="20.015615234375" />
                  <Point X="0.860200378418" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#182" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.125629508639" Y="4.823980206672" Z="1.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="-0.469912334385" Y="5.043943110425" Z="1.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.65" />
                  <Point X="-1.252178203445" Y="4.908583028187" Z="1.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.65" />
                  <Point X="-1.722648312199" Y="4.557135362634" Z="1.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.65" />
                  <Point X="-1.718940200075" Y="4.407359683241" Z="1.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.65" />
                  <Point X="-1.77462546924" Y="4.326430634745" Z="1.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.65" />
                  <Point X="-1.872414581505" Y="4.317067670483" Z="1.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.65" />
                  <Point X="-2.064319912689" Y="4.518716837131" Z="1.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.65" />
                  <Point X="-2.362504832927" Y="4.483112020102" Z="1.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.65" />
                  <Point X="-2.993716423558" Y="4.088685555161" Z="1.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.65" />
                  <Point X="-3.133485194254" Y="3.368875200345" Z="1.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.65" />
                  <Point X="-2.998905846443" Y="3.11037973065" Z="1.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.65" />
                  <Point X="-3.015286797011" Y="3.033516770875" Z="1.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.65" />
                  <Point X="-3.084696734957" Y="2.99665885273" Z="1.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.65" />
                  <Point X="-3.564984147952" Y="3.246708754492" Z="1.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.65" />
                  <Point X="-3.938447569163" Y="3.192419266796" Z="1.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.65" />
                  <Point X="-4.326631857437" Y="2.642563931115" Z="1.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.65" />
                  <Point X="-3.994353963097" Y="1.839337683475" Z="1.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.65" />
                  <Point X="-3.686156501953" Y="1.590844785858" Z="1.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.65" />
                  <Point X="-3.675446429225" Y="1.532884212133" Z="1.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.65" />
                  <Point X="-3.712962534561" Y="1.487423517907" Z="1.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.65" />
                  <Point X="-4.444349026869" Y="1.565864070969" Z="1.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.65" />
                  <Point X="-4.871196482541" Y="1.412996219563" Z="1.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.65" />
                  <Point X="-5.003445168995" Y="0.831045370337" Z="1.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.65" />
                  <Point X="-4.095721209114" Y="0.188177663805" Z="1.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.65" />
                  <Point X="-3.566850179384" Y="0.042329351451" Z="1.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.65" />
                  <Point X="-3.5455709517" Y="0.019377686936" Z="1.65" />
                  <Point X="-3.539556741714" Y="0" Z="1.65" />
                  <Point X="-3.542793649435" Y="-0.010429264125" Z="1.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.65" />
                  <Point X="-3.55851841574" Y="-0.036546631683" Z="1.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.65" />
                  <Point X="-4.541166579658" Y="-0.307534392951" Z="1.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.65" />
                  <Point X="-5.033152444448" Y="-0.636645021899" Z="1.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.65" />
                  <Point X="-4.935160716439" Y="-1.175635487009" Z="1.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.65" />
                  <Point X="-3.788696396723" Y="-1.381844329081" Z="1.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.65" />
                  <Point X="-3.209892799467" Y="-1.312316924358" Z="1.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.65" />
                  <Point X="-3.195372060448" Y="-1.332858219357" Z="1.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.65" />
                  <Point X="-4.047157140572" Y="-2.00195148008" Z="1.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.65" />
                  <Point X="-4.40019080394" Y="-2.523884372212" Z="1.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.65" />
                  <Point X="-4.08790118129" Y="-3.003452242808" Z="1.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.65" />
                  <Point X="-3.023992208709" Y="-2.815964105668" Z="1.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.65" />
                  <Point X="-2.566769358056" Y="-2.561561111075" Z="1.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.65" />
                  <Point X="-3.039452907501" Y="-3.411086173783" Z="1.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.65" />
                  <Point X="-3.156661884968" Y="-3.972547513625" Z="1.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.65" />
                  <Point X="-2.736747004072" Y="-4.272687029783" Z="1.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.65" />
                  <Point X="-2.304911785335" Y="-4.259002307355" Z="1.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.65" />
                  <Point X="-2.135961502823" Y="-4.096141769278" Z="1.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.65" />
                  <Point X="-1.859932707158" Y="-3.991184258974" Z="1.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.65" />
                  <Point X="-1.577050342366" Y="-4.075951787535" Z="1.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.65" />
                  <Point X="-1.404227808407" Y="-4.315410260848" Z="1.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.65" />
                  <Point X="-1.396226996457" Y="-4.751347331373" Z="1.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.65" />
                  <Point X="-1.309636461511" Y="-4.906123274894" Z="1.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.65" />
                  <Point X="-1.012526887399" Y="-4.975940169876" Z="1.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="-0.557247649777" Y="-4.04186060328" Z="1.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="-0.359799490546" Y="-3.436233016523" Z="1.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="-0.164710335927" Y="-3.255359378332" Z="1.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.65" />
                  <Point X="0.088648743434" Y="-3.231752666957" Z="1.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.65" />
                  <Point X="0.310646368887" Y="-3.365412739245" Z="1.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.65" />
                  <Point X="0.677507210624" Y="-4.490675426536" Z="1.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.65" />
                  <Point X="0.880768635723" Y="-5.002299882919" Z="1.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.65" />
                  <Point X="1.060656110338" Y="-4.967269420509" Z="1.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.65" />
                  <Point X="1.034219943712" Y="-3.856830841699" Z="1.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.65" />
                  <Point X="0.976175044097" Y="-3.186284326887" Z="1.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.65" />
                  <Point X="1.074135189454" Y="-2.972964221958" Z="1.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.65" />
                  <Point X="1.27269931876" Y="-2.868170622319" Z="1.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.65" />
                  <Point X="1.498801003679" Y="-2.902168561089" Z="1.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.65" />
                  <Point X="2.303513134109" Y="-3.859400712666" Z="1.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.65" />
                  <Point X="2.730355334248" Y="-4.282435738058" Z="1.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.65" />
                  <Point X="2.923487119629" Y="-4.152989213659" Z="1.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.65" />
                  <Point X="2.54250097529" Y="-3.192141592205" Z="1.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.65" />
                  <Point X="2.257582021067" Y="-2.646689940221" Z="1.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.65" />
                  <Point X="2.265268748407" Y="-2.443395971216" Z="1.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.65" />
                  <Point X="2.389502407156" Y="-2.29363256156" Z="1.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.65" />
                  <Point X="2.581816863292" Y="-2.245865988424" Z="1.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.65" />
                  <Point X="3.595272004902" Y="-2.775248524985" Z="1.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.65" />
                  <Point X="4.126208959577" Y="-2.959706466834" Z="1.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.65" />
                  <Point X="4.295525499896" Y="-2.708121650852" Z="1.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.65" />
                  <Point X="3.614877568823" Y="-1.938508454239" Z="1.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.65" />
                  <Point X="3.157585395189" Y="-1.559907958202" Z="1.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.65" />
                  <Point X="3.097765967032" Y="-1.398495076051" Z="1.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.65" />
                  <Point X="3.146390215393" Y="-1.241190446003" Z="1.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.65" />
                  <Point X="3.281263742898" Y="-1.141576062817" Z="1.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.65" />
                  <Point X="4.379469615134" Y="-1.244962211043" Z="1.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.65" />
                  <Point X="4.936548841618" Y="-1.184956267733" Z="1.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.65" />
                  <Point X="5.011234229298" Y="-0.813121155177" Z="1.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.65" />
                  <Point X="4.202836337661" Y="-0.342696334733" Z="1.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.65" />
                  <Point X="3.715584039563" Y="-0.202100971147" Z="1.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.65" />
                  <Point X="3.636021518632" Y="-0.142591039697" Z="1.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.65" />
                  <Point X="3.593462991689" Y="-0.062807115867" Z="1.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.65" />
                  <Point X="3.587908458735" Y="0.033803415333" Z="1.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.65" />
                  <Point X="3.619357919768" Y="0.121357698878" Z="1.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.65" />
                  <Point X="3.68781137479" Y="0.186047873095" Z="1.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.65" />
                  <Point X="4.593131479646" Y="0.447275598036" Z="1.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.65" />
                  <Point X="5.02495634205" Y="0.717264047407" Z="1.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.65" />
                  <Point X="4.946659059787" Y="1.138074233444" Z="1.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.65" />
                  <Point X="3.959153121651" Y="1.287327916409" Z="1.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.65" />
                  <Point X="3.430175145997" Y="1.22637833383" Z="1.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.65" />
                  <Point X="3.344593299115" Y="1.248185269817" Z="1.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.65" />
                  <Point X="3.282503516313" Y="1.299229146157" Z="1.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.65" />
                  <Point X="3.245078737367" Y="1.376678780796" Z="1.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.65" />
                  <Point X="3.241123128566" Y="1.459278597149" Z="1.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.65" />
                  <Point X="3.275333603712" Y="1.535689178235" Z="1.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.65" />
                  <Point X="4.050387674845" Y="2.150590690147" Z="1.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.65" />
                  <Point X="4.374139344331" Y="2.576079715703" Z="1.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.65" />
                  <Point X="4.155636071235" Y="2.915470254719" Z="1.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.65" />
                  <Point X="3.032053401006" Y="2.568476905092" Z="1.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.65" />
                  <Point X="2.481786262754" Y="2.259486692696" Z="1.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.65" />
                  <Point X="2.405300332547" Y="2.248458258457" Z="1.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.65" />
                  <Point X="2.338015467848" Y="2.268931101696" Z="1.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.65" />
                  <Point X="2.281827498539" Y="2.319009392534" Z="1.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.65" />
                  <Point X="2.250971444302" Y="2.384458111286" Z="1.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.65" />
                  <Point X="2.253041249961" Y="2.457683313128" Z="1.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.65" />
                  <Point X="2.827148447767" Y="3.480085680949" Z="1.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.65" />
                  <Point X="2.997371403445" Y="4.095602595563" Z="1.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.65" />
                  <Point X="2.614332830181" Y="4.350109857857" Z="1.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.65" />
                  <Point X="2.211700472314" Y="4.568126497772" Z="1.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.65" />
                  <Point X="1.794523883314" Y="4.74753386843" Z="1.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.65" />
                  <Point X="1.287844504293" Y="4.903550857606" Z="1.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.65" />
                  <Point X="0.628369881515" Y="5.030753790179" Z="1.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.65" />
                  <Point X="0.067615052914" Y="4.607467299279" Z="1.65" />
                  <Point X="0" Y="4.355124473572" Z="1.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>