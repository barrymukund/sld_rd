<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#139" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1007" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.65024230957" Y="21.163009765625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362548828" Y="21.532625" />
                  <Point X="0.506827850342" Y="21.583822265625" />
                  <Point X="0.378634918213" Y="21.768525390625" />
                  <Point X="0.356748718262" Y="21.790982421875" />
                  <Point X="0.330493682861" Y="21.810224609375" />
                  <Point X="0.302492889404" Y="21.82433203125" />
                  <Point X="0.247505004883" Y="21.841396484375" />
                  <Point X="0.049133758545" Y="21.90296484375" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658312798" Y="21.907234375" />
                  <Point X="-0.036824390411" Y="21.90296484375" />
                  <Point X="-0.091812286377" Y="21.8858984375" />
                  <Point X="-0.290183532715" Y="21.82433203125" />
                  <Point X="-0.31818460083" Y="21.810224609375" />
                  <Point X="-0.344440093994" Y="21.79098046875" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.401858459473" Y="21.71732421875" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.864630981445" Y="20.316951171875" />
                  <Point X="-0.916584777832" Y="20.12305859375" />
                  <Point X="-1.079314086914" Y="20.15464453125" />
                  <Point X="-1.139292114258" Y="20.170076171875" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.225428588867" Y="20.35706640625" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.229645141602" Y="20.54981640625" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323645751953" Y="20.868796875" />
                  <Point X="-1.374271484375" Y="20.913193359375" />
                  <Point X="-1.556906616211" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643027099609" Y="21.109033203125" />
                  <Point X="-1.710218994141" Y="21.1134375" />
                  <Point X="-1.952616210938" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657836914" Y="21.10519921875" />
                  <Point X="-2.098645751953" Y="21.0677890625" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312788574219" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.480148681641" Y="20.711509765625" />
                  <Point X="-2.801711669922" Y="20.91061328125" />
                  <Point X="-2.884723144531" Y="20.974529296875" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.585330078125" Y="22.04353515625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858154297" Y="22.3523515625" />
                  <Point X="-2.406587890625" Y="22.3838828125" />
                  <Point X="-2.405577392578" Y="22.414822265625" />
                  <Point X="-2.414567138672" Y="22.444443359375" />
                  <Point X="-2.428791259766" Y="22.473275390625" />
                  <Point X="-2.446816162109" Y="22.498423828125" />
                  <Point X="-2.464156005859" Y="22.51576171875" />
                  <Point X="-2.489305908203" Y="22.533783203125" />
                  <Point X="-2.518135253906" Y="22.548001953125" />
                  <Point X="-2.547755126953" Y="22.556986328125" />
                  <Point X="-2.57869140625" Y="22.555974609375" />
                  <Point X="-2.610218261719" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.648737548828" Y="21.95593359375" />
                  <Point X="-3.818023925781" Y="21.858197265625" />
                  <Point X="-4.082861083984" Y="22.206140625" />
                  <Point X="-4.142372558594" Y="22.305931640625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.388647460938" Y="23.28456640625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.064015625" Y="23.554431640625" />
                  <Point X="-3.055629882812" Y="23.57595703125" />
                  <Point X="-3.052184570312" Y="23.58662109375" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037597656" Y="23.641396484375" />
                  <Point X="-3.042736816406" Y="23.664263671875" />
                  <Point X="-3.0488828125" Y="23.68630078125" />
                  <Point X="-3.060888183594" Y="23.71528515625" />
                  <Point X="-3.073480712891" Y="23.737013671875" />
                  <Point X="-3.091326171875" Y="23.754681640625" />
                  <Point X="-3.109576416016" Y="23.768494140625" />
                  <Point X="-3.118722412109" Y="23.7746171875" />
                  <Point X="-3.139457275391" Y="23.7868203125" />
                  <Point X="-3.168721923828" Y="23.798044921875" />
                  <Point X="-3.200608886719" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.506395507812" Y="23.637830078125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.849822753906" Y="24.11743359375" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.854905761719" Y="24.693302734375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.513108154297" Y="24.78734375" />
                  <Point X="-3.491515869141" Y="24.798744140625" />
                  <Point X="-3.481703125" Y="24.8047109375" />
                  <Point X="-3.459973632812" Y="24.81979296875" />
                  <Point X="-3.43601953125" Y="24.841318359375" />
                  <Point X="-3.415523193359" Y="24.870744140625" />
                  <Point X="-3.406090332031" Y="24.891951171875" />
                  <Point X="-3.402160400391" Y="24.902400390625" />
                  <Point X="-3.394917236328" Y="24.92573828125" />
                  <Point X="-3.389474365234" Y="24.9544765625" />
                  <Point X="-3.390242919922" Y="24.9873359375" />
                  <Point X="-3.394497802734" Y="25.008626953125" />
                  <Point X="-3.396925048828" Y="25.018169921875" />
                  <Point X="-3.404168212891" Y="25.0415078125" />
                  <Point X="-3.417486816406" Y="25.070833984375" />
                  <Point X="-3.43957421875" Y="25.099412109375" />
                  <Point X="-3.457413818359" Y="25.115107421875" />
                  <Point X="-3.465997070312" Y="25.121826171875" />
                  <Point X="-3.4877265625" Y="25.136908203125" />
                  <Point X="-3.501924072266" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.694612304688" Y="25.469134765625" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.891743164062" Y="25.52246875" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.792791992188" Y="26.09394140625" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.996505126953" Y="26.33018359375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703139648438" Y="26.305263671875" />
                  <Point X="-3.689808105469" Y="26.309466796875" />
                  <Point X="-3.641713623047" Y="26.324630859375" />
                  <Point X="-3.622784667969" Y="26.332958984375" />
                  <Point X="-3.604040283203" Y="26.343779296875" />
                  <Point X="-3.587355224609" Y="26.35601171875" />
                  <Point X="-3.573714111328" Y="26.37156640625" />
                  <Point X="-3.561299560547" Y="26.389296875" />
                  <Point X="-3.551351806641" Y="26.4074296875" />
                  <Point X="-3.546002441406" Y="26.42034375" />
                  <Point X="-3.526704345703" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532051025391" Y="26.58937890625" />
                  <Point X="-3.538505615234" Y="26.60177734375" />
                  <Point X="-3.561790771484" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.268510742188" Y="27.20591796875" />
                  <Point X="-4.351860839844" Y="27.269875" />
                  <Point X="-4.342482421875" Y="27.28594140625" />
                  <Point X="-4.081155761719" Y="27.733654296875" />
                  <Point X="-3.997194335938" Y="27.841576171875" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.348431884766" Y="27.926525390625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728515625" Y="27.836341796875" />
                  <Point X="-3.167085693359" Y="27.82983203125" />
                  <Point X="-3.146796875" Y="27.825794921875" />
                  <Point X="-3.128229492188" Y="27.824169921875" />
                  <Point X="-3.061247558594" Y="27.818310546875" />
                  <Point X="-3.040559814453" Y="27.81876171875" />
                  <Point X="-3.019101806641" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946076660156" Y="27.86023046875" />
                  <Point X="-2.932897460938" Y="27.87341015625" />
                  <Point X="-2.885353027344" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845060302734" Y="28.0546875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.165086181641" Y="28.692990234375" />
                  <Point X="-3.183333740234" Y="28.724595703125" />
                  <Point X="-3.155773681641" Y="28.7457265625" />
                  <Point X="-2.700627685547" Y="29.09468359375" />
                  <Point X="-2.568395751953" Y="29.168146484375" />
                  <Point X="-2.167037109375" Y="29.3911328125" />
                  <Point X="-2.086596679688" Y="29.28630078125" />
                  <Point X="-2.043194824219" Y="29.229740234375" />
                  <Point X="-2.028887817383" Y="29.214794921875" />
                  <Point X="-2.012308959961" Y="29.200884765625" />
                  <Point X="-1.995114013672" Y="29.189396484375" />
                  <Point X="-1.974448730469" Y="29.178638671875" />
                  <Point X="-1.899897949219" Y="29.139828125" />
                  <Point X="-1.880625366211" Y="29.13233203125" />
                  <Point X="-1.859718383789" Y="29.126728515625" />
                  <Point X="-1.839268798828" Y="29.123580078125" />
                  <Point X="-1.818622680664" Y="29.12493359375" />
                  <Point X="-1.797306884766" Y="29.128693359375" />
                  <Point X="-1.777452026367" Y="29.134482421875" />
                  <Point X="-1.755927978516" Y="29.1433984375" />
                  <Point X="-1.678278076172" Y="29.1755625" />
                  <Point X="-1.660145996094" Y="29.185509765625" />
                  <Point X="-1.642416381836" Y="29.197923828125" />
                  <Point X="-1.626864135742" Y="29.2115625" />
                  <Point X="-1.61463269043" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265919921875" />
                  <Point X="-1.588474731445" Y="29.288138671875" />
                  <Point X="-1.563201171875" Y="29.368296875" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.538852294922" Y="29.644611328125" />
                  <Point X="-0.949622619629" Y="29.80980859375" />
                  <Point X="-0.789337768555" Y="29.8285703125" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.175844223022" Y="29.44283984375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425926208" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.297518890381" Y="29.85098828125" />
                  <Point X="0.307419555664" Y="29.8879375" />
                  <Point X="0.329561981201" Y="29.885619140625" />
                  <Point X="0.844032348633" Y="29.831740234375" />
                  <Point X="0.976665161133" Y="29.79971875" />
                  <Point X="1.481037475586" Y="29.6779453125" />
                  <Point X="1.566190673828" Y="29.647060546875" />
                  <Point X="1.894645141602" Y="29.527927734375" />
                  <Point X="1.97812109375" Y="29.488888671875" />
                  <Point X="2.294564941406" Y="29.3408984375" />
                  <Point X="2.375249511719" Y="29.293892578125" />
                  <Point X="2.680984130859" Y="29.115771484375" />
                  <Point X="2.757036132812" Y="29.0616875" />
                  <Point X="2.943260498047" Y="28.92925390625" />
                  <Point X="2.334584716797" Y="27.874998046875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.139568847656" Y="27.53315625" />
                  <Point X="2.129907714844" Y="27.503646484375" />
                  <Point X="2.128417236328" Y="27.498630859375" />
                  <Point X="2.111607177734" Y="27.43576953125" />
                  <Point X="2.108391845703" Y="27.40988671875" />
                  <Point X="2.10887109375" Y="27.37591796875" />
                  <Point X="2.109544677734" Y="27.365884765625" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140071289062" Y="27.247470703125" />
                  <Point X="2.149394775391" Y="27.23373046875" />
                  <Point X="2.183029052734" Y="27.184162109375" />
                  <Point X="2.200676757812" Y="27.16464453125" />
                  <Point X="2.227716796875" Y="27.14201953125" />
                  <Point X="2.235338378906" Y="27.136267578125" />
                  <Point X="2.284906738281" Y="27.1026328125" />
                  <Point X="2.304953857422" Y="27.09226953125" />
                  <Point X="2.327042480469" Y="27.08400390625" />
                  <Point X="2.348967041016" Y="27.078662109375" />
                  <Point X="2.364034667969" Y="27.076845703125" />
                  <Point X="2.418391845703" Y="27.070291015625" />
                  <Point X="2.445237304688" Y="27.070876953125" />
                  <Point X="2.481565429688" Y="27.076875" />
                  <Point X="2.490632568359" Y="27.078830078125" />
                  <Point X="2.553493896484" Y="27.095640625" />
                  <Point X="2.565288085938" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.757017089844" Y="27.78476953125" />
                  <Point X="3.967325927734" Y="27.90619140625" />
                  <Point X="4.123276855469" Y="27.689455078125" />
                  <Point X="4.165673828125" Y="27.619392578125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.476991943359" Y="26.857373046875" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221423339844" Y="26.660240234375" />
                  <Point X="3.203975341797" Y="26.64162890625" />
                  <Point X="3.191434570312" Y="26.62526953125" />
                  <Point X="3.146193115234" Y="26.566248046875" />
                  <Point X="3.133095703125" Y="26.54300390625" />
                  <Point X="3.119954833984" Y="26.509349609375" />
                  <Point X="3.116958251953" Y="26.5003828125" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.101573974609" Y="26.353181640625" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.124524902344" Y="26.260814453125" />
                  <Point X="3.142153320312" Y="26.22758203125" />
                  <Point X="3.146713134766" Y="26.219884765625" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198895996094" Y="26.145447265625" />
                  <Point X="3.216138916016" Y="26.129359375" />
                  <Point X="3.234348388672" Y="26.116033203125" />
                  <Point X="3.249463134766" Y="26.107525390625" />
                  <Point X="3.303990478516" Y="26.076830078125" />
                  <Point X="3.320521972656" Y="26.0695" />
                  <Point X="3.356117919922" Y="26.0594375" />
                  <Point X="3.376553955078" Y="26.056736328125" />
                  <Point X="3.450278564453" Y="26.046994140625" />
                  <Point X="3.462698730469" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.598184570312" Y="26.193115234375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.859297851562" Y="25.846986328125" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="3.998739257813" Y="25.4051953125" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704785888672" Y="25.325583984375" />
                  <Point X="3.681546386719" Y="25.31506640625" />
                  <Point X="3.661468505859" Y="25.3034609375" />
                  <Point X="3.589036376953" Y="25.261595703125" />
                  <Point X="3.567673828125" Y="25.244748046875" />
                  <Point X="3.541314453125" Y="25.2169765625" />
                  <Point X="3.535484619141" Y="25.2102265625" />
                  <Point X="3.492025390625" Y="25.154849609375" />
                  <Point X="3.480300537109" Y="25.135568359375" />
                  <Point X="3.470526611328" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.459665283203" Y="25.071634765625" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443483154297" Y="24.978123046875" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.449194580078" Y="24.920478515625" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526611328" Y="24.8233359375" />
                  <Point X="3.480300537109" Y="24.80187109375" />
                  <Point X="3.492022949219" Y="24.78259375" />
                  <Point X="3.504069580078" Y="24.7672421875" />
                  <Point X="3.547528808594" Y="24.711865234375" />
                  <Point X="3.559999755859" Y="24.69876171875" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.60911328125" Y="24.66423828125" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692708496094" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.734482421875" Y="24.3351015625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855021484375" Y="24.051267578125" />
                  <Point X="4.837903320312" Y="23.97625390625" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="3.751607910156" Y="23.953478515625" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659912109" Y="23.994490234375" />
                  <Point X="3.335254150391" Y="23.98592578125" />
                  <Point X="3.193095703125" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112396972656" Y="23.9060390625" />
                  <Point X="3.088578613281" Y="23.877392578125" />
                  <Point X="3.002652832031" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.96634375" Y="23.657537109375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="2.998258789062" Y="23.39808203125" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.055248291016" Y="22.5142578125" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.124817382812" Y="22.250224609375" />
                  <Point X="4.089405761719" Y="22.19991015625" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.092393310547" Y="22.65479296875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754225830078" Y="22.840173828125" />
                  <Point X="2.707326660156" Y="22.84864453125" />
                  <Point X="2.538135498047" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444835449219" Y="22.864822265625" />
                  <Point X="2.405873535156" Y="22.844318359375" />
                  <Point X="2.265317382812" Y="22.77034375" />
                  <Point X="2.242385009766" Y="22.753451171875" />
                  <Point X="2.221425537109" Y="22.7324921875" />
                  <Point X="2.204532226563" Y="22.7095625" />
                  <Point X="2.184026855469" Y="22.6706015625" />
                  <Point X="2.110053222656" Y="22.530044921875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.104145263672" Y="22.389841796875" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.758832519531" Y="21.12254296875" />
                  <Point X="2.861283935547" Y="20.94509375" />
                  <Point X="2.781853271484" Y="20.888359375" />
                  <Point X="2.742266845703" Y="20.862734375" />
                  <Point X="2.701764160156" Y="20.836517578125" />
                  <Point X="1.980940673828" Y="21.7759140625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.675668945312" Y="22.129181640625" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417101318359" Y="22.258880859375" />
                  <Point X="1.366513183594" Y="22.2542265625" />
                  <Point X="1.184014526367" Y="22.23743359375" />
                  <Point X="1.156362426758" Y="22.2306015625" />
                  <Point X="1.128976928711" Y="22.2192578125" />
                  <Point X="1.104594604492" Y="22.204537109375" />
                  <Point X="1.065531616211" Y="22.172056640625" />
                  <Point X="0.924610900879" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.863944824219" Y="21.920455078125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.991765075684" Y="20.370236328125" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975706237793" Y="20.129921875" />
                  <Point X="0.939108886719" Y="20.1232734375" />
                  <Point X="0.929315917969" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058409423828" Y="20.247359375" />
                  <Point X="-1.115620727539" Y="20.262080078125" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.131241333008" Y="20.344666015625" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.136470458984" Y="20.568349609375" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230574707031" Y="20.905138671875" />
                  <Point X="-1.250210693359" Y="20.929064453125" />
                  <Point X="-1.261008789063" Y="20.94022265625" />
                  <Point X="-1.311634521484" Y="20.984619140625" />
                  <Point X="-1.49426965332" Y="21.144787109375" />
                  <Point X="-1.506739746094" Y="21.15403515625" />
                  <Point X="-1.533022338867" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315551758" Y="21.194525390625" />
                  <Point X="-1.621456176758" Y="21.201552734375" />
                  <Point X="-1.636813232422" Y="21.203830078125" />
                  <Point X="-1.704005249023" Y="21.208234375" />
                  <Point X="-1.94640246582" Y="21.22412109375" />
                  <Point X="-1.961927246094" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048339844" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095437255859" Y="21.184189453125" />
                  <Point X="-2.151425048828" Y="21.146779296875" />
                  <Point X="-2.353403320312" Y="21.011822265625" />
                  <Point X="-2.359688476562" Y="21.00723828125" />
                  <Point X="-2.380446533203" Y="20.989865234375" />
                  <Point X="-2.402760986328" Y="20.9672265625" />
                  <Point X="-2.410471679688" Y="20.958369140625" />
                  <Point X="-2.503201904297" Y="20.837521484375" />
                  <Point X="-2.747586669922" Y="20.988837890625" />
                  <Point X="-2.826765869141" Y="21.049802734375" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.503057617188" Y="21.99603515625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849365234" Y="22.289919921875" />
                  <Point X="-2.323946289062" Y="22.318890625" />
                  <Point X="-2.319682617188" Y="22.333822265625" />
                  <Point X="-2.313412353516" Y="22.365353515625" />
                  <Point X="-2.311638427734" Y="22.38078125" />
                  <Point X="-2.310627929688" Y="22.411720703125" />
                  <Point X="-2.314671630859" Y="22.442412109375" />
                  <Point X="-2.323661376953" Y="22.472033203125" />
                  <Point X="-2.329370849609" Y="22.486474609375" />
                  <Point X="-2.343594970703" Y="22.515306640625" />
                  <Point X="-2.351576416016" Y="22.528619140625" />
                  <Point X="-2.369601318359" Y="22.553767578125" />
                  <Point X="-2.379644775391" Y="22.565603515625" />
                  <Point X="-2.396984619141" Y="22.58294140625" />
                  <Point X="-2.408822021484" Y="22.592982421875" />
                  <Point X="-2.433971923828" Y="22.61100390625" />
                  <Point X="-2.447284423828" Y="22.618984375" />
                  <Point X="-2.476113769531" Y="22.633203125" />
                  <Point X="-2.490560302734" Y="22.638912109375" />
                  <Point X="-2.520180175781" Y="22.647896484375" />
                  <Point X="-2.550860351562" Y="22.651935546875" />
                  <Point X="-2.581796630859" Y="22.650923828125" />
                  <Point X="-2.597226074219" Y="22.6491484375" />
                  <Point X="-2.628752929688" Y="22.642876953125" />
                  <Point X="-2.64368359375" Y="22.63861328125" />
                  <Point X="-2.672648925781" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.696237548828" Y="22.038205078125" />
                  <Point X="-3.793088378906" Y="21.9822890625" />
                  <Point X="-4.004015869141" Y="22.25940625" />
                  <Point X="-4.060780029297" Y="22.35458984375" />
                  <Point X="-4.181265625" Y="22.556625" />
                  <Point X="-3.330815185547" Y="23.209197265625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.002627929688" Y="23.473572265625" />
                  <Point X="-2.983578613281" Y="23.50388671875" />
                  <Point X="-2.975495605469" Y="23.519947265625" />
                  <Point X="-2.967109863281" Y="23.54147265625" />
                  <Point X="-2.960219238281" Y="23.56280078125" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953125" Y="23.5976015625" />
                  <Point X="-2.947838623047" Y="23.6290859375" />
                  <Point X="-2.94708203125" Y="23.64430078125" />
                  <Point X="-2.94778125" Y="23.66716796875" />
                  <Point X="-2.951229003906" Y="23.68978515625" />
                  <Point X="-2.957375" Y="23.711822265625" />
                  <Point X="-2.961113769531" Y="23.722654296875" />
                  <Point X="-2.973119140625" Y="23.751638671875" />
                  <Point X="-2.978693847656" Y="23.762919921875" />
                  <Point X="-2.991286376953" Y="23.7846484375" />
                  <Point X="-3.006642089844" Y="23.8045234375" />
                  <Point X="-3.024487548828" Y="23.82219140625" />
                  <Point X="-3.033995117188" Y="23.830431640625" />
                  <Point X="-3.052245361328" Y="23.844244140625" />
                  <Point X="-3.070537353516" Y="23.856490234375" />
                  <Point X="-3.091272216797" Y="23.868693359375" />
                  <Point X="-3.105436279297" Y="23.87551953125" />
                  <Point X="-3.134700927734" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891142578125" />
                  <Point X="-3.181688476562" Y="23.897623046875" />
                  <Point X="-3.197305175781" Y="23.89946875" />
                  <Point X="-3.228625488281" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.518795410156" Y="23.732017578125" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.740763183594" Y="24.02588671875" />
                  <Point X="-4.755779785156" Y="24.1308828125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.830317871094" Y="24.6015390625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.498185546875" Y="24.691150390625" />
                  <Point X="-3.478417724609" Y="24.698904296875" />
                  <Point X="-3.468752441406" Y="24.703333984375" />
                  <Point X="-3.44716015625" Y="24.714734375" />
                  <Point X="-3.427534667969" Y="24.72666796875" />
                  <Point X="-3.405805175781" Y="24.74175" />
                  <Point X="-3.396476318359" Y="24.749130859375" />
                  <Point X="-3.372522216797" Y="24.77065625" />
                  <Point X="-3.358066162109" Y="24.78701953125" />
                  <Point X="-3.337569824219" Y="24.8164453125" />
                  <Point X="-3.328722412109" Y="24.832134765625" />
                  <Point X="-3.319289550781" Y="24.853341796875" />
                  <Point X="-3.3114296875" Y="24.874240234375" />
                  <Point X="-3.304186523438" Y="24.897578125" />
                  <Point X="-3.301576660156" Y="24.908060546875" />
                  <Point X="-3.296133789062" Y="24.936798828125" />
                  <Point X="-3.294500244141" Y="24.956697265625" />
                  <Point X="-3.295268798828" Y="24.989556640625" />
                  <Point X="-3.297084960938" Y="25.005953125" />
                  <Point X="-3.30133984375" Y="25.027244140625" />
                  <Point X="-3.306194335938" Y="25.046330078125" />
                  <Point X="-3.3134375" Y="25.06966796875" />
                  <Point X="-3.317670654297" Y="25.080791015625" />
                  <Point X="-3.330989257812" Y="25.1101171875" />
                  <Point X="-3.3423203125" Y="25.128927734375" />
                  <Point X="-3.364407714844" Y="25.157505859375" />
                  <Point X="-3.376822509766" Y="25.170736328125" />
                  <Point X="-3.394662109375" Y="25.186431640625" />
                  <Point X="-3.411828613281" Y="25.199869140625" />
                  <Point X="-3.433558105469" Y="25.214951171875" />
                  <Point X="-3.44048046875" Y="25.219326171875" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496564453125" Y="25.24563671875" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.670024414062" Y="25.5608984375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.701099121094" Y="26.06909375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.008905029297" Y="26.23599609375" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736703857422" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704888183594" Y="26.2080546875" />
                  <Point X="-3.684604248047" Y="26.21208984375" />
                  <Point X="-3.661242919922" Y="26.21886328125" />
                  <Point X="-3.6131484375" Y="26.23402734375" />
                  <Point X="-3.603455810547" Y="26.23767578125" />
                  <Point X="-3.584526855469" Y="26.24600390625" />
                  <Point X="-3.575290527344" Y="26.25068359375" />
                  <Point X="-3.556546142578" Y="26.26150390625" />
                  <Point X="-3.547870605469" Y="26.2671640625" />
                  <Point X="-3.531185546875" Y="26.279396484375" />
                  <Point X="-3.515930419922" Y="26.293373046875" />
                  <Point X="-3.502289306641" Y="26.308927734375" />
                  <Point X="-3.495893798828" Y="26.317078125" />
                  <Point X="-3.483479248047" Y="26.33480859375" />
                  <Point X="-3.478010009766" Y="26.343603515625" />
                  <Point X="-3.468062255859" Y="26.361736328125" />
                  <Point X="-3.458234375" Y="26.38398828125" />
                  <Point X="-3.438936279297" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429710449219" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436013427734" Y="26.604533203125" />
                  <Point X="-3.443511474609" Y="26.6238125" />
                  <Point X="-3.454240722656" Y="26.64564453125" />
                  <Point X="-3.477525878906" Y="26.690375" />
                  <Point X="-3.482799072266" Y="26.699283203125" />
                  <Point X="-3.494290283203" Y="26.716482421875" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521502197266" Y="26.748912109375" />
                  <Point X="-3.536443603516" Y="26.76321484375" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.210678222656" Y="27.281287109375" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.002293457031" Y="27.68030859375" />
                  <Point X="-3.922213378906" Y="27.7832421875" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.395931884766" Y="27.84425390625" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923095703" Y="27.75771875" />
                  <Point X="-3.225994628906" Y="27.749388671875" />
                  <Point X="-3.216300048828" Y="27.745740234375" />
                  <Point X="-3.195657226562" Y="27.73923046875" />
                  <Point X="-3.185625488281" Y="27.736658203125" />
                  <Point X="-3.165336669922" Y="27.73262109375" />
                  <Point X="-3.136512207031" Y="27.72953125" />
                  <Point X="-3.069530273438" Y="27.723671875" />
                  <Point X="-3.059176269531" Y="27.723333984375" />
                  <Point X="-3.038488525391" Y="27.72378515625" />
                  <Point X="-3.028154785156" Y="27.72457421875" />
                  <Point X="-3.006696777344" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976423095703" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902743896484" Y="27.7731953125" />
                  <Point X="-2.886610839844" Y="27.78614453125" />
                  <Point X="-2.865721191406" Y="27.806236328125" />
                  <Point X="-2.818176757812" Y="27.853779296875" />
                  <Point X="-2.811265869141" Y="27.86148828125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.750421875" Y="28.062966796875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-3.059388427734" Y="28.699916015625" />
                  <Point X="-2.648377441406" Y="29.01503515625" />
                  <Point X="-2.522259277344" Y="29.0851015625" />
                  <Point X="-2.192524902344" Y="29.268294921875" />
                  <Point X="-2.161965332031" Y="29.22846875" />
                  <Point X="-2.118563476562" Y="29.171908203125" />
                  <Point X="-2.111819335938" Y="29.164046875" />
                  <Point X="-2.097512451172" Y="29.1491015625" />
                  <Point X="-2.089949707031" Y="29.142017578125" />
                  <Point X="-2.073370849609" Y="29.128107421875" />
                  <Point X="-2.065084960938" Y="29.121892578125" />
                  <Point X="-2.047890014648" Y="29.110404296875" />
                  <Point X="-2.03898059082" Y="29.105130859375" />
                  <Point X="-2.018315307617" Y="29.094373046875" />
                  <Point X="-1.943764404297" Y="29.0555625" />
                  <Point X="-1.934335083008" Y="29.0512890625" />
                  <Point X="-1.9150625" Y="29.04379296875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.88431237793" Y="29.034966796875" />
                  <Point X="-1.874174316406" Y="29.032833984375" />
                  <Point X="-1.853724731445" Y="29.029685546875" />
                  <Point X="-1.833054199219" Y="29.028783203125" />
                  <Point X="-1.812408081055" Y="29.03013671875" />
                  <Point X="-1.80212109375" Y="29.031376953125" />
                  <Point X="-1.780805297852" Y="29.03513671875" />
                  <Point X="-1.770715087891" Y="29.037490234375" />
                  <Point X="-1.750860229492" Y="29.043279296875" />
                  <Point X="-1.741095458984" Y="29.04671484375" />
                  <Point X="-1.719571411133" Y="29.055630859375" />
                  <Point X="-1.641921508789" Y="29.087794921875" />
                  <Point X="-1.632585205078" Y="29.0922734375" />
                  <Point X="-1.61445324707" Y="29.102220703125" />
                  <Point X="-1.605657226562" Y="29.107689453125" />
                  <Point X="-1.587927612305" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564227050781" Y="29.14013671875" />
                  <Point X="-1.550251831055" Y="29.155388671875" />
                  <Point X="-1.538020385742" Y="29.1720703125" />
                  <Point X="-1.532360229492" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.516856567383" Y="29.2087265625" />
                  <Point X="-1.508526245117" Y="29.227658203125" />
                  <Point X="-1.504877563477" Y="29.2373515625" />
                  <Point X="-1.497871826172" Y="29.2595703125" />
                  <Point X="-1.472598144531" Y="29.339728515625" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.931162658691" Y="29.7163203125" />
                  <Point X="-0.778293212891" Y="29.73421484375" />
                  <Point X="-0.365222686768" Y="29.78255859375" />
                  <Point X="-0.267607116699" Y="29.418251953125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190307617" Y="29.785005859375" />
                  <Point X="0.827866149902" Y="29.737912109375" />
                  <Point X="0.954369934082" Y="29.70737109375" />
                  <Point X="1.453623046875" Y="29.586833984375" />
                  <Point X="1.533799194336" Y="29.55775390625" />
                  <Point X="1.858242919922" Y="29.440076171875" />
                  <Point X="1.937876220703" Y="29.402833984375" />
                  <Point X="2.250443603516" Y="29.25665625" />
                  <Point X="2.327427490234" Y="29.211806640625" />
                  <Point X="2.629438232422" Y="29.03585546875" />
                  <Point X="2.701979736328" Y="28.984267578125" />
                  <Point X="2.817780273438" Y="28.901916015625" />
                  <Point X="2.252312255859" Y="27.922498046875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.060837646484" Y="27.5898359375" />
                  <Point X="2.049284179688" Y="27.56271484375" />
                  <Point X="2.039622924805" Y="27.533205078125" />
                  <Point X="2.036642089844" Y="27.523173828125" />
                  <Point X="2.01983203125" Y="27.4603125" />
                  <Point X="2.017331787109" Y="27.44748046875" />
                  <Point X="2.014116455078" Y="27.42159765625" />
                  <Point X="2.013401245117" Y="27.408546875" />
                  <Point X="2.013880493164" Y="27.374578125" />
                  <Point X="2.015227905273" Y="27.35451171875" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318847656" Y="27.223888671875" />
                  <Point X="2.055681640625" Y="27.20384375" />
                  <Point X="2.070783935547" Y="27.180388671875" />
                  <Point X="2.104418212891" Y="27.1308203125" />
                  <Point X="2.112563232422" Y="27.120447265625" />
                  <Point X="2.1302109375" Y="27.1009296875" />
                  <Point X="2.139713623047" Y="27.09178515625" />
                  <Point X="2.166753662109" Y="27.06916015625" />
                  <Point X="2.181996826172" Y="27.05765625" />
                  <Point X="2.231565185547" Y="27.024021484375" />
                  <Point X="2.24128125" Y="27.0182421875" />
                  <Point X="2.261328369141" Y="27.00787890625" />
                  <Point X="2.271659423828" Y="27.003294921875" />
                  <Point X="2.293748046875" Y="26.995029296875" />
                  <Point X="2.304554199219" Y="26.991703125" />
                  <Point X="2.326478759766" Y="26.986361328125" />
                  <Point X="2.352664794922" Y="26.982529296875" />
                  <Point X="2.407021972656" Y="26.975974609375" />
                  <Point X="2.42046484375" Y="26.975314453125" />
                  <Point X="2.447310302734" Y="26.975900390625" />
                  <Point X="2.460712890625" Y="26.977146484375" />
                  <Point X="2.497041015625" Y="26.98314453125" />
                  <Point X="2.515175292969" Y="26.9870546875" />
                  <Point X="2.578036621094" Y="27.003865234375" />
                  <Point X="2.584006103516" Y="27.005673828125" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627658691406" Y="27.023576171875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.804517089844" Y="27.702498046875" />
                  <Point X="3.940403564453" Y="27.780951171875" />
                  <Point X="4.043958740234" Y="27.637033203125" />
                  <Point X="4.084396484375" Y="27.570208984375" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.419159667969" Y="26.9327421875" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.1521171875" Y="26.72521484375" />
                  <Point X="3.134669189453" Y="26.706603515625" />
                  <Point X="3.128579589844" Y="26.69942578125" />
                  <Point X="3.116038818359" Y="26.68306640625" />
                  <Point X="3.070797363281" Y="26.624044921875" />
                  <Point X="3.063427734375" Y="26.612884765625" />
                  <Point X="3.050330322266" Y="26.589640625" />
                  <Point X="3.044602539062" Y="26.577556640625" />
                  <Point X="3.031461669922" Y="26.54390234375" />
                  <Point X="3.025468505859" Y="26.52596875" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.008533691406" Y="26.333984375" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.026025390625" Y="26.253951171875" />
                  <Point X="3.035142089844" Y="26.228630859375" />
                  <Point X="3.040601318359" Y="26.216296875" />
                  <Point X="3.058229736328" Y="26.183064453125" />
                  <Point X="3.067349365234" Y="26.167669921875" />
                  <Point X="3.104976806641" Y="26.110478515625" />
                  <Point X="3.111741210938" Y="26.101419921875" />
                  <Point X="3.126296630859" Y="26.084173828125" />
                  <Point X="3.134087646484" Y="26.075986328125" />
                  <Point X="3.151330566406" Y="26.0598984375" />
                  <Point X="3.160034423828" Y="26.0526953125" />
                  <Point X="3.178243896484" Y="26.039369140625" />
                  <Point X="3.202864257812" Y="26.02473828125" />
                  <Point X="3.257391601562" Y="25.99404296875" />
                  <Point X="3.265482910156" Y="25.989984375" />
                  <Point X="3.294679443359" Y="25.97808203125" />
                  <Point X="3.330275390625" Y="25.96801953125" />
                  <Point X="3.343669433594" Y="25.965255859375" />
                  <Point X="3.36410546875" Y="25.9625546875" />
                  <Point X="3.437830078125" Y="25.9528125" />
                  <Point X="3.444032714844" Y="25.95219921875" />
                  <Point X="3.465708984375" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.610584472656" Y="26.098927734375" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.765428710938" Y="25.83237109375" />
                  <Point X="4.783870117188" Y="25.713923828125" />
                  <Point X="3.974151367188" Y="25.496958984375" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686026123047" Y="25.41954296875" />
                  <Point X="3.665616210938" Y="25.4121328125" />
                  <Point X="3.642376708984" Y="25.401615234375" />
                  <Point X="3.634004882812" Y="25.397314453125" />
                  <Point X="3.613927001953" Y="25.385708984375" />
                  <Point X="3.541494873047" Y="25.34384375" />
                  <Point X="3.530207763672" Y="25.336189453125" />
                  <Point X="3.508845214844" Y="25.319341796875" />
                  <Point X="3.498769775391" Y="25.3101484375" />
                  <Point X="3.472410400391" Y="25.282376953125" />
                  <Point X="3.460750732422" Y="25.268876953125" />
                  <Point X="3.417291503906" Y="25.2135" />
                  <Point X="3.410854980469" Y="25.204208984375" />
                  <Point X="3.399130126953" Y="25.184927734375" />
                  <Point X="3.393841796875" Y="25.1749375" />
                  <Point X="3.384067871094" Y="25.15347265625" />
                  <Point X="3.380004394531" Y="25.14292578125" />
                  <Point X="3.373158691406" Y="25.12142578125" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.366360839844" Y="25.08950390625" />
                  <Point X="3.351874267578" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584472656" Y="24.973736328125" />
                  <Point X="3.350280273438" Y="24.93705859375" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.355890380859" Y="24.902609375" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158691406" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067871094" Y="24.783966796875" />
                  <Point X="3.393841796875" Y="24.762501953125" />
                  <Point X="3.399129882813" Y="24.75251171875" />
                  <Point X="3.410852294922" Y="24.733234375" />
                  <Point X="3.429333251953" Y="24.708595703125" />
                  <Point X="3.472792480469" Y="24.65321875" />
                  <Point X="3.478713134766" Y="24.64637109375" />
                  <Point X="3.501141357422" Y="24.62419140625" />
                  <Point X="3.530177001953" Y="24.6012734375" />
                  <Point X="3.541493896484" Y="24.593595703125" />
                  <Point X="3.561571777344" Y="24.581990234375" />
                  <Point X="3.63400390625" Y="24.540123046875" />
                  <Point X="3.639486816406" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.68302734375" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.70989453125" Y="24.243337890625" />
                  <Point X="4.784876464844" Y="24.22324609375" />
                  <Point X="4.761612304688" Y="24.068939453125" />
                  <Point X="4.745284179688" Y="23.997388671875" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.7640078125" Y="24.047666015625" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366721191406" Y="24.089158203125" />
                  <Point X="3.354483642578" Y="24.087322265625" />
                  <Point X="3.315077880859" Y="24.0787578125" />
                  <Point X="3.172919433594" Y="24.047859375" />
                  <Point X="3.157875732422" Y="24.0432578125" />
                  <Point X="3.128753662109" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123779297" Y="23.99846875" />
                  <Point X="3.050373779297" Y="23.977998046875" />
                  <Point X="3.039348632812" Y="23.966775390625" />
                  <Point X="3.015530273438" Y="23.93812890625" />
                  <Point X="2.929604492188" Y="23.834787109375" />
                  <Point X="2.921325927734" Y="23.82315234375" />
                  <Point X="2.906605957031" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.871743408203" Y="23.6662421875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158935547" Y="23.394517578125" />
                  <Point X="2.896541259766" Y="23.380626953125" />
                  <Point X="2.918349121094" Y="23.34670703125" />
                  <Point X="2.997021484375" Y="23.224337890625" />
                  <Point X="3.001740966797" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.997416015625" Y="22.438888671875" />
                  <Point X="4.087170898438" Y="22.370015625" />
                  <Point X="4.0455" Y="22.302587890625" />
                  <Point X="4.011718017578" Y="22.254587890625" />
                  <Point X="4.001273193359" Y="22.239748046875" />
                  <Point X="3.139893310547" Y="22.737064453125" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815022216797" Y="22.92048828125" />
                  <Point X="2.783119140625" Y="22.930673828125" />
                  <Point X="2.771111083984" Y="22.933662109375" />
                  <Point X="2.724211914062" Y="22.9421328125" />
                  <Point X="2.555020751953" Y="22.9726875" />
                  <Point X="2.539360595703" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317626953" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444844970703" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400593505859" Y="22.948890625" />
                  <Point X="2.361631591797" Y="22.92838671875" />
                  <Point X="2.221075439453" Y="22.854412109375" />
                  <Point X="2.208974121094" Y="22.84683203125" />
                  <Point X="2.186041748047" Y="22.829939453125" />
                  <Point X="2.175210693359" Y="22.820626953125" />
                  <Point X="2.154251220703" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.788841796875" />
                  <Point X="2.128048339844" Y="22.765912109375" />
                  <Point X="2.120464599609" Y="22.75380859375" />
                  <Point X="2.099959228516" Y="22.71484765625" />
                  <Point X="2.025985595703" Y="22.574291015625" />
                  <Point X="2.019836303711" Y="22.55980859375" />
                  <Point X="2.010012207031" Y="22.53003125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.010657592773" Y="22.372958984375" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.676560058594" Y="21.07504296875" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723752441406" Y="20.963916015625" />
                  <Point X="2.056309326172" Y="21.83374609375" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808835327148" Y="22.15037109375" />
                  <Point X="1.783252929688" Y="22.17199609375" />
                  <Point X="1.773299560547" Y="22.179353515625" />
                  <Point X="1.727044189453" Y="22.209091796875" />
                  <Point X="1.560176025391" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470092773" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455385131836" Y="22.3513046875" />
                  <Point X="1.424120727539" Y="22.35362109375" />
                  <Point X="1.408397705078" Y="22.35348046875" />
                  <Point X="1.357809570312" Y="22.348826171875" />
                  <Point X="1.175310913086" Y="22.332033203125" />
                  <Point X="1.161228027344" Y="22.32966015625" />
                  <Point X="1.133575805664" Y="22.322828125" />
                  <Point X="1.120006713867" Y="22.318369140625" />
                  <Point X="1.09262121582" Y="22.307025390625" />
                  <Point X="1.079876220703" Y="22.3005859375" />
                  <Point X="1.055493896484" Y="22.285865234375" />
                  <Point X="1.043856567383" Y="22.277583984375" />
                  <Point X="1.004793640137" Y="22.245103515625" />
                  <Point X="0.86387298584" Y="22.127931640625" />
                  <Point X="0.852652709961" Y="22.11691015625" />
                  <Point X="0.832183166504" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.771112365723" Y="21.9406328125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091369629" Y="20.84766015625" />
                  <Point X="0.742005187988" Y="21.18759765625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146179199" Y="21.54641796875" />
                  <Point X="0.62678704834" Y="21.576185546875" />
                  <Point X="0.62040625" Y="21.58679296875" />
                  <Point X="0.584871520996" Y="21.637990234375" />
                  <Point X="0.456678649902" Y="21.822693359375" />
                  <Point X="0.446669189453" Y="21.834830078125" />
                  <Point X="0.424782989502" Y="21.857287109375" />
                  <Point X="0.41290637207" Y="21.867607421875" />
                  <Point X="0.386651397705" Y="21.886849609375" />
                  <Point X="0.373238189697" Y="21.895064453125" />
                  <Point X="0.345237335205" Y="21.909171875" />
                  <Point X="0.330649688721" Y="21.9150625" />
                  <Point X="0.275661743164" Y="21.932126953125" />
                  <Point X="0.077290603638" Y="21.9936953125" />
                  <Point X="0.063372959137" Y="21.996892578125" />
                  <Point X="0.035222297668" Y="22.00116015625" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008652074814" Y="22.002234375" />
                  <Point X="-0.022896093369" Y="22.001162109375" />
                  <Point X="-0.051062213898" Y="21.996892578125" />
                  <Point X="-0.064984169006" Y="21.9936953125" />
                  <Point X="-0.119971961975" Y="21.97662890625" />
                  <Point X="-0.318343261719" Y="21.9150625" />
                  <Point X="-0.332927764893" Y="21.909171875" />
                  <Point X="-0.360928771973" Y="21.895064453125" />
                  <Point X="-0.374345397949" Y="21.88684765625" />
                  <Point X="-0.400600830078" Y="21.867603515625" />
                  <Point X="-0.412478179932" Y="21.85728125" />
                  <Point X="-0.434361877441" Y="21.83482421875" />
                  <Point X="-0.444368347168" Y="21.822689453125" />
                  <Point X="-0.479903045654" Y="21.771490234375" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.956393859863" Y="20.3415390625" />
                  <Point X="-0.985425109863" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.757207492114" Y="24.353177935754" />
                  <Point X="-4.565422955278" Y="23.725878981257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665388655456" Y="24.377780897328" />
                  <Point X="-4.469925982651" Y="23.738451302167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.122412149126" Y="22.601784769822" />
                  <Point X="-4.032169884409" Y="22.306615621977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.573569818797" Y="24.402383858901" />
                  <Point X="-4.374429004271" Y="23.75102360426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.041947930257" Y="22.663527012725" />
                  <Point X="-3.860917507762" Y="22.071403181281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.481750982139" Y="24.426986820475" />
                  <Point X="-4.278932025891" Y="23.763595906352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.961483711388" Y="22.725269255627" />
                  <Point X="-3.743147064415" Y="22.011122262166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770146809533" Y="25.695215911551" />
                  <Point X="-4.734351656627" Y="25.578135241941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.38993214548" Y="24.451589782049" />
                  <Point X="-4.183435047511" Y="23.776168208445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881019492519" Y="22.787011498529" />
                  <Point X="-3.658710452332" Y="22.059871392322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.737747227856" Y="25.91417049887" />
                  <Point X="-4.62614665354" Y="25.549141468143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.298113308822" Y="24.476192743622" />
                  <Point X="-4.087938069131" Y="23.788740510538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80055527365" Y="22.848753741432" />
                  <Point X="-3.574273956968" Y="22.10862090425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.693896983646" Y="26.095671656658" />
                  <Point X="-4.517941794333" Y="25.520148164957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206294472164" Y="24.500795705196" />
                  <Point X="-3.992441090751" Y="23.801312812631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.720091054781" Y="22.910495984334" />
                  <Point X="-3.489837461605" Y="22.157370416178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.647219403568" Y="26.267925015518" />
                  <Point X="-4.409736935126" Y="25.491154861771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.114475635505" Y="24.52539866677" />
                  <Point X="-3.896944112371" Y="23.813885114724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.639626835912" Y="22.972238227236" />
                  <Point X="-3.405400966241" Y="22.206119928106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.560311091145" Y="26.308589578146" />
                  <Point X="-4.301532075919" Y="25.462161558585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.022656798847" Y="24.550001628343" />
                  <Point X="-3.801447133991" Y="23.826457416817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.559162617043" Y="23.033980470139" />
                  <Point X="-3.320964470877" Y="22.254869440034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.456804230366" Y="26.29496273542" />
                  <Point X="-4.193327216711" Y="25.433168255398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.930837962188" Y="24.574604589917" />
                  <Point X="-3.705950155611" Y="23.83902971891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.478698398174" Y="23.095722713041" />
                  <Point X="-3.236527975513" Y="22.303618951962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921112514021" Y="21.271941463829" />
                  <Point X="-2.861335636673" Y="21.076420108032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.353297369587" Y="26.281335892695" />
                  <Point X="-4.085122357504" Y="25.404174952212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.83901912553" Y="24.599207551491" />
                  <Point X="-3.610453177231" Y="23.851602021003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.398234179305" Y="23.157464955943" />
                  <Point X="-3.15209148015" Y="22.35236846389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.856164502185" Y="21.384434933135" />
                  <Point X="-2.732330319711" Y="20.979391573133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.249790508808" Y="26.267709049969" />
                  <Point X="-3.976917498297" Y="25.375181649026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.747200205919" Y="24.62381024174" />
                  <Point X="-3.514956198852" Y="23.864174323096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317769966543" Y="23.21920721882" />
                  <Point X="-3.067654984786" Y="22.401117975818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.79121649035" Y="21.496928402442" />
                  <Point X="-2.609793335816" Y="20.903520002381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146283648029" Y="26.254082207243" />
                  <Point X="-3.86871263909" Y="25.34618834584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.655381277625" Y="24.648412903586" />
                  <Point X="-3.419459220472" Y="23.876746625189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237305785342" Y="23.280949584928" />
                  <Point X="-2.983218489422" Y="22.449867487746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.726268478514" Y="21.609421871749" />
                  <Point X="-2.493957949257" Y="20.849568368826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.04277678725" Y="26.240455364517" />
                  <Point X="-3.760507779883" Y="25.317195042654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56356234933" Y="24.673015565432" />
                  <Point X="-3.323962242092" Y="23.889318927282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.156841604141" Y="23.342691951037" />
                  <Point X="-2.898781994058" Y="22.498616999674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.661320466679" Y="21.721915341055" />
                  <Point X="-2.422920813441" Y="20.942145211018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.9392698956" Y="26.226828420815" />
                  <Point X="-3.652302920676" Y="25.288201739467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.472908552541" Y="24.701429200712" />
                  <Point X="-3.228051732256" Y="23.900538628929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.07637742294" Y="23.404434317145" />
                  <Point X="-2.814345498695" Y="22.547366511602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.596372454843" Y="21.834408810362" />
                  <Point X="-2.34632789192" Y="21.016549896987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.187364895701" Y="27.363239445414" />
                  <Point X="-4.147484587496" Y="27.232796834897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.835762988933" Y="26.213201427997" />
                  <Point X="-3.544098061469" Y="25.259208436281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.389945833342" Y="24.754999217268" />
                  <Point X="-3.123137654385" Y="23.882308986493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.999631937427" Y="23.478339988782" />
                  <Point X="-2.729909003331" Y="22.59611602353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.531424443008" Y="21.946902279668" />
                  <Point X="-2.263838304693" Y="21.071667458492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.122171529552" Y="27.474930396922" />
                  <Point X="-4.017696008375" Y="27.133206364911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.733899114398" Y="26.20494855113" />
                  <Point X="-3.430598508974" Y="25.212896971693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320108534867" Y="24.851500550567" />
                  <Point X="-2.995676207964" Y="23.790330224595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947634410825" Y="23.633192586628" />
                  <Point X="-2.643570845607" Y="22.638645478149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466476418514" Y="22.059395707572" />
                  <Point X="-2.181348717466" Y="21.126785019997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056978163403" Y="27.58662134843" />
                  <Point X="-3.887907429253" Y="27.033615894925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640784653924" Y="26.225313718153" />
                  <Point X="-2.548185654996" Y="22.651583421559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.401528384204" Y="22.171889103369" />
                  <Point X="-2.098859239244" Y="21.181902938042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990798006693" Y="27.695084653449" />
                  <Point X="-3.758118850131" Y="26.934025424939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553179851862" Y="26.263700165824" />
                  <Point X="-2.436992055778" Y="22.61281439028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.33692893291" Y="22.285522662834" />
                  <Point X="-2.010376548435" Y="21.217417941002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.919482799555" Y="27.786751965328" />
                  <Point X="-3.628330271009" Y="26.834434954953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.478182994811" Y="26.343325343441" />
                  <Point X="-1.91240394237" Y="21.221892829799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84816724388" Y="27.878418137193" />
                  <Point X="-3.491778434731" Y="26.712722867588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423812226291" Y="26.490415416746" />
                  <Point X="-1.81103197504" Y="21.215248908902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776851688205" Y="27.970084309059" />
                  <Point X="-1.709660007711" Y="21.208604988005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.691151030069" Y="28.014698930871" />
                  <Point X="-1.607142928022" Y="21.198215573349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.570516676772" Y="27.945050584397" />
                  <Point X="-1.490440930573" Y="21.141429383294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449882323476" Y="27.875402237924" />
                  <Point X="-1.354707184615" Y="21.022393148796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136136791329" Y="20.307481605592" />
                  <Point X="-1.122822725165" Y="20.263933257419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.329248030394" Y="27.805754088401" />
                  <Point X="-1.20872342271" Y="20.869830622797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.173562799047" Y="20.754825404822" />
                  <Point X="-1.015891812719" Y="20.239106846332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.211053391398" Y="27.744085687834" />
                  <Point X="-0.952411572926" Y="20.356401181667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.106459186748" Y="27.726902303562" />
                  <Point X="-0.906012448457" Y="20.529565327788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.007248532894" Y="27.727327720504" />
                  <Point X="-0.859613323989" Y="20.702729473908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918631298528" Y="27.762402651319" />
                  <Point X="-0.81321419952" Y="20.875893620029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.840425356202" Y="27.831531383965" />
                  <Point X="-0.766815075052" Y="21.04905776615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.016604970802" Y="28.732717781588" />
                  <Point X="-2.947132699094" Y="28.50548421976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.769719570657" Y="27.925192024058" />
                  <Point X="-0.720415950583" Y="21.22222191227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936128142736" Y="28.794418781667" />
                  <Point X="-0.674016826114" Y="21.395386058391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.855651314671" Y="28.856119781747" />
                  <Point X="-0.62433091927" Y="21.557799623773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775174486606" Y="28.917820781826" />
                  <Point X="-0.556556309963" Y="21.661047709337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.694697658541" Y="28.979521781906" />
                  <Point X="-0.487593587386" Y="21.760409651501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612336161254" Y="29.035058306727" />
                  <Point X="-0.416581966161" Y="21.85306994816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.52741883992" Y="29.082235107773" />
                  <Point X="-0.334197715633" Y="21.908532050482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.442501873197" Y="29.129413068696" />
                  <Point X="-0.243915719414" Y="21.938161790632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.357584929413" Y="29.176591104651" />
                  <Point X="-0.153184191028" Y="21.966321177315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.272667985629" Y="29.223769140607" />
                  <Point X="-0.062394421127" Y="21.994290064588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.183241183184" Y="29.25619609355" />
                  <Point X="0.034837023958" Y="22.001189181717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.037475673464" Y="29.104347438282" />
                  <Point X="0.142672943522" Y="21.973402625729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.920236908571" Y="29.045805561029" />
                  <Point X="0.252428289287" Y="21.939337909523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.816033089636" Y="29.029899070892" />
                  <Point X="0.363860241225" Y="21.899789261629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.723998681163" Y="29.053796928832" />
                  <Point X="0.510464317108" Y="21.745197780032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.635930768474" Y="29.090668609893" />
                  <Point X="0.794431351135" Y="21.141312307105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554844548256" Y="29.150376378055" />
                  <Point X="0.724733000221" Y="21.694214184581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.493310597882" Y="29.274036739233" />
                  <Point X="0.761814413834" Y="21.897855189652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477040162783" Y="29.54574738787" />
                  <Point X="0.811229253841" Y="22.061155374706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.390478761746" Y="29.587546646512" />
                  <Point X="0.884827603814" Y="22.145354862865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.298980753047" Y="29.613198989054" />
                  <Point X="0.964033592448" Y="22.211212591426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207482744347" Y="29.638851331597" />
                  <Point X="1.043239423638" Y="22.277070834964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.115984735647" Y="29.66450367414" />
                  <Point X="1.129045845358" Y="22.321339519681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.024486726947" Y="29.690156016682" />
                  <Point X="1.223754344948" Y="22.336490819689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932988718248" Y="29.715808359225" />
                  <Point X="1.320376838872" Y="22.345381726317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.837168397364" Y="29.727323055644" />
                  <Point X="1.417217404915" Y="22.353559351185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.741259811865" Y="29.738549051513" />
                  <Point X="1.522555342677" Y="22.333943325514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645350835252" Y="29.749773768104" />
                  <Point X="1.643681809527" Y="22.262685348135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.549441858638" Y="29.760998484696" />
                  <Point X="1.767325337632" Y="22.183194434357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.453532882025" Y="29.772223201288" />
                  <Point X="1.918657858485" Y="22.013136906147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.167236637012" Y="29.160719222511" />
                  <Point X="2.083795246284" Y="21.797925692741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.046858277216" Y="29.091908193048" />
                  <Point X="2.015459233063" Y="22.346371564406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.097516238626" Y="22.077975192896" />
                  <Point X="2.248932804816" Y="21.58271392089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.052957988555" Y="29.090352742667" />
                  <Point X="2.03808768294" Y="22.597286083758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.308673025303" Y="21.712241308166" />
                  <Point X="2.414070363348" Y="21.367502149039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.141225581877" Y="29.126571297805" />
                  <Point X="2.100925129191" Y="22.716682902033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51982981198" Y="21.346507423436" />
                  <Point X="2.579207921881" Y="21.152290377187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.215045121319" Y="29.21004730781" />
                  <Point X="2.170061262739" Y="22.815477642468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730986290494" Y="20.980774546665" />
                  <Point X="2.73398620013" Y="20.970962284376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266286310366" Y="29.367373774327" />
                  <Point X="2.252450144754" Y="22.870924595881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.312685431051" Y="29.540537932822" />
                  <Point X="2.338021870075" Y="22.915960937931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.359084551737" Y="29.713702091317" />
                  <Point X="2.424467211065" Y="22.95813981188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.438558423347" Y="29.778683614142" />
                  <Point X="2.518819478102" Y="22.974456296067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.541185105962" Y="29.767935704471" />
                  <Point X="2.622422398924" Y="22.9605152551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.643811788576" Y="29.757187794799" />
                  <Point X="2.727568544303" Y="22.941526554046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746438471191" Y="29.746439885128" />
                  <Point X="2.836148934308" Y="22.911304944965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.850021706613" Y="29.732563232201" />
                  <Point X="2.956365186023" Y="22.843024147145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.957279176058" Y="29.706668701298" />
                  <Point X="2.864088931544" Y="23.469775019616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.882001914639" Y="23.411184291955" />
                  <Point X="3.076999435662" Y="22.773376139717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.064537029099" Y="29.680772915709" />
                  <Point X="2.882083592699" Y="23.735845978942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.058288270631" Y="23.159506446739" />
                  <Point X="3.197633685101" Y="22.703728132947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.17179488214" Y="29.65487713012" />
                  <Point X="2.945375864569" Y="23.853755129652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.188076757707" Y="23.059916277821" />
                  <Point X="3.31826793432" Y="22.634080126893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.279052735182" Y="29.628981344532" />
                  <Point X="3.018009359154" Y="23.941110517582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.317865244783" Y="22.960326108902" />
                  <Point X="3.438902183539" Y="22.56443212084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.386310588223" Y="29.603085558943" />
                  <Point X="2.029905379008" Y="27.497981852262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.159034737648" Y="27.075618751429" />
                  <Point X="3.095399472063" Y="24.012907708011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.44765373186" Y="22.860735939984" />
                  <Point X="3.559536432758" Y="22.494784114787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.495234131645" Y="29.57174154561" />
                  <Point X="2.086998854306" Y="27.636166352968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281627464749" Y="26.999564852868" />
                  <Point X="3.183360566109" Y="24.050128777112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.577442218936" Y="22.761145771066" />
                  <Point X="3.680170681978" Y="22.425136108733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.606964644981" Y="29.531216347384" />
                  <Point X="2.151946870759" Y="27.748659807171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.387459229291" Y="26.978333592581" />
                  <Point X="3.276511304041" Y="24.070375285916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707230706012" Y="22.661555602147" />
                  <Point X="3.800804931197" Y="22.35548810268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.71869520853" Y="29.490690984919" />
                  <Point X="2.216894887212" Y="27.861153261375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.485891885271" Y="26.98130372591" />
                  <Point X="3.370024808631" Y="24.089435238448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.837019193089" Y="22.561965433229" />
                  <Point X="3.921439180416" Y="22.285840096626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.830425772079" Y="29.450165622454" />
                  <Point X="2.281842935197" Y="27.973646612441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578309573665" Y="27.003947931715" />
                  <Point X="3.470314444774" Y="24.086331463348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.966807680165" Y="22.462375264311" />
                  <Point X="4.024696662979" Y="22.273028933292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.945298457509" Y="29.399362842407" />
                  <Point X="2.346791021" Y="28.086139839811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665188822655" Y="27.04470755655" />
                  <Point X="3.573821333518" Y="24.072704529152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061212632839" Y="29.345153502395" />
                  <Point X="2.411739106803" Y="28.19863306718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.749625357537" Y="27.093456939218" />
                  <Point X="3.364794991942" Y="25.081327729714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.505641555273" Y="24.620639379237" />
                  <Point X="3.677328222262" Y="24.059077594957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177126808169" Y="29.290944162382" />
                  <Point X="2.476687192607" Y="28.31112629455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.83406189242" Y="27.142206321886" />
                  <Point X="3.024033466498" Y="26.520837301375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169212312906" Y="26.045978691453" />
                  <Point X="3.421922493563" Y="25.219400935334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628652978337" Y="24.543215987889" />
                  <Point X="3.780835104773" Y="24.045450681149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294861910516" Y="29.230778838467" />
                  <Point X="2.54163527841" Y="28.423619521919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.918498427302" Y="27.190955704554" />
                  <Point X="3.085826167407" Y="26.643651327689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288546776686" Y="25.980582092005" />
                  <Point X="3.494798196497" Y="25.305964095451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740241388423" Y="24.503155588453" />
                  <Point X="3.884341955175" Y="24.031823872365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.415731597007" Y="29.160360751795" />
                  <Point X="2.606583364213" Y="28.536112749288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002934962185" Y="27.239705087222" />
                  <Point X="3.158457360006" Y="26.731014245077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.394632313466" Y="25.958520780131" />
                  <Point X="3.576391377065" Y="25.364013671023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.84844627849" Y="24.474162184328" />
                  <Point X="3.987848805577" Y="24.018197063581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536601461312" Y="29.089942083523" />
                  <Point X="2.671531450016" Y="28.648605976658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.087371497067" Y="27.28845446989" />
                  <Point X="3.238504490639" Y="26.794120722127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495841711032" Y="25.95240860097" />
                  <Point X="3.661579181755" Y="25.410305760875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956651168557" Y="24.445168780203" />
                  <Point X="4.091355655979" Y="24.004570254797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658879176962" Y="29.014918540991" />
                  <Point X="2.736479535819" Y="28.761099204027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.17180803195" Y="27.337203852558" />
                  <Point X="3.318968692936" Y="26.855863019233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.591408811253" Y="25.964751544854" />
                  <Point X="3.752578875491" Y="25.437588018222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.064856058624" Y="24.416175376078" />
                  <Point X="4.194862506381" Y="23.990943446013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785819255562" Y="28.924645096395" />
                  <Point X="2.801427621622" Y="28.873592431396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.256244566832" Y="27.385953235226" />
                  <Point X="3.399432895233" Y="26.91760531634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686905779146" Y="25.977323881251" />
                  <Point X="3.844397669369" Y="25.462191119725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.173060948691" Y="24.387181971953" />
                  <Point X="4.298369356783" Y="23.977316637228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.340681101715" Y="27.434702617894" />
                  <Point X="3.479897106745" Y="26.979347583306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782402747038" Y="25.989896217648" />
                  <Point X="3.936216463246" Y="25.486794221228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.281265838758" Y="24.358188567828" />
                  <Point X="4.401876207185" Y="23.963689828444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425117636597" Y="27.483452000562" />
                  <Point X="3.560361321249" Y="27.041089840484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87789971493" Y="26.002468554045" />
                  <Point X="4.028035286273" Y="25.511397227389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389470728826" Y="24.329195163703" />
                  <Point X="4.505383057587" Y="23.95006301966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.50955417148" Y="27.53220138323" />
                  <Point X="3.640825535754" Y="27.102832097662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973396682822" Y="26.015040890441" />
                  <Point X="4.119854129822" Y="25.536000166426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.497675618893" Y="24.300201759579" />
                  <Point X="4.608889907989" Y="23.936436210876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593990706362" Y="27.580950765898" />
                  <Point X="3.721289750258" Y="27.16457435484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068893650714" Y="26.027613226838" />
                  <Point X="4.21167297337" Y="25.560603105464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.60588050896" Y="24.271208355454" />
                  <Point X="4.712396758391" Y="23.922809402092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.678427241245" Y="27.629700148566" />
                  <Point X="3.801753964763" Y="27.226316612018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.164390618606" Y="26.040185563235" />
                  <Point X="4.303491816919" Y="25.585206044501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.714085407485" Y="24.242214923665" />
                  <Point X="4.763411841267" Y="24.080875428567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762863776127" Y="27.678449531234" />
                  <Point X="3.882218179267" Y="27.288058869196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.259887586499" Y="26.052757899632" />
                  <Point X="4.395310660467" Y="25.609808983539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.847300380508" Y="27.727198686582" />
                  <Point X="3.962682393772" Y="27.349801126374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.355384554391" Y="26.065330236028" />
                  <Point X="4.487129504016" Y="25.634411922576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.931737052553" Y="27.775947620614" />
                  <Point X="4.043146608276" Y="27.411543383552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.450881522283" Y="26.077902572425" />
                  <Point X="4.578948347564" Y="25.659014861614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.103762962627" Y="27.538205066084" />
                  <Point X="4.123610822781" Y="27.47328564073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.546378490175" Y="26.090474908822" />
                  <Point X="4.670767191112" Y="25.683617800651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.641875304592" Y="26.103047747213" />
                  <Point X="4.762586034661" Y="25.708220739689" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.558479431152" Y="21.138421875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.42878414917" Y="21.529654296875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.219348144531" Y="21.7506640625" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.063652515411" Y="21.79516796875" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.323813842773" Y="21.663158203125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.772867980957" Y="20.29236328125" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.867037536621" Y="20.01666796875" />
                  <Point X="-1.10023059082" Y="20.061931640625" />
                  <Point X="-1.162963745117" Y="20.078072265625" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.319615844727" Y="20.369466796875" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.322819824219" Y="20.531283203125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.436908447266" Y="20.841767578125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.716432617188" Y="21.018640625" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.045866333008" Y="20.988798828125" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734375" Y="20.842705078125" />
                  <Point X="-2.435819580078" Y="20.6132265625" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.513996337891" Y="20.620732421875" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-2.942680908203" Y="20.8992578125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.667602539062" Y="22.09103515625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763427734" Y="22.402412109375" />
                  <Point X="-2.513987548828" Y="22.431244140625" />
                  <Point X="-2.531327392578" Y="22.44858203125" />
                  <Point X="-2.560156738281" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.601237548828" Y="21.873662109375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.891633544922" Y="21.798052734375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.223965332031" Y="22.2572734375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.446479736328" Y="23.359935546875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.144149902344" Y="23.61044140625" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651855469" Y="23.649947265625" />
                  <Point X="-3.148657226562" Y="23.678931640625" />
                  <Point X="-3.166907470703" Y="23.692744140625" />
                  <Point X="-3.187642333984" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.493995605469" Y="23.543642578125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.821767089844" Y="23.575287109375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.943865722656" Y="24.103982421875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.879493652344" Y="24.78506640625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.535871582031" Y="24.88275390625" />
                  <Point X="-3.514142089844" Y="24.8978359375" />
                  <Point X="-3.502323974609" Y="24.909353515625" />
                  <Point X="-3.492891113281" Y="24.930560546875" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.487655761719" Y="24.990009765625" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.502325927734" Y="25.028087890625" />
                  <Point X="-3.520165527344" Y="25.043783203125" />
                  <Point X="-3.541895019531" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.719200195312" Y="25.37737109375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.985719726562" Y="25.536375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.884484863281" Y="26.1187890625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.984105224609" Y="26.42437109375" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731704833984" Y="26.3958671875" />
                  <Point X="-3.718373291016" Y="26.4000703125" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.633770507812" Y="26.45669921875" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.622770507812" Y="26.55791015625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.326343261719" Y="27.130548828125" />
                  <Point X="-4.476105957031" Y="27.24546484375" />
                  <Point X="-4.424528808594" Y="27.333830078125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.072175292969" Y="27.89991015625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.300931884766" Y="28.008796875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514160156" Y="27.92043359375" />
                  <Point X="-3.119946777344" Y="27.91880859375" />
                  <Point X="-3.05296484375" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-3.000073730469" Y="27.940583984375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939698730469" Y="28.046408203125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.247358398438" Y="28.645490234375" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.213576171875" Y="28.8211171875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.614532470703" Y="29.25119140625" />
                  <Point X="-2.141549316406" Y="29.513970703125" />
                  <Point X="-2.011228027344" Y="29.3441328125" />
                  <Point X="-1.967826416016" Y="29.287572265625" />
                  <Point X="-1.951247436523" Y="29.273662109375" />
                  <Point X="-1.93058215332" Y="29.262904296875" />
                  <Point X="-1.85603137207" Y="29.22409375" />
                  <Point X="-1.835124389648" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.792284545898" Y="29.231166015625" />
                  <Point X="-1.714634765625" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.679077514648" Y="29.31670703125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.685488769531" Y="29.673423828125" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.564498657227" Y="29.736083984375" />
                  <Point X="-0.968082824707" Y="29.903296875" />
                  <Point X="-0.800381896973" Y="29.92292578125" />
                  <Point X="-0.224200027466" Y="29.990359375" />
                  <Point X="-0.084081283569" Y="29.467427734375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282108307" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594028473" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.205755950928" Y="29.875576171875" />
                  <Point X="0.236648422241" Y="29.990869140625" />
                  <Point X="0.339457489014" Y="29.9801015625" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="0.998960144043" Y="29.89206640625" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.598582885742" Y="29.7363671875" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="2.018366210938" Y="29.574943359375" />
                  <Point X="2.338685058594" Y="29.425140625" />
                  <Point X="2.423071289062" Y="29.375978515625" />
                  <Point X="2.732533691406" Y="29.195685546875" />
                  <Point X="2.812092773438" Y="29.139107421875" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.416857177734" Y="27.827498046875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.220192382812" Y="27.474087890625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.203861572266" Y="27.3772578125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.228005615234" Y="27.287072265625" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.288679931641" Y="27.21487890625" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.360336914062" Y="27.172978515625" />
                  <Point X="2.375404541016" Y="27.171162109375" />
                  <Point X="2.42976171875" Y="27.164607421875" />
                  <Point X="2.46608984375" Y="27.17060546875" />
                  <Point X="2.528951171875" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.709517089844" Y="27.867041015625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.01903515625" Y="27.996982421875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.246950683594" Y="27.668576171875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.53482421875" Y="26.78200390625" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.266830322266" Y="26.56747265625" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.208447998047" Y="26.474796875" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.194614257812" Y="26.37237890625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.226076904297" Y="26.272099609375" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947265625" Y="26.1988203125" />
                  <Point X="3.296062011719" Y="26.1903125" />
                  <Point X="3.350589355469" Y="26.1596171875" />
                  <Point X="3.36856640625" Y="26.153619140625" />
                  <Point X="3.389002441406" Y="26.15091796875" />
                  <Point X="3.462727050781" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.585784667969" Y="26.287302734375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.860352050781" Y="26.275220703125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.953166992188" Y="25.8616015625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.023327148437" Y="25.313431640625" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087890625" Y="25.232818359375" />
                  <Point X="3.709010009766" Y="25.221212890625" />
                  <Point X="3.636577880859" Y="25.17934765625" />
                  <Point X="3.610218505859" Y="25.151576171875" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.552969726562" Y="25.053765625" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.542498779297" Y="24.93834765625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759277344" Y="24.841240234375" />
                  <Point X="3.578805908203" Y="24.825888671875" />
                  <Point X="3.622265136719" Y="24.77051171875" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.656654785156" Y="24.746486328125" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.7590703125" Y="24.426865234375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.992450683594" Y="24.325568359375" />
                  <Point X="4.948430664062" Y="24.033595703125" />
                  <Point X="4.930522460938" Y="23.955119140625" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="3.739208007812" Y="23.859291015625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.355430419922" Y="23.89309375" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.161626953125" Y="23.81665625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.060944091797" Y="23.64883203125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.078168457031" Y="23.44945703125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.113080566406" Y="22.589626953125" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.328217285156" Y="22.398646484375" />
                  <Point X="4.204134277344" Y="22.197861328125" />
                  <Point X="4.16709375" Y="22.145232421875" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.044893310547" Y="22.572521484375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.69044140625" Y="22.75515625" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077392578" Y="22.78075390625" />
                  <Point X="2.450115478516" Y="22.76025" />
                  <Point X="2.309559326172" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.268094482422" Y="22.62635546875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.197633056641" Y="22.406724609375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.841104980469" Y="21.17004296875" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.982535400391" Y="20.914955078125" />
                  <Point X="2.835296142578" Y="20.809787109375" />
                  <Point X="2.793889160156" Y="20.782984375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.905572021484" Y="21.71808203125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.624293701172" Y="22.049271484375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.375216796875" Y="22.159626953125" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.126269775391" Y="22.099009765625" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.95677722168" Y="21.90027734375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.085952392578" Y="20.38263671875" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="0.994363708496" Y="20.036755859375" />
                  <Point X="0.956088928223" Y="20.029802734375" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#138" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.041940917734" Y="4.511650117458" Z="0.55" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="-0.812349928673" Y="5.003865791466" Z="0.55" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.55" />
                  <Point X="-1.584152528955" Y="4.815509069241" Z="0.55" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.55" />
                  <Point X="-1.741217125809" Y="4.69817964729" Z="0.55" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.55" />
                  <Point X="-1.732919404343" Y="4.363023419925" Z="0.55" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.55" />
                  <Point X="-1.817574798158" Y="4.30864029066" Z="0.55" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.55" />
                  <Point X="-1.913649924864" Y="4.338533584323" Z="0.55" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.55" />
                  <Point X="-1.977716765489" Y="4.405853366124" Z="0.55" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.55" />
                  <Point X="-2.644971511584" Y="4.326179708828" Z="0.55" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.55" />
                  <Point X="-3.250154269711" Y="3.892077472508" Z="0.55" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.55" />
                  <Point X="-3.296815524515" Y="3.651771614417" Z="0.55" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.55" />
                  <Point X="-2.995664451945" Y="3.073330797019" Z="0.55" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.55" />
                  <Point X="-3.04158439484" Y="3.007219173046" Z="0.55" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.55" />
                  <Point X="-3.121745668587" Y="2.999900247228" Z="0.55" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.55" />
                  <Point X="-3.28208773388" Y="3.083378424231" Z="0.55" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.55" />
                  <Point X="-4.117794784715" Y="2.961893681479" Z="0.55" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.55" />
                  <Point X="-4.473866727525" Y="2.390315334124" Z="0.55" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.55" />
                  <Point X="-4.362937058356" Y="2.122161408039" Z="0.55" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.55" />
                  <Point X="-3.673277036251" Y="1.566103559192" Z="0.55" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.55" />
                  <Point X="-3.686120532424" Y="1.507114645758" Z="0.55" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.55" />
                  <Point X="-3.739564416453" Y="1.479035985021" Z="0.55" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.55" />
                  <Point X="-3.983734922552" Y="1.505223055388" Z="0.55" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.55" />
                  <Point X="-4.938900564155" Y="1.163147408859" Z="0.55" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.55" />
                  <Point X="-5.041337259618" Y="0.574974237073" Z="0.55" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.55" />
                  <Point X="-4.738297182979" Y="0.360355379726" Z="0.55" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.55" />
                  <Point X="-3.554831217034" Y="0.033987505218" Z="0.55" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.55" />
                  <Point X="-3.541564562082" Y="0.006469164629" Z="0.55" />
                  <Point X="-3.539556741714" Y="0" Z="0.55" />
                  <Point X="-3.546800039053" Y="-0.023337786432" Z="0.55" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.55" />
                  <Point X="-3.570537378089" Y="-0.044888477916" Z="0.55" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.55" />
                  <Point X="-3.898590605794" Y="-0.13535667703" Z="0.55" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.55" />
                  <Point X="-4.999517852653" Y="-0.871814540093" Z="0.55" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.55" />
                  <Point X="-4.876367707698" Y="-1.405807924187" Z="0.55" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.55" />
                  <Point X="-4.49362510018" Y="-1.47464993571" Z="0.55" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.55" />
                  <Point X="-3.19842396453" Y="-1.319066981523" Z="0.55" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.55" />
                  <Point X="-3.19870865342" Y="-1.345740937502" Z="0.55" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.55" />
                  <Point X="-3.483073754893" Y="-1.569115093049" Z="0.55" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.55" />
                  <Point X="-4.273064701536" Y="-2.737055440736" Z="0.55" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.55" />
                  <Point X="-3.937575355076" Y="-3.200949429657" Z="0.55" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.55" />
                  <Point X="-3.582393525756" Y="-3.138357251903" Z="0.55" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.55" />
                  <Point X="-2.559256215936" Y="-2.569074253196" Z="0.55" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.55" />
                  <Point X="-2.717059761265" Y="-2.852684856736" Z="0.55" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.55" />
                  <Point X="-2.9793407862" Y="-4.109078853654" Z="0.55" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.55" />
                  <Point X="-2.546473956598" Y="-4.39049937596" Z="0.55" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.55" />
                  <Point X="-2.402307465941" Y="-4.385930785159" Z="0.55" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.55" />
                  <Point X="-2.024243820837" Y="-4.021494290865" Z="0.55" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.55" />
                  <Point X="-1.72585866486" Y="-3.999971945536" Z="0.55" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.55" />
                  <Point X="-1.47603185385" Y="-4.164542744976" Z="0.55" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.55" />
                  <Point X="-1.378015135464" Y="-4.447190143801" Z="0.55" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.55" />
                  <Point X="-1.375344095838" Y="-4.592726021557" Z="0.55" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.55" />
                  <Point X="-1.181578611776" Y="-4.939071446449" Z="0.55" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.55" />
                  <Point X="-0.882720927272" Y="-5.001135964097" Z="0.55" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="-0.730727743045" Y="-4.689297171734" Z="0.55" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="-0.288893792323" Y="-3.334071423541" Z="0.55" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="-0.054987898336" Y="-3.221305582839" Z="0.55" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.55" />
                  <Point X="0.198371181025" Y="-3.265806462449" Z="0.55" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.55" />
                  <Point X="0.381552067109" Y="-3.467574332227" Z="0.55" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.55" />
                  <Point X="0.504027117356" Y="-3.843238858082" Z="0.55" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.55" />
                  <Point X="0.958869536907" Y="-4.988111799568" Z="0.55" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.55" />
                  <Point X="1.138194338975" Y="-4.950273085397" Z="0.55" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.55" />
                  <Point X="1.129368728641" Y="-4.579557529899" Z="0.55" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.55" />
                  <Point X="0.999480420662" Y="-3.079061313899" Z="0.55" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.55" />
                  <Point X="1.152080902669" Y="-2.9081549174" Z="0.55" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.55" />
                  <Point X="1.373642381586" Y="-2.858881749858" Z="0.55" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.55" />
                  <Point X="1.591098558143" Y="-2.961507360653" Z="0.55" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.55" />
                  <Point X="1.859748540357" Y="-3.281075552522" Z="0.55" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.55" />
                  <Point X="2.814902421543" Y="-4.227710037699" Z="0.55" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.55" />
                  <Point X="3.005441029374" Y="-4.094451444213" Z="0.55" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.55" />
                  <Point X="2.878250302367" Y="-3.773676241935" Z="0.55" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.55" />
                  <Point X="2.240681090934" Y="-2.553107464475" Z="0.55" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.55" />
                  <Point X="2.306184887768" Y="-2.365651961061" Z="0.55" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.55" />
                  <Point X="2.467246417311" Y="-2.252716422199" Z="0.55" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.55" />
                  <Point X="2.675399339038" Y="-2.262766918557" Z="0.55" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.55" />
                  <Point X="3.013737355172" Y="-2.439499197907" Z="0.55" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.55" />
                  <Point X="4.201826285361" Y="-2.852264662557" Z="0.55" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.55" />
                  <Point X="4.364594368324" Y="-2.596357815159" Z="0.55" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.55" />
                  <Point X="4.137362731491" Y="-2.33942547406" Z="0.55" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.55" />
                  <Point X="3.114070296159" Y="-1.492223081487" Z="0.55" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.55" />
                  <Point X="3.104577717351" Y="-1.324470156428" Z="0.55" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.55" />
                  <Point X="3.193917176404" Y="-1.184030266637" Z="0.55" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.55" />
                  <Point X="3.359893966189" Y="-1.1244854585" Z="0.55" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.55" />
                  <Point X="3.726525680672" Y="-1.159000517776" Z="0.55" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.55" />
                  <Point X="4.973113729206" Y="-1.024723940557" Z="0.55" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.55" />
                  <Point X="5.03573596262" Y="-0.650606336148" Z="0.55" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.55" />
                  <Point X="4.765855503511" Y="-0.493556858619" Z="0.55" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.55" />
                  <Point X="3.675520659643" Y="-0.178943619435" Z="0.55" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.55" />
                  <Point X="3.61198349068" Y="-0.111960943265" Z="0.55" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.55" />
                  <Point X="3.585450315706" Y="-0.020967908825" Z="0.55" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.55" />
                  <Point X="3.595921134719" Y="0.075642622408" Z="0.55" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.55" />
                  <Point X="3.64339594772" Y="0.15198779531" Z="0.55" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.55" />
                  <Point X="3.727874754709" Y="0.209205224807" Z="0.55" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.55" />
                  <Point X="4.030112313795" Y="0.29641507415" Z="0.55" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.55" />
                  <Point X="4.996416004442" Y="0.900573976922" Z="0.55" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.55" />
                  <Point X="4.902777105896" Y="1.318328106056" Z="0.55" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.55" />
                  <Point X="4.57310212936" Y="1.368155861545" Z="0.55" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.55" />
                  <Point X="3.389396848246" Y="1.23176768264" Z="0.55" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.55" />
                  <Point X="3.314433348405" Y="1.265162709787" Z="0.55" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.55" />
                  <Point X="3.261691188533" Y="1.330862969569" Z="0.55" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.55" />
                  <Point X="3.237426824367" Y="1.413763852676" Z="0.55" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.55" />
                  <Point X="3.250444525452" Y="1.492609679273" Z="0.55" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.55" />
                  <Point X="3.300357379864" Y="1.568334666135" Z="0.55" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.55" />
                  <Point X="3.559106135539" Y="1.773617119316" Z="0.55" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.55" />
                  <Point X="4.283572140891" Y="2.725742972922" Z="0.55" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.55" />
                  <Point X="4.053464772552" Y="3.057465181847" Z="0.55" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.55" />
                  <Point X="3.678361113791" Y="2.941622815763" Z="0.55" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.55" />
                  <Point X="2.44701662016" Y="2.2501888373" Z="0.55" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.55" />
                  <Point X="2.375234444539" Y="2.252083647671" Z="0.55" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.55" />
                  <Point X="2.310598307042" Y="2.287534762905" Z="0.55" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.55" />
                  <Point X="2.26322383733" Y="2.34642655334" Z="0.55" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.55" />
                  <Point X="2.247346055088" Y="2.414523999293" Z="0.55" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.55" />
                  <Point X="2.262339105358" Y="2.492452955722" Z="0.55" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.55" />
                  <Point X="2.454002537097" Y="2.833777968164" Z="0.55" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.55" />
                  <Point X="2.834914070046" Y="4.211133275905" Z="0.55" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.55" />
                  <Point X="2.442085335172" Y="4.450461647292" Z="0.55" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.55" />
                  <Point X="2.033391560302" Y="4.651515612293" Z="0.55" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.55" />
                  <Point X="1.609475445857" Y="4.814652303894" Z="0.55" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.55" />
                  <Point X="1.004538669664" Y="4.971949619632" Z="0.55" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.55" />
                  <Point X="0.338509439574" Y="5.061109919958" Z="0.55" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.55" />
                  <Point X="0.151303643819" Y="4.919797388493" Z="0.55" />
                  <Point X="0" Y="4.355124473572" Z="0.55" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>