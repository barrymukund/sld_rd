<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#201" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3084" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.894691650391" Y="20.250712890625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557720458984" Y="21.502861328125" />
                  <Point X="0.542363342285" Y="21.532623046875" />
                  <Point X="0.40691619873" Y="21.727775390625" />
                  <Point X="0.378635681152" Y="21.7685234375" />
                  <Point X="0.356755401611" Y="21.790974609375" />
                  <Point X="0.330499603271" Y="21.810220703125" />
                  <Point X="0.302494873047" Y="21.82433203125" />
                  <Point X="0.092898124695" Y="21.8893828125" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.008664536476" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.246420684814" Y="21.8379140625" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318185058594" Y="21.81022265625" />
                  <Point X="-0.344440246582" Y="21.790978515625" />
                  <Point X="-0.366323272705" Y="21.7685234375" />
                  <Point X="-0.501770721436" Y="21.573369140625" />
                  <Point X="-0.530051086426" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.62018182373" Y="21.229248046875" />
                  <Point X="-0.916584838867" Y="20.12305859375" />
                  <Point X="-1.031843261719" Y="20.1454296875" />
                  <Point X="-1.079351928711" Y="20.15465234375" />
                  <Point X="-1.246418212891" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.266581054688" Y="20.735505859375" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.516614868164" Y="21.038025390625" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189941406" Y="21.089705078125" />
                  <Point X="-1.612888061523" Y="21.102005859375" />
                  <Point X="-1.643028198242" Y="21.109033203125" />
                  <Point X="-1.899142578125" Y="21.1258203125" />
                  <Point X="-1.95261730957" Y="21.12932421875" />
                  <Point X="-1.983421386719" Y="21.1262890625" />
                  <Point X="-2.014469482422" Y="21.11796875" />
                  <Point X="-2.042657714844" Y="21.105197265625" />
                  <Point X="-2.256066162109" Y="20.962603515625" />
                  <Point X="-2.300624267578" Y="20.932830078125" />
                  <Point X="-2.312786132812" Y="20.923177734375" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.373948486328" Y="20.849912109375" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.73209765625" Y="20.86751171875" />
                  <Point X="-2.801712158203" Y="20.910615234375" />
                  <Point X="-3.104721435547" Y="21.143921875" />
                  <Point X="-3.039611083984" Y="21.256697265625" />
                  <Point X="-2.423760986328" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405576171875" Y="22.4148125" />
                  <Point X="-2.4145625" Y="22.4444296875" />
                  <Point X="-2.428781982422" Y="22.473259765625" />
                  <Point X="-2.446808349609" Y="22.4984140625" />
                  <Point X="-2.461160400391" Y="22.512765625" />
                  <Point X="-2.464156982422" Y="22.51576171875" />
                  <Point X="-2.489317382812" Y="22.533791015625" />
                  <Point X="-2.518145507812" Y="22.548005859375" />
                  <Point X="-2.547762695312" Y="22.55698828125" />
                  <Point X="-2.5786953125" Y="22.555974609375" />
                  <Point X="-2.610219726562" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-2.861899658203" Y="22.41021484375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.02786328125" Y="22.133884765625" />
                  <Point X="-4.082857910156" Y="22.206134765625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.1834921875" Y="22.674662109375" />
                  <Point X="-3.105954345703" Y="23.501484375" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.58016796875" />
                  <Point X="-3.047482421875" Y="23.60477734375" />
                  <Point X="-3.046151611328" Y="23.609916015625" />
                  <Point X="-3.043347412109" Y="23.640345703125" />
                  <Point X="-3.045556640625" Y="23.672015625" />
                  <Point X="-3.05255859375" Y="23.70176171875" />
                  <Point X="-3.068641601562" Y="23.727744140625" />
                  <Point X="-3.089473876953" Y="23.75169921875" />
                  <Point X="-3.112971679688" Y="23.77123046875" />
                  <Point X="-3.134880126953" Y="23.784125" />
                  <Point X="-3.139460205078" Y="23.7868203125" />
                  <Point X="-3.168729492188" Y="23.798046875" />
                  <Point X="-3.200611572266" Y="23.804525390625" />
                  <Point X="-3.231928710938" Y="23.805615234375" />
                  <Point X="-3.513086181641" Y="23.7686015625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.812566894531" Y="23.9231328125" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.760354003906" Y="24.4506875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.517493408203" Y="24.785171875" />
                  <Point X="-3.4877265625" Y="24.80053125" />
                  <Point X="-3.464775390625" Y="24.8164609375" />
                  <Point X="-3.459987548828" Y="24.819783203125" />
                  <Point X="-3.437528808594" Y="24.84166796875" />
                  <Point X="-3.418279541016" Y="24.867927734375" />
                  <Point X="-3.404168457031" Y="24.895931640625" />
                  <Point X="-3.396515380859" Y="24.92058984375" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390647949219" Y="24.9538984375" />
                  <Point X="-3.390647949219" Y="24.9835390625" />
                  <Point X="-3.394917480469" Y="25.01169921875" />
                  <Point X="-3.402565185547" Y="25.03633984375" />
                  <Point X="-3.404162597656" Y="25.041490234375" />
                  <Point X="-3.418274902344" Y="25.06950390625" />
                  <Point X="-3.437521972656" Y="25.095763671875" />
                  <Point X="-3.459978759766" Y="25.1176484375" />
                  <Point X="-3.482931152344" Y="25.133578125" />
                  <Point X="-3.493392578125" Y="25.1398828125" />
                  <Point X="-3.513759521484" Y="25.150435546875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.789164794922" Y="25.226521484375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.838350097656" Y="25.883296875" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.645553222656" Y="26.415630859375" />
                  <Point X="-3.765666992188" Y="26.29979296875" />
                  <Point X="-3.744985351562" Y="26.299341796875" />
                  <Point X="-3.723422607422" Y="26.301228515625" />
                  <Point X="-3.703137451172" Y="26.305263671875" />
                  <Point X="-3.652321533203" Y="26.32128515625" />
                  <Point X="-3.641711425781" Y="26.324630859375" />
                  <Point X="-3.622773925781" Y="26.33296484375" />
                  <Point X="-3.604031005859" Y="26.343787109375" />
                  <Point X="-3.587352539062" Y="26.356015625" />
                  <Point X="-3.573715820312" Y="26.37156640625" />
                  <Point X="-3.561301757812" Y="26.389294921875" />
                  <Point X="-3.551351806641" Y="26.4074296875" />
                  <Point X="-3.530961669922" Y="26.45665625" />
                  <Point X="-3.526704345703" Y="26.46693359375" />
                  <Point X="-3.520916259766" Y="26.486791015625" />
                  <Point X="-3.517157470703" Y="26.508107421875" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532050537109" Y="26.58937890625" />
                  <Point X="-3.556653320312" Y="26.636640625" />
                  <Point X="-3.561790283203" Y="26.6465078125" />
                  <Point X="-3.573284667969" Y="26.6637109375" />
                  <Point X="-3.587196533203" Y="26.6802890625" />
                  <Point X="-3.602135498047" Y="26.69458984375" />
                  <Point X="-3.749143310547" Y="26.80739453125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.135015136719" Y="27.6413828125" />
                  <Point X="-4.08115625" Y="27.733654296875" />
                  <Point X="-3.750504882813" Y="28.158662109375" />
                  <Point X="-3.747058105469" Y="28.156671875" />
                  <Point X="-3.206656494141" Y="27.844669921875" />
                  <Point X="-3.187729980469" Y="27.836341796875" />
                  <Point X="-3.167088378906" Y="27.82983203125" />
                  <Point X="-3.146794677734" Y="27.825794921875" />
                  <Point X="-3.076021972656" Y="27.819603515625" />
                  <Point X="-3.061245361328" Y="27.818310546875" />
                  <Point X="-3.040561279297" Y="27.81876171875" />
                  <Point X="-3.019102783203" Y="27.821587890625" />
                  <Point X="-2.999014160156" Y="27.82650390625" />
                  <Point X="-2.980465332031" Y="27.83565234375" />
                  <Point X="-2.9622109375" Y="27.84728125" />
                  <Point X="-2.946078369141" Y="27.860228515625" />
                  <Point X="-2.895843505859" Y="27.910462890625" />
                  <Point X="-2.885354736328" Y="27.920951171875" />
                  <Point X="-2.872409179688" Y="27.937080078125" />
                  <Point X="-2.860779296875" Y="27.955333984375" />
                  <Point X="-2.851629638672" Y="27.97388671875" />
                  <Point X="-2.846712646484" Y="27.99398046875" />
                  <Point X="-2.843887207031" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.849627685547" Y="28.106892578125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-2.934938476562" Y="28.294365234375" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-2.7944296875" Y="29.022763671875" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.170374023438" Y="29.389279296875" />
                  <Point X="-2.167036621094" Y="29.3911328125" />
                  <Point X="-2.043197509766" Y="29.229744140625" />
                  <Point X="-2.028895507812" Y="29.214802734375" />
                  <Point X="-2.012317382812" Y="29.200890625" />
                  <Point X="-1.995115600586" Y="29.189396484375" />
                  <Point X="-1.916346069336" Y="29.148390625" />
                  <Point X="-1.899899536133" Y="29.139828125" />
                  <Point X="-1.880625854492" Y="29.13233203125" />
                  <Point X="-1.859718261719" Y="29.126728515625" />
                  <Point X="-1.839269287109" Y="29.123580078125" />
                  <Point X="-1.818623657227" Y="29.12493359375" />
                  <Point X="-1.797307373047" Y="29.128693359375" />
                  <Point X="-1.77745300293" Y="29.134482421875" />
                  <Point X="-1.695409179688" Y="29.168466796875" />
                  <Point X="-1.678279052734" Y="29.1755625" />
                  <Point X="-1.660145507812" Y="29.185509765625" />
                  <Point X="-1.642416137695" Y="29.197923828125" />
                  <Point X="-1.626864013672" Y="29.2115625" />
                  <Point X="-1.61463269043" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.568776367188" Y="29.350615234375" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.565136474609" Y="29.487080078125" />
                  <Point X="-1.584202026367" Y="29.6318984375" />
                  <Point X="-1.071071411133" Y="29.77576171875" />
                  <Point X="-0.949625061035" Y="29.80980859375" />
                  <Point X="-0.306812927246" Y="29.88504296875" />
                  <Point X="-0.293769134521" Y="29.88294140625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425933838" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.17959413147" Y="29.41088671875" />
                  <Point X="0.307419555664" Y="29.887939453125" />
                  <Point X="0.738002075195" Y="29.84284375" />
                  <Point X="0.844030944824" Y="29.831740234375" />
                  <Point X="1.375868164062" Y="29.703337890625" />
                  <Point X="1.481040527344" Y="29.6779453125" />
                  <Point X="1.826940795898" Y="29.552484375" />
                  <Point X="1.894646118164" Y="29.527927734375" />
                  <Point X="2.229374755859" Y="29.37138671875" />
                  <Point X="2.294558837891" Y="29.340900390625" />
                  <Point X="2.617961425781" Y="29.152486328125" />
                  <Point X="2.680975341797" Y="29.115775390625" />
                  <Point X="2.943259277344" Y="28.929251953125" />
                  <Point X="2.860381347656" Y="28.785703125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142075683594" Y="27.5399296875" />
                  <Point X="2.133076660156" Y="27.516056640625" />
                  <Point X="2.115315429688" Y="27.449638671875" />
                  <Point X="2.113197021484" Y="27.439552734375" />
                  <Point X="2.108151123047" Y="27.40678125" />
                  <Point X="2.107727783203" Y="27.380951171875" />
                  <Point X="2.114653320313" Y="27.323517578125" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442138672" Y="27.289603515625" />
                  <Point X="2.129708007812" Y="27.267515625" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.175608398437" Y="27.19509765625" />
                  <Point X="2.18201171875" Y="27.186705078125" />
                  <Point X="2.202732421875" Y="27.16246875" />
                  <Point X="2.221597412109" Y="27.14559375" />
                  <Point X="2.273970703125" Y="27.1100546875" />
                  <Point X="2.284906005859" Y="27.102634765625" />
                  <Point X="2.304955566406" Y="27.09226953125" />
                  <Point X="2.327045166016" Y="27.08400390625" />
                  <Point X="2.348966796875" Y="27.078662109375" />
                  <Point X="2.406393310547" Y="27.07173828125" />
                  <Point X="2.417428710938" Y="27.0710546875" />
                  <Point X="2.448330078125" Y="27.0709453125" />
                  <Point X="2.473208740234" Y="27.074169921875" />
                  <Point X="2.539627441406" Y="27.091931640625" />
                  <Point X="2.545317871094" Y="27.093646484375" />
                  <Point X="2.571266845703" Y="27.102357421875" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="2.846310546875" Y="27.258970703125" />
                  <Point X="3.967326416016" Y="27.906189453125" />
                  <Point X="4.085891113281" Y="27.741412109375" />
                  <Point X="4.12327734375" Y="27.689453125" />
                  <Point X="4.262198730469" Y="27.459884765625" />
                  <Point X="4.169251953125" Y="27.388564453125" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221425048828" Y="26.660240234375" />
                  <Point X="3.203974853516" Y="26.64162890625" />
                  <Point X="3.156173339844" Y="26.579267578125" />
                  <Point X="3.150670654297" Y="26.5712734375" />
                  <Point X="3.13221875" Y="26.541298828125" />
                  <Point X="3.121629638672" Y="26.517083984375" />
                  <Point X="3.103823486328" Y="26.4534140625" />
                  <Point X="3.100105712891" Y="26.440119140625" />
                  <Point X="3.096652099609" Y="26.417818359375" />
                  <Point X="3.095836425781" Y="26.39425" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.112356445312" Y="26.30092578125" />
                  <Point X="3.114880615234" Y="26.29128125" />
                  <Point X="3.125130615234" Y="26.25911328125" />
                  <Point X="3.136282714844" Y="26.235740234375" />
                  <Point X="3.176039306641" Y="26.1753125" />
                  <Point X="3.184340332031" Y="26.1626953125" />
                  <Point X="3.198893066406" Y="26.145451171875" />
                  <Point X="3.21613671875" Y="26.129361328125" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.291958984375" Y="26.083603515625" />
                  <Point X="3.301357910156" Y="26.0789765625" />
                  <Point X="3.331366210938" Y="26.066205078125" />
                  <Point X="3.356121826172" Y="26.059435546875" />
                  <Point X="3.434018310547" Y="26.049140625" />
                  <Point X="3.439520751953" Y="26.048576171875" />
                  <Point X="3.468859130859" Y="26.04642578125" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.733073974609" Y="26.079220703125" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.829881347656" Y="25.99875390625" />
                  <Point X="4.845936523438" Y="25.932806640625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.792084472656" Y="25.617771484375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704791503906" Y="25.3255859375" />
                  <Point X="3.681549560547" Y="25.315068359375" />
                  <Point X="3.605018798828" Y="25.27083203125" />
                  <Point X="3.597311035156" Y="25.265865234375" />
                  <Point X="3.567015625" Y="25.24420703125" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.501611816406" Y="25.167064453125" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.48030078125" Y="25.13556640625" />
                  <Point X="3.47052734375" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.448374755859" Y="25.0126796875" />
                  <Point X="3.447062011719" Y="25.003330078125" />
                  <Point X="3.443865722656" Y="24.967833984375" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.460485107422" Y="24.8615234375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.823333984375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.537942871094" Y="24.724080078125" />
                  <Point X="3.544486816406" Y="24.7165859375" />
                  <Point X="3.568386230469" Y="24.691947265625" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.66556640625" Y="24.631607421875" />
                  <Point X="3.670105957031" Y="24.62914453125" />
                  <Point X="3.698166015625" Y="24.614900390625" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="3.941137695312" Y="24.547677734375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.863986816406" Y="24.11073046875" />
                  <Point X="4.855022460938" Y="24.05126953125" />
                  <Point X="4.801174804688" Y="23.81530078125" />
                  <Point X="4.671665039062" Y="23.8323515625" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.224456054688" Y="23.96184375" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163976806641" Y="23.943404296875" />
                  <Point X="3.136150878906" Y="23.926513671875" />
                  <Point X="3.112397705078" Y="23.906041015625" />
                  <Point X="3.021609375" Y="23.796849609375" />
                  <Point X="3.002653564453" Y="23.774052734375" />
                  <Point X="2.987933349609" Y="23.749671875" />
                  <Point X="2.976589355469" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.956745361328" Y="23.553228515625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450439453" Y="23.432001953125" />
                  <Point X="3.059574951172" Y="23.302708984375" />
                  <Point X="3.076930664062" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.319019042969" Y="23.079185546875" />
                  <Point X="4.213121582031" Y="22.3931171875" />
                  <Point X="4.150078125" Y="22.291103515625" />
                  <Point X="4.124807128906" Y="22.250208984375" />
                  <Point X="4.028982421875" Y="22.1140546875" />
                  <Point X="3.911828369141" Y="22.181693359375" />
                  <Point X="2.800954345703" Y="22.823056640625" />
                  <Point X="2.786137451172" Y="22.829984375" />
                  <Point X="2.754224121094" Y="22.84017578125" />
                  <Point X="2.575458740234" Y="22.8724609375" />
                  <Point X="2.538133789062" Y="22.879201171875" />
                  <Point X="2.506777099609" Y="22.879603515625" />
                  <Point X="2.474605957031" Y="22.87464453125" />
                  <Point X="2.444833984375" Y="22.864822265625" />
                  <Point X="2.296323730469" Y="22.786662109375" />
                  <Point X="2.265315917969" Y="22.77034375" />
                  <Point X="2.242388183594" Y="22.753453125" />
                  <Point X="2.221427490234" Y="22.732494140625" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.126372070313" Y="22.56105078125" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.127960449219" Y="22.2579765625" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.285731445312" Y="21.941978515625" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.811837158203" Y="20.909775390625" />
                  <Point X="2.781865722656" Y="20.8883671875" />
                  <Point X="2.701763427734" Y="20.83651953125" />
                  <Point X="2.606245605469" Y="20.961" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747504760742" Y="22.077818359375" />
                  <Point X="1.721923217773" Y="22.099443359375" />
                  <Point X="1.545612182617" Y="22.212794921875" />
                  <Point X="1.508799804688" Y="22.2364609375" />
                  <Point X="1.479987426758" Y="22.24883203125" />
                  <Point X="1.448369384766" Y="22.2565625" />
                  <Point X="1.4171015625" Y="22.258880859375" />
                  <Point X="1.224275512695" Y="22.24113671875" />
                  <Point X="1.184014770508" Y="22.23743359375" />
                  <Point X="1.156366943359" Y="22.230603515625" />
                  <Point X="1.128979125977" Y="22.219259765625" />
                  <Point X="1.104594848633" Y="22.204537109375" />
                  <Point X="0.955699523926" Y="22.080734375" />
                  <Point X="0.924611206055" Y="22.054884765625" />
                  <Point X="0.904141784668" Y="22.03113671875" />
                  <Point X="0.88725012207" Y="22.0033125" />
                  <Point X="0.875624511719" Y="21.97419140625" />
                  <Point X="0.831105407715" Y="21.769369140625" />
                  <Point X="0.821810241699" Y="21.72660546875" />
                  <Point X="0.81972479248" Y="21.710375" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.857691833496" Y="21.388623046875" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="1.004046142578" Y="20.136134765625" />
                  <Point X="0.975669250488" Y="20.129916015625" />
                  <Point X="0.929315429688" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.01374206543" Y="20.238689453125" />
                  <Point X="-1.058443237305" Y="20.2473671875" />
                  <Point X="-1.141246459961" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123333984375" Y="20.502306640625" />
                  <Point X="-1.17340637207" Y="20.7540390625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250210327148" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.453976928711" Y="21.10944921875" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506736938477" Y="21.15403125" />
                  <Point X="-1.533021240234" Y="21.170376953125" />
                  <Point X="-1.546836425781" Y="21.177474609375" />
                  <Point X="-1.576534545898" Y="21.189775390625" />
                  <Point X="-1.591316894531" Y="21.1945234375" />
                  <Point X="-1.62145690918" Y="21.20155078125" />
                  <Point X="-1.636814819336" Y="21.203830078125" />
                  <Point X="-1.892929077148" Y="21.2206171875" />
                  <Point X="-1.946403808594" Y="21.22412109375" />
                  <Point X="-1.961932617188" Y="21.2238671875" />
                  <Point X="-1.992736694336" Y="21.22083203125" />
                  <Point X="-2.008011962891" Y="21.21805078125" />
                  <Point X="-2.039060180664" Y="21.20973046875" />
                  <Point X="-2.053675537109" Y="21.204501953125" />
                  <Point X="-2.081863769531" Y="21.19173046875" />
                  <Point X="-2.095436523437" Y="21.1841875" />
                  <Point X="-2.308844970703" Y="21.04159375" />
                  <Point X="-2.353403076172" Y="21.0118203125" />
                  <Point X="-2.359682128906" Y="21.0072421875" />
                  <Point X="-2.380443603516" Y="20.9898671875" />
                  <Point X="-2.402760253906" Y="20.9672265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.449317138672" Y="20.907744140625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.682086669922" Y="20.948283203125" />
                  <Point X="-2.747588134766" Y="20.98883984375" />
                  <Point X="-2.980861816406" Y="21.168453125" />
                  <Point X="-2.957338378906" Y="21.209197265625" />
                  <Point X="-2.34148828125" Y="22.275880859375" />
                  <Point X="-2.334849609375" Y="22.28991796875" />
                  <Point X="-2.323947509766" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.38076953125" />
                  <Point X="-2.310626953125" Y="22.41170703125" />
                  <Point X="-2.314668701172" Y="22.44239453125" />
                  <Point X="-2.323655029297" Y="22.47201171875" />
                  <Point X="-2.329362060547" Y="22.486451171875" />
                  <Point X="-2.343581542969" Y="22.51528125" />
                  <Point X="-2.351562988281" Y="22.52859765625" />
                  <Point X="-2.369589355469" Y="22.553751953125" />
                  <Point X="-2.379634277344" Y="22.56558984375" />
                  <Point X="-2.393986328125" Y="22.57994140625" />
                  <Point X="-2.408822509766" Y="22.592982421875" />
                  <Point X="-2.433982910156" Y="22.61101171875" />
                  <Point X="-2.447303710938" Y="22.61899609375" />
                  <Point X="-2.476131835938" Y="22.6332109375" />
                  <Point X="-2.490573730469" Y="22.638916015625" />
                  <Point X="-2.520190917969" Y="22.6478984375" />
                  <Point X="-2.550874267578" Y="22.6519375" />
                  <Point X="-2.581806884766" Y="22.650923828125" />
                  <Point X="-2.597231445312" Y="22.6491484375" />
                  <Point X="-2.628755859375" Y="22.642876953125" />
                  <Point X="-2.643686279297" Y="22.63861328125" />
                  <Point X="-2.672650390625" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-2.909399902344" Y="22.492486328125" />
                  <Point X="-3.793086914062" Y="21.9822890625" />
                  <Point X="-3.952269775391" Y="22.191423828125" />
                  <Point X="-4.004018798828" Y="22.25941015625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-4.125659667969" Y="22.59929296875" />
                  <Point X="-3.048121826172" Y="23.426115234375" />
                  <Point X="-3.03648046875" Y="23.436689453125" />
                  <Point X="-3.015102294922" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833740234" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890625" Y="23.556349609375" />
                  <Point X="-2.955516845703" Y="23.580958984375" />
                  <Point X="-2.951552490234" Y="23.60119921875" />
                  <Point X="-2.948748291016" Y="23.63162890625" />
                  <Point X="-2.948577636719" Y="23.64695703125" />
                  <Point X="-2.950786865234" Y="23.678626953125" />
                  <Point X="-2.953083984375" Y="23.693783203125" />
                  <Point X="-2.9600859375" Y="23.723529296875" />
                  <Point X="-2.971781494141" Y="23.75176171875" />
                  <Point X="-2.987864501953" Y="23.777744140625" />
                  <Point X="-2.996956787109" Y="23.790083984375" />
                  <Point X="-3.0177890625" Y="23.8140390625" />
                  <Point X="-3.028748535156" Y="23.8247578125" />
                  <Point X="-3.052246337891" Y="23.8442890625" />
                  <Point X="-3.064784667969" Y="23.8531015625" />
                  <Point X="-3.086693115234" Y="23.86599609375" />
                  <Point X="-3.105438720703" Y="23.87551953125" />
                  <Point X="-3.134708007813" Y="23.88674609375" />
                  <Point X="-3.149811767578" Y="23.89114453125" />
                  <Point X="-3.181693847656" Y="23.897623046875" />
                  <Point X="-3.197307617188" Y="23.89946875" />
                  <Point X="-3.228624755859" Y="23.90055859375" />
                  <Point X="-3.244328125" Y="23.899802734375" />
                  <Point X="-3.525485595703" Y="23.8627890625" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.720521972656" Y="23.94664453125" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.735765625" Y="24.358923828125" />
                  <Point X="-3.508287841797" Y="24.687826171875" />
                  <Point X="-3.500469970703" Y="24.690287109375" />
                  <Point X="-3.473931640625" Y="24.700748046875" />
                  <Point X="-3.444164794922" Y="24.716107421875" />
                  <Point X="-3.433558837891" Y="24.72248828125" />
                  <Point X="-3.410617431641" Y="24.738412109375" />
                  <Point X="-3.410616699219" Y="24.73841015625" />
                  <Point X="-3.3936875" Y="24.751744140625" />
                  <Point X="-3.371228759766" Y="24.77362890625" />
                  <Point X="-3.360909423828" Y="24.78550390625" />
                  <Point X="-3.34166015625" Y="24.811763671875" />
                  <Point X="-3.333441650391" Y="24.825177734375" />
                  <Point X="-3.319330566406" Y="24.853181640625" />
                  <Point X="-3.313437988281" Y="24.867771484375" />
                  <Point X="-3.305784912109" Y="24.8924296875" />
                  <Point X="-3.300990966797" Y="24.911498046875" />
                  <Point X="-3.296721435547" Y="24.939658203125" />
                  <Point X="-3.295647949219" Y="24.9538984375" />
                  <Point X="-3.295647949219" Y="24.9835390625" />
                  <Point X="-3.296721435547" Y="24.997779296875" />
                  <Point X="-3.300990966797" Y="25.025939453125" />
                  <Point X="-3.304187011719" Y="25.039859375" />
                  <Point X="-3.311829345703" Y="25.064482421875" />
                  <Point X="-3.311829101562" Y="25.064482421875" />
                  <Point X="-3.319320068359" Y="25.08423046875" />
                  <Point X="-3.333432373047" Y="25.112244140625" />
                  <Point X="-3.34165234375" Y="25.1256640625" />
                  <Point X="-3.360899414062" Y="25.151923828125" />
                  <Point X="-3.371218994141" Y="25.163798828125" />
                  <Point X="-3.39367578125" Y="25.18568359375" />
                  <Point X="-3.405812988281" Y="25.195693359375" />
                  <Point X="-3.428765380859" Y="25.211623046875" />
                  <Point X="-3.449688232422" Y="25.224232421875" />
                  <Point X="-3.470055175781" Y="25.23478515625" />
                  <Point X="-3.479408447266" Y="25.2390078125" />
                  <Point X="-3.498525146484" Y="25.246421875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.764577148438" Y="25.31828515625" />
                  <Point X="-4.785446289062" Y="25.591826171875" />
                  <Point X="-4.744373535156" Y="25.869390625" />
                  <Point X="-4.731331542969" Y="25.957525390625" />
                  <Point X="-4.633586425781" Y="26.318234375" />
                  <Point X="-3.778068847656" Y="26.20560546875" />
                  <Point X="-3.767739013672" Y="26.20481640625" />
                  <Point X="-3.747057373047" Y="26.204365234375" />
                  <Point X="-3.736704589844" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704888183594" Y="26.2080546875" />
                  <Point X="-3.684603027344" Y="26.21208984375" />
                  <Point X="-3.674571533203" Y="26.21466015625" />
                  <Point X="-3.623755615234" Y="26.230681640625" />
                  <Point X="-3.603445556641" Y="26.237677734375" />
                  <Point X="-3.584508056641" Y="26.24601171875" />
                  <Point X="-3.575270507812" Y="26.2506953125" />
                  <Point X="-3.556527587891" Y="26.261517578125" />
                  <Point X="-3.547858398438" Y="26.267173828125" />
                  <Point X="-3.531179931641" Y="26.27940234375" />
                  <Point X="-3.515925537109" Y="26.293380859375" />
                  <Point X="-3.502288818359" Y="26.308931640625" />
                  <Point X="-3.495897216797" Y="26.317076171875" />
                  <Point X="-3.483483154297" Y="26.3348046875" />
                  <Point X="-3.478014404297" Y="26.34359765625" />
                  <Point X="-3.468064453125" Y="26.361732421875" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.443193115234" Y="26.42030078125" />
                  <Point X="-3.435499755859" Y="26.440349609375" />
                  <Point X="-3.429711669922" Y="26.46020703125" />
                  <Point X="-3.427359619141" Y="26.47029296875" />
                  <Point X="-3.423600830078" Y="26.491609375" />
                  <Point X="-3.422360839844" Y="26.50189453125" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432791259766" Y="26.594689453125" />
                  <Point X="-3.436013183594" Y="26.604533203125" />
                  <Point X="-3.443509765625" Y="26.623810546875" />
                  <Point X="-3.447784423828" Y="26.633244140625" />
                  <Point X="-3.472387207031" Y="26.680505859375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494294189453" Y="26.71648828125" />
                  <Point X="-3.500512939453" Y="26.724779296875" />
                  <Point X="-3.514424804688" Y="26.741357421875" />
                  <Point X="-3.521503173828" Y="26.7489140625" />
                  <Point X="-3.536442138672" Y="26.76321484375" />
                  <Point X="-3.544302734375" Y="26.769958984375" />
                  <Point X="-3.691310546875" Y="26.882763671875" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.05296875" Y="27.593494140625" />
                  <Point X="-4.002296386719" Y="27.680306640625" />
                  <Point X="-3.726338378906" Y="28.035013671875" />
                  <Point X="-3.254160644531" Y="27.762400390625" />
                  <Point X="-3.244918457031" Y="27.75771484375" />
                  <Point X="-3.225991943359" Y="27.74938671875" />
                  <Point X="-3.216302978516" Y="27.745740234375" />
                  <Point X="-3.195661376953" Y="27.73923046875" />
                  <Point X="-3.185624023438" Y="27.736658203125" />
                  <Point X="-3.165330322266" Y="27.73262109375" />
                  <Point X="-3.155073974609" Y="27.73115625" />
                  <Point X="-3.084301269531" Y="27.72496484375" />
                  <Point X="-3.059173583984" Y="27.723333984375" />
                  <Point X="-3.038489501953" Y="27.72378515625" />
                  <Point X="-3.028156494141" Y="27.72457421875" />
                  <Point X="-3.006697998047" Y="27.727400390625" />
                  <Point X="-2.996520996094" Y="27.729310546875" />
                  <Point X="-2.976432373047" Y="27.7342265625" />
                  <Point X="-2.956992431641" Y="27.741302734375" />
                  <Point X="-2.938443603516" Y="27.750451171875" />
                  <Point X="-2.929423095703" Y="27.755529296875" />
                  <Point X="-2.911168701172" Y="27.767158203125" />
                  <Point X="-2.902749511719" Y="27.77319140625" />
                  <Point X="-2.886616943359" Y="27.786138671875" />
                  <Point X="-2.878903564453" Y="27.793052734375" />
                  <Point X="-2.828668701172" Y="27.843287109375" />
                  <Point X="-2.811267333984" Y="27.861486328125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288818359" Y="27.886033203125" />
                  <Point X="-2.780658935547" Y="27.904287109375" />
                  <Point X="-2.775577148438" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759352294922" Y="27.951306640625" />
                  <Point X="-2.754435302734" Y="27.971400390625" />
                  <Point X="-2.752525634766" Y="27.981580078125" />
                  <Point X="-2.749700195312" Y="28.0030390625" />
                  <Point X="-2.748909912109" Y="28.013365234375" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.754989257812" Y="28.115171875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.852666015625" Y="28.341865234375" />
                  <Point X="-3.059386474609" Y="28.699916015625" />
                  <Point X="-2.736627685547" Y="28.94737109375" />
                  <Point X="-2.648369140625" Y="29.015037109375" />
                  <Point X="-2.192524169922" Y="29.268294921875" />
                  <Point X="-2.118565917969" Y="29.171912109375" />
                  <Point X="-2.111824951172" Y="29.164052734375" />
                  <Point X="-2.097522949219" Y="29.149111328125" />
                  <Point X="-2.089963867188" Y="29.14203125" />
                  <Point X="-2.073385742188" Y="29.128119140625" />
                  <Point X="-2.065097412109" Y="29.12190234375" />
                  <Point X="-2.047895629883" Y="29.110408203125" />
                  <Point X="-2.038982543945" Y="29.105130859375" />
                  <Point X="-1.960213012695" Y="29.064125" />
                  <Point X="-1.934334960938" Y="29.0512890625" />
                  <Point X="-1.915061157227" Y="29.04379296875" />
                  <Point X="-1.905219116211" Y="29.0405703125" />
                  <Point X="-1.884311523438" Y="29.034966796875" />
                  <Point X="-1.874174804688" Y="29.032833984375" />
                  <Point X="-1.853725708008" Y="29.029685546875" />
                  <Point X="-1.83305456543" Y="29.028783203125" />
                  <Point X="-1.812408813477" Y="29.03013671875" />
                  <Point X="-1.802122314453" Y="29.031376953125" />
                  <Point X="-1.780806030273" Y="29.03513671875" />
                  <Point X="-1.77071496582" Y="29.037490234375" />
                  <Point X="-1.750860595703" Y="29.043279296875" />
                  <Point X="-1.741097412109" Y="29.04671484375" />
                  <Point X="-1.659053710938" Y="29.08069921875" />
                  <Point X="-1.632589111328" Y="29.092271484375" />
                  <Point X="-1.614455444336" Y="29.10221875" />
                  <Point X="-1.60565625" Y="29.107689453125" />
                  <Point X="-1.587926879883" Y="29.120103515625" />
                  <Point X="-1.579778686523" Y="29.126498046875" />
                  <Point X="-1.5642265625" Y="29.14013671875" />
                  <Point X="-1.550251464844" Y="29.155388671875" />
                  <Point X="-1.538020019531" Y="29.1720703125" />
                  <Point X="-1.532360229492" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.516855712891" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.478173217773" Y="29.322048828125" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.47094921875" Y="29.49948046875" />
                  <Point X="-1.479266357422" Y="29.56265625" />
                  <Point X="-1.04542565918" Y="29.6842890625" />
                  <Point X="-0.931166992188" Y="29.7163203125" />
                  <Point X="-0.365222961426" Y="29.78255859375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.271357177734" Y="29.386298828125" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.728106750488" Y="29.748361328125" />
                  <Point X="0.827852722168" Y="29.737916015625" />
                  <Point X="1.353572875977" Y="29.610990234375" />
                  <Point X="1.453623535156" Y="29.586833984375" />
                  <Point X="1.794548461914" Y="29.463177734375" />
                  <Point X="1.858249267578" Y="29.44007421875" />
                  <Point X="2.189130126953" Y="29.28533203125" />
                  <Point X="2.250430908203" Y="29.256662109375" />
                  <Point X="2.570138671875" Y="29.070400390625" />
                  <Point X="2.629430664063" Y="29.035857421875" />
                  <Point X="2.817779052734" Y="28.9019140625" />
                  <Point X="2.778108886719" Y="28.833203125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062371826172" Y="27.5931015625" />
                  <Point X="2.053181640625" Y="27.573439453125" />
                  <Point X="2.044182617188" Y="27.54956640625" />
                  <Point X="2.041301513672" Y="27.540599609375" />
                  <Point X="2.023540283203" Y="27.474181640625" />
                  <Point X="2.019303344727" Y="27.454009765625" />
                  <Point X="2.014257446289" Y="27.42123828125" />
                  <Point X="2.01316394043" Y="27.408337890625" />
                  <Point X="2.012740600586" Y="27.3825078125" />
                  <Point X="2.013410888672" Y="27.369578125" />
                  <Point X="2.020336425781" Y="27.31214453125" />
                  <Point X="2.023800537109" Y="27.289033203125" />
                  <Point X="2.029143554687" Y="27.267109375" />
                  <Point X="2.032468261719" Y="27.256306640625" />
                  <Point X="2.040734130859" Y="27.23421875" />
                  <Point X="2.045318237305" Y="27.223888671875" />
                  <Point X="2.055681152344" Y="27.20384375" />
                  <Point X="2.061459716797" Y="27.19412890625" />
                  <Point X="2.096997314453" Y="27.141755859375" />
                  <Point X="2.109803955078" Y="27.124970703125" />
                  <Point X="2.130524658203" Y="27.100734375" />
                  <Point X="2.139395751953" Y="27.091662109375" />
                  <Point X="2.158260742188" Y="27.074787109375" />
                  <Point X="2.168254638672" Y="27.066984375" />
                  <Point X="2.220627929688" Y="27.0314453125" />
                  <Point X="2.241278076172" Y="27.018244140625" />
                  <Point X="2.261327636719" Y="27.00787890625" />
                  <Point X="2.271662353516" Y="27.003294921875" />
                  <Point X="2.293751953125" Y="26.995029296875" />
                  <Point X="2.304553955078" Y="26.991705078125" />
                  <Point X="2.326475585938" Y="26.98636328125" />
                  <Point X="2.337595214844" Y="26.984345703125" />
                  <Point X="2.395021728516" Y="26.977421875" />
                  <Point X="2.417092529297" Y="26.9760546875" />
                  <Point X="2.447993896484" Y="26.9759453125" />
                  <Point X="2.460541259766" Y="26.976732421875" />
                  <Point X="2.485419921875" Y="26.97995703125" />
                  <Point X="2.497751220703" Y="26.98239453125" />
                  <Point X="2.564169921875" Y="27.00015625" />
                  <Point X="2.57555078125" Y="27.0035859375" />
                  <Point X="2.601499755859" Y="27.012296875" />
                  <Point X="2.610322509766" Y="27.0157578125" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="2.893810302734" Y="27.176697265625" />
                  <Point X="3.940405273438" Y="27.78094921875" />
                  <Point X="4.008778808594" Y="27.68592578125" />
                  <Point X="4.043959960938" Y="27.63703125" />
                  <Point X="4.136884277344" Y="27.48347265625" />
                  <Point X="4.111419921875" Y="27.46393359375" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168130126953" Y="26.739861328125" />
                  <Point X="3.152122802734" Y="26.72521875" />
                  <Point X="3.134672607422" Y="26.706607421875" />
                  <Point X="3.128577148438" Y="26.699423828125" />
                  <Point X="3.080775634766" Y="26.6370625" />
                  <Point X="3.069770263672" Y="26.62107421875" />
                  <Point X="3.051318359375" Y="26.591099609375" />
                  <Point X="3.045177246094" Y="26.579361328125" />
                  <Point X="3.034588134766" Y="26.555146484375" />
                  <Point X="3.030140136719" Y="26.542669921875" />
                  <Point X="3.012333984375" Y="26.479" />
                  <Point X="3.006224853516" Y="26.454658203125" />
                  <Point X="3.002771240234" Y="26.432357421875" />
                  <Point X="3.001708984375" Y="26.421103515625" />
                  <Point X="3.000893310547" Y="26.39753515625" />
                  <Point X="3.001175048828" Y="26.386236328125" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.01931640625" Y="26.281728515625" />
                  <Point X="3.024364746094" Y="26.262439453125" />
                  <Point X="3.034614746094" Y="26.230271484375" />
                  <Point X="3.039390380859" Y="26.218203125" />
                  <Point X="3.050542480469" Y="26.194830078125" />
                  <Point X="3.056918945312" Y="26.183525390625" />
                  <Point X="3.096675537109" Y="26.12309765625" />
                  <Point X="3.111738769531" Y="26.10142578125" />
                  <Point X="3.126291503906" Y="26.084181640625" />
                  <Point X="3.13408203125" Y="26.0759921875" />
                  <Point X="3.151325683594" Y="26.05990234375" />
                  <Point X="3.160032226562" Y="26.052697265625" />
                  <Point X="3.178241699219" Y="26.03937109375" />
                  <Point X="3.187744628906" Y="26.03325" />
                  <Point X="3.245357421875" Y="26.000818359375" />
                  <Point X="3.264155273438" Y="25.991564453125" />
                  <Point X="3.294163574219" Y="25.97879296875" />
                  <Point X="3.306308105469" Y="25.9745703125" />
                  <Point X="3.331063720703" Y="25.96780078125" />
                  <Point X="3.343674804688" Y="25.96525390625" />
                  <Point X="3.421571289062" Y="25.954958984375" />
                  <Point X="3.432576171875" Y="25.953830078125" />
                  <Point X="3.461914550781" Y="25.9516796875" />
                  <Point X="3.471601318359" Y="25.95146484375" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.745473388672" Y="25.985033203125" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.737577148438" Y="25.976283203125" />
                  <Point X="4.75268359375" Y="25.914232421875" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.76749609375" Y="25.70953515625" />
                  <Point X="3.691991455078" Y="25.421353515625" />
                  <Point X="3.68602734375" Y="25.41954296875" />
                  <Point X="3.665625244141" Y="25.41213671875" />
                  <Point X="3.642383300781" Y="25.401619140625" />
                  <Point X="3.634008300781" Y="25.39731640625" />
                  <Point X="3.557477539062" Y="25.353080078125" />
                  <Point X="3.542062011719" Y="25.343146484375" />
                  <Point X="3.511766601562" Y="25.32148828125" />
                  <Point X="3.501362792969" Y="25.31287109375" />
                  <Point X="3.481877441406" Y="25.294240234375" />
                  <Point X="3.472795898438" Y="25.2842265625" />
                  <Point X="3.426877441406" Y="25.22571484375" />
                  <Point X="3.4108515625" Y="25.204203125" />
                  <Point X="3.399127929688" Y="25.184921875" />
                  <Point X="3.393842773438" Y="25.174935546875" />
                  <Point X="3.384069335938" Y="25.15347265625" />
                  <Point X="3.380006103516" Y="25.1429296875" />
                  <Point X="3.373159667969" Y="25.1214296875" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.3550703125" Y="25.030548828125" />
                  <Point X="3.352444824219" Y="25.011849609375" />
                  <Point X="3.349248535156" Y="24.976353515625" />
                  <Point X="3.348983154297" Y="24.96311328125" />
                  <Point X="3.350296142578" Y="24.936724609375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.367180908203" Y="24.843654296875" />
                  <Point X="3.373158935547" Y="24.816013671875" />
                  <Point X="3.380005371094" Y="24.79451171875" />
                  <Point X="3.384069580078" Y="24.783962890625" />
                  <Point X="3.393843261719" Y="24.7625" />
                  <Point X="3.399130126953" Y="24.75251171875" />
                  <Point X="3.410853515625" Y="24.733232421875" />
                  <Point X="3.417290039063" Y="24.72394140625" />
                  <Point X="3.463208496094" Y="24.6654296875" />
                  <Point X="3.476296386719" Y="24.65044140625" />
                  <Point X="3.500195800781" Y="24.625802734375" />
                  <Point X="3.509964599609" Y="24.61703515625" />
                  <Point X="3.530613769531" Y="24.600931640625" />
                  <Point X="3.541494140625" Y="24.593595703125" />
                  <Point X="3.618025146484" Y="24.549359375" />
                  <Point X="3.627104248047" Y="24.54443359375" />
                  <Point X="3.655164306641" Y="24.530189453125" />
                  <Point X="3.664187255859" Y="24.526185546875" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="3.916550048828" Y="24.4559140625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.770048339844" Y="24.124892578125" />
                  <Point X="4.76161328125" Y="24.06894140625" />
                  <Point X="4.727803222656" Y="23.92078125" />
                  <Point X="4.684065429688" Y="23.9265390625" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354482177734" Y="24.087322265625" />
                  <Point X="3.204279052734" Y="24.05467578125" />
                  <Point X="3.17291796875" Y="24.047859375" />
                  <Point X="3.157876220703" Y="24.0432578125" />
                  <Point X="3.128758056641" Y="24.031634765625" />
                  <Point X="3.114681640625" Y="24.02461328125" />
                  <Point X="3.086855712891" Y="24.00772265625" />
                  <Point X="3.07412890625" Y="23.998474609375" />
                  <Point X="3.050375732422" Y="23.978001953125" />
                  <Point X="3.039349365234" Y="23.96677734375" />
                  <Point X="2.948561035156" Y="23.8575859375" />
                  <Point X="2.929605224609" Y="23.8347890625" />
                  <Point X="2.921326904297" Y="23.823154296875" />
                  <Point X="2.906606689453" Y="23.7987734375" />
                  <Point X="2.900164794922" Y="23.78602734375" />
                  <Point X="2.888820800781" Y="23.758640625" />
                  <Point X="2.884362792969" Y="23.745072265625" />
                  <Point X="2.877531005859" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.862145019531" Y="23.56193359375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876785644531" Y="23.42333203125" />
                  <Point X="2.889157470703" Y="23.39451953125" />
                  <Point X="2.896540527344" Y="23.380626953125" />
                  <Point X="2.979665039062" Y="23.251333984375" />
                  <Point X="2.997020751953" Y="23.224337890625" />
                  <Point X="3.001743164062" Y="23.21764453125" />
                  <Point X="3.019788818359" Y="23.195556640625" />
                  <Point X="3.043485595703" Y="23.1718828125" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.261186523438" Y="23.00381640625" />
                  <Point X="4.087169677734" Y="22.370017578125" />
                  <Point X="4.069264648437" Y="22.341044921875" />
                  <Point X="4.045482421875" Y="22.30255859375" />
                  <Point X="4.001276123047" Y="22.239748046875" />
                  <Point X="3.959328125" Y="22.263966796875" />
                  <Point X="2.848454101562" Y="22.905330078125" />
                  <Point X="2.84119140625" Y="22.909115234375" />
                  <Point X="2.815037597656" Y="22.920482421875" />
                  <Point X="2.783124267578" Y="22.930673828125" />
                  <Point X="2.771108154297" Y="22.9336640625" />
                  <Point X="2.592342773438" Y="22.96594921875" />
                  <Point X="2.555017822266" Y="22.972689453125" />
                  <Point X="2.539352539062" Y="22.974193359375" />
                  <Point X="2.507995849609" Y="22.974595703125" />
                  <Point X="2.492304443359" Y="22.973494140625" />
                  <Point X="2.460133300781" Y="22.96853515625" />
                  <Point X="2.444841796875" Y="22.964861328125" />
                  <Point X="2.415069824219" Y="22.9550390625" />
                  <Point X="2.400589355469" Y="22.948890625" />
                  <Point X="2.252079101562" Y="22.87073046875" />
                  <Point X="2.221071289062" Y="22.854412109375" />
                  <Point X="2.208969482422" Y="22.846830078125" />
                  <Point X="2.186041748047" Y="22.829939453125" />
                  <Point X="2.175215820312" Y="22.820630859375" />
                  <Point X="2.154255126953" Y="22.799671875" />
                  <Point X="2.144942626953" Y="22.788841796875" />
                  <Point X="2.128047119141" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.042304077148" Y="22.605294921875" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.03447277832" Y="22.24109375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236816406" Y="22.168453125" />
                  <Point X="2.0640703125" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.203459228516" Y="21.894478515625" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723752441406" Y="20.963916015625" />
                  <Point X="2.681614013672" Y="21.01883203125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828652709961" Y="22.129853515625" />
                  <Point X="1.808834594727" Y="22.150369140625" />
                  <Point X="1.783253173828" Y="22.171994140625" />
                  <Point X="1.773297973633" Y="22.179353515625" />
                  <Point X="1.596986938477" Y="22.292705078125" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.546280883789" Y="22.32375390625" />
                  <Point X="1.517468505859" Y="22.336125" />
                  <Point X="1.502549926758" Y="22.34111328125" />
                  <Point X="1.470931884766" Y="22.34884375" />
                  <Point X="1.455393920898" Y="22.351302734375" />
                  <Point X="1.424126098633" Y="22.35362109375" />
                  <Point X="1.408396240234" Y="22.35348046875" />
                  <Point X="1.21557019043" Y="22.335736328125" />
                  <Point X="1.175309448242" Y="22.332033203125" />
                  <Point X="1.161230957031" Y="22.32966015625" />
                  <Point X="1.133583251953" Y="22.322830078125" />
                  <Point X="1.120013916016" Y="22.318373046875" />
                  <Point X="1.092625976562" Y="22.307029296875" />
                  <Point X="1.079876342773" Y="22.3005859375" />
                  <Point X="1.05549206543" Y="22.28586328125" />
                  <Point X="1.043857421875" Y="22.277583984375" />
                  <Point X="0.894962097168" Y="22.15378125" />
                  <Point X="0.863873901367" Y="22.127931640625" />
                  <Point X="0.852652709961" Y="22.116908203125" />
                  <Point X="0.832183349609" Y="22.09316015625" />
                  <Point X="0.822934814453" Y="22.080435546875" />
                  <Point X="0.806043212891" Y="22.052611328125" />
                  <Point X="0.799020935059" Y="22.03853515625" />
                  <Point X="0.787395385742" Y="22.0094140625" />
                  <Point X="0.782792053223" Y="21.994369140625" />
                  <Point X="0.738272949219" Y="21.789546875" />
                  <Point X="0.728977783203" Y="21.746783203125" />
                  <Point X="0.727584899902" Y="21.738712890625" />
                  <Point X="0.72472479248" Y="21.710326171875" />
                  <Point X="0.7247421875" Y="21.67683203125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.763504516602" Y="21.37622265625" />
                  <Point X="0.833091003418" Y="20.847662109375" />
                  <Point X="0.655065124512" Y="21.512064453125" />
                  <Point X="0.652606445312" Y="21.519876953125" />
                  <Point X="0.642143859863" Y="21.546423828125" />
                  <Point X="0.626786621094" Y="21.576185546875" />
                  <Point X="0.620407592773" Y="21.586791015625" />
                  <Point X="0.484960388184" Y="21.781943359375" />
                  <Point X="0.456679992676" Y="21.82269140625" />
                  <Point X="0.446670227051" Y="21.834828125" />
                  <Point X="0.424789978027" Y="21.857279296875" />
                  <Point X="0.412919464111" Y="21.86759375" />
                  <Point X="0.386663726807" Y="21.88683984375" />
                  <Point X="0.373248748779" Y="21.89505859375" />
                  <Point X="0.345244018555" Y="21.909169921875" />
                  <Point X="0.330654144287" Y="21.9150625" />
                  <Point X="0.121057373047" Y="21.98011328125" />
                  <Point X="0.077295211792" Y="21.9936953125" />
                  <Point X="0.063377120972" Y="21.996890625" />
                  <Point X="0.035217838287" Y="22.00116015625" />
                  <Point X="0.020976644516" Y="22.002234375" />
                  <Point X="-0.008664559364" Y="22.002234375" />
                  <Point X="-0.022905754089" Y="22.00116015625" />
                  <Point X="-0.051065036774" Y="21.996890625" />
                  <Point X="-0.064983276367" Y="21.9936953125" />
                  <Point X="-0.274580047607" Y="21.92864453125" />
                  <Point X="-0.318342376709" Y="21.9150625" />
                  <Point X="-0.332930908203" Y="21.909169921875" />
                  <Point X="-0.360932800293" Y="21.895060546875" />
                  <Point X="-0.374346313477" Y="21.88684375" />
                  <Point X="-0.40060144043" Y="21.867599609375" />
                  <Point X="-0.412476409912" Y="21.85728125" />
                  <Point X="-0.43435949707" Y="21.834826171875" />
                  <Point X="-0.444367767334" Y="21.82269140625" />
                  <Point X="-0.579815246582" Y="21.627537109375" />
                  <Point X="-0.60809552002" Y="21.586791015625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.711944885254" Y="21.2538359375" />
                  <Point X="-0.985425598145" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.989245860902" Y="29.700038495002" />
                  <Point X="-1.469219279526" Y="29.486340560845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.133047846561" Y="29.190785040674" />
                  <Point X="-2.815384660143" Y="28.886989118225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64348497461" Y="29.749990713379" />
                  <Point X="-1.46415088248" Y="29.38460671014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060737644953" Y="29.118989170212" />
                  <Point X="-3.047649843718" Y="28.679587549441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362190340573" Y="29.771240707017" />
                  <Point X="-1.495660234165" Y="29.266587396457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.955570410513" Y="29.061822193281" />
                  <Point X="-2.999888247482" Y="28.596861935683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.337296024706" Y="29.678333924085" />
                  <Point X="-1.583763500164" Y="29.123370848707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.782684943218" Y="29.034805316136" />
                  <Point X="-2.952126651247" Y="28.514136321925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.312401708839" Y="29.585427141152" />
                  <Point X="-2.904365055012" Y="28.431410708167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.287507392971" Y="29.492520358219" />
                  <Point X="-2.856603458776" Y="28.348685094409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.654403289518" Y="27.993481724629" />
                  <Point X="-3.813894917849" Y="27.922471476629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.560186426549" Y="29.76594751657" />
                  <Point X="0.347738866584" Y="29.67135976875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.262613077104" Y="29.399613575286" />
                  <Point X="-2.80884184556" Y="28.265959488212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552709275992" Y="27.934768370117" />
                  <Point X="-3.937672839463" Y="27.763371548861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749274782047" Y="29.746144630038" />
                  <Point X="0.316100343027" Y="29.553282944044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.237718761237" Y="29.306706792353" />
                  <Point X="-2.767463747672" Y="28.180391757877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451015262465" Y="27.876055015605" />
                  <Point X="-4.041490747782" Y="27.61315839157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.916363192236" Y="29.716546736778" />
                  <Point X="0.28446181947" Y="29.435206119337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.206406652214" Y="29.21665739503" />
                  <Point X="-2.752192130596" Y="28.083200673412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.349321248939" Y="27.817341661093" />
                  <Point X="-4.123501258931" Y="27.472654513052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.067806861736" Y="29.67998335619" />
                  <Point X="0.252823334883" Y="29.317129311981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.145900612501" Y="29.139605973086" />
                  <Point X="-2.753041119372" Y="27.978832232797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.247144337252" Y="27.758843306694" />
                  <Point X="-4.20551145077" Y="27.332150776699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.219250531236" Y="29.643419975601" />
                  <Point X="0.202740349027" Y="29.190840483574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027853886898" Y="29.088173315073" />
                  <Point X="-2.832484559864" Y="27.839471287773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.088788540322" Y="27.725357403514" />
                  <Point X="-4.16496660428" Y="27.24621205894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.37069400193" Y="29.606856506499" />
                  <Point X="-4.079205122589" Y="27.180405084225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.511852489619" Y="29.565713867935" />
                  <Point X="-3.993443640898" Y="27.114598109511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.640563598848" Y="29.519029299424" />
                  <Point X="-3.907682159206" Y="27.048791134796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.769274708077" Y="29.472344730912" />
                  <Point X="-3.821920677515" Y="26.982984160082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.893418559083" Y="29.423626688027" />
                  <Point X="-3.736159195823" Y="26.917177185367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007331247953" Y="29.370353438274" />
                  <Point X="-3.650398136283" Y="26.851370022699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121243936824" Y="29.317080188522" />
                  <Point X="-3.564637539504" Y="26.785562653996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.618681337027" Y="26.316272119768" />
                  <Point X="-4.63623611284" Y="26.308456230012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.235155354536" Y="29.263806372813" />
                  <Point X="-3.492441762849" Y="26.713715838263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.438417365579" Y="26.292540364326" />
                  <Point X="-4.668281753098" Y="26.190198145271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338037993223" Y="29.205622228319" />
                  <Point X="-3.446413423836" Y="26.630218528671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.258153394132" Y="26.268808608884" />
                  <Point X="-4.700327393357" Y="26.071940060531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.439212883217" Y="29.146677745119" />
                  <Point X="-3.421650607729" Y="26.537253198272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077889422684" Y="26.245076853442" />
                  <Point X="-4.731866939712" Y="25.953907303314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.540387773211" Y="29.087733261919" />
                  <Point X="-3.441633189984" Y="26.424365932988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.897625451237" Y="26.221345098" />
                  <Point X="-4.748340504003" Y="25.842582353484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.640214494253" Y="29.028188535229" />
                  <Point X="-3.528835861333" Y="26.28155035581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.68504354059" Y="26.212002216139" />
                  <Point X="-4.764813861584" Y="25.731257495688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730142415728" Y="28.964236579022" />
                  <Point X="-4.781287219164" Y="25.619932637892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.815720046194" Y="28.898347748469" />
                  <Point X="-4.676447223021" Y="25.562619965084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734908322405" Y="28.75837760447" />
                  <Point X="-4.530634437318" Y="25.523549553506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.654096420343" Y="28.6184073811" />
                  <Point X="-4.384821651616" Y="25.484479141927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.573284518282" Y="28.47843715773" />
                  <Point X="-4.239008865914" Y="25.445408730349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49247261622" Y="28.33846693436" />
                  <Point X="-4.093196080211" Y="25.40633831877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411660714159" Y="28.198496710989" />
                  <Point X="-3.947383294509" Y="25.367267907191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330848812097" Y="28.058526487619" />
                  <Point X="-3.801570508806" Y="25.328197495613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.250036910036" Y="27.918556264249" />
                  <Point X="-3.65575746716" Y="25.289127197988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.169225007975" Y="27.778586040879" />
                  <Point X="-3.509944338505" Y="25.250056939101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.088413105913" Y="27.638615817509" />
                  <Point X="-3.40324232591" Y="25.193573289431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.033120723959" Y="27.510007616526" />
                  <Point X="-3.337484726869" Y="25.118860012343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012978929757" Y="27.397049465515" />
                  <Point X="-3.302069622641" Y="25.03063738618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022557870204" Y="27.297323838119" />
                  <Point X="-3.298449799901" Y="24.928258588642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337526163539" Y="24.465631985324" />
                  <Point X="-4.775751094376" Y="24.270521675498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053900510588" Y="27.207288034233" />
                  <Point X="-3.348987971785" Y="24.801767098358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.750933988585" Y="24.622809201733" />
                  <Point X="-4.761768385889" Y="24.172756731956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.107972004621" Y="27.127371767976" />
                  <Point X="-4.747785677403" Y="24.074991788415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.183171422317" Y="27.056862259395" />
                  <Point X="-4.72889206374" Y="23.979413320729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965313469419" Y="27.746332573601" />
                  <Point X="3.59446280399" Y="27.581219219386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.285116352416" Y="26.998260620139" />
                  <Point X="-4.705042021494" Y="23.886041597224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021983902562" Y="27.667573429587" />
                  <Point X="2.807378229774" Y="27.126796142723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.474349361908" Y="26.978522137714" />
                  <Point X="-4.681191872824" Y="23.792669921103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074308745599" Y="27.586879504204" />
                  <Point X="-4.611166472822" Y="23.719856791426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.123881327547" Y="27.504960193233" />
                  <Point X="-4.279539422216" Y="23.763516220722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.898715355271" Y="27.300719396963" />
                  <Point X="-3.947912371609" Y="23.807175650018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.575863363432" Y="27.052985982629" />
                  <Point X="-3.616285321003" Y="23.850835079314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.253011371593" Y="26.805252568295" />
                  <Point X="-3.284661893558" Y="23.894492895475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068586204378" Y="26.6191507471" />
                  <Point X="-3.099960928308" Y="23.87273661695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015884601919" Y="26.491696035466" />
                  <Point X="-3.012330187236" Y="23.807761890133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001589427861" Y="26.381340967456" />
                  <Point X="-2.961311041922" Y="23.726486630668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018652251041" Y="26.284947379329" />
                  <Point X="-2.949088416177" Y="23.627938047802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050414463733" Y="26.19509838107" />
                  <Point X="-2.98173556218" Y="23.509412155449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.102722901455" Y="26.11439715157" />
                  <Point X="-3.204132664478" Y="23.306404139519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.174283731006" Y="26.042267639173" />
                  <Point X="-3.526987287672" Y="23.05866955363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.279398106563" Y="25.985077127951" />
                  <Point X="-3.849841910866" Y="22.810934967742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441339740301" Y="25.953187742178" />
                  <Point X="-4.172695287903" Y="22.563200936678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.746834735339" Y="25.98521243073" />
                  <Point X="-4.133561799674" Y="22.476633841736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.078464370614" Y="26.028873010794" />
                  <Point X="-4.084557663017" Y="22.394461442616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410094005888" Y="26.072533590859" />
                  <Point X="-4.03555352636" Y="22.312289043497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707253252794" Y="26.100846965228" />
                  <Point X="-3.982943519636" Y="22.231722081166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.730093214569" Y="26.007025524923" />
                  <Point X="-3.923824926595" Y="22.154052928165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752849500905" Y="25.913166829913" />
                  <Point X="-2.573669292452" Y="22.651190499658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83956277657" Y="22.532807093293" />
                  <Point X="-3.864706887425" Y="22.076383528565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76799069109" Y="25.815917675655" />
                  <Point X="4.168651362626" Y="25.549074614389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419186670461" Y="25.215391434812" />
                  <Point X="-2.432589144438" Y="22.610012982024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.626640046734" Y="22.078387268603" />
                  <Point X="-3.805588848255" Y="21.998714128964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783131881274" Y="25.718668521397" />
                  <Point X="4.755244953761" Y="25.706252461323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.366021061509" Y="25.087730134176" />
                  <Point X="-2.358897397809" Y="22.538832215035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349246888159" Y="24.976271344571" />
                  <Point X="-2.3179135586" Y="22.453088949428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.360720118634" Y="24.877389109433" />
                  <Point X="-2.31652076262" Y="22.349718615693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.384133330095" Y="24.783822896333" />
                  <Point X="-2.373558719206" Y="22.220333234811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.434357184046" Y="24.70219355034" />
                  <Point X="-2.454370552652" Y="22.08036304199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.499034944622" Y="24.626999498192" />
                  <Point X="-2.535182386098" Y="21.94039284917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592002567243" Y="24.56440090413" />
                  <Point X="-2.615994219544" Y="21.800422656349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707633941094" Y="24.511892862232" />
                  <Point X="-2.69680605299" Y="21.660452463529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.853446817977" Y="24.472822491249" />
                  <Point X="-2.777617886435" Y="21.520482270708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.999259835181" Y="24.433752182742" />
                  <Point X="-2.858429719881" Y="21.380512077888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.145072959442" Y="24.394681921899" />
                  <Point X="3.456570596445" Y="24.08814091999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954336218178" Y="23.864531768038" />
                  <Point X="-2.939241553327" Y="21.240541885067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.290886083702" Y="24.355611661056" />
                  <Point X="3.636834023582" Y="24.064408922206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880008338016" Y="23.727448417214" />
                  <Point X="-2.939360535148" Y="21.136498464489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436699207963" Y="24.316541400214" />
                  <Point X="3.81709745072" Y="24.040676924421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867285229195" Y="23.617793277742" />
                  <Point X="-2.853785546648" Y="21.070608457656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.582512332224" Y="24.277471139371" />
                  <Point X="3.997360877857" Y="24.016944926637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859750519417" Y="23.510448162355" />
                  <Point X="-2.768210558149" Y="21.004718450823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.728325456485" Y="24.238400878528" />
                  <Point X="4.177624304995" Y="23.993212928852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880134391697" Y="23.415533200554" />
                  <Point X="-2.673433586575" Y="20.942925430816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.774588732632" Y="24.155008169687" />
                  <Point X="4.357887732132" Y="23.969480931068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927420473953" Y="23.332595874332" />
                  <Point X="-0.113894930952" Y="21.978515014998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.432622866625" Y="21.836608195227" />
                  <Point X="-1.826012590851" Y="21.216231120187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413217183552" Y="20.954790791372" />
                  <Point X="-2.575735265574" Y="20.882433079374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.755593137666" Y="24.042560339456" />
                  <Point X="4.538151159269" Y="23.945748933283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.979399013183" Y="23.251747764559" />
                  <Point X="0.12235597013" Y="21.979710246631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.540093421606" Y="21.684768774865" />
                  <Point X="-1.624428319318" Y="21.201991773922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.72917886604" Y="23.926809501569" />
                  <Point X="4.718415056089" Y="23.922017144615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.040433728467" Y="23.174931724146" />
                  <Point X="2.57719514065" Y="22.968684616708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131119894441" Y="22.770079121289" />
                  <Point X="0.259984065344" Y="21.936995776067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.632158844375" Y="21.539788161265" />
                  <Point X="-1.50367823664" Y="21.151762728026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.124772075176" Y="23.108491128914" />
                  <Point X="2.743359787457" Y="22.938675437492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056910728945" Y="22.63304862564" />
                  <Point X="1.42856670905" Y="22.35329184374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826385842299" Y="22.085183648119" />
                  <Point X="0.384237273283" Y="21.888326422025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.667391894375" Y="21.420110950276" />
                  <Point X="-1.424065960995" Y="21.083217950388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.210533332191" Y="23.042684054167" />
                  <Point X="2.871781480366" Y="22.891862012533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00498122462" Y="22.505937674242" />
                  <Point X="1.567962468324" Y="22.311364387921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774950686007" Y="21.958292794646" />
                  <Point X="0.45991518465" Y="21.818029952552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.699030279891" Y="21.302034187029" />
                  <Point X="-1.345416815348" Y="21.014244359647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296294650916" Y="22.976877006895" />
                  <Point X="2.973475788056" Y="22.833148788991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.005389921529" Y="22.402129191371" />
                  <Point X="1.663530683532" Y="22.249923652278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749926175803" Y="21.843160718409" />
                  <Point X="0.515051663122" Y="21.738587847916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.730668819487" Y="21.183957355182" />
                  <Point X="-1.266767669701" Y="20.945270768906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382056058678" Y="22.911069999265" />
                  <Point X="3.075170095747" Y="22.77443556545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022772783729" Y="22.305878093798" />
                  <Point X="1.759098237719" Y="22.188482622328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726583869197" Y="21.728777607469" />
                  <Point X="0.570188916673" Y="21.659146088368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.762307465357" Y="21.065880476018" />
                  <Point X="-1.207480216454" Y="20.867676797312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467817466439" Y="22.845262991635" />
                  <Point X="3.176864403437" Y="22.715722341908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.040155372343" Y="22.209626874415" />
                  <Point X="1.837216681928" Y="22.119272748084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730548294769" Y="21.626552236996" />
                  <Point X="0.624808703831" Y="21.579473937938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.793946111228" Y="20.947803596854" />
                  <Point X="-1.177941397595" Y="20.77683788034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5535788742" Y="22.779455984004" />
                  <Point X="3.278558711127" Y="22.657009118366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073082483374" Y="22.120296522313" />
                  <Point X="1.896692306935" Y="22.041762555955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743480740547" Y="21.52831968637" />
                  <Point X="0.66059741244" Y="21.491417651163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.825584757098" Y="20.82972671769" />
                  <Point X="-1.15893929544" Y="20.681307714843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.639340281961" Y="22.713648976374" />
                  <Point X="3.380253018818" Y="22.598295894824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.120844318467" Y="22.037571014901" />
                  <Point X="1.956167931941" Y="21.964252363827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.756413186325" Y="21.430087135743" />
                  <Point X="0.685491687805" Y="21.398510850197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857223402969" Y="20.711649838526" />
                  <Point X="-1.139937272522" Y="20.585777514067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725101689722" Y="22.647841968744" />
                  <Point X="3.481947326508" Y="22.539582671283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168606153561" Y="21.95484550749" />
                  <Point X="2.015643556948" Y="21.886742171698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769345694642" Y="21.331854612961" />
                  <Point X="0.71038596317" Y="21.305604049231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.888862048839" Y="20.593572959363" />
                  <Point X="-1.121703618446" Y="20.489905213441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.810863097483" Y="22.582034961113" />
                  <Point X="3.583641634199" Y="22.480869447741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216367938812" Y="21.872119977887" />
                  <Point X="2.075119181954" Y="21.80923197957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782278278884" Y="21.233622123983" />
                  <Point X="0.735280238534" Y="21.212697248266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.92050069471" Y="20.475496080199" />
                  <Point X="-1.126066577237" Y="20.383972252577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896624505245" Y="22.516227953483" />
                  <Point X="3.685335941889" Y="22.422156224199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.264129589491" Y="21.789394388369" />
                  <Point X="2.134594806961" Y="21.731721787441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795210863125" Y="21.135389635004" />
                  <Point X="0.760174513899" Y="21.1197904473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.95213934058" Y="20.357419201035" />
                  <Point X="-1.140609932268" Y="20.273506687278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.982385913006" Y="22.450420945852" />
                  <Point X="3.787030249579" Y="22.363443000658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.311891240171" Y="21.706668798851" />
                  <Point X="2.194070431967" Y="21.654211595313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808143447367" Y="21.037157146025" />
                  <Point X="0.785068789264" Y="21.026883646334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.983777986451" Y="20.239342321871" />
                  <Point X="-0.993896105229" Y="20.234837445149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068147320767" Y="22.384613938222" />
                  <Point X="3.88872455727" Y="22.304729777116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359652890851" Y="21.623943209334" />
                  <Point X="2.253546056974" Y="21.576701403184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.821076031608" Y="20.938924657047" />
                  <Point X="0.809963064629" Y="20.933976845369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.012656012229" Y="22.255917169418" />
                  <Point X="3.990418816295" Y="22.246016531907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.407414541531" Y="21.541217619816" />
                  <Point X="2.31302168198" Y="21.499191211056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.455176192211" Y="21.458492030298" />
                  <Point X="2.372497306987" Y="21.421681018927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.50293784289" Y="21.37576644078" />
                  <Point X="2.431972931993" Y="21.344170826799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.55069949357" Y="21.293040851263" />
                  <Point X="2.491448557" Y="21.26666063467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.59846114425" Y="21.210315261745" />
                  <Point X="2.550924182006" Y="21.189150442541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64622279493" Y="21.127589672227" />
                  <Point X="2.610399807012" Y="21.111640250413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69398444561" Y="21.044864082709" />
                  <Point X="2.669875432019" Y="21.034130058284" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.802928649902" Y="20.226125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464319122314" Y="21.478455078125" />
                  <Point X="0.328871887207" Y="21.673607421875" />
                  <Point X="0.300591491699" Y="21.71435546875" />
                  <Point X="0.274335601807" Y="21.7336015625" />
                  <Point X="0.064738815308" Y="21.79865234375" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.218261398315" Y="21.74718359375" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278839111" Y="21.714357421875" />
                  <Point X="-0.423726257324" Y="21.519203125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.528418762207" Y="21.20466015625" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.049944580078" Y="20.052169921875" />
                  <Point X="-1.10025769043" Y="20.0619375" />
                  <Point X="-1.343407836914" Y="20.12449609375" />
                  <Point X="-1.349041748047" Y="20.14595703125" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.359755737305" Y="20.71697265625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.579252807617" Y="20.9666015625" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649241577148" Y="21.014236328125" />
                  <Point X="-1.905356079102" Y="21.0310234375" />
                  <Point X="-1.958830810547" Y="21.03452734375" />
                  <Point X="-1.98987890625" Y="21.02620703125" />
                  <Point X="-2.203287353516" Y="20.88361328125" />
                  <Point X="-2.247845458984" Y="20.85383984375" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.298579833984" Y="20.792080078125" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.782108642578" Y="20.786740234375" />
                  <Point X="-2.855836914062" Y="20.832390625" />
                  <Point X="-3.192542480469" Y="21.091642578125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.121883789062" Y="21.304197265625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513982421875" Y="22.43123828125" />
                  <Point X="-2.528334472656" Y="22.44558984375" />
                  <Point X="-2.531331054688" Y="22.4485859375" />
                  <Point X="-2.560159179688" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.814399414062" Y="22.327943359375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.103456542969" Y="22.076345703125" />
                  <Point X="-4.161699707031" Y="22.15286328125" />
                  <Point X="-4.403097167969" Y="22.557650390625" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.241324707031" Y="22.75003125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.139447998047" Y="23.628595703125" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161158691406" Y="23.689359375" />
                  <Point X="-3.183067138672" Y="23.70225390625" />
                  <Point X="-3.187647216797" Y="23.70494921875" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.500686767578" Y="23.6744140625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.904611816406" Y="23.89962109375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.991259765625" Y="24.435357421875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.784941894531" Y="24.542451171875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541895019531" Y="24.87857421875" />
                  <Point X="-3.518935791016" Y="24.894509765625" />
                  <Point X="-3.514148193359" Y="24.89783203125" />
                  <Point X="-3.494898925781" Y="24.924091796875" />
                  <Point X="-3.487245849609" Y="24.94875" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.485647949219" Y="24.9835390625" />
                  <Point X="-3.493299804688" Y="25.008193359375" />
                  <Point X="-3.494897460938" Y="25.01334375" />
                  <Point X="-3.51414453125" Y="25.039603515625" />
                  <Point X="-3.537096923828" Y="25.055533203125" />
                  <Point X="-3.537096191406" Y="25.055533203125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.813752441406" Y="25.1347578125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.932326660156" Y="25.897203125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.789083496094" Y="26.47084765625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.63315234375" Y="26.509818359375" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731703369141" Y="26.3958671875" />
                  <Point X="-3.680887451172" Y="26.411888671875" />
                  <Point X="-3.67027734375" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.426056640625" />
                  <Point X="-3.639120361328" Y="26.44378515625" />
                  <Point X="-3.618730224609" Y="26.49301171875" />
                  <Point X="-3.614472900391" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316650391" Y="26.545513671875" />
                  <Point X="-3.640919433594" Y="26.592775390625" />
                  <Point X="-3.646056396484" Y="26.602642578125" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.806976074219" Y="26.732025390625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.217061523437" Y="27.689271484375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.819458984375" Y="28.2247421875" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.69955859375" Y="28.2389453125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515380859" Y="27.92043359375" />
                  <Point X="-3.067742675781" Y="27.9142421875" />
                  <Point X="-3.052966064453" Y="27.91294921875" />
                  <Point X="-3.031507568359" Y="27.915775390625" />
                  <Point X="-3.013253173828" Y="27.927404296875" />
                  <Point X="-2.963018310547" Y="27.977638671875" />
                  <Point X="-2.952529541016" Y="27.988126953125" />
                  <Point X="-2.940899658203" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.944266113281" Y="28.09861328125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.0172109375" Y="28.246865234375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.852231933594" Y="29.09815625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.216511474609" Y="29.47232421875" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.133260009766" Y="29.503169921875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951248657227" Y="29.273662109375" />
                  <Point X="-1.872479125977" Y="29.23265625" />
                  <Point X="-1.856032592773" Y="29.22409375" />
                  <Point X="-1.835125" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.731764770508" Y="29.256234375" />
                  <Point X="-1.714634643555" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.659379516602" Y="29.379181640625" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.659323730469" Y="29.4746796875" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.096717163086" Y="29.867234375" />
                  <Point X="-0.968083557129" Y="29.903296875" />
                  <Point X="-0.317856201172" Y="29.9793984375" />
                  <Point X="-0.22420022583" Y="29.990359375" />
                  <Point X="-0.202006088257" Y="29.907529296875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282114029" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594032288" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.087831130981" Y="29.435474609375" />
                  <Point X="0.236648406982" Y="29.990869140625" />
                  <Point X="0.747897216797" Y="29.937326171875" />
                  <Point X="0.860209655762" Y="29.925564453125" />
                  <Point X="1.398163574219" Y="29.795685546875" />
                  <Point X="1.508457275391" Y="29.769056640625" />
                  <Point X="1.859333129883" Y="29.641791015625" />
                  <Point X="1.931042724609" Y="29.61578125" />
                  <Point X="2.269619384766" Y="29.45744140625" />
                  <Point X="2.338688964844" Y="29.425138671875" />
                  <Point X="2.665784423828" Y="29.234572265625" />
                  <Point X="2.732518554688" Y="29.195693359375" />
                  <Point X="3.041010498047" Y="28.976310546875" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.942653808594" Y="28.738203125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851806641" Y="27.491513671875" />
                  <Point X="2.207090576172" Y="27.425095703125" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.208970214844" Y="27.334890625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.254219482422" Y="27.248439453125" />
                  <Point X="2.274940185547" Y="27.224203125" />
                  <Point X="2.327313476562" Y="27.1886640625" />
                  <Point X="2.338248779297" Y="27.181244140625" />
                  <Point X="2.360338378906" Y="27.172978515625" />
                  <Point X="2.417764892578" Y="27.1660546875" />
                  <Point X="2.448666259766" Y="27.1659453125" />
                  <Point X="2.515084960938" Y="27.18370703125" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.798810791016" Y="27.341244140625" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.163003417969" Y="27.7968984375" />
                  <Point X="4.20259375" Y="27.741876953125" />
                  <Point X="4.374568359375" Y="27.4576875" />
                  <Point X="4.387513183594" Y="27.436296875" />
                  <Point X="4.227084472656" Y="27.3131953125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279372558594" Y="26.583833984375" />
                  <Point X="3.231571044922" Y="26.52147265625" />
                  <Point X="3.213119140625" Y="26.491498046875" />
                  <Point X="3.195312988281" Y="26.427828125" />
                  <Point X="3.191595214844" Y="26.414533203125" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.205396484375" Y="26.320123046875" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.255403076172" Y="26.22752734375" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.338560546875" Y="26.166388671875" />
                  <Point X="3.368568847656" Y="26.1536171875" />
                  <Point X="3.446465332031" Y="26.143322265625" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.720674560547" Y="26.173408203125" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.922185546875" Y="26.021224609375" />
                  <Point X="4.939189453125" Y="25.951380859375" />
                  <Point X="4.993383300781" Y="25.60330078125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.816672363281" Y="25.5260078125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729090820312" Y="25.2328203125" />
                  <Point X="3.652560058594" Y="25.188583984375" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.576346191406" Y="25.1084140625" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.541679199219" Y="24.994810546875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.553789306641" Y="24.879392578125" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.612677246094" Y="24.78273046875" />
                  <Point X="3.636576660156" Y="24.758091796875" />
                  <Point X="3.713107666016" Y="24.71385546875" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.965725341797" Y="24.63944140625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.957925292969" Y="24.096568359375" />
                  <Point X="4.948431640625" Y="24.03359765625" />
                  <Point X="4.879" Y="23.729337890625" />
                  <Point X="4.874546386719" Y="23.709822265625" />
                  <Point X="4.659265136719" Y="23.7381640625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.244633056641" Y="23.86901171875" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.185446044922" Y="23.8453046875" />
                  <Point X="3.094657714844" Y="23.73611328125" />
                  <Point X="3.075701904297" Y="23.71331640625" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.051345703125" Y="23.5445234375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.139484863281" Y="23.354083984375" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.3768515625" Y="23.1545546875" />
                  <Point X="4.339073730469" Y="22.416216796875" />
                  <Point X="4.230891601562" Y="22.241162109375" />
                  <Point X="4.204123046875" Y="22.19784375" />
                  <Point X="4.060542480469" Y="21.9938359375" />
                  <Point X="4.056688964844" Y="21.988361328125" />
                  <Point X="3.864328613281" Y="22.099419921875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340087891" Y="22.7466875" />
                  <Point X="2.558574707031" Y="22.77897265625" />
                  <Point X="2.521249755859" Y="22.785712890625" />
                  <Point X="2.489078613281" Y="22.78075390625" />
                  <Point X="2.340568359375" Y="22.70259375" />
                  <Point X="2.309560546875" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.210439941406" Y="22.516806640625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.221447998047" Y="22.274859375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.368003662109" Y="21.989478515625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.8670546875" Y="20.832470703125" />
                  <Point X="2.835303466797" Y="20.809791015625" />
                  <Point X="2.679774414062" Y="20.70912109375" />
                  <Point X="2.530876953125" Y="20.90316796875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548461914" Y="22.019533203125" />
                  <Point X="1.494237426758" Y="22.132884765625" />
                  <Point X="1.457425048828" Y="22.15655078125" />
                  <Point X="1.425806884766" Y="22.16428125" />
                  <Point X="1.232980834961" Y="22.146537109375" />
                  <Point X="1.192720092773" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.016436950684" Y="22.0076875" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.96845690918" Y="21.954013671875" />
                  <Point X="0.923937927246" Y="21.74919140625" />
                  <Point X="0.914642700195" Y="21.706427734375" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.951879089355" Y="21.4010234375" />
                  <Point X="1.127642333984" Y="20.06597265625" />
                  <Point X="1.024388549805" Y="20.043337890625" />
                  <Point X="0.994327148438" Y="20.03675" />
                  <Point X="0.860200561523" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#200" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.159865750374" Y="4.951751606804" Z="2.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="-0.329824227631" Y="5.060338377271" Z="2.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.1" />
                  <Point X="-1.116370524828" Y="4.946658738665" Z="2.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.1" />
                  <Point X="-1.715051979358" Y="4.499435428002" Z="2.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.1" />
                  <Point X="-1.713221434693" Y="4.425497245507" Z="2.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.1" />
                  <Point X="-1.757055289229" Y="4.33370850278" Z="2.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.1" />
                  <Point X="-1.855545577404" Y="4.308286160276" Z="2.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.1" />
                  <Point X="-2.099748472908" Y="4.564888257088" Z="2.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.1" />
                  <Point X="-2.246950282568" Y="4.547311601988" Z="2.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.1" />
                  <Point X="-2.88881003195" Y="4.169116134428" Z="2.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.1" />
                  <Point X="-3.066668240966" Y="3.253144849134" Z="2.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.1" />
                  <Point X="-3.000231871464" Y="3.12553611259" Z="2.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.1" />
                  <Point X="-3.004528688808" Y="3.044274879078" Z="2.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.1" />
                  <Point X="-3.069540353017" Y="2.995332827709" Z="2.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.1" />
                  <Point X="-3.680714499163" Y="3.31352570778" Z="2.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.1" />
                  <Point X="-3.865078253709" Y="3.286725188062" Z="2.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.1" />
                  <Point X="-4.266399410583" Y="2.745756538975" Z="2.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.1" />
                  <Point X="-3.843569969582" Y="1.723637068881" Z="2.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.1" />
                  <Point X="-3.691425374286" Y="1.600966196766" Z="2.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.1" />
                  <Point X="-3.671079750644" Y="1.543426307468" Z="2.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.1" />
                  <Point X="-3.702079946514" Y="1.49085478136" Z="2.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.1" />
                  <Point X="-4.632782069545" Y="1.590671759161" Z="2.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.1" />
                  <Point X="-4.843499358245" Y="1.515207096669" Z="2.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.1" />
                  <Point X="-4.987943859195" Y="0.935801743036" Z="2.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.1" />
                  <Point X="-3.832849219806" Y="0.117741325474" Z="2.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.1" />
                  <Point X="-3.571767027618" Y="0.04574192491" Z="2.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.1" />
                  <Point X="-3.547209929271" Y="0.024658446061" Z="2.1" />
                  <Point X="-3.539556741714" Y="0" Z="2.1" />
                  <Point X="-3.541154671864" Y="-0.005148505" Z="2.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.1" />
                  <Point X="-3.553601567506" Y="-0.033134058224" Z="2.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.1" />
                  <Point X="-4.804038568967" Y="-0.377970731282" Z="2.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.1" />
                  <Point X="-5.046912050183" Y="-0.540439309911" Z="2.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.1" />
                  <Point X="-4.959212401833" Y="-1.081474035436" Z="2.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.1" />
                  <Point X="-3.500316472582" Y="-1.343878399096" Z="2.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.1" />
                  <Point X="-3.214584595577" Y="-1.309555537337" Z="2.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.1" />
                  <Point X="-3.194007090596" Y="-1.32758801648" Z="2.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.1" />
                  <Point X="-4.277918525623" Y="-2.179020911138" Z="2.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.1" />
                  <Point X="-4.452196936742" Y="-2.436678025997" Z="2.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.1" />
                  <Point X="-4.149398110196" Y="-2.922657939097" Z="2.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.1" />
                  <Point X="-2.795555306281" Y="-2.684076000389" Z="2.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.1" />
                  <Point X="-2.569842916196" Y="-2.558487552935" Z="2.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.1" />
                  <Point X="-3.171341012779" Y="-3.639523076211" Z="2.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.1" />
                  <Point X="-3.229202334464" Y="-3.916693783613" Z="2.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.1" />
                  <Point X="-2.814585978039" Y="-4.224491069983" Z="2.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.1" />
                  <Point X="-2.265068097814" Y="-4.20707702098" Z="2.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.1" />
                  <Point X="-2.181664190908" Y="-4.126679374084" Z="2.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.1" />
                  <Point X="-1.914781179008" Y="-3.987589296289" Z="2.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.1" />
                  <Point X="-1.618376087668" Y="-4.039710032219" Z="2.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.1" />
                  <Point X="-1.41495117461" Y="-4.26150030873" Z="2.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.1" />
                  <Point X="-1.404770001256" Y="-4.816237867206" Z="2.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.1" />
                  <Point X="-1.362023763675" Y="-4.89264447744" Z="2.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.1" />
                  <Point X="-1.065629325632" Y="-4.965632799513" Z="2.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="-0.486278520712" Y="-3.777000188912" Z="2.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="-0.388806367091" Y="-3.47802639547" Z="2.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="-0.209596787669" Y="-3.269290476488" Z="2.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.1" />
                  <Point X="0.043762291691" Y="-3.217821568801" Z="2.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.1" />
                  <Point X="0.281639492341" Y="-3.323619360298" Z="2.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.1" />
                  <Point X="0.748476339689" Y="-4.755535840904" Z="2.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.1" />
                  <Point X="0.848818267056" Y="-5.008104098836" Z="2.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.1" />
                  <Point X="1.028935925896" Y="-4.974222466691" Z="2.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.1" />
                  <Point X="0.995295440787" Y="-3.561169923798" Z="2.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.1" />
                  <Point X="0.966641026412" Y="-3.230148286745" Z="2.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.1" />
                  <Point X="1.042248306775" Y="-2.999477119276" Z="2.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.1" />
                  <Point X="1.231404429422" Y="-2.871970615598" Z="2.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.1" />
                  <Point X="1.461042913217" Y="-2.877893597631" Z="2.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.1" />
                  <Point X="2.485053195189" Y="-4.09598827818" Z="2.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.1" />
                  <Point X="2.695767889446" Y="-4.304823524569" Z="2.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.1" />
                  <Point X="2.889960520187" Y="-4.176936482978" Z="2.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.1" />
                  <Point X="2.405148977849" Y="-2.95424105368" Z="2.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.1" />
                  <Point X="2.26449603794" Y="-2.684973680299" Z="2.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.1" />
                  <Point X="2.24853032776" Y="-2.475200339007" Z="2.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.1" />
                  <Point X="2.357698039365" Y="-2.310370982207" Z="2.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.1" />
                  <Point X="2.543533123214" Y="-2.238951971551" Z="2.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.1" />
                  <Point X="3.833172543427" Y="-2.912600522426" Z="2.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.1" />
                  <Point X="4.095274599029" Y="-3.00365993222" Z="2.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.1" />
                  <Point X="4.267270053721" Y="-2.753843219999" Z="2.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.1" />
                  <Point X="3.401133638641" Y="-1.774496946131" Z="2.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.1" />
                  <Point X="3.175387026611" Y="-1.587597225949" Z="2.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.1" />
                  <Point X="3.094979341901" Y="-1.428777997715" Z="2.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.1" />
                  <Point X="3.126947367706" Y="-1.264574155744" Z="2.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.1" />
                  <Point X="3.24909683337" Y="-1.148567673675" Z="2.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.1" />
                  <Point X="4.646583042869" Y="-1.280128358289" Z="2.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.1" />
                  <Point X="4.921590478513" Y="-1.250505856123" Z="2.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.1" />
                  <Point X="5.001210792939" Y="-0.879604490234" Z="2.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.1" />
                  <Point X="3.972510315267" Y="-0.280980665871" Z="2.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.1" />
                  <Point X="3.731973604075" Y="-0.211574433211" Z="2.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.1" />
                  <Point X="3.64585525734" Y="-0.155121533692" Z="2.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.1" />
                  <Point X="3.596740904592" Y="-0.079923155111" Z="2.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.1" />
                  <Point X="3.584630545832" Y="0.016687376075" Z="2.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.1" />
                  <Point X="3.609524181061" Y="0.108827204883" Z="2.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.1" />
                  <Point X="3.671421810277" Y="0.176574411031" Z="2.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.1" />
                  <Point X="4.823457502039" Y="0.508991266898" Z="2.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.1" />
                  <Point X="5.036631934708" Y="0.642273621697" Z="2.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.1" />
                  <Point X="4.964610768197" Y="1.064334012829" Z="2.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.1" />
                  <Point X="3.707992163952" Y="1.254261938854" Z="2.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.1" />
                  <Point X="3.446857176896" Y="1.224173600226" Z="2.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.1" />
                  <Point X="3.356931460768" Y="1.241239953466" Z="2.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.1" />
                  <Point X="3.291017650405" Y="1.286288036579" Z="2.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.1" />
                  <Point X="3.248209065412" Y="1.361507615027" Z="2.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.1" />
                  <Point X="3.237309829841" Y="1.445643154462" Z="2.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.1" />
                  <Point X="3.265096604377" Y="1.522334205912" Z="2.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.1" />
                  <Point X="4.251366486379" Y="2.304807150941" Z="2.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.1" />
                  <Point X="4.411189563921" Y="2.514853837751" Z="2.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.1" />
                  <Point X="4.197433420696" Y="2.857381420894" Z="2.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.1" />
                  <Point X="2.767654791231" Y="2.415826305272" Z="2.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.1" />
                  <Point X="2.496010207451" Y="2.263290360813" Z="2.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.1" />
                  <Point X="2.417600014005" Y="2.246975144688" Z="2.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.1" />
                  <Point X="2.349231579087" Y="2.26132051302" Z="2.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.1" />
                  <Point X="2.289438087215" Y="2.307793281295" Z="2.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.1" />
                  <Point X="2.252454558071" Y="2.372158429828" Z="2.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.1" />
                  <Point X="2.249237581844" Y="2.443459368431" Z="2.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.1" />
                  <Point X="2.979799047587" Y="3.744484290724" Z="2.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.1" />
                  <Point X="3.063831221654" Y="4.048340044514" Z="2.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.1" />
                  <Point X="2.684797714503" Y="4.309056853089" Z="2.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.1" />
                  <Point X="2.284645027228" Y="4.534012769105" Z="2.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.1" />
                  <Point X="1.870225516819" Y="4.72007632665" Z="2.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.1" />
                  <Point X="1.403742345732" Y="4.875569545869" Z="2.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.1" />
                  <Point X="0.746949153218" Y="5.018335373451" Z="2.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.1" />
                  <Point X="0.033378811179" Y="4.479695899146" Z="2.1" />
                  <Point X="0" Y="4.355124473572" Z="2.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>