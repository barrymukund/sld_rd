<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#215" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3553" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563301513672" Y="21.487478515625" />
                  <Point X="0.55772277832" Y="21.50285546875" />
                  <Point X="0.542365356445" Y="21.53262109375" />
                  <Point X="0.384357025146" Y="21.760279296875" />
                  <Point X="0.37863772583" Y="21.768521484375" />
                  <Point X="0.356758209229" Y="21.79097265625" />
                  <Point X="0.33050012207" Y="21.810220703125" />
                  <Point X="0.302494354248" Y="21.82433203125" />
                  <Point X="0.057995937347" Y="21.900212890625" />
                  <Point X="0.049151611328" Y="21.9029609375" />
                  <Point X="0.020988721848" Y="21.907232421875" />
                  <Point X="-0.008654577255" Y="21.907234375" />
                  <Point X="-0.036820350647" Y="21.90296484375" />
                  <Point X="-0.281328887939" Y="21.827078125" />
                  <Point X="-0.290179473877" Y="21.82433203125" />
                  <Point X="-0.318176757812" Y="21.8102265625" />
                  <Point X="-0.344435150146" Y="21.790982421875" />
                  <Point X="-0.366322692871" Y="21.7685234375" />
                  <Point X="-0.524330871582" Y="21.54086328125" />
                  <Point X="-0.529754089355" Y="21.532064453125" />
                  <Point X="-0.542694641113" Y="21.508255859375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.564983703613" Y="21.435251953125" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-1.073142456055" Y="20.153447265625" />
                  <Point X="-1.079356079102" Y="20.154654296875" />
                  <Point X="-1.24641796875" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.274921630859" Y="20.777435546875" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287936401367" Y="20.817029296875" />
                  <Point X="-1.304007202148" Y="20.8448671875" />
                  <Point X="-1.323643554688" Y="20.868796875" />
                  <Point X="-1.548755859375" Y="21.06621484375" />
                  <Point X="-1.556904418945" Y="21.073361328125" />
                  <Point X="-1.583188354492" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643026977539" Y="21.109033203125" />
                  <Point X="-1.941801147461" Y="21.128615234375" />
                  <Point X="-1.952615844727" Y="21.12932421875" />
                  <Point X="-1.983412963867" Y="21.126291015625" />
                  <Point X="-2.014462524414" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.291612304688" Y="20.938853515625" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.317627441406" Y="20.9183046875" />
                  <Point X="-2.337372802734" Y="20.8969296875" />
                  <Point X="-2.342958984375" Y="20.890298828125" />
                  <Point X="-2.480149414062" Y="20.7115078125" />
                  <Point X="-2.792641113281" Y="20.90499609375" />
                  <Point X="-2.801688964844" Y="20.91059765625" />
                  <Point X="-3.104721923828" Y="21.143921875" />
                  <Point X="-2.423759033203" Y="22.323384765625" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575439453" Y="22.414806640625" />
                  <Point X="-2.414559570312" Y="22.444423828125" />
                  <Point X="-2.428776855469" Y="22.47325390625" />
                  <Point X="-2.446805419922" Y="22.498412109375" />
                  <Point X="-2.463547851562" Y="22.515154296875" />
                  <Point X="-2.4641875" Y="22.51579296875" />
                  <Point X="-2.489371582031" Y="22.533828125" />
                  <Point X="-2.518194335938" Y="22.548025390625" />
                  <Point X="-2.547802001953" Y="22.556994140625" />
                  <Point X="-2.578721435547" Y="22.555974609375" />
                  <Point X="-2.610232666016" Y="22.549701171875" />
                  <Point X="-2.639184326172" Y="22.53880078125" />
                  <Point X="-2.684226806641" Y="22.512794921875" />
                  <Point X="-3.818024414062" Y="21.8581953125" />
                  <Point X="-4.075694580078" Y="22.19672265625" />
                  <Point X="-4.082852783203" Y="22.206126953125" />
                  <Point X="-4.306142089844" Y="22.580548828125" />
                  <Point X="-3.105952148438" Y="23.501486328125" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053855957031" Y="23.58016796875" />
                  <Point X="-3.046420654297" Y="23.608876953125" />
                  <Point X="-3.046153564453" Y="23.609908203125" />
                  <Point X="-3.043348876953" Y="23.64032421875" />
                  <Point X="-3.045554199219" Y="23.671998046875" />
                  <Point X="-3.052553466797" Y="23.701748046875" />
                  <Point X="-3.068636230469" Y="23.727736328125" />
                  <Point X="-3.089469482422" Y="23.7516953125" />
                  <Point X="-3.112969482422" Y="23.77123046875" />
                  <Point X="-3.138527099609" Y="23.7862734375" />
                  <Point X="-3.139557861328" Y="23.78687890625" />
                  <Point X="-3.168825927734" Y="23.798083984375" />
                  <Point X="-3.200662597656" Y="23.80453515625" />
                  <Point X="-3.231926513672" Y="23.805615234375" />
                  <Point X="-3.288788574219" Y="23.798130859375" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.831273925781" Y="23.996369140625" />
                  <Point X="-4.834076660156" Y="24.007341796875" />
                  <Point X="-4.892424316406" Y="24.41530078125" />
                  <Point X="-3.532866943359" Y="24.77959375" />
                  <Point X="-3.517493408203" Y="24.785171875" />
                  <Point X="-3.4877265625" Y="24.80053125" />
                  <Point X="-3.460956054688" Y="24.819111328125" />
                  <Point X="-3.460015380859" Y="24.819763671875" />
                  <Point X="-3.437565917969" Y="24.8416328125" />
                  <Point X="-3.418292236328" Y="24.867912109375" />
                  <Point X="-3.404167236328" Y="24.895935546875" />
                  <Point X="-3.395239257812" Y="24.924701171875" />
                  <Point X="-3.394916259766" Y="24.9257421875" />
                  <Point X="-3.390646728516" Y="24.95390625" />
                  <Point X="-3.390647949219" Y="24.98354296875" />
                  <Point X="-3.394917236328" Y="25.01169921875" />
                  <Point X="-3.403844970703" Y="25.04046484375" />
                  <Point X="-3.404156005859" Y="25.041466796875" />
                  <Point X="-3.418260498047" Y="25.069474609375" />
                  <Point X="-3.43751171875" Y="25.09575" />
                  <Point X="-3.459979248047" Y="25.1176484375" />
                  <Point X="-3.486734130859" Y="25.136216796875" />
                  <Point X="-3.4999453125" Y="25.143890625" />
                  <Point X="-3.532875732422" Y="25.157849609375" />
                  <Point X="-3.584708251953" Y="25.17173828125" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.826293457031" Y="25.9647734375" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.703551269531" Y="26.423265625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723424316406" Y="26.301228515625" />
                  <Point X="-3.703140869141" Y="26.305263671875" />
                  <Point X="-3.643860595703" Y="26.323953125" />
                  <Point X="-3.64164453125" Y="26.32465234375" />
                  <Point X="-3.622726318359" Y="26.332986328125" />
                  <Point X="-3.603996582031" Y="26.34380859375" />
                  <Point X="-3.587328125" Y="26.356037109375" />
                  <Point X="-3.573699707031" Y="26.371583984375" />
                  <Point X="-3.561294433594" Y="26.3893046875" />
                  <Point X="-3.551351318359" Y="26.4074296875" />
                  <Point X="-3.527567382813" Y="26.464849609375" />
                  <Point X="-3.526710693359" Y="26.46691796875" />
                  <Point X="-3.520919677734" Y="26.48677734375" />
                  <Point X="-3.517158691406" Y="26.508095703125" />
                  <Point X="-3.515803955078" Y="26.528740234375" />
                  <Point X="-3.518949951172" Y="26.549185546875" />
                  <Point X="-3.524552001953" Y="26.570095703125" />
                  <Point X="-3.532049560547" Y="26.589376953125" />
                  <Point X="-3.560750488281" Y="26.64451171875" />
                  <Point X="-3.561821777344" Y="26.64656640625" />
                  <Point X="-3.573295410156" Y="26.663724609375" />
                  <Point X="-3.587201171875" Y="26.68029296875" />
                  <Point X="-3.602134521484" Y="26.694587890625" />
                  <Point X="-3.631865722656" Y="26.71740234375" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.088166992188" Y="27.721642578125" />
                  <Point X="-4.081149414062" Y="27.733666015625" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187726806641" Y="27.836341796875" />
                  <Point X="-3.167088867188" Y="27.829833984375" />
                  <Point X="-3.146798339844" Y="27.825796875" />
                  <Point X="-3.064237548828" Y="27.81857421875" />
                  <Point X="-3.061249023438" Y="27.8183125" />
                  <Point X="-3.040582519531" Y="27.81876171875" />
                  <Point X="-3.019122802734" Y="27.821583984375" />
                  <Point X="-2.999026367188" Y="27.8265" />
                  <Point X="-2.980470214844" Y="27.8356484375" />
                  <Point X="-2.962214599609" Y="27.84727734375" />
                  <Point X="-2.946079589844" Y="27.8602265625" />
                  <Point X="-2.887477294922" Y="27.918828125" />
                  <Point X="-2.885355957031" Y="27.92094921875" />
                  <Point X="-2.872411376953" Y="27.937078125" />
                  <Point X="-2.860781005859" Y="27.95533203125" />
                  <Point X="-2.851631347656" Y="27.9738828125" />
                  <Point X="-2.846713623047" Y="27.993974609375" />
                  <Point X="-2.843887451172" Y="28.01543359375" />
                  <Point X="-2.843435791016" Y="28.0361171875" />
                  <Point X="-2.850658935547" Y="28.118677734375" />
                  <Point X="-2.850920410156" Y="28.12166796875" />
                  <Point X="-2.854954833984" Y="28.141953125" />
                  <Point X="-2.861463378906" Y="28.16259765625" />
                  <Point X="-2.869795898438" Y="28.18153515625" />
                  <Point X="-2.882970703125" Y="28.204353515625" />
                  <Point X="-3.183332519531" Y="28.72459375" />
                  <Point X="-2.712835693359" Y="29.0853203125" />
                  <Point X="-2.700616699219" Y="29.094689453125" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028891723633" Y="29.21480078125" />
                  <Point X="-2.012311889648" Y="29.200888671875" />
                  <Point X="-1.995114501953" Y="29.189396484375" />
                  <Point X="-1.903233520508" Y="29.141564453125" />
                  <Point X="-1.899928710938" Y="29.13984375" />
                  <Point X="-1.880637939453" Y="29.132337890625" />
                  <Point X="-1.859727050781" Y="29.126732421875" />
                  <Point X="-1.839281738281" Y="29.12358203125" />
                  <Point X="-1.818639404297" Y="29.12493359375" />
                  <Point X="-1.797318969727" Y="29.12869140625" />
                  <Point X="-1.777453613281" Y="29.134482421875" />
                  <Point X="-1.681744140625" Y="29.174126953125" />
                  <Point X="-1.678273803711" Y="29.175564453125" />
                  <Point X="-1.660131835938" Y="29.185517578125" />
                  <Point X="-1.64240637207" Y="29.197931640625" />
                  <Point X="-1.626859619141" Y="29.211568359375" />
                  <Point X="-1.614631835938" Y="29.22824609375" />
                  <Point X="-1.603811889648" Y="29.246986328125" />
                  <Point X="-1.59548059082" Y="29.265919921875" />
                  <Point X="-1.564328857422" Y="29.364720703125" />
                  <Point X="-1.563201416016" Y="29.368296875" />
                  <Point X="-1.559166259766" Y="29.38858203125" />
                  <Point X="-1.557279296875" Y="29.41014453125" />
                  <Point X="-1.55773046875" Y="29.430826171875" />
                  <Point X="-1.559228393555" Y="29.442203125" />
                  <Point X="-1.584202026367" Y="29.631896484375" />
                  <Point X="-0.965442443848" Y="29.805375" />
                  <Point X="-0.949620666504" Y="29.8098125" />
                  <Point X="-0.294711425781" Y="29.886458984375" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082114006042" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155911922" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.152965774536" Y="29.3115078125" />
                  <Point X="0.307419586182" Y="29.8879375" />
                  <Point X="0.830229675293" Y="29.833185546875" />
                  <Point X="0.844029968262" Y="29.831740234375" />
                  <Point X="1.466010742188" Y="29.68157421875" />
                  <Point X="1.481038452148" Y="29.6779453125" />
                  <Point X="1.885819580078" Y="29.53112890625" />
                  <Point X="1.894645263672" Y="29.527927734375" />
                  <Point X="2.286109375" Y="29.344853515625" />
                  <Point X="2.294568115234" Y="29.3408984375" />
                  <Point X="2.672769042969" Y="29.120556640625" />
                  <Point X="2.680978027344" Y="29.115775390625" />
                  <Point X="2.943259765625" Y="28.92925390625" />
                  <Point X="2.147581298828" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.112357177734" Y="27.438572265625" />
                  <Point X="2.109568603516" Y="27.423126953125" />
                  <Point X="2.107480957031" Y="27.401421875" />
                  <Point X="2.107727783203" Y="27.380953125" />
                  <Point X="2.115806884766" Y="27.313953125" />
                  <Point X="2.116099121094" Y="27.311529296875" />
                  <Point X="2.121441162109" Y="27.289609375" />
                  <Point X="2.12970703125" Y="27.26751953125" />
                  <Point X="2.140070556641" Y="27.24747265625" />
                  <Point X="2.181527587891" Y="27.186375" />
                  <Point X="2.191407714844" Y="27.1741328125" />
                  <Point X="2.206208740234" Y="27.15862109375" />
                  <Point X="2.221598632812" Y="27.145591796875" />
                  <Point X="2.282695556641" Y="27.104134765625" />
                  <Point X="2.284891357422" Y="27.10264453125" />
                  <Point X="2.304918457031" Y="27.0922890625" />
                  <Point X="2.3270234375" Y="27.08401171875" />
                  <Point X="2.348965820312" Y="27.078662109375" />
                  <Point X="2.415965576172" Y="27.070583984375" />
                  <Point X="2.431986328125" Y="27.070013671875" />
                  <Point X="2.453313232422" Y="27.07105859375" />
                  <Point X="2.473207519531" Y="27.074169921875" />
                  <Point X="2.550689208984" Y="27.094890625" />
                  <Point X="2.5603828125" Y="27.098048828125" />
                  <Point X="2.588534179688" Y="27.110146484375" />
                  <Point X="2.640667724609" Y="27.14024609375" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="4.118401367188" Y="27.69623046875" />
                  <Point X="4.123276855469" Y="27.689455078125" />
                  <Point X="4.26219921875" Y="27.459884765625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.148213134766" Y="26.5688828125" />
                  <Point X="3.139863525391" Y="26.5559375" />
                  <Point X="3.129371582031" Y="26.536345703125" />
                  <Point X="3.121629150391" Y="26.51708203125" />
                  <Point X="3.100857177734" Y="26.442806640625" />
                  <Point X="3.100105224609" Y="26.4401171875" />
                  <Point X="3.096651611328" Y="26.417818359375" />
                  <Point X="3.095835693359" Y="26.39425390625" />
                  <Point X="3.097738769531" Y="26.37176953125" />
                  <Point X="3.114790527344" Y="26.28912890625" />
                  <Point X="3.119134765625" Y="26.274294921875" />
                  <Point X="3.126950683594" Y="26.253923828125" />
                  <Point X="3.136282226562" Y="26.235740234375" />
                  <Point X="3.182660888672" Y="26.16524609375" />
                  <Point X="3.184345703125" Y="26.162685546875" />
                  <Point X="3.198907226562" Y="26.14543359375" />
                  <Point X="3.21614453125" Y="26.129353515625" />
                  <Point X="3.234345458984" Y="26.11603515625" />
                  <Point X="3.3015546875" Y="26.078201171875" />
                  <Point X="3.315900634766" Y="26.071630859375" />
                  <Point X="3.336309570312" Y="26.064263671875" />
                  <Point X="3.356118164062" Y="26.0594375" />
                  <Point X="3.446989501953" Y="26.047427734375" />
                  <Point X="3.456898193359" Y="26.046642578125" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.5377265625" Y="26.05350390625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.843844238281" Y="25.941400390625" />
                  <Point X="4.845935546875" Y="25.932810546875" />
                  <Point X="4.890864746094" Y="25.64423828125" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788085938" Y="25.325583984375" />
                  <Point X="3.681546875" Y="25.31506640625" />
                  <Point X="3.592268554688" Y="25.263462890625" />
                  <Point X="3.579820800781" Y="25.25487890625" />
                  <Point X="3.562276611328" Y="25.240591796875" />
                  <Point X="3.547530517578" Y="25.225578125" />
                  <Point X="3.493963623047" Y="25.1573203125" />
                  <Point X="3.492024658203" Y="25.154849609375" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470527587891" Y="25.11410546875" />
                  <Point X="3.463681152344" Y="25.09260546875" />
                  <Point X="3.445825439453" Y="24.999369140625" />
                  <Point X="3.444170166016" Y="24.984267578125" />
                  <Point X="3.443523681641" Y="24.962080078125" />
                  <Point X="3.445179199219" Y="24.941443359375" />
                  <Point X="3.463034912109" Y="24.848208984375" />
                  <Point X="3.463681396484" Y="24.844833984375" />
                  <Point X="3.470527587891" Y="24.823333984375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492024658203" Y="24.78258984375" />
                  <Point X="3.545591552734" Y="24.71433203125" />
                  <Point X="3.556147460938" Y="24.702939453125" />
                  <Point X="3.572398925781" Y="24.688048828125" />
                  <Point X="3.589036865234" Y="24.67584375" />
                  <Point X="3.678315185547" Y="24.624240234375" />
                  <Point X="3.686934326172" Y="24.619828125" />
                  <Point X="3.716581787109" Y="24.60784765625" />
                  <Point X="3.761997070312" Y="24.5956796875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.856190917969" Y="24.0590234375" />
                  <Point X="4.855021484375" Y="24.051265625" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="3.424375244141" Y="23.99655859375" />
                  <Point X="3.408036376953" Y="23.9972890625" />
                  <Point X="3.374656738281" Y="23.994490234375" />
                  <Point X="3.199435058594" Y="23.95640625" />
                  <Point X="3.193092529297" Y="23.95502734375" />
                  <Point X="3.163968505859" Y="23.943400390625" />
                  <Point X="3.136144042969" Y="23.9265078125" />
                  <Point X="3.112396728516" Y="23.9060390625" />
                  <Point X="3.006494140625" Y="23.778669921875" />
                  <Point X="3.002665283203" Y="23.77406640625" />
                  <Point X="2.987940673828" Y="23.74968359375" />
                  <Point X="2.976592285156" Y="23.72229296875" />
                  <Point X="2.969757324219" Y="23.694634765625" />
                  <Point X="2.954577880859" Y="23.52967578125" />
                  <Point X="2.954028320312" Y="23.523703125" />
                  <Point X="2.956347900391" Y="23.492431640625" />
                  <Point X="2.964081054688" Y="23.46080859375" />
                  <Point X="2.976451904297" Y="23.432" />
                  <Point X="3.073422119141" Y="23.281169921875" />
                  <Point X="3.080464111328" Y="23.271591796875" />
                  <Point X="3.095592773438" Y="23.253505859375" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.152773925781" Y="23.20675" />
                  <Point X="4.213122070312" Y="22.3931171875" />
                  <Point X="4.128102050781" Y="22.255541015625" />
                  <Point X="4.124807617188" Y="22.250208984375" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="2.800946533203" Y="22.823060546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.545682617188" Y="22.8778359375" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506784912109" Y="22.879603515625" />
                  <Point X="2.474609863281" Y="22.874646484375" />
                  <Point X="2.444831298828" Y="22.864822265625" />
                  <Point X="2.271584228516" Y="22.77364453125" />
                  <Point X="2.265313232422" Y="22.77034375" />
                  <Point X="2.242378417969" Y="22.753447265625" />
                  <Point X="2.221419921875" Y="22.732486328125" />
                  <Point X="2.204531005859" Y="22.70955859375" />
                  <Point X="2.113352294922" Y="22.5363125" />
                  <Point X="2.110052001953" Y="22.530041015625" />
                  <Point X="2.100227783203" Y="22.500263671875" />
                  <Point X="2.095270996094" Y="22.468091796875" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.133337890625" Y="22.22819921875" />
                  <Point X="2.136010742188" Y="22.2171953125" />
                  <Point X="2.143276855469" Y="22.193533203125" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.178902099609" Y="22.12701171875" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.785758544922" Y="20.8911484375" />
                  <Point X="2.781849365234" Y="20.88835546875" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="1.758547363281" Y="22.0657421875" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721924194336" Y="22.099443359375" />
                  <Point X="1.516245605469" Y="22.23167578125" />
                  <Point X="1.508776489258" Y="22.236474609375" />
                  <Point X="1.479979370117" Y="22.2488359375" />
                  <Point X="1.448363525391" Y="22.256564453125" />
                  <Point X="1.417099609375" Y="22.258880859375" />
                  <Point X="1.192199584961" Y="22.238185546875" />
                  <Point X="1.184057006836" Y="22.237439453125" />
                  <Point X="1.156370117188" Y="22.230603515625" />
                  <Point X="1.128974853516" Y="22.219255859375" />
                  <Point X="1.104592895508" Y="22.20453515625" />
                  <Point X="0.930896606445" Y="22.060111328125" />
                  <Point X="0.92460925293" Y="22.0548828125" />
                  <Point X="0.904138366699" Y="22.0311328125" />
                  <Point X="0.887248779297" Y="22.003310546875" />
                  <Point X="0.875624389648" Y="21.97419140625" />
                  <Point X="0.823690063477" Y="21.73525390625" />
                  <Point X="0.821998962402" Y="21.724578125" />
                  <Point X="0.819405944824" Y="21.698783203125" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.827417297363" Y="21.61858203125" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.979373596191" Y="20.1307265625" />
                  <Point X="0.975706604004" Y="20.129921875" />
                  <Point X="0.929315612793" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.055040405273" Y="20.24670703125" />
                  <Point X="-1.058427124023" Y="20.24736328125" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759155273" Y="20.490669921875" />
                  <Point X="-1.123333984375" Y="20.502306640625" />
                  <Point X="-1.181747070312" Y="20.79596875" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199024902344" Y="20.8504921875" />
                  <Point X="-1.205662231445" Y="20.864525390625" />
                  <Point X="-1.221733032227" Y="20.89236328125" />
                  <Point X="-1.230567871094" Y="20.905130859375" />
                  <Point X="-1.250204223633" Y="20.929060546875" />
                  <Point X="-1.261005737305" Y="20.94022265625" />
                  <Point X="-1.486118041992" Y="21.137640625" />
                  <Point X="-1.506739379883" Y="21.154037109375" />
                  <Point X="-1.53302331543" Y="21.170380859375" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576532226563" Y="21.189775390625" />
                  <Point X="-1.591315551758" Y="21.194525390625" />
                  <Point X="-1.621456176758" Y="21.201552734375" />
                  <Point X="-1.636813842773" Y="21.203830078125" />
                  <Point X="-1.935588012695" Y="21.223412109375" />
                  <Point X="-1.961927368164" Y="21.2238671875" />
                  <Point X="-1.992724609375" Y="21.220833984375" />
                  <Point X="-2.007997070312" Y="21.2180546875" />
                  <Point X="-2.039046630859" Y="21.209736328125" />
                  <Point X="-2.053665771484" Y="21.204505859375" />
                  <Point X="-2.081860839844" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.344391357422" Y="21.01784375" />
                  <Point X="-2.362333984375" Y="21.005060546875" />
                  <Point X="-2.379337402344" Y="20.990533203125" />
                  <Point X="-2.387409912109" Y="20.982767578125" />
                  <Point X="-2.407155273438" Y="20.961392578125" />
                  <Point X="-2.418327636719" Y="20.948130859375" />
                  <Point X="-2.503203369141" Y="20.837517578125" />
                  <Point X="-2.742629638672" Y="20.985765625" />
                  <Point X="-2.747556396484" Y="20.98881640625" />
                  <Point X="-2.980862304688" Y="21.168453125" />
                  <Point X="-2.341486572266" Y="22.275884765625" />
                  <Point X="-2.334846923828" Y="22.289923828125" />
                  <Point X="-2.323947021484" Y="22.31888671875" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310626220703" Y="22.411697265625" />
                  <Point X="-2.314666015625" Y="22.4423828125" />
                  <Point X="-2.323650146484" Y="22.472" />
                  <Point X="-2.329356445312" Y="22.48644140625" />
                  <Point X="-2.343573730469" Y="22.515271484375" />
                  <Point X="-2.351557128906" Y="22.52858984375" />
                  <Point X="-2.369585693359" Y="22.553748046875" />
                  <Point X="-2.379630859375" Y="22.565587890625" />
                  <Point X="-2.396373291016" Y="22.582330078125" />
                  <Point X="-2.408875488281" Y="22.593029296875" />
                  <Point X="-2.434059570312" Y="22.611064453125" />
                  <Point X="-2.447393554688" Y="22.61905078125" />
                  <Point X="-2.476216308594" Y="22.633248046875" />
                  <Point X="-2.490652832031" Y="22.6389453125" />
                  <Point X="-2.520260498047" Y="22.6479140625" />
                  <Point X="-2.550932861328" Y="22.651943359375" />
                  <Point X="-2.581852294922" Y="22.650923828125" />
                  <Point X="-2.597270507812" Y="22.649146484375" />
                  <Point X="-2.628781738281" Y="22.642873046875" />
                  <Point X="-2.643706542969" Y="22.638609375" />
                  <Point X="-2.672658203125" Y="22.627708984375" />
                  <Point X="-2.686685058594" Y="22.621072265625" />
                  <Point X="-2.731727539062" Y="22.59506640625" />
                  <Point X="-3.793089355469" Y="21.982287109375" />
                  <Point X="-4.000101074219" Y="22.254259765625" />
                  <Point X="-4.004007080078" Y="22.259390625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.048119873047" Y="23.4261171875" />
                  <Point X="-3.036477783203" Y="23.43669140625" />
                  <Point X="-3.015101806641" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833984375" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890136719" Y="23.556349609375" />
                  <Point X="-2.954454833984" Y="23.58505859375" />
                  <Point X="-2.951554931641" Y="23.601185546875" />
                  <Point X="-2.948750244141" Y="23.6316015625" />
                  <Point X="-2.948578369141" Y="23.646921875" />
                  <Point X="-2.950783691406" Y="23.678595703125" />
                  <Point X="-2.953079101562" Y="23.69375390625" />
                  <Point X="-2.960078369141" Y="23.72350390625" />
                  <Point X="-2.971770996094" Y="23.751740234375" />
                  <Point X="-2.987853759766" Y="23.777728515625" />
                  <Point X="-2.996947753906" Y="23.790072265625" />
                  <Point X="-3.017781005859" Y="23.81403125" />
                  <Point X="-3.028740478516" Y="23.82475" />
                  <Point X="-3.052240478516" Y="23.84428515625" />
                  <Point X="-3.064781005859" Y="23.8531015625" />
                  <Point X="-3.090338623047" Y="23.86814453125" />
                  <Point X="-3.105591796875" Y="23.875599609375" />
                  <Point X="-3.134859863281" Y="23.8868046875" />
                  <Point X="-3.149959228516" Y="23.89119140625" />
                  <Point X="-3.181795898438" Y="23.897642578125" />
                  <Point X="-3.197382568359" Y="23.899478515625" />
                  <Point X="-3.228646484375" Y="23.90055859375" />
                  <Point X="-3.244323730469" Y="23.899802734375" />
                  <Point X="-3.301185791016" Y="23.892318359375" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.739229003906" Y="24.019880859375" />
                  <Point X="-4.740761230469" Y="24.025880859375" />
                  <Point X="-4.786452148438" Y="24.34534375" />
                  <Point X="-3.508279052734" Y="24.687830078125" />
                  <Point X="-3.500464111328" Y="24.690291015625" />
                  <Point X="-3.473931640625" Y="24.700748046875" />
                  <Point X="-3.444164794922" Y="24.716107421875" />
                  <Point X="-3.433559814453" Y="24.722486328125" />
                  <Point X="-3.406789306641" Y="24.74106640625" />
                  <Point X="-3.393725585938" Y="24.75171484375" />
                  <Point X="-3.371276123047" Y="24.773583984375" />
                  <Point X="-3.360960449219" Y="24.78544921875" />
                  <Point X="-3.341686767578" Y="24.811728515625" />
                  <Point X="-3.333459228516" Y="24.82515234375" />
                  <Point X="-3.319334228516" Y="24.85317578125" />
                  <Point X="-3.313436767578" Y="24.867775390625" />
                  <Point X="-3.304508789062" Y="24.896541015625" />
                  <Point X="-3.300989501953" Y="24.91150390625" />
                  <Point X="-3.296719970703" Y="24.93966796875" />
                  <Point X="-3.295646728516" Y="24.95391015625" />
                  <Point X="-3.295647949219" Y="24.983546875" />
                  <Point X="-3.296721435547" Y="24.99778515625" />
                  <Point X="-3.300990722656" Y="25.02594140625" />
                  <Point X="-3.304186523438" Y="25.039859375" />
                  <Point X="-3.313114257812" Y="25.068625" />
                  <Point X="-3.319307617188" Y="25.0841953125" />
                  <Point X="-3.333412109375" Y="25.112203125" />
                  <Point X="-3.341627929688" Y="25.12562109375" />
                  <Point X="-3.360879150391" Y="25.151896484375" />
                  <Point X="-3.371203613281" Y="25.16378125" />
                  <Point X="-3.393671142578" Y="25.1856796875" />
                  <Point X="-3.405814208984" Y="25.195693359375" />
                  <Point X="-3.432569091797" Y="25.21426171875" />
                  <Point X="-3.439018066406" Y="25.21836328125" />
                  <Point X="-3.462868896484" Y="25.231357421875" />
                  <Point X="-3.495799316406" Y="25.24531640625" />
                  <Point X="-3.508287597656" Y="25.24961328125" />
                  <Point X="-3.560120117188" Y="25.263501953125" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.732316894531" Y="25.9508671875" />
                  <Point X="-4.731331542969" Y="25.957525390625" />
                  <Point X="-4.633586425781" Y="26.318234375" />
                  <Point X="-3.778065917969" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703369141" Y="26.204703125" />
                  <Point X="-3.715142822266" Y="26.20658984375" />
                  <Point X="-3.704888427734" Y="26.2080546875" />
                  <Point X="-3.684604980469" Y="26.21208984375" />
                  <Point X="-3.674575927734" Y="26.21466015625" />
                  <Point X="-3.615295654297" Y="26.233349609375" />
                  <Point X="-3.603345947266" Y="26.23771484375" />
                  <Point X="-3.584427734375" Y="26.246048828125" />
                  <Point X="-3.575197753906" Y="26.25073046875" />
                  <Point X="-3.556468017578" Y="26.261552734375" />
                  <Point X="-3.547802246094" Y="26.2672109375" />
                  <Point X="-3.531133789062" Y="26.279439453125" />
                  <Point X="-3.515890136719" Y="26.2934140625" />
                  <Point X="-3.50226171875" Y="26.3089609375" />
                  <Point X="-3.495874267578" Y="26.317103515625" />
                  <Point X="-3.483468994141" Y="26.33482421875" />
                  <Point X="-3.478004150391" Y="26.34361328125" />
                  <Point X="-3.468061035156" Y="26.36173828125" />
                  <Point X="-3.463582763672" Y="26.37107421875" />
                  <Point X="-3.439798828125" Y="26.428494140625" />
                  <Point X="-3.435509033203" Y="26.44032421875" />
                  <Point X="-3.429718017578" Y="26.46018359375" />
                  <Point X="-3.427364501953" Y="26.470271484375" />
                  <Point X="-3.423603515625" Y="26.49158984375" />
                  <Point X="-3.422362548828" Y="26.501875" />
                  <Point X="-3.4210078125" Y="26.52251953125" />
                  <Point X="-3.421908935547" Y="26.5431875" />
                  <Point X="-3.425054931641" Y="26.5636328125" />
                  <Point X="-3.427186035156" Y="26.57376953125" />
                  <Point X="-3.432788085938" Y="26.5946796875" />
                  <Point X="-3.436010498047" Y="26.604525390625" />
                  <Point X="-3.443508056641" Y="26.623806640625" />
                  <Point X="-3.447783203125" Y="26.6332421875" />
                  <Point X="-3.476484130859" Y="26.688376953125" />
                  <Point X="-3.482851074219" Y="26.699373046875" />
                  <Point X="-3.494324707031" Y="26.71653125" />
                  <Point X="-3.500528320312" Y="26.724796875" />
                  <Point X="-3.514434082031" Y="26.741365234375" />
                  <Point X="-3.521509033203" Y="26.748919921875" />
                  <Point X="-3.536442382812" Y="26.76321484375" />
                  <Point X="-3.54430078125" Y="26.769955078125" />
                  <Point X="-3.574031982422" Y="26.79276953125" />
                  <Point X="-4.227615234375" Y="27.29428125" />
                  <Point X="-4.006120849609" Y="27.67375390625" />
                  <Point X="-4.0022890625" Y="27.680318359375" />
                  <Point X="-3.726337646484" Y="28.035013671875" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244920166016" Y="27.75771875" />
                  <Point X="-3.225989990234" Y="27.749388671875" />
                  <Point X="-3.216296630859" Y="27.745740234375" />
                  <Point X="-3.195658691406" Y="27.739232421875" />
                  <Point X="-3.185627197266" Y="27.73666015625" />
                  <Point X="-3.165336669922" Y="27.732623046875" />
                  <Point X="-3.155077636719" Y="27.731158203125" />
                  <Point X="-3.072516845703" Y="27.723935546875" />
                  <Point X="-3.059184570312" Y="27.723333984375" />
                  <Point X="-3.038518066406" Y="27.723783203125" />
                  <Point X="-3.0281953125" Y="27.724572265625" />
                  <Point X="-3.006735595703" Y="27.72739453125" />
                  <Point X="-2.996549316406" Y="27.7293046875" />
                  <Point X="-2.976452880859" Y="27.734220703125" />
                  <Point X="-2.957018066406" Y="27.74129296875" />
                  <Point X="-2.938461914062" Y="27.75044140625" />
                  <Point X="-2.929430419922" Y="27.7555234375" />
                  <Point X="-2.911174804688" Y="27.76715234375" />
                  <Point X="-2.902753173828" Y="27.7731875" />
                  <Point X="-2.886618164062" Y="27.78613671875" />
                  <Point X="-2.878904785156" Y="27.79305078125" />
                  <Point X="-2.820302490234" Y="27.85165234375" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792291992188" Y="27.88603125" />
                  <Point X="-2.780661621094" Y="27.90428515625" />
                  <Point X="-2.775580810547" Y="27.91330859375" />
                  <Point X="-2.766431152344" Y="27.931859375" />
                  <Point X="-2.759355224609" Y="27.951296875" />
                  <Point X="-2.7544375" Y="27.971388671875" />
                  <Point X="-2.752526855469" Y="27.9815703125" />
                  <Point X="-2.749700683594" Y="28.003029296875" />
                  <Point X="-2.74891015625" Y="28.013359375" />
                  <Point X="-2.748458496094" Y="28.03404296875" />
                  <Point X="-2.748797363281" Y="28.044396484375" />
                  <Point X="-2.756020507812" Y="28.12695703125" />
                  <Point X="-2.757745361328" Y="28.14019921875" />
                  <Point X="-2.761779785156" Y="28.160484375" />
                  <Point X="-2.764350830078" Y="28.170517578125" />
                  <Point X="-2.770859375" Y="28.191162109375" />
                  <Point X="-2.774508544922" Y="28.200857421875" />
                  <Point X="-2.782841064453" Y="28.219794921875" />
                  <Point X="-2.787524414062" Y="28.229037109375" />
                  <Point X="-2.80069921875" Y="28.25185546875" />
                  <Point X="-3.059386230469" Y="28.6999140625" />
                  <Point X="-2.655033447266" Y="29.009927734375" />
                  <Point X="-2.648362060547" Y="29.01504296875" />
                  <Point X="-2.192523925781" Y="29.268296875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111819091797" Y="29.164046875" />
                  <Point X="-2.097515380859" Y="29.14910546875" />
                  <Point X="-2.089956542969" Y="29.14202734375" />
                  <Point X="-2.073376708984" Y="29.128115234375" />
                  <Point X="-2.065094970703" Y="29.12190234375" />
                  <Point X="-2.047897705078" Y="29.11041015625" />
                  <Point X="-2.038981933594" Y="29.105130859375" />
                  <Point X="-1.947100952148" Y="29.057298828125" />
                  <Point X="-1.934376586914" Y="29.05130859375" />
                  <Point X="-1.9150859375" Y="29.043802734375" />
                  <Point X="-1.905235595703" Y="29.040578125" />
                  <Point X="-1.884324707031" Y="29.03497265625" />
                  <Point X="-1.874194702148" Y="29.03283984375" />
                  <Point X="-1.853749389648" Y="29.029689453125" />
                  <Point X="-1.833074829102" Y="29.02878515625" />
                  <Point X="-1.812432495117" Y="29.03013671875" />
                  <Point X="-1.802149414062" Y="29.031375" />
                  <Point X="-1.780828979492" Y="29.0351328125" />
                  <Point X="-1.770731933594" Y="29.03748828125" />
                  <Point X="-1.750866455078" Y="29.043279296875" />
                  <Point X="-1.741098388672" Y="29.04671484375" />
                  <Point X="-1.645388916016" Y="29.086359375" />
                  <Point X="-1.632579467773" Y="29.092275390625" />
                  <Point X="-1.6144375" Y="29.102228515625" />
                  <Point X="-1.605634521484" Y="29.107703125" />
                  <Point X="-1.587909057617" Y="29.1201171875" />
                  <Point X="-1.57976184082" Y="29.126513671875" />
                  <Point X="-1.564215209961" Y="29.140150390625" />
                  <Point X="-1.550245605469" Y="29.155396484375" />
                  <Point X="-1.538017822266" Y="29.17207421875" />
                  <Point X="-1.532359863281" Y="29.18074609375" />
                  <Point X="-1.521540039062" Y="29.199486328125" />
                  <Point X="-1.516857788086" Y="29.208724609375" />
                  <Point X="-1.508526489258" Y="29.227658203125" />
                  <Point X="-1.504877441406" Y="29.237353515625" />
                  <Point X="-1.473725708008" Y="29.336154296875" />
                  <Point X="-1.470026977539" Y="29.34976171875" />
                  <Point X="-1.465991821289" Y="29.370046875" />
                  <Point X="-1.464527954102" Y="29.38030078125" />
                  <Point X="-1.462641113281" Y="29.40186328125" />
                  <Point X="-1.462301879883" Y="29.412216796875" />
                  <Point X="-1.462753173828" Y="29.4328984375" />
                  <Point X="-1.465041381836" Y="29.454603515625" />
                  <Point X="-1.479266357422" Y="29.562654296875" />
                  <Point X="-0.939796691895" Y="29.71390234375" />
                  <Point X="-0.931156799316" Y="29.716326171875" />
                  <Point X="-0.36522277832" Y="29.78255859375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.22043510437" Y="29.247107421875" />
                  <Point X="-0.207661560059" Y="29.218916015625" />
                  <Point X="-0.200119384766" Y="29.20534375" />
                  <Point X="-0.182261154175" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085354125977" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.00931760788" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227790833" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912307739" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763122559" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.1945730896" Y="29.1786171875" />
                  <Point X="0.21243132019" Y="29.20534375" />
                  <Point X="0.219973648071" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978118896" Y="29.2617265625" />
                  <Point X="0.244728713989" Y="29.286919921875" />
                  <Point X="0.378190765381" Y="29.7850078125" />
                  <Point X="0.820334838867" Y="29.738703125" />
                  <Point X="0.827850646973" Y="29.737916015625" />
                  <Point X="1.443715332031" Y="29.5892265625" />
                  <Point X="1.453618896484" Y="29.5868359375" />
                  <Point X="1.853427368164" Y="29.441822265625" />
                  <Point X="1.858248779297" Y="29.44007421875" />
                  <Point X="2.245864746094" Y="29.258798828125" />
                  <Point X="2.250440917969" Y="29.256658203125" />
                  <Point X="2.624945800781" Y="29.038470703125" />
                  <Point X="2.629440917969" Y="29.035853515625" />
                  <Point X="2.817779541016" Y="28.901916015625" />
                  <Point X="2.065308837891" Y="27.59859765625" />
                  <Point X="2.062372070313" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.02058190918" Y="27.46311328125" />
                  <Point X="2.018868652344" Y="27.455451171875" />
                  <Point X="2.015005126953" Y="27.43222265625" />
                  <Point X="2.012917358398" Y="27.410517578125" />
                  <Point X="2.012487792969" Y="27.40027734375" />
                  <Point X="2.012734741211" Y="27.37980859375" />
                  <Point X="2.013410888672" Y="27.369580078125" />
                  <Point X="2.021490112305" Y="27.302580078125" />
                  <Point X="2.023800537109" Y="27.28903515625" />
                  <Point X="2.029142578125" Y="27.267115234375" />
                  <Point X="2.032466308594" Y="27.25631640625" />
                  <Point X="2.040732177734" Y="27.2342265625" />
                  <Point X="2.045316772461" Y="27.223892578125" />
                  <Point X="2.055680419922" Y="27.203845703125" />
                  <Point X="2.061459228516" Y="27.1941328125" />
                  <Point X="2.102916259766" Y="27.13303515625" />
                  <Point X="2.107600097656" Y="27.1267109375" />
                  <Point X="2.122676513672" Y="27.10855078125" />
                  <Point X="2.137477539062" Y="27.0930390625" />
                  <Point X="2.144824707031" Y="27.086115234375" />
                  <Point X="2.160214599609" Y="27.0730859375" />
                  <Point X="2.168257324219" Y="27.06698046875" />
                  <Point X="2.229354248047" Y="27.0255234375" />
                  <Point X="2.241257324219" Y="27.0182578125" />
                  <Point X="2.261284423828" Y="27.00790234375" />
                  <Point X="2.271604248047" Y="27.003322265625" />
                  <Point X="2.293709228516" Y="26.995044921875" />
                  <Point X="2.304521240234" Y="26.99171484375" />
                  <Point X="2.326463623047" Y="26.986365234375" />
                  <Point X="2.337593994141" Y="26.984345703125" />
                  <Point X="2.40459375" Y="26.976267578125" />
                  <Point X="2.4125859375" Y="26.97564453125" />
                  <Point X="2.436635253906" Y="26.975126953125" />
                  <Point X="2.457962158203" Y="26.976171875" />
                  <Point X="2.4679921875" Y="26.97719921875" />
                  <Point X="2.487886474609" Y="26.980310546875" />
                  <Point X="2.497750732422" Y="26.98239453125" />
                  <Point X="2.575232421875" Y="27.003115234375" />
                  <Point X="2.597891113281" Y="27.010767578125" />
                  <Point X="2.626042480469" Y="27.022865234375" />
                  <Point X="2.636034423828" Y="27.027875" />
                  <Point X="2.68816796875" Y="27.057974609375" />
                  <Point X="3.940403320312" Y="27.780951171875" />
                  <Point X="4.041288818359" Y="27.640744140625" />
                  <Point X="4.043961914062" Y="27.637029296875" />
                  <Point X="4.136885253906" Y="27.48347265625" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116943359" Y="26.725212890625" />
                  <Point X="3.134668457031" Y="26.7066015625" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.072815673828" Y="26.626677734375" />
                  <Point X="3.068378662109" Y="26.620375" />
                  <Point X="3.056116455078" Y="26.600787109375" />
                  <Point X="3.045624511719" Y="26.5811953125" />
                  <Point X="3.041224853516" Y="26.5717734375" />
                  <Point X="3.033482421875" Y="26.552509765625" />
                  <Point X="3.030139648438" Y="26.54266796875" />
                  <Point X="3.009367675781" Y="26.468392578125" />
                  <Point X="3.006224609375" Y="26.454658203125" />
                  <Point X="3.002770996094" Y="26.432359375" />
                  <Point X="3.001708496094" Y="26.42110546875" />
                  <Point X="3.000892578125" Y="26.397541015625" />
                  <Point X="3.001174072266" Y="26.3862421875" />
                  <Point X="3.003077148438" Y="26.3637578125" />
                  <Point X="3.004698730469" Y="26.352572265625" />
                  <Point X="3.021750488281" Y="26.269931640625" />
                  <Point X="3.023619873047" Y="26.2624296875" />
                  <Point X="3.030438964844" Y="26.240263671875" />
                  <Point X="3.038254882812" Y="26.219892578125" />
                  <Point X="3.042430419922" Y="26.210548828125" />
                  <Point X="3.051761962891" Y="26.192365234375" />
                  <Point X="3.05691796875" Y="26.183525390625" />
                  <Point X="3.103296630859" Y="26.11303125" />
                  <Point X="3.111748779297" Y="26.10141015625" />
                  <Point X="3.126310302734" Y="26.084158203125" />
                  <Point X="3.134104492188" Y="26.075966796875" />
                  <Point X="3.151341796875" Y="26.05988671875" />
                  <Point X="3.160044433594" Y="26.0526875" />
                  <Point X="3.178245361328" Y="26.039369140625" />
                  <Point X="3.187743652344" Y="26.03325" />
                  <Point X="3.254952880859" Y="25.995416015625" />
                  <Point X="3.261996826172" Y="25.991828125" />
                  <Point X="3.283644775391" Y="25.982275390625" />
                  <Point X="3.304053710938" Y="25.974908203125" />
                  <Point X="3.313821533203" Y="25.971962890625" />
                  <Point X="3.333630126953" Y="25.96713671875" />
                  <Point X="3.343670898438" Y="25.965255859375" />
                  <Point X="3.434542236328" Y="25.95324609375" />
                  <Point X="3.457935302734" Y="25.9516484375" />
                  <Point X="3.489240234375" Y="25.951990234375" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.550125976562" Y="25.95931640625" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.751540039062" Y="25.9189296875" />
                  <Point X="4.752683105469" Y="25.914234375" />
                  <Point X="4.783870605469" Y="25.713921875" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686021972656" Y="25.419541015625" />
                  <Point X="3.665620605469" Y="25.412134765625" />
                  <Point X="3.642379394531" Y="25.4016171875" />
                  <Point X="3.634006347656" Y="25.397314453125" />
                  <Point X="3.544728027344" Y="25.3457109375" />
                  <Point X="3.538336669922" Y="25.341669921875" />
                  <Point X="3.519832519531" Y="25.32854296875" />
                  <Point X="3.502288330078" Y="25.314255859375" />
                  <Point X="3.494500244141" Y="25.30716015625" />
                  <Point X="3.479754150391" Y="25.292146484375" />
                  <Point X="3.472796142578" Y="25.284228515625" />
                  <Point X="3.419229248047" Y="25.215970703125" />
                  <Point X="3.410851806641" Y="25.204205078125" />
                  <Point X="3.399128173828" Y="25.184923828125" />
                  <Point X="3.393843017578" Y="25.1749375" />
                  <Point X="3.384069580078" Y="25.153474609375" />
                  <Point X="3.380006347656" Y="25.142931640625" />
                  <Point X="3.373159912109" Y="25.121431640625" />
                  <Point X="3.370376708984" Y="25.110474609375" />
                  <Point X="3.352520996094" Y="25.01723828125" />
                  <Point X="3.351391113281" Y="25.009720703125" />
                  <Point X="3.349210449219" Y="24.98703515625" />
                  <Point X="3.348563964844" Y="24.96484765625" />
                  <Point X="3.348827880859" Y="24.954484375" />
                  <Point X="3.350483398438" Y="24.93384765625" />
                  <Point X="3.351875" Y="24.92357421875" />
                  <Point X="3.369730712891" Y="24.83033984375" />
                  <Point X="3.373159912109" Y="24.816009765625" />
                  <Point X="3.380006103516" Y="24.794509765625" />
                  <Point X="3.384069580078" Y="24.78396484375" />
                  <Point X="3.393843017578" Y="24.762501953125" />
                  <Point X="3.399128173828" Y="24.752515625" />
                  <Point X="3.410851806641" Y="24.733234375" />
                  <Point X="3.417290283203" Y="24.723939453125" />
                  <Point X="3.470857177734" Y="24.655681640625" />
                  <Point X="3.47590625" Y="24.649763671875" />
                  <Point X="3.491968994141" Y="24.632896484375" />
                  <Point X="3.508220458984" Y="24.618005859375" />
                  <Point X="3.516207519531" Y="24.61144921875" />
                  <Point X="3.532845458984" Y="24.599244140625" />
                  <Point X="3.541496337891" Y="24.593595703125" />
                  <Point X="3.630774658203" Y="24.5419921875" />
                  <Point X="3.651341308594" Y="24.531748046875" />
                  <Point X="3.680988769531" Y="24.519767578125" />
                  <Point X="3.691995849609" Y="24.516083984375" />
                  <Point X="3.737411132812" Y="24.503916015625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.762252441406" Y="24.073185546875" />
                  <Point X="4.761611816406" Y="24.0689375" />
                  <Point X="4.727802734375" Y="23.920779296875" />
                  <Point X="3.436775146484" Y="24.09074609375" />
                  <Point X="3.428618164062" Y="24.091462890625" />
                  <Point X="3.400098632812" Y="24.09195703125" />
                  <Point X="3.366718994141" Y="24.089158203125" />
                  <Point X="3.354479736328" Y="24.087322265625" />
                  <Point X="3.179258056641" Y="24.04923828125" />
                  <Point X="3.157869628906" Y="24.043255859375" />
                  <Point X="3.128745605469" Y="24.03162890625" />
                  <Point X="3.114667480469" Y="24.02460546875" />
                  <Point X="3.086843017578" Y="24.007712890625" />
                  <Point X="3.074120117188" Y="23.998466796875" />
                  <Point X="3.050372802734" Y="23.977998046875" />
                  <Point X="3.039348632812" Y="23.966775390625" />
                  <Point X="2.933455566406" Y="23.83941796875" />
                  <Point X="2.933455322266" Y="23.83941796875" />
                  <Point X="2.921343505859" Y="23.82317578125" />
                  <Point X="2.906618896484" Y="23.79879296875" />
                  <Point X="2.900175292969" Y="23.786046875" />
                  <Point X="2.888826904297" Y="23.75865625" />
                  <Point X="2.884366699219" Y="23.745083984375" />
                  <Point X="2.877531738281" Y="23.71742578125" />
                  <Point X="2.875156982422" Y="23.70333984375" />
                  <Point X="2.859977539062" Y="23.538380859375" />
                  <Point X="2.859288574219" Y="23.51667578125" />
                  <Point X="2.861608154297" Y="23.485404296875" />
                  <Point X="2.864067138672" Y="23.469865234375" />
                  <Point X="2.871800292969" Y="23.4382421875" />
                  <Point X="2.8767890625" Y="23.42332421875" />
                  <Point X="2.889159912109" Y="23.394515625" />
                  <Point X="2.896541992188" Y="23.380625" />
                  <Point X="2.993512207031" Y="23.229794921875" />
                  <Point X="3.007596191406" Y="23.210638671875" />
                  <Point X="3.022724853516" Y="23.192552734375" />
                  <Point X="3.029844970703" Y="23.18493359375" />
                  <Point X="3.044880371094" Y="23.170517578125" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.09494140625" Y="23.131380859375" />
                  <Point X="4.087170410156" Y="22.370017578125" />
                  <Point X="4.047288574219" Y="22.305482421875" />
                  <Point X="4.045487060547" Y="22.30256640625" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="2.848446533203" Y="22.90533203125" />
                  <Point X="2.841173583984" Y="22.909123046875" />
                  <Point X="2.815026123047" Y="22.920484375" />
                  <Point X="2.78312109375" Y="22.930671875" />
                  <Point X="2.771107910156" Y="22.933662109375" />
                  <Point X="2.562566162109" Y="22.97132421875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508010009766" Y="22.974595703125" />
                  <Point X="2.492319335938" Y="22.97349609375" />
                  <Point X="2.460144287109" Y="22.9685390625" />
                  <Point X="2.444846435547" Y="22.96486328125" />
                  <Point X="2.415067871094" Y="22.9550390625" />
                  <Point X="2.400587158203" Y="22.948890625" />
                  <Point X="2.227340087891" Y="22.857712890625" />
                  <Point X="2.208965576172" Y="22.846828125" />
                  <Point X="2.186030761719" Y="22.829931640625" />
                  <Point X="2.175199462891" Y="22.820619140625" />
                  <Point X="2.154240966797" Y="22.799658203125" />
                  <Point X="2.144931396484" Y="22.788828125" />
                  <Point X="2.128042480469" Y="22.765900390625" />
                  <Point X="2.120463134766" Y="22.753802734375" />
                  <Point X="2.029284423828" Y="22.580556640625" />
                  <Point X="2.019835205078" Y="22.5598046875" />
                  <Point X="2.010011108398" Y="22.53002734375" />
                  <Point X="2.006335693359" Y="22.51473046875" />
                  <Point X="2.00137890625" Y="22.48255859375" />
                  <Point X="2.000278930664" Y="22.466865234375" />
                  <Point X="2.00068347168" Y="22.435515625" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.039850219727" Y="22.21131640625" />
                  <Point X="2.045196044922" Y="22.18930859375" />
                  <Point X="2.052462158203" Y="22.165646484375" />
                  <Point X="2.056179443359" Y="22.15559765625" />
                  <Point X="2.064721923828" Y="22.135984375" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.096629882812" Y="22.07951171875" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.72375390625" Y="20.963916015625" />
                  <Point X="1.833915893555" Y="22.12357421875" />
                  <Point X="1.828657470703" Y="22.12984765625" />
                  <Point X="1.808838256836" Y="22.1503671875" />
                  <Point X="1.783255249023" Y="22.171994140625" />
                  <Point X="1.773299072266" Y="22.179353515625" />
                  <Point X="1.567620361328" Y="22.3115859375" />
                  <Point X="1.567596801758" Y="22.3116015625" />
                  <Point X="1.546249389648" Y="22.323771484375" />
                  <Point X="1.517452148438" Y="22.3361328125" />
                  <Point X="1.502537963867" Y="22.341119140625" />
                  <Point X="1.47092199707" Y="22.34884765625" />
                  <Point X="1.455383056641" Y="22.3513046875" />
                  <Point X="1.424119262695" Y="22.35362109375" />
                  <Point X="1.40839440918" Y="22.35348046875" />
                  <Point X="1.18353112793" Y="22.3327890625" />
                  <Point X="1.16128515625" Y="22.329669921875" />
                  <Point X="1.133598266602" Y="22.322833984375" />
                  <Point X="1.120014770508" Y="22.31837109375" />
                  <Point X="1.092619506836" Y="22.3070234375" />
                  <Point X="1.079873291016" Y="22.30058203125" />
                  <Point X="1.055491455078" Y="22.285861328125" />
                  <Point X="1.04385559082" Y="22.27758203125" />
                  <Point X="0.870159301758" Y="22.133158203125" />
                  <Point X="0.852650512695" Y="22.11690625" />
                  <Point X="0.832179626465" Y="22.09315625" />
                  <Point X="0.822930358887" Y="22.0804296875" />
                  <Point X="0.806040710449" Y="22.052607421875" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394775391" Y="22.009412109375" />
                  <Point X="0.782791931152" Y="21.994369140625" />
                  <Point X="0.73085760498" Y="21.755431640625" />
                  <Point X="0.727475341797" Y="21.734080078125" />
                  <Point X="0.724882324219" Y="21.70828515625" />
                  <Point X="0.724417175293" Y="21.69732421875" />
                  <Point X="0.724753479004" Y="21.675421875" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.733230041504" Y="21.606181640625" />
                  <Point X="0.833091186523" Y="20.847662109375" />
                  <Point X="0.65506451416" Y="21.51206640625" />
                  <Point X="0.652605834961" Y="21.51987890625" />
                  <Point X="0.642148132324" Y="21.5464140625" />
                  <Point X="0.626790649414" Y="21.5761796875" />
                  <Point X="0.620409667969" Y="21.5867890625" />
                  <Point X="0.462406677246" Y="21.814439453125" />
                  <Point X="0.462406860352" Y="21.814439453125" />
                  <Point X="0.446673492432" Y="21.83482421875" />
                  <Point X="0.42479397583" Y="21.857275390625" />
                  <Point X="0.412922729492" Y="21.867591796875" />
                  <Point X="0.386664642334" Y="21.88683984375" />
                  <Point X="0.37324798584" Y="21.89505859375" />
                  <Point X="0.345242248535" Y="21.909169921875" />
                  <Point X="0.330652954102" Y="21.9150625" />
                  <Point X="0.086154624939" Y="21.990943359375" />
                  <Point X="0.086154502869" Y="21.990943359375" />
                  <Point X="0.063397335052" Y="21.99688671875" />
                  <Point X="0.035234485626" Y="22.001158203125" />
                  <Point X="0.020994924545" Y="22.002232421875" />
                  <Point X="-0.008648359299" Y="22.002234375" />
                  <Point X="-0.022892526627" Y="22.001162109375" />
                  <Point X="-0.051058349609" Y="21.996892578125" />
                  <Point X="-0.064979858398" Y="21.9936953125" />
                  <Point X="-0.309488464355" Y="21.91780859375" />
                  <Point X="-0.332923614502" Y="21.909171875" />
                  <Point X="-0.360920898438" Y="21.89506640625" />
                  <Point X="-0.374333526611" Y="21.8868515625" />
                  <Point X="-0.400591918945" Y="21.867607421875" />
                  <Point X="-0.412470306396" Y="21.857287109375" />
                  <Point X="-0.434357849121" Y="21.834828125" />
                  <Point X="-0.444367156982" Y="21.822689453125" />
                  <Point X="-0.602375305176" Y="21.595029296875" />
                  <Point X="-0.613221801758" Y="21.577431640625" />
                  <Point X="-0.626162414551" Y="21.553623046875" />
                  <Point X="-0.630923522949" Y="21.543478515625" />
                  <Point X="-0.639219055176" Y="21.52269921875" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.656746582031" Y="21.45983984375" />
                  <Point X="-0.985425048828" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150206075824" Y="22.504544428746" />
                  <Point X="-3.708652924864" Y="22.031036645845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.951578663606" Y="21.219173896906" />
                  <Point X="-2.722024793371" Y="20.973007509201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115752610263" Y="22.606894132963" />
                  <Point X="-3.62421649426" Y="22.079786182314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901908315892" Y="21.305205492837" />
                  <Point X="-2.486311177003" Y="20.859532125117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.040035442378" Y="22.664993933946" />
                  <Point X="-3.539780063656" Y="22.128535718784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852237968177" Y="21.391237088768" />
                  <Point X="-2.427674989024" Y="20.935949034489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.964318274492" Y="22.72309373493" />
                  <Point X="-3.455343633052" Y="22.177285255253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.802567620463" Y="21.477268684698" />
                  <Point X="-2.362269349542" Y="21.005106595885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.888601106607" Y="22.781193535914" />
                  <Point X="-3.370907202447" Y="22.226034791723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752897272749" Y="21.563300280629" />
                  <Point X="-2.282693709692" Y="21.059068692265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673371936158" Y="23.762053740701" />
                  <Point X="-4.631523511686" Y="23.717176799733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.812883938721" Y="22.839293336898" />
                  <Point X="-3.286470771843" Y="22.274784328192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703226925034" Y="21.64933187656" />
                  <Point X="-2.20266333141" Y="21.11254314138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.722375470237" Y="23.953900119963" />
                  <Point X="-4.515830711002" Y="23.73240798294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737166770836" Y="22.897393137881" />
                  <Point X="-3.202034341239" Y="22.323533864662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.65355657732" Y="21.735363472491" />
                  <Point X="-2.122632953128" Y="21.166017590495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755463949228" Y="24.128679692131" />
                  <Point X="-4.400137910319" Y="23.747639166148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661449602951" Y="22.955492938865" />
                  <Point X="-3.117597910635" Y="22.372283401132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.603886229606" Y="21.821395068422" />
                  <Point X="-2.034612891289" Y="21.21092415296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778995929521" Y="24.293211174118" />
                  <Point X="-4.284445109635" Y="23.762870349355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.585732435065" Y="23.013592739849" />
                  <Point X="-3.03316148003" Y="22.421032937601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.554215881891" Y="21.907426664353" />
                  <Point X="-1.915110437707" Y="21.222069983603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.126995012601" Y="20.376919661832" />
                  <Point X="-0.994637226952" Y="20.234983313973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715454280248" Y="24.36436762029" />
                  <Point X="-4.168752308951" Y="23.778101532562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.51001526718" Y="23.071692540833" />
                  <Point X="-2.948725049426" Y="22.469782474071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.504545534177" Y="21.993458260284" />
                  <Point X="-1.77675850795" Y="21.213002225796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125858297557" Y="20.514997206823" />
                  <Point X="-0.958115674784" Y="20.335115266823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.611526360699" Y="24.392215093904" />
                  <Point X="-4.053059508267" Y="23.79333271577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434298099294" Y="23.129792341816" />
                  <Point X="-2.864288618822" Y="22.51853201054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.454875186463" Y="22.079489856214" />
                  <Point X="-1.638406578192" Y="21.203934467988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.161078963868" Y="20.692063269956" />
                  <Point X="-0.929122257304" Y="20.443320155756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.50759844115" Y="24.420062567517" />
                  <Point X="-3.937366707583" Y="23.808563898977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358580931409" Y="23.1878921428" />
                  <Point X="-2.779852188218" Y="22.56728154701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405204838748" Y="22.165521452145" />
                  <Point X="-1.269729265883" Y="20.947872976807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.235672440026" Y="20.911351502396" />
                  <Point X="-0.900128839824" Y="20.551525044689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.403670521601" Y="24.44791004113" />
                  <Point X="-3.821673906899" Y="23.823795082184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.282863763523" Y="23.245991943784" />
                  <Point X="-2.695416000082" Y="22.616031343496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355534491034" Y="22.251553048076" />
                  <Point X="-0.871135422343" Y="20.659729933623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.299742602052" Y="24.475757514744" />
                  <Point X="-3.705981106216" Y="23.839026265391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.207146595638" Y="23.304091744768" />
                  <Point X="-2.59648472809" Y="22.649237065604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316636672428" Y="22.349136767151" />
                  <Point X="-0.842142004863" Y="20.767934822556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.195814682503" Y="24.503604988357" />
                  <Point X="-3.590288305532" Y="23.854257448599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.131429427753" Y="23.362191545752" />
                  <Point X="-2.424825572767" Y="22.604451681282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347011059309" Y="22.521005831864" />
                  <Point X="-0.813148587383" Y="20.876139711489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091886762954" Y="24.531452461971" />
                  <Point X="-3.474595504848" Y="23.869488631806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055712259867" Y="23.420291346735" />
                  <Point X="-0.784155169902" Y="20.984344600422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.987958843405" Y="24.559299935584" />
                  <Point X="-3.358902704164" Y="23.884719815013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.99234078425" Y="23.491630281811" />
                  <Point X="-0.755161752422" Y="21.092549489355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884030923856" Y="24.587147409198" />
                  <Point X="-3.243125516984" Y="23.899860504783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953664837092" Y="23.589451928884" />
                  <Point X="-0.726168334942" Y="21.200754378288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772512773608" Y="25.679224066933" />
                  <Point X="-4.659557920094" Y="25.558094816378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780103004307" Y="24.614994882811" />
                  <Point X="-3.075520653694" Y="23.859422816378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.969107855717" Y="23.745309061482" />
                  <Point X="-0.697174917461" Y="21.308959267222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.754723470612" Y="25.799443897662" />
                  <Point X="-4.486394012702" Y="25.511695783021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676175084758" Y="24.642842356425" />
                  <Point X="-0.668181499981" Y="21.417164156155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736934167616" Y="25.919663728391" />
                  <Point X="-4.31323010531" Y="25.465296749665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.572247165209" Y="24.670689830038" />
                  <Point X="-0.638463434828" Y="21.524591955599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.711295141053" Y="26.031465761186" />
                  <Point X="-4.140066197919" Y="25.418897716308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471534313693" Y="24.701985042011" />
                  <Point X="-0.590374818675" Y="21.612319750963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.682047574672" Y="26.13939810879" />
                  <Point X="-3.966902290527" Y="25.372498682952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.39073168398" Y="24.754631352855" />
                  <Point X="-0.534948450048" Y="21.692178770173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.652800008291" Y="26.247330456394" />
                  <Point X="-3.793738383135" Y="25.326099649595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331051653014" Y="24.829928877669" />
                  <Point X="-0.479522081421" Y="21.772037789383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.582786397098" Y="26.311546573111" />
                  <Point X="-3.620574475744" Y="25.279700616238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.297666494696" Y="24.933424201146" />
                  <Point X="-0.420979822897" Y="21.848555425763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.434711890497" Y="26.292052628115" />
                  <Point X="-3.424311341458" Y="25.208530694735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.332153118358" Y="25.109703099911" />
                  <Point X="-0.342915243643" Y="21.904137936246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.286637383895" Y="26.27255868312" />
                  <Point X="-0.244559053863" Y="21.937960358525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138562877294" Y="26.253064738124" />
                  <Point X="-0.143819047411" Y="21.969226450393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.805284698324" Y="20.9514372909" />
                  <Point X="0.821754050649" Y="20.933776072792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990488370693" Y="26.233570793128" />
                  <Point X="-0.041126013377" Y="21.998398176593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752911293274" Y="21.146897414349" />
                  <Point X="0.800400602311" Y="21.095971365277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842413864092" Y="26.214076848133" />
                  <Point X="0.099618349663" Y="21.986764848193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.700537888223" Y="21.342357537798" />
                  <Point X="0.779047153972" Y="21.258166657762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.706665302046" Y="26.2078008604" />
                  <Point X="0.282418484501" Y="21.93003222604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.643612050156" Y="21.542699547969" />
                  <Point X="0.757693705634" Y="21.420361950247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604329416812" Y="26.237355581798" />
                  <Point X="0.736340257295" Y="21.582557242732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.521722608218" Y="26.288067147662" />
                  <Point X="0.727228535676" Y="21.731624890527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465501588605" Y="26.367074008019" />
                  <Point X="0.750528941348" Y="21.845934787189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.217076738425" Y="27.312336204554" />
                  <Point X="-4.131379033559" Y="27.220436667335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.428259867217" Y="26.466433673931" />
                  <Point X="0.775082669726" Y="21.958900659798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167070932255" Y="27.398008065334" />
                  <Point X="-3.674732389924" Y="26.870039617798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442357679254" Y="26.620848249075" />
                  <Point X="0.810596905258" Y="22.060112827489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117065126086" Y="27.483679926114" />
                  <Point X="0.871408510818" Y="22.134196887115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.067059319916" Y="27.569351786894" />
                  <Point X="0.944574520018" Y="22.195032470848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017053513746" Y="27.655023647674" />
                  <Point X="1.017740529218" Y="22.25586805458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960651907074" Y="27.733836852118" />
                  <Point X="1.097895524962" Y="22.309208867828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901570951528" Y="27.809776806668" />
                  <Point X="1.204042692029" Y="22.334676489844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842489995982" Y="27.885716761218" />
                  <Point X="1.323673528623" Y="22.345684646561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.783409040436" Y="27.961656715768" />
                  <Point X="1.447805300772" Y="22.351866140825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.638580332875" Y="21.074916255719" />
                  <Point X="2.734919779293" Y="20.971604847839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716766321687" Y="28.029487672067" />
                  <Point X="1.686585296166" Y="22.23510246782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035186937286" Y="21.86127297562" />
                  <Point X="2.527335515583" Y="21.333508239571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435370112537" Y="27.86702370489" />
                  <Point X="2.316178054067" Y="21.699243416825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183516393468" Y="27.736240179694" />
                  <Point X="2.105020592551" Y="22.064978594078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041934711803" Y="27.723708936999" />
                  <Point X="2.025774490004" Y="22.289256157477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.937481489382" Y="27.750993092249" />
                  <Point X="2.000422500956" Y="22.455739359905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862294219516" Y="27.809661139288" />
                  <Point X="2.024245029794" Y="22.569489348021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.797211460913" Y="27.879164948036" />
                  <Point X="2.070605736331" Y="22.659070099591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754249515262" Y="27.972390424434" />
                  <Point X="2.117468415012" Y="22.748112551942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754729582703" Y="28.112201756372" />
                  <Point X="2.17772565176" Y="22.822791099337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874885466779" Y="28.380349689416" />
                  <Point X="2.259348075773" Y="22.874558288425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.04982428169" Y="28.707245123332" />
                  <Point X="2.346481593556" Y="22.920415552996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974080946196" Y="28.765316862991" />
                  <Point X="2.437262642646" Y="22.962361319124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.898337610702" Y="28.82338860265" />
                  <Point X="2.558310403956" Y="22.971850010113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.822594275208" Y="28.881460342309" />
                  <Point X="2.714240228366" Y="22.943932268092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.746850939714" Y="28.939532081968" />
                  <Point X="2.917086742656" Y="22.865702535865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.67110760422" Y="28.997603821627" />
                  <Point X="3.198483806367" Y="22.703237652284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588491715581" Y="29.048305650335" />
                  <Point X="3.479880870079" Y="22.540772768703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502925970918" Y="29.095844145745" />
                  <Point X="3.76127793379" Y="22.378307885121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.417360226255" Y="29.143382641154" />
                  <Point X="2.86218414195" Y="23.481764457504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425328082246" Y="22.877866516691" />
                  <Point X="4.009493751952" Y="22.251425531027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.331794481593" Y="29.190921136564" />
                  <Point X="2.867100574642" Y="23.615788751556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.88197430145" Y="22.527469922301" />
                  <Point X="4.063906000217" Y="22.332372061182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.24622873693" Y="29.238459631973" />
                  <Point X="2.882703395757" Y="23.73835329704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.947677176633" Y="29.057598803018" />
                  <Point X="2.926745699706" Y="23.830420231003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.794572340272" Y="29.032710489786" />
                  <Point X="2.987565325664" Y="23.904495689806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.695390726934" Y="29.065647753668" />
                  <Point X="3.049627190669" Y="23.977239010325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.605076393465" Y="29.108094011025" />
                  <Point X="3.128788086227" Y="24.031645865507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.536710824261" Y="29.174077436404" />
                  <Point X="3.231658493408" Y="24.060627382294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495008533213" Y="29.268653726984" />
                  <Point X="3.339664058897" Y="24.084102115991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465151567687" Y="29.375932574013" />
                  <Point X="3.467085720338" Y="24.086755635918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474316708641" Y="29.525057507031" />
                  <Point X="3.615160662348" Y="24.067261224004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.4001616274" Y="29.584832440854" />
                  <Point X="3.763235604357" Y="24.04776681209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.297187485892" Y="29.613702716195" />
                  <Point X="3.911310546367" Y="24.028272400176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.194213344383" Y="29.642572991536" />
                  <Point X="4.059385488377" Y="24.008777988262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.091239202875" Y="29.671443266876" />
                  <Point X="3.355921009254" Y="24.902447806923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.724276213481" Y="24.507435211734" />
                  <Point X="4.207460430386" Y="23.989283576347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.988265061367" Y="29.700313542217" />
                  <Point X="3.356993830714" Y="25.040593869393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.897439939438" Y="24.461036372943" />
                  <Point X="4.355535372396" Y="23.969789164433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.878993742542" Y="29.722430921642" />
                  <Point X="3.383355280329" Y="25.15162119831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070604037444" Y="24.414637135177" />
                  <Point X="4.503610314406" Y="23.950294752519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.761878867011" Y="29.73613711628" />
                  <Point X="3.434773673723" Y="25.235778244751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.24376813545" Y="24.368237897412" />
                  <Point X="4.651685256415" Y="23.930800340605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64476399148" Y="29.749843310917" />
                  <Point X="-0.240213001337" Y="29.316015487478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027727252841" Y="29.088152419465" />
                  <Point X="3.496449209276" Y="25.308935852886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.416932233456" Y="24.321838659647" />
                  <Point X="4.740212798712" Y="23.975162696907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.527649115949" Y="29.763549505555" />
                  <Point X="-0.292586332289" Y="29.511475531467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.089678606595" Y="29.101546572069" />
                  <Point X="3.575477835671" Y="25.363484549379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.590096331461" Y="24.275439421881" />
                  <Point X="4.764541708244" Y="24.088369658211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.410534240418" Y="29.777255700193" />
                  <Point X="-0.344959663241" Y="29.706935575456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.171955389645" Y="29.152612047" />
                  <Point X="3.661672841746" Y="25.41034824454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.763260429467" Y="24.229040184116" />
                  <Point X="4.782620171633" Y="24.208279402384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.226529536878" Y="29.233384961767" />
                  <Point X="2.020666717692" Y="27.30940838757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320569247802" Y="26.987802298223" />
                  <Point X="3.039725576588" Y="26.216601553617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236074341576" Y="26.006043281791" />
                  <Point X="3.763451089242" Y="25.4405009592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.258517114102" Y="29.338379007478" />
                  <Point X="2.018188578134" Y="27.451362389527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461018935107" Y="26.97648497086" />
                  <Point X="3.000892589913" Y="26.397541356079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.412486388245" Y="25.956161045508" />
                  <Point X="3.867379037023" Y="25.468348402538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.28751049461" Y="29.446583936059" />
                  <Point X="2.047731284911" Y="27.558978237805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.567908387634" Y="27.001156589174" />
                  <Point X="3.022137762265" Y="26.514055220646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.540608458396" Y="25.95806346905" />
                  <Point X="3.971306984804" Y="25.496195845877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.316503875117" Y="29.554788864641" />
                  <Point X="2.094130293644" Y="27.6485179153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.659991078283" Y="27.041706515622" />
                  <Point X="3.062156988885" Y="26.610436376854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.656301201083" Y="25.973294714452" />
                  <Point X="4.075234932585" Y="25.524043289215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.345497255625" Y="29.662993793222" />
                  <Point X="2.143800631591" Y="27.734549521705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.744427548534" Y="27.090456009576" />
                  <Point X="3.119777267888" Y="26.687942715225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.771993881865" Y="25.988526026238" />
                  <Point X="4.179162880366" Y="25.551890732553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.374490636133" Y="29.771198721804" />
                  <Point X="2.193470969538" Y="27.82058112811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.828864140135" Y="27.139205373399" />
                  <Point X="3.187299886179" Y="26.754830094787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.887686562647" Y="26.003757338024" />
                  <Point X="4.283090828147" Y="25.579738175892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.503774029469" Y="29.7718557787" />
                  <Point X="2.243141307485" Y="27.906612734515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.913300731735" Y="27.187954737221" />
                  <Point X="3.263017036735" Y="26.812929914353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.003379243429" Y="26.01898864981" />
                  <Point X="4.387018775928" Y="25.60758561923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.647728767581" Y="29.756779744525" />
                  <Point X="2.292811645432" Y="27.99264434092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.997737323336" Y="27.236704101043" />
                  <Point X="3.338734187292" Y="26.87102973392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.119071924211" Y="26.034219961596" />
                  <Point X="4.490946723709" Y="25.635433062568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791683505693" Y="29.741703710349" />
                  <Point X="2.342481983379" Y="28.078675947325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.082173914936" Y="27.285453464865" />
                  <Point X="3.414451337848" Y="26.929129553487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.234764604993" Y="26.049451273383" />
                  <Point X="4.59487467149" Y="25.663280505907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.953371288435" Y="29.707611313979" />
                  <Point X="2.392152321326" Y="28.16470755373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.166610506536" Y="27.334202828688" />
                  <Point X="3.490168488405" Y="26.987229373054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.350457285775" Y="26.064682585169" />
                  <Point X="4.698802619271" Y="25.691127949245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.12100923704" Y="29.667138145918" />
                  <Point X="2.441822659273" Y="28.250739160135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251047098137" Y="27.38295219251" />
                  <Point X="3.565885638961" Y="27.04532919262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.466149966557" Y="26.079913896955" />
                  <Point X="4.779146101402" Y="25.744266635589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.288647185645" Y="29.626664977858" />
                  <Point X="2.49149299722" Y="28.336770766541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.335483689737" Y="27.431701556332" />
                  <Point X="3.641602789518" Y="27.103429012187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.581842647339" Y="26.095145208741" />
                  <Point X="4.753111689307" Y="25.911481647139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.456740193402" Y="29.5857038186" />
                  <Point X="2.541163335167" Y="28.422802372946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419920281338" Y="27.480450920155" />
                  <Point X="3.717319940074" Y="27.161528831754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.697535328121" Y="26.110376520527" />
                  <Point X="4.707547679056" Y="26.099639588671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.653026237661" Y="29.514509329158" />
                  <Point X="2.590833673114" Y="28.508833979351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.504356872938" Y="27.529200283977" />
                  <Point X="3.793037090631" Y="27.219628651321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.84931228192" Y="29.443314839716" />
                  <Point X="2.64050401106" Y="28.594865585756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.588793464539" Y="27.577949647799" />
                  <Point X="3.868754241187" Y="27.277728470887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078115916161" Y="29.337249504252" />
                  <Point X="2.690174349007" Y="28.680897192161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.673230056139" Y="27.626699011622" />
                  <Point X="3.944471391744" Y="27.335828290454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.322091200876" Y="29.21491456554" />
                  <Point X="2.739844686954" Y="28.766928798566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75766664774" Y="27.675448375444" />
                  <Point X="4.0201885423" Y="27.393928110021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606505548421" Y="29.049214041187" />
                  <Point X="2.789515024901" Y="28.852960404971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.84210323934" Y="27.724197739266" />
                  <Point X="4.095905692857" Y="27.452027929588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92653983094" Y="27.772947103089" />
                  <Point X="4.01246123419" Y="27.680807678722" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.85812677002" Y="20.020123046875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464321105957" Y="21.478453125" />
                  <Point X="0.306312866211" Y="21.706111328125" />
                  <Point X="0.300593475342" Y="21.714353515625" />
                  <Point X="0.274335449219" Y="21.7336015625" />
                  <Point X="0.029827056885" Y="21.809486328125" />
                  <Point X="0.020982406616" Y="21.812232421875" />
                  <Point X="-0.008660801888" Y="21.812234375" />
                  <Point X="-0.253169342041" Y="21.73634765625" />
                  <Point X="-0.262019927979" Y="21.7336015625" />
                  <Point X="-0.288278259277" Y="21.714357421875" />
                  <Point X="-0.446286468506" Y="21.486697265625" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.47322064209" Y="21.4106640625" />
                  <Point X="-0.847743896484" Y="20.012923828125" />
                  <Point X="-1.091244262695" Y="20.0601875" />
                  <Point X="-1.100260253906" Y="20.0619375" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683105469" Y="20.465240234375" />
                  <Point X="-1.368096191406" Y="20.75890234375" />
                  <Point X="-1.370210571289" Y="20.769533203125" />
                  <Point X="-1.38628137207" Y="20.79737109375" />
                  <Point X="-1.611393676758" Y="20.9947890625" />
                  <Point X="-1.619542236328" Y="21.001935546875" />
                  <Point X="-1.649239990234" Y="21.014236328125" />
                  <Point X="-1.948014282227" Y="21.033818359375" />
                  <Point X="-1.958828979492" Y="21.03452734375" />
                  <Point X="-1.989878662109" Y="21.026208984375" />
                  <Point X="-2.238833251953" Y="20.85986328125" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.267590332031" Y="20.832466796875" />
                  <Point X="-2.457095214844" Y="20.585498046875" />
                  <Point X="-2.84265234375" Y="20.8242265625" />
                  <Point X="-2.855822998047" Y="20.832380859375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.224463378906" Y="21.1265234375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979980469" Y="22.431236328125" />
                  <Point X="-2.530722412109" Y="22.447978515625" />
                  <Point X="-2.531349609375" Y="22.44860546875" />
                  <Point X="-2.560172363281" Y="22.462802734375" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.636726074219" Y="22.4305234375" />
                  <Point X="-3.842959472656" Y="21.734103515625" />
                  <Point X="-4.151288085938" Y="22.139185546875" />
                  <Point X="-4.161698242187" Y="22.152861328125" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.420805664062" Y="22.612310546875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.138386474609" Y="23.6326953125" />
                  <Point X="-3.138119384766" Y="23.6337265625" />
                  <Point X="-3.140324707031" Y="23.665400390625" />
                  <Point X="-3.161157958984" Y="23.689359375" />
                  <Point X="-3.186715576172" Y="23.70440234375" />
                  <Point X="-3.187692626953" Y="23.7049765625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.276391357422" Y="23.703943359375" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.923318847656" Y="23.972857421875" />
                  <Point X="-4.927392089844" Y="23.988802734375" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.989397460938" Y="24.48766796875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541895019531" Y="24.87857421875" />
                  <Point X="-3.515111572266" Y="24.8971640625" />
                  <Point X="-3.514171386719" Y="24.89781640625" />
                  <Point X="-3.494897705078" Y="24.924095703125" />
                  <Point X="-3.485969726563" Y="24.952861328125" />
                  <Point X="-3.485646728516" Y="24.95390234375" />
                  <Point X="-3.485647949219" Y="24.9835390625" />
                  <Point X="-3.494575683594" Y="25.0123046875" />
                  <Point X="-3.494893066406" Y="25.013328125" />
                  <Point X="-3.514144287109" Y="25.039603515625" />
                  <Point X="-3.540899169922" Y="25.058171875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.609296386719" Y="25.079974609375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.920270019531" Y="25.9786796875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.773516113281" Y="26.528296875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731705810547" Y="26.3958671875" />
                  <Point X="-3.672425537109" Y="26.414556640625" />
                  <Point X="-3.670254882812" Y="26.4152421875" />
                  <Point X="-3.651525146484" Y="26.426064453125" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.6153359375" Y="26.501205078125" />
                  <Point X="-3.614474853516" Y="26.503283203125" />
                  <Point X="-3.610713867188" Y="26.5246015625" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.645016845703" Y="26.600646484375" />
                  <Point X="-3.6460625" Y="26.60265234375" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.689699462891" Y="26.64203515625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.170213378906" Y="27.76953125" />
                  <Point X="-4.160010253906" Y="27.78701171875" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138519042969" Y="27.920435546875" />
                  <Point X="-3.055958251953" Y="27.913212890625" />
                  <Point X="-3.052969726562" Y="27.912951171875" />
                  <Point X="-3.031510009766" Y="27.9157734375" />
                  <Point X="-3.013254394531" Y="27.92740234375" />
                  <Point X="-2.954652099609" Y="27.98600390625" />
                  <Point X="-2.952530761719" Y="27.988125" />
                  <Point X="-2.940900390625" Y="28.00637890625" />
                  <Point X="-2.93807421875" Y="28.027837890625" />
                  <Point X="-2.945297363281" Y="28.1103984375" />
                  <Point X="-2.945558837891" Y="28.113388671875" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-2.9652421875" Y="28.1568515625" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.770637939453" Y="29.160712890625" />
                  <Point X="-2.752870117188" Y="29.1743359375" />
                  <Point X="-2.141548828125" Y="29.51397265625" />
                  <Point X="-1.967826904297" Y="29.28757421875" />
                  <Point X="-1.951247070312" Y="29.273662109375" />
                  <Point X="-1.859366088867" Y="29.225830078125" />
                  <Point X="-1.856040283203" Y="29.22409765625" />
                  <Point X="-1.835129394531" Y="29.2184921875" />
                  <Point X="-1.813808837891" Y="29.22225" />
                  <Point X="-1.718099365234" Y="29.26189453125" />
                  <Point X="-1.714629150391" Y="29.26333203125" />
                  <Point X="-1.696903686523" Y="29.27574609375" />
                  <Point X="-1.686083740234" Y="29.294486328125" />
                  <Point X="-1.654932128906" Y="29.393287109375" />
                  <Point X="-1.65380456543" Y="29.39686328125" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.653415527344" Y="29.429802734375" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-0.991088378906" Y="29.89684765625" />
                  <Point X="-0.968082092285" Y="29.903298828125" />
                  <Point X="-0.224200073242" Y="29.990359375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282121658" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594039917" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.061202869415" Y="29.336095703125" />
                  <Point X="0.236648391724" Y="29.9908671875" />
                  <Point X="0.840124389648" Y="29.92766796875" />
                  <Point X="0.860209472656" Y="29.925564453125" />
                  <Point X="1.488306152344" Y="29.773921875" />
                  <Point X="1.50845690918" Y="29.769056640625" />
                  <Point X="1.918211914062" Y="29.620435546875" />
                  <Point X="1.93104309082" Y="29.61578125" />
                  <Point X="2.326354003906" Y="29.430908203125" />
                  <Point X="2.338696044922" Y="29.42513671875" />
                  <Point X="2.720592041016" Y="29.202642578125" />
                  <Point X="2.732518310547" Y="29.1956953125" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="3.061382080078" Y="28.94384765625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.204132324219" Y="27.41403125" />
                  <Point X="2.202044677734" Y="27.392326171875" />
                  <Point X="2.210123779297" Y="27.325326171875" />
                  <Point X="2.210416015625" Y="27.32290234375" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.260138916016" Y="27.23971484375" />
                  <Point X="2.260143554688" Y="27.239708984375" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.336036865234" Y="27.18274609375" />
                  <Point X="2.338232666016" Y="27.181255859375" />
                  <Point X="2.360337646484" Y="27.172978515625" />
                  <Point X="2.427337402344" Y="27.164900390625" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.526145996094" Y="27.186666015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.593167480469" Y="27.222517578125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.195513671875" Y="27.751716796875" />
                  <Point X="4.202592773438" Y="27.74187890625" />
                  <Point X="4.387513183594" Y="27.436296875" />
                  <Point X="4.383401367188" Y="27.433140625" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.223610595703" Y="26.511087890625" />
                  <Point X="3.213118652344" Y="26.49149609375" />
                  <Point X="3.192346679688" Y="26.417220703125" />
                  <Point X="3.191594726562" Y="26.41453125" />
                  <Point X="3.190778808594" Y="26.390966796875" />
                  <Point X="3.207830566406" Y="26.308326171875" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.262025146484" Y="26.2174609375" />
                  <Point X="3.263709960938" Y="26.214900390625" />
                  <Point X="3.280947265625" Y="26.1988203125" />
                  <Point X="3.348156494141" Y="26.160986328125" />
                  <Point X="3.348161865234" Y="26.160982421875" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.459436767578" Y="26.141609375" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.525327148438" Y="26.14769140625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.9361484375" Y="25.96387109375" />
                  <Point X="4.939187988281" Y="25.95138671875" />
                  <Point X="4.997858886719" Y="25.5745546875" />
                  <Point X="4.995814941406" Y="25.5740078125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087402344" Y="25.232818359375" />
                  <Point X="3.639809082031" Y="25.18121484375" />
                  <Point X="3.639809326172" Y="25.18121484375" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.568697998047" Y="25.098669921875" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985595703" Y="25.074736328125" />
                  <Point X="3.539129882812" Y="24.9815" />
                  <Point X="3.538483398438" Y="24.9593125" />
                  <Point X="3.556339111328" Y="24.866078125" />
                  <Point X="3.556985595703" Y="24.862703125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.620325927734" Y="24.772982421875" />
                  <Point X="3.636577392578" Y="24.758091796875" />
                  <Point X="3.725855712891" Y="24.70648828125" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.786583007812" Y="24.687443359375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.950129394531" Y="24.044861328125" />
                  <Point X="4.948430664062" Y="24.03359375" />
                  <Point X="4.874546386719" Y="23.709822265625" />
                  <Point X="4.867020507812" Y="23.7108125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394833740234" Y="23.901658203125" />
                  <Point X="3.219612060547" Y="23.86357421875" />
                  <Point X="3.21326953125" Y="23.8621953125" />
                  <Point X="3.185445068359" Y="23.845302734375" />
                  <Point X="3.079534667969" Y="23.717923828125" />
                  <Point X="3.075706054688" Y="23.7133203125" />
                  <Point X="3.064357666016" Y="23.6859296875" />
                  <Point X="3.049178222656" Y="23.520970703125" />
                  <Point X="3.048628662109" Y="23.514998046875" />
                  <Point X="3.056361816406" Y="23.483375" />
                  <Point X="3.15333203125" Y="23.332544921875" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.210606445312" Y="23.282119140625" />
                  <Point X="4.339073730469" Y="22.416216796875" />
                  <Point X="4.208915527344" Y="22.205599609375" />
                  <Point X="4.204125488281" Y="22.19784765625" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="4.049362304688" Y="21.99258984375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.528799072266" Y="22.78434765625" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489075439453" Y="22.78075390625" />
                  <Point X="2.315828369141" Y="22.689576171875" />
                  <Point X="2.309557373047" Y="22.686275390625" />
                  <Point X="2.288598876953" Y="22.665314453125" />
                  <Point X="2.197420166016" Y="22.492068359375" />
                  <Point X="2.194119873047" Y="22.485796875" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.226825439453" Y="22.24508203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.261174316406" Y="22.17451171875" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.840975830078" Y="20.81384375" />
                  <Point X="2.835281738281" Y="20.809775390625" />
                  <Point X="2.679776123047" Y="20.709119140625" />
                  <Point X="2.672074707031" Y="20.71915625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.464870727539" Y="22.151765625" />
                  <Point X="1.457420776367" Y="22.156552734375" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.200868041992" Y="22.14358203125" />
                  <Point X="1.192725463867" Y="22.1428359375" />
                  <Point X="1.165330200195" Y="22.13148828125" />
                  <Point X="0.991633789062" Y="21.987064453125" />
                  <Point X="0.985346496582" Y="21.9818359375" />
                  <Point X="0.96845690918" Y="21.954013671875" />
                  <Point X="0.916522521973" Y="21.715076171875" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.921604614258" Y="21.630982421875" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="0.999714050293" Y="20.0379296875" />
                  <Point X="0.99436340332" Y="20.036755859375" />
                  <Point X="0.860200378418" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#214" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.186493938389" Y="5.051129362463" Z="2.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="-0.220866811266" Y="5.073090251485" Z="2.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.45" />
                  <Point X="-1.010742330347" Y="4.976273180148" Z="2.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.45" />
                  <Point X="-1.709143720482" Y="4.454557701065" Z="2.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.45" />
                  <Point X="-1.708773506062" Y="4.439604238381" Z="2.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.45" />
                  <Point X="-1.743389593664" Y="4.339369066807" Z="2.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.45" />
                  <Point X="-1.842425240881" Y="4.301456096782" Z="2.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.45" />
                  <Point X="-2.127304019744" Y="4.600799361499" Z="2.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.45" />
                  <Point X="-2.157074521177" Y="4.59724461012" Z="2.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.45" />
                  <Point X="-2.80721617181" Y="4.231673251636" Z="2.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.45" />
                  <Point X="-3.014699499519" Y="3.163132353748" Z="2.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.45" />
                  <Point X="-3.001263224259" Y="3.137324409654" Z="2.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.45" />
                  <Point X="-2.996161271317" Y="3.05264229657" Z="2.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.45" />
                  <Point X="-3.057752055953" Y="2.994301474914" Z="2.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.45" />
                  <Point X="-3.77072699455" Y="3.365494449226" Z="2.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.45" />
                  <Point X="-3.808013230579" Y="3.360074237936" Z="2.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.45" />
                  <Point X="-4.219551951918" Y="2.826017456199" Z="2.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.45" />
                  <Point X="-3.726293530182" Y="1.633647701974" Z="2.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.45" />
                  <Point X="-3.6955233861" Y="1.608838405251" Z="2.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.45" />
                  <Point X="-3.667683445081" Y="1.551625714951" Z="2.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.45" />
                  <Point X="-3.693615711366" Y="1.493523541824" Z="2.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.45" />
                  <Point X="-4.779341102737" Y="1.609966627755" Z="2.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.45" />
                  <Point X="-4.821957150459" Y="1.59470444553" Z="2.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.45" />
                  <Point X="-4.975887284906" Y="1.017278921801" Z="2.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.45" />
                  <Point X="-3.628393228122" Y="0.062957506772" Z="2.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.45" />
                  <Point X="-3.575591242911" Y="0.048396148711" Z="2.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.45" />
                  <Point X="-3.548484689604" Y="0.028765703159" Z="2.45" />
                  <Point X="-3.539556741714" Y="0" Z="2.45" />
                  <Point X="-3.539879911531" Y="-0.001041247902" Z="2.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.45" />
                  <Point X="-3.549777352213" Y="-0.030479834423" Z="2.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.45" />
                  <Point X="-5.008494560651" Y="-0.432754549984" Z="2.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.45" />
                  <Point X="-5.057613965754" Y="-0.465612645031" Z="2.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.45" />
                  <Point X="-4.977919268251" Y="-1.008237350879" Z="2.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.45" />
                  <Point X="-3.276020976027" Y="-1.314349342442" Z="2.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.45" />
                  <Point X="-3.21823377033" Y="-1.307407791875" Z="2.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.45" />
                  <Point X="-3.192945447377" Y="-1.323488969798" Z="2.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.45" />
                  <Point X="-4.457399602884" Y="-2.316741579739" Z="2.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.45" />
                  <Point X="-4.492646151143" Y="-2.36885086783" Z="2.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.45" />
                  <Point X="-4.197229054901" Y="-2.8598179251" Z="2.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.45" />
                  <Point X="-2.617882159948" Y="-2.581496362951" Z="2.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.45" />
                  <Point X="-2.572233461416" Y="-2.556097007715" Z="2.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.45" />
                  <Point X="-3.273920650217" Y="-3.817196222544" Z="2.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.45" />
                  <Point X="-3.285622684072" Y="-3.873251993604" Z="2.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.45" />
                  <Point X="-2.875127402235" Y="-4.187005323472" Z="2.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.45" />
                  <Point X="-2.234078563076" Y="-4.166690687133" Z="2.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.45" />
                  <Point X="-2.217210726085" Y="-4.150430844488" Z="2.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.45" />
                  <Point X="-1.957441101558" Y="-3.984793214201" Z="2.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.45" />
                  <Point X="-1.650518334014" Y="-4.011522000306" Z="2.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.45" />
                  <Point X="-1.423291570547" Y="-4.219570345972" Z="2.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.45" />
                  <Point X="-1.411414560544" Y="-4.866708283966" Z="2.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.45" />
                  <Point X="-1.402769443136" Y="-4.882160968309" Z="2.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.45" />
                  <Point X="-1.106931222036" Y="-4.957615955897" Z="2.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="-0.431080309218" Y="-3.570997644404" Z="2.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="-0.411367271071" Y="-3.510532356873" Z="2.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="-0.244508472357" Y="-3.280125775053" Z="2.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.45" />
                  <Point X="0.008850607003" Y="-3.206986270235" Z="2.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.45" />
                  <Point X="0.259078588361" Y="-3.291113398895" Z="2.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.45" />
                  <Point X="0.803674551183" Y="-4.961538385412" Z="2.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.45" />
                  <Point X="0.823967980316" Y="-5.012618488993" Z="2.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.45" />
                  <Point X="1.00426467133" Y="-4.9796303915" Z="2.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.45" />
                  <Point X="0.9650208274" Y="-3.331211432098" Z="2.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.45" />
                  <Point X="0.959225679323" Y="-3.264264699969" Z="2.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.45" />
                  <Point X="1.017447398025" Y="-3.020098261636" Z="2.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.45" />
                  <Point X="1.199286182159" Y="-2.874926165926" Z="2.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.45" />
                  <Point X="1.431675509524" Y="-2.859013070496" Z="2.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.45" />
                  <Point X="2.626251020474" Y="-4.280000829135" Z="2.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.45" />
                  <Point X="2.668866543488" Y="-4.32223624741" Z="2.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.45" />
                  <Point X="2.863884276177" Y="-4.195562136892" Z="2.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.45" />
                  <Point X="2.298319646506" Y="-2.769207301493" Z="2.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.45" />
                  <Point X="2.269873606618" Y="-2.714749922581" Z="2.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.45" />
                  <Point X="2.235511556145" Y="-2.499937069511" Z="2.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.45" />
                  <Point X="2.332961308861" Y="-2.323389753822" Z="2.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.45" />
                  <Point X="2.513756880931" Y="-2.233574402873" Z="2.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.45" />
                  <Point X="4.018206295614" Y="-3.019429853769" Z="2.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.45" />
                  <Point X="4.071214540824" Y="-3.037845960853" Z="2.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.45" />
                  <Point X="4.245293595585" Y="-2.789404440447" Z="2.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.45" />
                  <Point X="3.234888359611" Y="-1.646932439825" Z="2.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.45" />
                  <Point X="3.189232739938" Y="-1.609133323086" Z="2.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.45" />
                  <Point X="3.0928119668" Y="-1.452331381231" Z="2.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.45" />
                  <Point X="3.111825152839" Y="-1.282761485542" Z="2.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.45" />
                  <Point X="3.224078125959" Y="-1.15400559323" Z="2.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.45" />
                  <Point X="4.854337931107" Y="-1.307479806147" Z="2.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.45" />
                  <Point X="4.909956196099" Y="-1.301488869315" Z="2.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.45" />
                  <Point X="4.993414786882" Y="-0.931313750835" Z="2.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.45" />
                  <Point X="3.793367853406" Y="-0.232979590089" Z="2.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.45" />
                  <Point X="3.744721043141" Y="-0.218942681483" Z="2.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.45" />
                  <Point X="3.653503720779" Y="-0.164867473465" Z="2.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.45" />
                  <Point X="3.599290392405" Y="-0.093235630079" Z="2.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.45" />
                  <Point X="3.582081058019" Y="0.003374901096" Z="2.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.45" />
                  <Point X="3.601875717621" Y="0.09908126511" Z="2.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.45" />
                  <Point X="3.658674371212" Y="0.169206162759" Z="2.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.45" />
                  <Point X="5.002599963901" Y="0.55699234268" Z="2.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.45" />
                  <Point X="5.04571295122" Y="0.583947735033" Z="2.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.45" />
                  <Point X="4.978573208071" Y="1.006980507907" Z="2.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.45" />
                  <Point X="3.512644752408" Y="1.22854395631" Z="2.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.45" />
                  <Point X="3.459832089817" Y="1.222458807423" Z="2.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.45" />
                  <Point X="3.366527808721" Y="1.235838040748" Z="2.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.45" />
                  <Point X="3.297639754699" Y="1.27622272913" Z="2.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.45" />
                  <Point X="3.250643765002" Y="1.349707819428" Z="2.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.45" />
                  <Point X="3.234343930832" Y="1.43503781015" Z="2.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.45" />
                  <Point X="3.257134493783" Y="1.511947005217" Z="2.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.45" />
                  <Point X="4.407683339794" Y="2.424753287114" Z="2.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.45" />
                  <Point X="4.440006401379" Y="2.467233710454" Z="2.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.45" />
                  <Point X="4.229942470277" Y="2.812201216807" Z="2.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.45" />
                  <Point X="2.562011428072" Y="2.297098060968" Z="2.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.45" />
                  <Point X="2.507073275549" Y="2.266248769348" Z="2.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.45" />
                  <Point X="2.427166432916" Y="2.245821611756" Z="2.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.45" />
                  <Point X="2.357955221162" Y="2.255401166272" Z="2.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.45" />
                  <Point X="2.295357433963" Y="2.29906963922" Z="2.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.45" />
                  <Point X="2.253608091003" Y="2.362592010916" Z="2.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.45" />
                  <Point X="2.246279173309" Y="2.432396300333" Z="2.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.45" />
                  <Point X="3.098527291892" Y="3.950127653883" Z="2.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.45" />
                  <Point X="3.115522191372" Y="4.011580282588" Z="2.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.45" />
                  <Point X="2.739603735642" Y="4.277126738268" Z="2.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.45" />
                  <Point X="2.34137968105" Y="4.50747986903" Z="2.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.45" />
                  <Point X="1.929104565101" Y="4.698720460821" Z="2.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.45" />
                  <Point X="1.493885111295" Y="4.853806303406" Z="2.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.45" />
                  <Point X="0.839177475654" Y="5.008676604885" Z="2.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.45" />
                  <Point X="0.006750623164" Y="4.380318143488" Z="2.45" />
                  <Point X="0" Y="4.355124473572" Z="2.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>