<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#141" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1074" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.658127807617" Y="21.133580078125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362854004" Y="21.532625" />
                  <Point X="0.503605163574" Y="21.588466796875" />
                  <Point X="0.378635070801" Y="21.768525390625" />
                  <Point X="0.356752349854" Y="21.790978515625" />
                  <Point X="0.330497161865" Y="21.81022265625" />
                  <Point X="0.302494110107" Y="21.82433203125" />
                  <Point X="0.242518844604" Y="21.8429453125" />
                  <Point X="0.049134975433" Y="21.90296484375" />
                  <Point X="0.020976791382" Y="21.907234375" />
                  <Point X="-0.008664384842" Y="21.907234375" />
                  <Point X="-0.036825450897" Y="21.90296484375" />
                  <Point X="-0.096800727844" Y="21.884349609375" />
                  <Point X="-0.290184448242" Y="21.82433203125" />
                  <Point X="-0.318184143066" Y="21.810224609375" />
                  <Point X="-0.34443963623" Y="21.79098046875" />
                  <Point X="-0.366324035645" Y="21.7685234375" />
                  <Point X="-0.405081726074" Y="21.7126796875" />
                  <Point X="-0.530051696777" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.856745483398" Y="20.346380859375" />
                  <Point X="-0.916584411621" Y="20.12305859375" />
                  <Point X="-1.079326782227" Y="20.1546484375" />
                  <Point X="-1.145112792969" Y="20.17157421875" />
                  <Point X="-1.246417480469" Y="20.197638671875" />
                  <Point X="-1.226377685547" Y="20.34985546875" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.230836791992" Y="20.555806640625" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323645141602" Y="20.868796875" />
                  <Point X="-1.378862915039" Y="20.917220703125" />
                  <Point X="-1.55690612793" Y="21.073361328125" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643028320312" Y="21.109033203125" />
                  <Point X="-1.716314331055" Y="21.1138359375" />
                  <Point X="-1.952617431641" Y="21.12932421875" />
                  <Point X="-1.983414672852" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.103723388672" Y="21.064396484375" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480148193359" Y="20.711509765625" />
                  <Point X="-2.801713623047" Y="20.91061328125" />
                  <Point X="-2.892783447266" Y="20.980734375" />
                  <Point X="-3.104722412109" Y="21.143919921875" />
                  <Point X="-2.599984375" Y="22.01815234375" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858154297" Y="22.3523515625" />
                  <Point X="-2.406587646484" Y="22.3838828125" />
                  <Point X="-2.405576904297" Y="22.4148203125" />
                  <Point X="-2.414565185547" Y="22.444439453125" />
                  <Point X="-2.428786865234" Y="22.47326953125" />
                  <Point X="-2.446812011719" Y="22.498419921875" />
                  <Point X="-2.464155029297" Y="22.51576171875" />
                  <Point X="-2.489310791016" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.5477578125" Y="22.55698828125" />
                  <Point X="-2.578691894531" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.623355712891" Y="21.970587890625" />
                  <Point X="-3.818022705078" Y="21.858197265625" />
                  <Point X="-3.822873291016" Y="21.8645703125" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.148150878906" Y="22.31562109375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.414287597656" Y="23.264892578125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.063912109375" Y="23.554697265625" />
                  <Point X="-3.055374755859" Y="23.57680859375" />
                  <Point X="-3.052032958984" Y="23.58720703125" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037353516" Y="23.64139453125" />
                  <Point X="-3.042736328125" Y="23.664263671875" />
                  <Point X="-3.048882568359" Y="23.68630078125" />
                  <Point X="-3.060887939453" Y="23.71528515625" />
                  <Point X="-3.073567382812" Y="23.737125" />
                  <Point X="-3.091552490234" Y="23.754853515625" />
                  <Point X="-3.11032421875" Y="23.76897265625" />
                  <Point X="-3.119243408203" Y="23.774923828125" />
                  <Point X="-3.13945703125" Y="23.7868203125" />
                  <Point X="-3.168721923828" Y="23.798044921875" />
                  <Point X="-3.200608642578" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.474352539062" Y="23.642046875" />
                  <Point X="-4.732102539062" Y="23.608115234375" />
                  <Point X="-4.732395019531" Y="23.609259765625" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.8513515625" Y="24.128123046875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.884113769531" Y="24.6854765625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.512826416016" Y="24.787494140625" />
                  <Point X="-3.490685791016" Y="24.799279296875" />
                  <Point X="-3.481160888672" Y="24.805091796875" />
                  <Point X="-3.459982177734" Y="24.8197890625" />
                  <Point X="-3.436024169922" Y="24.84131640625" />
                  <Point X="-3.415408935547" Y="24.871005859375" />
                  <Point X="-3.40579296875" Y="24.89280078125" />
                  <Point X="-3.401979003906" Y="24.90298828125" />
                  <Point X="-3.39491796875" Y="24.92573828125" />
                  <Point X="-3.389474609375" Y="24.9544765625" />
                  <Point X="-3.390296386719" Y="24.9876015625" />
                  <Point X="-3.394733154297" Y="25.009478515625" />
                  <Point X="-3.397106689453" Y="25.01875390625" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.417487304688" Y="25.0708359375" />
                  <Point X="-3.439801757812" Y="25.09961328125" />
                  <Point X="-3.458194335938" Y="25.11569140625" />
                  <Point X="-3.466551513672" Y="25.1222109375" />
                  <Point X="-3.487727539062" Y="25.136908203125" />
                  <Point X="-3.501915527344" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.665404296875" Y="25.46130859375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.890020996094" Y="25.534109375" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.789714355469" Y="26.105298828125" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.017442138672" Y="26.332939453125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703141357422" Y="26.305263671875" />
                  <Point X="-3.688600585938" Y="26.30984765625" />
                  <Point X="-3.641715332031" Y="26.324630859375" />
                  <Point X="-3.622783203125" Y="26.332958984375" />
                  <Point X="-3.604039306641" Y="26.343779296875" />
                  <Point X="-3.587354492188" Y="26.35601171875" />
                  <Point X="-3.573713623047" Y="26.37156640625" />
                  <Point X="-3.561299316406" Y="26.389296875" />
                  <Point X="-3.551351318359" Y="26.407431640625" />
                  <Point X="-3.545516845703" Y="26.421517578125" />
                  <Point X="-3.526703857422" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.539090332031" Y="26.60290234375" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.251756835938" Y="27.1930625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.335789550781" Y="27.29740625" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-3.989042480469" Y="27.8520546875" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.361290771484" Y="27.93394921875" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729492188" Y="27.836341796875" />
                  <Point X="-3.167087402344" Y="27.82983203125" />
                  <Point X="-3.146793457031" Y="27.825794921875" />
                  <Point X="-3.126542236328" Y="27.8240234375" />
                  <Point X="-3.061244140625" Y="27.818310546875" />
                  <Point X="-3.040560791016" Y="27.81876171875" />
                  <Point X="-3.019102294922" Y="27.821587890625" />
                  <Point X="-2.999013427734" Y="27.826505859375" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962210205078" Y="27.84728125" />
                  <Point X="-2.946076660156" Y="27.86023046875" />
                  <Point X="-2.931702148438" Y="27.87460546875" />
                  <Point X="-2.885353027344" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845207519531" Y="28.05637109375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.157661621094" Y="28.6801328125" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-3.144117919922" Y="28.754662109375" />
                  <Point X="-2.700621582031" Y="29.094685546875" />
                  <Point X="-2.555555664062" Y="29.17528125" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.090533447266" Y="29.29143359375" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028891967773" Y="29.21480078125" />
                  <Point X="-2.01231237793" Y="29.200888671875" />
                  <Point X="-1.995116210938" Y="29.1893984375" />
                  <Point X="-1.972576660156" Y="29.1776640625" />
                  <Point X="-1.899900146484" Y="29.139830078125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718261719" Y="29.126728515625" />
                  <Point X="-1.839268554688" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777452880859" Y="29.134482421875" />
                  <Point X="-1.753976196289" Y="29.14420703125" />
                  <Point X="-1.678278686523" Y="29.1755625" />
                  <Point X="-1.660145507812" Y="29.185509765625" />
                  <Point X="-1.642416137695" Y="29.197923828125" />
                  <Point X="-1.626864013672" Y="29.2115625" />
                  <Point X="-1.61463269043" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.587839111328" Y="29.29015625" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.523762817383" Y="29.648841796875" />
                  <Point X="-0.949623046875" Y="29.80980859375" />
                  <Point X="-0.773773193359" Y="29.830390625" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.179648239136" Y="29.457037109375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.29371472168" Y="29.836791015625" />
                  <Point X="0.307419342041" Y="29.8879375" />
                  <Point X="0.342738311768" Y="29.88423828125" />
                  <Point X="0.84403137207" Y="29.831740234375" />
                  <Point X="0.989542419434" Y="29.796609375" />
                  <Point X="1.481039794922" Y="29.6779453125" />
                  <Point X="1.574602050781" Y="29.644009765625" />
                  <Point X="1.894650024414" Y="29.52792578125" />
                  <Point X="1.986226928711" Y="29.485099609375" />
                  <Point X="2.294554443359" Y="29.340904296875" />
                  <Point X="2.383078125" Y="29.289330078125" />
                  <Point X="2.680974121094" Y="29.11577734375" />
                  <Point X="2.764420410156" Y="29.05643359375" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.351545898438" Y="27.904375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.139487792969" Y="27.53290625" />
                  <Point X="2.129404052734" Y="27.50181640625" />
                  <Point X="2.127994628906" Y="27.497048828125" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108398193359" Y="27.40949609375" />
                  <Point X="2.109042236328" Y="27.37416015625" />
                  <Point X="2.109709472656" Y="27.364517578125" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.150239990234" Y="27.232484375" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.200976806641" Y="27.16439453125" />
                  <Point X="2.229262939453" Y="27.140923828125" />
                  <Point X="2.236584472656" Y="27.135421875" />
                  <Point X="2.284906738281" Y="27.1026328125" />
                  <Point X="2.304952880859" Y="27.09226953125" />
                  <Point X="2.327040771484" Y="27.08400390625" />
                  <Point X="2.348970214844" Y="27.07866015625" />
                  <Point X="2.365404541016" Y="27.0766796875" />
                  <Point X="2.418395019531" Y="27.0702890625" />
                  <Point X="2.445623291016" Y="27.07094140625" />
                  <Point X="2.483532958984" Y="27.077361328125" />
                  <Point X="2.492214355469" Y="27.079251953125" />
                  <Point X="2.553495361328" Y="27.095640625" />
                  <Point X="2.565289794922" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.727639404297" Y="27.76780859375" />
                  <Point X="3.967326416016" Y="27.90619140625" />
                  <Point X="4.123272460938" Y="27.6894609375" />
                  <Point X="4.169790527344" Y="27.61258984375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.499322509766" Y="26.874509765625" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221421386719" Y="26.66023828125" />
                  <Point X="3.203974609375" Y="26.641626953125" />
                  <Point X="3.190296386719" Y="26.623783203125" />
                  <Point X="3.146192382812" Y="26.56624609375" />
                  <Point X="3.132953369141" Y="26.542638671875" />
                  <Point X="3.119388427734" Y="26.50746875" />
                  <Point X="3.116534423828" Y="26.4988671875" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.101921875" Y="26.35149609375" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.120678955078" Y="26.26898046875" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.147659179688" Y="26.218447265625" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198895751953" Y="26.145447265625" />
                  <Point X="3.216138671875" Y="26.129359375" />
                  <Point X="3.234348632812" Y="26.116033203125" />
                  <Point X="3.250834228516" Y="26.10675390625" />
                  <Point X="3.303990722656" Y="26.076830078125" />
                  <Point X="3.320521484375" Y="26.0695" />
                  <Point X="3.35612109375" Y="26.0594375" />
                  <Point X="3.378410888672" Y="26.0564921875" />
                  <Point X="3.450281738281" Y="26.046994140625" />
                  <Point X="3.462698242188" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.570277832031" Y="26.18944140625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.860595214844" Y="25.838654296875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.024331054688" Y="25.412052734375" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704788574219" Y="25.325583984375" />
                  <Point X="3.681550537109" Y="25.3150703125" />
                  <Point X="3.659651611328" Y="25.3024140625" />
                  <Point X="3.589040527344" Y="25.261599609375" />
                  <Point X="3.574311523438" Y="25.25109765625" />
                  <Point X="3.547528564453" Y="25.22557421875" />
                  <Point X="3.534389160156" Y="25.208830078125" />
                  <Point X="3.492022705078" Y="25.154845703125" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.459301025391" Y="25.069734375" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.449558837891" Y="24.918576171875" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492025634766" Y="24.782591796875" />
                  <Point X="3.505165039062" Y="24.765849609375" />
                  <Point X="3.547531494141" Y="24.71186328125" />
                  <Point X="3.559994873047" Y="24.698767578125" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.610934570312" Y="24.663185546875" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692708496094" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.708891113281" Y="24.341958984375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.051271484375" />
                  <Point X="4.836241210938" Y="23.968970703125" />
                  <Point X="4.801174316406" Y="23.815302734375" />
                  <Point X="3.781286865234" Y="23.949572265625" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.331679199219" Y="23.9851484375" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163974365234" Y="23.94340234375" />
                  <Point X="3.136147460938" Y="23.926509765625" />
                  <Point X="3.112397705078" Y="23.906041015625" />
                  <Point X="3.086418945312" Y="23.874796875" />
                  <Point X="3.002653564453" Y="23.774052734375" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.966034179688" Y="23.654171875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.000236572266" Y="23.395005859375" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.031499267578" Y="22.53248046875" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.124811523438" Y="22.250216796875" />
                  <Point X="4.085968261719" Y="22.195025390625" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.118826660156" Y="22.63953125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.703071533203" Y="22.849412109375" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833251953" Y="22.864822265625" />
                  <Point X="2.402337646484" Y="22.84245703125" />
                  <Point X="2.265315185547" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.182166503906" Y="22.667064453125" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.104913330078" Y="22.385587890625" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.743571289062" Y="21.1489765625" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781839599609" Y="20.888349609375" />
                  <Point X="2.738423828125" Y="20.86024609375" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="2.001111938477" Y="21.749625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721924194336" Y="22.099443359375" />
                  <Point X="1.671473388672" Y="22.13187890625" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417101806641" Y="22.258880859375" />
                  <Point X="1.361925415039" Y="22.2538046875" />
                  <Point X="1.184014892578" Y="22.23743359375" />
                  <Point X="1.156362426758" Y="22.2306015625" />
                  <Point X="1.128976928711" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="1.061989379883" Y="22.169111328125" />
                  <Point X="0.92461151123" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.862885498047" Y="21.91558203125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.987440124512" Y="20.403087890625" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.975725219727" Y="20.129927734375" />
                  <Point X="0.935556335449" Y="20.12262890625" />
                  <Point X="0.929315612793" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058424804688" Y="20.247365234375" />
                  <Point X="-1.121441650391" Y="20.263578125" />
                  <Point X="-1.141245727539" Y="20.268673828125" />
                  <Point X="-1.132190429688" Y="20.337455078125" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333984375" Y="20.502306640625" />
                  <Point X="-1.137662231445" Y="20.57433984375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573730469" Y="20.905138671875" />
                  <Point X="-1.250209106445" Y="20.929064453125" />
                  <Point X="-1.26100793457" Y="20.94022265625" />
                  <Point X="-1.316225708008" Y="20.988646484375" />
                  <Point X="-1.494268920898" Y="21.144787109375" />
                  <Point X="-1.506739746094" Y="21.15403515625" />
                  <Point X="-1.533022827148" Y="21.17037890625" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531982422" Y="21.189775390625" />
                  <Point X="-1.59131640625" Y="21.194525390625" />
                  <Point X="-1.621458251953" Y="21.201552734375" />
                  <Point X="-1.636815917969" Y="21.203830078125" />
                  <Point X="-1.710101928711" Y="21.2086328125" />
                  <Point X="-1.946405029297" Y="21.22412109375" />
                  <Point X="-1.961928833008" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048095703" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436279297" Y="21.184189453125" />
                  <Point X="-2.156502197266" Y="21.14338671875" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201660156" Y="20.83751953125" />
                  <Point X="-2.747598876953" Y="20.98884375" />
                  <Point X="-2.834825927734" Y="21.056005859375" />
                  <Point X="-2.980863525391" Y="21.16844921875" />
                  <Point X="-2.517711914062" Y="21.97065234375" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849365234" Y="22.289919921875" />
                  <Point X="-2.323946289062" Y="22.318890625" />
                  <Point X="-2.319682617188" Y="22.333822265625" />
                  <Point X="-2.313412109375" Y="22.365353515625" />
                  <Point X="-2.311638183594" Y="22.38078125" />
                  <Point X="-2.310627441406" Y="22.41171875" />
                  <Point X="-2.314670410156" Y="22.44240625" />
                  <Point X="-2.323658691406" Y="22.472025390625" />
                  <Point X="-2.3293671875" Y="22.486466796875" />
                  <Point X="-2.343588867188" Y="22.515296875" />
                  <Point X="-2.3515703125" Y="22.528609375" />
                  <Point X="-2.369595458984" Y="22.553759765625" />
                  <Point X="-2.379639160156" Y="22.56559765625" />
                  <Point X="-2.396982177734" Y="22.582939453125" />
                  <Point X="-2.408821533203" Y="22.592984375" />
                  <Point X="-2.433977294922" Y="22.611009765625" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122802734" Y="22.63320703125" />
                  <Point X="-2.490563232422" Y="22.6389140625" />
                  <Point X="-2.520181152344" Y="22.6478984375" />
                  <Point X="-2.550869140625" Y="22.6519375" />
                  <Point X="-2.581803222656" Y="22.650923828125" />
                  <Point X="-2.597226806641" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.670855712891" Y="22.052859375" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-4.004016357422" Y="22.25940625" />
                  <Point X="-4.066558105469" Y="22.364279296875" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.356455322266" Y="23.1895234375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.002552734375" Y="23.47369140625" />
                  <Point X="-2.983399902344" Y="23.504271484375" />
                  <Point X="-2.975288574219" Y="23.520478515625" />
                  <Point X="-2.966751220703" Y="23.54258984375" />
                  <Point X="-2.960067626953" Y="23.56338671875" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953369141" Y="23.597599609375" />
                  <Point X="-2.947838623047" Y="23.62908203125" />
                  <Point X="-2.947081787109" Y="23.644296875" />
                  <Point X="-2.947780761719" Y="23.667166015625" />
                  <Point X="-2.951228759766" Y="23.68978515625" />
                  <Point X="-2.957375" Y="23.711822265625" />
                  <Point X="-2.961113525391" Y="23.722654296875" />
                  <Point X="-2.973118896484" Y="23.751638671875" />
                  <Point X="-2.978730224609" Y="23.762982421875" />
                  <Point X="-2.991409667969" Y="23.784822265625" />
                  <Point X="-3.006876708984" Y="23.80478125" />
                  <Point X="-3.024861816406" Y="23.822509765625" />
                  <Point X="-3.034447998047" Y="23.830775390625" />
                  <Point X="-3.053219726562" Y="23.84489453125" />
                  <Point X="-3.071058105469" Y="23.856796875" />
                  <Point X="-3.091271728516" Y="23.868693359375" />
                  <Point X="-3.105436279297" Y="23.87551953125" />
                  <Point X="-3.134701171875" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891142578125" />
                  <Point X="-3.181688232422" Y="23.897623046875" />
                  <Point X="-3.197304931641" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.486752441406" Y="23.736234375" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.75730859375" Y="24.141572265625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.859525878906" Y="24.593712890625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.498033203125" Y="24.6912109375" />
                  <Point X="-3.477983642578" Y="24.699115234375" />
                  <Point X="-3.468188964844" Y="24.703634765625" />
                  <Point X="-3.446048339844" Y="24.715419921875" />
                  <Point X="-3.426998535156" Y="24.727044921875" />
                  <Point X="-3.405819824219" Y="24.7417421875" />
                  <Point X="-3.396487304688" Y="24.749125" />
                  <Point X="-3.372529296875" Y="24.77065234375" />
                  <Point X="-3.357990966797" Y="24.7871328125" />
                  <Point X="-3.337375732422" Y="24.816822265625" />
                  <Point X="-3.328492675781" Y="24.832658203125" />
                  <Point X="-3.318876708984" Y="24.854453125" />
                  <Point X="-3.311248779297" Y="24.874828125" />
                  <Point X="-3.304187744141" Y="24.897578125" />
                  <Point X="-3.301577636719" Y="24.90805859375" />
                  <Point X="-3.296134277344" Y="24.936796875" />
                  <Point X="-3.29450390625" Y="24.95683203125" />
                  <Point X="-3.295325683594" Y="24.98995703125" />
                  <Point X="-3.297191894531" Y="25.006484375" />
                  <Point X="-3.301628662109" Y="25.028361328125" />
                  <Point X="-3.306375732422" Y="25.046912109375" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.317669189453" Y="25.080787109375" />
                  <Point X="-3.330988769531" Y="25.1101171875" />
                  <Point X="-3.342413085938" Y="25.12905078125" />
                  <Point X="-3.364727539062" Y="25.157828125" />
                  <Point X="-3.377277587891" Y="25.171138671875" />
                  <Point X="-3.395670166016" Y="25.187216796875" />
                  <Point X="-3.412384521484" Y="25.200255859375" />
                  <Point X="-3.433560546875" Y="25.214953125" />
                  <Point X="-3.440483154297" Y="25.219328125" />
                  <Point X="-3.465598388672" Y="25.232826171875" />
                  <Point X="-3.496558837891" Y="25.245634765625" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.64081640625" Y="25.553072265625" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.698021484375" Y="26.080451171875" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.029842041016" Y="26.238751953125" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703613281" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704885742188" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.660037597656" Y="26.219244140625" />
                  <Point X="-3.61315234375" Y="26.23402734375" />
                  <Point X="-3.603462890625" Y="26.237671875" />
                  <Point X="-3.584530761719" Y="26.246" />
                  <Point X="-3.575288085938" Y="26.25068359375" />
                  <Point X="-3.556544189453" Y="26.26150390625" />
                  <Point X="-3.547869140625" Y="26.2671640625" />
                  <Point X="-3.531184326172" Y="26.279396484375" />
                  <Point X="-3.515929199219" Y="26.293375" />
                  <Point X="-3.502288330078" Y="26.3089296875" />
                  <Point X="-3.495892822266" Y="26.317078125" />
                  <Point X="-3.483478515625" Y="26.33480859375" />
                  <Point X="-3.478008300781" Y="26.343607421875" />
                  <Point X="-3.468060302734" Y="26.3617421875" />
                  <Point X="-3.457748046875" Y="26.3851640625" />
                  <Point X="-3.438935058594" Y="26.43058203125" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429711181641" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012207031" Y="26.60453125" />
                  <Point X="-3.443509521484" Y="26.623810546875" />
                  <Point X="-3.454824707031" Y="26.64676953125" />
                  <Point X="-3.477524414062" Y="26.690375" />
                  <Point X="-3.482800048828" Y="26.699287109375" />
                  <Point X="-3.494291992188" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521501708984" Y="26.748912109375" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.193924316406" Y="27.268431640625" />
                  <Point X="-4.227613769531" Y="27.294283203125" />
                  <Point X="-4.002296142578" Y="27.6803046875" />
                  <Point X="-3.914061279297" Y="27.793720703125" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.408790771484" Y="27.851677734375" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244924804688" Y="27.757720703125" />
                  <Point X="-3.225997314453" Y="27.749390625" />
                  <Point X="-3.216302001953" Y="27.745740234375" />
                  <Point X="-3.195659912109" Y="27.73923046875" />
                  <Point X="-3.185622802734" Y="27.736658203125" />
                  <Point X="-3.165328857422" Y="27.73262109375" />
                  <Point X="-3.155072021484" Y="27.73115625" />
                  <Point X="-3.134820800781" Y="27.729384765625" />
                  <Point X="-3.069522705078" Y="27.723671875" />
                  <Point X="-3.059172363281" Y="27.723333984375" />
                  <Point X="-3.038489013672" Y="27.72378515625" />
                  <Point X="-3.028156005859" Y="27.72457421875" />
                  <Point X="-3.006697509766" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423583984" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938450195312" Y="27.750447265625" />
                  <Point X="-2.929421630859" Y="27.755529296875" />
                  <Point X="-2.911167480469" Y="27.767158203125" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886612060547" Y="27.786142578125" />
                  <Point X="-2.878900390625" Y="27.793056640625" />
                  <Point X="-2.864525878906" Y="27.807431640625" />
                  <Point X="-2.818176757812" Y="27.853779296875" />
                  <Point X="-2.811265869141" Y="27.86148828125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.750569091797" Y="28.064650390625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059387207031" Y="28.699916015625" />
                  <Point X="-2.648376953125" Y="29.015033203125" />
                  <Point X="-2.50941796875" Y="29.092236328125" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.16590234375" Y="29.2336015625" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111819824219" Y="29.164048828125" />
                  <Point X="-2.097516357422" Y="29.149107421875" />
                  <Point X="-2.089957275391" Y="29.14202734375" />
                  <Point X="-2.073377685547" Y="29.128115234375" />
                  <Point X="-2.065092041016" Y="29.1218984375" />
                  <Point X="-2.047895751953" Y="29.110408203125" />
                  <Point X="-2.038985351562" Y="29.105134765625" />
                  <Point X="-2.016445800781" Y="29.093400390625" />
                  <Point X="-1.943769165039" Y="29.05556640625" />
                  <Point X="-1.934341308594" Y="29.05129296875" />
                  <Point X="-1.91506628418" Y="29.043794921875" />
                  <Point X="-1.905219238281" Y="29.0405703125" />
                  <Point X="-1.88431237793" Y="29.034966796875" />
                  <Point X="-1.874174194336" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.833053955078" Y="29.028783203125" />
                  <Point X="-1.812407470703" Y="29.03013671875" />
                  <Point X="-1.802120117188" Y="29.031376953125" />
                  <Point X="-1.7808046875" Y="29.03513671875" />
                  <Point X="-1.770713745117" Y="29.0374921875" />
                  <Point X="-1.750859863281" Y="29.04328125" />
                  <Point X="-1.741097167969" Y="29.04671484375" />
                  <Point X="-1.717620483398" Y="29.056439453125" />
                  <Point X="-1.641922973633" Y="29.087794921875" />
                  <Point X="-1.632588134766" Y="29.092271484375" />
                  <Point X="-1.614454956055" Y="29.10221875" />
                  <Point X="-1.60565625" Y="29.107689453125" />
                  <Point X="-1.587926879883" Y="29.120103515625" />
                  <Point X="-1.579778686523" Y="29.126498046875" />
                  <Point X="-1.5642265625" Y="29.14013671875" />
                  <Point X="-1.550251464844" Y="29.155388671875" />
                  <Point X="-1.538020019531" Y="29.1720703125" />
                  <Point X="-1.532360229492" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.51685546875" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.497235961914" Y="29.26158984375" />
                  <Point X="-1.472597900391" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-0.931163269043" Y="29.7163203125" />
                  <Point X="-0.762729431152" Y="29.73603515625" />
                  <Point X="-0.365222503662" Y="29.78255859375" />
                  <Point X="-0.271411132812" Y="29.43244921875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190490723" Y="29.785005859375" />
                  <Point X="0.827853393555" Y="29.737916015625" />
                  <Point X="0.967247070312" Y="29.70426171875" />
                  <Point X="1.453623535156" Y="29.586833984375" />
                  <Point X="1.542209716797" Y="29.554703125" />
                  <Point X="1.858256713867" Y="29.4400703125" />
                  <Point X="1.945983154297" Y="29.399044921875" />
                  <Point X="2.250421142578" Y="29.25666796875" />
                  <Point X="2.335255126953" Y="29.207244140625" />
                  <Point X="2.629443115234" Y="29.0358515625" />
                  <Point X="2.709363037109" Y="28.979015625" />
                  <Point X="2.817779541016" Y="28.901916015625" />
                  <Point X="2.2692734375" Y="27.951875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.060783447266" Y="27.589712890625" />
                  <Point X="2.049122070312" Y="27.56221484375" />
                  <Point X="2.039038330078" Y="27.531125" />
                  <Point X="2.036219360352" Y="27.52158984375" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017307983398" Y="27.44728515625" />
                  <Point X="2.014099121094" Y="27.421013671875" />
                  <Point X="2.01341394043" Y="27.407765625" />
                  <Point X="2.014058105469" Y="27.3724296875" />
                  <Point X="2.015392700195" Y="27.35314453125" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318359375" Y="27.223888671875" />
                  <Point X="2.055680908203" Y="27.20384375" />
                  <Point X="2.07162890625" Y="27.179142578125" />
                  <Point X="2.104417724609" Y="27.1308203125" />
                  <Point X="2.112694580078" Y="27.120302734375" />
                  <Point X="2.130642578125" Y="27.10053515625" />
                  <Point X="2.140313720703" Y="27.09128515625" />
                  <Point X="2.168599853516" Y="27.067814453125" />
                  <Point X="2.183242919922" Y="27.056810546875" />
                  <Point X="2.231565185547" Y="27.024021484375" />
                  <Point X="2.241279541016" Y="27.0182421875" />
                  <Point X="2.261325683594" Y="27.00787890625" />
                  <Point X="2.271657470703" Y="27.003294921875" />
                  <Point X="2.293745361328" Y="26.995029296875" />
                  <Point X="2.304549316406" Y="26.991705078125" />
                  <Point X="2.326478759766" Y="26.986361328125" />
                  <Point X="2.354038574219" Y="26.982361328125" />
                  <Point X="2.407029052734" Y="26.975970703125" />
                  <Point X="2.420670410156" Y="26.97531640625" />
                  <Point X="2.447898681641" Y="26.97596875" />
                  <Point X="2.461485595703" Y="26.977275390625" />
                  <Point X="2.499395263672" Y="26.9836953125" />
                  <Point X="2.516758056641" Y="26.9874765625" />
                  <Point X="2.5780390625" Y="27.003865234375" />
                  <Point X="2.584007080078" Y="27.005673828125" />
                  <Point X="2.604416748047" Y="27.013072265625" />
                  <Point X="2.627660888672" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.775139404297" Y="27.685537109375" />
                  <Point X="3.940403808594" Y="27.780953125" />
                  <Point X="4.043956542969" Y="27.637037109375" />
                  <Point X="4.088513671875" Y="27.56340625" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.441490234375" Y="26.94987890625" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168135742188" Y="26.7398671875" />
                  <Point X="3.152112792969" Y="26.7252109375" />
                  <Point X="3.134666015625" Y="26.706599609375" />
                  <Point X="3.128577880859" Y="26.699421875" />
                  <Point X="3.114899658203" Y="26.681578125" />
                  <Point X="3.070795654297" Y="26.624041015625" />
                  <Point X="3.063332519531" Y="26.612712890625" />
                  <Point X="3.050093505859" Y="26.58910546875" />
                  <Point X="3.044317626953" Y="26.576826171875" />
                  <Point X="3.030752685547" Y="26.54165625" />
                  <Point X="3.025044677734" Y="26.524453125" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.3525703125" />
                  <Point X="3.008881591797" Y="26.332298828125" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.024598144531" Y="26.258232421875" />
                  <Point X="3.034681884766" Y="26.22861328125" />
                  <Point X="3.050285888672" Y="26.19537109375" />
                  <Point X="3.056919433594" Y="26.1835234375" />
                  <Point X="3.068295654297" Y="26.166232421875" />
                  <Point X="3.104977050781" Y="26.110478515625" />
                  <Point X="3.111740722656" Y="26.101421875" />
                  <Point X="3.126295898438" Y="26.08417578125" />
                  <Point X="3.134087402344" Y="26.075986328125" />
                  <Point X="3.151330322266" Y="26.0598984375" />
                  <Point X="3.16003515625" Y="26.0526953125" />
                  <Point X="3.178245117188" Y="26.039369140625" />
                  <Point X="3.204235839844" Y="26.023966796875" />
                  <Point X="3.257392333984" Y="25.99404296875" />
                  <Point X="3.265481933594" Y="25.989984375" />
                  <Point X="3.294681396484" Y="25.97808203125" />
                  <Point X="3.330281005859" Y="25.96801953125" />
                  <Point X="3.343676269531" Y="25.965255859375" />
                  <Point X="3.365966064453" Y="25.962310546875" />
                  <Point X="3.437836914063" Y="25.9528125" />
                  <Point X="3.444033935547" Y="25.95219921875" />
                  <Point X="3.465708496094" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.582677734375" Y="26.09525390625" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.766726074219" Y="25.8240390625" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.999743164062" Y="25.50381640625" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686020019531" Y="25.419541015625" />
                  <Point X="3.66562890625" Y="25.41213671875" />
                  <Point X="3.642390869141" Y="25.401623046875" />
                  <Point X="3.634014160156" Y="25.397322265625" />
                  <Point X="3.612115234375" Y="25.384666015625" />
                  <Point X="3.541504150391" Y="25.3438515625" />
                  <Point X="3.533888183594" Y="25.338951171875" />
                  <Point X="3.508772949219" Y="25.31987109375" />
                  <Point X="3.481989990234" Y="25.29434765625" />
                  <Point X="3.472791992188" Y="25.284220703125" />
                  <Point X="3.459652587891" Y="25.2674765625" />
                  <Point X="3.417286132812" Y="25.2134921875" />
                  <Point X="3.410850830078" Y="25.204203125" />
                  <Point X="3.399129150391" Y="25.18492578125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005126953" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.365996826172" Y="25.087603515625" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.356254638672" Y="24.90070703125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.81601171875" />
                  <Point X="3.380005859375" Y="24.79451171875" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399130371094" Y="24.752513671875" />
                  <Point X="3.410855224609" Y="24.733232421875" />
                  <Point X="3.417292480469" Y="24.72394140625" />
                  <Point X="3.430431884766" Y="24.70719921875" />
                  <Point X="3.472798339844" Y="24.653212890625" />
                  <Point X="3.478715576172" Y="24.646369140625" />
                  <Point X="3.501133300781" Y="24.62419921875" />
                  <Point X="3.530173828125" Y="24.601275390625" />
                  <Point X="3.541493896484" Y="24.593595703125" />
                  <Point X="3.563393066406" Y="24.5809375" />
                  <Point X="3.63400390625" Y="24.540123046875" />
                  <Point X="3.639486816406" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.68302734375" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.684303222656" Y="24.2501953125" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.06894921875" />
                  <Point X="4.743622070312" Y="23.990107421875" />
                  <Point X="4.727802246094" Y="23.920783203125" />
                  <Point X="3.793686767578" Y="24.043759765625" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481933594" Y="24.087322265625" />
                  <Point X="3.311501953125" Y="24.07798046875" />
                  <Point X="3.172917724609" Y="24.047859375" />
                  <Point X="3.157873535156" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114676513672" Y="24.024609375" />
                  <Point X="3.086849609375" Y="24.007716796875" />
                  <Point X="3.074127197266" Y="23.998470703125" />
                  <Point X="3.050377441406" Y="23.978001953125" />
                  <Point X="3.039350097656" Y="23.966779296875" />
                  <Point X="3.013371337891" Y="23.93553515625" />
                  <Point X="2.929605957031" Y="23.834791015625" />
                  <Point X="2.921325927734" Y="23.82315234375" />
                  <Point X="2.906605224609" Y="23.79876953125" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.871433837891" Y="23.662876953125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158935547" Y="23.394517578125" />
                  <Point X="2.896541259766" Y="23.380626953125" />
                  <Point X="2.920326904297" Y="23.343630859375" />
                  <Point X="2.997021484375" Y="23.224337890625" />
                  <Point X="3.001740966797" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.973666992188" Y="22.457111328125" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045490966797" Y="22.30257421875" />
                  <Point X="4.008279785156" Y="22.249701171875" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="3.166326660156" Y="22.721802734375" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.719955566406" Y="22.942900390625" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444847412109" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400588623047" Y="22.948890625" />
                  <Point X="2.358093017578" Y="22.926525390625" />
                  <Point X="2.221070556641" Y="22.854412109375" />
                  <Point X="2.208966552734" Y="22.846828125" />
                  <Point X="2.186037597656" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144942138672" Y="22.78883984375" />
                  <Point X="2.128047607422" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.098098388672" Y="22.71130859375" />
                  <Point X="2.025984619141" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.0021875" Y="22.419859375" />
                  <Point X="2.011425537109" Y="22.368705078125" />
                  <Point X="2.041213134766" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.661298828125" Y="21.1014765625" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723752929688" Y="20.963916015625" />
                  <Point X="2.07648046875" Y="21.80745703125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808835327148" Y="22.150369140625" />
                  <Point X="1.783253417969" Y="22.171994140625" />
                  <Point X="1.773299560547" Y="22.179353515625" />
                  <Point X="1.722848510742" Y="22.2117890625" />
                  <Point X="1.560176025391" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470092773" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455385253906" Y="22.3513046875" />
                  <Point X="1.424121337891" Y="22.35362109375" />
                  <Point X="1.408398681641" Y="22.35348046875" />
                  <Point X="1.353222167969" Y="22.348404296875" />
                  <Point X="1.175311645508" Y="22.332033203125" />
                  <Point X="1.161228637695" Y="22.32966015625" />
                  <Point X="1.133576171875" Y="22.322828125" />
                  <Point X="1.120006713867" Y="22.318369140625" />
                  <Point X="1.09262121582" Y="22.307025390625" />
                  <Point X="1.07987512207" Y="22.300583984375" />
                  <Point X="1.055493408203" Y="22.28586328125" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="1.001251831055" Y="22.242158203125" />
                  <Point X="0.863874023438" Y="22.127931640625" />
                  <Point X="0.85265435791" Y="22.11691015625" />
                  <Point X="0.832184082031" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.770053161621" Y="21.935759765625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.749890686035" Y="21.15816796875" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146484375" Y="21.546416015625" />
                  <Point X="0.626787841797" Y="21.57618359375" />
                  <Point X="0.620406982422" Y="21.58679296875" />
                  <Point X="0.581649230957" Y="21.642634765625" />
                  <Point X="0.456679229736" Y="21.822693359375" />
                  <Point X="0.446668884277" Y="21.834830078125" />
                  <Point X="0.424786102295" Y="21.857283203125" />
                  <Point X="0.412913513184" Y="21.867599609375" />
                  <Point X="0.38665838623" Y="21.88684375" />
                  <Point X="0.373243682861" Y="21.8950625" />
                  <Point X="0.345240600586" Y="21.909171875" />
                  <Point X="0.330652374268" Y="21.9150625" />
                  <Point X="0.270677093506" Y="21.93367578125" />
                  <Point X="0.077293281555" Y="21.9936953125" />
                  <Point X="0.063376678467" Y="21.996890625" />
                  <Point X="0.035218582153" Y="22.00116015625" />
                  <Point X="0.020976791382" Y="22.002234375" />
                  <Point X="-0.008664410591" Y="22.002234375" />
                  <Point X="-0.022904714584" Y="22.00116015625" />
                  <Point X="-0.05106578064" Y="21.996890625" />
                  <Point X="-0.06498639679" Y="21.9936953125" />
                  <Point X="-0.124961685181" Y="21.975080078125" />
                  <Point X="-0.318345489502" Y="21.9150625" />
                  <Point X="-0.332930297852" Y="21.909171875" />
                  <Point X="-0.360929962158" Y="21.895064453125" />
                  <Point X="-0.374344970703" Y="21.88684765625" />
                  <Point X="-0.400600402832" Y="21.867603515625" />
                  <Point X="-0.41247668457" Y="21.857283203125" />
                  <Point X="-0.434360961914" Y="21.834826171875" />
                  <Point X="-0.44436907959" Y="21.822689453125" />
                  <Point X="-0.483126678467" Y="21.766845703125" />
                  <Point X="-0.608096679688" Y="21.5867890625" />
                  <Point X="-0.612471130371" Y="21.5798671875" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.948508361816" Y="20.37096875" />
                  <Point X="-0.985424804688" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131321789579" Y="20.344052990083" />
                  <Point X="-0.960530399308" Y="20.326102091619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119927690239" Y="20.438378708544" />
                  <Point X="-0.935636061044" Y="20.419008877792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129825600412" Y="20.534942307382" />
                  <Point X="-0.910741785463" Y="20.511915670553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.149232028071" Y="20.632505291679" />
                  <Point X="-0.885847509883" Y="20.604822463314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.567039372349" Y="20.877046134912" />
                  <Point X="-2.479899813051" Y="20.86788739817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168638461507" Y="20.730068276583" />
                  <Point X="-0.860953234302" Y="20.697729256075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751664727629" Y="20.991974328248" />
                  <Point X="-2.412073085614" Y="20.956281808411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.190518891712" Y="20.827891289025" />
                  <Point X="-0.836058958721" Y="20.790636048836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.895338670545" Y="21.102598354704" />
                  <Point X="-2.309671857294" Y="21.041042292177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.250881196234" Y="20.92975890944" />
                  <Point X="-0.81116468314" Y="20.883542841597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959819356269" Y="21.204898834425" />
                  <Point X="-2.186142025974" Y="21.123582070282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.372586598362" Y="21.038073949216" />
                  <Point X="-0.786270407559" Y="20.976449634358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907824194305" Y="21.294957209248" />
                  <Point X="-2.052207359162" Y="21.20502825611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496781927019" Y="21.146650690817" />
                  <Point X="-0.761376131978" Y="21.069356427119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818172248703" Y="20.903339202503" />
                  <Point X="0.82586768541" Y="20.902530379513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85582903234" Y="21.38501558407" />
                  <Point X="-0.736481856397" Y="21.16226321988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79183498091" Y="21.001630647452" />
                  <Point X="0.813115341289" Y="20.999393991448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803833870376" Y="21.475073958893" />
                  <Point X="-0.711587580816" Y="21.255170012642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765497713117" Y="21.099922092401" />
                  <Point X="0.800362997168" Y="21.096257603383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751838708412" Y="21.565132333716" />
                  <Point X="-0.686693305235" Y="21.348076805403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739160572102" Y="21.198213524025" />
                  <Point X="0.787610653046" Y="21.193121215318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.702295603995" Y="20.991879717774" />
                  <Point X="2.726013780585" Y="20.989386836962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.699843546447" Y="21.655190708539" />
                  <Point X="-0.661799029654" Y="21.440983598164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.712823615483" Y="21.296504936268" />
                  <Point X="0.774858308925" Y="21.289984827254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622567943924" Y="21.095782719074" />
                  <Point X="2.66730062965" Y="21.091081124349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.647848384483" Y="21.745249083361" />
                  <Point X="-0.63469424097" Y="21.533658056636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.686486658864" Y="21.394796348511" />
                  <Point X="0.762105964804" Y="21.386848439189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.542840283852" Y="21.199685720374" />
                  <Point X="2.608587325921" Y="21.192775427796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595853222518" Y="21.835307458184" />
                  <Point X="-0.582482678794" Y="21.62369368688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.660149702245" Y="21.493087760754" />
                  <Point X="0.749353620682" Y="21.483712051124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463112623781" Y="21.303588721674" />
                  <Point X="2.549874004794" Y="21.294469733072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854621858647" Y="22.06313265962" />
                  <Point X="-3.68410397447" Y="22.045210507804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.543858060554" Y="21.925365833007" />
                  <Point X="-0.520691429256" Y="21.712722451409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.61591849791" Y="21.593259934219" />
                  <Point X="0.736601276561" Y="21.580575663059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.383384963709" Y="21.407491722974" />
                  <Point X="2.491160683666" Y="21.396164038347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.933652303335" Y="22.16696238063" />
                  <Point X="-3.544133915905" Y="22.126022348397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.491862954029" Y="22.015424213657" />
                  <Point X="-0.458900925333" Y="21.801751294306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.544402905618" Y="21.696299812415" />
                  <Point X="0.724741915149" Y="21.67734541873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.303657303638" Y="21.511394724274" />
                  <Point X="2.432447362539" Y="21.497858343623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.010680912466" Y="22.270581700244" />
                  <Point X="-3.404163903748" Y="22.206834193868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.439867903581" Y="22.1054826002" />
                  <Point X="-0.372199332573" Y="21.888161876262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.472887932024" Y="21.799339625583" />
                  <Point X="0.734426662569" Y="21.771850797317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.223929643566" Y="21.615297725573" />
                  <Point X="2.373734041412" Y="21.599552648898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071456259574" Y="22.372492733184" />
                  <Point X="-3.264193891591" Y="22.287646039338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.387872853133" Y="22.195540986744" />
                  <Point X="-0.16679647675" Y="21.962096452737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.347479335199" Y="21.908043886807" />
                  <Point X="0.754725439533" Y="21.865240596446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.144201983494" Y="21.719200726873" />
                  <Point X="2.315020720285" Y="21.701246954174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.132231801745" Y="22.474403786625" />
                  <Point X="-3.124223879434" Y="22.368457884809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.336844598886" Y="22.285700987663" />
                  <Point X="0.775024266103" Y="21.958630390361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064474381713" Y="21.823103722047" />
                  <Point X="2.256307399157" Y="21.802941259449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160110810798" Y="22.57285727511" />
                  <Point X="-2.984253867277" Y="22.449269730279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311888838504" Y="22.378601318111" />
                  <Point X="0.805228151435" Y="22.05097912065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.984747108725" Y="21.927006682662" />
                  <Point X="2.19759407803" Y="21.904635564725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.050619843265" Y="22.656872597258" />
                  <Point X="-2.84428385512" Y="22.53008157575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325034642462" Y="22.475506284341" />
                  <Point X="0.877120957436" Y="22.138946168812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.905019835737" Y="22.030909643278" />
                  <Point X="2.138880756903" Y="22.00632987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.941128875733" Y="22.740887919405" />
                  <Point X="-2.704313842963" Y="22.610893421221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.392123250335" Y="22.578080867725" />
                  <Point X="0.979112356756" Y="22.223749727342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.823701313626" Y="22.134979850916" />
                  <Point X="2.080167435776" Y="22.108024175276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.8316379082" Y="22.824903241553" />
                  <Point X="1.093232314913" Y="22.30727852297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.668340516561" Y="22.24683221524" />
                  <Point X="2.040499928847" Y="22.207716684815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.722146940668" Y="22.9089185637" />
                  <Point X="2.022914691191" Y="22.305088254329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.612655973135" Y="22.992933885848" />
                  <Point X="2.005329718072" Y="22.40245979604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.503165005602" Y="23.076949207995" />
                  <Point X="2.003780567034" Y="22.498145904934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.898668573719" Y="22.298985150077" />
                  <Point X="4.033026498389" Y="22.284863563153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.39367403807" Y="23.160964530143" />
                  <Point X="2.034486777986" Y="22.590441838672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696394039828" Y="22.415768346834" />
                  <Point X="4.080102729553" Y="22.375438938435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.284182991451" Y="23.244979843978" />
                  <Point X="2.08212508476" Y="22.680958137428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.494119505937" Y="22.532551543591" />
                  <Point X="3.935858368997" Y="22.486122918202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.174691904104" Y="23.328995153532" />
                  <Point X="2.131977032564" Y="22.771241773136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.291844972045" Y="22.649334740348" />
                  <Point X="3.791611986497" Y="22.596807110483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.065200816757" Y="23.413010463087" />
                  <Point X="2.225804291533" Y="22.856903417393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089570441308" Y="22.766117936773" />
                  <Point X="3.647365603997" Y="22.707491302764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985947506409" Y="23.500203891069" />
                  <Point X="2.377093996573" Y="22.9365255152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887295915728" Y="22.882901132656" />
                  <Point X="3.503119221498" Y="22.818175495045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676267300514" Y="23.773386946942" />
                  <Point X="-4.413961496129" Y="23.745817495966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.952989060306" Y="23.592263105355" />
                  <Point X="3.358872838998" Y="22.928859687326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701340083063" Y="23.871545489136" />
                  <Point X="-4.010495781658" Y="23.798934827149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950890450267" Y="23.68756581911" />
                  <Point X="3.214626456498" Y="23.039543879607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726412865612" Y="23.96970403133" />
                  <Point X="-3.607030067186" Y="23.852052158332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993540391931" Y="23.787571795171" />
                  <Point X="3.070380073999" Y="23.150228071888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74669461939" Y="24.067359016109" />
                  <Point X="2.976940624994" Y="23.255572240277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760564902199" Y="24.164340128135" />
                  <Point X="2.91107703429" Y="23.358018069169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.774435351" Y="24.261321257608" />
                  <Point X="2.866923527927" Y="23.458182076248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752232675515" Y="24.354510948939" />
                  <Point X="2.861440992706" Y="23.554281600478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.496176193415" Y="24.423121614761" />
                  <Point X="2.870146759648" Y="23.64888987406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.240119711315" Y="24.491732280584" />
                  <Point X="2.883844692734" Y="23.742973449837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.984063229215" Y="24.560342946407" />
                  <Point X="2.928875199343" Y="23.833763839434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728005402087" Y="24.628953470861" />
                  <Point X="3.001803093524" Y="23.921622095445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479819703873" Y="24.698391389405" />
                  <Point X="3.087537423573" Y="24.008134340808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362853236218" Y="24.781621004829" />
                  <Point X="3.318130871726" Y="24.079421279341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333180550007" Y="23.972735259149" />
                  <Point X="4.730136799913" Y="23.931013476069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.312368176466" Y="24.87183809779" />
                  <Point X="4.75142486836" Y="24.024299296473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294719103023" Y="24.965506391981" />
                  <Point X="4.769005390555" Y="24.11797479569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.311297881485" Y="25.062772178372" />
                  <Point X="4.783181917387" Y="24.212008069238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.371093910495" Y="25.164580280831" />
                  <Point X="4.26839379518" Y="24.361637767705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661256749025" Y="25.290600910636" />
                  <Point X="3.684341976969" Y="24.518547373972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24785031431" Y="25.447777665285" />
                  <Point X="3.490806284496" Y="24.634412081484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784283293809" Y="25.599682329925" />
                  <Point X="3.407538197155" Y="24.738687196685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770364802936" Y="25.693742724145" />
                  <Point X="3.368197801405" Y="24.838345325454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.756446312064" Y="25.787803118364" />
                  <Point X="3.350436551652" Y="24.935735394585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.742527821192" Y="25.881863512583" />
                  <Point X="3.355112153984" Y="25.030767255536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726409084249" Y="25.975692651622" />
                  <Point X="3.374073015225" Y="25.124297675274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701240882648" Y="26.068570653598" />
                  <Point X="3.418581612715" Y="25.21514291973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676072609371" Y="26.161448648041" />
                  <Point X="3.491114145296" Y="25.30304272992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.650904325582" Y="26.254326641379" />
                  <Point X="3.613846125373" Y="25.38566636557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599632560929" Y="26.239356813057" />
                  <Point X="3.830369186014" Y="25.458432161422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.49139380665" Y="26.323503748121" />
                  <Point X="4.086425109592" Y="25.527042885948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.445710246003" Y="26.414225498974" />
                  <Point X="4.342482065284" Y="25.595653501994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422009412099" Y="26.50725772751" />
                  <Point X="4.598539020976" Y="25.66426411804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435917802986" Y="26.604242844856" />
                  <Point X="4.779694303855" Y="25.740747217127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.486685376792" Y="26.705102018436" />
                  <Point X="3.252526118601" Y="25.996782347919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.576257286883" Y="25.962756831045" />
                  <Point X="4.764574289897" Y="25.83785968119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.59975605445" Y="26.812509512101" />
                  <Point X="3.107136732717" Y="26.107586674697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979724432912" Y="26.015874011765" />
                  <Point X="4.747587303953" Y="25.935168371916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744002224514" Y="26.923193682054" />
                  <Point X="3.043514342026" Y="26.209796943974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.38319157894" Y="26.068991192486" />
                  <Point X="4.723722054343" Y="26.033199997284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.888248394578" Y="27.033877852007" />
                  <Point X="3.013803899242" Y="26.308442923901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.032494564642" Y="27.14456202196" />
                  <Point X="3.001161790977" Y="26.405294949581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176740734707" Y="27.255246191913" />
                  <Point X="3.017941817658" Y="26.499054584268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193608381471" Y="27.352542339586" />
                  <Point X="3.051201990321" Y="26.591082085814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141075050327" Y="27.442544150548" />
                  <Point X="3.113716662944" Y="26.680034815513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088541719182" Y="27.532545961511" />
                  <Point X="3.202193608316" Y="26.76625880039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036008388038" Y="27.622547772474" />
                  <Point X="3.311683809834" Y="26.850274203049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.977683939694" Y="27.711940912492" />
                  <Point X="2.171499113473" Y="27.06563573018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617093471034" Y="27.01880187599" />
                  <Point X="3.421174011351" Y="26.934289605708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908986358224" Y="27.800243792285" />
                  <Point X="2.0770991473" Y="27.171080852992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.759792397565" Y="27.099326901002" />
                  <Point X="3.53066525755" Y="27.018304898566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84028740263" Y="27.888546527652" />
                  <Point X="-3.390826851213" Y="27.841306320114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884447056243" Y="27.788083659009" />
                  <Point X="2.028009706533" Y="27.271763647682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.899762720598" Y="27.180138713798" />
                  <Point X="3.640156741755" Y="27.102320166409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771588447036" Y="27.97684926302" />
                  <Point X="-3.593100664362" Y="27.958089441118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.800587082071" Y="27.874792907113" />
                  <Point X="2.014314386906" Y="27.368726370337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.039733043631" Y="27.260950526594" />
                  <Point X="3.74964822596" Y="27.186335434252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755850707401" Y="27.965614211223" />
                  <Point X="2.02070615472" Y="27.463577855027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.179703366664" Y="27.341762339391" />
                  <Point X="3.859139710165" Y="27.270350702095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750209901667" Y="28.060544625209" />
                  <Point X="2.047208701922" Y="27.556315611629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.319673689697" Y="27.422574152187" />
                  <Point X="3.96863119437" Y="27.354365969938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761129428904" Y="28.157215600327" />
                  <Point X="2.093253368725" Y="27.646999408696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.45964401273" Y="27.503385964983" />
                  <Point X="4.078122678575" Y="27.438381237781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803798193646" Y="28.257223554774" />
                  <Point X="2.145248609796" Y="27.737057775204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599614335763" Y="27.58419777778" />
                  <Point X="4.108284255317" Y="27.530734414882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862511520876" Y="28.358917860691" />
                  <Point X="2.197243850867" Y="27.827116141712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.739584658796" Y="27.665009590576" />
                  <Point X="4.046553345372" Y="27.632745881523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921224848106" Y="28.460612166608" />
                  <Point X="2.249239091938" Y="27.917174508221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879554357271" Y="27.745821469016" />
                  <Point X="3.9727287245" Y="27.736028448402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979938175336" Y="28.562306472525" />
                  <Point X="2.301234303771" Y="28.007232877802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.038651502566" Y="28.664000778442" />
                  <Point X="2.353229497275" Y="28.09729124931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988513252179" Y="28.754254322536" />
                  <Point X="2.40522469078" Y="28.187349620817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878942262727" Y="28.83826123404" />
                  <Point X="2.457219884284" Y="28.277407992325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.769371273276" Y="28.922268145545" />
                  <Point X="2.509215077789" Y="28.367466363833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659800283824" Y="29.00627505705" />
                  <Point X="2.561210271293" Y="28.457524735341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.518868448293" Y="29.086985810811" />
                  <Point X="2.613205464798" Y="28.547583106848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.374286562901" Y="29.167312928872" />
                  <Point X="-2.08369216993" Y="29.136770227426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.637572585023" Y="29.089881169617" />
                  <Point X="2.665200658302" Y="28.637641478356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.229704678555" Y="29.247640047043" />
                  <Point X="-2.172022210936" Y="29.241577375396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.536267040735" Y="29.174756814416" />
                  <Point X="2.717195851807" Y="28.727699849864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495835805945" Y="29.266030606961" />
                  <Point X="-0.12020306542" Y="29.121445779761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.084819141686" Y="29.099897077471" />
                  <Point X="2.769191045312" Y="28.817758221372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468258137744" Y="29.358655363792" />
                  <Point X="-0.211108835956" Y="29.226523647813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197861698176" Y="29.183539112577" />
                  <Point X="2.807452415596" Y="28.909260075866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464939038979" Y="29.453829799013" />
                  <Point X="-0.242719288384" Y="29.325369326801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241397317774" Y="29.274486621131" />
                  <Point X="2.649835045321" Y="29.021349615592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47769142708" Y="29.550693415571" />
                  <Point X="-0.269056278057" Y="29.423660742518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266291609289" Y="29.367393412217" />
                  <Point X="2.455273165223" Y="29.13732217977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262052835016" Y="29.623552172817" />
                  <Point X="-0.29539333586" Y="29.521952165396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.291185900805" Y="29.460300203304" />
                  <Point X="2.255220583605" Y="29.253871839933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.014239102817" Y="29.693029186564" />
                  <Point X="-0.321730400352" Y="29.620243588977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.316080192321" Y="29.55320699439" />
                  <Point X="1.993278197863" Y="29.376926380628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645315564654" Y="29.749777046773" />
                  <Point X="-0.348067464845" Y="29.718535012558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.340974483836" Y="29.646113785476" />
                  <Point X="1.677472622245" Y="29.505642170705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.365868775352" Y="29.739020576562" />
                  <Point X="1.175921843637" Y="29.653880568296" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.566364807129" Y="21.1089921875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.425561096191" Y="21.534298828125" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.7336015625" />
                  <Point X="0.214360549927" Y="21.75221484375" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.068639648438" Y="21.793619140625" />
                  <Point X="-0.2620234375" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.327036712646" Y="21.658513671875" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.76498260498" Y="20.32179296875" />
                  <Point X="-0.84774420166" Y="20.012921875" />
                  <Point X="-0.872937561035" Y="20.0178125" />
                  <Point X="-1.10023046875" Y="20.061931640625" />
                  <Point X="-1.168784790039" Y="20.0795703125" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.320565063477" Y="20.362255859375" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.324011352539" Y="20.5372734375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.441500244141" Y="20.845794921875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.722526855469" Y="21.0190390625" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.050944335938" Y="20.98540625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.431392089844" Y="20.61899609375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.522645263672" Y="20.626087890625" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-2.950740966797" Y="20.905462890625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.682256835938" Y="22.06565234375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.402412109375" />
                  <Point X="-2.513984863281" Y="22.4312421875" />
                  <Point X="-2.531327880859" Y="22.448583984375" />
                  <Point X="-2.560156982422" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.575855712891" Y="21.88831640625" />
                  <Point X="-3.842958740234" Y="21.73410546875" />
                  <Point X="-3.898466552734" Y="21.80703125" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.229743652344" Y="22.266962890625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.472119873047" Y="23.34026171875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.143998291016" Y="23.61102734375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651611328" Y="23.649947265625" />
                  <Point X="-3.148656982422" Y="23.678931640625" />
                  <Point X="-3.167428710938" Y="23.69305078125" />
                  <Point X="-3.187642333984" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.461952636719" Y="23.547859375" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.824439453125" Y="23.585748046875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.94539453125" Y="24.114671875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.908701660156" Y="24.777240234375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.535323242188" Y="24.883138671875" />
                  <Point X="-3.51414453125" Y="24.8978359375" />
                  <Point X="-3.502325195312" Y="24.909353515625" />
                  <Point X="-3.492709228516" Y="24.9311484375" />
                  <Point X="-3.485648193359" Y="24.9538984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.487837646484" Y="24.990595703125" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.502325927734" Y="25.028087890625" />
                  <Point X="-3.520718505859" Y="25.044166015625" />
                  <Point X="-3.54189453125" Y="25.05886328125" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.6899921875" Y="25.369544921875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.983997558594" Y="25.548015625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.881407226562" Y="26.130146484375" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.005041992188" Y="26.427126953125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704345703" Y="26.3958671875" />
                  <Point X="-3.717163574219" Y="26.400451171875" />
                  <Point X="-3.670278320312" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.633285644531" Y="26.45787109375" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.623355957031" Y="26.55903515625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.309589355469" Y="27.117693359375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.4178359375" Y="27.345294921875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.0640234375" Y="27.910388671875" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.313790771484" Y="28.016220703125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514892578" Y="27.92043359375" />
                  <Point X="-3.118263671875" Y="27.918662109375" />
                  <Point X="-3.052965576172" Y="27.91294921875" />
                  <Point X="-3.031507080078" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.998878417969" Y="27.941779296875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939845947266" Y="28.048091796875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.239934082031" Y="28.6326328125" />
                  <Point X="-3.307278808594" Y="28.74927734375" />
                  <Point X="-3.201919921875" Y="28.8300546875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.601693115234" Y="29.258326171875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.015164794922" Y="29.349265625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247070312" Y="29.273662109375" />
                  <Point X="-1.928707519531" Y="29.261927734375" />
                  <Point X="-1.856031005859" Y="29.22409375" />
                  <Point X="-1.835124145508" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.79033203125" Y="29.231974609375" />
                  <Point X="-1.714634643555" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.678442138672" Y="29.31872265625" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.684644897461" Y="29.667013671875" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.549409057617" Y="29.740314453125" />
                  <Point X="-0.968083068848" Y="29.903296875" />
                  <Point X="-0.78481640625" Y="29.92474609375" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.087885253906" Y="29.481625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282119751" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594039917" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.201951828003" Y="29.86137890625" />
                  <Point X="0.236648345947" Y="29.9908671875" />
                  <Point X="0.352633056641" Y="29.978720703125" />
                  <Point X="0.860210083008" Y="29.925564453125" />
                  <Point X="1.01183770752" Y="29.88895703125" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.606994140625" Y="29.73331640625" />
                  <Point X="1.931044067383" Y="29.61578125" />
                  <Point X="2.026470947266" Y="29.571154296875" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.430900878906" Y="29.371416015625" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.819477294922" Y="29.133853515625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.433818359375" Y="27.856875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.219769775391" Y="27.4725078125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.204026367188" Y="27.375890625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.228851074219" Y="27.285826171875" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.289926025391" Y="27.214033203125" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.360336181641" Y="27.172978515625" />
                  <Point X="2.376770507812" Y="27.170998046875" />
                  <Point X="2.429760986328" Y="27.164607421875" />
                  <Point X="2.467670654297" Y="27.17102734375" />
                  <Point X="2.528951660156" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.680139404297" Y="27.850080078125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.023679443359" Y="27.99052734375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.251067382812" Y="27.6617734375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.557154785156" Y="26.799140625" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.265693115234" Y="26.56598828125" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.208024169922" Y="26.47328125" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.194962158203" Y="26.370693359375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287953125" />
                  <Point X="3.227022705078" Y="26.270662109375" />
                  <Point X="3.263704101562" Y="26.214908203125" />
                  <Point X="3.280947021484" Y="26.1988203125" />
                  <Point X="3.297432617188" Y="26.189541015625" />
                  <Point X="3.350589111328" Y="26.1596171875" />
                  <Point X="3.368565917969" Y="26.153619140625" />
                  <Point X="3.390855712891" Y="26.150673828125" />
                  <Point X="3.4627265625" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.557877929688" Y="26.28362890625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.862346679688" Y="26.26702734375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.954464355469" Y="25.85326953125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.048918945312" Y="25.3202890625" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729086914062" Y="25.232818359375" />
                  <Point X="3.707187988281" Y="25.220162109375" />
                  <Point X="3.636576904297" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.609125732422" Y="25.15018359375" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.552605224609" Y="25.051865234375" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.542863037109" Y="24.9364453125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.579898193359" Y="24.8245" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.658476074219" Y="24.74543359375" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.733479003906" Y="24.43372265625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.991336914062" Y="24.3181796875" />
                  <Point X="4.948431640625" Y="24.033599609375" />
                  <Point X="4.928860351562" Y="23.9478359375" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="3.768886962891" Y="23.855384765625" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.351856445312" Y="23.89231640625" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.159466552734" Y="23.81405859375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.060634521484" Y="23.645466796875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.080146240234" Y="23.446380859375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.089331542969" Y="22.607849609375" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.325077148438" Y="22.39356640625" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.16365625" Y="22.14034765625" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.071326660156" Y="22.557259765625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.6861875" Y="22.755923828125" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.446582275391" Y="22.758388671875" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.266234619141" Y="22.6228203125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.198401123047" Y="22.402470703125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.82584375" Y="21.1964765625" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.978810058594" Y="20.912294921875" />
                  <Point X="2.835294189453" Y="20.80978515625" />
                  <Point X="2.790046142578" Y="20.78049609375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.925743408203" Y="21.69179296875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.620098266602" Y="22.05196875" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.370628540039" Y="22.159205078125" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.122726806641" Y="22.096064453125" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.955717895508" Y="21.895404296875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.081627441406" Y="20.41548828125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="0.994366638184" Y="20.0367578125" />
                  <Point X="0.952538696289" Y="20.029158203125" />
                  <Point X="0.860200500488" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#140" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.045744944593" Y="4.525846939695" Z="0.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="-0.796784583478" Y="5.005687487782" Z="0.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.6" />
                  <Point X="-1.569062786887" Y="4.819739703738" Z="0.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.6" />
                  <Point X="-1.740373088827" Y="4.691768543442" Z="0.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.6" />
                  <Point X="-1.732283985967" Y="4.365038704621" Z="0.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.6" />
                  <Point X="-1.815622555934" Y="4.309448942664" Z="0.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.6" />
                  <Point X="-1.911775591075" Y="4.337557860966" Z="0.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.6" />
                  <Point X="-1.98165327218" Y="4.410983523897" Z="0.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.6" />
                  <Point X="-2.632132117099" Y="4.333312995704" Z="0.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.6" />
                  <Point X="-3.238498003977" Y="3.901014203538" Z="0.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.6" />
                  <Point X="-3.289391418594" Y="3.638912686505" Z="0.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.6" />
                  <Point X="-2.995811788059" Y="3.075014839457" Z="0.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.6" />
                  <Point X="-3.040389049484" Y="3.008414518402" Z="0.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.6" />
                  <Point X="-3.12006162615" Y="2.999752911114" Z="0.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.6" />
                  <Point X="-3.294946661792" Y="3.090802530152" Z="0.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.6" />
                  <Point X="-4.109642638554" Y="2.972372117175" Z="0.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.6" />
                  <Point X="-4.46717423343" Y="2.401781179442" Z="0.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.6" />
                  <Point X="-4.346183281299" Y="2.109305784195" Z="0.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.6" />
                  <Point X="-3.67386246651" Y="1.567228160404" Z="0.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.6" />
                  <Point X="-3.685635345915" Y="1.508285989684" Z="0.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.6" />
                  <Point X="-3.738355240003" Y="1.479417236516" Z="0.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.6" />
                  <Point X="-4.004671927293" Y="1.507979465187" Z="0.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.6" />
                  <Point X="-4.935823105899" Y="1.174504172982" Z="0.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.6" />
                  <Point X="-5.039614891863" Y="0.58661383404" Z="0.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.6" />
                  <Point X="-4.709089184167" Y="0.352529119912" Z="0.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.6" />
                  <Point X="-3.555377533504" Y="0.034366680047" Z="0.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.6" />
                  <Point X="-3.541746670701" Y="0.007055915643" Z="0.6" />
                  <Point X="-3.539556741714" Y="0" Z="0.6" />
                  <Point X="-3.546617930434" Y="-0.022751035418" Z="0.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.6" />
                  <Point X="-3.569991061619" Y="-0.044509303087" Z="0.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.6" />
                  <Point X="-3.927798604606" Y="-0.143182936844" Z="0.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.6" />
                  <Point X="-5.001046697734" Y="-0.861125016538" Z="0.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.6" />
                  <Point X="-4.879040117186" Y="-1.395345540679" Z="0.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.6" />
                  <Point X="-4.461582886387" Y="-1.470431499045" Z="0.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.6" />
                  <Point X="-3.198945275209" Y="-1.318760160742" Z="0.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.6" />
                  <Point X="-3.198556990103" Y="-1.345155359404" Z="0.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.6" />
                  <Point X="-3.508713908788" Y="-1.588789474277" Z="0.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.6" />
                  <Point X="-4.278843160736" Y="-2.727365846712" Z="0.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.6" />
                  <Point X="-3.944408347177" Y="-3.1919722848" Z="0.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.6" />
                  <Point X="-3.557011647708" Y="-3.123703017983" Z="0.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.6" />
                  <Point X="-2.559597722396" Y="-2.568732746736" Z="0.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.6" />
                  <Point X="-2.731713995185" Y="-2.878066734784" Z="0.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.6" />
                  <Point X="-2.987400836144" Y="-4.102872883653" Z="0.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.6" />
                  <Point X="-2.555122731483" Y="-4.385144269316" Z="0.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.6" />
                  <Point X="-2.39788038955" Y="-4.380161308895" Z="0.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.6" />
                  <Point X="-2.029321897291" Y="-4.024887358065" Z="0.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.6" />
                  <Point X="-1.73195293951" Y="-3.999572505238" Z="0.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.6" />
                  <Point X="-1.480623603328" Y="-4.160515883274" Z="0.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.6" />
                  <Point X="-1.379206620598" Y="-4.441200149121" Z="0.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.6" />
                  <Point X="-1.376293318593" Y="-4.599936081094" Z="0.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.6" />
                  <Point X="-1.187399423128" Y="-4.937573802288" Z="0.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.6" />
                  <Point X="-0.888621198187" Y="-4.999990700724" Z="0.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="-0.722842284261" Y="-4.659868236804" Z="0.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="-0.292116778606" Y="-3.338715132313" Z="0.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="-0.059975281863" Y="-3.222853482634" Z="0.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.6" />
                  <Point X="0.193383797498" Y="-3.264258562654" Z="0.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.6" />
                  <Point X="0.378329080827" Y="-3.462930623455" Z="0.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.6" />
                  <Point X="0.511912576141" Y="-3.872667793011" Z="0.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.6" />
                  <Point X="0.955319495944" Y="-4.988756712448" Z="0.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.6" />
                  <Point X="1.134669874037" Y="-4.951045646084" Z="0.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.6" />
                  <Point X="1.125043783872" Y="-4.546706316799" Z="0.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.6" />
                  <Point X="0.998421085363" Y="-3.083935087217" Z="0.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.6" />
                  <Point X="1.148537915705" Y="-2.91110079488" Z="0.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.6" />
                  <Point X="1.369054060549" Y="-2.859303971334" Z="0.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.6" />
                  <Point X="1.586903214758" Y="-2.958810142491" Z="0.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.6" />
                  <Point X="1.879919658255" Y="-3.307363059802" Z="0.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.6" />
                  <Point X="2.81105937212" Y="-4.230197569533" Z="0.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.6" />
                  <Point X="3.001715851659" Y="-4.097112251915" Z="0.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.6" />
                  <Point X="2.862988969318" Y="-3.747242848765" Z="0.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.6" />
                  <Point X="2.241449315031" Y="-2.557361213372" Z="0.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.6" />
                  <Point X="2.304325063252" Y="-2.369185779704" Z="0.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.6" />
                  <Point X="2.463712598668" Y="-2.254576246715" Z="0.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.6" />
                  <Point X="2.67114559014" Y="-2.26199869446" Z="0.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.6" />
                  <Point X="3.040170748342" Y="-2.454760530956" Z="0.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.6" />
                  <Point X="4.198389134189" Y="-2.857148380933" Z="0.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.6" />
                  <Point X="4.361454874305" Y="-2.601437989508" Z="0.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.6" />
                  <Point X="4.113613405915" Y="-2.321201973159" Z="0.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.6" />
                  <Point X="3.116048255206" Y="-1.495299666792" Z="0.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.6" />
                  <Point X="3.104268092336" Y="-1.327834925502" Z="0.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.6" />
                  <Point X="3.191756859995" Y="-1.186628456608" Z="0.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.6" />
                  <Point X="3.356319865131" Y="-1.12526230415" Z="0.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.6" />
                  <Point X="3.75620495042" Y="-1.16290786747" Z="0.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.6" />
                  <Point X="4.971451688861" Y="-1.032007228156" Z="0.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.6" />
                  <Point X="5.034622247469" Y="-0.657993373377" Z="0.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.6" />
                  <Point X="4.740263723245" Y="-0.486699562078" Z="0.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.6" />
                  <Point X="3.677341722367" Y="-0.179996226331" Z="0.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.6" />
                  <Point X="3.613076128315" Y="-0.113353220375" Z="0.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.6" />
                  <Point X="3.58581452825" Y="-0.022869690964" Z="0.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.6" />
                  <Point X="3.595556922174" Y="0.073740840268" Z="0.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.6" />
                  <Point X="3.642303310086" Y="0.1505955182" Z="0.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.6" />
                  <Point X="3.726053691985" Y="0.208152617911" Z="0.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.6" />
                  <Point X="4.055704094061" Y="0.30327237069" Z="0.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.6" />
                  <Point X="4.997713292515" Y="0.892241707398" Z="0.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.6" />
                  <Point X="4.904771740163" Y="1.31013474821" Z="0.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.6" />
                  <Point X="4.545195356283" Y="1.364481864039" Z="0.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.6" />
                  <Point X="3.391250407235" Y="1.231522712239" Z="0.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.6" />
                  <Point X="3.315804255256" Y="1.26439100797" Z="0.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.6" />
                  <Point X="3.262637203432" Y="1.329425068505" Z="0.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.6" />
                  <Point X="3.237774638594" Y="1.412078167591" Z="0.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.6" />
                  <Point X="3.250020825593" Y="1.491094630085" Z="0.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.6" />
                  <Point X="3.299219935494" Y="1.566850780321" Z="0.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.6" />
                  <Point X="3.581437114599" Y="1.790752281626" Z="0.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.6" />
                  <Point X="4.287688831957" Y="2.718940097594" Z="0.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.6" />
                  <Point X="4.058108922492" Y="3.051010866978" Z="0.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.6" />
                  <Point X="3.648983490483" Y="2.924661638005" Z="0.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.6" />
                  <Point X="2.44859705846" Y="2.25061146709" Z="0.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.6" />
                  <Point X="2.376601075812" Y="2.251918857252" Z="0.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.6" />
                  <Point X="2.311844541624" Y="2.286689141941" Z="0.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.6" />
                  <Point X="2.264069458294" Y="2.345180318758" Z="0.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.6" />
                  <Point X="2.247510845507" Y="2.41315736802" Z="0.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.6" />
                  <Point X="2.261916475567" Y="2.490872517422" Z="0.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.6" />
                  <Point X="2.470963714854" Y="2.863155591472" Z="0.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.6" />
                  <Point X="2.842298494292" Y="4.205881881344" Z="0.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.6" />
                  <Point X="2.449914766763" Y="4.445900202318" Z="0.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.6" />
                  <Point X="2.041496510848" Y="4.647725197997" Z="0.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.6" />
                  <Point X="1.617886738469" Y="4.811601465918" Z="0.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.6" />
                  <Point X="1.017416207602" Y="4.968840584994" Z="0.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.6" />
                  <Point X="0.351684914208" Y="5.059730095877" Z="0.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.6" />
                  <Point X="0.14749961696" Y="4.905600566256" Z="0.6" />
                  <Point X="0" Y="4.355124473572" Z="0.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>