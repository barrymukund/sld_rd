<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#171" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2079" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004716796875" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.776409606934" Y="20.692146484375" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557721679688" Y="21.502857421875" />
                  <Point X="0.54236328125" Y="21.532625" />
                  <Point X="0.455260620117" Y="21.658123046875" />
                  <Point X="0.378635437012" Y="21.768525390625" />
                  <Point X="0.356752288818" Y="21.790978515625" />
                  <Point X="0.330497253418" Y="21.81022265625" />
                  <Point X="0.3024949646" Y="21.82433203125" />
                  <Point X="0.167708862305" Y="21.8661640625" />
                  <Point X="0.049135822296" Y="21.90296484375" />
                  <Point X="0.020976730347" Y="21.907234375" />
                  <Point X="-0.008664747238" Y="21.907234375" />
                  <Point X="-0.036823841095" Y="21.90296484375" />
                  <Point X="-0.171609924316" Y="21.8611328125" />
                  <Point X="-0.290182983398" Y="21.82433203125" />
                  <Point X="-0.318184967041" Y="21.81022265625" />
                  <Point X="-0.344440307617" Y="21.790978515625" />
                  <Point X="-0.366323608398" Y="21.768525390625" />
                  <Point X="-0.453425964355" Y="21.64302734375" />
                  <Point X="-0.530051269531" Y="21.532625" />
                  <Point X="-0.538189208984" Y="21.518427734375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.738463745117" Y="20.787814453125" />
                  <Point X="-0.916584655762" Y="20.12305859375" />
                  <Point X="-0.943340393066" Y="20.128251953125" />
                  <Point X="-1.079355224609" Y="20.154654296875" />
                  <Point X="-1.23242578125" Y="20.19403515625" />
                  <Point X="-1.24641809082" Y="20.197634765625" />
                  <Point X="-1.240616088867" Y="20.241705078125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.248709106445" Y="20.64565625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.44773840332" Y="20.977623046875" />
                  <Point X="-1.556905395508" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.64302746582" Y="21.109033203125" />
                  <Point X="-1.807727905273" Y="21.119828125" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.179894775391" Y="21.0135" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.440354736328" Y="20.763369140625" />
                  <Point X="-2.480147216797" Y="20.71151171875" />
                  <Point X="-2.602365722656" Y="20.787185546875" />
                  <Point X="-2.801726318359" Y="20.910623046875" />
                  <Point X="-3.013684082031" Y="21.07382421875" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.819797851562" Y="21.637423828125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575195312" Y="22.4148046875" />
                  <Point X="-2.414559326172" Y="22.444421875" />
                  <Point X="-2.428776367188" Y="22.473251953125" />
                  <Point X="-2.446806396484" Y="22.4984140625" />
                  <Point X="-2.464155029297" Y="22.51576171875" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.242627441406" Y="22.19040234375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.925368164062" Y="21.9992265625" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.234828125" Y="22.46096484375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.798889892578" Y="22.96977734375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556152344" Y="23.67201171875" />
                  <Point X="-3.052556152344" Y="23.70175390625" />
                  <Point X="-3.068636230469" Y="23.727736328125" />
                  <Point X="-3.089467041016" Y="23.751693359375" />
                  <Point X="-3.112971191406" Y="23.771232421875" />
                  <Point X="-3.139460205078" Y="23.786822265625" />
                  <Point X="-3.168729003906" Y="23.798046875" />
                  <Point X="-3.200612548828" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.993719970703" Y="23.70532421875" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.772480957031" Y="23.766197265625" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.874284179688" Y="24.288466796875" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-4.322233886719" Y="24.56808203125" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.510053955078" Y="24.789029296875" />
                  <Point X="-3.479723876953" Y="24.80649609375" />
                  <Point X="-3.466561523438" Y="24.815634765625" />
                  <Point X="-3.441752929688" Y="24.83616796875" />
                  <Point X="-3.425646728516" Y="24.85326953125" />
                  <Point X="-3.414229248047" Y="24.873798828125" />
                  <Point X="-3.401881347656" Y="24.90439453125" />
                  <Point X="-3.397366210938" Y="24.918779296875" />
                  <Point X="-3.390789794922" Y="24.947548828125" />
                  <Point X="-3.388401123047" Y="24.968947265625" />
                  <Point X="-3.390892578125" Y="24.990333984375" />
                  <Point X="-3.398060791016" Y="25.02101171875" />
                  <Point X="-3.402662841797" Y="25.03541796875" />
                  <Point X="-3.414418701172" Y="25.064107421875" />
                  <Point X="-3.425945800781" Y="25.084576171875" />
                  <Point X="-3.442143798828" Y="25.101591796875" />
                  <Point X="-3.468728759766" Y="25.123357421875" />
                  <Point X="-3.481956054688" Y="25.1324375" />
                  <Point X="-3.510510009766" Y="25.148671875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.227284667969" Y="25.343916015625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.864185546875" Y="25.708703125" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.743552734375" Y="26.2756484375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.331497558594" Y="26.37428515625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703138916016" Y="26.305263671875" />
                  <Point X="-3.670460449219" Y="26.31556640625" />
                  <Point X="-3.641712890625" Y="26.324630859375" />
                  <Point X="-3.622782470703" Y="26.332958984375" />
                  <Point X="-3.604038818359" Y="26.343779296875" />
                  <Point X="-3.587354248047" Y="26.35601171875" />
                  <Point X="-3.573713623047" Y="26.37156640625" />
                  <Point X="-3.561299316406" Y="26.389296875" />
                  <Point X="-3.551351318359" Y="26.4074296875" />
                  <Point X="-3.538239013672" Y="26.4390859375" />
                  <Point X="-3.526703857422" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.547871826172" Y="26.619771484375" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.000450195312" Y="27.000228515625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.23540234375" Y="27.46939453125" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.866760253906" Y="28.00923046875" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.554174804688" Y="28.045310546875" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187727783203" Y="27.836341796875" />
                  <Point X="-3.167084472656" Y="27.82983203125" />
                  <Point X="-3.146794677734" Y="27.825794921875" />
                  <Point X="-3.101282714844" Y="27.8218125" />
                  <Point X="-3.061245361328" Y="27.818310546875" />
                  <Point X="-3.040559082031" Y="27.81876171875" />
                  <Point X="-3.019101318359" Y="27.821587890625" />
                  <Point X="-2.999012207031" Y="27.826505859375" />
                  <Point X="-2.980462890625" Y="27.83565234375" />
                  <Point X="-2.962209228516" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.913773193359" Y="27.892533203125" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.847417724609" Y="28.081630859375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.046300048828" Y="28.487248046875" />
                  <Point X="-3.183333496094" Y="28.724595703125" />
                  <Point X="-2.969273681641" Y="28.88871484375" />
                  <Point X="-2.700629638672" Y="29.094681640625" />
                  <Point X="-2.362965087891" Y="29.282279296875" />
                  <Point X="-2.167035888672" Y="29.391134765625" />
                  <Point X="-2.149581298828" Y="29.36838671875" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028890869141" Y="29.214798828125" />
                  <Point X="-2.012310791016" Y="29.20088671875" />
                  <Point X="-1.995115356445" Y="29.189396484375" />
                  <Point X="-1.94446081543" Y="29.163025390625" />
                  <Point X="-1.899899291992" Y="29.139828125" />
                  <Point X="-1.880625488281" Y="29.13233203125" />
                  <Point X="-1.859719360352" Y="29.126728515625" />
                  <Point X="-1.839268920898" Y="29.123580078125" />
                  <Point X="-1.818621948242" Y="29.12493359375" />
                  <Point X="-1.797307006836" Y="29.128693359375" />
                  <Point X="-1.777452514648" Y="29.134482421875" />
                  <Point X="-1.724692260742" Y="29.156337890625" />
                  <Point X="-1.678278564453" Y="29.1755625" />
                  <Point X="-1.660147460938" Y="29.185509765625" />
                  <Point X="-1.642417480469" Y="29.197923828125" />
                  <Point X="-1.626864990234" Y="29.2115625" />
                  <Point X="-1.614633300781" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.578307739258" Y="29.32038671875" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.57779699707" Y="29.583248046875" />
                  <Point X="-1.584201660156" Y="29.631896484375" />
                  <Point X="-1.297416992188" Y="29.71230078125" />
                  <Point X="-0.949623657227" Y="29.80980859375" />
                  <Point X="-0.540293029785" Y="29.857716796875" />
                  <Point X="-0.294711242676" Y="29.886458984375" />
                  <Point X="-0.236708724976" Y="29.669990234375" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082114013672" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155910969" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425956726" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.236654342651" Y="29.623837890625" />
                  <Point X="0.307419555664" Y="29.8879375" />
                  <Point X="0.540370239258" Y="29.863541015625" />
                  <Point X="0.844045166016" Y="29.83173828125" />
                  <Point X="1.182705322266" Y="29.749974609375" />
                  <Point X="1.48102331543" Y="29.677951171875" />
                  <Point X="1.700771484375" Y="29.598248046875" />
                  <Point X="1.894645385742" Y="29.527927734375" />
                  <Point X="2.107800537109" Y="29.4282421875" />
                  <Point X="2.294559326172" Y="29.340900390625" />
                  <Point X="2.50051953125" Y="29.220908203125" />
                  <Point X="2.680977050781" Y="29.1157734375" />
                  <Point X="2.875187255859" Y="28.977662109375" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.605963623047" Y="28.3450390625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.121654785156" Y="27.473341796875" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.112181640625" Y="27.344017578125" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.162924316406" Y="27.213791015625" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.19446484375" Y="27.170328125" />
                  <Point X="2.221597167969" Y="27.14559375" />
                  <Point X="2.255277099609" Y="27.122740234375" />
                  <Point X="2.284905761719" Y="27.102634765625" />
                  <Point X="2.304947265625" Y="27.0922734375" />
                  <Point X="2.327036865234" Y="27.084005859375" />
                  <Point X="2.348965087891" Y="27.078662109375" />
                  <Point X="2.385898925781" Y="27.074208984375" />
                  <Point X="2.418389892578" Y="27.070291015625" />
                  <Point X="2.436468505859" Y="27.06984375" />
                  <Point X="2.473207519531" Y="27.074169921875" />
                  <Point X="2.515919677734" Y="27.085591796875" />
                  <Point X="2.553493896484" Y="27.095640625" />
                  <Point X="2.565290771484" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.11014453125" />
                  <Point X="3.286975097656" Y="27.513388671875" />
                  <Point X="3.967326904297" Y="27.906189453125" />
                  <Point X="4.016229248047" Y="27.8382265625" />
                  <Point X="4.1232734375" Y="27.689458984375" />
                  <Point X="4.231541015625" Y="27.510546875" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.834287353516" Y="27.131537109375" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221422119141" Y="26.66023828125" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.173234375" Y="26.601525390625" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.13660546875" Y="26.55091015625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.110179199219" Y="26.476140625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.107138916016" Y="26.3262109375" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136282714844" Y="26.235740234375" />
                  <Point X="3.161849121094" Y="26.196880859375" />
                  <Point X="3.184340332031" Y="26.1626953125" />
                  <Point X="3.198891601562" Y="26.145453125" />
                  <Point X="3.216134521484" Y="26.12936328125" />
                  <Point X="3.234346435547" Y="26.11603515625" />
                  <Point X="3.271395751953" Y="26.0951796875" />
                  <Point X="3.303988525391" Y="26.07683203125" />
                  <Point X="3.320520751953" Y="26.069501953125" />
                  <Point X="3.356120605469" Y="26.0594375" />
                  <Point X="3.406213623047" Y="26.052818359375" />
                  <Point X="3.45028125" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.15167578125" Y="26.13433203125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.799961914062" Y="26.12165625" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.8800546875" Y="25.713669921875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.408208007812" Y="25.514912109375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704785644531" Y="25.325583984375" />
                  <Point X="3.681546875" Y="25.31506640625" />
                  <Point X="3.63233203125" Y="25.286619140625" />
                  <Point X="3.589036865234" Y="25.261595703125" />
                  <Point X="3.574312255859" Y="25.25109765625" />
                  <Point X="3.547530517578" Y="25.225576171875" />
                  <Point X="3.518001708984" Y="25.18794921875" />
                  <Point X="3.492024658203" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.453837646484" Y="25.04120703125" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.455021728516" Y="24.890048828125" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.521553466797" Y="24.74496484375" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559994873047" Y="24.698767578125" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.638250732422" Y="24.647396484375" />
                  <Point X="3.681545654297" Y="24.62237109375" />
                  <Point X="3.692708496094" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.325014160156" Y="24.444818359375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.880692382812" Y="24.22153515625" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.811311035156" Y="23.85972265625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.226476074219" Y="23.8909609375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.278067382812" Y="23.97349609375" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163974121094" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112397460938" Y="23.906041015625" />
                  <Point X="3.054013916016" Y="23.83582421875" />
                  <Point X="3.002653320312" Y="23.774052734375" />
                  <Point X="2.987932128906" Y="23.749669921875" />
                  <Point X="2.976589111328" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.961389892578" Y="23.60369921875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.029906005859" Y="23.348857421875" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.675259033203" Y="22.80583203125" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.197171875" Y="22.3673046875" />
                  <Point X="4.124814453125" Y="22.25021875" />
                  <Point X="4.034411376953" Y="22.12176953125" />
                  <Point X="4.028980957031" Y="22.1140546875" />
                  <Point X="3.515327636719" Y="22.410611328125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.639265380859" Y="22.860935546875" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.349330810547" Y="22.814560546875" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.15426953125" Y="22.61405859375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.116437011719" Y="22.321783203125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.514651123047" Y="21.5454765625" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781861083984" Y="20.888365234375" />
                  <Point X="2.701763183594" Y="20.836517578125" />
                  <Point X="2.303678710938" Y="21.3553125" />
                  <Point X="1.758546264648" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923583984" Y="22.099443359375" />
                  <Point X="1.60854284668" Y="22.1723359375" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986572266" Y="22.24883203125" />
                  <Point X="1.448366210938" Y="22.2565625" />
                  <Point X="1.417100585938" Y="22.258880859375" />
                  <Point X="1.293099243164" Y="22.247470703125" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156368164062" Y="22.230603515625" />
                  <Point X="1.128982666016" Y="22.21926171875" />
                  <Point X="1.104594848633" Y="22.2045390625" />
                  <Point X="1.008844299316" Y="22.124923828125" />
                  <Point X="0.92461126709" Y="22.05488671875" />
                  <Point X="0.904139892578" Y="22.031134765625" />
                  <Point X="0.887247619629" Y="22.003306640625" />
                  <Point X="0.875624267578" Y="21.974189453125" />
                  <Point X="0.846995361328" Y="21.842474609375" />
                  <Point X="0.821809997559" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.922566040039" Y="20.89585546875" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.97572277832" Y="20.129927734375" />
                  <Point X="0.929315490723" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058456054687" Y="20.24737109375" />
                  <Point X="-1.14124621582" Y="20.268669921875" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.155534545898" Y="20.664189453125" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902722168" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575195312" Y="20.905138671875" />
                  <Point X="-1.250209350586" Y="20.9290625" />
                  <Point X="-1.261006591797" Y="20.94021875" />
                  <Point X="-1.385100219727" Y="21.049046875" />
                  <Point X="-1.494267211914" Y="21.144783203125" />
                  <Point X="-1.506735839844" Y="21.15403125" />
                  <Point X="-1.53301965332" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531860352" Y="21.189775390625" />
                  <Point X="-1.591315795898" Y="21.194525390625" />
                  <Point X="-1.62145690918" Y="21.201552734375" />
                  <Point X="-1.636814086914" Y="21.203830078125" />
                  <Point X="-1.801514648438" Y="21.214625" />
                  <Point X="-1.946403564453" Y="21.22412109375" />
                  <Point X="-1.961927856445" Y="21.2238671875" />
                  <Point X="-1.992726196289" Y="21.220833984375" />
                  <Point X="-2.008" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053666992188" Y="21.204505859375" />
                  <Point X="-2.081861328125" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.232674072266" Y="21.092490234375" />
                  <Point X="-2.353403320312" Y="21.011822265625" />
                  <Point X="-2.359686279297" Y="21.007240234375" />
                  <Point X="-2.380448730469" Y="20.98986328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503200195312" Y="20.837521484375" />
                  <Point X="-2.552354980469" Y="20.86795703125" />
                  <Point X="-2.747612548828" Y="20.988853515625" />
                  <Point X="-2.9557265625" Y="21.149095703125" />
                  <Point X="-2.98086328125" Y="21.16844921875" />
                  <Point X="-2.737525390625" Y="21.589923828125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665771484" Y="22.442380859375" />
                  <Point X="-2.323649902344" Y="22.471998046875" />
                  <Point X="-2.329355957031" Y="22.4864375" />
                  <Point X="-2.343572998047" Y="22.515267578125" />
                  <Point X="-2.3515546875" Y="22.5285859375" />
                  <Point X="-2.369584716797" Y="22.553748046875" />
                  <Point X="-2.379633056641" Y="22.565591796875" />
                  <Point X="-2.396981689453" Y="22.582939453125" />
                  <Point X="-2.408822021484" Y="22.592984375" />
                  <Point X="-2.433978027344" Y="22.611009765625" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.290127441406" Y="22.272673828125" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.849774902344" Y="22.056765625" />
                  <Point X="-4.004022460937" Y="22.259416015625" />
                  <Point X="-4.153235351563" Y="22.509623046875" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.741057617188" Y="22.894408203125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950786376953" Y="23.67862109375" />
                  <Point X="-2.953082763672" Y="23.693775390625" />
                  <Point X="-2.960082763672" Y="23.723517578125" />
                  <Point X="-2.971774902344" Y="23.751748046875" />
                  <Point X="-2.987854980469" Y="23.77773046875" />
                  <Point X="-2.996946533203" Y="23.7900703125" />
                  <Point X="-3.01777734375" Y="23.81402734375" />
                  <Point X="-3.028737060547" Y="23.824748046875" />
                  <Point X="-3.052241210938" Y="23.844287109375" />
                  <Point X="-3.064785644531" Y="23.85310546875" />
                  <Point X="-3.091274658203" Y="23.8686953125" />
                  <Point X="-3.105443359375" Y="23.8755234375" />
                  <Point X="-3.134712158203" Y="23.886748046875" />
                  <Point X="-3.149812255859" Y="23.89114453125" />
                  <Point X="-3.181695800781" Y="23.897623046875" />
                  <Point X="-3.19730859375" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.006119873047" Y="23.79951171875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.680436035156" Y="23.789708984375" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.780241210938" Y="24.301916015625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.297645996094" Y="24.476318359375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.496566162109" Y="24.691802734375" />
                  <Point X="-3.473744140625" Y="24.7012421875" />
                  <Point X="-3.462644042969" Y="24.706705078125" />
                  <Point X="-3.432313964844" Y="24.724171875" />
                  <Point X="-3.425543701172" Y="24.7284609375" />
                  <Point X="-3.405989257812" Y="24.74244921875" />
                  <Point X="-3.381180664062" Y="24.762982421875" />
                  <Point X="-3.372595458984" Y="24.77103515625" />
                  <Point X="-3.356489257812" Y="24.78813671875" />
                  <Point X="-3.342623046875" Y="24.807095703125" />
                  <Point X="-3.331205566406" Y="24.827625" />
                  <Point X="-3.326133300781" Y="24.838244140625" />
                  <Point X="-3.313785400391" Y="24.86883984375" />
                  <Point X="-3.311241455078" Y="24.875943359375" />
                  <Point X="-3.304755126953" Y="24.897609375" />
                  <Point X="-3.298178710938" Y="24.92637890625" />
                  <Point X="-3.296376220703" Y="24.937009765625" />
                  <Point X="-3.293987548828" Y="24.958408203125" />
                  <Point X="-3.294039306641" Y="24.979939453125" />
                  <Point X="-3.296530761719" Y="25.001326171875" />
                  <Point X="-3.298384277344" Y="25.01194921875" />
                  <Point X="-3.305552490234" Y="25.042626953125" />
                  <Point X="-3.307565917969" Y="25.049919921875" />
                  <Point X="-3.314756591797" Y="25.071439453125" />
                  <Point X="-3.326512451172" Y="25.10012890625" />
                  <Point X="-3.331642333984" Y="25.11072265625" />
                  <Point X="-3.343169433594" Y="25.13119140625" />
                  <Point X="-3.357137939453" Y="25.150078125" />
                  <Point X="-3.3733359375" Y="25.16709375" />
                  <Point X="-3.381962646484" Y="25.17509765625" />
                  <Point X="-3.408547607422" Y="25.19686328125" />
                  <Point X="-3.414963623047" Y="25.2016796875" />
                  <Point X="-3.435002197266" Y="25.2150234375" />
                  <Point X="-3.463556152344" Y="25.2312578125" />
                  <Point X="-3.474445556641" Y="25.236560546875" />
                  <Point X="-3.496811523438" Y="25.24573828125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.202696777344" Y="25.4356796875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.770208984375" Y="25.694796875" />
                  <Point X="-4.73133203125" Y="25.957521484375" />
                  <Point X="-4.651859375" Y="26.25080078125" />
                  <Point X="-4.633585449219" Y="26.318236328125" />
                  <Point X="-4.343897949219" Y="26.28009765625" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736704101562" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704887939453" Y="26.2080546875" />
                  <Point X="-3.684603759766" Y="26.21208984375" />
                  <Point X="-3.674573730469" Y="26.21466015625" />
                  <Point X="-3.641895263672" Y="26.224962890625" />
                  <Point X="-3.613147705078" Y="26.23402734375" />
                  <Point X="-3.603457519531" Y="26.237673828125" />
                  <Point X="-3.584527099609" Y="26.246001953125" />
                  <Point X="-3.575286865234" Y="26.25068359375" />
                  <Point X="-3.556543212891" Y="26.26150390625" />
                  <Point X="-3.547867919922" Y="26.2671640625" />
                  <Point X="-3.531183349609" Y="26.279396484375" />
                  <Point X="-3.515928466797" Y="26.293375" />
                  <Point X="-3.502287841797" Y="26.3089296875" />
                  <Point X="-3.495892822266" Y="26.317078125" />
                  <Point X="-3.483478515625" Y="26.33480859375" />
                  <Point X="-3.478010253906" Y="26.343603515625" />
                  <Point X="-3.468062255859" Y="26.361736328125" />
                  <Point X="-3.463582519531" Y="26.37107421875" />
                  <Point X="-3.450470214844" Y="26.40273046875" />
                  <Point X="-3.438935058594" Y="26.430578125" />
                  <Point X="-3.435498535156" Y="26.4403515625" />
                  <Point X="-3.429710449219" Y="26.4602109375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.447784423828" Y="26.63324609375" />
                  <Point X="-3.463605957031" Y="26.663638671875" />
                  <Point X="-3.477524169922" Y="26.690375" />
                  <Point X="-3.482802246094" Y="26.6992890625" />
                  <Point X="-3.494293945312" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.942617675781" Y="27.07559765625" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.153355957031" Y="27.421505859375" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.791779541016" Y="27.950896484375" />
                  <Point X="-3.726337402344" Y="28.035013671875" />
                  <Point X="-3.601675048828" Y="27.9630390625" />
                  <Point X="-3.254157470703" Y="27.762400390625" />
                  <Point X="-3.244921630859" Y="27.75771875" />
                  <Point X="-3.2259921875" Y="27.749388671875" />
                  <Point X="-3.216298583984" Y="27.745740234375" />
                  <Point X="-3.195655273438" Y="27.73923046875" />
                  <Point X="-3.185623535156" Y="27.736658203125" />
                  <Point X="-3.165333740234" Y="27.73262109375" />
                  <Point X="-3.155075683594" Y="27.73115625" />
                  <Point X="-3.109563720703" Y="27.727173828125" />
                  <Point X="-3.069526367188" Y="27.723671875" />
                  <Point X="-3.059173828125" Y="27.723333984375" />
                  <Point X="-3.038487548828" Y="27.72378515625" />
                  <Point X="-3.028153808594" Y="27.72457421875" />
                  <Point X="-3.006696044922" Y="27.727400390625" />
                  <Point X="-2.99651171875" Y="27.7293125" />
                  <Point X="-2.976422607422" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.93844921875" Y="27.750447265625" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911165527344" Y="27.767158203125" />
                  <Point X="-2.902745117188" Y="27.773193359375" />
                  <Point X="-2.886613769531" Y="27.786140625" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.846598144531" Y="27.825357421875" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752779296875" Y="28.08991015625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.964027587891" Y="28.534748046875" />
                  <Point X="-3.059388183594" Y="28.699916015625" />
                  <Point X="-2.911471191406" Y="28.81332421875" />
                  <Point X="-2.648385498047" Y="29.015029296875" />
                  <Point X="-2.316827880859" Y="29.199234375" />
                  <Point X="-2.192524414062" Y="29.268294921875" />
                  <Point X="-2.118564941406" Y="29.17191015625" />
                  <Point X="-2.111821533203" Y="29.16405078125" />
                  <Point X="-2.097517089844" Y="29.149107421875" />
                  <Point X="-2.089955078125" Y="29.1420234375" />
                  <Point X="-2.073375" Y="29.128111328125" />
                  <Point X="-2.065091796875" Y="29.1218984375" />
                  <Point X="-2.047896484375" Y="29.110408203125" />
                  <Point X="-2.038984130859" Y="29.105130859375" />
                  <Point X="-1.988329589844" Y="29.078759765625" />
                  <Point X="-1.943768188477" Y="29.0555625" />
                  <Point X="-1.934334594727" Y="29.0512890625" />
                  <Point X="-1.915060668945" Y="29.04379296875" />
                  <Point X="-1.905220458984" Y="29.0405703125" />
                  <Point X="-1.884314331055" Y="29.034966796875" />
                  <Point X="-1.874174682617" Y="29.032833984375" />
                  <Point X="-1.853724243164" Y="29.029685546875" />
                  <Point X="-1.8330546875" Y="29.028783203125" />
                  <Point X="-1.812407592773" Y="29.03013671875" />
                  <Point X="-1.802119506836" Y="29.031376953125" />
                  <Point X="-1.78080456543" Y="29.03513671875" />
                  <Point X="-1.77071472168" Y="29.037490234375" />
                  <Point X="-1.750860229492" Y="29.043279296875" />
                  <Point X="-1.741095581055" Y="29.04671484375" />
                  <Point X="-1.688335327148" Y="29.0685703125" />
                  <Point X="-1.641921630859" Y="29.087794921875" />
                  <Point X="-1.632583862305" Y="29.0922734375" />
                  <Point X="-1.614452758789" Y="29.102220703125" />
                  <Point X="-1.605659423828" Y="29.107689453125" />
                  <Point X="-1.587929443359" Y="29.120103515625" />
                  <Point X="-1.579780883789" Y="29.126498046875" />
                  <Point X="-1.564228393555" Y="29.14013671875" />
                  <Point X="-1.550253173828" Y="29.15538671875" />
                  <Point X="-1.538021484375" Y="29.172068359375" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.5215390625" Y="29.19948828125" />
                  <Point X="-1.516856689453" Y="29.2087265625" />
                  <Point X="-1.508525634766" Y="29.22766015625" />
                  <Point X="-1.504876953125" Y="29.23735546875" />
                  <Point X="-1.487704467773" Y="29.2918203125" />
                  <Point X="-1.47259753418" Y="29.339732421875" />
                  <Point X="-1.470026367188" Y="29.349763671875" />
                  <Point X="-1.465991210938" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.271771240234" Y="29.620828125" />
                  <Point X="-0.931164794922" Y="29.7163203125" />
                  <Point X="-0.529249572754" Y="29.763361328125" />
                  <Point X="-0.36522253418" Y="29.78255859375" />
                  <Point X="-0.32847164917" Y="29.64540234375" />
                  <Point X="-0.225666366577" Y="29.2617265625" />
                  <Point X="-0.220435150146" Y="29.247107421875" />
                  <Point X="-0.207661453247" Y="29.218916015625" />
                  <Point X="-0.200119293213" Y="29.20534375" />
                  <Point X="-0.182261199951" Y="29.1786171875" />
                  <Point X="-0.172608886719" Y="29.166455078125" />
                  <Point X="-0.15145123291" Y="29.143865234375" />
                  <Point X="-0.126896499634" Y="29.1250234375" />
                  <Point X="-0.099600570679" Y="29.11043359375" />
                  <Point X="-0.085354026794" Y="29.1042578125" />
                  <Point X="-0.054916057587" Y="29.09392578125" />
                  <Point X="-0.039853721619" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629489899" Y="29.08511328125" />
                  <Point X="0.052165550232" Y="29.090154296875" />
                  <Point X="0.067227882385" Y="29.09392578125" />
                  <Point X="0.097665855408" Y="29.1042578125" />
                  <Point X="0.111912246704" Y="29.11043359375" />
                  <Point X="0.139208175659" Y="29.1250234375" />
                  <Point X="0.163763214111" Y="29.143865234375" />
                  <Point X="0.184920715332" Y="29.166455078125" />
                  <Point X="0.194573181152" Y="29.1786171875" />
                  <Point X="0.212431259155" Y="29.20534375" />
                  <Point X="0.219973724365" Y="29.218916015625" />
                  <Point X="0.232747116089" Y="29.247107421875" />
                  <Point X="0.23797819519" Y="29.2617265625" />
                  <Point X="0.328417327881" Y="29.59925" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.530475219727" Y="29.76905859375" />
                  <Point X="0.827864990234" Y="29.7379140625" />
                  <Point X="1.160409790039" Y="29.657626953125" />
                  <Point X="1.453608520508" Y="29.58683984375" />
                  <Point X="1.668379638672" Y="29.50894140625" />
                  <Point X="1.858246704102" Y="29.44007421875" />
                  <Point X="2.067555664063" Y="29.3421875" />
                  <Point X="2.250432373047" Y="29.256662109375" />
                  <Point X="2.452696777344" Y="29.138822265625" />
                  <Point X="2.629434570312" Y="29.03585546875" />
                  <Point X="2.817778808594" Y="28.9019140625" />
                  <Point X="2.523691162109" Y="28.3925390625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.029879516602" Y="27.4978828125" />
                  <Point X="2.019831542969" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.017864868164" Y="27.33264453125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318359375" Y="27.223888671875" />
                  <Point X="2.055680908203" Y="27.20384375" />
                  <Point X="2.061459960938" Y="27.19412890625" />
                  <Point X="2.084313232422" Y="27.16044921875" />
                  <Point X="2.104417724609" Y="27.1308203125" />
                  <Point X="2.109808105469" Y="27.1236328125" />
                  <Point X="2.130463867188" Y="27.100123046875" />
                  <Point X="2.157596191406" Y="27.075388671875" />
                  <Point X="2.168255615234" Y="27.066982421875" />
                  <Point X="2.201935546875" Y="27.04412890625" />
                  <Point X="2.231564208984" Y="27.0240234375" />
                  <Point X="2.241277099609" Y="27.01824609375" />
                  <Point X="2.261318603516" Y="27.007884765625" />
                  <Point X="2.271647216797" Y="27.00330078125" />
                  <Point X="2.293736816406" Y="26.995033203125" />
                  <Point X="2.304544189453" Y="26.99170703125" />
                  <Point X="2.326472412109" Y="26.98636328125" />
                  <Point X="2.337593261719" Y="26.984345703125" />
                  <Point X="2.374527099609" Y="26.979892578125" />
                  <Point X="2.407018066406" Y="26.975974609375" />
                  <Point X="2.416040283203" Y="26.9753203125" />
                  <Point X="2.447578369141" Y="26.97549609375" />
                  <Point X="2.484317382813" Y="26.979822265625" />
                  <Point X="2.497749511719" Y="26.98239453125" />
                  <Point X="2.540461669922" Y="26.99381640625" />
                  <Point X="2.578035888672" Y="27.003865234375" />
                  <Point X="2.583999755859" Y="27.005671875" />
                  <Point X="2.604413085938" Y="27.0130703125" />
                  <Point X="2.62765625" Y="27.02357421875" />
                  <Point X="2.636033935547" Y="27.02787109375" />
                  <Point X="3.334475097656" Y="27.431115234375" />
                  <Point X="3.940405761719" Y="27.78094921875" />
                  <Point X="4.043956787109" Y="27.637037109375" />
                  <Point X="4.136885253906" Y="27.48347265625" />
                  <Point X="3.776455322266" Y="27.20690625" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.1681328125" Y="26.739865234375" />
                  <Point X="3.152115478516" Y="26.725212890625" />
                  <Point X="3.134667724609" Y="26.7066015625" />
                  <Point X="3.128577636719" Y="26.699421875" />
                  <Point X="3.097837646484" Y="26.6593203125" />
                  <Point X="3.070795410156" Y="26.624041015625" />
                  <Point X="3.065636230469" Y="26.6166015625" />
                  <Point X="3.049738769531" Y="26.58937109375" />
                  <Point X="3.034763183594" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.018689453125" Y="26.5017265625" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.3525703125" />
                  <Point X="3.014098632812" Y="26.307013671875" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034684570313" Y="26.228607421875" />
                  <Point X="3.050286865234" Y="26.19537109375" />
                  <Point X="3.056918945312" Y="26.183525390625" />
                  <Point X="3.082485351562" Y="26.144666015625" />
                  <Point X="3.1049765625" Y="26.11048046875" />
                  <Point X="3.111739257813" Y="26.101423828125" />
                  <Point X="3.126290527344" Y="26.084181640625" />
                  <Point X="3.134079101562" Y="26.07599609375" />
                  <Point X="3.151322021484" Y="26.05990625" />
                  <Point X="3.160029785156" Y="26.05269921875" />
                  <Point X="3.178241699219" Y="26.03937109375" />
                  <Point X="3.187745849609" Y="26.03325" />
                  <Point X="3.224795166016" Y="26.01239453125" />
                  <Point X="3.257387939453" Y="25.994046875" />
                  <Point X="3.265482421875" Y="25.989986328125" />
                  <Point X="3.294676269531" Y="25.9780859375" />
                  <Point X="3.330276123047" Y="25.968021484375" />
                  <Point X="3.34367578125" Y="25.965255859375" />
                  <Point X="3.393768798828" Y="25.95863671875" />
                  <Point X="3.437836425781" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.164075683594" Y="26.04014453125" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.707657714844" Y="26.099185546875" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.383620117188" Y="25.60667578125" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686027587891" Y="25.41954296875" />
                  <Point X="3.665614746094" Y="25.4121328125" />
                  <Point X="3.642375976562" Y="25.401615234375" />
                  <Point X="3.634005371094" Y="25.397314453125" />
                  <Point X="3.584790527344" Y="25.3688671875" />
                  <Point X="3.541495361328" Y="25.34384375" />
                  <Point X="3.533887207031" Y="25.33894921875" />
                  <Point X="3.508774658203" Y="25.31987109375" />
                  <Point X="3.481992919922" Y="25.294349609375" />
                  <Point X="3.472796386719" Y="25.2842265625" />
                  <Point X="3.443267578125" Y="25.246599609375" />
                  <Point X="3.417290527344" Y="25.213498046875" />
                  <Point X="3.410854248047" Y="25.20420703125" />
                  <Point X="3.399130615234" Y="25.184927734375" />
                  <Point X="3.393843261719" Y="25.174939453125" />
                  <Point X="3.384069580078" Y="25.1534765625" />
                  <Point X="3.380005615234" Y="25.1429296875" />
                  <Point X="3.373158935547" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.360533203125" Y="25.059076171875" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.00496484375" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280029297" Y="24.937056640625" />
                  <Point X="3.351874267578" Y="24.923576171875" />
                  <Point X="3.361717285156" Y="24.8721796875" />
                  <Point X="3.370376464844" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.816009765625" />
                  <Point X="3.380006103516" Y="24.794509765625" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399128417969" Y="24.752515625" />
                  <Point X="3.410852294922" Y="24.733234375" />
                  <Point X="3.417290527344" Y="24.72394140625" />
                  <Point X="3.446819335938" Y="24.686314453125" />
                  <Point X="3.472796386719" Y="24.653212890625" />
                  <Point X="3.478717041016" Y="24.6463671875" />
                  <Point X="3.501133544922" Y="24.62419921875" />
                  <Point X="3.530174316406" Y="24.601275390625" />
                  <Point X="3.541494384766" Y="24.593595703125" />
                  <Point X="3.590709472656" Y="24.5651484375" />
                  <Point X="3.634004394531" Y="24.540123046875" />
                  <Point X="3.639486328125" Y="24.537189453125" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.68302734375" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.300426269531" Y="24.3530546875" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.238875976563" Y="23.9851484375" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354481689453" Y="24.087322265625" />
                  <Point X="3.257890136719" Y="24.066328125" />
                  <Point X="3.172917480469" Y="24.047859375" />
                  <Point X="3.157873291016" Y="24.0432578125" />
                  <Point X="3.128752685547" Y="24.0316328125" />
                  <Point X="3.114676269531" Y="24.024609375" />
                  <Point X="3.086849365234" Y="24.007716796875" />
                  <Point X="3.074126953125" Y="23.998470703125" />
                  <Point X="3.050377197266" Y="23.978001953125" />
                  <Point X="3.039349853516" Y="23.966779296875" />
                  <Point X="2.980966308594" Y="23.8965625" />
                  <Point X="2.929605712891" Y="23.834791015625" />
                  <Point X="2.921326416016" Y="23.823154296875" />
                  <Point X="2.906605224609" Y="23.798771484375" />
                  <Point X="2.900163330078" Y="23.786025390625" />
                  <Point X="2.8888203125" Y="23.758640625" />
                  <Point X="2.884362304688" Y="23.745072265625" />
                  <Point X="2.877530761719" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.866789550781" Y="23.612404296875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158935547" Y="23.394517578125" />
                  <Point X="2.896541259766" Y="23.380626953125" />
                  <Point X="2.949996337891" Y="23.297482421875" />
                  <Point X="2.997021484375" Y="23.224337890625" />
                  <Point X="3.001740966797" Y="23.2176484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.617426513672" Y="22.730462890625" />
                  <Point X="4.087170898438" Y="22.370015625" />
                  <Point X="4.045498046875" Y="22.30258203125" />
                  <Point X="4.001274658203" Y="22.239748046875" />
                  <Point X="3.562827392578" Y="22.492884765625" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.656149414062" Y="22.954423828125" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846435547" Y="22.96486328125" />
                  <Point X="2.415069091797" Y="22.9550390625" />
                  <Point X="2.40058984375" Y="22.948890625" />
                  <Point X="2.305086914062" Y="22.89862890625" />
                  <Point X="2.221071777344" Y="22.854412109375" />
                  <Point X="2.208968994141" Y="22.846830078125" />
                  <Point X="2.186038818359" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940429687" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.070201660156" Y="22.658302734375" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.022949462891" Y="22.304900390625" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.432378662109" Y="21.4979765625" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723751464844" Y="20.963916015625" />
                  <Point X="2.379047363281" Y="21.41314453125" />
                  <Point X="1.833914916992" Y="22.123576171875" />
                  <Point X="1.828658569336" Y="22.12984765625" />
                  <Point X="1.808834350586" Y="22.15037109375" />
                  <Point X="1.783251098633" Y="22.17199609375" />
                  <Point X="1.773297973633" Y="22.179353515625" />
                  <Point X="1.659917236328" Y="22.25224609375" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.546280029297" Y="22.323755859375" />
                  <Point X="1.517466308594" Y="22.336126953125" />
                  <Point X="1.502547485352" Y="22.34111328125" />
                  <Point X="1.470927124023" Y="22.34884375" />
                  <Point X="1.455391235352" Y="22.351302734375" />
                  <Point X="1.424125610352" Y="22.35362109375" />
                  <Point X="1.408395629883" Y="22.35348046875" />
                  <Point X="1.28439440918" Y="22.3420703125" />
                  <Point X="1.175308959961" Y="22.332033203125" />
                  <Point X="1.161228027344" Y="22.32966015625" />
                  <Point X="1.133582641602" Y="22.322830078125" />
                  <Point X="1.120017822266" Y="22.318373046875" />
                  <Point X="1.092632202148" Y="22.30703125" />
                  <Point X="1.079885131836" Y="22.300591796875" />
                  <Point X="1.055497314453" Y="22.285869140625" />
                  <Point X="1.043857055664" Y="22.2775859375" />
                  <Point X="0.948106384277" Y="22.197970703125" />
                  <Point X="0.863873352051" Y="22.12793359375" />
                  <Point X="0.852650695801" Y="22.116908203125" />
                  <Point X="0.832179199219" Y="22.09315625" />
                  <Point X="0.822930725098" Y="22.0804296875" />
                  <Point X="0.806038391113" Y="22.0526015625" />
                  <Point X="0.79901776123" Y="22.03852734375" />
                  <Point X="0.78739440918" Y="22.00941015625" />
                  <Point X="0.782791809082" Y="21.9943671875" />
                  <Point X="0.754162902832" Y="21.86265234375" />
                  <Point X="0.728977539063" Y="21.74678125" />
                  <Point X="0.727584655762" Y="21.7387109375" />
                  <Point X="0.72472454834" Y="21.710322265625" />
                  <Point X="0.724742248535" Y="21.676830078125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.82837878418" Y="20.883455078125" />
                  <Point X="0.833091369629" Y="20.84766015625" />
                  <Point X="0.655064880371" Y="21.512064453125" />
                  <Point X="0.652606079102" Y="21.519876953125" />
                  <Point X="0.642147033691" Y="21.546416015625" />
                  <Point X="0.626788635254" Y="21.57618359375" />
                  <Point X="0.620407653809" Y="21.58679296875" />
                  <Point X="0.533304992676" Y="21.712291015625" />
                  <Point X="0.456679748535" Y="21.822693359375" />
                  <Point X="0.446668518066" Y="21.83483203125" />
                  <Point X="0.424785430908" Y="21.85728515625" />
                  <Point X="0.412913726807" Y="21.867599609375" />
                  <Point X="0.386658599854" Y="21.88684375" />
                  <Point X="0.373244659424" Y="21.8950625" />
                  <Point X="0.34524230957" Y="21.909171875" />
                  <Point X="0.330654083252" Y="21.9150625" />
                  <Point X="0.195867980957" Y="21.95689453125" />
                  <Point X="0.07729486084" Y="21.9936953125" />
                  <Point X="0.063377067566" Y="21.996890625" />
                  <Point X="0.035218082428" Y="22.00116015625" />
                  <Point X="0.020976737976" Y="22.002234375" />
                  <Point X="-0.00866476059" Y="22.002234375" />
                  <Point X="-0.022906103134" Y="22.00116015625" />
                  <Point X="-0.051065090179" Y="21.996890625" />
                  <Point X="-0.064982879639" Y="21.9936953125" />
                  <Point X="-0.199768981934" Y="21.95186328125" />
                  <Point X="-0.318342102051" Y="21.9150625" />
                  <Point X="-0.332930786133" Y="21.909169921875" />
                  <Point X="-0.360932678223" Y="21.895060546875" />
                  <Point X="-0.37434588623" Y="21.886845703125" />
                  <Point X="-0.400601318359" Y="21.8676015625" />
                  <Point X="-0.412473175049" Y="21.85728515625" />
                  <Point X="-0.434356536865" Y="21.83483203125" />
                  <Point X="-0.44436807251" Y="21.822693359375" />
                  <Point X="-0.531470458984" Y="21.6971953125" />
                  <Point X="-0.60809564209" Y="21.58679296875" />
                  <Point X="-0.612471252441" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778259277" Y="21.52378515625" />
                  <Point X="-0.642753051758" Y="21.512064453125" />
                  <Point X="-0.83022668457" Y="20.81240234375" />
                  <Point X="-0.985424926758" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698891849119" Y="24.368803749965" />
                  <Point X="-4.596416310565" Y="23.721798663202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.606623478301" Y="24.393527240001" />
                  <Point X="-4.502196738652" Y="23.734202754368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74428624928" Y="25.869978824827" />
                  <Point X="-4.696454512895" Y="25.567981126779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.514355107483" Y="24.418250730037" />
                  <Point X="-4.407977166739" Y="23.746606845534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.687554773949" Y="26.119073442571" />
                  <Point X="-4.596007473387" Y="25.541066534984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.422086736666" Y="24.442974220074" />
                  <Point X="-4.313757594826" Y="23.759010936699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.129597658724" Y="22.596270861191" />
                  <Point X="-4.102362990683" Y="22.424317934595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.62268764789" Y="26.316801583205" />
                  <Point X="-4.49556043388" Y="25.514151943188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.329818365848" Y="24.46769771011" />
                  <Point X="-4.219538022913" Y="23.771415027865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.043836282447" Y="22.662077897867" />
                  <Point X="-3.973764708285" Y="22.219663390362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.524455113263" Y="26.303868824955" />
                  <Point X="-4.395113394372" Y="25.487237351393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.237549969813" Y="24.492421040928" />
                  <Point X="-4.125318451" Y="23.783819119031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.958074906171" Y="22.727884934544" />
                  <Point X="-3.852306927458" Y="22.060092198744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.426222578636" Y="26.290936066705" />
                  <Point X="-4.294666354864" Y="25.460322759597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145281560277" Y="24.517144286509" />
                  <Point X="-4.031098879087" Y="23.796223210196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.872313529894" Y="22.79369197122" />
                  <Point X="-3.747929327279" Y="22.008361023556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.327990053553" Y="26.278003368716" />
                  <Point X="-4.194219313482" Y="25.433408155973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053013150742" Y="24.541867532089" />
                  <Point X="-3.936879315314" Y="23.80862735275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786552153618" Y="22.859499007897" />
                  <Point X="-3.659803664895" Y="22.05924054524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.229757577864" Y="26.265070982587" />
                  <Point X="-4.093772251777" Y="25.406493424028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960744741206" Y="24.56659077767" />
                  <Point X="-3.842659754476" Y="23.821031513842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.700790768528" Y="22.925305988927" />
                  <Point X="-3.571678002511" Y="22.110120066924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.131525102175" Y="26.252138596458" />
                  <Point X="-3.993325190071" Y="25.379578692082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.868476331671" Y="24.59131402325" />
                  <Point X="-3.748440193639" Y="23.833435674935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.61502937348" Y="22.991112907086" />
                  <Point X="-3.483552340127" Y="22.160999588608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206208630822" Y="27.330954894612" />
                  <Point X="-4.196635157867" Y="27.270510365244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.033292626486" Y="26.239206210329" />
                  <Point X="-3.892878128366" Y="25.352663960137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776207922135" Y="24.616037268831" />
                  <Point X="-3.654220632801" Y="23.845839836027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.529267978433" Y="23.056919825245" />
                  <Point X="-3.395426677743" Y="22.211879110292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130553692581" Y="27.460571469745" />
                  <Point X="-4.087144386955" Y="27.186495900598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.935060150797" Y="26.2262738242" />
                  <Point X="-3.79243106666" Y="25.325749228191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683939512599" Y="24.640760514411" />
                  <Point X="-3.560001071963" Y="23.858243997119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443506583385" Y="23.122726743405" />
                  <Point X="-3.307301015359" Y="22.262758631976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054898508028" Y="27.590186489722" />
                  <Point X="-3.977653616043" Y="27.102481435951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836827675109" Y="26.213341438072" />
                  <Point X="-3.691984004954" Y="25.298834496245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591671103064" Y="24.665483759992" />
                  <Point X="-3.465781511126" Y="23.870648158212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.357745188337" Y="23.188533661564" />
                  <Point X="-3.219175335307" Y="22.313638042108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.977945320216" Y="27.711606239656" />
                  <Point X="-3.868162710231" Y="27.018466119579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73926209108" Y="26.204619640175" />
                  <Point X="-3.591536943249" Y="25.2719197643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.499497907781" Y="24.690808164706" />
                  <Point X="-3.371561950288" Y="23.883052319304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271983793289" Y="23.254340579723" />
                  <Point X="-3.131049650979" Y="22.36451742524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9500610507" Y="21.221800376088" />
                  <Point X="-2.936159527508" Y="21.134029612982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.898030323634" Y="27.814325864986" />
                  <Point X="-3.758671740939" Y="26.934450402414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646090471585" Y="26.223640242502" />
                  <Point X="-3.490816380021" Y="25.243278211703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.410932759585" Y="24.73891288218" />
                  <Point X="-3.277342389451" Y="23.895456480396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.186222398242" Y="23.320147497882" />
                  <Point X="-3.04292396665" Y="22.415396808373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874582842869" Y="21.352532783114" />
                  <Point X="-2.826616358912" Y="21.04968432238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.818115327052" Y="27.917045490316" />
                  <Point X="-3.649180771648" Y="26.850434685248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.555963233305" Y="26.261882311347" />
                  <Point X="-3.384112190544" Y="25.176857529813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.329398421004" Y="24.831408384503" />
                  <Point X="-3.181494886987" Y="23.897582222583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.100461003194" Y="23.385954416042" />
                  <Point X="-2.954798282322" Y="22.466276191505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799104635038" Y="21.483265190139" />
                  <Point X="-2.717881959749" Y="20.970445401009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738200374673" Y="28.019765394726" />
                  <Point X="-3.53961305223" Y="26.765934386859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.473907007678" Y="26.351082748546" />
                  <Point X="-3.079652055325" Y="23.861854945959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015820720446" Y="23.458839758684" />
                  <Point X="-2.866672597994" Y="22.517155574637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.723626426434" Y="21.613997592283" />
                  <Point X="-2.611239807109" Y="20.904416404291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.636187593919" Y="27.982965101768" />
                  <Point X="-2.962470486422" Y="23.729282693847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948633875261" Y="23.641921769169" />
                  <Point X="-2.778546913665" Y="22.568034957769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.648148214404" Y="21.7447299728" />
                  <Point X="-2.504597540738" Y="20.838386689503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.530322766941" Y="27.921843946125" />
                  <Point X="-2.690421229337" Y="22.618914340901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572670002375" Y="21.875462353318" />
                  <Point X="-2.424517435236" Y="20.940063858139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.42445798897" Y="27.860723099908" />
                  <Point X="-2.59897071499" Y="22.648801573468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.497191790346" Y="22.006194733835" />
                  <Point X="-2.341010203885" Y="21.020103005751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.318593211" Y="27.799602253691" />
                  <Point X="-2.501758350502" Y="22.642309915982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.421713578317" Y="22.136927114353" />
                  <Point X="-2.254030876843" Y="21.078220203938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.213750870827" Y="27.744936825663" />
                  <Point X="-2.396018215296" Y="22.581976033202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346235366287" Y="22.26765949487" />
                  <Point X="-2.167051619644" Y="21.136337843096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114826235029" Y="27.727634312596" />
                  <Point X="-2.079789722273" Y="21.192670962436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.018361647112" Y="27.725863930567" />
                  <Point X="-1.988137694177" Y="21.22128588727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.927109087381" Y="27.757000999389" />
                  <Point X="-1.891836117121" Y="21.220544715309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988835441882" Y="28.754008919657" />
                  <Point X="-2.950357954424" Y="28.511071624941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.842414150872" Y="27.82954127176" />
                  <Point X="-1.794643005217" Y="21.214174613851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.903066474533" Y="28.819768028187" />
                  <Point X="-2.817812783762" Y="28.281497408948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763646788841" Y="27.939506776476" />
                  <Point X="-1.697449861476" Y="21.207804311383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81729737598" Y="28.885526308326" />
                  <Point X="-1.599463365678" Y="21.196424981162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.731528277426" Y="28.951284588464" />
                  <Point X="-1.495210768525" Y="21.145483044023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.64567846429" Y="29.016533256786" />
                  <Point X="-1.383535212437" Y="21.047674388663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.557273426802" Y="29.065648873479" />
                  <Point X="-1.271835925439" Y="20.94971590223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.468868389313" Y="29.114764490172" />
                  <Point X="-1.064664209175" Y="20.248968220916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.380463351824" Y="29.163880106864" />
                  <Point X="-0.973205008849" Y="20.278800612368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.292058333631" Y="29.212995845388" />
                  <Point X="-0.912753557903" Y="20.504408228433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.203653365012" Y="29.262111896908" />
                  <Point X="-0.852302106956" Y="20.730015844499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.08821820778" Y="29.140566054125" />
                  <Point X="-0.791850693193" Y="20.955623695327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.981698100348" Y="29.075307620526" />
                  <Point X="-0.731399300818" Y="21.181231681202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.878945697577" Y="29.033837537941" />
                  <Point X="-0.670947908444" Y="21.406839667076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.782908501716" Y="29.034765603147" />
                  <Point X="-0.604162968177" Y="21.592459205352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.691847991122" Y="29.067115222503" />
                  <Point X="-0.525849959994" Y="21.705293387356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.602446439933" Y="29.109939099308" />
                  <Point X="-0.447536956316" Y="21.818127597809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.520705828327" Y="29.201132245017" />
                  <Point X="-0.363307399539" Y="21.893606162171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.46687952926" Y="29.46856942379" />
                  <Point X="-0.272762126832" Y="21.929208865515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.389579463266" Y="29.587799071079" />
                  <Point X="-0.181084500807" Y="21.957662171381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.297484752932" Y="29.613619010258" />
                  <Point X="-0.089406820038" Y="21.986115131601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.205390006541" Y="29.63943872178" />
                  <Point X="0.004224329568" Y="22.002234375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.113295246182" Y="29.665258345116" />
                  <Point X="0.103025821994" Y="21.985709358585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.021200485824" Y="29.691077968453" />
                  <Point X="0.204182539513" Y="21.954314036174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.929053433101" Y="29.716567430795" />
                  <Point X="0.305339180201" Y="21.922919198855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.834619823637" Y="29.727620142047" />
                  <Point X="0.409939895638" Y="21.869779329371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.740186214173" Y="29.738672853298" />
                  <Point X="0.530406292652" Y="21.716467488797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.645752604709" Y="29.74972556455" />
                  <Point X="0.664602301079" Y="21.476470293368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.551318995245" Y="29.760778275802" />
                  <Point X="0.72472781782" Y="21.704135777016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.456885330271" Y="29.77183063658" />
                  <Point X="0.778251986242" Y="21.973480533613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.358320496243" Y="29.756799822485" />
                  <Point X="0.851864024498" Y="22.115995471619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.16782608903" Y="29.161348526451" />
                  <Point X="0.936582243577" Y="22.188388743633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.061306771524" Y="29.096095080248" />
                  <Point X="1.021573587827" Y="22.259057571183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.036234878219" Y="29.087524397482" />
                  <Point X="1.109080557965" Y="22.31384336197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.127473249814" Y="29.118751046669" />
                  <Point X="1.201994858683" Y="22.334488611121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.210419392076" Y="29.20233277137" />
                  <Point X="1.296797460294" Y="22.343211597645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.275196708784" Y="29.400627945928" />
                  <Point X="1.391599997045" Y="22.351934993682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.33564811779" Y="29.626235826793" />
                  <Point X="1.488972533146" Y="22.344432052428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.407165936177" Y="29.781973148669" />
                  <Point X="1.592937232671" Y="22.29530782937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.504972547248" Y="29.771729565917" />
                  <Point X="1.700025803385" Y="22.226460259867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.602779079982" Y="29.761486477764" />
                  <Point X="1.808172756712" Y="22.15093032553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.700585585086" Y="29.751243564064" />
                  <Point X="1.928138118541" Y="22.000781896618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.798392090189" Y="29.741000650364" />
                  <Point X="2.049339626482" Y="21.842828748316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.897736990182" Y="29.721044693605" />
                  <Point X="2.028819237522" Y="22.579672441234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.112349483432" Y="22.052283224603" />
                  <Point X="2.170541134423" Y="21.684875600014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.997745413761" Y="29.69689941379" />
                  <Point X="2.102753447765" Y="22.720153265374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244894806411" Y="21.822708046922" />
                  <Point X="2.291742642364" Y="21.526922451712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.097753837341" Y="29.672754133975" />
                  <Point X="2.182087652486" Y="22.826539866192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37744012939" Y="21.593132869241" />
                  <Point X="2.41294416252" Y="21.368969226293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.197762248718" Y="29.648608931206" />
                  <Point X="2.269795818253" Y="22.88005535777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509985392717" Y="21.363558068195" />
                  <Point X="2.534145714134" Y="21.211015802252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.297770639625" Y="29.624463857678" />
                  <Point X="2.35857936544" Y="22.926781158283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642530613814" Y="21.133983533771" />
                  <Point X="2.655347265748" Y="21.05306237821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.397779030533" Y="29.60031878415" />
                  <Point X="2.448589490417" Y="22.965762651418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.498687725234" Y="29.570489416179" />
                  <Point X="2.543501704653" Y="22.973793571069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.600734146329" Y="29.533476726467" />
                  <Point X="2.642359315421" Y="22.956914237385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.702780614932" Y="29.496463736798" />
                  <Point X="2.023250777698" Y="27.473094761226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.069253854537" Y="27.182642765151" />
                  <Point X="2.741375788907" Y="22.939031883977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.804827176955" Y="29.459450157299" />
                  <Point X="2.092184420594" Y="27.645147925024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.185623384866" Y="27.055197522817" />
                  <Point X="2.842398154101" Y="22.90848482876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.907746729654" Y="29.416924731598" />
                  <Point X="2.167662677782" Y="27.775880020417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.29118541889" Y="26.995988126648" />
                  <Point X="2.948203723552" Y="22.847737810422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01162531161" Y="29.368344233478" />
                  <Point X="2.243140934971" Y="27.906612115811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390218582059" Y="26.978000398731" />
                  <Point X="2.896990845622" Y="23.778366252068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980725757569" Y="23.24968482493" />
                  <Point X="3.05406853557" Y="22.786616749235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.115503899904" Y="29.319763695335" />
                  <Point X="2.318619192159" Y="28.037344211205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.486061318717" Y="26.980156231031" />
                  <Point X="2.97549652167" Y="23.889983977051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.095745099866" Y="23.130764334329" />
                  <Point X="3.159933347588" Y="22.725495688048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.219382495592" Y="29.271183110507" />
                  <Point X="2.394097449348" Y="28.168076306598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578469561029" Y="27.003996607203" />
                  <Point X="3.056855691739" Y="23.983585449837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.205236053594" Y="23.046748715428" />
                  <Point X="3.265798159606" Y="22.664374626861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.32472164394" Y="29.213380959117" />
                  <Point X="2.469575706536" Y="28.298808401992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667953571577" Y="27.046299856112" />
                  <Point X="3.144438172079" Y="24.03789448797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.314727007321" Y="22.962733096528" />
                  <Point X="3.371662971624" Y="22.603253565675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.430683485947" Y="29.15164727469" />
                  <Point X="2.545053958581" Y="28.429540529861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.756079274529" Y="27.097179121656" />
                  <Point X="3.236843440638" Y="24.061753639684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424217961049" Y="22.878717477628" />
                  <Point X="3.477527783642" Y="22.542132504488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.536645231221" Y="29.089914201011" />
                  <Point X="2.620532197596" Y="28.560272739996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.844204977482" Y="27.1480583872" />
                  <Point X="3.329826698302" Y="24.081963511814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533708914777" Y="22.794701858727" />
                  <Point X="3.583392588631" Y="22.481011487679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642909201084" Y="29.026272956374" />
                  <Point X="2.696010436611" Y="28.691004950131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932330680434" Y="27.198937652744" />
                  <Point X="3.034395813525" Y="26.554523764102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.104645715291" Y="26.110983340419" />
                  <Point X="3.424494740373" Y="24.091536073838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.643199854923" Y="22.710686325579" />
                  <Point X="3.689257364466" Y="22.419890654943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.751302290498" Y="28.949188979947" />
                  <Point X="2.771488675626" Y="28.821737160267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020456383387" Y="27.249816918289" />
                  <Point X="3.11121735658" Y="26.676774686316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.215627478022" Y="26.017555123914" />
                  <Point X="3.364311166899" Y="25.07880325806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421331267592" Y="24.718792510946" />
                  <Point X="3.522593361256" Y="24.079448813692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752690750952" Y="22.626671070976" />
                  <Point X="3.795122140301" Y="22.358769822206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.108582086339" Y="27.300696183833" />
                  <Point X="3.194200165121" Y="26.760124909241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.319159242783" Y="25.971164343383" />
                  <Point X="3.435488933822" Y="25.23668758038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5368451163" Y="24.596749829742" />
                  <Point X="3.620825863153" Y="24.066516262091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862181646981" Y="22.542655816373" />
                  <Point X="3.900986916136" Y="22.29764898947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.196707789292" Y="27.351575449377" />
                  <Point X="3.279961518066" Y="26.825932093228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.417831309999" Y="25.955456485585" />
                  <Point X="3.517453822425" Y="25.326464696855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642701071531" Y="24.535684688105" />
                  <Point X="3.71905836505" Y="24.053583710491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.97167254301" Y="22.45864056177" />
                  <Point X="4.005410901247" Y="22.24562495135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284833492244" Y="27.402454714922" />
                  <Point X="3.365722871011" Y="26.891739277216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.514154183682" Y="25.954580852011" />
                  <Point X="3.605065909451" Y="25.380586805739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.744205596738" Y="24.502093394378" />
                  <Point X="3.817290866948" Y="24.04065115889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.081163439039" Y="22.374625307167" />
                  <Point X="4.082970158399" Y="22.36321813007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372959189065" Y="27.453334019182" />
                  <Point X="3.451484223955" Y="26.957546461204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.608373734661" Y="25.966985075351" />
                  <Point X="3.694679237204" Y="25.42207357794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.844652649485" Y="24.475178718992" />
                  <Point X="3.915523368845" Y="24.027718607289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.461084877976" Y="27.504213373382" />
                  <Point X="3.5372455769" Y="27.023353645191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.702593285639" Y="25.97938929869" />
                  <Point X="3.786947625319" Y="25.446796958765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945099702232" Y="24.448264043606" />
                  <Point X="4.013755870742" Y="24.014786055688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.549210566887" Y="27.555092727582" />
                  <Point X="3.623006929845" Y="27.089160829179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.796812836618" Y="25.991793522029" />
                  <Point X="3.879216013434" Y="25.47152033959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04554675498" Y="24.42134936822" />
                  <Point X="4.111988372639" Y="24.001853504087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637336255797" Y="27.605972081782" />
                  <Point X="3.708768282789" Y="27.154968013166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.891032387596" Y="26.004197745369" />
                  <Point X="3.971484401549" Y="25.496243720415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.145993807727" Y="24.394434692834" />
                  <Point X="4.210220874536" Y="23.988920952487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725461944708" Y="27.656851435982" />
                  <Point X="3.794529649742" Y="27.220775108707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.985251938575" Y="26.016601968708" />
                  <Point X="4.063752789664" Y="25.520967101241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.246440860475" Y="24.367520017448" />
                  <Point X="4.308453349639" Y="23.975988570056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.813587633619" Y="27.707730790182" />
                  <Point X="3.880291069157" Y="27.286581873019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079471489554" Y="26.029006192047" />
                  <Point X="4.156021177779" Y="25.545690482066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.346887891363" Y="24.340605480073" />
                  <Point X="4.406685813707" Y="23.963056257298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.90171332253" Y="27.758610144382" />
                  <Point X="3.966052488572" Y="27.352388637331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.173691038173" Y="26.041410430284" />
                  <Point X="4.248289565894" Y="25.570413862891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.447334896853" Y="24.313691103058" />
                  <Point X="4.504918277776" Y="23.95012394454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009587486847" Y="27.684802532072" />
                  <Point X="4.051813907987" Y="27.418195401643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.26791056603" Y="26.053814799604" />
                  <Point X="4.340557954009" Y="25.595137243716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.547781902343" Y="24.286776726043" />
                  <Point X="4.603150741844" Y="23.937191631782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362130093888" Y="26.066219168923" />
                  <Point X="4.432826330146" Y="25.619860700166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.648228907833" Y="24.259862349028" />
                  <Point X="4.701383205912" Y="23.924259319024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.456349621746" Y="26.078623538243" />
                  <Point X="4.525094695801" Y="25.644584222796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748675913324" Y="24.232947972013" />
                  <Point X="4.767971725971" Y="24.111119005681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.550569149603" Y="26.091027907562" />
                  <Point X="4.617363061457" Y="25.669307745427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.644788677461" Y="26.103432276882" />
                  <Point X="4.709631427112" Y="25.694031268057" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.68464666748" Y="20.66755859375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.377216278076" Y="21.603955078125" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.7336015625" />
                  <Point X="0.139549758911" Y="21.77543359375" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.143450897217" Y="21.77040234375" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.375381561279" Y="21.588859375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.646700744629" Y="20.7632265625" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.961441833496" Y="20.0349921875" />
                  <Point X="-1.100257202148" Y="20.0619375" />
                  <Point X="-1.256096313477" Y="20.10203125" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.334803344727" Y="20.25410546875" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.341883666992" Y="20.627123046875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.510376586914" Y="20.90619921875" />
                  <Point X="-1.619543701172" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.813941040039" Y="21.02503125" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878295898" Y="21.026208984375" />
                  <Point X="-2.127115478516" Y="20.934509765625" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.364986083984" Y="20.705537109375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.652376953125" Y="20.7064140625" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.071641601562" Y="20.998552734375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.9020703125" Y="21.684923828125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979736328" Y="22.431236328125" />
                  <Point X="-2.531328369141" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.195127441406" Y="22.108130859375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.000961425781" Y="21.9416875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.316420898438" Y="22.412306640625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.856722167969" Y="23.045146484375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140325927734" Y="23.66540234375" />
                  <Point X="-3.161156738281" Y="23.689359375" />
                  <Point X="-3.187645751953" Y="23.70494921875" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.981320068359" Y="23.61113671875" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.864525878906" Y="23.742685546875" />
                  <Point X="-4.927392578125" Y="23.9888046875" />
                  <Point X="-4.968327148438" Y="24.275017578125" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.346821777344" Y="24.659845703125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.527133789062" Y="24.8888203125" />
                  <Point X="-3.502325195312" Y="24.909353515625" />
                  <Point X="-3.489977294922" Y="24.93994921875" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.490569091797" Y="24.999396484375" />
                  <Point X="-3.502324951172" Y="25.0280859375" />
                  <Point X="-3.528909912109" Y="25.0498515625" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.251872558594" Y="25.25215234375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.958162109375" Y="25.722609375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.835245605469" Y="26.30049609375" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.319097167969" Y="26.46847265625" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704101562" Y="26.3958671875" />
                  <Point X="-3.699025634766" Y="26.406169921875" />
                  <Point X="-3.670278076172" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.6260078125" Y="26.47544140625" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.632137695312" Y="26.575904296875" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.058282714844" Y="26.924859375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.317448730469" Y="27.517283203125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.941740966797" Y="28.067564453125" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.506674560547" Y="28.12758203125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138513671875" Y="27.92043359375" />
                  <Point X="-3.093001708984" Y="27.916451171875" />
                  <Point X="-3.052964355469" Y="27.91294921875" />
                  <Point X="-3.031506591797" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.980948242188" Y="27.959708984375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942056152344" Y="28.0733515625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.128572509766" Y="28.439748046875" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.027075927734" Y="28.96410546875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.409102294922" Y="29.36532421875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.074212646484" Y="29.42621875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246582031" Y="29.273662109375" />
                  <Point X="-1.900592041016" Y="29.247291015625" />
                  <Point X="-1.856030517578" Y="29.22409375" />
                  <Point X="-1.835124389648" Y="29.218490234375" />
                  <Point X="-1.813809448242" Y="29.22225" />
                  <Point X="-1.761049194336" Y="29.24410546875" />
                  <Point X="-1.714635498047" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.668911010742" Y="29.348953125" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.67198425293" Y="29.57084765625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.323062866211" Y="29.8037734375" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.551336303711" Y="29.952072265625" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.144945755005" Y="29.694578125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.02428212738" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594047546" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.144891479492" Y="29.64842578125" />
                  <Point X="0.236648422241" Y="29.9908671875" />
                  <Point X="0.550265136719" Y="29.9580234375" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="1.205000976562" Y="29.8423203125" />
                  <Point X="1.508455566406" Y="29.769056640625" />
                  <Point X="1.733163330078" Y="29.6875546875" />
                  <Point X="1.931044921875" Y="29.61578125" />
                  <Point X="2.148045410156" Y="29.514296875" />
                  <Point X="2.338686279297" Y="29.425140625" />
                  <Point X="2.548342529297" Y="29.302994140625" />
                  <Point X="2.732519287109" Y="29.195693359375" />
                  <Point X="2.930243896484" Y="29.05508203125" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.688236083984" Y="28.2975390625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.213430175781" Y="27.44880078125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.206498291016" Y="27.355390625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.241535400391" Y="27.2671328125" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.308618652344" Y="27.2013515625" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360336914062" Y="27.172978515625" />
                  <Point X="2.397270751953" Y="27.168525390625" />
                  <Point X="2.42976171875" Y="27.164607421875" />
                  <Point X="2.448665527344" Y="27.1659453125" />
                  <Point X="2.491377685547" Y="27.1773671875" />
                  <Point X="2.528951904297" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.239475097656" Y="27.595662109375" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.093341552734" Y="27.893712890625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.312817871094" Y="27.55973046875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.892119628906" Y="27.05616796875" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.248631103516" Y="26.54373046875" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.201668945312" Y="26.4505546875" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.200179199219" Y="26.345408203125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.241212890625" Y="26.249095703125" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947021484" Y="26.1988203125" />
                  <Point X="3.317996337891" Y="26.17796484375" />
                  <Point X="3.350589111328" Y="26.1596171875" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.418658447266" Y="26.147" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.139275878906" Y="26.22851953125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.892266113281" Y="26.144126953125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.973923828125" Y="25.72828515625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.432795898438" Y="25.4231484375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088378906" Y="25.232818359375" />
                  <Point X="3.679873535156" Y="25.20437109375" />
                  <Point X="3.636578369141" Y="25.17934765625" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.592735839844" Y="25.129298828125" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.547142089844" Y="25.023337890625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.548326171875" Y="24.90791796875" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.596287597656" Y="24.803615234375" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.685791992188" Y="24.72964453125" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.349602050781" Y="24.53658203125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.974630859375" Y="24.207373046875" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.903930175781" Y="23.838587890625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.214076171875" Y="23.7967734375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.298244628906" Y="23.8806640625" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.185445068359" Y="23.845302734375" />
                  <Point X="3.127061523438" Y="23.7750859375" />
                  <Point X="3.075700927734" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.055990234375" Y="23.594994140625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.109815673828" Y="23.400232421875" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.733091552734" Y="22.881201171875" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.277985351562" Y="22.31736328125" />
                  <Point X="4.204130859375" Y="22.19785546875" />
                  <Point X="4.112099609375" Y="22.067091796875" />
                  <Point X="4.056688476562" Y="21.988361328125" />
                  <Point X="3.467827636719" Y="22.32833984375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.622381347656" Y="22.767447265625" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.393574707031" Y="22.7304921875" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.238337402344" Y="22.569814453125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.209924560547" Y="22.338666015625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.596923583984" Y="21.5929765625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.922932373047" Y="20.8723828125" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.732400390625" Y="20.74318359375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.228310058594" Y="21.29748046875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.557168457031" Y="22.09242578125" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425805297852" Y="22.16428125" />
                  <Point X="1.301804077148" Y="22.15287109375" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.1314921875" />
                  <Point X="1.06958215332" Y="22.051876953125" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.939827819824" Y="21.822296875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.016753356934" Y="20.908255859375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.077253173828" Y="20.05492578125" />
                  <Point X="0.99436505127" Y="20.0367578125" />
                  <Point X="0.899288208008" Y="20.019484375" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#170" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.102805347483" Y="4.73879927325" Z="1.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="-0.563304405554" Y="5.033012932527" Z="1.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.35" />
                  <Point X="-1.342716655857" Y="4.883199221201" Z="1.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.35" />
                  <Point X="-1.727712534092" Y="4.595601985722" Z="1.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.35" />
                  <Point X="-1.72275271033" Y="4.395267975064" Z="1.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.35" />
                  <Point X="-1.786338922581" Y="4.321578722722" Z="1.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.35" />
                  <Point X="-1.883660584239" Y="4.322922010621" Z="1.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.35" />
                  <Point X="-2.040700872544" Y="4.487935890493" Z="1.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.35" />
                  <Point X="-2.439541199834" Y="4.440312298846" Z="1.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.35" />
                  <Point X="-3.063654017963" Y="4.035065168983" Z="1.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.35" />
                  <Point X="-3.17802982978" Y="3.446028767819" Z="1.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.35" />
                  <Point X="-2.998021829762" Y="3.100275476023" Z="1.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.35" />
                  <Point X="-3.022458869146" Y="3.02634469874" Z="1.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.35" />
                  <Point X="-3.094800989583" Y="2.997542869411" Z="1.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.35" />
                  <Point X="-3.487830580478" Y="3.202164118966" Z="1.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.35" />
                  <Point X="-3.987360446131" Y="3.129548652619" Z="1.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.35" />
                  <Point X="-4.366786822006" Y="2.573768859208" Z="1.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.35" />
                  <Point X="-4.094876625441" Y="1.916471426538" Z="1.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.35" />
                  <Point X="-3.682643920398" Y="1.584097178585" Z="1.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.35" />
                  <Point X="-3.678357548279" Y="1.525856148576" Z="1.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.35" />
                  <Point X="-3.720217593258" Y="1.485136008938" Z="1.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.35" />
                  <Point X="-4.318726998419" Y="1.549325612174" Z="1.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.35" />
                  <Point X="-4.889661232072" Y="1.344855634826" Z="1.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.35" />
                  <Point X="-5.013779375529" Y="0.761207788538" Z="1.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.35" />
                  <Point X="-4.270969201986" Y="0.235135222693" Z="1.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.35" />
                  <Point X="-3.563572280561" Y="0.040054302478" Z="1.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.35" />
                  <Point X="-3.544478299986" Y="0.015857180852" Z="1.35" />
                  <Point X="-3.539556741714" Y="0" Z="1.35" />
                  <Point X="-3.543886301149" Y="-0.013949770209" Z="1.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.35" />
                  <Point X="-3.561796314562" Y="-0.038821680656" Z="1.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.35" />
                  <Point X="-4.365918586786" Y="-0.260576834063" Z="1.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.35" />
                  <Point X="-5.023979373959" Y="-0.700782163225" Z="1.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.35" />
                  <Point X="-4.919126259509" Y="-1.238409788058" Z="1.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.35" />
                  <Point X="-3.980949679484" Y="-1.407154949071" Z="1.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.35" />
                  <Point X="-3.206764935393" Y="-1.31415784904" Z="1.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.35" />
                  <Point X="-3.19628204035" Y="-1.336371687942" Z="1.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.35" />
                  <Point X="-3.893316217205" Y="-1.883905192708" Z="1.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.35" />
                  <Point X="-4.365520048739" Y="-2.582021936355" Z="1.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.35" />
                  <Point X="-4.046903228686" Y="-3.057315111949" Z="1.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.35" />
                  <Point X="-3.176283476995" Y="-2.903889509186" Z="1.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.35" />
                  <Point X="-2.564720319296" Y="-2.563610149835" Z="1.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.35" />
                  <Point X="-2.951527503982" Y="-3.258794905497" Z="1.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.35" />
                  <Point X="-3.108301585304" Y="-4.009783333633" Z="1.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.35" />
                  <Point X="-2.684854354761" Y="-4.304817669649" Z="1.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.35" />
                  <Point X="-2.331474243682" Y="-4.293619164938" Z="1.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.35" />
                  <Point X="-2.105493044099" Y="-4.075783366075" Z="1.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.35" />
                  <Point X="-1.823367059259" Y="-3.993580900763" Z="1.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.35" />
                  <Point X="-1.549499845498" Y="-4.100112957746" Z="1.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.35" />
                  <Point X="-1.397078897604" Y="-4.351350228926" Z="1.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.35" />
                  <Point X="-1.390531659925" Y="-4.70808697415" Z="1.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.35" />
                  <Point X="-1.274711593401" Y="-4.915109139864" Z="1.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.35" />
                  <Point X="-0.97712526191" Y="-4.982811750118" Z="1.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="-0.604560402486" Y="-4.218434212858" Z="1.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="-0.340461572849" Y="-3.408370763891" Z="1.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="-0.134786034766" Y="-3.246071979561" Z="1.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.35" />
                  <Point X="0.118573044595" Y="-3.241040065727" Z="1.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.35" />
                  <Point X="0.329984286584" Y="-3.393274991876" Z="1.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.35" />
                  <Point X="0.630194457915" Y="-4.314101816958" Z="1.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.35" />
                  <Point X="0.9020688815" Y="-4.998430405642" Z="1.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.35" />
                  <Point X="1.081802899966" Y="-4.962634056388" Z="1.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.35" />
                  <Point X="1.060169612329" Y="-4.053938120299" Z="1.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.35" />
                  <Point X="0.982531055887" Y="-3.157041686981" Z="1.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.35" />
                  <Point X="1.09539311124" Y="-2.955288957078" Z="1.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.35" />
                  <Point X="1.300229244985" Y="-2.865637293466" Z="1.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.35" />
                  <Point X="1.523973063987" Y="-2.918351870061" Z="1.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.35" />
                  <Point X="2.182486426722" Y="-3.701675668991" Z="1.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.35" />
                  <Point X="2.753413630783" Y="-4.267510547051" Z="1.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.35" />
                  <Point X="2.945838185923" Y="-4.137024367446" Z="1.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.35" />
                  <Point X="2.634068973584" Y="-3.350741951222" Z="1.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.35" />
                  <Point X="2.252972676485" Y="-2.621167446835" Z="1.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.35" />
                  <Point X="2.276427695506" Y="-2.422193059356" Z="1.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.35" />
                  <Point X="2.410705319016" Y="-2.282473614461" Z="1.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.35" />
                  <Point X="2.607339356677" Y="-2.250475333006" Z="1.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.35" />
                  <Point X="3.436671645885" Y="-2.683680526691" Z="1.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.35" />
                  <Point X="4.146831866609" Y="-2.930404156577" Z="1.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.35" />
                  <Point X="4.314362464013" Y="-2.677640604754" Z="1.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.35" />
                  <Point X="3.757373522278" Y="-2.047849459645" Z="1.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.35" />
                  <Point X="3.145717640908" Y="-1.541448446371" Z="1.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.35" />
                  <Point X="3.099623717119" Y="-1.378306461608" Z="1.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.35" />
                  <Point X="3.15935211385" Y="-1.225601306176" Z="1.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.35" />
                  <Point X="3.30270834925" Y="-1.136914988913" Z="1.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.35" />
                  <Point X="4.201393996645" Y="-1.22151811288" Z="1.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.35" />
                  <Point X="4.946521083687" Y="-1.14125654214" Z="1.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.35" />
                  <Point X="5.017916520204" Y="-0.768798931806" Z="1.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.35" />
                  <Point X="4.356387019256" Y="-0.383840113975" Z="1.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.35" />
                  <Point X="3.704657663221" Y="-0.195785329771" Z="1.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.35" />
                  <Point X="3.629465692827" Y="-0.134237377034" Z="1.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.35" />
                  <Point X="3.591277716421" Y="-0.051396423037" Z="1.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.35" />
                  <Point X="3.590093734003" Y="0.045214108171" Z="1.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.35" />
                  <Point X="3.625913745573" Y="0.129711361541" Z="1.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.35" />
                  <Point X="3.698737751131" Y="0.192363514471" Z="1.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.35" />
                  <Point X="4.43958079805" Y="0.406131818794" Z="1.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.35" />
                  <Point X="5.017172613612" Y="0.767257664548" Z="1.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.35" />
                  <Point X="4.93469125418" Y="1.18723438052" Z="1.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.35" />
                  <Point X="4.126593760117" Y="1.309371901446" Z="1.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.35" />
                  <Point X="3.419053792065" Y="1.227848156233" Z="1.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.35" />
                  <Point X="3.336367858012" Y="1.252815480718" Z="1.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.35" />
                  <Point X="3.276827426919" Y="1.307856552542" Z="1.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.35" />
                  <Point X="3.242991852003" Y="1.386792891309" Z="1.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.35" />
                  <Point X="3.243665327717" Y="1.468368892274" Z="1.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.35" />
                  <Point X="3.282158269935" Y="1.544592493117" Z="1.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.35" />
                  <Point X="3.916401800489" Y="2.047779716284" Z="1.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.35" />
                  <Point X="4.349439197939" Y="2.616896967672" Z="1.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.35" />
                  <Point X="4.127771171594" Y="2.954196143936" Z="1.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.35" />
                  <Point X="3.208319140857" Y="2.670243971639" Z="1.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.35" />
                  <Point X="2.472303632956" Y="2.256950913952" Z="1.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.35" />
                  <Point X="2.397100544908" Y="2.24944700097" Z="1.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.35" />
                  <Point X="2.330538060356" Y="2.27400482748" Z="1.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.35" />
                  <Point X="2.276753772755" Y="2.326486800026" Z="1.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.35" />
                  <Point X="2.249982701789" Y="2.392657898924" Z="1.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.35" />
                  <Point X="2.255577028706" Y="2.467165942926" Z="1.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.35" />
                  <Point X="2.725381381221" Y="3.303819941098" Z="1.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.35" />
                  <Point X="2.953064857973" Y="4.127110962929" Z="1.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.35" />
                  <Point X="2.567356240633" Y="4.377478527703" Z="1.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.35" />
                  <Point X="2.163070769038" Y="4.590868983551" Z="1.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.35" />
                  <Point X="1.744056127644" Y="4.765838896284" Z="1.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.35" />
                  <Point X="1.210579276667" Y="4.922205065431" Z="1.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.35" />
                  <Point X="0.549317033713" Y="5.039032734664" Z="1.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.35" />
                  <Point X="0.09043921407" Y="4.692648232701" Z="1.35" />
                  <Point X="0" Y="4.355124473572" Z="1.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>