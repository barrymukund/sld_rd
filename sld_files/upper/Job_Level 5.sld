<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#125" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="538" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004716796875" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.595044128418" Y="21.36901171875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542361328125" Y="21.532626953125" />
                  <Point X="0.529387512207" Y="21.551318359375" />
                  <Point X="0.378633666992" Y="21.76852734375" />
                  <Point X="0.35675201416" Y="21.79098046875" />
                  <Point X="0.330496673584" Y="21.810224609375" />
                  <Point X="0.302493469238" Y="21.82433203125" />
                  <Point X="0.282417205811" Y="21.8305625" />
                  <Point X="0.049134460449" Y="21.90296484375" />
                  <Point X="0.020976732254" Y="21.907234375" />
                  <Point X="-0.008664748192" Y="21.907234375" />
                  <Point X="-0.036822174072" Y="21.90296484375" />
                  <Point X="-0.056898582458" Y="21.896734375" />
                  <Point X="-0.290181335449" Y="21.82433203125" />
                  <Point X="-0.318184997559" Y="21.81022265625" />
                  <Point X="-0.344440338135" Y="21.790978515625" />
                  <Point X="-0.366324554443" Y="21.7685234375" />
                  <Point X="-0.379298248291" Y="21.749830078125" />
                  <Point X="-0.530052246094" Y="21.532623046875" />
                  <Point X="-0.53818927002" Y="21.518427734375" />
                  <Point X="-0.55099005127" Y="21.4874765625" />
                  <Point X="-0.916584716797" Y="20.12305859375" />
                  <Point X="-1.079341796875" Y="20.154650390625" />
                  <Point X="-1.098549560547" Y="20.159591796875" />
                  <Point X="-1.24641784668" Y="20.19763671875" />
                  <Point X="-1.218784057617" Y="20.407537109375" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201293945" Y="20.45206640625" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.221304931641" Y="20.50788671875" />
                  <Point X="-1.277035888672" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323646362305" Y="20.868796875" />
                  <Point X="-1.342130004883" Y="20.885005859375" />
                  <Point X="-1.556907226563" Y="21.073361328125" />
                  <Point X="-1.583188964844" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643029296875" Y="21.109033203125" />
                  <Point X="-1.667560913086" Y="21.110640625" />
                  <Point X="-1.952618408203" Y="21.12932421875" />
                  <Point X="-1.983414672852" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657226562" Y="21.10519921875" />
                  <Point X="-2.063098388672" Y="21.091541015625" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312789550781" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.480147949219" Y="20.71151171875" />
                  <Point X="-2.801715820312" Y="20.9106171875" />
                  <Point X="-2.828301269531" Y="20.931087890625" />
                  <Point X="-3.104721435547" Y="21.143921875" />
                  <Point X="-2.482750244141" Y="22.22120703125" />
                  <Point X="-2.423760986328" Y="22.323380859375" />
                  <Point X="-2.412855957031" Y="22.352361328125" />
                  <Point X="-2.406587158203" Y="22.38390234375" />
                  <Point X="-2.405697265625" Y="22.415701171875" />
                  <Point X="-2.415354980469" Y="22.44601171875" />
                  <Point X="-2.430943847656" Y="22.47619921875" />
                  <Point X="-2.448176757812" Y="22.499783203125" />
                  <Point X="-2.464150634766" Y="22.5157578125" />
                  <Point X="-2.489310791016" Y="22.533787109375" />
                  <Point X="-2.518140380859" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578691650391" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-4.082861816406" Y="22.206142578125" />
                  <Point X="-4.101924316406" Y="22.23810546875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.209166503906" Y="23.422287109375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083404296875" Y="23.526220703125" />
                  <Point X="-3.064828613281" Y="23.5557109375" />
                  <Point X="-3.053245605469" Y="23.582525390625" />
                  <Point X="-3.046151611328" Y="23.609916015625" />
                  <Point X="-3.043347900391" Y="23.6403515625" />
                  <Point X="-3.045559814453" Y="23.67202734375" />
                  <Point X="-3.052984375" Y="23.702771484375" />
                  <Point X="-3.070089599609" Y="23.729375" />
                  <Point X="-3.093030273438" Y="23.75456640625" />
                  <Point X="-3.115083984375" Y="23.772474609375" />
                  <Point X="-3.139455566406" Y="23.786818359375" />
                  <Point X="-3.168716064453" Y="23.798041015625" />
                  <Point X="-3.200603759766" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.730690917969" Y="23.60830078125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.83912109375" Y="24.042607421875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.650449951172" Y="24.7480859375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.515483886719" Y="24.7861328125" />
                  <Point X="-3.485522216797" Y="24.802064453125" />
                  <Point X="-3.459981445312" Y="24.8197890625" />
                  <Point X="-3.436248291016" Y="24.843455078125" />
                  <Point X="-3.416270263672" Y="24.872072265625" />
                  <Point X="-3.403436279297" Y="24.898291015625" />
                  <Point X="-3.394918212891" Y="24.925734375" />
                  <Point X="-3.390673095703" Y="24.9560703125" />
                  <Point X="-3.391405761719" Y="24.988076171875" />
                  <Point X="-3.395650390625" Y="25.0140625" />
                  <Point X="-3.404168457031" Y="25.0415078125" />
                  <Point X="-3.419694091797" Y="25.071392578125" />
                  <Point X="-3.441136474609" Y="25.099173828125" />
                  <Point X="-3.462172851562" Y="25.119171875" />
                  <Point X="-3.4877265625" Y="25.136908203125" />
                  <Point X="-3.501924560547" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.824487792969" Y="25.976974609375" />
                  <Point X="-4.814333984375" Y="26.014443359375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.849946289063" Y="26.310888671875" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744997314453" Y="26.299341796875" />
                  <Point X="-3.723448242188" Y="26.301224609375" />
                  <Point X="-3.7031796875" Y="26.30525" />
                  <Point X="-3.698268066406" Y="26.306796875" />
                  <Point X="-3.641709228516" Y="26.324630859375" />
                  <Point X="-3.622775878906" Y="26.332962890625" />
                  <Point X="-3.604032226562" Y="26.34378515625" />
                  <Point X="-3.587353271484" Y="26.356015625" />
                  <Point X="-3.573716064453" Y="26.371564453125" />
                  <Point X="-3.561301757812" Y="26.38929296875" />
                  <Point X="-3.551352539062" Y="26.407427734375" />
                  <Point X="-3.549399414062" Y="26.412142578125" />
                  <Point X="-3.526705078125" Y="26.466931640625" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532047607422" Y="26.589373046875" />
                  <Point X="-3.534404052734" Y="26.593900390625" />
                  <Point X="-3.561787353516" Y="26.646501953125" />
                  <Point X="-3.573282226562" Y="26.66370703125" />
                  <Point X="-3.587194824219" Y="26.680287109375" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.351859375" Y="27.269875" />
                  <Point X="-4.081155761719" Y="27.733654296875" />
                  <Point X="-4.054259277344" Y="27.7682265625" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.258419189453" Y="27.874556640625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187727539062" Y="27.836341796875" />
                  <Point X="-3.167090087891" Y="27.829833984375" />
                  <Point X="-3.146829101562" Y="27.825798828125" />
                  <Point X="-3.140013916016" Y="27.825201171875" />
                  <Point X="-3.061243652344" Y="27.818310546875" />
                  <Point X="-3.040566894531" Y="27.81876171875" />
                  <Point X="-3.019111328125" Y="27.8215859375" />
                  <Point X="-2.999020263672" Y="27.826501953125" />
                  <Point X="-2.980469238281" Y="27.8356484375" />
                  <Point X="-2.962216796875" Y="27.847275390625" />
                  <Point X="-2.946095214844" Y="27.8602109375" />
                  <Point X="-2.941265136719" Y="27.8650390625" />
                  <Point X="-2.885353271484" Y="27.920951171875" />
                  <Point X="-2.872406738281" Y="27.937083984375" />
                  <Point X="-2.860777587891" Y="27.955337890625" />
                  <Point X="-2.851628662109" Y="27.973888671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435546875" Y="28.036119140625" />
                  <Point X="-2.844028564453" Y="28.0428984375" />
                  <Point X="-2.850920166016" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-2.700621337891" Y="29.094685546875" />
                  <Point X="-2.658270996094" Y="29.11821484375" />
                  <Point X="-2.167036621094" Y="29.3911328125" />
                  <Point X="-2.059041015625" Y="29.250390625" />
                  <Point X="-2.043194824219" Y="29.229740234375" />
                  <Point X="-2.028887451172" Y="29.214794921875" />
                  <Point X="-2.012308227539" Y="29.200884765625" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.987569946289" Y="29.18546875" />
                  <Point X="-1.899898803711" Y="29.139828125" />
                  <Point X="-1.880625366211" Y="29.13233203125" />
                  <Point X="-1.859718505859" Y="29.126728515625" />
                  <Point X="-1.839268798828" Y="29.123580078125" />
                  <Point X="-1.818622436523" Y="29.12493359375" />
                  <Point X="-1.797306884766" Y="29.128693359375" />
                  <Point X="-1.777447143555" Y="29.134484375" />
                  <Point X="-1.769588500977" Y="29.137740234375" />
                  <Point X="-1.67827331543" Y="29.175564453125" />
                  <Point X="-1.660146118164" Y="29.185509765625" />
                  <Point X="-1.642416625977" Y="29.197923828125" />
                  <Point X="-1.626864257812" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595479614258" Y="29.265923828125" />
                  <Point X="-1.592921875" Y="29.274037109375" />
                  <Point X="-1.563200195312" Y="29.36830078125" />
                  <Point X="-1.559165649414" Y="29.3885859375" />
                  <Point X="-1.557279052734" Y="29.4101484375" />
                  <Point X="-1.557730224609" Y="29.430828125" />
                  <Point X="-1.584201538086" Y="29.631896484375" />
                  <Point X="-0.949622253418" Y="29.80980859375" />
                  <Point X="-0.898293212891" Y="29.815818359375" />
                  <Point X="-0.294711181641" Y="29.886458984375" />
                  <Point X="-0.149215988159" Y="29.343462890625" />
                  <Point X="-0.133903381348" Y="29.286314453125" />
                  <Point X="-0.12112991333" Y="29.258123046875" />
                  <Point X="-0.103271614075" Y="29.231396484375" />
                  <Point X="-0.082114059448" Y="29.208806640625" />
                  <Point X="-0.054818141937" Y="29.194216796875" />
                  <Point X="-0.024380065918" Y="29.183884765625" />
                  <Point X="0.006155892849" Y="29.17884375" />
                  <Point X="0.036691745758" Y="29.183884765625" />
                  <Point X="0.067129974365" Y="29.194216796875" />
                  <Point X="0.094426002502" Y="29.208806640625" />
                  <Point X="0.115583602905" Y="29.231396484375" />
                  <Point X="0.133441741943" Y="29.258123046875" />
                  <Point X="0.146215209961" Y="29.286314453125" />
                  <Point X="0.307419525146" Y="29.8879375" />
                  <Point X="0.844041442871" Y="29.83173828125" />
                  <Point X="0.886520446777" Y="29.821482421875" />
                  <Point X="1.481023193359" Y="29.677951171875" />
                  <Point X="1.507315185547" Y="29.668416015625" />
                  <Point X="1.894648803711" Y="29.52792578125" />
                  <Point X="1.921385864258" Y="29.515421875" />
                  <Point X="2.294573486328" Y="29.34089453125" />
                  <Point X="2.320444824219" Y="29.325822265625" />
                  <Point X="2.680977783203" Y="29.115775390625" />
                  <Point X="2.705344970703" Y="29.098447265625" />
                  <Point X="2.943260498047" Y="28.92925390625" />
                  <Point X="2.215856445313" Y="27.669353515625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.140566162109" Y="27.536044921875" />
                  <Point X="2.131375244141" Y="27.509693359375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108418457031" Y="27.4138515625" />
                  <Point X="2.107744140625" Y="27.38944921875" />
                  <Point X="2.108391113281" Y="27.375451171875" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140066894531" Y="27.247478515625" />
                  <Point X="2.143470947266" Y="27.2424609375" />
                  <Point X="2.183024658203" Y="27.184169921875" />
                  <Point X="2.197622070313" Y="27.167314453125" />
                  <Point X="2.215937744141" Y="27.150609375" />
                  <Point X="2.226613769531" Y="27.1421875" />
                  <Point X="2.284906005859" Y="27.1026328125" />
                  <Point X="2.304961669922" Y="27.092265625" />
                  <Point X="2.327061767578" Y="27.083998046875" />
                  <Point X="2.34899609375" Y="27.078658203125" />
                  <Point X="2.354475830078" Y="27.077998046875" />
                  <Point X="2.418387451172" Y="27.070291015625" />
                  <Point X="2.441105957031" Y="27.070287109375" />
                  <Point X="2.466372070312" Y="27.073326171875" />
                  <Point X="2.4795703125" Y="27.07587109375" />
                  <Point X="2.553494628906" Y="27.095640625" />
                  <Point X="2.5652890625" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.962660400391" Y="27.903498046875" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.123271972656" Y="27.6894609375" />
                  <Point X="4.13685546875" Y="27.667013671875" />
                  <Point X="4.262197753906" Y="27.4598828125" />
                  <Point X="3.320674804688" Y="26.737427734375" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.218286621094" Y="26.656947265625" />
                  <Point X="3.204463134766" Y="26.641724609375" />
                  <Point X="3.199394775391" Y="26.635654296875" />
                  <Point X="3.14619140625" Y="26.56624609375" />
                  <Point X="3.1346796875" Y="26.546814453125" />
                  <Point X="3.124504638672" Y="26.523763671875" />
                  <Point X="3.119924560547" Y="26.510986328125" />
                  <Point X="3.100106201172" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739746094" Y="26.371765625" />
                  <Point X="3.099139892578" Y="26.36498046875" />
                  <Point X="3.115408691406" Y="26.2861328125" />
                  <Point X="3.122641601562" Y="26.2645625" />
                  <Point X="3.133647705078" Y="26.2413984375" />
                  <Point X="3.140090576172" Y="26.229953125" />
                  <Point X="3.184340087891" Y="26.1626953125" />
                  <Point X="3.198884521484" Y="26.145458984375" />
                  <Point X="3.216121826172" Y="26.12937109375" />
                  <Point X="3.234348144531" Y="26.116033203125" />
                  <Point X="3.239873046875" Y="26.112923828125" />
                  <Point X="3.303996826172" Y="26.076828125" />
                  <Point X="3.325436767578" Y="26.0680078125" />
                  <Point X="3.350874267578" Y="26.0610234375" />
                  <Point X="3.363581787109" Y="26.058451171875" />
                  <Point X="3.45028125" Y="26.046994140625" />
                  <Point X="3.462698730469" Y="26.04617578125" />
                  <Point X="3.488203369141" Y="26.046984375" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.845936523438" Y="25.93280859375" />
                  <Point X="4.850216796875" Y="25.905314453125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.819596923828" Y="25.357193359375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.700321044922" Y="25.323595703125" />
                  <Point X="3.68091015625" Y="25.3143515625" />
                  <Point X="3.674216308594" Y="25.310830078125" />
                  <Point X="3.589036865234" Y="25.261595703125" />
                  <Point X="3.570666748047" Y="25.247763671875" />
                  <Point X="3.551956054688" Y="25.22973828125" />
                  <Point X="3.5431328125" Y="25.21997265625" />
                  <Point X="3.492025146484" Y="25.154849609375" />
                  <Point X="3.480300048828" Y="25.13556640625" />
                  <Point X="3.470526367188" Y="25.1141015625" />
                  <Point X="3.463680664062" Y="25.0926015625" />
                  <Point X="3.462214599609" Y="25.0849453125" />
                  <Point X="3.445178466797" Y="24.995990234375" />
                  <Point X="3.443628662109" Y="24.9728671875" />
                  <Point X="3.445094726562" Y="24.946404296875" />
                  <Point X="3.446644775391" Y="24.933791015625" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480300292969" Y="24.80187109375" />
                  <Point X="3.492019287109" Y="24.78259765625" />
                  <Point X="3.496417480469" Y="24.7769921875" />
                  <Point X="3.547525146484" Y="24.711869140625" />
                  <Point X="3.56432421875" Y="24.6952265625" />
                  <Point X="3.585966552734" Y="24.6785703125" />
                  <Point X="3.596365234375" Y="24.671607421875" />
                  <Point X="3.681544677734" Y="24.62237109375" />
                  <Point X="3.692708984375" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.051271484375" />
                  <Point X="4.849537597656" Y="24.027236328125" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="3.543853027344" Y="23.980830078125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.400498779297" Y="23.996673828125" />
                  <Point X="3.368965576172" Y="23.992833984375" />
                  <Point X="3.360271728516" Y="23.99136328125" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.162729980469" Y="23.944310546875" />
                  <Point X="3.130709960938" Y="23.923193359375" />
                  <Point X="3.109513671875" Y="23.901958984375" />
                  <Point X="3.103701171875" Y="23.895580078125" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.986627197266" Y="23.749583984375" />
                  <Point X="2.974333496094" Y="23.716615234375" />
                  <Point X="2.969491455078" Y="23.688517578125" />
                  <Point X="2.968511230469" Y="23.68108984375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.955109130859" Y="23.491515625" />
                  <Point X="2.965750488281" Y="23.45435546875" />
                  <Point X="2.980223876953" Y="23.42680859375" />
                  <Point X="2.984412353516" Y="23.419619140625" />
                  <Point X="3.076930419922" Y="23.275712890625" />
                  <Point X="3.086930664063" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="4.213122070312" Y="22.3931171875" />
                  <Point X="4.124811035156" Y="22.25021484375" />
                  <Point X="4.113466308594" Y="22.234095703125" />
                  <Point X="4.028980957031" Y="22.114052734375" />
                  <Point X="2.907359863281" Y="22.76162109375" />
                  <Point X="2.800954833984" Y="22.8230546875" />
                  <Point X="2.778269775391" Y="22.832484375" />
                  <Point X="2.745032714844" Y="22.841478515625" />
                  <Point X="2.7371015625" Y="22.843265625" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.505974853516" Y="22.880916015625" />
                  <Point X="2.467714355469" Y="22.873408203125" />
                  <Point X="2.437817871094" Y="22.860751953125" />
                  <Point X="2.430608398438" Y="22.8573359375" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.241146240234" Y="22.75387109375" />
                  <Point X="2.216854736328" Y="22.72761328125" />
                  <Point X="2.200477050781" Y="22.7013203125" />
                  <Point X="2.197045166016" Y="22.6953359375" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.098733398438" Y="22.499890625" />
                  <Point X="2.094406005859" Y="22.460833984375" />
                  <Point X="2.0977109375" Y="22.427208984375" />
                  <Point X="2.098767822266" Y="22.419619140625" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781835693359" Y="20.888345703125" />
                  <Point X="2.769165771484" Y="20.88014453125" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="1.839742919922" Y="21.95992578125" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721924072266" Y="22.099443359375" />
                  <Point X="1.705036132812" Y="22.11030078125" />
                  <Point X="1.508800537109" Y="22.2364609375" />
                  <Point X="1.479748535156" Y="22.250353515625" />
                  <Point X="1.440955444336" Y="22.258052734375" />
                  <Point X="1.406028442383" Y="22.257572265625" />
                  <Point X="1.398630493164" Y="22.257181640625" />
                  <Point X="1.184013549805" Y="22.23743359375" />
                  <Point X="1.155378051758" Y="22.23146484375" />
                  <Point X="1.122471313477" Y="22.216201171875" />
                  <Point X="1.095376586914" Y="22.19659375" />
                  <Point X="1.090333740234" Y="22.1926796875" />
                  <Point X="0.924611816406" Y="22.05488671875" />
                  <Point X="0.904141174316" Y="22.03113671875" />
                  <Point X="0.887249023438" Y="22.003310546875" />
                  <Point X="0.875624450684" Y="21.974189453125" />
                  <Point X="0.871360351562" Y="21.954572265625" />
                  <Point X="0.821810241699" Y="21.726603515625" />
                  <Point X="0.819724609375" Y="21.710373046875" />
                  <Point X="0.819742370605" Y="21.67687890625" />
                  <Point X="1.022039855957" Y="20.140275390625" />
                  <Point X="1.022065063477" Y="20.140083984375" />
                  <Point X="0.975707824707" Y="20.129921875" />
                  <Point X="0.96396105957" Y="20.1277890625" />
                  <Point X="0.929315551758" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058448242188" Y="20.247369140625" />
                  <Point X="-1.074880493164" Y="20.251595703125" />
                  <Point X="-1.141246337891" Y="20.268671875" />
                  <Point X="-1.124596801758" Y="20.39513671875" />
                  <Point X="-1.120775756836" Y="20.424158203125" />
                  <Point X="-1.120077392578" Y="20.4318984375" />
                  <Point X="-1.119451660156" Y="20.4589609375" />
                  <Point X="-1.121759033203" Y="20.49066796875" />
                  <Point X="-1.123333862305" Y="20.502306640625" />
                  <Point X="-1.128130249023" Y="20.526419921875" />
                  <Point X="-1.183861206055" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736938477" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250212036133" Y="20.92906640625" />
                  <Point X="-1.261010131836" Y="20.94022265625" />
                  <Point X="-1.279493774414" Y="20.956431640625" />
                  <Point X="-1.494270996094" Y="21.144787109375" />
                  <Point X="-1.506739257812" Y="21.15403515625" />
                  <Point X="-1.533020996094" Y="21.17037890625" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591317016602" Y="21.194525390625" />
                  <Point X="-1.621459960938" Y="21.201552734375" />
                  <Point X="-1.636817749023" Y="21.203830078125" />
                  <Point X="-1.661349365234" Y="21.2054375" />
                  <Point X="-1.946406860352" Y="21.22412109375" />
                  <Point X="-1.961930175781" Y="21.2238671875" />
                  <Point X="-1.99272644043" Y="21.220833984375" />
                  <Point X="-2.007999633789" Y="21.2180546875" />
                  <Point X="-2.039048095703" Y="21.209736328125" />
                  <Point X="-2.053667724609" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436035156" Y="21.184189453125" />
                  <Point X="-2.115877197266" Y="21.17053125" />
                  <Point X="-2.353402587891" Y="21.011822265625" />
                  <Point X="-2.359684570312" Y="21.0072421875" />
                  <Point X="-2.380448974609" Y="20.98986328125" />
                  <Point X="-2.402762451172" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.747595458984" Y="20.98884375" />
                  <Point X="-2.770342529297" Y="21.006359375" />
                  <Point X="-2.980862060547" Y="21.168453125" />
                  <Point X="-2.400477783203" Y="22.17370703125" />
                  <Point X="-2.341488525391" Y="22.275880859375" />
                  <Point X="-2.334847412109" Y="22.289923828125" />
                  <Point X="-2.323942382812" Y="22.318904296875" />
                  <Point X="-2.319678466797" Y="22.333841796875" />
                  <Point X="-2.313409667969" Y="22.3653828125" />
                  <Point X="-2.311624267578" Y="22.381244140625" />
                  <Point X="-2.310734375" Y="22.41304296875" />
                  <Point X="-2.315180908203" Y="22.444541015625" />
                  <Point X="-2.324838623047" Y="22.4748515625" />
                  <Point X="-2.3309453125" Y="22.4896015625" />
                  <Point X="-2.346534179688" Y="22.5197890625" />
                  <Point X="-2.354239257812" Y="22.532248046875" />
                  <Point X="-2.371472167969" Y="22.55583203125" />
                  <Point X="-2.381" Y="22.56695703125" />
                  <Point X="-2.396973876953" Y="22.582931640625" />
                  <Point X="-2.408815673828" Y="22.592978515625" />
                  <Point X="-2.433975830078" Y="22.6110078125" />
                  <Point X="-2.447294189453" Y="22.618990234375" />
                  <Point X="-2.476123779297" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181152344" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581802978516" Y="22.650923828125" />
                  <Point X="-2.597226318359" Y="22.6491484375" />
                  <Point X="-2.628753173828" Y="22.642876953125" />
                  <Point X="-2.643683837891" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.686683837891" Y="22.621072265625" />
                  <Point X="-3.793088134766" Y="21.9822890625" />
                  <Point X="-4.004018066406" Y="22.25941015625" />
                  <Point X="-4.020333007812" Y="22.286765625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.151334228516" Y="23.34691796875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.035749023438" Y="23.437482421875" />
                  <Point X="-3.013198730469" Y="23.46221875" />
                  <Point X="-3.003021728516" Y="23.475587890625" />
                  <Point X="-2.984446044922" Y="23.505078125" />
                  <Point X="-2.977617431641" Y="23.5180390625" />
                  <Point X="-2.966034423828" Y="23.544853515625" />
                  <Point X="-2.961280029297" Y="23.55870703125" />
                  <Point X="-2.954186035156" Y="23.58609765625" />
                  <Point X="-2.951552246094" Y="23.601201171875" />
                  <Point X="-2.948748535156" Y="23.63163671875" />
                  <Point X="-2.948578613281" Y="23.64696875" />
                  <Point X="-2.950790527344" Y="23.67864453125" />
                  <Point X="-2.953214355469" Y="23.694328125" />
                  <Point X="-2.960638916016" Y="23.725072265625" />
                  <Point X="-2.973076416016" Y="23.754150390625" />
                  <Point X="-2.990181640625" Y="23.78075390625" />
                  <Point X="-2.999850097656" Y="23.79333984375" />
                  <Point X="-3.022790771484" Y="23.81853125" />
                  <Point X="-3.033145019531" Y="23.828314453125" />
                  <Point X="-3.055198730469" Y="23.84622265625" />
                  <Point X="-3.066898193359" Y="23.85434765625" />
                  <Point X="-3.091269775391" Y="23.86869140625" />
                  <Point X="-3.105435546875" Y="23.875517578125" />
                  <Point X="-3.134696044922" Y="23.886740234375" />
                  <Point X="-3.149790771484" Y="23.89113671875" />
                  <Point X="-3.181678466797" Y="23.897619140625" />
                  <Point X="-3.197294677734" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.02588671875" />
                  <Point X="-4.745078125" Y="24.05605859375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.625862060547" Y="24.656322265625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.499425537109" Y="24.690673828125" />
                  <Point X="-3.470882568359" Y="24.70225390625" />
                  <Point X="-3.440920898438" Y="24.718185546875" />
                  <Point X="-3.431359375" Y="24.724017578125" />
                  <Point X="-3.405818603516" Y="24.7417421875" />
                  <Point X="-3.392901611328" Y="24.75251953125" />
                  <Point X="-3.369168457031" Y="24.776185546875" />
                  <Point X="-3.358352294922" Y="24.78907421875" />
                  <Point X="-3.338374267578" Y="24.81769140625" />
                  <Point X="-3.330944335938" Y="24.8303046875" />
                  <Point X="-3.318110351562" Y="24.8565234375" />
                  <Point X="-3.312706298828" Y="24.87012890625" />
                  <Point X="-3.304188232422" Y="24.897572265625" />
                  <Point X="-3.300834960938" Y="24.912568359375" />
                  <Point X="-3.29658984375" Y="24.942904296875" />
                  <Point X="-3.295697998047" Y="24.958244140625" />
                  <Point X="-3.296430664062" Y="24.99025" />
                  <Point X="-3.297648193359" Y="25.003390625" />
                  <Point X="-3.301892822266" Y="25.029376953125" />
                  <Point X="-3.304919921875" Y="25.04222265625" />
                  <Point X="-3.313437988281" Y="25.06966796875" />
                  <Point X="-3.319866210938" Y="25.0853046875" />
                  <Point X="-3.335391845703" Y="25.115189453125" />
                  <Point X="-3.344489257812" Y="25.1294375" />
                  <Point X="-3.365931640625" Y="25.15721875" />
                  <Point X="-3.375682128906" Y="25.16802734375" />
                  <Point X="-3.396718505859" Y="25.188025390625" />
                  <Point X="-3.408004394531" Y="25.19721484375" />
                  <Point X="-3.433558105469" Y="25.214951171875" />
                  <Point X="-3.440481689453" Y="25.219328125" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496563964844" Y="25.24563671875" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731330566406" Y="25.957529296875" />
                  <Point X="-4.722641113281" Y="25.989595703125" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-3.862346191406" Y="26.216701171875" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.767739501953" Y="26.20481640625" />
                  <Point X="-3.747070556641" Y="26.204365234375" />
                  <Point X="-3.736728271484" Y="26.204703125" />
                  <Point X="-3.715179199219" Y="26.2065859375" />
                  <Point X="-3.704942382812" Y="26.208044921875" />
                  <Point X="-3.684673828125" Y="26.2120703125" />
                  <Point X="-3.669699462891" Y="26.216193359375" />
                  <Point X="-3.613140625" Y="26.23402734375" />
                  <Point X="-3.603443847656" Y="26.237677734375" />
                  <Point X="-3.584510498047" Y="26.246009765625" />
                  <Point X="-3.575273925781" Y="26.25069140625" />
                  <Point X="-3.556530273438" Y="26.261513671875" />
                  <Point X="-3.547854980469" Y="26.26717578125" />
                  <Point X="-3.531176025391" Y="26.27940625" />
                  <Point X="-3.515931396484" Y="26.293375" />
                  <Point X="-3.502294189453" Y="26.308923828125" />
                  <Point X="-3.495897949219" Y="26.317072265625" />
                  <Point X="-3.483483642578" Y="26.33480078125" />
                  <Point X="-3.478012939453" Y="26.34359765625" />
                  <Point X="-3.468063720703" Y="26.361732421875" />
                  <Point X="-3.461632080078" Y="26.37578515625" />
                  <Point X="-3.438937744141" Y="26.43057421875" />
                  <Point X="-3.435500244141" Y="26.44034765625" />
                  <Point X="-3.429710693359" Y="26.4602109375" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011230469" Y="26.60452734375" />
                  <Point X="-3.443505371094" Y="26.62380078125" />
                  <Point X="-3.450135498047" Y="26.63776171875" />
                  <Point X="-3.477518798828" Y="26.69036328125" />
                  <Point X="-3.482795166016" Y="26.69927734375" />
                  <Point X="-3.494290039062" Y="26.716482421875" />
                  <Point X="-3.500508544922" Y="26.7247734375" />
                  <Point X="-3.514421142578" Y="26.741353515625" />
                  <Point X="-3.521501953125" Y="26.748912109375" />
                  <Point X="-3.536443115234" Y="26.76321484375" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.227613769531" Y="27.294283203125" />
                  <Point X="-4.002297851562" Y="27.680302734375" />
                  <Point X="-3.979278076172" Y="27.709892578125" />
                  <Point X="-3.726336669922" Y="28.035013671875" />
                  <Point X="-3.305919189453" Y="27.79228515625" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244921386719" Y="27.75771875" />
                  <Point X="-3.225991943359" Y="27.749388671875" />
                  <Point X="-3.216298095703" Y="27.745740234375" />
                  <Point X="-3.195660644531" Y="27.739232421875" />
                  <Point X="-3.185645751953" Y="27.7366640625" />
                  <Point X="-3.165384765625" Y="27.73262890625" />
                  <Point X="-3.148292724609" Y="27.7305625" />
                  <Point X="-3.069522460938" Y="27.723671875" />
                  <Point X="-3.059171142578" Y="27.723333984375" />
                  <Point X="-3.038494384766" Y="27.72378515625" />
                  <Point X="-3.028168945313" Y="27.72457421875" />
                  <Point X="-3.006713378906" Y="27.7273984375" />
                  <Point X="-2.996532226562" Y="27.72930859375" />
                  <Point X="-2.976441162109" Y="27.734224609375" />
                  <Point X="-2.957009765625" Y="27.741294921875" />
                  <Point X="-2.938458740234" Y="27.75044140625" />
                  <Point X="-2.929429199219" Y="27.7555234375" />
                  <Point X="-2.911176757812" Y="27.767150390625" />
                  <Point X="-2.902763427734" Y="27.7731796875" />
                  <Point X="-2.886641845703" Y="27.786115234375" />
                  <Point X="-2.87408984375" Y="27.79786328125" />
                  <Point X="-2.818177978516" Y="27.853775390625" />
                  <Point X="-2.811260986328" Y="27.8614921875" />
                  <Point X="-2.798314453125" Y="27.877625" />
                  <Point X="-2.792284912109" Y="27.886041015625" />
                  <Point X="-2.780655761719" Y="27.904294921875" />
                  <Point X="-2.775575927734" Y="27.913318359375" />
                  <Point X="-2.766427001953" Y="27.931869140625" />
                  <Point X="-2.759351318359" Y="27.95130859375" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.013365234375" />
                  <Point X="-2.748458251953" Y="28.034044921875" />
                  <Point X="-2.749389892578" Y="28.051177734375" />
                  <Point X="-2.756281494141" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761781005859" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.05938671875" Y="28.699916015625" />
                  <Point X="-2.648361816406" Y="29.01504296875" />
                  <Point X="-2.6121328125" Y="29.035171875" />
                  <Point X="-2.192524902344" Y="29.268296875" />
                  <Point X="-2.134409423828" Y="29.19255859375" />
                  <Point X="-2.118563232422" Y="29.171908203125" />
                  <Point X="-2.111818603516" Y="29.164044921875" />
                  <Point X="-2.097511230469" Y="29.149099609375" />
                  <Point X="-2.089948486328" Y="29.142017578125" />
                  <Point X="-2.073369384766" Y="29.128107421875" />
                  <Point X="-2.065087646484" Y="29.12189453125" />
                  <Point X="-2.047894165039" Y="29.11040625" />
                  <Point X="-2.031437011719" Y="29.101203125" />
                  <Point X="-1.94376574707" Y="29.0555625" />
                  <Point X="-1.934334838867" Y="29.0512890625" />
                  <Point X="-1.915061157227" Y="29.04379296875" />
                  <Point X="-1.905219604492" Y="29.0405703125" />
                  <Point X="-1.88431262207" Y="29.034966796875" />
                  <Point X="-1.874174438477" Y="29.032833984375" />
                  <Point X="-1.853724731445" Y="29.029685546875" />
                  <Point X="-1.833054199219" Y="29.028783203125" />
                  <Point X="-1.812407836914" Y="29.03013671875" />
                  <Point X="-1.802120483398" Y="29.031376953125" />
                  <Point X="-1.780804931641" Y="29.03513671875" />
                  <Point X="-1.770712890625" Y="29.0374921875" />
                  <Point X="-1.750853149414" Y="29.043283203125" />
                  <Point X="-1.733226928711" Y="29.049974609375" />
                  <Point X="-1.641911743164" Y="29.087798828125" />
                  <Point X="-1.63257800293" Y="29.092275390625" />
                  <Point X="-1.614450927734" Y="29.102220703125" />
                  <Point X="-1.605657104492" Y="29.107689453125" />
                  <Point X="-1.587927612305" Y="29.120103515625" />
                  <Point X="-1.579779785156" Y="29.126498046875" />
                  <Point X="-1.564227416992" Y="29.14013671875" />
                  <Point X="-1.550252441406" Y="29.15538671875" />
                  <Point X="-1.538020751953" Y="29.172068359375" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.516855102539" Y="29.20873046875" />
                  <Point X="-1.508523925781" Y="29.227666015625" />
                  <Point X="-1.502317504883" Y="29.245474609375" />
                  <Point X="-1.472595947266" Y="29.33973828125" />
                  <Point X="-1.470025268555" Y="29.34976953125" />
                  <Point X="-1.465990722656" Y="29.3700546875" />
                  <Point X="-1.46452722168" Y="29.3803046875" />
                  <Point X="-1.462640625" Y="29.4018671875" />
                  <Point X="-1.462301635742" Y="29.412220703125" />
                  <Point X="-1.462752807617" Y="29.432900390625" />
                  <Point X="-1.46354309082" Y="29.443228515625" />
                  <Point X="-1.47926574707" Y="29.562654296875" />
                  <Point X="-0.931164428711" Y="29.7163203125" />
                  <Point X="-0.887245788574" Y="29.721462890625" />
                  <Point X="-0.365222381592" Y="29.78255859375" />
                  <Point X="-0.240979019165" Y="29.318875" />
                  <Point X="-0.225666397095" Y="29.2617265625" />
                  <Point X="-0.220435317993" Y="29.247107421875" />
                  <Point X="-0.207661911011" Y="29.218916015625" />
                  <Point X="-0.200119308472" Y="29.20534375" />
                  <Point X="-0.182260925293" Y="29.1786171875" />
                  <Point X="-0.172608901978" Y="29.166455078125" />
                  <Point X="-0.151451400757" Y="29.143865234375" />
                  <Point X="-0.126896514893" Y="29.1250234375" />
                  <Point X="-0.099600578308" Y="29.11043359375" />
                  <Point X="-0.085354034424" Y="29.1042578125" />
                  <Point X="-0.054916061401" Y="29.09392578125" />
                  <Point X="-0.039853725433" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629491806" Y="29.08511328125" />
                  <Point X="0.052165405273" Y="29.090154296875" />
                  <Point X="0.067227592468" Y="29.09392578125" />
                  <Point X="0.097665718079" Y="29.1042578125" />
                  <Point X="0.111912261963" Y="29.11043359375" />
                  <Point X="0.139208190918" Y="29.1250234375" />
                  <Point X="0.16376322937" Y="29.143865234375" />
                  <Point X="0.184920883179" Y="29.166455078125" />
                  <Point X="0.194573196411" Y="29.1786171875" />
                  <Point X="0.212431289673" Y="29.20534375" />
                  <Point X="0.219973754883" Y="29.218916015625" />
                  <Point X="0.232747146606" Y="29.247107421875" />
                  <Point X="0.237978225708" Y="29.2617265625" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.827876220703" Y="29.737912109375" />
                  <Point X="0.864224914551" Y="29.72913671875" />
                  <Point X="1.453586547852" Y="29.586845703125" />
                  <Point X="1.474926269531" Y="29.579107421875" />
                  <Point X="1.858260375977" Y="29.440068359375" />
                  <Point X="1.881141479492" Y="29.4293671875" />
                  <Point X="2.250448486328" Y="29.256654296875" />
                  <Point X="2.272622802734" Y="29.243736328125" />
                  <Point X="2.629438232422" Y="29.03585546875" />
                  <Point X="2.650289550781" Y="29.02102734375" />
                  <Point X="2.817780517578" Y="28.901916015625" />
                  <Point X="2.133583984375" Y="27.716853515625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.061472412109" Y="27.5912265625" />
                  <Point X="2.050865478516" Y="27.567330078125" />
                  <Point X="2.041674560547" Y="27.540978515625" />
                  <Point X="2.039599853516" Y="27.534234375" />
                  <Point X="2.019831542969" Y="27.46030859375" />
                  <Point X="2.017596679688" Y="27.4494453125" />
                  <Point X="2.014408203125" Y="27.427529296875" />
                  <Point X="2.013454711914" Y="27.4164765625" />
                  <Point X="2.012780395508" Y="27.39207421875" />
                  <Point X="2.012845458984" Y="27.3850625" />
                  <Point X="2.01407434082" Y="27.364078125" />
                  <Point X="2.021782348633" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.02914453125" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045319335938" Y="27.22388671875" />
                  <Point X="2.055677001953" Y="27.2038515625" />
                  <Point X="2.06485546875" Y="27.189126953125" />
                  <Point X="2.104409179688" Y="27.1308359375" />
                  <Point X="2.111211669922" Y="27.1219765625" />
                  <Point X="2.125809082031" Y="27.10512109375" />
                  <Point X="2.133604003906" Y="27.097125" />
                  <Point X="2.151919677734" Y="27.080419921875" />
                  <Point X="2.157099853516" Y="27.0760234375" />
                  <Point X="2.173271728516" Y="27.063576171875" />
                  <Point X="2.231563964844" Y="27.024021484375" />
                  <Point X="2.241282226562" Y="27.018240234375" />
                  <Point X="2.261337890625" Y="27.007873046875" />
                  <Point X="2.271675292969" Y="27.003287109375" />
                  <Point X="2.293775390625" Y="26.99501953125" />
                  <Point X="2.304590576172" Y="26.991693359375" />
                  <Point X="2.326524902344" Y="26.986353515625" />
                  <Point X="2.343113037109" Y="26.9836796875" />
                  <Point X="2.407013916016" Y="26.975974609375" />
                  <Point X="2.41837109375" Y="26.975291015625" />
                  <Point X="2.441089599609" Y="26.975287109375" />
                  <Point X="2.452450927734" Y="26.975966796875" />
                  <Point X="2.477717041016" Y="26.979005859375" />
                  <Point X="2.484358886719" Y="26.980044921875" />
                  <Point X="2.504113525391" Y="26.984095703125" />
                  <Point X="2.578037841797" Y="27.003865234375" />
                  <Point X="2.584006347656" Y="27.005673828125" />
                  <Point X="2.604415039063" Y="27.013072265625" />
                  <Point X="2.627659912109" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.940403076172" Y="27.780953125" />
                  <Point X="4.043953369141" Y="27.637041015625" />
                  <Point X="4.055578125" Y="27.617830078125" />
                  <Point X="4.136883300781" Y="27.483470703125" />
                  <Point X="3.262842529297" Y="26.812796875" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.166443359375" Y="26.738345703125" />
                  <Point X="3.147957275391" Y="26.7208125" />
                  <Point X="3.134133789062" Y="26.70558984375" />
                  <Point X="3.123997070312" Y="26.69344921875" />
                  <Point X="3.070793701172" Y="26.624041015625" />
                  <Point X="3.064457519531" Y="26.614666015625" />
                  <Point X="3.052945800781" Y="26.595234375" />
                  <Point X="3.047770263672" Y="26.585177734375" />
                  <Point X="3.037595214844" Y="26.562126953125" />
                  <Point X="3.035076416016" Y="26.5558203125" />
                  <Point X="3.028435058594" Y="26.536572265625" />
                  <Point X="3.008616699219" Y="26.46570703125" />
                  <Point X="3.006225585938" Y="26.454662109375" />
                  <Point X="3.002771972656" Y="26.43236328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.006100097656" Y="26.34578125" />
                  <Point X="3.022368896484" Y="26.26693359375" />
                  <Point X="3.025337646484" Y="26.2559296875" />
                  <Point X="3.032570556641" Y="26.234359375" />
                  <Point X="3.036834716797" Y="26.22379296875" />
                  <Point X="3.047840820312" Y="26.20062890625" />
                  <Point X="3.0607265625" Y="26.17773828125" />
                  <Point X="3.104976074219" Y="26.11048046875" />
                  <Point X="3.111735107422" Y="26.1014296875" />
                  <Point X="3.126279541016" Y="26.084193359375" />
                  <Point X="3.134064941406" Y="26.0760078125" />
                  <Point X="3.151302246094" Y="26.059919921875" />
                  <Point X="3.160019042969" Y="26.05270703125" />
                  <Point X="3.178245361328" Y="26.039369140625" />
                  <Point X="3.193279785156" Y="26.030134765625" />
                  <Point X="3.257403564453" Y="25.9940390625" />
                  <Point X="3.267853271484" Y="25.98897265625" />
                  <Point X="3.289293212891" Y="25.98015234375" />
                  <Point X="3.300283447266" Y="25.9763984375" />
                  <Point X="3.325720947266" Y="25.9694140625" />
                  <Point X="3.351135986328" Y="25.96426953125" />
                  <Point X="3.437835449219" Y="25.9528125" />
                  <Point X="3.444033935547" Y="25.95219921875" />
                  <Point X="3.465708984375" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603271484" Y="25.952796875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752684570312" Y="25.914232421875" />
                  <Point X="4.75634765625" Y="25.890701171875" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="3.795009033203" Y="25.44895703125" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.683718261719" Y="25.418724609375" />
                  <Point X="3.659474365234" Y="25.409365234375" />
                  <Point X="3.640063476562" Y="25.40012109375" />
                  <Point X="3.62667578125" Y="25.393078125" />
                  <Point X="3.541496337891" Y="25.34384375" />
                  <Point X="3.531893066406" Y="25.33748828125" />
                  <Point X="3.513522949219" Y="25.32365625" />
                  <Point X="3.504756103516" Y="25.3161796875" />
                  <Point X="3.486045410156" Y="25.298154296875" />
                  <Point X="3.468398925781" Y="25.278623046875" />
                  <Point X="3.417291259766" Y="25.2135" />
                  <Point X="3.410853027344" Y="25.20420703125" />
                  <Point X="3.399127929688" Y="25.184923828125" />
                  <Point X="3.393841064453" Y="25.17493359375" />
                  <Point X="3.384067382812" Y="25.15346875" />
                  <Point X="3.380004150391" Y="25.142923828125" />
                  <Point X="3.373158447266" Y="25.121423828125" />
                  <Point X="3.368909912109" Y="25.1028125" />
                  <Point X="3.351873779297" Y="25.013857421875" />
                  <Point X="3.350391113281" Y="25.00234375" />
                  <Point X="3.348841308594" Y="24.979220703125" />
                  <Point X="3.348774169922" Y="24.967611328125" />
                  <Point X="3.350240234375" Y="24.9411484375" />
                  <Point X="3.353340332031" Y="24.915921875" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067626953" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.762501953125" />
                  <Point X="3.399127685547" Y="24.752515625" />
                  <Point X="3.410846679688" Y="24.7332421875" />
                  <Point X="3.421677734375" Y="24.718349609375" />
                  <Point X="3.472785400391" Y="24.6532265625" />
                  <Point X="3.480665039062" Y="24.644380859375" />
                  <Point X="3.497464111328" Y="24.62773828125" />
                  <Point X="3.506383544922" Y="24.61994140625" />
                  <Point X="3.528025878906" Y="24.60328515625" />
                  <Point X="3.548823242188" Y="24.589359375" />
                  <Point X="3.634002685547" Y="24.540123046875" />
                  <Point X="3.639489746094" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.68302734375" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.06894921875" />
                  <Point X="4.756918457031" Y="24.048373046875" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.556252929688" Y="24.075017578125" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.424840332031" Y="24.091556640625" />
                  <Point X="3.40095703125" Y="24.091671875" />
                  <Point X="3.389015380859" Y="24.0909765625" />
                  <Point X="3.357482177734" Y="24.08713671875" />
                  <Point X="3.340094482422" Y="24.0841953125" />
                  <Point X="3.172917480469" Y="24.047859375" />
                  <Point X="3.161477294922" Y="24.044611328125" />
                  <Point X="3.131112548828" Y="24.03389453125" />
                  <Point X="3.110427734375" Y="24.0236171875" />
                  <Point X="3.078407714844" Y="24.0025" />
                  <Point X="3.063474609375" Y="23.99030859375" />
                  <Point X="3.042278320312" Y="23.96907421875" />
                  <Point X="3.030653320312" Y="23.95631640625" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.923183837891" Y="23.82610546875" />
                  <Point X="2.907157714844" Y="23.801638671875" />
                  <Point X="2.897614257812" Y="23.782775390625" />
                  <Point X="2.885320556641" Y="23.749806640625" />
                  <Point X="2.880713378906" Y="23.732748046875" />
                  <Point X="2.875871337891" Y="23.704650390625" />
                  <Point X="2.873910888672" Y="23.689794921875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.85908203125" Y="23.520515625" />
                  <Point X="2.860162597656" Y="23.488328125" />
                  <Point X="2.863780029297" Y="23.465361328125" />
                  <Point X="2.874421386719" Y="23.428201171875" />
                  <Point X="2.881651855469" Y="23.410169921875" />
                  <Point X="2.896125244141" Y="23.382623046875" />
                  <Point X="2.904502197266" Y="23.368244140625" />
                  <Point X="2.997020263672" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486572266" Y="23.171880859375" />
                  <Point X="3.052795898438" Y="23.163720703125" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.045493408203" Y="22.302576171875" />
                  <Point X="4.035778320312" Y="22.2887734375" />
                  <Point X="4.001272949219" Y="22.23974609375" />
                  <Point X="2.954859863281" Y="22.843892578125" />
                  <Point X="2.848454833984" Y="22.905326171875" />
                  <Point X="2.837419433594" Y="22.91077734375" />
                  <Point X="2.814734375" Y="22.92020703125" />
                  <Point X="2.803084716797" Y="22.924185546875" />
                  <Point X="2.76984765625" Y="22.9331796875" />
                  <Point X="2.753985351562" Y="22.93675390625" />
                  <Point X="2.555018066406" Y="22.9726875" />
                  <Point X="2.543198486328" Y="22.974064453125" />
                  <Point X="2.5110390625" Y="22.97578125" />
                  <Point X="2.487681884766" Y="22.974138671875" />
                  <Point X="2.449421386719" Y="22.966630859375" />
                  <Point X="2.430679443359" Y="22.960892578125" />
                  <Point X="2.400782958984" Y="22.948236328125" />
                  <Point X="2.386364013672" Y="22.941404296875" />
                  <Point X="2.221071044922" Y="22.854412109375" />
                  <Point X="2.2118125" Y="22.848845703125" />
                  <Point X="2.187643310547" Y="22.832373046875" />
                  <Point X="2.171410888672" Y="22.818384765625" />
                  <Point X="2.147119384766" Y="22.792126953125" />
                  <Point X="2.136218505859" Y="22.777841796875" />
                  <Point X="2.119840820312" Y="22.751548828125" />
                  <Point X="2.112977050781" Y="22.739580078125" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.02111328125" Y="22.563431640625" />
                  <Point X="2.009793945312" Y="22.533279296875" />
                  <Point X="2.004311157227" Y="22.5103515625" />
                  <Point X="1.999983764648" Y="22.471294921875" />
                  <Point X="1.999861694336" Y="22.451541015625" />
                  <Point X="2.003166503906" Y="22.417916015625" />
                  <Point X="2.005280151367" Y="22.402736328125" />
                  <Point X="2.041213378906" Y="22.203767578125" />
                  <Point X="2.043014770508" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723754150391" Y="20.9639140625" />
                  <Point X="1.915111450195" Y="22.0177578125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657958984" Y="22.12984765625" />
                  <Point X="1.808835327148" Y="22.15037109375" />
                  <Point X="1.783252807617" Y="22.17199609375" />
                  <Point X="1.773299072266" Y="22.179353515625" />
                  <Point X="1.756411010742" Y="22.1902109375" />
                  <Point X="1.560175415039" Y="22.31637109375" />
                  <Point X="1.549784301758" Y="22.322166015625" />
                  <Point X="1.520732299805" Y="22.33605859375" />
                  <Point X="1.498242431641" Y="22.34353515625" />
                  <Point X="1.45944921875" Y="22.351234375" />
                  <Point X="1.439648681641" Y="22.35304296875" />
                  <Point X="1.404721679688" Y="22.3525625" />
                  <Point X="1.38992590332" Y="22.35178125" />
                  <Point X="1.175308837891" Y="22.332033203125" />
                  <Point X="1.164628540039" Y="22.330435546875" />
                  <Point X="1.135993041992" Y="22.324466796875" />
                  <Point X="1.115403686523" Y="22.31764453125" />
                  <Point X="1.082496948242" Y="22.302380859375" />
                  <Point X="1.066776977539" Y="22.293162109375" />
                  <Point X="1.039682250977" Y="22.2735546875" />
                  <Point X="1.029596557617" Y="22.265728515625" />
                  <Point X="0.863874633789" Y="22.127935546875" />
                  <Point X="0.852652709961" Y="22.11691015625" />
                  <Point X="0.832181945801" Y="22.09316015625" />
                  <Point X="0.822933349609" Y="22.080435546875" />
                  <Point X="0.806041137695" Y="22.052609375" />
                  <Point X="0.799018859863" Y="22.03853125" />
                  <Point X="0.787394165039" Y="22.00941015625" />
                  <Point X="0.782792175293" Y="21.9943671875" />
                  <Point X="0.778528076172" Y="21.97475" />
                  <Point X="0.728977905273" Y="21.74678125" />
                  <Point X="0.727585021973" Y="21.7387109375" />
                  <Point X="0.72472467041" Y="21.710322265625" />
                  <Point X="0.72474230957" Y="21.676828125" />
                  <Point X="0.725555175781" Y="21.664478515625" />
                  <Point X="0.833091064453" Y="20.84766015625" />
                  <Point X="0.686807067871" Y="21.393599609375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605957031" Y="21.519876953125" />
                  <Point X="0.642145874023" Y="21.54641796875" />
                  <Point X="0.626785705566" Y="21.5761875" />
                  <Point X="0.620403869629" Y="21.586796875" />
                  <Point X="0.607430114746" Y="21.60548828125" />
                  <Point X="0.456676239014" Y="21.822697265625" />
                  <Point X="0.446669006348" Y="21.83483203125" />
                  <Point X="0.424787414551" Y="21.85728515625" />
                  <Point X="0.412913024902" Y="21.867603515625" />
                  <Point X="0.386657623291" Y="21.88684765625" />
                  <Point X="0.373238311768" Y="21.89506640625" />
                  <Point X="0.345235076904" Y="21.909173828125" />
                  <Point X="0.330651000977" Y="21.9150625" />
                  <Point X="0.310574707031" Y="21.92129296875" />
                  <Point X="0.077292037964" Y="21.9936953125" />
                  <Point X="0.063376476288" Y="21.996890625" />
                  <Point X="0.035218677521" Y="22.00116015625" />
                  <Point X="0.020976739883" Y="22.002234375" />
                  <Point X="-0.008664761543" Y="22.002234375" />
                  <Point X="-0.022906848907" Y="22.00116015625" />
                  <Point X="-0.051064350128" Y="21.996890625" />
                  <Point X="-0.064979469299" Y="21.9936953125" />
                  <Point X="-0.085055892944" Y="21.98746484375" />
                  <Point X="-0.3183387146" Y="21.9150625" />
                  <Point X="-0.332927093506" Y="21.909171875" />
                  <Point X="-0.360930786133" Y="21.8950625" />
                  <Point X="-0.374345916748" Y="21.886845703125" />
                  <Point X="-0.400601348877" Y="21.8676015625" />
                  <Point X="-0.412474700928" Y="21.857283203125" />
                  <Point X="-0.434358978271" Y="21.834828125" />
                  <Point X="-0.444369903564" Y="21.822689453125" />
                  <Point X="-0.457343658447" Y="21.80399609375" />
                  <Point X="-0.60809765625" Y="21.5867890625" />
                  <Point X="-0.612471618652" Y="21.5798671875" />
                  <Point X="-0.625977416992" Y="21.554734375" />
                  <Point X="-0.638778198242" Y="21.523783203125" />
                  <Point X="-0.642752929688" Y="21.512064453125" />
                  <Point X="-0.985425170898" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.13616839432" Y="29.6634807416" />
                  <Point X="0.352956189899" Y="29.690831114454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.347195431627" Y="29.715280947838" />
                  <Point X="-0.803820720703" Y="29.731226654302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.543611509143" Y="29.554194607798" />
                  <Point X="0.327721675452" Y="29.596654416408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.321484221069" Y="29.619325185873" />
                  <Point X="-1.171282086951" Y="29.649000781262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.833608667779" Y="29.44900977716" />
                  <Point X="0.302487161005" Y="29.502477718362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.295773010512" Y="29.523369423907" />
                  <Point X="-1.472783904929" Y="29.56447155004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059248420758" Y="29.346072356649" />
                  <Point X="0.277252646558" Y="29.408301020315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.270061799955" Y="29.427413661942" />
                  <Point X="-1.466963660216" Y="29.469210395908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.272938819144" Y="29.243552216796" />
                  <Point X="0.252018132111" Y="29.314124322269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.244350589398" Y="29.331457899976" />
                  <Point X="-1.46541336592" Y="29.37409835173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.446503579269" Y="29.142433295108" />
                  <Point X="0.220539961655" Y="29.220165657495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.215121349079" Y="29.235379285704" />
                  <Point X="-1.491447466805" Y="29.279949575857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.620068339393" Y="29.041314373419" />
                  <Point X="0.142853540354" Y="29.127820620398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.143577076069" Y="29.137822997929" />
                  <Point X="-1.529204101459" Y="29.186210159885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.146067237107" Y="29.207751495252" />
                  <Point X="-2.292310079186" Y="29.212858407831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.762420373427" Y="28.941285424144" />
                  <Point X="-1.628316679211" Y="29.094613340658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.045718028847" Y="29.109189316974" />
                  <Point X="-2.453289038456" Y="29.123422010252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785170061855" Y="28.84543308081" />
                  <Point X="-2.614267911685" Y="29.033985609669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.731372955172" Y="28.752253810463" />
                  <Point X="-2.741829963904" Y="28.943382267982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67757584849" Y="28.659074540116" />
                  <Point X="-2.860414206071" Y="28.852465414259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623778741808" Y="28.56589526977" />
                  <Point X="-2.978998448237" Y="28.761548560537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.569981635126" Y="28.472715999423" />
                  <Point X="-3.041345594108" Y="28.668667864138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.516184528444" Y="28.379536729076" />
                  <Point X="-2.98533463272" Y="28.571654011558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.462387421762" Y="28.28635745873" />
                  <Point X="-2.929323671332" Y="28.474640158978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408590315079" Y="28.193178188383" />
                  <Point X="-2.873312709944" Y="28.377626306398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.354793208397" Y="28.099998918037" />
                  <Point X="-2.817301748556" Y="28.280612453818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.300996101715" Y="28.00681964769" />
                  <Point X="-2.76855483628" Y="28.183852267422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.247198995033" Y="27.913640377343" />
                  <Point X="-2.752632269913" Y="28.088238332444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956270884335" Y="27.758900365144" />
                  <Point X="3.905290134872" Y="27.760680652144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.193401888351" Y="27.820461106997" />
                  <Point X="-2.751005132488" Y="27.993123604845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.711900274908" Y="28.026678802619" />
                  <Point X="-3.732267780734" Y="28.027390051595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.026431618337" Y="27.661392391616" />
                  <Point X="3.750035564784" Y="27.671044354491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.139604781668" Y="27.72728183665" />
                  <Point X="-2.783892603934" Y="27.899214153946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.536655837633" Y="27.925501225312" />
                  <Point X="-3.80426596142" Y="27.934846376758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088041687656" Y="27.564183013879" />
                  <Point X="3.594780994696" Y="27.581408056837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085807468877" Y="27.634102573501" />
                  <Point X="-2.864965779913" Y="27.806987384928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361411400357" Y="27.824323648005" />
                  <Point X="-3.876264142107" Y="27.842302701921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.116875502685" Y="27.468118208162" />
                  <Point X="3.439526424609" Y="27.491771759183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041555039747" Y="27.54058999567" />
                  <Point X="-3.948262322793" Y="27.749759027085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998385828485" Y="27.377198052053" />
                  <Point X="3.284271854521" Y="27.402135461529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017151342583" Y="27.446384284845" />
                  <Point X="-4.015863362835" Y="27.657061800713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879896154286" Y="27.286277895945" />
                  <Point X="3.129017284433" Y="27.312499163875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015605451527" Y="27.351380361842" />
                  <Point X="-4.070239413487" Y="27.563902747535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.761406480087" Y="27.195357739836" />
                  <Point X="2.973762714345" Y="27.222862866222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032685443152" Y="27.255726008683" />
                  <Point X="-4.124615464138" Y="27.470743694358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642916805887" Y="27.104437583728" />
                  <Point X="2.818508144257" Y="27.133226568568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085415873406" Y="27.158826714774" />
                  <Point X="-4.17899151479" Y="27.37758464118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.524427131688" Y="27.013517427619" />
                  <Point X="2.663253574169" Y="27.043590270914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177739083546" Y="27.060544810526" />
                  <Point X="-4.213880262048" Y="27.283745076372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405937457489" Y="26.922597271511" />
                  <Point X="-4.084092221233" Y="27.184154871408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.287447783289" Y="26.831677115402" />
                  <Point X="-3.954304180418" Y="27.084564666443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169295773299" Y="26.7407451678" />
                  <Point X="-3.824516139602" Y="26.984974461479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089521544227" Y="26.648473038556" />
                  <Point X="-3.694728098787" Y="26.885384256514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034904614974" Y="26.555322397045" />
                  <Point X="-3.564940057971" Y="26.78579405155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007644530458" Y="26.461216433464" />
                  <Point X="-3.476097837691" Y="26.687633706146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002860448776" Y="26.366325590569" />
                  <Point X="-3.431810505167" Y="26.591029251707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.021609728471" Y="26.270612944586" />
                  <Point X="-3.423111931983" Y="26.49566758413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.063116543658" Y="26.174105587953" />
                  <Point X="-3.450946821948" Y="26.401581693198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.726703262284" Y="26.020953952914" />
                  <Point X="4.166815154627" Y="26.040505676462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133511759674" Y="26.076589426132" />
                  <Point X="-3.502810701466" Y="26.308334913071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.75004349198" Y="25.925080987424" />
                  <Point X="3.596149678341" Y="25.965375847308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.3031656333" Y="25.97560707561" />
                  <Point X="-3.661386459324" Y="26.21881459385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.956742825262" Y="26.229128665422" />
                  <Point X="-4.651161196339" Y="26.25337828929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765881071216" Y="25.829470020262" />
                  <Point X="-4.6766785613" Y="26.159211468601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780762067919" Y="25.733892457698" />
                  <Point X="-4.702195926261" Y="26.065044647912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.535588489275" Y="25.647396201015" />
                  <Point X="-4.727713340244" Y="25.970877828936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.221733429774" Y="25.563298354493" />
                  <Point X="-4.743340827418" Y="25.876365646105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.907878370272" Y="25.479200507971" />
                  <Point X="-4.757334790065" Y="25.78179641934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628252054644" Y="25.393907367375" />
                  <Point X="-4.771328752712" Y="25.687227192576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491716210901" Y="25.303617397393" />
                  <Point X="-4.785322715358" Y="25.592657965811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415707349914" Y="25.211213778599" />
                  <Point X="-4.381108991823" Y="25.483484604838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372301905831" Y="25.117671623398" />
                  <Point X="-3.973184236238" Y="25.37418165177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353675302582" Y="25.023264172008" />
                  <Point X="-3.565259480653" Y="25.264878698702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351822750602" Y="24.92827095784" />
                  <Point X="-3.371186273564" Y="25.163043606264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.36929685527" Y="24.83260284195" />
                  <Point X="-3.312277405342" Y="25.065928556547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.409075060572" Y="24.736155849704" />
                  <Point X="-3.295974006492" Y="24.970301322606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.486715587111" Y="24.638386676065" />
                  <Point X="-3.310956426866" Y="24.875766613546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637877427316" Y="24.538050081579" />
                  <Point X="-3.363823335094" Y="24.782554859953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.013764400637" Y="24.42986591252" />
                  <Point X="-3.495888631416" Y="24.692108775016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.421691030517" Y="24.320562894" />
                  <Point X="-3.806574679403" Y="24.607900264173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783313352027" Y="24.212876857559" />
                  <Point X="-4.120430687849" Y="24.523802450789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769057106087" Y="24.118316789929" />
                  <Point X="-4.434286696295" Y="24.439704637406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.751328726438" Y="24.02387797188" />
                  <Point X="3.653498460636" Y="24.062215049533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296348032176" Y="24.074687017319" />
                  <Point X="-4.748142704741" Y="24.355606824022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729808182862" Y="23.929571579113" />
                  <Point X="4.636203952089" Y="23.932840310879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.061017465748" Y="23.987847035076" />
                  <Point X="-4.774456275998" Y="24.26146780747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980179526784" Y="23.8956120514" />
                  <Point X="-4.760792608498" Y="24.165932754978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908096142026" Y="23.803071351955" />
                  <Point X="-4.747128940998" Y="24.070397702487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876640187304" Y="23.70911191139" />
                  <Point X="-4.72767792257" Y="23.974660551247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866972305005" Y="23.614391614571" />
                  <Point X="-3.024478182551" Y="23.820125599019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676572804886" Y="23.842897245013" />
                  <Point X="-4.703178615983" Y="23.878747109901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859112495133" Y="23.519608178472" />
                  <Point X="-2.960094854349" Y="23.722819376948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247244055295" Y="23.767767617495" />
                  <Point X="-4.678679309396" Y="23.782833668554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876123593245" Y="23.423956231127" />
                  <Point X="-2.949140759662" Y="23.627378944824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931030525728" Y="23.326980932086" />
                  <Point X="-2.971116565335" Y="23.53308845016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.993547289347" Y="23.229739891886" />
                  <Point X="-3.033270683565" Y="23.440201013087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.095269250508" Y="23.13112977602" />
                  <Point X="-3.148407441803" Y="23.349163770573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.225057846199" Y="23.031539551679" />
                  <Point X="-3.266897020166" Y="23.258243611118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354846441889" Y="22.931949327337" />
                  <Point X="2.642933860528" Y="22.956809862489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440440306902" Y="22.963881093199" />
                  <Point X="-3.385386600649" Y="23.167323451737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48463503758" Y="22.832359102996" />
                  <Point X="2.942016523069" Y="22.851307759063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.260399557175" Y="22.875110348011" />
                  <Point X="-3.503876181131" Y="23.076403292355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.61442363327" Y="22.732768878655" />
                  <Point X="3.117261218763" Y="22.750130172732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.141084895134" Y="22.784219001113" />
                  <Point X="-3.622365761614" Y="22.985483132974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.74421222896" Y="22.633178654314" />
                  <Point X="3.292506061623" Y="22.648952581262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087428092605" Y="22.691034831237" />
                  <Point X="-3.740855342096" Y="22.894562973593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.874000824651" Y="22.533588429973" />
                  <Point X="3.467750904482" Y="22.547774989791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038302760243" Y="22.597692418936" />
                  <Point X="-3.859344922579" Y="22.803642814212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.003789420341" Y="22.433998205632" />
                  <Point X="3.642995747341" Y="22.446597398321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.003590431594" Y="22.503846693455" />
                  <Point X="-3.977834503061" Y="22.712722654831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.066609925394" Y="22.336746558547" />
                  <Point X="3.818240590201" Y="22.34541980685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.004441534384" Y="22.408759065583" />
                  <Point X="-2.376537279648" Y="22.561746216896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.765889715143" Y="22.575342703547" />
                  <Point X="-4.096324083543" Y="22.621802495449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.004174603173" Y="22.243868941334" />
                  <Point X="3.99348543306" Y="22.24424221538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021467039363" Y="22.313106615139" />
                  <Point X="1.5330630694" Y="22.330162057593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.258988049085" Y="22.339732968201" />
                  <Point X="-2.321626630692" Y="22.464770788072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921144245816" Y="22.485706404516" />
                  <Point X="-4.164873339962" Y="22.529138381523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038743198515" Y="22.21744541166" />
                  <Point X="1.695399649115" Y="22.229435232605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.014542251186" Y="22.253211296854" />
                  <Point X="-2.312956343043" Y="22.369410108248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076398776489" Y="22.396070105486" />
                  <Point X="-4.106979379274" Y="22.432058773159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072556478349" Y="22.1212067192" />
                  <Point X="1.82876486492" Y="22.129720109937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.904825246303" Y="22.161984792383" />
                  <Point X="-2.341789766647" Y="22.275359086879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.231653307162" Y="22.306433806456" />
                  <Point X="-4.049085418586" Y="22.334979164794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.1285675194" Y="22.024192863838" />
                  <Point X="1.904160583916" Y="22.032029326705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816602984679" Y="22.070007674937" />
                  <Point X="-2.395586102652" Y="22.182179789619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.386907837835" Y="22.216797507426" />
                  <Point X="-3.987548505478" Y="22.237772341728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.184578560452" Y="21.927179008476" />
                  <Point X="1.97910932561" Y="21.934354152264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778858004774" Y="21.976267851971" />
                  <Point X="-2.449383167487" Y="22.089000517811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.542162368508" Y="22.127161208396" />
                  <Point X="-3.913219805101" Y="22.140118819607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240589601503" Y="21.830165153114" />
                  <Point X="2.054058119759" Y="21.836678975991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75835231051" Y="21.881926019885" />
                  <Point X="0.372581202097" Y="21.895397443839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.305414852893" Y="21.919073587791" />
                  <Point X="-2.503180305223" Y="21.995821248549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697416899181" Y="22.037524909365" />
                  <Point X="-3.838891104725" Y="22.042465297486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.296600642555" Y="21.733151297753" />
                  <Point X="2.129006913907" Y="21.739003799719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73784663186" Y="21.787584187254" />
                  <Point X="0.474667868134" Y="21.796774592198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.439416935686" Y="21.828695136928" />
                  <Point X="-2.556977442959" Y="21.902641979287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.352611683606" Y="21.636137442391" />
                  <Point X="2.203955708055" Y="21.641328623446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724733801233" Y="21.692984190682" />
                  <Point X="0.542281535608" Y="21.699355564193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.504596932663" Y="21.735913365869" />
                  <Point X="-2.610774580695" Y="21.809462710025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408622724657" Y="21.539123587029" />
                  <Point X="2.278904502203" Y="21.543653447174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734361180657" Y="21.597590088476" />
                  <Point X="0.609895395112" Y="21.601936529482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.569011300121" Y="21.643104858439" />
                  <Point X="-2.664571718431" Y="21.716283440763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464633765709" Y="21.442109731667" />
                  <Point X="2.353853296351" Y="21.445978270901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746933558101" Y="21.502093144673" />
                  <Point X="0.656894223179" Y="21.505237387533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.627892802869" Y="21.550103139115" />
                  <Point X="-2.718368856167" Y="21.6231041715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52064480676" Y="21.345095876305" />
                  <Point X="2.428802090499" Y="21.348303094629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759505935545" Y="21.406596200869" />
                  <Point X="0.682605138823" Y="21.409281635866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.65775178328" Y="21.456087930979" />
                  <Point X="-2.772165993903" Y="21.529924902238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.576655847812" Y="21.248082020943" />
                  <Point X="2.503750884647" Y="21.250627918356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772078312989" Y="21.311099257066" />
                  <Point X="0.708316344687" Y="21.313325874064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.682986335763" Y="21.361911234261" />
                  <Point X="-2.825963131639" Y="21.436745632976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632666888863" Y="21.151068165581" />
                  <Point X="2.578699678796" Y="21.152952742084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784650690433" Y="21.215602313263" />
                  <Point X="0.734027607247" Y="21.217370110282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.708220888246" Y="21.267734537543" />
                  <Point X="-2.879760269375" Y="21.343566363714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.688677929914" Y="21.054054310219" />
                  <Point X="2.653648472944" Y="21.055277565811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.797223067878" Y="21.12010536946" />
                  <Point X="0.759738869806" Y="21.121414346501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733455440729" Y="21.173557840825" />
                  <Point X="-1.678431937317" Y="21.206557147238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.007947915666" Y="21.218064098761" />
                  <Point X="-2.933557407111" Y="21.250387094452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809795445322" Y="21.024608425657" />
                  <Point X="0.785450132366" Y="21.025458582719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.758689993212" Y="21.079381144107" />
                  <Point X="-1.447102248048" Y="21.103421029774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.178110489908" Y="21.128948400084" />
                  <Point X="-2.965254914908" Y="21.156436089106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.822367822766" Y="20.929111481854" />
                  <Point X="0.811161394926" Y="20.929502818937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.783924545695" Y="20.985204447389" />
                  <Point X="-1.334214900915" Y="21.004421010038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313309208202" Y="21.038611736653" />
                  <Point X="-2.83593311377" Y="21.05686216559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.809159098178" Y="20.891027750672" />
                  <Point X="-1.231085325147" Y="20.905761739186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.419007356605" Y="20.947244890621" />
                  <Point X="-2.696034922562" Y="20.956918906394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.834393650661" Y="20.796851053954" />
                  <Point X="-1.184569349217" Y="20.809079458805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.490044239725" Y="20.854667646533" />
                  <Point X="-2.533334543323" Y="20.856179377246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.859628203144" Y="20.702674357236" />
                  <Point X="-1.165312607362" Y="20.713349091853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.884862755627" Y="20.608497660518" />
                  <Point X="-1.146272241119" Y="20.617626280904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.91009730811" Y="20.5143209638" />
                  <Point X="-1.127231878989" Y="20.521903470098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.935331860593" Y="20.420144267082" />
                  <Point X="-1.12055432838" Y="20.426612378184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.960566413076" Y="20.325967570364" />
                  <Point X="-1.132910817329" Y="20.331985969578" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001372070312" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.977974609375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.50328112793" Y="21.344423828125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.451344970703" Y="21.4971484375" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.254259597778" Y="21.73983203125" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.028741065979" Y="21.80600390625" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.301252838135" Y="21.6956640625" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.82806628418" Y="20.086361328125" />
                  <Point X="-0.84774420166" Y="20.012921875" />
                  <Point X="-1.100230957031" Y="20.061931640625" />
                  <Point X="-1.122218017578" Y="20.067587890625" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.312971313477" Y="20.4199375" />
                  <Point X="-1.309150390625" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.314479492188" Y="20.489353515625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.404766235352" Y="20.813580078125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.673772460938" Y="21.01584375" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.010319580078" Y="21.01255078125" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734375" Y="20.842705078125" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.855837646484" Y="20.832390625" />
                  <Point X="-2.886260498047" Y="20.85581640625" />
                  <Point X="-3.228580810547" Y="21.119390625" />
                  <Point X="-2.565022705078" Y="22.26870703125" />
                  <Point X="-2.506033447266" Y="22.370880859375" />
                  <Point X="-2.499764648438" Y="22.402421875" />
                  <Point X="-2.515353515625" Y="22.432609375" />
                  <Point X="-2.531327392578" Y="22.448583984375" />
                  <Point X="-2.560156982422" Y="22.46280078125" />
                  <Point X="-2.591683837891" Y="22.456529296875" />
                  <Point X="-3.778910888672" Y="21.771083984375" />
                  <Point X="-3.842959228516" Y="21.73410546875" />
                  <Point X="-3.843802490234" Y="21.735212890625" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.183516113281" Y="22.1894453125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.266998779297" Y="23.49765625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145211181641" Y="23.60634375" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.140329101562" Y="23.66541015625" />
                  <Point X="-3.163269775391" Y="23.6906015625" />
                  <Point X="-3.187641357422" Y="23.7049453125" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.718291015625" Y="23.51411328125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.9331640625" Y="24.02915625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.675037841797" Y="24.839849609375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.539685058594" Y="24.880111328125" />
                  <Point X="-3.514144287109" Y="24.8978359375" />
                  <Point X="-3.494166259766" Y="24.926453125" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.486380859375" Y="24.98590234375" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.516341308594" Y="25.04112890625" />
                  <Point X="-3.541895019531" Y="25.058865234375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.92365625" Y="25.43215625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.997776367188" Y="25.4548984375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.906026855469" Y="26.039291015625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.837546386719" Y="26.405076171875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731717285156" Y="26.39586328125" />
                  <Point X="-3.726836669922" Y="26.397400390625" />
                  <Point X="-3.670277832031" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.637166748047" Y="26.4485" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.618672607422" Y="26.5500390625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968505859" Y="26.619220703125" />
                  <Point X="-4.443619628906" Y="27.2205390625" />
                  <Point X="-4.47610546875" Y="27.245466796875" />
                  <Point X="-4.471375976562" Y="27.253568359375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.129240234375" Y="27.826560546875" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.210919189453" Y="27.956828125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.13851953125" Y="27.920435546875" />
                  <Point X="-3.131735107422" Y="27.91983984375" />
                  <Point X="-3.05296484375" Y="27.91294921875" />
                  <Point X="-3.031509277344" Y="27.9157734375" />
                  <Point X="-3.013256835938" Y="27.927400390625" />
                  <Point X="-3.008440429688" Y="27.93221484375" />
                  <Point X="-2.952528564453" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.938667236328" Y="28.034619140625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.299327148438" Y="28.73550390625" />
                  <Point X="-3.295169921875" Y="28.758560546875" />
                  <Point X="-2.752873046875" Y="29.174333984375" />
                  <Point X="-2.704408447266" Y="29.201259765625" />
                  <Point X="-2.141549316406" Y="29.513970703125" />
                  <Point X="-1.983672485352" Y="29.30822265625" />
                  <Point X="-1.967826416016" Y="29.287572265625" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.943702392578" Y="29.269734375" />
                  <Point X="-1.85603125" Y="29.22409375" />
                  <Point X="-1.835124389648" Y="29.218490234375" />
                  <Point X="-1.813808837891" Y="29.22225" />
                  <Point X="-1.805950195312" Y="29.225505859375" />
                  <Point X="-1.714634887695" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.683525634766" Y="29.3026015625" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.418427734375" />
                  <Point X="-1.689137329102" Y="29.701138671875" />
                  <Point X="-1.670126953125" Y="29.70646875" />
                  <Point X="-0.968082641602" Y="29.903296875" />
                  <Point X="-0.909339294434" Y="29.910173828125" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.057453018188" Y="29.36805078125" />
                  <Point X="-0.03193029213" Y="29.374890625" />
                  <Point X="-0.031930294037" Y="29.374890625" />
                  <Point X="-0.057453018188" Y="29.36805078125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282159805" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594078064" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.232384048462" Y="29.974953125" />
                  <Point X="0.247229232788" Y="29.989759765625" />
                  <Point X="0.860209960938" Y="29.925564453125" />
                  <Point X="0.908817565918" Y="29.913828125" />
                  <Point X="1.508456420898" Y="29.769056640625" />
                  <Point X="1.539703613281" Y="29.757724609375" />
                  <Point X="1.931044921875" Y="29.61578125" />
                  <Point X="1.961631469727" Y="29.6014765625" />
                  <Point X="2.338685058594" Y="29.425140625" />
                  <Point X="2.368265380859" Y="29.407908203125" />
                  <Point X="2.732533935547" Y="29.195685546875" />
                  <Point X="2.760401855469" Y="29.1758671875" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.29812890625" Y="27.621853515625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.223150634766" Y="27.48515234375" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202708007812" Y="27.38682421875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.222086425781" Y="27.295794921875" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.279955810547" Y="27.220798828125" />
                  <Point X="2.338248046875" Y="27.181244140625" />
                  <Point X="2.360348144531" Y="27.1729765625" />
                  <Point X="2.365849365234" Y="27.172314453125" />
                  <Point X="2.429760986328" Y="27.164607421875" />
                  <Point X="2.455027099609" Y="27.167646484375" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.915160400391" Y="27.98576953125" />
                  <Point X="3.994248046875" Y="28.0314296875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.218133789062" Y="27.7161953125" />
                  <Point X="4.387512207031" Y="27.436294921875" />
                  <Point X="3.378507080078" Y="26.66205859375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.274792480469" Y="26.577859375" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.2114140625" Y="26.485400390625" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.1921796875" Y="26.3841796875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.219454589844" Y="26.28216796875" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.28094140625" Y="26.198822265625" />
                  <Point X="3.286466308594" Y="26.195712890625" />
                  <Point X="3.350590087891" Y="26.1596171875" />
                  <Point X="3.376027587891" Y="26.1526328125" />
                  <Point X="3.462727050781" Y="26.14117578125" />
                  <Point X="3.475803466797" Y="26.141171875" />
                  <Point X="4.781131835938" Y="26.313021484375" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.9440859375" Y="25.919927734375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.844184814453" Y="25.2654296875" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.721756835938" Y="25.22858203125" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.617866699219" Y="25.161322265625" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.555519287109" Y="25.067078125" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.53994921875" Y="24.95166015625" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.571157226562" Y="24.835634765625" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.643907226562" Y="24.75385546875" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.938212890625" Y="24.378865234375" />
                  <Point X="4.998068359375" Y="24.362826171875" />
                  <Point X="4.948431640625" Y="24.033599609375" />
                  <Point X="4.942156738281" Y="24.0061015625" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="3.531453125" Y="23.886642578125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.380448974609" Y="23.89853125" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1979453125" Y="23.856078125" />
                  <Point X="3.176749023438" Y="23.83484375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.067953613281" Y="23.700482421875" />
                  <Point X="3.063111572266" Y="23.672384765625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.049849121094" Y="23.498541015625" />
                  <Point X="3.064322509766" Y="23.470994140625" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460449219" Y="23.314458984375" />
                  <Point X="4.279326171875" Y="22.4620625" />
                  <Point X="4.339073730469" Y="22.416216796875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.191153808594" Y="22.17941796875" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="2.859859863281" Y="22.679349609375" />
                  <Point X="2.753454833984" Y="22.740783203125" />
                  <Point X="2.720217773438" Y="22.74977734375" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.504749267578" Y="22.785923828125" />
                  <Point X="2.474852783203" Y="22.773267578125" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.297490966797" Y="22.677384765625" />
                  <Point X="2.28111328125" Y="22.651091796875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.188950439453" Y="22.470126953125" />
                  <Point X="2.192255371094" Y="22.436501953125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.947934326172" Y="20.985009765625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.835293945312" Y="20.80978515625" />
                  <Point X="2.820790527344" Y="20.800396484375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.764374389648" Y="21.90209375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.653661132812" Y="22.030390625" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.442262207031" Y="22.1630625" />
                  <Point X="1.407335205078" Y="22.16258203125" />
                  <Point X="1.192718261719" Y="22.142833984375" />
                  <Point X="1.178165649414" Y="22.13923828125" />
                  <Point X="1.151070800781" Y="22.119630859375" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.964192626953" Y="21.93439453125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.689279296875" />
                  <Point X="1.116227050781" Y="20.15267578125" />
                  <Point X="1.127642211914" Y="20.065970703125" />
                  <Point X="0.994369140625" Y="20.0367578125" />
                  <Point X="0.980938781738" Y="20.034318359375" />
                  <Point X="0.860200439453" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#124" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.015312729718" Y="4.412272361799" Z="0.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="-0.921307345037" Y="4.991113917252" Z="0.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.2" />
                  <Point X="-1.689780723436" Y="4.785894627758" Z="0.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.2" />
                  <Point X="-1.747125384685" Y="4.743057374226" Z="0.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.2" />
                  <Point X="-1.737367332973" Y="4.348916427052" Z="0.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.2" />
                  <Point X="-1.831240493722" Y="4.302979726633" Z="0.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.2" />
                  <Point X="-1.926770261387" Y="4.345363647817" Z="0.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.2" />
                  <Point X="-1.950161218653" Y="4.369942261713" Z="0.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.2" />
                  <Point X="-2.734847272975" Y="4.276246700695" Z="0.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.2" />
                  <Point X="-3.33174812985" Y="3.829520355301" Z="0.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.2" />
                  <Point X="-3.348784265961" Y="3.741784109804" Z="0.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.2" />
                  <Point X="-2.994633099151" Y="3.061542499955" Z="0.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.2" />
                  <Point X="-3.049951812332" Y="2.998851755555" Z="0.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.2" />
                  <Point X="-3.133533965652" Y="3.000931600022" Z="0.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.2" />
                  <Point X="-3.192075238493" Y="3.031409682785" Z="0.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.2" />
                  <Point X="-4.174859807845" Y="2.888544631605" Z="0.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.2" />
                  <Point X="-4.520714186189" Y="2.310054416899" Z="0.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.2" />
                  <Point X="-4.480213497757" Y="2.212150774946" Z="0.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.2" />
                  <Point X="-3.669179024437" Y="1.558231350708" Z="0.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.2" />
                  <Point X="-3.689516837987" Y="1.498915238275" Z="0.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.2" />
                  <Point X="-3.7480286516" Y="1.476367224557" Z="0.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.2" />
                  <Point X="-3.83717588936" Y="1.485928186794" Z="0.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.2" />
                  <Point X="-4.960442771941" Y="1.083650059998" Z="0.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.2" />
                  <Point X="-5.053393833907" Y="0.493497058308" Z="0.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.2" />
                  <Point X="-4.942753174663" Y="0.415139198428" Z="0.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.2" />
                  <Point X="-3.551007001741" Y="0.031333281416" Z="0.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.2" />
                  <Point X="-3.540289801749" Y="0.002361907531" Z="0.2" />
                  <Point X="-3.539556741714" Y="0" Z="0.2" />
                  <Point X="-3.548074799386" Y="-0.02744504353" Z="0.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.2" />
                  <Point X="-3.574361593383" Y="-0.047542701717" Z="0.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.2" />
                  <Point X="-3.694134614109" Y="-0.080572858328" Z="0.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.2" />
                  <Point X="-4.988815937082" Y="-0.946641204972" Z="0.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.2" />
                  <Point X="-4.85766084128" Y="-1.479044608744" Z="0.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.2" />
                  <Point X="-4.717920596735" Y="-1.504178992364" Z="0.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.2" />
                  <Point X="-3.194774789778" Y="-1.321214726984" Z="0.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.2" />
                  <Point X="-3.199770296639" Y="-1.349839984184" Z="0.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.2" />
                  <Point X="-3.303592677632" Y="-1.431394424448" Z="0.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.2" />
                  <Point X="-4.232615487134" Y="-2.804882598903" Z="0.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.2" />
                  <Point X="-3.889744410372" Y="-3.263789443654" Z="0.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.2" />
                  <Point X="-3.760066672089" Y="-3.240936889342" Z="0.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.2" />
                  <Point X="-2.556865670716" Y="-2.571464798416" Z="0.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.2" />
                  <Point X="-2.614480123827" Y="-2.675011710403" Z="0.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.2" />
                  <Point X="-2.922920436592" Y="-4.152520643663" Z="0.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.2" />
                  <Point X="-2.485932532402" Y="-4.427985122472" Z="0.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.2" />
                  <Point X="-2.433297000679" Y="-4.426317119006" Z="0.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.2" />
                  <Point X="-1.98869728566" Y="-3.99774282046" Z="0.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.2" />
                  <Point X="-1.68319874231" Y="-4.002768027624" Z="0.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.2" />
                  <Point X="-1.443889607504" Y="-4.192730776888" Z="0.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.2" />
                  <Point X="-1.369674739528" Y="-4.489120106559" Z="0.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.2" />
                  <Point X="-1.36869953655" Y="-4.542255604798" Z="0.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.2" />
                  <Point X="-1.140832932315" Y="-4.94955495558" Z="0.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.2" />
                  <Point X="-0.841419030868" Y="-5.009152807713" Z="0.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="-0.78592595454" Y="-4.895299716242" Z="0.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="-0.266332888343" Y="-3.301565462138" Z="0.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="-0.020076213648" Y="-3.210470284273" Z="0.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.2" />
                  <Point X="0.233282865714" Y="-3.276641761015" Z="0.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.2" />
                  <Point X="0.404112971089" Y="-3.50008029363" Z="0.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.2" />
                  <Point X="0.448828905861" Y="-3.637236313573" Z="0.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.2" />
                  <Point X="0.983719823648" Y="-4.983597409411" Z="0.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.2" />
                  <Point X="1.162865593541" Y="-4.944865160589" Z="0.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.2" />
                  <Point X="1.159643342027" Y="-4.809516021599" Z="0.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.2" />
                  <Point X="1.00689576775" Y="-3.044944900676" Z="0.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.2" />
                  <Point X="1.17688181142" Y="-2.887533775041" Z="0.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.2" />
                  <Point X="1.405760628849" Y="-2.85592619953" Z="0.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.2" />
                  <Point X="1.620465961836" Y="-2.980387887787" Z="0.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.2" />
                  <Point X="1.718550715073" Y="-3.097063001567" Z="0.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.2" />
                  <Point X="2.8418037675" Y="-4.210297314857" Z="0.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.2" />
                  <Point X="3.031517273384" Y="-4.075825790299" Z="0.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.2" />
                  <Point X="2.98507963371" Y="-3.958709994121" Z="0.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.2" />
                  <Point X="2.235303522256" Y="-2.523331222192" Z="0.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.2" />
                  <Point X="2.319203659383" Y="-2.340915230557" Z="0.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.2" />
                  <Point X="2.491983147815" Y="-2.239697650584" Z="0.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.2" />
                  <Point X="2.705175581321" Y="-2.268144487235" Z="0.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.2" />
                  <Point X="2.828703602986" Y="-2.332669866564" Z="0.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.2" />
                  <Point X="4.225886343566" Y="-2.818078633924" Z="0.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.2" />
                  <Point X="4.38657082646" Y="-2.560796594711" Z="0.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.2" />
                  <Point X="4.303608010521" Y="-2.466989980366" Z="0.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.2" />
                  <Point X="3.100224582831" Y="-1.47068698435" Z="0.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.2" />
                  <Point X="3.106745092452" Y="-1.300916772912" Z="0.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.2" />
                  <Point X="3.209039391271" Y="-1.165842936839" Z="0.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.2" />
                  <Point X="3.3849126736" Y="-1.119047538944" Z="0.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.2" />
                  <Point X="3.518770792434" Y="-1.131649069918" Z="0.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.2" />
                  <Point X="4.984748011621" Y="-0.973740927365" Z="0.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.2" />
                  <Point X="5.043531968677" Y="-0.598897075548" Z="0.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.2" />
                  <Point X="4.944997965373" Y="-0.5415579344" Z="0.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.2" />
                  <Point X="3.662773220578" Y="-0.171575371163" Z="0.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.2" />
                  <Point X="3.604335027241" Y="-0.102215003491" Z="0.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.2" />
                  <Point X="3.582900827892" Y="-0.007655433858" Z="0.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.2" />
                  <Point X="3.598470622532" Y="0.088955097386" Z="0.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.2" />
                  <Point X="3.651044411159" Y="0.161733735084" Z="0.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.2" />
                  <Point X="3.740622193774" Y="0.216573473079" Z="0.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.2" />
                  <Point X="3.850969851934" Y="0.248413998369" Z="0.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.2" />
                  <Point X="4.98733498793" Y="0.958899863585" Z="0.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.2" />
                  <Point X="4.888814666021" Y="1.375681610978" Z="0.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.2" />
                  <Point X="4.768449540904" Y="1.393873844088" Z="0.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.2" />
                  <Point X="3.376421935325" Y="1.233482475443" Z="0.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.2" />
                  <Point X="3.304837000452" Y="1.270564622505" Z="0.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.2" />
                  <Point X="3.255069084239" Y="1.340928277018" Z="0.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.2" />
                  <Point X="3.234992124776" Y="1.425563648275" Z="0.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.2" />
                  <Point X="3.25341042446" Y="1.503215023585" Z="0.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.2" />
                  <Point X="3.308319490458" Y="1.578721866831" Z="0.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.2" />
                  <Point X="3.402789282124" Y="1.653670983142" Z="0.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.2" />
                  <Point X="4.254755303433" Y="2.773363100219" Z="0.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.2" />
                  <Point X="4.020955722971" Y="3.102645385934" Z="0.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.2" />
                  <Point X="3.88400447695" Y="3.060351060067" Z="0.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.2" />
                  <Point X="2.435953552062" Y="2.247230428764" Z="0.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.2" />
                  <Point X="2.365668025627" Y="2.253237180602" Z="0.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.2" />
                  <Point X="2.301874664967" Y="2.293454109653" Z="0.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.2" />
                  <Point X="2.257304490582" Y="2.355150195415" Z="0.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.2" />
                  <Point X="2.246192522156" Y="2.424090418205" Z="0.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.2" />
                  <Point X="2.265297513893" Y="2.50351602382" Z="0.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.2" />
                  <Point X="2.335274292792" Y="2.628134605005" Z="0.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.2" />
                  <Point X="2.783223100329" Y="4.247893037832" Z="0.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.2" />
                  <Point X="2.387279314033" Y="4.482391762112" Z="0.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.2" />
                  <Point X="1.976656906481" Y="4.678048512368" Z="0.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.2" />
                  <Point X="1.550596397575" Y="4.836008169723" Z="0.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.2" />
                  <Point X="0.914395904101" Y="4.993712862095" Z="0.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.2" />
                  <Point X="0.246281117138" Y="5.070768688525" Z="0.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.2" />
                  <Point X="0.177931831835" Y="5.019175144152" Z="0.2" />
                  <Point X="0" Y="4.355124473572" Z="0.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>