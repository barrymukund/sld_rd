<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#211" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3419" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557721374512" Y="21.502859375" />
                  <Point X="0.542363830566" Y="21.532623046875" />
                  <Point X="0.390801757812" Y="21.750994140625" />
                  <Point X="0.378636199951" Y="21.7685234375" />
                  <Point X="0.356755157471" Y="21.7909765625" />
                  <Point X="0.330501342773" Y="21.810220703125" />
                  <Point X="0.302497070312" Y="21.824330078125" />
                  <Point X="0.067963447571" Y="21.897119140625" />
                  <Point X="0.04913809967" Y="21.902962890625" />
                  <Point X="0.020985534668" Y="21.907232421875" />
                  <Point X="-0.008656398773" Y="21.907234375" />
                  <Point X="-0.036822170258" Y="21.90296484375" />
                  <Point X="-0.271355957031" Y="21.830173828125" />
                  <Point X="-0.290181304932" Y="21.82433203125" />
                  <Point X="-0.31818069458" Y="21.810224609375" />
                  <Point X="-0.344437561035" Y="21.79098046875" />
                  <Point X="-0.366323150635" Y="21.7685234375" />
                  <Point X="-0.517885253906" Y="21.550150390625" />
                  <Point X="-0.53005065918" Y="21.532623046875" />
                  <Point X="-0.53818737793" Y="21.5184296875" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.580754577637" Y="21.376392578125" />
                  <Point X="-0.916584777832" Y="20.12305859375" />
                  <Point X="-1.061343139648" Y="20.15115625" />
                  <Point X="-1.079348632812" Y="20.15465234375" />
                  <Point X="-1.24641809082" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.272538818359" Y="20.76545703125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287939086914" Y="20.81703515625" />
                  <Point X="-1.304011962891" Y="20.844873046875" />
                  <Point X="-1.323645507812" Y="20.868794921875" />
                  <Point X="-1.53957421875" Y="21.05816015625" />
                  <Point X="-1.55690625" Y="21.073359375" />
                  <Point X="-1.583188720703" Y="21.089703125" />
                  <Point X="-1.612884887695" Y="21.10200390625" />
                  <Point X="-1.64302722168" Y="21.109033203125" />
                  <Point X="-1.929612915039" Y="21.12781640625" />
                  <Point X="-1.952616455078" Y="21.12932421875" />
                  <Point X="-1.983415283203" Y="21.126291015625" />
                  <Point X="-2.014462768555" Y="21.11797265625" />
                  <Point X="-2.042656494141" Y="21.10519921875" />
                  <Point X="-2.281455322266" Y="20.945638671875" />
                  <Point X="-2.300623046875" Y="20.93283203125" />
                  <Point X="-2.312785400391" Y="20.923177734375" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.351812988281" Y="20.878759765625" />
                  <Point X="-2.480147705078" Y="20.711509765625" />
                  <Point X="-2.775341796875" Y="20.894287109375" />
                  <Point X="-2.801709960938" Y="20.91061328125" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-2.423760986328" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575195312" Y="22.4148046875" />
                  <Point X="-2.414559326172" Y="22.444421875" />
                  <Point X="-2.428776367188" Y="22.473251953125" />
                  <Point X="-2.446806640625" Y="22.4984140625" />
                  <Point X="-2.462866210938" Y="22.51447265625" />
                  <Point X="-2.464155273438" Y="22.51576171875" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.639184082031" Y="22.53880078125" />
                  <Point X="-2.734990478516" Y="22.483486328125" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.062027832031" Y="22.17876953125" />
                  <Point X="-4.082861083984" Y="22.206140625" />
                  <Point X="-4.306142089844" Y="22.580548828125" />
                  <Point X="-3.105955078125" Y="23.501484375" />
                  <Point X="-3.084574951172" Y="23.524408203125" />
                  <Point X="-3.066609375" Y="23.55154296875" />
                  <Point X="-3.053855712891" Y="23.580169921875" />
                  <Point X="-3.046723632813" Y="23.60770703125" />
                  <Point X="-3.046151123047" Y="23.60991796875" />
                  <Point X="-3.043347167969" Y="23.640349609375" />
                  <Point X="-3.045557128906" Y="23.672017578125" />
                  <Point X="-3.052558837891" Y="23.70176171875" />
                  <Point X="-3.068640380859" Y="23.7277421875" />
                  <Point X="-3.089471679688" Y="23.751697265625" />
                  <Point X="-3.112973144531" Y="23.771232421875" />
                  <Point X="-3.137488037109" Y="23.78566015625" />
                  <Point X="-3.139455810547" Y="23.786818359375" />
                  <Point X="-3.168715820312" Y="23.798041015625" />
                  <Point X="-3.200604492188" Y="23.8045234375" />
                  <Point X="-3.231928466797" Y="23.805615234375" />
                  <Point X="-3.352875" Y="23.789693359375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.825928710938" Y="23.975443359375" />
                  <Point X="-4.834076660156" Y="24.00734375" />
                  <Point X="-4.892424316406" Y="24.41530078125" />
                  <Point X="-3.532875" Y="24.779591796875" />
                  <Point X="-3.517490234375" Y="24.785171875" />
                  <Point X="-3.487730712891" Y="24.800529296875" />
                  <Point X="-3.462040039062" Y="24.818359375" />
                  <Point X="-3.459954345703" Y="24.819806640625" />
                  <Point X="-3.437512695312" Y="24.84168359375" />
                  <Point X="-3.418272216797" Y="24.8679375" />
                  <Point X="-3.404167480469" Y="24.89593359375" />
                  <Point X="-3.395603759766" Y="24.923525390625" />
                  <Point X="-3.394916503906" Y="24.925740234375" />
                  <Point X="-3.390646972656" Y="24.953900390625" />
                  <Point X="-3.390646972656" Y="24.983537109375" />
                  <Point X="-3.394916015625" Y="25.0116953125" />
                  <Point X="-3.403479736328" Y="25.0392890625" />
                  <Point X="-3.404166992188" Y="25.04150390625" />
                  <Point X="-3.418275390625" Y="25.069505859375" />
                  <Point X="-3.437520996094" Y="25.095763671875" />
                  <Point X="-3.459977783203" Y="25.1176484375" />
                  <Point X="-3.485668457031" Y="25.135478515625" />
                  <Point X="-3.497978759766" Y="25.142716796875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-3.643124511719" Y="25.187390625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.82973828125" Y="25.941494140625" />
                  <Point X="-4.824487792969" Y="25.9769765625" />
                  <Point X="-4.703551757812" Y="26.423267578125" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744986572266" Y="26.299341796875" />
                  <Point X="-3.723421386719" Y="26.301228515625" />
                  <Point X="-3.703135253906" Y="26.305263671875" />
                  <Point X="-3.6462734375" Y="26.32319140625" />
                  <Point X="-3.641709228516" Y="26.324630859375" />
                  <Point X="-3.622765869141" Y="26.332966796875" />
                  <Point X="-3.604022705078" Y="26.343791015625" />
                  <Point X="-3.587344238281" Y="26.3560234375" />
                  <Point X="-3.573708496094" Y="26.37157421875" />
                  <Point X="-3.561295410156" Y="26.3893046875" />
                  <Point X="-3.551350585938" Y="26.40743359375" />
                  <Point X="-3.528534423828" Y="26.462515625" />
                  <Point X="-3.526703125" Y="26.4669375" />
                  <Point X="-3.520913818359" Y="26.48680078125" />
                  <Point X="-3.517156738281" Y="26.508115234375" />
                  <Point X="-3.515804931641" Y="26.528755859375" />
                  <Point X="-3.518952636719" Y="26.549201171875" />
                  <Point X="-3.524555664062" Y="26.570107421875" />
                  <Point X="-3.532051269531" Y="26.58937890625" />
                  <Point X="-3.559581298828" Y="26.642263671875" />
                  <Point X="-3.561791015625" Y="26.6465078125" />
                  <Point X="-3.573283935547" Y="26.663708984375" />
                  <Point X="-3.587195068359" Y="26.680287109375" />
                  <Point X="-3.602135498047" Y="26.69458984375" />
                  <Point X="-3.665374267578" Y="26.743115234375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.101551757813" Y="27.6987109375" />
                  <Point X="-4.081151123047" Y="27.733662109375" />
                  <Point X="-3.750504882813" Y="28.158662109375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.1877265625" Y="27.836341796875" />
                  <Point X="-3.167088378906" Y="27.829833984375" />
                  <Point X="-3.146797607422" Y="27.825796875" />
                  <Point X="-3.067604736328" Y="27.818869140625" />
                  <Point X="-3.061248291016" Y="27.8183125" />
                  <Point X="-3.040582519531" Y="27.81876171875" />
                  <Point X="-3.019122314453" Y="27.821583984375" />
                  <Point X="-2.999022705078" Y="27.8265" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962208251953" Y="27.847283203125" />
                  <Point X="-2.946078369141" Y="27.860228515625" />
                  <Point X="-2.889866699219" Y="27.916439453125" />
                  <Point X="-2.885354736328" Y="27.920951171875" />
                  <Point X="-2.872409179688" Y="27.937080078125" />
                  <Point X="-2.860779296875" Y="27.955333984375" />
                  <Point X="-2.851629638672" Y="27.97388671875" />
                  <Point X="-2.846712646484" Y="27.99398046875" />
                  <Point X="-2.843887207031" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.850364257812" Y="28.1153125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.897818359375" Y="28.2300703125" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.7361484375" Y="29.067447265625" />
                  <Point X="-2.700622070312" Y="29.09468359375" />
                  <Point X="-2.167035888672" Y="29.3911328125" />
                  <Point X="-2.043194946289" Y="29.2297421875" />
                  <Point X="-2.028897949219" Y="29.2148046875" />
                  <Point X="-2.012321289063" Y="29.200892578125" />
                  <Point X="-1.99511706543" Y="29.189396484375" />
                  <Point X="-1.906975830078" Y="29.14351171875" />
                  <Point X="-1.899901000977" Y="29.139828125" />
                  <Point X="-1.880625366211" Y="29.13233203125" />
                  <Point X="-1.859716186523" Y="29.126728515625" />
                  <Point X="-1.839268676758" Y="29.123580078125" />
                  <Point X="-1.818624633789" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.777451293945" Y="29.134482421875" />
                  <Point X="-1.685646118164" Y="29.172509765625" />
                  <Point X="-1.67827722168" Y="29.1755625" />
                  <Point X="-1.660135253906" Y="29.185515625" />
                  <Point X="-1.642406982422" Y="29.197931640625" />
                  <Point X="-1.626860229492" Y="29.211568359375" />
                  <Point X="-1.614632446289" Y="29.22824609375" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.565599365234" Y="29.36069140625" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165405273" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773046875" Y="29.430826171875" />
                  <Point X="-1.560916503906" Y="29.455025390625" />
                  <Point X="-1.584202026367" Y="29.631896484375" />
                  <Point X="-0.995622314453" Y="29.7969140625" />
                  <Point X="-0.949624938965" Y="29.80980859375" />
                  <Point X="-0.294711364746" Y="29.886458984375" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271453857" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155910969" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.160573959351" Y="29.33990234375" />
                  <Point X="0.307419555664" Y="29.8879375" />
                  <Point X="0.803879150391" Y="29.8359453125" />
                  <Point X="0.844029602051" Y="29.831740234375" />
                  <Point X="1.440255371094" Y="29.68779296875" />
                  <Point X="1.481040283203" Y="29.6779453125" />
                  <Point X="1.868997192383" Y="29.53723046875" />
                  <Point X="1.894630981445" Y="29.52793359375" />
                  <Point X="2.269900146484" Y="29.352431640625" />
                  <Point X="2.294575439453" Y="29.340892578125" />
                  <Point X="2.657108642578" Y="29.1296796875" />
                  <Point X="2.680989746094" Y="29.115767578125" />
                  <Point X="2.943260498047" Y="28.929255859375" />
                  <Point X="2.147580810547" Y="27.55109765625" />
                  <Point X="2.142075683594" Y="27.5399296875" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.113202392578" Y="27.441734375" />
                  <Point X="2.110631591797" Y="27.428322265625" />
                  <Point X="2.107698486328" Y="27.403455078125" />
                  <Point X="2.107727783203" Y="27.380953125" />
                  <Point X="2.115477294922" Y="27.3166875" />
                  <Point X="2.116099121094" Y="27.311529296875" />
                  <Point X="2.121442871094" Y="27.289603515625" />
                  <Point X="2.129711669922" Y="27.267509765625" />
                  <Point X="2.140072998047" Y="27.24746875" />
                  <Point X="2.179838867188" Y="27.18886328125" />
                  <Point X="2.188400390625" Y="27.17803125" />
                  <Point X="2.204889892578" Y="27.16003125" />
                  <Point X="2.221599365234" Y="27.145591796875" />
                  <Point X="2.280196044922" Y="27.10583203125" />
                  <Point X="2.284876953125" Y="27.102654296875" />
                  <Point X="2.304937255859" Y="27.092279296875" />
                  <Point X="2.327035400391" Y="27.0840078125" />
                  <Point X="2.348967041016" Y="27.078662109375" />
                  <Point X="2.413233398438" Y="27.0709140625" />
                  <Point X="2.427425537109" Y="27.070271484375" />
                  <Point X="2.451488525391" Y="27.070986328125" />
                  <Point X="2.473210205078" Y="27.074169921875" />
                  <Point X="2.547531005859" Y="27.094044921875" />
                  <Point X="2.555609375" Y="27.096595703125" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="2.699422607422" Y="27.17416796875" />
                  <Point X="3.967325195312" Y="27.90619140625" />
                  <Point X="4.109111816406" Y="27.709140625" />
                  <Point X="4.123275878906" Y="27.689455078125" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.23078515625" Y="26.668451171875" />
                  <Point X="3.221419677734" Y="26.660236328125" />
                  <Point X="3.203972900391" Y="26.641625" />
                  <Point X="3.150484130859" Y="26.571845703125" />
                  <Point X="3.143202392578" Y="26.56083984375" />
                  <Point X="3.130440673828" Y="26.5382890625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.101705322266" Y="26.44583984375" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.114095458984" Y="26.292498046875" />
                  <Point X="3.117707763672" Y="26.279638671875" />
                  <Point X="3.126218017578" Y="26.2558984375" />
                  <Point X="3.136282226562" Y="26.235740234375" />
                  <Point X="3.180769042969" Y="26.168123046875" />
                  <Point X="3.18433984375" Y="26.1626953125" />
                  <Point X="3.198894287109" Y="26.14544921875" />
                  <Point X="3.216138916016" Y="26.129359375" />
                  <Point X="3.23434765625" Y="26.11603515625" />
                  <Point X="3.298814941406" Y="26.07974609375" />
                  <Point X="3.31128515625" Y="26.073873046875" />
                  <Point X="3.334435546875" Y="26.0649609375" />
                  <Point X="3.356118896484" Y="26.0594375" />
                  <Point X="3.443283203125" Y="26.04791796875" />
                  <Point X="3.451343994141" Y="26.047201171875" />
                  <Point X="3.471417724609" Y="26.0462734375" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.593540039062" Y="26.0608515625" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.839854492188" Y="25.957787109375" />
                  <Point X="4.845937011719" Y="25.9328046875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.716580810547" Y="25.32958984375" />
                  <Point X="3.704795166016" Y="25.325587890625" />
                  <Point X="3.681549072266" Y="25.3150703125" />
                  <Point X="3.595912841797" Y="25.265572265625" />
                  <Point X="3.585315429688" Y="25.258455078125" />
                  <Point X="3.564125732422" Y="25.24205859375" />
                  <Point X="3.547529296875" Y="25.225576171875" />
                  <Point X="3.496147705078" Y="25.160103515625" />
                  <Point X="3.4920234375" Y="25.15484765625" />
                  <Point X="3.480296875" Y="25.1355625" />
                  <Point X="3.470524169922" Y="25.11409765625" />
                  <Point X="3.463680664062" Y="25.0926015625" />
                  <Point X="3.446553222656" Y="25.003169921875" />
                  <Point X="3.444989990234" Y="24.990318359375" />
                  <Point X="3.443615722656" Y="24.964330078125" />
                  <Point X="3.445178955078" Y="24.941443359375" />
                  <Point X="3.462306396484" Y="24.85201171875" />
                  <Point X="3.463681152344" Y="24.844833984375" />
                  <Point X="3.470526611328" Y="24.823333984375" />
                  <Point X="3.480298828125" Y="24.801873046875" />
                  <Point X="3.4920234375" Y="24.782591796875" />
                  <Point X="3.543405029297" Y="24.717119140625" />
                  <Point X="3.552393066406" Y="24.7071953125" />
                  <Point X="3.570831298828" Y="24.689517578125" />
                  <Point X="3.589036865234" Y="24.67584375" />
                  <Point X="3.674665771484" Y="24.626349609375" />
                  <Point X="3.681520996094" Y="24.62275" />
                  <Point X="3.700482421875" Y="24.613763671875" />
                  <Point X="3.716580566406" Y="24.60784765625" />
                  <Point X="3.813179199219" Y="24.58196484375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.858418457031" Y="24.073796875" />
                  <Point X="4.855021484375" Y="24.051265625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.424381103516" Y="23.99655859375" />
                  <Point X="3.40803515625" Y="23.9972890625" />
                  <Point X="3.374660644531" Y="23.994490234375" />
                  <Point X="3.206587158203" Y="23.957958984375" />
                  <Point X="3.193096435547" Y="23.95502734375" />
                  <Point X="3.163981445312" Y="23.94340625" />
                  <Point X="3.136153564453" Y="23.926515625" />
                  <Point X="3.112397949219" Y="23.906041015625" />
                  <Point X="3.010808105469" Y="23.783859375" />
                  <Point X="3.002653808594" Y="23.774052734375" />
                  <Point X="2.987933349609" Y="23.749671875" />
                  <Point X="2.976589355469" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.955197265625" Y="23.536404296875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956346923828" Y="23.492435546875" />
                  <Point X="2.964077880859" Y="23.460814453125" />
                  <Point X="2.976449951172" Y="23.432001953125" />
                  <Point X="3.069464355469" Y="23.287326171875" />
                  <Point X="3.074732421875" Y="23.27993359375" />
                  <Point X="3.093818847656" Y="23.25569140625" />
                  <Point X="3.110627929688" Y="23.23908984375" />
                  <Point X="3.200272216797" Y="23.170302734375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.134381347656" Y="22.265701171875" />
                  <Point X="4.124810546875" Y="22.250212890625" />
                  <Point X="4.028981445312" Y="22.1140546875" />
                  <Point X="2.800953857422" Y="22.823056640625" />
                  <Point X="2.786131835938" Y="22.829986328125" />
                  <Point X="2.754221191406" Y="22.84017578125" />
                  <Point X="2.554187011719" Y="22.876302734375" />
                  <Point X="2.538130859375" Y="22.879201171875" />
                  <Point X="2.506772216797" Y="22.879603515625" />
                  <Point X="2.474604736328" Y="22.87464453125" />
                  <Point X="2.444834716797" Y="22.864822265625" />
                  <Point X="2.278655273438" Y="22.77736328125" />
                  <Point X="2.265316650391" Y="22.77034375" />
                  <Point X="2.242388671875" Y="22.753453125" />
                  <Point X="2.221427978516" Y="22.732494140625" />
                  <Point X="2.204532226563" Y="22.709560546875" />
                  <Point X="2.117073242188" Y="22.543380859375" />
                  <Point X="2.110053222656" Y="22.53004296875" />
                  <Point X="2.100229980469" Y="22.50026953125" />
                  <Point X="2.095271728516" Y="22.46809765625" />
                  <Point X="2.095675537109" Y="22.436744140625" />
                  <Point X="2.131801513672" Y="22.236708984375" />
                  <Point X="2.133656982422" Y="22.228521484375" />
                  <Point X="2.142459472656" Y="22.196349609375" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.209424560547" Y="22.07414453125" />
                  <Point X="2.861283935547" Y="20.94509375" />
                  <Point X="2.793211914063" Y="20.896470703125" />
                  <Point X="2.781858886719" Y="20.888361328125" />
                  <Point X="2.701763427734" Y="20.836517578125" />
                  <Point X="1.758545776367" Y="22.065744140625" />
                  <Point X="1.747500976562" Y="22.077822265625" />
                  <Point X="1.721922119141" Y="22.099443359375" />
                  <Point X="1.524634155273" Y="22.22628125" />
                  <Point X="1.508798461914" Y="22.2364609375" />
                  <Point X="1.479985473633" Y="22.24883203125" />
                  <Point X="1.448366577148" Y="22.2565625" />
                  <Point X="1.417100219727" Y="22.258880859375" />
                  <Point X="1.201332641602" Y="22.23902734375" />
                  <Point X="1.184013305664" Y="22.23743359375" />
                  <Point X="1.156359130859" Y="22.2306015625" />
                  <Point X="1.128971435547" Y="22.219255859375" />
                  <Point X="1.104593139648" Y="22.204537109375" />
                  <Point X="0.93798260498" Y="22.066005859375" />
                  <Point X="0.924609436035" Y="22.054884765625" />
                  <Point X="0.904137145996" Y="22.031130859375" />
                  <Point X="0.887247436523" Y="22.003306640625" />
                  <Point X="0.875624267578" Y="21.974189453125" />
                  <Point X="0.82580859375" Y="21.745" />
                  <Point X="0.82446496582" Y="21.737306640625" />
                  <Point X="0.819753540039" Y="21.701765625" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.836067077637" Y="21.55287890625" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.986423339844" Y="20.132271484375" />
                  <Point X="0.975711547852" Y="20.129923828125" />
                  <Point X="0.929315429688" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.043241455078" Y="20.244416015625" />
                  <Point X="-1.05844152832" Y="20.2473671875" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.179364379883" Y="20.783990234375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.199027832031" Y="20.850498046875" />
                  <Point X="-1.205667480469" Y="20.864537109375" />
                  <Point X="-1.221740234375" Y="20.892375" />
                  <Point X="-1.23057800293" Y="20.905142578125" />
                  <Point X="-1.250211547852" Y="20.929064453125" />
                  <Point X="-1.261007446289" Y="20.94021875" />
                  <Point X="-1.476936157227" Y="21.129583984375" />
                  <Point X="-1.494268188477" Y="21.144783203125" />
                  <Point X="-1.506739257812" Y="21.154033203125" />
                  <Point X="-1.533021728516" Y="21.170376953125" />
                  <Point X="-1.546833251953" Y="21.177470703125" />
                  <Point X="-1.576529418945" Y="21.189771484375" />
                  <Point X="-1.591309448242" Y="21.194521484375" />
                  <Point X="-1.621451782227" Y="21.20155078125" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.923399902344" Y="21.22261328125" />
                  <Point X="-1.946403198242" Y="21.22412109375" />
                  <Point X="-1.961927368164" Y="21.2238671875" />
                  <Point X="-1.992726196289" Y="21.220833984375" />
                  <Point X="-2.008000854492" Y="21.2180546875" />
                  <Point X="-2.039048583984" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861083984" Y="21.191732421875" />
                  <Point X="-2.095435791016" Y="21.184189453125" />
                  <Point X="-2.334234619141" Y="21.02462890625" />
                  <Point X="-2.35340234375" Y="21.011822265625" />
                  <Point X="-2.359686767578" Y="21.007240234375" />
                  <Point X="-2.380442138672" Y="20.9898671875" />
                  <Point X="-2.402759277344" Y="20.9672265625" />
                  <Point X="-2.410470947266" Y="20.958369140625" />
                  <Point X="-2.427181396484" Y="20.936591796875" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.725330566406" Y="20.97505859375" />
                  <Point X="-2.7475859375" Y="20.988837890625" />
                  <Point X="-2.980862304688" Y="21.168453125" />
                  <Point X="-2.341488525391" Y="22.275880859375" />
                  <Point X="-2.334849365234" Y="22.28991796875" />
                  <Point X="-2.323947509766" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665771484" Y="22.442380859375" />
                  <Point X="-2.323649902344" Y="22.471998046875" />
                  <Point X="-2.329355957031" Y="22.4864375" />
                  <Point X="-2.343572998047" Y="22.515267578125" />
                  <Point X="-2.351554931641" Y="22.5285859375" />
                  <Point X="-2.369585205078" Y="22.553748046875" />
                  <Point X="-2.379633544922" Y="22.565591796875" />
                  <Point X="-2.395693115234" Y="22.581650390625" />
                  <Point X="-2.408821777344" Y="22.592984375" />
                  <Point X="-2.433977539062" Y="22.611009765625" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.64368359375" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.686684570312" Y="22.621072265625" />
                  <Point X="-2.782490966797" Y="22.5657578125" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.986434326172" Y="22.23630859375" />
                  <Point X="-4.004020019531" Y="22.259412109375" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.048122802734" Y="23.426115234375" />
                  <Point X="-3.036481445312" Y="23.436689453125" />
                  <Point X="-3.015101318359" Y="23.45961328125" />
                  <Point X="-3.005363037109" Y="23.471962890625" />
                  <Point X="-2.987397460938" Y="23.49909765625" />
                  <Point X="-2.979831787109" Y="23.5128828125" />
                  <Point X="-2.967078125" Y="23.541509765625" />
                  <Point X="-2.961890136719" Y="23.5563515625" />
                  <Point X="-2.954758056641" Y="23.583888671875" />
                  <Point X="-2.951551757812" Y="23.601201171875" />
                  <Point X="-2.948747802734" Y="23.6316328125" />
                  <Point X="-2.948577636719" Y="23.646962890625" />
                  <Point X="-2.950787597656" Y="23.678630859375" />
                  <Point X="-2.953084716797" Y="23.69378515625" />
                  <Point X="-2.960086425781" Y="23.723529296875" />
                  <Point X="-2.971781494141" Y="23.75176171875" />
                  <Point X="-2.987863037109" Y="23.7777421875" />
                  <Point X="-2.996954101562" Y="23.790080078125" />
                  <Point X="-3.017785400391" Y="23.81403515625" />
                  <Point X="-3.028744873047" Y="23.82475390625" />
                  <Point X="-3.052246337891" Y="23.8442890625" />
                  <Point X="-3.064788330078" Y="23.85310546875" />
                  <Point X="-3.089303222656" Y="23.867533203125" />
                  <Point X="-3.105435302734" Y="23.875517578125" />
                  <Point X="-3.1346953125" Y="23.886740234375" />
                  <Point X="-3.149791015625" Y="23.89113671875" />
                  <Point X="-3.1816796875" Y="23.897619140625" />
                  <Point X="-3.197295166016" Y="23.89946484375" />
                  <Point X="-3.228619140625" Y="23.900556640625" />
                  <Point X="-3.244327636719" Y="23.899802734375" />
                  <Point X="-3.365274169922" Y="23.883880859375" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.733883789062" Y="23.998955078125" />
                  <Point X="-4.740761230469" Y="24.025880859375" />
                  <Point X="-4.786452148438" Y="24.34534375" />
                  <Point X="-3.508287109375" Y="24.687828125" />
                  <Point X="-3.500483154297" Y="24.69028515625" />
                  <Point X="-3.473924316406" Y="24.70075" />
                  <Point X="-3.444164794922" Y="24.716107421875" />
                  <Point X="-3.433564941406" Y="24.722484375" />
                  <Point X="-3.407874267578" Y="24.740314453125" />
                  <Point X="-3.393640625" Y="24.75178125" />
                  <Point X="-3.371198974609" Y="24.773658203125" />
                  <Point X="-3.360886962891" Y="24.78552734375" />
                  <Point X="-3.341646484375" Y="24.81178125" />
                  <Point X="-3.333431396484" Y="24.825193359375" />
                  <Point X="-3.319326660156" Y="24.853189453125" />
                  <Point X="-3.313437011719" Y="24.8677734375" />
                  <Point X="-3.304873291016" Y="24.895365234375" />
                  <Point X="-3.300989990234" Y="24.9115" />
                  <Point X="-3.296720458984" Y="24.93966015625" />
                  <Point X="-3.295646972656" Y="24.953900390625" />
                  <Point X="-3.295646972656" Y="24.983537109375" />
                  <Point X="-3.296720214844" Y="24.99777734375" />
                  <Point X="-3.300989257812" Y="25.025935546875" />
                  <Point X="-3.304185058594" Y="25.039853515625" />
                  <Point X="-3.312748779297" Y="25.067447265625" />
                  <Point X="-3.319326904297" Y="25.08425" />
                  <Point X="-3.333435302734" Y="25.112251953125" />
                  <Point X="-3.341652832031" Y="25.125666015625" />
                  <Point X="-3.3608984375" Y="25.151923828125" />
                  <Point X="-3.371218017578" Y="25.163798828125" />
                  <Point X="-3.393674804688" Y="25.18568359375" />
                  <Point X="-3.405812011719" Y="25.195693359375" />
                  <Point X="-3.431502685547" Y="25.2135234375" />
                  <Point X="-3.437516845703" Y="25.21737109375" />
                  <Point X="-3.46018359375" Y="25.229875" />
                  <Point X="-3.495080810547" Y="25.2450078125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-3.618536621094" Y="25.279154296875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.73576171875" Y="25.927587890625" />
                  <Point X="-4.731330566406" Y="25.957533203125" />
                  <Point X="-4.633586914062" Y="26.318236328125" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.74705859375" Y="26.204365234375" />
                  <Point X="-3.736706787109" Y="26.204703125" />
                  <Point X="-3.715141601563" Y="26.20658984375" />
                  <Point X="-3.704887939453" Y="26.2080546875" />
                  <Point X="-3.684601806641" Y="26.21208984375" />
                  <Point X="-3.674569335938" Y="26.21466015625" />
                  <Point X="-3.617707519531" Y="26.232587890625" />
                  <Point X="-3.603445800781" Y="26.237677734375" />
                  <Point X="-3.584502441406" Y="26.246013671875" />
                  <Point X="-3.575256591797" Y="26.25069921875" />
                  <Point X="-3.556513427734" Y="26.2615234375" />
                  <Point X="-3.547838623047" Y="26.267185546875" />
                  <Point X="-3.53116015625" Y="26.27941796875" />
                  <Point X="-3.515915039062" Y="26.293390625" />
                  <Point X="-3.502279296875" Y="26.30894140625" />
                  <Point X="-3.495885009766" Y="26.31708984375" />
                  <Point X="-3.483471923828" Y="26.3348203125" />
                  <Point X="-3.478004394531" Y="26.343615234375" />
                  <Point X="-3.468059570312" Y="26.361744140625" />
                  <Point X="-3.463582275391" Y="26.371078125" />
                  <Point X="-3.440766113281" Y="26.42616015625" />
                  <Point X="-3.435498046875" Y="26.44035546875" />
                  <Point X="-3.429708740234" Y="26.46021875" />
                  <Point X="-3.427356201172" Y="26.47030859375" />
                  <Point X="-3.423599121094" Y="26.491623046875" />
                  <Point X="-3.422359863281" Y="26.50190625" />
                  <Point X="-3.421008056641" Y="26.522546875" />
                  <Point X="-3.421911132813" Y="26.5432109375" />
                  <Point X="-3.425058837891" Y="26.56365625" />
                  <Point X="-3.427190917969" Y="26.573794921875" />
                  <Point X="-3.432793945312" Y="26.594701171875" />
                  <Point X="-3.436017089844" Y="26.604544921875" />
                  <Point X="-3.443512695312" Y="26.62381640625" />
                  <Point X="-3.44778515625" Y="26.633244140625" />
                  <Point X="-3.475315185547" Y="26.68612890625" />
                  <Point X="-3.482800292969" Y="26.69928515625" />
                  <Point X="-3.494293212891" Y="26.716486328125" />
                  <Point X="-3.500510742188" Y="26.724775390625" />
                  <Point X="-3.514421875" Y="26.741353515625" />
                  <Point X="-3.521500488281" Y="26.74891015625" />
                  <Point X="-3.536440917969" Y="26.763212890625" />
                  <Point X="-3.544302734375" Y="26.769958984375" />
                  <Point X="-3.607541503906" Y="26.818484375" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.019505371094" Y="27.650822265625" />
                  <Point X="-4.002291015625" Y="27.680314453125" />
                  <Point X="-3.726338134766" Y="28.035013671875" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244919921875" Y="27.75771875" />
                  <Point X="-3.225989501953" Y="27.749388671875" />
                  <Point X="-3.216296142578" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.739232421875" />
                  <Point X="-3.185626464844" Y="27.73666015625" />
                  <Point X="-3.165335693359" Y="27.732623046875" />
                  <Point X="-3.155076416016" Y="27.731158203125" />
                  <Point X="-3.075883544922" Y="27.72423046875" />
                  <Point X="-3.059183837891" Y="27.723333984375" />
                  <Point X="-3.038518066406" Y="27.723783203125" />
                  <Point X="-3.028195556641" Y="27.724572265625" />
                  <Point X="-3.006735351562" Y="27.72739453125" />
                  <Point X="-2.996552246094" Y="27.7293046875" />
                  <Point X="-2.976452636719" Y="27.734220703125" />
                  <Point X="-2.95700390625" Y="27.741296875" />
                  <Point X="-2.938445556641" Y="27.75044921875" />
                  <Point X="-2.929419433594" Y="27.75553125" />
                  <Point X="-2.911163330078" Y="27.767162109375" />
                  <Point X="-2.902746337891" Y="27.773193359375" />
                  <Point X="-2.886616455078" Y="27.786138671875" />
                  <Point X="-2.878903564453" Y="27.793052734375" />
                  <Point X="-2.822691894531" Y="27.849263671875" />
                  <Point X="-2.811267333984" Y="27.861486328125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288818359" Y="27.886033203125" />
                  <Point X="-2.780658935547" Y="27.904287109375" />
                  <Point X="-2.775577148438" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759352294922" Y="27.951306640625" />
                  <Point X="-2.754435302734" Y="27.971400390625" />
                  <Point X="-2.752525634766" Y="27.981580078125" />
                  <Point X="-2.749700195312" Y="28.0030390625" />
                  <Point X="-2.748909912109" Y="28.013365234375" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.755725830078" Y="28.123591796875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.815546142578" Y="28.2775703125" />
                  <Point X="-3.059386962891" Y="28.6999140625" />
                  <Point X="-2.678346191406" Y="28.9920546875" />
                  <Point X="-2.64837109375" Y="29.01503515625" />
                  <Point X="-2.192522705078" Y="29.268294921875" />
                  <Point X="-2.118563232422" Y="29.17191015625" />
                  <Point X="-2.111825439453" Y="29.1640546875" />
                  <Point X="-2.097528320313" Y="29.1491171875" />
                  <Point X="-2.089969482422" Y="29.142037109375" />
                  <Point X="-2.073392822266" Y="29.128125" />
                  <Point X="-2.065102294922" Y="29.121904296875" />
                  <Point X="-2.047898193359" Y="29.110408203125" />
                  <Point X="-2.038984375" Y="29.105130859375" />
                  <Point X="-1.950843017578" Y="29.05924609375" />
                  <Point X="-1.934333496094" Y="29.051287109375" />
                  <Point X="-1.915057739258" Y="29.043791015625" />
                  <Point X="-1.905216918945" Y="29.0405703125" />
                  <Point X="-1.884307739258" Y="29.034966796875" />
                  <Point X="-1.874173583984" Y="29.0328359375" />
                  <Point X="-1.853726074219" Y="29.0296875" />
                  <Point X="-1.833053344727" Y="29.028783203125" />
                  <Point X="-1.812409301758" Y="29.03013671875" />
                  <Point X="-1.802124633789" Y="29.031376953125" />
                  <Point X="-1.780806640625" Y="29.03513671875" />
                  <Point X="-1.770715209961" Y="29.037490234375" />
                  <Point X="-1.750859985352" Y="29.043279296875" />
                  <Point X="-1.741096069336" Y="29.04671484375" />
                  <Point X="-1.649290893555" Y="29.0847421875" />
                  <Point X="-1.632582885742" Y="29.0922734375" />
                  <Point X="-1.614440917969" Y="29.1022265625" />
                  <Point X="-1.605637939453" Y="29.107701171875" />
                  <Point X="-1.587909667969" Y="29.1201171875" />
                  <Point X="-1.579762451172" Y="29.126513671875" />
                  <Point X="-1.564215698242" Y="29.140150390625" />
                  <Point X="-1.55024621582" Y="29.155396484375" />
                  <Point X="-1.538018432617" Y="29.17207421875" />
                  <Point X="-1.532360717773" Y="29.180744140625" />
                  <Point X="-1.521539550781" Y="29.199486328125" />
                  <Point X="-1.516857055664" Y="29.2087265625" />
                  <Point X="-1.508525878906" Y="29.22766015625" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.47499621582" Y="29.332125" />
                  <Point X="-1.470026245117" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464526733398" Y="29.380306640625" />
                  <Point X="-1.462640625" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462753173828" Y="29.4328984375" />
                  <Point X="-1.463543334961" Y="29.4432265625" />
                  <Point X="-1.466729370117" Y="29.46742578125" />
                  <Point X="-1.479266357422" Y="29.562654296875" />
                  <Point X="-0.969976501465" Y="29.70544140625" />
                  <Point X="-0.931167236328" Y="29.7163203125" />
                  <Point X="-0.365222625732" Y="29.78255859375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.22043510437" Y="29.247107421875" />
                  <Point X="-0.207661560059" Y="29.218916015625" />
                  <Point X="-0.200119094849" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600669861" Y="29.11043359375" />
                  <Point X="-0.085354125977" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.00931760788" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227790833" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912307739" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763122559" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.1945730896" Y="29.1786171875" />
                  <Point X="0.21243132019" Y="29.20534375" />
                  <Point X="0.219973648071" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978118896" Y="29.2617265625" />
                  <Point X="0.252336868286" Y="29.315314453125" />
                  <Point X="0.378190765381" Y="29.7850078125" />
                  <Point X="0.79398425293" Y="29.741462890625" />
                  <Point X="0.827849487305" Y="29.737916015625" />
                  <Point X="1.417960083008" Y="29.5954453125" />
                  <Point X="1.45362109375" Y="29.586833984375" />
                  <Point X="1.836604858398" Y="29.447923828125" />
                  <Point X="1.858232299805" Y="29.440080078125" />
                  <Point X="2.229655273438" Y="29.266376953125" />
                  <Point X="2.25044921875" Y="29.25665234375" />
                  <Point X="2.609285644531" Y="29.04759375" />
                  <Point X="2.629449462891" Y="29.03584765625" />
                  <Point X="2.817780029297" Y="28.90191796875" />
                  <Point X="2.065308349609" Y="27.59859765625" />
                  <Point X="2.062370849609" Y="27.5931015625" />
                  <Point X="2.053180664062" Y="27.573435546875" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.021427124023" Y="27.466275390625" />
                  <Point X="2.019900878906" Y="27.459619140625" />
                  <Point X="2.016285644531" Y="27.439451171875" />
                  <Point X="2.013352539063" Y="27.414583984375" />
                  <Point X="2.012698608398" Y="27.40333203125" />
                  <Point X="2.012727905273" Y="27.380830078125" />
                  <Point X="2.013410888672" Y="27.369580078125" />
                  <Point X="2.021160644531" Y="27.305314453125" />
                  <Point X="2.02380078125" Y="27.28903515625" />
                  <Point X="2.02914440918" Y="27.267109375" />
                  <Point X="2.032469848633" Y="27.2563046875" />
                  <Point X="2.040738891602" Y="27.2342109375" />
                  <Point X="2.045322753906" Y="27.223880859375" />
                  <Point X="2.055684326172" Y="27.20383984375" />
                  <Point X="2.061461425781" Y="27.19412890625" />
                  <Point X="2.101227294922" Y="27.1355234375" />
                  <Point X="2.105308105469" Y="27.129955078125" />
                  <Point X="2.118350341797" Y="27.113859375" />
                  <Point X="2.13483984375" Y="27.095859375" />
                  <Point X="2.142774902344" Y="27.088150390625" />
                  <Point X="2.159484375" Y="27.0737109375" />
                  <Point X="2.168258789062" Y="27.06698046875" />
                  <Point X="2.226837402344" Y="27.027232421875" />
                  <Point X="2.241235107422" Y="27.018271484375" />
                  <Point X="2.261295410156" Y="27.007896484375" />
                  <Point X="2.271634521484" Y="27.00330859375" />
                  <Point X="2.293732666016" Y="26.995037109375" />
                  <Point X="2.304538330078" Y="26.9917109375" />
                  <Point X="2.326469970703" Y="26.986365234375" />
                  <Point X="2.337595947266" Y="26.984345703125" />
                  <Point X="2.401862304688" Y="26.97659765625" />
                  <Point X="2.408936523438" Y="26.97601171875" />
                  <Point X="2.430246582031" Y="26.9753125" />
                  <Point X="2.454309570312" Y="26.97602734375" />
                  <Point X="2.465264892578" Y="26.976990234375" />
                  <Point X="2.486986572266" Y="26.980173828125" />
                  <Point X="2.497752929688" Y="26.98239453125" />
                  <Point X="2.572073730469" Y="27.00226953125" />
                  <Point X="2.591766113281" Y="27.00874609375" />
                  <Point X="2.624690673828" Y="27.022296875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="2.746922607422" Y="27.091896484375" />
                  <Point X="3.940402832031" Y="27.780953125" />
                  <Point X="4.031999267578" Y="27.653654296875" />
                  <Point X="4.043958496094" Y="27.637033203125" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.172952880859" Y="26.7438203125" />
                  <Point X="3.168140625" Y="26.73987109375" />
                  <Point X="3.152111083984" Y="26.725208984375" />
                  <Point X="3.134664306641" Y="26.70659765625" />
                  <Point X="3.128575683594" Y="26.699419921875" />
                  <Point X="3.075086914062" Y="26.629640625" />
                  <Point X="3.0605234375" Y="26.60762890625" />
                  <Point X="3.04776171875" Y="26.585078125" />
                  <Point X="3.042713378906" Y="26.574744140625" />
                  <Point X="3.033902587891" Y="26.553541015625" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.010215576172" Y="26.47142578125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.021055419922" Y="26.27330078125" />
                  <Point X="3.022635498047" Y="26.266806640625" />
                  <Point X="3.028280029297" Y="26.24758203125" />
                  <Point X="3.036790283203" Y="26.223841796875" />
                  <Point X="3.041222412109" Y="26.213462890625" />
                  <Point X="3.051286621094" Y="26.1933046875" />
                  <Point X="3.056918701172" Y="26.183525390625" />
                  <Point X="3.101405517578" Y="26.115908203125" />
                  <Point X="3.111738525391" Y="26.10142578125" />
                  <Point X="3.12629296875" Y="26.0841796875" />
                  <Point X="3.134085205078" Y="26.07598828125" />
                  <Point X="3.151329833984" Y="26.0598984375" />
                  <Point X="3.160038330078" Y="26.052693359375" />
                  <Point X="3.178247070312" Y="26.039369140625" />
                  <Point X="3.187747314453" Y="26.03325" />
                  <Point X="3.252214599609" Y="25.9969609375" />
                  <Point X="3.258337646484" Y="25.99380078125" />
                  <Point X="3.277155029297" Y="25.98521484375" />
                  <Point X="3.300305419922" Y="25.976302734375" />
                  <Point X="3.310984863281" Y="25.972900390625" />
                  <Point X="3.332668212891" Y="25.967376953125" />
                  <Point X="3.343672119141" Y="25.965255859375" />
                  <Point X="3.430836425781" Y="25.953736328125" />
                  <Point X="3.446958007812" Y="25.952302734375" />
                  <Point X="3.467031738281" Y="25.951375" />
                  <Point X="3.475437744141" Y="25.951359375" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.605939453125" Y="25.9666640625" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.747550292969" Y="25.93531640625" />
                  <Point X="4.752684570312" Y="25.914228515625" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="3.691992919922" Y="25.421353515625" />
                  <Point X="3.686035400391" Y="25.419544921875" />
                  <Point X="3.665634521484" Y="25.412140625" />
                  <Point X="3.642388427734" Y="25.401623046875" />
                  <Point X="3.634008789062" Y="25.3973203125" />
                  <Point X="3.548372558594" Y="25.347822265625" />
                  <Point X="3.527177734375" Y="25.333587890625" />
                  <Point X="3.505988037109" Y="25.31719140625" />
                  <Point X="3.497182617188" Y="25.30946484375" />
                  <Point X="3.480586181641" Y="25.292982421875" />
                  <Point X="3.472795166016" Y="25.2842265625" />
                  <Point X="3.421413574219" Y="25.21875390625" />
                  <Point X="3.410851806641" Y="25.204205078125" />
                  <Point X="3.399125244141" Y="25.184919921875" />
                  <Point X="3.393836181641" Y="25.174927734375" />
                  <Point X="3.384063476562" Y="25.153462890625" />
                  <Point X="3.380000976562" Y="25.142916015625" />
                  <Point X="3.373157470703" Y="25.121419921875" />
                  <Point X="3.370376464844" Y="25.110470703125" />
                  <Point X="3.353249023438" Y="25.0210390625" />
                  <Point X="3.350122558594" Y="24.9953359375" />
                  <Point X="3.348748291016" Y="24.96934765625" />
                  <Point X="3.348836669922" Y="24.95785546875" />
                  <Point X="3.350399902344" Y="24.93496875" />
                  <Point X="3.351874755859" Y="24.92357421875" />
                  <Point X="3.369002197266" Y="24.834142578125" />
                  <Point X="3.373158691406" Y="24.81601171875" />
                  <Point X="3.380004150391" Y="24.79451171875" />
                  <Point X="3.384067871094" Y="24.78396484375" />
                  <Point X="3.393840087891" Y="24.76250390625" />
                  <Point X="3.399127929688" Y="24.752513671875" />
                  <Point X="3.410852539062" Y="24.733232421875" />
                  <Point X="3.417289306641" Y="24.72394140625" />
                  <Point X="3.468670898438" Y="24.65846875" />
                  <Point X="3.4729921875" Y="24.653345703125" />
                  <Point X="3.486646972656" Y="24.63862109375" />
                  <Point X="3.505085205078" Y="24.620943359375" />
                  <Point X="3.513778808594" Y="24.613556640625" />
                  <Point X="3.531984375" Y="24.5998828125" />
                  <Point X="3.541496337891" Y="24.593595703125" />
                  <Point X="3.627125244141" Y="24.5441015625" />
                  <Point X="3.640835693359" Y="24.53690234375" />
                  <Point X="3.659797119141" Y="24.527916015625" />
                  <Point X="3.667712890625" Y="24.52459375" />
                  <Point X="3.691993408203" Y="24.516083984375" />
                  <Point X="3.788592041016" Y="24.490201171875" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.764479980469" Y="24.087958984375" />
                  <Point X="4.76161328125" Y="24.068943359375" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="3.436781005859" Y="24.09074609375" />
                  <Point X="3.428622314453" Y="24.09146484375" />
                  <Point X="3.400096191406" Y="24.09195703125" />
                  <Point X="3.366721679688" Y="24.089158203125" />
                  <Point X="3.354483154297" Y="24.087322265625" />
                  <Point X="3.186409667969" Y="24.050791015625" />
                  <Point X="3.157879394531" Y="24.0432578125" />
                  <Point X="3.128764404297" Y="24.03163671875" />
                  <Point X="3.114688964844" Y="24.0246171875" />
                  <Point X="3.086861083984" Y="24.0077265625" />
                  <Point X="3.074131835938" Y="23.9984765625" />
                  <Point X="3.050376220703" Y="23.978001953125" />
                  <Point X="3.039349853516" Y="23.96677734375" />
                  <Point X="2.937760009766" Y="23.844595703125" />
                  <Point X="2.921327636719" Y="23.823154296875" />
                  <Point X="2.906607177734" Y="23.7987734375" />
                  <Point X="2.900164794922" Y="23.78602734375" />
                  <Point X="2.888820800781" Y="23.758640625" />
                  <Point X="2.884362792969" Y="23.745072265625" />
                  <Point X="2.877531005859" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.860596923828" Y="23.545109375" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861606933594" Y="23.48541015625" />
                  <Point X="2.864064941406" Y="23.469873046875" />
                  <Point X="2.871795898438" Y="23.438251953125" />
                  <Point X="2.876785400391" Y="23.42333203125" />
                  <Point X="2.889157470703" Y="23.39451953125" />
                  <Point X="2.896540039062" Y="23.380626953125" />
                  <Point X="2.989554443359" Y="23.235951171875" />
                  <Point X="3.000090576172" Y="23.221166015625" />
                  <Point X="3.019177001953" Y="23.196923828125" />
                  <Point X="3.027062255859" Y="23.188099609375" />
                  <Point X="3.043871337891" Y="23.171498046875" />
                  <Point X="3.052795166016" Y="23.163720703125" />
                  <Point X="3.142439453125" Y="23.09493359375" />
                  <Point X="4.087170166016" Y="22.370015625" />
                  <Point X="4.053567871094" Y="22.315642578125" />
                  <Point X="4.0454921875" Y="22.30257421875" />
                  <Point X="4.001274169922" Y="22.239748046875" />
                  <Point X="2.848453857422" Y="22.905328125" />
                  <Point X="2.841188720703" Y="22.909115234375" />
                  <Point X="2.815029052734" Y="22.920484375" />
                  <Point X="2.783118408203" Y="22.930673828125" />
                  <Point X="2.77110546875" Y="22.9336640625" />
                  <Point X="2.571071289062" Y="22.969791015625" />
                  <Point X="2.539349609375" Y="22.974193359375" />
                  <Point X="2.507990966797" Y="22.974595703125" />
                  <Point X="2.492297851562" Y="22.973494140625" />
                  <Point X="2.460130371094" Y="22.96853515625" />
                  <Point X="2.444838867188" Y="22.964861328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.400590332031" Y="22.948890625" />
                  <Point X="2.234410888672" Y="22.861431640625" />
                  <Point X="2.208970703125" Y="22.846830078125" />
                  <Point X="2.186042724609" Y="22.829939453125" />
                  <Point X="2.175216308594" Y="22.820630859375" />
                  <Point X="2.154255615234" Y="22.799671875" />
                  <Point X="2.144943359375" Y="22.788841796875" />
                  <Point X="2.128047607422" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.033005126953" Y="22.587625" />
                  <Point X="2.019836669922" Y="22.55980859375" />
                  <Point X="2.010013427734" Y="22.53003515625" />
                  <Point X="2.006338378906" Y="22.514740234375" />
                  <Point X="2.001380249023" Y="22.482568359375" />
                  <Point X="2.000279663086" Y="22.466875" />
                  <Point X="2.00068347168" Y="22.435521484375" />
                  <Point X="2.002187866211" Y="22.419861328125" />
                  <Point X="2.038313842773" Y="22.219826171875" />
                  <Point X="2.042024902344" Y="22.203451171875" />
                  <Point X="2.050827392578" Y="22.171279296875" />
                  <Point X="2.054786376953" Y="22.159765625" />
                  <Point X="2.064145996094" Y="22.1373359375" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.127152099609" Y="22.02664453125" />
                  <Point X="2.735894042969" Y="20.972275390625" />
                  <Point X="2.723752441406" Y="20.963916015625" />
                  <Point X="1.833914428711" Y="22.123576171875" />
                  <Point X="1.828652954102" Y="22.129853515625" />
                  <Point X="1.80882824707" Y="22.150375" />
                  <Point X="1.783249389648" Y="22.17199609375" />
                  <Point X="1.773296875" Y="22.179353515625" />
                  <Point X="1.576008789062" Y="22.30619140625" />
                  <Point X="1.546278808594" Y="22.32375390625" />
                  <Point X="1.517465820312" Y="22.336125" />
                  <Point X="1.502547363281" Y="22.34111328125" />
                  <Point X="1.470928466797" Y="22.34884375" />
                  <Point X="1.455391357422" Y="22.351302734375" />
                  <Point X="1.424125" Y="22.35362109375" />
                  <Point X="1.408395751953" Y="22.35348046875" />
                  <Point X="1.192628173828" Y="22.333626953125" />
                  <Point X="1.161228393555" Y="22.32966015625" />
                  <Point X="1.13357421875" Y="22.322828125" />
                  <Point X="1.120000488281" Y="22.318369140625" />
                  <Point X="1.092612915039" Y="22.3070234375" />
                  <Point X="1.079869384766" Y="22.30058203125" />
                  <Point X="1.055490966797" Y="22.28586328125" />
                  <Point X="1.043856079102" Y="22.2775859375" />
                  <Point X="0.877245483398" Y="22.1390546875" />
                  <Point X="0.852647644043" Y="22.116904296875" />
                  <Point X="0.832175476074" Y="22.093150390625" />
                  <Point X="0.82292767334" Y="22.08042578125" />
                  <Point X="0.806038024902" Y="22.0526015625" />
                  <Point X="0.79901739502" Y="22.03852734375" />
                  <Point X="0.787394165039" Y="22.00941015625" />
                  <Point X="0.782791748047" Y="21.9943671875" />
                  <Point X="0.732976196289" Y="21.765177734375" />
                  <Point X="0.730288818359" Y="21.749791015625" />
                  <Point X="0.725577453613" Y="21.71425" />
                  <Point X="0.724753479004" Y="21.70180859375" />
                  <Point X="0.724742370605" Y="21.676923828125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.741879760742" Y="21.540478515625" />
                  <Point X="0.833090759277" Y="20.8476640625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652606872559" Y="21.519875" />
                  <Point X="0.642145324707" Y="21.546419921875" />
                  <Point X="0.626787841797" Y="21.57618359375" />
                  <Point X="0.620408203125" Y="21.586791015625" />
                  <Point X="0.468846099854" Y="21.805162109375" />
                  <Point X="0.456680419922" Y="21.82269140625" />
                  <Point X="0.446672454834" Y="21.834826171875" />
                  <Point X="0.424791442871" Y="21.857279296875" />
                  <Point X="0.412918273926" Y="21.86759765625" />
                  <Point X="0.386664489746" Y="21.886841796875" />
                  <Point X="0.373246368408" Y="21.895060546875" />
                  <Point X="0.345242095947" Y="21.909169921875" />
                  <Point X="0.330655944824" Y="21.915060546875" />
                  <Point X="0.096122314453" Y="21.987849609375" />
                  <Point X="0.063382621765" Y="21.996888671875" />
                  <Point X="0.035230026245" Y="22.001158203125" />
                  <Point X="0.020991802216" Y="22.002232421875" />
                  <Point X="-0.00865014267" Y="22.002234375" />
                  <Point X="-0.022894458771" Y="22.001162109375" />
                  <Point X="-0.051060134888" Y="21.996892578125" />
                  <Point X="-0.064981788635" Y="21.9936953125" />
                  <Point X="-0.299515563965" Y="21.920904296875" />
                  <Point X="-0.332927490234" Y="21.909171875" />
                  <Point X="-0.360926849365" Y="21.895064453125" />
                  <Point X="-0.374339630127" Y="21.88684765625" />
                  <Point X="-0.400596374512" Y="21.867603515625" />
                  <Point X="-0.412472686768" Y="21.85728515625" />
                  <Point X="-0.434358306885" Y="21.834828125" />
                  <Point X="-0.444367614746" Y="21.822689453125" />
                  <Point X="-0.59592980957" Y="21.60431640625" />
                  <Point X="-0.608095214844" Y="21.5867890625" />
                  <Point X="-0.612468139648" Y="21.57987109375" />
                  <Point X="-0.625974365234" Y="21.554740234375" />
                  <Point X="-0.63877722168" Y="21.523787109375" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.672517578125" Y="21.40098046875" />
                  <Point X="-0.985425415039" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076856676534" Y="29.677798082494" />
                  <Point X="0.352850245889" Y="29.690435661749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.343796662935" Y="29.702595678774" />
                  <Point X="-0.942827293776" Y="29.71305179733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.484401166181" Y="29.575669895801" />
                  <Point X="0.327509726396" Y="29.595863510998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.31821802162" Y="29.607134730765" />
                  <Point X="-1.261864676157" Y="29.62360614439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.759605996165" Y="29.475851706461" />
                  <Point X="0.302169206904" Y="29.501291360247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.292639380304" Y="29.511673782756" />
                  <Point X="-1.475272366391" Y="29.532316718315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.993644211621" Y="29.376752083051" />
                  <Point X="0.276828687412" Y="29.406719209496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267060738989" Y="29.416212834747" />
                  <Point X="-1.463073800541" Y="29.437089320392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.204687004374" Y="29.278053846236" />
                  <Point X="0.251488172383" Y="29.312147058667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241482097674" Y="29.320751886738" />
                  <Point X="-1.472148375166" Y="29.342233246517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.382117337705" Y="29.179942317084" />
                  <Point X="0.21929484545" Y="29.217694524114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.210507364663" Y="29.225196749598" />
                  <Point X="-1.501605054508" Y="29.247732943603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.185910276924" Y="29.259677535691" />
                  <Point X="-2.207359461212" Y="29.260051932595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550241007779" Y="29.081993236342" />
                  <Point X="0.137495312937" Y="29.124107869101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.131829793452" Y="29.12880895632" />
                  <Point X="-1.551895665695" Y="29.153596298322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.111158881744" Y="29.16335827407" />
                  <Point X="-2.373168941917" Y="29.167931676683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.701890662468" Y="28.984331710609" />
                  <Point X="-1.705954906324" Y="29.061270941206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.963363497737" Y="29.065764024882" />
                  <Point X="-2.538978422622" Y="29.07581142077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.809421341634" Y="28.887440284458" />
                  <Point X="-2.689600550809" Y="28.983426068633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.755112070111" Y="28.793373785155" />
                  <Point X="-2.810769865106" Y="28.890526615717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.700802798588" Y="28.699307285851" />
                  <Point X="-2.931939179403" Y="28.797627162801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646493527064" Y="28.605240786548" />
                  <Point X="-3.0531084937" Y="28.704727709885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.592184255541" Y="28.511174287244" />
                  <Point X="-3.006843128242" Y="28.608905673763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.537874984018" Y="28.417107787941" />
                  <Point X="-2.951427917317" Y="28.512923926494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483565712495" Y="28.323041288637" />
                  <Point X="-2.896012706392" Y="28.416942179225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.429256440972" Y="28.228974789334" />
                  <Point X="-2.840597495467" Y="28.320960431957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.374947169449" Y="28.13490829003" />
                  <Point X="-2.785470596117" Y="28.224983717185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320637897925" Y="28.040841790727" />
                  <Point X="-2.75643965138" Y="28.129462508995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266328626402" Y="27.946775291423" />
                  <Point X="-2.74846713526" Y="28.034308877045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212019354879" Y="27.85270879212" />
                  <Point X="-2.763627959216" Y="27.939559039047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.585864252741" Y="27.953911226937" />
                  <Point X="-3.786707694198" Y="27.957416962248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979335213551" Y="27.726845707894" />
                  <Point X="3.850578803421" Y="27.729093159392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.157710083356" Y="27.758642292816" />
                  <Point X="-2.826316807832" Y="27.845638805806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416163390227" Y="27.855934616199" />
                  <Point X="-3.859637737544" Y="27.863675489726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.047829863539" Y="27.630635658167" />
                  <Point X="3.690838469518" Y="27.636866966128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103400811833" Y="27.664575793513" />
                  <Point X="-2.934772234165" Y="27.75251743115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.245353798161" Y="27.757938652515" />
                  <Point X="-3.932567780891" Y="27.769934017203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10594008068" Y="27.534606869389" />
                  <Point X="3.531098135614" Y="27.544640772864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.052058175641" Y="27.570457511397" />
                  <Point X="-4.004705018238" Y="27.676178706201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080284551945" Y="27.440040217145" />
                  <Point X="3.371357801711" Y="27.4524145796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02400960183" Y="27.47593262991" />
                  <Point X="-4.059604626916" Y="27.582122511271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.95921336539" Y="27.347139051403" />
                  <Point X="3.211617467807" Y="27.360188386336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012727534199" Y="27.381115087968" />
                  <Point X="-4.114504146138" Y="27.488066314779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.838142178835" Y="27.254237885661" />
                  <Point X="3.051877133904" Y="27.267962193071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.024566333523" Y="27.285893969793" />
                  <Point X="-4.16940366536" Y="27.394010118287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717070992281" Y="27.161336719919" />
                  <Point X="2.8921368" Y="27.175735999807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064134908232" Y="27.190188826589" />
                  <Point X="-4.224303184583" Y="27.299953921796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.595999805726" Y="27.068435554177" />
                  <Point X="2.732396429158" Y="27.083509807188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136851446919" Y="27.09390508352" />
                  <Point X="-4.10854815478" Y="27.202918939071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474928619171" Y="26.975534388435" />
                  <Point X="2.533545669167" Y="26.99196628895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290607751092" Y="26.996206786084" />
                  <Point X="-3.981840096679" Y="27.105692770525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353857432616" Y="26.882633222694" />
                  <Point X="-3.855132038579" Y="27.00846660198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.232786246062" Y="26.789732056952" />
                  <Point X="-3.728423980478" Y="26.911240433435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.126394668769" Y="26.696574657677" />
                  <Point X="-3.60171601424" Y="26.814014266493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.057766990902" Y="26.602758087086" />
                  <Point X="-3.494778418442" Y="26.717133192651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020553814005" Y="26.508393174341" />
                  <Point X="-3.442497442767" Y="26.621206153662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.00145332956" Y="26.413712103372" />
                  <Point X="-3.421151060969" Y="26.525819080017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01172552635" Y="26.318518330346" />
                  <Point X="-3.438927344816" Y="26.431114895042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.037123909981" Y="26.223060528747" />
                  <Point X="-3.482206484745" Y="26.336855864075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707730762855" Y="26.098885506496" />
                  <Point X="4.621663721105" Y="26.100387812298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.094073775513" Y="26.127051993982" />
                  <Point X="-3.589719259227" Y="26.24371803537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.140600058211" Y="26.253333695484" />
                  <Point X="-4.648770645683" Y="26.262203846083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.730959949136" Y="26.003465568377" />
                  <Point X="3.984447396982" Y="26.016495993445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.19296786023" Y="26.030311320148" />
                  <Point X="-4.674396541046" Y="26.167636676586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753645740507" Y="25.908055115252" />
                  <Point X="-4.700022436409" Y="26.07306950709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768479371941" Y="25.812781722088" />
                  <Point X="-4.725648331772" Y="25.978502337593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783313003375" Y="25.717508328924" />
                  <Point X="-4.742244519614" Y="25.883777553965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.463486956997" Y="25.628076442165" />
                  <Point X="-4.756267991667" Y="25.789007863416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130577669546" Y="25.538872924228" />
                  <Point X="-4.77029146372" Y="25.694238172867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.797668382096" Y="25.449669406292" />
                  <Point X="-4.784314935773" Y="25.599468482318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.567154962013" Y="25.358678561842" />
                  <Point X="-4.436724771291" Y="25.498386802265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.458151334352" Y="25.265566756076" />
                  <Point X="-4.057415947675" Y="25.396751470956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392366883503" Y="25.171700556773" />
                  <Point X="-3.678107124059" Y="25.295116139648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.36400105021" Y="25.077181213071" />
                  <Point X="-3.405385112372" Y="25.195341288063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349439608478" Y="24.982420912817" />
                  <Point X="-3.326735301787" Y="25.098953979348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358832804198" Y="24.887242482812" />
                  <Point X="-3.297577293478" Y="25.003430553256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381033373732" Y="24.791840499265" />
                  <Point X="-3.301714842674" Y="24.908488303281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.439367986215" Y="24.695807793652" />
                  <Point X="-3.340198312183" Y="24.814145563576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533081655766" Y="24.599157544302" />
                  <Point X="-3.436349089004" Y="24.720809410464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.750737938584" Y="24.500343868589" />
                  <Point X="-3.721238810511" Y="24.630767707886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130048070685" Y="24.398708514441" />
                  <Point X="-4.054149711865" Y="24.541564218121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.509357180062" Y="24.297073178145" />
                  <Point X="-4.387060613219" Y="24.452360728355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.780967546508" Y="24.197317730399" />
                  <Point X="-4.719971514573" Y="24.363157238589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766680206453" Y="24.102552645683" />
                  <Point X="-4.775549290926" Y="24.26911288112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.747675933372" Y="24.00786989534" />
                  <Point X="3.962142695428" Y="24.021581429011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.139545453469" Y="24.035939917279" />
                  <Point X="-4.761925913388" Y="24.173860613016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019595751814" Y="23.943019175946" />
                  <Point X="-4.748302535851" Y="24.078608344913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941724643534" Y="23.849363950033" />
                  <Point X="-4.729877891776" Y="23.98327227039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887720619881" Y="23.755292122608" />
                  <Point X="-3.075449086431" Y="23.859379637009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.495665847809" Y="23.866714547863" />
                  <Point X="-4.705499423383" Y="23.887832271477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871221162874" Y="23.660565650537" />
                  <Point X="-2.978535755644" Y="23.762673537364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.132889429349" Y="23.782822855688" />
                  <Point X="-4.68112095499" Y="23.792392272564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.862491991134" Y="23.565703547632" />
                  <Point X="-2.949987151715" Y="23.667160748464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.863939841718" Y="23.470663804142" />
                  <Point X="-2.957764164862" Y="23.57228202557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.900146548602" Y="23.375017342558" />
                  <Point X="-3.001347208763" Y="23.478028299267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.961926191923" Y="23.278924503709" />
                  <Point X="-3.102003926965" Y="23.384770797654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.032552397212" Y="23.182677247546" />
                  <Point X="-3.223075095576" Y="23.291869631599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154693406162" Y="23.08553079714" />
                  <Point X="-3.344146264187" Y="23.198968465544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.281400898811" Y="22.988304638465" />
                  <Point X="-3.465217432798" Y="23.106067299489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.408108391461" Y="22.891078479789" />
                  <Point X="2.856456866136" Y="22.900707592982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326611413769" Y="22.909956079755" />
                  <Point X="-3.586288601409" Y="23.013166133434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.534815884111" Y="22.793852321114" />
                  <Point X="3.026157594913" Y="22.802730984578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.172221696194" Y="22.817636491135" />
                  <Point X="-3.70735977002" Y="22.920264967378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.66152337676" Y="22.696626162439" />
                  <Point X="3.19585832369" Y="22.704754376175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.104673462149" Y="22.723801078782" />
                  <Point X="-3.828430938631" Y="22.827363801323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.78823086941" Y="22.599400003764" />
                  <Point X="3.365559052467" Y="22.606777767772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.055123328034" Y="22.629651508426" />
                  <Point X="-3.949502107242" Y="22.734462635268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.914938362059" Y="22.502173845088" />
                  <Point X="3.535259781244" Y="22.508801159368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.011781347168" Y="22.535393574352" />
                  <Point X="-2.437388748561" Y="22.613054127249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.692848981636" Y="22.617513202204" />
                  <Point X="-4.070573275853" Y="22.641561469213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.041645854709" Y="22.404947686413" />
                  <Point X="3.704960510021" Y="22.410824550965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00061839986" Y="22.440573953158" />
                  <Point X="-2.344260123275" Y="22.516414089884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85258839062" Y="22.525286992796" />
                  <Point X="-4.176355708326" Y="22.548393437276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.049950156409" Y="22.309788263123" />
                  <Point X="3.874661238798" Y="22.312847942562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015654038831" Y="22.345297033939" />
                  <Point X="-2.311829035866" Y="22.420833531984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012328635877" Y="22.433060797984" />
                  <Point X="-4.119097250225" Y="22.452379516008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032867738961" Y="22.249982096521" />
                  <Point X="1.653128715821" Y="22.256610465826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.031675248961" Y="22.26745797644" />
                  <Point X="-2.321917250029" Y="22.325995151253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.172068881133" Y="22.340834603172" />
                  <Point X="-4.061838792124" Y="22.35636559474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056964032225" Y="22.154547022994" />
                  <Point X="1.798556352211" Y="22.159057545826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.919751611113" Y="22.174397139641" />
                  <Point X="-2.366957755039" Y="22.231766865028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331809126389" Y="22.248608408361" />
                  <Point X="-4.004580334023" Y="22.260351673472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.108685385146" Y="22.058629752256" />
                  <Point X="1.880695909504" Y="22.062609323356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823391968026" Y="22.081064632304" />
                  <Point X="-2.421267079885" Y="22.137700366655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491549371646" Y="22.156382213549" />
                  <Point X="-3.931442693151" Y="22.164060580038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.164100610733" Y="21.962648004731" />
                  <Point X="1.9545927899" Y="21.966304977347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.781144283615" Y="21.986787597214" />
                  <Point X="0.045294162756" Y="21.999631908851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.024905585832" Y="22.000857250021" />
                  <Point X="-2.475576404731" Y="22.043633868282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651289616902" Y="22.064156018738" />
                  <Point X="-3.858148571915" Y="22.067766755228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.219515847564" Y="21.86666625701" />
                  <Point X="2.028489670297" Y="21.870000631338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.760570437005" Y="21.892132243878" />
                  <Point X="0.365366709234" Y="21.899030550606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.327372831177" Y="21.911122364263" />
                  <Point X="-2.529885729577" Y="21.94956736991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.274931084395" Y="21.770684509289" />
                  <Point X="2.102386550693" Y="21.773696285328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739996590395" Y="21.797476890543" />
                  <Point X="0.470920272921" Y="21.802173635135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.44748048756" Y="21.818204380039" />
                  <Point X="-2.584195054424" Y="21.855500871537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330346321226" Y="21.674702761568" />
                  <Point X="2.17628343109" Y="21.677391939319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724814331852" Y="21.702727426687" />
                  <Point X="0.537674498045" Y="21.705993964637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.512636105367" Y="21.724327204414" />
                  <Point X="-2.63850437927" Y="21.761434373164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.385761558056" Y="21.578721013847" />
                  <Point X="2.250180311486" Y="21.581087593309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733047313974" Y="21.607569248285" />
                  <Point X="0.604428723168" Y="21.609814294139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.577791723174" Y="21.630450028789" />
                  <Point X="-2.692813704116" Y="21.667367874791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.441176794887" Y="21.482739266126" />
                  <Point X="2.324077191883" Y="21.4847832473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.745584811834" Y="21.512335934282" />
                  <Point X="0.654479030915" Y="21.513926191603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.633556571327" Y="21.53640893667" />
                  <Point X="-2.747123028962" Y="21.573301376418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496592031718" Y="21.386757518405" />
                  <Point X="2.397974072279" Y="21.38847890129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758122549762" Y="21.417102616088" />
                  <Point X="0.680145221489" Y="21.418463715416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.661557885444" Y="21.441883230262" />
                  <Point X="-2.801432353808" Y="21.479234878046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.552007268549" Y="21.290775770684" />
                  <Point X="2.471870952676" Y="21.292174555281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77066028769" Y="21.321869297895" />
                  <Point X="0.705723944557" Y="21.32300276598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.68689825861" Y="21.347311076957" />
                  <Point X="-2.855741678654" Y="21.385168379673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.607422505379" Y="21.194794022963" />
                  <Point X="2.545767833073" Y="21.195870209271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.783198025619" Y="21.226635979701" />
                  <Point X="0.731302667625" Y="21.227541816544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.712238808033" Y="21.252738926728" />
                  <Point X="-2.910051003501" Y="21.2911018813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66283774221" Y="21.098812275242" />
                  <Point X="2.619664713469" Y="21.099565863262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795735763547" Y="21.131402661507" />
                  <Point X="0.756881390693" Y="21.132080867108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.737579357456" Y="21.1581667765" />
                  <Point X="-1.536396316178" Y="21.17211017838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.098821273031" Y="21.181927342519" />
                  <Point X="-2.964360328347" Y="21.197035382927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718252979041" Y="21.002830527521" />
                  <Point X="2.693561593866" Y="21.003261517252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808273501475" Y="21.036169343313" />
                  <Point X="0.782460113761" Y="21.036619917672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.762919906878" Y="21.063594626271" />
                  <Point X="-1.414662062869" Y="21.07497082792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.23740009932" Y="21.089331773765" />
                  <Point X="-2.89296453431" Y="21.100774693543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820811239404" Y="20.940936025119" />
                  <Point X="0.808038836829" Y="20.941158968236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.788260456301" Y="20.969022476042" />
                  <Point X="-1.30411909933" Y="20.97802682215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372312267818" Y="20.996672203262" />
                  <Point X="-2.766701552288" Y="21.003556293829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.813601005724" Y="20.874450325813" />
                  <Point X="-1.215440822837" Y="20.881464465912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.452907759287" Y="20.903064531634" />
                  <Point X="-2.613585913603" Y="20.90586917925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.838941555147" Y="20.779878175585" />
                  <Point X="-1.179729649055" Y="20.785826653891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864282104569" Y="20.685306025356" />
                  <Point X="-1.160764290357" Y="20.690481141159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889622653992" Y="20.590733875127" />
                  <Point X="-1.141798914721" Y="20.595135628132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.914963203415" Y="20.496161724899" />
                  <Point X="-1.122993916988" Y="20.499792914511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.940303752838" Y="20.40158957467" />
                  <Point X="-1.123326541303" Y="20.404784249326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.96564430226" Y="20.307017424441" />
                  <Point X="-1.135806851791" Y="20.309987622792" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.842355957031" Y="20.07898046875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464319580078" Y="21.478455078125" />
                  <Point X="0.312757446289" Y="21.696826171875" />
                  <Point X="0.300591949463" Y="21.71435546875" />
                  <Point X="0.274338195801" Y="21.733599609375" />
                  <Point X="0.039804515839" Y="21.806388671875" />
                  <Point X="0.020979211807" Y="21.812232421875" />
                  <Point X="-0.00866262722" Y="21.812234375" />
                  <Point X="-0.243196456909" Y="21.739443359375" />
                  <Point X="-0.262021759033" Y="21.7336015625" />
                  <Point X="-0.288278564453" Y="21.714357421875" />
                  <Point X="-0.439840698242" Y="21.495984375" />
                  <Point X="-0.452006195068" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.488991607666" Y="21.3518046875" />
                  <Point X="-0.84774395752" Y="20.012923828125" />
                  <Point X="-1.079444580078" Y="20.057896484375" />
                  <Point X="-1.100255615234" Y="20.0619375" />
                  <Point X="-1.35158984375" Y="20.1266015625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.365713378906" Y="20.746923828125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386283569336" Y="20.79737109375" />
                  <Point X="-1.602212280273" Y="20.986736328125" />
                  <Point X="-1.619544311523" Y="21.001935546875" />
                  <Point X="-1.649240356445" Y="21.014236328125" />
                  <Point X="-1.935826171875" Y="21.03301953125" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989877197266" Y="21.026208984375" />
                  <Point X="-2.228676025391" Y="20.8666484375" />
                  <Point X="-2.24784375" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.276444580078" Y="20.820927734375" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.825352783203" Y="20.813515625" />
                  <Point X="-2.855833251953" Y="20.832388671875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.195155029297" Y="21.177287109375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979736328" Y="22.431236328125" />
                  <Point X="-2.530039306641" Y="22.447294921875" />
                  <Point X="-2.531328369141" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.687489990234" Y="22.40121484375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.13762109375" Y="22.12123046875" />
                  <Point X="-4.161703613281" Y="22.15287109375" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.369525390625" Y="22.651658203125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821289062" Y="23.60398828125" />
                  <Point X="-3.138689208984" Y="23.631525390625" />
                  <Point X="-3.138116699219" Y="23.633736328125" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.161157958984" Y="23.689359375" />
                  <Point X="-3.185672851562" Y="23.703787109375" />
                  <Point X="-3.187640625" Y="23.7049453125" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.340475830078" Y="23.695505859375" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.917973632812" Y="23.951931640625" />
                  <Point X="-4.927392578125" Y="23.988806640625" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.930981445312" Y="24.5033203125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541896484375" Y="24.87857421875" />
                  <Point X="-3.516205810547" Y="24.896404296875" />
                  <Point X="-3.514138427734" Y="24.89783984375" />
                  <Point X="-3.494897949219" Y="24.92409375" />
                  <Point X="-3.486334228516" Y="24.951685546875" />
                  <Point X="-3.485646972656" Y="24.953900390625" />
                  <Point X="-3.485646972656" Y="24.983537109375" />
                  <Point X="-3.494210693359" Y="25.011130859375" />
                  <Point X="-3.494897949219" Y="25.013345703125" />
                  <Point X="-3.514143554688" Y="25.039603515625" />
                  <Point X="-3.539834228516" Y="25.05743359375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.667712402344" Y="25.095626953125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.92371484375" Y="25.955400390625" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.773696777344" Y="26.5276328125" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.737837402344" Y="26.5236015625" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731701171875" Y="26.3958671875" />
                  <Point X="-3.674839355469" Y="26.413794921875" />
                  <Point X="-3.670275146484" Y="26.415234375" />
                  <Point X="-3.651531982422" Y="26.42605859375" />
                  <Point X="-3.639118896484" Y="26.4437890625" />
                  <Point X="-3.616302734375" Y="26.49887109375" />
                  <Point X="-3.614471435547" Y="26.50329296875" />
                  <Point X="-3.610714355469" Y="26.524607421875" />
                  <Point X="-3.616317382812" Y="26.545513671875" />
                  <Point X="-3.643847412109" Y="26.5983984375" />
                  <Point X="-3.646057128906" Y="26.602642578125" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.72320703125" Y="26.66774609375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.183598144531" Y="27.746599609375" />
                  <Point X="-4.160010253906" Y="27.78701171875" />
                  <Point X="-3.778697998047" Y="28.277134765625" />
                  <Point X="-3.763853271484" Y="28.276064453125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138518798828" Y="27.920435546875" />
                  <Point X="-3.059325927734" Y="27.9135078125" />
                  <Point X="-3.052969482422" Y="27.912951171875" />
                  <Point X="-3.031509277344" Y="27.9157734375" />
                  <Point X="-3.013253173828" Y="27.927404296875" />
                  <Point X="-2.957041503906" Y="27.983615234375" />
                  <Point X="-2.952529541016" Y="27.988126953125" />
                  <Point X="-2.940899658203" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.945002685547" Y="28.107033203125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-2.980090576172" Y="28.1825703125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.793950683594" Y="29.14283984375" />
                  <Point X="-2.752873779297" Y="29.17433203125" />
                  <Point X="-2.152314208984" Y="29.507990234375" />
                  <Point X="-2.141548583984" Y="29.513970703125" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.95124987793" Y="29.273662109375" />
                  <Point X="-1.863108642578" Y="29.22777734375" />
                  <Point X="-1.856033813477" Y="29.22409375" />
                  <Point X="-1.835124633789" Y="29.218490234375" />
                  <Point X="-1.813806640625" Y="29.22225" />
                  <Point X="-1.722001586914" Y="29.26027734375" />
                  <Point X="-1.714632568359" Y="29.263330078125" />
                  <Point X="-1.696904296875" Y="29.27574609375" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.656202514648" Y="29.3892578125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.655103637695" Y="29.442625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.021268249512" Y="29.88838671875" />
                  <Point X="-0.96808380127" Y="29.903296875" />
                  <Point X="-0.240028778076" Y="29.988505859375" />
                  <Point X="-0.221026260376" Y="29.978513671875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282115936" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.068810966492" Y="29.364490234375" />
                  <Point X="0.236648391724" Y="29.9908671875" />
                  <Point X="0.813773925781" Y="29.930427734375" />
                  <Point X="0.860208862305" Y="29.925564453125" />
                  <Point X="1.46255065918" Y="29.780140625" />
                  <Point X="1.508457763672" Y="29.769056640625" />
                  <Point X="1.901389526367" Y="29.626537109375" />
                  <Point X="1.931029052734" Y="29.615787109375" />
                  <Point X="2.310145019531" Y="29.438486328125" />
                  <Point X="2.338703125" Y="29.425130859375" />
                  <Point X="2.704931640625" Y="29.211765625" />
                  <Point X="2.732529785156" Y="29.1956875" />
                  <Point X="3.068740722656" Y="28.95659375" />
                  <Point X="3.027459716797" Y="28.885091796875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851806641" Y="27.491513671875" />
                  <Point X="2.204977539062" Y="27.417193359375" />
                  <Point X="2.202044433594" Y="27.392326171875" />
                  <Point X="2.209793945312" Y="27.328060546875" />
                  <Point X="2.210415771484" Y="27.32290234375" />
                  <Point X="2.218684570312" Y="27.30080859375" />
                  <Point X="2.258450439453" Y="27.242203125" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.333536621094" Y="27.184443359375" />
                  <Point X="2.338239990234" Y="27.18125" />
                  <Point X="2.360338134766" Y="27.172978515625" />
                  <Point X="2.424604492188" Y="27.16523046875" />
                  <Point X="2.448667480469" Y="27.1659453125" />
                  <Point X="2.52298828125" Y="27.1858203125" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.651922607422" Y="27.256439453125" />
                  <Point X="3.994247314453" Y="28.0314296875" />
                  <Point X="4.186224121094" Y="27.764626953125" />
                  <Point X="4.202593261719" Y="27.741876953125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.338739257812" Y="27.39887109375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279370117188" Y="26.583830078125" />
                  <Point X="3.225881347656" Y="26.51405078125" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.193195068359" Y="26.42025390625" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.207135498047" Y="26.3116953125" />
                  <Point X="3.215645751953" Y="26.287955078125" />
                  <Point X="3.260132568359" Y="26.220337890625" />
                  <Point X="3.263703369141" Y="26.21491015625" />
                  <Point X="3.280947998047" Y="26.1988203125" />
                  <Point X="3.345415283203" Y="26.16253125" />
                  <Point X="3.368565673828" Y="26.153619140625" />
                  <Point X="3.455729980469" Y="26.142099609375" />
                  <Point X="3.455735595703" Y="26.14209765625" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.581140625" Y="26.1550390625" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.932158691406" Y="25.9802578125" />
                  <Point X="4.939189453125" Y="25.951380859375" />
                  <Point X="4.997858886719" Y="25.574556640625" />
                  <Point X="4.944631347656" Y="25.560294921875" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729089355469" Y="25.2328203125" />
                  <Point X="3.643453125" Y="25.183322265625" />
                  <Point X="3.622263427734" Y="25.16692578125" />
                  <Point X="3.570881835938" Y="25.101453125" />
                  <Point X="3.566757568359" Y="25.096197265625" />
                  <Point X="3.556984863281" Y="25.074732421875" />
                  <Point X="3.539857421875" Y="24.98530078125" />
                  <Point X="3.538483154297" Y="24.9593125" />
                  <Point X="3.555610595703" Y="24.869880859375" />
                  <Point X="3.556985351562" Y="24.862703125" />
                  <Point X="3.566757568359" Y="24.8412421875" />
                  <Point X="3.618139160156" Y="24.77576953125" />
                  <Point X="3.636577392578" Y="24.758091796875" />
                  <Point X="3.722206298828" Y="24.70859765625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.837766357422" Y="24.673728515625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.952356933594" Y="24.059634765625" />
                  <Point X="4.948430664062" Y="24.03359375" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="4.807661621094" Y="23.718626953125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394838134766" Y="23.901658203125" />
                  <Point X="3.226764648438" Y="23.865126953125" />
                  <Point X="3.213273925781" Y="23.8621953125" />
                  <Point X="3.185446044922" Y="23.8453046875" />
                  <Point X="3.083856201172" Y="23.723123046875" />
                  <Point X="3.075701904297" Y="23.71331640625" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.049797607422" Y="23.52769921875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056359863281" Y="23.483376953125" />
                  <Point X="3.149374267578" Y="23.338701171875" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.258104980469" Y="23.245671875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.215194824219" Y="22.215759765625" />
                  <Point X="4.204126953125" Y="22.197849609375" />
                  <Point X="4.056688720703" Y="21.988361328125" />
                  <Point X="3.996495361328" Y="22.02311328125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737336914062" Y="22.7466875" />
                  <Point X="2.537302734375" Y="22.782814453125" />
                  <Point X="2.521246582031" Y="22.785712890625" />
                  <Point X="2.489079101562" Y="22.78075390625" />
                  <Point X="2.322899658203" Y="22.693294921875" />
                  <Point X="2.309561035156" Y="22.686275390625" />
                  <Point X="2.288600341797" Y="22.66531640625" />
                  <Point X="2.201141357422" Y="22.49913671875" />
                  <Point X="2.194121337891" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453626953125" />
                  <Point X="2.2252890625" Y="22.253591796875" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.291697021484" Y="22.12164453125" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.8484296875" Y="20.819166015625" />
                  <Point X="2.835298828125" Y="20.809787109375" />
                  <Point X="2.679774902344" Y="20.70912109375" />
                  <Point X="2.631732421875" Y="20.77173046875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670547119141" Y="22.019533203125" />
                  <Point X="1.473259277344" Y="22.14637109375" />
                  <Point X="1.457423583984" Y="22.15655078125" />
                  <Point X="1.4258046875" Y="22.16428125" />
                  <Point X="1.210036987305" Y="22.144427734375" />
                  <Point X="1.192717773438" Y="22.142833984375" />
                  <Point X="1.165330078125" Y="22.13148828125" />
                  <Point X="0.998719787598" Y="21.99295703125" />
                  <Point X="0.985346496582" Y="21.9818359375" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.918641052246" Y="21.724822265625" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.930254394531" Y="21.565279296875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.006764038086" Y="20.039474609375" />
                  <Point X="0.994356079102" Y="20.036755859375" />
                  <Point X="0.860200622559" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#210" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.17888588467" Y="5.022735717989" Z="2.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="-0.251997501656" Y="5.069446858853" Z="2.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.35" />
                  <Point X="-1.040921814484" Y="4.967811911153" Z="2.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.35" />
                  <Point X="-1.710831794447" Y="4.467379908761" Z="2.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.35" />
                  <Point X="-1.710044342814" Y="4.435573668988" Z="2.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.35" />
                  <Point X="-1.747294078111" Y="4.337751762799" Z="2.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.35" />
                  <Point X="-1.846173908459" Y="4.303407543494" Z="2.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.35" />
                  <Point X="-2.119431006362" Y="4.590539045953" Z="2.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.35" />
                  <Point X="-2.182753310146" Y="4.582978036368" Z="2.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.35" />
                  <Point X="-2.830528703279" Y="4.213799789576" Z="2.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.35" />
                  <Point X="-3.029547711361" Y="3.188850209572" Z="2.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.35" />
                  <Point X="-3.000968552032" Y="3.133956324779" Z="2.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.35" />
                  <Point X="-2.998551962028" Y="3.050251605858" Z="2.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.35" />
                  <Point X="-3.061120140828" Y="2.994596147141" Z="2.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.35" />
                  <Point X="-3.745009138725" Y="3.350646237385" Z="2.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.35" />
                  <Point X="-3.824317522902" Y="3.339117366544" Z="2.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.35" />
                  <Point X="-4.232936940108" Y="2.803085765564" Z="2.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.35" />
                  <Point X="-3.759801084296" Y="1.659358949662" Z="2.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.35" />
                  <Point X="-3.694352525582" Y="1.606589202827" Z="2.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.35" />
                  <Point X="-3.668653818099" Y="1.549283027099" Z="2.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.35" />
                  <Point X="-3.696034064265" Y="1.492761038834" Z="2.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.35" />
                  <Point X="-4.737467093253" Y="1.604453808156" Z="2.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.35" />
                  <Point X="-4.828112066969" Y="1.571990917284" Z="2.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.35" />
                  <Point X="-4.979332020417" Y="0.993999727868" Z="2.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.35" />
                  <Point X="-3.686809225746" Y="0.078610026401" Z="2.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.35" />
                  <Point X="-3.57449860997" Y="0.047637799053" Z="2.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.35" />
                  <Point X="-3.548120472366" Y="0.027592201131" Z="2.35" />
                  <Point X="-3.539556741714" Y="0" Z="2.35" />
                  <Point X="-3.540244128769" Y="-0.00221474993" Z="2.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.35" />
                  <Point X="-3.550869985154" Y="-0.03123818408" Z="2.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.35" />
                  <Point X="-4.950078563027" Y="-0.417102030355" Z="2.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.35" />
                  <Point X="-5.054556275591" Y="-0.48699169214" Z="2.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.35" />
                  <Point X="-4.972574449274" Y="-1.029162117895" Z="2.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.35" />
                  <Point X="-3.340105403614" Y="-1.322786215772" Z="2.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.35" />
                  <Point X="-3.217191148972" Y="-1.308021433436" Z="2.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.35" />
                  <Point X="-3.193248774011" Y="-1.324660125993" Z="2.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.35" />
                  <Point X="-4.406119295095" Y="-2.277392817281" Z="2.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.35" />
                  <Point X="-4.481089232743" Y="-2.388230055878" Z="2.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.35" />
                  <Point X="-4.183563070699" Y="-2.877772214814" Z="2.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.35" />
                  <Point X="-2.668645916043" Y="-2.610804830791" Z="2.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.35" />
                  <Point X="-2.571550448496" Y="-2.556780020635" Z="2.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.35" />
                  <Point X="-3.244612182378" Y="-3.766432466449" Z="2.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.35" />
                  <Point X="-3.269502584184" Y="-3.885663933607" Z="2.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.35" />
                  <Point X="-2.857829852465" Y="-4.19771553676" Z="2.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.35" />
                  <Point X="-2.242932715858" Y="-4.178229639661" Z="2.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.35" />
                  <Point X="-2.207054573177" Y="-4.143644710087" Z="2.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.35" />
                  <Point X="-1.945252552258" Y="-3.985592094797" Z="2.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.35" />
                  <Point X="-1.641334835058" Y="-4.01957572371" Z="2.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.35" />
                  <Point X="-1.420908600279" Y="-4.231550335332" Z="2.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.35" />
                  <Point X="-1.409516115033" Y="-4.852288164892" Z="2.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.35" />
                  <Point X="-1.391127820433" Y="-4.885156256632" Z="2.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.35" />
                  <Point X="-1.095130680207" Y="-4.959906482645" Z="2.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="-0.446851226788" Y="-3.629855514263" Z="2.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="-0.404921298506" Y="-3.501244939329" Z="2.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="-0.234533705304" Y="-3.277029975463" Z="2.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.35" />
                  <Point X="0.018825374057" Y="-3.210082069825" Z="2.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.35" />
                  <Point X="0.265524560927" Y="-3.300400816439" Z="2.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.35" />
                  <Point X="0.787903633613" Y="-4.902680515553" Z="2.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.35" />
                  <Point X="0.831068062241" Y="-5.011328663234" Z="2.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.35" />
                  <Point X="1.011313601206" Y="-4.978085270126" Z="2.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.35" />
                  <Point X="0.973670716939" Y="-3.396913858298" Z="2.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.35" />
                  <Point X="0.96134434992" Y="-3.254517153334" Z="2.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.35" />
                  <Point X="1.024533371954" Y="-3.014206506676" Z="2.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.35" />
                  <Point X="1.208462824234" Y="-2.874081722975" Z="2.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.35" />
                  <Point X="1.440066196293" Y="-2.86440750682" Z="2.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.35" />
                  <Point X="2.585908784678" Y="-4.227425814576" Z="2.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.35" />
                  <Point X="2.676552642333" Y="-4.317261183741" Z="2.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.35" />
                  <Point X="2.871334631609" Y="-4.190240521488" Z="2.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.35" />
                  <Point X="2.328842312604" Y="-2.822074087832" Z="2.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.35" />
                  <Point X="2.268337158424" Y="-2.706242424786" Z="2.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.35" />
                  <Point X="2.239231205178" Y="-2.492869432224" Z="2.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.35" />
                  <Point X="2.340028946148" Y="-2.31967010479" Z="2.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.35" />
                  <Point X="2.522264378726" Y="-2.235110851067" Z="2.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.35" />
                  <Point X="3.965339509275" Y="-2.988907187671" Z="2.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.35" />
                  <Point X="4.078088843168" Y="-3.028078524101" Z="2.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.35" />
                  <Point X="4.251572583624" Y="-2.779244091748" Z="2.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.35" />
                  <Point X="3.282387010763" Y="-1.683379441626" Z="2.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.35" />
                  <Point X="3.185276821845" Y="-1.602980152475" Z="2.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.35" />
                  <Point X="3.093431216829" Y="-1.445601843084" Z="2.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.35" />
                  <Point X="3.116145785658" Y="-1.2775651056" Z="2.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.35" />
                  <Point X="3.231226328077" Y="-1.152451901929" Z="2.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.35" />
                  <Point X="4.79497939161" Y="-1.299665106759" Z="2.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.35" />
                  <Point X="4.913280276789" Y="-1.286922294118" Z="2.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.35" />
                  <Point X="4.995642217184" Y="-0.916539676377" Z="2.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.35" />
                  <Point X="3.844551413938" Y="-0.24669418317" Z="2.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.35" />
                  <Point X="3.741078917693" Y="-0.216837467691" Z="2.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.35" />
                  <Point X="3.65131844551" Y="-0.162082919244" Z="2.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.35" />
                  <Point X="3.598561967316" Y="-0.089432065802" Z="2.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.35" />
                  <Point X="3.582809483109" Y="0.007178465376" Z="2.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.35" />
                  <Point X="3.60406099289" Y="0.101865819331" Z="2.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.35" />
                  <Point X="3.662316496659" Y="0.171311376551" Z="2.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.35" />
                  <Point X="4.951416403369" Y="0.543277749599" Z="2.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.35" />
                  <Point X="5.043118375074" Y="0.60061227408" Z="2.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.35" />
                  <Point X="4.974583939535" Y="1.023367223599" Z="2.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.35" />
                  <Point X="3.568458298563" Y="1.235891951323" Z="2.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.35" />
                  <Point X="3.456124971839" Y="1.222948748224" Z="2.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.35" />
                  <Point X="3.363785995021" Y="1.237381444382" Z="2.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.35" />
                  <Point X="3.2957477249" Y="1.279098531259" Z="2.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.35" />
                  <Point X="3.249948136548" Y="1.353079189599" Z="2.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.35" />
                  <Point X="3.235191330549" Y="1.438067908525" Z="2.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.35" />
                  <Point X="3.259409382524" Y="1.514914776844" Z="2.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.35" />
                  <Point X="4.363021381676" Y="2.390482962493" Z="2.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.35" />
                  <Point X="4.431773019248" Y="2.48083946111" Z="2.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.35" />
                  <Point X="4.220654170397" Y="2.825109846546" Z="2.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.35" />
                  <Point X="2.620766674689" Y="2.331020416483" Z="2.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.35" />
                  <Point X="2.50391239895" Y="2.265403509767" Z="2.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.35" />
                  <Point X="2.42443317037" Y="2.246151192594" Z="2.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.35" />
                  <Point X="2.355462751998" Y="2.2570924082" Z="2.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.35" />
                  <Point X="2.293666192035" Y="2.301562108384" Z="2.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.35" />
                  <Point X="2.253278510165" Y="2.365325273462" Z="2.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.35" />
                  <Point X="2.247124432891" Y="2.435557176932" Z="2.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.35" />
                  <Point X="3.064604936376" Y="3.891372407266" Z="2.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.35" />
                  <Point X="3.100753342881" Y="4.02208307171" Z="2.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.35" />
                  <Point X="2.72394487246" Y="4.286249628217" Z="2.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.35" />
                  <Point X="2.325169779958" Y="4.515060697623" Z="2.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.35" />
                  <Point X="1.912281979878" Y="4.704822136772" Z="2.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.35" />
                  <Point X="1.46813003542" Y="4.860024372681" Z="2.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.35" />
                  <Point X="0.812826526386" Y="5.011436253047" Z="2.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.35" />
                  <Point X="0.014358676883" Y="4.408711787962" Z="2.35" />
                  <Point X="0" Y="4.355124473572" Z="2.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>