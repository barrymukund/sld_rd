<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#159" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1677" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.729096801758" Y="20.868720703125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.474598480225" Y="21.630259765625" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.356752044678" Y="21.79098046875" />
                  <Point X="0.330496551514" Y="21.810224609375" />
                  <Point X="0.302495025635" Y="21.82433203125" />
                  <Point X="0.197633346558" Y="21.856876953125" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.00866468811" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.14168560791" Y="21.870419921875" />
                  <Point X="-0.290182922363" Y="21.82433203125" />
                  <Point X="-0.318184906006" Y="21.81022265625" />
                  <Point X="-0.344440093994" Y="21.790978515625" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.434088287354" Y="21.67088671875" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.785776489258" Y="20.611240234375" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-1.079323974609" Y="20.154646484375" />
                  <Point X="-1.197500244141" Y="20.185052734375" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.234920776367" Y="20.284966796875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.241560180664" Y="20.609716796875" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868796875" />
                  <Point X="-1.420188110352" Y="20.953462890625" />
                  <Point X="-1.556905639648" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.64302734375" Y="21.109033203125" />
                  <Point X="-1.771162231445" Y="21.117431640625" />
                  <Point X="-1.952616577148" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.149426025391" Y="21.033859375" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.466916748047" Y="20.72875390625" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.550473144531" Y="20.7550546875" />
                  <Point X="-2.801727294922" Y="20.910625" />
                  <Point X="-2.96532421875" Y="21.03658984375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.731872314453" Y="21.789716796875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575439453" Y="22.414806640625" />
                  <Point X="-2.414559570312" Y="22.444423828125" />
                  <Point X="-2.428776855469" Y="22.47325390625" />
                  <Point X="-2.446806640625" Y="22.4984140625" />
                  <Point X="-2.464155273438" Y="22.51576171875" />
                  <Point X="-2.489311523438" Y="22.533787109375" />
                  <Point X="-2.518140625" Y="22.54800390625" />
                  <Point X="-2.547758789062" Y="22.55698828125" />
                  <Point X="-2.578693115234" Y="22.555974609375" />
                  <Point X="-2.610218994141" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.394918701172" Y="22.1024765625" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.884370117188" Y="21.94536328125" />
                  <Point X="-4.082862548828" Y="22.206142578125" />
                  <Point X="-4.200157226562" Y="22.402828125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.645049072266" Y="23.08782421875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64034375" />
                  <Point X="-3.045556884766" Y="23.672015625" />
                  <Point X="-3.05255859375" Y="23.701759765625" />
                  <Point X="-3.068641357422" Y="23.727744140625" />
                  <Point X="-3.089475097656" Y="23.751701171875" />
                  <Point X="-3.112975341797" Y="23.771234375" />
                  <Point X="-3.139458007813" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608154297" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.185973144531" Y="23.680013671875" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.756446289062" Y="23.703421875" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.865111328125" Y="24.224330078125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.146985839844" Y="24.6150390625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.5109453125" Y="24.788521484375" />
                  <Point X="-3.483891845703" Y="24.80371484375" />
                  <Point X="-3.470523925781" Y="24.81280078125" />
                  <Point X="-3.442438720703" Y="24.835607421875" />
                  <Point X="-3.426203369141" Y="24.852515625" />
                  <Point X="-3.414603027344" Y="24.87288671875" />
                  <Point X="-3.403348144531" Y="24.8999609375" />
                  <Point X="-3.398641601563" Y="24.914474609375" />
                  <Point X="-3.390972167969" Y="24.946765625" />
                  <Point X="-3.388402832031" Y="24.968095703125" />
                  <Point X="-3.390692382812" Y="24.989458984375" />
                  <Point X="-3.396768066406" Y="25.0166171875" />
                  <Point X="-3.401238037109" Y="25.031076171875" />
                  <Point X="-3.414087158203" Y="25.063287109375" />
                  <Point X="-3.425393798828" Y="25.08382421875" />
                  <Point X="-3.441385498047" Y="25.100966796875" />
                  <Point X="-3.464687011719" Y="25.120451171875" />
                  <Point X="-3.477870605469" Y="25.1296953125" />
                  <Point X="-3.509707275391" Y="25.148208984375" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.402532226562" Y="25.390873046875" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.87451953125" Y="25.638865234375" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.762017578125" Y="26.2075078125" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.205875976562" Y="26.35774609375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703138916016" Y="26.305263671875" />
                  <Point X="-3.677715576172" Y="26.313279296875" />
                  <Point X="-3.641712890625" Y="26.324630859375" />
                  <Point X="-3.622784667969" Y="26.332958984375" />
                  <Point X="-3.604040283203" Y="26.343779296875" />
                  <Point X="-3.587355224609" Y="26.35601171875" />
                  <Point X="-3.573714111328" Y="26.37156640625" />
                  <Point X="-3.561299560547" Y="26.389296875" />
                  <Point X="-3.551351806641" Y="26.4074296875" />
                  <Point X="-3.541150634766" Y="26.432056640625" />
                  <Point X="-3.526704345703" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.544359130859" Y="26.6130234375" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.10097265625" Y="27.077361328125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.275557128906" Y="27.400599609375" />
                  <Point X="-4.081156982422" Y="27.73365234375" />
                  <Point X="-3.915673095703" Y="27.946361328125" />
                  <Point X="-3.750504882813" Y="28.15866015625" />
                  <Point X="-3.477020751953" Y="28.000765625" />
                  <Point X="-3.206656738281" Y="27.844669921875" />
                  <Point X="-3.187729003906" Y="27.836341796875" />
                  <Point X="-3.167086669922" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.111386474609" Y="27.822697265625" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040560546875" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012939453" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.920945068359" Y="27.885361328125" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846533691406" Y="28.07152734375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.090844726562" Y="28.56440234375" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-3.039211181641" Y="28.835091796875" />
                  <Point X="-2.70062109375" Y="29.094685546875" />
                  <Point X="-2.440001220703" Y="29.23948046875" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.125962158203" Y="29.33760546875" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028892333984" Y="29.21480078125" />
                  <Point X="-2.012312866211" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.955706176758" Y="29.168880859375" />
                  <Point X="-1.899898803711" Y="29.139828125" />
                  <Point X="-1.880625854492" Y="29.13233203125" />
                  <Point X="-1.859719482422" Y="29.126728515625" />
                  <Point X="-1.83926940918" Y="29.123580078125" />
                  <Point X="-1.818622802734" Y="29.12493359375" />
                  <Point X="-1.797307495117" Y="29.128693359375" />
                  <Point X="-1.777451416016" Y="29.134482421875" />
                  <Point X="-1.736404663086" Y="29.151486328125" />
                  <Point X="-1.678277587891" Y="29.1755625" />
                  <Point X="-1.660147583008" Y="29.185509765625" />
                  <Point X="-1.642417602539" Y="29.197923828125" />
                  <Point X="-1.626865234375" Y="29.2115625" />
                  <Point X="-1.614633666992" Y="29.228244140625" />
                  <Point X="-1.603811523438" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.582120239258" Y="29.308294921875" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.582861206055" Y="29.62171484375" />
                  <Point X="-1.584201660156" Y="29.631896484375" />
                  <Point X="-1.387955200195" Y="29.68691796875" />
                  <Point X="-0.94962322998" Y="29.80980859375" />
                  <Point X="-0.633684936523" Y="29.846787109375" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.213884414673" Y="29.58480859375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.259478546143" Y="29.70901953125" />
                  <Point X="0.307419586182" Y="29.8879375" />
                  <Point X="0.461317565918" Y="29.8718203125" />
                  <Point X="0.844044616699" Y="29.83173828125" />
                  <Point X="1.105439819336" Y="29.76862890625" />
                  <Point X="1.4810234375" Y="29.677951171875" />
                  <Point X="1.650303833008" Y="29.616552734375" />
                  <Point X="1.894643920898" Y="29.5279296875" />
                  <Point X="2.059170410156" Y="29.450984375" />
                  <Point X="2.294560302734" Y="29.340900390625" />
                  <Point X="2.453543212891" Y="29.24827734375" />
                  <Point X="2.680972167969" Y="29.11577734375" />
                  <Point X="2.830880126953" Y="29.009169921875" />
                  <Point X="2.943259033203" Y="28.929251953125" />
                  <Point X="2.504196533203" Y="28.1687734375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.124190673828" Y="27.48282421875" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.111192871094" Y="27.35221875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070556641" Y="27.24747265625" />
                  <Point X="2.157850097656" Y="27.22126953125" />
                  <Point X="2.183028320312" Y="27.1841640625" />
                  <Point X="2.194464355469" Y="27.170330078125" />
                  <Point X="2.221597900391" Y="27.14559375" />
                  <Point X="2.247800292969" Y="27.127814453125" />
                  <Point X="2.284906494141" Y="27.102634765625" />
                  <Point X="2.304947753906" Y="27.0922734375" />
                  <Point X="2.327037841797" Y="27.084005859375" />
                  <Point X="2.348964355469" Y="27.078662109375" />
                  <Point X="2.377698242188" Y="27.075197265625" />
                  <Point X="2.418389160156" Y="27.070291015625" />
                  <Point X="2.436469238281" Y="27.06984375" />
                  <Point X="2.473208496094" Y="27.074169921875" />
                  <Point X="2.506437988281" Y="27.083056640625" />
                  <Point X="2.553494873047" Y="27.095640625" />
                  <Point X="2.565288085938" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.463240966797" Y="27.615158203125" />
                  <Point X="3.967325195312" Y="27.90619140625" />
                  <Point X="3.988364013672" Y="27.876951171875" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.206840820313" Y="27.55136328125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.700301513672" Y="27.028724609375" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.18005859375" Y="26.610427734375" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606201172" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.112721435547" Y="26.48523046875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.105052490234" Y="26.336326171875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.156173339844" Y="26.205505859375" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198895019531" Y="26.145447265625" />
                  <Point X="3.216137451172" Y="26.129359375" />
                  <Point X="3.234347412109" Y="26.11603515625" />
                  <Point X="3.263171142578" Y="26.099810546875" />
                  <Point X="3.303989501953" Y="26.07683203125" />
                  <Point X="3.320522216797" Y="26.0695" />
                  <Point X="3.356119628906" Y="26.0594375" />
                  <Point X="3.395091308594" Y="26.054287109375" />
                  <Point X="3.450280273438" Y="26.046994140625" />
                  <Point X="3.462698730469" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.319116699219" Y="26.156375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.787994140625" Y="26.17081640625" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.872270996094" Y="25.7636640625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.254657226562" Y="25.473767578125" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704787597656" Y="25.325583984375" />
                  <Point X="3.681547363281" Y="25.315068359375" />
                  <Point X="3.643258789062" Y="25.2929375" />
                  <Point X="3.589037353516" Y="25.26159765625" />
                  <Point X="3.574309814453" Y="25.251095703125" />
                  <Point X="3.547530517578" Y="25.225576171875" />
                  <Point X="3.524557373047" Y="25.196302734375" />
                  <Point X="3.492024658203" Y="25.15484765625" />
                  <Point X="3.48030078125" Y="25.13556640625" />
                  <Point X="3.47052734375" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.456023193359" Y="25.0526171875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443483154297" Y="24.978123046875" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.452836669922" Y="24.9014609375" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.514997802734" Y="24.753318359375" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559993652344" Y="24.69876953125" />
                  <Point X="3.589036132812" Y="24.675841796875" />
                  <Point X="3.627324707031" Y="24.6537109375" />
                  <Point X="3.681546142578" Y="24.622369140625" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.478564941406" Y="24.40367578125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.887375" Y="24.265859375" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.821283203125" Y="23.903421875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.048400390625" Y="23.914404296875" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658691406" Y="23.994490234375" />
                  <Point X="3.29951171875" Y="23.97815625" />
                  <Point X="3.193094482422" Y="23.95502734375" />
                  <Point X="3.163974609375" Y="23.94340234375" />
                  <Point X="3.136147705078" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.066975585938" Y="23.85141015625" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.963247558594" Y="23.623888671875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.018038085938" Y="23.36731640625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.817755126953" Y="22.6964921875" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.055034179688" Y="22.151072265625" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="3.356727294922" Y="22.5021796875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.664787841797" Y="22.856326171875" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.370533447266" Y="22.82571875" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531494141" Y="22.709560546875" />
                  <Point X="2.165427978516" Y="22.635259765625" />
                  <Point X="2.110052490234" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.111827392578" Y="22.3473046875" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.606219238281" Y="21.386876953125" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781833496094" Y="20.888345703125" />
                  <Point X="2.703836914063" Y="20.837859375" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="2.182652099609" Y="21.5130390625" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.747507446289" Y="22.07781640625" />
                  <Point X="1.721923461914" Y="22.099443359375" />
                  <Point X="1.63371472168" Y="22.15615234375" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.248833984375" />
                  <Point X="1.448365844727" Y="22.256564453125" />
                  <Point X="1.417100341797" Y="22.258880859375" />
                  <Point X="1.320629150391" Y="22.25000390625" />
                  <Point X="1.184013549805" Y="22.23743359375" />
                  <Point X="1.156362792969" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104594970703" Y="22.204537109375" />
                  <Point X="1.030102294922" Y="22.14259765625" />
                  <Point X="0.924611206055" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.853351379395" Y="21.871716796875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.94851574707" Y="20.698748046875" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.975718505859" Y="20.12992578125" />
                  <Point X="0.929315246582" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058415405273" Y="20.247361328125" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.140733520508" Y="20.27256640625" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759155273" Y="20.490669921875" />
                  <Point X="-1.123333984375" Y="20.502306640625" />
                  <Point X="-1.148385620117" Y="20.62825" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573242188" Y="20.905138671875" />
                  <Point X="-1.250208251953" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94022265625" />
                  <Point X="-1.357550292969" Y="21.024888671875" />
                  <Point X="-1.494267822266" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.533023681641" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315795898" Y="21.194525390625" />
                  <Point X="-1.621456665039" Y="21.201552734375" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.764948852539" Y="21.212228515625" />
                  <Point X="-1.946403198242" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667724609" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095436279297" Y="21.184189453125" />
                  <Point X="-2.202204833984" Y="21.112849609375" />
                  <Point X="-2.353402587891" Y="21.011822265625" />
                  <Point X="-2.359682373047" Y="21.007244140625" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410470947266" Y="20.958369140625" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.747611572266" Y="20.98885546875" />
                  <Point X="-2.907366699219" Y="21.111861328125" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.649599853516" Y="21.742216796875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310626220703" Y="22.411697265625" />
                  <Point X="-2.314666015625" Y="22.4423828125" />
                  <Point X="-2.323650146484" Y="22.472" />
                  <Point X="-2.329356445312" Y="22.48644140625" />
                  <Point X="-2.343573730469" Y="22.515271484375" />
                  <Point X="-2.351556884766" Y="22.52858984375" />
                  <Point X="-2.369586669922" Y="22.55375" />
                  <Point X="-2.379633300781" Y="22.565591796875" />
                  <Point X="-2.396981933594" Y="22.582939453125" />
                  <Point X="-2.408822509766" Y="22.592984375" />
                  <Point X="-2.433978759766" Y="22.611009765625" />
                  <Point X="-2.447294433594" Y="22.618990234375" />
                  <Point X="-2.476123535156" Y="22.63320703125" />
                  <Point X="-2.490564208984" Y="22.6389140625" />
                  <Point X="-2.520182373047" Y="22.6478984375" />
                  <Point X="-2.550870117188" Y="22.6519375" />
                  <Point X="-2.581804443359" Y="22.650923828125" />
                  <Point X="-2.597228515625" Y="22.6491484375" />
                  <Point X="-2.628754394531" Y="22.642876953125" />
                  <Point X="-2.643685058594" Y="22.63861328125" />
                  <Point X="-2.672649658203" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.442418701172" Y="22.184748046875" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.808776855469" Y="22.00290234375" />
                  <Point X="-4.004020019531" Y="22.259412109375" />
                  <Point X="-4.118564453125" Y="22.451486328125" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.587216796875" Y="23.012455078125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748535156" Y="23.631626953125" />
                  <Point X="-2.948577880859" Y="23.646955078125" />
                  <Point X="-2.950787109375" Y="23.678626953125" />
                  <Point X="-2.953084472656" Y="23.693783203125" />
                  <Point X="-2.960086181641" Y="23.72352734375" />
                  <Point X="-2.971779541016" Y="23.7517578125" />
                  <Point X="-2.987862304688" Y="23.7777421875" />
                  <Point X="-2.996956054688" Y="23.790083984375" />
                  <Point X="-3.017789794922" Y="23.814041015625" />
                  <Point X="-3.02875" Y="23.824759765625" />
                  <Point X="-3.052250244141" Y="23.84429296875" />
                  <Point X="-3.064790283203" Y="23.853107421875" />
                  <Point X="-3.091272949219" Y="23.868693359375" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.134700683594" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891142578125" />
                  <Point X="-3.181687255859" Y="23.897623046875" />
                  <Point X="-3.197304443359" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.198373046875" Y="23.774201171875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.664401367188" Y="23.72693359375" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.771068359375" Y="24.23778125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.122397949219" Y="24.523275390625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497043457031" Y="24.691607421875" />
                  <Point X="-3.475112792969" Y="24.7005390625" />
                  <Point X="-3.464426757812" Y="24.705689453125" />
                  <Point X="-3.437373291016" Y="24.7208828125" />
                  <Point X="-3.430489501953" Y="24.72514453125" />
                  <Point X="-3.410637451172" Y="24.7390546875" />
                  <Point X="-3.382552246094" Y="24.761861328125" />
                  <Point X="-3.3739140625" Y="24.76980859375" />
                  <Point X="-3.357678710938" Y="24.786716796875" />
                  <Point X="-3.343650146484" Y="24.805505859375" />
                  <Point X="-3.332049804688" Y="24.825876953125" />
                  <Point X="-3.326880859375" Y="24.836419921875" />
                  <Point X="-3.315625976562" Y="24.863494140625" />
                  <Point X="-3.312980957031" Y="24.87065625" />
                  <Point X="-3.306212890625" Y="24.892521484375" />
                  <Point X="-3.298543457031" Y="24.9248125" />
                  <Point X="-3.296654052734" Y="24.935404296875" />
                  <Point X="-3.294084716797" Y="24.956734375" />
                  <Point X="-3.293943847656" Y="24.97821875" />
                  <Point X="-3.296233398438" Y="24.99958203125" />
                  <Point X="-3.297983886719" Y="25.01019921875" />
                  <Point X="-3.304059570312" Y="25.037357421875" />
                  <Point X="-3.306006347656" Y="25.04467578125" />
                  <Point X="-3.312999511719" Y="25.066275390625" />
                  <Point X="-3.325848632812" Y="25.098486328125" />
                  <Point X="-3.330865966797" Y="25.109103515625" />
                  <Point X="-3.342172607422" Y="25.129640625" />
                  <Point X="-3.355927246094" Y="25.148626953125" />
                  <Point X="-3.371918945313" Y="25.16576953125" />
                  <Point X="-3.3804453125" Y="25.173845703125" />
                  <Point X="-3.403746826172" Y="25.193330078125" />
                  <Point X="-3.410146240234" Y="25.198234375" />
                  <Point X="-3.430114013672" Y="25.211818359375" />
                  <Point X="-3.461950683594" Y="25.23033203125" />
                  <Point X="-3.473210693359" Y="25.23591796875" />
                  <Point X="-3.496379394531" Y="25.24555859375" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.377944335938" Y="25.48263671875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.78054296875" Y="25.624958984375" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.670324707031" Y="26.18266015625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.218276367188" Y="26.26355859375" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703613281" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704888671875" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.674572998047" Y="26.21466015625" />
                  <Point X="-3.649149658203" Y="26.22267578125" />
                  <Point X="-3.613146972656" Y="26.23402734375" />
                  <Point X="-3.603453857422" Y="26.23767578125" />
                  <Point X="-3.584525634766" Y="26.24600390625" />
                  <Point X="-3.575290527344" Y="26.25068359375" />
                  <Point X="-3.556546142578" Y="26.26150390625" />
                  <Point X="-3.547870605469" Y="26.2671640625" />
                  <Point X="-3.531185546875" Y="26.279396484375" />
                  <Point X="-3.515930419922" Y="26.293373046875" />
                  <Point X="-3.502289306641" Y="26.308927734375" />
                  <Point X="-3.495893798828" Y="26.317078125" />
                  <Point X="-3.483479248047" Y="26.33480859375" />
                  <Point X="-3.478010009766" Y="26.343603515625" />
                  <Point X="-3.468062255859" Y="26.361736328125" />
                  <Point X="-3.463583740234" Y="26.37107421875" />
                  <Point X="-3.453382568359" Y="26.395701171875" />
                  <Point X="-3.438936279297" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429710449219" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012207031" Y="26.60453125" />
                  <Point X="-3.443509521484" Y="26.623810546875" />
                  <Point X="-3.447784667969" Y="26.63324609375" />
                  <Point X="-3.460093505859" Y="26.656890625" />
                  <Point X="-3.477524414062" Y="26.690375" />
                  <Point X="-3.482800048828" Y="26.699287109375" />
                  <Point X="-3.494291992188" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521502197266" Y="26.748912109375" />
                  <Point X="-3.536443603516" Y="26.76321484375" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.043140380859" Y="27.15273046875" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.193510742187" Y="27.3527109375" />
                  <Point X="-4.002296875" Y="27.6803046875" />
                  <Point X="-3.840692138672" Y="27.88802734375" />
                  <Point X="-3.726339111328" Y="28.03501171875" />
                  <Point X="-3.524520507812" Y="27.9184921875" />
                  <Point X="-3.254156494141" Y="27.762396484375" />
                  <Point X="-3.244916503906" Y="27.75771484375" />
                  <Point X="-3.225988769531" Y="27.74938671875" />
                  <Point X="-3.216301025391" Y="27.745740234375" />
                  <Point X="-3.195658691406" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165330810547" Y="27.73262109375" />
                  <Point X="-3.155073730469" Y="27.73115625" />
                  <Point X="-3.119666015625" Y="27.72805859375" />
                  <Point X="-3.069524414062" Y="27.723671875" />
                  <Point X="-3.059173095703" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155761719" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938449951172" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902746826172" Y="27.773193359375" />
                  <Point X="-2.886614990234" Y="27.786140625" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.853770019531" Y="27.818185546875" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751895263672" Y="28.079806640625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.008572265625" Y="28.61190234375" />
                  <Point X="-3.059386474609" Y="28.6999140625" />
                  <Point X="-2.981409179688" Y="28.75969921875" />
                  <Point X="-2.648369384766" Y="29.015037109375" />
                  <Point X="-2.393863769531" Y="29.156435546875" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111820556641" Y="29.164048828125" />
                  <Point X="-2.097517578125" Y="29.149107421875" />
                  <Point X="-2.089958007813" Y="29.14202734375" />
                  <Point X="-2.073378417969" Y="29.128115234375" />
                  <Point X="-2.065094726563" Y="29.121900390625" />
                  <Point X="-2.047896850586" Y="29.110408203125" />
                  <Point X="-2.038982299805" Y="29.105130859375" />
                  <Point X="-1.999573486328" Y="29.084615234375" />
                  <Point X="-1.943766235352" Y="29.0555625" />
                  <Point X="-1.934335571289" Y="29.0512890625" />
                  <Point X="-1.9150625" Y="29.04379296875" />
                  <Point X="-1.905220458984" Y="29.0405703125" />
                  <Point X="-1.884314086914" Y="29.034966796875" />
                  <Point X="-1.874175048828" Y="29.032833984375" />
                  <Point X="-1.853724975586" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812408325195" Y="29.03013671875" />
                  <Point X="-1.802120727539" Y="29.031376953125" />
                  <Point X="-1.780805419922" Y="29.03513671875" />
                  <Point X="-1.770717163086" Y="29.037490234375" />
                  <Point X="-1.750861206055" Y="29.043279296875" />
                  <Point X="-1.741093261719" Y="29.04671484375" />
                  <Point X="-1.700046508789" Y="29.06371875" />
                  <Point X="-1.641919311523" Y="29.087794921875" />
                  <Point X="-1.632580810547" Y="29.092275390625" />
                  <Point X="-1.614450683594" Y="29.10222265625" />
                  <Point X="-1.605659545898" Y="29.107689453125" />
                  <Point X="-1.58792956543" Y="29.120103515625" />
                  <Point X="-1.579780761719" Y="29.126498046875" />
                  <Point X="-1.564228393555" Y="29.14013671875" />
                  <Point X="-1.550253173828" Y="29.155388671875" />
                  <Point X="-1.538021606445" Y="29.1720703125" />
                  <Point X="-1.532361694336" Y="29.180744140625" />
                  <Point X="-1.521539550781" Y="29.19948828125" />
                  <Point X="-1.516857299805" Y="29.2087265625" />
                  <Point X="-1.508526123047" Y="29.22766015625" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.491516967773" Y="29.279728515625" />
                  <Point X="-1.472597412109" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465991210938" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.362308959961" Y="29.5954453125" />
                  <Point X="-0.931163696289" Y="29.7163203125" />
                  <Point X="-0.622641235352" Y="29.752431640625" />
                  <Point X="-0.36522265625" Y="29.78255859375" />
                  <Point X="-0.30564730835" Y="29.560220703125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.351241546631" Y="29.684431640625" />
                  <Point X="0.378190734863" Y="29.7850078125" />
                  <Point X="0.451422607422" Y="29.777337890625" />
                  <Point X="0.827865356445" Y="29.7379140625" />
                  <Point X="1.083144287109" Y="29.67628125" />
                  <Point X="1.453606933594" Y="29.58683984375" />
                  <Point X="1.617911865234" Y="29.52724609375" />
                  <Point X="1.858240966797" Y="29.440078125" />
                  <Point X="2.018924804688" Y="29.3649296875" />
                  <Point X="2.250437744141" Y="29.256658203125" />
                  <Point X="2.405720458984" Y="29.16619140625" />
                  <Point X="2.629427246094" Y="29.035859375" />
                  <Point X="2.775823242188" Y="28.93175" />
                  <Point X="2.817778564453" Y="28.9019140625" />
                  <Point X="2.421924072266" Y="28.2162734375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301269531" Y="27.540595703125" />
                  <Point X="2.032415405273" Y="27.507365234375" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411376953" Y="27.369578125" />
                  <Point X="2.016876220703" Y="27.340845703125" />
                  <Point X="2.021782714844" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.04532043457" Y="27.223884765625" />
                  <Point X="2.055681884766" Y="27.20384375" />
                  <Point X="2.061458740234" Y="27.1941328125" />
                  <Point X="2.07923828125" Y="27.1679296875" />
                  <Point X="2.104416503906" Y="27.13082421875" />
                  <Point X="2.109807617188" Y="27.123634765625" />
                  <Point X="2.130462158203" Y="27.100125" />
                  <Point X="2.157595703125" Y="27.075388671875" />
                  <Point X="2.168257080078" Y="27.066982421875" />
                  <Point X="2.194459472656" Y="27.049203125" />
                  <Point X="2.231565673828" Y="27.0240234375" />
                  <Point X="2.24127734375" Y="27.01824609375" />
                  <Point X="2.261318603516" Y="27.007884765625" />
                  <Point X="2.271648193359" Y="27.00330078125" />
                  <Point X="2.29373828125" Y="26.995033203125" />
                  <Point X="2.304543701172" Y="26.99170703125" />
                  <Point X="2.326470214844" Y="26.98636328125" />
                  <Point X="2.337591308594" Y="26.984345703125" />
                  <Point X="2.366325195312" Y="26.980880859375" />
                  <Point X="2.407016113281" Y="26.975974609375" />
                  <Point X="2.416039794922" Y="26.9753203125" />
                  <Point X="2.447579101562" Y="26.97549609375" />
                  <Point X="2.484318359375" Y="26.979822265625" />
                  <Point X="2.497752197266" Y="26.98239453125" />
                  <Point X="2.530981689453" Y="26.99128125" />
                  <Point X="2.578038574219" Y="27.003865234375" />
                  <Point X="2.584009277344" Y="27.005673828125" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627658691406" Y="27.023576171875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.510740966797" Y="27.53288671875" />
                  <Point X="3.940402832031" Y="27.780951171875" />
                  <Point X="4.043957275391" Y="27.63703515625" />
                  <Point X="4.125563964844" Y="27.5021796875" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.642469238281" Y="27.10409375" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116943359" Y="26.725212890625" />
                  <Point X="3.134668457031" Y="26.7066015625" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.104661132812" Y="26.66822265625" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065635742188" Y="26.6166015625" />
                  <Point X="3.049739257812" Y="26.58937109375" />
                  <Point X="3.034762939453" Y="26.555544921875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.021231689453" Y="26.51081640625" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.012012451172" Y="26.31712890625" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682861328" Y="26.228609375" />
                  <Point X="3.050286376953" Y="26.195369140625" />
                  <Point X="3.056919189453" Y="26.1835234375" />
                  <Point X="3.076809570312" Y="26.153291015625" />
                  <Point X="3.104976806641" Y="26.110478515625" />
                  <Point X="3.111739257813" Y="26.101423828125" />
                  <Point X="3.126293701172" Y="26.084177734375" />
                  <Point X="3.134085693359" Y="26.075986328125" />
                  <Point X="3.151328125" Y="26.0598984375" />
                  <Point X="3.160039306641" Y="26.05269140625" />
                  <Point X="3.178249267578" Y="26.0393671875" />
                  <Point X="3.187748046875" Y="26.03325" />
                  <Point X="3.216571777344" Y="26.017025390625" />
                  <Point X="3.257390136719" Y="25.994046875" />
                  <Point X="3.265475830078" Y="25.98998828125" />
                  <Point X="3.294680664062" Y="25.97808203125" />
                  <Point X="3.330278076172" Y="25.96801953125" />
                  <Point X="3.343672851563" Y="25.965255859375" />
                  <Point X="3.38264453125" Y="25.96010546875" />
                  <Point X="3.437833496094" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708984375" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.331516601562" Y="26.0621875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.778401855469" Y="25.749048828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.230069335938" Y="25.56553125" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686022949219" Y="25.419541015625" />
                  <Point X="3.665625" Y="25.41213671875" />
                  <Point X="3.642384765625" Y="25.40162109375" />
                  <Point X="3.634007080078" Y="25.397318359375" />
                  <Point X="3.595718505859" Y="25.3751875" />
                  <Point X="3.541497070312" Y="25.34384765625" />
                  <Point X="3.533881347656" Y="25.338947265625" />
                  <Point X="3.508771728516" Y="25.319869140625" />
                  <Point X="3.481992431641" Y="25.294349609375" />
                  <Point X="3.472796386719" Y="25.2842265625" />
                  <Point X="3.449823242188" Y="25.254953125" />
                  <Point X="3.417290527344" Y="25.213498046875" />
                  <Point X="3.410852294922" Y="25.204205078125" />
                  <Point X="3.399128417969" Y="25.184923828125" />
                  <Point X="3.393842773438" Y="25.174935546875" />
                  <Point X="3.384069335938" Y="25.15347265625" />
                  <Point X="3.380006103516" Y="25.1429296875" />
                  <Point X="3.373159667969" Y="25.1214296875" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.36271875" Y="25.070486328125" />
                  <Point X="3.351874267578" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584472656" Y="24.973736328125" />
                  <Point X="3.350280273438" Y="24.93705859375" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.359532470703" Y="24.883591796875" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.81601171875" />
                  <Point X="3.380005859375" Y="24.79451171875" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399128417969" Y="24.752515625" />
                  <Point X="3.410852294922" Y="24.733234375" />
                  <Point X="3.417290527344" Y="24.72394140625" />
                  <Point X="3.440263671875" Y="24.69466796875" />
                  <Point X="3.472796386719" Y="24.653212890625" />
                  <Point X="3.47871875" Y="24.646365234375" />
                  <Point X="3.501128417969" Y="24.624205078125" />
                  <Point X="3.530170898438" Y="24.60127734375" />
                  <Point X="3.541495849609" Y="24.593591796875" />
                  <Point X="3.579784423828" Y="24.5714609375" />
                  <Point X="3.634005859375" Y="24.540119140625" />
                  <Point X="3.639499511719" Y="24.5371796875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.453977050781" Y="24.311912109375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.7286640625" Y="23.924556640625" />
                  <Point X="4.727802246094" Y="23.920779296875" />
                  <Point X="4.060800292969" Y="24.008591796875" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720214844" Y="24.089158203125" />
                  <Point X="3.354480712891" Y="24.087322265625" />
                  <Point X="3.279333740234" Y="24.07098828125" />
                  <Point X="3.172916503906" Y="24.047859375" />
                  <Point X="3.157872314453" Y="24.043255859375" />
                  <Point X="3.128752441406" Y="24.031630859375" />
                  <Point X="3.114676757812" Y="24.024609375" />
                  <Point X="3.086849853516" Y="24.007716796875" />
                  <Point X="3.074125244141" Y="23.99846875" />
                  <Point X="3.050374755859" Y="23.977998046875" />
                  <Point X="3.039348876953" Y="23.966775390625" />
                  <Point X="2.993927246094" Y="23.912146484375" />
                  <Point X="2.929604736328" Y="23.834787109375" />
                  <Point X="2.921325683594" Y="23.82315234375" />
                  <Point X="2.906605957031" Y="23.798771484375" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.868647216797" Y="23.63259375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.938128173828" Y="23.31594140625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.759922851562" Y="22.621123046875" />
                  <Point X="4.087170898438" Y="22.370017578125" />
                  <Point X="4.045496337891" Y="22.302580078125" />
                  <Point X="4.0012734375" Y="22.23974609375" />
                  <Point X="3.404227294922" Y="22.584451171875" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.681671875" Y="22.949814453125" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444847167969" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589111328" Y="22.948890625" />
                  <Point X="2.3262890625" Y="22.909787109375" />
                  <Point X="2.221071044922" Y="22.854412109375" />
                  <Point X="2.208967285156" Y="22.846828125" />
                  <Point X="2.186038085938" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144942382812" Y="22.788841796875" />
                  <Point X="2.128047607422" Y="22.76591015625" />
                  <Point X="2.120463134766" Y="22.7538046875" />
                  <Point X="2.081359619141" Y="22.67950390625" />
                  <Point X="2.02598425293" Y="22.574287109375" />
                  <Point X="2.019835571289" Y="22.559806640625" />
                  <Point X="2.010012084961" Y="22.53003125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.01833972168" Y="22.330421875" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.523946777344" Y="21.339376953125" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.963916015625" />
                  <Point X="2.258020751953" Y="21.57087109375" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828655639648" Y="22.1298515625" />
                  <Point X="1.808837158203" Y="22.1503671875" />
                  <Point X="1.783253173828" Y="22.171994140625" />
                  <Point X="1.773297607422" Y="22.179353515625" />
                  <Point X="1.685088867188" Y="22.2360625" />
                  <Point X="1.560174316406" Y="22.31637109375" />
                  <Point X="1.546284667969" Y="22.32375390625" />
                  <Point X="1.517470947266" Y="22.336126953125" />
                  <Point X="1.502547241211" Y="22.3411171875" />
                  <Point X="1.470926635742" Y="22.34884765625" />
                  <Point X="1.455385009766" Y="22.3513046875" />
                  <Point X="1.424119506836" Y="22.35362109375" />
                  <Point X="1.408395507812" Y="22.35348046875" />
                  <Point X="1.311924438477" Y="22.344603515625" />
                  <Point X="1.17530871582" Y="22.332033203125" />
                  <Point X="1.161225952148" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120007324219" Y="22.318369140625" />
                  <Point X="1.092621582031" Y="22.307025390625" />
                  <Point X="1.079875854492" Y="22.300583984375" />
                  <Point X="1.055493896484" Y="22.28586328125" />
                  <Point X="1.043857055664" Y="22.277583984375" />
                  <Point X="0.969364501953" Y="22.21564453125" />
                  <Point X="0.863873474121" Y="22.127931640625" />
                  <Point X="0.852653625488" Y="22.11691015625" />
                  <Point X="0.83218359375" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.760518859863" Y="21.89189453125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091308594" Y="20.84766015625" />
                  <Point X="0.820859741211" Y="20.89330859375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407165527" Y="21.58679296875" />
                  <Point X="0.552642700195" Y="21.684427734375" />
                  <Point X="0.456679534912" Y="21.822693359375" />
                  <Point X="0.446671112061" Y="21.834828125" />
                  <Point X="0.424787750244" Y="21.857283203125" />
                  <Point X="0.412912780762" Y="21.867603515625" />
                  <Point X="0.386657348633" Y="21.88684765625" />
                  <Point X="0.373240112305" Y="21.89506640625" />
                  <Point X="0.345238677979" Y="21.909173828125" />
                  <Point X="0.330654296875" Y="21.9150625" />
                  <Point X="0.225792526245" Y="21.947607421875" />
                  <Point X="0.077295059204" Y="21.9936953125" />
                  <Point X="0.063377120972" Y="21.996890625" />
                  <Point X="0.035217838287" Y="22.00116015625" />
                  <Point X="0.020976644516" Y="22.002234375" />
                  <Point X="-0.008664708138" Y="22.002234375" />
                  <Point X="-0.022905902863" Y="22.00116015625" />
                  <Point X="-0.051065185547" Y="21.996890625" />
                  <Point X="-0.064983123779" Y="21.9936953125" />
                  <Point X="-0.169844909668" Y="21.961150390625" />
                  <Point X="-0.318342071533" Y="21.9150625" />
                  <Point X="-0.332930755615" Y="21.909169921875" />
                  <Point X="-0.360932647705" Y="21.895060546875" />
                  <Point X="-0.374346160889" Y="21.88684375" />
                  <Point X="-0.400601287842" Y="21.867599609375" />
                  <Point X="-0.412475646973" Y="21.85728125" />
                  <Point X="-0.434359039307" Y="21.834826171875" />
                  <Point X="-0.44436819458" Y="21.822689453125" />
                  <Point X="-0.5121328125" Y="21.725052734375" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.877539428711" Y="20.635828125" />
                  <Point X="-0.985425109863" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.67712497766" Y="28.006598045848" />
                  <Point X="-4.168944486956" Y="27.249262414962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.594739562871" Y="27.959033006026" />
                  <Point X="-4.093342620324" Y="27.191251627003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941770478169" Y="28.79008976843" />
                  <Point X="-3.031593084011" Y="28.651775084735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.512354182697" Y="27.911467912903" />
                  <Point X="-4.017740850149" Y="27.133240690515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.553829856637" Y="26.307736011937" />
                  <Point X="-4.69558042396" Y="26.089459279715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.716171877932" Y="28.963053695236" />
                  <Point X="-2.978282424413" Y="28.559438848064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429969002303" Y="27.863902512145" />
                  <Point X="-3.942139270618" Y="27.07522946046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.449477279975" Y="26.293997435039" />
                  <Point X="-4.750025644503" Y="25.831193538571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.524417186245" Y="29.083902573029" />
                  <Point X="-2.924971902172" Y="28.467102399881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.347583821909" Y="27.816337111388" />
                  <Point X="-3.866537691086" Y="27.017218230406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345124703313" Y="26.280258858142" />
                  <Point X="-4.783453159866" Y="25.605292225253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347204473125" Y="29.182358767521" />
                  <Point X="-2.871661379932" Y="28.374765951698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.265198641515" Y="27.76877171063" />
                  <Point X="-3.790936111555" Y="26.959207000352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24077212665" Y="26.266520281244" />
                  <Point X="-4.694711999433" Y="25.567514175468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.184723236563" Y="29.258130477396" />
                  <Point X="-2.818350857692" Y="28.282429503514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.174248477191" Y="27.734395228542" />
                  <Point X="-3.715334532023" Y="26.901195770298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136419229658" Y="26.252782197611" />
                  <Point X="-4.598226689429" Y="25.541661070282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.123371942887" Y="29.178175731429" />
                  <Point X="-2.768730775004" Y="28.184410276763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.067970726871" Y="27.723621159109" />
                  <Point X="-3.639732952492" Y="26.843184540243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.032066244633" Y="26.239044249536" />
                  <Point X="-4.501741379425" Y="25.515807965096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.052227059364" Y="29.113301791337" />
                  <Point X="-2.748681403427" Y="28.040856148016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.936598387833" Y="27.751489367625" />
                  <Point X="-3.56413137296" Y="26.785173310189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.927713259609" Y="26.225306301462" />
                  <Point X="-4.405256069421" Y="25.489954859911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.96819134778" Y="29.06827798573" />
                  <Point X="-3.494919472533" Y="26.717322837152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.823360274584" Y="26.211568353387" />
                  <Point X="-4.308770770406" Y="25.464101737803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.877482603155" Y="29.033529749906" />
                  <Point X="-3.443083681503" Y="26.622715502047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.713132369862" Y="26.206876988318" />
                  <Point X="-4.21228547573" Y="25.438248609014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.407529259397" Y="29.582766985002" />
                  <Point X="-1.46937592152" Y="29.48753147687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.759515174165" Y="29.040756207095" />
                  <Point X="-3.426825794668" Y="26.473322998787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.569082153332" Y="26.254267416199" />
                  <Point X="-4.115800181054" Y="25.412395480225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.269040225015" Y="29.621593943336" />
                  <Point X="-1.491957424967" Y="29.278331557298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.600367354233" Y="29.111394905492" />
                  <Point X="-4.019314886378" Y="25.386542351435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676313273854" Y="24.374853553279" />
                  <Point X="-4.770032492732" Y="24.230538611693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130551553701" Y="29.660420342593" />
                  <Point X="-3.922829591703" Y="25.360689222646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.539175560622" Y="24.411599659519" />
                  <Point X="-4.749587930808" Y="24.087593022717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.992062882388" Y="29.69924674185" />
                  <Point X="-3.826344297027" Y="25.334836093857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.402037847389" Y="24.44834576576" />
                  <Point X="-4.722589596868" Y="23.954739357647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.862479973667" Y="29.724359469315" />
                  <Point X="-3.729859002351" Y="25.308982965068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.264900134156" Y="24.485091872001" />
                  <Point X="-4.690612987067" Y="23.829551565158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.739887106083" Y="29.738708477338" />
                  <Point X="-3.633373707675" Y="25.283129836279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127762420924" Y="24.521837978242" />
                  <Point X="-4.652072348057" Y="23.714471491269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.617294280378" Y="29.753057420871" />
                  <Point X="-3.536888412999" Y="25.257276707489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990624742663" Y="24.558584030631" />
                  <Point X="-4.528208091659" Y="23.730778266382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.494702372983" Y="29.767404950333" />
                  <Point X="-3.446824344712" Y="25.22153575716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853487065826" Y="24.595330080827" />
                  <Point X="-4.404343835261" Y="23.747085041494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.372110465588" Y="29.781752479794" />
                  <Point X="-3.370648945082" Y="25.164408132572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716349388989" Y="24.632076131024" />
                  <Point X="-4.280479578863" Y="23.763391816607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.333995403426" Y="29.666017075026" />
                  <Point X="-3.316082902249" Y="25.07400501656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579211712152" Y="24.668822181221" />
                  <Point X="-4.156615282291" Y="23.779698653581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.300909224532" Y="29.542537869109" />
                  <Point X="-3.298107296614" Y="24.927257568297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.428418795319" Y="24.726595457058" />
                  <Point X="-4.032750906729" Y="23.79600561219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.267823149013" Y="29.419058504007" />
                  <Point X="-3.908886531167" Y="23.812312570799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.234737073494" Y="29.295579138904" />
                  <Point X="-3.785022155605" Y="23.828619529408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.189947730592" Y="29.190121225208" />
                  <Point X="-3.661157780043" Y="23.844926488017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.120996249489" Y="29.121869741578" />
                  <Point X="-3.537293404481" Y="23.861233446627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.420076292987" Y="29.780620939035" />
                  <Point X="0.346767982042" Y="29.667736039453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.029434950305" Y="29.088434324649" />
                  <Point X="-3.413429028918" Y="23.877540405236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.526137366481" Y="29.769513216549" />
                  <Point X="0.267200409942" Y="29.370785269338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.093111497981" Y="29.10271185322" />
                  <Point X="-3.289564653356" Y="23.893847363845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.134825959838" Y="22.592259092726" />
                  <Point X="-4.170112099679" Y="22.537923202276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.632198582208" Y="29.758405713083" />
                  <Point X="-3.174753353096" Y="23.896213808982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.909040631578" Y="22.765510555472" />
                  <Point X="-4.115886517521" Y="22.4469958228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.738259797935" Y="29.747298209616" />
                  <Point X="-3.082647471075" Y="23.863616976084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683255303319" Y="22.938762018219" />
                  <Point X="-4.061661237024" Y="22.356067978807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.843058122431" Y="29.73424602419" />
                  <Point X="-3.008503001972" Y="23.803361992732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.457470683544" Y="23.112012389994" />
                  <Point X="-4.007435956526" Y="22.265140134815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.940979658438" Y="29.710604513108" />
                  <Point X="-2.956624282666" Y="23.708820761376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.231686588191" Y="23.285261954232" />
                  <Point X="-3.946746459098" Y="22.184166311992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.038901194446" Y="29.686963002025" />
                  <Point X="-3.885622369206" Y="22.103861702878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.136822777704" Y="29.663321563701" />
                  <Point X="-3.824498279314" Y="22.023557093764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.234744399905" Y="29.639680185345" />
                  <Point X="-3.704994209495" Y="22.033149770327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.332666022107" Y="29.61603880699" />
                  <Point X="-3.523773478497" Y="22.137777771124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.430587644309" Y="29.592397428635" />
                  <Point X="-3.342552792612" Y="22.242405702453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.523734957728" Y="29.561404259458" />
                  <Point X="-3.161332143477" Y="22.347033577192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.615415011174" Y="29.528151708056" />
                  <Point X="-2.980111494342" Y="22.451661451931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.707095047109" Y="29.494899129691" />
                  <Point X="-2.798890845207" Y="22.55628932667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.798775082554" Y="29.46164655057" />
                  <Point X="-2.629529391969" Y="22.642655641148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.888770609517" Y="29.425800055855" />
                  <Point X="-2.514056843553" Y="22.646040319153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.97565660906" Y="29.385165308814" />
                  <Point X="-2.426849062657" Y="22.605901071943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.062542817705" Y="29.344530883761" />
                  <Point X="-2.357969616086" Y="22.53753866486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.149429233775" Y="29.303896778116" />
                  <Point X="-2.3132879135" Y="22.431914999612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.236315649846" Y="29.263262672471" />
                  <Point X="-2.663150200697" Y="21.718746867814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319262093694" Y="29.216561541643" />
                  <Point X="-2.931783032432" Y="21.130661128511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.40144386802" Y="29.168682923007" />
                  <Point X="-2.856267788039" Y="21.072516954003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483625351588" Y="29.120803856643" />
                  <Point X="-2.780752509912" Y="21.014372831438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.565806819194" Y="29.072924765702" />
                  <Point X="-2.702277843416" Y="20.96078576734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646928334981" Y="29.02341349209" />
                  <Point X="-2.621488641652" Y="20.910762775007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.724416725433" Y="28.968307696065" />
                  <Point X="-2.540699439887" Y="20.860739782674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.801905267912" Y="28.913202134142" />
                  <Point X="-2.310843537446" Y="21.040259379986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.096099848137" Y="27.651929643376" />
                  <Point X="-2.110740492815" Y="21.173963593982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015384950773" Y="27.353212147284" />
                  <Point X="-1.965272113401" Y="21.223537801201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.045057715364" Y="27.224476744273" />
                  <Point X="-1.855488358779" Y="21.218162504955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100889270969" Y="27.136022347041" />
                  <Point X="-1.746838292279" Y="21.211041482092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.16900094859" Y="27.066477679452" />
                  <Point X="-1.638188416961" Y="21.203920164833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.248521368357" Y="27.014500934175" />
                  <Point X="-1.543275424348" Y="21.175645903184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341877223351" Y="26.983828890864" />
                  <Point X="-1.466089104857" Y="21.12007495867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449919310057" Y="26.975771661216" />
                  <Point X="-1.393917530059" Y="21.056781984501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.582270471532" Y="27.005147124107" />
                  <Point X="-1.321745880576" Y="20.993489125337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754854740216" Y="27.096476139176" />
                  <Point X="-1.250268686127" Y="20.929126899198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936075168183" Y="27.201103673345" />
                  <Point X="-1.19500189001" Y="20.839802848617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.117295596149" Y="27.305731207514" />
                  <Point X="-0.439970040992" Y="21.828022485899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.640126879569" Y="21.519807982907" />
                  <Point X="-1.164969219895" Y="20.711621651513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.298516024115" Y="27.410358741683" />
                  <Point X="-0.258011330609" Y="21.933786875295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720632935194" Y="21.221412074891" />
                  <Point X="-1.138408896221" Y="20.578093509782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.479736452081" Y="27.514986275852" />
                  <Point X="-0.116143137421" Y="21.977817281881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.800200539078" Y="20.924461255832" />
                  <Point X="-1.120078193599" Y="20.431892862928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.660956649292" Y="27.619613454688" />
                  <Point X="0.012988021073" Y="22.002234375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.879768149811" Y="20.627510426225" />
                  <Point X="-1.116872105136" Y="20.262402352638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.842176798874" Y="27.724240560184" />
                  <Point X="3.237797775196" Y="26.793578476757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002695372221" Y="26.431552523508" />
                  <Point X="0.113433466532" Y="21.982479343652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.959335998264" Y="20.330559230563" />
                  <Point X="-1.018408424905" Y="20.239595670451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.967670208778" Y="27.7430560117" />
                  <Point X="3.46358268758" Y="26.966829299112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019161419793" Y="26.282480559673" />
                  <Point X="0.207707038335" Y="21.953220460301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.027209346075" Y="27.660310789616" />
                  <Point X="3.689367434333" Y="27.140079866417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.062582425411" Y="26.174915591333" />
                  <Point X="0.301980661312" Y="21.923961655752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083226617288" Y="27.572142369341" />
                  <Point X="3.915151549309" Y="27.313329460869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121002613372" Y="26.09044733837" />
                  <Point X="0.389703247406" Y="21.884615139031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.194620533785" Y="26.029381541139" />
                  <Point X="0.459823522369" Y="21.818163440115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.27878828001" Y="25.98456105105" />
                  <Point X="0.518342778717" Y="21.733847739089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376692019914" Y="25.960892136372" />
                  <Point X="0.91531493655" Y="22.170703802963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.773732136712" Y="21.952685410013" />
                  <Point X="0.576862228741" Y="21.6495323363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484065021942" Y="25.951804606669" />
                  <Point X="1.125699210486" Y="22.320239721751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724728463299" Y="21.702798916643" />
                  <Point X="0.63374595836" Y="21.562698144968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.607087821173" Y="25.966815651372" />
                  <Point X="1.251165182845" Y="22.339012923154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.740314379817" Y="21.552371669835" />
                  <Point X="0.672319632354" Y="21.447668940492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.730952142174" Y="25.983122525965" />
                  <Point X="1.371638568416" Y="22.350098215083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.759407480414" Y="21.40734501291" />
                  <Point X="0.705405746229" Y="21.324189634454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854816463175" Y="25.999429400557" />
                  <Point X="1.482295921489" Y="22.346068142484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77850058101" Y="21.262318355985" />
                  <Point X="0.738491860105" Y="21.200710328416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.978680784176" Y="26.015736275149" />
                  <Point X="3.542864477586" Y="25.344638013972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361836434159" Y="25.065879272432" />
                  <Point X="1.571539763378" Y="22.309064154262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.797593681606" Y="21.11729169906" />
                  <Point X="0.771577973981" Y="21.077231022378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102545105177" Y="26.032043149742" />
                  <Point X="3.708900929357" Y="25.425884275187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.35889469101" Y="24.88692193164" />
                  <Point X="1.651450701187" Y="22.257688754035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816686782202" Y="20.972265042135" />
                  <Point X="0.804664087856" Y="20.953751716341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.226409426178" Y="26.048350024334" />
                  <Point X="3.846038577945" Y="25.462630281884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392824038673" Y="24.764741091767" />
                  <Point X="1.731361913305" Y="22.206313776208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.350273830358" Y="26.064657027011" />
                  <Point X="3.983176226533" Y="25.49937628858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451387457105" Y="24.680493394388" />
                  <Point X="2.288594151474" Y="22.889948722888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000543118427" Y="22.44638902931" />
                  <Point X="1.808493140321" Y="22.150657996723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.474138700638" Y="26.080964747418" />
                  <Point X="4.120313875121" Y="25.536122295277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518963997988" Y="24.610124688485" />
                  <Point X="3.148372145379" Y="24.039463278779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866088196448" Y="23.604784115971" />
                  <Point X="2.451567388876" Y="22.96647804762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022728000368" Y="22.306123298153" />
                  <Point X="1.871971789896" Y="22.073979091569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.598003570917" Y="26.097272467825" />
                  <Point X="4.257451587226" Y="25.572868399781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599666615486" Y="24.559968368075" />
                  <Point X="3.282577044651" Y="24.071693247555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871637680176" Y="23.438902117949" />
                  <Point X="2.56741990786" Y="22.97044782899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.049768007602" Y="22.17333380433" />
                  <Point X="1.933323193056" Y="21.994024514193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.708983645396" Y="26.093739342612" />
                  <Point X="4.394589553925" Y="25.609614896326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68574799276" Y="24.518094611392" />
                  <Point X="3.408912239419" Y="24.091804934092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.921677420169" Y="23.341529106779" />
                  <Point X="2.668804084248" Y="22.95213831651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.099210364717" Y="22.075040904196" />
                  <Point X="1.994674596217" Y="21.914069936818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739869222882" Y="25.966871507688" />
                  <Point X="4.531727520624" Y="25.64636139287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782046702769" Y="24.491954167413" />
                  <Point X="3.514826547503" Y="24.080471212694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.978029997353" Y="23.253877012421" />
                  <Point X="2.770187985696" Y="22.933828380662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.152520903026" Y="21.982704480757" />
                  <Point X="2.056025999378" Y="21.834115359442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765500356699" Y="25.831912539052" />
                  <Point X="4.668865487322" Y="25.683107889415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.878532072195" Y="24.466101153728" />
                  <Point X="3.619179233782" Y="24.06673280459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.040192683131" Y="23.175171700723" />
                  <Point X="2.860453962455" Y="22.898398342114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205831441336" Y="21.890368057318" />
                  <Point X="2.117377402539" Y="21.754160782066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.975017441621" Y="24.440248140044" />
                  <Point X="3.723531920061" Y="24.052994396487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.11502278496" Y="23.115972499184" />
                  <Point X="2.942839318502" Y="22.850833211837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259141979645" Y="21.798031633878" />
                  <Point X="2.1787288057" Y="21.674206204691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.071502811047" Y="24.41439512636" />
                  <Point X="3.82788460634" Y="24.039255988384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.190624395341" Y="23.057961316634" />
                  <Point X="3.025224674548" Y="22.80326808156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.312452517954" Y="21.705695210439" />
                  <Point X="2.24008020886" Y="21.594251627315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.167988180473" Y="24.388542112676" />
                  <Point X="3.932237292618" Y="24.025517580281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.266226005721" Y="22.999950134083" />
                  <Point X="3.107610030594" Y="22.755702951283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.365763056264" Y="21.613358787" />
                  <Point X="2.301431575846" Y="21.514296994234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.264473549899" Y="24.362689098992" />
                  <Point X="4.036589978897" Y="24.011779172178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.341827616102" Y="22.941938951532" />
                  <Point X="3.18999538664" Y="22.708137821007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.419073594573" Y="21.521022363561" />
                  <Point X="2.36278292788" Y="21.434342338131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.360958919324" Y="24.336836085308" />
                  <Point X="4.140942716683" Y="23.998040843389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.417429226483" Y="22.883927768982" />
                  <Point X="3.272380742686" Y="22.66057269073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.472384132882" Y="21.428685940122" />
                  <Point X="2.424134279915" Y="21.354387682028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.457444287241" Y="24.3109830693" />
                  <Point X="4.245295470029" Y="23.984302538561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.493030836863" Y="22.825916586431" />
                  <Point X="3.354766098733" Y="22.613007560453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52569467133" Y="21.336349516895" />
                  <Point X="2.48548563195" Y="21.274433025925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.553929614672" Y="24.285129990949" />
                  <Point X="4.349648223376" Y="23.970564233732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.568632447244" Y="22.767905403881" />
                  <Point X="3.437151431688" Y="22.565442394619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.579005213857" Y="21.244013099952" />
                  <Point X="2.546836983984" Y="21.194478369823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.650414942103" Y="24.259276912598" />
                  <Point X="4.454000976722" Y="23.956825928904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644234057625" Y="22.70989422133" />
                  <Point X="3.519536729953" Y="22.517877175367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632315756385" Y="21.151676683008" />
                  <Point X="2.608188336019" Y="21.11452371372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.746900269533" Y="24.233423834247" />
                  <Point X="4.558353730068" Y="23.943087624075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.719835668005" Y="22.651883038779" />
                  <Point X="3.601922028219" Y="22.470311956116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.685626298913" Y="21.059340266065" />
                  <Point X="2.669539688054" Y="21.034569057617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7641083412" Y="24.085494487317" />
                  <Point X="4.662706483414" Y="23.929349319247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.795437320927" Y="22.593871921735" />
                  <Point X="3.684307326484" Y="22.422746736864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.871039021866" Y="22.535860878633" />
                  <Point X="3.766692624749" Y="22.375181517613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.946640722805" Y="22.47784983553" />
                  <Point X="3.849077923015" Y="22.327616298362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.022242423744" Y="22.419838792427" />
                  <Point X="3.93146322128" Y="22.28005107911" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.63733392334" Y="20.8441328125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.396554351807" Y="21.576091796875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.169474075317" Y="21.766146484375" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.113526428223" Y="21.779689453125" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.356043609619" Y="21.616720703125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.69401348877" Y="20.58665234375" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.926040283203" Y="20.02812109375" />
                  <Point X="-1.100230102539" Y="20.061931640625" />
                  <Point X="-1.221171875" Y="20.093048828125" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.329108032227" Y="20.2973671875" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.334734741211" Y="20.59118359375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.482825927734" Y="20.882037109375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.777375488281" Y="21.022634765625" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.096647216797" Y="20.954869140625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.391548339844" Y="20.670921875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.600484130859" Y="20.674283203125" />
                  <Point X="-2.855839599609" Y="20.832392578125" />
                  <Point X="-3.023281738281" Y="20.961318359375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.814144775391" Y="21.837216796875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979980469" Y="22.431236328125" />
                  <Point X="-2.531328613281" Y="22.448583984375" />
                  <Point X="-2.560157714844" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.347418701172" Y="22.020205078125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.959963378906" Y="21.88782421875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.28175" Y="22.354169921875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.702881347656" Y="23.163193359375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.161160400391" Y="23.689361328125" />
                  <Point X="-3.187643066406" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.173573242188" Y="23.585826171875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.848491210938" Y="23.67991015625" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.959154296875" Y="24.21087890625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.171573730469" Y="24.706802734375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.530410400391" Y="24.886546875" />
                  <Point X="-3.502325195312" Y="24.909353515625" />
                  <Point X="-3.4910703125" Y="24.936427734375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.4894765625" Y="24.995876953125" />
                  <Point X="-3.502325683594" Y="25.028087890625" />
                  <Point X="-3.525627197266" Y="25.047572265625" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.427120117188" Y="25.299109375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.96849609375" Y="25.652771484375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.853710449219" Y="26.23235546875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.193475585937" Y="26.45193359375" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704833984" Y="26.3958671875" />
                  <Point X="-3.706281494141" Y="26.4038828125" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.628918701172" Y="26.468412109375" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.628624755859" Y="26.56915625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.158805175781" Y="27.0019921875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.357603515625" Y="27.44848828125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.990654052734" Y="28.0046953125" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.429520996094" Y="28.0830390625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514648438" Y="27.92043359375" />
                  <Point X="-3.103106933594" Y="27.9173359375" />
                  <Point X="-3.052965332031" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.988120117188" Y="27.952537109375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941172119141" Y="28.063248046875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.1731171875" Y="28.51690234375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.097013427734" Y="28.910484375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.486138671875" Y="29.322525390625" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.050593505859" Y="29.3954375" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.911838745117" Y="29.253146484375" />
                  <Point X="-1.85603125" Y="29.22409375" />
                  <Point X="-1.83512487793" Y="29.218490234375" />
                  <Point X="-1.813809570312" Y="29.22225" />
                  <Point X="-1.772762817383" Y="29.23925390625" />
                  <Point X="-1.714635620117" Y="29.263330078125" />
                  <Point X="-1.696905639648" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.672723510742" Y="29.336861328125" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.677048461914" Y="29.609314453125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.41360144043" Y="29.778390625" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.644728393555" Y="29.941142578125" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.122121459961" Y="29.609396484375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282119751" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594039917" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.167715606689" Y="29.733607421875" />
                  <Point X="0.236648406982" Y="29.9908671875" />
                  <Point X="0.471212280273" Y="29.966302734375" />
                  <Point X="0.860210144043" Y="29.925564453125" />
                  <Point X="1.127735595703" Y="29.860974609375" />
                  <Point X="1.508455566406" Y="29.769056640625" />
                  <Point X="1.682695678711" Y="29.705859375" />
                  <Point X="1.931044921875" Y="29.61578125" />
                  <Point X="2.099415527344" Y="29.5370390625" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.501365722656" Y="29.33036328125" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.885937255859" Y="29.08658984375" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.586468994141" Y="28.1212734375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.215966064453" Y="27.458283203125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.205509521484" Y="27.363591796875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.236461914062" Y="27.274609375" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.301141113281" Y="27.20642578125" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360337402344" Y="27.172978515625" />
                  <Point X="2.389071289062" Y="27.169513671875" />
                  <Point X="2.429762207031" Y="27.164607421875" />
                  <Point X="2.448664794922" Y="27.1659453125" />
                  <Point X="2.481894287109" Y="27.17483203125" />
                  <Point X="2.528951171875" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.415740966797" Y="27.6974296875" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.0654765625" Y="27.9324375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.288117675781" Y="27.600546875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.758133789062" Y="26.95335546875" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.255456054688" Y="26.5526328125" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.204211181641" Y="26.45964453125" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.198092529297" Y="26.3555234375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.235537109375" Y="26.257720703125" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280946777344" Y="26.1988203125" />
                  <Point X="3.309770507812" Y="26.182595703125" />
                  <Point X="3.350588867188" Y="26.1596171875" />
                  <Point X="3.36856640625" Y="26.153619140625" />
                  <Point X="3.407538085938" Y="26.14846875" />
                  <Point X="3.462727050781" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.306716796875" Y="26.2505625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.880298339844" Y="26.193287109375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.966140136719" Y="25.778279296875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.279245117188" Y="25.38200390625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087646484" Y="25.232818359375" />
                  <Point X="3.690799072266" Y="25.2106875" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.599291503906" Y="25.13765234375" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.549327636719" Y="25.034748046875" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.546140869141" Y="24.919330078125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.589731933594" Y="24.81196875" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636576416016" Y="24.758091796875" />
                  <Point X="3.674864990234" Y="24.7359609375" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.503152832031" Y="24.495439453125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.981313476562" Y="24.251697265625" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.91390234375" Y="23.882287109375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.036000732422" Y="23.820216796875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836669922" Y="23.901658203125" />
                  <Point X="3.319689697266" Y="23.88532421875" />
                  <Point X="3.213272460938" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.140023925781" Y="23.790673828125" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.057847900391" Y="23.61518359375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.097947998047" Y="23.41869140625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.875587402344" Y="22.771861328125" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.296821777344" Y="22.347845703125" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.132722167969" Y="22.09639453125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.309227294922" Y="22.419908203125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.647903808594" Y="22.762837890625" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.414777832031" Y="22.741650390625" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.249496337891" Y="22.591015625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.205315185547" Y="22.3641875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.688491699219" Y="21.434376953125" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.945283447266" Y="20.88834765625" />
                  <Point X="2.835296142578" Y="20.809787109375" />
                  <Point X="2.755458496094" Y="20.758109375" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.107283447266" Y="21.45520703125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.582340576172" Y="22.0762421875" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425805175781" Y="22.16428125" />
                  <Point X="1.329333984375" Y="22.155404296875" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.131490234375" />
                  <Point X="1.090840087891" Y="22.06955078125" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.946183837891" Y="21.8515390625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.04270300293" Y="20.7111484375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.098400268555" Y="20.059560546875" />
                  <Point X="0.994366088867" Y="20.0367578125" />
                  <Point X="0.920588623047" Y="20.023353515625" />
                  <Point X="0.860200561523" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#158" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.079981186327" Y="4.653618339828" Z="1.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="-0.656696476724" Y="5.022082754629" Z="1.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.05" />
                  <Point X="-1.433255108269" Y="4.857815414216" Z="1.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.05" />
                  <Point X="-1.732776755986" Y="4.63406860881" Z="1.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.05" />
                  <Point X="-1.726565220585" Y="4.383176266887" Z="1.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.05" />
                  <Point X="-1.798052375923" Y="4.316726810699" Z="1.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.05" />
                  <Point X="-1.894906586973" Y="4.328776350759" Z="1.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.05" />
                  <Point X="-2.017081832398" Y="4.457154943855" Z="1.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.05" />
                  <Point X="-2.51657756674" Y="4.397512577589" Z="1.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.05" />
                  <Point X="-3.133591612369" Y="3.981444782805" Z="1.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.05" />
                  <Point X="-3.222574465305" Y="3.523182335294" Z="1.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.05" />
                  <Point X="-2.99713781308" Y="3.090171221397" Z="1.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.05" />
                  <Point X="-3.029630941282" Y="3.019172626605" Z="1.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.05" />
                  <Point X="-3.10490524421" Y="2.998426886092" Z="1.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.05" />
                  <Point X="-3.410677013004" Y="3.15761948344" Z="1.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.05" />
                  <Point X="-4.0362733231" Y="3.066678038441" Z="1.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.05" />
                  <Point X="-4.406941786576" Y="2.504973787302" Z="1.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.05" />
                  <Point X="-4.195399287784" Y="1.993605169601" Z="1.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.05" />
                  <Point X="-3.679131338843" Y="1.577349571313" Z="1.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.05" />
                  <Point X="-3.681268667334" Y="1.518828085019" Z="1.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.05" />
                  <Point X="-3.727472651956" Y="1.482848499969" Z="1.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.05" />
                  <Point X="-4.193104969969" Y="1.532787153379" Z="1.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.05" />
                  <Point X="-4.908125981603" Y="1.276715050088" Z="1.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.05" />
                  <Point X="-5.024113582062" Y="0.691370206739" Z="1.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.05" />
                  <Point X="-4.446217194859" Y="0.28209278158" Z="1.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.05" />
                  <Point X="-3.560294381738" Y="0.037779253506" Z="1.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.05" />
                  <Point X="-3.543385648272" Y="0.012336674768" Z="1.05" />
                  <Point X="-3.539556741714" Y="0" Z="1.05" />
                  <Point X="-3.544978952863" Y="-0.017470276292" Z="1.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.05" />
                  <Point X="-3.565074213385" Y="-0.041096729628" Z="1.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.05" />
                  <Point X="-4.190670593914" Y="-0.213619275176" Z="1.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.05" />
                  <Point X="-5.014806303469" Y="-0.76491930455" Z="1.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.05" />
                  <Point X="-4.90309180258" Y="-1.301184089106" Z="1.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.05" />
                  <Point X="-4.173202962245" Y="-1.43246556906" Z="1.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.05" />
                  <Point X="-3.20363707132" Y="-1.315998773721" Z="1.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.05" />
                  <Point X="-3.197192020251" Y="-1.339885156527" Z="1.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.05" />
                  <Point X="-3.739475293838" Y="-1.765858905336" Z="1.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.05" />
                  <Point X="-4.330849293538" Y="-2.640159500498" Z="1.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.05" />
                  <Point X="-4.005905276083" Y="-3.111177981089" Z="1.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.05" />
                  <Point X="-3.32857474528" Y="-2.991814912705" Z="1.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.05" />
                  <Point X="-2.562671280536" Y="-2.565659188596" Z="1.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.05" />
                  <Point X="-2.863602100463" Y="-3.106503637212" Z="1.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.05" />
                  <Point X="-3.05994128564" Y="-4.047019153641" Z="1.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.05" />
                  <Point X="-2.63296170545" Y="-4.336948309516" Z="1.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.05" />
                  <Point X="-2.358036702029" Y="-4.328236022521" Z="1.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.05" />
                  <Point X="-2.075024585376" Y="-4.055424962871" Z="1.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.05" />
                  <Point X="-1.786801411359" Y="-3.995977542553" Z="1.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.05" />
                  <Point X="-1.52194934863" Y="-4.124274127957" Z="1.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.05" />
                  <Point X="-1.389929986802" Y="-4.387290197004" Z="1.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.05" />
                  <Point X="-1.384836323392" Y="-4.664826616928" Z="1.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.05" />
                  <Point X="-1.239786725292" Y="-4.924095004833" Z="1.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.05" />
                  <Point X="-0.941723636421" Y="-4.989683330361" Z="1.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="-0.651873155196" Y="-4.395007822436" Z="1.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="-0.321123655151" Y="-3.38050851126" Z="1.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="-0.104861733605" Y="-3.23678458079" Z="1.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.05" />
                  <Point X="0.148497345756" Y="-3.250327464498" Z="1.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.05" />
                  <Point X="0.349322204281" Y="-3.421137244508" Z="1.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.05" />
                  <Point X="0.582881705205" Y="-4.137528207379" Z="1.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.05" />
                  <Point X="0.923369127278" Y="-4.994560928364" Z="1.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.05" />
                  <Point X="1.102949689595" Y="-4.957998692266" Z="1.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.05" />
                  <Point X="1.086119280946" Y="-4.251045398899" Z="1.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.05" />
                  <Point X="0.988887067678" Y="-3.127799047075" Z="1.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.05" />
                  <Point X="1.116651033026" Y="-2.937613692199" Z="1.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.05" />
                  <Point X="1.327759171211" Y="-2.863103964613" Z="1.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.05" />
                  <Point X="1.549145124295" Y="-2.934535179033" Z="1.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.05" />
                  <Point X="2.061459719335" Y="-3.543950625315" Z="1.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.05" />
                  <Point X="2.776471927318" Y="-4.252585356044" Z="1.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.05" />
                  <Point X="2.968189252217" Y="-4.121059521234" Z="1.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.05" />
                  <Point X="2.725636971877" Y="-3.509342310239" Z="1.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.05" />
                  <Point X="2.248363331904" Y="-2.59564495345" Z="1.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.05" />
                  <Point X="2.287586642604" Y="-2.400990147495" Z="1.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.05" />
                  <Point X="2.431908230877" Y="-2.271314667363" Z="1.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.05" />
                  <Point X="2.632861850062" Y="-2.255084677587" Z="1.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.05" />
                  <Point X="3.278071286868" Y="-2.592112528397" Z="1.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.05" />
                  <Point X="4.167454773641" Y="-2.901101846319" Z="1.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.05" />
                  <Point X="4.33319942813" Y="-2.647159558656" Z="1.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.05" />
                  <Point X="3.899869475733" Y="-2.15719046505" Z="1.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.05" />
                  <Point X="3.133849886627" Y="-1.522988934539" Z="1.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.05" />
                  <Point X="3.101481467206" Y="-1.358117847166" Z="1.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.05" />
                  <Point X="3.172314012308" Y="-1.210012166349" Z="1.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.05" />
                  <Point X="3.324152955603" Y="-1.132253915008" Z="1.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.05" />
                  <Point X="4.023318378155" Y="-1.198074014716" Z="1.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.05" />
                  <Point X="4.956493325757" Y="-1.097556816546" Z="1.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.05" />
                  <Point X="5.02459881111" Y="-0.724476708434" Z="1.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.05" />
                  <Point X="4.509937700852" Y="-0.424983893216" Z="1.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.05" />
                  <Point X="3.693731286879" Y="-0.189469688395" Z="1.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.05" />
                  <Point X="3.622909867022" Y="-0.12588371437" Z="1.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.05" />
                  <Point X="3.589092441153" Y="-0.039985730208" Z="1.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.05" />
                  <Point X="3.592279009271" Y="0.05662480101" Z="1.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.05" />
                  <Point X="3.632469571378" Y="0.138065024205" Z="1.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.05" />
                  <Point X="3.709664127473" Y="0.198679155847" Z="1.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.05" />
                  <Point X="4.286030116455" Y="0.364988039553" Z="1.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.05" />
                  <Point X="5.009388885173" Y="0.817251281688" Z="1.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.05" />
                  <Point X="4.922723448573" Y="1.236394527596" Z="1.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.05" />
                  <Point X="4.294034398583" Y="1.331415886483" Z="1.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.05" />
                  <Point X="3.407932438133" Y="1.229317978635" Z="1.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.05" />
                  <Point X="3.32814241691" Y="1.257445691619" Z="1.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.05" />
                  <Point X="3.271151337524" Y="1.316483958927" Z="1.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.05" />
                  <Point X="3.24090496664" Y="1.396907001822" Z="1.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.05" />
                  <Point X="3.246207526867" Y="1.477459187398" Z="1.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.05" />
                  <Point X="3.288982936159" Y="1.553495807999" Z="1.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.05" />
                  <Point X="3.782415926133" Y="1.944968742421" Z="1.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.05" />
                  <Point X="4.324739051546" Y="2.657714219641" Z="1.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.05" />
                  <Point X="4.099906271954" Y="2.992922033153" Z="1.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.05" />
                  <Point X="3.384584880707" Y="2.772011038185" Z="1.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.05" />
                  <Point X="2.462821003157" Y="2.254415135207" Z="1.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.05" />
                  <Point X="2.38890075727" Y="2.250435743483" Z="1.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.05" />
                  <Point X="2.323060652863" Y="2.279078553264" Z="1.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.05" />
                  <Point X="2.271680046971" Y="2.333964207519" Z="1.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.05" />
                  <Point X="2.248993959276" Y="2.400857686563" Z="1.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.05" />
                  <Point X="2.25811280745" Y="2.476648572725" Z="1.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.05" />
                  <Point X="2.623614314674" Y="3.127554201248" Z="1.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.05" />
                  <Point X="2.908758312501" Y="4.158619330295" Z="1.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.05" />
                  <Point X="2.520379651085" Y="4.404847197549" Z="1.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.05" />
                  <Point X="2.114441065762" Y="4.613611469329" Z="1.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.05" />
                  <Point X="1.693588371974" Y="4.784143924138" Z="1.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.05" />
                  <Point X="1.133314049041" Y="4.940859273257" Z="1.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.05" />
                  <Point X="0.470264185911" Y="5.04731167915" Z="1.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.05" />
                  <Point X="0.113263375226" Y="4.777829166123" Z="1.05" />
                  <Point X="0" Y="4.355124473572" Z="1.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>