<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#127" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="605" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004715820312" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.602929626465" Y="21.339583984375" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362670898" Y="21.532625" />
                  <Point X="0.526165893555" Y="21.5559609375" />
                  <Point X="0.378634887695" Y="21.768525390625" />
                  <Point X="0.356748809814" Y="21.790982421875" />
                  <Point X="0.330493927002" Y="21.810224609375" />
                  <Point X="0.30249041748" Y="21.82433203125" />
                  <Point X="0.27742678833" Y="21.832109375" />
                  <Point X="0.049131427765" Y="21.90296484375" />
                  <Point X="0.020983259201" Y="21.907232421875" />
                  <Point X="-0.008658372879" Y="21.907234375" />
                  <Point X="-0.03682566452" Y="21.90296484375" />
                  <Point X="-0.061889297485" Y="21.895185546875" />
                  <Point X="-0.290184814453" Y="21.82433203125" />
                  <Point X="-0.318184844971" Y="21.81022265625" />
                  <Point X="-0.344440185547" Y="21.790978515625" />
                  <Point X="-0.366324707031" Y="21.7685234375" />
                  <Point X="-0.38252166748" Y="21.745185546875" />
                  <Point X="-0.53005255127" Y="21.532623046875" />
                  <Point X="-0.53818927002" Y="21.518427734375" />
                  <Point X="-0.55099005127" Y="21.4874765625" />
                  <Point X="-0.911943725586" Y="20.14037890625" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-1.079332641602" Y="20.1546484375" />
                  <Point X="-1.104369018555" Y="20.16108984375" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.219733276367" Y="20.400326171875" />
                  <Point X="-1.214962890625" Y="20.43655859375" />
                  <Point X="-1.214201049805" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.222496337891" Y="20.513876953125" />
                  <Point X="-1.277035888672" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323645263672" Y="20.868796875" />
                  <Point X="-1.346720825195" Y="20.889033203125" />
                  <Point X="-1.55690625" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.61288671875" Y="21.102005859375" />
                  <Point X="-1.643026123047" Y="21.109033203125" />
                  <Point X="-1.67365234375" Y="21.111041015625" />
                  <Point X="-1.952615112305" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.04265612793" Y="21.10519921875" />
                  <Point X="-2.068175537109" Y="21.0881484375" />
                  <Point X="-2.300622802734" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480147949219" Y="20.711509765625" />
                  <Point X="-2.801720458984" Y="20.910619140625" />
                  <Point X="-2.836362548828" Y="20.93729296875" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.497404541016" Y="22.195826171875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412857177734" Y="22.352357421875" />
                  <Point X="-2.406587646484" Y="22.38389453125" />
                  <Point X="-2.405724853516" Y="22.41589453125" />
                  <Point X="-2.415539550781" Y="22.44636328125" />
                  <Point X="-2.431470703125" Y="22.476896484375" />
                  <Point X="-2.448519042969" Y="22.500125" />
                  <Point X="-2.464151367188" Y="22.5157578125" />
                  <Point X="-2.489310791016" Y="22.533787109375" />
                  <Point X="-2.518140136719" Y="22.54800390625" />
                  <Point X="-2.5477578125" Y="22.55698828125" />
                  <Point X="-2.578691650391" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-3.801029052734" Y="21.868009765625" />
                  <Point X="-3.818024169922" Y="21.858197265625" />
                  <Point X="-4.082859863281" Y="22.206138671875" />
                  <Point X="-4.107701660156" Y="22.247794921875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.234806640625" Y="23.40261328125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083143554688" Y="23.52663671875" />
                  <Point X="-3.064416503906" Y="23.5567109375" />
                  <Point X="-3.053094238281" Y="23.583109375" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.043347167969" Y="23.6403359375" />
                  <Point X="-3.045553222656" Y="23.672" />
                  <Point X="-3.053066894531" Y="23.70296484375" />
                  <Point X="-3.070396728516" Y="23.729703125" />
                  <Point X="-3.093854248047" Y="23.7552109375" />
                  <Point X="-3.115598632812" Y="23.772779296875" />
                  <Point X="-3.139461425781" Y="23.786822265625" />
                  <Point X="-3.16872265625" Y="23.79804296875" />
                  <Point X="-3.200608154297" Y="23.8045234375" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.698648925781" Y="23.61251953125" />
                  <Point X="-4.732102539062" Y="23.608115234375" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.840649902344" Y="24.053296875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.679657714844" Y="24.740259765625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.515082275391" Y="24.78633203125" />
                  <Point X="-3.496760742188" Y="24.79546484375" />
                  <Point X="-3.4849765625" Y="24.80244140625" />
                  <Point X="-3.459978271484" Y="24.819791015625" />
                  <Point X="-3.435963623047" Y="24.843865234375" />
                  <Point X="-3.415803710938" Y="24.873068359375" />
                  <Point X="-3.40325390625" Y="24.89887890625" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.390685546875" Y="24.956560546875" />
                  <Point X="-3.391600097656" Y="24.98915234375" />
                  <Point X="-3.395831787109" Y="25.014646484375" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.420021728516" Y="25.071814453125" />
                  <Point X="-3.442010498047" Y="25.099974609375" />
                  <Point X="-3.462718261719" Y="25.11955078125" />
                  <Point X="-3.487725585938" Y="25.136908203125" />
                  <Point X="-3.501924804688" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.869860351562" Y="25.51609375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.824488769531" Y="25.97696875" />
                  <Point X="-4.811256347656" Y="26.02580078125" />
                  <Point X="-4.703550292969" Y="26.423267578125" />
                  <Point X="-3.870883300781" Y="26.31364453125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984619141" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703136230469" Y="26.305263671875" />
                  <Point X="-3.697059814453" Y="26.3071796875" />
                  <Point X="-3.641710205078" Y="26.324630859375" />
                  <Point X="-3.622784912109" Y="26.332958984375" />
                  <Point X="-3.604040527344" Y="26.343779296875" />
                  <Point X="-3.58735546875" Y="26.35601171875" />
                  <Point X="-3.573714355469" Y="26.37156640625" />
                  <Point X="-3.561299804688" Y="26.389296875" />
                  <Point X="-3.551350830078" Y="26.407431640625" />
                  <Point X="-3.548912597656" Y="26.413318359375" />
                  <Point X="-3.526703369141" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532046875" Y="26.58937109375" />
                  <Point X="-3.534988769531" Y="26.5950234375" />
                  <Point X="-3.561786621094" Y="26.6465" />
                  <Point X="-3.57328125" Y="26.663705078125" />
                  <Point X="-3.587193359375" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.351860351563" Y="27.269875" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-4.046107177734" Y="27.778705078125" />
                  <Point X="-3.750503417969" Y="28.158662109375" />
                  <Point X="-3.271278076172" Y="27.88198046875" />
                  <Point X="-3.206656738281" Y="27.844671875" />
                  <Point X="-3.187729248047" Y="27.836341796875" />
                  <Point X="-3.167087158203" Y="27.82983203125" />
                  <Point X="-3.146792236328" Y="27.825794921875" />
                  <Point X="-3.138329101562" Y="27.8250546875" />
                  <Point X="-3.061242919922" Y="27.818310546875" />
                  <Point X="-3.040567626953" Y="27.81876171875" />
                  <Point X="-3.019111328125" Y="27.8215859375" />
                  <Point X="-2.999020996094" Y="27.826501953125" />
                  <Point X="-2.980470458984" Y="27.8356484375" />
                  <Point X="-2.962217285156" Y="27.847275390625" />
                  <Point X="-2.946086181641" Y="27.860220703125" />
                  <Point X="-2.940075439453" Y="27.86623046875" />
                  <Point X="-2.885358886719" Y="27.920947265625" />
                  <Point X="-2.872406738281" Y="27.937083984375" />
                  <Point X="-2.860777587891" Y="27.955337890625" />
                  <Point X="-2.851628662109" Y="27.973888671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.03612109375" />
                  <Point X="-2.844176269531" Y="28.044583984375" />
                  <Point X="-2.850920410156" Y="28.121671875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.183333007812" Y="28.724595703125" />
                  <Point X="-2.700626464844" Y="29.09468359375" />
                  <Point X="-2.645432128906" Y="29.12534765625" />
                  <Point X="-2.167037109375" Y="29.391134765625" />
                  <Point X="-2.062977294922" Y="29.25551953125" />
                  <Point X="-2.043194702148" Y="29.229740234375" />
                  <Point X="-2.028887817383" Y="29.214794921875" />
                  <Point X="-2.012309204102" Y="29.200884765625" />
                  <Point X="-1.995120117188" Y="29.189400390625" />
                  <Point X="-1.985700927734" Y="29.18449609375" />
                  <Point X="-1.899904174805" Y="29.13983203125" />
                  <Point X="-1.880625244141" Y="29.13233203125" />
                  <Point X="-1.859718505859" Y="29.126728515625" />
                  <Point X="-1.839268676758" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777448608398" Y="29.134484375" />
                  <Point X="-1.767637695313" Y="29.138548828125" />
                  <Point X="-1.678274658203" Y="29.175564453125" />
                  <Point X="-1.660146118164" Y="29.185509765625" />
                  <Point X="-1.642416625977" Y="29.197923828125" />
                  <Point X="-1.626864501953" Y="29.2115625" />
                  <Point X="-1.614633056641" Y="29.228244140625" />
                  <Point X="-1.603811157227" Y="29.24698828125" />
                  <Point X="-1.595480712891" Y="29.265919921875" />
                  <Point X="-1.592287475586" Y="29.276046875" />
                  <Point X="-1.563201416016" Y="29.368296875" />
                  <Point X="-1.559165649414" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.584201904297" Y="29.6318984375" />
                  <Point X="-0.949624145508" Y="29.80980859375" />
                  <Point X="-0.882730285645" Y="29.817638671875" />
                  <Point X="-0.294711212158" Y="29.886458984375" />
                  <Point X="-0.153020172119" Y="29.35766015625" />
                  <Point X="-0.133903381348" Y="29.286314453125" />
                  <Point X="-0.12112991333" Y="29.258123046875" />
                  <Point X="-0.103271614075" Y="29.231396484375" />
                  <Point X="-0.082114082336" Y="29.208806640625" />
                  <Point X="-0.054818141937" Y="29.194216796875" />
                  <Point X="-0.02437991333" Y="29.183884765625" />
                  <Point X="0.006155911922" Y="29.17884375" />
                  <Point X="0.036691745758" Y="29.183884765625" />
                  <Point X="0.067129974365" Y="29.194216796875" />
                  <Point X="0.094426025391" Y="29.208806640625" />
                  <Point X="0.115583602905" Y="29.231396484375" />
                  <Point X="0.133441741943" Y="29.258123046875" />
                  <Point X="0.146215209961" Y="29.286314453125" />
                  <Point X="0.307419372559" Y="29.8879375" />
                  <Point X="0.844034118652" Y="29.831740234375" />
                  <Point X="0.89940032959" Y="29.818373046875" />
                  <Point X="1.481032104492" Y="29.677947265625" />
                  <Point X="1.515726074219" Y="29.665365234375" />
                  <Point X="1.894663085938" Y="29.527919921875" />
                  <Point X="1.929493041992" Y="29.5116328125" />
                  <Point X="2.294549316406" Y="29.34090625" />
                  <Point X="2.328271728516" Y="29.321259765625" />
                  <Point X="2.680988525391" Y="29.115767578125" />
                  <Point X="2.712729003906" Y="29.0931953125" />
                  <Point X="2.943260009766" Y="28.92925390625" />
                  <Point X="2.232817626953" Y="27.698732421875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.140349609375" Y="27.53544140625" />
                  <Point X="2.133224121094" Y="27.5154140625" />
                  <Point X="2.130952880859" Y="27.508111328125" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108400878906" Y="27.41310546875" />
                  <Point X="2.107891357422" Y="27.3873359375" />
                  <Point X="2.108555908203" Y="27.374083984375" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140068603516" Y="27.247474609375" />
                  <Point X="2.144318359375" Y="27.2412109375" />
                  <Point X="2.183026367187" Y="27.184166015625" />
                  <Point X="2.198198486328" Y="27.16679296875" />
                  <Point X="2.217760498047" Y="27.1492421875" />
                  <Point X="2.227860107422" Y="27.141341796875" />
                  <Point X="2.284906005859" Y="27.1026328125" />
                  <Point X="2.304954101562" Y="27.09226953125" />
                  <Point X="2.327043212891" Y="27.08400390625" />
                  <Point X="2.348964599609" Y="27.078662109375" />
                  <Point X="2.355832519531" Y="27.077833984375" />
                  <Point X="2.418389404297" Y="27.070291015625" />
                  <Point X="2.441916015625" Y="27.070388671875" />
                  <Point X="2.468760742188" Y="27.0738515625" />
                  <Point X="2.4811484375" Y="27.076294921875" />
                  <Point X="2.553492431641" Y="27.095640625" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.933282714844" Y="27.886537109375" />
                  <Point X="3.967325195312" Y="27.90619140625" />
                  <Point X="4.123275390625" Y="27.68945703125" />
                  <Point X="4.14097265625" Y="27.6602109375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.343005615234" Y="26.7545625" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.217843994141" Y="26.65645703125" />
                  <Point X="3.202883300781" Y="26.63975" />
                  <Point X="3.198258056641" Y="26.634169921875" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.134362548828" Y="26.546087890625" />
                  <Point X="3.123763427734" Y="26.5215234375" />
                  <Point X="3.119500244141" Y="26.50947265625" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.099487304688" Y="26.363296875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.122996826172" Y="26.263822265625" />
                  <Point X="3.13494921875" Y="26.239216796875" />
                  <Point X="3.141037353516" Y="26.22851171875" />
                  <Point X="3.184340820312" Y="26.162693359375" />
                  <Point X="3.198892822266" Y="26.14544921875" />
                  <Point X="3.216132080078" Y="26.12936328125" />
                  <Point X="3.234339355469" Y="26.1160390625" />
                  <Point X="3.241232910156" Y="26.112158203125" />
                  <Point X="3.303985839844" Y="26.076833984375" />
                  <Point X="3.326265869141" Y="26.067783203125" />
                  <Point X="3.353556396484" Y="26.0605546875" />
                  <Point X="3.365434082031" Y="26.05820703125" />
                  <Point X="3.450280029297" Y="26.046994140625" />
                  <Point X="3.462698242188" Y="26.04617578125" />
                  <Point X="3.488203369141" Y="26.046984375" />
                  <Point X="4.765625" Y="26.21516015625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845937011719" Y="25.93280859375" />
                  <Point X="4.851514160156" Y="25.896982421875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.845188476562" Y="25.36405078125" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.699712646484" Y="25.3233046875" />
                  <Point X="3.678480957031" Y="25.3130078125" />
                  <Point X="3.672395263672" Y="25.309779296875" />
                  <Point X="3.589036865234" Y="25.26159765625" />
                  <Point X="3.570065185547" Y="25.2471796875" />
                  <Point X="3.550261962891" Y="25.22776171875" />
                  <Point X="3.542040283203" Y="25.218580078125" />
                  <Point X="3.492025390625" Y="25.154849609375" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.461850585938" Y="25.083046875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443680175781" Y="24.972005859375" />
                  <Point X="3.445510742188" Y="24.943640625" />
                  <Point X="3.447009277344" Y="24.931888671875" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526611328" Y="24.8233359375" />
                  <Point X="3.480300537109" Y="24.80187109375" />
                  <Point X="3.492020263672" Y="24.78259765625" />
                  <Point X="3.497511230469" Y="24.775599609375" />
                  <Point X="3.547526123047" Y="24.711869140625" />
                  <Point X="3.565034423828" Y="24.694685546875" />
                  <Point X="3.588497558594" Y="24.6769765625" />
                  <Point X="3.598186279297" Y="24.6705546875" />
                  <Point X="3.681544677734" Y="24.62237109375" />
                  <Point X="3.692708251953" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.888033203125" Y="24.293958984375" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.051271484375" />
                  <Point X="4.847875488281" Y="24.019953125" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="3.573532470703" Y="23.976921875" />
                  <Point X="3.424382324219" Y="23.99655859375" />
                  <Point X="3.399594970703" Y="23.996560546875" />
                  <Point X="3.364487792969" Y="23.991943359375" />
                  <Point X="3.356698486328" Y="23.9905859375" />
                  <Point X="3.193095458984" Y="23.95502734375" />
                  <Point X="3.162730224609" Y="23.944310546875" />
                  <Point X="3.130097167969" Y="23.922572265625" />
                  <Point X="3.106740478516" Y="23.898740234375" />
                  <Point X="3.101540771484" Y="23.892982421875" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.986627197266" Y="23.749583984375" />
                  <Point X="2.974201904297" Y="23.71583203125" />
                  <Point X="2.969050292969" Y="23.684369140625" />
                  <Point X="2.968201660156" Y="23.677724609375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.955109130859" Y="23.491515625" />
                  <Point X="2.966159912109" Y="23.453583984375" />
                  <Point X="2.982611328125" Y="23.422958984375" />
                  <Point X="2.986390625" Y="23.416541015625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.197744140625" Y="22.404916015625" />
                  <Point X="4.213123046875" Y="22.393115234375" />
                  <Point X="4.124812011719" Y="22.25021484375" />
                  <Point X="4.110030761719" Y="22.22921484375" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="2.933792724609" Y="22.746361328125" />
                  <Point X="2.800954345703" Y="22.8230546875" />
                  <Point X="2.786129150391" Y="22.829986328125" />
                  <Point X="2.754227294922" Y="22.840171875" />
                  <Point X="2.732850585938" Y="22.844033203125" />
                  <Point X="2.538136962891" Y="22.879197265625" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.4448359375" Y="22.86482421875" />
                  <Point X="2.427076904297" Y="22.855478515625" />
                  <Point X="2.265317871094" Y="22.770345703125" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531005859" Y="22.70955859375" />
                  <Point X="2.195184570313" Y="22.691798828125" />
                  <Point X="2.110052001953" Y="22.530041015625" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.099535888672" Y="22.415365234375" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.850400634766" Y="20.963943359375" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.781854736328" Y="20.888359375" />
                  <Point X="2.765330078125" Y="20.8776640625" />
                  <Point X="2.701764160156" Y="20.83651953125" />
                  <Point X="1.85991394043" Y="21.933638671875" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721924560547" Y="22.099443359375" />
                  <Point X="1.700841308594" Y="22.112998046875" />
                  <Point X="1.50880090332" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417102783203" Y="22.2588828125" />
                  <Point X="1.394044433594" Y="22.25676171875" />
                  <Point X="1.184015991211" Y="22.237435546875" />
                  <Point X="1.156362426758" Y="22.2306015625" />
                  <Point X="1.128976928711" Y="22.2192578125" />
                  <Point X="1.104594604492" Y="22.204537109375" />
                  <Point X="1.086789550781" Y="22.189732421875" />
                  <Point X="0.924610717773" Y="22.054884765625" />
                  <Point X="0.904141174316" Y="22.03113671875" />
                  <Point X="0.887249023438" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.870300720215" Y="21.949697265625" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724609375" Y="21.710373046875" />
                  <Point X="0.819742370605" Y="21.676880859375" />
                  <Point X="1.017714904785" Y="20.17312890625" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975714233398" Y="20.129923828125" />
                  <Point X="0.960407287598" Y="20.127142578125" />
                  <Point X="0.929315856934" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058429931641" Y="20.247365234375" />
                  <Point X="-1.080698120117" Y="20.25309375" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.125546020508" Y="20.38792578125" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077270508" Y="20.4318984375" />
                  <Point X="-1.119451660156" Y="20.45896484375" />
                  <Point X="-1.121759033203" Y="20.490669921875" />
                  <Point X="-1.123333862305" Y="20.502306640625" />
                  <Point X="-1.129321655273" Y="20.53241015625" />
                  <Point X="-1.183861206055" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026855469" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230573852539" Y="20.905138671875" />
                  <Point X="-1.250209106445" Y="20.929064453125" />
                  <Point X="-1.261008056641" Y="20.94022265625" />
                  <Point X="-1.284083496094" Y="20.960458984375" />
                  <Point X="-1.494269042969" Y="21.144787109375" />
                  <Point X="-1.506739868164" Y="21.15403515625" />
                  <Point X="-1.533022705078" Y="21.17037890625" />
                  <Point X="-1.546835083008" Y="21.177474609375" />
                  <Point X="-1.576532592773" Y="21.189775390625" />
                  <Point X="-1.591314941406" Y="21.1945234375" />
                  <Point X="-1.621454223633" Y="21.20155078125" />
                  <Point X="-1.636811279297" Y="21.203830078125" />
                  <Point X="-1.66743762207" Y="21.205837890625" />
                  <Point X="-1.946400390625" Y="21.22412109375" />
                  <Point X="-1.96192578125" Y="21.2238671875" />
                  <Point X="-1.992725463867" Y="21.220833984375" />
                  <Point X="-2.007999633789" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053669189453" Y="21.204505859375" />
                  <Point X="-2.081862060547" Y="21.191732421875" />
                  <Point X="-2.09543359375" Y="21.184189453125" />
                  <Point X="-2.120953125" Y="21.167138671875" />
                  <Point X="-2.353400390625" Y="21.011822265625" />
                  <Point X="-2.359678466797" Y="21.00724609375" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201171875" Y="20.837521484375" />
                  <Point X="-2.747604248047" Y="20.988849609375" />
                  <Point X="-2.778404541016" Y="21.012564453125" />
                  <Point X="-2.980863037109" Y="21.16844921875" />
                  <Point X="-2.415132080078" Y="22.148326171875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334848144531" Y="22.289921875" />
                  <Point X="-2.323944091797" Y="22.3188984375" />
                  <Point X="-2.319680664062" Y="22.333833984375" />
                  <Point X="-2.313411132812" Y="22.36537109375" />
                  <Point X="-2.311622070312" Y="22.381333984375" />
                  <Point X="-2.310759277344" Y="22.413333984375" />
                  <Point X="-2.315300537109" Y="22.445021484375" />
                  <Point X="-2.325115234375" Y="22.475490234375" />
                  <Point X="-2.331314941406" Y="22.49030859375" />
                  <Point X="-2.34724609375" Y="22.520841796875" />
                  <Point X="-2.354884277344" Y="22.53310546875" />
                  <Point X="-2.371932617188" Y="22.556333984375" />
                  <Point X="-2.381342773438" Y="22.567298828125" />
                  <Point X="-2.396975097656" Y="22.582931640625" />
                  <Point X="-2.408815429688" Y="22.592978515625" />
                  <Point X="-2.433974853516" Y="22.6110078125" />
                  <Point X="-2.447293945312" Y="22.618990234375" />
                  <Point X="-2.476123291016" Y="22.63320703125" />
                  <Point X="-2.490563232422" Y="22.6389140625" />
                  <Point X="-2.520180908203" Y="22.6478984375" />
                  <Point X="-2.550869140625" Y="22.6519375" />
                  <Point X="-2.581802978516" Y="22.650923828125" />
                  <Point X="-2.597226318359" Y="22.6491484375" />
                  <Point X="-2.628753173828" Y="22.642876953125" />
                  <Point X="-2.643683837891" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.686683837891" Y="22.621072265625" />
                  <Point X="-3.793089111328" Y="21.9822890625" />
                  <Point X="-4.004013671875" Y="22.25940234375" />
                  <Point X="-4.026108886719" Y="22.296453125" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.176974365234" Y="23.327244140625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.035584228516" Y="23.4376640625" />
                  <Point X="-3.012773193359" Y="23.46281640625" />
                  <Point X="-3.002500244141" Y="23.476419921875" />
                  <Point X="-2.983773193359" Y="23.506494140625" />
                  <Point X="-2.977108154297" Y="23.519263671875" />
                  <Point X="-2.965785888672" Y="23.545662109375" />
                  <Point X="-2.961128662109" Y="23.559291015625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552978516" Y="23.601193359375" />
                  <Point X="-2.948748291016" Y="23.631615234375" />
                  <Point X="-2.948576904297" Y="23.646939453125" />
                  <Point X="-2.950782958984" Y="23.678603515625" />
                  <Point X="-2.953232177734" Y="23.69440234375" />
                  <Point X="-2.960745849609" Y="23.7253671875" />
                  <Point X="-2.973346679688" Y="23.754634765625" />
                  <Point X="-2.990676513672" Y="23.781373046875" />
                  <Point X="-3.000469970703" Y="23.794009765625" />
                  <Point X="-3.023927490234" Y="23.819517578125" />
                  <Point X="-3.034150634766" Y="23.82910546875" />
                  <Point X="-3.055895019531" Y="23.846673828125" />
                  <Point X="-3.067416259766" Y="23.854654296875" />
                  <Point X="-3.091279052734" Y="23.868697265625" />
                  <Point X="-3.105447265625" Y="23.8755234375" />
                  <Point X="-3.134708496094" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891138671875" />
                  <Point X="-3.181687011719" Y="23.897619140625" />
                  <Point X="-3.197298583984" Y="23.89946484375" />
                  <Point X="-3.228619628906" Y="23.900556640625" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.746606933594" Y="24.06674609375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.655069824219" Y="24.64849609375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.499215087891" Y="24.69075390625" />
                  <Point X="-3.472700683594" Y="24.701310546875" />
                  <Point X="-3.454379150391" Y="24.710443359375" />
                  <Point X="-3.448363769531" Y="24.713716796875" />
                  <Point X="-3.430810791016" Y="24.724396484375" />
                  <Point X="-3.4058125" Y="24.74174609375" />
                  <Point X="-3.392719970703" Y="24.75269921875" />
                  <Point X="-3.368705322266" Y="24.7767734375" />
                  <Point X="-3.357783203125" Y="24.78989453125" />
                  <Point X="-3.337623291016" Y="24.81909765625" />
                  <Point X="-3.330367675781" Y="24.83152734375" />
                  <Point X="-3.317817871094" Y="24.857337890625" />
                  <Point X="-3.312523681641" Y="24.87071875" />
                  <Point X="-3.304187744141" Y="24.897576171875" />
                  <Point X="-3.300801025391" Y="24.9128125" />
                  <Point X="-3.296568603516" Y="24.94363671875" />
                  <Point X="-3.295722900391" Y="24.959224609375" />
                  <Point X="-3.296637451172" Y="24.99181640625" />
                  <Point X="-3.297882324219" Y="25.004708984375" />
                  <Point X="-3.302114013672" Y="25.030203125" />
                  <Point X="-3.305100830078" Y="25.0428046875" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.319988769531" Y="25.0855390625" />
                  <Point X="-3.335842773438" Y="25.11584765625" />
                  <Point X="-3.345144775391" Y="25.13028125" />
                  <Point X="-3.367133544922" Y="25.15844140625" />
                  <Point X="-3.376748046875" Y="25.169009765625" />
                  <Point X="-3.397455810547" Y="25.1885859375" />
                  <Point X="-3.408549072266" Y="25.19759375" />
                  <Point X="-3.433556396484" Y="25.214951171875" />
                  <Point X="-3.440483886719" Y="25.219330078125" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496563720703" Y="25.24563671875" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731332519531" Y="25.95751953125" />
                  <Point X="-4.719562988281" Y="26.000953125" />
                  <Point X="-4.633584960938" Y="26.318236328125" />
                  <Point X="-3.883283447266" Y="26.21945703125" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056640625" Y="26.204365234375" />
                  <Point X="-3.736703125" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704891113281" Y="26.208052734375" />
                  <Point X="-3.684603759766" Y="26.212087890625" />
                  <Point X="-3.668490966797" Y="26.216576171875" />
                  <Point X="-3.613141357422" Y="26.23402734375" />
                  <Point X="-3.603446289062" Y="26.237677734375" />
                  <Point X="-3.584520996094" Y="26.246005859375" />
                  <Point X="-3.575290771484" Y="26.25068359375" />
                  <Point X="-3.556546386719" Y="26.26150390625" />
                  <Point X="-3.547870849609" Y="26.2671640625" />
                  <Point X="-3.531185791016" Y="26.279396484375" />
                  <Point X="-3.515930664062" Y="26.293373046875" />
                  <Point X="-3.502289550781" Y="26.308927734375" />
                  <Point X="-3.495894042969" Y="26.317078125" />
                  <Point X="-3.483479492188" Y="26.33480859375" />
                  <Point X="-3.478010498047" Y="26.343603515625" />
                  <Point X="-3.468061523438" Y="26.36173828125" />
                  <Point X="-3.461143310547" Y="26.37696484375" />
                  <Point X="-3.438934082031" Y="26.43058203125" />
                  <Point X="-3.435498291016" Y="26.440353515625" />
                  <Point X="-3.429710693359" Y="26.4602109375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011474609" Y="26.60452734375" />
                  <Point X="-3.443504882812" Y="26.623798828125" />
                  <Point X="-3.450719482422" Y="26.6388828125" />
                  <Point X="-3.477517333984" Y="26.690359375" />
                  <Point X="-3.482793945312" Y="26.699275390625" />
                  <Point X="-3.494288574219" Y="26.71648046875" />
                  <Point X="-3.500506591797" Y="26.72476953125" />
                  <Point X="-3.514418701172" Y="26.741349609375" />
                  <Point X="-3.521498535156" Y="26.748908203125" />
                  <Point X="-3.536440917969" Y="26.763212890625" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.227613769531" Y="27.294283203125" />
                  <Point X="-4.002296142578" Y="27.6803046875" />
                  <Point X="-3.971125488281" Y="27.72037109375" />
                  <Point X="-3.726336181641" Y="28.035013671875" />
                  <Point X="-3.318778076172" Y="27.799708984375" />
                  <Point X="-3.254156738281" Y="27.762400390625" />
                  <Point X="-3.244924560547" Y="27.757720703125" />
                  <Point X="-3.225997070313" Y="27.749390625" />
                  <Point X="-3.216301757812" Y="27.745740234375" />
                  <Point X="-3.195659667969" Y="27.73923046875" />
                  <Point X="-3.185621582031" Y="27.736658203125" />
                  <Point X="-3.165326660156" Y="27.73262109375" />
                  <Point X="-3.146606689453" Y="27.730416015625" />
                  <Point X="-3.069520507812" Y="27.723671875" />
                  <Point X="-3.059170410156" Y="27.723333984375" />
                  <Point X="-3.038495117188" Y="27.72378515625" />
                  <Point X="-3.028169921875" Y="27.72457421875" />
                  <Point X="-3.006713623047" Y="27.7273984375" />
                  <Point X="-2.996531494141" Y="27.72930859375" />
                  <Point X="-2.976441162109" Y="27.734224609375" />
                  <Point X="-2.957009521484" Y="27.741296875" />
                  <Point X="-2.938458984375" Y="27.750443359375" />
                  <Point X="-2.929431884766" Y="27.7555234375" />
                  <Point X="-2.911178710938" Y="27.767150390625" />
                  <Point X="-2.902758056641" Y="27.77318359375" />
                  <Point X="-2.886626953125" Y="27.78612890625" />
                  <Point X="-2.872905761719" Y="27.79905078125" />
                  <Point X="-2.818189208984" Y="27.853767578125" />
                  <Point X="-2.811272216797" Y="27.861482421875" />
                  <Point X="-2.798320068359" Y="27.877619140625" />
                  <Point X="-2.792284912109" Y="27.886041015625" />
                  <Point X="-2.780655761719" Y="27.904294921875" />
                  <Point X="-2.775575927734" Y="27.913318359375" />
                  <Point X="-2.766427001953" Y="27.931869140625" />
                  <Point X="-2.759351318359" Y="27.95130859375" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034048828125" />
                  <Point X="-2.749537841797" Y="28.052865234375" />
                  <Point X="-2.756281982422" Y="28.129953125" />
                  <Point X="-2.75774609375" Y="28.14020703125" />
                  <Point X="-2.76178125" Y="28.1604921875" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059386962891" Y="28.699916015625" />
                  <Point X="-2.648372070312" Y="29.015037109375" />
                  <Point X="-2.599295410156" Y="29.042302734375" />
                  <Point X="-2.192525390625" Y="29.268296875" />
                  <Point X="-2.138346191406" Y="29.1976875" />
                  <Point X="-2.118563720703" Y="29.171908203125" />
                  <Point X="-2.111819580078" Y="29.164046875" />
                  <Point X="-2.097512695312" Y="29.1491015625" />
                  <Point X="-2.089950195312" Y="29.14201953125" />
                  <Point X="-2.073371582031" Y="29.128109375" />
                  <Point X="-2.065085205078" Y="29.121892578125" />
                  <Point X="-2.047896118164" Y="29.110408203125" />
                  <Point X="-2.029573974609" Y="29.100234375" />
                  <Point X="-1.94377722168" Y="29.0555703125" />
                  <Point X="-1.934347167969" Y="29.051294921875" />
                  <Point X="-1.915067993164" Y="29.043794921875" />
                  <Point X="-1.905219604492" Y="29.0405703125" />
                  <Point X="-1.884312744141" Y="29.034966796875" />
                  <Point X="-1.874174316406" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.833054077148" Y="29.028783203125" />
                  <Point X="-1.812407714844" Y="29.03013671875" />
                  <Point X="-1.802120117188" Y="29.031376953125" />
                  <Point X="-1.7808046875" Y="29.03513671875" />
                  <Point X="-1.77071081543" Y="29.0374921875" />
                  <Point X="-1.750852661133" Y="29.043283203125" />
                  <Point X="-1.731277832031" Y="29.050783203125" />
                  <Point X="-1.641914794922" Y="29.087798828125" />
                  <Point X="-1.63258203125" Y="29.092275390625" />
                  <Point X="-1.614453369141" Y="29.102220703125" />
                  <Point X="-1.605657104492" Y="29.107689453125" />
                  <Point X="-1.587927612305" Y="29.120103515625" />
                  <Point X="-1.579779174805" Y="29.126498046875" />
                  <Point X="-1.564227050781" Y="29.14013671875" />
                  <Point X="-1.550252197266" Y="29.155388671875" />
                  <Point X="-1.538020751953" Y="29.1720703125" />
                  <Point X="-1.532360595703" Y="29.180744140625" />
                  <Point X="-1.521538696289" Y="29.19948828125" />
                  <Point X="-1.516857055664" Y="29.2087265625" />
                  <Point X="-1.508526611328" Y="29.227658203125" />
                  <Point X="-1.501684814453" Y="29.247478515625" />
                  <Point X="-1.472598876953" Y="29.339728515625" />
                  <Point X="-1.470026855469" Y="29.349763671875" />
                  <Point X="-1.465991088867" Y="29.370052734375" />
                  <Point X="-1.46452722168" Y="29.3803046875" />
                  <Point X="-1.46264074707" Y="29.401865234375" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.56265625" />
                  <Point X="-0.931166137695" Y="29.7163203125" />
                  <Point X="-0.87168572998" Y="29.723283203125" />
                  <Point X="-0.365222412109" Y="29.78255859375" />
                  <Point X="-0.244783157349" Y="29.333072265625" />
                  <Point X="-0.225666397095" Y="29.2617265625" />
                  <Point X="-0.220435317993" Y="29.247107421875" />
                  <Point X="-0.207661911011" Y="29.218916015625" />
                  <Point X="-0.200119308472" Y="29.20534375" />
                  <Point X="-0.182260925293" Y="29.1786171875" />
                  <Point X="-0.172608901978" Y="29.166455078125" />
                  <Point X="-0.151451400757" Y="29.143865234375" />
                  <Point X="-0.126896362305" Y="29.1250234375" />
                  <Point X="-0.09960043335" Y="29.11043359375" />
                  <Point X="-0.085353881836" Y="29.1042578125" />
                  <Point X="-0.054915763855" Y="29.09392578125" />
                  <Point X="-0.03985357666" Y="29.090154296875" />
                  <Point X="-0.009317810059" Y="29.08511328125" />
                  <Point X="0.021629640579" Y="29.08511328125" />
                  <Point X="0.052165405273" Y="29.090154296875" />
                  <Point X="0.067227592468" Y="29.09392578125" />
                  <Point X="0.097665718079" Y="29.1042578125" />
                  <Point X="0.111912109375" Y="29.11043359375" />
                  <Point X="0.139208190918" Y="29.1250234375" />
                  <Point X="0.16376322937" Y="29.143865234375" />
                  <Point X="0.184920883179" Y="29.166455078125" />
                  <Point X="0.194573196411" Y="29.1786171875" />
                  <Point X="0.212431289673" Y="29.20534375" />
                  <Point X="0.219973754883" Y="29.218916015625" />
                  <Point X="0.232747146606" Y="29.247107421875" />
                  <Point X="0.237978225708" Y="29.2617265625" />
                  <Point X="0.378190643311" Y="29.7850078125" />
                  <Point X="0.827855041504" Y="29.737916015625" />
                  <Point X="0.877104858398" Y="29.726025390625" />
                  <Point X="1.453611816406" Y="29.5868359375" />
                  <Point X="1.483337524414" Y="29.576056640625" />
                  <Point X="1.858268188477" Y="29.440064453125" />
                  <Point X="1.889251708984" Y="29.425576171875" />
                  <Point X="2.250426757812" Y="29.2566640625" />
                  <Point X="2.28044921875" Y="29.239173828125" />
                  <Point X="2.629442138672" Y="29.0358515625" />
                  <Point X="2.657672119141" Y="29.015775390625" />
                  <Point X="2.817779541016" Y="28.901916015625" />
                  <Point X="2.150545166016" Y="27.746232421875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.061336425781" Y="27.59093359375" />
                  <Point X="2.050845703125" Y="27.56728515625" />
                  <Point X="2.043720458984" Y="27.5472578125" />
                  <Point X="2.039177612305" Y="27.53265234375" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017543945313" Y="27.449076171875" />
                  <Point X="2.014337646484" Y="27.4264140625" />
                  <Point X="2.013419433594" Y="27.414984375" />
                  <Point X="2.01291003418" Y="27.38921484375" />
                  <Point X="2.014239135742" Y="27.3627109375" />
                  <Point X="2.021782348633" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.02914453125" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045318847656" Y="27.22388671875" />
                  <Point X="2.055678222656" Y="27.20384765625" />
                  <Point X="2.065704589844" Y="27.187873046875" />
                  <Point X="2.104412597656" Y="27.130828125" />
                  <Point X="2.111471923828" Y="27.12167578125" />
                  <Point X="2.126644042969" Y="27.104302734375" />
                  <Point X="2.134756835938" Y="27.09608203125" />
                  <Point X="2.154318847656" Y="27.07853125" />
                  <Point X="2.174518066406" Y="27.06273046875" />
                  <Point X="2.231563964844" Y="27.024021484375" />
                  <Point X="2.241282226562" Y="27.018240234375" />
                  <Point X="2.261330322266" Y="27.007876953125" />
                  <Point X="2.27166015625" Y="27.003294921875" />
                  <Point X="2.293749267578" Y="26.995029296875" />
                  <Point X="2.304551757812" Y="26.991705078125" />
                  <Point X="2.326473144531" Y="26.98636328125" />
                  <Point X="2.344459960938" Y="26.983517578125" />
                  <Point X="2.407016845703" Y="26.975974609375" />
                  <Point X="2.418783691406" Y="26.975291015625" />
                  <Point X="2.442310302734" Y="26.975388671875" />
                  <Point X="2.454070068359" Y="26.976169921875" />
                  <Point X="2.480914794922" Y="26.9796328125" />
                  <Point X="2.505690185547" Y="26.98451953125" />
                  <Point X="2.578034179688" Y="27.003865234375" />
                  <Point X="2.583991943359" Y="27.005669921875" />
                  <Point X="2.604414794922" Y="27.0130703125" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.940403076172" Y="27.780953125" />
                  <Point X="4.043959960938" Y="27.637033203125" />
                  <Point X="4.059694824219" Y="27.611029296875" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.285173339844" Y="26.829931640625" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.166202392578" Y="26.738123046875" />
                  <Point X="3.147072021484" Y="26.71983203125" />
                  <Point X="3.132111328125" Y="26.703125" />
                  <Point X="3.122860839844" Y="26.69196484375" />
                  <Point X="3.070794677734" Y="26.624041015625" />
                  <Point X="3.0642578125" Y="26.614328125" />
                  <Point X="3.052428466797" Y="26.594169921875" />
                  <Point X="3.047135986328" Y="26.583724609375" />
                  <Point X="3.036536865234" Y="26.55916015625" />
                  <Point X="3.028010498047" Y="26.53505859375" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.006447265625" Y="26.344099609375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.025467773438" Y="26.255546875" />
                  <Point X="3.033056152344" Y="26.233234375" />
                  <Point X="3.037545166016" Y="26.2223125" />
                  <Point X="3.049497558594" Y="26.19770703125" />
                  <Point X="3.061673828125" Y="26.176296875" />
                  <Point X="3.104977294922" Y="26.110478515625" />
                  <Point X="3.111737792969" Y="26.10142578125" />
                  <Point X="3.126289794922" Y="26.084181640625" />
                  <Point X="3.134081298828" Y="26.075990234375" />
                  <Point X="3.151320556641" Y="26.059904296875" />
                  <Point X="3.160028564453" Y="26.05269921875" />
                  <Point X="3.178235839844" Y="26.039375" />
                  <Point X="3.194628662109" Y="26.029375" />
                  <Point X="3.257381591797" Y="25.99405078125" />
                  <Point X="3.268231689453" Y="25.988818359375" />
                  <Point X="3.29051171875" Y="25.979767578125" />
                  <Point X="3.301941650391" Y="25.97594921875" />
                  <Point X="3.329232177734" Y="25.968720703125" />
                  <Point X="3.352987548828" Y="25.964025390625" />
                  <Point X="3.437833496094" Y="25.9528125" />
                  <Point X="3.444033203125" Y="25.95219921875" />
                  <Point X="3.465708496094" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603271484" Y="25.952796875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.75268359375" Y="25.914236328125" />
                  <Point X="4.75764453125" Y="25.882369140625" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.820600585938" Y="25.455814453125" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.683407714844" Y="25.418609375" />
                  <Point X="3.6582578125" Y="25.408783203125" />
                  <Point X="3.637026123047" Y="25.398486328125" />
                  <Point X="3.624854736328" Y="25.392029296875" />
                  <Point X="3.541496337891" Y="25.34384765625" />
                  <Point X="3.531555175781" Y="25.337234375" />
                  <Point X="3.512583496094" Y="25.32281640625" />
                  <Point X="3.503552978516" Y="25.31501171875" />
                  <Point X="3.483749755859" Y="25.29559375" />
                  <Point X="3.467306396484" Y="25.27723046875" />
                  <Point X="3.417291503906" Y="25.2135" />
                  <Point X="3.410854003906" Y="25.20420703125" />
                  <Point X="3.399129638672" Y="25.18492578125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005126953" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.368546386719" Y="25.100916015625" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350363525391" Y="25.001916015625" />
                  <Point X="3.348864990234" Y="24.9779296875" />
                  <Point X="3.348877441406" Y="24.965888671875" />
                  <Point X="3.350708007812" Y="24.9375234375" />
                  <Point X="3.353705078125" Y="24.91401953125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158691406" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067871094" Y="24.783966796875" />
                  <Point X="3.393841796875" Y="24.762501953125" />
                  <Point X="3.399129394531" Y="24.752513671875" />
                  <Point X="3.410849121094" Y="24.733240234375" />
                  <Point X="3.422772216797" Y="24.71695703125" />
                  <Point X="3.472787109375" Y="24.6532265625" />
                  <Point X="3.480982666016" Y="24.644068359375" />
                  <Point X="3.498490966797" Y="24.626884765625" />
                  <Point X="3.507803710938" Y="24.618859375" />
                  <Point X="3.531266845703" Y="24.601150390625" />
                  <Point X="3.550644287109" Y="24.588306640625" />
                  <Point X="3.634002685547" Y="24.540123046875" />
                  <Point X="3.639487548828" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027832031" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.06894921875" />
                  <Point X="4.755256347656" Y="24.04108984375" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.585932373047" Y="24.071109375" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.424389892578" Y="24.09155859375" />
                  <Point X="3.399602539062" Y="24.091560546875" />
                  <Point X="3.387207519531" Y="24.09075" />
                  <Point X="3.352100341797" Y="24.0861328125" />
                  <Point X="3.336521728516" Y="24.08341796875" />
                  <Point X="3.172918701172" Y="24.047859375" />
                  <Point X="3.161478515625" Y="24.044611328125" />
                  <Point X="3.13111328125" Y="24.03389453125" />
                  <Point X="3.110062255859" Y="24.023375" />
                  <Point X="3.077429199219" Y="24.00163671875" />
                  <Point X="3.062248779297" Y="23.98906640625" />
                  <Point X="3.038892089844" Y="23.965234375" />
                  <Point X="3.028492675781" Y="23.95371875" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.923183105469" Y="23.826103515625" />
                  <Point X="2.907157226562" Y="23.80163671875" />
                  <Point X="2.897476318359" Y="23.782404296875" />
                  <Point X="2.885051025391" Y="23.74865234375" />
                  <Point X="2.880450195312" Y="23.731181640625" />
                  <Point X="2.875298583984" Y="23.69971875" />
                  <Point X="2.873601318359" Y="23.6864296875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.85908203125" Y="23.520515625" />
                  <Point X="2.860162597656" Y="23.488328125" />
                  <Point X="2.863900878906" Y="23.464943359375" />
                  <Point X="2.874951660156" Y="23.42701171875" />
                  <Point X="2.882470703125" Y="23.408626953125" />
                  <Point X="2.898922119141" Y="23.378001953125" />
                  <Point X="2.906480712891" Y="23.365166015625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="4.087171142578" Y="22.370015625" />
                  <Point X="4.045490234375" Y="22.3025703125" />
                  <Point X="4.032345214844" Y="22.28389453125" />
                  <Point X="4.001273925781" Y="22.23974609375" />
                  <Point X="2.981292724609" Y="22.8286328125" />
                  <Point X="2.848454345703" Y="22.905326171875" />
                  <Point X="2.84119140625" Y="22.90911328125" />
                  <Point X="2.8150234375" Y="22.920486328125" />
                  <Point X="2.783121582031" Y="22.930671875" />
                  <Point X="2.771114013672" Y="22.933658203125" />
                  <Point X="2.749737304688" Y="22.93751953125" />
                  <Point X="2.555023681641" Y="22.97268359375" />
                  <Point X="2.539367675781" Y="22.974189453125" />
                  <Point X="2.508014404297" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444849853516" Y="22.964865234375" />
                  <Point X="2.415074707031" Y="22.95504296875" />
                  <Point X="2.400594238281" Y="22.94889453125" />
                  <Point X="2.382835205078" Y="22.939548828125" />
                  <Point X="2.221076171875" Y="22.854416015625" />
                  <Point X="2.208970458984" Y="22.846830078125" />
                  <Point X="2.186038085938" Y="22.829935546875" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144939697266" Y="22.788837890625" />
                  <Point X="2.128044921875" Y="22.765904296875" />
                  <Point X="2.120462158203" Y="22.75380078125" />
                  <Point X="2.111115722656" Y="22.736041015625" />
                  <Point X="2.025983154297" Y="22.574283203125" />
                  <Point X="2.019835205078" Y="22.559806640625" />
                  <Point X="2.010012084961" Y="22.530033203125" />
                  <Point X="2.006337158203" Y="22.514736328125" />
                  <Point X="2.001379394531" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.00218762207" Y="22.419859375" />
                  <Point X="2.006048217773" Y="22.398482421875" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723754150391" Y="20.963916015625" />
                  <Point X="1.935282470703" Y="21.991470703125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657958984" Y="22.12984765625" />
                  <Point X="1.80883581543" Y="22.150369140625" />
                  <Point X="1.78325402832" Y="22.171994140625" />
                  <Point X="1.773299682617" Y="22.179353515625" />
                  <Point X="1.752216430664" Y="22.192908203125" />
                  <Point X="1.560176025391" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517469970703" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455391479492" Y="22.3513046875" />
                  <Point X="1.424128417969" Y="22.353623046875" />
                  <Point X="1.408400512695" Y="22.353484375" />
                  <Point X="1.385342285156" Y="22.35136328125" />
                  <Point X="1.175313720703" Y="22.332037109375" />
                  <Point X="1.161224487305" Y="22.32966015625" />
                  <Point X="1.133570922852" Y="22.322826171875" />
                  <Point X="1.120006713867" Y="22.318369140625" />
                  <Point X="1.09262121582" Y="22.307025390625" />
                  <Point X="1.079876220703" Y="22.3005859375" />
                  <Point X="1.055493896484" Y="22.285865234375" />
                  <Point X="1.043856689453" Y="22.277583984375" />
                  <Point X="1.026051635742" Y="22.262779296875" />
                  <Point X="0.86387286377" Y="22.127931640625" />
                  <Point X="0.852652404785" Y="22.116908203125" />
                  <Point X="0.832182861328" Y="22.09316015625" />
                  <Point X="0.822933349609" Y="22.080435546875" />
                  <Point X="0.806041137695" Y="22.052609375" />
                  <Point X="0.799018981934" Y="22.03853125" />
                  <Point X="0.787394287109" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.777468383789" Y="21.969875" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584716797" Y="21.7387109375" />
                  <Point X="0.72472467041" Y="21.710322265625" />
                  <Point X="0.72474230957" Y="21.676830078125" />
                  <Point X="0.725555175781" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.694692565918" Y="21.364171875" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605957031" Y="21.519876953125" />
                  <Point X="0.642146362305" Y="21.54641796875" />
                  <Point X="0.626787353516" Y="21.576185546875" />
                  <Point X="0.620406494141" Y="21.58679296875" />
                  <Point X="0.604209716797" Y="21.61012890625" />
                  <Point X="0.456678771973" Y="21.822693359375" />
                  <Point X="0.446669311523" Y="21.834830078125" />
                  <Point X="0.42478326416" Y="21.857287109375" />
                  <Point X="0.412906799316" Y="21.867607421875" />
                  <Point X="0.386651824951" Y="21.886849609375" />
                  <Point X="0.373235168457" Y="21.89506640625" />
                  <Point X="0.345231658936" Y="21.909173828125" />
                  <Point X="0.330644897461" Y="21.915064453125" />
                  <Point X="0.305581298828" Y="21.922841796875" />
                  <Point X="0.077285949707" Y="21.993697265625" />
                  <Point X="0.063371723175" Y="21.996890625" />
                  <Point X="0.035223583221" Y="22.001158203125" />
                  <Point X="0.020989521027" Y="22.002232421875" />
                  <Point X="-0.00865212822" Y="22.002234375" />
                  <Point X="-0.022895553589" Y="22.001162109375" />
                  <Point X="-0.051062862396" Y="21.996892578125" />
                  <Point X="-0.064986602783" Y="21.9936953125" />
                  <Point X="-0.090050216675" Y="21.985916015625" />
                  <Point X="-0.318345855713" Y="21.9150625" />
                  <Point X="-0.332934967041" Y="21.909169921875" />
                  <Point X="-0.360934936523" Y="21.895060546875" />
                  <Point X="-0.374345794678" Y="21.886845703125" />
                  <Point X="-0.400601196289" Y="21.8676015625" />
                  <Point X="-0.412474090576" Y="21.857283203125" />
                  <Point X="-0.434358673096" Y="21.834828125" />
                  <Point X="-0.444370361328" Y="21.822689453125" />
                  <Point X="-0.46056729126" Y="21.7993515625" />
                  <Point X="-0.608098266602" Y="21.5867890625" />
                  <Point X="-0.612472839355" Y="21.579865234375" />
                  <Point X="-0.625977416992" Y="21.554734375" />
                  <Point X="-0.638778198242" Y="21.523783203125" />
                  <Point X="-0.642752929688" Y="21.512064453125" />
                  <Point X="-0.985425231934" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853757622652" Y="27.871231146982" />
                  <Point X="-4.150415351878" Y="27.235046593636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.581150018126" Y="26.311333120963" />
                  <Point X="-4.710795593825" Y="26.033307286663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683969734378" Y="28.010553298005" />
                  <Point X="-4.073216934224" Y="27.175809984147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.482391944102" Y="26.298331343764" />
                  <Point X="-4.773176349151" Y="25.67474217476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.601383121186" Y="27.962871711139" />
                  <Point X="-3.996018516571" Y="27.116573374658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.383633870078" Y="26.285329566565" />
                  <Point X="-4.715730502267" Y="25.573146040558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.518796507993" Y="27.915190124272" />
                  <Point X="-3.918820098917" Y="27.057336765169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.284875796054" Y="26.272327789366" />
                  <Point X="-4.622551942552" Y="25.54817895631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041744490766" Y="28.713442326259" />
                  <Point X="-3.05311642548" Y="28.689055133566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4362098948" Y="27.867508537406" />
                  <Point X="-3.841621681263" Y="26.99810015568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.18611772203" Y="26.259326012167" />
                  <Point X="-4.529373382837" Y="25.523211872063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878595685511" Y="28.838526917802" />
                  <Point X="-2.995129615036" Y="28.588619099462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353623281608" Y="27.81982695054" />
                  <Point X="-3.76442326361" Y="26.938863546191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087359648006" Y="26.246324234968" />
                  <Point X="-4.436194823122" Y="25.498244787815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.715446880256" Y="28.963611509344" />
                  <Point X="-2.937142804592" Y="28.488183065358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.27103647932" Y="27.772145769189" />
                  <Point X="-3.687224845956" Y="26.879626936702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.988601573982" Y="26.233322457769" />
                  <Point X="-4.343016263407" Y="25.473277703567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.565062988846" Y="29.061321654807" />
                  <Point X="-2.879155994148" Y="28.387747031254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183006292293" Y="27.736137964083" />
                  <Point X="-3.610026428303" Y="26.820390327213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.889843499958" Y="26.220320680571" />
                  <Point X="-4.249837703693" Y="25.448310619319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760977527065" Y="24.352167730748" />
                  <Point X="-4.781219860416" Y="24.30875790679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.423590499314" Y="29.13992123727" />
                  <Point X="-2.821169183704" Y="28.28731099715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.083430926751" Y="27.724888874201" />
                  <Point X="-3.533279210931" Y="26.760186115598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.791085211963" Y="26.207319362234" />
                  <Point X="-4.156659143978" Y="25.423343535071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.641189424385" Y="24.38426499554" />
                  <Point X="-4.756616320854" Y="24.13673121725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.282118009782" Y="29.218520819734" />
                  <Point X="-2.766956190936" Y="28.178781984923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973810181278" Y="27.735182171099" />
                  <Point X="-3.468814516639" Y="26.673641948234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683956654704" Y="26.212268144261" />
                  <Point X="-4.063480584263" Y="25.398376450823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.521401321705" Y="24.416362260333" />
                  <Point X="-4.727569662537" Y="23.97423282663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.168616203135" Y="29.237137079179" />
                  <Point X="-2.751908370999" Y="27.986262988515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808461719039" Y="27.864983942266" />
                  <Point X="-3.422632044126" Y="26.547891429745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.556015107505" Y="26.261850527252" />
                  <Point X="-3.970302024548" Y="25.373409366576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.401613219026" Y="24.448459525125" />
                  <Point X="-4.690472382682" Y="23.828999049612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.102444550479" Y="29.154253495843" />
                  <Point X="-3.877123464833" Y="25.348442282328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.281825116346" Y="24.480556789918" />
                  <Point X="-4.638205221493" Y="23.716297188097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.024133785812" Y="29.097402322222" />
                  <Point X="-3.783944905118" Y="25.32347519808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.162037013666" Y="24.51265405471" />
                  <Point X="-4.526528485444" Y="23.730999571015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.939684689477" Y="29.053714843343" />
                  <Point X="-3.690766345403" Y="25.298508113832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.042248910986" Y="24.544751319503" />
                  <Point X="-4.414851749395" Y="23.745701953933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.846221562736" Y="29.029358015053" />
                  <Point X="-3.597587785689" Y="25.273541029584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.922460808306" Y="24.576848584295" />
                  <Point X="-4.303175013346" Y="23.760404336851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.731438666388" Y="29.050721580228" />
                  <Point X="-3.504520456257" Y="25.248335411224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.802672705626" Y="24.608945849088" />
                  <Point X="-4.191498277296" Y="23.775106719768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.374157554698" Y="29.592124246415" />
                  <Point X="-1.462622921973" Y="29.402409654069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.597336481549" Y="29.113515493271" />
                  <Point X="-3.4197390261" Y="25.205360624527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.682884602946" Y="24.64104311388" />
                  <Point X="-4.079821541247" Y="23.789809102686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.253572168614" Y="29.625931290985" />
                  <Point X="-3.348139697594" Y="25.134116729611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563096547328" Y="24.673140277746" />
                  <Point X="-3.968144805198" Y="23.804511485604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.132986782529" Y="29.659738335556" />
                  <Point X="-3.299399737844" Y="25.013850760202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435786025381" Y="24.721369422716" />
                  <Point X="-3.856468069149" Y="23.819213868522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.012401396445" Y="29.693545380127" />
                  <Point X="-3.7447913331" Y="23.83391625144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.894985374166" Y="29.720555702082" />
                  <Point X="-3.633114597051" Y="23.848618634358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.784113293964" Y="29.733532494968" />
                  <Point X="-3.521437861002" Y="23.863321017275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102620278502" Y="22.616971300866" />
                  <Point X="-4.152922197801" Y="22.509098486816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.673241490063" Y="29.746508695324" />
                  <Point X="-3.409761124952" Y="23.878023400193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93939606875" Y="22.742217597875" />
                  <Point X="-4.094097578979" Y="22.410459138577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.562369686162" Y="29.75948489568" />
                  <Point X="-3.298084388903" Y="23.892725783111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776171858998" Y="22.867463894884" />
                  <Point X="-4.035272960156" Y="22.311819790338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.451497882261" Y="29.772461096036" />
                  <Point X="-3.190496035317" Y="23.898660601543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.612947649246" Y="22.992710191892" />
                  <Point X="-3.973554877886" Y="22.219385494488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.356736450857" Y="29.750888491079" />
                  <Point X="-3.098112117984" Y="23.871989401209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449723439494" Y="23.117956488901" />
                  <Point X="-3.908555161945" Y="22.133988684756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.318484659291" Y="29.608130572414" />
                  <Point X="-3.019834593507" Y="23.81506694377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.286499229742" Y="23.24320278591" />
                  <Point X="-3.843555446003" Y="22.048591875024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.280232867725" Y="29.46537265375" />
                  <Point X="-2.959409485957" Y="23.719859854684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123275179574" Y="23.368448740688" />
                  <Point X="-3.761017698" Y="22.00080549642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.241981097064" Y="29.322614690255" />
                  <Point X="-3.617580137395" Y="22.083619187399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.195245682028" Y="29.198049960834" />
                  <Point X="-3.474142576791" Y="22.166432878377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124960187437" Y="29.123988539994" />
                  <Point X="-3.330705016186" Y="22.249246569356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036197870565" Y="29.089550792408" />
                  <Point X="-3.187267455581" Y="22.332060260335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.392234154245" Y="29.783537084181" />
                  <Point X="0.358293909358" Y="29.710751994138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.071309177033" Y="29.095311249587" />
                  <Point X="-3.043829894976" Y="22.414873951313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.492174491378" Y="29.773070678402" />
                  <Point X="-2.900392334371" Y="22.497687642292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.592114828511" Y="29.762604272623" />
                  <Point X="-2.756954773766" Y="22.580501333271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692055165645" Y="29.752137866844" />
                  <Point X="-2.622464282692" Y="22.644127971722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791995502778" Y="29.741671461065" />
                  <Point X="-2.516417522228" Y="22.646756833036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888264169531" Y="29.723331132746" />
                  <Point X="-2.429696353109" Y="22.607941829967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.982478143248" Y="29.700584500991" />
                  <Point X="-2.357873207368" Y="22.537177912661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076692116964" Y="29.677837869235" />
                  <Point X="-2.310769244996" Y="22.413403535553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.515193463652" Y="21.975014383926" />
                  <Point X="-2.914961957782" Y="21.11770808166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.170906090681" Y="29.655091237479" />
                  <Point X="-2.837833198269" Y="21.058322089807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.265120064398" Y="29.632344605724" />
                  <Point X="-2.760704414554" Y="20.998936149855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.359334038114" Y="29.609597973968" />
                  <Point X="-2.680082275943" Y="20.947041733653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.453548011831" Y="29.586851342213" />
                  <Point X="-2.598745419286" Y="20.896680035248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.543208873485" Y="29.55434053013" />
                  <Point X="-2.517408562628" Y="20.846318336843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.632865611514" Y="29.521820874905" />
                  <Point X="-2.327270408561" Y="21.029281773693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.722522349544" Y="29.489301219679" />
                  <Point X="-2.175008053098" Y="21.131020298317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.812179087573" Y="29.456781564454" />
                  <Point X="-2.032686489471" Y="21.211440726055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.900085947039" Y="29.420509282539" />
                  <Point X="-1.922677643876" Y="21.22256630635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.986140157684" Y="29.380263982406" />
                  <Point X="-1.82096525288" Y="21.215900082343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072194368328" Y="29.340018682273" />
                  <Point X="-1.719252861884" Y="21.209233858336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158248578973" Y="29.299773382141" />
                  <Point X="-1.618351934733" Y="21.200827444498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.244302789618" Y="29.259528082008" />
                  <Point X="-1.528919284875" Y="21.167827230639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326988928198" Y="29.212059928024" />
                  <Point X="-1.452090662466" Y="21.107797592688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.409416715369" Y="29.164037737655" />
                  <Point X="-1.377693752453" Y="21.042553130676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49184450254" Y="29.116015547286" />
                  <Point X="-1.30329684244" Y="20.977308668664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574272289711" Y="29.067993356916" />
                  <Point X="-1.231565759561" Y="20.906347321915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.655472872302" Y="29.017339417833" />
                  <Point X="-1.18069066173" Y="20.790660170895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73419017287" Y="28.961360063266" />
                  <Point X="2.239046903242" Y="27.899521894904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013485223517" Y="27.415803311733" />
                  <Point X="-1.149347436612" Y="20.633086783671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.812907489828" Y="28.905380743847" />
                  <Point X="2.7840477986" Y="28.843490936284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.037572124584" Y="27.242668687364" />
                  <Point X="-0.582833748428" Y="21.623190158147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.653695224386" Y="21.471227232557" />
                  <Point X="-1.120298009303" Y="20.470594331173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.096013114781" Y="27.143206644885" />
                  <Point X="-0.348259086342" Y="21.901447993967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.795290605187" Y="20.942785808118" />
                  <Point X="-1.113021339035" Y="20.261410050522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.16630369744" Y="27.069156135443" />
                  <Point X="-0.223340165781" Y="21.944548333214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.936885985988" Y="20.414344383678" />
                  <Point X="-1.018375414495" Y="20.239589740298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.246197360556" Y="27.015699498501" />
                  <Point X="-0.100782383367" Y="21.982585195362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.336592040887" Y="26.98476236565" />
                  <Point X="0.013200410444" Y="22.002232935109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.437031700791" Y="26.975366761007" />
                  <Point X="0.109394148854" Y="21.983731922441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.551880368325" Y="26.996871372947" />
                  <Point X="0.200962639575" Y="21.955312034092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.684098613274" Y="27.055625163856" />
                  <Point X="0.292531130295" Y="21.926892145742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.827536206294" Y="27.138438924349" />
                  <Point X="0.380450793942" Y="21.890647322482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.970973799313" Y="27.221252684842" />
                  <Point X="0.454701954433" Y="21.825090299612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.114411392333" Y="27.304066445334" />
                  <Point X="0.51752492706" Y="21.735025448779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.257848985353" Y="27.386880205827" />
                  <Point X="0.580222029876" Y="21.644690669263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.401286578373" Y="27.46969396632" />
                  <Point X="0.939833678481" Y="22.191091187995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7272262849" Y="21.735153161107" />
                  <Point X="0.640607962622" Y="21.54939956954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.544724171393" Y="27.552507726813" />
                  <Point X="3.163979113641" Y="26.735997315514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001160091686" Y="26.386830796141" />
                  <Point X="1.100174562762" Y="22.310154173575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741745709822" Y="21.541501017934" />
                  <Point X="0.681687799334" Y="21.412706413262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.688161764413" Y="27.635321487306" />
                  <Point X="3.327884907133" Y="26.862705273571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033665958568" Y="26.231750702228" />
                  <Point X="1.216987804396" Y="22.335871828268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764824039733" Y="21.366203505744" />
                  <Point X="0.719939628234" Y="21.269948574662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.831599357432" Y="27.718135247798" />
                  <Point X="3.491108856706" Y="26.987951012623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.091493462646" Y="26.13097303452" />
                  <Point X="1.326508018997" Y="22.345949536015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.787902369645" Y="21.190905993554" />
                  <Point X="0.758191473879" Y="21.12719077197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955760318948" Y="27.759610138626" />
                  <Point X="3.654332806279" Y="27.113196751676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.159874231151" Y="26.052826915408" />
                  <Point X="1.434546870726" Y="22.352850450832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.810980699557" Y="21.015608481364" />
                  <Point X="0.796443319524" Y="20.984432969278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019362961879" Y="27.671217296156" />
                  <Point X="3.817556755852" Y="27.238442490728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241465239394" Y="26.003010246837" />
                  <Point X="1.529217273156" Y="22.331082633612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.08026592256" Y="27.577034966416" />
                  <Point X="3.980780705425" Y="27.363688229781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330206833789" Y="25.968528059754" />
                  <Point X="1.611724409103" Y="22.283230607241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.428287712011" Y="25.954074031473" />
                  <Point X="1.692368932456" Y="22.231384195273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.534600162214" Y="25.957272666269" />
                  <Point X="1.773013366525" Y="22.179537591837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.646276992587" Y="25.971975251466" />
                  <Point X="2.177828325698" Y="22.822876922908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.00058953914" Y="22.442787118553" />
                  <Point X="1.845001459983" Y="22.109127406051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757953822961" Y="25.986677836663" />
                  <Point X="2.322164809868" Y="22.907618361693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.028019755189" Y="22.276822256301" />
                  <Point X="1.910200549561" Y="22.024158154463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.869630653334" Y="26.00138042186" />
                  <Point X="3.570959161197" Y="25.360877340013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356290602752" Y="24.900519130811" />
                  <Point X="2.454794221194" Y="22.967253901747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.066029840303" Y="22.133545996477" />
                  <Point X="1.975399688574" Y="21.939189008888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981307483708" Y="26.016083007057" />
                  <Point X="3.705692445614" Y="25.425024650469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.395389624721" Y="24.759578103609" />
                  <Point X="3.008269648838" Y="23.929396636261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868347165904" Y="23.629331903275" />
                  <Point X="2.561593750223" Y="22.971497080456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.123749737714" Y="22.032537565528" />
                  <Point X="2.040598858494" Y="21.854219929593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.092984314081" Y="26.030785592254" />
                  <Point X="3.825480617421" Y="25.457122063505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.458880852339" Y="24.670946330229" />
                  <Point X="3.167630520573" Y="24.046357978157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876761312604" Y="23.422586948703" />
                  <Point X="2.658273069823" Y="22.95403740001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181736579983" Y="21.932101599673" />
                  <Point X="2.105798028414" Y="21.769250850299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204661144455" Y="26.045488177451" />
                  <Point X="3.945268775108" Y="25.48921944626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.531184392756" Y="24.601212622631" />
                  <Point X="3.28445588201" Y="24.07210162385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.93449066364" Y="23.321598791117" />
                  <Point X="2.754952302528" Y="22.936577533216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239723422252" Y="21.831665633818" />
                  <Point X="2.170997198334" Y="21.684281771005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.316337974828" Y="26.060190762648" />
                  <Point X="4.065056932795" Y="25.521316829014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613141634696" Y="24.552181344758" />
                  <Point X="3.398311253672" Y="24.091476105916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.995245695729" Y="23.227099227489" />
                  <Point X="2.845836882622" Y="22.906690993795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.297710264521" Y="21.731229667963" />
                  <Point X="2.236196368254" Y="21.599312691711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428014805201" Y="26.074893347845" />
                  <Point X="4.184845090481" Y="25.553414211769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.700115131591" Y="24.513907460351" />
                  <Point X="3.498973637826" Y="24.082558134969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065844029316" Y="23.153708692043" />
                  <Point X="2.928477541807" Y="22.859125308934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.35569710679" Y="21.630793702108" />
                  <Point X="2.301395538175" Y="21.514343612417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.539691635575" Y="26.089595933042" />
                  <Point X="4.304633248168" Y="25.585511594523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793293734972" Y="24.488940469745" />
                  <Point X="3.597731546833" Y="24.06955600389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.143042477457" Y="23.094472147934" />
                  <Point X="3.011064352804" Y="22.811444146259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.413683949059" Y="21.530357736253" />
                  <Point X="2.366594708095" Y="21.429374533122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.651368465948" Y="26.104298518239" />
                  <Point X="4.424421405854" Y="25.617608977278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.886472338353" Y="24.46397347914" />
                  <Point X="3.696489706111" Y="24.05655440952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220240925598" Y="23.035235603825" />
                  <Point X="3.093651024561" Y="22.763762684985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.471670791328" Y="21.429921770398" />
                  <Point X="2.431793878015" Y="21.344405453828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.723486724245" Y="26.03416747185" />
                  <Point X="4.544209563541" Y="25.649706360032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979650941734" Y="24.439006488535" />
                  <Point X="3.79524786539" Y="24.04355281515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.297439373739" Y="22.975999059716" />
                  <Point X="3.176237696318" Y="22.716081223711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.529657633597" Y="21.329485804543" />
                  <Point X="2.496993047935" Y="21.259436374534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.757614088858" Y="25.882564691044" />
                  <Point X="4.663997721228" Y="25.681803742787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072829545116" Y="24.414039497929" />
                  <Point X="3.894006024668" Y="24.03055122078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.374637821879" Y="22.916762515607" />
                  <Point X="3.258824368076" Y="22.668399762437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.587644475867" Y="21.229049838687" />
                  <Point X="2.562192217855" Y="21.17446729524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783852047273" Y="25.714043024045" />
                  <Point X="4.783785878914" Y="25.713901125542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.166008148497" Y="24.389072507324" />
                  <Point X="3.992764183947" Y="24.01754962641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.45183627002" Y="22.857525971498" />
                  <Point X="3.341411039833" Y="22.620718301164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.645631318136" Y="21.128613872832" />
                  <Point X="2.627391387775" Y="21.089498215946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.259186751878" Y="24.364105516719" />
                  <Point X="4.091522343225" Y="24.00454803204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529034718161" Y="22.798289427389" />
                  <Point X="3.423997711591" Y="22.57303683989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.703618160405" Y="21.028177906977" />
                  <Point X="2.692590557696" Y="21.004529136651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.352365355259" Y="24.339138526114" />
                  <Point X="4.190280502503" Y="23.99154643767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606233166302" Y="22.73905288328" />
                  <Point X="3.506584383348" Y="22.525355378616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.44554395864" Y="24.314171535508" />
                  <Point X="4.289038661782" Y="23.9785448433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.683431614443" Y="22.679816339171" />
                  <Point X="3.589171055106" Y="22.477673917342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.538722562021" Y="24.289204544903" />
                  <Point X="4.38779682106" Y="23.96554324893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760630062583" Y="22.620579795062" />
                  <Point X="3.671757726863" Y="22.429992456068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.631901165402" Y="24.264237554298" />
                  <Point X="4.486554980339" Y="23.95254165456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.837828510724" Y="22.561343250953" />
                  <Point X="3.75434439862" Y="22.382310994794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725079768783" Y="24.239270563692" />
                  <Point X="4.585313139617" Y="23.939540060189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.915026958865" Y="22.502106706844" />
                  <Point X="3.836931070378" Y="22.334629533521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766934809761" Y="24.104239838328" />
                  <Point X="4.684071298896" Y="23.926538465819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.992225407006" Y="22.442870162735" />
                  <Point X="3.919517742135" Y="22.286948072247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069423855147" Y="22.383633618626" />
                  <Point X="4.004397713154" Y="22.244184607108" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.99646484375" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.511166687012" Y="21.31499609375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.448122070312" Y="21.50179296875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.24927230835" Y="21.741376953125" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.033728199005" Y="21.804455078125" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.304475891113" Y="21.69101953125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.820180847168" Y="20.115791015625" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.100230712891" Y="20.061931640625" />
                  <Point X="-1.1280390625" Y="20.0690859375" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.313920532227" Y="20.4127265625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.315671020508" Y="20.49534375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.409358154297" Y="20.817607421875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.67986706543" Y="21.016244140625" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.015397827148" Y="21.009158203125" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.462103759766" Y="20.5886015625" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-2.894320556641" Y="20.862021484375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.579677001953" Y="22.243326171875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499764160156" Y="22.40241796875" />
                  <Point X="-2.5156953125" Y="22.432951171875" />
                  <Point X="-2.531327636719" Y="22.448583984375" />
                  <Point X="-2.560156982422" Y="22.46280078125" />
                  <Point X="-2.591683837891" Y="22.456529296875" />
                  <Point X="-3.753529052734" Y="21.78573828125" />
                  <Point X="-3.842959472656" Y="21.73410546875" />
                  <Point X="-3.850635498047" Y="21.744189453125" />
                  <Point X="-4.161703613281" Y="22.15287109375" />
                  <Point X="-4.189294433594" Y="22.19913671875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.292638916016" Y="23.477982421875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145059814453" Y="23.606927734375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140323486328" Y="23.665396484375" />
                  <Point X="-3.163781005859" Y="23.690904296875" />
                  <Point X="-3.187643798828" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.686249023438" Y="23.51833203125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.805732421875" Y="23.51251171875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.934692871094" Y="24.039845703125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.704245605469" Y="24.8320234375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.539142333984" Y="24.880486328125" />
                  <Point X="-3.514144042969" Y="24.8978359375" />
                  <Point X="-3.493984130859" Y="24.9270390625" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.486562744141" Y="24.98648828125" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.516887451172" Y="25.0415078125" />
                  <Point X="-3.541894775391" Y="25.058865234375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.894448242188" Y="25.424330078125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.996054199219" Y="25.466537109375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.902949707031" Y="26.0506484375" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.858483154297" Y="26.40783203125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731705078125" Y="26.3958671875" />
                  <Point X="-3.725628662109" Y="26.397783203125" />
                  <Point X="-3.670279052734" Y="26.415234375" />
                  <Point X="-3.651534667969" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.636681884766" Y="26.449671875" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.619258056641" Y="26.5511640625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968017578" Y="26.619220703125" />
                  <Point X="-4.426865234375" Y="27.207681640625" />
                  <Point X="-4.476105957031" Y="27.245466796875" />
                  <Point X="-4.46468359375" Y="27.26503515625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.121088378906" Y="27.8370390625" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.223778076172" Y="27.964251953125" />
                  <Point X="-3.159156738281" Y="27.926943359375" />
                  <Point X="-3.138514648438" Y="27.92043359375" />
                  <Point X="-3.130051513672" Y="27.919693359375" />
                  <Point X="-3.052965332031" Y="27.91294921875" />
                  <Point X="-3.031509033203" Y="27.9157734375" />
                  <Point X="-3.013255859375" Y="27.927400390625" />
                  <Point X="-3.007245117188" Y="27.93341015625" />
                  <Point X="-2.952528564453" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.938814697266" Y="28.036302734375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.291902832031" Y="28.72264453125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.283513671875" Y="28.76749609375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.691568847656" Y="29.208392578125" />
                  <Point X="-2.141548828125" Y="29.51397265625" />
                  <Point X="-1.987608398438" Y="29.3133515625" />
                  <Point X="-1.967825805664" Y="29.287572265625" />
                  <Point X="-1.951247070312" Y="29.273662109375" />
                  <Point X="-1.941827880859" Y="29.2687578125" />
                  <Point X="-1.856031005859" Y="29.22409375" />
                  <Point X="-1.835124267578" Y="29.218490234375" />
                  <Point X="-1.813808837891" Y="29.22225" />
                  <Point X="-1.803998046875" Y="29.226314453125" />
                  <Point X="-1.714634887695" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.682890014648" Y="29.304615234375" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.655037475586" Y="29.710701171875" />
                  <Point X="-0.968083435059" Y="29.903296875" />
                  <Point X="-0.893773803711" Y="29.911994140625" />
                  <Point X="-0.224199981689" Y="29.990359375" />
                  <Point X="-0.061257141113" Y="29.382248046875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282178879" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594097137" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.228580078125" Y="29.960755859375" />
                  <Point X="0.23664831543" Y="29.9908671875" />
                  <Point X="0.260404785156" Y="29.98837890625" />
                  <Point X="0.860210449219" Y="29.925564453125" />
                  <Point X="0.921694946289" Y="29.910720703125" />
                  <Point X="1.508456542969" Y="29.769056640625" />
                  <Point X="1.548115112305" Y="29.754673828125" />
                  <Point X="1.931044921875" Y="29.61578125" />
                  <Point X="1.969736206055" Y="29.5976875" />
                  <Point X="2.338685058594" Y="29.425140625" />
                  <Point X="2.376094726562" Y="29.403345703125" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.767786376953" Y="29.17061328125" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.315090087891" Y="27.651232421875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.222728027344" Y="27.4835703125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202872802734" Y="27.38545703125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.222932128906" Y="27.294548828125" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.281202148438" Y="27.219953125" />
                  <Point X="2.338248046875" Y="27.181244140625" />
                  <Point X="2.360337158203" Y="27.172978515625" />
                  <Point X="2.367205078125" Y="27.172150390625" />
                  <Point X="2.429761962891" Y="27.164607421875" />
                  <Point X="2.456606689453" Y="27.1680703125" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.885782714844" Y="27.96880859375" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.222250488281" Y="27.709392578125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.400837890625" Y="26.679193359375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.273655273438" Y="26.576375" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.210989990234" Y="26.48388671875" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.19252734375" Y="26.382494140625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.220400878906" Y="26.2807265625" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280943603516" Y="26.198822265625" />
                  <Point X="3.287837158203" Y="26.19494140625" />
                  <Point X="3.350590087891" Y="26.1596171875" />
                  <Point X="3.377880615234" Y="26.152388671875" />
                  <Point X="3.4627265625" Y="26.14117578125" />
                  <Point X="3.475803466797" Y="26.141171875" />
                  <Point X="4.753225097656" Y="26.30934765625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.945383300781" Y="25.911595703125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.869776367188" Y="25.272287109375" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.719935791016" Y="25.227529296875" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.616774169922" Y="25.1599296875" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.555154785156" Y="25.065177734375" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.540313476562" Y="24.9497578125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759277344" Y="24.841240234375" />
                  <Point X="3.572250244141" Y="24.8342421875" />
                  <Point X="3.622265136719" Y="24.77051171875" />
                  <Point X="3.645728271484" Y="24.752802734375" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167480469" Y="24.699611328125" />
                  <Point X="4.91262109375" Y="24.38572265625" />
                  <Point X="4.998068359375" Y="24.362826171875" />
                  <Point X="4.948431640625" Y="24.033599609375" />
                  <Point X="4.940494628906" Y="23.998818359375" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="3.561132568359" Y="23.882734375" />
                  <Point X="3.411982421875" Y="23.90237109375" />
                  <Point X="3.376875244141" Y="23.89775390625" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.197945556641" Y="23.856078125" />
                  <Point X="3.174588867188" Y="23.83224609375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.067953613281" Y="23.700482421875" />
                  <Point X="3.062802001953" Y="23.66901953125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.049849121094" Y="23.498541015625" />
                  <Point X="3.066300537109" Y="23.467916015625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.255576660156" Y="22.48028515625" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.204134765625" Y="22.197861328125" />
                  <Point X="4.187716308594" Y="22.17453515625" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="2.886292724609" Y="22.66408984375" />
                  <Point X="2.753454345703" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.715963867188" Y="22.750546875" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.471318603516" Y="22.771408203125" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.279253417969" Y="22.647556640625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.193023681641" Y="22.432248046875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.932673095703" Y="21.011443359375" />
                  <Point X="2.986674072266" Y="20.917912109375" />
                  <Point X="2.835298339844" Y="20.809787109375" />
                  <Point X="2.816947265625" Y="20.79791015625" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="1.784545410156" Y="21.875806640625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.649465942383" Y="22.033087890625" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.402746582031" Y="22.16216015625" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.147527709961" Y="22.116685546875" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.963133117676" Y="21.92951953125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.111902099609" Y="20.185529296875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="0.994363769531" Y="20.036755859375" />
                  <Point X="0.977389160156" Y="20.033671875" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#126" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.019116756578" Y="4.426469184036" Z="0.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="-0.905741999842" Y="4.992935613568" Z="0.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.25" />
                  <Point X="-1.674690981367" Y="4.790125262255" Z="0.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.25" />
                  <Point X="-1.746281347703" Y="4.736646270378" Z="0.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.25" />
                  <Point X="-1.736731914597" Y="4.350931711748" Z="0.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.25" />
                  <Point X="-1.829288251499" Y="4.303788378637" Z="0.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.25" />
                  <Point X="-1.924895927598" Y="4.344387924461" Z="0.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.25" />
                  <Point X="-1.954097725343" Y="4.375072419486" Z="0.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.25" />
                  <Point X="-2.72200787849" Y="4.283379987571" Z="0.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.25" />
                  <Point X="-3.320091864116" Y="3.83845708633" Z="0.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.25" />
                  <Point X="-3.34136016004" Y="3.728925181891" Z="0.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.25" />
                  <Point X="-2.994780435264" Y="3.063226542393" Z="0.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.25" />
                  <Point X="-3.048756466976" Y="3.000047100911" Z="0.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.25" />
                  <Point X="-3.131849923214" Y="3.000784263909" Z="0.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.25" />
                  <Point X="-3.204934166406" Y="3.038833788706" Z="0.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.25" />
                  <Point X="-4.166707661684" Y="2.899023067301" Z="0.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.25" />
                  <Point X="-4.514021692094" Y="2.321520262217" Z="0.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.25" />
                  <Point X="-4.4634597207" Y="2.199295151102" Z="0.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.25" />
                  <Point X="-3.669764454696" Y="1.55935595192" Z="0.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.25" />
                  <Point X="-3.689031651478" Y="1.500086582201" Z="0.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.25" />
                  <Point X="-3.746819475151" Y="1.476748476052" Z="0.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.25" />
                  <Point X="-3.858112894101" Y="1.488684596593" Z="0.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.25" />
                  <Point X="-4.957365313686" Y="1.095006824121" Z="0.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.25" />
                  <Point X="-5.051671466152" Y="0.505136655274" Z="0.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.25" />
                  <Point X="-4.913545175851" Y="0.407312938614" Z="0.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.25" />
                  <Point X="-3.551553318211" Y="0.031712456245" Z="0.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.25" />
                  <Point X="-3.540471910368" Y="0.002948658545" Z="0.25" />
                  <Point X="-3.539556741714" Y="0" Z="0.25" />
                  <Point X="-3.547892690767" Y="-0.026858292516" Z="0.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.25" />
                  <Point X="-3.573815276912" Y="-0.047163526889" Z="0.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.25" />
                  <Point X="-3.723342612921" Y="-0.088399118142" Z="0.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.25" />
                  <Point X="-4.990344782163" Y="-0.935951681418" Z="0.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.25" />
                  <Point X="-4.860333250768" Y="-1.468582225236" Z="0.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.25" />
                  <Point X="-4.685878382942" Y="-1.499960555699" Z="0.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.25" />
                  <Point X="-3.195296100457" Y="-1.320907906204" Z="0.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.25" />
                  <Point X="-3.199618633322" Y="-1.349254406087" Z="0.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.25" />
                  <Point X="-3.329232831526" Y="-1.451068805677" Z="0.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.25" />
                  <Point X="-4.238393946334" Y="-2.795193004879" Z="0.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.25" />
                  <Point X="-3.896577402472" Y="-3.254812298797" Z="0.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.25" />
                  <Point X="-3.734684794042" Y="-3.226282655422" Z="0.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.25" />
                  <Point X="-2.557207177176" Y="-2.571123291956" Z="0.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.25" />
                  <Point X="-2.629134357747" Y="-2.700393588451" Z="0.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.25" />
                  <Point X="-2.930980486536" Y="-4.146314673662" Z="0.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.25" />
                  <Point X="-2.494581307287" Y="-4.422630015827" Z="0.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.25" />
                  <Point X="-2.428869924288" Y="-4.420547642742" Z="0.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.25" />
                  <Point X="-1.993775362114" Y="-4.001135887661" Z="0.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.25" />
                  <Point X="-1.68929301696" Y="-4.002368587326" Z="0.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.25" />
                  <Point X="-1.448481356982" Y="-4.188703915187" Z="0.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.25" />
                  <Point X="-1.370866224661" Y="-4.483130111879" Z="0.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.25" />
                  <Point X="-1.369648759305" Y="-4.549465664335" Z="0.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.25" />
                  <Point X="-1.146653743667" Y="-4.948057311419" Z="0.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.25" />
                  <Point X="-0.847319301783" Y="-5.00800754434" Z="0.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="-0.778040495755" Y="-4.865870781312" Z="0.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="-0.269555874626" Y="-3.30620917091" Z="0.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="-0.025063597175" Y="-3.212018184068" Z="0.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.25" />
                  <Point X="0.228295482187" Y="-3.27509386122" Z="0.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.25" />
                  <Point X="0.400889984807" Y="-3.495436584858" Z="0.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.25" />
                  <Point X="0.456714364646" Y="-3.666665248503" Z="0.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.25" />
                  <Point X="0.980169782685" Y="-4.984242322291" Z="0.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.25" />
                  <Point X="1.159341128603" Y="-4.945637721276" Z="0.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.25" />
                  <Point X="1.155318397258" Y="-4.776664808499" Z="0.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.25" />
                  <Point X="1.005836432452" Y="-3.049818673994" Z="0.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.25" />
                  <Point X="1.173338824455" Y="-2.890479652521" Z="0.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.25" />
                  <Point X="1.401172307812" Y="-2.856348421006" Z="0.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.25" />
                  <Point X="1.616270618451" Y="-2.977690669625" Z="0.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.25" />
                  <Point X="1.738721832971" Y="-3.123350508847" Z="0.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.25" />
                  <Point X="2.837960718078" Y="-4.212784846692" Z="0.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.25" />
                  <Point X="3.027792095669" Y="-4.078486598001" Z="0.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.25" />
                  <Point X="2.969818300661" Y="-3.932276600952" Z="0.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.25" />
                  <Point X="2.236071746353" Y="-2.527584971089" Z="0.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.25" />
                  <Point X="2.317343834867" Y="-2.3444490492" Z="0.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.25" />
                  <Point X="2.488449329172" Y="-2.2415574751" Z="0.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.25" />
                  <Point X="2.700921832423" Y="-2.267376263138" Z="0.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.25" />
                  <Point X="2.855136996155" Y="-2.347931199613" Z="0.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.25" />
                  <Point X="4.222449192394" Y="-2.8229623523" Z="0.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.25" />
                  <Point X="4.383431332441" Y="-2.565876769061" Z="0.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.25" />
                  <Point X="4.279858684945" Y="-2.448766479465" Z="0.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.25" />
                  <Point X="3.102202541878" Y="-1.473763569655" Z="0.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.25" />
                  <Point X="3.106435467438" Y="-1.304281541985" Z="0.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.25" />
                  <Point X="3.206879074862" Y="-1.16844112681" Z="0.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.25" />
                  <Point X="3.381338572542" Y="-1.119824384595" Z="0.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.25" />
                  <Point X="3.548450062182" Y="-1.135556419612" Z="0.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.25" />
                  <Point X="4.983085971276" Y="-0.981024214964" Z="0.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.25" />
                  <Point X="5.042418253526" Y="-0.606284112777" Z="0.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.25" />
                  <Point X="4.919406185107" Y="-0.53470063786" Z="0.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.25" />
                  <Point X="3.664594283302" Y="-0.172627978059" Z="0.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.25" />
                  <Point X="3.605427664875" Y="-0.103607280602" Z="0.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.25" />
                  <Point X="3.583265040437" Y="-0.009557215996" Z="0.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.25" />
                  <Point X="3.598106409987" Y="0.087053315246" Z="0.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.25" />
                  <Point X="3.649951773525" Y="0.160341457973" Z="0.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.25" />
                  <Point X="3.738801131051" Y="0.215520866183" Z="0.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.25" />
                  <Point X="3.8765616322" Y="0.255271294909" Z="0.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.25" />
                  <Point X="4.988632276004" Y="0.950567594062" Z="0.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.25" />
                  <Point X="4.890809300289" Y="1.367488253132" Z="0.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.25" />
                  <Point X="4.740542767826" Y="1.390199846582" Z="0.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.25" />
                  <Point X="3.378275494314" Y="1.233237505042" Z="0.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.25" />
                  <Point X="3.306207907303" Y="1.269792920688" Z="0.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.25" />
                  <Point X="3.256015099138" Y="1.339490375954" Z="0.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.25" />
                  <Point X="3.235339939004" Y="1.423877963189" Z="0.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.25" />
                  <Point X="3.252986724602" Y="1.501699974397" Z="0.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.25" />
                  <Point X="3.307182046088" Y="1.577237981017" Z="0.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.25" />
                  <Point X="3.425120261183" Y="1.670806145453" Z="0.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.25" />
                  <Point X="4.258871994499" Y="2.76656022489" Z="0.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.25" />
                  <Point X="4.025599872912" Y="3.096191071064" Z="0.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.25" />
                  <Point X="3.854626853641" Y="3.043389882309" Z="0.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.25" />
                  <Point X="2.437533990362" Y="2.247653058555" Z="0.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.25" />
                  <Point X="2.3670346569" Y="2.253072390184" Z="0.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.25" />
                  <Point X="2.303120899549" Y="2.292608488689" Z="0.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.25" />
                  <Point X="2.258150111546" Y="2.353903960833" Z="0.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.25" />
                  <Point X="2.246357312575" Y="2.422723786932" Z="0.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.25" />
                  <Point X="2.264874884102" Y="2.50193558552" Z="0.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.25" />
                  <Point X="2.35223547055" Y="2.657512228314" Z="0.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.25" />
                  <Point X="2.790607524574" Y="4.242641643271" Z="0.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.25" />
                  <Point X="2.395108745624" Y="4.477830317138" Z="0.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.25" />
                  <Point X="1.984761857027" Y="4.674258098072" Z="0.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.25" />
                  <Point X="1.559007690187" Y="4.832957331747" Z="0.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.25" />
                  <Point X="0.927273442038" Y="4.990603827457" Z="0.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.25" />
                  <Point X="0.259456591772" Y="5.069388864444" Z="0.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.25" />
                  <Point X="0.174127804975" Y="5.004978321915" Z="0.25" />
                  <Point X="0" Y="4.355124473572" Z="0.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>