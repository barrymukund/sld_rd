<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#145" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1208" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.673898681641" Y="21.07472265625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362854004" Y="21.532625" />
                  <Point X="0.497159240723" Y="21.59775390625" />
                  <Point X="0.378635223389" Y="21.768525390625" />
                  <Point X="0.356748565674" Y="21.790982421875" />
                  <Point X="0.330493225098" Y="21.810224609375" />
                  <Point X="0.302494262695" Y="21.824330078125" />
                  <Point X="0.232544082642" Y="21.8460390625" />
                  <Point X="0.049135128021" Y="21.902962890625" />
                  <Point X="0.02098301506" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036825149536" Y="21.90296484375" />
                  <Point X="-0.106775177002" Y="21.88125390625" />
                  <Point X="-0.290184143066" Y="21.82433203125" />
                  <Point X="-0.318184143066" Y="21.810224609375" />
                  <Point X="-0.34443963623" Y="21.79098046875" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.411527374268" Y="21.703392578125" />
                  <Point X="-0.530051696777" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.840974609375" Y="20.40523828125" />
                  <Point X="-0.916584899902" Y="20.12305859375" />
                  <Point X="-1.079319091797" Y="20.154646484375" />
                  <Point X="-1.156755371094" Y="20.174568359375" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.228276367188" Y="20.335435546875" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.233219604492" Y="20.567787109375" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.32364453125" Y="20.868794921875" />
                  <Point X="-1.388045654297" Y="20.9252734375" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643028198242" Y="21.109033203125" />
                  <Point X="-1.728502685547" Y="21.114634765625" />
                  <Point X="-1.9526171875" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042656982422" Y="21.10519921875" />
                  <Point X="-2.11387890625" Y="21.057611328125" />
                  <Point X="-2.300623291016" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.480147949219" Y="20.711509765625" />
                  <Point X="-2.489932128906" Y="20.717568359375" />
                  <Point X="-2.801723632812" Y="20.91062109375" />
                  <Point X="-2.908903564453" Y="20.993146484375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.629292724609" Y="21.967388671875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575439453" Y="22.414806640625" />
                  <Point X="-2.414559570312" Y="22.444423828125" />
                  <Point X="-2.428776855469" Y="22.47325390625" />
                  <Point X="-2.446806640625" Y="22.4984140625" />
                  <Point X="-2.464155273438" Y="22.51576171875" />
                  <Point X="-2.489311279297" Y="22.533787109375" />
                  <Point X="-2.518140136719" Y="22.54800390625" />
                  <Point X="-2.547758300781" Y="22.55698828125" />
                  <Point X="-2.578692626953" Y="22.555974609375" />
                  <Point X="-2.61021875" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.572592041016" Y="21.999896484375" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-3.836539550781" Y="21.8825234375" />
                  <Point X="-4.082862304687" Y="22.206142578125" />
                  <Point X="-4.159708007812" Y="22.335" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.465567871094" Y="23.225544921875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083060302734" Y="23.524125" />
                  <Point X="-3.063723388672" Y="23.555193359375" />
                  <Point X="-3.054882568359" Y="23.578474609375" />
                  <Point X="-3.051728759766" Y="23.588380859375" />
                  <Point X="-3.046151367188" Y="23.609916015625" />
                  <Point X="-3.042037353516" Y="23.6413984375" />
                  <Point X="-3.042736816406" Y="23.664267578125" />
                  <Point X="-3.048884033203" Y="23.686306640625" />
                  <Point X="-3.060889160156" Y="23.715287109375" />
                  <Point X="-3.073729248047" Y="23.73733203125" />
                  <Point X="-3.09197265625" Y="23.755166015625" />
                  <Point X="-3.111786621094" Y="23.7698984375" />
                  <Point X="-3.120284179688" Y="23.77553515625" />
                  <Point X="-3.139455078125" Y="23.786818359375" />
                  <Point X="-3.168716064453" Y="23.798041015625" />
                  <Point X="-3.200603515625" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.410268554688" Y="23.650484375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.737739257812" Y="23.630185546875" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.854409179688" Y="24.149501953125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.942529785156" Y="24.66982421875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.512338378906" Y="24.787755859375" />
                  <Point X="-3.489108886719" Y="24.800294921875" />
                  <Point X="-3.480068847656" Y="24.80584765625" />
                  <Point X="-3.459978271484" Y="24.819791015625" />
                  <Point X="-3.436022460938" Y="24.84131640625" />
                  <Point X="-3.415195556641" Y="24.8714921875" />
                  <Point X="-3.405215576172" Y="24.894458984375" />
                  <Point X="-3.401614746094" Y="24.90416015625" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.389474365234" Y="24.954478515625" />
                  <Point X="-3.390396728516" Y="24.98808984375" />
                  <Point X="-3.395197753906" Y="25.011140625" />
                  <Point X="-3.397471191406" Y="25.0199296875" />
                  <Point X="-3.404168212891" Y="25.0415078125" />
                  <Point X="-3.417486816406" Y="25.0708359375" />
                  <Point X="-3.440219726562" Y="25.0999765625" />
                  <Point X="-3.459704589844" Y="25.116810546875" />
                  <Point X="-3.467641601562" Y="25.122966796875" />
                  <Point X="-3.487724121094" Y="25.13690625" />
                  <Point X="-3.501916259766" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.60698828125" Y="25.44565625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.886576171875" Y="25.557388671875" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.783559570312" Y="26.12801171875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.059316162109" Y="26.338451171875" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985351562" Y="26.299341796875" />
                  <Point X="-3.723422851562" Y="26.301228515625" />
                  <Point X="-3.703143554688" Y="26.30526171875" />
                  <Point X="-3.686184570312" Y="26.310607421875" />
                  <Point X="-3.641717529297" Y="26.32462890625" />
                  <Point X="-3.622775634766" Y="26.332962890625" />
                  <Point X="-3.604031982422" Y="26.34378515625" />
                  <Point X="-3.587352539062" Y="26.356015625" />
                  <Point X="-3.573715332031" Y="26.37156640625" />
                  <Point X="-3.561301269531" Y="26.389294921875" />
                  <Point X="-3.551352050781" Y="26.407427734375" />
                  <Point X="-3.544547119141" Y="26.42385546875" />
                  <Point X="-3.526704589844" Y="26.466931640625" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532049804688" Y="26.589376953125" />
                  <Point X="-3.540260742188" Y="26.605150390625" />
                  <Point X="-3.561789550781" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.218249023438" Y="27.1673515625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.322404785156" Y="27.320337890625" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-3.972738037109" Y="27.87301171875" />
                  <Point X="-3.750504638672" Y="28.15866015625" />
                  <Point X="-3.387008789062" Y="27.948796875" />
                  <Point X="-3.206657226562" Y="27.844669921875" />
                  <Point X="-3.187729003906" Y="27.836341796875" />
                  <Point X="-3.167086425781" Y="27.82983203125" />
                  <Point X="-3.146794433594" Y="27.825794921875" />
                  <Point X="-3.123175048828" Y="27.823728515625" />
                  <Point X="-3.061245117188" Y="27.818310546875" />
                  <Point X="-3.040560546875" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012939453" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.929312011719" Y="27.87699609375" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845502197266" Y="28.05973828125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.142813476562" Y="28.6544140625" />
                  <Point X="-3.183333496094" Y="28.724595703125" />
                  <Point X="-3.120804931641" Y="28.772537109375" />
                  <Point X="-2.700629150391" Y="29.094681640625" />
                  <Point X="-2.529877197266" Y="29.189546875" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.098406494141" Y="29.301693359375" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028892333984" Y="29.21480078125" />
                  <Point X="-2.012312988281" Y="29.200888671875" />
                  <Point X="-1.995115234375" Y="29.189396484375" />
                  <Point X="-1.968827026367" Y="29.1757109375" />
                  <Point X="-1.899899169922" Y="29.139828125" />
                  <Point X="-1.880625244141" Y="29.13233203125" />
                  <Point X="-1.859718383789" Y="29.126728515625" />
                  <Point X="-1.839268676758" Y="29.123580078125" />
                  <Point X="-1.818622436523" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.77745300293" Y="29.134482421875" />
                  <Point X="-1.750072021484" Y="29.14582421875" />
                  <Point X="-1.678279052734" Y="29.1755625" />
                  <Point X="-1.660145507812" Y="29.185509765625" />
                  <Point X="-1.642416137695" Y="29.197923828125" />
                  <Point X="-1.626863769531" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.586568237305" Y="29.2941875" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.493583496094" Y="29.657302734375" />
                  <Point X="-0.949623474121" Y="29.80980859375" />
                  <Point X="-0.742642028809" Y="29.83403515625" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.187256271362" Y="29.485431640625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082114013672" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425964355" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.286106811523" Y="29.808396484375" />
                  <Point X="0.307419616699" Y="29.8879375" />
                  <Point X="0.369089111328" Y="29.881478515625" />
                  <Point X="0.844044921875" Y="29.83173828125" />
                  <Point X="1.015297180176" Y="29.790392578125" />
                  <Point X="1.481024291992" Y="29.677951171875" />
                  <Point X="1.591424560547" Y="29.637908203125" />
                  <Point X="1.894645263672" Y="29.527927734375" />
                  <Point X="2.002435913086" Y="29.477517578125" />
                  <Point X="2.294558349609" Y="29.340900390625" />
                  <Point X="2.398736816406" Y="29.28020703125" />
                  <Point X="2.680971435547" Y="29.11577734375" />
                  <Point X="2.779188964844" Y="29.0459296875" />
                  <Point X="2.943259033203" Y="28.929251953125" />
                  <Point X="2.385468261719" Y="27.963130859375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.127149414062" Y="27.493888671875" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108413330078" Y="27.408796875" />
                  <Point X="2.109386962891" Y="27.370728515625" />
                  <Point X="2.1100390625" Y="27.36178515625" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442138672" Y="27.289603515625" />
                  <Point X="2.129708007812" Y="27.267515625" />
                  <Point X="2.140071533203" Y="27.247470703125" />
                  <Point X="2.151931884766" Y="27.2299921875" />
                  <Point X="2.183029296875" Y="27.184162109375" />
                  <Point X="2.194462646484" Y="27.170330078125" />
                  <Point X="2.221594726562" Y="27.14559375" />
                  <Point X="2.239073486328" Y="27.133732421875" />
                  <Point X="2.284903320312" Y="27.102634765625" />
                  <Point X="2.304953369141" Y="27.09226953125" />
                  <Point X="2.327041748047" Y="27.08400390625" />
                  <Point X="2.348967285156" Y="27.078662109375" />
                  <Point X="2.368134765625" Y="27.0763515625" />
                  <Point X="2.418392089844" Y="27.070291015625" />
                  <Point X="2.436468261719" Y="27.06984375" />
                  <Point X="2.473207763672" Y="27.074169921875" />
                  <Point X="2.495374023438" Y="27.08009765625" />
                  <Point X="2.553494140625" Y="27.095640625" />
                  <Point X="2.5652890625" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.668884277344" Y="27.73388671875" />
                  <Point X="3.967326171875" Y="27.90619140625" />
                  <Point X="4.123272460938" Y="27.6894609375" />
                  <Point X="4.178023925781" Y="27.598984375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.543984619141" Y="26.908779296875" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221420410156" Y="26.660236328125" />
                  <Point X="3.203973632812" Y="26.641625" />
                  <Point X="3.188020507812" Y="26.6208125" />
                  <Point X="3.14619140625" Y="26.566244140625" />
                  <Point X="3.136606933594" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.115687255859" Y="26.4958359375" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.102617431641" Y="26.348125" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.120679931641" Y="26.2689765625" />
                  <Point X="3.136283691406" Y="26.23573828125" />
                  <Point X="3.149552001953" Y="26.215572265625" />
                  <Point X="3.184341308594" Y="26.162693359375" />
                  <Point X="3.198893798828" Y="26.145451171875" />
                  <Point X="3.216137939453" Y="26.129361328125" />
                  <Point X="3.234344726562" Y="26.116037109375" />
                  <Point X="3.253572265625" Y="26.105212890625" />
                  <Point X="3.303986816406" Y="26.076833984375" />
                  <Point X="3.320521240234" Y="26.069501953125" />
                  <Point X="3.356119873047" Y="26.0594375" />
                  <Point X="3.382116699219" Y="26.056001953125" />
                  <Point X="3.450280517578" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.514463867188" Y="26.18209375" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.863189941406" Y="25.821990234375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.075514648438" Y="25.425767578125" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704787841797" Y="25.325583984375" />
                  <Point X="3.681549560547" Y="25.315068359375" />
                  <Point X="3.656008544922" Y="25.300306640625" />
                  <Point X="3.589039550781" Y="25.26159765625" />
                  <Point X="3.574308105469" Y="25.25109375" />
                  <Point X="3.547530517578" Y="25.225576171875" />
                  <Point X="3.532205810547" Y="25.206048828125" />
                  <Point X="3.492024658203" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.458572265625" Y="25.0659296875" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.941443359375" />
                  <Point X="3.450287109375" Y="24.914771484375" />
                  <Point X="3.463680908203" Y="24.844833984375" />
                  <Point X="3.47052734375" Y="24.823333984375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.507349365234" Y="24.763064453125" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559995605469" Y="24.698767578125" />
                  <Point X="3.58903515625" Y="24.67584375" />
                  <Point X="3.614576416016" Y="24.661080078125" />
                  <Point X="3.681545166016" Y="24.62237109375" />
                  <Point X="3.692708251953" Y="24.616861328125" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.657707519531" Y="24.355673828125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.832917480469" Y="23.95440625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.840645751953" Y="23.941755859375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.37466015625" Y="23.994490234375" />
                  <Point X="3.324531982422" Y="23.983595703125" />
                  <Point X="3.193095947266" Y="23.95502734375" />
                  <Point X="3.163973144531" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112396728516" Y="23.9060390625" />
                  <Point X="3.082097412109" Y="23.86959765625" />
                  <Point X="3.002652587891" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.965415039062" Y="23.64744140625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976451171875" Y="23.432" />
                  <Point X="3.004192871094" Y="23.3888515625" />
                  <Point X="3.076931396484" Y="23.2757109375" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.984000488281" Y="22.568927734375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.124801269531" Y="22.25019921875" />
                  <Point X="4.079094482422" Y="22.185255859375" />
                  <Point X="4.028981689453" Y="22.114052734375" />
                  <Point X="3.171693603516" Y="22.6090078125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754225585938" Y="22.840173828125" />
                  <Point X="2.694564941406" Y="22.85094921875" />
                  <Point X="2.538135253906" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444834228516" Y="22.864822265625" />
                  <Point X="2.395270996094" Y="22.83873828125" />
                  <Point X="2.265316162109" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.178447265625" Y="22.659998046875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.436740234375" />
                  <Point X="2.106450195312" Y="22.377080078125" />
                  <Point X="2.134701171875" Y="22.2206484375" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.713048583984" Y="21.20184375" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.781840576172" Y="20.888349609375" />
                  <Point X="2.730737548828" Y="20.855271484375" />
                  <Point X="2.701764160156" Y="20.836517578125" />
                  <Point X="2.041454223633" Y="21.69705078125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721924194336" Y="22.099443359375" />
                  <Point X="1.663082763672" Y="22.1372734375" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448366333008" Y="22.2565625" />
                  <Point X="1.417100097656" Y="22.258880859375" />
                  <Point X="1.352746826172" Y="22.252958984375" />
                  <Point X="1.184013061523" Y="22.23743359375" />
                  <Point X="1.156362792969" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104594604492" Y="22.204537109375" />
                  <Point X="1.054902832031" Y="22.16321875" />
                  <Point X="0.924610900879" Y="22.054884765625" />
                  <Point X="0.904141479492" Y="22.03113671875" />
                  <Point X="0.887249206543" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.860766784668" Y="21.905833984375" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.978790222168" Y="20.4687890625" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975717041016" Y="20.12992578125" />
                  <Point X="0.929315551758" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058423095703" Y="20.24736328125" />
                  <Point X="-1.133085693359" Y="20.266572265625" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.134089233398" Y="20.32303515625" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.140044921875" Y="20.5863203125" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575195312" Y="20.905140625" />
                  <Point X="-1.250209716797" Y="20.929064453125" />
                  <Point X="-1.261006469727" Y="20.94021875" />
                  <Point X="-1.325407592773" Y="20.996697265625" />
                  <Point X="-1.494267456055" Y="21.144783203125" />
                  <Point X="-1.506735717773" Y="21.15403125" />
                  <Point X="-1.53301940918" Y="21.170376953125" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576531494141" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621458251953" Y="21.201552734375" />
                  <Point X="-1.636815673828" Y="21.203830078125" />
                  <Point X="-1.722290161133" Y="21.209431640625" />
                  <Point X="-1.946404541016" Y="21.22412109375" />
                  <Point X="-1.961928710938" Y="21.2238671875" />
                  <Point X="-1.992725708008" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047729492" Y="21.209736328125" />
                  <Point X="-2.053667724609" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095435302734" Y="21.184189453125" />
                  <Point X="-2.166657226563" Y="21.1366015625" />
                  <Point X="-2.353401611328" Y="21.011822265625" />
                  <Point X="-2.359680419922" Y="21.007244140625" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503201171875" Y="20.837521484375" />
                  <Point X="-2.747609375" Y="20.9888515625" />
                  <Point X="-2.850946044922" Y="21.06841796875" />
                  <Point X="-2.98086328125" Y="21.16844921875" />
                  <Point X="-2.547020263672" Y="21.919888671875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310626220703" Y="22.411697265625" />
                  <Point X="-2.314666015625" Y="22.4423828125" />
                  <Point X="-2.323650146484" Y="22.472" />
                  <Point X="-2.329356445312" Y="22.48644140625" />
                  <Point X="-2.343573730469" Y="22.515271484375" />
                  <Point X="-2.351556884766" Y="22.52858984375" />
                  <Point X="-2.369586669922" Y="22.55375" />
                  <Point X="-2.379633300781" Y="22.565591796875" />
                  <Point X="-2.396981933594" Y="22.582939453125" />
                  <Point X="-2.408822265625" Y="22.592984375" />
                  <Point X="-2.433978271484" Y="22.611009765625" />
                  <Point X="-2.447293945312" Y="22.618990234375" />
                  <Point X="-2.476122802734" Y="22.63320703125" />
                  <Point X="-2.490563720703" Y="22.6389140625" />
                  <Point X="-2.520181884766" Y="22.6478984375" />
                  <Point X="-2.550869628906" Y="22.6519375" />
                  <Point X="-2.581803955078" Y="22.650923828125" />
                  <Point X="-2.597227783203" Y="22.6491484375" />
                  <Point X="-2.62875390625" Y="22.642876953125" />
                  <Point X="-2.643684570313" Y="22.63861328125" />
                  <Point X="-2.672649414062" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.620092041016" Y="22.08216796875" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-4.004020263672" Y="22.259412109375" />
                  <Point X="-4.078115478516" Y="22.383658203125" />
                  <Point X="-4.181265625" Y="22.556625" />
                  <Point X="-3.407735595703" Y="23.15017578125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039154785156" Y="23.433935546875" />
                  <Point X="-3.016260498047" Y="23.456576171875" />
                  <Point X="-3.00240625" Y="23.47392578125" />
                  <Point X="-2.983069335938" Y="23.504994140625" />
                  <Point X="-2.974911376953" Y="23.52146875" />
                  <Point X="-2.966070556641" Y="23.54475" />
                  <Point X="-2.959762939453" Y="23.5645625" />
                  <Point X="-2.954185546875" Y="23.58609765625" />
                  <Point X="-2.951952148438" Y="23.59760546875" />
                  <Point X="-2.947838134766" Y="23.629087890625" />
                  <Point X="-2.947081787109" Y="23.644302734375" />
                  <Point X="-2.94778125" Y="23.667171875" />
                  <Point X="-2.951229736328" Y="23.689791015625" />
                  <Point X="-2.957376953125" Y="23.711830078125" />
                  <Point X="-2.961116455078" Y="23.7226640625" />
                  <Point X="-2.973121582031" Y="23.75164453125" />
                  <Point X="-2.978798583984" Y="23.7631015625" />
                  <Point X="-2.991638671875" Y="23.785146484375" />
                  <Point X="-3.007320800781" Y="23.805265625" />
                  <Point X="-3.025564208984" Y="23.823099609375" />
                  <Point X="-3.035288574219" Y="23.83140234375" />
                  <Point X="-3.055102539062" Y="23.846134765625" />
                  <Point X="-3.07209765625" Y="23.857408203125" />
                  <Point X="-3.091268554688" Y="23.86869140625" />
                  <Point X="-3.105435546875" Y="23.875517578125" />
                  <Point X="-3.134696533203" Y="23.886740234375" />
                  <Point X="-3.149790527344" Y="23.89113671875" />
                  <Point X="-3.181677978516" Y="23.897619140625" />
                  <Point X="-3.197294433594" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.422668457031" Y="23.744671875" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.740762207031" Y="24.025880859375" />
                  <Point X="-4.760366210938" Y="24.162951171875" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.917941894531" Y="24.578060546875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497775634766" Y="24.6913125" />
                  <Point X="-3.477238037109" Y="24.699478515625" />
                  <Point X="-3.467212890625" Y="24.704158203125" />
                  <Point X="-3.443983398438" Y="24.716697265625" />
                  <Point X="-3.425903320312" Y="24.727802734375" />
                  <Point X="-3.405812744141" Y="24.74174609375" />
                  <Point X="-3.396483398438" Y="24.749126953125" />
                  <Point X="-3.372527587891" Y="24.77065234375" />
                  <Point X="-3.357836669922" Y="24.787353515625" />
                  <Point X="-3.337009765625" Y="24.817529296875" />
                  <Point X="-3.328066162109" Y="24.833630859375" />
                  <Point X="-3.318086181641" Y="24.85659765625" />
                  <Point X="-3.310884521484" Y="24.876" />
                  <Point X="-3.304187744141" Y="24.897576171875" />
                  <Point X="-3.301577392578" Y="24.90805859375" />
                  <Point X="-3.296133789062" Y="24.93680078125" />
                  <Point X="-3.294510009766" Y="24.957083984375" />
                  <Point X="-3.295432373047" Y="24.9906953125" />
                  <Point X="-3.297392578125" Y="25.0074609375" />
                  <Point X="-3.302193603516" Y="25.03051171875" />
                  <Point X="-3.306740478516" Y="25.04808984375" />
                  <Point X="-3.3134375" Y="25.06966796875" />
                  <Point X="-3.317669677734" Y="25.0807890625" />
                  <Point X="-3.33098828125" Y="25.1101171875" />
                  <Point X="-3.342583007813" Y="25.12926953125" />
                  <Point X="-3.365315917969" Y="25.15841015625" />
                  <Point X="-3.378112792969" Y="25.17186328125" />
                  <Point X="-3.39759765625" Y="25.188697265625" />
                  <Point X="-3.413471679688" Y="25.201009765625" />
                  <Point X="-3.433554199219" Y="25.21494921875" />
                  <Point X="-3.440481689453" Y="25.219326171875" />
                  <Point X="-3.465598388672" Y="25.23282421875" />
                  <Point X="-3.496558105469" Y="25.2456328125" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.582400390625" Y="25.537419921875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.73133203125" Y="25.957521484375" />
                  <Point X="-4.691866210938" Y="26.1031640625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.071716308594" Y="26.244263671875" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057373047" Y="26.204365234375" />
                  <Point X="-3.736704589844" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704891845703" Y="26.208052734375" />
                  <Point X="-3.684612548828" Y="26.2120859375" />
                  <Point X="-3.657624511719" Y="26.220001953125" />
                  <Point X="-3.613157470703" Y="26.2340234375" />
                  <Point X="-3.603458984375" Y="26.237673828125" />
                  <Point X="-3.584517089844" Y="26.2460078125" />
                  <Point X="-3.575273681641" Y="26.25069140625" />
                  <Point X="-3.556530029297" Y="26.261513671875" />
                  <Point X="-3.547855712891" Y="26.267173828125" />
                  <Point X="-3.531176269531" Y="26.279404296875" />
                  <Point X="-3.515926757812" Y="26.29337890625" />
                  <Point X="-3.502289550781" Y="26.3089296875" />
                  <Point X="-3.495896728516" Y="26.317076171875" />
                  <Point X="-3.483482666016" Y="26.3348046875" />
                  <Point X="-3.478014648438" Y="26.34359765625" />
                  <Point X="-3.468065429688" Y="26.36173046875" />
                  <Point X="-3.456779296875" Y="26.387498046875" />
                  <Point X="-3.438936767578" Y="26.43057421875" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012451172" Y="26.60453125" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.455994384766" Y="26.649015625" />
                  <Point X="-3.477523193359" Y="26.69037109375" />
                  <Point X="-3.482800048828" Y="26.69928515625" />
                  <Point X="-3.494292236328" Y="26.716484375" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.160416503906" Y="27.242720703125" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.002296142578" Y="27.6803046875" />
                  <Point X="-3.897756835938" Y="27.814677734375" />
                  <Point X="-3.726338623047" Y="28.035009765625" />
                  <Point X="-3.434508544922" Y="27.8665234375" />
                  <Point X="-3.254156982422" Y="27.762396484375" />
                  <Point X="-3.244916259766" Y="27.75771484375" />
                  <Point X="-3.225988037109" Y="27.74938671875" />
                  <Point X="-3.21630078125" Y="27.745740234375" />
                  <Point X="-3.195658203125" Y="27.73923046875" />
                  <Point X="-3.185623535156" Y="27.736658203125" />
                  <Point X="-3.165331542969" Y="27.73262109375" />
                  <Point X="-3.15507421875" Y="27.73115625" />
                  <Point X="-3.131454833984" Y="27.72908984375" />
                  <Point X="-3.069524902344" Y="27.723671875" />
                  <Point X="-3.059173583984" Y="27.723333984375" />
                  <Point X="-3.038489013672" Y="27.72378515625" />
                  <Point X="-3.028155761719" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938449951172" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902742431641" Y="27.7731953125" />
                  <Point X="-2.886610107422" Y="27.78614453125" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.862136474609" Y="27.809822265625" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.750863769531" Y="28.068017578125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059388183594" Y="28.69991796875" />
                  <Point X="-2.648381591797" Y="29.01503125" />
                  <Point X="-2.483739990234" Y="29.106501953125" />
                  <Point X="-2.192524414062" Y="29.268296875" />
                  <Point X="-2.173775390625" Y="29.243861328125" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111820556641" Y="29.164048828125" />
                  <Point X="-2.097517578125" Y="29.149107421875" />
                  <Point X="-2.089958251953" Y="29.14202734375" />
                  <Point X="-2.07337890625" Y="29.128115234375" />
                  <Point X="-2.065095458984" Y="29.121900390625" />
                  <Point X="-2.047897583008" Y="29.110408203125" />
                  <Point X="-2.038983276367" Y="29.105130859375" />
                  <Point X="-2.012695068359" Y="29.0914453125" />
                  <Point X="-1.943767333984" Y="29.0555625" />
                  <Point X="-1.934334228516" Y="29.0512890625" />
                  <Point X="-1.915060302734" Y="29.04379296875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.8843125" Y="29.034966796875" />
                  <Point X="-1.874174316406" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.833054077148" Y="29.028783203125" />
                  <Point X="-1.812407836914" Y="29.03013671875" />
                  <Point X="-1.802120605469" Y="29.031376953125" />
                  <Point X="-1.780804931641" Y="29.03513671875" />
                  <Point X="-1.770713623047" Y="29.0374921875" />
                  <Point X="-1.750859863281" Y="29.04328125" />
                  <Point X="-1.741097412109" Y="29.04671484375" />
                  <Point X="-1.713716430664" Y="29.058056640625" />
                  <Point X="-1.641923583984" Y="29.087794921875" />
                  <Point X="-1.632589111328" Y="29.092271484375" />
                  <Point X="-1.614455444336" Y="29.10221875" />
                  <Point X="-1.60565625" Y="29.107689453125" />
                  <Point X="-1.587926879883" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564226928711" Y="29.14013671875" />
                  <Point X="-1.550251464844" Y="29.155388671875" />
                  <Point X="-1.538020019531" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516854370117" Y="29.20873046875" />
                  <Point X="-1.508524169922" Y="29.2276640625" />
                  <Point X="-1.504877441406" Y="29.237353515625" />
                  <Point X="-1.495965087891" Y="29.265619140625" />
                  <Point X="-1.472597900391" Y="29.33973046875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.467937744141" Y="29.565830078125" />
                  <Point X="-0.931164489746" Y="29.7163203125" />
                  <Point X="-0.731598022461" Y="29.7396796875" />
                  <Point X="-0.36522253418" Y="29.78255859375" />
                  <Point X="-0.279019287109" Y="29.46084375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896308899" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.377869720459" Y="29.78380859375" />
                  <Point X="0.378190734863" Y="29.785005859375" />
                  <Point X="0.827866760254" Y="29.7379140625" />
                  <Point X="0.99300177002" Y="29.698044921875" />
                  <Point X="1.453606079102" Y="29.58683984375" />
                  <Point X="1.559032226562" Y="29.5486015625" />
                  <Point X="1.858246826172" Y="29.44007421875" />
                  <Point X="1.962191162109" Y="29.391462890625" />
                  <Point X="2.250435302734" Y="29.25666015625" />
                  <Point X="2.350914550781" Y="29.19812109375" />
                  <Point X="2.629424316406" Y="29.035861328125" />
                  <Point X="2.724132080078" Y="28.968509765625" />
                  <Point X="2.817778564453" Y="28.9019140625" />
                  <Point X="2.303195800781" Y="28.010630859375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.035374145508" Y="27.5184296875" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017266357422" Y="27.446939453125" />
                  <Point X="2.014072509766" Y="27.41996875" />
                  <Point X="2.013444335937" Y="27.4063671875" />
                  <Point X="2.01441796875" Y="27.368298828125" />
                  <Point X="2.015722290039" Y="27.350412109375" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.023800537109" Y="27.289033203125" />
                  <Point X="2.029143554687" Y="27.267109375" />
                  <Point X="2.032468261719" Y="27.256306640625" />
                  <Point X="2.040734130859" Y="27.23421875" />
                  <Point X="2.045319580078" Y="27.223884765625" />
                  <Point X="2.055683105469" Y="27.20383984375" />
                  <Point X="2.061461181641" Y="27.19412890625" />
                  <Point X="2.073321533203" Y="27.176650390625" />
                  <Point X="2.104418945313" Y="27.1308203125" />
                  <Point X="2.109805908203" Y="27.12363671875" />
                  <Point X="2.130458496094" Y="27.100126953125" />
                  <Point X="2.157590576172" Y="27.075390625" />
                  <Point X="2.168249755859" Y="27.066984375" />
                  <Point X="2.185728515625" Y="27.055123046875" />
                  <Point X="2.231558349609" Y="27.024025390625" />
                  <Point X="2.241276367188" Y="27.018244140625" />
                  <Point X="2.261326416016" Y="27.00787890625" />
                  <Point X="2.271658447266" Y="27.003294921875" />
                  <Point X="2.293746826172" Y="26.995029296875" />
                  <Point X="2.304554443359" Y="26.991703125" />
                  <Point X="2.326479980469" Y="26.986361328125" />
                  <Point X="2.356765380859" Y="26.98203515625" />
                  <Point X="2.407022705078" Y="26.975974609375" />
                  <Point X="2.416042236328" Y="26.9753203125" />
                  <Point X="2.447577880859" Y="26.97549609375" />
                  <Point X="2.484317382813" Y="26.979822265625" />
                  <Point X="2.497750488281" Y="26.98239453125" />
                  <Point X="2.519916748047" Y="26.988322265625" />
                  <Point X="2.578036865234" Y="27.003865234375" />
                  <Point X="2.584004638672" Y="27.005673828125" />
                  <Point X="2.604415039063" Y="27.013072265625" />
                  <Point X="2.627659912109" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.716384277344" Y="27.651615234375" />
                  <Point X="3.940403564453" Y="27.780953125" />
                  <Point X="4.043956542969" Y="27.637037109375" />
                  <Point X="4.096747070313" Y="27.54980078125" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.48615234375" Y="26.9841484375" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168130859375" Y="26.73986328125" />
                  <Point X="3.152111816406" Y="26.725208984375" />
                  <Point X="3.134665039062" Y="26.70659765625" />
                  <Point X="3.128575683594" Y="26.69941796875" />
                  <Point X="3.112622558594" Y="26.67860546875" />
                  <Point X="3.070793457031" Y="26.624037109375" />
                  <Point X="3.065636230469" Y="26.6166015625" />
                  <Point X="3.049740722656" Y="26.589373046875" />
                  <Point X="3.034763671875" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.024197509766" Y="26.521421875" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.3525703125" />
                  <Point X="3.009577148438" Y="26.328927734375" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034684570313" Y="26.22860546875" />
                  <Point X="3.050288330078" Y="26.1953671875" />
                  <Point X="3.056921142578" Y="26.183521484375" />
                  <Point X="3.070189453125" Y="26.16335546875" />
                  <Point X="3.104978759766" Y="26.1104765625" />
                  <Point X="3.111742675781" Y="26.101419921875" />
                  <Point X="3.126295166016" Y="26.084177734375" />
                  <Point X="3.134083740234" Y="26.0759921875" />
                  <Point X="3.151327880859" Y="26.05990234375" />
                  <Point X="3.160033447266" Y="26.052697265625" />
                  <Point X="3.178240234375" Y="26.039373046875" />
                  <Point X="3.187741455078" Y="26.03325390625" />
                  <Point X="3.206968994141" Y="26.0224296875" />
                  <Point X="3.257383544922" Y="25.99405078125" />
                  <Point X="3.2654765625" Y="25.989990234375" />
                  <Point X="3.29467578125" Y="25.9780859375" />
                  <Point X="3.330274414062" Y="25.968021484375" />
                  <Point X="3.343673583984" Y="25.965255859375" />
                  <Point X="3.369670410156" Y="25.9618203125" />
                  <Point X="3.437834228516" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.526863769531" Y="26.08790625" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.769320800781" Y="25.807375" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.050926757813" Y="25.51753125" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686021728516" Y="25.419541015625" />
                  <Point X="3.665622314453" Y="25.412134765625" />
                  <Point X="3.642384033203" Y="25.401619140625" />
                  <Point X="3.634011962891" Y="25.397318359375" />
                  <Point X="3.608470947266" Y="25.382556640625" />
                  <Point X="3.541501953125" Y="25.34384765625" />
                  <Point X="3.533886474609" Y="25.33894921875" />
                  <Point X="3.508770507812" Y="25.3198671875" />
                  <Point X="3.481992919922" Y="25.294349609375" />
                  <Point X="3.472796386719" Y="25.2842265625" />
                  <Point X="3.457471679688" Y="25.26469921875" />
                  <Point X="3.417290527344" Y="25.213498046875" />
                  <Point X="3.410854248047" Y="25.20420703125" />
                  <Point X="3.399130615234" Y="25.184927734375" />
                  <Point X="3.393843261719" Y="25.174939453125" />
                  <Point X="3.384069580078" Y="25.1534765625" />
                  <Point X="3.380005615234" Y="25.1429296875" />
                  <Point X="3.373158935547" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.365267822266" Y="25.083798828125" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.937056640625" />
                  <Point X="3.351874511719" Y="24.923572265625" />
                  <Point X="3.356982910156" Y="24.896900390625" />
                  <Point X="3.370376708984" Y="24.826962890625" />
                  <Point X="3.373159667969" Y="24.8160078125" />
                  <Point X="3.380006103516" Y="24.7945078125" />
                  <Point X="3.384069580078" Y="24.783962890625" />
                  <Point X="3.393843261719" Y="24.7625" />
                  <Point X="3.399130615234" Y="24.75251171875" />
                  <Point X="3.410854248047" Y="24.733232421875" />
                  <Point X="3.417290527344" Y="24.72394140625" />
                  <Point X="3.432615234375" Y="24.7044140625" />
                  <Point X="3.472796386719" Y="24.653212890625" />
                  <Point X="3.478718994141" Y="24.646365234375" />
                  <Point X="3.5011328125" Y="24.624201171875" />
                  <Point X="3.530172363281" Y="24.60127734375" />
                  <Point X="3.541493164062" Y="24.593595703125" />
                  <Point X="3.567034423828" Y="24.57883203125" />
                  <Point X="3.634003173828" Y="24.540123046875" />
                  <Point X="3.639498535156" Y="24.537181640625" />
                  <Point X="3.659150146484" Y="24.527986328125" />
                  <Point X="3.683021972656" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.633119628906" Y="24.26391015625" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.740298339844" Y="23.975541015625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.853045654297" Y="24.035943359375" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366721435547" Y="24.089158203125" />
                  <Point X="3.354484375" Y="24.087322265625" />
                  <Point X="3.304356201172" Y="24.076427734375" />
                  <Point X="3.172920166016" Y="24.047859375" />
                  <Point X="3.157876953125" Y="24.0432578125" />
                  <Point X="3.128754150391" Y="24.0316328125" />
                  <Point X="3.114674560547" Y="24.024609375" />
                  <Point X="3.086848144531" Y="24.007716796875" />
                  <Point X="3.074123535156" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.039348144531" Y="23.966775390625" />
                  <Point X="3.009048828125" Y="23.930333984375" />
                  <Point X="2.929604003906" Y="23.834787109375" />
                  <Point X="2.921325195312" Y="23.82315234375" />
                  <Point X="2.90660546875" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.870814697266" Y="23.656146484375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876785644531" Y="23.42333203125" />
                  <Point X="2.889158203125" Y="23.394517578125" />
                  <Point X="2.896541992188" Y="23.380623046875" />
                  <Point X="2.924283691406" Y="23.337474609375" />
                  <Point X="2.997022216797" Y="23.224333984375" />
                  <Point X="3.00174609375" Y="23.217640625" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.926168212891" Y="22.49355859375" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045477294922" Y="22.30255078125" />
                  <Point X="4.00140625" Y="22.239931640625" />
                  <Point X="4.001274658203" Y="22.239744140625" />
                  <Point X="3.219193603516" Y="22.691279296875" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021972656" Y="22.92048828125" />
                  <Point X="2.783118652344" Y="22.930673828125" />
                  <Point X="2.771110595703" Y="22.933662109375" />
                  <Point X="2.711449951172" Y="22.9444375" />
                  <Point X="2.555020263672" Y="22.9726875" />
                  <Point X="2.539360351562" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400590820312" Y="22.948890625" />
                  <Point X="2.351027587891" Y="22.922806640625" />
                  <Point X="2.221072753906" Y="22.854412109375" />
                  <Point X="2.208970214844" Y="22.846830078125" />
                  <Point X="2.186039550781" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940429687" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.094379394531" Y="22.7042421875" />
                  <Point X="2.025985229492" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435515625" />
                  <Point X="2.002187866211" Y="22.41985546875" />
                  <Point X="2.012962524414" Y="22.3601953125" />
                  <Point X="2.041213500977" Y="22.203763671875" />
                  <Point X="2.043015380859" Y="22.1957734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.630776123047" Y="21.15434375" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723752685547" Y="20.963916015625" />
                  <Point X="2.116822753906" Y="21.7548828125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808835327148" Y="22.150369140625" />
                  <Point X="1.783253417969" Y="22.171994140625" />
                  <Point X="1.773299560547" Y="22.179353515625" />
                  <Point X="1.714458007812" Y="22.21718359375" />
                  <Point X="1.560176025391" Y="22.31637109375" />
                  <Point X="1.546279663086" Y="22.323755859375" />
                  <Point X="1.517465332031" Y="22.336126953125" />
                  <Point X="1.502547485352" Y="22.34111328125" />
                  <Point X="1.470927368164" Y="22.34884375" />
                  <Point X="1.455391235352" Y="22.351302734375" />
                  <Point X="1.424125" Y="22.35362109375" />
                  <Point X="1.408394897461" Y="22.35348046875" />
                  <Point X="1.344041625977" Y="22.34755859375" />
                  <Point X="1.175307861328" Y="22.332033203125" />
                  <Point X="1.161225097656" Y="22.32966015625" />
                  <Point X="1.133574829102" Y="22.322828125" />
                  <Point X="1.120007324219" Y="22.318369140625" />
                  <Point X="1.092621582031" Y="22.307025390625" />
                  <Point X="1.079876342773" Y="22.3005859375" />
                  <Point X="1.055493896484" Y="22.285865234375" />
                  <Point X="1.043856323242" Y="22.277583984375" />
                  <Point X="0.994164611816" Y="22.236265625" />
                  <Point X="0.863872680664" Y="22.127931640625" />
                  <Point X="0.852652404785" Y="22.116908203125" />
                  <Point X="0.832183044434" Y="22.09316015625" />
                  <Point X="0.822933776855" Y="22.080435546875" />
                  <Point X="0.806041625977" Y="22.052609375" />
                  <Point X="0.799019287109" Y="22.03853125" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.767934387207" Y="21.92601171875" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091064453" Y="20.84766015625" />
                  <Point X="0.765661621094" Y="21.099310546875" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146484375" Y="21.546416015625" />
                  <Point X="0.626787841797" Y="21.57618359375" />
                  <Point X="0.620406982422" Y="21.58679296875" />
                  <Point X="0.575203308105" Y="21.651921875" />
                  <Point X="0.456679229736" Y="21.822693359375" />
                  <Point X="0.446668762207" Y="21.834830078125" />
                  <Point X="0.424782104492" Y="21.857287109375" />
                  <Point X="0.412905792236" Y="21.867607421875" />
                  <Point X="0.386650512695" Y="21.886849609375" />
                  <Point X="0.373235198975" Y="21.89506640625" />
                  <Point X="0.345236297607" Y="21.909171875" />
                  <Point X="0.330652526855" Y="21.915060546875" />
                  <Point X="0.260702423096" Y="21.93676953125" />
                  <Point X="0.077293426514" Y="21.993693359375" />
                  <Point X="0.063379947662" Y="21.996888671875" />
                  <Point X="0.035227794647" Y="22.001158203125" />
                  <Point X="0.020989276886" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022895498276" Y="22.001162109375" />
                  <Point X="-0.051062511444" Y="21.996892578125" />
                  <Point X="-0.064985801697" Y="21.9936953125" />
                  <Point X="-0.134935760498" Y="21.971984375" />
                  <Point X="-0.31834475708" Y="21.9150625" />
                  <Point X="-0.33292956543" Y="21.909171875" />
                  <Point X="-0.360929656982" Y="21.895064453125" />
                  <Point X="-0.374344970703" Y="21.88684765625" />
                  <Point X="-0.400600402832" Y="21.867603515625" />
                  <Point X="-0.412477142334" Y="21.85728125" />
                  <Point X="-0.434361114502" Y="21.83482421875" />
                  <Point X="-0.444368499756" Y="21.822689453125" />
                  <Point X="-0.489572143555" Y="21.75755859375" />
                  <Point X="-0.608096557617" Y="21.5867890625" />
                  <Point X="-0.612470092773" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.932737548828" Y="20.429826171875" />
                  <Point X="-0.985425170898" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083981199206" Y="27.442876554507" />
                  <Point X="3.857322671873" Y="27.732986239933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008609632767" Y="27.385042181956" />
                  <Point X="3.774241779292" Y="27.685019354866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.933238066327" Y="27.327207809404" />
                  <Point X="3.691160864732" Y="27.637052497931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768979230492" Y="28.817391164039" />
                  <Point X="2.572281571957" Y="29.069152686152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857866499888" Y="27.269373436852" />
                  <Point X="3.608079899754" Y="27.589085705528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717748820687" Y="28.728657520062" />
                  <Point X="2.351004722468" Y="29.198068559733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.733049788266" Y="25.994884331936" />
                  <Point X="4.647920884154" Y="26.103844360413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.782494933449" Y="27.211539064301" />
                  <Point X="3.524998934776" Y="27.541118913125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666518410881" Y="28.639923876085" />
                  <Point X="2.146807354372" Y="29.305123694023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771891531518" Y="25.790863589361" />
                  <Point X="4.538608300509" Y="26.089452508822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70712336701" Y="27.153704691749" />
                  <Point X="3.441917969798" Y="27.493152120723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.615288001076" Y="28.551190232108" />
                  <Point X="1.956839740429" Y="29.393965573555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.723983783793" Y="25.697877131857" />
                  <Point X="4.42929532845" Y="26.075061154378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.63175180057" Y="27.095870319197" />
                  <Point X="3.35883700482" Y="27.44518532832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.564057591271" Y="28.46245658813" />
                  <Point X="1.777328513523" Y="29.469423887997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624296223098" Y="25.671165812683" />
                  <Point X="4.319982309637" Y="26.060669859777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.556380234131" Y="27.038035946646" />
                  <Point X="3.275756039842" Y="27.397218535917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.512827181466" Y="28.373722944153" />
                  <Point X="1.609099320051" Y="29.530441858152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524608662402" Y="25.644454493508" />
                  <Point X="4.210669290823" Y="26.046278565176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.481008681326" Y="26.980201556643" />
                  <Point X="3.192675074864" Y="27.349251743514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461596771661" Y="28.284989300176" />
                  <Point X="1.442357938362" Y="29.589555516063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424921101707" Y="25.617743174333" />
                  <Point X="4.10135627201" Y="26.031887270575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40563731467" Y="26.92236692838" />
                  <Point X="3.109594109887" Y="27.301284951111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.410366361856" Y="28.196255656199" />
                  <Point X="1.293774120296" Y="29.625428552356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.325233541011" Y="25.591031855158" />
                  <Point X="3.992043253197" Y="26.017495975974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330265948014" Y="26.864532300118" />
                  <Point X="3.026513144909" Y="27.253318158708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359135952051" Y="28.107522012222" />
                  <Point X="1.14519030223" Y="29.661301588648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.225545980316" Y="25.564320535983" />
                  <Point X="3.882730234383" Y="26.003104681373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.254894581359" Y="26.806697671855" />
                  <Point X="2.943432179931" Y="27.205351366305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307905542246" Y="28.018788368245" />
                  <Point X="0.996606484163" Y="29.697174624941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12585841962" Y="25.537609216808" />
                  <Point X="3.77341721557" Y="25.988713386772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.179523214703" Y="26.748863043592" />
                  <Point X="2.860351214953" Y="27.157384573902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.256675171873" Y="27.930054673797" />
                  <Point X="0.848022581102" Y="29.733047770023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.026170901306" Y="25.510897843387" />
                  <Point X="3.664104196757" Y="25.974322092171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.113234245184" Y="26.679403477113" />
                  <Point X="2.777270249975" Y="27.109417781499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205444805492" Y="27.84132097424" />
                  <Point X="0.71437835272" Y="29.74979900351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.926483511275" Y="25.484186305772" />
                  <Point X="3.554791177943" Y="25.95993079757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055418424269" Y="26.599098774981" />
                  <Point X="2.694189284997" Y="27.061450989096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.154214439111" Y="27.752587274683" />
                  <Point X="0.583078794124" Y="29.763549196524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.826796121244" Y="25.457474768157" />
                  <Point X="3.439960190165" Y="25.952602181173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.016646016417" Y="26.49441961565" />
                  <Point X="2.609599465296" Y="27.015415442671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.10298407273" Y="27.663853575125" />
                  <Point X="0.451779235528" Y="29.777299389539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.766379255719" Y="24.100557619114" />
                  <Point X="4.640246124415" Y="24.262000665069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.727108731212" Y="25.430763230542" />
                  <Point X="3.300859191198" Y="25.97633776251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.004194395281" Y="26.35605138561" />
                  <Point X="2.51188776148" Y="26.986175142016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053144373983" Y="27.573339902167" />
                  <Point X="0.364658424839" Y="29.73450336385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741021236697" Y="23.97870882505" />
                  <Point X="4.487769232203" Y="24.302856609038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.633095726891" Y="25.39678881042" />
                  <Point X="2.398497339095" Y="26.977002685998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02010359775" Y="27.461324588907" />
                  <Point X="0.333871153664" Y="29.619603695647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.658605104359" Y="23.929891085673" />
                  <Point X="4.335292296343" Y="24.343712608874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550043741703" Y="25.348784925577" />
                  <Point X="2.248729619152" Y="27.014391047591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021109786606" Y="27.30573114758" />
                  <Point X="0.303083882489" Y="29.504704027444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524226662449" Y="23.947582069622" />
                  <Point X="4.182815360483" Y="24.384568608709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.476629266918" Y="25.288445589939" />
                  <Point X="0.272296611315" Y="29.389804359242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389848220539" Y="23.965273053571" />
                  <Point X="4.030338424623" Y="24.425424608544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.416039219396" Y="25.211691735938" />
                  <Point X="0.24150934014" Y="29.274904691039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.255469778629" Y="23.98296403752" />
                  <Point X="3.877861488764" Y="24.46628060838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371403575123" Y="25.114517177002" />
                  <Point X="0.19531407742" Y="29.179726352684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.121091336719" Y="24.000655021469" />
                  <Point X="3.725384552904" Y="24.507136608215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349520626755" Y="24.988220495332" />
                  <Point X="0.123892073035" Y="29.11683677123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.349039044315" Y="29.722160997485" />
                  <Point X="-0.393629353028" Y="29.77923399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.986712894809" Y="24.018346005418" />
                  <Point X="3.532523370925" Y="24.599682085943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.379797826754" Y="24.795161868227" />
                  <Point X="0.027378905769" Y="29.086062413748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.2861113332" Y="29.48731162189" />
                  <Point X="-0.504086127499" Y="29.766306635882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.852334450771" Y="24.03603699209" />
                  <Point X="-0.121266466399" Y="29.122014235697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.221648066679" Y="29.250496825002" />
                  <Point X="-0.61454290197" Y="29.753379281764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717955606928" Y="24.05372849049" />
                  <Point X="-0.72499967644" Y="29.740451927646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583576763086" Y="24.071419988889" />
                  <Point X="-0.835455321678" Y="29.727523128175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449197919243" Y="24.089111487289" />
                  <Point X="-0.944367450958" Y="29.712618718371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.333587381745" Y="24.082780649032" />
                  <Point X="-1.043262104288" Y="29.684892524049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.230530873975" Y="24.060381385474" />
                  <Point X="-1.142156757619" Y="29.657166329727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.131559952798" Y="24.032752809544" />
                  <Point X="-1.241051410949" Y="29.629440135405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052410572165" Y="23.979753818658" />
                  <Point X="-1.33994606428" Y="29.601713941083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.989156469807" Y="23.906409799352" />
                  <Point X="-1.43884071761" Y="29.573987746761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079128798696" Y="22.357003259347" />
                  <Point X="4.041706197104" Y="22.40490200511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927198616494" Y="23.831406656927" />
                  <Point X="-1.464821337339" Y="29.452935845261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.024453548054" Y="22.272678810574" />
                  <Point X="3.740687123651" Y="22.635883270986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8818958168" Y="23.735086017991" />
                  <Point X="-1.47959472771" Y="29.317539344325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.870753046364" Y="22.315100903256" />
                  <Point X="3.439670304268" Y="22.866861651784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.865777856408" Y="23.601410488201" />
                  <Point X="-1.51623698444" Y="29.21013371589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.651129361136" Y="22.441900823074" />
                  <Point X="3.138653484884" Y="23.097840032583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.871387989669" Y="23.439924266758" />
                  <Point X="-1.574836751575" Y="29.130832419162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431505675908" Y="22.568700742892" />
                  <Point X="-1.656916722138" Y="29.081584412335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.211881979651" Y="22.695500676826" />
                  <Point X="-1.748261692111" Y="29.044195063973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.992257963139" Y="22.822301020668" />
                  <Point X="-1.857996169071" Y="29.0303432112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.788331347203" Y="22.929009607996" />
                  <Point X="-2.035612336551" Y="29.103375960188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64656632416" Y="22.956154984656" />
                  <Point X="-2.257022639838" Y="29.23246264684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.511638697275" Y="22.974548893319" />
                  <Point X="-2.341088760082" Y="29.185756795676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408503223729" Y="22.952250701346" />
                  <Point X="-2.425154880326" Y="29.139050944513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.322628169979" Y="22.907860179487" />
                  <Point X="-2.509221139996" Y="29.092345271806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.237198826341" Y="22.862899174698" />
                  <Point X="-2.59328772023" Y="29.045640009404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.160871759043" Y="22.806287787476" />
                  <Point X="-2.674365732924" Y="28.995109554985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.104708438886" Y="22.723867980826" />
                  <Point X="-2.749760750694" Y="28.937305198768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.05618505558" Y="22.631669500933" />
                  <Point X="-2.825155768465" Y="28.879500842552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.011503886706" Y="22.53455321083" />
                  <Point X="-2.900550786235" Y="28.821696486335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008690794762" Y="22.383848226003" />
                  <Point X="-2.975945804006" Y="28.763892130119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047990017934" Y="22.179241935831" />
                  <Point X="-3.051340821776" Y="28.706087773902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.355043376419" Y="21.631925980681" />
                  <Point X="-2.772036060708" Y="28.194288403821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69634745778" Y="21.040771099388" />
                  <Point X="-2.749117626003" Y="28.010648566776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.598364187741" Y="22.291820019842" />
                  <Point X="-2.783316480249" Y="27.900115525778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.429855121254" Y="22.353196211119" />
                  <Point X="-2.84596363814" Y="27.825994652981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.315737667007" Y="22.34495431345" />
                  <Point X="-2.917433186658" Y="27.763165925241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.203266174989" Y="22.334605680197" />
                  <Point X="-3.009734311452" Y="27.727000399243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.101412551515" Y="22.310666794951" />
                  <Point X="-3.131957879463" Y="27.729133854055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.02133413289" Y="22.258856918468" />
                  <Point X="-3.298508108384" Y="27.78800284758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.948252754303" Y="22.198091239138" />
                  <Point X="-3.51813185946" Y="27.914802851681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.875171082072" Y="22.137325935657" />
                  <Point X="-3.729464970971" Y="28.030991321044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812495740832" Y="22.063241135901" />
                  <Point X="-3.789616302601" Y="27.953675936308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.774729496565" Y="21.957274145909" />
                  <Point X="-3.849767634231" Y="27.876360551572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748490134797" Y="21.836553419117" />
                  <Point X="-3.909918799244" Y="27.799044953577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724933698186" Y="21.71239870472" />
                  <Point X="-3.970069306823" Y="27.721728514104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.742525434547" Y="21.535576730648" />
                  <Point X="-4.026228385851" Y="27.639303279056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766956963084" Y="21.350000221816" />
                  <Point X="0.662751740951" Y="21.483376823915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.323828922317" Y="21.917178249585" />
                  <Point X="-4.077780939163" Y="27.550981959965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.79138849162" Y="21.164423712983" />
                  <Point X="0.725679092893" Y="21.24852790804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.164682498389" Y="21.966570804865" />
                  <Point X="-3.424593530553" Y="26.56063462374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.6537483702" Y="26.853939443223" />
                  <Point X="-4.129333492474" Y="27.462660640873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.815820020157" Y="20.97884720415" />
                  <Point X="0.788606530781" Y="21.013678882159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.016263615889" Y="22.002232733259" />
                  <Point X="-3.440530263938" Y="26.42672713396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.954766395347" Y="27.084919367328" />
                  <Point X="-4.180886045786" Y="27.374339321782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.09125325203" Y="21.985542470351" />
                  <Point X="-3.486217692575" Y="26.330898797619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.188282052684" Y="21.955428093508" />
                  <Point X="-3.553903722519" Y="26.263227386942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.285311492327" Y="21.925314534536" />
                  <Point X="-3.644036540056" Y="26.224286554214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.375279465379" Y="21.886162710488" />
                  <Point X="-3.749063218104" Y="26.204408993617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.44525148004" Y="21.82141722682" />
                  <Point X="-3.881158146202" Y="26.21917721317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.501965503121" Y="21.739702287769" />
                  <Point X="-4.015536212645" Y="26.236867716544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.558680109611" Y="21.657988095449" />
                  <Point X="-4.149914771507" Y="26.254558850185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.614806740036" Y="21.575521328084" />
                  <Point X="-3.297850845355" Y="25.009661179491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47536027356" Y="25.236862886758" />
                  <Point X="-4.28429368414" Y="26.272250436631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.653742450444" Y="21.471051186493" />
                  <Point X="-3.311881492802" Y="24.873314010965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.631717995592" Y="25.282686066381" />
                  <Point X="-4.418672596773" Y="26.289942023078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.684529698273" Y="21.356151488409" />
                  <Point X="-3.361777924775" Y="24.782872953223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.784194887766" Y="25.323542010301" />
                  <Point X="-4.553051509405" Y="26.307633609524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.715316946103" Y="21.241251790326" />
                  <Point X="-3.434961910581" Y="24.722238605146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93667177994" Y="25.364397954221" />
                  <Point X="-4.646025736597" Y="26.272329615307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.746104193932" Y="21.126352092242" />
                  <Point X="-3.525110947207" Y="24.683318531905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.089148672114" Y="25.405253898141" />
                  <Point X="-4.677071565902" Y="26.15776088642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.776891441761" Y="21.011452394159" />
                  <Point X="-3.62479870697" Y="24.656607467524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.241625564288" Y="25.446109842061" />
                  <Point X="-4.708117273114" Y="26.04319200126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807678689591" Y="20.896552696075" />
                  <Point X="-2.947522938796" Y="23.635428437043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.14657162462" Y="23.890199136862" />
                  <Point X="-3.724486466732" Y="24.629896403143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.394102456462" Y="25.486965785981" />
                  <Point X="-4.736174318457" Y="25.92479780335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.83846593742" Y="20.781652997992" />
                  <Point X="-2.976514789302" Y="23.518230735178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.271805335522" Y="23.896185398879" />
                  <Point X="-3.824174226495" Y="24.603185338763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.546579348636" Y="25.527821729901" />
                  <Point X="-4.755371713941" Y="25.795063770739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.869253185249" Y="20.666753299908" />
                  <Point X="-3.034674499941" Y="23.43836619182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.381118360208" Y="23.881794111795" />
                  <Point X="-3.92386196958" Y="24.576474253036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699056553661" Y="25.568678074252" />
                  <Point X="-4.774569109426" Y="25.665329738129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900040433078" Y="20.551853601825" />
                  <Point X="-2.314119303553" Y="22.36179201935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539652304855" Y="22.650461097149" />
                  <Point X="-3.109102324704" Y="23.379323885008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490431384895" Y="23.867402824711" />
                  <Point X="-4.023549448514" Y="24.549762829211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.930827680908" Y="20.436953903741" />
                  <Point X="-1.171645494662" Y="20.745186649339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.461161147635" Y="21.115749786751" />
                  <Point X="-2.35256550214" Y="22.2566953312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.6493008871" Y="22.636499304156" />
                  <Point X="-3.184473917844" Y="23.321489546632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599744409581" Y="23.853011537627" />
                  <Point X="-4.123236927448" Y="24.523051405386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.961615173363" Y="20.322054518765" />
                  <Point X="-1.130468777997" Y="20.538177277082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.651274033091" Y="21.204777605342" />
                  <Point X="-2.403795803352" Y="22.16796154823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.735696256501" Y="22.59277475596" />
                  <Point X="-3.259845510984" Y="23.263655208256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.709057434267" Y="23.838620250543" />
                  <Point X="-4.222924406382" Y="24.496339981562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017631115381" Y="20.239446076699" />
                  <Point X="-1.126712616689" Y="20.379064031526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.778336992931" Y="21.213105199229" />
                  <Point X="-2.455026104563" Y="22.079227765259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.818777175449" Y="22.544807904641" />
                  <Point X="-3.335217104125" Y="23.20582086988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.818370458953" Y="23.824228963459" />
                  <Point X="-4.322611885316" Y="24.469628557737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.905400510715" Y="21.221433507254" />
                  <Point X="-2.506256405775" Y="21.990493982289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901858094397" Y="22.496841053323" />
                  <Point X="-3.410588690798" Y="23.147986523227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92768348364" Y="23.809837676375" />
                  <Point X="-4.42229936425" Y="24.442917133912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.020665981036" Y="21.214661003151" />
                  <Point X="-2.55748671678" Y="21.901760211854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.984939013345" Y="22.448874202004" />
                  <Point X="-3.485960113106" Y="23.090151966196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036996508326" Y="23.795446389291" />
                  <Point X="-4.521986843184" Y="24.416205710087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.109876854184" Y="21.174540135416" />
                  <Point X="-2.608717065927" Y="21.813026490238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.068019932293" Y="22.400907350686" />
                  <Point X="-3.561331535414" Y="23.032317409164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146309533012" Y="23.781055102207" />
                  <Point X="-4.621674322118" Y="24.389494286263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.189084671584" Y="21.12161594018" />
                  <Point X="-2.659947415074" Y="21.724292768623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.151100851241" Y="22.352940499368" />
                  <Point X="-3.636702957722" Y="22.974482852133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255622557698" Y="23.766663815123" />
                  <Point X="-4.721361801053" Y="24.362782862438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.268291940652" Y="21.068691043112" />
                  <Point X="-2.711177764222" Y="21.635559047007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234181770189" Y="22.304973648049" />
                  <Point X="-3.71207438003" Y="22.916648295102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.364935582384" Y="23.752272528039" />
                  <Point X="-4.777076635705" Y="24.27978902052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.34749920972" Y="21.015766146044" />
                  <Point X="-2.762408113369" Y="21.546825325392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317262689137" Y="22.257006796731" />
                  <Point X="-3.787445802338" Y="22.85881373807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.474248790366" Y="23.737881475562" />
                  <Point X="-4.750062296263" Y="24.090906664481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.416784426116" Y="20.950141600684" />
                  <Point X="-2.813638462516" Y="21.458091603776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.400343608085" Y="22.209039945412" />
                  <Point X="-3.862817224646" Y="22.800979181039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.583562203508" Y="23.723490685679" />
                  <Point X="-4.702362484189" Y="23.875548110838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4765195653" Y="20.872293513909" />
                  <Point X="-2.864868811663" Y="21.369357882161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483424527033" Y="22.161073094094" />
                  <Point X="-3.938188646954" Y="22.743144624008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.632417631512" Y="20.917528360911" />
                  <Point X="-2.916099160811" Y="21.280624160545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.566505445981" Y="22.113106242775" />
                  <Point X="-4.013560069262" Y="22.685310066976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.900931451516" Y="21.106904799633" />
                  <Point X="-2.967329509958" Y="21.19189043893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.649586377592" Y="22.065139407665" />
                  <Point X="-4.08893149157" Y="22.627475509945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73266733221" Y="22.017172602002" />
                  <Point X="-4.164302913878" Y="22.569640952914" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.582135803223" Y="21.050134765625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.41911517334" Y="21.5435859375" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.733599609375" />
                  <Point X="0.20438583374" Y="21.75530859375" />
                  <Point X="0.02097677803" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.078614524841" Y="21.7905234375" />
                  <Point X="-0.2620234375" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.333482635498" Y="21.6492265625" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.749211608887" Y="20.380650390625" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.88473828125" Y="20.020103515625" />
                  <Point X="-1.100230224609" Y="20.061931640625" />
                  <Point X="-1.180426147461" Y="20.082564453125" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.322463623047" Y="20.3478359375" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.326394287109" Y="20.54925390625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.45068371582" Y="20.853849609375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.734715209961" Y="21.019837890625" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.061100585938" Y="20.97862109375" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.422538085938" Y="20.63053515625" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.539942871094" Y="20.636796875" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-2.966861083984" Y="20.917875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.711565185547" Y="22.014888671875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979980469" Y="22.431236328125" />
                  <Point X="-2.531328613281" Y="22.448583984375" />
                  <Point X="-2.560157470703" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.525092041016" Y="21.917625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.912132568359" Y="21.824984375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.24130078125" Y="22.286341796875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.523400146484" Y="23.3009140625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535400391" Y="23.58891796875" />
                  <Point X="-3.143694580078" Y="23.61219921875" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.136651611328" Y="23.64994921875" />
                  <Point X="-3.148656738281" Y="23.6789296875" />
                  <Point X="-3.168470703125" Y="23.693662109375" />
                  <Point X="-3.187641601562" Y="23.7049453125" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.397868652344" Y="23.556296875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.829784179688" Y="23.606673828125" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.948452148438" Y="24.13605078125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.967117675781" Y="24.761587890625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.534234375" Y="24.883892578125" />
                  <Point X="-3.514143798828" Y="24.8978359375" />
                  <Point X="-3.502324951172" Y="24.909353515625" />
                  <Point X="-3.492344970703" Y="24.9323203125" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.488201904297" Y="24.99176953125" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.502326660156" Y="25.02808984375" />
                  <Point X="-3.521811523438" Y="25.044923828125" />
                  <Point X="-3.541894042969" Y="25.05886328125" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.631576171875" Y="25.353892578125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.980552734375" Y="25.571294921875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.875252441406" Y="26.152859375" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.046916015625" Y="26.432638671875" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731703613281" Y="26.3958671875" />
                  <Point X="-3.714744628906" Y="26.401212890625" />
                  <Point X="-3.670277587891" Y="26.415234375" />
                  <Point X="-3.651533935547" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.632314941406" Y="26.460212890625" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.624527099609" Y="26.56128515625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.276081542969" Y="27.091982421875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.404451171875" Y="27.3682265625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.047718994141" Y="27.931345703125" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.339508789062" Y="28.0310703125" />
                  <Point X="-3.159157226562" Y="27.926943359375" />
                  <Point X="-3.138514648438" Y="27.92043359375" />
                  <Point X="-3.114895263672" Y="27.9183671875" />
                  <Point X="-3.052965332031" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.996487548828" Y="27.944169921875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.940140625" Y="28.051458984375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.2250859375" Y="28.6069140625" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.178607421875" Y="28.847927734375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.576014404297" Y="29.272591796875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.023037719727" Y="29.359525390625" />
                  <Point X="-1.967826538086" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.924958984375" Y="29.2599765625" />
                  <Point X="-1.85603112793" Y="29.22409375" />
                  <Point X="-1.835124267578" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.786427612305" Y="29.233591796875" />
                  <Point X="-1.714634643555" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.677171386719" Y="29.32275390625" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.682956787109" Y="29.65419140625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.519229492188" Y="29.748775390625" />
                  <Point X="-0.968082885742" Y="29.903296875" />
                  <Point X="-0.753685791016" Y="29.928390625" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.095493347168" Y="29.51001953125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282138824" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.03659405899" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.19434387207" Y="29.832984375" />
                  <Point X="0.236648422241" Y="29.9908671875" />
                  <Point X="0.378984008789" Y="29.9759609375" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="1.037592773438" Y="29.88273828125" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.623816650391" Y="29.72721484375" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="2.042680908203" Y="29.563572265625" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.446559570312" Y="29.36229296875" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.834246337891" Y="29.123349609375" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.467740722656" Y="27.915630859375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.218924560547" Y="27.46934765625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.204355957031" Y="27.373158203125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.230542236328" Y="27.283333984375" />
                  <Point X="2.261639648438" Y="27.23750390625" />
                  <Point X="2.274939697266" Y="27.224203125" />
                  <Point X="2.292418457031" Y="27.212341796875" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.360336669922" Y="27.172978515625" />
                  <Point X="2.379504150391" Y="27.17066796875" />
                  <Point X="2.429761474609" Y="27.164607421875" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.470831298828" Y="27.171873046875" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.621384277344" Y="27.816158203125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.032967529297" Y="27.977619140625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.25930078125" Y="27.64816796875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.601816894531" Y="26.83341015625" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.263418457031" Y="26.56301953125" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.207177001953" Y="26.47025" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.195657714844" Y="26.367322265625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646240234" Y="26.287955078125" />
                  <Point X="3.228914550781" Y="26.2677890625" />
                  <Point X="3.263703857422" Y="26.21491015625" />
                  <Point X="3.280947998047" Y="26.1988203125" />
                  <Point X="3.300175537109" Y="26.18799609375" />
                  <Point X="3.350590087891" Y="26.1596171875" />
                  <Point X="3.368566162109" Y="26.153619140625" />
                  <Point X="3.394562988281" Y="26.15018359375" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.502063964844" Y="26.27628125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.8663359375" Y="26.250640625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.957059082031" Y="25.83660546875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.100102539062" Y="25.33400390625" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.703546142578" Y="25.218056640625" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.606939941406" Y="25.1473984375" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.551876708984" Y="25.048060546875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.543591308594" Y="24.932642578125" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.582083496094" Y="24.82171484375" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636577148438" Y="24.758091796875" />
                  <Point X="3.662118408203" Y="24.743328125" />
                  <Point X="3.729087158203" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.682295410156" Y="24.4474375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.989109375" Y="24.30340625" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.925536621094" Y="23.933271484375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.828245849609" Y="23.847568359375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.344707763672" Y="23.890763671875" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.155145996094" Y="23.808861328125" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.060015380859" Y="23.638736328125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.084102050781" Y="23.440228515625" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.041832763672" Y="22.644296875" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.318798828125" Y="22.38340625" />
                  <Point X="4.204130859375" Y="22.19785546875" />
                  <Point X="4.156782714844" Y="22.130580078125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.124193603516" Y="22.526736328125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.677679931641" Y="22.7574609375" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.439514404297" Y="22.754669921875" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.262515136719" Y="22.61575390625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.199937744141" Y="22.39396484375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.795321044922" Y="21.24934375" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.971359863281" Y="20.90697265625" />
                  <Point X="2.835296142578" Y="20.809787109375" />
                  <Point X="2.782360107422" Y="20.775521484375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.966085571289" Y="21.63921875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.611707519531" Y="22.05736328125" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805297852" Y="22.16428125" />
                  <Point X="1.361452026367" Y="22.158359375" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.131490234375" />
                  <Point X="1.115640991211" Y="22.090171875" />
                  <Point X="0.985349060059" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.953599243164" Y="21.88565625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.072977539062" Y="20.481189453125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.123071289062" Y="20.06496875" />
                  <Point X="0.994368469238" Y="20.0367578125" />
                  <Point X="0.945438354492" Y="20.027869140625" />
                  <Point X="0.860200439453" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#144" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.053352998312" Y="4.554240584169" Z="0.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="-0.765653893088" Y="5.009330880415" Z="0.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.7" />
                  <Point X="-1.53888330275" Y="4.828200972733" Z="0.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.7" />
                  <Point X="-1.738685014862" Y="4.678946335746" Z="0.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.7" />
                  <Point X="-1.731013149215" Y="4.369069274014" Z="0.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.7" />
                  <Point X="-1.811718071487" Y="4.311066246672" Z="0.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.7" />
                  <Point X="-1.908026923497" Y="4.335606414254" Z="0.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.7" />
                  <Point X="-1.989526285562" Y="4.421243839443" Z="0.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.7" />
                  <Point X="-2.606453328131" Y="4.347579569456" Z="0.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.7" />
                  <Point X="-3.215185472508" Y="3.918887665597" Z="0.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.7" />
                  <Point X="-3.274543206752" Y="3.61319483068" Z="0.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.7" />
                  <Point X="-2.996106460286" Y="3.078382924333" Z="0.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.7" />
                  <Point X="-3.037998358773" Y="3.010805209114" Z="0.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.7" />
                  <Point X="-3.116693541274" Y="2.999458238887" Z="0.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.7" />
                  <Point X="-3.320664517617" Y="3.105650741994" Z="0.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.7" />
                  <Point X="-4.093338346231" Y="2.993328988567" Z="0.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.7" />
                  <Point X="-4.45378924524" Y="2.424712870077" Z="0.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.7" />
                  <Point X="-4.312675727185" Y="2.083594536508" Z="0.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.7" />
                  <Point X="-3.675033327029" Y="1.569477362828" Z="0.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.7" />
                  <Point X="-3.684664972897" Y="1.510628677537" Z="0.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.7" />
                  <Point X="-3.735936887104" Y="1.480179739505" Z="0.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.7" />
                  <Point X="-4.046545936777" Y="1.513492284786" Z="0.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.7" />
                  <Point X="-4.929668189389" Y="1.197217701228" Z="0.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.7" />
                  <Point X="-5.036170156351" Y="0.609893027973" Z="0.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.7" />
                  <Point X="-4.650673186543" Y="0.336876600282" Z="0.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.7" />
                  <Point X="-3.556470166445" Y="0.035125029704" Z="0.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.7" />
                  <Point X="-3.542110887939" Y="0.008229417671" Z="0.7" />
                  <Point X="-3.539556741714" Y="0" Z="0.7" />
                  <Point X="-3.546253713196" Y="-0.02157753339" Z="0.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.7" />
                  <Point X="-3.568898428678" Y="-0.04375095343" Z="0.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.7" />
                  <Point X="-3.98621460223" Y="-0.158835456474" Z="0.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.7" />
                  <Point X="-5.004104387898" Y="-0.83974596943" Z="0.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.7" />
                  <Point X="-4.884384936162" Y="-1.374420773663" Z="0.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.7" />
                  <Point X="-4.3974984588" Y="-1.461994625715" Z="0.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.7" />
                  <Point X="-3.199987896567" Y="-1.318146519182" Z="0.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.7" />
                  <Point X="-3.19825366347" Y="-1.343984203209" Z="0.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.7" />
                  <Point X="-3.559994216577" Y="-1.628138236735" Z="0.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.7" />
                  <Point X="-4.290400079136" Y="-2.707986658665" Z="0.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.7" />
                  <Point X="-3.958074331378" Y="-3.174017995086" Z="0.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.7" />
                  <Point X="-3.506247891613" Y="-3.094394550144" Z="0.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.7" />
                  <Point X="-2.560280735316" Y="-2.568049733816" Z="0.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.7" />
                  <Point X="-2.761022463025" Y="-2.928830490879" Z="0.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.7" />
                  <Point X="-3.003520936032" Y="-4.09046094365" Z="0.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.7" />
                  <Point X="-2.572420281254" Y="-4.374434056027" Z="0.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.7" />
                  <Point X="-2.389026236767" Y="-4.368622356368" Z="0.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.7" />
                  <Point X="-2.039478050199" Y="-4.031673492466" Z="0.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.7" />
                  <Point X="-1.744141488809" Y="-3.998773624641" Z="0.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.7" />
                  <Point X="-1.489807102284" Y="-4.15246215987" Z="0.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.7" />
                  <Point X="-1.381589590865" Y="-4.429220159762" Z="0.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.7" />
                  <Point X="-1.378191764104" Y="-4.614356200169" Z="0.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.7" />
                  <Point X="-1.199041045831" Y="-4.934578513964" Z="0.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.7" />
                  <Point X="-0.900421740017" Y="-4.997700173976" Z="0.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="-0.707071366691" Y="-4.601010366945" Z="0.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="-0.298562751171" Y="-3.348002549857" Z="0.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="-0.069950048917" Y="-3.225949282224" Z="0.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.7" />
                  <Point X="0.183409030445" Y="-3.261162763064" Z="0.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.7" />
                  <Point X="0.371883108261" Y="-3.453643205911" Z="0.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.7" />
                  <Point X="0.52768349371" Y="-3.931525662871" Z="0.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.7" />
                  <Point X="0.948219414019" Y="-4.990046538207" Z="0.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.7" />
                  <Point X="1.127620944161" Y="-4.952590767458" Z="0.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.7" />
                  <Point X="1.116393894333" Y="-4.481003890599" Z="0.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.7" />
                  <Point X="0.996302414766" Y="-3.093682633852" Z="0.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.7" />
                  <Point X="1.141451941776" Y="-2.91699254984" Z="0.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.7" />
                  <Point X="1.359877418474" Y="-2.860148414285" Z="0.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.7" />
                  <Point X="1.578512527988" Y="-2.953415706167" Z="0.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.7" />
                  <Point X="1.920261894051" Y="-3.35993807436" Z="0.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.7" />
                  <Point X="2.803373273275" Y="-4.235172633202" Z="0.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.7" />
                  <Point X="2.994265496227" Y="-4.102433867319" Z="0.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.7" />
                  <Point X="2.83246630322" Y="-3.694376062426" Z="0.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.7" />
                  <Point X="2.242985763225" Y="-2.565868711167" Z="0.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.7" />
                  <Point X="2.300605414219" Y="-2.376253416991" Z="0.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.7" />
                  <Point X="2.456644961381" Y="-2.258295895748" Z="0.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.7" />
                  <Point X="2.662638092345" Y="-2.260462246266" Z="0.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.7" />
                  <Point X="3.093037534681" Y="-2.485283197054" Z="0.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.7" />
                  <Point X="4.191514831845" Y="-2.866915817686" Z="0.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.7" />
                  <Point X="4.355175886266" Y="-2.611598338208" Z="0.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.7" />
                  <Point X="4.066114754763" Y="-2.284754971357" Z="0.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.7" />
                  <Point X="3.1200041733" Y="-1.501452837403" Z="0.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.7" />
                  <Point X="3.103648842307" Y="-1.334564463649" Z="0.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.7" />
                  <Point X="3.187436227175" Y="-1.191824836551" Z="0.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.7" />
                  <Point X="3.349171663013" Y="-1.126815995452" Z="0.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.7" />
                  <Point X="3.815563489917" Y="-1.170722566858" Z="0.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.7" />
                  <Point X="4.968127608171" Y="-1.046573803354" Z="0.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.7" />
                  <Point X="5.032394817167" Y="-0.672767447834" Z="0.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.7" />
                  <Point X="4.689080162713" Y="-0.472984968998" Z="0.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.7" />
                  <Point X="3.680983847814" Y="-0.182101440123" Z="0.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.7" />
                  <Point X="3.615261403583" Y="-0.116137774597" Z="0.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.7" />
                  <Point X="3.58654295334" Y="-0.02667325524" Z="0.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.7" />
                  <Point X="3.594828497084" Y="0.069937275988" Z="0.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.7" />
                  <Point X="3.640118034817" Y="0.147810963978" Z="0.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.7" />
                  <Point X="3.722411566538" Y="0.206047404119" Z="0.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.7" />
                  <Point X="4.106887654593" Y="0.316986963771" Z="0.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.7" />
                  <Point X="5.000307868661" Y="0.875577168352" Z="0.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.7" />
                  <Point X="4.908761008699" Y="1.293748032518" Z="0.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.7" />
                  <Point X="4.489381810127" Y="1.357133869027" Z="0.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.7" />
                  <Point X="3.394957525212" Y="1.231032771438" Z="0.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.7" />
                  <Point X="3.318546068957" Y="1.262847604337" Z="0.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.7" />
                  <Point X="3.26452923323" Y="1.326549266376" Z="0.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.7" />
                  <Point X="3.238470267049" Y="1.40870679742" Z="0.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.7" />
                  <Point X="3.249173425876" Y="1.48806453171" Z="0.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.7" />
                  <Point X="3.296945046753" Y="1.563883008694" Z="0.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.7" />
                  <Point X="3.626099072717" Y="1.825022606247" Z="0.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.7" />
                  <Point X="4.295922214088" Y="2.705334346938" Z="0.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.7" />
                  <Point X="4.067397222373" Y="3.038102237239" Z="0.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.7" />
                  <Point X="3.590228243866" Y="2.890739282489" Z="0.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.7" />
                  <Point X="2.451757935059" Y="2.251456726672" Z="0.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.7" />
                  <Point X="2.379334338358" Y="2.251589276414" Z="0.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.7" />
                  <Point X="2.314337010788" Y="2.284997900013" Z="0.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.7" />
                  <Point X="2.265760700222" Y="2.342687849594" Z="0.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.7" />
                  <Point X="2.247840426344" Y="2.410424105474" Z="0.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.7" />
                  <Point X="2.261071215986" Y="2.487711640823" Z="0.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.7" />
                  <Point X="2.50488607037" Y="2.921910838089" Z="0.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.7" />
                  <Point X="2.857067342783" Y="4.195379092222" Z="0.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.7" />
                  <Point X="2.465573629946" Y="4.436777312369" Z="0.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.7" />
                  <Point X="2.05770641194" Y="4.640144369404" Z="0.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.7" />
                  <Point X="1.634709323692" Y="4.805499789967" Z="0.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.7" />
                  <Point X="1.043171283477" Y="4.962622515719" Z="0.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.7" />
                  <Point X="0.378035863475" Y="5.056970447716" Z="0.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.7" />
                  <Point X="0.139891563241" Y="4.877206921782" Z="0.7" />
                  <Point X="0" Y="4.355124473572" Z="0.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>