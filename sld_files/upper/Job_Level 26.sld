<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#167" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1945" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.76063885498" Y="20.75100390625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557720153809" Y="21.502861328125" />
                  <Point X="0.542363037109" Y="21.532623046875" />
                  <Point X="0.461706329346" Y="21.64883203125" />
                  <Point X="0.378635223389" Y="21.7685234375" />
                  <Point X="0.356755706787" Y="21.790974609375" />
                  <Point X="0.330500366211" Y="21.810220703125" />
                  <Point X="0.302494873047" Y="21.82433203125" />
                  <Point X="0.177683532715" Y="21.863068359375" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976791382" Y="21.907234375" />
                  <Point X="-0.00866468811" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.161635116577" Y="21.864228515625" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318185058594" Y="21.81022265625" />
                  <Point X="-0.344440246582" Y="21.790978515625" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.446980133057" Y="21.6523125" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.754234436035" Y="20.72895703125" />
                  <Point X="-0.916584899902" Y="20.12305859375" />
                  <Point X="-0.931540222168" Y="20.1259609375" />
                  <Point X="-1.079322998047" Y="20.154646484375" />
                  <Point X="-1.220783691406" Y="20.19104296875" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.238717651367" Y="20.256125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.246326049805" Y="20.63367578125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868796875" />
                  <Point X="-1.438555053711" Y="20.9695703125" />
                  <Point X="-1.556905639648" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643027587891" Y="21.109033203125" />
                  <Point X="-1.795539306641" Y="21.119029296875" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.16973828125" Y="21.020287109375" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.449208984375" Y="20.751830078125" />
                  <Point X="-2.480147460938" Y="20.711509765625" />
                  <Point X="-2.585068359375" Y="20.776474609375" />
                  <Point X="-2.801726318359" Y="20.910623046875" />
                  <Point X="-2.997564453125" Y="21.061412109375" />
                  <Point X="-3.104722412109" Y="21.143919921875" />
                  <Point X="-2.790489257812" Y="21.6881875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858642578" Y="22.352349609375" />
                  <Point X="-2.406587890625" Y="22.38387890625" />
                  <Point X="-2.405576416016" Y="22.41481640625" />
                  <Point X="-2.414563964844" Y="22.444435546875" />
                  <Point X="-2.428783691406" Y="22.473263671875" />
                  <Point X="-2.446809326172" Y="22.498416015625" />
                  <Point X="-2.464154052734" Y="22.515759765625" />
                  <Point X="-2.489305908203" Y="22.533783203125" />
                  <Point X="-2.518135253906" Y="22.548001953125" />
                  <Point X="-2.547755126953" Y="22.556986328125" />
                  <Point X="-2.57869140625" Y="22.555974609375" />
                  <Point X="-2.610218261719" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.293391357422" Y="22.16109375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.911701904297" Y="21.981271484375" />
                  <Point X="-4.08286328125" Y="22.206142578125" />
                  <Point X="-4.223270996094" Y="22.4415859375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.747609619141" Y="23.009125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.58016796875" />
                  <Point X="-3.046151611328" Y="23.609916015625" />
                  <Point X="-3.043347412109" Y="23.640345703125" />
                  <Point X="-3.045556640625" Y="23.672015625" />
                  <Point X="-3.05255859375" Y="23.70176171875" />
                  <Point X="-3.068641845703" Y="23.727744140625" />
                  <Point X="-3.089474365234" Y="23.75169921875" />
                  <Point X="-3.112973876953" Y="23.771232421875" />
                  <Point X="-3.139456542969" Y="23.786818359375" />
                  <Point X="-3.168716064453" Y="23.798041015625" />
                  <Point X="-3.200603515625" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.057804199219" Y="23.69688671875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.767135742188" Y="23.745271484375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.8712265625" Y="24.267087890625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.263817871094" Y="24.583734375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.510330566406" Y="24.78887109375" />
                  <Point X="-3.48108984375" Y="24.805580078125" />
                  <Point X="-3.46789453125" Y="24.81467578125" />
                  <Point X="-3.441995361328" Y="24.835966796875" />
                  <Point X="-3.425827880859" Y="24.853021484375" />
                  <Point X="-3.414340820312" Y="24.873521484375" />
                  <Point X="-3.402358398438" Y="24.902943359375" />
                  <Point X="-3.397795166016" Y="24.91732421875" />
                  <Point X="-3.390854492188" Y="24.947267578125" />
                  <Point X="-3.388400878906" Y="24.968666015625" />
                  <Point X="-3.390830322266" Y="24.99006640625" />
                  <Point X="-3.397634033203" Y="25.0195703125" />
                  <Point X="-3.402177978516" Y="25.033947265625" />
                  <Point X="-3.414297119141" Y="25.06380859375" />
                  <Point X="-3.425758544922" Y="25.084322265625" />
                  <Point X="-3.441904296875" Y="25.10139453125" />
                  <Point X="-3.46739453125" Y="25.12240234375" />
                  <Point X="-3.480573242188" Y="25.131513671875" />
                  <Point X="-3.510223144531" Y="25.1485078125" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.285700195312" Y="25.359568359375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.867630371094" Y="25.685423828125" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.749707519531" Y="26.252935546875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.289623046875" Y="26.3687734375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423828125" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.672878173828" Y="26.3148046875" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.622779052734" Y="26.332962890625" />
                  <Point X="-3.604034423828" Y="26.34378515625" />
                  <Point X="-3.587353271484" Y="26.356015625" />
                  <Point X="-3.57371484375" Y="26.371568359375" />
                  <Point X="-3.561300048828" Y="26.389298828125" />
                  <Point X="-3.551351806641" Y="26.407431640625" />
                  <Point X="-3.539209716797" Y="26.436744140625" />
                  <Point X="-3.526704345703" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.486794921875" />
                  <Point X="-3.517157470703" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951660156" Y="26.5491953125" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532049804688" Y="26.589376953125" />
                  <Point X="-3.546700439453" Y="26.617521484375" />
                  <Point X="-3.561789550781" Y="26.646505859375" />
                  <Point X="-3.573281494141" Y="26.663705078125" />
                  <Point X="-3.587193603516" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.033957519531" Y="27.025939453125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.248787109375" Y="27.446462890625" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.883064453125" Y="27.9882734375" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.528456787109" Y="28.030462890625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728515625" Y="27.836341796875" />
                  <Point X="-3.167085693359" Y="27.82983203125" />
                  <Point X="-3.146794921875" Y="27.825794921875" />
                  <Point X="-3.104651123047" Y="27.822107421875" />
                  <Point X="-3.061245605469" Y="27.818310546875" />
                  <Point X="-3.040559814453" Y="27.81876171875" />
                  <Point X="-3.019101806641" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.916163818359" Y="27.890142578125" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.847123046875" Y="28.078263671875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.0611484375" Y="28.512966796875" />
                  <Point X="-3.183333496094" Y="28.724595703125" />
                  <Point X="-2.9925859375" Y="28.870841796875" />
                  <Point X="-2.700629394531" Y="29.094681640625" />
                  <Point X="-2.388643554688" Y="29.268013671875" />
                  <Point X="-2.167036621094" Y="29.391134765625" />
                  <Point X="-2.141708251953" Y="29.358125" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028893432617" Y="29.21480078125" />
                  <Point X="-2.012314697266" Y="29.200888671875" />
                  <Point X="-1.995114379883" Y="29.189396484375" />
                  <Point X="-1.948208496094" Y="29.164978515625" />
                  <Point X="-1.89989831543" Y="29.139828125" />
                  <Point X="-1.880626098633" Y="29.13233203125" />
                  <Point X="-1.859719482422" Y="29.126728515625" />
                  <Point X="-1.839269775391" Y="29.123580078125" />
                  <Point X="-1.818623291016" Y="29.12493359375" />
                  <Point X="-1.797307617188" Y="29.128693359375" />
                  <Point X="-1.777452270508" Y="29.134482421875" />
                  <Point X="-1.728596557617" Y="29.154720703125" />
                  <Point X="-1.678278076172" Y="29.1755625" />
                  <Point X="-1.660147583008" Y="29.185509765625" />
                  <Point X="-1.642417480469" Y="29.197923828125" />
                  <Point X="-1.626864990234" Y="29.2115625" />
                  <Point X="-1.614633300781" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.579578735352" Y="29.31635546875" />
                  <Point X="-1.563201171875" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.579484985352" Y="29.5960703125" />
                  <Point X="-1.584201660156" Y="29.631896484375" />
                  <Point X="-1.327596313477" Y="29.70383984375" />
                  <Point X="-0.949623413086" Y="29.80980859375" />
                  <Point X="-0.571423522949" Y="29.85407421875" />
                  <Point X="-0.294711395264" Y="29.886458984375" />
                  <Point X="-0.229100646973" Y="29.641595703125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.244262451172" Y="29.652232421875" />
                  <Point X="0.307419586182" Y="29.8879375" />
                  <Point X="0.514019348145" Y="29.86630078125" />
                  <Point X="0.844030578613" Y="29.831740234375" />
                  <Point X="1.156950317383" Y="29.75619140625" />
                  <Point X="1.481038696289" Y="29.6779453125" />
                  <Point X="1.683948852539" Y="29.604349609375" />
                  <Point X="1.894646850586" Y="29.527927734375" />
                  <Point X="2.091590820313" Y="29.43582421875" />
                  <Point X="2.294555175781" Y="29.34090234375" />
                  <Point X="2.484860351562" Y="29.23003125" />
                  <Point X="2.680991210938" Y="29.115767578125" />
                  <Point X="2.86041796875" Y="28.98816796875" />
                  <Point X="2.943260498047" Y="28.92925390625" />
                  <Point X="2.572041259766" Y="28.286283203125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.122500488281" Y="27.47650390625" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.111852050781" Y="27.346751953125" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.161233154297" Y="27.216283203125" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.19446484375" Y="27.170328125" />
                  <Point X="2.221597412109" Y="27.14559375" />
                  <Point X="2.252784912109" Y="27.124431640625" />
                  <Point X="2.284906005859" Y="27.102634765625" />
                  <Point X="2.304946289062" Y="27.0922734375" />
                  <Point X="2.32703515625" Y="27.084005859375" />
                  <Point X="2.348965820312" Y="27.078662109375" />
                  <Point X="2.383166259766" Y="27.0745390625" />
                  <Point X="2.418390625" Y="27.070291015625" />
                  <Point X="2.436467285156" Y="27.06984375" />
                  <Point X="2.47320703125" Y="27.074169921875" />
                  <Point X="2.512758300781" Y="27.08474609375" />
                  <Point X="2.553493408203" Y="27.095640625" />
                  <Point X="2.565292480469" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.34573046875" Y="27.547314453125" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.006940917969" Y="27.851134765625" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.223307617188" Y="27.52415234375" />
                  <Point X="4.262198730469" Y="27.459884765625" />
                  <Point X="3.789625488281" Y="27.097265625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.175508789062" Y="26.6044921875" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606689453" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.111026611328" Y="26.479171875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.106443359375" Y="26.32958203125" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282470703" Y="26.23573828125" />
                  <Point X="3.159956787109" Y="26.19975390625" />
                  <Point X="3.184340087891" Y="26.162693359375" />
                  <Point X="3.198896972656" Y="26.1454453125" />
                  <Point X="3.216140380859" Y="26.129357421875" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.268653564453" Y="26.09672265625" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320522949219" Y="26.0695" />
                  <Point X="3.356119140625" Y="26.0594375" />
                  <Point X="3.402505126953" Y="26.053306640625" />
                  <Point X="3.450279785156" Y="26.046994140625" />
                  <Point X="3.462699462891" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.207489746094" Y="26.1416796875" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.79597265625" Y="26.13804296875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.877459960938" Y="25.7303359375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.357024414062" Y="25.501197265625" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704786132812" Y="25.325583984375" />
                  <Point X="3.681547119141" Y="25.31506640625" />
                  <Point X="3.635974365234" Y="25.288724609375" />
                  <Point X="3.589037109375" Y="25.261595703125" />
                  <Point X="3.574315917969" Y="25.251099609375" />
                  <Point X="3.547530029297" Y="25.225576171875" />
                  <Point X="3.520186279297" Y="25.190732421875" />
                  <Point X="3.492024169922" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.454565917969" Y="25.045009765625" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.454293457031" Y="24.893853515625" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470527099609" Y="24.8233359375" />
                  <Point X="3.480300537109" Y="24.801873046875" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.519368164062" Y="24.74775" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559994628906" Y="24.698767578125" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.634608398438" Y="24.649501953125" />
                  <Point X="3.681545654297" Y="24.62237109375" />
                  <Point X="3.692708984375" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.376197753906" Y="24.431103515625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.882919921875" Y="24.236310546875" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.814634765625" Y="23.8742890625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.167117675781" Y="23.898775390625" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658691406" Y="23.994490234375" />
                  <Point X="3.285215332031" Y="23.975048828125" />
                  <Point X="3.193094482422" Y="23.95502734375" />
                  <Point X="3.163974853516" Y="23.943404296875" />
                  <Point X="3.136147705078" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.058334228516" Y="23.841017578125" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.962009033203" Y="23.6104296875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.025949951172" Y="23.355009765625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.7227578125" Y="22.76938671875" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.203450683594" Y="22.37746484375" />
                  <Point X="4.1248046875" Y="22.250203125" />
                  <Point X="4.041285888672" Y="22.13153515625" />
                  <Point X="4.028981933594" Y="22.114052734375" />
                  <Point X="3.4624609375" Y="22.441134765625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224121094" Y="22.840173828125" />
                  <Point X="2.647772460938" Y="22.8593984375" />
                  <Point X="2.538133789062" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.356398193359" Y="22.818279296875" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.157988769531" Y="22.621125" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.114900390625" Y="22.3302890625" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.545173828125" Y="21.492611328125" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781839111328" Y="20.88834765625" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="2.263336425781" Y="21.407888671875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923583984" Y="22.099443359375" />
                  <Point X="1.61693347168" Y="22.16694140625" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986816406" Y="22.24883203125" />
                  <Point X="1.448366577148" Y="22.2565625" />
                  <Point X="1.417100219727" Y="22.258880859375" />
                  <Point X="1.302275634766" Y="22.248314453125" />
                  <Point X="1.184013427734" Y="22.23743359375" />
                  <Point X="1.156363647461" Y="22.2306015625" />
                  <Point X="1.128977539062" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="1.015930419922" Y="22.130814453125" />
                  <Point X="0.92461151123" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.849114135742" Y="21.85222265625" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.931215759277" Y="20.83015234375" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="0.975712280273" Y="20.129923828125" />
                  <Point X="0.929315673828" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058407226562" Y="20.247359375" />
                  <Point X="-1.141245849609" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.153151611328" Y="20.652208984375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573242188" Y="20.905138671875" />
                  <Point X="-1.250208251953" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94022265625" />
                  <Point X="-1.375917236328" Y="21.04099609375" />
                  <Point X="-1.494267822266" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.533023681641" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.62145690918" Y="21.201552734375" />
                  <Point X="-1.636814331055" Y="21.203830078125" />
                  <Point X="-1.789326049805" Y="21.213826171875" />
                  <Point X="-1.946403442383" Y="21.22412109375" />
                  <Point X="-1.961927856445" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.222517333984" Y="21.09927734375" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359682373047" Y="21.007244140625" />
                  <Point X="-2.380450683594" Y="20.989861328125" />
                  <Point X="-2.402763427734" Y="20.96722265625" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.503201904297" Y="20.83751953125" />
                  <Point X="-2.535056884766" Y="20.857244140625" />
                  <Point X="-2.747604003906" Y="20.98884765625" />
                  <Point X="-2.939607177734" Y="21.13668359375" />
                  <Point X="-2.980863769531" Y="21.16844921875" />
                  <Point X="-2.708216796875" Y="21.6406875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849609375" Y="22.28991796875" />
                  <Point X="-2.323947021484" Y="22.31888671875" />
                  <Point X="-2.31968359375" Y="22.333818359375" />
                  <Point X="-2.313412841797" Y="22.36534765625" />
                  <Point X="-2.311638671875" Y="22.380775390625" />
                  <Point X="-2.310627197266" Y="22.411712890625" />
                  <Point X="-2.314669433594" Y="22.442400390625" />
                  <Point X="-2.323656982422" Y="22.47201953125" />
                  <Point X="-2.329364990234" Y="22.4864609375" />
                  <Point X="-2.343584716797" Y="22.5152890625" />
                  <Point X="-2.351565673828" Y="22.528603515625" />
                  <Point X="-2.369591308594" Y="22.553755859375" />
                  <Point X="-2.379635986328" Y="22.56559375" />
                  <Point X="-2.396980712891" Y="22.5829375" />
                  <Point X="-2.408818847656" Y="22.59298046875" />
                  <Point X="-2.433970703125" Y="22.61100390625" />
                  <Point X="-2.447284423828" Y="22.618984375" />
                  <Point X="-2.476113769531" Y="22.633203125" />
                  <Point X="-2.490560302734" Y="22.638912109375" />
                  <Point X="-2.520180175781" Y="22.647896484375" />
                  <Point X="-2.550860351562" Y="22.651935546875" />
                  <Point X="-2.581796630859" Y="22.650923828125" />
                  <Point X="-2.597226074219" Y="22.6491484375" />
                  <Point X="-2.628752929688" Y="22.642876953125" />
                  <Point X="-2.64368359375" Y="22.63861328125" />
                  <Point X="-2.672648925781" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.340891357422" Y="22.243365234375" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.836108642578" Y="22.038810546875" />
                  <Point X="-4.004020751953" Y="22.259412109375" />
                  <Point X="-4.141678222656" Y="22.490244140625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.68977734375" Y="22.933755859375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481201172" Y="23.436689453125" />
                  <Point X="-3.015102783203" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833740234" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890625" Y="23.556349609375" />
                  <Point X="-2.954186035156" Y="23.58609765625" />
                  <Point X="-2.951552490234" Y="23.60119921875" />
                  <Point X="-2.948748291016" Y="23.63162890625" />
                  <Point X="-2.948577636719" Y="23.64695703125" />
                  <Point X="-2.950786865234" Y="23.678626953125" />
                  <Point X="-2.953083984375" Y="23.693783203125" />
                  <Point X="-2.9600859375" Y="23.723529296875" />
                  <Point X="-2.971781982422" Y="23.751763671875" />
                  <Point X="-2.987865234375" Y="23.77774609375" />
                  <Point X="-2.996957275391" Y="23.790083984375" />
                  <Point X="-3.017789794922" Y="23.8140390625" />
                  <Point X="-3.028748046875" Y="23.824755859375" />
                  <Point X="-3.052247558594" Y="23.8442890625" />
                  <Point X="-3.064788818359" Y="23.85310546875" />
                  <Point X="-3.091271484375" Y="23.86869140625" />
                  <Point X="-3.105435546875" Y="23.875517578125" />
                  <Point X="-3.134695068359" Y="23.886740234375" />
                  <Point X="-3.149790527344" Y="23.89113671875" />
                  <Point X="-3.181677978516" Y="23.897619140625" />
                  <Point X="-3.197294433594" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.070204101562" Y="23.79107421875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.675090820312" Y="23.768783203125" />
                  <Point X="-4.740763183594" Y="24.02588671875" />
                  <Point X="-4.77718359375" Y="24.280537109375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.239229980469" Y="24.491970703125" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.496711914063" Y="24.6917421875" />
                  <Point X="-3.474166503906" Y="24.7010234375" />
                  <Point X="-3.463197265625" Y="24.706388671875" />
                  <Point X="-3.433956542969" Y="24.72309765625" />
                  <Point X="-3.427173339844" Y="24.72736328125" />
                  <Point X="-3.407565917969" Y="24.7412890625" />
                  <Point X="-3.381666748047" Y="24.762580078125" />
                  <Point X="-3.37305078125" Y="24.770609375" />
                  <Point X="-3.356883300781" Y="24.7876640625" />
                  <Point X="-3.342951904297" Y="24.80658203125" />
                  <Point X="-3.33146484375" Y="24.82708203125" />
                  <Point X="-3.326357666016" Y="24.837689453125" />
                  <Point X="-3.314375244141" Y="24.867111328125" />
                  <Point X="-3.311807861328" Y="24.8742109375" />
                  <Point X="-3.305248779297" Y="24.895873046875" />
                  <Point X="-3.298308105469" Y="24.92581640625" />
                  <Point X="-3.296472900391" Y="24.9364453125" />
                  <Point X="-3.294019287109" Y="24.95784375" />
                  <Point X="-3.294007080078" Y="24.9793828125" />
                  <Point X="-3.296436523438" Y="25.000783203125" />
                  <Point X="-3.298259765625" Y="25.0114140625" />
                  <Point X="-3.305063476562" Y="25.04091796875" />
                  <Point X="-3.307050537109" Y="25.04819921875" />
                  <Point X="-3.314151367188" Y="25.069671875" />
                  <Point X="-3.326270507812" Y="25.099533203125" />
                  <Point X="-3.331363769531" Y="25.11014453125" />
                  <Point X="-3.342825195312" Y="25.130658203125" />
                  <Point X="-3.356736572266" Y="25.14959765625" />
                  <Point X="-3.372882324219" Y="25.166669921875" />
                  <Point X="-3.381484863281" Y="25.174705078125" />
                  <Point X="-3.406975097656" Y="25.195712890625" />
                  <Point X="-3.413369384766" Y="25.200544921875" />
                  <Point X="-3.433332519531" Y="25.213935546875" />
                  <Point X="-3.462982421875" Y="25.2309296875" />
                  <Point X="-3.474004882812" Y="25.23633203125" />
                  <Point X="-3.496657714844" Y="25.245673828125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.261112304688" Y="25.45133203125" />
                  <Point X="-4.785446289062" Y="25.591826171875" />
                  <Point X="-4.773653808594" Y="25.671517578125" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.658014648438" Y="26.228087890625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.302022949219" Y="26.2745859375" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703369141" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704889892578" Y="26.2080546875" />
                  <Point X="-3.684604248047" Y="26.21208984375" />
                  <Point X="-3.674571044922" Y="26.21466015625" />
                  <Point X="-3.644311035156" Y="26.224201171875" />
                  <Point X="-3.613145019531" Y="26.23402734375" />
                  <Point X="-3.603446289062" Y="26.237677734375" />
                  <Point X="-3.584513183594" Y="26.246009765625" />
                  <Point X="-3.575278808594" Y="26.25069140625" />
                  <Point X="-3.556534179688" Y="26.261513671875" />
                  <Point X="-3.547862060547" Y="26.267171875" />
                  <Point X="-3.531180908203" Y="26.27940234375" />
                  <Point X="-3.515926269531" Y="26.293380859375" />
                  <Point X="-3.502287841797" Y="26.30893359375" />
                  <Point X="-3.495895019531" Y="26.317080078125" />
                  <Point X="-3.483480224609" Y="26.334810546875" />
                  <Point X="-3.478011474609" Y="26.343603515625" />
                  <Point X="-3.468063232422" Y="26.361736328125" />
                  <Point X="-3.463583740234" Y="26.371076171875" />
                  <Point X="-3.451441650391" Y="26.400388671875" />
                  <Point X="-3.438936279297" Y="26.430580078125" />
                  <Point X="-3.435499755859" Y="26.4403515625" />
                  <Point X="-3.429711181641" Y="26.4602109375" />
                  <Point X="-3.427359130859" Y="26.470298828125" />
                  <Point X="-3.423600830078" Y="26.49161328125" />
                  <Point X="-3.422360839844" Y="26.501896484375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057373047" Y="26.5636484375" />
                  <Point X="-3.427189208984" Y="26.57378515625" />
                  <Point X="-3.432791503906" Y="26.59469140625" />
                  <Point X="-3.436013183594" Y="26.604533203125" />
                  <Point X="-3.443509033203" Y="26.62380859375" />
                  <Point X="-3.447783203125" Y="26.6332421875" />
                  <Point X="-3.462433837891" Y="26.66138671875" />
                  <Point X="-3.477522949219" Y="26.69037109375" />
                  <Point X="-3.482799560547" Y="26.69928515625" />
                  <Point X="-3.494291503906" Y="26.716484375" />
                  <Point X="-3.500506835938" Y="26.72476953125" />
                  <Point X="-3.514418945312" Y="26.741349609375" />
                  <Point X="-3.521498291016" Y="26.748908203125" />
                  <Point X="-3.536440429688" Y="26.763212890625" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.976125" Y="27.10130859375" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.166740722656" Y="27.39857421875" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.808083740234" Y="27.929939453125" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.575956787109" Y="27.94819140625" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923095703" Y="27.75771875" />
                  <Point X="-3.225994628906" Y="27.749388671875" />
                  <Point X="-3.216300048828" Y="27.745740234375" />
                  <Point X="-3.195657226562" Y="27.73923046875" />
                  <Point X="-3.185623779297" Y="27.736658203125" />
                  <Point X="-3.165333007812" Y="27.73262109375" />
                  <Point X="-3.155075683594" Y="27.73115625" />
                  <Point X="-3.112931884766" Y="27.72746875" />
                  <Point X="-3.069526367188" Y="27.723671875" />
                  <Point X="-3.059174072266" Y="27.723333984375" />
                  <Point X="-3.03848828125" Y="27.72378515625" />
                  <Point X="-3.028154785156" Y="27.72457421875" />
                  <Point X="-3.006696777344" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976423095703" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902746826172" Y="27.773193359375" />
                  <Point X="-2.886614990234" Y="27.786140625" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.848988769531" Y="27.822966796875" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752484619141" Y="28.08654296875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.978875976562" Y="28.560466796875" />
                  <Point X="-3.059388183594" Y="28.699916015625" />
                  <Point X="-2.934783447266" Y="28.795451171875" />
                  <Point X="-2.648385498047" Y="29.015029296875" />
                  <Point X="-2.342506103516" Y="29.18496875" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.118564941406" Y="29.171912109375" />
                  <Point X="-2.111822998047" Y="29.16405078125" />
                  <Point X="-2.097520996094" Y="29.149109375" />
                  <Point X="-2.089960449219" Y="29.142029296875" />
                  <Point X="-2.073381835938" Y="29.1281171875" />
                  <Point X="-2.065091552734" Y="29.1218984375" />
                  <Point X="-2.047891357422" Y="29.11040625" />
                  <Point X="-2.038980957031" Y="29.105130859375" />
                  <Point X="-1.992075073242" Y="29.080712890625" />
                  <Point X="-1.943764892578" Y="29.0555625" />
                  <Point X="-1.93433605957" Y="29.0512890625" />
                  <Point X="-1.915063842773" Y="29.04379296875" />
                  <Point X="-1.905220458984" Y="29.0405703125" />
                  <Point X="-1.884313842773" Y="29.034966796875" />
                  <Point X="-1.874175292969" Y="29.032833984375" />
                  <Point X="-1.853725708008" Y="29.029685546875" />
                  <Point X="-1.833055053711" Y="29.028783203125" />
                  <Point X="-1.812408691406" Y="29.03013671875" />
                  <Point X="-1.802121459961" Y="29.031376953125" />
                  <Point X="-1.780805786133" Y="29.03513671875" />
                  <Point X="-1.770716430664" Y="29.037490234375" />
                  <Point X="-1.750861206055" Y="29.043279296875" />
                  <Point X="-1.741094970703" Y="29.04671484375" />
                  <Point X="-1.692239257812" Y="29.066953125" />
                  <Point X="-1.641920654297" Y="29.087794921875" />
                  <Point X="-1.632582275391" Y="29.092275390625" />
                  <Point X="-1.614451782227" Y="29.10222265625" />
                  <Point X="-1.605659790039" Y="29.107689453125" />
                  <Point X="-1.5879296875" Y="29.120103515625" />
                  <Point X="-1.579780883789" Y="29.126498046875" />
                  <Point X="-1.564228393555" Y="29.14013671875" />
                  <Point X="-1.550253173828" Y="29.15538671875" />
                  <Point X="-1.538021362305" Y="29.172068359375" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153918457" Y="29.19948828125" />
                  <Point X="-1.516856445312" Y="29.208728515625" />
                  <Point X="-1.508525512695" Y="29.227662109375" />
                  <Point X="-1.504877441406" Y="29.23735546875" />
                  <Point X="-1.488975708008" Y="29.2877890625" />
                  <Point X="-1.472598144531" Y="29.339732421875" />
                  <Point X="-1.470026977539" Y="29.349763671875" />
                  <Point X="-1.465991577148" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265869141" Y="29.562654296875" />
                  <Point X="-1.301950439453" Y="29.6123671875" />
                  <Point X="-0.931163635254" Y="29.7163203125" />
                  <Point X="-0.560379821777" Y="29.75971875" />
                  <Point X="-0.365222747803" Y="29.78255859375" />
                  <Point X="-0.320863616943" Y="29.6170078125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.336025390625" Y="29.62764453125" />
                  <Point X="0.378190673828" Y="29.7850078125" />
                  <Point X="0.504124267578" Y="29.771818359375" />
                  <Point X="0.827851806641" Y="29.737916015625" />
                  <Point X="1.134654907227" Y="29.66384375" />
                  <Point X="1.453622314453" Y="29.586833984375" />
                  <Point X="1.651557006836" Y="29.51504296875" />
                  <Point X="1.858249145508" Y="29.44007421875" />
                  <Point X="2.051346191406" Y="29.34976953125" />
                  <Point X="2.250429443359" Y="29.256662109375" />
                  <Point X="2.437037841797" Y="29.1479453125" />
                  <Point X="2.629445068359" Y="29.0358515625" />
                  <Point X="2.805361083984" Y="28.910748046875" />
                  <Point X="2.817780273438" Y="28.901916015625" />
                  <Point X="2.489768798828" Y="28.333783203125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.030725219727" Y="27.501044921875" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017912719727" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.01753527832" Y="27.33537890625" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318359375" Y="27.223888671875" />
                  <Point X="2.055680908203" Y="27.20384375" />
                  <Point X="2.061459960938" Y="27.19412890625" />
                  <Point X="2.082622070312" Y="27.16294140625" />
                  <Point X="2.104417724609" Y="27.1308203125" />
                  <Point X="2.109808105469" Y="27.1236328125" />
                  <Point X="2.130464111328" Y="27.10012109375" />
                  <Point X="2.157596679688" Y="27.07538671875" />
                  <Point X="2.168256103516" Y="27.066982421875" />
                  <Point X="2.199443603516" Y="27.0458203125" />
                  <Point X="2.231564697266" Y="27.0240234375" />
                  <Point X="2.241275146484" Y="27.01824609375" />
                  <Point X="2.261315429688" Y="27.007884765625" />
                  <Point X="2.271645263672" Y="27.00330078125" />
                  <Point X="2.293734130859" Y="26.995033203125" />
                  <Point X="2.304544921875" Y="26.99170703125" />
                  <Point X="2.326475585938" Y="26.98636328125" />
                  <Point X="2.337595458984" Y="26.984345703125" />
                  <Point X="2.371795898438" Y="26.98022265625" />
                  <Point X="2.407020263672" Y="26.975974609375" />
                  <Point X="2.416040771484" Y="26.9753203125" />
                  <Point X="2.447576904297" Y="26.97549609375" />
                  <Point X="2.484316650391" Y="26.979822265625" />
                  <Point X="2.497748046875" Y="26.98239453125" />
                  <Point X="2.537299316406" Y="26.992970703125" />
                  <Point X="2.578034423828" Y="27.003865234375" />
                  <Point X="2.583994384766" Y="27.005669921875" />
                  <Point X="2.604423339844" Y="27.01307421875" />
                  <Point X="2.627664794922" Y="27.023580078125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.39323046875" Y="27.46504296875" />
                  <Point X="3.940402832031" Y="27.780951171875" />
                  <Point X="4.043959228516" Y="27.637033203125" />
                  <Point X="4.136884277344" Y="27.48347265625" />
                  <Point X="3.73179296875" Y="27.172634765625" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116943359" Y="26.725212890625" />
                  <Point X="3.134668457031" Y="26.7066015625" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.100111328125" Y="26.662287109375" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065634765625" Y="26.6166015625" />
                  <Point X="3.049740234375" Y="26.589373046875" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.019537109375" Y="26.5047578125" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.3525703125" />
                  <Point X="3.013403076172" Y="26.310384765625" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.024597900391" Y="26.258232421875" />
                  <Point X="3.034682373047" Y="26.228611328125" />
                  <Point X="3.050285400391" Y="26.19537109375" />
                  <Point X="3.056918212891" Y="26.1835234375" />
                  <Point X="3.080592529297" Y="26.1475390625" />
                  <Point X="3.104975830078" Y="26.110478515625" />
                  <Point X="3.111740478516" Y="26.101421875" />
                  <Point X="3.126297363281" Y="26.084173828125" />
                  <Point X="3.134089599609" Y="26.075982421875" />
                  <Point X="3.151333007812" Y="26.05989453125" />
                  <Point X="3.160039306641" Y="26.05269140625" />
                  <Point X="3.178245117188" Y="26.039369140625" />
                  <Point X="3.187744628906" Y="26.03325" />
                  <Point X="3.222052001953" Y="26.0139375" />
                  <Point X="3.25738671875" Y="25.994046875" />
                  <Point X="3.265478515625" Y="25.98998828125" />
                  <Point X="3.294680664062" Y="25.97808203125" />
                  <Point X="3.330276855469" Y="25.96801953125" />
                  <Point X="3.343671142578" Y="25.965255859375" />
                  <Point X="3.390057128906" Y="25.959125" />
                  <Point X="3.437831787109" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465709960938" Y="25.95122265625" />
                  <Point X="3.491214111328" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.219889648438" Y="26.0474921875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783590820312" Y="25.715720703125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.332436523438" Y="25.5929609375" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686026367188" Y="25.41954296875" />
                  <Point X="3.665615722656" Y="25.4121328125" />
                  <Point X="3.642376708984" Y="25.401615234375" />
                  <Point X="3.634006103516" Y="25.397314453125" />
                  <Point X="3.588433349609" Y="25.37097265625" />
                  <Point X="3.54149609375" Y="25.34384375" />
                  <Point X="3.533885742188" Y="25.338947265625" />
                  <Point X="3.508781005859" Y="25.319875" />
                  <Point X="3.481995117188" Y="25.2943515625" />
                  <Point X="3.472794921875" Y="25.284224609375" />
                  <Point X="3.445451171875" Y="25.249380859375" />
                  <Point X="3.4172890625" Y="25.21349609375" />
                  <Point X="3.410852783203" Y="25.204205078125" />
                  <Point X="3.399129638672" Y="25.18492578125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005371094" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.361261474609" Y="25.06287890625" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.360989257812" Y="24.875984375" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373159423828" Y="24.81601171875" />
                  <Point X="3.380005615234" Y="24.79451171875" />
                  <Point X="3.384069091797" Y="24.783966796875" />
                  <Point X="3.393842529297" Y="24.76250390625" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410852783203" Y="24.733234375" />
                  <Point X="3.417290771484" Y="24.72394140625" />
                  <Point X="3.444634277344" Y="24.689099609375" />
                  <Point X="3.472796630859" Y="24.653212890625" />
                  <Point X="3.478716552734" Y="24.6463671875" />
                  <Point X="3.501133544922" Y="24.62419921875" />
                  <Point X="3.530174560547" Y="24.601275390625" />
                  <Point X="3.541494628906" Y="24.593595703125" />
                  <Point X="3.587067382812" Y="24.56725390625" />
                  <Point X="3.634004638672" Y="24.540123046875" />
                  <Point X="3.639487792969" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026855469" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.351609863281" Y="24.33933984375" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="4.179517578125" Y="23.992962890625" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720214844" Y="24.089158203125" />
                  <Point X="3.354480712891" Y="24.087322265625" />
                  <Point X="3.265037353516" Y="24.067880859375" />
                  <Point X="3.172916503906" Y="24.047859375" />
                  <Point X="3.157877197266" Y="24.0432578125" />
                  <Point X="3.128757568359" Y="24.031634765625" />
                  <Point X="3.114677246094" Y="24.02461328125" />
                  <Point X="3.086850097656" Y="24.007720703125" />
                  <Point X="3.074121826172" Y="23.99846875" />
                  <Point X="3.050371337891" Y="23.97799609375" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.985286132812" Y="23.90175390625" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.867408691406" Y="23.619134765625" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.946040039062" Y="23.303634765625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.664925537109" Y="22.694017578125" />
                  <Point X="4.087171142578" Y="22.370015625" />
                  <Point X="4.045490722656" Y="22.3025703125" />
                  <Point X="4.001274414063" Y="22.23974609375" />
                  <Point X="3.5099609375" Y="22.52340625" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815025878906" Y="22.920484375" />
                  <Point X="2.783120605469" Y="22.930671875" />
                  <Point X="2.771107421875" Y="22.933662109375" />
                  <Point X="2.664655761719" Y="22.95288671875" />
                  <Point X="2.555017089844" Y="22.9726875" />
                  <Point X="2.539358886719" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444847167969" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589111328" Y="22.948890625" />
                  <Point X="2.312153808594" Y="22.90234765625" />
                  <Point X="2.221071044922" Y="22.854412109375" />
                  <Point X="2.208967285156" Y="22.846828125" />
                  <Point X="2.186038085938" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144942138672" Y="22.78883984375" />
                  <Point X="2.128047607422" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.073920654297" Y="22.665369140625" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.021412597656" Y="22.31340625" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.462901367188" Y="21.445111328125" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.963916015625" />
                  <Point X="2.338705078125" Y="21.465720703125" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808834350586" Y="22.15037109375" />
                  <Point X="1.783251098633" Y="22.17199609375" />
                  <Point X="1.773297973633" Y="22.179353515625" />
                  <Point X="1.668307861328" Y="22.2468515625" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.323755859375" />
                  <Point X="1.517466674805" Y="22.336126953125" />
                  <Point X="1.502547851562" Y="22.34111328125" />
                  <Point X="1.470927490234" Y="22.34884375" />
                  <Point X="1.455391357422" Y="22.351302734375" />
                  <Point X="1.424125" Y="22.35362109375" />
                  <Point X="1.408394897461" Y="22.35348046875" />
                  <Point X="1.2935703125" Y="22.3429140625" />
                  <Point X="1.175308105469" Y="22.332033203125" />
                  <Point X="1.161225097656" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120008544922" Y="22.318369140625" />
                  <Point X="1.092622314453" Y="22.307025390625" />
                  <Point X="1.079876708984" Y="22.3005859375" />
                  <Point X="1.055494384766" Y="22.285865234375" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.955192993164" Y="22.203861328125" />
                  <Point X="0.863874023438" Y="22.127931640625" />
                  <Point X="0.85265435791" Y="22.11691015625" />
                  <Point X="0.832184082031" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.756281616211" Y="21.872400390625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833090698242" Y="20.847662109375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143554688" Y="21.546423828125" />
                  <Point X="0.626786315918" Y="21.576185546875" />
                  <Point X="0.620406982422" Y="21.586791015625" />
                  <Point X="0.539750366211" Y="21.703" />
                  <Point X="0.456679229736" Y="21.82269140625" />
                  <Point X="0.44667098999" Y="21.834826171875" />
                  <Point X="0.424791442871" Y="21.85727734375" />
                  <Point X="0.412920349121" Y="21.86759375" />
                  <Point X="0.38666506958" Y="21.88683984375" />
                  <Point X="0.373248596191" Y="21.89505859375" />
                  <Point X="0.345243133545" Y="21.909169921875" />
                  <Point X="0.330654022217" Y="21.9150625" />
                  <Point X="0.205842575073" Y="21.953798828125" />
                  <Point X="0.077294914246" Y="21.9936953125" />
                  <Point X="0.063377120972" Y="21.996890625" />
                  <Point X="0.035218135834" Y="22.00116015625" />
                  <Point X="0.020976791382" Y="22.002234375" />
                  <Point X="-0.008664708138" Y="22.002234375" />
                  <Point X="-0.022905902863" Y="22.00116015625" />
                  <Point X="-0.051065185547" Y="21.996890625" />
                  <Point X="-0.064983123779" Y="21.9936953125" />
                  <Point X="-0.189794265747" Y="21.954958984375" />
                  <Point X="-0.318342224121" Y="21.9150625" />
                  <Point X="-0.332930908203" Y="21.909169921875" />
                  <Point X="-0.360932800293" Y="21.895060546875" />
                  <Point X="-0.374346313477" Y="21.88684375" />
                  <Point X="-0.40060144043" Y="21.867599609375" />
                  <Point X="-0.412475952148" Y="21.85728125" />
                  <Point X="-0.434359344482" Y="21.834826171875" />
                  <Point X="-0.444368041992" Y="21.822689453125" />
                  <Point X="-0.525024658203" Y="21.706478515625" />
                  <Point X="-0.60809564209" Y="21.5867890625" />
                  <Point X="-0.612468444824" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.845997375488" Y="20.753544921875" />
                  <Point X="-0.985425537109" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.77094654355" Y="28.820797576971" />
                  <Point X="1.943398889978" Y="29.40025268223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.723265117583" Y="28.738210884933" />
                  <Point X="1.573294972581" Y="29.54342864918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956301496031" Y="27.758855931926" />
                  <Point X="3.931821689612" Y="27.775996876914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.675583691617" Y="28.655624192896" />
                  <Point X="1.288871484419" Y="29.626610533703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102269327432" Y="27.54067457011" />
                  <Point X="3.841043877585" Y="27.723586599265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62790226565" Y="28.573037500858" />
                  <Point X="1.036081103154" Y="29.687642678319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.080320520902" Y="27.440069703965" />
                  <Point X="3.750266065557" Y="27.671176321617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580220839683" Y="28.49045080882" />
                  <Point X="0.793521332754" Y="29.741511272087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.001294435706" Y="27.379430778602" />
                  <Point X="3.65948825353" Y="27.618766043968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532539413717" Y="28.407864116782" />
                  <Point X="0.59876571057" Y="29.761907040917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.922268350509" Y="27.318791853239" />
                  <Point X="3.568710441503" Y="27.566355766319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.484857980865" Y="28.325277429565" />
                  <Point X="0.404008664736" Y="29.782303806597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.843242265313" Y="27.258152927877" />
                  <Point X="3.477932629475" Y="27.513945488671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.43717648805" Y="28.242690784335" />
                  <Point X="0.355493558823" Y="29.700300863542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.764216180116" Y="27.197514002514" />
                  <Point X="3.387154836208" Y="27.461535197886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.389494995235" Y="28.160104139105" />
                  <Point X="0.329327807718" Y="29.602648733776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.685190077878" Y="27.136875089084" />
                  <Point X="3.296377304481" Y="27.409124723969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.34181350242" Y="28.077517493875" />
                  <Point X="0.303162076158" Y="29.504996590325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606163963784" Y="27.076236183956" />
                  <Point X="3.205599772753" Y="27.356714250053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294132009605" Y="27.994930848644" />
                  <Point X="0.276996344598" Y="29.407344446874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52713784969" Y="27.015597278828" />
                  <Point X="3.114822241026" Y="27.304303776136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24645051679" Y="27.912344203414" />
                  <Point X="0.250830613038" Y="29.309692303423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.351534373453" Y="29.731472807717" />
                  <Point X="-0.41600470788" Y="29.776615421874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.715338349015" Y="26.067636746364" />
                  <Point X="4.661142270494" Y="26.105585249086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.448111735595" Y="26.9549583737" />
                  <Point X="3.024044709299" Y="27.251893302219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.198769023975" Y="27.829757558184" />
                  <Point X="0.218537122062" Y="29.216330863307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.313282594192" Y="29.588715037596" />
                  <Point X="-0.557913438136" Y="29.760007398604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.749373918752" Y="25.927831197934" />
                  <Point X="4.521727887525" Y="26.087230665043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369085621501" Y="26.894319468572" />
                  <Point X="2.933267177572" Y="27.199482828302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.151087531159" Y="27.747170912954" />
                  <Point X="0.160250668087" Y="29.141169891823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.275030774775" Y="29.445957239358" />
                  <Point X="-0.699820122374" Y="29.743397942698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770978259528" Y="25.796730089733" />
                  <Point X="4.382313504557" Y="26.068876081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.290059507407" Y="26.833680563444" />
                  <Point X="2.842489645844" Y="27.147072354385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103406038344" Y="27.664584267724" />
                  <Point X="0.063446825728" Y="29.092979086038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.236778955358" Y="29.303199441119" />
                  <Point X="-0.841726770424" Y="29.726788461452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74028830451" Y="25.702245841651" />
                  <Point X="4.242899121588" Y="26.050521496957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.211033393313" Y="26.773041658316" />
                  <Point X="2.751712114117" Y="27.094661880468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056822593035" Y="27.581228761353" />
                  <Point X="-0.974894583512" Y="29.704059982091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.620500685295" Y="25.670148449677" />
                  <Point X="4.10348430713" Y="26.032167215046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.136897174733" Y="26.708978811487" />
                  <Point X="2.66093458239" Y="27.042251406552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.026779941471" Y="27.486291266513" />
                  <Point X="-1.093166699435" Y="29.670901423288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.500713066081" Y="25.638051057703" />
                  <Point X="3.96406940738" Y="26.013812992857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.078401049198" Y="26.63396465361" />
                  <Point X="2.557841190848" Y="26.998464590516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012909685777" Y="27.380029738175" />
                  <Point X="-1.211438815358" Y="29.637742864485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.380925446866" Y="25.605953665729" />
                  <Point X="3.82465450763" Y="25.995458770669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.03275479658" Y="26.549952917852" />
                  <Point X="2.425194333357" Y="26.975371334119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03548423664" Y="27.248249281557" />
                  <Point X="-1.329710762994" Y="29.604584187846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.261137677691" Y="25.573856378758" />
                  <Point X="3.685239607881" Y="25.97710454848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005930987962" Y="26.452761564918" />
                  <Point X="-1.447982161941" Y="29.571425127011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.141349806532" Y="25.541759163197" />
                  <Point X="3.545824708131" Y="25.958750326291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.008297178589" Y="26.335131154472" />
                  <Point X="-1.466896061398" Y="29.468695196055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021561935373" Y="25.509661947636" />
                  <Point X="3.377243481077" Y="25.960818586342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055454769601" Y="26.186137467829" />
                  <Point X="-1.469127689725" Y="29.3542842131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.901774064215" Y="25.477564732076" />
                  <Point X="-1.498165447348" Y="29.258643083948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.781986193056" Y="25.445467516515" />
                  <Point X="-1.538743964028" Y="29.171082881285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.664613457743" Y="25.411679204629" />
                  <Point X="-1.609990494115" Y="29.10499665279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.571280067859" Y="25.361058361859" />
                  <Point X="-1.710489327455" Y="29.059393107544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.490011207345" Y="25.301989844681" />
                  <Point X="-1.832457266983" Y="29.028822392289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429161233342" Y="25.228623869246" />
                  <Point X="-2.274803909247" Y="29.222583259772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.776710834746" Y="24.169085894299" />
                  <Point X="4.646349898867" Y="24.260365604289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381262225567" Y="25.14618952963" />
                  <Point X="-2.367154836446" Y="29.171274489225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.760577696876" Y="24.064408853118" />
                  <Point X="4.378055319801" Y="24.33225390508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358110702709" Y="25.046426814524" />
                  <Point X="-2.459506361391" Y="29.119966137224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.737758638154" Y="23.964413344117" />
                  <Point X="4.109758580219" Y="24.404143718679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.3504247361" Y="24.93583500035" />
                  <Point X="-2.551857886336" Y="29.068657785223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.612827264118" Y="23.935917648044" />
                  <Point X="3.841461604392" Y="24.4760336977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.378105404501" Y="24.80047920174" />
                  <Point X="-2.644209411281" Y="29.017349433223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.4088486187" Y="23.962771447267" />
                  <Point X="-2.723870938074" Y="28.957155448856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204869973281" Y="23.989625246491" />
                  <Point X="-2.802931452449" Y="28.896540631064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.000890471917" Y="24.016479645054" />
                  <Point X="-2.881991966824" Y="28.835925813272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.796910849068" Y="24.043334128682" />
                  <Point X="-2.961052180889" Y="28.7753107852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592931226219" Y="24.070188612309" />
                  <Point X="-3.040111791437" Y="28.714695334541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.39663042502" Y="24.091666327134" />
                  <Point X="-2.97439580272" Y="28.552706917929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.264987593082" Y="24.067870044475" />
                  <Point X="-2.862000989851" Y="28.35803363677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.142994073344" Y="24.037317240676" />
                  <Point X="-2.765631835081" Y="28.174581642217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055622085231" Y="23.982522179449" />
                  <Point X="-2.749023094013" Y="28.046978490588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.992666660166" Y="23.910630456719" />
                  <Point X="-2.763142738" Y="27.940891585813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931720721762" Y="23.837331676279" />
                  <Point X="-2.812848046282" Y="27.859722031428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.886886349077" Y="23.752751456072" />
                  <Point X="-2.880814695126" Y="27.791339205363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870112965327" Y="23.648522719883" />
                  <Point X="-2.968829687331" Y="27.736994380448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860087174807" Y="23.53956926805" />
                  <Point X="-3.12198431602" Y="27.728260820036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.883802687391" Y="23.406989901433" />
                  <Point X="-3.72270820439" Y="28.032918629123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019325973367" Y="23.196121889057" />
                  <Point X="-3.784520628503" Y="27.960226568509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070961173434" Y="22.343785408592" />
                  <Point X="-3.842928895642" Y="27.885150891522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018653069829" Y="22.264438351113" />
                  <Point X="-3.901337182454" Y="27.810075228309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.357335040675" Y="22.611524634348" />
                  <Point X="-3.959745469266" Y="27.734999565096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717933090897" Y="22.943265113596" />
                  <Point X="-4.015342379212" Y="27.657955354609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.507601692577" Y="22.97456715829" />
                  <Point X="-4.063395532426" Y="27.575628948792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.388060764517" Y="22.94229703131" />
                  <Point X="-4.111448685641" Y="27.493302542975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293504223503" Y="22.892532648182" />
                  <Point X="-4.159501838855" Y="27.410976137159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.201259449534" Y="22.841149548344" />
                  <Point X="-4.207554375518" Y="27.328649299628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133165952198" Y="22.772855542549" />
                  <Point X="-3.490222445444" Y="26.710394488859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086610316443" Y="22.689480563718" />
                  <Point X="-3.422494391385" Y="26.546997208926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042010032818" Y="22.604736432586" />
                  <Point X="-3.435562258245" Y="26.440173841878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006198648232" Y="22.513838248095" />
                  <Point X="-3.47392098014" Y="26.351059322172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006154255662" Y="22.397895746175" />
                  <Point X="-3.534126895998" Y="26.277242372369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030130571355" Y="22.265133763256" />
                  <Point X="-3.630308586822" Y="26.228615931389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.074255861589" Y="22.118263316476" />
                  <Point X="-3.761760548485" Y="26.204685999926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.186650933845" Y="21.923589853692" />
                  <Point X="-3.963581761908" Y="26.230029149003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299046006102" Y="21.728916390908" />
                  <Point X="1.948291857925" Y="21.97451708952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.407229253634" Y="22.353373203688" />
                  <Point X="-4.167561455381" Y="26.256883682081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.411441078358" Y="21.534242928124" />
                  <Point X="2.140613864167" Y="21.723878185053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.260839756" Y="22.339902647514" />
                  <Point X="-4.371540830779" Y="26.283737992442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523835876476" Y="21.339569657294" />
                  <Point X="2.332935870409" Y="21.473239280586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.124062475953" Y="22.319701544126" />
                  <Point X="-4.575519590961" Y="26.310591872024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.636230443079" Y="21.144896548573" />
                  <Point X="2.525258173323" Y="21.222600168388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.03230448123" Y="22.267977597789" />
                  <Point X="-4.652481291444" Y="26.248507448923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726434038108" Y="20.965761725427" />
                  <Point X="2.717580485411" Y="20.971961049766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.956588013995" Y="22.205021252982" />
                  <Point X="-4.678896005155" Y="26.151029644651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.880871681149" Y="22.142064814074" />
                  <Point X="-3.319696128221" Y="25.083334058956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587466946463" Y="25.270829204402" />
                  <Point X="-4.705310689377" Y="26.053551819731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.817065272885" Y="22.070768956194" />
                  <Point X="-3.294917496852" Y="24.950010288553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855763963163" Y="25.342719212041" />
                  <Point X="-4.731563386156" Y="25.955960569981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779854079995" Y="21.980850928029" />
                  <Point X="-3.320538888503" Y="24.851976994193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124060979862" Y="25.414609219681" />
                  <Point X="-4.74711338175" Y="25.850875208183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.757976140722" Y="21.880196440097" />
                  <Point X="-3.371818215202" Y="24.77190957937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.392357531769" Y="25.486498901869" />
                  <Point X="-4.762663377344" Y="25.745789846384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736098343671" Y="21.779541852578" />
                  <Point X="-3.452556154225" Y="24.712469306961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.660653598322" Y="25.55838824421" />
                  <Point X="-4.778213426484" Y="25.640704522079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725108566007" Y="21.671263391809" />
                  <Point X="-3.56231499842" Y="24.67334969112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741435202331" Y="21.54385777205" />
                  <Point X="0.563948973112" Y="21.668134967677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.116280992989" Y="21.981595461974" />
                  <Point X="-3.682102996609" Y="24.641252564506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758253742315" Y="21.416107717639" />
                  <Point X="0.662882993186" Y="21.482887035104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.066122024421" Y="21.993341843825" />
                  <Point X="-3.801890994798" Y="24.609155437893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7750722823" Y="21.288357663227" />
                  <Point X="0.701134750726" Y="21.340129280192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.180882923403" Y="21.957724704451" />
                  <Point X="-3.921678992987" Y="24.577058311279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791890822284" Y="21.160607608816" />
                  <Point X="0.739386508266" Y="21.19737152528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.29564349675" Y="21.922107337066" />
                  <Point X="-4.041466991175" Y="24.544961184666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808709362269" Y="21.032857554405" />
                  <Point X="0.777638265807" Y="21.054613770368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.39220993952" Y="21.873750302299" />
                  <Point X="-2.949808374299" Y="23.664600006045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279163163209" Y="23.895216711985" />
                  <Point X="-4.161254989364" Y="24.512864058052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825527902254" Y="20.905107499994" />
                  <Point X="0.815890023347" Y="20.911856015456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.457628665434" Y="21.803583401392" />
                  <Point X="-2.961729071149" Y="23.556973381908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.418577950334" Y="23.876862410935" />
                  <Point X="-4.281042905891" Y="24.480766874259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.511796149483" Y="21.725538296116" />
                  <Point X="-3.005571062071" Y="23.471698288509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557992737459" Y="23.858508109886" />
                  <Point X="-4.400830670132" Y="24.448669583833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.565963663871" Y="21.647493212085" />
                  <Point X="-3.075784912337" Y="23.40488896982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697407524584" Y="23.840153808836" />
                  <Point X="-4.520618434373" Y="24.416572293407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.618633815108" Y="21.568399663087" />
                  <Point X="-3.154811037372" Y="23.344250072352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836822311709" Y="23.821799507787" />
                  <Point X="-4.640406198613" Y="24.384475002981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.652398162317" Y="21.476068127593" />
                  <Point X="-3.233837162407" Y="23.283611174885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.976237098834" Y="23.803445206737" />
                  <Point X="-4.760193962854" Y="24.352377712555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.67856392871" Y="21.378416008533" />
                  <Point X="-2.392733292746" Y="22.578690318999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.457414411477" Y="22.623980525914" />
                  <Point X="-3.312863287442" Y="23.222972277418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115652041602" Y="23.78509101467" />
                  <Point X="-4.7720599549" Y="24.244712783702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.704729695104" Y="21.280763889472" />
                  <Point X="-2.310834552354" Y="22.405370617674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.643850095367" Y="22.638550611233" />
                  <Point X="-3.391889412477" Y="23.16233337995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255067306173" Y="23.766737047932" />
                  <Point X="-4.753627345742" Y="24.115832545889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.730895461497" Y="21.183111770411" />
                  <Point X="-2.329996072248" Y="22.302814072415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.740303791007" Y="22.590114630076" />
                  <Point X="-3.470915537512" Y="23.101694482483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.394482570745" Y="23.748383081195" />
                  <Point X="-4.729864822063" Y="23.983220261749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.757061227891" Y="21.085459651351" />
                  <Point X="-2.374788249342" Y="22.218204306537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.831081354945" Y="22.537704178713" />
                  <Point X="-3.549941662547" Y="23.041055585016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.533897835317" Y="23.730029114457" />
                  <Point X="-4.693789191239" Y="23.841986247168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.783226994284" Y="20.98780753229" />
                  <Point X="-2.422469596289" Y="22.135617559169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.921858918883" Y="22.48529372735" />
                  <Point X="-3.628967787582" Y="22.980416687549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.809392760677" Y="20.89015541323" />
                  <Point X="-2.470150943237" Y="22.053030811801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012636482821" Y="22.432883275988" />
                  <Point X="-3.707993940561" Y="22.919777809648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835558527071" Y="20.792503294169" />
                  <Point X="-2.517832290184" Y="21.970444064433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103414046759" Y="22.380472824625" />
                  <Point X="-3.787020186822" Y="22.859138997063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.861724349887" Y="20.694851214616" />
                  <Point X="-1.454531913425" Y="21.109939539313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.556988156216" Y="21.181680172851" />
                  <Point X="-2.565513637131" Y="21.887857317065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.194191610697" Y="22.328062373262" />
                  <Point X="-3.866046433082" Y="22.798500184479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.887890210154" Y="20.597199161287" />
                  <Point X="-1.183362476543" Y="20.804091069544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.766376662524" Y="21.212321997451" />
                  <Point X="-2.613194984078" Y="21.805270569697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.284969174634" Y="22.275651921899" />
                  <Point X="-3.945072679343" Y="22.737861371895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.914056070422" Y="20.499547107957" />
                  <Point X="-1.156561357002" Y="20.669351137676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.948799008876" Y="21.224081913572" />
                  <Point X="-2.660876331026" Y="21.722683822329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.37574669865" Y="22.223241442583" />
                  <Point X="-4.024098925603" Y="22.67722255931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.940221930689" Y="20.401895054628" />
                  <Point X="-1.129759845515" Y="20.534610931365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.073583103097" Y="21.195483091062" />
                  <Point X="-2.70855767872" Y="21.640097075485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.466524158614" Y="22.170830918417" />
                  <Point X="-4.103125171863" Y="22.616583746726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.966387790956" Y="20.304243001298" />
                  <Point X="-1.1221994399" Y="20.413343492429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.160753109262" Y="21.140546600552" />
                  <Point X="-2.756239130201" Y="21.557510401312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557301618579" Y="22.118420394251" />
                  <Point X="-4.179933367704" Y="22.554391838517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.047850612706" Y="20.245310297239" />
                  <Point X="-1.136179155732" Y="20.307158608905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.245505377538" Y="21.083917191747" />
                  <Point X="-2.803920581682" Y="21.47492372714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.648079078543" Y="22.066009870086" />
                  <Point X="-4.061186345662" Y="22.355270692612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330257378549" Y="21.027287595801" />
                  <Point X="-2.851602033163" Y="21.392337052967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738856538508" Y="22.01359934592" />
                  <Point X="-3.906001978988" Y="22.130635843321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405543344539" Y="20.964029810776" />
                  <Point X="-2.899283484644" Y="21.309750378795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.46381086897" Y="20.888855584683" />
                  <Point X="-2.946964936125" Y="21.227163704622" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.668875854492" Y="20.726416015625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318969727" Y="21.478455078125" />
                  <Point X="0.383662353516" Y="21.5946640625" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.149524475098" Y="21.772337890625" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.133476028442" Y="21.773498046875" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.368935638428" Y="21.598146484375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.662471557617" Y="20.704369140625" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.949641113281" Y="20.032701171875" />
                  <Point X="-1.10023059082" Y="20.061931640625" />
                  <Point X="-1.244455200195" Y="20.0990390625" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.332905029297" Y="20.268525390625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.339500610352" Y="20.615142578125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.501192871094" Y="20.89814453125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.801752563477" Y="21.024232421875" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.116959472656" Y="20.941296875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.373840087891" Y="20.693998046875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.635079345703" Y="20.695703125" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.055521728516" Y="20.986140625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.87276171875" Y="21.7356875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.40241015625" />
                  <Point X="-2.513982666016" Y="22.43123828125" />
                  <Point X="-2.531327392578" Y="22.44858203125" />
                  <Point X="-2.560156738281" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.245891357422" Y="22.078822265625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.987295166016" Y="21.923732421875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.304863769531" Y="22.392927734375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.805441894531" Y="23.084494140625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161158935547" Y="23.689359375" />
                  <Point X="-3.187641601562" Y="23.7049453125" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.045404296875" Y="23.60269921875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.859180664062" Y="23.721759765625" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.96526953125" Y="24.25363671875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.288405761719" Y="24.675498046875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.528223144531" Y="24.8880625" />
                  <Point X="-3.502323974609" Y="24.909353515625" />
                  <Point X="-3.490341552734" Y="24.938775390625" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.490204589844" Y="24.99822265625" />
                  <Point X="-3.502323730469" Y="25.028083984375" />
                  <Point X="-3.527813964844" Y="25.049091796875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.310288085937" Y="25.2678046875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.961606933594" Y="25.699330078125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.841400390625" Y="26.277783203125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.277223144531" Y="26.4629609375" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731705322266" Y="26.3958671875" />
                  <Point X="-3.7014453125" Y="26.405408203125" />
                  <Point X="-3.670279296875" Y="26.415234375" />
                  <Point X="-3.651534667969" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.443787109375" />
                  <Point X="-3.626977783203" Y="26.473099609375" />
                  <Point X="-3.614472412109" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.61631640625" Y="26.54551171875" />
                  <Point X="-3.630967041016" Y="26.57365625" />
                  <Point X="-3.646056152344" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.091790039063" Y="26.9505703125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.330833496094" Y="27.4943515625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.958045166016" Y="28.046607421875" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.480956787109" Y="28.112734375" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514160156" Y="27.92043359375" />
                  <Point X="-3.096370361328" Y="27.91674609375" />
                  <Point X="-3.05296484375" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.983338867188" Y="27.957318359375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941761474609" Y="28.069984375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.143420898438" Y="28.465466796875" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.050388427734" Y="28.946232421875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.434781005859" Y="29.35105859375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.066339355469" Y="29.41595703125" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247802734" Y="29.273662109375" />
                  <Point X="-1.904341918945" Y="29.249244140625" />
                  <Point X="-1.856031738281" Y="29.22409375" />
                  <Point X="-1.83512512207" Y="29.218490234375" />
                  <Point X="-1.813809448242" Y="29.22225" />
                  <Point X="-1.764953735352" Y="29.24248828125" />
                  <Point X="-1.714635498047" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.670181762695" Y="29.344921875" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.673672363281" Y="29.583669921875" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.35324230957" Y="29.7953125" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.582467041016" Y="29.9484296875" />
                  <Point X="-0.224200042725" Y="29.990359375" />
                  <Point X="-0.137337646484" Y="29.66618359375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282115936" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.152499572754" Y="29.6768203125" />
                  <Point X="0.236648391724" Y="29.9908671875" />
                  <Point X="0.52391418457" Y="29.960783203125" />
                  <Point X="0.860209960938" Y="29.925564453125" />
                  <Point X="1.179245605469" Y="29.8485390625" />
                  <Point X="1.508455810547" Y="29.769056640625" />
                  <Point X="1.716340820313" Y="29.69365625" />
                  <Point X="1.931043701172" Y="29.61578125" />
                  <Point X="2.131835205078" Y="29.52187890625" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.532683349609" Y="29.3121171875" />
                  <Point X="2.732533935547" Y="29.195685546875" />
                  <Point X="2.915474609375" Y="29.065587890625" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.654313720703" Y="28.238783203125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.214275634766" Y="27.451962890625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.206168701172" Y="27.358125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.239844238281" Y="27.269625" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.306126220703" Y="27.20304296875" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360336181641" Y="27.172978515625" />
                  <Point X="2.394536621094" Y="27.16885546875" />
                  <Point X="2.429760986328" Y="27.164607421875" />
                  <Point X="2.448666015625" Y="27.1659453125" />
                  <Point X="2.488217285156" Y="27.176521484375" />
                  <Point X="2.528952392578" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.29823046875" Y="27.6295859375" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.084053222656" Y="27.90662109375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.304584472656" Y="27.5733359375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.847457763672" Y="27.021896484375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.25090625" Y="26.546697265625" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.202516113281" Y="26.4535859375" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.199483642578" Y="26.348779296875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.239321044922" Y="26.25196875" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.315255126953" Y="26.1795078125" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.368567138672" Y="26.153619140625" />
                  <Point X="3.414953125" Y="26.14748828125" />
                  <Point X="3.462727783203" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.19508984375" Y="26.2358671875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.888276855469" Y="26.160513671875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.971329101563" Y="25.744951171875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.381612304687" Y="25.40943359375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088134766" Y="25.232818359375" />
                  <Point X="3.683515380859" Y="25.2064765625" />
                  <Point X="3.636578125" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.594921386719" Y="25.132083984375" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.547870361328" Y="25.027140625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.54759765625" Y="24.91172265625" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758544922" Y="24.8412421875" />
                  <Point X="3.594102050781" Y="24.806400390625" />
                  <Point X="3.622264404297" Y="24.770513671875" />
                  <Point X="3.636576660156" Y="24.758091796875" />
                  <Point X="3.682149414062" Y="24.73175" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.400785644531" Y="24.5228671875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.976858398438" Y="24.2221484375" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.90725390625" Y="23.853154296875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.154717773438" Y="23.804587890625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836669922" Y="23.901658203125" />
                  <Point X="3.305393310547" Y="23.882216796875" />
                  <Point X="3.213272460938" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.131382324219" Y="23.78028125" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.056609375" Y="23.601724609375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.105859863281" Y="23.406384765625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.780590087891" Y="22.844755859375" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.284264160156" Y="22.3275234375" />
                  <Point X="4.204130859375" Y="22.19785546875" />
                  <Point X="4.118974121094" Y="22.076859375" />
                  <Point X="4.056688476562" Y="21.988361328125" />
                  <Point X="3.4149609375" Y="22.35886328125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.630889160156" Y="22.76591015625" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.400642578125" Y="22.7342109375" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.242056884766" Y="22.576880859375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.208388183594" Y="22.347171875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.627446289062" Y="21.540111328125" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.9303828125" Y="20.877705078125" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.740086425781" Y="20.748158203125" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.187967773438" Y="21.350056640625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.565559082031" Y="22.08703125" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425805541992" Y="22.16428125" />
                  <Point X="1.310980957031" Y="22.15371484375" />
                  <Point X="1.19271875" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.07666796875" Y="22.057767578125" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.941946533203" Y="21.832044921875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.025403076172" Y="20.842552734375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.084302246094" Y="20.056470703125" />
                  <Point X="0.994363586426" Y="20.036755859375" />
                  <Point X="0.90638873291" Y="20.0207734375" />
                  <Point X="0.860200683594" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#166" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.095197293765" Y="4.710405628776" Z="1.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="-0.594435095944" Y="5.029369539894" Z="1.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.25" />
                  <Point X="-1.372896139994" Y="4.874737952206" Z="1.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.25" />
                  <Point X="-1.729400608057" Y="4.608424193418" Z="1.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.25" />
                  <Point X="-1.724023547081" Y="4.391237405672" Z="1.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.25" />
                  <Point X="-1.790243407028" Y="4.319961418715" Z="1.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.25" />
                  <Point X="-1.887409251817" Y="4.324873457334" Z="1.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.25" />
                  <Point X="-2.032827859162" Y="4.477675574947" Z="1.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.25" />
                  <Point X="-2.465219988803" Y="4.426045725093" Z="1.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.25" />
                  <Point X="-3.086966549432" Y="4.017191706924" Z="1.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.25" />
                  <Point X="-3.192878041622" Y="3.471746623644" Z="1.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.25" />
                  <Point X="-2.997727157535" Y="3.096907391148" Z="1.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.25" />
                  <Point X="-3.024849559858" Y="3.023954008028" Z="1.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.25" />
                  <Point X="-3.098169074459" Y="2.997837541638" Z="1.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.25" />
                  <Point X="-3.462112724653" Y="3.187315907124" Z="1.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.25" />
                  <Point X="-4.003664738454" Y="3.108591781226" Z="1.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.25" />
                  <Point X="-4.380171810196" Y="2.550837168573" Z="1.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.25" />
                  <Point X="-4.128384179555" Y="1.942182674226" Z="1.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.25" />
                  <Point X="-3.68147305988" Y="1.581847976161" Z="1.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.25" />
                  <Point X="-3.679327921298" Y="1.523513460724" Z="1.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.25" />
                  <Point X="-3.722635946158" Y="1.484373505948" Z="1.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.25" />
                  <Point X="-4.276852988936" Y="1.543812792576" Z="1.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.25" />
                  <Point X="-4.895816148582" Y="1.32214210658" Z="1.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.25" />
                  <Point X="-5.01722411104" Y="0.737928594605" Z="1.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.25" />
                  <Point X="-4.32938519961" Y="0.250787742322" Z="1.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.25" />
                  <Point X="-3.56247964762" Y="0.039295952821" Z="1.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.25" />
                  <Point X="-3.544114082748" Y="0.014683678824" Z="1.25" />
                  <Point X="-3.539556741714" Y="0" Z="1.25" />
                  <Point X="-3.544250518387" Y="-0.015123272237" Z="1.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.25" />
                  <Point X="-3.562888947503" Y="-0.039580030313" Z="1.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.25" />
                  <Point X="-4.307502589162" Y="-0.244924314434" Z="1.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.25" />
                  <Point X="-5.020921683795" Y="-0.722161210333" Z="1.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.25" />
                  <Point X="-4.913781440533" Y="-1.259334555074" Z="1.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.25" />
                  <Point X="-4.045034107071" Y="-1.4155918224" Z="1.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.25" />
                  <Point X="-3.205722314035" Y="-1.3147714906" Z="1.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.25" />
                  <Point X="-3.196585366983" Y="-1.337542844137" Z="1.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.25" />
                  <Point X="-3.842035909416" Y="-1.84455643025" Z="1.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.25" />
                  <Point X="-4.353963130338" Y="-2.601401124402" Z="1.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.25" />
                  <Point X="-4.033237244485" Y="-3.075269401662" Z="1.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.25" />
                  <Point X="-3.22704723309" Y="-2.933197977026" Z="1.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.25" />
                  <Point X="-2.564037306376" Y="-2.564293162756" Z="1.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.25" />
                  <Point X="-2.922219036142" Y="-3.208031149402" Z="1.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.25" />
                  <Point X="-3.092181485416" Y="-4.022195273636" Z="1.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.25" />
                  <Point X="-2.667556804991" Y="-4.315527882938" Z="1.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.25" />
                  <Point X="-2.340328396464" Y="-4.305158117465" Z="1.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.25" />
                  <Point X="-2.095336891191" Y="-4.068997231673" Z="1.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.25" />
                  <Point X="-1.811178509959" Y="-3.99437978136" Z="1.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.25" />
                  <Point X="-1.540316346542" Y="-4.10816668115" Z="1.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.25" />
                  <Point X="-1.394695927337" Y="-4.363330218285" Z="1.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.25" />
                  <Point X="-1.388633214414" Y="-4.693666855076" Z="1.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.25" />
                  <Point X="-1.263069970698" Y="-4.918104428187" Z="1.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.25" />
                  <Point X="-0.96532472008" Y="-4.985102276866" Z="1.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="-0.620331320056" Y="-4.277292082717" Z="1.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="-0.334015600283" Y="-3.399083346348" Z="1.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="-0.124811267712" Y="-3.242976179971" Z="1.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.25" />
                  <Point X="0.128547811649" Y="-3.244135865317" Z="1.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.25" />
                  <Point X="0.336430259149" Y="-3.40256240942" Z="1.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.25" />
                  <Point X="0.614423540345" Y="-4.255243947098" Z="1.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.25" />
                  <Point X="0.909168963426" Y="-4.997140579883" Z="1.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.25" />
                  <Point X="1.088851829843" Y="-4.961088935014" Z="1.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.25" />
                  <Point X="1.068819501868" Y="-4.119640546499" Z="1.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.25" />
                  <Point X="0.984649726484" Y="-3.147294140346" Z="1.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.25" />
                  <Point X="1.102479085169" Y="-2.949397202119" Z="1.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.25" />
                  <Point X="1.30940588706" Y="-2.864792850515" Z="1.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.25" />
                  <Point X="1.532363750757" Y="-2.923746306385" Z="1.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.25" />
                  <Point X="2.142144190927" Y="-3.649100654432" Z="1.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.25" />
                  <Point X="2.761099729628" Y="-4.262535483382" Z="1.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.25" />
                  <Point X="2.953288541354" Y="-4.131702752042" Z="1.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.25" />
                  <Point X="2.664591639682" Y="-3.403608737561" Z="1.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.25" />
                  <Point X="2.251436228292" Y="-2.61265994904" Z="1.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.25" />
                  <Point X="2.280147344539" Y="-2.415125422069" Z="1.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.25" />
                  <Point X="2.417772956303" Y="-2.278753965428" Z="1.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.25" />
                  <Point X="2.615846854472" Y="-2.252011781199" Z="1.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.25" />
                  <Point X="3.383804859546" Y="-2.653157860593" Z="1.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.25" />
                  <Point X="4.153706168953" Y="-2.920636719824" Z="1.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.25" />
                  <Point X="4.320641452052" Y="-2.667480256054" Z="1.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.25" />
                  <Point X="3.80487217343" Y="-2.084296461447" Z="1.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.25" />
                  <Point X="3.141761722815" Y="-1.53529527576" Z="1.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.25" />
                  <Point X="3.100242967148" Y="-1.371576923461" Z="1.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.25" />
                  <Point X="3.163672746669" Y="-1.220404926234" Z="1.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.25" />
                  <Point X="3.309856551368" Y="-1.135361297611" Z="1.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.25" />
                  <Point X="4.142035457148" Y="-1.213703413492" Z="1.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.25" />
                  <Point X="4.949845164377" Y="-1.126689966942" Z="1.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.25" />
                  <Point X="5.020143950506" Y="-0.754024857348" Z="1.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.25" />
                  <Point X="4.407570579788" Y="-0.397554707055" Z="1.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.25" />
                  <Point X="3.701015537774" Y="-0.193680115979" Z="1.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.25" />
                  <Point X="3.627280417559" Y="-0.131452822812" Z="1.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.25" />
                  <Point X="3.590549291332" Y="-0.047592858761" Z="1.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.25" />
                  <Point X="3.590822159093" Y="0.049017672451" Z="1.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.25" />
                  <Point X="3.628099020842" Y="0.132495915763" Z="1.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.25" />
                  <Point X="3.702379876578" Y="0.194468728263" Z="1.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.25" />
                  <Point X="4.388397237518" Y="0.392417225714" Z="1.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.25" />
                  <Point X="5.014578037466" Y="0.783922203594" Z="1.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.25" />
                  <Point X="4.930701985644" Y="1.203621096212" Z="1.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.25" />
                  <Point X="4.182407306272" Y="1.316719896459" Z="1.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.25" />
                  <Point X="3.415346674088" Y="1.228338097034" Z="1.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.25" />
                  <Point X="3.333626044311" Y="1.254358884352" Z="1.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.25" />
                  <Point X="3.27493539712" Y="1.31073235467" Z="1.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.25" />
                  <Point X="3.242296223549" Y="1.39016426148" Z="1.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.25" />
                  <Point X="3.244512727434" Y="1.471398990648" Z="1.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.25" />
                  <Point X="3.284433158676" Y="1.547560264744" Z="1.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.25" />
                  <Point X="3.87173984237" Y="2.013509391663" Z="1.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.25" />
                  <Point X="4.341205815808" Y="2.630502718328" Z="1.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.25" />
                  <Point X="4.118482871714" Y="2.967104773675" Z="1.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.25" />
                  <Point X="3.267074387473" Y="2.704166327154" Z="1.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.25" />
                  <Point X="2.469142756356" Y="2.25610565437" Z="1.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.25" />
                  <Point X="2.394367282362" Y="2.249776581807" Z="1.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.25" />
                  <Point X="2.328045591192" Y="2.275696069408" Z="1.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.25" />
                  <Point X="2.275062530827" Y="2.328979269191" Z="1.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.25" />
                  <Point X="2.249653120951" Y="2.39539116147" Z="1.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.25" />
                  <Point X="2.256422288287" Y="2.470326819526" Z="1.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.25" />
                  <Point X="2.691459025705" Y="3.245064694482" Z="1.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.25" />
                  <Point X="2.938296009482" Y="4.137613752051" Z="1.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.25" />
                  <Point X="2.551697377451" Y="4.386601417652" Z="1.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.25" />
                  <Point X="2.146860867946" Y="4.598449812144" Z="1.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.25" />
                  <Point X="1.727233542421" Y="4.771940572235" Z="1.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.25" />
                  <Point X="1.184824200791" Y="4.928423134707" Z="1.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.25" />
                  <Point X="0.522966084445" Y="5.041792382826" Z="1.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.25" />
                  <Point X="0.098047267788" Y="4.721041877175" Z="1.25" />
                  <Point X="0" Y="4.355124473572" Z="1.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>