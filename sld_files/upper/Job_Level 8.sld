<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#131" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="739" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.618700378418" Y="21.2807265625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362426758" Y="21.532625" />
                  <Point X="0.519719726562" Y="21.565248046875" />
                  <Point X="0.378634765625" Y="21.768525390625" />
                  <Point X="0.35674887085" Y="21.790982421875" />
                  <Point X="0.330493835449" Y="21.810224609375" />
                  <Point X="0.302491699219" Y="21.82433203125" />
                  <Point X="0.267453308105" Y="21.835205078125" />
                  <Point X="0.049132545471" Y="21.90296484375" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658312798" Y="21.907234375" />
                  <Point X="-0.036824996948" Y="21.90296484375" />
                  <Point X="-0.071863235474" Y="21.89208984375" />
                  <Point X="-0.290184143066" Y="21.82433203125" />
                  <Point X="-0.318184906006" Y="21.81022265625" />
                  <Point X="-0.344440093994" Y="21.790978515625" />
                  <Point X="-0.366324188232" Y="21.7685234375" />
                  <Point X="-0.388967071533" Y="21.7358984375" />
                  <Point X="-0.530052001953" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.896172912598" Y="20.199236328125" />
                  <Point X="-0.916584716797" Y="20.12305859375" />
                  <Point X="-1.079324707031" Y="20.154646484375" />
                  <Point X="-1.116008544922" Y="20.1640859375" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.221631713867" Y="20.38590625" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.224879394531" Y="20.525857421875" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323646240234" Y="20.868796875" />
                  <Point X="-1.355905151367" Y="20.8970859375" />
                  <Point X="-1.556907104492" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643026611328" Y="21.109033203125" />
                  <Point X="-1.685841186523" Y="21.11183984375" />
                  <Point X="-1.952615722656" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657958984" Y="21.10519921875" />
                  <Point X="-2.078333496094" Y="21.081361328125" />
                  <Point X="-2.300624267578" Y="20.93283203125" />
                  <Point X="-2.312788574219" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.480148925781" Y="20.711509765625" />
                  <Point X="-2.801706542969" Y="20.910611328125" />
                  <Point X="-2.852483642578" Y="20.94970703125" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-2.526713134766" Y="22.1450625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858154297" Y="22.3523515625" />
                  <Point X="-2.406587646484" Y="22.3838828125" />
                  <Point X="-2.405779296875" Y="22.41626953125" />
                  <Point X="-2.415901855469" Y="22.447046875" />
                  <Point X="-2.432523193359" Y="22.478275390625" />
                  <Point X="-2.4492109375" Y="22.500818359375" />
                  <Point X="-2.464155029297" Y="22.51576171875" />
                  <Point X="-2.489311279297" Y="22.533787109375" />
                  <Point X="-2.518140136719" Y="22.54800390625" />
                  <Point X="-2.547758300781" Y="22.55698828125" />
                  <Point X="-2.578692382812" Y="22.555974609375" />
                  <Point X="-2.61021875" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-3.750265380859" Y="21.897318359375" />
                  <Point X="-3.818023681641" Y="21.858197265625" />
                  <Point X="-4.082862304687" Y="22.206142578125" />
                  <Point X="-4.119259277344" Y="22.267173828125" />
                  <Point X="-4.306143066406" Y="22.580548828125" />
                  <Point X="-3.286087158203" Y="23.363265625" />
                  <Point X="-3.105954833984" Y="23.501484375" />
                  <Point X="-3.083063232422" Y="23.524119140625" />
                  <Point X="-3.064495849609" Y="23.553220703125" />
                  <Point X="-3.056718261719" Y="23.57240234375" />
                  <Point X="-3.052791259766" Y="23.584279296875" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037353516" Y="23.64139453125" />
                  <Point X="-3.042736083984" Y="23.66426171875" />
                  <Point X="-3.048881103516" Y="23.686296875" />
                  <Point X="-3.060883300781" Y="23.71527734375" />
                  <Point X="-3.073079589844" Y="23.7364921875" />
                  <Point X="-3.090290039062" Y="23.753884765625" />
                  <Point X="-3.106459472656" Y="23.766474609375" />
                  <Point X="-3.116640136719" Y="23.773392578125" />
                  <Point X="-3.139460205078" Y="23.786822265625" />
                  <Point X="-3.168722167969" Y="23.79804296875" />
                  <Point X="-3.200607666016" Y="23.8045234375" />
                  <Point X="-3.231928710938" Y="23.805615234375" />
                  <Point X="-4.634563964844" Y="23.620955078125" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.843707519531" Y="24.07467578125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.738073730469" Y="24.724607421875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.514344482422" Y="24.786703125" />
                  <Point X="-3.494938964844" Y="24.796587890625" />
                  <Point X="-3.483892333984" Y="24.803193359375" />
                  <Point X="-3.459977539063" Y="24.819791015625" />
                  <Point X="-3.436021728516" Y="24.841318359375" />
                  <Point X="-3.416070068359" Y="24.869541015625" />
                  <Point X="-3.407364990234" Y="24.888400390625" />
                  <Point X="-3.402889648438" Y="24.900052734375" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.390716308594" Y="24.9574921875" />
                  <Point X="-3.391995361328" Y="24.9912578125" />
                  <Point X="-3.396196533203" Y="25.0158203125" />
                  <Point X="-3.404168212891" Y="25.041505859375" />
                  <Point X="-3.417491943359" Y="25.07084375" />
                  <Point X="-3.4385625" Y="25.09851171875" />
                  <Point X="-3.454212890625" Y="25.11268359375" />
                  <Point X="-3.463809082031" Y="25.120306640625" />
                  <Point X="-3.487723876953" Y="25.13690625" />
                  <Point X="-3.501916748047" Y="25.14504296875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.811444335938" Y="25.50044140625" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.824488769531" Y="25.976970703125" />
                  <Point X="-4.8051015625" Y="26.048513671875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.912757080078" Y="26.31915625" />
                  <Point X="-3.765666015625" Y="26.29979296875" />
                  <Point X="-3.744984619141" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703144042969" Y="26.30526171875" />
                  <Point X="-3.694649169922" Y="26.307939453125" />
                  <Point X="-3.641718017578" Y="26.32462890625" />
                  <Point X="-3.622783691406" Y="26.332958984375" />
                  <Point X="-3.604039550781" Y="26.343779296875" />
                  <Point X="-3.587354492188" Y="26.35601171875" />
                  <Point X="-3.573713378906" Y="26.37156640625" />
                  <Point X="-3.561299072266" Y="26.389296875" />
                  <Point X="-3.551352539062" Y="26.407427734375" />
                  <Point X="-3.547943847656" Y="26.41565625" />
                  <Point X="-3.526705078125" Y="26.466931640625" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.536163085938" Y="26.597279296875" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281494141" Y="26.663705078125" />
                  <Point X="-3.587193847656" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.335525878906" Y="27.25733984375" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.081155761719" Y="27.733654296875" />
                  <Point X="-4.029802978516" Y="27.799662109375" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.296996337891" Y="27.896830078125" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187729003906" Y="27.836341796875" />
                  <Point X="-3.167086425781" Y="27.82983203125" />
                  <Point X="-3.146794921875" Y="27.825794921875" />
                  <Point X="-3.134963867188" Y="27.824759765625" />
                  <Point X="-3.061245605469" Y="27.818310546875" />
                  <Point X="-3.040560546875" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012939453" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946075927734" Y="27.86023046875" />
                  <Point X="-2.937677978516" Y="27.86862890625" />
                  <Point X="-2.885352294922" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.844470947266" Y="28.047951171875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854956054688" Y="28.141958984375" />
                  <Point X="-2.86146484375" Y="28.1626015625" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-2.700618896484" Y="29.0946875" />
                  <Point X="-2.61975390625" Y="29.13961328125" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.070851074219" Y="29.265783203125" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028892333984" Y="29.21480078125" />
                  <Point X="-2.012312866211" Y="29.200888671875" />
                  <Point X="-1.995116943359" Y="29.1893984375" />
                  <Point X="-1.98194909668" Y="29.18254296875" />
                  <Point X="-1.899900878906" Y="29.139830078125" />
                  <Point X="-1.880625244141" Y="29.13233203125" />
                  <Point X="-1.859718383789" Y="29.126728515625" />
                  <Point X="-1.839268554688" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777450805664" Y="29.134482421875" />
                  <Point X="-1.763735473633" Y="29.1401640625" />
                  <Point X="-1.678276977539" Y="29.1755625" />
                  <Point X="-1.660146118164" Y="29.185509765625" />
                  <Point X="-1.642416381836" Y="29.197923828125" />
                  <Point X="-1.626864501953" Y="29.2115625" />
                  <Point X="-1.614633056641" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.591016113281" Y="29.280080078125" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584202026367" Y="29.6318984375" />
                  <Point X="-0.949624816895" Y="29.80980859375" />
                  <Point X="-0.851598876953" Y="29.821283203125" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.160627990723" Y="29.386052734375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155905724" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425933838" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.307419616699" Y="29.8879375" />
                  <Point X="0.844031555176" Y="29.831740234375" />
                  <Point X="0.925154602051" Y="29.812154296875" />
                  <Point X="1.481039428711" Y="29.6779453125" />
                  <Point X="1.532545288086" Y="29.659263671875" />
                  <Point X="1.894645874023" Y="29.527927734375" />
                  <Point X="1.945701171875" Y="29.50405078125" />
                  <Point X="2.294568847656" Y="29.340896484375" />
                  <Point X="2.343932861328" Y="29.312138671875" />
                  <Point X="2.680972167969" Y="29.115779296875" />
                  <Point X="2.727497070312" Y="29.08269140625" />
                  <Point X="2.943259765625" Y="28.92925390625" />
                  <Point X="2.266739990234" Y="27.757486328125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.140013183594" Y="27.53448046875" />
                  <Point X="2.132042480469" Y="27.51129296875" />
                  <Point X="2.130107421875" Y="27.504951171875" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108384277344" Y="27.411826171875" />
                  <Point X="2.108204345703" Y="27.38332421875" />
                  <Point X="2.108885498047" Y="27.3713515625" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140069580078" Y="27.24747265625" />
                  <Point X="2.146010498047" Y="27.238716796875" />
                  <Point X="2.18302734375" Y="27.1841640625" />
                  <Point X="2.199184326172" Y="27.165919921875" />
                  <Point X="2.221239013672" Y="27.146677734375" />
                  <Point X="2.230352783203" Y="27.139650390625" />
                  <Point X="2.28490625" Y="27.1026328125" />
                  <Point X="2.304963867188" Y="27.092265625" />
                  <Point X="2.327060302734" Y="27.084" />
                  <Point X="2.348975830078" Y="27.078662109375" />
                  <Point X="2.358568359375" Y="27.077505859375" />
                  <Point X="2.418392089844" Y="27.070291015625" />
                  <Point X="2.443263183594" Y="27.070572265625" />
                  <Point X="2.473269775391" Y="27.074880859375" />
                  <Point X="2.48430859375" Y="27.077140625" />
                  <Point X="2.553491699219" Y="27.095640625" />
                  <Point X="2.565286132812" Y="27.099638671875" />
                  <Point X="2.588533691406" Y="27.110146484375" />
                  <Point X="3.87452734375" Y="27.852615234375" />
                  <Point X="3.967325683594" Y="27.906193359375" />
                  <Point X="4.123267578125" Y="27.68946875" />
                  <Point X="4.149206542969" Y="27.646603515625" />
                  <Point X="4.262197753906" Y="27.4598828125" />
                  <Point X="3.387667724609" Y="26.788833984375" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.217159423828" Y="26.65568359375" />
                  <Point X="3.199923583984" Y="26.636009765625" />
                  <Point X="3.195982421875" Y="26.631203125" />
                  <Point X="3.146191162109" Y="26.56624609375" />
                  <Point X="3.133837402344" Y="26.54484765625" />
                  <Point X="3.122391601562" Y="26.517251953125" />
                  <Point X="3.118653564453" Y="26.50644140625" />
                  <Point X="3.100106201172" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739746094" Y="26.371765625" />
                  <Point X="3.100183349609" Y="26.359923828125" />
                  <Point X="3.115408691406" Y="26.2861328125" />
                  <Point X="3.123606445312" Y="26.26258984375" />
                  <Point X="3.137450439453" Y="26.235109375" />
                  <Point X="3.142928710938" Y="26.22563671875" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198897216797" Y="26.1454453125" />
                  <Point X="3.216140869141" Y="26.129357421875" />
                  <Point X="3.234344238281" Y="26.116037109375" />
                  <Point X="3.243975341797" Y="26.110615234375" />
                  <Point X="3.303986328125" Y="26.076833984375" />
                  <Point X="3.327635253906" Y="26.067431640625" />
                  <Point X="3.358633544922" Y="26.059712890625" />
                  <Point X="3.369141845703" Y="26.057716796875" />
                  <Point X="3.450280517578" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203369141" Y="26.046984375" />
                  <Point X="4.709811523438" Y="26.2078125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845936035156" Y="25.932810546875" />
                  <Point X="4.854108886719" Y="25.88031640625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.896372070312" Y="25.377765625" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.698776611328" Y="25.32284375" />
                  <Point X="3.673902832031" Y="25.31044140625" />
                  <Point X="3.668753417969" Y="25.307673828125" />
                  <Point X="3.589037109375" Y="25.26159765625" />
                  <Point X="3.569077636719" Y="25.246197265625" />
                  <Point X="3.547088623047" Y="25.223994140625" />
                  <Point X="3.539853759766" Y="25.21579296875" />
                  <Point X="3.492024169922" Y="25.154845703125" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.461121826172" Y="25.0792421875" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443781982422" Y="24.97058984375" />
                  <Point X="3.446340820312" Y="24.938421875" />
                  <Point X="3.447737304688" Y="24.9280859375" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526123047" Y="24.823337890625" />
                  <Point X="3.480299804688" Y="24.801873046875" />
                  <Point X="3.492024169922" Y="24.782591796875" />
                  <Point X="3.499700195312" Y="24.772810546875" />
                  <Point X="3.547530029297" Y="24.71186328125" />
                  <Point X="3.566201171875" Y="24.693818359375" />
                  <Point X="3.593306884766" Y="24.67400390625" />
                  <Point X="3.601828613281" Y="24.66844921875" />
                  <Point X="3.681544921875" Y="24.62237109375" />
                  <Point X="3.692708251953" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.836849609375" Y="24.307673828125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855022949219" Y="24.051275390625" />
                  <Point X="4.844551757812" Y="24.005388671875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.632890869141" Y="23.969107421875" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374660644531" Y="23.994490234375" />
                  <Point X="3.349551269531" Y="23.989033203125" />
                  <Point X="3.193096435547" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112396728516" Y="23.9060390625" />
                  <Point X="3.097219482422" Y="23.88778515625" />
                  <Point X="3.002652587891" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.967582275391" Y="23.67099609375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976451660156" Y="23.432" />
                  <Point X="2.99034765625" Y="23.41038671875" />
                  <Point X="3.076931884766" Y="23.2757109375" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.150245605469" Y="22.44136328125" />
                  <Point X="4.213123046875" Y="22.393115234375" />
                  <Point X="4.124815429687" Y="22.250220703125" />
                  <Point X="4.103154785156" Y="22.2194453125" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="2.986659667969" Y="22.715837890625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754222900391" Y="22.840173828125" />
                  <Point X="2.724338623047" Y="22.8455703125" />
                  <Point X="2.538132568359" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444832763672" Y="22.864822265625" />
                  <Point X="2.420006103516" Y="22.851755859375" />
                  <Point X="2.265314697266" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.191466064453" Y="22.684734375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.101072265625" Y="22.406857421875" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.819877929688" Y="21.016810546875" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781857421875" Y="20.888361328125" />
                  <Point X="2.757637451172" Y="20.87268359375" />
                  <Point X="2.701763916016" Y="20.836517578125" />
                  <Point X="1.900256469727" Y="21.881064453125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.692450439453" Y="22.118392578125" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417103881836" Y="22.2588828125" />
                  <Point X="1.384868896484" Y="22.25591796875" />
                  <Point X="1.184016967773" Y="22.237435546875" />
                  <Point X="1.156361938477" Y="22.2306015625" />
                  <Point X="1.12897668457" Y="22.2192578125" />
                  <Point X="1.104593383789" Y="22.204537109375" />
                  <Point X="1.079702392578" Y="22.18383984375" />
                  <Point X="0.924609680176" Y="22.054884765625" />
                  <Point X="0.904141479492" Y="22.03113671875" />
                  <Point X="0.887249206543" Y="22.003310546875" />
                  <Point X="0.875624206543" Y="21.974189453125" />
                  <Point X="0.868182067871" Y="21.93994921875" />
                  <Point X="0.821809936523" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="1.009064941406" Y="20.23883203125" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.97568157959" Y="20.12991796875" />
                  <Point X="0.953304443359" Y="20.1258515625" />
                  <Point X="0.929315979004" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05843737793" Y="20.247365234375" />
                  <Point X="-1.092334472656" Y="20.256087890625" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.127444458008" Y="20.373505859375" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.131704711914" Y="20.544390625" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230575439453" Y="20.905140625" />
                  <Point X="-1.250211914062" Y="20.92906640625" />
                  <Point X="-1.261009887695" Y="20.94022265625" />
                  <Point X="-1.293268798828" Y="20.96851171875" />
                  <Point X="-1.494270751953" Y="21.144787109375" />
                  <Point X="-1.506739501953" Y="21.15403515625" />
                  <Point X="-1.533021728516" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315307617" Y="21.1945234375" />
                  <Point X="-1.621455444336" Y="21.20155078125" />
                  <Point X="-1.63681237793" Y="21.203830078125" />
                  <Point X="-1.679626953125" Y="21.20663671875" />
                  <Point X="-1.946401489258" Y="21.22412109375" />
                  <Point X="-1.961926635742" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048339844" Y="21.209736328125" />
                  <Point X="-2.053666992188" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.0954375" Y="21.184189453125" />
                  <Point X="-2.131113037109" Y="21.1603515625" />
                  <Point X="-2.353403808594" Y="21.011822265625" />
                  <Point X="-2.359689453125" Y="21.00723828125" />
                  <Point X="-2.380446533203" Y="20.989865234375" />
                  <Point X="-2.402760986328" Y="20.9672265625" />
                  <Point X="-2.410471679688" Y="20.958369140625" />
                  <Point X="-2.503202880859" Y="20.83751953125" />
                  <Point X="-2.747574951172" Y="20.988830078125" />
                  <Point X="-2.79452734375" Y="21.02498046875" />
                  <Point X="-2.980862548828" Y="21.168453125" />
                  <Point X="-2.444440673828" Y="22.0975625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849365234" Y="22.289919921875" />
                  <Point X="-2.323946289062" Y="22.318890625" />
                  <Point X="-2.319682617188" Y="22.333822265625" />
                  <Point X="-2.313412109375" Y="22.365353515625" />
                  <Point X="-2.3116171875" Y="22.38151171875" />
                  <Point X="-2.310808837891" Y="22.4138984375" />
                  <Point X="-2.315534912109" Y="22.445951171875" />
                  <Point X="-2.325657470703" Y="22.476728515625" />
                  <Point X="-2.332040527344" Y="22.491681640625" />
                  <Point X="-2.348661865234" Y="22.52291015625" />
                  <Point X="-2.356167724609" Y="22.534798828125" />
                  <Point X="-2.37285546875" Y="22.557341796875" />
                  <Point X="-2.382037353516" Y="22.56799609375" />
                  <Point X="-2.396981445312" Y="22.582939453125" />
                  <Point X="-2.408822265625" Y="22.592984375" />
                  <Point X="-2.433978515625" Y="22.611009765625" />
                  <Point X="-2.447293945312" Y="22.618990234375" />
                  <Point X="-2.476122802734" Y="22.63320703125" />
                  <Point X="-2.490563720703" Y="22.6389140625" />
                  <Point X="-2.520181884766" Y="22.6478984375" />
                  <Point X="-2.550869628906" Y="22.6519375" />
                  <Point X="-2.581803710938" Y="22.650923828125" />
                  <Point X="-2.597227294922" Y="22.6491484375" />
                  <Point X="-2.628753662109" Y="22.642876953125" />
                  <Point X="-2.643684326172" Y="22.63861328125" />
                  <Point X="-2.672649414062" Y="22.6277109375" />
                  <Point X="-2.686683837891" Y="22.621072265625" />
                  <Point X="-3.793088867188" Y="21.9822890625" />
                  <Point X="-4.004018554688" Y="22.25941015625" />
                  <Point X="-4.037666992188" Y="22.31583203125" />
                  <Point X="-4.181266113281" Y="22.556625" />
                  <Point X="-3.228254882812" Y="23.287896484375" />
                  <Point X="-3.048122558594" Y="23.426115234375" />
                  <Point X="-3.039159667969" Y="23.433931640625" />
                  <Point X="-3.016268066406" Y="23.45656640625" />
                  <Point X="-3.002975585938" Y="23.473021484375" />
                  <Point X="-2.984408203125" Y="23.502123046875" />
                  <Point X="-2.976457519531" Y="23.5175234375" />
                  <Point X="-2.968679931641" Y="23.536705078125" />
                  <Point X="-2.960825927734" Y="23.560458984375" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953369141" Y="23.597599609375" />
                  <Point X="-2.947838623047" Y="23.62908203125" />
                  <Point X="-2.947081787109" Y="23.644296875" />
                  <Point X="-2.947780517578" Y="23.6671640625" />
                  <Point X="-2.951227783203" Y="23.68978125" />
                  <Point X="-2.957372802734" Y="23.71181640625" />
                  <Point X="-2.961110595703" Y="23.722646484375" />
                  <Point X="-2.973112792969" Y="23.751626953125" />
                  <Point X="-2.9785234375" Y="23.762625" />
                  <Point X="-2.990719726562" Y="23.78383984375" />
                  <Point X="-3.005551757812" Y="23.8033125" />
                  <Point X="-3.022762207031" Y="23.820705078125" />
                  <Point X="-3.031926269531" Y="23.828841796875" />
                  <Point X="-3.048095703125" Y="23.841431640625" />
                  <Point X="-3.06845703125" Y="23.855267578125" />
                  <Point X="-3.091277099609" Y="23.868697265625" />
                  <Point X="-3.105446777344" Y="23.875525390625" />
                  <Point X="-3.134708740234" Y="23.88674609375" />
                  <Point X="-3.149801025391" Y="23.891138671875" />
                  <Point X="-3.181686523438" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619140625" Y="23.900556640625" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-4.646963867188" Y="23.715142578125" />
                  <Point X="-4.660920410156" Y="23.7133046875" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.749664550781" Y="24.088125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.713485839844" Y="24.63284375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.498832275391" Y="24.6908984375" />
                  <Point X="-3.471225097656" Y="24.702052734375" />
                  <Point X="-3.451819580078" Y="24.7119375" />
                  <Point X="-3.429726318359" Y="24.7251484375" />
                  <Point X="-3.405811523438" Y="24.74174609375" />
                  <Point X="-3.396479492188" Y="24.74912890625" />
                  <Point X="-3.372523681641" Y="24.77065625" />
                  <Point X="-3.358448486328" Y="24.786478515625" />
                  <Point X="-3.338496826172" Y="24.814701171875" />
                  <Point X="-3.329815185547" Y="24.829728515625" />
                  <Point X="-3.321110107422" Y="24.848587890625" />
                  <Point X="-3.312159423828" Y="24.871892578125" />
                  <Point X="-3.304187744141" Y="24.897576171875" />
                  <Point X="-3.300738769531" Y="24.913275390625" />
                  <Point X="-3.296537109375" Y="24.94503125" />
                  <Point X="-3.295784423828" Y="24.961087890625" />
                  <Point X="-3.297063476562" Y="24.994853515625" />
                  <Point X="-3.298355224609" Y="25.0072734375" />
                  <Point X="-3.302556396484" Y="25.0318359375" />
                  <Point X="-3.305465820312" Y="25.043978515625" />
                  <Point X="-3.3134375" Y="25.0696640625" />
                  <Point X="-3.317670410156" Y="25.0807890625" />
                  <Point X="-3.330994140625" Y="25.110126953125" />
                  <Point X="-3.341913085938" Y="25.128400390625" />
                  <Point X="-3.362983642578" Y="25.156068359375" />
                  <Point X="-3.374796142578" Y="25.168931640625" />
                  <Point X="-3.390446533203" Y="25.183103515625" />
                  <Point X="-3.409638916016" Y="25.198349609375" />
                  <Point X="-3.433553710938" Y="25.21494921875" />
                  <Point X="-3.440474609375" Y="25.219322265625" />
                  <Point X="-3.465603271484" Y="25.232828125" />
                  <Point X="-3.4965625" Y="25.245634765625" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.713408691406" Y="26.023666015625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-3.925157226562" Y="26.22496875" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.767738037109" Y="26.20481640625" />
                  <Point X="-3.747056640625" Y="26.204365234375" />
                  <Point X="-3.736703369141" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704891845703" Y="26.208052734375" />
                  <Point X="-3.684612792969" Y="26.2120859375" />
                  <Point X="-3.666088867188" Y="26.217333984375" />
                  <Point X="-3.613157714844" Y="26.2340234375" />
                  <Point X="-3.603461914062" Y="26.237671875" />
                  <Point X="-3.584527587891" Y="26.246001953125" />
                  <Point X="-3.5752890625" Y="26.25068359375" />
                  <Point X="-3.556544921875" Y="26.26150390625" />
                  <Point X="-3.547869873047" Y="26.2671640625" />
                  <Point X="-3.531184814453" Y="26.279396484375" />
                  <Point X="-3.5159296875" Y="26.293373046875" />
                  <Point X="-3.502288574219" Y="26.308927734375" />
                  <Point X="-3.495892578125" Y="26.317078125" />
                  <Point X="-3.483478271484" Y="26.33480859375" />
                  <Point X="-3.478009277344" Y="26.343603515625" />
                  <Point X="-3.468062744141" Y="26.361734375" />
                  <Point X="-3.460176513672" Y="26.379298828125" />
                  <Point X="-3.438937744141" Y="26.43057421875" />
                  <Point X="-3.435500244141" Y="26.44034765625" />
                  <Point X="-3.429710693359" Y="26.4602109375" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012207031" Y="26.60453125" />
                  <Point X="-3.443509521484" Y="26.623810546875" />
                  <Point X="-3.451897460938" Y="26.641146484375" />
                  <Point X="-3.477524414062" Y="26.690375" />
                  <Point X="-3.482801757812" Y="26.6992890625" />
                  <Point X="-3.494293212891" Y="26.716486328125" />
                  <Point X="-3.500507324219" Y="26.72476953125" />
                  <Point X="-3.514419677734" Y="26.741349609375" />
                  <Point X="-3.521497802734" Y="26.748908203125" />
                  <Point X="-3.536439697266" Y="26.763212890625" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.002298095703" Y="27.680302734375" />
                  <Point X="-3.954821777344" Y="27.741328125" />
                  <Point X="-3.726336669922" Y="28.035013671875" />
                  <Point X="-3.344496337891" Y="27.81455859375" />
                  <Point X="-3.254157226562" Y="27.762400390625" />
                  <Point X="-3.244923828125" Y="27.757720703125" />
                  <Point X="-3.225995605469" Y="27.749390625" />
                  <Point X="-3.21630078125" Y="27.745740234375" />
                  <Point X="-3.195658203125" Y="27.73923046875" />
                  <Point X="-3.185623779297" Y="27.736658203125" />
                  <Point X="-3.165332275391" Y="27.73262109375" />
                  <Point X="-3.143244140625" Y="27.73012109375" />
                  <Point X="-3.069525878906" Y="27.723671875" />
                  <Point X="-3.059174072266" Y="27.723333984375" />
                  <Point X="-3.038489013672" Y="27.72378515625" />
                  <Point X="-3.028155761719" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938449951172" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886611816406" Y="27.786142578125" />
                  <Point X="-2.870500976562" Y="27.801455078125" />
                  <Point X="-2.818175292969" Y="27.853779296875" />
                  <Point X="-2.811263427734" Y="27.861490234375" />
                  <Point X="-2.798320068359" Y="27.8776171875" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.749832519531" Y="28.05623046875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.76178125" Y="28.1604921875" />
                  <Point X="-2.764353271484" Y="28.17052734375" />
                  <Point X="-2.770862060547" Y="28.191169921875" />
                  <Point X="-2.774510498047" Y="28.20086328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522705078" Y="28.229033203125" />
                  <Point X="-3.05938671875" Y="28.699916015625" />
                  <Point X="-2.648366210938" Y="29.015041015625" />
                  <Point X="-2.5736171875" Y="29.056568359375" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.146219726563" Y="29.207951171875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111820556641" Y="29.164048828125" />
                  <Point X="-2.097517578125" Y="29.149107421875" />
                  <Point X="-2.089958007813" Y="29.14202734375" />
                  <Point X="-2.073378417969" Y="29.128115234375" />
                  <Point X="-2.065093017578" Y="29.121900390625" />
                  <Point X="-2.047896972656" Y="29.11041015625" />
                  <Point X="-2.025818847656" Y="29.098279296875" />
                  <Point X="-1.943770751953" Y="29.05556640625" />
                  <Point X="-1.934341064453" Y="29.05129296875" />
                  <Point X="-1.915065429688" Y="29.043794921875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.8843125" Y="29.034966796875" />
                  <Point X="-1.874174194336" Y="29.032833984375" />
                  <Point X="-1.853724365234" Y="29.029685546875" />
                  <Point X="-1.833053955078" Y="29.028783203125" />
                  <Point X="-1.812407470703" Y="29.03013671875" />
                  <Point X="-1.802120117188" Y="29.031376953125" />
                  <Point X="-1.7808046875" Y="29.03513671875" />
                  <Point X="-1.770716308594" Y="29.037490234375" />
                  <Point X="-1.750860351562" Y="29.043279296875" />
                  <Point X="-1.727377441406" Y="29.052396484375" />
                  <Point X="-1.641918945312" Y="29.087794921875" />
                  <Point X="-1.632581787109" Y="29.0922734375" />
                  <Point X="-1.614451049805" Y="29.102220703125" />
                  <Point X="-1.605657592773" Y="29.107689453125" />
                  <Point X="-1.587927734375" Y="29.120103515625" />
                  <Point X="-1.579778442383" Y="29.1265" />
                  <Point X="-1.5642265625" Y="29.140138671875" />
                  <Point X="-1.550252197266" Y="29.155388671875" />
                  <Point X="-1.538020874023" Y="29.1720703125" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153894043" Y="29.19948828125" />
                  <Point X="-1.516855834961" Y="29.208728515625" />
                  <Point X="-1.508525146484" Y="29.227662109375" />
                  <Point X="-1.500413085938" Y="29.251513671875" />
                  <Point X="-1.472597900391" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266235352" Y="29.56265625" />
                  <Point X="-0.931167236328" Y="29.7163203125" />
                  <Point X="-0.840553894043" Y="29.726927734375" />
                  <Point X="-0.365222503662" Y="29.78255859375" />
                  <Point X="-0.252390960693" Y="29.36146484375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190795898" Y="29.7850078125" />
                  <Point X="0.827866149902" Y="29.7379140625" />
                  <Point X="0.902858886719" Y="29.71980859375" />
                  <Point X="1.45360559082" Y="29.58683984375" />
                  <Point X="1.500152954102" Y="29.56995703125" />
                  <Point X="1.858247436523" Y="29.44007421875" />
                  <Point X="1.905456298828" Y="29.41799609375" />
                  <Point X="2.250453857422" Y="29.25665234375" />
                  <Point X="2.296112060547" Y="29.230052734375" />
                  <Point X="2.629422607422" Y="29.035865234375" />
                  <Point X="2.672438476562" Y="29.0052734375" />
                  <Point X="2.817779541016" Y="28.901916015625" />
                  <Point X="2.184467529297" Y="27.804986328125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.061125" Y="27.59047265625" />
                  <Point X="2.050172851562" Y="27.56536328125" />
                  <Point X="2.042202270508" Y="27.54217578125" />
                  <Point X="2.03833215332" Y="27.5294921875" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017456054688" Y="27.44844140625" />
                  <Point X="2.014233398437" Y="27.4245" />
                  <Point X="2.013386230469" Y="27.41242578125" />
                  <Point X="2.013206298828" Y="27.383923828125" />
                  <Point X="2.014568725586" Y="27.359978515625" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045317626953" Y="27.223890625" />
                  <Point X="2.055678710937" Y="27.20384765625" />
                  <Point X="2.067397949219" Y="27.185376953125" />
                  <Point X="2.104414794922" Y="27.13082421875" />
                  <Point X="2.111907470703" Y="27.1211796875" />
                  <Point X="2.128064453125" Y="27.102935546875" />
                  <Point X="2.136728759766" Y="27.0943359375" />
                  <Point X="2.158783447266" Y="27.07509375" />
                  <Point X="2.177010986328" Y="27.0610390625" />
                  <Point X="2.231564453125" Y="27.024021484375" />
                  <Point X="2.241285888672" Y="27.018240234375" />
                  <Point X="2.261343505859" Y="27.007873046875" />
                  <Point X="2.2716796875" Y="27.003287109375" />
                  <Point X="2.293776123047" Y="26.995021484375" />
                  <Point X="2.304578613281" Y="26.99169921875" />
                  <Point X="2.326494140625" Y="26.986361328125" />
                  <Point X="2.347199707031" Y="26.983189453125" />
                  <Point X="2.4070234375" Y="26.975974609375" />
                  <Point X="2.419466308594" Y="26.975296875" />
                  <Point X="2.444337402344" Y="26.975578125" />
                  <Point X="2.456765625" Y="26.976537109375" />
                  <Point X="2.486772216797" Y="26.980845703125" />
                  <Point X="2.508849853516" Y="26.985365234375" />
                  <Point X="2.578032958984" Y="27.003865234375" />
                  <Point X="2.583989990234" Y="27.005669921875" />
                  <Point X="2.604414550781" Y="27.0130703125" />
                  <Point X="2.627662109375" Y="27.023578125" />
                  <Point X="2.636033691406" Y="27.027875" />
                  <Point X="3.92202734375" Y="27.77034375" />
                  <Point X="3.940403564453" Y="27.780953125" />
                  <Point X="4.043948486328" Y="27.637048828125" />
                  <Point X="4.067929199219" Y="27.597419921875" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.329835449219" Y="26.864203125" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.165823242188" Y="26.73776953125" />
                  <Point X="3.145702880859" Y="26.71828515625" />
                  <Point X="3.128467041016" Y="26.698611328125" />
                  <Point X="3.120584716797" Y="26.688998046875" />
                  <Point X="3.070793457031" Y="26.624041015625" />
                  <Point X="3.063917724609" Y="26.613744140625" />
                  <Point X="3.051563964844" Y="26.592345703125" />
                  <Point X="3.0460859375" Y="26.581244140625" />
                  <Point X="3.034640136719" Y="26.5536484375" />
                  <Point X="3.0271640625" Y="26.53202734375" />
                  <Point X="3.008616699219" Y="26.46570703125" />
                  <Point X="3.006225585938" Y="26.454662109375" />
                  <Point X="3.002771972656" Y="26.43236328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.007143554688" Y="26.340724609375" />
                  <Point X="3.022368896484" Y="26.26693359375" />
                  <Point X="3.025692138672" Y="26.254892578125" />
                  <Point X="3.033889892578" Y="26.231349609375" />
                  <Point X="3.038764404297" Y="26.21984765625" />
                  <Point X="3.052608398438" Y="26.1923671875" />
                  <Point X="3.063564941406" Y="26.173421875" />
                  <Point X="3.1049765625" Y="26.110478515625" />
                  <Point X="3.111740722656" Y="26.101421875" />
                  <Point X="3.126297607422" Y="26.084173828125" />
                  <Point X="3.134090332031" Y="26.075982421875" />
                  <Point X="3.151333984375" Y="26.05989453125" />
                  <Point X="3.160040283203" Y="26.05269140625" />
                  <Point X="3.178243652344" Y="26.03937109375" />
                  <Point X="3.197371826172" Y="26.02783203125" />
                  <Point X="3.2573828125" Y="25.99405078125" />
                  <Point X="3.268888427734" Y="25.9885546875" />
                  <Point X="3.292537353516" Y="25.97915234375" />
                  <Point X="3.304680664062" Y="25.97524609375" />
                  <Point X="3.335678955078" Y="25.96752734375" />
                  <Point X="3.356695556641" Y="25.96353515625" />
                  <Point X="3.437834228516" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603271484" Y="25.952796875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.914236328125" />
                  <Point X="4.760239746094" Y="25.865701171875" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.871784179688" Y="25.469529296875" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.682916992188" Y="25.41842578125" />
                  <Point X="3.656385742188" Y="25.407861328125" />
                  <Point X="3.631511962891" Y="25.395458984375" />
                  <Point X="3.621213134766" Y="25.389923828125" />
                  <Point X="3.541496826172" Y="25.34384765625" />
                  <Point X="3.531003417969" Y="25.336810546875" />
                  <Point X="3.511043945312" Y="25.32141015625" />
                  <Point X="3.501577880859" Y="25.313046875" />
                  <Point X="3.479588867188" Y="25.29084375" />
                  <Point X="3.465119140625" Y="25.27444140625" />
                  <Point X="3.417289550781" Y="25.213494140625" />
                  <Point X="3.410854980469" Y="25.20420703125" />
                  <Point X="3.399131835938" Y="25.1849296875" />
                  <Point X="3.393843261719" Y="25.174939453125" />
                  <Point X="3.384069580078" Y="25.1534765625" />
                  <Point X="3.380005615234" Y="25.1429296875" />
                  <Point X="3.373158935547" Y="25.121427734375" />
                  <Point X="3.367817382812" Y="25.097111328125" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350321777344" Y="25.00120703125" />
                  <Point X="3.348925292969" Y="24.9758046875" />
                  <Point X="3.349081054688" Y="24.963056640625" />
                  <Point X="3.351639892578" Y="24.930888671875" />
                  <Point X="3.354432861328" Y="24.910216796875" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.81601171875" />
                  <Point X="3.380004394531" Y="24.794513671875" />
                  <Point X="3.384067138672" Y="24.783970703125" />
                  <Point X="3.393840820312" Y="24.762505859375" />
                  <Point X="3.399128417969" Y="24.752515625" />
                  <Point X="3.410852783203" Y="24.733234375" />
                  <Point X="3.424965576172" Y="24.714162109375" />
                  <Point X="3.472795410156" Y="24.65321484375" />
                  <Point X="3.481510253906" Y="24.643552734375" />
                  <Point X="3.500181396484" Y="24.6255078125" />
                  <Point X="3.510137695312" Y="24.617125" />
                  <Point X="3.537243408203" Y="24.597310546875" />
                  <Point X="3.554286865234" Y="24.586201171875" />
                  <Point X="3.634003173828" Y="24.540123046875" />
                  <Point X="3.639487060547" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027832031" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761613769531" Y="24.068947265625" />
                  <Point X="4.751932617188" Y="24.0265234375" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.645290771484" Y="24.063294921875" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366721923828" Y="24.089158203125" />
                  <Point X="3.354485107422" Y="24.087322265625" />
                  <Point X="3.329375732422" Y="24.081865234375" />
                  <Point X="3.172920898438" Y="24.047859375" />
                  <Point X="3.157877441406" Y="24.0432578125" />
                  <Point X="3.128754638672" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074124023438" Y="23.99846875" />
                  <Point X="3.050373779297" Y="23.977998046875" />
                  <Point X="3.039348144531" Y="23.966775390625" />
                  <Point X="3.024170898438" Y="23.948521484375" />
                  <Point X="2.929604003906" Y="23.834787109375" />
                  <Point X="2.921325195312" Y="23.82315234375" />
                  <Point X="2.90660546875" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.872981933594" Y="23.679701171875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889159667969" Y="23.394515625" />
                  <Point X="2.896542724609" Y="23.380623046875" />
                  <Point X="2.910438720703" Y="23.359009765625" />
                  <Point X="2.997022949219" Y="23.224333984375" />
                  <Point X="3.001743896484" Y="23.21764453125" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="4.087171386719" Y="22.370015625" />
                  <Point X="4.045493164062" Y="22.30257421875" />
                  <Point X="4.025467773437" Y="22.274123046875" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="3.034159667969" Y="22.798109375" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815024902344" Y="22.920484375" />
                  <Point X="2.783118408203" Y="22.930671875" />
                  <Point X="2.771104980469" Y="22.933662109375" />
                  <Point X="2.741220703125" Y="22.93905859375" />
                  <Point X="2.555014648438" Y="22.9726875" />
                  <Point X="2.539357666016" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444847900391" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400587646484" Y="22.948890625" />
                  <Point X="2.375760986328" Y="22.93582421875" />
                  <Point X="2.221069580078" Y="22.854412109375" />
                  <Point X="2.208965332031" Y="22.846828125" />
                  <Point X="2.186036865234" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.78883984375" />
                  <Point X="2.128047363281" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.107398193359" Y="22.728978515625" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.0021875" Y="22.419859375" />
                  <Point X="2.007584472656" Y="22.389974609375" />
                  <Point X="2.041213134766" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723752685547" Y="20.963916015625" />
                  <Point X="1.975625" Y="21.938896484375" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808835327148" Y="22.15037109375" />
                  <Point X="1.783252929688" Y="22.17199609375" />
                  <Point X="1.773299560547" Y="22.179353515625" />
                  <Point X="1.743825683594" Y="22.198302734375" />
                  <Point X="1.560176025391" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470092773" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455391601563" Y="22.3513046875" />
                  <Point X="1.424129760742" Y="22.353623046875" />
                  <Point X="1.408402832031" Y="22.353484375" />
                  <Point X="1.37616784668" Y="22.35051953125" />
                  <Point X="1.175315795898" Y="22.332037109375" />
                  <Point X="1.1612265625" Y="22.329662109375" />
                  <Point X="1.133571533203" Y="22.322828125" />
                  <Point X="1.120005859375" Y="22.318369140625" />
                  <Point X="1.092620605469" Y="22.307025390625" />
                  <Point X="1.079877319336" Y="22.3005859375" />
                  <Point X="1.055494018555" Y="22.285865234375" />
                  <Point X="1.043854248047" Y="22.277583984375" />
                  <Point X="1.018963195801" Y="22.25688671875" />
                  <Point X="0.863870483398" Y="22.127931640625" />
                  <Point X="0.852649291992" Y="22.11690625" />
                  <Point X="0.832181091309" Y="22.093158203125" />
                  <Point X="0.822933776855" Y="22.080435546875" />
                  <Point X="0.806041625977" Y="22.052609375" />
                  <Point X="0.799019470215" Y="22.03853125" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791625977" Y="21.9943671875" />
                  <Point X="0.775349487305" Y="21.960126953125" />
                  <Point X="0.728977355957" Y="21.74678125" />
                  <Point X="0.727584594727" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091430664" Y="20.84766015625" />
                  <Point X="0.710463256836" Y="21.305314453125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146057129" Y="21.54641796875" />
                  <Point X="0.62678692627" Y="21.576185546875" />
                  <Point X="0.62040625" Y="21.58679296875" />
                  <Point X="0.597763549805" Y="21.619416015625" />
                  <Point X="0.456678649902" Y="21.822693359375" />
                  <Point X="0.446669494629" Y="21.834830078125" />
                  <Point X="0.424783569336" Y="21.857287109375" />
                  <Point X="0.412906524658" Y="21.867607421875" />
                  <Point X="0.386651550293" Y="21.886849609375" />
                  <Point X="0.373236694336" Y="21.89506640625" />
                  <Point X="0.345234649658" Y="21.909173828125" />
                  <Point X="0.330647460938" Y="21.915064453125" />
                  <Point X="0.295609039307" Y="21.9259375" />
                  <Point X="0.077288223267" Y="21.993697265625" />
                  <Point X="0.063372219086" Y="21.996890625" />
                  <Point X="0.035222892761" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008652074814" Y="22.002234375" />
                  <Point X="-0.022895795822" Y="22.001162109375" />
                  <Point X="-0.051062511444" Y="21.996892578125" />
                  <Point X="-0.06498550415" Y="21.9936953125" />
                  <Point X="-0.100023651123" Y="21.9828203125" />
                  <Point X="-0.318344604492" Y="21.9150625" />
                  <Point X="-0.332933410645" Y="21.909169921875" />
                  <Point X="-0.360934112549" Y="21.895060546875" />
                  <Point X="-0.374346160889" Y="21.88684375" />
                  <Point X="-0.400601287842" Y="21.867599609375" />
                  <Point X="-0.412474609375" Y="21.857283203125" />
                  <Point X="-0.434358734131" Y="21.834828125" />
                  <Point X="-0.444369384766" Y="21.822689453125" />
                  <Point X="-0.467012237549" Y="21.790064453125" />
                  <Point X="-0.608097106934" Y="21.5867890625" />
                  <Point X="-0.612471130371" Y="21.5798671875" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.985425231934" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662769005896" Y="21.043391615474" />
                  <Point X="2.689517047353" Y="21.052601704749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601785326245" Y="21.122867215323" />
                  <Point X="2.64112792753" Y="21.136413959324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.540801646593" Y="21.202342815172" />
                  <Point X="2.592738807708" Y="21.220226213899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479817966942" Y="21.28181841502" />
                  <Point X="2.544349687885" Y="21.304038468474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.418834287291" Y="21.361294014869" />
                  <Point X="2.495960568063" Y="21.387850723049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.08682920946" Y="20.254671234749" />
                  <Point X="-0.9687786469" Y="20.295319303203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.816387189522" Y="20.910001194984" />
                  <Point X="0.824515603003" Y="20.912800032198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.35785060764" Y="21.440769614718" />
                  <Point X="2.44757144824" Y="21.471662977624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13190503719" Y="20.339624347282" />
                  <Point X="-0.939120412489" Y="20.406005416985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.791739325591" Y="21.001988219537" />
                  <Point X="0.811861581099" Y="21.00891686775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.296866927989" Y="21.520245214567" />
                  <Point X="2.399182328417" Y="21.555475232199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119791498396" Y="20.444269337897" />
                  <Point X="-0.909462178077" Y="20.516691530767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.767091461661" Y="21.09397524409" />
                  <Point X="0.799207559195" Y="21.105033703302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.235883248338" Y="21.599720814416" />
                  <Point X="2.350793208595" Y="21.639287486774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131006725915" Y="20.540881590085" />
                  <Point X="-0.879803943666" Y="20.627377644549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.74244359773" Y="21.185962268642" />
                  <Point X="0.786553537291" Y="21.201150538854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.174899568687" Y="21.679196414265" />
                  <Point X="2.302404088772" Y="21.723099741349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.927146504851" Y="22.282543419688" />
                  <Point X="4.061758622823" Y="22.328894088989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.149711152582" Y="20.634915104206" />
                  <Point X="-0.850145709254" Y="20.738063758331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.717795733799" Y="21.277949293195" />
                  <Point X="0.773899515387" Y="21.297267374406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113915889036" Y="21.758672014113" />
                  <Point X="2.25401496895" Y="21.806911995924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.818134456984" Y="22.345481526139" />
                  <Point X="4.025908937474" Y="22.417024017109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168415583486" Y="20.728948616867" />
                  <Point X="-0.820487474842" Y="20.848749872113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693147931046" Y="21.369936338813" />
                  <Point X="0.761245493483" Y="21.393384209958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.052932209384" Y="21.838147613962" />
                  <Point X="2.205625849127" Y="21.890724250499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709122409117" Y="22.40841963259" />
                  <Point X="3.93552666052" Y="22.486376868114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188493327606" Y="20.822509259867" />
                  <Point X="-0.790829240431" Y="20.959435985895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.668500154201" Y="21.461923393352" />
                  <Point X="0.748591471579" Y="21.48950104551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.991948529733" Y="21.917623213811" />
                  <Point X="2.157236729305" Y="21.974536505074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.60011036125" Y="22.471357739041" />
                  <Point X="3.845144383566" Y="22.55572971912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.232720720485" Y="20.907754511948" />
                  <Point X="-0.761171006019" Y="21.070122099677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.639120797204" Y="21.55228123419" />
                  <Point X="0.735937449675" Y="21.585617881062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.930964672382" Y="21.997098752473" />
                  <Point X="2.108847609482" Y="22.05834875965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491098313383" Y="22.534295845492" />
                  <Point X="3.754762106613" Y="22.625082570125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.308718083487" Y="20.982060486042" />
                  <Point X="-0.731512771608" Y="21.180808213459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.587064834205" Y="21.634830893406" />
                  <Point X="0.724739330526" Y="21.682236024135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.86998075008" Y="22.07657426877" />
                  <Point X="2.061932938075" Y="22.142668707528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382086265517" Y="22.597233951943" />
                  <Point X="3.664379829659" Y="22.694435421131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.390985382921" Y="21.054207547888" />
                  <Point X="-0.701854537196" Y="21.291494327241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.530781418753" Y="21.715924924008" />
                  <Point X="0.737761299691" Y="21.787193812411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.804108599825" Y="22.154366633203" />
                  <Point X="2.035731403476" Y="22.234120760368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.27307421765" Y="22.660172058394" />
                  <Point X="3.573997552705" Y="22.763788272136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.473252682355" Y="21.126354609735" />
                  <Point X="-0.672196302785" Y="21.402180441023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.474498003301" Y="21.79701895461" />
                  <Point X="0.761366666741" Y="21.89579575682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.70758197287" Y="22.221603814838" />
                  <Point X="2.018648294738" Y="22.328712539021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.164062169783" Y="22.723110164845" />
                  <Point X="3.483615275751" Y="22.833141123142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.53918465463" Y="20.859798762505" />
                  <Point X="-2.46704807011" Y="20.884637380484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.578772845616" Y="21.190495068478" />
                  <Point X="-0.642473419742" Y="21.512888815114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.404850774849" Y="21.873511455378" />
                  <Point X="0.785965420912" Y="22.004739751847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.60580810976" Y="22.287034228171" />
                  <Point X="2.001847217398" Y="22.423401428873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.055050121916" Y="22.786048271296" />
                  <Point X="3.393232998797" Y="22.902493974147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.643463912353" Y="20.924366499291" />
                  <Point X="-2.315052691065" Y="21.037447551294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.800654991084" Y="21.21456888361" />
                  <Point X="-0.573066262235" Y="21.637261580716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.27967342594" Y="21.930883402303" />
                  <Point X="0.872301144814" Y="22.134941490312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.48414471681" Y="22.345616127164" />
                  <Point X="2.009136012568" Y="22.52638512703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.946038058075" Y="22.848986372246" />
                  <Point X="3.302850721844" Y="22.971846825153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.747720410646" Y="20.988942072776" />
                  <Point X="-0.481432615954" Y="21.76928754015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.1262067105" Y="21.978514539169" />
                  <Point X="2.063506150389" Y="22.645580231533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835740892956" Y="22.911481977341" />
                  <Point X="3.21246844489" Y="23.041199676159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837889742085" Y="21.058368246803" />
                  <Point X="2.13038619609" Y="22.769082842759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.664350297358" Y="22.952941427331" />
                  <Point X="3.122086167936" Y="23.110552527164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.928057661808" Y="21.127794906922" />
                  <Point X="3.034492957245" Y="23.180865730699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95086117839" Y="21.220416991195" />
                  <Point X="2.973807496748" Y="23.260444015638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.878459101645" Y="21.345820990191" />
                  <Point X="2.920919739706" Y="23.342707265196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.635181361787" Y="23.932974878081" />
                  <Point X="4.738720477965" Y="23.968626254837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806057024899" Y="21.471224989187" />
                  <Point X="2.87539052955" Y="23.427504265641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424092100779" Y="23.96076498136" />
                  <Point X="4.76289161658" Y="24.077423010019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.733654948153" Y="21.596628988183" />
                  <Point X="2.859339986549" Y="23.52245158519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.21300283977" Y="23.988555084638" />
                  <Point X="4.77886901972" Y="24.183398435822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.661252871408" Y="21.722032987178" />
                  <Point X="2.868032946184" Y="23.625918775946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.001913578762" Y="24.016345187917" />
                  <Point X="4.682483386517" Y="24.250684165498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588850794662" Y="21.847436986174" />
                  <Point X="2.880837195349" Y="23.730801597214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.790824317753" Y="24.044135291196" />
                  <Point X="4.518384057658" Y="24.294654199963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.516448717917" Y="21.97284098517" />
                  <Point X="2.945075625122" Y="23.853394627132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.579735578698" Y="24.071925574197" />
                  <Point X="4.354284728798" Y="24.338624234427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.444046642658" Y="22.098244983654" />
                  <Point X="3.073804790367" Y="23.998193598074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.295286387784" Y="24.0744558279" />
                  <Point X="4.190185399938" Y="24.382594268891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.371644839152" Y="22.223648888565" />
                  <Point X="4.026086071079" Y="24.426564303355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.317933617698" Y="22.342617109968" />
                  <Point X="3.861986742219" Y="24.470534337819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315249473859" Y="22.444015299523" />
                  <Point X="3.697887413359" Y="24.514504372283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825257100467" Y="22.024551942116" />
                  <Point X="-3.564187096508" Y="22.114445553481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353886456277" Y="22.531185484295" />
                  <Point X="3.576695257497" Y="24.573248531218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.885851643414" Y="22.104161532478" />
                  <Point X="-3.133010225406" Y="22.363385621126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.427447783593" Y="22.606330252743" />
                  <Point X="3.483497656383" Y="24.641631988375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.946446186362" Y="22.183771122839" />
                  <Point X="-2.701833354304" Y="22.612325688771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.593719755374" Y="22.649552186255" />
                  <Point X="3.4203719404" Y="24.720370025966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006497944013" Y="22.263567609166" />
                  <Point X="3.376437297257" Y="24.805716079864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056209319007" Y="22.346924574773" />
                  <Point X="3.356517593251" Y="24.899331140439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.105920133885" Y="22.430281733245" />
                  <Point X="3.350123674691" Y="24.997603502435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155630948763" Y="22.513638891716" />
                  <Point X="3.369494806797" Y="25.104747482832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066228279982" Y="22.644896663992" />
                  <Point X="3.426090587918" Y="25.224708937781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.828700971277" Y="22.827157840002" />
                  <Point X="3.634468661221" Y="25.396933227136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752397459891" Y="25.43753936892" />
                  <Point X="4.77222907757" Y="25.788695555792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591173662572" Y="23.009419016012" />
                  <Point X="4.75738181968" Y="25.884057199632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353646353868" Y="23.191680192022" />
                  <Point X="4.737257159681" Y="25.977601688199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.116116557177" Y="23.373942224715" />
                  <Point X="4.714689090233" Y="26.070304843422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974110041877" Y="23.523312953912" />
                  <Point X="4.441295240892" Y="26.076641756503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947648791494" Y="23.632898257814" />
                  <Point X="3.96886487461" Y="26.014444900749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963318146916" Y="23.727976830772" />
                  <Point X="3.497229490812" Y="25.952521779415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.013372274647" Y="23.811215777148" />
                  <Point X="3.288005462364" Y="25.98095413377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.11157573045" Y="23.877875580307" />
                  <Point X="3.174282057725" Y="26.042269989988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.398725128447" Y="23.87947607815" />
                  <Point X="3.100469900652" Y="26.117328390824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871154028755" Y="23.817279727171" />
                  <Point X="3.048763901779" Y="26.199998552352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.343582929063" Y="23.755083376192" />
                  <Point X="3.01765867224" Y="26.289762127617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66866459604" Y="23.74362274639" />
                  <Point X="3.001312977998" Y="26.384607818444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692254063707" Y="23.835974206002" />
                  <Point X="3.015390737465" Y="26.489929144474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715843531373" Y="23.928325665614" />
                  <Point X="3.059139087841" Y="26.605466874257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73943299904" Y="24.020677125227" />
                  <Point X="3.176046515136" Y="26.746195294386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.753686087677" Y="24.116243357947" />
                  <Point X="3.413571173654" Y="26.928455557864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767381596735" Y="24.212001580713" />
                  <Point X="3.651100293594" Y="27.110717357532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781077105793" Y="24.307759803479" />
                  <Point X="-4.270161172569" Y="24.483682267358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.348047296175" Y="24.801191537598" />
                  <Point X="3.888629413534" Y="27.292979157201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300090030861" Y="24.918178513016" />
                  <Point X="4.126158533474" Y="27.475240956869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300289735169" Y="25.018583714021" />
                  <Point X="2.390258248854" Y="26.97799651967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.785155650003" Y="27.113970599302" />
                  <Point X="4.088839584411" Y="27.562864976921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330349409887" Y="25.108707302682" />
                  <Point X="2.231770685378" Y="27.023898839915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.216332502049" Y="27.362910660385" />
                  <Point X="4.037701532123" Y="27.645730698141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.395175263101" Y="25.186859936078" />
                  <Point X="2.138938394956" Y="27.09240808363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.647509354094" Y="27.611850721469" />
                  <Point X="3.979761547322" Y="27.726254326173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50649289067" Y="25.249004167773" />
                  <Point X="2.076819147678" Y="27.171492676189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.670382681545" Y="25.293046351951" />
                  <Point X="2.032360625927" Y="27.256658344216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.834481679945" Y="25.337016500201" />
                  <Point X="2.01560748055" Y="27.351363738366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.998580678345" Y="25.380986648452" />
                  <Point X="2.018323121123" Y="27.452772773116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.162679676745" Y="25.424956796702" />
                  <Point X="2.049724552188" Y="27.564059117641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.326778675145" Y="25.468926944953" />
                  <Point X="2.116688959984" Y="27.687590777066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.490877673545" Y="25.512897093203" />
                  <Point X="2.189091245761" Y="27.812994848037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.654976671944" Y="25.556867241454" />
                  <Point X="2.261493176253" Y="27.938398796673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782234818011" Y="25.61352271246" />
                  <Point X="2.333895106744" Y="28.06380274531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766569241915" Y="25.7193907676" />
                  <Point X="2.406297037236" Y="28.189206693946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750903665818" Y="25.825258822741" />
                  <Point X="2.478698967727" Y="28.314610642583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735238089722" Y="25.931126877882" />
                  <Point X="-3.893834472559" Y="26.220845377193" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465027024535" Y="26.368495622332" />
                  <Point X="2.551100898219" Y="28.440014591219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708790820591" Y="26.040707367653" />
                  <Point X="-4.104923154159" Y="26.248635679978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425173924405" Y="26.482692109895" />
                  <Point X="2.62350282871" Y="28.565418539856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678762535473" Y="26.151520900111" />
                  <Point X="-4.316011000276" Y="26.276426270443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429320329368" Y="26.581738352883" />
                  <Point X="2.695904759202" Y="28.690822488492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648734250356" Y="26.26233443257" />
                  <Point X="-4.527098846394" Y="26.304216860907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.466593965386" Y="26.669377975467" />
                  <Point X="2.768306689693" Y="28.816226437128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.52306410684" Y="26.75040771115" />
                  <Point X="2.787632388879" Y="28.923354773717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.610519612038" Y="26.820768330489" />
                  <Point X="2.692438334317" Y="28.991050796823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.700902080037" Y="26.890121115713" />
                  <Point X="2.592783945892" Y="29.057211003816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.791284548035" Y="26.959473900936" />
                  <Point X="2.484389599763" Y="29.120361802032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881667016034" Y="27.02882668616" />
                  <Point X="2.375995253634" Y="29.183512600248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972049484033" Y="27.098179471383" />
                  <Point X="2.267600256734" Y="29.246663174385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062431952032" Y="27.167532256607" />
                  <Point X="2.146289516165" Y="29.305366501332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.15281442003" Y="27.23688504183" />
                  <Point X="2.022552186959" Y="29.363234286804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.214960949009" Y="27.315960240546" />
                  <Point X="-3.028289433" Y="27.724564011412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918905662066" Y="27.76222786419" />
                  <Point X="1.898814880023" Y="29.421102079944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.141564017135" Y="27.441706795633" />
                  <Point X="-3.235648217319" Y="27.753638620825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776457643554" Y="27.911750615135" />
                  <Point X="1.762730917537" Y="29.474718578647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.068167085261" Y="27.567453350721" />
                  <Point X="-3.346747212568" Y="27.815858133665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748726271477" Y="28.021773257008" />
                  <Point X="1.620624454543" Y="29.526261364124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.991347559407" Y="27.694378399425" />
                  <Point X="-3.455759396641" Y="27.878796193217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755406904982" Y="28.119946895131" />
                  <Point X="1.478517880569" Y="29.577804111388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884578609097" Y="27.831615861972" />
                  <Point X="-3.564771580714" Y="27.941734252768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.779470358024" Y="28.21213514849" />
                  <Point X="-0.127315781568" Y="29.125345203877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.233652129722" Y="29.249636423245" />
                  <Point X="1.312148212576" Y="29.620992405397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.777808641116" Y="27.96885367493" />
                  <Point X="-3.673783764788" Y="28.004672312319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.826438149505" Y="28.296436805661" />
                  <Point X="-0.197624382316" Y="29.2016099759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.264507372004" Y="29.36073469989" />
                  <Point X="1.140621034983" Y="29.662404826435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874827147248" Y="28.380249102271" />
                  <Point X="-0.233197919244" Y="29.289834989546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.294165567374" Y="29.471420800229" />
                  <Point X="0.96909385739" Y="29.703817247472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.923216144992" Y="28.464061398881" />
                  <Point X="-0.257845651173" Y="29.381822059551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.323823762744" Y="29.582106900568" />
                  <Point X="0.788342009534" Y="29.742053359815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.971605142735" Y="28.547873695492" />
                  <Point X="-0.282493463927" Y="29.473809101725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353481958114" Y="29.692793000907" />
                  <Point X="0.564597138432" Y="29.765485787075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.019994140479" Y="28.631685992102" />
                  <Point X="-1.860907645898" Y="29.030791478377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.577728272042" Y="29.12829795631" />
                  <Point X="-0.30714127668" Y="29.5657961439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015159242501" Y="28.733824745696" />
                  <Point X="-1.998304653444" Y="29.083955859409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.499040739444" Y="29.255866211318" />
                  <Point X="-0.331789089434" Y="29.657783186074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.7772731307" Y="28.91620946752" />
                  <Point X="-2.0983532031" Y="29.149980345805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466489088898" Y="29.367548608172" />
                  <Point X="-0.356436902188" Y="29.749770228249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.43047506108" Y="29.136095583838" />
                  <Point X="-2.162013559021" Y="29.228534292102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466793723058" Y="29.467917678931" />
                  <Point X="-0.649704882878" Y="29.749263929116" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.5269375" Y="21.256138671875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.441675994873" Y="21.511080078125" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.239297592163" Y="21.74447265625" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.043702919006" Y="21.801359375" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.310921813965" Y="21.681732421875" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.804409912109" Y="20.1746484375" />
                  <Point X="-0.84774432373" Y="20.012921875" />
                  <Point X="-1.10023046875" Y="20.061931640625" />
                  <Point X="-1.139680664063" Y="20.07208203125" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.315818969727" Y="20.398306640625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.318054077148" Y="20.50732421875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.418541503906" Y="20.82566015625" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.692055541992" Y="21.01704296875" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.025553955078" Y="21.00237109375" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734375" Y="20.842705078125" />
                  <Point X="-2.453527832031" Y="20.5901484375" />
                  <Point X="-2.457094970703" Y="20.5855" />
                  <Point X="-2.479401123047" Y="20.599310546875" />
                  <Point X="-2.855839111328" Y="20.832392578125" />
                  <Point X="-2.910440673828" Y="20.87443359375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.608985595703" Y="22.1925625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.402412109375" />
                  <Point X="-2.516384521484" Y="22.433640625" />
                  <Point X="-2.531328613281" Y="22.448583984375" />
                  <Point X="-2.560157470703" Y="22.46280078125" />
                  <Point X="-2.591683837891" Y="22.456529296875" />
                  <Point X="-3.702765380859" Y="21.815046875" />
                  <Point X="-3.842959228516" Y="21.73410546875" />
                  <Point X="-3.864301513672" Y="21.76214453125" />
                  <Point X="-4.161703613281" Y="22.15287109375" />
                  <Point X="-4.2008515625" Y="22.218515625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.343919433594" Y="23.438634765625" />
                  <Point X="-3.163787109375" Y="23.576853515625" />
                  <Point X="-3.152534179688" Y="23.58891796875" />
                  <Point X="-3.144756591797" Y="23.608099609375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651611328" Y="23.649947265625" />
                  <Point X="-3.148653808594" Y="23.678927734375" />
                  <Point X="-3.164823242188" Y="23.691517578125" />
                  <Point X="-3.187643310547" Y="23.704947265625" />
                  <Point X="-3.219528808594" Y="23.711427734375" />
                  <Point X="-4.6221640625" Y="23.526767578125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.811077148438" Y="23.5334375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.937750488281" Y="24.061224609375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.762661621094" Y="24.81637109375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.538058349609" Y="24.88123828125" />
                  <Point X="-3.514143554688" Y="24.8978359375" />
                  <Point X="-3.502324951172" Y="24.909353515625" />
                  <Point X="-3.493619873047" Y="24.928212890625" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.486927246094" Y="24.987662109375" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.502328857422" Y="25.028091796875" />
                  <Point X="-3.517979248047" Y="25.042263671875" />
                  <Point X="-3.541894042969" Y="25.05886328125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.836032226562" Y="25.408677734375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.992609375" Y="25.48981640625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.896794433594" Y="26.073361328125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.900356933594" Y="26.41334375" />
                  <Point X="-3.753265869141" Y="26.39398046875" />
                  <Point X="-3.731704345703" Y="26.3958671875" />
                  <Point X="-3.723209472656" Y="26.398544921875" />
                  <Point X="-3.670278320312" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.635711181641" Y="26.452013671875" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.620428710938" Y="26.553412109375" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968017578" Y="26.619220703125" />
                  <Point X="-4.393358398438" Y="27.181970703125" />
                  <Point X="-4.476105957031" Y="27.24546484375" />
                  <Point X="-4.451298828125" Y="27.287966796875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.104784179688" Y="27.85799609375" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.249496337891" Y="27.9791015625" />
                  <Point X="-3.159157226562" Y="27.926943359375" />
                  <Point X="-3.138514648438" Y="27.92043359375" />
                  <Point X="-3.12668359375" Y="27.9193984375" />
                  <Point X="-3.052965332031" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-3.004854980469" Y="27.935802734375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939109375" Y="28.039671875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067626953" Y="28.134033203125" />
                  <Point X="-3.277054931641" Y="28.696927734375" />
                  <Point X="-3.307278808594" Y="28.74927734375" />
                  <Point X="-3.260201171875" Y="28.78537109375" />
                  <Point X="-2.752873535156" Y="29.174333984375" />
                  <Point X="-2.665890136719" Y="29.222658203125" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-1.995482421875" Y="29.323615234375" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.938079345703" Y="29.266806640625" />
                  <Point X="-1.85603112793" Y="29.22409375" />
                  <Point X="-1.835124267578" Y="29.218490234375" />
                  <Point X="-1.813808837891" Y="29.22225" />
                  <Point X="-1.800093505859" Y="29.227931640625" />
                  <Point X="-1.714634887695" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.681619140625" Y="29.308646484375" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.688864990234" Y="29.699068359375" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.624857788086" Y="29.719162109375" />
                  <Point X="-0.968082885742" Y="29.903296875" />
                  <Point X="-0.862643249512" Y="29.915638671875" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.068865089417" Y="29.410640625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282110214" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594032288" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.220971984863" Y="29.93236328125" />
                  <Point X="0.2366484375" Y="29.990869140625" />
                  <Point X="0.286755584717" Y="29.98562109375" />
                  <Point X="0.860210144043" Y="29.925564453125" />
                  <Point X="0.947450073242" Y="29.904501953125" />
                  <Point X="1.508456054688" Y="29.769056640625" />
                  <Point X="1.56493762207" Y="29.7485703125" />
                  <Point X="1.931044555664" Y="29.61578125" />
                  <Point X="1.985946166992" Y="29.59010546875" />
                  <Point X="2.338685058594" Y="29.425140625" />
                  <Point X="2.391753662109" Y="29.394224609375" />
                  <Point X="2.732519775391" Y="29.195693359375" />
                  <Point X="2.782555175781" Y="29.160109375" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.349012451172" Y="27.709986328125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.2218828125" Y="27.48041015625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.203202392578" Y="27.382724609375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.224623046875" Y="27.292056640625" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.283694580078" Y="27.21826171875" />
                  <Point X="2.338248046875" Y="27.181244140625" />
                  <Point X="2.360344482422" Y="27.172978515625" />
                  <Point X="2.369937011719" Y="27.171822265625" />
                  <Point X="2.429760742188" Y="27.164607421875" />
                  <Point X="2.459767333984" Y="27.168916015625" />
                  <Point X="2.528950439453" Y="27.187416015625" />
                  <Point X="2.541033691406" Y="27.19241796875" />
                  <Point X="3.82702734375" Y="27.93488671875" />
                  <Point X="3.994247802734" Y="28.031431640625" />
                  <Point X="4.000458740234" Y="28.022798828125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.230483886719" Y="27.695787109375" />
                  <Point X="4.387512207031" Y="27.436294921875" />
                  <Point X="3.4455" Y="26.71346484375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.271380126953" Y="26.573408203125" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.210143066406" Y="26.48085546875" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.193223144531" Y="26.379123046875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.222292480469" Y="26.2778515625" />
                  <Point X="3.263704101562" Y="26.214908203125" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.290578857422" Y="26.1933984375" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.381588134766" Y="26.1518984375" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803466797" Y="26.141171875" />
                  <Point X="4.697411621094" Y="26.302" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.852373535156" Y="26.307994140625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.947978027344" Y="25.894931640625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.920959960938" Y="25.286001953125" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.716293701172" Y="25.225423828125" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.614588378906" Y="25.15714453125" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.554426269531" Y="25.061373046875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.541041748047" Y="24.945955078125" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.841240234375" />
                  <Point X="3.574434814453" Y="24.831458984375" />
                  <Point X="3.622264648438" Y="24.77051171875" />
                  <Point X="3.649370361328" Y="24.750697265625" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167480469" Y="24.699611328125" />
                  <Point X="4.8614375" Y="24.3994375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.996905273438" Y="24.355115234375" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.937170898438" Y="23.98425390625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.620490966797" Y="23.874919921875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.369726806641" Y="23.896201171875" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.170268066406" Y="23.827048828125" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.062182617188" Y="23.662291015625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.070256591797" Y="23.461763671875" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.208078125" Y="22.516732421875" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.204134277344" Y="22.197861328125" />
                  <Point X="4.180842285156" Y="22.164767578125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="2.939159667969" Y="22.63356640625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.707456542969" Y="22.75208203125" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.464251220703" Y="22.7676875" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.275533935547" Y="22.640490234375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.194560058594" Y="22.423740234375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.902150390625" Y="21.064310546875" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.835293945312" Y="20.80978515625" />
                  <Point X="2.809261474609" Y="20.79293359375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.824887817383" Y="21.823232421875" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.641075195312" Y="22.038482421875" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.393569946289" Y="22.16131640625" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.131490234375" />
                  <Point X="1.140441894531" Y="22.11079296875" />
                  <Point X="0.985349060059" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.961014587402" Y="21.919771484375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.103252197266" Y="20.251232421875" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="0.994365478516" Y="20.0367578125" />
                  <Point X="0.970288818359" Y="20.0323828125" />
                  <Point X="0.860200378418" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#130" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.026724810296" Y="4.45486282851" Z="0.35" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="-0.874611309453" Y="4.996579006201" Z="0.35" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.35" />
                  <Point X="-1.64451149723" Y="4.79858653125" Z="0.35" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.35" />
                  <Point X="-1.744593273738" Y="4.723824062682" Z="0.35" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.35" />
                  <Point X="-1.735461077846" Y="4.35496228114" Z="0.35" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.35" />
                  <Point X="-1.825383767052" Y="4.305405682645" Z="0.35" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.35" />
                  <Point X="-1.92114726002" Y="4.342436477748" Z="0.35" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.35" />
                  <Point X="-1.961970738725" Y="4.385332735032" Z="0.35" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.35" />
                  <Point X="-2.696329089521" Y="4.297646561323" Z="0.35" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.35" />
                  <Point X="-3.296779332648" Y="3.85633054839" Z="0.35" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.35" />
                  <Point X="-3.326511948198" Y="3.703207326067" Z="0.35" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.35" />
                  <Point X="-2.995075107491" Y="3.066594627268" Z="0.35" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.35" />
                  <Point X="-3.046365776264" Y="3.002437791622" Z="0.35" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.35" />
                  <Point X="-3.128481838338" Y="3.000489591682" Z="0.35" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.35" />
                  <Point X="-3.230652022231" Y="3.053682000547" Z="0.35" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.35" />
                  <Point X="-4.150403369361" Y="2.919979938693" Z="0.35" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.35" />
                  <Point X="-4.500636703905" Y="2.344451952853" Z="0.35" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.35" />
                  <Point X="-4.429952166585" Y="2.173583903414" Z="0.35" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.35" />
                  <Point X="-3.670935315214" Y="1.561605154344" Z="0.35" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.35" />
                  <Point X="-3.68806127846" Y="1.502429270054" Z="0.35" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.35" />
                  <Point X="-3.744401122251" Y="1.477510979042" Z="0.35" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.35" />
                  <Point X="-3.899986903585" Y="1.494197416192" Z="0.35" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.35" />
                  <Point X="-4.951210397175" Y="1.117720352367" Z="0.35" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.35" />
                  <Point X="-5.048226730641" Y="0.528415849207" Z="0.35" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.35" />
                  <Point X="-4.855129178227" Y="0.391660418985" Z="0.35" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.35" />
                  <Point X="-3.552645951152" Y="0.032470805903" Z="0.35" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.35" />
                  <Point X="-3.540836127606" Y="0.004122160573" Z="0.35" />
                  <Point X="-3.539556741714" Y="0" Z="0.35" />
                  <Point X="-3.547528473529" Y="-0.025684790488" Z="0.35" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.35" />
                  <Point X="-3.572722643971" Y="-0.046405177231" Z="0.35" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.35" />
                  <Point X="-3.781758610545" Y="-0.104051637771" Z="0.35" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.35" />
                  <Point X="-4.993402472326" Y="-0.91457263431" Z="0.35" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.35" />
                  <Point X="-4.865678069745" Y="-1.44765745822" Z="0.35" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.35" />
                  <Point X="-4.621793955354" Y="-1.49152368237" Z="0.35" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.35" />
                  <Point X="-3.196338721814" Y="-1.320294264643" Z="0.35" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.35" />
                  <Point X="-3.199315306688" Y="-1.348083249892" Z="0.35" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.35" />
                  <Point X="-3.380513139315" Y="-1.490417568134" Z="0.35" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.35" />
                  <Point X="-4.249950864735" Y="-2.775813816832" Z="0.35" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.35" />
                  <Point X="-3.910243386673" Y="-3.236858009084" Z="0.35" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.35" />
                  <Point X="-3.683921037946" Y="-3.196974187582" Z="0.35" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.35" />
                  <Point X="-2.557890190096" Y="-2.570440279036" Z="0.35" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.35" />
                  <Point X="-2.658442825586" Y="-2.751157344546" Z="0.35" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.35" />
                  <Point X="-2.947100586424" Y="-4.133902733659" Z="0.35" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.35" />
                  <Point X="-2.511878857058" Y="-4.411919802538" Z="0.35" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.35" />
                  <Point X="-2.420015771506" Y="-4.409008690214" Z="0.35" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.35" />
                  <Point X="-2.003931515021" Y="-4.007922022062" Z="0.35" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.35" />
                  <Point X="-1.70148156626" Y="-4.001569706729" Z="0.35" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.35" />
                  <Point X="-1.457664855938" Y="-4.180650191783" Z="0.35" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.35" />
                  <Point X="-1.373249194929" Y="-4.47115012252" Z="0.35" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.35" />
                  <Point X="-1.371547204816" Y="-4.563885783409" Z="0.35" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.35" />
                  <Point X="-1.15829536637" Y="-4.945062023096" Z="0.35" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.35" />
                  <Point X="-0.859119843613" Y="-5.005717017592" Z="0.35" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="-0.762269578185" Y="-4.807012911453" Z="0.35" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="-0.276001847191" Y="-3.315496588454" Z="0.35" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="-0.035038364228" Y="-3.215113983659" Z="0.35" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.35" />
                  <Point X="0.218320715133" Y="-3.271998061629" Z="0.35" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.35" />
                  <Point X="0.394444012241" Y="-3.486149167314" Z="0.35" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.35" />
                  <Point X="0.472485282216" Y="-3.725523118363" Z="0.35" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.35" />
                  <Point X="0.973069700759" Y="-4.98553214805" Z="0.35" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.35" />
                  <Point X="1.152292198727" Y="-4.947182842649" Z="0.35" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.35" />
                  <Point X="1.146668507719" Y="-4.710962382299" Z="0.35" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.35" />
                  <Point X="1.003717761855" Y="-3.059566220629" Z="0.35" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.35" />
                  <Point X="1.166252850527" Y="-2.896371407481" Z="0.35" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.35" />
                  <Point X="1.391995665737" Y="-2.857192863956" Z="0.35" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.35" />
                  <Point X="1.607879931681" Y="-2.972296233301" Z="0.35" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.35" />
                  <Point X="1.779064068766" Y="-3.175925523405" Z="0.35" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.35" />
                  <Point X="2.830274619233" Y="-4.217759910361" Z="0.35" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.35" />
                  <Point X="3.020341740237" Y="-4.083808213405" Z="0.35" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.35" />
                  <Point X="2.939295634563" Y="-3.879409814613" Z="0.35" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.35" />
                  <Point X="2.237608194546" Y="-2.536092468884" Z="0.35" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.35" />
                  <Point X="2.313624185834" Y="-2.351516686487" Z="0.35" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.35" />
                  <Point X="2.481381691885" Y="-2.245277124133" Z="0.35" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.35" />
                  <Point X="2.692414334628" Y="-2.265839814944" Z="0.35" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.35" />
                  <Point X="2.908003782494" Y="-2.378453865711" Z="0.35" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.35" />
                  <Point X="4.215574890049" Y="-2.832729789052" Z="0.35" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.35" />
                  <Point X="4.377152344402" Y="-2.57603711776" Z="0.35" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.35" />
                  <Point X="4.232360033794" Y="-2.412319477663" Z="0.35" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.35" />
                  <Point X="3.106158459972" Y="-1.479916740266" Z="0.35" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.35" />
                  <Point X="3.105816217409" Y="-1.311011080133" Z="0.35" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.35" />
                  <Point X="3.202558442043" Y="-1.173637506752" Z="0.35" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.35" />
                  <Point X="3.374190370424" Y="-1.121378075896" Z="0.35" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.35" />
                  <Point X="3.607808601679" Y="-1.143371119" Z="0.35" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.35" />
                  <Point X="4.979761890586" Y="-0.995590790162" Z="0.35" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.35" />
                  <Point X="5.040190823224" Y="-0.621058187234" Z="0.35" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.35" />
                  <Point X="4.868222624575" Y="-0.52098604478" Z="0.35" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.35" />
                  <Point X="3.668236408749" Y="-0.174733191851" Z="0.35" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.35" />
                  <Point X="3.607612940144" Y="-0.106391834823" Z="0.35" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.35" />
                  <Point X="3.583993465527" Y="-0.013360780272" Z="0.35" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.35" />
                  <Point X="3.597377984898" Y="0.083249750967" Z="0.35" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.35" />
                  <Point X="3.647766498257" Y="0.157556903752" Z="0.35" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.35" />
                  <Point X="3.735159005603" Y="0.213415652391" Z="0.35" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.35" />
                  <Point X="3.927745192732" Y="0.268985887989" Z="0.35" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.35" />
                  <Point X="4.99122685215" Y="0.933903055015" Z="0.35" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.35" />
                  <Point X="4.894798568825" Y="1.35110153744" Z="0.35" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.35" />
                  <Point X="4.684729221671" Y="1.38285185157" Z="0.35" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.35" />
                  <Point X="3.381982612291" Y="1.232747564241" Z="0.35" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.35" />
                  <Point X="3.308949721004" Y="1.268249517055" Z="0.35" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.35" />
                  <Point X="3.257907128937" Y="1.336614573825" Z="0.35" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.35" />
                  <Point X="3.236035567458" Y="1.420506593018" Z="0.35" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.35" />
                  <Point X="3.252139324885" Y="1.498669876022" Z="0.35" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.35" />
                  <Point X="3.304907157347" Y="1.57427020939" Z="0.35" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.35" />
                  <Point X="3.469782219302" Y="1.705076470074" Z="0.35" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.35" />
                  <Point X="4.267105376629" Y="2.752954474234" Z="0.35" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.35" />
                  <Point X="4.034888172792" Y="3.083282441325" Z="0.35" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.35" />
                  <Point X="3.795871607025" Y="3.009467526794" Z="0.35" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.35" />
                  <Point X="2.440694866961" Y="2.248498318137" Z="0.35" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.35" />
                  <Point X="2.369767919447" Y="2.252742809346" Z="0.35" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.35" />
                  <Point X="2.305613368714" Y="2.290917246761" Z="0.35" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.35" />
                  <Point X="2.259841353474" Y="2.351411491669" Z="0.35" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.35" />
                  <Point X="2.246686893413" Y="2.419990524386" Z="0.35" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.35" />
                  <Point X="2.264029624521" Y="2.498774708921" Z="0.35" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.35" />
                  <Point X="2.386157826066" Y="2.71626747493" Z="0.35" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.35" />
                  <Point X="2.805376373065" Y="4.232138854149" Z="0.35" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.35" />
                  <Point X="2.410767608807" Y="4.468707427189" Z="0.35" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.35" />
                  <Point X="2.000971758119" Y="4.666677269479" Z="0.35" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.35" />
                  <Point X="1.57583027541" Y="4.826855655796" Z="0.35" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.35" />
                  <Point X="0.953028517914" Y="4.984385758182" Z="0.35" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.35" />
                  <Point X="0.285807541039" Y="5.066629216282" Z="0.35" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.35" />
                  <Point X="0.166519751257" Y="4.976584677441" Z="0.35" />
                  <Point X="0" Y="4.355124473572" Z="0.35" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>