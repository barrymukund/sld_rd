<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#135" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="873" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.634471435547" Y="21.2218671875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542362426758" Y="21.532625" />
                  <Point X="0.513273742676" Y="21.57453515625" />
                  <Point X="0.378634918213" Y="21.768525390625" />
                  <Point X="0.356748718262" Y="21.790982421875" />
                  <Point X="0.330493682861" Y="21.810224609375" />
                  <Point X="0.302492431641" Y="21.82433203125" />
                  <Point X="0.257479156494" Y="21.83830078125" />
                  <Point X="0.049133304596" Y="21.90296484375" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658312798" Y="21.907234375" />
                  <Point X="-0.036824542999" Y="21.90296484375" />
                  <Point X="-0.081837684631" Y="21.888994140625" />
                  <Point X="-0.290183685303" Y="21.82433203125" />
                  <Point X="-0.318184906006" Y="21.81022265625" />
                  <Point X="-0.344440093994" Y="21.790978515625" />
                  <Point X="-0.366323883057" Y="21.7685234375" />
                  <Point X="-0.395412536621" Y="21.726611328125" />
                  <Point X="-0.530051696777" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.880401855469" Y="20.25809375" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-1.079327636719" Y="20.1546484375" />
                  <Point X="-1.127652099609" Y="20.167080078125" />
                  <Point X="-1.24641784668" Y="20.19763671875" />
                  <Point X="-1.223530151367" Y="20.371486328125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.227262329102" Y="20.5378359375" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323645141602" Y="20.868796875" />
                  <Point X="-1.365087768555" Y="20.905140625" />
                  <Point X="-1.55690612793" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643026977539" Y="21.109033203125" />
                  <Point X="-1.698030273438" Y="21.112638671875" />
                  <Point X="-1.952616088867" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042656738281" Y="21.10519921875" />
                  <Point X="-2.088488525391" Y="21.074576171875" />
                  <Point X="-2.300623291016" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480148193359" Y="20.711509765625" />
                  <Point X="-2.801720947266" Y="20.910619140625" />
                  <Point X="-2.868603027344" Y="20.9621171875" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.556021484375" Y="22.094298828125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.411279052734" Y="22.353029296875" />
                  <Point X="-2.405863525391" Y="22.375560546875" />
                  <Point X="-2.406064208984" Y="22.398732421875" />
                  <Point X="-2.410209472656" Y="22.430212890625" />
                  <Point X="-2.416627929688" Y="22.454166015625" />
                  <Point X="-2.429026611328" Y="22.475640625" />
                  <Point X="-2.441692871094" Y="22.4921484375" />
                  <Point X="-2.449889404297" Y="22.50149609375" />
                  <Point X="-2.464155761719" Y="22.51576171875" />
                  <Point X="-2.489311767578" Y="22.533787109375" />
                  <Point X="-2.518140625" Y="22.54800390625" />
                  <Point X="-2.547758789062" Y="22.55698828125" />
                  <Point X="-2.578693359375" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-3.699501464844" Y="21.926626953125" />
                  <Point X="-3.818022949219" Y="21.858197265625" />
                  <Point X="-4.082864013672" Y="22.20614453125" />
                  <Point X="-4.130815917969" Y="22.286552734375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.3373671875" Y="23.323916015625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083060302734" Y="23.524125" />
                  <Point X="-3.0642421875" Y="23.55385546875" />
                  <Point X="-3.056159667969" Y="23.574208984375" />
                  <Point X="-3.052487304688" Y="23.585453125" />
                  <Point X="-3.046151611328" Y="23.609916015625" />
                  <Point X="-3.042037353516" Y="23.6413984375" />
                  <Point X="-3.042737060547" Y="23.664267578125" />
                  <Point X="-3.048884277344" Y="23.686306640625" />
                  <Point X="-3.060890136719" Y="23.7152890625" />
                  <Point X="-3.073294921875" Y="23.736771484375" />
                  <Point X="-3.090838623047" Y="23.754310546875" />
                  <Point X="-3.108046386719" Y="23.767509765625" />
                  <Point X="-3.117681640625" Y="23.77400390625" />
                  <Point X="-3.139459228516" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608154297" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.570479980469" Y="23.629392578125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.846765136719" Y="24.0960546875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.796489746094" Y="24.708955078125" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.513689697266" Y="24.7870390625" />
                  <Point X="-3.493190917969" Y="24.797681640625" />
                  <Point X="-3.482798095703" Y="24.803951171875" />
                  <Point X="-3.459975830078" Y="24.819791015625" />
                  <Point X="-3.436020263672" Y="24.841318359375" />
                  <Point X="-3.415780029297" Y="24.870173828125" />
                  <Point X="-3.4067109375" Y="24.89020703125" />
                  <Point X="-3.402525390625" Y="24.9012265625" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.389474365234" Y="24.954478515625" />
                  <Point X="-3.390129150391" Y="24.9867578125" />
                  <Point X="-3.394019775391" Y="25.006875" />
                  <Point X="-3.396560791016" Y="25.016994140625" />
                  <Point X="-3.404168212891" Y="25.041505859375" />
                  <Point X="-3.417483398438" Y="25.070830078125" />
                  <Point X="-3.439088623047" Y="25.098982421875" />
                  <Point X="-3.455837890625" Y="25.113921875" />
                  <Point X="-3.464906494141" Y="25.1210703125" />
                  <Point X="-3.487728759766" Y="25.13691015625" />
                  <Point X="-3.501923095703" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.753027832031" Y="25.484787109375" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.824488769531" Y="25.97696875" />
                  <Point X="-4.798946289062" Y="26.071228515625" />
                  <Point X="-4.703550292969" Y="26.423267578125" />
                  <Point X="-3.954631103516" Y="26.324669921875" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744991210938" Y="26.299341796875" />
                  <Point X="-3.723435546875" Y="26.3012265625" />
                  <Point X="-3.703155029297" Y="26.3052578125" />
                  <Point X="-3.692221923828" Y="26.308703125" />
                  <Point X="-3.641708984375" Y="26.324630859375" />
                  <Point X="-3.622775878906" Y="26.332962890625" />
                  <Point X="-3.604032226562" Y="26.34378515625" />
                  <Point X="-3.587352783203" Y="26.356015625" />
                  <Point X="-3.573715576172" Y="26.37156640625" />
                  <Point X="-3.561301513672" Y="26.389294921875" />
                  <Point X="-3.551350830078" Y="26.407431640625" />
                  <Point X="-3.546971923828" Y="26.41800390625" />
                  <Point X="-3.526703369141" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532049316406" Y="26.589376953125" />
                  <Point X="-3.537333007813" Y="26.59952734375" />
                  <Point X="-3.5617890625" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.302018066406" Y="27.23162890625" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.081155029297" Y="27.733654296875" />
                  <Point X="-4.013498291016" Y="27.820619140625" />
                  <Point X="-3.750503417969" Y="28.158662109375" />
                  <Point X="-3.322713867188" Y="27.911677734375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167086181641" Y="27.82983203125" />
                  <Point X="-3.146796142578" Y="27.825794921875" />
                  <Point X="-3.131596923828" Y="27.82446484375" />
                  <Point X="-3.061246826172" Y="27.818310546875" />
                  <Point X="-3.040560302734" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946076171875" Y="27.86023046875" />
                  <Point X="-2.935287597656" Y="27.87101953125" />
                  <Point X="-2.885352539062" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.844765625" Y="28.051318359375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.179934082031" Y="28.718708984375" />
                  <Point X="-3.179086914062" Y="28.7278515625" />
                  <Point X="-2.700622558594" Y="29.094685546875" />
                  <Point X="-2.59407421875" Y="29.153880859375" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.078723876953" Y="29.27604296875" />
                  <Point X="-2.04319519043" Y="29.2297421875" />
                  <Point X="-2.028892333984" Y="29.21480078125" />
                  <Point X="-2.012312988281" Y="29.200888671875" />
                  <Point X="-1.995115356445" Y="29.189396484375" />
                  <Point X="-1.978198974609" Y="29.18058984375" />
                  <Point X="-1.899899414062" Y="29.139828125" />
                  <Point X="-1.880625366211" Y="29.13233203125" />
                  <Point X="-1.859718505859" Y="29.126728515625" />
                  <Point X="-1.839268920898" Y="29.123580078125" />
                  <Point X="-1.818622680664" Y="29.12493359375" />
                  <Point X="-1.797306884766" Y="29.128693359375" />
                  <Point X="-1.777451416016" Y="29.134482421875" />
                  <Point X="-1.759831787109" Y="29.14178125" />
                  <Point X="-1.678277587891" Y="29.1755625" />
                  <Point X="-1.660145751953" Y="29.185509765625" />
                  <Point X="-1.642416259766" Y="29.197923828125" />
                  <Point X="-1.626864013672" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.589745117188" Y="29.284111328125" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584202026367" Y="29.6318984375" />
                  <Point X="-1.569033081055" Y="29.636150390625" />
                  <Point X="-0.949624938965" Y="29.80980859375" />
                  <Point X="-0.820468933105" Y="29.82492578125" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.168236175537" Y="29.414447265625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082114021301" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155905724" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425994873" Y="29.208806640625" />
                  <Point X="0.115583656311" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.305126922607" Y="29.879380859375" />
                  <Point X="0.307419769287" Y="29.8879375" />
                  <Point X="0.844044799805" Y="29.83173828125" />
                  <Point X="0.950909179688" Y="29.8059375" />
                  <Point X="1.48102331543" Y="29.677951171875" />
                  <Point X="1.549368041992" Y="29.653162109375" />
                  <Point X="1.894650756836" Y="29.52792578125" />
                  <Point X="1.961912353516" Y="29.496470703125" />
                  <Point X="2.294562011719" Y="29.340900390625" />
                  <Point X="2.359591064453" Y="29.303015625" />
                  <Point X="2.680968017578" Y="29.11578125" />
                  <Point X="2.742267089844" Y="29.0721875" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.300662353516" Y="27.8162421875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.139762939453" Y="27.5337421875" />
                  <Point X="2.130947021484" Y="27.50739453125" />
                  <Point X="2.129262207031" Y="27.501791015625" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108383300781" Y="27.410771484375" />
                  <Point X="2.108532958984" Y="27.37953515625" />
                  <Point X="2.109215087891" Y="27.3686171875" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.1400703125" Y="27.24747265625" />
                  <Point X="2.147702392578" Y="27.236224609375" />
                  <Point X="2.183028076172" Y="27.1841640625" />
                  <Point X="2.199996582031" Y="27.16521875" />
                  <Point X="2.224543945312" Y="27.14428515625" />
                  <Point X="2.232845703125" Y="27.137958984375" />
                  <Point X="2.284906738281" Y="27.1026328125" />
                  <Point X="2.304953125" Y="27.09226953125" />
                  <Point X="2.327041259766" Y="27.08400390625" />
                  <Point X="2.348970947266" Y="27.07866015625" />
                  <Point X="2.361305419922" Y="27.077173828125" />
                  <Point X="2.418395751953" Y="27.0702890625" />
                  <Point X="2.44434375" Y="27.070732421875" />
                  <Point X="2.47751171875" Y="27.075884765625" />
                  <Point X="2.487472412109" Y="27.077984375" />
                  <Point X="2.553494628906" Y="27.095640625" />
                  <Point X="2.565289550781" Y="27.099640625" />
                  <Point X="2.588533691406" Y="27.110146484375" />
                  <Point X="3.815771972656" Y="27.818693359375" />
                  <Point X="3.967325439453" Y="27.906193359375" />
                  <Point X="4.123270019531" Y="27.68946484375" />
                  <Point X="4.157439941406" Y="27.632998046875" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.432329589844" Y="26.823103515625" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.216647949219" Y="26.655095703125" />
                  <Point X="3.197137451172" Y="26.632453125" />
                  <Point X="3.193708251953" Y="26.628234375" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.133427001953" Y="26.543841796875" />
                  <Point X="3.121133544922" Y="26.513216796875" />
                  <Point X="3.117805908203" Y="26.503412109375" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739746094" Y="26.371765625" />
                  <Point X="3.10087890625" Y="26.356552734375" />
                  <Point X="3.115408691406" Y="26.2861328125" />
                  <Point X="3.124108642578" Y="26.261607421875" />
                  <Point X="3.139844726562" Y="26.23125390625" />
                  <Point X="3.144820556641" Y="26.222763671875" />
                  <Point X="3.184340087891" Y="26.1626953125" />
                  <Point X="3.198892578125" Y="26.145451171875" />
                  <Point X="3.216135986328" Y="26.129361328125" />
                  <Point X="3.234346679688" Y="26.11603515625" />
                  <Point X="3.246719482422" Y="26.1090703125" />
                  <Point X="3.303988769531" Y="26.07683203125" />
                  <Point X="3.328722412109" Y="26.06716796875" />
                  <Point X="3.363427978516" Y="26.058958984375" />
                  <Point X="3.372849365234" Y="26.0572265625" />
                  <Point X="3.450281005859" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.653998046875" Y="26.200462890625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.856703613281" Y="25.863650390625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.947555664062" Y="25.39148046875" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.698089355469" Y="25.322498046875" />
                  <Point X="3.669573730469" Y="25.307990234375" />
                  <Point X="3.665112060547" Y="25.305568359375" />
                  <Point X="3.589037841797" Y="25.26159765625" />
                  <Point X="3.568302001953" Y="25.245404296875" />
                  <Point X="3.544127929688" Y="25.22041796875" />
                  <Point X="3.537669433594" Y="25.21301171875" />
                  <Point X="3.492024902344" Y="25.154849609375" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.460393554688" Y="25.075439453125" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443877197266" Y="24.9694765625" />
                  <Point X="3.447164794922" Y="24.93350390625" />
                  <Point X="3.448466064453" Y="24.92428125" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.4920234375" Y="24.782591796875" />
                  <Point X="3.501884765625" Y="24.770025390625" />
                  <Point X="3.547529296875" Y="24.71186328125" />
                  <Point X="3.567118652344" Y="24.69315625" />
                  <Point X="3.597866455078" Y="24.671236328125" />
                  <Point X="3.605471191406" Y="24.66634375" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.785666015625" Y="24.32138671875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.841227539062" Y="23.990822265625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.692249267578" Y="23.96129296875" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374657714844" Y="23.994490234375" />
                  <Point X="3.342400146484" Y="23.987478515625" />
                  <Point X="3.193093505859" Y="23.95502734375" />
                  <Point X="3.163973876953" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112395996094" Y="23.906037109375" />
                  <Point X="3.092898193359" Y="23.8825859375" />
                  <Point X="3.002651855469" Y="23.774048828125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.966963134766" Y="23.664265625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976451660156" Y="23.432" />
                  <Point X="2.994303466797" Y="23.404234375" />
                  <Point X="3.076931884766" Y="23.2757109375" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.102747070312" Y="22.477810546875" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.124811035156" Y="22.25021484375" />
                  <Point X="4.096280273438" Y="22.209677734375" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.039526611328" Y="22.685314453125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.715832763672" Y="22.847107421875" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444834472656" Y="22.864822265625" />
                  <Point X="2.412940185547" Y="22.848037109375" />
                  <Point X="2.26531640625" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531494141" Y="22.709560546875" />
                  <Point X="2.187745849609" Y="22.677666015625" />
                  <Point X="2.110052490234" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.102608642578" Y="22.398349609375" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.789355224609" Y="21.06967578125" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781839111328" Y="20.88834765625" />
                  <Point X="2.749953369141" Y="20.867708984375" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="1.940598510742" Y="21.82848828125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.684059814453" Y="22.123787109375" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448366088867" Y="22.2565625" />
                  <Point X="1.417100341797" Y="22.258880859375" />
                  <Point X="1.375688842773" Y="22.2550703125" />
                  <Point X="1.184013549805" Y="22.23743359375" />
                  <Point X="1.156363037109" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104595336914" Y="22.204537109375" />
                  <Point X="1.072618530273" Y="22.17794921875" />
                  <Point X="0.924611633301" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.866063537598" Y="21.930203125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="1.000415161133" Y="20.304533203125" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.975716430664" Y="20.12992578125" />
                  <Point X="0.946208251953" Y="20.124564453125" />
                  <Point X="0.929315429688" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058437988281" Y="20.2473671875" />
                  <Point X="-1.103983642578" Y="20.259083984375" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.129342895508" Y="20.3590859375" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.134087646484" Y="20.556369140625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230573486328" Y="20.905138671875" />
                  <Point X="-1.250208740234" Y="20.929064453125" />
                  <Point X="-1.26100769043" Y="20.94022265625" />
                  <Point X="-1.302450195312" Y="20.97656640625" />
                  <Point X="-1.494268554688" Y="21.144787109375" />
                  <Point X="-1.506739990234" Y="21.15403515625" />
                  <Point X="-1.533023071289" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315551758" Y="21.194525390625" />
                  <Point X="-1.621456054688" Y="21.201552734375" />
                  <Point X="-1.636813110352" Y="21.203830078125" />
                  <Point X="-1.69181640625" Y="21.207435546875" />
                  <Point X="-1.94640222168" Y="21.22412109375" />
                  <Point X="-1.961927124023" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053668457031" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095435058594" Y="21.184189453125" />
                  <Point X="-2.141266845703" Y="21.15356640625" />
                  <Point X="-2.353401611328" Y="21.011822265625" />
                  <Point X="-2.359680419922" Y="21.007244140625" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201416016" Y="20.837521484375" />
                  <Point X="-2.74760546875" Y="20.988849609375" />
                  <Point X="-2.810645019531" Y="21.037388671875" />
                  <Point X="-2.980863037109" Y="21.16844921875" />
                  <Point X="-2.473749023438" Y="22.046798828125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.336204345703" Y="22.28651953125" />
                  <Point X="-2.323722167969" Y="22.31616796875" />
                  <Point X="-2.318909667969" Y="22.330828125" />
                  <Point X="-2.313494140625" Y="22.353359375" />
                  <Point X="-2.3108671875" Y="22.3763828125" />
                  <Point X="-2.311067871094" Y="22.3995546875" />
                  <Point X="-2.311877197266" Y="22.411134765625" />
                  <Point X="-2.316022460938" Y="22.442615234375" />
                  <Point X="-2.318446777344" Y="22.45480078125" />
                  <Point X="-2.324865234375" Y="22.47875390625" />
                  <Point X="-2.334355957031" Y="22.501666015625" />
                  <Point X="-2.346754638672" Y="22.523140625" />
                  <Point X="-2.353656738281" Y="22.533470703125" />
                  <Point X="-2.366322998047" Y="22.549978515625" />
                  <Point X="-2.382716064453" Y="22.568673828125" />
                  <Point X="-2.396982421875" Y="22.582939453125" />
                  <Point X="-2.408822753906" Y="22.592984375" />
                  <Point X="-2.433978759766" Y="22.611009765625" />
                  <Point X="-2.447294433594" Y="22.618990234375" />
                  <Point X="-2.476123291016" Y="22.63320703125" />
                  <Point X="-2.490564208984" Y="22.6389140625" />
                  <Point X="-2.520182373047" Y="22.6478984375" />
                  <Point X="-2.550870117188" Y="22.6519375" />
                  <Point X="-2.5818046875" Y="22.650923828125" />
                  <Point X="-2.597228759766" Y="22.6491484375" />
                  <Point X="-2.628754638672" Y="22.642876953125" />
                  <Point X="-2.643685302734" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.686683837891" Y="22.621072265625" />
                  <Point X="-3.747001464844" Y="22.0088984375" />
                  <Point X="-3.793087158203" Y="21.982291015625" />
                  <Point X="-4.004022705078" Y="22.259416015625" />
                  <Point X="-4.049223144531" Y="22.3352109375" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.279534912109" Y="23.248546875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039154785156" Y="23.433935546875" />
                  <Point X="-3.016260498047" Y="23.456576171875" />
                  <Point X="-3.002788818359" Y="23.47331640625" />
                  <Point X="-2.983970703125" Y="23.503046875" />
                  <Point X="-2.975948974609" Y="23.51879296875" />
                  <Point X="-2.967866455078" Y="23.539146484375" />
                  <Point X="-2.960521728516" Y="23.561634765625" />
                  <Point X="-2.954186035156" Y="23.58609765625" />
                  <Point X="-2.951952636719" Y="23.59760546875" />
                  <Point X="-2.947838378906" Y="23.629087890625" />
                  <Point X="-2.947081787109" Y="23.644302734375" />
                  <Point X="-2.947781494141" Y="23.667171875" />
                  <Point X="-2.951229980469" Y="23.689791015625" />
                  <Point X="-2.957377197266" Y="23.711830078125" />
                  <Point X="-2.961116699219" Y="23.7226640625" />
                  <Point X="-2.973122558594" Y="23.751646484375" />
                  <Point X="-2.978620849609" Y="23.762794921875" />
                  <Point X="-2.991025634766" Y="23.78427734375" />
                  <Point X="-3.006128662109" Y="23.803955078125" />
                  <Point X="-3.023672363281" Y="23.821494140625" />
                  <Point X="-3.03301953125" Y="23.829689453125" />
                  <Point X="-3.050227294922" Y="23.842888671875" />
                  <Point X="-3.069497802734" Y="23.855876953125" />
                  <Point X="-3.091275390625" Y="23.868693359375" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.134699462891" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891142578125" />
                  <Point X="-3.181687255859" Y="23.897623046875" />
                  <Point X="-3.197304443359" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.582879882812" Y="23.723580078125" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.740763183594" Y="24.02588671875" />
                  <Point X="-4.752722167969" Y="24.10950390625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.771901855469" Y="24.61719140625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.4984921875" Y="24.69103125" />
                  <Point X="-3.479305908203" Y="24.69848046875" />
                  <Point X="-3.469915527344" Y="24.702724609375" />
                  <Point X="-3.449416748047" Y="24.7133671875" />
                  <Point X="-3.428631103516" Y="24.72590625" />
                  <Point X="-3.405808837891" Y="24.74174609375" />
                  <Point X="-3.396477539062" Y="24.749130859375" />
                  <Point X="-3.372521972656" Y="24.770658203125" />
                  <Point X="-3.358245605469" Y="24.786763671875" />
                  <Point X="-3.338005371094" Y="24.815619140625" />
                  <Point X="-3.329235351562" Y="24.830994140625" />
                  <Point X="-3.320166259766" Y="24.85102734375" />
                  <Point X="-3.311795166016" Y="24.87306640625" />
                  <Point X="-3.304187744141" Y="24.897576171875" />
                  <Point X="-3.301577392578" Y="24.90805859375" />
                  <Point X="-3.296133789062" Y="24.93680078125" />
                  <Point X="-3.294493896484" Y="24.956404296875" />
                  <Point X="-3.295148681641" Y="24.98868359375" />
                  <Point X="-3.296857421875" Y="25.004796875" />
                  <Point X="-3.300748046875" Y="25.0249140625" />
                  <Point X="-3.305830078125" Y="25.04515234375" />
                  <Point X="-3.3134375" Y="25.0696640625" />
                  <Point X="-3.317667724609" Y="25.080783203125" />
                  <Point X="-3.330982910156" Y="25.110107421875" />
                  <Point X="-3.342118896484" Y="25.12866796875" />
                  <Point X="-3.363724121094" Y="25.1568203125" />
                  <Point X="-3.375853027344" Y="25.16987890625" />
                  <Point X="-3.392602294922" Y="25.184818359375" />
                  <Point X="-3.410739501953" Y="25.199115234375" />
                  <Point X="-3.433561767578" Y="25.214955078125" />
                  <Point X="-3.440483154297" Y="25.219328125" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496565429688" Y="25.24563671875" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.728439941406" Y="25.57655078125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731332519531" Y="25.95751953125" />
                  <Point X="-4.707252929688" Y="26.046380859375" />
                  <Point X="-4.633584960938" Y="26.318236328125" />
                  <Point X="-3.96703125" Y="26.230482421875" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.767738769531" Y="26.20481640625" />
                  <Point X="-3.747063720703" Y="26.204365234375" />
                  <Point X="-3.736716308594" Y="26.204703125" />
                  <Point X="-3.715160644531" Y="26.206587890625" />
                  <Point X="-3.704914306641" Y="26.208048828125" />
                  <Point X="-3.684633789062" Y="26.212080078125" />
                  <Point X="-3.663653076172" Y="26.218099609375" />
                  <Point X="-3.613140136719" Y="26.23402734375" />
                  <Point X="-3.603443115234" Y="26.237677734375" />
                  <Point X="-3.584510009766" Y="26.246009765625" />
                  <Point X="-3.575273925781" Y="26.25069140625" />
                  <Point X="-3.556530273438" Y="26.261513671875" />
                  <Point X="-3.547855957031" Y="26.267173828125" />
                  <Point X="-3.531176513672" Y="26.279404296875" />
                  <Point X="-3.515927001953" Y="26.29337890625" />
                  <Point X="-3.502289794922" Y="26.3089296875" />
                  <Point X="-3.495896972656" Y="26.317076171875" />
                  <Point X="-3.483482910156" Y="26.3348046875" />
                  <Point X="-3.478013427734" Y="26.343599609375" />
                  <Point X="-3.468062744141" Y="26.361736328125" />
                  <Point X="-3.459202636719" Y="26.381650390625" />
                  <Point X="-3.438934082031" Y="26.43058203125" />
                  <Point X="-3.435498291016" Y="26.440353515625" />
                  <Point X="-3.429710693359" Y="26.4602109375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.43601171875" Y="26.604529296875" />
                  <Point X="-3.443507568359" Y="26.623806640625" />
                  <Point X="-3.453066162109" Y="26.643392578125" />
                  <Point X="-3.477522216797" Y="26.69037109375" />
                  <Point X="-3.482800537109" Y="26.699287109375" />
                  <Point X="-3.494293212891" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.22761328125" Y="27.29428125" />
                  <Point X="-4.00229296875" Y="27.68030859375" />
                  <Point X="-3.938517089844" Y="27.76228515625" />
                  <Point X="-3.726336425781" Y="28.035013671875" />
                  <Point X="-3.370213867188" Y="27.82940625" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923583984" Y="27.757720703125" />
                  <Point X="-3.225995361328" Y="27.749390625" />
                  <Point X="-3.216300537109" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.73923046875" />
                  <Point X="-3.185625" Y="27.736658203125" />
                  <Point X="-3.165334960938" Y="27.73262109375" />
                  <Point X="-3.139878662109" Y="27.729826171875" />
                  <Point X="-3.069528564453" Y="27.723671875" />
                  <Point X="-3.059175292969" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155517578" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902745117188" Y="27.773193359375" />
                  <Point X="-2.886611572266" Y="27.786142578125" />
                  <Point X="-2.868110839844" Y="27.803845703125" />
                  <Point X="-2.81817578125" Y="27.853779296875" />
                  <Point X="-2.811264404297" Y="27.861490234375" />
                  <Point X="-2.798320800781" Y="27.8776171875" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.750127197266" Y="28.05959765625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059387207031" Y="28.699916015625" />
                  <Point X="-2.648370361328" Y="29.015037109375" />
                  <Point X="-2.547937011719" Y="29.0708359375" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.154092773438" Y="29.2182109375" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111820800781" Y="29.164048828125" />
                  <Point X="-2.097517822266" Y="29.149107421875" />
                  <Point X="-2.089958251953" Y="29.14202734375" />
                  <Point X="-2.07337890625" Y="29.128115234375" />
                  <Point X="-2.065095703125" Y="29.12190234375" />
                  <Point X="-2.047898193359" Y="29.11041015625" />
                  <Point X="-2.022067138672" Y="29.09632421875" />
                  <Point X="-1.943767578125" Y="29.0555625" />
                  <Point X="-1.934334350586" Y="29.0512890625" />
                  <Point X="-1.915060302734" Y="29.04379296875" />
                  <Point X="-1.905219604492" Y="29.0405703125" />
                  <Point X="-1.88431262207" Y="29.034966796875" />
                  <Point X="-1.874174438477" Y="29.032833984375" />
                  <Point X="-1.853724853516" Y="29.029685546875" />
                  <Point X="-1.833054321289" Y="29.028783203125" />
                  <Point X="-1.812408081055" Y="29.03013671875" />
                  <Point X="-1.80212109375" Y="29.031376953125" />
                  <Point X="-1.780805297852" Y="29.03513671875" />
                  <Point X="-1.770715820312" Y="29.037490234375" />
                  <Point X="-1.750860351562" Y="29.043279296875" />
                  <Point X="-1.723474609375" Y="29.054013671875" />
                  <Point X="-1.641920410156" Y="29.087794921875" />
                  <Point X="-1.632584350586" Y="29.0922734375" />
                  <Point X="-1.614452514648" Y="29.102220703125" />
                  <Point X="-1.605656738281" Y="29.107689453125" />
                  <Point X="-1.587927246094" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564226928711" Y="29.14013671875" />
                  <Point X="-1.550251708984" Y="29.155388671875" />
                  <Point X="-1.538020263672" Y="29.1720703125" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.51685546875" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.499141967773" Y="29.255544921875" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465991210938" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.931162841797" Y="29.716322265625" />
                  <Point X="-0.809424987793" Y="29.7305703125" />
                  <Point X="-0.365222503662" Y="29.78255859375" />
                  <Point X="-0.25999911499" Y="29.389859375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896308899" Y="29.1250234375" />
                  <Point X="-0.099600372314" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912307739" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763122559" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194573242188" Y="29.1786171875" />
                  <Point X="0.21243132019" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190856934" Y="29.7850078125" />
                  <Point X="0.827879333496" Y="29.737912109375" />
                  <Point X="0.92861340332" Y="29.713591796875" />
                  <Point X="1.453590209961" Y="29.586845703125" />
                  <Point X="1.516975830078" Y="29.56385546875" />
                  <Point X="1.858257568359" Y="29.4400703125" />
                  <Point X="1.921668579102" Y="29.410416015625" />
                  <Point X="2.250437988281" Y="29.25666015625" />
                  <Point X="2.311769287109" Y="29.2209296875" />
                  <Point X="2.629428710938" Y="29.035861328125" />
                  <Point X="2.687209472656" Y="28.99476953125" />
                  <Point X="2.817779296875" Y="28.901916015625" />
                  <Point X="2.218389892578" Y="27.8637421875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.060963867188" Y="27.590115234375" />
                  <Point X="2.049672363281" Y="27.56388671875" />
                  <Point X="2.040856445312" Y="27.5375390625" />
                  <Point X="2.037486816406" Y="27.52633203125" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017387207031" Y="27.44791796875" />
                  <Point X="2.014163574219" Y="27.422921875" />
                  <Point X="2.013384399414" Y="27.41031640625" />
                  <Point X="2.013534057617" Y="27.379080078125" />
                  <Point X="2.01489831543" Y="27.357244140625" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318847656" Y="27.223888671875" />
                  <Point X="2.055680664062" Y="27.203845703125" />
                  <Point X="2.069090576172" Y="27.182884765625" />
                  <Point X="2.104416259766" Y="27.13082421875" />
                  <Point X="2.112262451172" Y="27.120783203125" />
                  <Point X="2.129230957031" Y="27.101837890625" />
                  <Point X="2.138353271484" Y="27.09293359375" />
                  <Point X="2.162900634766" Y="27.072" />
                  <Point X="2.179504150391" Y="27.05934765625" />
                  <Point X="2.231565185547" Y="27.024021484375" />
                  <Point X="2.241280029297" Y="27.0182421875" />
                  <Point X="2.261326416016" Y="27.00787890625" />
                  <Point X="2.271657958984" Y="27.003294921875" />
                  <Point X="2.29374609375" Y="26.995029296875" />
                  <Point X="2.304550048828" Y="26.991705078125" />
                  <Point X="2.326479736328" Y="26.986361328125" />
                  <Point X="2.349939941406" Y="26.98285546875" />
                  <Point X="2.407030273438" Y="26.975970703125" />
                  <Point X="2.420018798828" Y="26.975302734375" />
                  <Point X="2.445966796875" Y="26.97574609375" />
                  <Point X="2.458926269531" Y="26.976857421875" />
                  <Point X="2.492094238281" Y="26.982009765625" />
                  <Point X="2.512015625" Y="26.986208984375" />
                  <Point X="2.578037841797" Y="27.003865234375" />
                  <Point X="2.584005126953" Y="27.005673828125" />
                  <Point X="2.604416503906" Y="27.013072265625" />
                  <Point X="2.627660644531" Y="27.023578125" />
                  <Point X="2.636033691406" Y="27.027875" />
                  <Point X="3.863271972656" Y="27.736421875" />
                  <Point X="3.940403076172" Y="27.780953125" />
                  <Point X="4.043952392578" Y="27.63704296875" />
                  <Point X="4.076162841797" Y="27.583814453125" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.374497314453" Y="26.89847265625" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.165541503906" Y="26.737505859375" />
                  <Point X="3.144679931641" Y="26.717109375" />
                  <Point X="3.125169433594" Y="26.694466796875" />
                  <Point X="3.118311035156" Y="26.686029296875" />
                  <Point X="3.070794677734" Y="26.624041015625" />
                  <Point X="3.063649169922" Y="26.613275390625" />
                  <Point X="3.050884277344" Y="26.59087109375" />
                  <Point X="3.045264892578" Y="26.579232421875" />
                  <Point X="3.032971435547" Y="26.548607421875" />
                  <Point X="3.026316162109" Y="26.528998046875" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.007839111328" Y="26.337353515625" />
                  <Point X="3.022368896484" Y="26.26693359375" />
                  <Point X="3.025875" Y="26.254373046875" />
                  <Point X="3.034574951172" Y="26.22984765625" />
                  <Point X="3.039768798828" Y="26.2178828125" />
                  <Point X="3.055504882812" Y="26.187529296875" />
                  <Point X="3.065456542969" Y="26.170548828125" />
                  <Point X="3.104976074219" Y="26.11048046875" />
                  <Point X="3.111738037109" Y="26.10142578125" />
                  <Point X="3.126290527344" Y="26.084181640625" />
                  <Point X="3.134081054688" Y="26.0759921875" />
                  <Point X="3.151324462891" Y="26.05990234375" />
                  <Point X="3.160033935547" Y="26.0526953125" />
                  <Point X="3.178244628906" Y="26.039369140625" />
                  <Point X="3.200118652344" Y="26.02628515625" />
                  <Point X="3.257387939453" Y="25.994046875" />
                  <Point X="3.269415283203" Y="25.988345703125" />
                  <Point X="3.294148925781" Y="25.978681640625" />
                  <Point X="3.306855224609" Y="25.97471875" />
                  <Point X="3.341560791016" Y="25.966509765625" />
                  <Point X="3.360403564453" Y="25.963044921875" />
                  <Point X="3.437835205078" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.666397949219" Y="26.106275390625" />
                  <Point X="4.704704589844" Y="26.111318359375" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.762834472656" Y="25.84903515625" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.922967773438" Y="25.483244140625" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.682559570313" Y="25.4182890625" />
                  <Point X="3.655011230469" Y="25.407169921875" />
                  <Point X="3.626495605469" Y="25.392662109375" />
                  <Point X="3.617572265625" Y="25.387818359375" />
                  <Point X="3.541498046875" Y="25.34384765625" />
                  <Point X="3.530566162109" Y="25.336470703125" />
                  <Point X="3.509830322266" Y="25.32027734375" />
                  <Point X="3.500026367188" Y="25.3114609375" />
                  <Point X="3.475852294922" Y="25.286474609375" />
                  <Point X="3.462935302734" Y="25.271662109375" />
                  <Point X="3.417290771484" Y="25.2135" />
                  <Point X="3.410854003906" Y="25.204208984375" />
                  <Point X="3.399129394531" Y="25.184927734375" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.367089355469" Y="25.09330859375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.35029296875" Y="25.000650390625" />
                  <Point X="3.348991455078" Y="24.974134765625" />
                  <Point X="3.349271484375" Y="24.960830078125" />
                  <Point X="3.352559082031" Y="24.924857421875" />
                  <Point X="3.355161621094" Y="24.906412109375" />
                  <Point X="3.370376464844" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380004638672" Y="24.794513671875" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399126953125" Y="24.752517578125" />
                  <Point X="3.410850341797" Y="24.733236328125" />
                  <Point X="3.427149169922" Y="24.711376953125" />
                  <Point X="3.472793701172" Y="24.65321484375" />
                  <Point X="3.481919189453" Y="24.643158203125" />
                  <Point X="3.501508544922" Y="24.624451171875" />
                  <Point X="3.511972412109" Y="24.61580078125" />
                  <Point X="3.542720214844" Y="24.593880859375" />
                  <Point X="3.5579296875" Y="24.584095703125" />
                  <Point X="3.63400390625" Y="24.540123046875" />
                  <Point X="3.639489013672" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.761078125" Y="24.229623046875" />
                  <Point X="4.784876464844" Y="24.22324609375" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.748608398438" Y="24.01195703125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.704649169922" Y="24.05548046875" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097412109" Y="24.09195703125" />
                  <Point X="3.366719482422" Y="24.089158203125" />
                  <Point X="3.354479003906" Y="24.087322265625" />
                  <Point X="3.322221435547" Y="24.080310546875" />
                  <Point X="3.172914794922" Y="24.047859375" />
                  <Point X="3.15787109375" Y="24.043255859375" />
                  <Point X="3.128751464844" Y="24.031630859375" />
                  <Point X="3.114675537109" Y="24.024609375" />
                  <Point X="3.086848876953" Y="24.007716796875" />
                  <Point X="3.074122314453" Y="23.998466796875" />
                  <Point X="3.05037109375" Y="23.977994140625" />
                  <Point X="3.039346435547" Y="23.966771484375" />
                  <Point X="3.019848632813" Y="23.9433203125" />
                  <Point X="2.929602294922" Y="23.834783203125" />
                  <Point X="2.921324462891" Y="23.8231484375" />
                  <Point X="2.906605957031" Y="23.79876953125" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.872362792969" Y="23.672970703125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.423330078125" />
                  <Point X="2.889159179688" Y="23.394515625" />
                  <Point X="2.89654296875" Y="23.380623046875" />
                  <Point X="2.914394775391" Y="23.352857421875" />
                  <Point X="2.997023193359" Y="23.224333984375" />
                  <Point X="3.001745117188" Y="23.217642578125" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="4.044914794922" Y="22.40244140625" />
                  <Point X="4.087170898438" Y="22.370017578125" />
                  <Point X="4.045494873047" Y="22.302578125" />
                  <Point X="4.018592773438" Y="22.26435546875" />
                  <Point X="4.001274169922" Y="22.239748046875" />
                  <Point X="3.087026611328" Y="22.767587890625" />
                  <Point X="2.848454589844" Y="22.905328125" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.732716796875" Y="22.940595703125" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444845947266" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400591308594" Y="22.948890625" />
                  <Point X="2.368697021484" Y="22.93210546875" />
                  <Point X="2.221073242188" Y="22.854412109375" />
                  <Point X="2.208970947266" Y="22.846830078125" />
                  <Point X="2.186040039062" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144941162109" Y="22.78883984375" />
                  <Point X="2.128046875" Y="22.765908203125" />
                  <Point X="2.120463134766" Y="22.7538046875" />
                  <Point X="2.103677490234" Y="22.72191015625" />
                  <Point X="2.02598425293" Y="22.574287109375" />
                  <Point X="2.019835571289" Y="22.559806640625" />
                  <Point X="2.010012084961" Y="22.53003125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.0021875" Y="22.419859375" />
                  <Point X="2.009120849609" Y="22.381466796875" />
                  <Point X="2.041213134766" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.707082763672" Y="21.02217578125" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.963916015625" />
                  <Point X="2.015967163086" Y="21.8863203125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657714844" Y="22.12984765625" />
                  <Point X="1.808835571289" Y="22.150369140625" />
                  <Point X="1.783253417969" Y="22.171994140625" />
                  <Point X="1.773299682617" Y="22.179353515625" />
                  <Point X="1.735435180664" Y="22.203697265625" />
                  <Point X="1.560176147461" Y="22.31637109375" />
                  <Point X="1.546279663086" Y="22.323755859375" />
                  <Point X="1.517465332031" Y="22.336126953125" />
                  <Point X="1.502547363281" Y="22.34111328125" />
                  <Point X="1.470927001953" Y="22.34884375" />
                  <Point X="1.455391113281" Y="22.351302734375" />
                  <Point X="1.424125244141" Y="22.35362109375" />
                  <Point X="1.408395507812" Y="22.35348046875" />
                  <Point X="1.366984008789" Y="22.349669921875" />
                  <Point X="1.17530871582" Y="22.332033203125" />
                  <Point X="1.161225708008" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.1200078125" Y="22.318369140625" />
                  <Point X="1.092621826172" Y="22.307025390625" />
                  <Point X="1.079875244141" Y="22.300583984375" />
                  <Point X="1.055493530273" Y="22.28586328125" />
                  <Point X="1.043858032227" Y="22.277583984375" />
                  <Point X="1.011881225586" Y="22.25099609375" />
                  <Point X="0.863874328613" Y="22.127931640625" />
                  <Point X="0.852654663086" Y="22.11691015625" />
                  <Point X="0.832184204102" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.773231140137" Y="21.950380859375" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091369629" Y="20.84766015625" />
                  <Point X="0.726234375" Y="21.246455078125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146057129" Y="21.54641796875" />
                  <Point X="0.62678692627" Y="21.576185546875" />
                  <Point X="0.62040625" Y="21.58679296875" />
                  <Point X="0.591317443848" Y="21.628703125" />
                  <Point X="0.456678649902" Y="21.822693359375" />
                  <Point X="0.446669189453" Y="21.834830078125" />
                  <Point X="0.424782989502" Y="21.857287109375" />
                  <Point X="0.41290637207" Y="21.867607421875" />
                  <Point X="0.386651397705" Y="21.886849609375" />
                  <Point X="0.373237579346" Y="21.895064453125" />
                  <Point X="0.345236450195" Y="21.909171875" />
                  <Point X="0.330648651123" Y="21.915064453125" />
                  <Point X="0.285635375977" Y="21.929033203125" />
                  <Point X="0.07728956604" Y="21.993697265625" />
                  <Point X="0.063372665405" Y="21.996892578125" />
                  <Point X="0.035222595215" Y="22.00116015625" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008652074814" Y="22.002234375" />
                  <Point X="-0.022896093369" Y="22.001162109375" />
                  <Point X="-0.051062362671" Y="21.996892578125" />
                  <Point X="-0.064984466553" Y="21.9936953125" />
                  <Point X="-0.109997581482" Y="21.979724609375" />
                  <Point X="-0.318343719482" Y="21.9150625" />
                  <Point X="-0.332932373047" Y="21.909169921875" />
                  <Point X="-0.360933685303" Y="21.895060546875" />
                  <Point X="-0.374346160889" Y="21.88684375" />
                  <Point X="-0.400601287842" Y="21.867599609375" />
                  <Point X="-0.412475067139" Y="21.857283203125" />
                  <Point X="-0.434358886719" Y="21.834828125" />
                  <Point X="-0.444368804932" Y="21.822689453125" />
                  <Point X="-0.473457427979" Y="21.78077734375" />
                  <Point X="-0.608096679688" Y="21.5867890625" />
                  <Point X="-0.612470092773" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.972164855957" Y="20.282681640625" />
                  <Point X="-0.985424865723" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799510161991" Y="28.87027291756" />
                  <Point X="1.338733641283" Y="29.614575730885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.751945008351" Y="28.787887581274" />
                  <Point X="0.94103309322" Y="29.710593288826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704379854711" Y="28.705502244987" />
                  <Point X="0.639426075778" Y="29.757648747383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.656814701071" Y="28.6231169087" />
                  <Point X="0.377984786308" Y="29.784238745342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.609249547432" Y="28.540731572414" />
                  <Point X="0.352847702744" Y="29.690425736569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.968916775921" Y="27.74132552408" />
                  <Point X="3.917310031979" Y="27.767620473484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.561684393792" Y="28.458346236127" />
                  <Point X="0.327710619179" Y="29.596612727795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079447636012" Y="27.578386245333" />
                  <Point X="3.819211177087" Y="27.710983344042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.514119240152" Y="28.375960899841" />
                  <Point X="0.302573535615" Y="29.502799719022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.104796805269" Y="27.458849205898" />
                  <Point X="3.721112609845" Y="27.654346068035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.466554086512" Y="28.293575563554" />
                  <Point X="0.277436452051" Y="29.408986710249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.350910509252" Y="29.729145478145" />
                  <Point X="-0.43883170982" Y="29.773943567385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021293642777" Y="27.394775199726" />
                  <Point X="3.623014042604" Y="27.597708792029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.418988932872" Y="28.211190227267" />
                  <Point X="0.252299368487" Y="29.315173701476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.317824366486" Y="29.605666253805" />
                  <Point X="-0.608999796399" Y="29.754027545613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.937790480285" Y="27.330701193554" />
                  <Point X="3.524915475362" Y="27.541071516022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371423779232" Y="28.128804890981" />
                  <Point X="0.222221992958" Y="29.223877897186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.284738223721" Y="29.482187029465" />
                  <Point X="-0.779167882978" Y="29.734111523842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854287317793" Y="27.266627187381" />
                  <Point X="3.42681690812" Y="27.484434240015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.323858625592" Y="28.046419554694" />
                  <Point X="0.165778034189" Y="29.146016538074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.251652162166" Y="29.358707846504" />
                  <Point X="-0.945578081509" Y="29.712280762474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.7707841553" Y="27.202553181209" />
                  <Point X="3.328718340879" Y="27.427796964009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.276293471953" Y="27.964034218407" />
                  <Point X="0.061547696439" Y="29.092503555192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.213998562437" Y="29.232901386602" />
                  <Point X="-1.080560384066" Y="29.674436688282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687280992808" Y="27.138479175037" />
                  <Point X="3.230619773637" Y="27.371159688002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228728318313" Y="27.881648882121" />
                  <Point X="-1.215542686622" Y="29.636592614091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603777830316" Y="27.074405168865" />
                  <Point X="3.132521206395" Y="27.314522411995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18116313293" Y="27.799263562008" />
                  <Point X="-1.350524989178" Y="29.598748539899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.520274667824" Y="27.010331162693" />
                  <Point X="3.034422639153" Y="27.257885135989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133597938731" Y="27.716878246387" />
                  <Point X="-1.478570287899" Y="29.55736988571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436771505331" Y="26.946257156521" />
                  <Point X="2.936324071912" Y="27.201247859982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086032744532" Y="27.634492930766" />
                  <Point X="-1.463532390669" Y="29.44308670179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353268448197" Y="26.882183096666" />
                  <Point X="2.83822550467" Y="27.144610583975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044673110904" Y="27.548945724106" />
                  <Point X="-1.472291085881" Y="29.340928487329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.711471148494" Y="26.083523262717" />
                  <Point X="4.666731422823" Y="26.10631929155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.269765700127" Y="26.818108879335" />
                  <Point X="2.740126937428" Y="27.087973307969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.018876817144" Y="27.455468599705" />
                  <Point X="-1.501312585145" Y="29.249094687211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741102990142" Y="25.961804092707" />
                  <Point X="4.500441907838" Y="26.084427038843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.186262952057" Y="26.754034662004" />
                  <Point X="2.642028370187" Y="27.031336031962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.015716613107" Y="27.350457811512" />
                  <Point X="-1.543835021296" Y="29.16413995803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.763667307575" Y="25.843686006149" />
                  <Point X="4.334152394967" Y="26.06253478506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.116110267489" Y="26.683158247566" />
                  <Point X="2.518130343756" Y="26.987844236995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042529331263" Y="27.230175056666" />
                  <Point X="-1.622693752778" Y="29.097699496059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.781698028502" Y="25.727877902389" />
                  <Point X="4.167862882096" Y="26.040642531277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.059230049815" Y="26.605519173469" />
                  <Point X="2.296339401037" Y="26.994231374203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212550528016" Y="27.036923937392" />
                  <Point X="-1.736325216531" Y="29.048976626129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.663257416669" Y="25.681605415796" />
                  <Point X="4.001573369225" Y="26.018750277493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023053389213" Y="26.517331110148" />
                  <Point X="-2.280305242693" Y="29.2195273009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526120135479" Y="25.644859358062" />
                  <Point X="3.835283856354" Y="25.99685802371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0017523487" Y="26.421563539815" />
                  <Point X="-2.380408761717" Y="29.163911598852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.388982854289" Y="25.608113300327" />
                  <Point X="3.668994343482" Y="25.974965769927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01372150582" Y="26.308843957078" />
                  <Point X="-2.480512280741" Y="29.108295896803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251845573098" Y="25.571367242593" />
                  <Point X="3.502704830611" Y="25.953073516143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.061016408301" Y="26.178125008057" />
                  <Point X="-2.580615836981" Y="29.052680213717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.114708291908" Y="25.534621184858" />
                  <Point X="-2.675368409739" Y="28.994338068367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.977571010718" Y="25.497875127124" />
                  <Point X="-2.758913299876" Y="28.930285323492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840433903414" Y="25.46112898079" />
                  <Point X="-2.842458190013" Y="28.866232578616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.703296911151" Y="25.42438277584" />
                  <Point X="-2.92600308015" Y="28.802179833741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593317431083" Y="25.373799127282" />
                  <Point X="-3.009547970287" Y="28.738127088866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.502334273302" Y="25.313536369071" />
                  <Point X="-3.02420130648" Y="28.638972344001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.437891683518" Y="25.239750516022" />
                  <Point X="-2.93698738537" Y="28.48791363907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386684954008" Y="25.159220655318" />
                  <Point X="-2.84977346426" Y="28.336854934139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361728459319" Y="25.065315631917" />
                  <Point X="-2.77043258008" Y="28.189807741889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349181877867" Y="24.965087441895" />
                  <Point X="-2.751339829291" Y="28.073458506886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771393210763" Y="24.13381358065" />
                  <Point X="4.386232736125" Y="24.330062644617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365983363748" Y="24.849905664674" />
                  <Point X="-2.755084471788" Y="27.968745504962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.754096248557" Y="24.036005830518" />
                  <Point X="3.944876924473" Y="24.448323670361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.426256732286" Y="24.712573856902" />
                  <Point X="-2.794888590422" Y="27.882405723825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.73229989313" Y="23.940490635739" />
                  <Point X="-2.861984538765" Y="27.809971824489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.503865534568" Y="23.95026276239" />
                  <Point X="-2.946563795625" Y="27.746446115783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.221705630934" Y="23.987409421543" />
                  <Point X="-3.119743826603" Y="27.728064756335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.9395457273" Y="24.024556080696" />
                  <Point X="-3.784554930826" Y="27.960181940468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.657385535322" Y="24.061702886768" />
                  <Point X="-3.843957606897" Y="27.883828123119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.390356084692" Y="24.091140195053" />
                  <Point X="-3.903360282967" Y="27.80747430577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.238196917646" Y="24.062048170462" />
                  <Point X="-3.962762618957" Y="27.731120315141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.109082696105" Y="24.021214159653" />
                  <Point X="-4.018339458251" Y="27.652817136588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029624972073" Y="23.955078899631" />
                  <Point X="-4.066307220126" Y="27.570636939443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.96735416198" Y="23.880186469559" />
                  <Point X="-4.114274982" Y="27.488456742298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909260181015" Y="23.803165838748" />
                  <Point X="-4.162242743875" Y="27.406276545153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.876799763247" Y="23.713084255127" />
                  <Point X="-4.210210505749" Y="27.324096348008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866715586409" Y="23.611601407287" />
                  <Point X="-3.964082710055" Y="27.092066979699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859898397754" Y="23.508453945826" />
                  <Point X="-3.550505830575" Y="26.774718041706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.894692664655" Y="23.384104388768" />
                  <Point X="-3.438547875556" Y="26.611051621776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.9960581007" Y="23.225835126829" />
                  <Point X="-3.423009679372" Y="26.496513522805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.337573650651" Y="22.945203270156" />
                  <Point X="-3.450076030965" Y="26.403683525191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751151643124" Y="22.627853765064" />
                  <Point X="-3.494176672364" Y="26.319532931748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.07777338731" Y="22.354810681468" />
                  <Point X="-3.572015465947" Y="26.252572785461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025839327527" Y="22.274651414048" />
                  <Point X="3.302107091689" Y="22.643411406827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.711236598393" Y="22.944474960516" />
                  <Point X="-3.696982206398" Y="26.209625527486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.456480467316" Y="22.96765870014" />
                  <Point X="-3.940252756401" Y="26.22695707125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.343267988144" Y="22.918722346903" />
                  <Point X="-4.22241388426" Y="26.264104354177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240333932246" Y="22.864548875427" />
                  <Point X="-4.504575337186" Y="26.301251802734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.155656817261" Y="22.801073027926" />
                  <Point X="-4.647364605067" Y="26.267385576059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103273551659" Y="22.721142642303" />
                  <Point X="-4.672751728613" Y="26.173699969019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059025156329" Y="22.637067333248" />
                  <Point X="-4.698138852159" Y="26.08001436198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017182829436" Y="22.55176607109" />
                  <Point X="-4.723525832942" Y="25.9863286822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000449266548" Y="22.453671254668" />
                  <Point X="-4.74149208819" Y="25.888861953906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016845186574" Y="22.338696123571" />
                  <Point X="-3.307888022886" Y="25.051783205559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.904522009687" Y="25.355783405868" />
                  <Point X="-4.756163124534" Y="25.789716227718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038052294117" Y="22.221269569992" />
                  <Point X="-3.295943447843" Y="24.939076148016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345875733387" Y="25.474043367747" />
                  <Point X="-4.770834160877" Y="25.69057050153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092301611027" Y="22.087007169834" />
                  <Point X="-3.322458000487" Y="24.845964994795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179515431037" Y="21.935948516416" />
                  <Point X="1.848447989984" Y="22.104635803131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.36745618305" Y="22.349713369763" />
                  <Point X="-3.376644604596" Y="24.766953456035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266729251048" Y="21.784889862998" />
                  <Point X="1.982781868457" Y="21.929568280745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.190209161343" Y="22.333404245594" />
                  <Point X="-3.46492696857" Y="24.705314574646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.353943071058" Y="21.633831209579" />
                  <Point X="2.117116124965" Y="21.75450056574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.064081637127" Y="22.291048436489" />
                  <Point X="-3.594514827817" Y="24.664721894302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.441156891069" Y="21.482772556161" />
                  <Point X="2.2514505055" Y="21.57943278754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.982074280015" Y="22.226212279408" />
                  <Point X="-3.731652631466" Y="24.627976102774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528370711079" Y="21.331713902743" />
                  <Point X="2.385784886036" Y="21.40436500934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.902565904147" Y="22.160102827785" />
                  <Point X="-3.86879007255" Y="24.59123012651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.61558453109" Y="21.180655249325" />
                  <Point X="2.520119266572" Y="21.229297231139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.830158486397" Y="22.090375257286" />
                  <Point X="-4.005927363017" Y="24.554484073502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.7027983511" Y="21.029596595906" />
                  <Point X="2.654453647108" Y="21.054229452939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786370934787" Y="22.006065136627" />
                  <Point X="-4.143064653484" Y="24.517738020494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764573764496" Y="21.910550357042" />
                  <Point X="-4.280201943951" Y="24.480991967487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743709762629" Y="21.814560104396" />
                  <Point X="-2.953647303297" Y="23.698457625353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.327356537003" Y="23.888871990638" />
                  <Point X="-4.417339234418" Y="24.444245914479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725423154123" Y="21.71725660424" />
                  <Point X="-2.953123983033" Y="23.591569987785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.493645955595" Y="23.866979688817" />
                  <Point X="-4.554476524886" Y="24.407499861472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73316298897" Y="21.606691968835" />
                  <Point X="0.537346579249" Y="21.706465413016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036870797463" Y="21.999043779993" />
                  <Point X="-2.98511011076" Y="23.501246741318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.659935374187" Y="23.845087386996" />
                  <Point X="-4.691613815353" Y="24.370753808464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748209205622" Y="21.492404545957" />
                  <Point X="0.642126141782" Y="21.546456566743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.171032842566" Y="21.960781763754" />
                  <Point X="-3.047675848147" Y="23.426504584208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.826224792779" Y="23.823195085175" />
                  <Point X="-4.781378968863" Y="24.309870446081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.763255422274" Y="21.378117123079" />
                  <Point X="0.679524340617" Y="21.420780240097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.301076760574" Y="21.920421456956" />
                  <Point X="-3.131142427587" Y="23.362411938039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.992514211371" Y="23.801302783354" />
                  <Point X="-4.764931232468" Y="24.194868913225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778301638926" Y="21.263829700201" />
                  <Point X="0.712610294829" Y="21.29730111183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.402842251617" Y="21.865652571947" />
                  <Point X="-3.214645632871" Y="23.298337953671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.158803629963" Y="23.779410481533" />
                  <Point X="-4.748483535709" Y="24.079867400566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.793347855578" Y="21.149542277323" />
                  <Point X="0.745696416481" Y="21.173821898247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.466159077501" Y="21.791293113541" />
                  <Point X="-3.298148790519" Y="23.23426394503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.325093048556" Y="23.757518179712" />
                  <Point X="-4.724150128679" Y="23.960847917836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.80839407223" Y="21.035254854445" />
                  <Point X="0.778782655349" Y="21.050342624941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.520827219942" Y="21.712526930816" />
                  <Point X="-3.3816517821" Y="23.170189851774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.491382467148" Y="23.735625877891" />
                  <Point X="-4.692840700838" Y="23.838273974966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823440288882" Y="20.920967431567" />
                  <Point X="0.811868894217" Y="20.926863351634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.57549551818" Y="21.633760827474" />
                  <Point X="-2.361604331213" Y="22.54382872328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572395430795" Y="22.651232153044" />
                  <Point X="-3.465154773681" Y="23.106115758519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.657672893746" Y="23.713734089675" />
                  <Point X="-4.661531272998" Y="23.715700032097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.626617651075" Y="21.55318786264" />
                  <Point X="-2.311981042839" Y="22.411923402391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703455089764" Y="22.611389392116" />
                  <Point X="-3.548657765263" Y="23.042041665263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.656256502771" Y="21.461668619298" />
                  <Point X="-2.325414894948" Y="22.31214729935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801553766904" Y="22.554752172105" />
                  <Point X="-3.632160756844" Y="22.977967572007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.681393600444" Y="21.367855617714" />
                  <Point X="-2.369221213859" Y="22.227846741108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899652444044" Y="22.498114952094" />
                  <Point X="-3.715663748425" Y="22.913893478752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.706530698117" Y="21.274042616129" />
                  <Point X="-2.416786311787" Y="22.145461376435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.997751121184" Y="22.441477732084" />
                  <Point X="-3.799166740007" Y="22.849819385496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.73166779579" Y="21.180229614545" />
                  <Point X="-2.464351409714" Y="22.063076011761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.095849798324" Y="22.384840512073" />
                  <Point X="-3.882669731588" Y="22.78574529224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.756804893463" Y="21.08641661296" />
                  <Point X="-2.511916499004" Y="21.980690642687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193948475465" Y="22.328203292062" />
                  <Point X="-3.966172723169" Y="22.721671198985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.781941991136" Y="20.992603611376" />
                  <Point X="-2.559481586166" Y="21.898305272528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.292047152605" Y="22.271566072052" />
                  <Point X="-4.049675714751" Y="22.657597105729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807079088809" Y="20.898790609791" />
                  <Point X="-2.607046673328" Y="21.815919902369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.390145829745" Y="22.214928852041" />
                  <Point X="-4.133178706332" Y="22.593523012473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.832216186481" Y="20.804977608207" />
                  <Point X="-1.487527649323" Y="21.13887547587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.601328617394" Y="21.19685996528" />
                  <Point X="-2.654611760491" Y="21.733534532211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488244506885" Y="22.158291632031" />
                  <Point X="-4.142525368926" Y="22.491664382358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857353284154" Y="20.711164606622" />
                  <Point X="-1.226501543849" Y="20.899255039574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.851935928779" Y="21.217929775684" />
                  <Point X="-2.702176847653" Y="21.651149162052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.586343184025" Y="22.10165441202" />
                  <Point X="-4.051187125045" Y="22.338504230013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.882490381827" Y="20.617351605038" />
                  <Point X="-1.175961242214" Y="20.76688247709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.042610007539" Y="21.208462078796" />
                  <Point X="-2.749741934815" Y="21.568763791894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684441861165" Y="22.045017192009" />
                  <Point X="-3.939909156712" Y="22.175184280604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.9076274795" Y="20.523538603453" />
                  <Point X="-1.152361182809" Y="20.648236653638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.142504125033" Y="21.152739681339" />
                  <Point X="-2.797307021978" Y="21.486378421735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.782540629915" Y="21.988380018676" />
                  <Point X="-3.807339593462" Y="22.001015721724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.932764577173" Y="20.429725601869" />
                  <Point X="-1.128761195868" Y="20.529590867109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.233036966878" Y="21.092247475698" />
                  <Point X="-2.84487210914" Y="21.403993051576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.957901674846" Y="20.335912600284" />
                  <Point X="-1.121424269707" Y="20.419231523934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.323569808722" Y="21.031755270058" />
                  <Point X="-2.892437196303" Y="21.321607681418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.983038915991" Y="20.242099671803" />
                  <Point X="-1.134579105109" Y="20.319313254779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.403811263681" Y="20.966019340889" />
                  <Point X="-2.940002283465" Y="21.239222311259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.463196551185" Y="20.889656663622" />
                  <Point X="-2.923157629913" Y="21.124018539011" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.542708435059" Y="21.197279296875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.435230072021" Y="21.5203671875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.229322860718" Y="21.747568359375" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.05367779541" Y="21.798263671875" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.317367736816" Y="21.6724453125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.788638977051" Y="20.233505859375" />
                  <Point X="-0.847744140625" Y="20.012921875" />
                  <Point X="-0.855236816406" Y="20.014376953125" />
                  <Point X="-1.10023034668" Y="20.061931640625" />
                  <Point X="-1.151322143555" Y="20.075076171875" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.317717407227" Y="20.38388671875" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.320436889648" Y="20.519302734375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.427725219727" Y="20.83371484375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.704244140625" Y="21.017841796875" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.035710327148" Y="20.9955859375" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.444673339844" Y="20.6016875" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.496698974609" Y="20.610021484375" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-2.926560791016" Y="20.886845703125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.638293945312" Y="22.141798828125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.500251220703" Y="22.386330078125" />
                  <Point X="-2.504396484375" Y="22.417810546875" />
                  <Point X="-2.517062744141" Y="22.434318359375" />
                  <Point X="-2.531329101562" Y="22.448583984375" />
                  <Point X="-2.560157958984" Y="22.46280078125" />
                  <Point X="-2.591683837891" Y="22.456529296875" />
                  <Point X="-3.652001464844" Y="21.84435546875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.877967529297" Y="21.780099609375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.212408691406" Y="22.23789453125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.395199462891" Y="23.39928515625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535400391" Y="23.58891796875" />
                  <Point X="-3.144452880859" Y="23.609271484375" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.136651855469" Y="23.64994921875" />
                  <Point X="-3.148657714844" Y="23.678931640625" />
                  <Point X="-3.165865478516" Y="23.692130859375" />
                  <Point X="-3.187643066406" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.558080078125" Y="23.535205078125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.816421875" Y="23.554361328125" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.940808105469" Y="24.082603515625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.821077636719" Y="24.80071875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.536965087891" Y="24.88199609375" />
                  <Point X="-3.514142822266" Y="24.8978359375" />
                  <Point X="-3.502324707031" Y="24.909353515625" />
                  <Point X="-3.493255615234" Y="24.92938671875" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.487291503906" Y="24.9888359375" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.50232421875" Y="25.0280859375" />
                  <Point X="-3.519073486328" Y="25.043025390625" />
                  <Point X="-3.541895751953" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.777615722656" Y="25.3930234375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.989164550781" Y="25.513095703125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.890639648438" Y="26.096076171875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.942230957031" Y="26.418857421875" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731710449219" Y="26.395865234375" />
                  <Point X="-3.720790771484" Y="26.399306640625" />
                  <Point X="-3.670277832031" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.634741210938" Y="26.454357421875" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.621599853516" Y="26.555662109375" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.359850585938" Y="27.156259765625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.437913574219" Y="27.3108984375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.088479248047" Y="27.878953125" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.275213867188" Y="27.99394921875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514404297" Y="27.92043359375" />
                  <Point X="-3.123315185547" Y="27.919103515625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-3.002464355469" Y="27.938193359375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939404052734" Y="28.0430390625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.262206542969" Y="28.671208984375" />
                  <Point X="-3.307278808594" Y="28.74927734375" />
                  <Point X="-3.236888671875" Y="28.803244140625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.640211425781" Y="29.23692578125" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.003355102539" Y="29.333875" />
                  <Point X="-1.967826538086" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.934330810547" Y="29.26485546875" />
                  <Point X="-1.85603125" Y="29.22409375" />
                  <Point X="-1.835124389648" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.796188964844" Y="29.229548828125" />
                  <Point X="-1.714634765625" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.680348510742" Y="29.312677734375" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.687177001953" Y="29.68624609375" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.594678222656" Y="29.727623046875" />
                  <Point X="-0.968083007812" Y="29.903296875" />
                  <Point X="-0.831512451172" Y="29.91928125" />
                  <Point X="-0.224199981689" Y="29.990359375" />
                  <Point X="-0.076473182678" Y="29.43903515625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282142639" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.03659406662" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.213364044189" Y="29.90396875" />
                  <Point X="0.236648529053" Y="29.990869140625" />
                  <Point X="0.313106536865" Y="29.982861328125" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="0.973205200195" Y="29.898283203125" />
                  <Point X="1.508455932617" Y="29.769056640625" />
                  <Point X="1.581760253906" Y="29.74246875" />
                  <Point X="1.931044067383" Y="29.61578125" />
                  <Point X="2.00215625" Y="29.582525390625" />
                  <Point X="2.338685546875" Y="29.425140625" />
                  <Point X="2.407412597656" Y="29.3851015625" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.79732421875" Y="29.149607421875" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.382934814453" Y="27.7687421875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.221037597656" Y="27.47725" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.203531982422" Y="27.379990234375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.226314208984" Y="27.289564453125" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.286187255859" Y="27.2165703125" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.360336425781" Y="27.172978515625" />
                  <Point X="2.372670898438" Y="27.1714921875" />
                  <Point X="2.429761230469" Y="27.164607421875" />
                  <Point X="2.462929199219" Y="27.169759765625" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033691406" Y="27.19241796875" />
                  <Point X="3.768271972656" Y="27.90096484375" />
                  <Point X="3.994247558594" Y="28.031431640625" />
                  <Point X="4.009746826172" Y="28.009890625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.238717285156" Y="27.682181640625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.490161865234" Y="26.747734375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.26910546875" Y="26.570439453125" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.209295654297" Y="26.477826171875" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.193918701172" Y="26.375751953125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.224184570312" Y="26.274978515625" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.2933203125" Y="26.19185546875" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.385295166016" Y="26.151408203125" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.641598144531" Y="26.294650390625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.856362792969" Y="26.291607421875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.950572753906" Y="25.878265625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.972143554688" Y="25.299716796875" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.712651855469" Y="25.223318359375" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.612403564453" Y="25.154361328125" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.553697753906" Y="25.0575703125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.541770507812" Y="24.942150390625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.576620361328" Y="24.828673828125" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.653012695312" Y="24.748591796875" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.74116796875" Y="24.699611328125" />
                  <Point X="4.81025390625" Y="24.413150390625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.994677734375" Y="24.34033984375" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.933846679688" Y="23.9696875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.679849365234" Y="23.86710546875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.362578857422" Y="23.894646484375" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.165947753906" Y="23.8218515625" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.061563476562" Y="23.655560546875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.074212158203" Y="23.455611328125" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.160579589844" Y="22.5531796875" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.33449609375" Y="22.408806640625" />
                  <Point X="4.204134277344" Y="22.197861328125" />
                  <Point X="4.173967773438" Y="22.155" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="2.992026611328" Y="22.60304296875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.698948730469" Y="22.753619140625" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.457183349609" Y="22.76396875" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.271814208984" Y="22.633421875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.196096435547" Y="22.415232421875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.871627685547" Y="21.11717578125" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.801575195312" Y="20.787958984375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.865229980469" Y="21.77065625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.632684448242" Y="22.043876953125" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805175781" Y="22.16428125" />
                  <Point X="1.384393676758" Y="22.160470703125" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.133355834961" Y="22.10490234375" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.958895935059" Y="21.910025390625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.094602416992" Y="20.31693359375" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="0.994366821289" Y="20.0367578125" />
                  <Point X="0.963188659668" Y="20.03109375" />
                  <Point X="0.860200439453" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#134" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.034332864015" Y="4.483256472984" Z="0.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="-0.843480619063" Y="5.000222398833" Z="0.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.45" />
                  <Point X="-1.614332013093" Y="4.807047800245" Z="0.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.45" />
                  <Point X="-1.742905199774" Y="4.711001854986" Z="0.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.45" />
                  <Point X="-1.734190241094" Y="4.358992850533" Z="0.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.45" />
                  <Point X="-1.821479282605" Y="4.307022986653" Z="0.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.45" />
                  <Point X="-1.917398592442" Y="4.340485031035" Z="0.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.45" />
                  <Point X="-1.969843752107" Y="4.395593050578" Z="0.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.45" />
                  <Point X="-2.670650300553" Y="4.311913135075" Z="0.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.45" />
                  <Point X="-3.273466801179" Y="3.874204010449" Z="0.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.45" />
                  <Point X="-3.311663736356" Y="3.677489470242" Z="0.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.45" />
                  <Point X="-2.995369779718" Y="3.069962712144" Z="0.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.45" />
                  <Point X="-3.043975085552" Y="3.004828482334" Z="0.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.45" />
                  <Point X="-3.125113753463" Y="3.000194919455" Z="0.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.45" />
                  <Point X="-3.256369878055" Y="3.068530212389" Z="0.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.45" />
                  <Point X="-4.134099077038" Y="2.940936810086" Z="0.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.45" />
                  <Point X="-4.487251715715" Y="2.367383643488" Z="0.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.45" />
                  <Point X="-4.396444612471" Y="2.147872655727" Z="0.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.45" />
                  <Point X="-3.672106175733" Y="1.563854356768" Z="0.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.45" />
                  <Point X="-3.687090905442" Y="1.504771957906" Z="0.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.45" />
                  <Point X="-3.741982769352" Y="1.478273482031" Z="0.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.45" />
                  <Point X="-3.941860913068" Y="1.49971023579" Z="0.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.45" />
                  <Point X="-4.945055480665" Y="1.140433880613" Z="0.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.45" />
                  <Point X="-5.044781995129" Y="0.55169504314" Z="0.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.45" />
                  <Point X="-4.796713180603" Y="0.376007899355" Z="0.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.45" />
                  <Point X="-3.553738584093" Y="0.03322915556" Z="0.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.45" />
                  <Point X="-3.541200344844" Y="0.005295662601" Z="0.45" />
                  <Point X="-3.539556741714" Y="0" Z="0.45" />
                  <Point X="-3.547164256291" Y="-0.02451128846" Z="0.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.45" />
                  <Point X="-3.57163001103" Y="-0.045646827574" Z="0.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.45" />
                  <Point X="-3.84017460817" Y="-0.119704157401" Z="0.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.45" />
                  <Point X="-4.99646016249" Y="-0.893193587201" Z="0.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.45" />
                  <Point X="-4.871022888721" Y="-1.426732691204" Z="0.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.45" />
                  <Point X="-4.557709527767" Y="-1.48308680904" Z="0.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.45" />
                  <Point X="-3.197381343172" Y="-1.319680623083" Z="0.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.45" />
                  <Point X="-3.199011980054" Y="-1.346912093697" Z="0.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.45" />
                  <Point X="-3.431793447104" Y="-1.529766330591" Z="0.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.45" />
                  <Point X="-4.261507783135" Y="-2.756434628784" Z="0.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.45" />
                  <Point X="-3.923909370875" Y="-3.21890371937" Z="0.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.45" />
                  <Point X="-3.633157281851" Y="-3.167665719743" Z="0.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.45" />
                  <Point X="-2.558573203016" Y="-2.569757266116" Z="0.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.45" />
                  <Point X="-2.687751293426" Y="-2.801921100641" Z="0.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.45" />
                  <Point X="-2.963220686312" Y="-4.121490793656" Z="0.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.45" />
                  <Point X="-2.529176406828" Y="-4.401209589249" Z="0.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.45" />
                  <Point X="-2.411161618723" Y="-4.397469737687" Z="0.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.45" />
                  <Point X="-2.014087667929" Y="-4.014708156463" Z="0.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.45" />
                  <Point X="-1.71367011556" Y="-4.000770826133" Z="0.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.45" />
                  <Point X="-1.466848354894" Y="-4.172596468379" Z="0.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.45" />
                  <Point X="-1.375632165196" Y="-4.45917013316" Z="0.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.45" />
                  <Point X="-1.373445650327" Y="-4.578305902483" Z="0.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.45" />
                  <Point X="-1.169936989073" Y="-4.942066734772" Z="0.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.45" />
                  <Point X="-0.870920385443" Y="-5.003426490845" Z="0.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="-0.746498660615" Y="-4.748155041593" Z="0.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="-0.282447819757" Y="-3.324784005997" Z="0.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="-0.045013131282" Y="-3.218209783249" Z="0.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.45" />
                  <Point X="0.208345948079" Y="-3.268902262039" Z="0.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.45" />
                  <Point X="0.387998039675" Y="-3.47686174977" Z="0.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.45" />
                  <Point X="0.488256199786" Y="-3.784380988222" Z="0.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.45" />
                  <Point X="0.965969618833" Y="-4.986821973809" Z="0.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.45" />
                  <Point X="1.145243268851" Y="-4.948727964023" Z="0.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.45" />
                  <Point X="1.13801861818" Y="-4.645259956099" Z="0.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.45" />
                  <Point X="1.001599091258" Y="-3.069313767264" Z="0.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.45" />
                  <Point X="1.159166876598" Y="-2.90226316244" Z="0.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.45" />
                  <Point X="1.382819023662" Y="-2.858037306907" Z="0.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.45" />
                  <Point X="1.599489244912" Y="-2.966901796977" Z="0.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.45" />
                  <Point X="1.819406304562" Y="-3.228500537964" Z="0.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.45" />
                  <Point X="2.822588520388" Y="-4.22273497403" Z="0.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.45" />
                  <Point X="3.012891384806" Y="-4.089129828809" Z="0.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.45" />
                  <Point X="2.908772968465" Y="-3.826543028274" Z="0.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.45" />
                  <Point X="2.23914464274" Y="-2.54459996668" Z="0.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.45" />
                  <Point X="2.309904536801" Y="-2.358584323774" Z="0.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.45" />
                  <Point X="2.474314054598" Y="-2.248996773166" Z="0.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.45" />
                  <Point X="2.683906836833" Y="-2.264303366751" Z="0.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.45" />
                  <Point X="2.960870568833" Y="-2.408976531809" Z="0.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.45" />
                  <Point X="4.208700587705" Y="-2.842497225805" Z="0.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.45" />
                  <Point X="4.370873356363" Y="-2.586197466459" Z="0.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.45" />
                  <Point X="4.184861382642" Y="-2.375872475861" Z="0.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.45" />
                  <Point X="3.110114378065" Y="-1.486069910876" Z="0.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.45" />
                  <Point X="3.10519696738" Y="-1.317740618281" Z="0.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.45" />
                  <Point X="3.198237809223" Y="-1.178833886695" Z="0.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.45" />
                  <Point X="3.367042168307" Y="-1.122931767198" Z="0.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.45" />
                  <Point X="3.667167141175" Y="-1.151185818388" Z="0.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.45" />
                  <Point X="4.976437809896" Y="-1.01015736536" Z="0.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.45" />
                  <Point X="5.037963392922" Y="-0.635832261691" Z="0.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.45" />
                  <Point X="4.817039064043" Y="-0.507271451699" Z="0.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.45" />
                  <Point X="3.671878534196" Y="-0.176838405643" Z="0.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.45" />
                  <Point X="3.609798215412" Y="-0.109176389044" Z="0.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.45" />
                  <Point X="3.584721890616" Y="-0.017164344549" Z="0.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.45" />
                  <Point X="3.596649559808" Y="0.079446186687" Z="0.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.45" />
                  <Point X="3.645581222988" Y="0.154772349531" Z="0.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.45" />
                  <Point X="3.731516880156" Y="0.211310438599" Z="0.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.45" />
                  <Point X="3.978928753264" Y="0.28270048107" Z="0.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.45" />
                  <Point X="4.993821428296" Y="0.917238515969" Z="0.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.45" />
                  <Point X="4.89878783736" Y="1.334714821748" Z="0.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.45" />
                  <Point X="4.628915675516" Y="1.375503856558" Z="0.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.45" />
                  <Point X="3.385689730269" Y="1.23225762344" Z="0.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.45" />
                  <Point X="3.311691534704" Y="1.266706113421" Z="0.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.45" />
                  <Point X="3.259799158735" Y="1.333738771697" Z="0.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.45" />
                  <Point X="3.236731195913" Y="1.417135222847" Z="0.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.45" />
                  <Point X="3.251291925168" Y="1.495639777648" Z="0.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.45" />
                  <Point X="3.302632268605" Y="1.571302437762" Z="0.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.45" />
                  <Point X="3.514444177421" Y="1.739346794695" Z="0.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.45" />
                  <Point X="4.27533875876" Y="2.739348723578" Z="0.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.45" />
                  <Point X="4.044176472672" Y="3.070373811586" Z="0.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.45" />
                  <Point X="3.737116360408" Y="2.975545171278" Z="0.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.45" />
                  <Point X="2.443855743561" Y="2.249343577718" Z="0.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.45" />
                  <Point X="2.372501181993" Y="2.252413228508" Z="0.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.45" />
                  <Point X="2.308105837878" Y="2.289226004833" Z="0.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.45" />
                  <Point X="2.261532595402" Y="2.348919022504" Z="0.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.45" />
                  <Point X="2.24701647425" Y="2.41725726184" Z="0.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.45" />
                  <Point X="2.263184364939" Y="2.495613832321" Z="0.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.45" />
                  <Point X="2.420080181581" Y="2.775022721547" Z="0.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.45" />
                  <Point X="2.820145221556" Y="4.221636065027" Z="0.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.45" />
                  <Point X="2.426426471989" Y="4.459584537241" Z="0.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.45" />
                  <Point X="2.01718165921" Y="4.659096440886" Z="0.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.45" />
                  <Point X="1.592652860634" Y="4.820753979845" Z="0.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.45" />
                  <Point X="0.978783593789" Y="4.978167688907" Z="0.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.45" />
                  <Point X="0.312158490307" Y="5.06386956812" Z="0.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.45" />
                  <Point X="0.158911697538" Y="4.948191032967" Z="0.45" />
                  <Point X="0" Y="4.355124473572" Z="0.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>