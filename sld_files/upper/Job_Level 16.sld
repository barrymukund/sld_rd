<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#147" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1275" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.00471484375" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.681784179688" Y="21.045294921875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.54236315918" Y="21.532625" />
                  <Point X="0.493936431885" Y="21.6023984375" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.356752044678" Y="21.790978515625" />
                  <Point X="0.330496856689" Y="21.81022265625" />
                  <Point X="0.302495178223" Y="21.82433203125" />
                  <Point X="0.227557617188" Y="21.84758984375" />
                  <Point X="0.049136035919" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.008664536476" Y="21.907234375" />
                  <Point X="-0.036824085236" Y="21.90296484375" />
                  <Point X="-0.111761489868" Y="21.87970703125" />
                  <Point X="-0.290183227539" Y="21.82433203125" />
                  <Point X="-0.318184906006" Y="21.81022265625" />
                  <Point X="-0.344440093994" Y="21.790978515625" />
                  <Point X="-0.366324035645" Y="21.7685234375" />
                  <Point X="-0.414750793457" Y="21.698748046875" />
                  <Point X="-0.530051696777" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.833089111328" Y="20.43466796875" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-1.079328735352" Y="20.1546484375" />
                  <Point X="-1.162575683594" Y="20.17606640625" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.229225463867" Y="20.3282265625" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.234411132812" Y="20.57377734375" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.32364453125" Y="20.868794921875" />
                  <Point X="-1.392637451172" Y="20.92930078125" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.64302734375" Y="21.109033203125" />
                  <Point X="-1.734596313477" Y="21.11503515625" />
                  <Point X="-1.952616577148" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657836914" Y="21.10519921875" />
                  <Point X="-2.118958007812" Y="21.054216796875" />
                  <Point X="-2.300624267578" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480147949219" Y="20.71151171875" />
                  <Point X="-2.498580566406" Y="20.722923828125" />
                  <Point X="-2.801722900391" Y="20.91062109375" />
                  <Point X="-2.916963378906" Y="20.999353515625" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.643947021484" Y="21.9420078125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561035156" Y="22.444427734375" />
                  <Point X="-2.428778564453" Y="22.473255859375" />
                  <Point X="-2.446806884766" Y="22.4984140625" />
                  <Point X="-2.464155517578" Y="22.51576171875" />
                  <Point X="-2.489311523438" Y="22.533787109375" />
                  <Point X="-2.518140380859" Y="22.54800390625" />
                  <Point X="-2.547758789062" Y="22.55698828125" />
                  <Point X="-2.578693115234" Y="22.555974609375" />
                  <Point X="-2.610218994141" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.547209960938" Y="22.01455078125" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-3.843372314453" Y="21.8915" />
                  <Point X="-4.082862304687" Y="22.206142578125" />
                  <Point X="-4.165486328125" Y="22.344689453125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.491208251953" Y="23.205869140625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083065429688" Y="23.5241171875" />
                  <Point X="-3.063635742188" Y="23.555421875" />
                  <Point X="-3.054643554688" Y="23.5792890625" />
                  <Point X="-3.051577636719" Y="23.58896484375" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.042037597656" Y="23.641396484375" />
                  <Point X="-3.042736816406" Y="23.664263671875" />
                  <Point X="-3.048883300781" Y="23.686302734375" />
                  <Point X="-3.060888183594" Y="23.71528515625" />
                  <Point X="-3.073803222656" Y="23.737427734375" />
                  <Point X="-3.09216796875" Y="23.755310546875" />
                  <Point X="-3.112502929688" Y="23.770349609375" />
                  <Point X="-3.1208046875" Y="23.77583984375" />
                  <Point X="-3.139454345703" Y="23.78681640625" />
                  <Point X="-3.168716064453" Y="23.798041015625" />
                  <Point X="-3.200603759766" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.3782265625" Y="23.654703125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.740411621094" Y="23.6406484375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.855937988281" Y="24.16019140625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.971737792969" Y="24.661998046875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.512110839844" Y="24.78787890625" />
                  <Point X="-3.488334472656" Y="24.800796875" />
                  <Point X="-3.479521240234" Y="24.8062265625" />
                  <Point X="-3.459977050781" Y="24.819791015625" />
                  <Point X="-3.436021972656" Y="24.84131640625" />
                  <Point X="-3.415095703125" Y="24.871720703125" />
                  <Point X="-3.404933837891" Y="24.895275390625" />
                  <Point X="-3.401432128906" Y="24.904748046875" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.389474365234" Y="24.9544765625" />
                  <Point X="-3.390444091797" Y="24.988314453125" />
                  <Point X="-3.395427001953" Y="25.011951171875" />
                  <Point X="-3.397652832031" Y="25.020513671875" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.417484619141" Y="25.07083203125" />
                  <Point X="-3.440399169922" Y="25.10012890625" />
                  <Point X="-3.460426757812" Y="25.11734375" />
                  <Point X="-3.468185791016" Y="25.123345703125" />
                  <Point X="-3.487729980469" Y="25.13691015625" />
                  <Point X="-3.501922607422" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.577780273438" Y="25.437830078125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.884854003906" Y="25.56902734375" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.780481933594" Y="26.139369140625" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.080253173828" Y="26.341208984375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423828125" Y="26.301228515625" />
                  <Point X="-3.703137451172" Y="26.305263671875" />
                  <Point X="-3.684969238281" Y="26.3109921875" />
                  <Point X="-3.641711425781" Y="26.324630859375" />
                  <Point X="-3.622784912109" Y="26.332958984375" />
                  <Point X="-3.604040527344" Y="26.343779296875" />
                  <Point X="-3.58735546875" Y="26.35601171875" />
                  <Point X="-3.573714355469" Y="26.37156640625" />
                  <Point X="-3.561299804688" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.544061523438" Y="26.425029296875" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532049316406" Y="26.589376953125" />
                  <Point X="-3.540845703125" Y="26.606275390625" />
                  <Point X="-3.5617890625" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.201495605469" Y="27.15449609375" />
                  <Point X="-4.351860351563" Y="27.269875" />
                  <Point X="-4.315712402344" Y="27.3318046875" />
                  <Point X="-4.081155761719" Y="27.733654296875" />
                  <Point X="-3.9645859375" Y="27.883490234375" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.399867431641" Y="27.95622265625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729980469" Y="27.836341796875" />
                  <Point X="-3.167088378906" Y="27.82983203125" />
                  <Point X="-3.146791992188" Y="27.825794921875" />
                  <Point X="-3.121488525391" Y="27.82358203125" />
                  <Point X="-3.061242675781" Y="27.818310546875" />
                  <Point X="-3.040561523438" Y="27.81876171875" />
                  <Point X="-3.019102783203" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.928116699219" Y="27.87819140625" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845649658203" Y="28.061423828125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.135389404297" Y="28.6415546875" />
                  <Point X="-3.183332519531" Y="28.72459375" />
                  <Point X="-3.109148925781" Y="28.781470703125" />
                  <Point X="-2.700619873047" Y="29.0946875" />
                  <Point X="-2.517038085938" Y="29.1966796875" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.102343017578" Y="29.30682421875" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028891479492" Y="29.21480078125" />
                  <Point X="-2.012311645508" Y="29.200888671875" />
                  <Point X="-1.995116577148" Y="29.1893984375" />
                  <Point X="-1.966954101562" Y="29.174736328125" />
                  <Point X="-1.899900634766" Y="29.139830078125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718261719" Y="29.126728515625" />
                  <Point X="-1.839268310547" Y="29.123580078125" />
                  <Point X="-1.818621826172" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.77745324707" Y="29.134482421875" />
                  <Point X="-1.748119995117" Y="29.1466328125" />
                  <Point X="-1.678279296875" Y="29.1755625" />
                  <Point X="-1.660145385742" Y="29.185509765625" />
                  <Point X="-1.642416015625" Y="29.197923828125" />
                  <Point X="-1.626863891602" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.585932739258" Y="29.296203125" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-1.478493896484" Y="29.661533203125" />
                  <Point X="-0.949623474121" Y="29.80980859375" />
                  <Point X="-0.727077331543" Y="29.83585546875" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.191060287476" Y="29.499626953125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907154" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425926208" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441650391" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.282302642822" Y="29.794201171875" />
                  <Point X="0.307419464111" Y="29.8879375" />
                  <Point X="0.382263916016" Y="29.880099609375" />
                  <Point X="0.844045288086" Y="29.83173828125" />
                  <Point X="1.028174804688" Y="29.787283203125" />
                  <Point X="1.48102355957" Y="29.677951171875" />
                  <Point X="1.59983605957" Y="29.634857421875" />
                  <Point X="1.894649414062" Y="29.52792578125" />
                  <Point X="2.010541748047" Y="29.473728515625" />
                  <Point X="2.294556884766" Y="29.34090234375" />
                  <Point X="2.406566894531" Y="29.275646484375" />
                  <Point X="2.680973632812" Y="29.11577734375" />
                  <Point X="2.786573974609" Y="29.0406796875" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.402429443359" Y="27.9925078125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133077148438" Y="27.516056640625" />
                  <Point X="2.126726806641" Y="27.492310546875" />
                  <Point X="2.111607421875" Y="27.43576953125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.110204101563" Y="27.36041796875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121443115234" Y="27.2896015625" />
                  <Point X="2.129709716797" Y="27.267513671875" />
                  <Point X="2.140069580078" Y="27.247474609375" />
                  <Point X="2.152775390625" Y="27.228748046875" />
                  <Point X="2.18302734375" Y="27.184166015625" />
                  <Point X="2.194461425781" Y="27.17033203125" />
                  <Point X="2.221594970703" Y="27.14559375" />
                  <Point X="2.240320068359" Y="27.13288671875" />
                  <Point X="2.284903564453" Y="27.102634765625" />
                  <Point X="2.304952392578" Y="27.09226953125" />
                  <Point X="2.327040039062" Y="27.08400390625" />
                  <Point X="2.348969482422" Y="27.07866015625" />
                  <Point X="2.369503662109" Y="27.076185546875" />
                  <Point X="2.418394287109" Y="27.0702890625" />
                  <Point X="2.436467529297" Y="27.06984375" />
                  <Point X="2.4732109375" Y="27.074171875" />
                  <Point X="2.496957763672" Y="27.0805234375" />
                  <Point X="2.553497314453" Y="27.095642578125" />
                  <Point X="2.565285644531" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.639506591797" Y="27.71692578125" />
                  <Point X="3.967325927734" Y="27.90619140625" />
                  <Point X="4.123270996094" Y="27.689462890625" />
                  <Point X="4.182140136719" Y="27.5921796875" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.566315673828" Y="26.9259140625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221423339844" Y="26.660240234375" />
                  <Point X="3.203974853516" Y="26.64162890625" />
                  <Point X="3.186884277344" Y="26.619333984375" />
                  <Point X="3.146192626953" Y="26.566248046875" />
                  <Point X="3.136605957031" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.115263671875" Y="26.494322265625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.102965576172" Y="26.346439453125" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136281982422" Y="26.235740234375" />
                  <Point X="3.15049609375" Y="26.214134765625" />
                  <Point X="3.184339599609" Y="26.1626953125" />
                  <Point X="3.198896972656" Y="26.1454453125" />
                  <Point X="3.216140380859" Y="26.129357421875" />
                  <Point X="3.234345214844" Y="26.116037109375" />
                  <Point X="3.254943603516" Y="26.10444140625" />
                  <Point X="3.303987304688" Y="26.076833984375" />
                  <Point X="3.320520751953" Y="26.069501953125" />
                  <Point X="3.356122070312" Y="26.0594375" />
                  <Point X="3.383972412109" Y="26.0557578125" />
                  <Point X="3.450282714844" Y="26.046994140625" />
                  <Point X="3.462697998047" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.486557128906" Y="26.178419921875" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.864487304688" Y="25.813658203125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.101106445313" Y="25.432625" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704787841797" Y="25.325583984375" />
                  <Point X="3.681548828125" Y="25.315068359375" />
                  <Point X="3.654186767578" Y="25.29925390625" />
                  <Point X="3.589038818359" Y="25.26159765625" />
                  <Point X="3.574312988281" Y="25.25109765625" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.531113037109" Y="25.20465625" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.458208496094" Y="25.064029296875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.450651367188" Y="24.91287109375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480300292969" Y="24.80187109375" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.508441650391" Y="24.761671875" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559998291016" Y="24.698763671875" />
                  <Point X="3.589038330078" Y="24.675841796875" />
                  <Point X="3.616400634766" Y="24.66002734375" />
                  <Point X="3.681548339844" Y="24.622369140625" />
                  <Point X="3.692709716797" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.632115722656" Y="24.36253125" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.831255371094" Y="23.947123046875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.870324951172" Y="23.937849609375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659667969" Y="23.994490234375" />
                  <Point X="3.320957275391" Y="23.982818359375" />
                  <Point X="3.193095458984" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.0799375" Y="23.867" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.965105224609" Y="23.644078125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.006170410156" Y="23.385775390625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.960251220703" Y="22.587150390625" />
                  <Point X="4.213121582031" Y="22.393115234375" />
                  <Point X="4.124811523438" Y="22.250216796875" />
                  <Point X="4.075657226562" Y="22.180375" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.198126953125" Y="22.593748046875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.690310302734" Y="22.851716796875" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444834960938" Y="22.864822265625" />
                  <Point X="2.391737792969" Y="22.83687890625" />
                  <Point X="2.265316894531" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531494141" Y="22.709560546875" />
                  <Point X="2.176586914063" Y="22.656462890625" />
                  <Point X="2.110052490234" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.107218261719" Y="22.372828125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.697787109375" Y="21.22827734375" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781834960938" Y="20.888345703125" />
                  <Point X="2.72689453125" Y="20.852783203125" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="2.061625244141" Y="21.670763671875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.658887573242" Y="22.139970703125" />
                  <Point X="1.50880090332" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448366088867" Y="22.2565625" />
                  <Point X="1.417100341797" Y="22.258880859375" />
                  <Point X="1.348158935547" Y="22.252537109375" />
                  <Point X="1.184013549805" Y="22.23743359375" />
                  <Point X="1.156363037109" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104594970703" Y="22.204537109375" />
                  <Point X="1.051360351563" Y="22.1602734375" />
                  <Point X="0.924611328125" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624206543" Y="21.974189453125" />
                  <Point X="0.859707275391" Y="21.900958984375" />
                  <Point X="0.821809936523" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.974465393066" Y="20.501640625" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.975680480957" Y="20.12991796875" />
                  <Point X="0.929315795898" Y="20.1214921875" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058427246094" Y="20.247365234375" />
                  <Point X="-1.138904785156" Y="20.2680703125" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.135038208008" Y="20.315826171875" />
                  <Point X="-1.120775756836" Y="20.42416015625" />
                  <Point X="-1.120077392578" Y="20.431900390625" />
                  <Point X="-1.119451660156" Y="20.458966796875" />
                  <Point X="-1.121759033203" Y="20.490669921875" />
                  <Point X="-1.123333862305" Y="20.502306640625" />
                  <Point X="-1.141236450195" Y="20.592310546875" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666137695" Y="20.864533203125" />
                  <Point X="-1.22173828125" Y="20.89237109375" />
                  <Point X="-1.230574829102" Y="20.905138671875" />
                  <Point X="-1.250208618164" Y="20.9290625" />
                  <Point X="-1.261006225586" Y="20.94021875" />
                  <Point X="-1.329999145508" Y="21.000724609375" />
                  <Point X="-1.494267211914" Y="21.144783203125" />
                  <Point X="-1.506735839844" Y="21.15403125" />
                  <Point X="-1.53301953125" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531860352" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.621456665039" Y="21.201552734375" />
                  <Point X="-1.636813842773" Y="21.203830078125" />
                  <Point X="-1.7283828125" Y="21.20983203125" />
                  <Point X="-1.946403076172" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048339844" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095437255859" Y="21.184189453125" />
                  <Point X="-2.171737304688" Y="21.13320703125" />
                  <Point X="-2.353403564453" Y="21.011822265625" />
                  <Point X="-2.359687255859" Y="21.007240234375" />
                  <Point X="-2.380448730469" Y="20.98986328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503202148438" Y="20.837521484375" />
                  <Point X="-2.747597167969" Y="20.98884375" />
                  <Point X="-2.859005615234" Y="21.074625" />
                  <Point X="-2.98086328125" Y="21.16844921875" />
                  <Point X="-2.561674560547" Y="21.8945078125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314667236328" Y="22.442390625" />
                  <Point X="-2.323652587891" Y="22.4720078125" />
                  <Point X="-2.329359375" Y="22.486447265625" />
                  <Point X="-2.343576904297" Y="22.515275390625" />
                  <Point X="-2.351558349609" Y="22.528591796875" />
                  <Point X="-2.369586669922" Y="22.55375" />
                  <Point X="-2.379633544922" Y="22.565591796875" />
                  <Point X="-2.396982177734" Y="22.582939453125" />
                  <Point X="-2.408822509766" Y="22.592984375" />
                  <Point X="-2.433978515625" Y="22.611009765625" />
                  <Point X="-2.447294189453" Y="22.618990234375" />
                  <Point X="-2.476123046875" Y="22.63320703125" />
                  <Point X="-2.490564208984" Y="22.6389140625" />
                  <Point X="-2.520182617188" Y="22.6478984375" />
                  <Point X="-2.550870117188" Y="22.6519375" />
                  <Point X="-2.581804443359" Y="22.650923828125" />
                  <Point X="-2.597228515625" Y="22.6491484375" />
                  <Point X="-2.628754394531" Y="22.642876953125" />
                  <Point X="-2.643685058594" Y="22.63861328125" />
                  <Point X="-2.672649658203" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.594709960938" Y="22.096822265625" />
                  <Point X="-3.793087646484" Y="21.9822890625" />
                  <Point X="-4.004021240234" Y="22.2594140625" />
                  <Point X="-4.083893798828" Y="22.39334765625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.433375976562" Y="23.1305" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039158691406" Y="23.433931640625" />
                  <Point X="-3.01626953125" Y="23.456564453125" />
                  <Point X="-3.002348632812" Y="23.47401953125" />
                  <Point X="-2.982918945312" Y="23.50532421875" />
                  <Point X="-2.974736083984" Y="23.521927734375" />
                  <Point X="-2.965743896484" Y="23.545794921875" />
                  <Point X="-2.959612060547" Y="23.565146484375" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951952880859" Y="23.597603515625" />
                  <Point X="-2.947838623047" Y="23.6290859375" />
                  <Point X="-2.94708203125" Y="23.64430078125" />
                  <Point X="-2.94778125" Y="23.66716796875" />
                  <Point X="-2.951229003906" Y="23.68978515625" />
                  <Point X="-2.957375488281" Y="23.71182421875" />
                  <Point X="-2.961114746094" Y="23.722658203125" />
                  <Point X="-2.973119628906" Y="23.751640625" />
                  <Point X="-2.978826904297" Y="23.7631484375" />
                  <Point X="-2.991741943359" Y="23.785291015625" />
                  <Point X="-3.007527099609" Y="23.805490234375" />
                  <Point X="-3.025891845703" Y="23.823373046875" />
                  <Point X="-3.035679199219" Y="23.83169140625" />
                  <Point X="-3.056014160156" Y="23.84673046875" />
                  <Point X="-3.072617675781" Y="23.8577109375" />
                  <Point X="-3.091267333984" Y="23.8686875" />
                  <Point X="-3.105430175781" Y="23.875513671875" />
                  <Point X="-3.134691894531" Y="23.88673828125" />
                  <Point X="-3.149790771484" Y="23.89113671875" />
                  <Point X="-3.181678466797" Y="23.897619140625" />
                  <Point X="-3.197294677734" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.390626464844" Y="23.748890625" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.761895019531" Y="24.173640625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.947149902344" Y="24.570234375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497656005859" Y="24.691359375" />
                  <Point X="-3.476890869141" Y="24.6996484375" />
                  <Point X="-3.4667578125" Y="24.704404296875" />
                  <Point X="-3.442981445313" Y="24.717322265625" />
                  <Point X="-3.425354980469" Y="24.728181640625" />
                  <Point X="-3.405810791016" Y="24.74174609375" />
                  <Point X="-3.396481201172" Y="24.74912890625" />
                  <Point X="-3.372526123047" Y="24.770654296875" />
                  <Point X="-3.357766113281" Y="24.787455078125" />
                  <Point X="-3.33683984375" Y="24.817859375" />
                  <Point X="-3.327866943359" Y="24.834087890625" />
                  <Point X="-3.317705078125" Y="24.857642578125" />
                  <Point X="-3.310701660156" Y="24.876587890625" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.301576904297" Y="24.90805859375" />
                  <Point X="-3.296133789062" Y="24.936796875" />
                  <Point X="-3.294513427734" Y="24.957197265625" />
                  <Point X="-3.295483154297" Y="24.99103515625" />
                  <Point X="-3.297487304688" Y="25.00791015625" />
                  <Point X="-3.302470214844" Y="25.031546875" />
                  <Point X="-3.306921875" Y="25.048671875" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.317668212891" Y="25.08078515625" />
                  <Point X="-3.330985107422" Y="25.110111328125" />
                  <Point X="-3.342655029297" Y="25.129359375" />
                  <Point X="-3.365569580078" Y="25.15865625" />
                  <Point X="-3.378473876953" Y="25.172171875" />
                  <Point X="-3.398501464844" Y="25.18938671875" />
                  <Point X="-3.41401953125" Y="25.201390625" />
                  <Point X="-3.433563720703" Y="25.214955078125" />
                  <Point X="-3.440480224609" Y="25.219326171875" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496565917969" Y="25.24563671875" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.553192382812" Y="25.52959375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.6887890625" Y="26.114521484375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.092653076172" Y="26.247021484375" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736703857422" Y="26.204703125" />
                  <Point X="-3.715142578125" Y="26.20658984375" />
                  <Point X="-3.704890380859" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.656401611328" Y="26.220388671875" />
                  <Point X="-3.613143798828" Y="26.23402734375" />
                  <Point X="-3.603449462891" Y="26.23767578125" />
                  <Point X="-3.584522949219" Y="26.24600390625" />
                  <Point X="-3.575290771484" Y="26.25068359375" />
                  <Point X="-3.556546386719" Y="26.26150390625" />
                  <Point X="-3.547870849609" Y="26.2671640625" />
                  <Point X="-3.531185791016" Y="26.279396484375" />
                  <Point X="-3.515930664062" Y="26.293373046875" />
                  <Point X="-3.502289550781" Y="26.308927734375" />
                  <Point X="-3.495894042969" Y="26.317078125" />
                  <Point X="-3.483479492188" Y="26.33480859375" />
                  <Point X="-3.478011230469" Y="26.3436015625" />
                  <Point X="-3.468062988281" Y="26.361734375" />
                  <Point X="-3.45629296875" Y="26.388673828125" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.43601171875" Y="26.604529296875" />
                  <Point X="-3.443507568359" Y="26.623806640625" />
                  <Point X="-3.456578857422" Y="26.650140625" />
                  <Point X="-3.477522216797" Y="26.69037109375" />
                  <Point X="-3.482800537109" Y="26.699287109375" />
                  <Point X="-3.494293212891" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.143663085938" Y="27.229865234375" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.002292724609" Y="27.680310546875" />
                  <Point X="-3.889604980469" Y="27.82515625" />
                  <Point X="-3.726337890625" Y="28.03501171875" />
                  <Point X="-3.447367431641" Y="27.87394921875" />
                  <Point X="-3.254156982422" Y="27.7623984375" />
                  <Point X="-3.244925537109" Y="27.757720703125" />
                  <Point X="-3.225998535156" Y="27.749390625" />
                  <Point X="-3.216302978516" Y="27.745740234375" />
                  <Point X="-3.195661376953" Y="27.73923046875" />
                  <Point X="-3.185621582031" Y="27.736658203125" />
                  <Point X="-3.165325195312" Y="27.73262109375" />
                  <Point X="-3.155068603516" Y="27.73115625" />
                  <Point X="-3.129765136719" Y="27.728943359375" />
                  <Point X="-3.069519287109" Y="27.723671875" />
                  <Point X="-3.059170654297" Y="27.723333984375" />
                  <Point X="-3.038489501953" Y="27.72378515625" />
                  <Point X="-3.028156982422" Y="27.72457421875" />
                  <Point X="-3.006698242188" Y="27.727400390625" />
                  <Point X="-2.996521240234" Y="27.729310546875" />
                  <Point X="-2.976432373047" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886612304688" Y="27.786142578125" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.860941162109" Y="27.811017578125" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751011230469" Y="28.069703125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.053116943359" Y="28.6890546875" />
                  <Point X="-3.051346191406" Y="28.706080078125" />
                  <Point X="-2.648374511719" Y="29.015037109375" />
                  <Point X="-2.470901123047" Y="29.113634765625" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.177711914062" Y="29.2489921875" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111818603516" Y="29.164046875" />
                  <Point X="-2.097514892578" Y="29.14910546875" />
                  <Point X="-2.089956298828" Y="29.14202734375" />
                  <Point X="-2.073376464844" Y="29.128115234375" />
                  <Point X="-2.065093505859" Y="29.121900390625" />
                  <Point X="-2.0478984375" Y="29.11041015625" />
                  <Point X="-2.038986572266" Y="29.105134765625" />
                  <Point X="-2.01082409668" Y="29.09047265625" />
                  <Point X="-1.943770751953" Y="29.05556640625" />
                  <Point X="-1.934341064453" Y="29.05129296875" />
                  <Point X="-1.915065551758" Y="29.043794921875" />
                  <Point X="-1.905219238281" Y="29.0405703125" />
                  <Point X="-1.88431237793" Y="29.034966796875" />
                  <Point X="-1.874173950195" Y="29.032833984375" />
                  <Point X="-1.853723999023" Y="29.029685546875" />
                  <Point X="-1.833053710938" Y="29.028783203125" />
                  <Point X="-1.812407226562" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.780804443359" Y="29.03513671875" />
                  <Point X="-1.770712646484" Y="29.0374921875" />
                  <Point X="-1.75085949707" Y="29.04328125" />
                  <Point X="-1.741097900391" Y="29.04671484375" />
                  <Point X="-1.711764648438" Y="29.058865234375" />
                  <Point X="-1.641923950195" Y="29.087794921875" />
                  <Point X="-1.632590087891" Y="29.092271484375" />
                  <Point X="-1.614456176758" Y="29.10221875" />
                  <Point X="-1.60565612793" Y="29.107689453125" />
                  <Point X="-1.587926757812" Y="29.120103515625" />
                  <Point X="-1.579778686523" Y="29.126498046875" />
                  <Point X="-1.56422644043" Y="29.14013671875" />
                  <Point X="-1.550251220703" Y="29.155388671875" />
                  <Point X="-1.538020019531" Y="29.1720703125" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.51685534668" Y="29.208728515625" />
                  <Point X="-1.508524902344" Y="29.227662109375" />
                  <Point X="-1.504876953125" Y="29.23735546875" />
                  <Point X="-1.495329467773" Y="29.26763671875" />
                  <Point X="-1.47259753418" Y="29.339732421875" />
                  <Point X="-1.470026367188" Y="29.349763671875" />
                  <Point X="-1.465991088867" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.452848144531" Y="29.570060546875" />
                  <Point X="-0.931164794922" Y="29.7163203125" />
                  <Point X="-0.716033874512" Y="29.7415" />
                  <Point X="-0.365222625732" Y="29.78255859375" />
                  <Point X="-0.28282333374" Y="29.4750390625" />
                  <Point X="-0.225666244507" Y="29.2617265625" />
                  <Point X="-0.220435317993" Y="29.247107421875" />
                  <Point X="-0.207661773682" Y="29.218916015625" />
                  <Point X="-0.200119155884" Y="29.20534375" />
                  <Point X="-0.182260925293" Y="29.1786171875" />
                  <Point X="-0.172608764648" Y="29.166455078125" />
                  <Point X="-0.151451248169" Y="29.143865234375" />
                  <Point X="-0.126896362305" Y="29.1250234375" />
                  <Point X="-0.09960043335" Y="29.11043359375" />
                  <Point X="-0.085354034424" Y="29.1042578125" />
                  <Point X="-0.054915912628" Y="29.09392578125" />
                  <Point X="-0.03985357666" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629491806" Y="29.08511328125" />
                  <Point X="0.052165405273" Y="29.090154296875" />
                  <Point X="0.067227592468" Y="29.09392578125" />
                  <Point X="0.097665863037" Y="29.1042578125" />
                  <Point X="0.111912406921" Y="29.11043359375" />
                  <Point X="0.139208343506" Y="29.1250234375" />
                  <Point X="0.16376322937" Y="29.143865234375" />
                  <Point X="0.18492074585" Y="29.166455078125" />
                  <Point X="0.194573059082" Y="29.1786171875" />
                  <Point X="0.212431289673" Y="29.20534375" />
                  <Point X="0.219973449707" Y="29.218916015625" />
                  <Point X="0.232746994019" Y="29.247107421875" />
                  <Point X="0.237978225708" Y="29.2617265625" />
                  <Point X="0.374065673828" Y="29.76961328125" />
                  <Point X="0.378190582275" Y="29.7850078125" />
                  <Point X="0.827879760742" Y="29.737912109375" />
                  <Point X="1.005879211426" Y="29.6949375" />
                  <Point X="1.453591430664" Y="29.586845703125" />
                  <Point X="1.567443969727" Y="29.54555078125" />
                  <Point X="1.858254516602" Y="29.4400703125" />
                  <Point X="1.970297973633" Y="29.387673828125" />
                  <Point X="2.250427734375" Y="29.2566640625" />
                  <Point X="2.358744628906" Y="29.193560546875" />
                  <Point X="2.629427490234" Y="29.035861328125" />
                  <Point X="2.731517333984" Y="28.963259765625" />
                  <Point X="2.817779052734" Y="28.901916015625" />
                  <Point X="2.320156982422" Y="28.0400078125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053180419922" Y="27.573435546875" />
                  <Point X="2.044182495117" Y="27.549564453125" />
                  <Point X="2.041302368164" Y="27.540599609375" />
                  <Point X="2.034951904297" Y="27.516853515625" />
                  <Point X="2.019832519531" Y="27.4603125" />
                  <Point X="2.017912719727" Y="27.45146484375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.015887329102" Y="27.349044921875" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023801025391" Y="27.289033203125" />
                  <Point X="2.029144775391" Y="27.267107421875" />
                  <Point X="2.032470214844" Y="27.256302734375" />
                  <Point X="2.040736816406" Y="27.23421484375" />
                  <Point X="2.04532019043" Y="27.223884765625" />
                  <Point X="2.055679931641" Y="27.203845703125" />
                  <Point X="2.061456542969" Y="27.19413671875" />
                  <Point X="2.074162353516" Y="27.17541015625" />
                  <Point X="2.104414306641" Y="27.130828125" />
                  <Point X="2.109801513672" Y="27.123642578125" />
                  <Point X="2.130456298828" Y="27.10012890625" />
                  <Point X="2.15758984375" Y="27.075390625" />
                  <Point X="2.168250244141" Y="27.066984375" />
                  <Point X="2.186975341797" Y="27.05427734375" />
                  <Point X="2.231558837891" Y="27.024025390625" />
                  <Point X="2.241274414062" Y="27.01824609375" />
                  <Point X="2.261323242188" Y="27.007880859375" />
                  <Point X="2.271656494141" Y="27.003294921875" />
                  <Point X="2.293744140625" Y="26.995029296875" />
                  <Point X="2.304548583984" Y="26.991705078125" />
                  <Point X="2.326478027344" Y="26.986361328125" />
                  <Point X="2.337603027344" Y="26.984341796875" />
                  <Point X="2.358137207031" Y="26.9818671875" />
                  <Point X="2.407027832031" Y="26.975970703125" />
                  <Point X="2.416054199219" Y="26.975318359375" />
                  <Point X="2.447581054688" Y="26.97549609375" />
                  <Point X="2.484324462891" Y="26.97982421875" />
                  <Point X="2.4977578125" Y="26.9823984375" />
                  <Point X="2.521504638672" Y="26.98875" />
                  <Point X="2.578044189453" Y="27.003869140625" />
                  <Point X="2.583996337891" Y="27.005671875" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627661132812" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.687006591797" Y="27.634654296875" />
                  <Point X="3.940403320312" Y="27.780953125" />
                  <Point X="4.043951904297" Y="27.63704296875" />
                  <Point X="4.100862792969" Y="27.54299609375" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.508483398438" Y="27.001283203125" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.152117919922" Y="26.72521484375" />
                  <Point X="3.134669433594" Y="26.706603515625" />
                  <Point X="3.128578613281" Y="26.69942578125" />
                  <Point X="3.111488037109" Y="26.677130859375" />
                  <Point X="3.070796386719" Y="26.624044921875" />
                  <Point X="3.06563671875" Y="26.616603515625" />
                  <Point X="3.049738769531" Y="26.58937109375" />
                  <Point X="3.034762695312" Y="26.555544921875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.023774169922" Y="26.519908203125" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.009925537109" Y="26.3272421875" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682861328" Y="26.228611328125" />
                  <Point X="3.050285400391" Y="26.195373046875" />
                  <Point X="3.056917236328" Y="26.18352734375" />
                  <Point X="3.071131347656" Y="26.161921875" />
                  <Point X="3.104974853516" Y="26.110482421875" />
                  <Point X="3.111737548828" Y="26.10142578125" />
                  <Point X="3.126294921875" Y="26.08417578125" />
                  <Point X="3.134089599609" Y="26.075982421875" />
                  <Point X="3.151333007812" Y="26.05989453125" />
                  <Point X="3.160042724609" Y="26.052689453125" />
                  <Point X="3.178247558594" Y="26.039369140625" />
                  <Point X="3.187742675781" Y="26.03325390625" />
                  <Point X="3.208341064453" Y="26.021658203125" />
                  <Point X="3.257384765625" Y="25.99405078125" />
                  <Point X="3.265475097656" Y="25.989990234375" />
                  <Point X="3.294677246094" Y="25.978083984375" />
                  <Point X="3.330278564453" Y="25.96801953125" />
                  <Point X="3.343678466797" Y="25.965255859375" />
                  <Point X="3.371528808594" Y="25.961576171875" />
                  <Point X="3.437839111328" Y="25.9528125" />
                  <Point X="3.444034423828" Y="25.95219921875" />
                  <Point X="3.465708251953" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.49895703125" Y="26.084232421875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.770618164062" Y="25.79904296875" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.076518554688" Y="25.524388671875" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686021728516" Y="25.419541015625" />
                  <Point X="3.665623535156" Y="25.412134765625" />
                  <Point X="3.642384521484" Y="25.401619140625" />
                  <Point X="3.634010498047" Y="25.397318359375" />
                  <Point X="3.6066484375" Y="25.38150390625" />
                  <Point X="3.541500488281" Y="25.34384765625" />
                  <Point X="3.533885498047" Y="25.338947265625" />
                  <Point X="3.508776611328" Y="25.319873046875" />
                  <Point X="3.481993896484" Y="25.2943515625" />
                  <Point X="3.472795654297" Y="25.284224609375" />
                  <Point X="3.456378417969" Y="25.2633046875" />
                  <Point X="3.417289794922" Y="25.21349609375" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130615234" Y="25.1849296875" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.364904296875" Y="25.0818984375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603759766" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.357347167969" Y="24.895001953125" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816013671875" />
                  <Point X="3.38000390625" Y="24.794513671875" />
                  <Point X="3.384067626953" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.762501953125" />
                  <Point X="3.399130615234" Y="24.752509765625" />
                  <Point X="3.410854736328" Y="24.73323046875" />
                  <Point X="3.417289794922" Y="24.723943359375" />
                  <Point X="3.43370703125" Y="24.7030234375" />
                  <Point X="3.472795654297" Y="24.65321484375" />
                  <Point X="3.478716796875" Y="24.6463671875" />
                  <Point X="3.501139160156" Y="24.624193359375" />
                  <Point X="3.530179199219" Y="24.601271484375" />
                  <Point X="3.541500488281" Y="24.593591796875" />
                  <Point X="3.568862792969" Y="24.57777734375" />
                  <Point X="3.634010498047" Y="24.540119140625" />
                  <Point X="3.639496582031" Y="24.53718359375" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026367188" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.607527832031" Y="24.270767578125" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761613769531" Y="24.06894921875" />
                  <Point X="4.738636230469" Y="23.9682578125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.882724853516" Y="24.032037109375" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366720947266" Y="24.089158203125" />
                  <Point X="3.354483154297" Y="24.087322265625" />
                  <Point X="3.300780761719" Y="24.075650390625" />
                  <Point X="3.172918945312" Y="24.047859375" />
                  <Point X="3.157875244141" Y="24.0432578125" />
                  <Point X="3.128753417969" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123291016" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="3.006889404297" Y="23.927736328125" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.870504882812" Y="23.652783203125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.926260498047" Y="23.334400390625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.902418945312" Y="22.51178125" />
                  <Point X="4.087169677734" Y="22.370015625" />
                  <Point X="4.045488769531" Y="22.3025703125" />
                  <Point X="4.001273925781" Y="22.23974609375" />
                  <Point X="3.245626953125" Y="22.67601953125" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.707194335938" Y="22.945205078125" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444845458984" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400592285156" Y="22.948890625" />
                  <Point X="2.347495117188" Y="22.920947265625" />
                  <Point X="2.22107421875" Y="22.854412109375" />
                  <Point X="2.208972167969" Y="22.846830078125" />
                  <Point X="2.186040771484" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144941162109" Y="22.78883984375" />
                  <Point X="2.128046875" Y="22.765908203125" />
                  <Point X="2.120463134766" Y="22.7538046875" />
                  <Point X="2.092518554688" Y="22.70070703125" />
                  <Point X="2.02598425293" Y="22.574287109375" />
                  <Point X="2.019835571289" Y="22.559806640625" />
                  <Point X="2.010012084961" Y="22.53003125" />
                  <Point X="2.006337158203" Y="22.514736328125" />
                  <Point X="2.001379394531" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.01373059082" Y="22.3559453125" />
                  <Point X="2.041213378906" Y="22.203767578125" />
                  <Point X="2.043014770508" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.615514648438" Y="21.18077734375" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.72375390625" Y="20.9639140625" />
                  <Point X="2.136993896484" Y="21.728595703125" />
                  <Point X="1.833914672852" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808835693359" Y="22.150369140625" />
                  <Point X="1.78325390625" Y="22.171994140625" />
                  <Point X="1.773299682617" Y="22.179353515625" />
                  <Point X="1.710262939453" Y="22.219880859375" />
                  <Point X="1.560176269531" Y="22.31637109375" />
                  <Point X="1.546279663086" Y="22.323755859375" />
                  <Point X="1.517465209961" Y="22.336126953125" />
                  <Point X="1.502547363281" Y="22.34111328125" />
                  <Point X="1.470927001953" Y="22.34884375" />
                  <Point X="1.455391113281" Y="22.351302734375" />
                  <Point X="1.424125366211" Y="22.35362109375" />
                  <Point X="1.408395507812" Y="22.35348046875" />
                  <Point X="1.339454101562" Y="22.34713671875" />
                  <Point X="1.17530871582" Y="22.332033203125" />
                  <Point X="1.161225708008" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.1200078125" Y="22.318369140625" />
                  <Point X="1.092621826172" Y="22.307025390625" />
                  <Point X="1.079875854492" Y="22.300583984375" />
                  <Point X="1.055493896484" Y="22.28586328125" />
                  <Point X="1.043857299805" Y="22.277583984375" />
                  <Point X="0.990622619629" Y="22.2333203125" />
                  <Point X="0.863873596191" Y="22.127931640625" />
                  <Point X="0.852653869629" Y="22.11691015625" />
                  <Point X="0.832183776855" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041259766" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394287109" Y="22.00941015625" />
                  <Point X="0.782791748047" Y="21.9943671875" />
                  <Point X="0.766874816895" Y="21.92113671875" />
                  <Point X="0.728977478027" Y="21.74678125" />
                  <Point X="0.727584594727" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091247559" Y="20.84766015625" />
                  <Point X="0.773547119141" Y="21.0698828125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605957031" Y="21.519876953125" />
                  <Point X="0.642146911621" Y="21.546416015625" />
                  <Point X="0.626788391113" Y="21.57618359375" />
                  <Point X="0.620407409668" Y="21.58679296875" />
                  <Point X="0.571980712891" Y="21.65656640625" />
                  <Point X="0.456679656982" Y="21.822693359375" />
                  <Point X="0.446668273926" Y="21.83483203125" />
                  <Point X="0.424784881592" Y="21.85728515625" />
                  <Point X="0.41291317749" Y="21.867599609375" />
                  <Point X="0.386658050537" Y="21.88684375" />
                  <Point X="0.373244995117" Y="21.895060546875" />
                  <Point X="0.345243377686" Y="21.909169921875" />
                  <Point X="0.330654571533" Y="21.9150625" />
                  <Point X="0.255716964722" Y="21.9383203125" />
                  <Point X="0.077295455933" Y="21.9936953125" />
                  <Point X="0.063377220154" Y="21.996890625" />
                  <Point X="0.035217788696" Y="22.00116015625" />
                  <Point X="0.02097659111" Y="22.002234375" />
                  <Point X="-0.008664463997" Y="22.002234375" />
                  <Point X="-0.022905660629" Y="22.00116015625" />
                  <Point X="-0.051065093994" Y="21.996890625" />
                  <Point X="-0.064983482361" Y="21.9936953125" />
                  <Point X="-0.139920928955" Y="21.9704375" />
                  <Point X="-0.31834274292" Y="21.9150625" />
                  <Point X="-0.332931427002" Y="21.909169921875" />
                  <Point X="-0.360933013916" Y="21.895060546875" />
                  <Point X="-0.374346069336" Y="21.88684375" />
                  <Point X="-0.400601348877" Y="21.867599609375" />
                  <Point X="-0.412474853516" Y="21.857283203125" />
                  <Point X="-0.434358825684" Y="21.834828125" />
                  <Point X="-0.444369018555" Y="21.822689453125" />
                  <Point X="-0.49279586792" Y="21.7529140625" />
                  <Point X="-0.608096618652" Y="21.5867890625" />
                  <Point X="-0.612471191406" Y="21.5798671875" />
                  <Point X="-0.625976257324" Y="21.554736328125" />
                  <Point X="-0.638777709961" Y="21.52378515625" />
                  <Point X="-0.642753112793" Y="21.512064453125" />
                  <Point X="-0.924851989746" Y="20.459255859375" />
                  <Point X="-0.985424926758" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.36198619459" Y="29.770480021669" />
                  <Point X="-1.467624158133" Y="29.474225222217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.337398473857" Y="29.678717044444" />
                  <Point X="-1.465061447143" Y="29.376560661418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.13736874526" Y="29.196416463822" />
                  <Point X="-2.493806949793" Y="29.100909134766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.412806183514" Y="29.781382539406" />
                  <Point X="0.374466572575" Y="29.771109471617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.312810753124" Y="29.586954067219" />
                  <Point X="-1.494481415618" Y="29.270326367485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.060319762532" Y="29.118710439382" />
                  <Point X="-2.756436148584" Y="28.932186615902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676709783833" Y="29.753744058852" />
                  <Point X="0.346075106519" Y="29.665150764076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.28822303239" Y="29.495191089995" />
                  <Point X="-1.548752024886" Y="29.157433364419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.938254490608" Y="29.053066493279" />
                  <Point X="-2.953632622255" Y="28.780996742892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.910359220945" Y="29.717998999699" />
                  <Point X="0.317683637113" Y="29.559192055638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.263635187074" Y="29.403428146152" />
                  <Point X="-3.036583929275" Y="28.660418770026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.103439330342" Y="29.671383421948" />
                  <Point X="0.289292167706" Y="29.453233347199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.239047306698" Y="29.311665211703" />
                  <Point X="-2.98740815764" Y="28.575244141184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.296519386246" Y="29.624767829863" />
                  <Point X="0.260900698299" Y="29.347274638761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.208796209067" Y="29.221419731745" />
                  <Point X="-2.938232386005" Y="28.490069512342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.482675156408" Y="29.576296881005" />
                  <Point X="0.22979310663" Y="29.240588147556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.146205143282" Y="29.139839720136" />
                  <Point X="-2.88905661437" Y="28.4048948835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.638625460517" Y="29.519732401911" />
                  <Point X="0.112727536434" Y="29.110869285421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.016604820267" Y="29.08511328125" />
                  <Point X="-2.839880842735" Y="28.319720254657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.794574869982" Y="29.463167683099" />
                  <Point X="-2.7907050711" Y="28.234545625815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665805017428" Y="28.0000633019" />
                  <Point X="-3.77663086817" Y="27.970367604693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.93736200873" Y="29.403076144477" />
                  <Point X="-2.758656320339" Y="28.144781825561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549453677565" Y="27.932888312316" />
                  <Point X="-3.873299917795" Y="27.846113973774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.071061009513" Y="29.340549446627" />
                  <Point X="-2.749196904854" Y="28.048965231162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.433102467532" Y="27.865713287944" />
                  <Point X="-3.969967262763" Y="27.721860799617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.204758623157" Y="29.278022377094" />
                  <Point X="-2.760728602987" Y="27.947524084821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.316752186608" Y="27.798538014617" />
                  <Point X="-4.047586773377" Y="27.602711477292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326563555995" Y="29.212308673343" />
                  <Point X="-2.845499834899" Y="27.82645846455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182761849597" Y="27.736089380074" />
                  <Point X="-4.115636744798" Y="27.486126305266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.442197072104" Y="29.144941343463" />
                  <Point X="-4.183686716218" Y="27.36954113324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557830008181" Y="29.077573858164" />
                  <Point X="-4.193938539723" Y="27.268442928272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667681102542" Y="29.008657133047" />
                  <Point X="-4.09893861193" Y="27.195546845066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768132156001" Y="28.937221674562" />
                  <Point X="-4.003938803519" Y="27.122650729872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.783804204422" Y="28.843069750141" />
                  <Point X="-3.908938995109" Y="27.049754614677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.716629197992" Y="28.726719024277" />
                  <Point X="-3.813939186699" Y="26.976858499483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.649454191563" Y="28.610368298414" />
                  <Point X="-3.718939378289" Y="26.903962384289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.582279185134" Y="28.494017572551" />
                  <Point X="-3.623939569879" Y="26.831066269094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.515104178704" Y="28.377666846688" />
                  <Point X="-3.530685398322" Y="26.757702411915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.447929172275" Y="28.261316120824" />
                  <Point X="-3.469858485471" Y="26.675649696953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.380754165846" Y="28.144965394961" />
                  <Point X="-3.430926503555" Y="26.587730252928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.50156002685" Y="26.300854864971" />
                  <Point X="-4.64900167098" Y="26.261347995496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313579142378" Y="28.028614664532" />
                  <Point X="-3.423650536338" Y="26.491328605328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255436488087" Y="26.268452231282" />
                  <Point X="-4.677739778962" Y="26.155296405531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24640396195" Y="27.912263892046" />
                  <Point X="-3.458491513001" Y="26.383641756629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009312981075" Y="26.236049589085" />
                  <Point X="-4.70647781562" Y="26.049244834677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179228781523" Y="27.79591311956" />
                  <Point X="-3.564587300273" Y="26.25686223897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75949604967" Y="26.204636596972" />
                  <Point X="-4.733379815881" Y="25.943685228294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.112053601095" Y="27.679562347074" />
                  <Point X="-4.748534099763" Y="25.841273413027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.049826358176" Y="27.564537370448" />
                  <Point X="-4.763688383644" Y="25.73886159776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019334167033" Y="27.458015775317" />
                  <Point X="-4.778842667526" Y="25.636449782493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01475469283" Y="27.358437471764" />
                  <Point X="-4.681887372858" Y="25.564077638262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.943353672042" Y="27.776852773628" />
                  <Point X="3.9245957288" Y="27.771826597885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030044784315" Y="27.26418320229" />
                  <Point X="-4.49836171104" Y="25.514901953998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002682424547" Y="27.694398627811" />
                  <Point X="3.606720002194" Y="27.588300816508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072874567043" Y="27.177308170845" />
                  <Point X="-4.314835565331" Y="25.465726399392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.059540448658" Y="27.611282452315" />
                  <Point X="3.288843758754" Y="27.404774896646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135324594685" Y="27.09569036818" />
                  <Point X="-4.131309419622" Y="25.416550844785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.110752353788" Y="27.526653403799" />
                  <Point X="2.970967515314" Y="27.221248976785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.23260238696" Y="27.023404636923" />
                  <Point X="-3.947783273913" Y="25.367375290179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040430151096" Y="27.409459389239" />
                  <Point X="2.653091271874" Y="27.037723056923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420282736909" Y="26.975342197988" />
                  <Point X="-3.764257128204" Y="25.318199735573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.843481916349" Y="27.258336031649" />
                  <Point X="-3.580730982495" Y="25.269024180967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.646533681602" Y="27.107212674058" />
                  <Point X="-3.428523347707" Y="25.211456856651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449585766248" Y="26.956089402049" />
                  <Point X="-3.347022759315" Y="25.134943636354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.252638599519" Y="26.804966330634" />
                  <Point X="-3.306597565939" Y="25.047424297134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.103445770583" Y="26.666638995465" />
                  <Point X="-3.29490997377" Y="24.952204740878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.03251371957" Y="26.549281572539" />
                  <Point X="-3.322532885003" Y="24.846451966982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.004484153185" Y="26.443419835723" />
                  <Point X="-3.448412948975" Y="24.714371268359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.006154953745" Y="26.345516288245" />
                  <Point X="-4.774351594218" Y="24.260735842013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.026509413846" Y="26.252619012253" />
                  <Point X="-4.760804342484" Y="24.166014580036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068725066787" Y="26.165579425227" />
                  <Point X="-4.747257298435" Y="24.07129326241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.127448587481" Y="26.082963108035" />
                  <Point X="-4.728522159155" Y="23.977962090711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.226841396347" Y="26.011244093765" />
                  <Point X="-4.705009295418" Y="23.885911106422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.396308048748" Y="25.958301309281" />
                  <Point X="-4.681496431682" Y="23.793860122133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976776673378" Y="26.015486171343" />
                  <Point X="-4.570792538806" Y="23.72517190369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698378107651" Y="26.110487455775" />
                  <Point X="-3.849203132651" Y="23.820169965197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.726984147111" Y="26.019801183808" />
                  <Point X="-3.189608951134" Y="23.898556456327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.749461236259" Y="25.927472664554" />
                  <Point X="-3.045365562001" Y="23.83885511882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765275438135" Y="25.833358830037" />
                  <Point X="-2.97671877611" Y="23.758897732524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.779974789872" Y="25.738946272325" />
                  <Point X="-2.947947085614" Y="23.668255846618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.480133838887" Y="25.292303702081" />
                  <Point X="-2.959157716494" Y="23.566900729988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.391711577978" Y="25.170259791539" />
                  <Point X="-3.020872456965" Y="23.452013077979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361437628611" Y="25.063796674115" />
                  <Point X="-3.207831246248" Y="23.303566384234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349120156298" Y="24.962144980217" />
                  <Point X="-3.404779108658" Y="23.152443126411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362630615729" Y="24.867413859772" />
                  <Point X="-3.601726952375" Y="23.001319873597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387785093586" Y="24.775802744661" />
                  <Point X="-3.798674792917" Y="22.850196621634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442309812561" Y="24.692061361939" />
                  <Point X="-3.995622633458" Y="22.699073369671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.515061785198" Y="24.613203957116" />
                  <Point X="-4.178362204691" Y="22.551757211995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.626202166511" Y="24.544632695396" />
                  <Point X="-4.12779085242" Y="22.466956527857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789350698161" Y="24.489996975559" />
                  <Point X="-4.077219462203" Y="22.382155853887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972876816472" Y="24.440821413611" />
                  <Point X="-4.026647822412" Y="22.29735524679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.156402934783" Y="24.391645851664" />
                  <Point X="-2.451499066173" Y="22.621063846844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890386048048" Y="22.503464434482" />
                  <Point X="-3.969662312613" Y="22.214273230982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.339929053094" Y="24.342470289716" />
                  <Point X="3.404704172224" Y="24.091877538146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.084650706689" Y="24.006119470521" />
                  <Point X="-2.364122645982" Y="22.546125050932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.208261414709" Y="22.319938749552" />
                  <Point X="-3.907483614137" Y="22.132582725886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523455171405" Y="24.293294727769" />
                  <Point X="3.658564883889" Y="24.061548073688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963236485662" Y="23.875235390908" />
                  <Point X="-2.319895669794" Y="22.459624396347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526136781371" Y="22.136413064623" />
                  <Point X="-3.845304915661" Y="22.05089222079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70698224521" Y="24.244119421845" />
                  <Point X="3.904688744416" Y="24.029145526215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88821027286" Y="23.756780940638" />
                  <Point X="-2.313902972823" Y="22.362878897522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775983028553" Y="24.16425688888" />
                  <Point X="4.150812423464" Y="23.996742930115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870590018959" Y="23.653708370696" />
                  <Point X="-2.35429169707" Y="22.253705534337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.759938165758" Y="24.061606443713" />
                  <Point X="4.396936102511" Y="23.964340334015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86131115517" Y="23.552870869498" />
                  <Point X="-2.421466826753" Y="22.137354775449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.73603296865" Y="23.956849828314" />
                  <Point X="4.643059781559" Y="23.931937737915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.867418877959" Y="23.456156191748" />
                  <Point X="-2.488641956435" Y="22.02100401656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.904777631547" Y="23.367815202463" />
                  <Point X="-2.555817086117" Y="21.904653257671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.958716905817" Y="23.283916950305" />
                  <Point X="-2.622991986132" Y="21.788302560321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015516658872" Y="23.200785161127" />
                  <Point X="-2.690166864207" Y="21.671951868851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.102345931636" Y="23.125699757505" />
                  <Point X="2.537021581464" Y="22.974221554414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357137552125" Y="22.926021774022" />
                  <Point X="-2.757341742282" Y="21.55560117738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.197345829357" Y="23.052803666241" />
                  <Point X="2.760113997549" Y="22.935647750003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.128393550101" Y="22.766378766267" />
                  <Point X="-2.824516620358" Y="21.439250485909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.292345727079" Y="22.979907574978" />
                  <Point X="2.900931840297" Y="22.875028540108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.06660616972" Y="22.651471650453" />
                  <Point X="-2.891691498433" Y="21.322899794438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.387345624801" Y="22.907011483714" />
                  <Point X="3.017282925461" Y="22.807853482278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012880185018" Y="22.5387245791" />
                  <Point X="1.276113595083" Y="22.341308566317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.047220468358" Y="22.279976837858" />
                  <Point X="0.010671485553" Y="22.002234375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.325603589709" Y="21.912129740149" />
                  <Point X="-2.958866376508" Y="21.206549102967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.482345522522" Y="22.834115392451" />
                  <Point X="3.133634010626" Y="22.740678424447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000663068313" Y="22.437099775406" />
                  <Point X="1.556797804673" Y="22.318166436466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.871976706884" Y="22.134669176353" />
                  <Point X="0.201727621994" Y="21.955076475329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.479098607592" Y="21.772649636926" />
                  <Point X="-2.917133148341" Y="21.119380250613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.577345420244" Y="22.761219301188" />
                  <Point X="3.249985080614" Y="22.67350336255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016089761657" Y="22.342882108291" />
                  <Point X="1.665189237798" Y="22.248858596299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789291367623" Y="22.014162469333" />
                  <Point X="0.363368896598" Y="21.900036887184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.56295485809" Y="21.651829185186" />
                  <Point X="-2.822373927179" Y="21.04641967026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.672345317966" Y="22.688323209924" />
                  <Point X="3.366335760621" Y="22.606328196158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.033031826212" Y="22.249070483667" />
                  <Point X="1.77316579904" Y="22.179439591546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764259411079" Y="21.909103939654" />
                  <Point X="0.453981240419" Y="21.825965154396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.634416392763" Y="21.534329887541" />
                  <Point X="-1.822450122826" Y="21.21599720899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.1987247037" Y="21.115174738912" />
                  <Point X="-2.724219333795" Y="20.974368877151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.767345215687" Y="22.615427118661" />
                  <Point X="3.482686440628" Y="22.539153029765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056036200389" Y="22.156883250012" />
                  <Point X="1.850596593481" Y="22.101835873247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.741560083647" Y="21.804670436061" />
                  <Point X="0.511898897606" Y="21.743132906728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.665362010702" Y="21.427686797066" />
                  <Point X="-1.56739053183" Y="21.18598898328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4085216948" Y="20.960608567434" />
                  <Point X="-2.613353571196" Y="20.905724031569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.862345113409" Y="22.542531027397" />
                  <Point X="3.599037120635" Y="22.471977863373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.101680582912" Y="22.070762388309" />
                  <Point X="1.913193871234" Y="22.02025752613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724728986288" Y="21.701809319977" />
                  <Point X="0.569455994139" Y="21.660204047124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.69375353094" Y="21.321728075008" />
                  <Point X="-1.461468358209" Y="21.116019507023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.957344667668" Y="22.469634844104" />
                  <Point X="3.715387800642" Y="22.404802696981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150856281087" Y="21.985587739783" />
                  <Point X="1.975791148987" Y="21.938679179014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733286840873" Y="21.605751153063" />
                  <Point X="0.626254161804" Y="21.577071833142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.722145051178" Y="21.215769352949" />
                  <Point X="-1.375566214019" Y="21.040685680048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.052343971339" Y="22.396738593665" />
                  <Point X="3.831738480649" Y="22.337627530588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.200031979262" Y="21.900413091257" />
                  <Point X="2.03838842674" Y="21.857100831897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.745793807997" Y="21.510751147664" />
                  <Point X="0.6614709476" Y="21.488156905317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.750536571416" Y="21.10981063089" />
                  <Point X="-1.289664559419" Y="20.965351721888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.040267489661" Y="22.295151473013" />
                  <Point X="3.948089160655" Y="22.270452364196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.249207677438" Y="21.815238442732" />
                  <Point X="2.100985704493" Y="21.77552248478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758300775121" Y="21.415751142266" />
                  <Point X="0.686058749957" Y="21.396393949964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.778928091654" Y="21.003851908832" />
                  <Point X="-1.218153385979" Y="20.886161845922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.298383375613" Y="21.730063794206" />
                  <Point X="2.16358294663" Y="21.693944128121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770807742244" Y="21.320751136867" />
                  <Point X="0.710646552314" Y="21.30463099461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.807319611892" Y="20.897893186773" />
                  <Point X="-1.182048301407" Y="20.797484937037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347559073788" Y="21.64488914568" />
                  <Point X="2.226180140535" Y="21.612365758537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.783314709368" Y="21.225751131469" />
                  <Point X="0.735234354671" Y="21.212868039256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.83571113213" Y="20.791934464715" />
                  <Point X="-1.163474898747" Y="20.704110428141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396734771963" Y="21.559714497155" />
                  <Point X="2.288777334439" Y="21.530787388953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795821676492" Y="21.130751126071" />
                  <Point X="0.759822157028" Y="21.121105083903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864102652368" Y="20.685975742656" />
                  <Point X="-1.144901496087" Y="20.610735919246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.445910470138" Y="21.474539848629" />
                  <Point X="2.351374528344" Y="21.449209019369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808328643615" Y="21.035751120672" />
                  <Point X="0.784409919406" Y="21.029342117836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.892494172606" Y="20.580017020597" />
                  <Point X="-1.126328376557" Y="20.517361334486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.495086168314" Y="21.389365200103" />
                  <Point X="2.413971722249" Y="21.367630649786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820835610739" Y="20.940751115274" />
                  <Point X="0.808997631269" Y="20.937579138235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920885692844" Y="20.474058298539" />
                  <Point X="-1.12127553024" Y="20.420364003437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544261866489" Y="21.304190551578" />
                  <Point X="2.476568916154" Y="21.286052280202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.949277329544" Y="20.368099545274" />
                  <Point X="-1.134697192089" Y="20.318416442845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.593437564664" Y="21.219015903052" />
                  <Point X="2.539166110058" Y="21.204473910618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.977668985155" Y="20.262140786942" />
                  <Point X="-1.043572716728" Y="20.244481935289" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.642613172028" Y="21.133841230194" />
                  <Point X="2.601763303963" Y="21.122895541035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69178870541" Y="21.048666537512" />
                  <Point X="2.664360497868" Y="21.041317171451" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.590021179199" Y="21.02070703125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.415892150879" Y="21.54823046875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.199398239136" Y="21.756859375" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.083602111816" Y="21.7889765625" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.336705688477" Y="21.64458203125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.74132623291" Y="20.410080078125" />
                  <Point X="-0.84774420166" Y="20.012921875" />
                  <Point X="-0.890638427734" Y="20.021248046875" />
                  <Point X="-1.100231079102" Y="20.061931640625" />
                  <Point X="-1.186247070312" Y="20.0840625" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.323412719727" Y="20.340626953125" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.32758581543" Y="20.555244140625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.455275756836" Y="20.857876953125" />
                  <Point X="-1.619543701172" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.740809814453" Y="21.02023828125" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.066178466797" Y="20.9752265625" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.418110839844" Y="20.6363046875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.548591552734" Y="20.64215234375" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-2.974921142578" Y="20.92408203125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.726219482422" Y="21.9895078125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.513980224609" Y="22.431236328125" />
                  <Point X="-2.531328857422" Y="22.448583984375" />
                  <Point X="-2.560157714844" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.499709960938" Y="21.932279296875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.918965332031" Y="21.8339609375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.247079101562" Y="22.29603125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.549040527344" Y="23.28123828125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535400391" Y="23.588916015625" />
                  <Point X="-3.143543212891" Y="23.612783203125" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651855469" Y="23.649947265625" />
                  <Point X="-3.148656738281" Y="23.6789296875" />
                  <Point X="-3.168991699219" Y="23.69396875" />
                  <Point X="-3.187641357422" Y="23.7049453125" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.365826660156" Y="23.560515625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.832456542969" Y="23.61713671875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.949980957031" Y="24.146740234375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.996325683594" Y="24.75376171875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.5336875" Y="24.884271484375" />
                  <Point X="-3.514143310547" Y="24.8978359375" />
                  <Point X="-3.502324462891" Y="24.909353515625" />
                  <Point X="-3.492162597656" Y="24.932908203125" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.488383789062" Y="24.99235546875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.502324462891" Y="25.0280859375" />
                  <Point X="-3.522352050781" Y="25.04530078125" />
                  <Point X="-3.541896240234" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.602368164062" Y="25.34606640625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.978830566406" Y="25.58293359375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.872174804688" Y="26.164216796875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.067853271484" Y="26.435396484375" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731705078125" Y="26.3958671875" />
                  <Point X="-3.713536865234" Y="26.401595703125" />
                  <Point X="-3.670279052734" Y="26.415234375" />
                  <Point X="-3.651534667969" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.631830078125" Y="26.461384765625" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.625112548828" Y="26.56241015625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.259328125" Y="27.079126953125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.397758789063" Y="27.379693359375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.039566894531" Y="27.94182421875" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.352367431641" Y="28.038494140625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515380859" Y="27.92043359375" />
                  <Point X="-3.113211914062" Y="27.918220703125" />
                  <Point X="-3.052966064453" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.995292236328" Y="27.945365234375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.940288085938" Y="28.05314453125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.217661865234" Y="28.5940546875" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.166951171875" Y="28.85686328125" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.563175048828" Y="29.279724609375" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.026974365234" Y="29.36465625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246826172" Y="29.273662109375" />
                  <Point X="-1.923084228516" Y="29.259" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124023438" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.784475341797" Y="29.234400390625" />
                  <Point X="-1.714634643555" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.676536010742" Y="29.32476953125" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.682112792969" Y="29.647779296875" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.504139770508" Y="29.753005859375" />
                  <Point X="-0.968083007812" Y="29.903296875" />
                  <Point X="-0.738120361328" Y="29.9302109375" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.099297317505" Y="29.52421484375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.0242821064" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594024658" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.190539749146" Y="29.8187890625" />
                  <Point X="0.236648376465" Y="29.990869140625" />
                  <Point X="0.392159423828" Y="29.97458203125" />
                  <Point X="0.860210266113" Y="29.925564453125" />
                  <Point X="1.050470581055" Y="29.87962890625" />
                  <Point X="1.508455932617" Y="29.769056640625" />
                  <Point X="1.632228027344" Y="29.7241640625" />
                  <Point X="1.931043334961" Y="29.61578125" />
                  <Point X="2.050785644531" Y="29.559783203125" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.454389160156" Y="29.357732421875" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.841630615234" Y="29.118099609375" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.484701904297" Y="27.9450078125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.218501708984" Y="27.467767578125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.204520751953" Y="27.371791015625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682617188" Y="27.3008125" />
                  <Point X="2.231388427734" Y="27.2820859375" />
                  <Point X="2.261640380859" Y="27.23750390625" />
                  <Point X="2.274939697266" Y="27.224203125" />
                  <Point X="2.293664794922" Y="27.21149609375" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.3603359375" Y="27.172978515625" />
                  <Point X="2.380870117188" Y="27.17050390625" />
                  <Point X="2.429760742188" Y="27.164607421875" />
                  <Point X="2.4486640625" Y="27.1659453125" />
                  <Point X="2.472410888672" Y="27.172296875" />
                  <Point X="2.528950439453" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.592006591797" Y="27.799197265625" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.037611816406" Y="27.9711640625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.263417480469" Y="27.64136328125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.624147949219" Y="26.850544921875" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.262280517578" Y="26.561537109375" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.206753173828" Y="26.468736328125" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.196005615234" Y="26.36563671875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.229860839844" Y="26.26634765625" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.301546142578" Y="26.187224609375" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.368565673828" Y="26.153619140625" />
                  <Point X="3.396416015625" Y="26.149939453125" />
                  <Point X="3.462726318359" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.474157226562" Y="26.272607421875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.868330566406" Y="26.242447265625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.958356445312" Y="25.8282734375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.125694335938" Y="25.340861328125" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.701725097656" Y="25.21700390625" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.60584765625" Y="25.1460078125" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.551512695313" Y="25.04616015625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.543955566406" Y="24.930740234375" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.583176269531" Y="24.8203203125" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636576171875" Y="24.758091796875" />
                  <Point X="3.663938476562" Y="24.74227734375" />
                  <Point X="3.729086181641" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.656703613281" Y="24.454294921875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.987995605469" Y="24.296017578125" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.923874511719" Y="23.92598828125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.857925048828" Y="23.843662109375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.341133789062" Y="23.889986328125" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.152985595703" Y="23.806263671875" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.059705566406" Y="23.635373046875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.086080322266" Y="23.437150390625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.018083496094" Y="22.66251953125" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.315658691406" Y="22.378326171875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.153345214844" Y="22.125697265625" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.150626953125" Y="22.5114765625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.673426269531" Y="22.758228515625" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.43598046875" Y="22.752810546875" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.260655273438" Y="22.61221875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.200705810547" Y="22.3897109375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.780059570312" Y="21.27577734375" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.967634521484" Y="20.9043125" />
                  <Point X="2.835294189453" Y="20.80978515625" />
                  <Point X="2.778517089844" Y="20.773033203125" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.986256713867" Y="21.612931640625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.607512207031" Y="22.060060546875" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805175781" Y="22.16428125" />
                  <Point X="1.356863769531" Y="22.1579375" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.112097900391" Y="22.0872265625" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.952539855957" Y="21.88078125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.068652709961" Y="20.514041015625" />
                  <Point X="1.127642578125" Y="20.065970703125" />
                  <Point X="1.119547119141" Y="20.0641953125" />
                  <Point X="0.994366821289" Y="20.0367578125" />
                  <Point X="0.94188885498" Y="20.02722265625" />
                  <Point X="0.860200439453" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#146" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.057157025171" Y="4.568437406406" Z="0.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="-0.750088547893" Y="5.011152576731" Z="0.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.75" />
                  <Point X="-1.523793560681" Y="4.832431607231" Z="0.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.75" />
                  <Point X="-1.73784097788" Y="4.672535231898" Z="0.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.75" />
                  <Point X="-1.730377730839" Y="4.37108455871" Z="0.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.75" />
                  <Point X="-1.809765829264" Y="4.311874898676" Z="0.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.75" />
                  <Point X="-1.906152589708" Y="4.334630690897" Z="0.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.75" />
                  <Point X="-1.993462792253" Y="4.426373997216" Z="0.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.75" />
                  <Point X="-2.593613933646" Y="4.354712856332" Z="0.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.75" />
                  <Point X="-3.203529206774" Y="3.927824396627" Z="0.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.75" />
                  <Point X="-3.267119100831" Y="3.600335902768" Z="0.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.75" />
                  <Point X="-2.996253796399" Y="3.08006696677" Z="0.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.75" />
                  <Point X="-3.036803013417" Y="3.01200055447" Z="0.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.75" />
                  <Point X="-3.115009498836" Y="2.999310902774" Z="0.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.75" />
                  <Point X="-3.333523445529" Y="3.113074847915" Z="0.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.75" />
                  <Point X="-4.085186200069" Y="3.003807424264" Z="0.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.75" />
                  <Point X="-4.447096751145" Y="2.436178715395" Z="0.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.75" />
                  <Point X="-4.295921950127" Y="2.070738912664" Z="0.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.75" />
                  <Point X="-3.675618757288" Y="1.57060196404" Z="0.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.75" />
                  <Point X="-3.684179786388" Y="1.511800021463" Z="0.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.75" />
                  <Point X="-3.734727710654" Y="1.480560991" Z="0.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.75" />
                  <Point X="-4.067482941519" Y="1.516248694585" Z="0.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.75" />
                  <Point X="-4.926590731134" Y="1.20857446535" Z="0.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.75" />
                  <Point X="-5.034447788596" Y="0.621532624939" Z="0.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.75" />
                  <Point X="-4.621465187731" Y="0.329050340468" Z="0.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.75" />
                  <Point X="-3.557016482916" Y="0.035504204533" Z="0.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.75" />
                  <Point X="-3.542292996558" Y="0.008816168685" Z="0.75" />
                  <Point X="-3.539556741714" Y="0" Z="0.75" />
                  <Point X="-3.546071604577" Y="-0.020990782376" Z="0.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.75" />
                  <Point X="-3.568352112208" Y="-0.043371778601" Z="0.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.75" />
                  <Point X="-4.015422601042" Y="-0.166661716288" Z="0.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.75" />
                  <Point X="-5.005633232979" Y="-0.829056445876" Z="0.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.75" />
                  <Point X="-4.887057345651" Y="-1.363958390155" Z="0.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.75" />
                  <Point X="-4.365456245006" Y="-1.45777618905" Z="0.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.75" />
                  <Point X="-3.200509207246" Y="-1.317839698402" Z="0.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.75" />
                  <Point X="-3.198102000153" Y="-1.343398625112" Z="0.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.75" />
                  <Point X="-3.585634370471" Y="-1.647812617964" Z="0.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.75" />
                  <Point X="-4.296178538336" Y="-2.698297064641" Z="0.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.75" />
                  <Point X="-3.964907323479" Y="-3.16504085023" Z="0.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.75" />
                  <Point X="-3.480866013566" Y="-3.079740316224" Z="0.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.75" />
                  <Point X="-2.560622241776" Y="-2.567708227356" Z="0.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.75" />
                  <Point X="-2.775676696944" Y="-2.954212368926" Z="0.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.75" />
                  <Point X="-3.011580985976" Y="-4.084254973649" Z="0.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.75" />
                  <Point X="-2.581069056139" Y="-4.369078949383" Z="0.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.75" />
                  <Point X="-2.384599160376" Y="-4.362852880104" Z="0.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.75" />
                  <Point X="-2.044556126652" Y="-4.035066559667" Z="0.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.75" />
                  <Point X="-1.750235763459" Y="-3.998374184343" Z="0.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.75" />
                  <Point X="-1.494398851762" Y="-4.148435298168" Z="0.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.75" />
                  <Point X="-1.382781075999" Y="-4.423230165082" Z="0.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.75" />
                  <Point X="-1.37914098686" Y="-4.621566259706" Z="0.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.75" />
                  <Point X="-1.204861857182" Y="-4.933080869803" Z="0.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.75" />
                  <Point X="-0.906322010932" Y="-4.996554910603" Z="0.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="-0.699185907906" Y="-4.571581432015" Z="0.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="-0.301785737454" Y="-3.352646258629" Z="0.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="-0.074937432443" Y="-3.22749718202" Z="0.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.75" />
                  <Point X="0.178421646918" Y="-3.259614863269" Z="0.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.75" />
                  <Point X="0.368660121978" Y="-3.448999497139" Z="0.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.75" />
                  <Point X="0.535568952495" Y="-3.960954597801" Z="0.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.75" />
                  <Point X="0.944669373056" Y="-4.990691451087" Z="0.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.75" />
                  <Point X="1.124096479223" Y="-4.953363328145" Z="0.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.75" />
                  <Point X="1.112068949563" Y="-4.448152677499" Z="0.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.75" />
                  <Point X="0.995243079468" Y="-3.09855640717" Z="0.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.75" />
                  <Point X="1.137908954812" Y="-2.91993842732" Z="0.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.75" />
                  <Point X="1.355289097436" Y="-2.86057063576" Z="0.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.75" />
                  <Point X="1.574317184604" Y="-2.950718488005" Z="0.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.75" />
                  <Point X="1.940433011949" Y="-3.386225581639" Z="0.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.75" />
                  <Point X="2.799530223853" Y="-4.237660165037" Z="0.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.75" />
                  <Point X="2.990540318512" Y="-4.105094675021" Z="0.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.75" />
                  <Point X="2.817204970171" Y="-3.667942669256" Z="0.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.75" />
                  <Point X="2.243753987322" Y="-2.570122460065" Z="0.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.75" />
                  <Point X="2.298745589703" Y="-2.379787235635" Z="0.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.75" />
                  <Point X="2.453111142738" Y="-2.260155720264" Z="0.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.75" />
                  <Point X="2.658384343448" Y="-2.259694022169" Z="0.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.75" />
                  <Point X="3.11947092785" Y="-2.500544530103" Z="0.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.75" />
                  <Point X="4.188077680673" Y="-2.871799536062" Z="0.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.75" />
                  <Point X="4.352036392246" Y="-2.616678512558" Z="0.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.75" />
                  <Point X="4.042365429188" Y="-2.266531470456" Z="0.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.75" />
                  <Point X="3.121982132346" Y="-1.504529422708" Z="0.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.75" />
                  <Point X="3.103339217293" Y="-1.337929232723" Z="0.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.75" />
                  <Point X="3.185275910766" Y="-1.194423026522" Z="0.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.75" />
                  <Point X="3.345597561955" Y="-1.127592841103" Z="0.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.75" />
                  <Point X="3.845242759665" Y="-1.174629916552" Z="0.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.75" />
                  <Point X="4.966465567826" Y="-1.053857090953" Z="0.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.75" />
                  <Point X="5.031281102016" Y="-0.680154485062" Z="0.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.75" />
                  <Point X="4.663488382447" Y="-0.466127672458" Z="0.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.75" />
                  <Point X="3.682804910538" Y="-0.183154047019" Z="0.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.75" />
                  <Point X="3.616354041217" Y="-0.117530051707" Z="0.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.75" />
                  <Point X="3.586907165884" Y="-0.028575037378" Z="0.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.75" />
                  <Point X="3.59446428454" Y="0.068035493849" Z="0.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.75" />
                  <Point X="3.639025397183" Y="0.146418686868" Z="0.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.75" />
                  <Point X="3.720590503815" Y="0.204994797223" Z="0.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.75" />
                  <Point X="4.132479434859" Y="0.323844260311" Z="0.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.75" />
                  <Point X="5.001605156735" Y="0.867244898828" Z="0.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.75" />
                  <Point X="4.910755642967" Y="1.285554674672" Z="0.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.75" />
                  <Point X="4.461475037049" Y="1.35345987152" Z="0.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.75" />
                  <Point X="3.396811084201" Y="1.230787801038" Z="0.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.75" />
                  <Point X="3.319916975807" Y="1.26207590252" Z="0.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.75" />
                  <Point X="3.265475248129" Y="1.325111365312" Z="0.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.75" />
                  <Point X="3.238818081276" Y="1.407021112334" Z="0.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.75" />
                  <Point X="3.248749726018" Y="1.486549482523" Z="0.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.75" />
                  <Point X="3.295807602382" Y="1.562399122881" Z="0.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.75" />
                  <Point X="3.648430051777" Y="1.842157768558" Z="0.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.75" />
                  <Point X="4.300038905153" Y="2.698531471609" Z="0.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.75" />
                  <Point X="4.072041372313" Y="3.031647922369" Z="0.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.75" />
                  <Point X="3.560850620557" Y="2.873778104732" Z="0.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.75" />
                  <Point X="2.453338373359" Y="2.251879356463" Z="0.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.75" />
                  <Point X="2.380700969631" Y="2.251424485995" Z="0.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.75" />
                  <Point X="2.31558324537" Y="2.284152279049" Z="0.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.75" />
                  <Point X="2.266606321186" Y="2.341441615012" Z="0.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.75" />
                  <Point X="2.248005216763" Y="2.409057474201" Z="0.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.75" />
                  <Point X="2.260648586195" Y="2.486131202523" Z="0.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.75" />
                  <Point X="2.521847248128" Y="2.951288461398" Z="0.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.75" />
                  <Point X="2.864451767028" Y="4.190127697661" Z="0.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.75" />
                  <Point X="2.473403061537" Y="4.432215867395" Z="0.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.75" />
                  <Point X="2.065811362486" Y="4.636353955108" Z="0.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.75" />
                  <Point X="1.643120616304" Y="4.802448951991" Z="0.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.75" />
                  <Point X="1.056048821415" Y="4.959513481082" Z="0.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.75" />
                  <Point X="0.391211338109" Y="5.055590623635" Z="0.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.75" />
                  <Point X="0.136087536382" Y="4.863010099545" Z="0.75" />
                  <Point X="0" Y="4.355124473572" Z="0.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>