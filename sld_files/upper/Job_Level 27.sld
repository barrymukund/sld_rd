<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#169" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2012" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004717773438" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.768524108887" Y="20.721576171875" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557721679688" Y="21.502857421875" />
                  <Point X="0.542363098145" Y="21.532625" />
                  <Point X="0.458483581543" Y="21.653478515625" />
                  <Point X="0.378635437012" Y="21.768525390625" />
                  <Point X="0.356748931885" Y="21.790982421875" />
                  <Point X="0.330494049072" Y="21.810224609375" />
                  <Point X="0.302494659424" Y="21.824330078125" />
                  <Point X="0.17269593811" Y="21.86461328125" />
                  <Point X="0.049135520935" Y="21.902962890625" />
                  <Point X="0.020983409882" Y="21.907232421875" />
                  <Point X="-0.008658220291" Y="21.907234375" />
                  <Point X="-0.036824142456" Y="21.90296484375" />
                  <Point X="-0.166622848511" Y="21.8626796875" />
                  <Point X="-0.290183258057" Y="21.82433203125" />
                  <Point X="-0.318184509277" Y="21.810224609375" />
                  <Point X="-0.344439849854" Y="21.79098046875" />
                  <Point X="-0.366323608398" Y="21.7685234375" />
                  <Point X="-0.450203155518" Y="21.64766796875" />
                  <Point X="-0.530051269531" Y="21.532623046875" />
                  <Point X="-0.538188781738" Y="21.518427734375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.746349121094" Y="20.758384765625" />
                  <Point X="-0.916584655762" Y="20.123056640625" />
                  <Point X="-0.937439086914" Y="20.12710546875" />
                  <Point X="-1.079325317383" Y="20.154646484375" />
                  <Point X="-1.226604248047" Y="20.192541015625" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.239666992188" Y="20.248916015625" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.247517578125" Y="20.639666015625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.443147094727" Y="20.973595703125" />
                  <Point X="-1.556905395508" Y="21.073359375" />
                  <Point X="-1.583188720703" Y="21.089705078125" />
                  <Point X="-1.612885620117" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.801633789062" Y="21.119427734375" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.9834140625" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.17481640625" Y="21.01689453125" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.444781982422" Y="20.757599609375" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.593717041016" Y="20.781830078125" />
                  <Point X="-2.801724609375" Y="20.91062109375" />
                  <Point X="-3.005624023438" Y="21.067619140625" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.805143554688" Y="21.662806640625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575195312" Y="22.4148046875" />
                  <Point X="-2.41455859375" Y="22.444421875" />
                  <Point X="-2.428774902344" Y="22.473251953125" />
                  <Point X="-2.446805664062" Y="22.4984140625" />
                  <Point X="-2.464154296875" Y="22.51576171875" />
                  <Point X="-2.489310791016" Y="22.533787109375" />
                  <Point X="-2.518140136719" Y="22.54800390625" />
                  <Point X="-2.5477578125" Y="22.55698828125" />
                  <Point X="-2.578691894531" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.268009277344" Y="22.175748046875" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.91853515625" Y="21.99025" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.229049316406" Y="22.451275390625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.773249755859" Y="22.989451171875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556396484" Y="23.672013671875" />
                  <Point X="-3.052557861328" Y="23.701759765625" />
                  <Point X="-3.068641357422" Y="23.727744140625" />
                  <Point X="-3.089473632812" Y="23.75169921875" />
                  <Point X="-3.112973388672" Y="23.771232421875" />
                  <Point X="-3.139456054688" Y="23.786818359375" />
                  <Point X="-3.168715820312" Y="23.798041015625" />
                  <Point X="-3.200603759766" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.025761962891" Y="23.70110546875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.769808105469" Y="23.755734375" />
                  <Point X="-4.834078125" Y="24.007349609375" />
                  <Point X="-4.872755371094" Y="24.27777734375" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-4.293025878906" Y="24.575908203125" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.510187011719" Y="24.788953125" />
                  <Point X="-3.480403808594" Y="24.806041015625" />
                  <Point X="-3.467237548828" Y="24.815150390625" />
                  <Point X="-3.441882568359" Y="24.836060546875" />
                  <Point X="-3.425740722656" Y="24.853140625" />
                  <Point X="-3.414285400391" Y="24.87366015625" />
                  <Point X="-3.402119384766" Y="24.903669921875" />
                  <Point X="-3.397581542969" Y="24.918046875" />
                  <Point X="-3.390822753906" Y="24.947404296875" />
                  <Point X="-3.388400878906" Y="24.968806640625" />
                  <Point X="-3.390862060547" Y="24.990203125" />
                  <Point X="-3.397848388672" Y="25.020294921875" />
                  <Point X="-3.402419189453" Y="25.0346796875" />
                  <Point X="-3.414355712891" Y="25.063953125" />
                  <Point X="-3.4258515625" Y="25.08444921875" />
                  <Point X="-3.442026611328" Y="25.10149609375" />
                  <Point X="-3.4680625" Y="25.122880859375" />
                  <Point X="-3.481255859375" Y="25.13196875" />
                  <Point X="-3.510360107422" Y="25.1485859375" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.2564921875" Y="25.3517421875" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.865907714844" Y="25.6970625" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.746629882812" Y="26.26429296875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.310560058594" Y="26.371529296875" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703138671875" Y="26.305263671875" />
                  <Point X="-3.671669433594" Y="26.315185546875" />
                  <Point X="-3.641712646484" Y="26.324630859375" />
                  <Point X="-3.622784667969" Y="26.332958984375" />
                  <Point X="-3.604040283203" Y="26.343779296875" />
                  <Point X="-3.587355224609" Y="26.35601171875" />
                  <Point X="-3.573714111328" Y="26.37156640625" />
                  <Point X="-3.561299560547" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.538724365234" Y="26.4379140625" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.547286376953" Y="26.618646484375" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.017204101562" Y="27.013083984375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.242094726562" Y="27.4579296875" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.874912353516" Y="27.998751953125" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.541315917969" Y="28.03788671875" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187729736328" Y="27.836341796875" />
                  <Point X="-3.167087890625" Y="27.82983203125" />
                  <Point X="-3.146793945312" Y="27.825794921875" />
                  <Point X="-3.102966064453" Y="27.8219609375" />
                  <Point X="-3.061244628906" Y="27.818310546875" />
                  <Point X="-3.040561279297" Y="27.81876171875" />
                  <Point X="-3.019102539062" Y="27.821587890625" />
                  <Point X="-2.999013427734" Y="27.826505859375" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962210205078" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.914968505859" Y="27.891337890625" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.847270263672" Y="28.079947265625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.053724121094" Y="28.500107421875" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-2.980929931641" Y="28.879775390625" />
                  <Point X="-2.700620117188" Y="29.0946875" />
                  <Point X="-2.375804443359" Y="29.275146484375" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.14564453125" Y="29.363255859375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028892211914" Y="29.21480078125" />
                  <Point X="-2.012312744141" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.946334838867" Y="29.164001953125" />
                  <Point X="-1.899899047852" Y="29.139828125" />
                  <Point X="-1.880625854492" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.839269287109" Y="29.123580078125" />
                  <Point X="-1.818622558594" Y="29.12493359375" />
                  <Point X="-1.797307495117" Y="29.128693359375" />
                  <Point X="-1.777452148438" Y="29.134482421875" />
                  <Point X="-1.72664440918" Y="29.155529296875" />
                  <Point X="-1.678278320312" Y="29.1755625" />
                  <Point X="-1.660147460938" Y="29.185509765625" />
                  <Point X="-1.642417480469" Y="29.197923828125" />
                  <Point X="-1.626864990234" Y="29.2115625" />
                  <Point X="-1.614633300781" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.59548034668" Y="29.265921875" />
                  <Point X="-1.578943237305" Y="29.31837109375" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.578641113281" Y="29.589658203125" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.312506713867" Y="29.7080703125" />
                  <Point X="-0.949623779297" Y="29.80980859375" />
                  <Point X="-0.555858520508" Y="29.85589453125" />
                  <Point X="-0.294711181641" Y="29.886458984375" />
                  <Point X="-0.232904556274" Y="29.65579296875" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082114006042" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155911922" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.240458358765" Y="29.63803515625" />
                  <Point X="0.307419586182" Y="29.887939453125" />
                  <Point X="0.527194519043" Y="29.864921875" />
                  <Point X="0.844031005859" Y="29.831740234375" />
                  <Point X="1.169827880859" Y="29.75308203125" />
                  <Point X="1.48103918457" Y="29.6779453125" />
                  <Point X="1.692360229492" Y="29.601298828125" />
                  <Point X="1.894645507812" Y="29.527927734375" />
                  <Point X="2.099695800781" Y="29.432033203125" />
                  <Point X="2.294559082031" Y="29.340900390625" />
                  <Point X="2.492690429688" Y="29.225470703125" />
                  <Point X="2.680975585938" Y="29.115775390625" />
                  <Point X="2.867802734375" Y="28.9829140625" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.589002441406" Y="28.315662109375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142075683594" Y="27.5399296875" />
                  <Point X="2.133076660156" Y="27.516056640625" />
                  <Point X="2.122077392578" Y="27.47492578125" />
                  <Point X="2.111606933594" Y="27.43576953125" />
                  <Point X="2.108618896484" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.112016845703" Y="27.345384765625" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.162078857422" Y="27.215037109375" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.19446484375" Y="27.170328125" />
                  <Point X="2.221597167969" Y="27.14559375" />
                  <Point X="2.254030761719" Y="27.1235859375" />
                  <Point X="2.284905761719" Y="27.102634765625" />
                  <Point X="2.304944580078" Y="27.092275390625" />
                  <Point X="2.327032226562" Y="27.0840078125" />
                  <Point X="2.348966308594" Y="27.078662109375" />
                  <Point X="2.384533447266" Y="27.074375" />
                  <Point X="2.418391113281" Y="27.070291015625" />
                  <Point X="2.436466064453" Y="27.06984375" />
                  <Point X="2.473208251953" Y="27.074169921875" />
                  <Point X="2.51433984375" Y="27.085169921875" />
                  <Point X="2.553494628906" Y="27.095640625" />
                  <Point X="2.565287353516" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.11014453125" />
                  <Point X="3.316352783203" Y="27.530349609375" />
                  <Point X="3.967326660156" Y="27.906189453125" />
                  <Point X="4.011585205078" Y="27.8446796875" />
                  <Point X="4.1232734375" Y="27.689458984375" />
                  <Point X="4.227424316406" Y="27.517349609375" />
                  <Point X="4.262198730469" Y="27.459884765625" />
                  <Point X="3.811956542969" Y="27.114400390625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221421630859" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.174371337891" Y="26.6030078125" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606933594" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.110603027344" Y="26.47765625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.106791503906" Y="26.327896484375" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282714844" Y="26.23573828125" />
                  <Point X="3.160903076172" Y="26.19831640625" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198896484375" Y="26.145447265625" />
                  <Point X="3.216139648438" Y="26.129359375" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.270024414062" Y="26.095951171875" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320521484375" Y="26.069501953125" />
                  <Point X="3.356119873047" Y="26.0594375" />
                  <Point X="3.404359375" Y="26.0530625" />
                  <Point X="3.450280517578" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.179583007812" Y="26.138005859375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.797967285156" Y="26.129849609375" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.878757324219" Y="25.722001953125" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.382616210938" Y="25.5080546875" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704785644531" Y="25.325583984375" />
                  <Point X="3.681547119141" Y="25.31506640625" />
                  <Point X="3.634153320312" Y="25.287671875" />
                  <Point X="3.589037109375" Y="25.261595703125" />
                  <Point X="3.574316162109" Y="25.251099609375" />
                  <Point X="3.547530029297" Y="25.225576171875" />
                  <Point X="3.51909375" Y="25.18933984375" />
                  <Point X="3.492024169922" Y="25.15484765625" />
                  <Point X="3.480301513672" Y="25.1355703125" />
                  <Point X="3.470527099609" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.454201904297" Y="25.043109375" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.454657958984" Y="24.891951171875" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526855469" Y="24.8233359375" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.5204609375" Y="24.746357421875" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559994628906" Y="24.698767578125" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.6364296875" Y="24.64844921875" />
                  <Point X="3.681545654297" Y="24.62237109375" />
                  <Point X="3.692708984375" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.350605957031" Y="24.4379609375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.881806152344" Y="24.228921875" />
                  <Point X="4.855023925781" Y="24.051279296875" />
                  <Point X="4.81297265625" Y="23.867005859375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.196796875" Y="23.894869140625" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.281641845703" Y="23.9742734375" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163973144531" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.056174072266" Y="23.838419921875" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.961699462891" Y="23.607064453125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.027928222656" Y="23.35193359375" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.699008544922" Y="22.787609375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.200310546875" Y="22.372384765625" />
                  <Point X="4.124815429687" Y="22.250220703125" />
                  <Point X="4.037848144531" Y="22.126654296875" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.488894287109" Y="22.425873046875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786137939453" Y="22.829984375" />
                  <Point X="2.754224853516" Y="22.84017578125" />
                  <Point X="2.643519287109" Y="22.860169921875" />
                  <Point X="2.538134521484" Y="22.879201171875" />
                  <Point X="2.506777587891" Y="22.879603515625" />
                  <Point X="2.47460546875" Y="22.87464453125" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.352864501953" Y="22.816419921875" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.156129394531" Y="22.617591796875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.115668701172" Y="22.326037109375" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.529912597656" Y="21.519044921875" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.781852539062" Y="20.888357421875" />
                  <Point X="2.701763916016" Y="20.83651953125" />
                  <Point X="2.283507568359" Y="21.3816015625" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.747507080078" Y="22.07781640625" />
                  <Point X="1.721923583984" Y="22.099443359375" />
                  <Point X="1.612738037109" Y="22.169638671875" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986816406" Y="22.24883203125" />
                  <Point X="1.448366577148" Y="22.2565625" />
                  <Point X="1.417100585938" Y="22.258880859375" />
                  <Point X="1.29768737793" Y="22.247892578125" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156363891602" Y="22.230603515625" />
                  <Point X="1.128977661133" Y="22.219259765625" />
                  <Point X="1.104595092773" Y="22.204537109375" />
                  <Point X="1.012387573242" Y="22.127869140625" />
                  <Point X="0.924611572266" Y="22.054884765625" />
                  <Point X="0.904140808105" Y="22.031134765625" />
                  <Point X="0.887248779297" Y="22.00330859375" />
                  <Point X="0.875624267578" Y="21.974189453125" />
                  <Point X="0.8480546875" Y="21.84734765625" />
                  <Point X="0.821809997559" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.926890991211" Y="20.86300390625" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975713684082" Y="20.129923828125" />
                  <Point X="0.929315185547" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058395751953" Y="20.24735546875" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.154343017578" Y="20.65819921875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250210327148" Y="20.929064453125" />
                  <Point X="-1.261006835938" Y="20.94021875" />
                  <Point X="-1.380509155273" Y="21.04501953125" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735229492" Y="21.15403125" />
                  <Point X="-1.533018554688" Y="21.170376953125" />
                  <Point X="-1.546833984375" Y="21.177474609375" />
                  <Point X="-1.576530883789" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.621457885742" Y="21.201552734375" />
                  <Point X="-1.636815063477" Y="21.203830078125" />
                  <Point X="-1.795421020508" Y="21.214224609375" />
                  <Point X="-1.946404296875" Y="21.22412109375" />
                  <Point X="-1.961928466797" Y="21.2238671875" />
                  <Point X="-1.992725585938" Y="21.220833984375" />
                  <Point X="-2.007998535156" Y="21.2180546875" />
                  <Point X="-2.039047607422" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436035156" Y="21.184189453125" />
                  <Point X="-2.227595214844" Y="21.095884765625" />
                  <Point X="-2.353402587891" Y="21.011822265625" />
                  <Point X="-2.359682373047" Y="21.007244140625" />
                  <Point X="-2.380450683594" Y="20.989861328125" />
                  <Point X="-2.402763427734" Y="20.96722265625" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.543706542969" Y="20.8626015625" />
                  <Point X="-2.747608642578" Y="20.9888515625" />
                  <Point X="-2.947666259766" Y="21.142890625" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.72287109375" Y="21.615306640625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665039062" Y="22.44237890625" />
                  <Point X="-2.3236484375" Y="22.47199609375" />
                  <Point X="-2.329354492188" Y="22.4864375" />
                  <Point X="-2.343570800781" Y="22.515267578125" />
                  <Point X="-2.351554199219" Y="22.528587890625" />
                  <Point X="-2.369584960938" Y="22.55375" />
                  <Point X="-2.379632324219" Y="22.565591796875" />
                  <Point X="-2.396980957031" Y="22.582939453125" />
                  <Point X="-2.408822021484" Y="22.592984375" />
                  <Point X="-2.433978515625" Y="22.611009765625" />
                  <Point X="-2.447293945312" Y="22.618990234375" />
                  <Point X="-2.476123291016" Y="22.63320703125" />
                  <Point X="-2.490563232422" Y="22.6389140625" />
                  <Point X="-2.520180908203" Y="22.6478984375" />
                  <Point X="-2.550869140625" Y="22.6519375" />
                  <Point X="-2.581803222656" Y="22.650923828125" />
                  <Point X="-2.597226806641" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.315509277344" Y="22.25801953125" />
                  <Point X="-3.793087890625" Y="21.9822890625" />
                  <Point X="-3.842941650391" Y="22.047787109375" />
                  <Point X="-4.004016357422" Y="22.25940625" />
                  <Point X="-4.147456542969" Y="22.49993359375" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.715417480469" Y="22.91408203125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950786621094" Y="23.678623046875" />
                  <Point X="-2.953083496094" Y="23.693779296875" />
                  <Point X="-2.960084960938" Y="23.723525390625" />
                  <Point X="-2.971779785156" Y="23.751759765625" />
                  <Point X="-2.98786328125" Y="23.777744140625" />
                  <Point X="-2.996956542969" Y="23.790083984375" />
                  <Point X="-3.017788818359" Y="23.8140390625" />
                  <Point X="-3.028747802734" Y="23.824755859375" />
                  <Point X="-3.052247558594" Y="23.8442890625" />
                  <Point X="-3.064788330078" Y="23.85310546875" />
                  <Point X="-3.091270996094" Y="23.86869140625" />
                  <Point X="-3.105435302734" Y="23.875517578125" />
                  <Point X="-3.134695068359" Y="23.886740234375" />
                  <Point X="-3.149790527344" Y="23.89113671875" />
                  <Point X="-3.181678466797" Y="23.897619140625" />
                  <Point X="-3.197294677734" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.038161865234" Y="23.79529296875" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.677763183594" Y="23.77924609375" />
                  <Point X="-4.740762695312" Y="24.025888671875" />
                  <Point X="-4.778712402344" Y="24.291228515625" />
                  <Point X="-4.786451660156" Y="24.345341796875" />
                  <Point X="-4.268437988281" Y="24.48414453125" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.496635986328" Y="24.6917734375" />
                  <Point X="-3.473947021484" Y="24.70113671875" />
                  <Point X="-3.46291015625" Y="24.706552734375" />
                  <Point X="-3.433126953125" Y="24.723640625" />
                  <Point X="-3.426351806641" Y="24.727916015625" />
                  <Point X="-3.406794433594" Y="24.741859375" />
                  <Point X="-3.381439453125" Y="24.76276953125" />
                  <Point X="-3.372837890625" Y="24.77080859375" />
                  <Point X="-3.356696044922" Y="24.787888671875" />
                  <Point X="-3.342791259766" Y="24.80683203125" />
                  <Point X="-3.3313359375" Y="24.8273515625" />
                  <Point X="-3.326245117188" Y="24.83796875" />
                  <Point X="-3.314079101562" Y="24.867978515625" />
                  <Point X="-3.311524902344" Y="24.875076171875" />
                  <Point X="-3.305003417969" Y="24.896732421875" />
                  <Point X="-3.298244628906" Y="24.92608984375" />
                  <Point X="-3.296425292969" Y="24.93672265625" />
                  <Point X="-3.294003417969" Y="24.958125" />
                  <Point X="-3.294023193359" Y="24.979662109375" />
                  <Point X="-3.296484375" Y="25.00105859375" />
                  <Point X="-3.298323242188" Y="25.0116875" />
                  <Point X="-3.305309570312" Y="25.041779296875" />
                  <Point X="-3.307309326172" Y="25.049064453125" />
                  <Point X="-3.314451171875" Y="25.070548828125" />
                  <Point X="-3.326387695312" Y="25.099822265625" />
                  <Point X="-3.331498779297" Y="25.11042578125" />
                  <Point X="-3.342994628906" Y="25.130921875" />
                  <Point X="-3.356937255859" Y="25.14983984375" />
                  <Point X="-3.373112304688" Y="25.16688671875" />
                  <Point X="-3.381729492188" Y="25.174908203125" />
                  <Point X="-3.407765380859" Y="25.19629296875" />
                  <Point X="-3.414171875" Y="25.2011171875" />
                  <Point X="-3.434152099609" Y="25.21446875" />
                  <Point X="-3.463256347656" Y="25.2310859375" />
                  <Point X="-3.474214111328" Y="25.23644140625" />
                  <Point X="-3.496729980469" Y="25.245705078125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.231904296875" Y="25.443505859375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.771931152344" Y="25.68315625" />
                  <Point X="-4.73133203125" Y="25.957521484375" />
                  <Point X="-4.654936523438" Y="26.2394453125" />
                  <Point X="-4.633585449219" Y="26.318236328125" />
                  <Point X="-4.322959960938" Y="26.277341796875" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736703857422" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704888916016" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.674572509766" Y="26.21466015625" />
                  <Point X="-3.643103271484" Y="26.22458203125" />
                  <Point X="-3.613146484375" Y="26.23402734375" />
                  <Point X="-3.603453125" Y="26.23767578125" />
                  <Point X="-3.584525146484" Y="26.24600390625" />
                  <Point X="-3.575290527344" Y="26.25068359375" />
                  <Point X="-3.556546142578" Y="26.26150390625" />
                  <Point X="-3.547870605469" Y="26.2671640625" />
                  <Point X="-3.531185546875" Y="26.279396484375" />
                  <Point X="-3.515930419922" Y="26.293373046875" />
                  <Point X="-3.502289306641" Y="26.308927734375" />
                  <Point X="-3.495893798828" Y="26.317078125" />
                  <Point X="-3.483479248047" Y="26.33480859375" />
                  <Point X="-3.478010498047" Y="26.343603515625" />
                  <Point X="-3.4680625" Y="26.361736328125" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.450956054688" Y="26.40155859375" />
                  <Point X="-3.438935791016" Y="26.430578125" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012207031" Y="26.60453125" />
                  <Point X="-3.443509521484" Y="26.623810546875" />
                  <Point X="-3.447784667969" Y="26.63324609375" />
                  <Point X="-3.463020751953" Y="26.662513671875" />
                  <Point X="-3.477524414062" Y="26.690375" />
                  <Point X="-3.482800048828" Y="26.699287109375" />
                  <Point X="-3.494291992188" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521501708984" Y="26.748912109375" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.959371582031" Y="27.088453125" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.160048339844" Y="27.410041015625" />
                  <Point X="-4.002294921875" Y="27.68030859375" />
                  <Point X="-3.799931640625" Y="27.94041796875" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.588816162109" Y="27.955615234375" />
                  <Point X="-3.254157470703" Y="27.762400390625" />
                  <Point X="-3.244925048828" Y="27.757720703125" />
                  <Point X="-3.225997558594" Y="27.749390625" />
                  <Point X="-3.216302490234" Y="27.745740234375" />
                  <Point X="-3.195660644531" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165329345703" Y="27.73262109375" />
                  <Point X="-3.155072753906" Y="27.73115625" />
                  <Point X="-3.111244873047" Y="27.727322265625" />
                  <Point X="-3.0695234375" Y="27.723671875" />
                  <Point X="-3.059172851562" Y="27.723333984375" />
                  <Point X="-3.038489501953" Y="27.72378515625" />
                  <Point X="-3.028156738281" Y="27.72457421875" />
                  <Point X="-3.006697998047" Y="27.727400390625" />
                  <Point X="-2.996512939453" Y="27.7293125" />
                  <Point X="-2.976423828125" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938450195312" Y="27.750447265625" />
                  <Point X="-2.929421630859" Y="27.755529296875" />
                  <Point X="-2.911167480469" Y="27.767158203125" />
                  <Point X="-2.902748291016" Y="27.77319140625" />
                  <Point X="-2.886615966797" Y="27.786138671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.847793457031" Y="27.824162109375" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752631835938" Y="28.0882265625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.971451660156" Y="28.547607421875" />
                  <Point X="-3.059386474609" Y="28.6999140625" />
                  <Point X="-2.923127929688" Y="28.8043828125" />
                  <Point X="-2.648368408203" Y="29.0150390625" />
                  <Point X="-2.329667236328" Y="29.1921015625" />
                  <Point X="-2.192524658203" Y="29.268294921875" />
                  <Point X="-2.118564941406" Y="29.17191015625" />
                  <Point X="-2.111820068359" Y="29.164048828125" />
                  <Point X="-2.097516845703" Y="29.149107421875" />
                  <Point X="-2.089957763672" Y="29.14202734375" />
                  <Point X="-2.073378417969" Y="29.128115234375" />
                  <Point X="-2.065094970703" Y="29.121900390625" />
                  <Point X="-2.047897094727" Y="29.110408203125" />
                  <Point X="-2.038982666016" Y="29.105130859375" />
                  <Point X="-1.990202636719" Y="29.079736328125" />
                  <Point X="-1.943766845703" Y="29.0555625" />
                  <Point X="-1.934335327148" Y="29.0512890625" />
                  <Point X="-1.915062011719" Y="29.04379296875" />
                  <Point X="-1.905220214844" Y="29.0405703125" />
                  <Point X="-1.884313598633" Y="29.034966796875" />
                  <Point X="-1.874174926758" Y="29.032833984375" />
                  <Point X="-1.853724975586" Y="29.029685546875" />
                  <Point X="-1.83305480957" Y="29.028783203125" />
                  <Point X="-1.812408081055" Y="29.03013671875" />
                  <Point X="-1.802120239258" Y="29.031376953125" />
                  <Point X="-1.780805175781" Y="29.03513671875" />
                  <Point X="-1.770716186523" Y="29.037490234375" />
                  <Point X="-1.750860961914" Y="29.043279296875" />
                  <Point X="-1.741094848633" Y="29.04671484375" />
                  <Point X="-1.690287231445" Y="29.06776171875" />
                  <Point X="-1.641921020508" Y="29.087794921875" />
                  <Point X="-1.632583251953" Y="29.0922734375" />
                  <Point X="-1.614452392578" Y="29.102220703125" />
                  <Point X="-1.605659423828" Y="29.107689453125" />
                  <Point X="-1.587929443359" Y="29.120103515625" />
                  <Point X="-1.579780883789" Y="29.126498046875" />
                  <Point X="-1.564228393555" Y="29.14013671875" />
                  <Point X="-1.550253173828" Y="29.15538671875" />
                  <Point X="-1.538021484375" Y="29.172068359375" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.5215390625" Y="29.19948828125" />
                  <Point X="-1.516856567383" Y="29.2087265625" />
                  <Point X="-1.508525634766" Y="29.22766015625" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.48833996582" Y="29.2898046875" />
                  <Point X="-1.47259777832" Y="29.339732421875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.286860839844" Y="29.61659765625" />
                  <Point X="-0.931164978027" Y="29.7163203125" />
                  <Point X="-0.544815185547" Y="29.7615390625" />
                  <Point X="-0.365222412109" Y="29.78255859375" />
                  <Point X="-0.324667510986" Y="29.631205078125" />
                  <Point X="-0.225666366577" Y="29.2617265625" />
                  <Point X="-0.220435150146" Y="29.247107421875" />
                  <Point X="-0.207661453247" Y="29.218916015625" />
                  <Point X="-0.200119293213" Y="29.20534375" />
                  <Point X="-0.182261199951" Y="29.1786171875" />
                  <Point X="-0.172608886719" Y="29.166455078125" />
                  <Point X="-0.15145123291" Y="29.143865234375" />
                  <Point X="-0.126896499634" Y="29.1250234375" />
                  <Point X="-0.099600570679" Y="29.11043359375" />
                  <Point X="-0.085354026794" Y="29.1042578125" />
                  <Point X="-0.054916057587" Y="29.09392578125" />
                  <Point X="-0.039853721619" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629489899" Y="29.08511328125" />
                  <Point X="0.052165550232" Y="29.090154296875" />
                  <Point X="0.067227882385" Y="29.09392578125" />
                  <Point X="0.097665855408" Y="29.1042578125" />
                  <Point X="0.111912246704" Y="29.11043359375" />
                  <Point X="0.139208175659" Y="29.1250234375" />
                  <Point X="0.163763061523" Y="29.143865234375" />
                  <Point X="0.184920715332" Y="29.166455078125" />
                  <Point X="0.194573181152" Y="29.1786171875" />
                  <Point X="0.212431259155" Y="29.20534375" />
                  <Point X="0.219973724365" Y="29.218916015625" />
                  <Point X="0.232747116089" Y="29.247107421875" />
                  <Point X="0.23797819519" Y="29.2617265625" />
                  <Point X="0.332221313477" Y="29.613447265625" />
                  <Point X="0.378190795898" Y="29.7850078125" />
                  <Point X="0.51729901123" Y="29.770439453125" />
                  <Point X="0.827866333008" Y="29.7379140625" />
                  <Point X="1.147532348633" Y="29.660736328125" />
                  <Point X="1.453607177734" Y="29.58683984375" />
                  <Point X="1.659968261719" Y="29.5119921875" />
                  <Point X="1.858247680664" Y="29.44007421875" />
                  <Point X="2.059451171875" Y="29.345978515625" />
                  <Point X="2.250433837891" Y="29.25666015625" />
                  <Point X="2.444868164062" Y="29.143384765625" />
                  <Point X="2.629429443359" Y="29.035859375" />
                  <Point X="2.81274609375" Y="28.905494140625" />
                  <Point X="2.817778808594" Y="28.901916015625" />
                  <Point X="2.506729980469" Y="28.363162109375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062371826172" Y="27.5931015625" />
                  <Point X="2.053181640625" Y="27.573439453125" />
                  <Point X="2.044182617188" Y="27.54956640625" />
                  <Point X="2.041301635742" Y="27.540599609375" />
                  <Point X="2.030302368164" Y="27.49946875" />
                  <Point X="2.01983190918" Y="27.4603125" />
                  <Point X="2.017912231445" Y="27.45146484375" />
                  <Point X="2.013646484375" Y="27.42021875" />
                  <Point X="2.012755615234" Y="27.38323828125" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.017700073242" Y="27.33401171875" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318359375" Y="27.223888671875" />
                  <Point X="2.055680908203" Y="27.20384375" />
                  <Point X="2.061459960938" Y="27.19412890625" />
                  <Point X="2.083467773438" Y="27.1616953125" />
                  <Point X="2.104417724609" Y="27.1308203125" />
                  <Point X="2.109808105469" Y="27.1236328125" />
                  <Point X="2.130463867188" Y="27.100123046875" />
                  <Point X="2.157596191406" Y="27.075388671875" />
                  <Point X="2.168255615234" Y="27.066982421875" />
                  <Point X="2.200689208984" Y="27.044974609375" />
                  <Point X="2.231564208984" Y="27.0240234375" />
                  <Point X="2.241279052734" Y="27.018244140625" />
                  <Point X="2.261317871094" Y="27.007884765625" />
                  <Point X="2.271641845703" Y="27.0033046875" />
                  <Point X="2.293729492188" Y="26.995037109375" />
                  <Point X="2.304537597656" Y="26.991708984375" />
                  <Point X="2.326471679688" Y="26.98636328125" />
                  <Point X="2.33759765625" Y="26.984345703125" />
                  <Point X="2.373164794922" Y="26.98005859375" />
                  <Point X="2.407022460938" Y="26.975974609375" />
                  <Point X="2.416041015625" Y="26.9753203125" />
                  <Point X="2.447574951172" Y="26.97549609375" />
                  <Point X="2.484317138672" Y="26.979822265625" />
                  <Point X="2.497751953125" Y="26.98239453125" />
                  <Point X="2.538883544922" Y="26.99339453125" />
                  <Point X="2.578038330078" Y="27.003865234375" />
                  <Point X="2.584010253906" Y="27.00567578125" />
                  <Point X="2.604405029297" Y="27.013068359375" />
                  <Point X="2.627651611328" Y="27.023572265625" />
                  <Point X="2.636033935547" Y="27.02787109375" />
                  <Point X="3.363852783203" Y="27.448076171875" />
                  <Point X="3.940405029297" Y="27.78094921875" />
                  <Point X="4.043959960938" Y="27.63703125" />
                  <Point X="4.136884277344" Y="27.48347265625" />
                  <Point X="3.754124023438" Y="27.18976953125" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168135986328" Y="26.7398671875" />
                  <Point X="3.152114990234" Y="26.725212890625" />
                  <Point X="3.134667236328" Y="26.7066015625" />
                  <Point X="3.128576171875" Y="26.699421875" />
                  <Point X="3.098973632812" Y="26.660802734375" />
                  <Point X="3.070793945312" Y="26.624041015625" />
                  <Point X="3.065634033203" Y="26.616599609375" />
                  <Point X="3.049740722656" Y="26.589373046875" />
                  <Point X="3.034763671875" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.01911328125" Y="26.5032421875" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.013751464844" Y="26.30869921875" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682617188" Y="26.228611328125" />
                  <Point X="3.050285888672" Y="26.19537109375" />
                  <Point X="3.056918701172" Y="26.1835234375" />
                  <Point X="3.0815390625" Y="26.1461015625" />
                  <Point X="3.104976318359" Y="26.110478515625" />
                  <Point X="3.111742431641" Y="26.10141796875" />
                  <Point X="3.126298583984" Y="26.084171875" />
                  <Point X="3.134088623047" Y="26.075986328125" />
                  <Point X="3.151331787109" Y="26.0598984375" />
                  <Point X="3.160034667969" Y="26.0526953125" />
                  <Point X="3.178241210938" Y="26.03937109375" />
                  <Point X="3.187744873047" Y="26.03325" />
                  <Point X="3.223423095703" Y="26.013166015625" />
                  <Point X="3.257386962891" Y="25.994046875" />
                  <Point X="3.265484130859" Y="25.989984375" />
                  <Point X="3.294676025391" Y="25.9780859375" />
                  <Point X="3.330274414062" Y="25.968021484375" />
                  <Point X="3.343673583984" Y="25.965255859375" />
                  <Point X="3.391913085938" Y="25.958880859375" />
                  <Point X="3.437834228516" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.191982910156" Y="26.043818359375" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.705663085938" Y="26.10737890625" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.358028320312" Y="25.599818359375" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686027587891" Y="25.41954296875" />
                  <Point X="3.665614501953" Y="25.4121328125" />
                  <Point X="3.642375976562" Y="25.401615234375" />
                  <Point X="3.634005859375" Y="25.397314453125" />
                  <Point X="3.586612060547" Y="25.369919921875" />
                  <Point X="3.541495849609" Y="25.34384375" />
                  <Point X="3.533885253906" Y="25.338947265625" />
                  <Point X="3.508781738281" Y="25.319876953125" />
                  <Point X="3.481995605469" Y="25.294353515625" />
                  <Point X="3.472794677734" Y="25.284224609375" />
                  <Point X="3.444358398438" Y="25.24798828125" />
                  <Point X="3.417288818359" Y="25.21349609375" />
                  <Point X="3.410854003906" Y="25.20420703125" />
                  <Point X="3.399131347656" Y="25.1849296875" />
                  <Point X="3.393843505859" Y="25.17494140625" />
                  <Point X="3.384069091797" Y="25.1534765625" />
                  <Point X="3.380005126953" Y="25.142927734375" />
                  <Point X="3.373158691406" Y="25.12142578125" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.360897460938" Y="25.060978515625" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.00496484375" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.361353759766" Y="24.87408203125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.816013671875" />
                  <Point X="3.380004638672" Y="24.794513671875" />
                  <Point X="3.384068359375" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.76250390625" />
                  <Point X="3.399129150391" Y="24.752513671875" />
                  <Point X="3.410853759766" Y="24.733232421875" />
                  <Point X="3.417290771484" Y="24.72394140625" />
                  <Point X="3.445727050781" Y="24.68770703125" />
                  <Point X="3.472796630859" Y="24.653212890625" />
                  <Point X="3.478716552734" Y="24.6463671875" />
                  <Point X="3.501133544922" Y="24.62419921875" />
                  <Point X="3.530174560547" Y="24.601275390625" />
                  <Point X="3.541494628906" Y="24.593595703125" />
                  <Point X="3.588888671875" Y="24.566201171875" />
                  <Point X="3.634004638672" Y="24.540123046875" />
                  <Point X="3.639487792969" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026855469" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.326018066406" Y="24.346197265625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761614746094" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="4.209196777344" Y="23.989056640625" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354482421875" Y="24.087322265625" />
                  <Point X="3.261465087891" Y="24.06710546875" />
                  <Point X="3.172918212891" Y="24.047859375" />
                  <Point X="3.157874755859" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114674560547" Y="24.024609375" />
                  <Point X="3.086848144531" Y="24.007716796875" />
                  <Point X="3.074122802734" Y="23.99846875" />
                  <Point X="3.050373291016" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.983125976562" Y="23.89915625" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.867099121094" Y="23.61576953125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158935547" Y="23.394517578125" />
                  <Point X="2.896541259766" Y="23.380626953125" />
                  <Point X="2.948018554688" Y="23.30055859375" />
                  <Point X="2.997021484375" Y="23.224337890625" />
                  <Point X="3.001740966797" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.641176269531" Y="22.712240234375" />
                  <Point X="4.087169921875" Y="22.370015625" />
                  <Point X="4.045491943359" Y="22.30257421875" />
                  <Point X="4.0012734375" Y="22.23974609375" />
                  <Point X="3.536394287109" Y="22.50814453125" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841201416016" Y="22.909107421875" />
                  <Point X="2.815038085938" Y="22.920482421875" />
                  <Point X="2.783125" Y="22.930673828125" />
                  <Point X="2.771109375" Y="22.9336640625" />
                  <Point X="2.660403808594" Y="22.953658203125" />
                  <Point X="2.555019042969" Y="22.972689453125" />
                  <Point X="2.539353271484" Y="22.974193359375" />
                  <Point X="2.507996337891" Y="22.974595703125" />
                  <Point X="2.492305175781" Y="22.973494140625" />
                  <Point X="2.460133056641" Y="22.96853515625" />
                  <Point X="2.444841064453" Y="22.964861328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589599609" Y="22.948890625" />
                  <Point X="2.308620361328" Y="22.90048828125" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.208967773438" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.78883984375" />
                  <Point X="2.128047363281" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.072061523438" Y="22.6618359375" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.022181030273" Y="22.309154296875" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.447640136719" Y="21.471544921875" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.72375390625" Y="20.963916015625" />
                  <Point X="2.358876220703" Y="21.43943359375" />
                  <Point X="1.833914672852" Y="22.123576171875" />
                  <Point X="1.828654541016" Y="22.1298515625" />
                  <Point X="1.808837524414" Y="22.1503671875" />
                  <Point X="1.78325402832" Y="22.171994140625" />
                  <Point X="1.773297973633" Y="22.179353515625" />
                  <Point X="1.664112304688" Y="22.249548828125" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.323755859375" />
                  <Point X="1.517466796875" Y="22.336126953125" />
                  <Point X="1.502547851562" Y="22.34111328125" />
                  <Point X="1.470927612305" Y="22.34884375" />
                  <Point X="1.455391479492" Y="22.351302734375" />
                  <Point X="1.424125488281" Y="22.35362109375" />
                  <Point X="1.408395507812" Y="22.35348046875" />
                  <Point X="1.288982177734" Y="22.3424921875" />
                  <Point X="1.17530859375" Y="22.332033203125" />
                  <Point X="1.161231445312" Y="22.329662109375" />
                  <Point X="1.133581665039" Y="22.32283203125" />
                  <Point X="1.120008911133" Y="22.31837109375" />
                  <Point X="1.092622680664" Y="22.30702734375" />
                  <Point X="1.079872558594" Y="22.300583984375" />
                  <Point X="1.055489868164" Y="22.285861328125" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.95165032959" Y="22.200916015625" />
                  <Point X="0.863874389648" Y="22.127931640625" />
                  <Point X="0.852652587891" Y="22.116908203125" />
                  <Point X="0.832181884766" Y="22.093158203125" />
                  <Point X="0.822932800293" Y="22.080431640625" />
                  <Point X="0.806040771484" Y="22.05260546875" />
                  <Point X="0.799019348145" Y="22.038529296875" />
                  <Point X="0.787394836426" Y="22.00941015625" />
                  <Point X="0.782791809082" Y="21.9943671875" />
                  <Point X="0.755222290039" Y="21.867525390625" />
                  <Point X="0.728977539063" Y="21.74678125" />
                  <Point X="0.727584655762" Y="21.7387109375" />
                  <Point X="0.72472454834" Y="21.710322265625" />
                  <Point X="0.724742248535" Y="21.676830078125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.832703735352" Y="20.850603515625" />
                  <Point X="0.833091125488" Y="20.84766015625" />
                  <Point X="0.655064880371" Y="21.512064453125" />
                  <Point X="0.652606079102" Y="21.519876953125" />
                  <Point X="0.642146728516" Y="21.546416015625" />
                  <Point X="0.626788146973" Y="21.57618359375" />
                  <Point X="0.620407348633" Y="21.58679296875" />
                  <Point X="0.536527832031" Y="21.707646484375" />
                  <Point X="0.456679626465" Y="21.822693359375" />
                  <Point X="0.446669281006" Y="21.834830078125" />
                  <Point X="0.424782775879" Y="21.857287109375" />
                  <Point X="0.412906890869" Y="21.867607421875" />
                  <Point X="0.386651916504" Y="21.886849609375" />
                  <Point X="0.373235595703" Y="21.89506640625" />
                  <Point X="0.345236236572" Y="21.909171875" />
                  <Point X="0.330653045654" Y="21.915060546875" />
                  <Point X="0.200854431152" Y="21.95534375" />
                  <Point X="0.077293968201" Y="21.993693359375" />
                  <Point X="0.063380336761" Y="21.996888671875" />
                  <Point X="0.035228187561" Y="22.001158203125" />
                  <Point X="0.020989667892" Y="22.002232421875" />
                  <Point X="-0.008651978493" Y="22.002234375" />
                  <Point X="-0.022896144867" Y="22.001162109375" />
                  <Point X="-0.051062114716" Y="21.996892578125" />
                  <Point X="-0.064983917236" Y="21.9936953125" />
                  <Point X="-0.194782531738" Y="21.95341015625" />
                  <Point X="-0.318342987061" Y="21.9150625" />
                  <Point X="-0.332927215576" Y="21.909171875" />
                  <Point X="-0.360928527832" Y="21.895064453125" />
                  <Point X="-0.374345458984" Y="21.88684765625" />
                  <Point X="-0.400600860596" Y="21.867603515625" />
                  <Point X="-0.412477783203" Y="21.85728125" />
                  <Point X="-0.434361602783" Y="21.83482421875" />
                  <Point X="-0.444368225098" Y="21.822689453125" />
                  <Point X="-0.52824786377" Y="21.701833984375" />
                  <Point X="-0.608095825195" Y="21.5867890625" />
                  <Point X="-0.612469482422" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777832031" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.838112182617" Y="20.78297265625" />
                  <Point X="-0.985425292969" Y="20.23319140625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.15080688597" Y="29.213928060113" />
                  <Point X="-3.03658986825" Y="28.660429422044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.359898322031" Y="29.596120633562" />
                  <Point X="-1.474263683724" Y="29.52465722412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.088524367918" Y="29.140824548485" />
                  <Point X="-2.989060702594" Y="28.578106992665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.034734389896" Y="29.68728366078" />
                  <Point X="-1.462471585953" Y="29.420003796293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.999373178603" Y="29.084510446054" />
                  <Point X="-2.941531636124" Y="28.495784501307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701691102768" Y="28.020784148038" />
                  <Point X="-3.771197605855" Y="27.9773516645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.780836146975" Y="29.733914942966" />
                  <Point X="-1.487549614424" Y="29.292311356575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.894862322576" Y="29.037794128608" />
                  <Point X="-2.894002628028" Y="28.413461973474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608512199642" Y="27.966986840526" />
                  <Point X="-3.940801876898" Y="27.759349205253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.560245983532" Y="29.759733027115" />
                  <Point X="-2.846473619932" Y="28.33113944564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.515332925417" Y="27.913189764903" />
                  <Point X="-4.06790422619" Y="27.567904894305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.362242504205" Y="29.771437384598" />
                  <Point X="-2.798944611836" Y="28.248816917807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422153551724" Y="27.859392751435" />
                  <Point X="-4.17083118837" Y="27.391567041835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.336531316815" Y="29.67548156928" />
                  <Point X="-2.761697131929" Y="28.160069778117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.32897417803" Y="27.805595737966" />
                  <Point X="-4.191543150756" Y="27.266602823002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.310820124209" Y="29.579525757221" />
                  <Point X="-2.749768757998" Y="28.055501505085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234007235242" Y="27.75291572164" />
                  <Point X="-4.111078511335" Y="27.20486076177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.285108927134" Y="29.483569947954" />
                  <Point X="-2.765860779702" Y="27.933424145593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.097602888858" Y="27.726128668843" />
                  <Point X="-4.030613871914" Y="27.143118700537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.25939773006" Y="29.387614138688" />
                  <Point X="-3.950149299954" Y="27.08137659715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.530338067551" Y="29.769073886401" />
                  <Point X="0.342465166494" Y="29.651677868476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.233686532985" Y="29.291658329421" />
                  <Point X="-3.869685249126" Y="27.019634168124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.683877278754" Y="29.752993885479" />
                  <Point X="0.306412633551" Y="29.517127797262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.197888663754" Y="29.202005372448" />
                  <Point X="-3.789221198297" Y="26.957891739097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.835909463806" Y="29.735972190102" />
                  <Point X="0.270360114129" Y="29.382577734497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.133591486252" Y="29.130160759764" />
                  <Point X="-3.708757147469" Y="26.896149310071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.965220004456" Y="29.704752435513" />
                  <Point X="0.232712451464" Y="29.247030915607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.022837996823" Y="29.087345272606" />
                  <Point X="-3.628293096641" Y="26.834406881045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.485531733477" Y="26.298744729613" />
                  <Point X="-4.670123997629" Y="26.183398681145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.094530545105" Y="29.673532680925" />
                  <Point X="-3.547829045812" Y="26.772664452018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337456709149" Y="26.279250325779" />
                  <Point X="-4.706667390884" Y="26.048541886366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.223841056163" Y="29.642312907845" />
                  <Point X="-3.483738073601" Y="26.700690987967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.189381648781" Y="26.259755944466" />
                  <Point X="-4.737269334159" Y="25.917397721585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.353151546666" Y="29.611093121922" />
                  <Point X="-3.440379739986" Y="26.615762333474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.041306584501" Y="26.240261565597" />
                  <Point X="-4.755534651834" Y="25.793962336048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.478918782213" Y="29.57765926457" />
                  <Point X="-3.421465334156" Y="26.515559417667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.893231520221" Y="26.220767186728" />
                  <Point X="-4.773799985494" Y="25.670526940521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.592350490238" Y="29.53651731413" />
                  <Point X="-3.46014894524" Y="26.379365266361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73983007953" Y="26.204601097236" />
                  <Point X="-4.739973683998" Y="25.579642011295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.705781835235" Y="29.495375136845" />
                  <Point X="-4.614503503495" Y="25.546022533351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.819212644432" Y="29.454232624755" />
                  <Point X="-4.489033322991" Y="25.512403055407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.925496629898" Y="29.408624281552" />
                  <Point X="-4.363563142487" Y="25.478783577463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.028030717861" Y="29.360672742327" />
                  <Point X="-4.238092961984" Y="25.445164099519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.130563929253" Y="29.312720655359" />
                  <Point X="-4.112622983859" Y="25.411544495114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.233096753346" Y="29.26476832638" />
                  <Point X="-3.987153016234" Y="25.377924884149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.327521655611" Y="29.211749605543" />
                  <Point X="-3.861683048609" Y="25.344305273183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420296619429" Y="29.157699888738" />
                  <Point X="-3.736213080984" Y="25.310685662218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.513070991618" Y="29.103649802243" />
                  <Point X="-3.610743113359" Y="25.277066051252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.605845150662" Y="29.049599582559" />
                  <Point X="-3.487642185809" Y="25.241966099751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69196205258" Y="28.991389446929" />
                  <Point X="-3.396400460532" Y="25.186958309172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.710849086346" Y="24.365599648242" />
                  <Point X="-4.782909036556" Y="24.320571593855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.775809739236" Y="28.93176134823" />
                  <Point X="-3.333602645612" Y="25.114176790763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.396989565876" Y="24.449698895069" />
                  <Point X="-4.768201815017" Y="24.217739737527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.767244019276" Y="28.81438694403" />
                  <Point X="-3.300860160515" Y="25.022614617886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.083131377661" Y="24.53379730941" />
                  <Point X="-4.753494500222" Y="24.114907939472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66606688221" Y="28.639142503644" />
                  <Point X="-3.301972470447" Y="24.90989762118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769274113657" Y="24.617895146242" />
                  <Point X="-4.73744828432" Y="24.012912779684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.564889745144" Y="28.463898063259" />
                  <Point X="-4.712773092263" Y="23.916309602633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463712432855" Y="28.288653513381" />
                  <Point X="-4.688097900206" Y="23.819706425583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.362534883662" Y="28.11340881547" />
                  <Point X="-4.663422374304" Y="23.723103457142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.261357334469" Y="27.938164117559" />
                  <Point X="-4.456829184253" Y="23.740175261598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.160179785277" Y="27.762919419647" />
                  <Point X="-4.229705021836" Y="23.770076241452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.060178976707" Y="27.588410030886" />
                  <Point X="-4.002580736099" Y="23.799977298363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017690149042" Y="27.449838116361" />
                  <Point X="-3.775455786496" Y="23.829878770105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017272759651" Y="27.337555354203" />
                  <Point X="-3.548330836893" Y="23.859780241846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038922569472" Y="27.239061708516" />
                  <Point X="-3.32120588729" Y="23.889681713588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08671782357" Y="27.156905549649" />
                  <Point X="-3.142842982886" Y="23.889113277748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148624669826" Y="27.083567292228" />
                  <Point X="-3.044976362866" Y="23.838245180854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232098507547" Y="27.023705586787" />
                  <Point X="-2.980796571289" Y="23.766327217303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.346638217635" Y="26.983255992878" />
                  <Point X="-2.950414701669" Y="23.673289968163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544569003621" Y="26.99491492652" />
                  <Point X="-2.962897566207" Y="23.55346786037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953231659708" Y="27.763123099169" />
                  <Point X="-3.314333455162" Y="23.221844395881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008835515838" Y="27.685846296393" />
                  <Point X="-4.100690396701" Y="22.618452095133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.062076691242" Y="27.607093126843" />
                  <Point X="-4.137582966836" Y="22.483377110423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111265609023" Y="27.525807825699" />
                  <Point X="-4.088914105159" Y="22.401766842158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760087211868" Y="27.194345259945" />
                  <Point X="-4.040245243483" Y="22.320156573893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.111347802349" Y="26.676945937241" />
                  <Point X="-3.989246753689" Y="22.240002018839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.020484812533" Y="26.508146491363" />
                  <Point X="-3.931463698216" Y="22.164086930944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001350205785" Y="26.384167913726" />
                  <Point X="-3.873680642743" Y="22.08817184305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019016009491" Y="26.283184784719" />
                  <Point X="-3.815897706983" Y="22.01225668035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052222220889" Y="26.191912380196" />
                  <Point X="-2.626799778041" Y="22.643265584245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.10386836755" Y="26.112162526068" />
                  <Point X="-2.469137390261" Y="22.629762029999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172902697318" Y="26.043278014651" />
                  <Point X="-2.384603516048" Y="22.570562708873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.266333563815" Y="25.989638151328" />
                  <Point X="-2.331852461774" Y="22.49150327765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.395603706753" Y="25.958393153448" />
                  <Point X="-2.311259515868" Y="22.392349230093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583355080856" Y="25.963691284584" />
                  <Point X="-2.354533505091" Y="22.253286692173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.810479997096" Y="25.993592735478" />
                  <Point X="-2.455710935123" Y="22.078042068722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.037604913336" Y="26.023494186372" />
                  <Point X="-2.556888365156" Y="21.90279744527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.264730187414" Y="26.053395860867" />
                  <Point X="-2.658065795188" Y="21.727552821818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.491856220868" Y="26.083298009874" />
                  <Point X="-2.759243241636" Y="21.552308188109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706191662858" Y="26.105207710282" />
                  <Point X="-2.860420717332" Y="21.377063536124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729862465461" Y="26.007976921045" />
                  <Point X="3.865448994745" Y="25.467831435817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392696714157" Y="25.172423024632" />
                  <Point X="-2.961598193028" Y="21.201818884139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753253979038" Y="25.910571612654" />
                  <Point X="4.179307390446" Y="25.55192997981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356449165103" Y="25.037751093827" />
                  <Point X="-2.915843283289" Y="21.118387776614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769148627477" Y="25.808481743004" />
                  <Point X="4.493166475128" Y="25.636028954327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351995418882" Y="24.922946135993" />
                  <Point X="-2.835531763859" Y="21.056550035393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371375751257" Y="24.823034363404" />
                  <Point X="-2.755220244428" Y="20.994712294171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.409774790912" Y="24.735006798108" />
                  <Point X="-2.666095959744" Y="20.938381379861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.467977425687" Y="24.65935389246" />
                  <Point X="-2.080099099858" Y="21.192530909599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.124122639691" Y="21.165021948796" />
                  <Point X="-2.576048988178" Y="20.882627024305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.541764905506" Y="24.593439478834" />
                  <Point X="-1.859398308554" Y="21.218418121708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.634924904775" Y="24.539630358882" />
                  <Point X="-1.697144105426" Y="21.207783852142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751148573965" Y="24.500233019406" />
                  <Point X="-1.558586186991" Y="21.182342500517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.876618595466" Y="24.466613442106" />
                  <Point X="3.224368493518" Y="24.059042343619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.998945768935" Y="23.918182591803" />
                  <Point X="-0.09656240847" Y="21.983894403158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.513156991872" Y="21.723577215819" />
                  <Point X="-1.471447738359" Y="21.12477069812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002088616966" Y="24.432993864805" />
                  <Point X="3.451315690174" Y="24.088832742991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881292040987" Y="23.732642434751" />
                  <Point X="0.091390216561" Y="21.989318289831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.633605501044" Y="21.536290685635" />
                  <Point X="-1.396858317906" Y="21.059357392619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.127558638467" Y="24.399374287505" />
                  <Point X="3.599390873542" Y="24.069338438537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866706851168" Y="23.611506648322" />
                  <Point X="0.211169187256" Y="21.952142549302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.672848455243" Y="21.399747017958" />
                  <Point X="-1.322268844436" Y="20.993944120246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.253028659967" Y="24.365754710205" />
                  <Point X="3.74746605691" Y="24.049844134082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860835850547" Y="23.495816091649" />
                  <Point X="0.330923438772" Y="21.914951362516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.708900926441" Y="21.265196985326" />
                  <Point X="-1.249062869392" Y="20.927666342108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.378498886371" Y="24.332135260942" />
                  <Point X="3.895541240277" Y="24.030349829628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.886797474007" Y="23.400016766156" />
                  <Point X="1.178073641225" Y="22.332287612173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.902702474505" Y="22.160216609691" />
                  <Point X="0.421909641614" Y="21.859783903799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.744953397638" Y="21.130646952695" />
                  <Point X="-1.197933989118" Y="20.847593264069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.503969397751" Y="24.298515989752" />
                  <Point X="4.043616423645" Y="24.010855525173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.9362350246" Y="23.318886828036" />
                  <Point X="1.388303059304" Y="22.351631584081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.777484615193" Y="21.969949858775" />
                  <Point X="0.482436021676" Y="21.785583035363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.781005868836" Y="20.996096920063" />
                  <Point X="-1.172850826516" Y="20.751245015309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.629439909131" Y="24.264896718563" />
                  <Point X="4.191691607013" Y="23.991361220719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.987614257716" Y="23.238970187815" />
                  <Point X="2.560028315707" Y="22.971784837346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.076008643713" Y="22.669335778596" />
                  <Point X="1.532460654385" Y="22.329689298773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749309426506" Y="21.840322098561" />
                  <Point X="0.536665842019" Y="21.707447639735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.817058340034" Y="20.861546887432" />
                  <Point X="-1.153031817192" Y="20.651607358501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.754910420511" Y="24.231277447373" />
                  <Point X="4.339766683386" Y="23.971866849407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050160183666" Y="23.166031271709" />
                  <Point X="2.699106081696" Y="22.946668322525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006173660753" Y="22.513676089734" />
                  <Point X="1.624370580487" Y="22.275099046411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725005763818" Y="21.713113536289" />
                  <Point X="0.590896209347" Y="21.629312585902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.853110821342" Y="20.726996848482" />
                  <Point X="-1.133212700684" Y="20.55196976867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770684818314" Y="24.129112436786" />
                  <Point X="4.487841745416" Y="23.952372469132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.130417508435" Y="23.104159665903" />
                  <Point X="2.827677958432" Y="22.914986999496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.005546480499" Y="22.401262235696" />
                  <Point X="1.712731884503" Y="22.218291368866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733224242021" Y="21.606227063118" />
                  <Point X="0.641015455212" Y="21.548608618265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889163316843" Y="20.592446800665" />
                  <Point X="-1.119696211772" Y="20.448393860017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74630315283" Y="24.001855132957" />
                  <Point X="4.635916807445" Y="23.932878088856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.210881645748" Y="23.042417290919" />
                  <Point X="2.922799157233" Y="22.862403373024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.023725897544" Y="22.300600047923" />
                  <Point X="1.797984908844" Y="22.159541422615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746851153094" Y="21.502720153889" />
                  <Point X="0.67037354184" Y="21.454931638509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.925215812344" Y="20.457896752847" />
                  <Point X="-1.133466152202" Y="20.327767497945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.291345783061" Y="22.980674915934" />
                  <Point X="3.015978488176" Y="22.808606332842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.042055899709" Y="22.200031956177" />
                  <Point X="1.861716897477" Y="22.087343640729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.760478064167" Y="21.39921324466" />
                  <Point X="0.69608476806" Y="21.358975847455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.961268307844" Y="20.32334670503" />
                  <Point X="-1.075738307435" Y="20.251817910572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.371809920375" Y="22.91893254095" />
                  <Point X="3.109157819119" Y="22.754809292661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078554933533" Y="22.110817135469" />
                  <Point X="1.919816749469" Y="22.011626509269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77410497524" Y="21.295706335432" />
                  <Point X="0.72179599428" Y="21.2630200564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452274057688" Y="22.857190165965" />
                  <Point X="3.202337150063" Y="22.701012252479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.126084002207" Y="22.028494645488" />
                  <Point X="1.97791660146" Y="21.935909377809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.787731886313" Y="21.192199426203" />
                  <Point X="0.7475072205" Y="21.167064265346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.532738195001" Y="22.795447790981" />
                  <Point X="3.295516481006" Y="22.647215212298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.173613070881" Y="21.946172155508" />
                  <Point X="2.036016453451" Y="21.86019224635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.801358797386" Y="21.088692516974" />
                  <Point X="0.773218446721" Y="21.071108474292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613202332314" Y="22.733705415996" />
                  <Point X="3.388695811949" Y="22.593418172116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221142139555" Y="21.863849665528" />
                  <Point X="2.094116305442" Y="21.78447511489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814985708458" Y="20.985185607746" />
                  <Point X="0.798929672941" Y="20.975152683237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.693666363897" Y="22.671962974944" />
                  <Point X="3.481875142892" Y="22.539621131934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268671208228" Y="21.781527175547" />
                  <Point X="2.152216157433" Y="21.70875798343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.828612619531" Y="20.881678698517" />
                  <Point X="0.824640899161" Y="20.879196892183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774130339131" Y="22.610220498681" />
                  <Point X="3.575054425953" Y="22.485824061833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.316200276902" Y="21.699204685567" />
                  <Point X="2.210316009425" Y="21.633040851971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854594314365" Y="22.548478022418" />
                  <Point X="3.668233641489" Y="22.432026949537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.363729345576" Y="21.616882195587" />
                  <Point X="2.268415861416" Y="21.557323720511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.935058289599" Y="22.486735546155" />
                  <Point X="3.761412857026" Y="22.378229837241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.41125841425" Y="21.534559705606" />
                  <Point X="2.326515713407" Y="21.481606589052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.015522264833" Y="22.424993069892" />
                  <Point X="3.854592072562" Y="22.324432724946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.458787456437" Y="21.452237199075" />
                  <Point X="2.384615559589" Y="21.405889453962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074812867013" Y="22.350020001732" />
                  <Point X="3.947771288099" Y="22.27063561265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506316412178" Y="21.369914638527" />
                  <Point X="2.442715398467" Y="21.330172314309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.55384536792" Y="21.287592077979" />
                  <Point X="2.500815237346" Y="21.254455174655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601374323662" Y="21.205269517431" />
                  <Point X="2.558915076224" Y="21.178738035002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.648903279403" Y="21.122946956882" />
                  <Point X="2.617014915103" Y="21.103020895348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696432235145" Y="21.040624396334" />
                  <Point X="2.675114753981" Y="21.027303755695" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.676761230469" Y="20.69698828125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.380439300537" Y="21.599310546875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274336212158" Y="21.733599609375" />
                  <Point X="0.144537506104" Y="21.7738828125" />
                  <Point X="0.020977081299" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.138463165283" Y="21.77194921875" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.372158508301" Y="21.593501953125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.654586181641" Y="20.733796875" />
                  <Point X="-0.84774407959" Y="20.012921875" />
                  <Point X="-0.955541137695" Y="20.033845703125" />
                  <Point X="-1.100231323242" Y="20.061931640625" />
                  <Point X="-1.250276123047" Y="20.100537109375" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.333854248047" Y="20.26131640625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.340692138672" Y="20.6211328125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.505784912109" Y="20.902171875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240356445" Y="21.014236328125" />
                  <Point X="-1.807846435547" Y="21.024630859375" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878662109" Y="21.026208984375" />
                  <Point X="-2.122037597656" Y="20.937904296875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.369413085938" Y="20.699767578125" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.643728027344" Y="20.70105859375" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-3.063581542969" Y="20.99234765625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.887416015625" Y="21.710306640625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979003906" Y="22.431236328125" />
                  <Point X="-2.531327636719" Y="22.448583984375" />
                  <Point X="-2.560156982422" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.220509277344" Y="22.0934765625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.994128417969" Y="21.9327109375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.310642089844" Y="22.4026171875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.83108203125" Y="23.0648203125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326171875" Y="23.665404296875" />
                  <Point X="-3.161158447266" Y="23.689359375" />
                  <Point X="-3.187641113281" Y="23.7049453125" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.013362060547" Y="23.60691796875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.861853027344" Y="23.73222265625" />
                  <Point X="-4.927393066406" Y="23.98880859375" />
                  <Point X="-4.966798339844" Y="24.264328125" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.317613769531" Y="24.667671875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.527680664063" Y="24.88844140625" />
                  <Point X="-3.502325683594" Y="24.9093515625" />
                  <Point X="-3.490159667969" Y="24.939361328125" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.490387207031" Y="24.998810546875" />
                  <Point X="-3.502323730469" Y="25.028083984375" />
                  <Point X="-3.528359619141" Y="25.04946875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.281080078125" Y="25.259978515625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.959884277344" Y="25.71096875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.838322753906" Y="26.289140625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.29816015625" Y="26.465716796875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731704833984" Y="26.3958671875" />
                  <Point X="-3.700235595703" Y="26.4057890625" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.626492675781" Y="26.47426953125" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.631552001953" Y="26.574779296875" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.075036621094" Y="26.93771484375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.324141113281" Y="27.505818359375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.949893066406" Y="28.0570859375" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.493815673828" Y="28.120158203125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515136719" Y="27.92043359375" />
                  <Point X="-3.094687255859" Y="27.916599609375" />
                  <Point X="-3.052965820312" Y="27.91294921875" />
                  <Point X="-3.031507080078" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.982143554688" Y="27.958513671875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941908691406" Y="28.07166796875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.135996582031" Y="28.452607421875" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.038732177734" Y="28.95516796875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.421941650391" Y="29.35819140625" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.070275878906" Y="29.421087890625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.902467041016" Y="29.248267578125" />
                  <Point X="-1.85603125" Y="29.22409375" />
                  <Point X="-1.835124755859" Y="29.218490234375" />
                  <Point X="-1.813809448242" Y="29.22225" />
                  <Point X="-1.763001708984" Y="29.243296875" />
                  <Point X="-1.714635498047" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.669546386719" Y="29.3469375" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.672828369141" Y="29.5772578125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.338152587891" Y="29.79954296875" />
                  <Point X="-0.968083068848" Y="29.903296875" />
                  <Point X="-0.566901611328" Y="29.95025" />
                  <Point X="-0.224199981689" Y="29.990359375" />
                  <Point X="-0.14114163208" Y="29.680380859375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282121658" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594039917" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.148695449829" Y="29.662623046875" />
                  <Point X="0.236648422241" Y="29.990869140625" />
                  <Point X="0.537089599609" Y="29.959404296875" />
                  <Point X="0.860210266113" Y="29.925564453125" />
                  <Point X="1.192123413086" Y="29.8454296875" />
                  <Point X="1.508455566406" Y="29.769056640625" />
                  <Point X="1.724752075195" Y="29.69060546875" />
                  <Point X="1.931044311523" Y="29.61578125" />
                  <Point X="2.139940429688" Y="29.518087890625" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.540512939453" Y="29.307556640625" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.922859375" Y="29.060333984375" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.671274902344" Y="28.268162109375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851806641" Y="27.491513671875" />
                  <Point X="2.213852539062" Y="27.4503828125" />
                  <Point X="2.203382080078" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.206333496094" Y="27.3567578125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.240689941406" Y="27.26837890625" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.307372314453" Y="27.202197265625" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360334960938" Y="27.172978515625" />
                  <Point X="2.395902099609" Y="27.16869140625" />
                  <Point X="2.429759765625" Y="27.164607421875" />
                  <Point X="2.448664550781" Y="27.1659453125" />
                  <Point X="2.489796142578" Y="27.1769453125" />
                  <Point X="2.528950927734" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.268852783203" Y="27.612623046875" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.088697753906" Y="27.900166015625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.308701171875" Y="27.566533203125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.869788818359" Y="27.03903125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.249769042969" Y="26.545212890625" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.202092773438" Y="26.4520703125" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.199831542969" Y="26.34709375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.240267089844" Y="26.25053125" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.316625732422" Y="26.178736328125" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.368566162109" Y="26.153619140625" />
                  <Point X="3.416805664062" Y="26.147244140625" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.167183105469" Y="26.232193359375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.890271484375" Y="26.1523203125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.972626464844" Y="25.7366171875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.407204101562" Y="25.416291015625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088378906" Y="25.232818359375" />
                  <Point X="3.681694580078" Y="25.205423828125" />
                  <Point X="3.636578369141" Y="25.17934765625" />
                  <Point X="3.622265380859" Y="25.166927734375" />
                  <Point X="3.593829101562" Y="25.13069140625" />
                  <Point X="3.566759521484" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.547506347656" Y="25.025240234375" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.547962158203" Y="24.9098203125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758544922" Y="24.8412421875" />
                  <Point X="3.595194824219" Y="24.8050078125" />
                  <Point X="3.622264404297" Y="24.770513671875" />
                  <Point X="3.636576660156" Y="24.758091796875" />
                  <Point X="3.683970703125" Y="24.730697265625" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.375193847656" Y="24.529724609375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.975744628906" Y="24.214759765625" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.905591796875" Y="23.84587109375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.184396972656" Y="23.800681640625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.301818603516" Y="23.88144140625" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.129222167969" Y="23.77768359375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.056299804688" Y="23.598359375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.107837890625" Y="23.40330859375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.756840820312" Y="22.862978515625" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.281124023438" Y="22.322443359375" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.115536132812" Y="22.0719765625" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.441394287109" Y="22.3436015625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.7466875" />
                  <Point X="2.626634765625" Y="22.766681640625" />
                  <Point X="2.52125" Y="22.785712890625" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.397108642578" Y="22.7323515625" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.240197265625" Y="22.57334765625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.20915625" Y="22.342919921875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.612185058594" Y="21.566544921875" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.926657714844" Y="20.87504296875" />
                  <Point X="2.835297851563" Y="20.809787109375" />
                  <Point X="2.736243164062" Y="20.745671875" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.208138916016" Y="21.32376953125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.561363647461" Y="22.089728515625" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425805541992" Y="22.16428125" />
                  <Point X="1.306392456055" Y="22.15329296875" />
                  <Point X="1.19271862793" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.073124755859" Y="22.054822265625" />
                  <Point X="0.985348754883" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.940887145996" Y="21.827169921875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.021078308105" Y="20.875404296875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.080778076172" Y="20.055697265625" />
                  <Point X="0.99436730957" Y="20.0367578125" />
                  <Point X="0.902838500977" Y="20.02012890625" />
                  <Point X="0.860200500488" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#168" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.099001320624" Y="4.724602451013" Z="1.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="-0.578869750749" Y="5.03119123621" Z="1.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.3" />
                  <Point X="-1.357806397926" Y="4.878968586704" Z="1.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.3" />
                  <Point X="-1.728556571075" Y="4.60201308957" Z="1.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.3" />
                  <Point X="-1.723388128706" Y="4.393252690368" Z="1.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.3" />
                  <Point X="-1.788291164805" Y="4.320770070718" Z="1.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.3" />
                  <Point X="-1.885534918028" Y="4.323897733978" Z="1.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.3" />
                  <Point X="-2.036764365853" Y="4.48280573272" Z="1.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.3" />
                  <Point X="-2.452380594318" Y="4.43317901197" Z="1.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.3" />
                  <Point X="-3.075310283697" Y="4.026128437953" Z="1.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.3" />
                  <Point X="-3.185453935701" Y="3.458887695732" Z="1.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.3" />
                  <Point X="-2.997874493648" Y="3.098591433586" Z="1.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.3" />
                  <Point X="-3.023654214502" Y="3.025149353384" Z="1.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.3" />
                  <Point X="-3.096485032021" Y="2.997690205525" Z="1.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.3" />
                  <Point X="-3.474971652565" Y="3.194740013045" Z="1.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.3" />
                  <Point X="-3.995512592293" Y="3.119070216922" Z="1.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.3" />
                  <Point X="-4.373479316101" Y="2.562303013891" Z="1.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.3" />
                  <Point X="-4.111630402498" Y="1.929327050382" Z="1.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.3" />
                  <Point X="-3.682058490139" Y="1.582972577373" Z="1.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.3" />
                  <Point X="-3.678842734788" Y="1.52468480465" Z="1.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.3" />
                  <Point X="-3.721426769708" Y="1.484754757443" Z="1.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.3" />
                  <Point X="-4.297789993677" Y="1.546569202375" Z="1.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.3" />
                  <Point X="-4.892738690327" Y="1.333498870703" Z="1.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.3" />
                  <Point X="-5.015501743284" Y="0.749568191571" Z="1.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.3" />
                  <Point X="-4.300177200798" Y="0.242961482507" Z="1.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.3" />
                  <Point X="-3.56302596409" Y="0.039675127649" Z="1.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.3" />
                  <Point X="-3.544296191367" Y="0.015270429838" Z="1.3" />
                  <Point X="-3.539556741714" Y="0" Z="1.3" />
                  <Point X="-3.544068409768" Y="-0.014536521223" Z="1.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.3" />
                  <Point X="-3.562342631033" Y="-0.039200855485" Z="1.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.3" />
                  <Point X="-4.336710587974" Y="-0.252750574249" Z="1.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.3" />
                  <Point X="-5.022450528877" Y="-0.711471686779" Z="1.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.3" />
                  <Point X="-4.916453850021" Y="-1.248872171566" Z="1.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.3" />
                  <Point X="-4.012991893278" Y="-1.411373385736" Z="1.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.3" />
                  <Point X="-3.206243624714" Y="-1.31446466982" Z="1.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.3" />
                  <Point X="-3.196433703667" Y="-1.33695726604" Z="1.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.3" />
                  <Point X="-3.867676063311" Y="-1.864230811479" Z="1.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.3" />
                  <Point X="-4.359741589539" Y="-2.591711530379" Z="1.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.3" />
                  <Point X="-4.040070236586" Y="-3.066292256805" Z="1.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.3" />
                  <Point X="-3.201665355042" Y="-2.918543743106" Z="1.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.3" />
                  <Point X="-2.564378812836" Y="-2.563951656295" Z="1.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.3" />
                  <Point X="-2.936873270062" Y="-3.23341302745" Z="1.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.3" />
                  <Point X="-3.10024153536" Y="-4.015989303634" Z="1.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.3" />
                  <Point X="-2.676205579876" Y="-4.310172776294" Z="1.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.3" />
                  <Point X="-2.335901320073" Y="-4.299388641202" Z="1.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.3" />
                  <Point X="-2.100414967645" Y="-4.072390298874" Z="1.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.3" />
                  <Point X="-1.817272784609" Y="-3.993980341062" Z="1.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.3" />
                  <Point X="-1.54490809602" Y="-4.104139819448" Z="1.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.3" />
                  <Point X="-1.39588741247" Y="-4.357340223605" Z="1.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.3" />
                  <Point X="-1.389582437169" Y="-4.700876914613" Z="1.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.3" />
                  <Point X="-1.26889078205" Y="-4.916606784025" Z="1.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.3" />
                  <Point X="-0.971224990995" Y="-4.983957013492" Z="1.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="-0.612445861271" Y="-4.247863147788" Z="1.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="-0.337238586566" Y="-3.40372705512" Z="1.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="-0.129798651239" Y="-3.244524079766" Z="1.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.3" />
                  <Point X="0.123560428122" Y="-3.242587965522" Z="1.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.3" />
                  <Point X="0.333207272867" Y="-3.397918700648" Z="1.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.3" />
                  <Point X="0.62230899913" Y="-4.284672882028" Z="1.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.3" />
                  <Point X="0.905618922463" Y="-4.997785492762" Z="1.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.3" />
                  <Point X="1.085327364905" Y="-4.961861495701" Z="1.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.3" />
                  <Point X="1.064494557099" Y="-4.086789333399" Z="1.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.3" />
                  <Point X="0.983590391186" Y="-3.152167913664" Z="1.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.3" />
                  <Point X="1.098936098204" Y="-2.952343079598" Z="1.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.3" />
                  <Point X="1.304817566023" Y="-2.86521507199" Z="1.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.3" />
                  <Point X="1.528168407372" Y="-2.921049088223" Z="1.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.3" />
                  <Point X="2.162315308824" Y="-3.675388161711" Z="1.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.3" />
                  <Point X="2.757256680205" Y="-4.265023015216" Z="1.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.3" />
                  <Point X="2.949563363639" Y="-4.134363559744" Z="1.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.3" />
                  <Point X="2.649330306633" Y="-3.377175344392" Z="1.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.3" />
                  <Point X="2.252204452388" Y="-2.616913697938" Z="1.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.3" />
                  <Point X="2.278287520022" Y="-2.418659240712" Z="1.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.3" />
                  <Point X="2.41423913766" Y="-2.280613789945" Z="1.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.3" />
                  <Point X="2.611593105575" Y="-2.251243557102" Z="1.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.3" />
                  <Point X="3.410238252715" Y="-2.668419193642" Z="1.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.3" />
                  <Point X="4.150269017781" Y="-2.9255204382" Z="1.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.3" />
                  <Point X="4.317501958032" Y="-2.672560430404" Z="1.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.3" />
                  <Point X="3.781122847854" Y="-2.066072960546" Z="1.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.3" />
                  <Point X="3.143739681861" Y="-1.538371861065" Z="1.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.3" />
                  <Point X="3.099933342133" Y="-1.374941692535" Z="1.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.3" />
                  <Point X="3.16151243026" Y="-1.223003116205" Z="1.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.3" />
                  <Point X="3.306282450309" Y="-1.136138143262" Z="1.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.3" />
                  <Point X="4.171714726896" Y="-1.217610763186" Z="1.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.3" />
                  <Point X="4.948183124032" Y="-1.133973254541" Z="1.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.3" />
                  <Point X="5.019030235355" Y="-0.761411894577" Z="1.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.3" />
                  <Point X="4.381978799522" Y="-0.390697410515" Z="1.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.3" />
                  <Point X="3.702836600498" Y="-0.194732722875" Z="1.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.3" />
                  <Point X="3.628373055193" Y="-0.132845099923" Z="1.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.3" />
                  <Point X="3.590913503876" Y="-0.049494640899" Z="1.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.3" />
                  <Point X="3.590457946548" Y="0.047115890311" Z="1.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.3" />
                  <Point X="3.627006383207" Y="0.131103638652" Z="1.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.3" />
                  <Point X="3.700558813855" Y="0.193416121367" Z="1.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.3" />
                  <Point X="4.413989017784" Y="0.399274522254" Z="1.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.3" />
                  <Point X="5.015875325539" Y="0.775589934071" Z="1.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.3" />
                  <Point X="4.932696619912" Y="1.195427738366" Z="1.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.3" />
                  <Point X="4.154500533195" Y="1.313045898952" Z="1.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.3" />
                  <Point X="3.417200233077" Y="1.228093126633" Z="1.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.3" />
                  <Point X="3.334996951162" Y="1.253587182535" Z="1.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.3" />
                  <Point X="3.275881412019" Y="1.309294453606" Z="1.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.3" />
                  <Point X="3.242644037776" Y="1.388478576394" Z="1.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.3" />
                  <Point X="3.244089027575" Y="1.469883941461" Z="1.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.3" />
                  <Point X="3.283295714306" Y="1.54607637893" Z="1.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.3" />
                  <Point X="3.89407082143" Y="2.030644553973" Z="1.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.3" />
                  <Point X="4.345322506873" Y="2.623699843" Z="1.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.3" />
                  <Point X="4.123127021654" Y="2.960650458805" Z="1.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.3" />
                  <Point X="3.237696764165" Y="2.687205149396" Z="1.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.3" />
                  <Point X="2.470723194656" Y="2.256528284161" Z="1.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.3" />
                  <Point X="2.395733913635" Y="2.249611791389" Z="1.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.3" />
                  <Point X="2.329291825774" Y="2.274850448444" Z="1.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.3" />
                  <Point X="2.275908151791" Y="2.327733034608" Z="1.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.3" />
                  <Point X="2.24981791137" Y="2.394024530197" Z="1.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.3" />
                  <Point X="2.255999658497" Y="2.468746381226" Z="1.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.3" />
                  <Point X="2.708420203463" Y="3.27444231779" Z="1.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.3" />
                  <Point X="2.945680433728" Y="4.13236235749" Z="1.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.3" />
                  <Point X="2.559526809042" Y="4.382039972677" Z="1.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.3" />
                  <Point X="2.154965818492" Y="4.594659397847" Z="1.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.3" />
                  <Point X="1.735644835032" Y="4.76888973426" Z="1.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.3" />
                  <Point X="1.197701738729" Y="4.925314100069" Z="1.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.3" />
                  <Point X="0.536141559079" Y="5.040412558745" Z="1.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.3" />
                  <Point X="0.094243240929" Y="4.706845054938" Z="1.3" />
                  <Point X="0" Y="4.355124473572" Z="1.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>