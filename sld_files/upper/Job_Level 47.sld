<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#209" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3352" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.926233398438" Y="20.132998046875" />
                  <Point X="0.563301940918" Y="21.4874765625" />
                  <Point X="0.557721679688" Y="21.502857421875" />
                  <Point X="0.542363220215" Y="21.532625" />
                  <Point X="0.394023925781" Y="21.746353515625" />
                  <Point X="0.378635406494" Y="21.768525390625" />
                  <Point X="0.356752380371" Y="21.790978515625" />
                  <Point X="0.330497192383" Y="21.81022265625" />
                  <Point X="0.30249520874" Y="21.82433203125" />
                  <Point X="0.072948799133" Y="21.89557421875" />
                  <Point X="0.049136070251" Y="21.90296484375" />
                  <Point X="0.020976823807" Y="21.907234375" />
                  <Point X="-0.00866480732" Y="21.907234375" />
                  <Point X="-0.036824054718" Y="21.90296484375" />
                  <Point X="-0.266370452881" Y="21.83172265625" />
                  <Point X="-0.290183197021" Y="21.82433203125" />
                  <Point X="-0.318185028076" Y="21.81022265625" />
                  <Point X="-0.344440216064" Y="21.790978515625" />
                  <Point X="-0.366323547363" Y="21.768525390625" />
                  <Point X="-0.514662841797" Y="21.554796875" />
                  <Point X="-0.530051208496" Y="21.532625" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.588640014648" Y="21.34696484375" />
                  <Point X="-0.916584716797" Y="20.12305859375" />
                  <Point X="-1.055444458008" Y="20.15001171875" />
                  <Point X="-1.079326660156" Y="20.1546484375" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.214962890625" Y="20.436560546875" />
                  <Point X="-1.214201049805" Y="20.4520703125" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.271347290039" Y="20.759466796875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.32364465332" Y="20.868794921875" />
                  <Point X="-1.534981933594" Y="21.0541328125" />
                  <Point X="-1.556905639648" Y="21.073359375" />
                  <Point X="-1.583190673828" Y="21.089705078125" />
                  <Point X="-1.612889404297" Y="21.102005859375" />
                  <Point X="-1.643028930664" Y="21.109033203125" />
                  <Point X="-1.923520385742" Y="21.12741796875" />
                  <Point X="-1.952618041992" Y="21.12932421875" />
                  <Point X="-1.98342199707" Y="21.1262890625" />
                  <Point X="-2.014469970703" Y="21.11796875" />
                  <Point X="-2.042658325195" Y="21.105197265625" />
                  <Point X="-2.276378662109" Y="20.94903125" />
                  <Point X="-2.300624511719" Y="20.932830078125" />
                  <Point X="-2.312787109375" Y="20.923177734375" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.356240234375" Y="20.872990234375" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.766692626953" Y="20.888931640625" />
                  <Point X="-2.801725341797" Y="20.910623046875" />
                  <Point X="-3.104721923828" Y="21.143919921875" />
                  <Point X="-3.098228271484" Y="21.15516796875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414560791016" Y="22.444427734375" />
                  <Point X="-2.428778808594" Y="22.4732578125" />
                  <Point X="-2.446808349609" Y="22.498416015625" />
                  <Point X="-2.462522460938" Y="22.51412890625" />
                  <Point X="-2.464145507812" Y="22.515751953125" />
                  <Point X="-2.489304931641" Y="22.533783203125" />
                  <Point X="-2.518134765625" Y="22.548001953125" />
                  <Point X="-2.54775390625" Y="22.556986328125" />
                  <Point X="-2.578689453125" Y="22.555974609375" />
                  <Point X="-2.610217529297" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-2.760372070312" Y="22.46883203125" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.0551953125" Y="22.16979296875" />
                  <Point X="-4.082860107422" Y="22.206138671875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.286052734375" Y="22.59596484375" />
                  <Point X="-3.105954345703" Y="23.501484375" />
                  <Point X="-3.084579833984" Y="23.52440234375" />
                  <Point X="-3.066615966797" Y="23.551529296875" />
                  <Point X="-3.053857177734" Y="23.580162109375" />
                  <Point X="-3.046876708984" Y="23.607115234375" />
                  <Point X="-3.046152587891" Y="23.60991015625" />
                  <Point X="-3.043348144531" Y="23.64033203125" />
                  <Point X="-3.045555419922" Y="23.672005859375" />
                  <Point X="-3.052555664062" Y="23.70175390625" />
                  <Point X="-3.068638671875" Y="23.727740234375" />
                  <Point X="-3.089472900391" Y="23.75169921875" />
                  <Point X="-3.112974853516" Y="23.771234375" />
                  <Point X="-3.136968505859" Y="23.78535546875" />
                  <Point X="-3.139457519531" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608886719" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.384917724609" Y="23.785474609375" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.823256835938" Y="23.964982421875" />
                  <Point X="-4.834076660156" Y="24.00733984375" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-4.877185058594" Y="24.4193828125" />
                  <Point X="-3.532875488281" Y="24.77958984375" />
                  <Point X="-3.517486572266" Y="24.785173828125" />
                  <Point X="-3.487728027344" Y="24.80053125" />
                  <Point X="-3.462583496094" Y="24.817982421875" />
                  <Point X="-3.459975097656" Y="24.81979296875" />
                  <Point X="-3.437519042969" Y="24.841677734375" />
                  <Point X="-3.41827734375" Y="24.8679296875" />
                  <Point X="-3.404168701172" Y="24.8959296875" />
                  <Point X="-3.395787109375" Y="24.922935546875" />
                  <Point X="-3.394917724609" Y="24.925736328125" />
                  <Point X="-3.3906484375" Y="24.95389453125" />
                  <Point X="-3.390647949219" Y="24.983537109375" />
                  <Point X="-3.394917236328" Y="25.01169921875" />
                  <Point X="-3.403296386719" Y="25.038697265625" />
                  <Point X="-3.404161376953" Y="25.041484375" />
                  <Point X="-3.418268554688" Y="25.0694921875" />
                  <Point X="-3.437513183594" Y="25.09575390625" />
                  <Point X="-3.459975097656" Y="25.117646484375" />
                  <Point X="-3.485119628906" Y="25.13509765625" />
                  <Point X="-3.497009765625" Y="25.142126953125" />
                  <Point X="-3.515187988281" Y="25.15116015625" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.672332763672" Y="25.195216796875" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.831460449219" Y="25.92985546875" />
                  <Point X="-4.824487792969" Y="25.9769765625" />
                  <Point X="-4.703551757812" Y="26.423265625" />
                  <Point X="-3.765668457031" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703137939453" Y="26.305263671875" />
                  <Point X="-3.647485107422" Y="26.322810546875" />
                  <Point X="-3.641711914063" Y="26.324630859375" />
                  <Point X="-3.622780761719" Y="26.3329609375" />
                  <Point X="-3.604038085938" Y="26.34378125" />
                  <Point X="-3.587356445312" Y="26.35601171875" />
                  <Point X="-3.573717529297" Y="26.3715625" />
                  <Point X="-3.561303222656" Y="26.389291015625" />
                  <Point X="-3.551352539062" Y="26.4074296875" />
                  <Point X="-3.529021728516" Y="26.461341796875" />
                  <Point X="-3.526705078125" Y="26.46693359375" />
                  <Point X="-3.52091796875" Y="26.48678515625" />
                  <Point X="-3.517158203125" Y="26.5081015625" />
                  <Point X="-3.515804443359" Y="26.52874609375" />
                  <Point X="-3.518951416016" Y="26.54919140625" />
                  <Point X="-3.524553955078" Y="26.570099609375" />
                  <Point X="-3.53205078125" Y="26.58937890625" />
                  <Point X="-3.558995361328" Y="26.641138671875" />
                  <Point X="-3.561790527344" Y="26.6465078125" />
                  <Point X="-3.57328125" Y="26.663705078125" />
                  <Point X="-3.587193115234" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.682128417969" Y="26.755970703125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.108245117188" Y="27.68724609375" />
                  <Point X="-4.08115625" Y="27.733654296875" />
                  <Point X="-3.750504638672" Y="28.158662109375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.1877265625" Y="27.836341796875" />
                  <Point X="-3.167082275391" Y="27.82983203125" />
                  <Point X="-3.146792724609" Y="27.825794921875" />
                  <Point X="-3.069283935547" Y="27.819013671875" />
                  <Point X="-3.061243408203" Y="27.818310546875" />
                  <Point X="-3.040557861328" Y="27.81876171875" />
                  <Point X="-3.019100585938" Y="27.821587890625" />
                  <Point X="-2.999010742188" Y="27.826505859375" />
                  <Point X="-2.9804609375" Y="27.835654296875" />
                  <Point X="-2.962208007812" Y="27.847283203125" />
                  <Point X="-2.946078369141" Y="27.860228515625" />
                  <Point X="-2.891062011719" Y="27.915244140625" />
                  <Point X="-2.885354736328" Y="27.920951171875" />
                  <Point X="-2.872409179688" Y="27.937080078125" />
                  <Point X="-2.860779296875" Y="27.955333984375" />
                  <Point X="-2.851629638672" Y="27.97388671875" />
                  <Point X="-2.846712646484" Y="27.99398046875" />
                  <Point X="-2.843887207031" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.850217041016" Y="28.11362890625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795654297" Y="28.18153515625" />
                  <Point X="-2.905242919922" Y="28.2429296875" />
                  <Point X="-3.183332275391" Y="28.724595703125" />
                  <Point X="-2.7478046875" Y="29.058509765625" />
                  <Point X="-2.700620605469" Y="29.094685546875" />
                  <Point X="-2.167035644531" Y="29.391134765625" />
                  <Point X="-2.04319519043" Y="29.2297421875" />
                  <Point X="-2.028891479492" Y="29.21480078125" />
                  <Point X="-2.012311767578" Y="29.200888671875" />
                  <Point X="-1.995113891602" Y="29.189396484375" />
                  <Point X="-1.908864135742" Y="29.14449609375" />
                  <Point X="-1.899915527344" Y="29.1398359375" />
                  <Point X="-1.880635009766" Y="29.1323359375" />
                  <Point X="-1.859725219727" Y="29.12673046875" />
                  <Point X="-1.839274536133" Y="29.123580078125" />
                  <Point X="-1.818627197266" Y="29.12493359375" />
                  <Point X="-1.797307983398" Y="29.128693359375" />
                  <Point X="-1.777452026367" Y="29.134482421875" />
                  <Point X="-1.687599243164" Y="29.171701171875" />
                  <Point X="-1.678278076172" Y="29.1755625" />
                  <Point X="-1.660137573242" Y="29.185515625" />
                  <Point X="-1.642408691406" Y="29.197931640625" />
                  <Point X="-1.626861450195" Y="29.21156640625" />
                  <Point X="-1.614633178711" Y="29.228244140625" />
                  <Point X="-1.603811645508" Y="29.246986328125" />
                  <Point X="-1.595479980469" Y="29.265921875" />
                  <Point X="-1.566234619141" Y="29.35867578125" />
                  <Point X="-1.563200683594" Y="29.368298828125" />
                  <Point X="-1.559165161133" Y="29.3885859375" />
                  <Point X="-1.557278930664" Y="29.410146484375" />
                  <Point X="-1.55773046875" Y="29.430826171875" />
                  <Point X="-1.561760498047" Y="29.461435546875" />
                  <Point X="-1.584202148438" Y="29.631896484375" />
                  <Point X="-1.010712097168" Y="29.79268359375" />
                  <Point X="-0.949625427246" Y="29.80980859375" />
                  <Point X="-0.294711120605" Y="29.88645703125" />
                  <Point X="-0.133903274536" Y="29.286314453125" />
                  <Point X="-0.121129814148" Y="29.258123046875" />
                  <Point X="-0.103271522522" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.05481804657" Y="29.194216796875" />
                  <Point X="-0.024379972458" Y="29.183884765625" />
                  <Point X="0.006155915737" Y="29.17884375" />
                  <Point X="0.036691837311" Y="29.183884765625" />
                  <Point X="0.06712991333" Y="29.194216796875" />
                  <Point X="0.094425926208" Y="29.208806640625" />
                  <Point X="0.115583534241" Y="29.231396484375" />
                  <Point X="0.133441680908" Y="29.258123046875" />
                  <Point X="0.146215301514" Y="29.286314453125" />
                  <Point X="0.164377929688" Y="29.354099609375" />
                  <Point X="0.307419555664" Y="29.887939453125" />
                  <Point X="0.790704162598" Y="29.83732421875" />
                  <Point X="0.844030212402" Y="29.831740234375" />
                  <Point X="1.427377807617" Y="29.69090234375" />
                  <Point X="1.481040039062" Y="29.6779453125" />
                  <Point X="1.8605859375" Y="29.54028125" />
                  <Point X="1.894647460938" Y="29.527927734375" />
                  <Point X="2.261794433594" Y="29.356224609375" />
                  <Point X="2.294557128906" Y="29.34090234375" />
                  <Point X="2.649278564453" Y="29.1342421875" />
                  <Point X="2.680990234375" Y="29.115767578125" />
                  <Point X="2.943260498047" Y="28.929255859375" />
                  <Point X="2.928226074219" Y="28.90321484375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142075683594" Y="27.5399296875" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.113625" Y="27.443314453125" />
                  <Point X="2.111155761719" Y="27.430732421875" />
                  <Point X="2.107800048828" Y="27.40428515625" />
                  <Point X="2.107727783203" Y="27.380953125" />
                  <Point X="2.1153125" Y="27.3180546875" />
                  <Point X="2.116099121094" Y="27.311529296875" />
                  <Point X="2.121442871094" Y="27.289603515625" />
                  <Point X="2.129711669922" Y="27.267509765625" />
                  <Point X="2.140072998047" Y="27.24746875" />
                  <Point X="2.178993164063" Y="27.190109375" />
                  <Point X="2.187016845703" Y="27.179869140625" />
                  <Point X="2.204352050781" Y="27.160623046875" />
                  <Point X="2.221599121094" Y="27.145591796875" />
                  <Point X="2.278957519531" Y="27.106671875" />
                  <Point X="2.284891845703" Y="27.10264453125" />
                  <Point X="2.304932617188" Y="27.09228125" />
                  <Point X="2.327026367188" Y="27.084009765625" />
                  <Point X="2.348962646484" Y="27.078662109375" />
                  <Point X="2.411862304688" Y="27.071078125" />
                  <Point X="2.425291503906" Y="27.070416015625" />
                  <Point X="2.450719726563" Y="27.070966796875" />
                  <Point X="2.473206054688" Y="27.074169921875" />
                  <Point X="2.545946533203" Y="27.093623046875" />
                  <Point X="2.553389404297" Y="27.0959453125" />
                  <Point X="2.573020263672" Y="27.10296484375" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="2.728800048828" Y="27.191126953125" />
                  <Point X="3.967326416016" Y="27.906189453125" />
                  <Point X="4.104468261719" Y="27.71559375" />
                  <Point X="4.123275390625" Y="27.68945703125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="4.258575683594" Y="27.457103515625" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221428710938" Y="26.660244140625" />
                  <Point X="3.203975830078" Y="26.641630859375" />
                  <Point X="3.151624511719" Y="26.573333984375" />
                  <Point X="3.144783935547" Y="26.56309765625" />
                  <Point X="3.130880859375" Y="26.539056640625" />
                  <Point X="3.121629638672" Y="26.517083984375" />
                  <Point X="3.102128662109" Y="26.447353515625" />
                  <Point X="3.100105712891" Y="26.440119140625" />
                  <Point X="3.096652099609" Y="26.417818359375" />
                  <Point X="3.095836425781" Y="26.39425" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.113747802734" Y="26.29418359375" />
                  <Point X="3.117076416016" Y="26.282125" />
                  <Point X="3.125935302734" Y="26.256697265625" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.179823730469" Y="26.16955859375" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198897460938" Y="26.1454453125" />
                  <Point X="3.216141113281" Y="26.129357421875" />
                  <Point X="3.234346923828" Y="26.11603515625" />
                  <Point X="3.297438964844" Y="26.08051953125" />
                  <Point X="3.309143554688" Y="26.0749453125" />
                  <Point X="3.333667480469" Y="26.065259765625" />
                  <Point X="3.356118408203" Y="26.0594375" />
                  <Point X="3.441429199219" Y="26.0481640625" />
                  <Point X="3.448796875" Y="26.047482421875" />
                  <Point X="3.470725830078" Y="26.04630859375" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.621446777344" Y="26.064525390625" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.837859863281" Y="25.96598046875" />
                  <Point X="4.845937011719" Y="25.9328046875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.716580322266" Y="25.32958984375" />
                  <Point X="3.704785644531" Y="25.325583984375" />
                  <Point X="3.681548095703" Y="25.315068359375" />
                  <Point X="3.597732910156" Y="25.266623046875" />
                  <Point X="3.58787109375" Y="25.2600703125" />
                  <Point X="3.564860595703" Y="25.24262109375" />
                  <Point X="3.547528808594" Y="25.22557421875" />
                  <Point X="3.497239746094" Y="25.161494140625" />
                  <Point X="3.492022949219" Y="25.154845703125" />
                  <Point X="3.480297119141" Y="25.1355625" />
                  <Point X="3.470525634766" Y="25.1141015625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.446917724609" Y="25.005072265625" />
                  <Point X="3.44540625" Y="24.993115234375" />
                  <Point X="3.443667236328" Y="24.9652265625" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.461942138672" Y="24.853916015625" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470525634766" Y="24.823337890625" />
                  <Point X="3.480297119141" Y="24.801876953125" />
                  <Point X="3.492022949219" Y="24.78259375" />
                  <Point X="3.542312011719" Y="24.718513671875" />
                  <Point X="3.550674560547" Y="24.7091953125" />
                  <Point X="3.570206054688" Y="24.690123046875" />
                  <Point X="3.589037109375" Y="24.675841796875" />
                  <Point X="3.672852294922" Y="24.627396484375" />
                  <Point X="3.679072021484" Y="24.624103515625" />
                  <Point X="3.699847167969" Y="24.614068359375" />
                  <Point X="3.716580322266" Y="24.60784765625" />
                  <Point X="3.838770751953" Y="24.575107421875" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.859532714844" Y="24.08118359375" />
                  <Point X="4.855022949219" Y="24.051271484375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.790380371094" Y="23.816720703125" />
                  <Point X="3.424380371094" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659423828" Y="23.994490234375" />
                  <Point X="3.210159912109" Y="23.958736328125" />
                  <Point X="3.193095214844" Y="23.95502734375" />
                  <Point X="3.1639765625" Y="23.943404296875" />
                  <Point X="3.136151123047" Y="23.926513671875" />
                  <Point X="3.1123984375" Y="23.906041015625" />
                  <Point X="3.012968994141" Y="23.78645703125" />
                  <Point X="3.002654296875" Y="23.774052734375" />
                  <Point X="2.987935302734" Y="23.749673828125" />
                  <Point X="2.976590332031" Y="23.722287109375" />
                  <Point X="2.969757324219" Y="23.694634765625" />
                  <Point X="2.955506591797" Y="23.53976953125" />
                  <Point X="2.954028320312" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.49243359375" />
                  <Point X="2.964078857422" Y="23.4608125" />
                  <Point X="2.976450439453" Y="23.432001953125" />
                  <Point X="3.067486816406" Y="23.29040234375" />
                  <Point X="3.072163818359" Y="23.28376953125" />
                  <Point X="3.093227783203" Y="23.256451171875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.224021972656" Y="23.152080078125" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.137520996094" Y="22.27078125" />
                  <Point X="4.124816894531" Y="22.25022265625" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="4.017562255859" Y="22.120646484375" />
                  <Point X="2.800954833984" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.558443847656" Y="22.87553125" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506784912109" Y="22.879603515625" />
                  <Point X="2.474610107422" Y="22.874646484375" />
                  <Point X="2.444831787109" Y="22.864822265625" />
                  <Point X="2.282186279297" Y="22.779224609375" />
                  <Point X="2.265313720703" Y="22.77034375" />
                  <Point X="2.242377685547" Y="22.7534453125" />
                  <Point X="2.221419677734" Y="22.732484375" />
                  <Point X="2.204531738281" Y="22.70955859375" />
                  <Point X="2.118932617188" Y="22.5469140625" />
                  <Point X="2.110052734375" Y="22.530041015625" />
                  <Point X="2.100228759766" Y="22.500265625" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.131033203125" Y="22.2409609375" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.224686035156" Y="22.047712890625" />
                  <Point X="2.861283935547" Y="20.94509375" />
                  <Point X="2.796935546875" Y="20.899130859375" />
                  <Point X="2.781853759766" Y="20.888357421875" />
                  <Point X="2.701765625" Y="20.836517578125" />
                  <Point X="2.686930419922" Y="20.8558515625" />
                  <Point X="1.758546630859" Y="22.065744140625" />
                  <Point X="1.747503051758" Y="22.0778203125" />
                  <Point X="1.721922485352" Y="22.099443359375" />
                  <Point X="1.528829956055" Y="22.223583984375" />
                  <Point X="1.508798950195" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448367675781" Y="22.2565625" />
                  <Point X="1.417100952148" Y="22.258880859375" />
                  <Point X="1.205921630859" Y="22.23944921875" />
                  <Point X="1.184014160156" Y="22.23743359375" />
                  <Point X="1.156365112305" Y="22.230603515625" />
                  <Point X="1.128977783203" Y="22.219259765625" />
                  <Point X="1.104594238281" Y="22.2045390625" />
                  <Point X="0.941526733398" Y="22.068953125" />
                  <Point X="0.924610473633" Y="22.05488671875" />
                  <Point X="0.904138183594" Y="22.0311328125" />
                  <Point X="0.887247436523" Y="22.003306640625" />
                  <Point X="0.875624389648" Y="21.974189453125" />
                  <Point X="0.826868103027" Y="21.749873046875" />
                  <Point X="0.821810302734" Y="21.726603515625" />
                  <Point X="0.819724853516" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.840392150879" Y="21.520029296875" />
                  <Point X="1.022065490723" Y="20.1400859375" />
                  <Point X="0.989949401855" Y="20.133044921875" />
                  <Point X="0.975661376953" Y="20.1299140625" />
                  <Point X="0.929315917969" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.037342529297" Y="20.243271484375" />
                  <Point X="-1.058420654297" Y="20.24736328125" />
                  <Point X="-1.141245849609" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077270508" Y="20.431900390625" />
                  <Point X="-1.119451660156" Y="20.458966796875" />
                  <Point X="-1.121759033203" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.178172729492" Y="20.778" />
                  <Point X="-1.183861572266" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575317383" Y="20.905140625" />
                  <Point X="-1.250209960938" Y="20.929064453125" />
                  <Point X="-1.261006835938" Y="20.94021875" />
                  <Point X="-1.472344116211" Y="21.125556640625" />
                  <Point X="-1.494267822266" Y="21.144783203125" />
                  <Point X="-1.506737792969" Y="21.154033203125" />
                  <Point X="-1.533022827148" Y="21.17037890625" />
                  <Point X="-1.546837768555" Y="21.177474609375" />
                  <Point X="-1.576536499023" Y="21.189775390625" />
                  <Point X="-1.591317749023" Y="21.1945234375" />
                  <Point X="-1.621457275391" Y="21.20155078125" />
                  <Point X="-1.636815673828" Y="21.203830078125" />
                  <Point X="-1.917307006836" Y="21.22221484375" />
                  <Point X="-1.946404663086" Y="21.22412109375" />
                  <Point X="-1.96193347168" Y="21.2238671875" />
                  <Point X="-1.992737426758" Y="21.22083203125" />
                  <Point X="-2.008012817383" Y="21.21805078125" />
                  <Point X="-2.039060668945" Y="21.20973046875" />
                  <Point X="-2.05367578125" Y="21.204501953125" />
                  <Point X="-2.081864257813" Y="21.19173046875" />
                  <Point X="-2.095437255859" Y="21.1841875" />
                  <Point X="-2.329157714844" Y="21.028021484375" />
                  <Point X="-2.353403564453" Y="21.0118203125" />
                  <Point X="-2.359680175781" Y="21.007244140625" />
                  <Point X="-2.380446044922" Y="20.989865234375" />
                  <Point X="-2.40276171875" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.431608886719" Y="20.930822265625" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.716681884766" Y="20.969703125" />
                  <Point X="-2.747599121094" Y="20.988845703125" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.341487548828" Y="22.2758828125" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314666992188" Y="22.442388671875" />
                  <Point X="-2.323651855469" Y="22.472005859375" />
                  <Point X="-2.329358642578" Y="22.486447265625" />
                  <Point X="-2.343576660156" Y="22.51527734375" />
                  <Point X="-2.351560546875" Y="22.528595703125" />
                  <Point X="-2.369590087891" Y="22.55375390625" />
                  <Point X="-2.379635742188" Y="22.56559375" />
                  <Point X="-2.395349853516" Y="22.581306640625" />
                  <Point X="-2.408805419922" Y="22.59296875" />
                  <Point X="-2.43396484375" Y="22.611" />
                  <Point X="-2.447284179688" Y="22.618984375" />
                  <Point X="-2.476114013672" Y="22.633203125" />
                  <Point X="-2.490559082031" Y="22.638912109375" />
                  <Point X="-2.520178222656" Y="22.647896484375" />
                  <Point X="-2.550859130859" Y="22.651935546875" />
                  <Point X="-2.581794677734" Y="22.650923828125" />
                  <Point X="-2.597223388672" Y="22.6491484375" />
                  <Point X="-2.628751464844" Y="22.642876953125" />
                  <Point X="-2.643681884766" Y="22.63861328125" />
                  <Point X="-2.672648193359" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-2.807872314453" Y="22.551103515625" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.979601806641" Y="22.22733203125" />
                  <Point X="-4.004020263672" Y="22.259412109375" />
                  <Point X="-4.181265625" Y="22.556626953125" />
                  <Point X="-3.048120849609" Y="23.4261171875" />
                  <Point X="-3.03648046875" Y="23.436689453125" />
                  <Point X="-3.015105957031" Y="23.459607421875" />
                  <Point X="-3.005372802734" Y="23.471951171875" />
                  <Point X="-2.987408935547" Y="23.499078125" />
                  <Point X="-2.979841064453" Y="23.51286328125" />
                  <Point X="-2.967082275391" Y="23.54149609375" />
                  <Point X="-2.961891357422" Y="23.55634375" />
                  <Point X="-2.954910888672" Y="23.583296875" />
                  <Point X="-2.951553710938" Y="23.601189453125" />
                  <Point X="-2.948749267578" Y="23.631611328125" />
                  <Point X="-2.948577880859" Y="23.646935546875" />
                  <Point X="-2.95078515625" Y="23.678609375" />
                  <Point X="-2.953081298828" Y="23.693767578125" />
                  <Point X="-2.960081542969" Y="23.723515625" />
                  <Point X="-2.971775146484" Y="23.75175" />
                  <Point X="-2.987858154297" Y="23.777736328125" />
                  <Point X="-2.996951660156" Y="23.790078125" />
                  <Point X="-3.017785888672" Y="23.814037109375" />
                  <Point X="-3.028746826172" Y="23.824755859375" />
                  <Point X="-3.052248779297" Y="23.844291015625" />
                  <Point X="-3.064789794922" Y="23.853107421875" />
                  <Point X="-3.088783447266" Y="23.867228515625" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.134701171875" Y="23.886744140625" />
                  <Point X="-3.149801757812" Y="23.891142578125" />
                  <Point X="-3.181688476562" Y="23.897623046875" />
                  <Point X="-3.197305175781" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-3.397317382812" Y="23.879662109375" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.731211914062" Y="23.988494140625" />
                  <Point X="-4.740761230469" Y="24.025876953125" />
                  <Point X="-4.786451660156" Y="24.34534375" />
                  <Point X="-3.508283447266" Y="24.687828125" />
                  <Point X="-3.500471435547" Y="24.690287109375" />
                  <Point X="-3.473919677734" Y="24.700751953125" />
                  <Point X="-3.444161132812" Y="24.716109375" />
                  <Point X="-3.433562011719" Y="24.722486328125" />
                  <Point X="-3.408417480469" Y="24.7399375" />
                  <Point X="-3.393670898438" Y="24.7517578125" />
                  <Point X="-3.37121484375" Y="24.773642578125" />
                  <Point X="-3.360896972656" Y="24.785517578125" />
                  <Point X="-3.341655273438" Y="24.81176953125" />
                  <Point X="-3.333438720703" Y="24.825181640625" />
                  <Point X="-3.319330078125" Y="24.853181640625" />
                  <Point X="-3.313437988281" Y="24.86776953125" />
                  <Point X="-3.305056396484" Y="24.894775390625" />
                  <Point X="-3.300991210938" Y="24.91149609375" />
                  <Point X="-3.296721923828" Y="24.939654296875" />
                  <Point X="-3.2956484375" Y="24.953892578125" />
                  <Point X="-3.295647949219" Y="24.98353515625" />
                  <Point X="-3.296721191406" Y="24.997775390625" />
                  <Point X="-3.300990478516" Y="25.0259375" />
                  <Point X="-3.304186523438" Y="25.039859375" />
                  <Point X="-3.312565673828" Y="25.066857421875" />
                  <Point X="-3.31931640625" Y="25.084220703125" />
                  <Point X="-3.333423583984" Y="25.112228515625" />
                  <Point X="-3.341640625" Y="25.12564453125" />
                  <Point X="-3.360885253906" Y="25.15190625" />
                  <Point X="-3.371205810547" Y="25.16378515625" />
                  <Point X="-3.393667724609" Y="25.185677734375" />
                  <Point X="-3.405809082031" Y="25.19569140625" />
                  <Point X="-3.430953613281" Y="25.213142578125" />
                  <Point X="-3.4367734375" Y="25.216875" />
                  <Point X="-3.454733886719" Y="25.227201171875" />
                  <Point X="-3.472912109375" Y="25.236234375" />
                  <Point X="-3.481583251953" Y="25.240017578125" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.647745117188" Y="25.28698046875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.737483886719" Y="25.91594921875" />
                  <Point X="-4.731331054688" Y="25.95753125" />
                  <Point X="-4.633586914062" Y="26.318234375" />
                  <Point X="-3.778068359375" Y="26.20560546875" />
                  <Point X="-3.767740234375" Y="26.20481640625" />
                  <Point X="-3.747056640625" Y="26.204365234375" />
                  <Point X="-3.736703613281" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704888671875" Y="26.2080546875" />
                  <Point X="-3.684603515625" Y="26.21208984375" />
                  <Point X="-3.674571533203" Y="26.21466015625" />
                  <Point X="-3.618918701172" Y="26.23220703125" />
                  <Point X="-3.603450439453" Y="26.23767578125" />
                  <Point X="-3.584519287109" Y="26.246005859375" />
                  <Point X="-3.575283203125" Y="26.2506875" />
                  <Point X="-3.556540527344" Y="26.2615078125" />
                  <Point X="-3.547866699219" Y="26.267166015625" />
                  <Point X="-3.531185058594" Y="26.279396484375" />
                  <Point X="-3.515934570312" Y="26.29337109375" />
                  <Point X="-3.502295654297" Y="26.308921875" />
                  <Point X="-3.495899414062" Y="26.3170703125" />
                  <Point X="-3.483485107422" Y="26.334798828125" />
                  <Point X="-3.478013183594" Y="26.343599609375" />
                  <Point X="-3.4680625" Y="26.36173828125" />
                  <Point X="-3.463583740234" Y="26.371076171875" />
                  <Point X="-3.441252929688" Y="26.42498828125" />
                  <Point X="-3.435501464844" Y="26.440345703125" />
                  <Point X="-3.429714355469" Y="26.460197265625" />
                  <Point X="-3.427362060547" Y="26.470283203125" />
                  <Point X="-3.423602294922" Y="26.491599609375" />
                  <Point X="-3.422361816406" Y="26.501884765625" />
                  <Point X="-3.421008056641" Y="26.522529296875" />
                  <Point X="-3.42191015625" Y="26.54319921875" />
                  <Point X="-3.425057128906" Y="26.56364453125" />
                  <Point X="-3.427188720703" Y="26.573779296875" />
                  <Point X="-3.432791259766" Y="26.5946875" />
                  <Point X="-3.436012451172" Y="26.604529296875" />
                  <Point X="-3.443509277344" Y="26.62380859375" />
                  <Point X="-3.447784912109" Y="26.63324609375" />
                  <Point X="-3.474729492188" Y="26.685005859375" />
                  <Point X="-3.482800537109" Y="26.699287109375" />
                  <Point X="-3.494291259766" Y="26.716484375" />
                  <Point X="-3.500506103516" Y="26.72476953125" />
                  <Point X="-3.51441796875" Y="26.741349609375" />
                  <Point X="-3.521498779297" Y="26.74891015625" />
                  <Point X="-3.53644140625" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.624295898438" Y="26.83133984375" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.026198730469" Y="27.639357421875" />
                  <Point X="-4.002296630859" Y="27.680306640625" />
                  <Point X="-3.726337890625" Y="28.035013671875" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244919921875" Y="27.75771875" />
                  <Point X="-3.225989501953" Y="27.749388671875" />
                  <Point X="-3.216296142578" Y="27.745740234375" />
                  <Point X="-3.195651855469" Y="27.73923046875" />
                  <Point X="-3.185621337891" Y="27.736658203125" />
                  <Point X="-3.165331787109" Y="27.73262109375" />
                  <Point X="-3.155072753906" Y="27.73115625" />
                  <Point X="-3.077563964844" Y="27.724375" />
                  <Point X="-3.059171875" Y="27.723333984375" />
                  <Point X="-3.038486328125" Y="27.72378515625" />
                  <Point X="-3.02815234375" Y="27.72457421875" />
                  <Point X="-3.006695068359" Y="27.727400390625" />
                  <Point X="-2.99651171875" Y="27.7293125" />
                  <Point X="-2.976421875" Y="27.73423046875" />
                  <Point X="-2.956990722656" Y="27.7413046875" />
                  <Point X="-2.938440917969" Y="27.750453125" />
                  <Point X="-2.929415771484" Y="27.755533203125" />
                  <Point X="-2.911162841797" Y="27.767162109375" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886615966797" Y="27.786138671875" />
                  <Point X="-2.878903564453" Y="27.793052734375" />
                  <Point X="-2.823887207031" Y="27.848068359375" />
                  <Point X="-2.811267333984" Y="27.861486328125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288818359" Y="27.886033203125" />
                  <Point X="-2.780658935547" Y="27.904287109375" />
                  <Point X="-2.775577148438" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759352294922" Y="27.951306640625" />
                  <Point X="-2.754435302734" Y="27.971400390625" />
                  <Point X="-2.752525634766" Y="27.981580078125" />
                  <Point X="-2.749700195312" Y="28.0030390625" />
                  <Point X="-2.748909912109" Y="28.013365234375" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.755578613281" Y="28.121908203125" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509033203" Y="28.200859375" />
                  <Point X="-2.782840576172" Y="28.219794921875" />
                  <Point X="-2.787523925781" Y="28.229037109375" />
                  <Point X="-2.822971191406" Y="28.290431640625" />
                  <Point X="-3.059385986328" Y="28.699916015625" />
                  <Point X="-2.690002685547" Y="28.9831171875" />
                  <Point X="-2.648369628906" Y="29.015037109375" />
                  <Point X="-2.192523193359" Y="29.268296875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111818847656" Y="29.164046875" />
                  <Point X="-2.097515136719" Y="29.14910546875" />
                  <Point X="-2.089956542969" Y="29.14202734375" />
                  <Point X="-2.073376708984" Y="29.128115234375" />
                  <Point X="-2.065093994141" Y="29.121900390625" />
                  <Point X="-2.047895996094" Y="29.110408203125" />
                  <Point X="-2.038981201172" Y="29.105130859375" />
                  <Point X="-1.952743408203" Y="29.060236328125" />
                  <Point X="-1.952743652344" Y="29.060236328125" />
                  <Point X="-1.934355957031" Y="29.051298828125" />
                  <Point X="-1.915075439453" Y="29.043798828125" />
                  <Point X="-1.905233886719" Y="29.040576171875" />
                  <Point X="-1.884323974609" Y="29.034970703125" />
                  <Point X="-1.874189331055" Y="29.032837890625" />
                  <Point X="-1.853738525391" Y="29.0296875" />
                  <Point X="-1.833060302734" Y="29.028783203125" />
                  <Point X="-1.812412963867" Y="29.03013671875" />
                  <Point X="-1.802127929688" Y="29.031376953125" />
                  <Point X="-1.78080871582" Y="29.03513671875" />
                  <Point X="-1.770717529297" Y="29.037490234375" />
                  <Point X="-1.750861572266" Y="29.043279296875" />
                  <Point X="-1.741096679688" Y="29.04671484375" />
                  <Point X="-1.651243896484" Y="29.08393359375" />
                  <Point X="-1.632580932617" Y="29.092275390625" />
                  <Point X="-1.614440429688" Y="29.102228515625" />
                  <Point X="-1.605641723633" Y="29.107701171875" />
                  <Point X="-1.587912719727" Y="29.1201171875" />
                  <Point X="-1.579770263672" Y="29.1265078125" />
                  <Point X="-1.564223022461" Y="29.140142578125" />
                  <Point X="-1.550248413086" Y="29.155392578125" />
                  <Point X="-1.538020141602" Y="29.1720703125" />
                  <Point X="-1.532362304688" Y="29.1807421875" />
                  <Point X="-1.521540771484" Y="29.199484375" />
                  <Point X="-1.516856811523" Y="29.2087265625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504876953125" Y="29.23735546875" />
                  <Point X="-1.475631591797" Y="29.330109375" />
                  <Point X="-1.470026245117" Y="29.349763671875" />
                  <Point X="-1.465990722656" Y="29.37005078125" />
                  <Point X="-1.464526611328" Y="29.380306640625" />
                  <Point X="-1.462640380859" Y="29.4018671875" />
                  <Point X="-1.462301513672" Y="29.412220703125" />
                  <Point X="-1.462753051758" Y="29.432900390625" />
                  <Point X="-1.463543334961" Y="29.4432265625" />
                  <Point X="-1.467573364258" Y="29.4738359375" />
                  <Point X="-1.479266479492" Y="29.562654296875" />
                  <Point X="-0.98506628418" Y="29.7012109375" />
                  <Point X="-0.931167358398" Y="29.7163203125" />
                  <Point X="-0.365222351074" Y="29.782556640625" />
                  <Point X="-0.225666290283" Y="29.2617265625" />
                  <Point X="-0.220435211182" Y="29.247107421875" />
                  <Point X="-0.207661819458" Y="29.218916015625" />
                  <Point X="-0.20011920166" Y="29.20534375" />
                  <Point X="-0.182260818481" Y="29.1786171875" />
                  <Point X="-0.172608810425" Y="29.166455078125" />
                  <Point X="-0.151451293945" Y="29.143865234375" />
                  <Point X="-0.12689641571" Y="29.1250234375" />
                  <Point X="-0.099600479126" Y="29.11043359375" />
                  <Point X="-0.085353935242" Y="29.1042578125" />
                  <Point X="-0.054915962219" Y="29.09392578125" />
                  <Point X="-0.039853626251" Y="29.090154296875" />
                  <Point X="-0.009317714691" Y="29.08511328125" />
                  <Point X="0.021629587173" Y="29.08511328125" />
                  <Point X="0.052165500641" Y="29.090154296875" />
                  <Point X="0.067227836609" Y="29.09392578125" />
                  <Point X="0.097665802002" Y="29.1042578125" />
                  <Point X="0.111912200928" Y="29.11043359375" />
                  <Point X="0.139208129883" Y="29.1250234375" />
                  <Point X="0.163763168335" Y="29.143865234375" />
                  <Point X="0.184920822144" Y="29.166455078125" />
                  <Point X="0.194573135376" Y="29.1786171875" />
                  <Point X="0.212431228638" Y="29.20534375" />
                  <Point X="0.219973388672" Y="29.218916015625" />
                  <Point X="0.232747085571" Y="29.247107421875" />
                  <Point X="0.237978302002" Y="29.2617265625" />
                  <Point X="0.256140930176" Y="29.32951171875" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.78080871582" Y="29.742841796875" />
                  <Point X="0.827852416992" Y="29.737916015625" />
                  <Point X="1.405082641602" Y="29.5985546875" />
                  <Point X="1.453621704102" Y="29.586833984375" />
                  <Point X="1.828193603516" Y="29.450974609375" />
                  <Point X="1.85824987793" Y="29.44007421875" />
                  <Point X="2.221549560547" Y="29.270169921875" />
                  <Point X="2.250430175781" Y="29.2566640625" />
                  <Point X="2.601455810547" Y="29.05215625" />
                  <Point X="2.629448486328" Y="29.03584765625" />
                  <Point X="2.817780029297" Y="28.90191796875" />
                  <Point X="2.065308105469" Y="27.59859765625" />
                  <Point X="2.062371826172" Y="27.5931015625" />
                  <Point X="2.053180664062" Y="27.573435546875" />
                  <Point X="2.044181518555" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.021849853516" Y="27.46785546875" />
                  <Point X="2.016911376953" Y="27.44269140625" />
                  <Point X="2.013555541992" Y="27.416244140625" />
                  <Point X="2.012800537109" Y="27.404580078125" />
                  <Point X="2.012728271484" Y="27.381248046875" />
                  <Point X="2.013411010742" Y="27.369580078125" />
                  <Point X="2.020995605469" Y="27.306681640625" />
                  <Point X="2.02380078125" Y="27.28903515625" />
                  <Point X="2.02914453125" Y="27.267109375" />
                  <Point X="2.032469970703" Y="27.2563046875" />
                  <Point X="2.040738769531" Y="27.2342109375" />
                  <Point X="2.045322875977" Y="27.223880859375" />
                  <Point X="2.055684326172" Y="27.20383984375" />
                  <Point X="2.061461425781" Y="27.19412890625" />
                  <Point X="2.100381591797" Y="27.13676953125" />
                  <Point X="2.116428955078" Y="27.1162890625" />
                  <Point X="2.133764160156" Y="27.09704296875" />
                  <Point X="2.141935302734" Y="27.089005859375" />
                  <Point X="2.159182373047" Y="27.073974609375" />
                  <Point X="2.168258300781" Y="27.06698046875" />
                  <Point X="2.225616699219" Y="27.028060546875" />
                  <Point X="2.241255371094" Y="27.018259765625" />
                  <Point X="2.261296142578" Y="27.007896484375" />
                  <Point X="2.271624267578" Y="27.0033125" />
                  <Point X="2.293718017578" Y="26.995041015625" />
                  <Point X="2.304526123047" Y="26.991712890625" />
                  <Point X="2.326462402344" Y="26.986365234375" />
                  <Point X="2.337590576172" Y="26.984345703125" />
                  <Point X="2.400490234375" Y="26.97676171875" />
                  <Point X="2.407184082031" Y="26.976193359375" />
                  <Point X="2.427348632812" Y="26.9754375" />
                  <Point X="2.452776855469" Y="26.97598828125" />
                  <Point X="2.464116943359" Y="26.976916015625" />
                  <Point X="2.486603271484" Y="26.980119140625" />
                  <Point X="2.497749511719" Y="26.98239453125" />
                  <Point X="2.570489990234" Y="27.00184765625" />
                  <Point X="2.585375732422" Y="27.0064921875" />
                  <Point X="2.605006591797" Y="27.01351171875" />
                  <Point X="2.612920654297" Y="27.01675" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="2.776299804688" Y="27.108853515625" />
                  <Point X="3.940405029297" Y="27.78094921875" />
                  <Point X="4.027355712891" Y="27.660107421875" />
                  <Point X="4.043958740234" Y="27.637033203125" />
                  <Point X="4.136884277344" Y="27.48347265625" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168133300781" Y="26.739865234375" />
                  <Point X="3.152128173828" Y="26.725224609375" />
                  <Point X="3.134675292969" Y="26.706611328125" />
                  <Point X="3.128578125" Y="26.69942578125" />
                  <Point X="3.076226806641" Y="26.63112890625" />
                  <Point X="3.062545654297" Y="26.61065625" />
                  <Point X="3.048642578125" Y="26.586615234375" />
                  <Point X="3.043324951172" Y="26.575919921875" />
                  <Point X="3.034073730469" Y="26.553947265625" />
                  <Point X="3.030140136719" Y="26.542669921875" />
                  <Point X="3.010639160156" Y="26.472939453125" />
                  <Point X="3.006224853516" Y="26.454658203125" />
                  <Point X="3.002771240234" Y="26.432357421875" />
                  <Point X="3.001708984375" Y="26.421103515625" />
                  <Point X="3.000893310547" Y="26.39753515625" />
                  <Point X="3.001175048828" Y="26.386236328125" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.020707763672" Y="26.274986328125" />
                  <Point X="3.027364990234" Y="26.250869140625" />
                  <Point X="3.036223876953" Y="26.22544140625" />
                  <Point X="3.040751464844" Y="26.214640625" />
                  <Point X="3.051099121094" Y="26.193681640625" />
                  <Point X="3.056919189453" Y="26.1835234375" />
                  <Point X="3.100459960938" Y="26.11734375" />
                  <Point X="3.111740966797" Y="26.101421875" />
                  <Point X="3.126297851562" Y="26.084173828125" />
                  <Point X="3.134090576172" Y="26.075982421875" />
                  <Point X="3.151334228516" Y="26.05989453125" />
                  <Point X="3.160040039062" Y="26.05269140625" />
                  <Point X="3.178245849609" Y="26.039369140625" />
                  <Point X="3.187745849609" Y="26.03325" />
                  <Point X="3.250837890625" Y="25.997734375" />
                  <Point X="3.274247070312" Y="25.9865859375" />
                  <Point X="3.298770996094" Y="25.976900390625" />
                  <Point X="3.309819824219" Y="25.97330078125" />
                  <Point X="3.332270751953" Y="25.967478515625" />
                  <Point X="3.343672851563" Y="25.965255859375" />
                  <Point X="3.428983642578" Y="25.953982421875" />
                  <Point X="3.443718994141" Y="25.952619140625" />
                  <Point X="3.465647949219" Y="25.9514453125" />
                  <Point X="3.474396484375" Y="25.95137890625" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.633846191406" Y="25.970337890625" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.745555664063" Y="25.943509765625" />
                  <Point X="4.752684082031" Y="25.91423046875" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="3.691992431641" Y="25.421353515625" />
                  <Point X="3.686029052734" Y="25.41954296875" />
                  <Point X="3.665619140625" Y="25.412134765625" />
                  <Point X="3.642381591797" Y="25.401619140625" />
                  <Point X="3.6340078125" Y="25.397318359375" />
                  <Point X="3.550192626953" Y="25.348873046875" />
                  <Point X="3.530468994141" Y="25.335767578125" />
                  <Point X="3.507458496094" Y="25.318318359375" />
                  <Point X="3.498244384766" Y="25.3103515625" />
                  <Point X="3.480912597656" Y="25.2933046875" />
                  <Point X="3.472794921875" Y="25.284224609375" />
                  <Point X="3.422505859375" Y="25.22014453125" />
                  <Point X="3.410852050781" Y="25.204205078125" />
                  <Point X="3.399126220703" Y="25.184921875" />
                  <Point X="3.393837402344" Y="25.1749296875" />
                  <Point X="3.384065917969" Y="25.15346875" />
                  <Point X="3.380003173828" Y="25.142923828125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.35361328125" Y="25.02294140625" />
                  <Point X="3.350590332031" Y="24.99902734375" />
                  <Point X="3.348851318359" Y="24.971138671875" />
                  <Point X="3.348858642578" Y="24.95919921875" />
                  <Point X="3.350370361328" Y="24.93541796875" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.368637939453" Y="24.836046875" />
                  <Point X="3.373158203125" Y="24.816015625" />
                  <Point X="3.380002685547" Y="24.794517578125" />
                  <Point X="3.384065917969" Y="24.783970703125" />
                  <Point X="3.393837402344" Y="24.762509765625" />
                  <Point X="3.399126220703" Y="24.752517578125" />
                  <Point X="3.410852050781" Y="24.733234375" />
                  <Point X="3.4172890625" Y="24.723943359375" />
                  <Point X="3.467578125" Y="24.65986328125" />
                  <Point X="3.484303222656" Y="24.6412265625" />
                  <Point X="3.503834716797" Y="24.622154296875" />
                  <Point X="3.512800537109" Y="24.6144296875" />
                  <Point X="3.531631591797" Y="24.6001484375" />
                  <Point X="3.541496826172" Y="24.593591796875" />
                  <Point X="3.625312011719" Y="24.545146484375" />
                  <Point X="3.637751464844" Y="24.538560546875" />
                  <Point X="3.658526611328" Y="24.528525390625" />
                  <Point X="3.666743652344" Y="24.5250234375" />
                  <Point X="3.691992919922" Y="24.516083984375" />
                  <Point X="3.814183349609" Y="24.48334375" />
                  <Point X="4.784877441406" Y="24.223248046875" />
                  <Point X="4.765594238281" Y="24.095345703125" />
                  <Point X="4.761614746094" Y="24.06894921875" />
                  <Point X="4.727801269531" Y="23.92077734375" />
                  <Point X="3.436771240234" Y="24.09074609375" />
                  <Point X="3.428621826172" Y="24.091462890625" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720947266" Y="24.089158203125" />
                  <Point X="3.354482421875" Y="24.087322265625" />
                  <Point X="3.189982910156" Y="24.051568359375" />
                  <Point X="3.157876953125" Y="24.0432578125" />
                  <Point X="3.128758300781" Y="24.031634765625" />
                  <Point X="3.114680908203" Y="24.02461328125" />
                  <Point X="3.08685546875" Y="24.00772265625" />
                  <Point X="3.074128417969" Y="23.99847265625" />
                  <Point X="3.050375732422" Y="23.978" />
                  <Point X="3.039350097656" Y="23.96677734375" />
                  <Point X="2.939920654297" Y="23.847193359375" />
                  <Point X="2.921327636719" Y="23.823154296875" />
                  <Point X="2.906608642578" Y="23.798775390625" />
                  <Point X="2.90016796875" Y="23.78603125" />
                  <Point X="2.888822998047" Y="23.75864453125" />
                  <Point X="2.884364257812" Y="23.745076171875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875156982422" Y="23.70333984375" />
                  <Point X="2.86090625" Y="23.548474609375" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607421875" Y="23.485408203125" />
                  <Point X="2.864065673828" Y="23.469869140625" />
                  <Point X="2.871797363281" Y="23.438248046875" />
                  <Point X="2.876786621094" Y="23.423328125" />
                  <Point X="2.889158203125" Y="23.394517578125" />
                  <Point X="2.896540527344" Y="23.380626953125" />
                  <Point X="2.987576904297" Y="23.23902734375" />
                  <Point X="2.996930908203" Y="23.22576171875" />
                  <Point X="3.017994873047" Y="23.198443359375" />
                  <Point X="3.026128662109" Y="23.18919921875" />
                  <Point X="3.043529296875" Y="23.171837890625" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.166189697266" Y="23.0767109375" />
                  <Point X="4.087170898438" Y="22.370015625" />
                  <Point X="4.056707763672" Y="22.32072265625" />
                  <Point X="4.0455" Y="22.3025859375" />
                  <Point X="4.0012734375" Y="22.23974609375" />
                  <Point X="2.848452148438" Y="22.905328125" />
                  <Point X="2.84119140625" Y="22.90911328125" />
                  <Point X="2.815026123047" Y="22.920484375" />
                  <Point X="2.78312109375" Y="22.930671875" />
                  <Point X="2.771107910156" Y="22.933662109375" />
                  <Point X="2.575327392578" Y="22.96901953125" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508010009766" Y="22.974595703125" />
                  <Point X="2.492319335938" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846435547" Y="22.96486328125" />
                  <Point X="2.415068115234" Y="22.9550390625" />
                  <Point X="2.400587890625" Y="22.948890625" />
                  <Point X="2.237942382812" Y="22.86329296875" />
                  <Point X="2.208963623047" Y="22.846826171875" />
                  <Point X="2.186027587891" Y="22.829927734375" />
                  <Point X="2.175197753906" Y="22.820615234375" />
                  <Point X="2.154239746094" Y="22.799654296875" />
                  <Point X="2.144931884766" Y="22.788828125" />
                  <Point X="2.128043945313" Y="22.76590234375" />
                  <Point X="2.120463867188" Y="22.753802734375" />
                  <Point X="2.034864746094" Y="22.591158203125" />
                  <Point X="2.019836303711" Y="22.559806640625" />
                  <Point X="2.010012329102" Y="22.53003125" />
                  <Point X="2.006337036133" Y="22.514734375" />
                  <Point X="2.001379394531" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.037545532227" Y="22.224078125" />
                  <Point X="2.041213378906" Y="22.203767578125" />
                  <Point X="2.043014770508" Y="22.195779296875" />
                  <Point X="2.051236816406" Y="22.168453125" />
                  <Point X="2.0640703125" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.142413818359" Y="22.000212890625" />
                  <Point X="2.735894287109" Y="20.972275390625" />
                  <Point X="2.723755371094" Y="20.963916015625" />
                  <Point X="1.833915893555" Y="22.123576171875" />
                  <Point X="1.828652099609" Y="22.12985546875" />
                  <Point X="1.808831176758" Y="22.150373046875" />
                  <Point X="1.783250488281" Y="22.17199609375" />
                  <Point X="1.773297119141" Y="22.179353515625" />
                  <Point X="1.580204711914" Y="22.303494140625" />
                  <Point X="1.560173706055" Y="22.31637109375" />
                  <Point X="1.546279907227" Y="22.32375390625" />
                  <Point X="1.517467163086" Y="22.336125" />
                  <Point X="1.502548461914" Y="22.34111328125" />
                  <Point X="1.4709296875" Y="22.34884375" />
                  <Point X="1.455392456055" Y="22.351302734375" />
                  <Point X="1.424125732422" Y="22.35362109375" />
                  <Point X="1.408396240234" Y="22.35348046875" />
                  <Point X="1.197216918945" Y="22.334048828125" />
                  <Point X="1.175309448242" Y="22.332033203125" />
                  <Point X="1.161231323242" Y="22.329662109375" />
                  <Point X="1.133582275391" Y="22.32283203125" />
                  <Point X="1.120011474609" Y="22.318373046875" />
                  <Point X="1.092624145508" Y="22.307029296875" />
                  <Point X="1.07987878418" Y="22.300587890625" />
                  <Point X="1.055495239258" Y="22.2858671875" />
                  <Point X="1.043857055664" Y="22.277587890625" />
                  <Point X="0.880789611816" Y="22.142001953125" />
                  <Point X="0.863873352051" Y="22.127935546875" />
                  <Point X="0.852648742676" Y="22.11690625" />
                  <Point X="0.832176391602" Y="22.09315234375" />
                  <Point X="0.822928649902" Y="22.080427734375" />
                  <Point X="0.806037780762" Y="22.0526015625" />
                  <Point X="0.799017272949" Y="22.03852734375" />
                  <Point X="0.787394226074" Y="22.00941015625" />
                  <Point X="0.782791992188" Y="21.9943671875" />
                  <Point X="0.734035644531" Y="21.77005078125" />
                  <Point X="0.728977844238" Y="21.74678125" />
                  <Point X="0.727584960938" Y="21.7387109375" />
                  <Point X="0.724724853516" Y="21.71032421875" />
                  <Point X="0.724742248535" Y="21.67683203125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.74620489502" Y="21.50762890625" />
                  <Point X="0.833091796875" Y="20.847658203125" />
                  <Point X="0.655064697266" Y="21.512064453125" />
                  <Point X="0.652606201172" Y="21.519876953125" />
                  <Point X="0.642146972656" Y="21.546416015625" />
                  <Point X="0.626788452148" Y="21.57618359375" />
                  <Point X="0.620407653809" Y="21.58679296875" />
                  <Point X="0.47206829834" Y="21.800521484375" />
                  <Point X="0.456679870605" Y="21.822693359375" />
                  <Point X="0.446668640137" Y="21.83483203125" />
                  <Point X="0.424785705566" Y="21.85728515625" />
                  <Point X="0.412913543701" Y="21.867599609375" />
                  <Point X="0.386658416748" Y="21.88684375" />
                  <Point X="0.373244903564" Y="21.895060546875" />
                  <Point X="0.345243011475" Y="21.909169921875" />
                  <Point X="0.330654510498" Y="21.9150625" />
                  <Point X="0.101108062744" Y="21.9863046875" />
                  <Point X="0.077295249939" Y="21.9936953125" />
                  <Point X="0.063377311707" Y="21.996890625" />
                  <Point X="0.035218029022" Y="22.00116015625" />
                  <Point X="0.020976835251" Y="22.002234375" />
                  <Point X="-0.008664813995" Y="22.002234375" />
                  <Point X="-0.022906009674" Y="22.00116015625" />
                  <Point X="-0.051065292358" Y="21.996890625" />
                  <Point X="-0.064983230591" Y="21.9936953125" />
                  <Point X="-0.294529663086" Y="21.922453125" />
                  <Point X="-0.318342468262" Y="21.9150625" />
                  <Point X="-0.332931152344" Y="21.909169921875" />
                  <Point X="-0.360933044434" Y="21.895060546875" />
                  <Point X="-0.374346252441" Y="21.88684375" />
                  <Point X="-0.400601379395" Y="21.867599609375" />
                  <Point X="-0.412473083496" Y="21.85728515625" />
                  <Point X="-0.434356323242" Y="21.83483203125" />
                  <Point X="-0.444368011475" Y="21.822693359375" />
                  <Point X="-0.592707214355" Y="21.60896484375" />
                  <Point X="-0.608095581055" Y="21.58679296875" />
                  <Point X="-0.612471191406" Y="21.579869140625" />
                  <Point X="-0.625977172852" Y="21.554736328125" />
                  <Point X="-0.638778198242" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.680403015137" Y="21.371552734375" />
                  <Point X="-0.985425231934" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764859587828" Y="25.708829670302" />
                  <Point X="4.607417608799" Y="26.098512242774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068925800719" Y="27.431326237625" />
                  <Point X="3.930077898916" Y="27.774986854002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.672407581772" Y="25.684057034622" />
                  <Point X="4.510131604316" Y="26.085704173047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.990712672925" Y="27.371311141548" />
                  <Point X="3.846996929639" Y="27.727020088385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.579955575715" Y="25.659284398942" />
                  <Point X="4.412845599834" Y="26.072896103321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.912499545132" Y="27.311296045471" />
                  <Point X="3.763915960363" Y="27.679053322768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.487503569658" Y="25.634511763262" />
                  <Point X="4.315559595351" Y="26.060088033594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.834286417339" Y="27.251280949394" />
                  <Point X="3.680834991086" Y="27.631086557151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.395051563602" Y="25.609739127582" />
                  <Point X="4.218273590869" Y="26.047279963868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756073289546" Y="27.191265853317" />
                  <Point X="3.597754021809" Y="27.583119791534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.302599557545" Y="25.584966491902" />
                  <Point X="4.120987586386" Y="26.034471894141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.677860161752" Y="27.13125075724" />
                  <Point X="3.514673052533" Y="27.535153025917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775483359226" Y="24.160938630725" />
                  <Point X="4.746112054346" Y="24.233635161301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.210147551489" Y="25.560193856222" />
                  <Point X="4.023701581903" Y="26.021663824415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.599647033959" Y="27.071235661163" />
                  <Point X="3.431592083256" Y="27.4871862603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.74305245003" Y="23.987608567279" />
                  <Point X="4.631212441529" Y="24.264422302005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.117695545432" Y="25.535421220542" />
                  <Point X="3.926415577421" Y="26.008855754688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.521433906166" Y="27.011220565086" />
                  <Point X="3.348511113979" Y="27.439219494683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.782341942339" Y="28.840537368218" />
                  <Point X="2.733257776721" Y="28.962024941251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.664210740425" Y="23.92914926578" />
                  <Point X="4.516312828712" Y="24.295209442709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.025243539376" Y="25.510648584862" />
                  <Point X="3.829129572938" Y="25.996047684962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.443220778372" Y="26.951205469009" />
                  <Point X="3.265430144703" Y="27.391252729066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.722063590626" Y="28.736132143646" />
                  <Point X="2.59220376066" Y="29.057546501583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.555993713128" Y="23.943396426916" />
                  <Point X="4.401413215895" Y="24.325996583412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932791533319" Y="25.485875949182" />
                  <Point X="3.731843568456" Y="25.983239615235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365007650579" Y="26.891190372932" />
                  <Point X="3.182349175426" Y="27.343285963449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661785238913" Y="28.631726919074" />
                  <Point X="2.458200459136" Y="29.135616931057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.447776685831" Y="23.957643588052" />
                  <Point X="4.286513603078" Y="24.356783724116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840339527262" Y="25.461103313502" />
                  <Point X="3.634557563973" Y="25.970431545509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.286794522786" Y="26.831175276856" />
                  <Point X="3.099268206149" Y="27.295319197832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6015068872" Y="28.527321694502" />
                  <Point X="2.324197157612" Y="29.213687360531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.339559658534" Y="23.971890749187" />
                  <Point X="4.17161399026" Y="24.38757086482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747887521206" Y="25.436330677821" />
                  <Point X="3.537271277501" Y="25.957624173731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.208581394993" Y="26.771160180779" />
                  <Point X="3.016187236873" Y="27.247352432215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541228535487" Y="28.42291646993" />
                  <Point X="2.193642940846" Y="29.283221005664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.231342631237" Y="23.986137910323" />
                  <Point X="4.056714377443" Y="24.418358005524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.656812624549" Y="25.40814957677" />
                  <Point X="3.436565237088" Y="25.953280989975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132998448525" Y="26.704635157481" />
                  <Point X="2.933106267596" Y="27.199385666598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.480950183775" Y="28.318511245357" />
                  <Point X="2.067311752001" Y="29.342302289907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12312560394" Y="24.000385071459" />
                  <Point X="3.941814764626" Y="24.449145146228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.572979684899" Y="25.362044003139" />
                  <Point X="3.327911514631" Y="25.968609009559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.066261205553" Y="26.616216249752" />
                  <Point X="2.850025298319" Y="27.151418900981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420671832062" Y="28.214106020785" />
                  <Point X="1.940980563157" Y="29.40138357415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.014908576644" Y="24.014632232594" />
                  <Point X="3.826915151809" Y="24.479932286931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.493349240702" Y="25.305536888259" />
                  <Point X="3.20274547633" Y="26.02480644501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015158674483" Y="26.489100072135" />
                  <Point X="2.766944322366" Y="27.10345215189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.360393480349" Y="28.109700796213" />
                  <Point X="1.816816445113" Y="29.455101169942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.906691549347" Y="24.02887939373" />
                  <Point X="3.712015716038" Y="24.510718989429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424409269865" Y="25.22256992331" />
                  <Point X="2.683863293796" Y="27.055485533029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.300115128636" Y="28.005295571641" />
                  <Point X="1.696762645218" Y="29.498645371325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.79847452205" Y="24.043126554866" />
                  <Point X="3.586606488557" Y="24.567518339222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369367434411" Y="25.105203866189" />
                  <Point X="2.599199907945" Y="27.011435385871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239836776923" Y="27.900890347069" />
                  <Point X="1.576708845322" Y="29.542189572707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690257494753" Y="24.057373716001" />
                  <Point X="3.424826156983" Y="24.7143393306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362315857766" Y="24.869057750395" />
                  <Point X="2.507426753137" Y="26.984982534392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.17955842521" Y="27.796485122496" />
                  <Point X="1.456655045426" Y="29.585733774089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.582040467456" Y="24.071620877137" />
                  <Point X="2.408537507072" Y="26.976142626829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.119280073497" Y="27.692079897924" />
                  <Point X="1.342953947446" Y="29.613554486476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.473823440159" Y="24.085868038273" />
                  <Point X="2.299112381285" Y="26.993379936654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.059388309802" Y="27.58671783443" />
                  <Point X="1.229418376862" Y="29.64096550418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.073529952075" Y="22.34794295428" />
                  <Point X="4.054475872045" Y="22.395103457264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.369924779572" Y="24.089426866736" />
                  <Point X="2.16630771504" Y="27.068483639706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.016482753366" Y="27.439313432661" />
                  <Point X="1.115882806279" Y="29.668376521884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009856360509" Y="22.251941243232" />
                  <Point X="3.90597745575" Y="22.509050554746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.275270301192" Y="24.070105541349" />
                  <Point X="1.002347235695" Y="29.695787539588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.885261137333" Y="22.306725861671" />
                  <Point X="3.757479039455" Y="22.622997652229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.181215875176" Y="24.049299034244" />
                  <Point X="0.888811665111" Y="29.723198557292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751628565202" Y="22.383878703697" />
                  <Point X="3.60898062316" Y="22.736944749711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.09384003914" Y="24.011962436881" />
                  <Point X="0.778308696024" Y="29.743103622901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617995993071" Y="22.461031545724" />
                  <Point X="3.460482206865" Y="22.850891847194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019352497565" Y="23.942726191334" />
                  <Point X="0.671320866219" Y="29.754308413483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48436342094" Y="22.538184387751" />
                  <Point X="3.31198379057" Y="22.964838944676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950398189768" Y="23.859794711607" />
                  <Point X="0.564333036415" Y="29.765513204065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.350730848809" Y="22.615337229777" />
                  <Point X="3.163485379356" Y="23.078786029582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.888814797415" Y="23.758619575966" />
                  <Point X="0.45734520661" Y="29.776717994647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.217098276678" Y="22.692490071804" />
                  <Point X="3.006829124962" Y="23.212924484896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.862826588622" Y="23.569343269451" />
                  <Point X="0.367561824795" Y="29.74534028219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.083465704548" Y="22.769642913831" />
                  <Point X="0.326705741213" Y="29.592863257105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.949833132417" Y="22.846795755857" />
                  <Point X="0.28584965763" Y="29.44038623202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818148425291" Y="22.919127462818" />
                  <Point X="0.244993701099" Y="29.28790889247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704990955091" Y="22.945602649232" />
                  <Point X="0.18934908263" Y="29.172034775663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.594465555723" Y="22.965563231734" />
                  <Point X="0.111796959081" Y="29.110383636674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489005919608" Y="22.972985610207" />
                  <Point X="0.019546044853" Y="29.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.397035482011" Y="22.947021050762" />
                  <Point X="-0.091774091683" Y="29.10704090727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.312540952569" Y="22.902552969328" />
                  <Point X="-0.462558345309" Y="29.77116475843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228182842178" Y="22.857747238894" />
                  <Point X="-0.56039294948" Y="29.75971452058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150806502904" Y="22.795661018553" />
                  <Point X="-0.658227553651" Y="29.74826428273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.089232904334" Y="22.694461642447" />
                  <Point X="-0.756062157822" Y="29.73681404488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.64392353037" Y="21.067954785789" />
                  <Point X="2.558304021395" Y="21.279870506851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.031416112949" Y="22.58396384227" />
                  <Point X="-0.853896761993" Y="29.72536380703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.427516484107" Y="21.34998164054" />
                  <Point X="2.21700307467" Y="21.871020612704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007789441367" Y="22.388842526048" />
                  <Point X="-0.95051270294" Y="29.710897271856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.211109437843" Y="21.632008495291" />
                  <Point X="-1.042548655947" Y="29.685094868743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.99470239158" Y="21.914035350042" />
                  <Point X="-1.13458413911" Y="29.659291302726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.79049168937" Y="22.165875193966" />
                  <Point X="-1.226619622274" Y="29.633487736709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.650754699786" Y="22.258136999379" />
                  <Point X="-1.318655105437" Y="29.607684170692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.516678106484" Y="22.336388832368" />
                  <Point X="-1.410690588601" Y="29.581880604675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.407350705822" Y="22.353384264023" />
                  <Point X="-1.466642762088" Y="29.46676771325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.30856249778" Y="22.344294278579" />
                  <Point X="-1.492401301421" Y="29.276922954875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.209774289737" Y="22.335204293134" />
                  <Point X="-1.54735501486" Y="29.159338788112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.114959229419" Y="22.316280421991" />
                  <Point X="-1.624509656438" Y="29.096703846718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.032085664078" Y="22.267800313619" />
                  <Point X="-1.711694245741" Y="29.05889389708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.955389791568" Y="22.204029878937" />
                  <Point X="-1.802995224643" Y="29.031272369222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.878693957192" Y="22.140259349871" />
                  <Point X="-1.909822014932" Y="29.042078573017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.809406796552" Y="22.058151709839" />
                  <Point X="-2.037431678578" Y="29.104324193433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76527594731" Y="21.913780014186" />
                  <Point X="-2.203645193769" Y="29.262117699301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.7294357907" Y="21.748888134193" />
                  <Point X="-2.287322906308" Y="29.215627925085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.756720173782" Y="21.42775753588" />
                  <Point X="-2.371000618846" Y="29.16913815087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.806244941906" Y="21.051580052935" />
                  <Point X="0.72372230454" Y="21.255830747789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.547652149009" Y="21.691619675022" />
                  <Point X="-2.454678331384" Y="29.122648376655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360376101111" Y="21.901544778692" />
                  <Point X="-2.538356043922" Y="29.076158602439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241242009122" Y="21.942812623124" />
                  <Point X="-2.62203375646" Y="29.029668828224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.124091220512" Y="21.979171619439" />
                  <Point X="-2.701977223324" Y="28.973936471631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.012312461871" Y="22.002234375" />
                  <Point X="-2.780205821267" Y="28.91395966552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.084278789027" Y="21.987706729811" />
                  <Point X="-2.858434419211" Y="28.853982859409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.175323182129" Y="21.959450129812" />
                  <Point X="-2.936663017154" Y="28.794006053297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.266367575231" Y="21.931193529813" />
                  <Point X="-2.75226187319" Y="28.083997825674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.865260469237" Y="28.363679165203" />
                  <Point X="-3.014891615098" Y="28.734029247186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.355363531123" Y="21.897866869805" />
                  <Point X="-2.781549856395" Y="27.902888747423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.43294264226" Y="21.836282527437" />
                  <Point X="-2.850934223829" Y="27.821021702649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.498437736097" Y="21.744789192713" />
                  <Point X="-2.927443598855" Y="27.756789670498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.563199253007" Y="21.651480191383" />
                  <Point X="-3.017457630516" Y="27.725982836443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.626278568134" Y="21.554007594534" />
                  <Point X="-3.120797038017" Y="27.728157464944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.670282696676" Y="21.40932225414" />
                  <Point X="-3.233099942087" Y="27.752517525966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.711138775601" Y="21.256845217527" />
                  <Point X="-3.365533938959" Y="27.826703790127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.751994845678" Y="21.104368159015" />
                  <Point X="-3.499166416178" Y="27.903856397238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.792850915755" Y="20.951891100503" />
                  <Point X="-3.632798893397" Y="27.98100900435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.833706985833" Y="20.799414041991" />
                  <Point X="-3.746571351415" Y="28.00900633903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.87456305591" Y="20.646936983478" />
                  <Point X="-3.814009934665" Y="27.922323309401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915419125987" Y="20.494459924966" />
                  <Point X="-3.881448517914" Y="27.835640279772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.956275196064" Y="20.341982866454" />
                  <Point X="-3.429158024703" Y="26.462582645656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557409295693" Y="26.780015680419" />
                  <Point X="-3.948887101164" Y="27.748957250143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017280567935" Y="20.239377079916" />
                  <Point X="-1.123244986736" Y="20.501648219821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.322077716367" Y="20.99377649496" />
                  <Point X="-3.481367116925" Y="26.338205303002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.705907636129" Y="26.893962590143" />
                  <Point X="-4.014892585554" Y="27.658727176369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130456417208" Y="20.265897756133" />
                  <Point X="-1.138869806168" Y="20.286721624542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.480764767835" Y="21.132941349411" />
                  <Point X="-3.553611967221" Y="26.263418201754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854405751312" Y="27.007908942347" />
                  <Point X="-4.075441590639" Y="27.554991842399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.609852361501" Y="21.198844974991" />
                  <Point X="-3.640689357208" Y="26.225342924498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002903866495" Y="27.12185529455" />
                  <Point X="-4.13599046309" Y="27.451256180148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.716435761118" Y="21.209048765734" />
                  <Point X="-3.734875758525" Y="26.204863067724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.151401981678" Y="27.235801646754" />
                  <Point X="-4.19653933554" Y="27.347520517897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.821683721648" Y="21.215947228749" />
                  <Point X="-2.31344330861" Y="22.433094917478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.352632429282" Y="22.530091394851" />
                  <Point X="-3.306346862788" Y="24.890617451135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438584059435" Y="25.217915998088" />
                  <Point X="-3.840982934831" Y="26.213888164411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.926931551514" Y="21.222845368358" />
                  <Point X="-2.347899349619" Y="22.264777231159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.500246930065" Y="22.641850724671" />
                  <Point X="-3.364615460822" Y="24.781237911654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.559382761914" Y="25.263303898063" />
                  <Point X="-3.949199789905" Y="26.228134899281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.025555793538" Y="21.213349552777" />
                  <Point X="-2.408177677737" Y="22.160371948186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.605028886775" Y="22.647595787756" />
                  <Point X="-2.966667446891" Y="23.542682633587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.10006098623" Y="23.872843229135" />
                  <Point X="-3.44142719563" Y="24.717754246222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.674282402037" Y="25.294091106351" />
                  <Point X="-4.05741664498" Y="26.242381634151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.111813444682" Y="21.173245350687" />
                  <Point X="-2.468456005854" Y="22.055966665212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.694865122408" Y="22.616348893088" />
                  <Point X="-3.029380106845" Y="23.444302533339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.213506974832" Y="23.900032523656" />
                  <Point X="-3.529500166573" Y="24.682143118302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.789182055568" Y="25.324878347823" />
                  <Point X="-4.165633500054" Y="26.25662836902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.192493784011" Y="21.119336817446" />
                  <Point X="-2.528734333972" Y="21.951561382239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777945918596" Y="22.568381699062" />
                  <Point X="-3.106419758337" Y="23.381382981497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.312261635622" Y="23.890859505847" />
                  <Point X="-3.62195223765" Y="24.657370643552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.904081709098" Y="25.355665589296" />
                  <Point X="-4.273850355128" Y="26.27087510389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.27317412334" Y="21.065428284206" />
                  <Point X="-2.589012662089" Y="21.847156099266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.861026807098" Y="22.520414733521" />
                  <Point X="-3.184632951928" Y="23.321368048274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.409547846978" Y="23.878051948149" />
                  <Point X="-3.714404308726" Y="24.632598168802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.018981362629" Y="25.386452830768" />
                  <Point X="-4.382067210203" Y="26.28512183876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353845667636" Y="21.011497982496" />
                  <Point X="-2.649290990206" Y="21.742750816292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.944107747574" Y="22.472447896621" />
                  <Point X="-3.262846145519" Y="23.261353115052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506833929424" Y="23.865244071389" />
                  <Point X="-3.806856379803" Y="24.607825694052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133881016159" Y="25.417240072241" />
                  <Point X="-4.490284065277" Y="26.29936857363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.426435398274" Y="20.937564490047" />
                  <Point X="-2.709569318324" Y="21.638345533319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027188688051" Y="22.42448105972" />
                  <Point X="-3.34105933911" Y="23.20133818183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.60412001187" Y="23.852436194629" />
                  <Point X="-3.899308450879" Y="24.583053219302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.248780669689" Y="25.448027313714" />
                  <Point X="-4.598500920351" Y="26.3136153085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.493555100024" Y="20.850092201013" />
                  <Point X="-2.769847646441" Y="21.533940250345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.110269628527" Y="22.37651422282" />
                  <Point X="-3.4192725327" Y="23.141323248607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701406094316" Y="23.839628317868" />
                  <Point X="-3.991760521956" Y="24.558280744552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.36368032322" Y="25.478814555186" />
                  <Point X="-4.661383735243" Y="26.215656356503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.62020765187" Y="20.909968886595" />
                  <Point X="-2.830125974558" Y="21.429534967372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193350569003" Y="22.328547385919" />
                  <Point X="-3.497485726291" Y="23.081308315385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798692176761" Y="23.826820441108" />
                  <Point X="-4.084212593032" Y="24.533508269802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.47857997675" Y="25.509601796659" />
                  <Point X="-4.702516637457" Y="26.063864481573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757669533649" Y="20.99659960259" />
                  <Point X="-2.890404302676" Y="21.325129684398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.276431509479" Y="22.280580549019" />
                  <Point X="-3.575698919882" Y="23.021293382162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.895978259207" Y="23.814012564348" />
                  <Point X="-4.176664664109" Y="24.508735795052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.593479630281" Y="25.540389038131" />
                  <Point X="-4.739556573046" Y="25.901942158757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.906397840627" Y="21.11111569948" />
                  <Point X="-2.950682630793" Y="21.220724401425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359512449956" Y="22.232613712119" />
                  <Point X="-3.653912113473" Y="22.96127844894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.993264341653" Y="23.801204687588" />
                  <Point X="-4.269116735185" Y="24.483963320302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708379283811" Y="25.571176279604" />
                  <Point X="-4.767023164445" Y="25.716324977594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442593390432" Y="22.184646875218" />
                  <Point X="-3.732125307063" Y="22.901263515717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.090550424099" Y="23.788396810827" />
                  <Point X="-4.361568806262" Y="24.459190845552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.525674330908" Y="22.136680038318" />
                  <Point X="-3.810338500654" Y="22.841248582495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.187836506545" Y="23.775588934067" />
                  <Point X="-4.454020877338" Y="24.434418370802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608755271384" Y="22.088713201417" />
                  <Point X="-3.888551694245" Y="22.781233649272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.285122588991" Y="23.762781057307" />
                  <Point X="-4.546472948415" Y="24.409645896052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.69183621186" Y="22.040746364517" />
                  <Point X="-3.966764887836" Y="22.72121871605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.382408671437" Y="23.749973180546" />
                  <Point X="-4.638925019491" Y="24.384873421302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.774917152337" Y="21.992779527616" />
                  <Point X="-4.044978081426" Y="22.661203782827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.479694753883" Y="23.737165303786" />
                  <Point X="-4.731377090568" Y="24.360100946552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963705290765" Y="22.206447186678" />
                  <Point X="-4.123191275017" Y="22.601188849605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.576980836329" Y="23.724357427026" />
                  <Point X="-4.763752932294" Y="24.186634586334" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000163818359" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.834470397949" Y="20.10841015625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.315979553223" Y="21.692185546875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.7336015625" />
                  <Point X="0.044789520264" Y="21.80484375" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.238211135864" Y="21.7409921875" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.436618408203" Y="21.50062890625" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.49687701416" Y="21.322376953125" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.073546142578" Y="20.056751953125" />
                  <Point X="-1.10023425293" Y="20.06193359375" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.364521850586" Y="20.74093359375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.597619873047" Y="20.982708984375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.6492421875" Y="21.014236328125" />
                  <Point X="-1.929733764648" Y="21.03262109375" />
                  <Point X="-1.958831420898" Y="21.03452734375" />
                  <Point X="-1.989879272461" Y="21.02620703125" />
                  <Point X="-2.223599853516" Y="20.870041015625" />
                  <Point X="-2.247845703125" Y="20.85383984375" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.280871582031" Y="20.815158203125" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.816703613281" Y="20.80816015625" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.224782714844" Y="21.116466796875" />
                  <Point X="-3.228580810547" Y="21.119390625" />
                  <Point X="-3.180500732422" Y="21.20266796875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513980957031" Y="22.43123828125" />
                  <Point X="-2.529695068359" Y="22.446951171875" />
                  <Point X="-2.531325683594" Y="22.44858203125" />
                  <Point X="-2.560155517578" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.712871826172" Y="22.386560546875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.130788574219" Y="22.11225390625" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.426211425781" Y="22.59641015625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.343885253906" Y="22.671333984375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822998047" Y="23.60398046875" />
                  <Point X="-3.138842529297" Y="23.63093359375" />
                  <Point X="-3.138118408203" Y="23.633728515625" />
                  <Point X="-3.140325683594" Y="23.66540234375" />
                  <Point X="-3.161159912109" Y="23.689361328125" />
                  <Point X="-3.185153564453" Y="23.703482421875" />
                  <Point X="-3.187642578125" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.372518066406" Y="23.691287109375" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.915301757812" Y="23.941470703125" />
                  <Point X="-4.927392089844" Y="23.988802734375" />
                  <Point X="-4.997375" Y="24.4781171875" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.9017734375" Y="24.511146484375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541894042969" Y="24.878576171875" />
                  <Point X="-3.516749511719" Y="24.89602734375" />
                  <Point X="-3.514141113281" Y="24.897837890625" />
                  <Point X="-3.494899414062" Y="24.92408984375" />
                  <Point X="-3.486517822266" Y="24.951095703125" />
                  <Point X="-3.4856484375" Y="24.953896484375" />
                  <Point X="-3.485647949219" Y="24.9835390625" />
                  <Point X="-3.494027099609" Y="25.010537109375" />
                  <Point X="-3.494896484375" Y="25.01333984375" />
                  <Point X="-3.514141113281" Y="25.0396015625" />
                  <Point X="-3.539285644531" Y="25.057052734375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.696920410156" Y="25.103453125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.925437011719" Y="25.94376171875" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.776774414062" Y="26.516275390625" />
                  <Point X="-4.773516601562" Y="26.528298828125" />
                  <Point X="-4.716900390625" Y="26.52084375" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704345703" Y="26.3958671875" />
                  <Point X="-3.676051513672" Y="26.4134140625" />
                  <Point X="-3.670278320312" Y="26.415234375" />
                  <Point X="-3.651535644531" Y="26.4260546875" />
                  <Point X="-3.639121337891" Y="26.443783203125" />
                  <Point X="-3.616790527344" Y="26.4976953125" />
                  <Point X="-3.614473876953" Y="26.503287109375" />
                  <Point X="-3.610714111328" Y="26.524603515625" />
                  <Point X="-3.616316650391" Y="26.54551171875" />
                  <Point X="-3.643261230469" Y="26.597271484375" />
                  <Point X="-3.646056396484" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.7399609375" Y="26.6806015625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.190291503906" Y="27.735134765625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.786850585938" Y="28.26665625" />
                  <Point X="-3.774671142578" Y="28.282310546875" />
                  <Point X="-3.750994140625" Y="28.268640625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138512695312" Y="27.92043359375" />
                  <Point X="-3.06100390625" Y="27.91365234375" />
                  <Point X="-3.052963378906" Y="27.91294921875" />
                  <Point X="-3.031506103516" Y="27.915775390625" />
                  <Point X="-3.013253173828" Y="27.927404296875" />
                  <Point X="-2.958236816406" Y="27.982419921875" />
                  <Point X="-2.952529541016" Y="27.988126953125" />
                  <Point X="-2.940899658203" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.94485546875" Y="28.105349609375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-2.987514648438" Y="28.195427734375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.805606689453" Y="29.13390234375" />
                  <Point X="-2.752872070313" Y="29.174333984375" />
                  <Point X="-2.165153808594" Y="29.500857421875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246826172" Y="29.273662109375" />
                  <Point X="-1.864985229492" Y="29.228755859375" />
                  <Point X="-1.856036499023" Y="29.224095703125" />
                  <Point X="-1.835126464844" Y="29.218490234375" />
                  <Point X="-1.813807373047" Y="29.22225" />
                  <Point X="-1.723954589844" Y="29.25946875" />
                  <Point X="-1.714633422852" Y="29.263330078125" />
                  <Point X="-1.696904418945" Y="29.27574609375" />
                  <Point X="-1.686083007812" Y="29.29448828125" />
                  <Point X="-1.656837768555" Y="29.3872421875" />
                  <Point X="-1.653803710938" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.655947631836" Y="29.44903515625" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.036358032227" Y="29.88415625" />
                  <Point X="-0.968084289551" Y="29.903296875" />
                  <Point X="-0.255593933105" Y="29.98668359375" />
                  <Point X="-0.224200088501" Y="29.990357421875" />
                  <Point X="-0.217222290039" Y="29.96431640625" />
                  <Point X="-0.168925888062" Y="29.9772578125" />
                  <Point X="-0.217222137451" Y="29.96431640625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282114029" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594032288" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.072614936829" Y="29.3786875" />
                  <Point X="0.236648391724" Y="29.990869140625" />
                  <Point X="0.800599304199" Y="29.931806640625" />
                  <Point X="0.860208557129" Y="29.925564453125" />
                  <Point X="1.449673095703" Y="29.78325" />
                  <Point X="1.508457641602" Y="29.769056640625" />
                  <Point X="1.892978271484" Y="29.629587890625" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="2.302039306641" Y="29.442279296875" />
                  <Point X="2.338683349609" Y="29.425142578125" />
                  <Point X="2.697101318359" Y="29.216328125" />
                  <Point X="2.732532470703" Y="29.195685546875" />
                  <Point X="3.068740722656" Y="28.95659375" />
                  <Point X="3.010498535156" Y="28.85571484375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851806641" Y="27.491513671875" />
                  <Point X="2.205400146484" Y="27.4187734375" />
                  <Point X="2.202044433594" Y="27.392326171875" />
                  <Point X="2.209629150391" Y="27.329427734375" />
                  <Point X="2.210415771484" Y="27.32290234375" />
                  <Point X="2.218684570312" Y="27.30080859375" />
                  <Point X="2.257604736328" Y="27.24344921875" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.332298339844" Y="27.185283203125" />
                  <Point X="2.338240966797" Y="27.18125" />
                  <Point X="2.360334716797" Y="27.172978515625" />
                  <Point X="2.423234375" Y="27.16539453125" />
                  <Point X="2.448662597656" Y="27.1659453125" />
                  <Point X="2.521403076172" Y="27.1853984375" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.681300292969" Y="27.273400390625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.181580566406" Y="27.771080078125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.316408203125" Y="27.381734375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279373535156" Y="26.5838359375" />
                  <Point X="3.227022216797" Y="26.5155390625" />
                  <Point X="3.213119140625" Y="26.491498046875" />
                  <Point X="3.193618164062" Y="26.421767578125" />
                  <Point X="3.191595214844" Y="26.414533203125" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.206787841797" Y="26.313380859375" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.2591875" Y="26.2217734375" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947998047" Y="26.1988203125" />
                  <Point X="3.344040039062" Y="26.1633046875" />
                  <Point X="3.344039794922" Y="26.1633046875" />
                  <Point X="3.368563964844" Y="26.153619140625" />
                  <Point X="3.453874755859" Y="26.142345703125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.609047363281" Y="26.158712890625" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.9301640625" Y="25.988451171875" />
                  <Point X="4.939189453125" Y="25.951380859375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.919039550781" Y="25.5534375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088378906" Y="25.232818359375" />
                  <Point X="3.645273193359" Y="25.184373046875" />
                  <Point X="3.622262695312" Y="25.166923828125" />
                  <Point X="3.571973632812" Y="25.10284375" />
                  <Point X="3.566756835938" Y="25.0961953125" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.540222167969" Y="24.987203125" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.555246337891" Y="24.87178515625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566756835938" Y="24.841244140625" />
                  <Point X="3.617045898438" Y="24.7771640625" />
                  <Point X="3.636577392578" Y="24.758091796875" />
                  <Point X="3.720392578125" Y="24.709646484375" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.863358154297" Y="24.66687109375" />
                  <Point X="4.998068359375" Y="24.362826171875" />
                  <Point X="4.953471191406" Y="24.067021484375" />
                  <Point X="4.948432128906" Y="24.033599609375" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="4.777982421875" Y="23.722533203125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.230336914062" Y="23.865904296875" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185446777344" Y="23.8453046875" />
                  <Point X="3.086017333984" Y="23.725720703125" />
                  <Point X="3.075702636719" Y="23.71331640625" />
                  <Point X="3.064357666016" Y="23.6859296875" />
                  <Point X="3.050106933594" Y="23.531064453125" />
                  <Point X="3.048628662109" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.147396728516" Y="23.34177734375" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.281854248047" Y="23.22744921875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.218334472656" Y="22.22083984375" />
                  <Point X="4.204133789063" Y="22.197859375" />
                  <Point X="4.0566875" Y="21.988361328125" />
                  <Point X="3.970062011719" Y="22.038375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.541560302734" Y="22.78204296875" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489075683594" Y="22.78075390625" />
                  <Point X="2.326430175781" Y="22.69515625" />
                  <Point X="2.309557617188" Y="22.686275390625" />
                  <Point X="2.288599609375" Y="22.665314453125" />
                  <Point X="2.203000488281" Y="22.502669921875" />
                  <Point X="2.194120605469" Y="22.485796875" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.224520751953" Y="22.25784375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.306958251953" Y="22.095212890625" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.852153076172" Y="20.821826171875" />
                  <Point X="2.835309082031" Y="20.809794921875" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="2.611561279297" Y="20.79801953125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670547729492" Y="22.019533203125" />
                  <Point X="1.477455200195" Y="22.143673828125" />
                  <Point X="1.457424194336" Y="22.15655078125" />
                  <Point X="1.425805541992" Y="22.16428125" />
                  <Point X="1.214626220703" Y="22.144849609375" />
                  <Point X="1.19271875" Y="22.142833984375" />
                  <Point X="1.165331420898" Y="22.131490234375" />
                  <Point X="1.002263977051" Y="21.995904296875" />
                  <Point X="0.985347717285" Y="21.981837890625" />
                  <Point X="0.96845690918" Y="21.95401171875" />
                  <Point X="0.919700561523" Y="21.7296953125" />
                  <Point X="0.914642700195" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.934579345703" Y="21.5324296875" />
                  <Point X="1.127642333984" Y="20.06597265625" />
                  <Point X="1.010291442871" Y="20.040248046875" />
                  <Point X="0.994323181152" Y="20.036748046875" />
                  <Point X="0.860200744629" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#208" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.175081857811" Y="5.008538895752" Z="2.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="-0.267562846851" Y="5.067625162536" Z="2.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.3" />
                  <Point X="-1.056011556553" Y="4.963581276655" Z="2.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.3" />
                  <Point X="-1.711675831429" Y="4.47379101261" Z="2.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.3" />
                  <Point X="-1.710679761189" Y="4.433558384292" Z="2.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.3" />
                  <Point X="-1.749246320335" Y="4.336943110796" Z="2.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.3" />
                  <Point X="-1.848048242248" Y="4.304383266851" Z="2.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.3" />
                  <Point X="-2.115494499671" Y="4.58540888818" Z="2.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.3" />
                  <Point X="-2.19559270463" Y="4.575844749492" Z="2.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.3" />
                  <Point X="-2.842184969013" Y="4.204863058547" Z="2.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.3" />
                  <Point X="-3.036971817282" Y="3.201709137485" Z="2.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.3" />
                  <Point X="-3.000821215918" Y="3.132272282341" Z="2.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.3" />
                  <Point X="-2.999747307384" Y="3.049056260502" Z="2.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.3" />
                  <Point X="-3.062804183266" Y="2.994743483255" Z="2.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.3" />
                  <Point X="-3.732150210812" Y="3.343222131464" Z="2.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.3" />
                  <Point X="-3.832469669063" Y="3.328638930847" Z="2.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.3" />
                  <Point X="-4.239629434203" Y="2.791619920246" Z="2.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.3" />
                  <Point X="-3.776554861353" Y="1.672214573506" Z="2.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.3" />
                  <Point X="-3.693767095323" Y="1.605464601615" Z="2.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.3" />
                  <Point X="-3.669139004608" Y="1.548111683173" Z="2.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.3" />
                  <Point X="-3.697243240715" Y="1.492379787339" Z="2.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.3" />
                  <Point X="-4.716530088512" Y="1.601697398357" Z="2.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.3" />
                  <Point X="-4.831189525224" Y="1.560634153161" Z="2.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.3" />
                  <Point X="-4.981054388172" Y="0.982360130902" Z="2.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.3" />
                  <Point X="-3.716017224558" Y="0.086436286216" Z="2.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.3" />
                  <Point X="-3.573952293499" Y="0.047258624225" Z="2.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.3" />
                  <Point X="-3.547938363747" Y="0.027005450117" Z="2.3" />
                  <Point X="-3.539556741714" Y="0" Z="2.3" />
                  <Point X="-3.540426237388" Y="-0.002801500944" Z="2.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.3" />
                  <Point X="-3.551416301624" Y="-0.031617358909" Z="2.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.3" />
                  <Point X="-4.920870564215" Y="-0.40927577054" Z="2.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.3" />
                  <Point X="-5.053027430509" Y="-0.497681215694" Z="2.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.3" />
                  <Point X="-4.969902039786" Y="-1.039624501403" Z="2.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.3" />
                  <Point X="-3.372147617408" Y="-1.327004652437" Z="2.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.3" />
                  <Point X="-3.216669838293" Y="-1.308328254216" Z="2.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.3" />
                  <Point X="-3.193400437328" Y="-1.32524570409" Z="2.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.3" />
                  <Point X="-4.380479141201" Y="-2.257718436053" Z="2.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.3" />
                  <Point X="-4.475310773542" Y="-2.397919649902" Z="2.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.3" />
                  <Point X="-4.176730078599" Y="-2.88674935967" Z="2.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.3" />
                  <Point X="-2.694027794091" Y="-2.62545906471" Z="2.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.3" />
                  <Point X="-2.571208942036" Y="-2.557121527095" Z="2.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.3" />
                  <Point X="-3.229957948458" Y="-3.741050588401" Z="2.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.3" />
                  <Point X="-3.26144253424" Y="-3.891869903608" Z="2.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.3" />
                  <Point X="-2.849181077579" Y="-4.203070643405" Z="2.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.3" />
                  <Point X="-2.247359792249" Y="-4.183999115925" Z="2.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.3" />
                  <Point X="-2.201976496723" Y="-4.140251642886" Z="2.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.3" />
                  <Point X="-1.939158277608" Y="-3.985991535096" Z="2.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.3" />
                  <Point X="-1.63674308558" Y="-4.023602585412" Z="2.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.3" />
                  <Point X="-1.419717115145" Y="-4.237540330011" Z="2.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.3" />
                  <Point X="-1.408566892278" Y="-4.845078105354" Z="2.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.3" />
                  <Point X="-1.385307009081" Y="-4.886653900794" Z="2.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.3" />
                  <Point X="-1.089230409292" Y="-4.961051746018" Z="2.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="-0.454736685573" Y="-3.659284449193" Z="2.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="-0.401698312223" Y="-3.496601230557" Z="2.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="-0.229546321777" Y="-3.275482075668" Z="2.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.3" />
                  <Point X="0.023812757584" Y="-3.21162996962" Z="2.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.3" />
                  <Point X="0.268747547209" Y="-3.305044525211" Z="2.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.3" />
                  <Point X="0.780018174828" Y="-4.873251580623" Z="2.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.3" />
                  <Point X="0.834618103204" Y="-5.010683750354" Z="2.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.3" />
                  <Point X="1.014838066144" Y="-4.977312709439" Z="2.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.3" />
                  <Point X="0.977995661709" Y="-3.429765071398" Z="2.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.3" />
                  <Point X="0.962403685218" Y="-3.249643380016" Z="2.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.3" />
                  <Point X="1.028076358918" Y="-3.011260629196" Z="2.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.3" />
                  <Point X="1.213051145272" Y="-2.8736595015" Z="2.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.3" />
                  <Point X="1.444261539678" Y="-2.867104724982" Z="2.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.3" />
                  <Point X="2.565737666781" Y="-4.201138307297" Z="2.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.3" />
                  <Point X="2.680395691756" Y="-4.314773651907" Z="2.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.3" />
                  <Point X="2.875059809325" Y="-4.187579713786" Z="2.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.3" />
                  <Point X="2.344103645653" Y="-2.848507481002" Z="2.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.3" />
                  <Point X="2.267568934327" Y="-2.701988675889" Z="2.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.3" />
                  <Point X="2.241091029694" Y="-2.489335613581" Z="2.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.3" />
                  <Point X="2.343562764791" Y="-2.317810280273" Z="2.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.3" />
                  <Point X="2.526518127624" Y="-2.235879075164" Z="2.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.3" />
                  <Point X="3.938906116105" Y="-2.973645854622" Z="2.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.3" />
                  <Point X="4.081525994341" Y="-3.023194805725" Z="2.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.3" />
                  <Point X="4.254712077643" Y="-2.774163917398" Z="2.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.3" />
                  <Point X="3.306136336338" Y="-1.701602942527" Z="2.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.3" />
                  <Point X="3.183298862798" Y="-1.59990356717" Z="2.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.3" />
                  <Point X="3.093740841843" Y="-1.44223707401" Z="2.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.3" />
                  <Point X="3.118306102067" Y="-1.274966915628" Z="2.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.3" />
                  <Point X="3.234800429135" Y="-1.151675056278" Z="2.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.3" />
                  <Point X="4.765300121862" Y="-1.295757757065" Z="2.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.3" />
                  <Point X="4.914942317134" Y="-1.279639006519" Z="2.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.3" />
                  <Point X="4.996755932335" Y="-0.909152639149" Z="2.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.3" />
                  <Point X="3.870143194204" Y="-0.25355147971" Z="2.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.3" />
                  <Point X="3.73925785497" Y="-0.215784860795" Z="2.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.3" />
                  <Point X="3.650225807876" Y="-0.160690642134" Z="2.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.3" />
                  <Point X="3.598197754771" Y="-0.087530283664" Z="2.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.3" />
                  <Point X="3.583173695653" Y="0.009080247515" Z="2.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.3" />
                  <Point X="3.605153630524" Y="0.103258096441" Z="2.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.3" />
                  <Point X="3.664137559383" Y="0.172363983447" Z="2.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.3" />
                  <Point X="4.925824623103" Y="0.536420453059" Z="2.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.3" />
                  <Point X="5.041821087001" Y="0.608944543603" Z="2.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.3" />
                  <Point X="4.972589305268" Y="1.031560581445" Z="2.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.3" />
                  <Point X="3.596365071641" Y="1.239565948829" Z="2.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.3" />
                  <Point X="3.454271412851" Y="1.223193718625" Z="2.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.3" />
                  <Point X="3.36241508817" Y="1.238153146198" Z="2.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.3" />
                  <Point X="3.294801710001" Y="1.280536432323" Z="2.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.3" />
                  <Point X="3.249600322321" Y="1.354764874685" Z="2.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.3" />
                  <Point X="3.235615030407" Y="1.439582957712" Z="2.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.3" />
                  <Point X="3.260546826895" Y="1.516398662658" Z="2.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.3" />
                  <Point X="4.340690402616" Y="2.373347800183" Z="2.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.3" />
                  <Point X="4.427656328182" Y="2.487642336438" Z="2.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.3" />
                  <Point X="4.216010020457" Y="2.831564161416" Z="2.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.3" />
                  <Point X="2.650144297997" Y="2.347981594241" Z="2.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.3" />
                  <Point X="2.50233196065" Y="2.264980879976" Z="2.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.3" />
                  <Point X="2.423066539097" Y="2.246315983013" Z="2.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.3" />
                  <Point X="2.354216517416" Y="2.257938029164" Z="2.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.3" />
                  <Point X="2.292820571071" Y="2.302808342966" Z="2.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.3" />
                  <Point X="2.253113719746" Y="2.366691904736" Z="2.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.3" />
                  <Point X="2.247547062681" Y="2.437137615232" Z="2.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.3" />
                  <Point X="3.047643758618" Y="3.861994783958" Z="2.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.3" />
                  <Point X="3.093368918636" Y="4.027334466271" Z="2.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.3" />
                  <Point X="2.716115440868" Y="4.290811073191" Z="2.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.3" />
                  <Point X="2.317064829412" Y="4.518851111919" Z="2.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.3" />
                  <Point X="1.903870687266" Y="4.707872974747" Z="2.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.3" />
                  <Point X="1.455252497482" Y="4.863133407318" Z="2.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.3" />
                  <Point X="0.799651051753" Y="5.012816077128" Z="2.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.3" />
                  <Point X="0.018162703742" Y="4.422908610199" Z="2.3" />
                  <Point X="0" Y="4.355124473572" Z="2.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>