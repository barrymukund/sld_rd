<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#199" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3017" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.886805969238" Y="20.280142578125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.54236315918" Y="21.532625" />
                  <Point X="0.41013885498" Y="21.723134765625" />
                  <Point X="0.378635528564" Y="21.768525390625" />
                  <Point X="0.356749481201" Y="21.790982421875" />
                  <Point X="0.330494750977" Y="21.810224609375" />
                  <Point X="0.302495483398" Y="21.824330078125" />
                  <Point X="0.097885955811" Y="21.88783203125" />
                  <Point X="0.049136341095" Y="21.902962890625" />
                  <Point X="0.020983621597" Y="21.907232421875" />
                  <Point X="-0.008657402039" Y="21.907234375" />
                  <Point X="-0.036823478699" Y="21.90296484375" />
                  <Point X="-0.241432693481" Y="21.8394609375" />
                  <Point X="-0.290182617188" Y="21.82433203125" />
                  <Point X="-0.318183258057" Y="21.810224609375" />
                  <Point X="-0.344439025879" Y="21.79098046875" />
                  <Point X="-0.366323120117" Y="21.7685234375" />
                  <Point X="-0.498547576904" Y="21.57801171875" />
                  <Point X="-0.53005090332" Y="21.532623046875" />
                  <Point X="-0.538187805176" Y="21.5184296875" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.628067321777" Y="21.1998203125" />
                  <Point X="-0.916584777832" Y="20.12305859375" />
                  <Point X="-1.025943725586" Y="20.14428515625" />
                  <Point X="-1.079322143555" Y="20.154646484375" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.265389892578" Y="20.729515625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937744141" Y="20.817033203125" />
                  <Point X="-1.304009643555" Y="20.84487109375" />
                  <Point X="-1.32364453125" Y="20.868796875" />
                  <Point X="-1.512022949219" Y="21.034" />
                  <Point X="-1.556905517578" Y="21.073361328125" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643027587891" Y="21.109033203125" />
                  <Point X="-1.893047607422" Y="21.125419921875" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983414550781" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.250987792969" Y="20.965998046875" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.378375732422" Y="20.844142578125" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.723448730469" Y="20.86215625" />
                  <Point X="-2.801726074219" Y="20.910623046875" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-3.02495703125" Y="21.282078125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414560791016" Y="22.444427734375" />
                  <Point X="-2.428778808594" Y="22.4732578125" />
                  <Point X="-2.446807617188" Y="22.498416015625" />
                  <Point X="-2.46415625" Y="22.515763671875" />
                  <Point X="-2.489316650391" Y="22.533791015625" />
                  <Point X="-2.518145263672" Y="22.548005859375" />
                  <Point X="-2.54776171875" Y="22.55698828125" />
                  <Point X="-2.578693847656" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-2.887281494141" Y="22.395560546875" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.021029785156" Y="22.12490625" />
                  <Point X="-4.082860839844" Y="22.206138671875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.157852050781" Y="22.6943359375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576416016" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.580166015625" />
                  <Point X="-3.047634033203" Y="23.604189453125" />
                  <Point X="-3.046151611328" Y="23.6099140625" />
                  <Point X="-3.043347412109" Y="23.64034765625" />
                  <Point X="-3.045557128906" Y="23.672017578125" />
                  <Point X="-3.05255859375" Y="23.70176171875" />
                  <Point X="-3.068640625" Y="23.7277421875" />
                  <Point X="-3.089472167969" Y="23.751697265625" />
                  <Point X="-3.112970458984" Y="23.77123046875" />
                  <Point X="-3.134357421875" Y="23.783818359375" />
                  <Point X="-3.139479248047" Y="23.78683203125" />
                  <Point X="-3.168729980469" Y="23.798046875" />
                  <Point X="-3.200611083984" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.545128662109" Y="23.7643828125" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.80989453125" Y="23.912669921875" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.731145996094" Y="24.458513671875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.517493896484" Y="24.785171875" />
                  <Point X="-3.487725585938" Y="24.80053125" />
                  <Point X="-3.465312744141" Y="24.816087890625" />
                  <Point X="-3.459979980469" Y="24.8197890625" />
                  <Point X="-3.437531982422" Y="24.8416640625" />
                  <Point X="-3.418282470703" Y="24.867921875" />
                  <Point X="-3.404168945312" Y="24.895931640625" />
                  <Point X="-3.396697998047" Y="24.92000390625" />
                  <Point X="-3.39491796875" Y="24.92573828125" />
                  <Point X="-3.390648681641" Y="24.953892578125" />
                  <Point X="-3.390647216797" Y="24.983533203125" />
                  <Point X="-3.394916503906" Y="25.0116953125" />
                  <Point X="-3.402387451172" Y="25.035767578125" />
                  <Point X="-3.404167480469" Y="25.04150390625" />
                  <Point X="-3.418276367188" Y="25.0695078125" />
                  <Point X="-3.437522460938" Y="25.095765625" />
                  <Point X="-3.459981689453" Y="25.117650390625" />
                  <Point X="-3.482390136719" Y="25.133201171875" />
                  <Point X="-3.492541015625" Y="25.13934375" />
                  <Point X="-3.513452148438" Y="25.150275390625" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.818372802734" Y="25.23434765625" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.840072265625" Y="25.87165625" />
                  <Point X="-4.824487792969" Y="25.9769765625" />
                  <Point X="-4.703551757812" Y="26.423267578125" />
                  <Point X="-4.624615722656" Y="26.412875" />
                  <Point X="-3.765666748047" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423339844" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.65353125" Y="26.320904296875" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.622783935547" Y="26.332958984375" />
                  <Point X="-3.604039794922" Y="26.343779296875" />
                  <Point X="-3.587354736328" Y="26.35601171875" />
                  <Point X="-3.573713623047" Y="26.37156640625" />
                  <Point X="-3.561299316406" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.531446533203" Y="26.455484375" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.58937890625" />
                  <Point X="-3.556067626953" Y="26.635515625" />
                  <Point X="-3.561789794922" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135498047" Y="26.69458984375" />
                  <Point X="-3.765896972656" Y="26.82025" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.141707519531" Y="27.629916015625" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.752630371094" Y="28.1559296875" />
                  <Point X="-3.734200195312" Y="28.149248046875" />
                  <Point X="-3.206657470703" Y="27.844671875" />
                  <Point X="-3.1877265625" Y="27.836341796875" />
                  <Point X="-3.167082275391" Y="27.82983203125" />
                  <Point X="-3.146793212891" Y="27.825794921875" />
                  <Point X="-3.077704589844" Y="27.81975" />
                  <Point X="-3.061243896484" Y="27.818310546875" />
                  <Point X="-3.040557861328" Y="27.81876171875" />
                  <Point X="-3.019100585938" Y="27.821587890625" />
                  <Point X="-2.999010742188" Y="27.826505859375" />
                  <Point X="-2.9804609375" Y="27.835654296875" />
                  <Point X="-2.962208007812" Y="27.847283203125" />
                  <Point X="-2.946078369141" Y="27.860228515625" />
                  <Point X="-2.897038818359" Y="27.909267578125" />
                  <Point X="-2.885354736328" Y="27.920951171875" />
                  <Point X="-2.872409179688" Y="27.937080078125" />
                  <Point X="-2.860779296875" Y="27.955333984375" />
                  <Point X="-2.851629638672" Y="27.97388671875" />
                  <Point X="-2.846712646484" Y="27.99398046875" />
                  <Point X="-2.843887207031" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.849480224609" Y="28.105208984375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.942362792969" Y="28.30722265625" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.806085693359" Y="29.013826171875" />
                  <Point X="-2.700619873047" Y="29.094685546875" />
                  <Point X="-2.183213378906" Y="29.382146484375" />
                  <Point X="-2.167035888672" Y="29.391134765625" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028895507812" Y="29.214802734375" />
                  <Point X="-2.012314941406" Y="29.200888671875" />
                  <Point X="-1.995114379883" Y="29.18939453125" />
                  <Point X="-1.918219116211" Y="29.14936328125" />
                  <Point X="-1.89989831543" Y="29.139826171875" />
                  <Point X="-1.880616821289" Y="29.132328125" />
                  <Point X="-1.859710205078" Y="29.1267265625" />
                  <Point X="-1.839264038086" Y="29.123580078125" />
                  <Point X="-1.818621582031" Y="29.12493359375" />
                  <Point X="-1.797306396484" Y="29.128693359375" />
                  <Point X="-1.777452880859" Y="29.134482421875" />
                  <Point X="-1.697361328125" Y="29.167658203125" />
                  <Point X="-1.678279052734" Y="29.1755625" />
                  <Point X="-1.660145996094" Y="29.185509765625" />
                  <Point X="-1.642416381836" Y="29.197923828125" />
                  <Point X="-1.626864135742" Y="29.2115625" />
                  <Point X="-1.61463269043" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.569411987305" Y="29.348599609375" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.56598034668" Y="29.4934921875" />
                  <Point X="-1.584201904297" Y="29.6318984375" />
                  <Point X="-1.086161254883" Y="29.77153125" />
                  <Point X="-0.94962487793" Y="29.80980859375" />
                  <Point X="-0.322378112793" Y="29.883220703125" />
                  <Point X="-0.294711700439" Y="29.886458984375" />
                  <Point X="-0.289965118408" Y="29.868744140625" />
                  <Point X="-0.133903457642" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113967896" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155897141" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425956726" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215118408" Y="29.286314453125" />
                  <Point X="0.183397994995" Y="29.42508203125" />
                  <Point X="0.307419647217" Y="29.887939453125" />
                  <Point X="0.72482635498" Y="29.844224609375" />
                  <Point X="0.844030517578" Y="29.831740234375" />
                  <Point X="1.362990722656" Y="29.706447265625" />
                  <Point X="1.481039550781" Y="29.6779453125" />
                  <Point X="1.818529418945" Y="29.55553515625" />
                  <Point X="1.894646850586" Y="29.52792578125" />
                  <Point X="2.221269775391" Y="29.375177734375" />
                  <Point X="2.294555908203" Y="29.34090234375" />
                  <Point X="2.610131347656" Y="29.157048828125" />
                  <Point X="2.680976074219" Y="29.115775390625" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.843420166016" Y="28.756326171875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142075683594" Y="27.5399296875" />
                  <Point X="2.133076660156" Y="27.516056640625" />
                  <Point X="2.115738037109" Y="27.45121875" />
                  <Point X="2.113694580078" Y="27.441611328125" />
                  <Point X="2.108226074219" Y="27.407259765625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.114488769531" Y="27.324884765625" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442138672" Y="27.289603515625" />
                  <Point X="2.129708007812" Y="27.267515625" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.174762939453" Y="27.19634375" />
                  <Point X="2.180857666016" Y="27.188314453125" />
                  <Point X="2.202423583984" Y="27.16283203125" />
                  <Point X="2.221597412109" Y="27.14559375" />
                  <Point X="2.272724609375" Y="27.110900390625" />
                  <Point X="2.284906005859" Y="27.102634765625" />
                  <Point X="2.304951171875" Y="27.092271484375" />
                  <Point X="2.327037597656" Y="27.084005859375" />
                  <Point X="2.348963378906" Y="27.078662109375" />
                  <Point X="2.405030029297" Y="27.07190234375" />
                  <Point X="2.415596435547" Y="27.07122265625" />
                  <Point X="2.447858642578" Y="27.07094921875" />
                  <Point X="2.473207519531" Y="27.074169921875" />
                  <Point X="2.538045654297" Y="27.091509765625" />
                  <Point X="2.543412353516" Y="27.0931171875" />
                  <Point X="2.570944335938" Y="27.10225" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="2.875688476562" Y="27.275935546875" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.081247314453" Y="27.747865234375" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.26219921875" Y="27.459884765625" />
                  <Point X="4.146920898438" Y="27.371427734375" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221427734375" Y="26.660244140625" />
                  <Point X="3.203974365234" Y="26.64162890625" />
                  <Point X="3.157310302734" Y="26.580751953125" />
                  <Point X="3.152064453125" Y="26.573173828125" />
                  <Point X="3.132476318359" Y="26.541716796875" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.104247314453" Y="26.4549296875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.112008789062" Y="26.302611328125" />
                  <Point X="3.114387207031" Y="26.293427734375" />
                  <Point X="3.124985351562" Y="26.259572265625" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.175093505859" Y="26.176748046875" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198893554688" Y="26.14544921875" />
                  <Point X="3.216137207031" Y="26.129359375" />
                  <Point X="3.234346435547" Y="26.116033203125" />
                  <Point X="3.290588378906" Y="26.084373046875" />
                  <Point X="3.299550537109" Y="26.07993359375" />
                  <Point X="3.33092578125" Y="26.06639453125" />
                  <Point X="3.356119628906" Y="26.0594375" />
                  <Point X="3.432162597656" Y="26.049388671875" />
                  <Point X="3.437325927734" Y="26.048849609375" />
                  <Point X="3.468521240234" Y="26.046451171875" />
                  <Point X="3.488203857422" Y="26.046984375" />
                  <Point X="3.760981445312" Y="26.082896484375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.82788671875" Y="26.00694921875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.766492675781" Y="25.6109140625" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704792724609" Y="25.3255859375" />
                  <Point X="3.681549072266" Y="25.315068359375" />
                  <Point X="3.606839355469" Y="25.271884765625" />
                  <Point X="3.5890390625" Y="25.26159765625" />
                  <Point X="3.574314697266" Y="25.251099609375" />
                  <Point X="3.547530029297" Y="25.225576171875" />
                  <Point X="3.502704101562" Y="25.16845703125" />
                  <Point X="3.492024169922" Y="25.15484765625" />
                  <Point X="3.480299804688" Y="25.13556640625" />
                  <Point X="3.470526611328" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.448738525391" Y="25.01458203125" />
                  <Point X="3.447470458984" Y="25.00571484375" />
                  <Point X="3.443910644531" Y="24.968314453125" />
                  <Point X="3.445178710938" Y="24.941443359375" />
                  <Point X="3.460120849609" Y="24.863421875" />
                  <Point X="3.463680908203" Y="24.844833984375" />
                  <Point X="3.470527832031" Y="24.82333203125" />
                  <Point X="3.48030078125" Y="24.80187109375" />
                  <Point X="3.492024169922" Y="24.782591796875" />
                  <Point X="3.536850097656" Y="24.72547265625" />
                  <Point X="3.543055419922" Y="24.718330078125" />
                  <Point X="3.568047607422" Y="24.692298828125" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.663745361328" Y="24.63266015625" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="3.966729248047" Y="24.5408203125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.865100585938" Y="24.118119140625" />
                  <Point X="4.855021484375" Y="24.051265625" />
                  <Point X="4.801174804688" Y="23.81530078125" />
                  <Point X="4.641985351562" Y="23.8362578125" />
                  <Point X="3.424381591797" Y="23.99655859375" />
                  <Point X="3.40803515625" Y="23.9972890625" />
                  <Point X="3.37466015625" Y="23.994490234375" />
                  <Point X="3.22803125" Y="23.962619140625" />
                  <Point X="3.193095947266" Y="23.95502734375" />
                  <Point X="3.163978515625" Y="23.943404296875" />
                  <Point X="3.136149658203" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.023769287109" Y="23.799447265625" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932617188" Y="23.749669921875" />
                  <Point X="2.976589355469" Y="23.72228515625" />
                  <Point X="2.969757324219" Y="23.694634765625" />
                  <Point X="2.9570546875" Y="23.55659375" />
                  <Point X="2.954028320312" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079345703" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.057597167969" Y="23.30578515625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.342768554688" Y="23.060962890625" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.153218261719" Y="22.29618359375" />
                  <Point X="4.124814453125" Y="22.250220703125" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.885395019531" Y="22.196953125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786136230469" Y="22.829984375" />
                  <Point X="2.754223632813" Y="22.84017578125" />
                  <Point X="2.579711914062" Y="22.871693359375" />
                  <Point X="2.538133300781" Y="22.879201171875" />
                  <Point X="2.506776611328" Y="22.879603515625" />
                  <Point X="2.474604736328" Y="22.87464453125" />
                  <Point X="2.444833251953" Y="22.864822265625" />
                  <Point X="2.299856933594" Y="22.7885234375" />
                  <Point X="2.265315185547" Y="22.77034375" />
                  <Point X="2.242384033203" Y="22.753451171875" />
                  <Point X="2.221425048828" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.128231933594" Y="22.564583984375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229492188" Y="22.500267578125" />
                  <Point X="2.095271484375" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.127192138672" Y="22.26223046875" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.300992431641" Y="21.915544921875" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.8155625" Y="20.912435546875" />
                  <Point X="2.781838867188" Y="20.888349609375" />
                  <Point X="2.701764892578" Y="20.836517578125" />
                  <Point X="2.586074707031" Y="20.9872890625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747505371094" Y="22.077818359375" />
                  <Point X="1.721923217773" Y="22.099443359375" />
                  <Point X="1.549807617188" Y="22.21009765625" />
                  <Point X="1.508799926758" Y="22.2364609375" />
                  <Point X="1.479986206055" Y="22.24883203125" />
                  <Point X="1.448365966797" Y="22.2565625" />
                  <Point X="1.417100463867" Y="22.258880859375" />
                  <Point X="1.228862670898" Y="22.241560546875" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156362182617" Y="22.2306015625" />
                  <Point X="1.128976806641" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="0.959242797852" Y="22.0836796875" />
                  <Point X="0.924611328125" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.832164611816" Y="21.7742421875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.862016784668" Y="21.3557734375" />
                  <Point X="1.022065551758" Y="20.140083984375" />
                  <Point X="1.007570129395" Y="20.13690625" />
                  <Point X="0.975708129883" Y="20.129921875" />
                  <Point X="0.929315734863" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.007842041016" Y="20.237544921875" />
                  <Point X="-1.058415283203" Y="20.247361328125" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.172215209961" Y="20.748048828125" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.82152734375" />
                  <Point X="-1.199026000977" Y="20.850494140625" />
                  <Point X="-1.205664916992" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230572998047" Y="20.90513671875" />
                  <Point X="-1.250207763672" Y="20.9290625" />
                  <Point X="-1.261006713867" Y="20.94022265625" />
                  <Point X="-1.449385131836" Y="21.10542578125" />
                  <Point X="-1.494267700195" Y="21.144787109375" />
                  <Point X="-1.506739990234" Y="21.15403515625" />
                  <Point X="-1.53302355957" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532226563" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.62145690918" Y="21.201552734375" />
                  <Point X="-1.636814453125" Y="21.203830078125" />
                  <Point X="-1.886834472656" Y="21.220216796875" />
                  <Point X="-1.946403564453" Y="21.22412109375" />
                  <Point X="-1.961927978516" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999267578" Y="21.2180546875" />
                  <Point X="-2.039047973633" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.303766845703" Y="21.04498828125" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380450683594" Y="20.989861328125" />
                  <Point X="-2.402763427734" Y="20.96722265625" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.453744628906" Y="20.901974609375" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.673437988281" Y="20.942927734375" />
                  <Point X="-2.747610351562" Y="20.988853515625" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.942684570312" Y="21.234578125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314666992188" Y="22.442388671875" />
                  <Point X="-2.323651855469" Y="22.472005859375" />
                  <Point X="-2.329358642578" Y="22.486447265625" />
                  <Point X="-2.343576660156" Y="22.51527734375" />
                  <Point X="-2.351559326172" Y="22.52859375" />
                  <Point X="-2.369588134766" Y="22.553751953125" />
                  <Point X="-2.379634277344" Y="22.56559375" />
                  <Point X="-2.396982910156" Y="22.58294140625" />
                  <Point X="-2.408825683594" Y="22.59298828125" />
                  <Point X="-2.433986083984" Y="22.611015625" />
                  <Point X="-2.447303710938" Y="22.61899609375" />
                  <Point X="-2.476132324219" Y="22.6332109375" />
                  <Point X="-2.490572753906" Y="22.638916015625" />
                  <Point X="-2.520189208984" Y="22.6478984375" />
                  <Point X="-2.550873291016" Y="22.6519375" />
                  <Point X="-2.581805419922" Y="22.650923828125" />
                  <Point X="-2.597229492188" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685302734" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-2.934781738281" Y="22.47783203125" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.945436523438" Y="22.1824453125" />
                  <Point X="-4.004019287109" Y="22.25941015625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-4.100020019531" Y="22.618966796875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036480712891" Y="23.436689453125" />
                  <Point X="-3.015102539062" Y="23.459611328125" />
                  <Point X="-3.005365966797" Y="23.471958984375" />
                  <Point X="-2.987400634766" Y="23.499091796875" />
                  <Point X="-2.979834716797" Y="23.512875" />
                  <Point X="-2.967079833984" Y="23.541501953125" />
                  <Point X="-2.961890869141" Y="23.556345703125" />
                  <Point X="-2.955668701172" Y="23.580369140625" />
                  <Point X="-2.951552246094" Y="23.601197265625" />
                  <Point X="-2.948748046875" Y="23.631630859375" />
                  <Point X="-2.948577880859" Y="23.6469609375" />
                  <Point X="-2.950787597656" Y="23.678630859375" />
                  <Point X="-2.953084472656" Y="23.69378515625" />
                  <Point X="-2.9600859375" Y="23.723529296875" />
                  <Point X="-2.971781738281" Y="23.751763671875" />
                  <Point X="-2.987863769531" Y="23.777744140625" />
                  <Point X="-2.996954589844" Y="23.790080078125" />
                  <Point X="-3.017786132812" Y="23.81403515625" />
                  <Point X="-3.028744140625" Y="23.824751953125" />
                  <Point X="-3.052242431641" Y="23.84428515625" />
                  <Point X="-3.064782714844" Y="23.8531015625" />
                  <Point X="-3.086169677734" Y="23.865689453125" />
                  <Point X="-3.086180664063" Y="23.865697265625" />
                  <Point X="-3.105469970703" Y="23.87553515625" />
                  <Point X="-3.134720703125" Y="23.88675" />
                  <Point X="-3.149811767578" Y="23.89114453125" />
                  <Point X="-3.181692871094" Y="23.897623046875" />
                  <Point X="-3.197307128906" Y="23.89946875" />
                  <Point X="-3.228625" Y="23.90055859375" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-3.557528320312" Y="23.8585703125" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.717849609375" Y="23.936181640625" />
                  <Point X="-4.740762207031" Y="24.025880859375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.706558105469" Y="24.36675" />
                  <Point X="-3.508287841797" Y="24.687826171875" />
                  <Point X="-3.500468994141" Y="24.6902890625" />
                  <Point X="-3.473933837891" Y="24.700748046875" />
                  <Point X="-3.444165527344" Y="24.716107421875" />
                  <Point X="-3.433556396484" Y="24.72248828125" />
                  <Point X="-3.411143554688" Y="24.738044921875" />
                  <Point X="-3.393678710938" Y="24.751751953125" />
                  <Point X="-3.371230712891" Y="24.773626953125" />
                  <Point X="-3.360914794922" Y="24.78549609375" />
                  <Point X="-3.341665283203" Y="24.81175390625" />
                  <Point X="-3.333443847656" Y="24.825173828125" />
                  <Point X="-3.319330322266" Y="24.85318359375" />
                  <Point X="-3.313438232422" Y="24.8677734375" />
                  <Point X="-3.305967285156" Y="24.891845703125" />
                  <Point X="-3.300991699219" Y="24.91149609375" />
                  <Point X="-3.296722412109" Y="24.939650390625" />
                  <Point X="-3.295648681641" Y="24.953888671875" />
                  <Point X="-3.295647216797" Y="24.983529296875" />
                  <Point X="-3.296720458984" Y="24.997771484375" />
                  <Point X="-3.300989746094" Y="25.02593359375" />
                  <Point X="-3.304185791016" Y="25.039853515625" />
                  <Point X="-3.311656738281" Y="25.06392578125" />
                  <Point X="-3.319326904297" Y="25.084248046875" />
                  <Point X="-3.333435791016" Y="25.112251953125" />
                  <Point X="-3.341654541016" Y="25.125669921875" />
                  <Point X="-3.360900634766" Y="25.151927734375" />
                  <Point X="-3.371223144531" Y="25.1638046875" />
                  <Point X="-3.393682373047" Y="25.185689453125" />
                  <Point X="-3.405819091797" Y="25.195697265625" />
                  <Point X="-3.428227539062" Y="25.211248046875" />
                  <Point X="-3.448529296875" Y="25.223533203125" />
                  <Point X="-3.469440429688" Y="25.23446484375" />
                  <Point X="-3.478938964844" Y="25.23878515625" />
                  <Point X="-3.498363037109" Y="25.246359375" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.79378515625" Y="25.326111328125" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.746095703125" Y="25.85775" />
                  <Point X="-4.731330566406" Y="25.957533203125" />
                  <Point X="-4.633586914062" Y="26.318236328125" />
                  <Point X="-3.778067382812" Y="26.20560546875" />
                  <Point X="-3.767738769531" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736703857422" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704888916016" Y="26.2080546875" />
                  <Point X="-3.684603759766" Y="26.21208984375" />
                  <Point X="-3.674571777344" Y="26.21466015625" />
                  <Point X="-3.62496484375" Y="26.23030078125" />
                  <Point X="-3.603453125" Y="26.23767578125" />
                  <Point X="-3.584524902344" Y="26.24600390625" />
                  <Point X="-3.575289306641" Y="26.25068359375" />
                  <Point X="-3.556545166016" Y="26.26150390625" />
                  <Point X="-3.547870117188" Y="26.2671640625" />
                  <Point X="-3.531185058594" Y="26.279396484375" />
                  <Point X="-3.515929931641" Y="26.293373046875" />
                  <Point X="-3.502288818359" Y="26.308927734375" />
                  <Point X="-3.495892822266" Y="26.317078125" />
                  <Point X="-3.483478515625" Y="26.33480859375" />
                  <Point X="-3.478009765625" Y="26.343603515625" />
                  <Point X="-3.468062011719" Y="26.361736328125" />
                  <Point X="-3.463583007813" Y="26.37107421875" />
                  <Point X="-3.443677978516" Y="26.41912890625" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443509033203" Y="26.62380859375" />
                  <Point X="-3.447784179688" Y="26.63324609375" />
                  <Point X="-3.471801757812" Y="26.6793828125" />
                  <Point X="-3.482800292969" Y="26.699287109375" />
                  <Point X="-3.494292480469" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521500976563" Y="26.748912109375" />
                  <Point X="-3.536441894531" Y="26.76321484375" />
                  <Point X="-3.544302734375" Y="26.769958984375" />
                  <Point X="-3.708064208984" Y="26.895619140625" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.059661132812" Y="27.58202734375" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.726339111328" Y="28.03501171875" />
                  <Point X="-3.254157470703" Y="27.7623984375" />
                  <Point X="-3.244919433594" Y="27.757716796875" />
                  <Point X="-3.225988525391" Y="27.74938671875" />
                  <Point X="-3.216296142578" Y="27.745740234375" />
                  <Point X="-3.195651855469" Y="27.73923046875" />
                  <Point X="-3.185621826172" Y="27.736658203125" />
                  <Point X="-3.165332763672" Y="27.73262109375" />
                  <Point X="-3.155073730469" Y="27.73115625" />
                  <Point X="-3.085985107422" Y="27.725111328125" />
                  <Point X="-3.059172363281" Y="27.723333984375" />
                  <Point X="-3.038486328125" Y="27.72378515625" />
                  <Point X="-3.02815234375" Y="27.72457421875" />
                  <Point X="-3.006695068359" Y="27.727400390625" />
                  <Point X="-2.99651171875" Y="27.7293125" />
                  <Point X="-2.976421875" Y="27.73423046875" />
                  <Point X="-2.956990722656" Y="27.7413046875" />
                  <Point X="-2.938440917969" Y="27.750453125" />
                  <Point X="-2.929415771484" Y="27.755533203125" />
                  <Point X="-2.911162841797" Y="27.767162109375" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886615966797" Y="27.786138671875" />
                  <Point X="-2.878903564453" Y="27.793052734375" />
                  <Point X="-2.829864013672" Y="27.842091796875" />
                  <Point X="-2.811267333984" Y="27.861486328125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288818359" Y="27.886033203125" />
                  <Point X="-2.780658935547" Y="27.904287109375" />
                  <Point X="-2.775577148438" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759352294922" Y="27.951306640625" />
                  <Point X="-2.754435302734" Y="27.971400390625" />
                  <Point X="-2.752525634766" Y="27.981580078125" />
                  <Point X="-2.749700195312" Y="28.0030390625" />
                  <Point X="-2.748909912109" Y="28.013365234375" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.754841796875" Y="28.11348828125" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.860090576172" Y="28.35472265625" />
                  <Point X="-3.059386962891" Y="28.699912109375" />
                  <Point X="-2.748283447266" Y="28.93843359375" />
                  <Point X="-2.648367919922" Y="29.015037109375" />
                  <Point X="-2.192523193359" Y="29.268296875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111823486328" Y="29.164052734375" />
                  <Point X="-2.097523681641" Y="29.14911328125" />
                  <Point X="-2.089963623047" Y="29.14203125" />
                  <Point X="-2.073383056641" Y="29.1281171875" />
                  <Point X="-2.06509765625" Y="29.12190234375" />
                  <Point X="-2.047896972656" Y="29.110408203125" />
                  <Point X="-2.038982299805" Y="29.10512890625" />
                  <Point X="-1.962087036133" Y="29.06509765625" />
                  <Point X="-1.943766235352" Y="29.055560546875" />
                  <Point X="-1.934329467773" Y="29.05128515625" />
                  <Point X="-1.915047973633" Y="29.043787109375" />
                  <Point X="-1.90520324707" Y="29.040564453125" />
                  <Point X="-1.884296630859" Y="29.034962890625" />
                  <Point X="-1.874159790039" Y="29.03283203125" />
                  <Point X="-1.853713745117" Y="29.029685546875" />
                  <Point X="-1.833048217773" Y="29.028783203125" />
                  <Point X="-1.812405883789" Y="29.03013671875" />
                  <Point X="-1.802119384766" Y="29.031376953125" />
                  <Point X="-1.780804199219" Y="29.03513671875" />
                  <Point X="-1.770712890625" Y="29.0374921875" />
                  <Point X="-1.750859375" Y="29.04328125" />
                  <Point X="-1.741097167969" Y="29.04671484375" />
                  <Point X="-1.661005615234" Y="29.079890625" />
                  <Point X="-1.641923583984" Y="29.087794921875" />
                  <Point X="-1.632588134766" Y="29.092271484375" />
                  <Point X="-1.614455078125" Y="29.10221875" />
                  <Point X="-1.605657226562" Y="29.107689453125" />
                  <Point X="-1.587927612305" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564227050781" Y="29.14013671875" />
                  <Point X="-1.550251831055" Y="29.155388671875" />
                  <Point X="-1.538020385742" Y="29.1720703125" />
                  <Point X="-1.532360229492" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.516855712891" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.478808837891" Y="29.320033203125" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.47179309082" Y="29.505892578125" />
                  <Point X="-1.479266113281" Y="29.56265625" />
                  <Point X="-1.06051550293" Y="29.68005859375" />
                  <Point X="-0.931166625977" Y="29.7163203125" />
                  <Point X="-0.365223083496" Y="29.78255859375" />
                  <Point X="-0.225666473389" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661560059" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.219973937988" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237977966309" Y="29.2617265625" />
                  <Point X="0.275160888672" Y="29.400494140625" />
                  <Point X="0.378190795898" Y="29.785009765625" />
                  <Point X="0.714931152344" Y="29.7497421875" />
                  <Point X="0.82785168457" Y="29.737916015625" />
                  <Point X="1.34069543457" Y="29.614099609375" />
                  <Point X="1.453621826172" Y="29.586833984375" />
                  <Point X="1.786137084961" Y="29.466228515625" />
                  <Point X="1.858250732422" Y="29.440072265625" />
                  <Point X="2.181025634766" Y="29.289123046875" />
                  <Point X="2.250425292969" Y="29.256666015625" />
                  <Point X="2.56230859375" Y="29.074962890625" />
                  <Point X="2.629432373047" Y="29.035857421875" />
                  <Point X="2.817779052734" Y="28.901916015625" />
                  <Point X="2.761147705078" Y="28.803826171875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062371826172" Y="27.5931015625" />
                  <Point X="2.053181640625" Y="27.573439453125" />
                  <Point X="2.044182617188" Y="27.54956640625" />
                  <Point X="2.041301513672" Y="27.540599609375" />
                  <Point X="2.023962890625" Y="27.47576171875" />
                  <Point X="2.019876098633" Y="27.456546875" />
                  <Point X="2.014407470703" Y="27.4221953125" />
                  <Point X="2.013243041992" Y="27.40905859375" />
                  <Point X="2.012744995117" Y="27.38275" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.02017199707" Y="27.31351171875" />
                  <Point X="2.023800537109" Y="27.28903515625" />
                  <Point X="2.029143310547" Y="27.267111328125" />
                  <Point X="2.032468261719" Y="27.256306640625" />
                  <Point X="2.040734130859" Y="27.23421875" />
                  <Point X="2.045318237305" Y="27.223888671875" />
                  <Point X="2.055681152344" Y="27.20384375" />
                  <Point X="2.061459716797" Y="27.19412890625" />
                  <Point X="2.096151855469" Y="27.143001953125" />
                  <Point X="2.108341308594" Y="27.126943359375" />
                  <Point X="2.129907226562" Y="27.1014609375" />
                  <Point X="2.138908935547" Y="27.092185546875" />
                  <Point X="2.158082763672" Y="27.074947265625" />
                  <Point X="2.168254882812" Y="27.066984375" />
                  <Point X="2.219382080078" Y="27.032291015625" />
                  <Point X="2.241277099609" Y="27.01824609375" />
                  <Point X="2.261322265625" Y="27.0078828125" />
                  <Point X="2.271653808594" Y="27.003298828125" />
                  <Point X="2.293740234375" Y="26.995033203125" />
                  <Point X="2.304542724609" Y="26.99170703125" />
                  <Point X="2.326468505859" Y="26.98636328125" />
                  <Point X="2.337591796875" Y="26.984345703125" />
                  <Point X="2.393658447266" Y="26.9775859375" />
                  <Point X="2.414791259766" Y="26.9762265625" />
                  <Point X="2.447053466797" Y="26.975953125" />
                  <Point X="2.459832519531" Y="26.97670703125" />
                  <Point X="2.485181396484" Y="26.979927734375" />
                  <Point X="2.497751220703" Y="26.98239453125" />
                  <Point X="2.562589355469" Y="26.999734375" />
                  <Point X="2.573322753906" Y="27.00294921875" />
                  <Point X="2.600854736328" Y="27.01208203125" />
                  <Point X="2.609851806641" Y="27.01558203125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="2.923188476563" Y="27.1936640625" />
                  <Point X="3.940402832031" Y="27.780951171875" />
                  <Point X="4.004134765625" Y="27.69237890625" />
                  <Point X="4.043959960938" Y="27.63703125" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="4.089088134766" Y="27.446796875" />
                  <Point X="3.172951171875" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.152124755859" Y="26.72522265625" />
                  <Point X="3.134671386719" Y="26.706607421875" />
                  <Point X="3.128576904297" Y="26.699423828125" />
                  <Point X="3.081912841797" Y="26.638546875" />
                  <Point X="3.071421142578" Y="26.623390625" />
                  <Point X="3.051833007812" Y="26.59193359375" />
                  <Point X="3.045532958984" Y="26.58000390625" />
                  <Point X="3.034686523438" Y="26.555373046875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.012757568359" Y="26.480515625" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.01896875" Y="26.2834140625" />
                  <Point X="3.023725585938" Y="26.265046875" />
                  <Point X="3.034323730469" Y="26.23119140625" />
                  <Point X="3.039141113281" Y="26.218880859375" />
                  <Point X="3.050438720703" Y="26.195046875" />
                  <Point X="3.056918945312" Y="26.1835234375" />
                  <Point X="3.095729492188" Y="26.124533203125" />
                  <Point X="3.111739501953" Y="26.101421875" />
                  <Point X="3.126292480469" Y="26.084177734375" />
                  <Point X="3.134082519531" Y="26.075990234375" />
                  <Point X="3.151326171875" Y="26.059900390625" />
                  <Point X="3.160032226562" Y="26.0526953125" />
                  <Point X="3.178241455078" Y="26.039369140625" />
                  <Point X="3.187744628906" Y="26.033248046875" />
                  <Point X="3.243986572266" Y="26.001587890625" />
                  <Point X="3.261910888672" Y="25.992708984375" />
                  <Point X="3.293286132813" Y="25.979169921875" />
                  <Point X="3.305638916016" Y="25.974822265625" />
                  <Point X="3.330832763672" Y="25.967865234375" />
                  <Point X="3.343673828125" Y="25.965255859375" />
                  <Point X="3.419716796875" Y="25.95520703125" />
                  <Point X="3.430043457031" Y="25.95412890625" />
                  <Point X="3.461238769531" Y="25.95173046875" />
                  <Point X="3.47109375" Y="25.951486328125" />
                  <Point X="3.500604003906" Y="25.952796875" />
                  <Point X="3.773381591797" Y="25.988708984375" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.735582519531" Y="25.984478515625" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.741904296875" Y="25.702677734375" />
                  <Point X="3.691991455078" Y="25.421353515625" />
                  <Point X="3.686024414062" Y="25.419541015625" />
                  <Point X="3.665628662109" Y="25.41213671875" />
                  <Point X="3.642385009766" Y="25.401619140625" />
                  <Point X="3.6340078125" Y="25.39731640625" />
                  <Point X="3.559298095703" Y="25.3541328125" />
                  <Point X="3.541497802734" Y="25.343845703125" />
                  <Point X="3.533888671875" Y="25.338951171875" />
                  <Point X="3.508778320312" Y="25.319875" />
                  <Point X="3.481993652344" Y="25.2943515625" />
                  <Point X="3.472795898438" Y="25.2842265625" />
                  <Point X="3.427969970703" Y="25.227107421875" />
                  <Point X="3.410852783203" Y="25.204205078125" />
                  <Point X="3.399128417969" Y="25.184923828125" />
                  <Point X="3.393841308594" Y="25.174935546875" />
                  <Point X="3.384068115234" Y="25.15347265625" />
                  <Point X="3.380004882812" Y="25.142927734375" />
                  <Point X="3.373158935547" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.355434082031" Y="25.032451171875" />
                  <Point X="3.352897949219" Y="25.014716796875" />
                  <Point X="3.349338134766" Y="24.97731640625" />
                  <Point X="3.349016357422" Y="24.9638359375" />
                  <Point X="3.350284423828" Y="24.93696484375" />
                  <Point X="3.351874267578" Y="24.92357421875" />
                  <Point X="3.36681640625" Y="24.845552734375" />
                  <Point X="3.373159423828" Y="24.816009765625" />
                  <Point X="3.380006347656" Y="24.7945078125" />
                  <Point X="3.3840703125" Y="24.7839609375" />
                  <Point X="3.393843261719" Y="24.7625" />
                  <Point X="3.399129882813" Y="24.75251171875" />
                  <Point X="3.410853271484" Y="24.733232421875" />
                  <Point X="3.417290039063" Y="24.72394140625" />
                  <Point X="3.462115966797" Y="24.666822265625" />
                  <Point X="3.474526611328" Y="24.652537109375" />
                  <Point X="3.499518798828" Y="24.626505859375" />
                  <Point X="3.509432373047" Y="24.617537109375" />
                  <Point X="3.530420166016" Y="24.60108203125" />
                  <Point X="3.541494384766" Y="24.593595703125" />
                  <Point X="3.616204345703" Y="24.550412109375" />
                  <Point X="3.634004394531" Y="24.540123046875" />
                  <Point X="3.639489013672" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026611328" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="3.942141357422" Y="24.449056640625" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.771162109375" Y="24.13228125" />
                  <Point X="4.761612304688" Y="24.0689375" />
                  <Point X="4.727802734375" Y="23.920779296875" />
                  <Point X="4.654384765625" Y="23.9304453125" />
                  <Point X="3.43678125" Y="24.09074609375" />
                  <Point X="3.428622558594" Y="24.09146484375" />
                  <Point X="3.400096435547" Y="24.09195703125" />
                  <Point X="3.366721435547" Y="24.089158203125" />
                  <Point X="3.354482177734" Y="24.087322265625" />
                  <Point X="3.207853271484" Y="24.055451171875" />
                  <Point X="3.17291796875" Y="24.047859375" />
                  <Point X="3.157876220703" Y="24.0432578125" />
                  <Point X="3.128758789062" Y="24.031634765625" />
                  <Point X="3.114683105469" Y="24.02461328125" />
                  <Point X="3.086854248047" Y="24.007720703125" />
                  <Point X="3.074126708984" Y="23.998470703125" />
                  <Point X="3.050374267578" Y="23.977998046875" />
                  <Point X="3.039349365234" Y="23.966775390625" />
                  <Point X="2.950721435547" Y="23.86018359375" />
                  <Point X="2.929605224609" Y="23.834787109375" />
                  <Point X="2.921326904297" Y="23.82315234375" />
                  <Point X="2.906606445312" Y="23.798771484375" />
                  <Point X="2.900164306641" Y="23.786025390625" />
                  <Point X="2.888821044922" Y="23.758640625" />
                  <Point X="2.884363037109" Y="23.745072265625" />
                  <Point X="2.877531005859" Y="23.717421875" />
                  <Point X="2.875156982422" Y="23.70333984375" />
                  <Point X="2.862454345703" Y="23.565298828125" />
                  <Point X="2.859427978516" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864066162109" Y="23.469869140625" />
                  <Point X="2.871798095703" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158203125" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.977687255859" Y="23.25441015625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.284936279297" Y="22.98559375" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.072405029297" Y="22.346125" />
                  <Point X="4.045500244141" Y="22.302587890625" />
                  <Point X="4.001273681641" Y="22.239748046875" />
                  <Point X="3.932894775391" Y="22.2792265625" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841197509766" Y="22.909109375" />
                  <Point X="2.815036865234" Y="22.920482421875" />
                  <Point X="2.783124267578" Y="22.930673828125" />
                  <Point X="2.771107910156" Y="22.9336640625" />
                  <Point X="2.596596191406" Y="22.965181640625" />
                  <Point X="2.555017578125" Y="22.972689453125" />
                  <Point X="2.539352050781" Y="22.974193359375" />
                  <Point X="2.507995361328" Y="22.974595703125" />
                  <Point X="2.492304199219" Y="22.973494140625" />
                  <Point X="2.460132324219" Y="22.96853515625" />
                  <Point X="2.444840332031" Y="22.964861328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.400589355469" Y="22.948890625" />
                  <Point X="2.255613037109" Y="22.872591796875" />
                  <Point X="2.221071289062" Y="22.854412109375" />
                  <Point X="2.208969970703" Y="22.846830078125" />
                  <Point X="2.186038818359" Y="22.8299375" />
                  <Point X="2.175208984375" Y="22.820626953125" />
                  <Point X="2.15425" Y="22.79966796875" />
                  <Point X="2.144938476562" Y="22.788837890625" />
                  <Point X="2.128045410156" Y="22.76590625" />
                  <Point X="2.120463867188" Y="22.7538046875" />
                  <Point X="2.04416394043" Y="22.608828125" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.559806640625" />
                  <Point X="2.010012573242" Y="22.53003125" />
                  <Point X="2.006337890625" Y="22.514736328125" />
                  <Point X="2.001379760742" Y="22.4825625" />
                  <Point X="2.000279418945" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.033704467773" Y="22.24534765625" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.218719970703" Y="21.868044921875" />
                  <Point X="2.735893066406" Y="20.97227734375" />
                  <Point X="2.723753417969" Y="20.963916015625" />
                  <Point X="2.661443359375" Y="21.04512109375" />
                  <Point X="1.833914916992" Y="22.123576171875" />
                  <Point X="1.828654541016" Y="22.1298515625" />
                  <Point X="1.808834472656" Y="22.150369140625" />
                  <Point X="1.783252319336" Y="22.171994140625" />
                  <Point X="1.773297973633" Y="22.179353515625" />
                  <Point X="1.601182495117" Y="22.2900078125" />
                  <Point X="1.560174682617" Y="22.31637109375" />
                  <Point X="1.546279418945" Y="22.323755859375" />
                  <Point X="1.517465698242" Y="22.336126953125" />
                  <Point X="1.502547241211" Y="22.34111328125" />
                  <Point X="1.470927001953" Y="22.34884375" />
                  <Point X="1.455390991211" Y="22.351302734375" />
                  <Point X="1.424125488281" Y="22.35362109375" />
                  <Point X="1.408395996094" Y="22.35348046875" />
                  <Point X="1.220158203125" Y="22.33616015625" />
                  <Point X="1.175309204102" Y="22.332033203125" />
                  <Point X="1.16122668457" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120006347656" Y="22.318369140625" />
                  <Point X="1.092620849609" Y="22.307025390625" />
                  <Point X="1.07987487793" Y="22.300583984375" />
                  <Point X="1.055493286133" Y="22.28586328125" />
                  <Point X="1.043857666016" Y="22.277583984375" />
                  <Point X="0.898505249023" Y="22.1567265625" />
                  <Point X="0.863873718262" Y="22.127931640625" />
                  <Point X="0.852653930664" Y="22.11691015625" />
                  <Point X="0.832183776855" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.739332214355" Y="21.794419921875" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.767829467773" Y="21.343373046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146972656" Y="21.546416015625" />
                  <Point X="0.626788391113" Y="21.57618359375" />
                  <Point X="0.620407592773" Y="21.58679296875" />
                  <Point X="0.488183288574" Y="21.777302734375" />
                  <Point X="0.456679840088" Y="21.822693359375" />
                  <Point X="0.446670074463" Y="21.834830078125" />
                  <Point X="0.4247840271" Y="21.857287109375" />
                  <Point X="0.412907562256" Y="21.867607421875" />
                  <Point X="0.386652893066" Y="21.886849609375" />
                  <Point X="0.37323638916" Y="21.89506640625" />
                  <Point X="0.345237182617" Y="21.909171875" />
                  <Point X="0.330654449463" Y="21.915060546875" />
                  <Point X="0.12604486084" Y="21.9785625" />
                  <Point X="0.077295211792" Y="21.993693359375" />
                  <Point X="0.063380836487" Y="21.996888671875" />
                  <Point X="0.035228092194" Y="22.001158203125" />
                  <Point X="0.020989871979" Y="22.002232421875" />
                  <Point X="-0.008651183128" Y="22.002234375" />
                  <Point X="-0.022895202637" Y="22.001162109375" />
                  <Point X="-0.051061321259" Y="21.996892578125" />
                  <Point X="-0.064983276367" Y="21.9936953125" />
                  <Point X="-0.269592437744" Y="21.93019140625" />
                  <Point X="-0.318342376709" Y="21.9150625" />
                  <Point X="-0.332927337646" Y="21.909171875" />
                  <Point X="-0.360927886963" Y="21.895064453125" />
                  <Point X="-0.37434362793" Y="21.88684765625" />
                  <Point X="-0.400599487305" Y="21.867603515625" />
                  <Point X="-0.412476409912" Y="21.85728125" />
                  <Point X="-0.434360534668" Y="21.83482421875" />
                  <Point X="-0.444367767334" Y="21.822689453125" />
                  <Point X="-0.576592224121" Y="21.632177734375" />
                  <Point X="-0.60809552002" Y="21.5867890625" />
                  <Point X="-0.612467834473" Y="21.57987109375" />
                  <Point X="-0.625975280762" Y="21.55473828125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.719830200195" Y="21.224408203125" />
                  <Point X="-0.985425354004" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.055687281294" Y="27.617651808435" />
                  <Point X="3.903449243331" Y="27.759616075519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.769030083257" Y="28.817479056292" />
                  <Point X="2.377369794921" Y="29.182708183806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.094860440645" Y="27.451226137564" />
                  <Point X="3.817417554002" Y="27.7099458149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.720280711098" Y="28.733042472466" />
                  <Point X="2.066544388346" Y="29.342661455788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.018443873217" Y="27.392589630717" />
                  <Point X="3.731385864672" Y="27.660275554282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.671531261269" Y="28.648605961068" />
                  <Point X="1.8002135726" Y="29.46112285057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.942027295894" Y="27.333953133097" />
                  <Point X="3.645354175342" Y="27.610605293663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.62278181144" Y="28.56416944967" />
                  <Point X="1.572249027532" Y="29.543807119146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.865610718572" Y="27.275316635477" />
                  <Point X="3.559322486013" Y="27.560935033045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574032361611" Y="28.479732938272" />
                  <Point X="1.363469741595" Y="29.608600844151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789194141249" Y="27.216680137857" />
                  <Point X="3.473290796683" Y="27.511264772426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.525282911782" Y="28.395296426874" />
                  <Point X="1.175509315996" Y="29.653980667815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.712777563927" Y="27.158043640237" />
                  <Point X="3.387259107353" Y="27.461594511808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.476533461953" Y="28.310859915476" />
                  <Point X="0.987549382829" Y="29.699360032278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.707273725795" Y="26.100764857385" />
                  <Point X="4.697036851933" Y="26.110310896696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.636360986604" Y="27.099407142617" />
                  <Point X="3.301227418024" Y="27.411924251189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.427784012124" Y="28.226423404078" />
                  <Point X="0.804256695269" Y="29.740387119802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748183667755" Y="25.932719610529" />
                  <Point X="4.574973316831" Y="26.094240875841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.559944409282" Y="27.040770644997" />
                  <Point X="3.215195728694" Y="27.362253990571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.379034562295" Y="28.14198689268" />
                  <Point X="0.647336418203" Y="29.756821536683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.773740375082" Y="25.77899148659" />
                  <Point X="4.452909781729" Y="26.078170854986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483527831959" Y="26.982134147377" />
                  <Point X="3.129164039364" Y="27.312583729952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330285112466" Y="28.057550381282" />
                  <Point X="0.490415901736" Y="29.773256176808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.721999734657" Y="25.697344305549" />
                  <Point X="4.330846246628" Y="26.062100834131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.407111254637" Y="26.923497649756" />
                  <Point X="3.043132350035" Y="27.262913469334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281535662637" Y="27.973113869884" />
                  <Point X="0.370259108261" Y="29.755408090622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.613794920704" Y="25.668350818148" />
                  <Point X="4.208782711526" Y="26.046030813277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330694677314" Y="26.864861152136" />
                  <Point X="2.957100660705" Y="27.213243208715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.232786212808" Y="27.888677358486" />
                  <Point X="0.342411852736" Y="29.651479967702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.505590106751" Y="25.639357330748" />
                  <Point X="4.086719176424" Y="26.029960792422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.254278099991" Y="26.806224654516" />
                  <Point X="2.871069072301" Y="27.163572853982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.18403676298" Y="27.804240847088" />
                  <Point X="0.314564597211" Y="29.547551844782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.397385292798" Y="25.610363843347" />
                  <Point X="3.964655641323" Y="26.013890771567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.177861522669" Y="26.747588156896" />
                  <Point X="2.785037549565" Y="27.113902438012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135287313151" Y="27.71980433569" />
                  <Point X="0.286717341686" Y="29.443623721862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.289180478845" Y="25.581370355947" />
                  <Point X="3.842592106221" Y="25.997820750712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.112586011875" Y="26.678562446663" />
                  <Point X="2.699006026829" Y="27.064232022043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086537863322" Y="27.635367824292" />
                  <Point X="0.258869900248" Y="29.339695772309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.180975664892" Y="25.552376868546" />
                  <Point X="3.720528597219" Y="25.981750705519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.057106265536" Y="26.600402038294" />
                  <Point X="2.611200945934" Y="27.016215475812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043045900435" Y="27.546028627005" />
                  <Point X="0.228623093714" Y="29.238005266904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072770850939" Y="25.523383381146" />
                  <Point X="3.598465122394" Y="25.965680628456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.019710558165" Y="26.505377990771" />
                  <Point X="2.505850252493" Y="26.984560477977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017268953025" Y="27.440169910535" />
                  <Point X="0.176125293141" Y="29.157064149124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.322116286534" Y="29.621681938712" />
                  <Point X="-0.48020405739" Y="29.769101169969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.964566036987" Y="25.494389893745" />
                  <Point X="3.474240273969" Y="25.951626064881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001008338454" Y="26.392921983991" />
                  <Point X="2.371118818446" Y="26.980303463998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021194704033" Y="27.306612979692" />
                  <Point X="0.094579946213" Y="29.103210306534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.275717086506" Y="29.448517875897" />
                  <Point X="-0.603967027109" Y="29.754615897533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.856361223034" Y="25.465396406344" />
                  <Point X="3.311932574798" Y="25.97308433415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033984482843" Y="26.232275123061" />
                  <Point X="-0.028750021633" Y="29.088321253319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.229317886478" Y="29.275353813081" />
                  <Point X="-0.727729996827" Y="29.740130625096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.748156409081" Y="25.436402918944" />
                  <Point X="-0.851492966546" Y="29.725645352659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.64492730276" Y="25.402769509112" />
                  <Point X="-0.969319710947" Y="29.705624460559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783105996475" Y="24.211504597698" />
                  <Point X="4.764721353732" Y="24.228648554409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.558365326931" Y="25.353593748654" />
                  <Point X="-1.076418819248" Y="29.675599885958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765936429044" Y="24.097619369545" />
                  <Point X="4.569261822226" Y="24.281021407463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.482292691216" Y="25.294636520296" />
                  <Point X="-1.183516200046" Y="29.645573700433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743326646842" Y="23.988807223739" />
                  <Point X="4.373802290721" Y="24.333394260517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.422812378671" Y="25.220206700268" />
                  <Point X="-1.290613580843" Y="29.615547514908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.668626306944" Y="23.928570308829" />
                  <Point X="4.178342759216" Y="24.385767113571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376887199931" Y="25.133136513473" />
                  <Point X="-1.397710961641" Y="29.585521329383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.506430933244" Y="23.949923832901" />
                  <Point X="3.98288322771" Y="24.438139966624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354272545119" Y="25.024328911448" />
                  <Point X="-1.474616820042" Y="29.527341093751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.344235642469" Y="23.971277279645" />
                  <Point X="3.787423245274" Y="24.490513240178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358147202036" Y="24.890819626615" />
                  <Point X="-1.463901848105" Y="29.387453111968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.182040351694" Y="23.992630726389" />
                  <Point X="3.524994327192" Y="24.605336056524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.475802477152" Y="24.651208198801" />
                  <Point X="-1.490644601853" Y="29.282495024477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019845060918" Y="24.013984173133" />
                  <Point X="-1.528323848023" Y="29.187735381162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857649770143" Y="24.035337619878" />
                  <Point X="-1.592020722737" Y="29.117237568967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.695454479368" Y="24.056691066622" />
                  <Point X="-1.681959732036" Y="29.071210943166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.533259188592" Y="24.078044513366" />
                  <Point X="-1.782290285783" Y="29.034874589332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.380780387301" Y="24.090337187082" />
                  <Point X="-1.943774263753" Y="29.055564726154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.265465716919" Y="24.067973748061" />
                  <Point X="-2.266890668861" Y="29.226979539692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154233231288" Y="24.041803610177" />
                  <Point X="-2.354180663121" Y="29.178482667403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.067515896105" Y="23.99277272466" />
                  <Point X="-2.44147065738" Y="29.129985795115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.002990744335" Y="23.923047293317" />
                  <Point X="-2.52876065164" Y="29.081488922826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.942155327487" Y="23.849881128495" />
                  <Point X="-2.616050645899" Y="29.032992050538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.892131591961" Y="23.766632907733" />
                  <Point X="-2.696511119163" Y="28.978126546889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.870853969155" Y="23.656578503192" />
                  <Point X="-2.772956488599" Y="28.91951689835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859845679837" Y="23.53694779025" />
                  <Point X="-2.849401488626" Y="28.86090690533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.045000121433" Y="22.301877285255" />
                  <Point X="3.436609135098" Y="22.869211058282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909053128534" Y="23.361164993185" />
                  <Point X="-2.925846488652" Y="28.80229691231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925277307079" Y="22.28362450699" />
                  <Point X="-3.002291488678" Y="28.74368691929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.559542966782" Y="22.494781188031" />
                  <Point X="-3.018263942848" Y="28.628685364961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193808626484" Y="22.705937869073" />
                  <Point X="-2.855797420367" Y="28.347286772951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.832654658569" Y="22.912823283768" />
                  <Point X="-2.756022303458" Y="28.124348862408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646966022699" Y="22.956084629236" />
                  <Point X="-2.751398451742" Y="27.990140942122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.489468768208" Y="22.97305708627" />
                  <Point X="-2.787355925473" Y="27.893775720031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38492699148" Y="22.940647761396" />
                  <Point X="-2.849823987664" Y="27.822132021622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.295883858932" Y="22.89378571701" />
                  <Point X="-2.922456573414" Y="27.759966894775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.20783650528" Y="22.845995093781" />
                  <Point X="-3.024337867297" Y="27.725076629512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.138787513252" Y="22.780488211725" />
                  <Point X="-3.17345875778" Y="27.734238000741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.09005879078" Y="22.696032371754" />
                  <Point X="-3.487298035499" Y="27.897001753032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044201283566" Y="22.608899080239" />
                  <Point X="-3.746627267166" Y="28.008934065034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006280602388" Y="22.51436457871" />
                  <Point X="-3.805194996397" Y="27.933653247298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009044970272" Y="22.38189065515" />
                  <Point X="-3.863762725628" Y="27.858372429562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.037254756821" Y="22.225688494812" />
                  <Point X="-3.922330454859" Y="27.783091611826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145516475938" Y="21.99483669968" />
                  <Point X="-3.980898184089" Y="27.707810794091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307981930854" Y="21.713439103191" />
                  <Point X="-4.033454890543" Y="27.626924606932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.470447335034" Y="21.432041554013" />
                  <Point X="2.098161292618" Y="21.779203904924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.493114184845" Y="22.343419460747" />
                  <Point X="-4.082550948253" Y="27.542811312612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632912739214" Y="21.150644004835" />
                  <Point X="2.448560325039" Y="21.322555412219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.348899207083" Y="22.348005994352" />
                  <Point X="-4.131646762493" Y="27.458697791253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.222112915582" Y="22.336340015087" />
                  <Point X="-4.180742576733" Y="27.374584269894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.10759911101" Y="22.313229756617" />
                  <Point X="-3.421631080898" Y="26.536805239162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.024123764427" Y="22.261175667823" />
                  <Point X="-3.441313659514" Y="26.425263431851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.950486250304" Y="22.199947651844" />
                  <Point X="-3.483530154552" Y="26.334734841554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.876848575133" Y="22.138719786044" />
                  <Point X="-3.549338492454" Y="26.266206000637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814663362032" Y="22.066812326592" />
                  <Point X="-3.643772392596" Y="26.224370928357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.777780354404" Y="21.971310178822" />
                  <Point X="-3.761964167579" Y="26.204690432782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754304861895" Y="21.863305320937" />
                  <Point X="-3.922654711136" Y="26.224640680044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730829364553" Y="21.755300467559" />
                  <Point X="-4.084849862143" Y="26.245993996453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.730681263686" Y="21.625542465047" />
                  <Point X="-4.247045013151" Y="26.267347312861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750175640447" Y="21.477467555818" />
                  <Point X="0.597418365469" Y="21.619916019252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.256102452358" Y="21.938198257367" />
                  <Point X="-4.409240164158" Y="26.28870062927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769670029022" Y="21.329392635573" />
                  <Point X="0.682141152137" Y="21.41101463374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.052020645251" Y="21.998611512497" />
                  <Point X="-4.571435315166" Y="26.310053945678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789164530914" Y="21.181317609657" />
                  <Point X="0.728540297725" Y="21.237850621691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.077753585409" Y="21.989731831575" />
                  <Point X="-3.296209467002" Y="24.990990495228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.599860605097" Y="25.274149762425" />
                  <Point X="-4.650918612382" Y="26.254277210624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808659032806" Y="21.033242583742" />
                  <Point X="0.774939443313" Y="21.064686609642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.182265608872" Y="21.957294761332" />
                  <Point X="-3.311178818695" Y="24.875053532708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.795320151447" Y="25.326522629322" />
                  <Point X="-4.679017548962" Y="26.150583784085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.828153534697" Y="20.885167557827" />
                  <Point X="0.821338588901" Y="20.891522597593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.286778046448" Y="21.924858077256" />
                  <Point X="-3.358284428454" Y="24.789084115645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990780331231" Y="25.378896086905" />
                  <Point X="-4.707116485543" Y="26.046890357547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.380485746147" Y="21.882345812109" />
                  <Point X="-3.429319381386" Y="24.725429172093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.186240511016" Y="25.431269544489" />
                  <Point X="-4.733665766814" Y="25.941751854054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.448863044372" Y="21.816212565448" />
                  <Point X="-3.523826679716" Y="24.683662544732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.3817006908" Y="25.483643002072" />
                  <Point X="-4.750556164557" Y="25.827606295956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.503594497053" Y="21.737354361956" />
                  <Point X="-3.632031525891" Y="24.654669087378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.577160870584" Y="25.536016459656" />
                  <Point X="-4.767446641267" Y="25.713460811495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.558325949735" Y="21.658496158463" />
                  <Point X="-3.740236372065" Y="24.625675630025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772621050368" Y="25.58838991724" />
                  <Point X="-4.784337117976" Y="25.599315327034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.612748131544" Y="21.579349555216" />
                  <Point X="-3.848441218239" Y="24.596682172671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.650177262464" Y="21.484356675656" />
                  <Point X="-2.94907236827" Y="23.628111043268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.240617585525" Y="23.899981356649" />
                  <Point X="-3.956646064413" Y="24.567688715317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.678024664299" Y="21.380428689173" />
                  <Point X="-2.97541963965" Y="23.522784162504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362972816008" Y="23.884183346138" />
                  <Point X="-4.064850910588" Y="24.538695257964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.705872066135" Y="21.276500702689" />
                  <Point X="-3.029926708166" Y="23.443716717392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.485036803107" Y="23.868113746778" />
                  <Point X="-4.173055756762" Y="24.50970180061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.733719472598" Y="21.172572720522" />
                  <Point X="-3.104202232933" Y="23.383083655964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.607100653881" Y="23.852044020293" />
                  <Point X="-4.281260602936" Y="24.480708343256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.761566883713" Y="21.068644742692" />
                  <Point X="-3.180618852925" Y="23.324447198134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.729164305304" Y="23.835974107909" />
                  <Point X="-4.38946544911" Y="24.451714885902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.789414294827" Y="20.964716764862" />
                  <Point X="-2.311529482387" Y="22.3841121401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595909097868" Y="22.649300421727" />
                  <Point X="-3.257035472917" Y="23.265810740303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.851227956727" Y="23.819904195526" />
                  <Point X="-4.497670295285" Y="24.422721428549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.817261705942" Y="20.860788787032" />
                  <Point X="-2.339430108407" Y="22.280233785972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.697955859134" Y="22.614564457295" />
                  <Point X="-3.333452092909" Y="23.207174282473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973291608151" Y="23.803834283143" />
                  <Point X="-4.605875141459" Y="24.393727971195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.845109117057" Y="20.756860809202" />
                  <Point X="-2.38788408217" Y="22.195521738685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783987217177" Y="22.564893887746" />
                  <Point X="-3.409868712901" Y="23.148537824643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.095355259574" Y="23.78776437076" />
                  <Point X="-4.714079939218" Y="24.364734468694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.872956528171" Y="20.652932831372" />
                  <Point X="-2.436633494968" Y="22.111085192755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.87001857522" Y="22.515223318197" />
                  <Point X="-3.486285332893" Y="23.089901366813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.217418910997" Y="23.771694458376" />
                  <Point X="-4.77935323699" Y="24.295706694784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900803939286" Y="20.549004853541" />
                  <Point X="-1.186337245459" Y="20.815268969142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.593614713938" Y="21.195061352743" />
                  <Point X="-2.485382907766" Y="22.026648646825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956049987531" Y="22.465552799255" />
                  <Point X="-3.562701952885" Y="23.031264908983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.33948256242" Y="23.755624545993" />
                  <Point X="-4.757916170883" Y="24.145820198432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.9286513504" Y="20.445076875711" />
                  <Point X="-1.153693079759" Y="20.654931683349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.750290159433" Y="21.21126746049" />
                  <Point X="-2.534132320564" Y="21.942212100895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.042081565096" Y="22.415882434414" />
                  <Point X="-3.639118572877" Y="22.972628451152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.461546213843" Y="23.73955463361" />
                  <Point X="-4.732059910698" Y="23.991812736934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.956498761515" Y="20.341148897881" />
                  <Point X="-1.122469688961" Y="20.495919291585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900117258138" Y="21.22108738154" />
                  <Point X="-2.582881733362" Y="21.857775554965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.128113142661" Y="22.366212069573" />
                  <Point X="-3.715535192869" Y="22.913991993322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.583609865266" Y="23.723484721226" />
                  <Point X="-4.688505694373" Y="23.821301664341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.98434617263" Y="20.237220920051" />
                  <Point X="-1.127777931607" Y="20.370973199128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.029876303391" Y="21.212193539997" />
                  <Point X="-2.631631146159" Y="21.773339009035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.214144720225" Y="22.316541704732" />
                  <Point X="-3.791951812861" Y="22.855355535492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.120898117068" Y="21.167176645615" />
                  <Point X="-2.680380558957" Y="21.688902463105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.30017629779" Y="22.266871339891" />
                  <Point X="-3.868368432853" Y="22.796719077662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.202048170428" Y="21.112954185809" />
                  <Point X="-2.729129971755" Y="21.604465917175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.386207875355" Y="22.21720097505" />
                  <Point X="-3.944785052845" Y="22.738082619831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.283198223787" Y="21.058731726003" />
                  <Point X="-2.777879384553" Y="21.520029371245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.472239452919" Y="22.167530610209" />
                  <Point X="-4.021201672837" Y="22.679446162001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363686314239" Y="21.003891975799" />
                  <Point X="-2.826628797351" Y="21.435592825315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558271030484" Y="22.117860245367" />
                  <Point X="-4.097618292829" Y="22.620809704171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.4286962216" Y="20.934618586358" />
                  <Point X="-2.875378210149" Y="21.351156279385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.644302608049" Y="22.068189880526" />
                  <Point X="-4.174034747912" Y="22.562173092561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.48679579642" Y="20.858901207571" />
                  <Point X="-2.924127622947" Y="21.266719733455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.730334185613" Y="22.018519515685" />
                  <Point X="-4.023264427802" Y="22.291681385713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849950862825" Y="21.067652676797" />
                  <Point X="-2.972877082807" Y="21.182283231412" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.79504309082" Y="20.2555546875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.332094451904" Y="21.668966796875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274336517334" Y="21.733599609375" />
                  <Point X="0.069727012634" Y="21.7971015625" />
                  <Point X="0.020977386475" Y="21.812232421875" />
                  <Point X="-0.008663691521" Y="21.812234375" />
                  <Point X="-0.213273040771" Y="21.74873046875" />
                  <Point X="-0.262022827148" Y="21.7336015625" />
                  <Point X="-0.288278564453" Y="21.714357421875" />
                  <Point X="-0.42050289917" Y="21.523845703125" />
                  <Point X="-0.452006347656" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.536304321289" Y="21.175232421875" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-1.044045166016" Y="20.051025390625" />
                  <Point X="-1.100232666016" Y="20.061931640625" />
                  <Point X="-1.337588378906" Y="20.123001953125" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.348092529297" Y="20.153166015625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.358564453125" Y="20.710982421875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282470703" Y="20.79737109375" />
                  <Point X="-1.574660888672" Y="20.96257421875" />
                  <Point X="-1.619543334961" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.899260742188" Y="21.030623046875" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.198208740234" Y="20.8870078125" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.303006835938" Y="20.786310546875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.773459716797" Y="20.781384765625" />
                  <Point X="-2.855839599609" Y="20.832392578125" />
                  <Point X="-3.184482666016" Y="21.0854375" />
                  <Point X="-3.228580810547" Y="21.119390625" />
                  <Point X="-3.107229492188" Y="21.329578125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513980957031" Y="22.43123828125" />
                  <Point X="-2.531329589844" Y="22.4485859375" />
                  <Point X="-2.560158203125" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.83978125" Y="22.3132890625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.096623046875" Y="22.0673671875" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.397318847656" Y="22.5479609375" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.215684570312" Y="22.769705078125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821533203" Y="23.603986328125" />
                  <Point X="-3.139599365234" Y="23.628009765625" />
                  <Point X="-3.138116943359" Y="23.633734375" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.161158203125" Y="23.689359375" />
                  <Point X="-3.182545166016" Y="23.701947265625" />
                  <Point X="-3.187648193359" Y="23.70494921875" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.532729003906" Y="23.6701953125" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.901939453125" Y="23.889158203125" />
                  <Point X="-4.927393066406" Y="23.988806640625" />
                  <Point X="-4.989730957031" Y="24.42466796875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.755733886719" Y="24.55027734375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541894775391" Y="24.87857421875" />
                  <Point X="-3.519481933594" Y="24.894130859375" />
                  <Point X="-3.514149169922" Y="24.89783203125" />
                  <Point X="-3.494899658203" Y="24.92408984375" />
                  <Point X="-3.487428710938" Y="24.948162109375" />
                  <Point X="-3.485648681641" Y="24.953896484375" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.493118164062" Y="25.007609375" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514144287109" Y="25.039603515625" />
                  <Point X="-3.536552734375" Y="25.055154296875" />
                  <Point X="-3.536552001953" Y="25.05515625" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.842960449219" Y="25.142583984375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.934048828125" Y="25.8855625" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.792161621094" Y="26.4594921875" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.612215332031" Y="26.5070625" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731704589844" Y="26.3958671875" />
                  <Point X="-3.68209765625" Y="26.4115078125" />
                  <Point X="-3.670278564453" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.619215087891" Y="26.49183984375" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.640333496094" Y="26.5916484375" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.823729736328" Y="26.744880859375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.22375390625" Y="27.6778046875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.827611083984" Y="28.214263671875" />
                  <Point X="-3.774670898438" Y="28.282310546875" />
                  <Point X="-3.686699707031" Y="28.23151953125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138512695312" Y="27.92043359375" />
                  <Point X="-3.069424072266" Y="27.914388671875" />
                  <Point X="-3.052963378906" Y="27.91294921875" />
                  <Point X="-3.031506103516" Y="27.915775390625" />
                  <Point X="-3.013253173828" Y="27.927404296875" />
                  <Point X="-2.964213623047" Y="27.976443359375" />
                  <Point X="-2.952529541016" Y="27.988126953125" />
                  <Point X="-2.940899658203" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.944118652344" Y="28.0969296875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.024635009766" Y="28.25972265625" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.863887939453" Y="29.08921875" />
                  <Point X="-2.752871337891" Y="29.174333984375" />
                  <Point X="-2.229351074219" Y="29.46519140625" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.129323730469" Y="29.498041015625" />
                  <Point X="-1.967826904297" Y="29.28757421875" />
                  <Point X="-1.951246459961" Y="29.27366015625" />
                  <Point X="-1.874351196289" Y="29.23362890625" />
                  <Point X="-1.856030395508" Y="29.224091796875" />
                  <Point X="-1.835123779297" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.733717163086" Y="29.25542578125" />
                  <Point X="-1.714634765625" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.660015014648" Y="29.377166015625" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.660167724609" Y="29.481091796875" />
                  <Point X="-1.689137573242" Y="29.701140625" />
                  <Point X="-1.111806884766" Y="29.86300390625" />
                  <Point X="-0.968083679199" Y="29.903296875" />
                  <Point X="-0.333421356201" Y="29.977576171875" />
                  <Point X="-0.22420022583" Y="29.990359375" />
                  <Point X="-0.19820211792" Y="29.89333203125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282104492" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594051361" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.091635101318" Y="29.449669921875" />
                  <Point X="0.2366484375" Y="29.990869140625" />
                  <Point X="0.734721374512" Y="29.93870703125" />
                  <Point X="0.860209594727" Y="29.925564453125" />
                  <Point X="1.385286010742" Y="29.798794921875" />
                  <Point X="1.508457275391" Y="29.769056640625" />
                  <Point X="1.850921875" Y="29.644841796875" />
                  <Point X="1.931042114258" Y="29.61578125" />
                  <Point X="2.261514160156" Y="29.461232421875" />
                  <Point X="2.338686279297" Y="29.425140625" />
                  <Point X="2.657954345703" Y="29.239134765625" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="3.033625732422" Y="28.9815625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.925692626953" Y="28.708826171875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851806641" Y="27.491513671875" />
                  <Point X="2.207513183594" Y="27.42667578125" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.208805419922" Y="27.3362578125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.253374023438" Y="27.249685546875" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.326067138672" Y="27.189509765625" />
                  <Point X="2.338248535156" Y="27.181244140625" />
                  <Point X="2.360334960938" Y="27.172978515625" />
                  <Point X="2.416401611328" Y="27.16621875" />
                  <Point X="2.448663818359" Y="27.1659453125" />
                  <Point X="2.513501953125" Y="27.18328515625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.828188476562" Y="27.35820703125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.158359863281" Y="27.8033515625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.370451660156" Y="27.464490234375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.204753417969" Y="27.29605859375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371826172" Y="26.583833984375" />
                  <Point X="3.232707763672" Y="26.52295703125" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.195737060547" Y="26.42934375" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.205048828125" Y="26.32180859375" />
                  <Point X="3.215646972656" Y="26.287953125" />
                  <Point X="3.254457519531" Y="26.228962890625" />
                  <Point X="3.263704589844" Y="26.214908203125" />
                  <Point X="3.280948242188" Y="26.198818359375" />
                  <Point X="3.337190185547" Y="26.167158203125" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.444608398438" Y="26.1435703125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.748581298828" Y="26.177083984375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.920190917969" Y="26.029419921875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.9920859375" Y="25.611634765625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.791080566406" Y="25.519150390625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729090332031" Y="25.2328203125" />
                  <Point X="3.654380615234" Y="25.18963671875" />
                  <Point X="3.636580322266" Y="25.179349609375" />
                  <Point X="3.622264160156" Y="25.16692578125" />
                  <Point X="3.577438232422" Y="25.109806640625" />
                  <Point X="3.566758300781" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.54204296875" Y="24.996712890625" />
                  <Point X="3.538483154297" Y="24.9593125" />
                  <Point X="3.553425292969" Y="24.881291015625" />
                  <Point X="3.556985351562" Y="24.862703125" />
                  <Point X="3.566758300781" Y="24.8412421875" />
                  <Point X="3.611584228516" Y="24.784123046875" />
                  <Point X="3.636576416016" Y="24.758091796875" />
                  <Point X="3.711286376953" Y="24.714908203125" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.991317138672" Y="24.632583984375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.9590390625" Y="24.10395703125" />
                  <Point X="4.948430664062" Y="24.03359375" />
                  <Point X="4.880662109375" Y="23.73662109375" />
                  <Point X="4.874546386719" Y="23.709822265625" />
                  <Point X="4.6295859375" Y="23.7420703125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394838134766" Y="23.901658203125" />
                  <Point X="3.248209228516" Y="23.869787109375" />
                  <Point X="3.213273925781" Y="23.8621953125" />
                  <Point X="3.185445068359" Y="23.845302734375" />
                  <Point X="3.096817138672" Y="23.7387109375" />
                  <Point X="3.075700927734" Y="23.713314453125" />
                  <Point X="3.064357666016" Y="23.6859296875" />
                  <Point X="3.051655029297" Y="23.547888671875" />
                  <Point X="3.048628662109" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.137507080078" Y="23.35716015625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.400600830078" Y="23.13633203125" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.234031738281" Y="22.2462421875" />
                  <Point X="4.204129882813" Y="22.19785546875" />
                  <Point X="4.063979248047" Y="21.998720703125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.837895019531" Y="22.114681640625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737339355469" Y="22.7466875" />
                  <Point X="2.562827636719" Y="22.778205078125" />
                  <Point X="2.521249023438" Y="22.785712890625" />
                  <Point X="2.489077148438" Y="22.78075390625" />
                  <Point X="2.344100830078" Y="22.704455078125" />
                  <Point X="2.309559082031" Y="22.686275390625" />
                  <Point X="2.288600097656" Y="22.66531640625" />
                  <Point X="2.212300048828" Y="22.52033984375" />
                  <Point X="2.19412109375" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.2206796875" Y="22.27911328125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.383264892578" Y="21.963044921875" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.870780273438" Y="20.835130859375" />
                  <Point X="2.835296630859" Y="20.809787109375" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="2.510705810547" Y="20.92945703125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670548583984" Y="22.019533203125" />
                  <Point X="1.498432861328" Y="22.1301875" />
                  <Point X="1.457425170898" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.237567138672" Y="22.1469609375" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.131490234375" />
                  <Point X="1.01998034668" Y="22.0106328125" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.924997070312" Y="21.754064453125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.956204040527" Y="21.368173828125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.027911499023" Y="20.044109375" />
                  <Point X="0.994364929199" Y="20.036755859375" />
                  <Point X="0.860200683594" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#198" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.156061723514" Y="4.937554784567" Z="2.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="-0.345389572826" Y="5.058516680955" Z="2.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.05" />
                  <Point X="-1.131460266896" Y="4.942428104167" Z="2.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.05" />
                  <Point X="-1.71589601634" Y="4.50584653185" Z="2.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.05" />
                  <Point X="-1.713856853068" Y="4.423481960811" Z="2.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.05" />
                  <Point X="-1.759007531452" Y="4.332899850776" Z="2.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.05" />
                  <Point X="-1.857419911193" Y="4.309261883632" Z="2.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.05" />
                  <Point X="-2.095811966217" Y="4.559758099315" Z="2.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.05" />
                  <Point X="-2.259789677052" Y="4.540178315111" Z="2.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.05" />
                  <Point X="-2.900466297684" Y="4.160179403398" Z="2.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.05" />
                  <Point X="-3.074092346887" Y="3.266003777046" Z="2.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.05" />
                  <Point X="-3.000084535351" Y="3.123852070152" Z="2.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.05" />
                  <Point X="-3.005724034164" Y="3.043079533723" Z="2.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.05" />
                  <Point X="-3.071224395455" Y="2.995480163822" Z="2.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.05" />
                  <Point X="-3.667855571251" Y="3.306101601859" Z="2.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.05" />
                  <Point X="-3.873230399871" Y="3.276246752366" Z="2.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.05" />
                  <Point X="-4.273091904677" Y="2.734290693657" Z="2.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.05" />
                  <Point X="-3.860323746639" Y="1.736492692725" Z="2.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.05" />
                  <Point X="-3.690839944027" Y="1.599841595554" Z="2.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.05" />
                  <Point X="-3.671564937153" Y="1.542254963542" Z="2.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.05" />
                  <Point X="-3.703289122963" Y="1.490473529865" Z="2.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.05" />
                  <Point X="-4.611845064803" Y="1.587915349362" Z="2.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.05" />
                  <Point X="-4.8465768165" Y="1.503850332546" Z="2.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.05" />
                  <Point X="-4.98966622695" Y="0.924162146069" Z="2.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.05" />
                  <Point X="-3.862057218618" Y="0.125567585289" Z="2.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.05" />
                  <Point X="-3.571220711147" Y="0.045362750081" Z="2.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.05" />
                  <Point X="-3.547027820652" Y="0.024071695047" Z="2.05" />
                  <Point X="-3.539556741714" Y="0" Z="2.05" />
                  <Point X="-3.541336780483" Y="-0.005735256013" Z="2.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.05" />
                  <Point X="-3.554147883976" Y="-0.033513233053" Z="2.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.05" />
                  <Point X="-4.774830570155" Y="-0.370144471467" Z="2.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.05" />
                  <Point X="-5.045383205101" Y="-0.551128833465" Z="2.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.05" />
                  <Point X="-4.956539992345" Y="-1.091936418944" Z="2.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.05" />
                  <Point X="-3.532358686375" Y="-1.348096835761" Z="2.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.05" />
                  <Point X="-3.214063284898" Y="-1.309862358117" Z="2.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.05" />
                  <Point X="-3.194158753913" Y="-1.328173594577" Z="2.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.05" />
                  <Point X="-4.252278371728" Y="-2.159346529909" Z="2.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.05" />
                  <Point X="-4.446418477542" Y="-2.446367620021" Z="2.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.05" />
                  <Point X="-4.142565118095" Y="-2.931635083954" Z="2.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.05" />
                  <Point X="-2.820937184329" Y="-2.698730234309" Z="2.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.05" />
                  <Point X="-2.569501409736" Y="-2.558829059395" Z="2.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.05" />
                  <Point X="-3.156686778859" Y="-3.614141198163" Z="2.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.05" />
                  <Point X="-3.22114228452" Y="-3.922899753615" Z="2.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.05" />
                  <Point X="-2.805937203153" Y="-4.229846176627" Z="2.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.05" />
                  <Point X="-2.269495174205" Y="-4.212846497244" Z="2.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.05" />
                  <Point X="-2.176586114454" Y="-4.123286306883" Z="2.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.05" />
                  <Point X="-1.908686904358" Y="-3.987988736587" Z="2.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.05" />
                  <Point X="-1.61378433819" Y="-4.043736893921" Z="2.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.05" />
                  <Point X="-1.413759689477" Y="-4.26749030341" Z="2.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.05" />
                  <Point X="-1.403820778501" Y="-4.809027807669" Z="2.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.05" />
                  <Point X="-1.356202952323" Y="-4.894142121602" Z="2.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.05" />
                  <Point X="-1.059729054718" Y="-4.966778062887" Z="2.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="-0.494163979497" Y="-3.806429123842" Z="2.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="-0.385583380809" Y="-3.473382686698" Z="2.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="-0.204609404142" Y="-3.267742576692" Z="2.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.05" />
                  <Point X="0.048749675218" Y="-3.219369468596" Z="2.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.05" />
                  <Point X="0.284862478624" Y="-3.32826306907" Z="2.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.05" />
                  <Point X="0.740590880904" Y="-4.726106905974" Z="2.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.05" />
                  <Point X="0.852368308019" Y="-5.007459185956" Z="2.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.05" />
                  <Point X="1.032460390834" Y="-4.973449906005" Z="2.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.05" />
                  <Point X="0.999620385556" Y="-3.594021136898" Z="2.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.05" />
                  <Point X="0.96770036171" Y="-3.225274513428" Z="2.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.05" />
                  <Point X="1.04579129374" Y="-2.996531241797" Z="2.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.05" />
                  <Point X="1.235992750459" Y="-2.871548394122" Z="2.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.05" />
                  <Point X="1.465238256601" Y="-2.880590815793" Z="2.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.05" />
                  <Point X="2.464882077291" Y="-4.069700770901" Z="2.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.05" />
                  <Point X="2.699610938868" Y="-4.302335992734" Z="2.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.05" />
                  <Point X="2.893685697903" Y="-4.174275675276" Z="2.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.05" />
                  <Point X="2.420410310898" Y="-2.980674446849" Z="2.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.05" />
                  <Point X="2.263727813843" Y="-2.680719931401" Z="2.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.05" />
                  <Point X="2.250390152276" Y="-2.471666520364" Z="2.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.05" />
                  <Point X="2.361231858008" Y="-2.308511157691" Z="2.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.05" />
                  <Point X="2.547786872111" Y="-2.239720195648" Z="2.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.05" />
                  <Point X="3.806739150258" Y="-2.897339189377" Z="2.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.05" />
                  <Point X="4.098711750201" Y="-2.998776213844" Z="2.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.05" />
                  <Point X="4.270409547741" Y="-2.748763045649" Z="2.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.05" />
                  <Point X="3.424882964217" Y="-1.792720447032" Z="2.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.05" />
                  <Point X="3.173409067564" Y="-1.584520640644" Z="2.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.05" />
                  <Point X="3.095288966916" Y="-1.425413228641" Z="2.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.05" />
                  <Point X="3.129107684116" Y="-1.261975965772" Z="2.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.05" />
                  <Point X="3.252670934429" Y="-1.147790828024" Z="2.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.05" />
                  <Point X="4.616903773121" Y="-1.276221008595" Z="2.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.05" />
                  <Point X="4.923252518858" Y="-1.243222568524" Z="2.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.05" />
                  <Point X="5.00232450809" Y="-0.872217453006" Z="2.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.05" />
                  <Point X="3.998102095533" Y="-0.287837962411" Z="2.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.05" />
                  <Point X="3.730152541352" Y="-0.210521826315" Z="2.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.05" />
                  <Point X="3.644762619705" Y="-0.153729256581" Z="2.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.05" />
                  <Point X="3.596376692047" Y="-0.078021372973" Z="2.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.05" />
                  <Point X="3.584994758377" Y="0.018589158214" Z="2.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.05" />
                  <Point X="3.610616818695" Y="0.110219481994" Z="2.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.05" />
                  <Point X="3.673242873001" Y="0.177627017927" Z="2.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.05" />
                  <Point X="4.797865721773" Y="0.502133970358" Z="2.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.05" />
                  <Point X="5.035334646635" Y="0.65060589122" Z="2.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.05" />
                  <Point X="4.962616133929" Y="1.072527370675" Z="2.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.05" />
                  <Point X="3.735898937029" Y="1.25793593636" Z="2.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.05" />
                  <Point X="3.445003617907" Y="1.224418570627" Z="2.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.05" />
                  <Point X="3.355560553918" Y="1.242011655283" Z="2.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.05" />
                  <Point X="3.290071635506" Y="1.287725937644" Z="2.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.05" />
                  <Point X="3.247861251184" Y="1.363193300112" Z="2.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.05" />
                  <Point X="3.237733529699" Y="1.447158203649" Z="2.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.05" />
                  <Point X="3.266234048747" Y="1.523818091726" Z="2.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.05" />
                  <Point X="4.22903550732" Y="2.287671988631" Z="2.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.05" />
                  <Point X="4.407072872855" Y="2.521656713079" Z="2.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.05" />
                  <Point X="4.192789270756" Y="2.863835735763" Z="2.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.05" />
                  <Point X="2.797032414539" Y="2.43278748303" Z="2.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.05" />
                  <Point X="2.494429769152" Y="2.262867731022" Z="2.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.05" />
                  <Point X="2.416233382731" Y="2.247139935107" Z="2.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.05" />
                  <Point X="2.347985344505" Y="2.262166133984" Z="2.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.05" />
                  <Point X="2.288592466251" Y="2.309039515877" Z="2.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.05" />
                  <Point X="2.252289767652" Y="2.373525061101" Z="2.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.05" />
                  <Point X="2.249660211635" Y="2.44503980673" Z="2.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.05" />
                  <Point X="2.96283786983" Y="3.715106667416" Z="2.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.05" />
                  <Point X="3.056446797409" Y="4.053591439075" Z="2.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.05" />
                  <Point X="2.676968282912" Y="4.313618298063" Z="2.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.05" />
                  <Point X="2.276540076682" Y="4.537803183401" Z="2.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.05" />
                  <Point X="1.861814224208" Y="4.723127164625" Z="2.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.05" />
                  <Point X="1.390864807794" Y="4.878678580506" Z="2.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.05" />
                  <Point X="0.733773678584" Y="5.019715197532" Z="2.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.05" />
                  <Point X="0.037182838039" Y="4.493892721383" Z="2.05" />
                  <Point X="0" Y="4.355124473572" Z="2.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>