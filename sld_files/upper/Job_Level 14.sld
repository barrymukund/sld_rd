<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#143" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1141" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004716796875" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.666013305664" Y="21.10415234375" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557719848633" Y="21.502861328125" />
                  <Point X="0.542362548828" Y="21.532623046875" />
                  <Point X="0.500381896973" Y="21.593107421875" />
                  <Point X="0.378634918213" Y="21.7685234375" />
                  <Point X="0.356755554199" Y="21.790974609375" />
                  <Point X="0.330499908447" Y="21.810220703125" />
                  <Point X="0.302495178223" Y="21.82433203125" />
                  <Point X="0.237532531738" Y="21.844494140625" />
                  <Point X="0.049136188507" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.008664536476" Y="21.907234375" />
                  <Point X="-0.036824085236" Y="21.90296484375" />
                  <Point X="-0.101786888123" Y="21.882802734375" />
                  <Point X="-0.290183227539" Y="21.82433203125" />
                  <Point X="-0.31818460083" Y="21.810224609375" />
                  <Point X="-0.344440093994" Y="21.79098046875" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.408304260254" Y="21.708037109375" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.848860107422" Y="20.37580859375" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-1.079359741211" Y="20.154654296875" />
                  <Point X="-1.150935424805" Y="20.173068359375" />
                  <Point X="-1.24641809082" Y="20.197634765625" />
                  <Point X="-1.227326904297" Y="20.342646484375" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.232028198242" Y="20.561796875" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323645019531" Y="20.868796875" />
                  <Point X="-1.383454467773" Y="20.921248046875" />
                  <Point X="-1.556905761719" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.64302722168" Y="21.109033203125" />
                  <Point X="-1.722407592773" Y="21.114236328125" />
                  <Point X="-1.952616333008" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657226562" Y="21.10519921875" />
                  <Point X="-2.108801025391" Y="21.06100390625" />
                  <Point X="-2.300623535156" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.4801484375" Y="20.711509765625" />
                  <Point X="-2.481283447266" Y="20.712212890625" />
                  <Point X="-2.801720947266" Y="20.910619140625" />
                  <Point X="-2.900843261719" Y="20.98694140625" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.614638427734" Y="21.992771484375" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858398438" Y="22.352349609375" />
                  <Point X="-2.406587890625" Y="22.38387890625" />
                  <Point X="-2.405576660156" Y="22.41481640625" />
                  <Point X="-2.414563476562" Y="22.44443359375" />
                  <Point X="-2.428783691406" Y="22.473263671875" />
                  <Point X="-2.446810546875" Y="22.49841796875" />
                  <Point X="-2.464155517578" Y="22.51576171875" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.597973876953" Y="21.9852421875" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.829706298828" Y="21.873546875" />
                  <Point X="-4.082863037109" Y="22.206142578125" />
                  <Point X="-4.153930175781" Y="22.325310546875" />
                  <Point X="-4.306143066406" Y="22.580548828125" />
                  <Point X="-3.439927734375" Y="23.24521875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.063814941406" Y="23.554951171875" />
                  <Point X="-3.055125732422" Y="23.5776484375" />
                  <Point X="-3.051881103516" Y="23.58779296875" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037353516" Y="23.64139453125" />
                  <Point X="-3.042736328125" Y="23.664263671875" />
                  <Point X="-3.048882568359" Y="23.68630078125" />
                  <Point X="-3.060887939453" Y="23.71528515625" />
                  <Point X="-3.073649658203" Y="23.73723046875" />
                  <Point X="-3.091767578125" Y="23.755013671875" />
                  <Point X="-3.111060302734" Y="23.769439453125" />
                  <Point X="-3.119763916016" Y="23.77523046875" />
                  <Point X="-3.139456298828" Y="23.7868203125" />
                  <Point X="-3.168721679688" Y="23.798044921875" />
                  <Point X="-3.200608642578" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.442311035156" Y="23.646265625" />
                  <Point X="-4.732102539062" Y="23.608115234375" />
                  <Point X="-4.735067382812" Y="23.61972265625" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.852880371094" Y="24.1388125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.913321777344" Y="24.677650390625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.512578125" Y="24.787626953125" />
                  <Point X="-3.489895263672" Y="24.799787109375" />
                  <Point X="-3.480616210938" Y="24.805466796875" />
                  <Point X="-3.459979248047" Y="24.8197890625" />
                  <Point X="-3.436023193359" Y="24.84131640625" />
                  <Point X="-3.415299316406" Y="24.87125390625" />
                  <Point X="-3.405501220703" Y="24.893634765625" />
                  <Point X="-3.401796142578" Y="24.90357421875" />
                  <Point X="-3.394917236328" Y="24.92573828125" />
                  <Point X="-3.389474365234" Y="24.9544765625" />
                  <Point X="-3.390347412109" Y="24.9878515625" />
                  <Point X="-3.394966064453" Y="25.010314453125" />
                  <Point X="-3.397288574219" Y="25.01933984375" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.417488037109" Y="25.070837890625" />
                  <Point X="-3.440013427734" Y="25.099796875" />
                  <Point X="-3.458951904297" Y="25.11625390625" />
                  <Point X="-3.467096923828" Y="25.122587890625" />
                  <Point X="-3.4877265625" Y="25.13690625" />
                  <Point X="-3.501915771484" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.636196289062" Y="25.453482421875" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.888298339844" Y="25.545748046875" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.786637207031" Y="26.116654296875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.038379150391" Y="26.3356953125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703142822266" Y="26.30526171875" />
                  <Point X="-3.687392822266" Y="26.3102265625" />
                  <Point X="-3.641716796875" Y="26.32462890625" />
                  <Point X="-3.622776611328" Y="26.332962890625" />
                  <Point X="-3.604032714844" Y="26.34378515625" />
                  <Point X="-3.587353027344" Y="26.356015625" />
                  <Point X="-3.573715576172" Y="26.37156640625" />
                  <Point X="-3.561301513672" Y="26.389294921875" />
                  <Point X="-3.551351318359" Y="26.407431640625" />
                  <Point X="-3.545031494141" Y="26.422689453125" />
                  <Point X="-3.526703857422" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.539675537109" Y="26.604025390625" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.235002929688" Y="27.18020703125" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.329097167969" Y="27.308873046875" />
                  <Point X="-4.081155761719" Y="27.733654296875" />
                  <Point X="-3.980890136719" Y="27.862533203125" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.374149414062" Y="27.941373046875" />
                  <Point X="-3.206656738281" Y="27.844671875" />
                  <Point X="-3.187728271484" Y="27.836341796875" />
                  <Point X="-3.167085449219" Y="27.82983203125" />
                  <Point X="-3.146797363281" Y="27.825794921875" />
                  <Point X="-3.124862060547" Y="27.823875" />
                  <Point X="-3.061248046875" Y="27.818310546875" />
                  <Point X="-3.040559570312" Y="27.81876171875" />
                  <Point X="-3.0191015625" Y="27.821587890625" />
                  <Point X="-2.999012207031" Y="27.826505859375" />
                  <Point X="-2.980462890625" Y="27.83565234375" />
                  <Point X="-2.962209228516" Y="27.84728125" />
                  <Point X="-2.946076660156" Y="27.86023046875" />
                  <Point X="-2.930506835938" Y="27.87580078125" />
                  <Point X="-2.885353027344" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845354980469" Y="28.0580546875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.150237548828" Y="28.6672734375" />
                  <Point X="-3.183333251953" Y="28.724595703125" />
                  <Point X="-3.132461181641" Y="28.763599609375" />
                  <Point X="-2.70062890625" Y="29.094681640625" />
                  <Point X="-2.542716552734" Y="29.1824140625" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.094469970703" Y="29.296564453125" />
                  <Point X="-2.04319519043" Y="29.2297421875" />
                  <Point X="-2.028890869141" Y="29.214798828125" />
                  <Point X="-2.012310791016" Y="29.20088671875" />
                  <Point X="-1.995117797852" Y="29.1893984375" />
                  <Point X="-1.970703979492" Y="29.1766875" />
                  <Point X="-1.899901977539" Y="29.139830078125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718383789" Y="29.126728515625" />
                  <Point X="-1.839268432617" Y="29.123580078125" />
                  <Point X="-1.818621826172" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.777452880859" Y="29.134482421875" />
                  <Point X="-1.752024169922" Y="29.145015625" />
                  <Point X="-1.678278930664" Y="29.1755625" />
                  <Point X="-1.660145507812" Y="29.185509765625" />
                  <Point X="-1.642416137695" Y="29.197923828125" />
                  <Point X="-1.626864013672" Y="29.2115625" />
                  <Point X="-1.61463269043" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.587203491211" Y="29.292171875" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-1.508673095703" Y="29.653072265625" />
                  <Point X="-0.94962310791" Y="29.80980859375" />
                  <Point X="-0.758207580566" Y="29.832212890625" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.183452407837" Y="29.471234375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113998413" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.0061559062" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425956726" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.289910827637" Y="29.82259375" />
                  <Point X="0.307419677734" Y="29.8879375" />
                  <Point X="0.355912963867" Y="29.882859375" />
                  <Point X="0.84403125" Y="29.831740234375" />
                  <Point X="1.00241973877" Y="29.7935" />
                  <Point X="1.48103894043" Y="29.6779453125" />
                  <Point X="1.583013305664" Y="29.640958984375" />
                  <Point X="1.894647583008" Y="29.527927734375" />
                  <Point X="1.994331298828" Y="29.48130859375" />
                  <Point X="2.294560546875" Y="29.340900390625" />
                  <Point X="2.390908203125" Y="29.28476953125" />
                  <Point X="2.680969970703" Y="29.115779296875" />
                  <Point X="2.7718046875" Y="29.051181640625" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.368507080078" Y="27.933751953125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.127571777344" Y="27.49546875" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108405273438" Y="27.4091328125" />
                  <Point X="2.109214111328" Y="27.3724296875" />
                  <Point X="2.109874267578" Y="27.363150390625" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140071533203" Y="27.247470703125" />
                  <Point X="2.151086181641" Y="27.23123828125" />
                  <Point X="2.183029296875" Y="27.184162109375" />
                  <Point X="2.201254394531" Y="27.164166015625" />
                  <Point X="2.230786865234" Y="27.139849609375" />
                  <Point X="2.237830810547" Y="27.134576171875" />
                  <Point X="2.284906738281" Y="27.1026328125" />
                  <Point X="2.304954345703" Y="27.09226953125" />
                  <Point X="2.327043457031" Y="27.08400390625" />
                  <Point X="2.348964599609" Y="27.078662109375" />
                  <Point X="2.366765625" Y="27.076515625" />
                  <Point X="2.418389404297" Y="27.070291015625" />
                  <Point X="2.436469482422" Y="27.06984375" />
                  <Point X="2.473210205078" Y="27.074171875" />
                  <Point X="2.493796142578" Y="27.079677734375" />
                  <Point X="2.553496582031" Y="27.095642578125" />
                  <Point X="2.565287353516" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.69826171875" Y="27.75084765625" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.123274414062" Y="27.689458984375" />
                  <Point X="4.173907226562" Y="27.605787109375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.521653564453" Y="26.89164453125" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221421386719" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.189158203125" Y="26.622298828125" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606201172" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.116111083984" Y="26.4973515625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.10226953125" Y="26.349810546875" />
                  <Point X="3.115408203125" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136283447266" Y="26.23573828125" />
                  <Point X="3.148605712891" Y="26.217009765625" />
                  <Point X="3.184341064453" Y="26.162693359375" />
                  <Point X="3.198894042969" Y="26.145451171875" />
                  <Point X="3.216138183594" Y="26.129361328125" />
                  <Point X="3.234343505859" Y="26.116037109375" />
                  <Point X="3.252199951172" Y="26.105984375" />
                  <Point X="3.303985595703" Y="26.076833984375" />
                  <Point X="3.320520019531" Y="26.069501953125" />
                  <Point X="3.356123535156" Y="26.0594375" />
                  <Point X="3.380266845703" Y="26.056248046875" />
                  <Point X="3.450284179688" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.542370605469" Y="26.185767578125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.861892578125" Y="25.830322265625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.049922851562" Y="25.41891015625" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704787841797" Y="25.325583984375" />
                  <Point X="3.681550048828" Y="25.3150703125" />
                  <Point X="3.657830078125" Y="25.301361328125" />
                  <Point X="3.589040039062" Y="25.261599609375" />
                  <Point X="3.5743125" Y="25.25109765625" />
                  <Point X="3.547528076172" Y="25.22557421875" />
                  <Point X="3.533296142578" Y="25.2074375" />
                  <Point X="3.492022216797" Y="25.154845703125" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.458936767578" Y="25.06783203125" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.449923095703" Y="24.916673828125" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492024902344" Y="24.782591796875" />
                  <Point X="3.506256835938" Y="24.76445703125" />
                  <Point X="3.547530761719" Y="24.71186328125" />
                  <Point X="3.559994873047" Y="24.698767578125" />
                  <Point X="3.58903515625" Y="24.67584375" />
                  <Point X="3.612755371094" Y="24.6621328125" />
                  <Point X="3.681545166016" Y="24.62237109375" />
                  <Point X="3.692708496094" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.683299316406" Y="24.34881640625" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.834579589844" Y="23.961689453125" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.810966552734" Y="23.9456640625" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658203125" Y="23.994490234375" />
                  <Point X="3.328104003906" Y="23.98437109375" />
                  <Point X="3.193093994141" Y="23.95502734375" />
                  <Point X="3.163973876953" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112396728516" Y="23.9060390625" />
                  <Point X="3.084257568359" Y="23.8721953125" />
                  <Point X="3.002652587891" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.965724609375" Y="23.650806640625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976451416016" Y="23.432" />
                  <Point X="3.002215087891" Y="23.391927734375" />
                  <Point X="3.076931640625" Y="23.2757109375" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="4.007749511719" Y="22.550703125" />
                  <Point X="4.213121582031" Y="22.393115234375" />
                  <Point X="4.12480859375" Y="22.2502109375" />
                  <Point X="4.082531494141" Y="22.190142578125" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.145260009766" Y="22.62426953125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754223632813" Y="22.840173828125" />
                  <Point X="2.698816894531" Y="22.8501796875" />
                  <Point X="2.538133300781" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833984375" Y="22.864822265625" />
                  <Point X="2.398804443359" Y="22.84059765625" />
                  <Point X="2.265315917969" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.180306884766" Y="22.66353125" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.436740234375" />
                  <Point X="2.105681884766" Y="22.381333984375" />
                  <Point X="2.134701171875" Y="22.2206484375" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.728309814453" Y="21.17541015625" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781838134766" Y="20.88834765625" />
                  <Point X="2.734581298828" Y="20.857759765625" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="2.021282958984" Y="21.723337890625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.667278320312" Y="22.134576171875" />
                  <Point X="1.50880090332" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417102172852" Y="22.258880859375" />
                  <Point X="1.357337280273" Y="22.2533828125" />
                  <Point X="1.184015258789" Y="22.23743359375" />
                  <Point X="1.1563671875" Y="22.230603515625" />
                  <Point X="1.128982055664" Y="22.21926171875" />
                  <Point X="1.104594116211" Y="22.2045390625" />
                  <Point X="1.0584453125" Y="22.166166015625" />
                  <Point X="0.924610290527" Y="22.05488671875" />
                  <Point X="0.904140014648" Y="22.031134765625" />
                  <Point X="0.887247680664" Y="22.003306640625" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.86182611084" Y="21.91070703125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.98311517334" Y="20.435939453125" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.975721313477" Y="20.12992578125" />
                  <Point X="0.932008239746" Y="20.121984375" />
                  <Point X="0.929315612793" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058464355469" Y="20.24737109375" />
                  <Point X="-1.127265869141" Y="20.265072265625" />
                  <Point X="-1.141246337891" Y="20.268669921875" />
                  <Point X="-1.133139648438" Y="20.33024609375" />
                  <Point X="-1.120775756836" Y="20.424158203125" />
                  <Point X="-1.120077392578" Y="20.4318984375" />
                  <Point X="-1.119451660156" Y="20.458962890625" />
                  <Point X="-1.121759033203" Y="20.49066796875" />
                  <Point X="-1.123333862305" Y="20.502306640625" />
                  <Point X="-1.138853515625" Y="20.580330078125" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026611328" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230573486328" Y="20.905138671875" />
                  <Point X="-1.250208496094" Y="20.929064453125" />
                  <Point X="-1.261007324219" Y="20.94022265625" />
                  <Point X="-1.320816772461" Y="20.992673828125" />
                  <Point X="-1.494267944336" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.53302355957" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.621456298828" Y="21.201552734375" />
                  <Point X="-1.636813598633" Y="21.203830078125" />
                  <Point X="-1.716193969727" Y="21.209033203125" />
                  <Point X="-1.946402709961" Y="21.22412109375" />
                  <Point X="-1.961927368164" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999633789" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.05366796875" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095436035156" Y="21.184189453125" />
                  <Point X="-2.161579833984" Y="21.139994140625" />
                  <Point X="-2.35340234375" Y="21.011822265625" />
                  <Point X="-2.359681396484" Y="21.007244140625" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201416016" Y="20.837521484375" />
                  <Point X="-2.747605712891" Y="20.988849609375" />
                  <Point X="-2.842885253906" Y="21.062212890625" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.532365966797" Y="21.945271484375" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947021484" Y="22.31888671875" />
                  <Point X="-2.319683105469" Y="22.333818359375" />
                  <Point X="-2.313412597656" Y="22.36534765625" />
                  <Point X="-2.311638671875" Y="22.380775390625" />
                  <Point X="-2.310627441406" Y="22.411712890625" />
                  <Point X="-2.314669433594" Y="22.442400390625" />
                  <Point X="-2.32365625" Y="22.472017578125" />
                  <Point X="-2.329363769531" Y="22.48645703125" />
                  <Point X="-2.343583984375" Y="22.515287109375" />
                  <Point X="-2.351565429688" Y="22.5286015625" />
                  <Point X="-2.369592285156" Y="22.553755859375" />
                  <Point X="-2.379637695313" Y="22.565595703125" />
                  <Point X="-2.396982666016" Y="22.582939453125" />
                  <Point X="-2.408821777344" Y="22.592982421875" />
                  <Point X="-2.433977294922" Y="22.6110078125" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.645473876953" Y="22.067513671875" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-4.004020019531" Y="22.259412109375" />
                  <Point X="-4.072337646484" Y="22.37396875" />
                  <Point X="-4.181265625" Y="22.556625" />
                  <Point X="-3.382095458984" Y="23.169849609375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.002480224609" Y="23.473806640625" />
                  <Point X="-2.983230224609" Y="23.504640625" />
                  <Point X="-2.975094238281" Y="23.520986328125" />
                  <Point X="-2.966405029297" Y="23.54368359375" />
                  <Point X="-2.959915771484" Y="23.56397265625" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953369141" Y="23.597599609375" />
                  <Point X="-2.947838623047" Y="23.62908203125" />
                  <Point X="-2.947081787109" Y="23.644296875" />
                  <Point X="-2.947780761719" Y="23.667166015625" />
                  <Point X="-2.951228759766" Y="23.68978515625" />
                  <Point X="-2.957375" Y="23.711822265625" />
                  <Point X="-2.961113525391" Y="23.722654296875" />
                  <Point X="-2.973118896484" Y="23.751638671875" />
                  <Point X="-2.978764404297" Y="23.763041015625" />
                  <Point X="-2.991526123047" Y="23.784986328125" />
                  <Point X="-3.007103759766" Y="23.805029296875" />
                  <Point X="-3.025221679688" Y="23.8228125" />
                  <Point X="-3.034878173828" Y="23.831095703125" />
                  <Point X="-3.054170898438" Y="23.845521484375" />
                  <Point X="-3.071578125" Y="23.857103515625" />
                  <Point X="-3.091270507812" Y="23.868693359375" />
                  <Point X="-3.105436035156" Y="23.87551953125" />
                  <Point X="-3.134701416016" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891142578125" />
                  <Point X="-3.181688232422" Y="23.897623046875" />
                  <Point X="-3.197304931641" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.4547109375" Y="23.740453125" />
                  <Point X="-4.660921386719" Y="23.713306640625" />
                  <Point X="-4.740762207031" Y="24.02587890625" />
                  <Point X="-4.758837402344" Y="24.15226171875" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.888733886719" Y="24.58588671875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497901855469" Y="24.69126171875" />
                  <Point X="-3.477604003906" Y="24.699298828125" />
                  <Point X="-3.467692382812" Y="24.703900390625" />
                  <Point X="-3.445009521484" Y="24.716060546875" />
                  <Point X="-3.426451416016" Y="24.727419921875" />
                  <Point X="-3.405814453125" Y="24.7417421875" />
                  <Point X="-3.396481445313" Y="24.749126953125" />
                  <Point X="-3.372525390625" Y="24.770654296875" />
                  <Point X="-3.357912353516" Y="24.78724609375" />
                  <Point X="-3.337188476562" Y="24.81718359375" />
                  <Point X="-3.328273681641" Y="24.833154296875" />
                  <Point X="-3.318475585938" Y="24.85553515625" />
                  <Point X="-3.311065429688" Y="24.8754140625" />
                  <Point X="-3.304186523438" Y="24.897578125" />
                  <Point X="-3.301576660156" Y="24.908060546875" />
                  <Point X="-3.296133789062" Y="24.936798828125" />
                  <Point X="-3.294506835938" Y="24.9569609375" />
                  <Point X="-3.295379882812" Y="24.9903359375" />
                  <Point X="-3.297293945312" Y="25.006984375" />
                  <Point X="-3.301912597656" Y="25.029447265625" />
                  <Point X="-3.306557617188" Y="25.047498046875" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.317669189453" Y="25.080787109375" />
                  <Point X="-3.330989501953" Y="25.110119140625" />
                  <Point X="-3.342501708984" Y="25.129166015625" />
                  <Point X="-3.365027099609" Y="25.158125" />
                  <Point X="-3.377700683594" Y="25.171505859375" />
                  <Point X="-3.396639160156" Y="25.187962890625" />
                  <Point X="-3.412929199219" Y="25.200630859375" />
                  <Point X="-3.433558837891" Y="25.21494921875" />
                  <Point X="-3.440476806641" Y="25.219322265625" />
                  <Point X="-3.465598388672" Y="25.23282421875" />
                  <Point X="-3.49655859375" Y="25.2456328125" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.611608398438" Y="25.54524609375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.694944335938" Y="26.091806640625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.050779296875" Y="26.2415078125" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736704101562" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704893066406" Y="26.208052734375" />
                  <Point X="-3.684612792969" Y="26.2120859375" />
                  <Point X="-3.658831542969" Y="26.21962109375" />
                  <Point X="-3.613155517578" Y="26.2340234375" />
                  <Point X="-3.603455566406" Y="26.237673828125" />
                  <Point X="-3.584515380859" Y="26.2460078125" />
                  <Point X="-3.575275146484" Y="26.25069140625" />
                  <Point X="-3.55653125" Y="26.261513671875" />
                  <Point X="-3.547857177734" Y="26.267173828125" />
                  <Point X="-3.531177490234" Y="26.279404296875" />
                  <Point X="-3.515927734375" Y="26.29337890625" />
                  <Point X="-3.502290283203" Y="26.3089296875" />
                  <Point X="-3.495896972656" Y="26.317076171875" />
                  <Point X="-3.483482910156" Y="26.3348046875" />
                  <Point X="-3.478012451172" Y="26.3436015625" />
                  <Point X="-3.468062255859" Y="26.36173828125" />
                  <Point X="-3.457262695312" Y="26.3863359375" />
                  <Point X="-3.438935058594" Y="26.43058203125" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429711181641" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012695312" Y="26.60453125" />
                  <Point X="-3.443509277344" Y="26.62380859375" />
                  <Point X="-3.455409423828" Y="26.647890625" />
                  <Point X="-3.477523681641" Y="26.69037109375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494291748047" Y="26.716484375" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.177170410156" Y="27.255576171875" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.002294433594" Y="27.68030859375" />
                  <Point X="-3.905909179688" Y="27.80419921875" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.421649414062" Y="27.8591015625" />
                  <Point X="-3.254156738281" Y="27.762400390625" />
                  <Point X="-3.244922851562" Y="27.75771875" />
                  <Point X="-3.225994384766" Y="27.749388671875" />
                  <Point X="-3.216299804688" Y="27.745740234375" />
                  <Point X="-3.195656982422" Y="27.73923046875" />
                  <Point X="-3.185625976562" Y="27.736658203125" />
                  <Point X="-3.165337890625" Y="27.73262109375" />
                  <Point X="-3.155080810547" Y="27.73115625" />
                  <Point X="-3.133145507812" Y="27.729236328125" />
                  <Point X="-3.069531494141" Y="27.723671875" />
                  <Point X="-3.059176757812" Y="27.723333984375" />
                  <Point X="-3.03848828125" Y="27.72378515625" />
                  <Point X="-3.028154541016" Y="27.72457421875" />
                  <Point X="-3.006696533203" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976422851562" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.93844921875" Y="27.750447265625" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911165527344" Y="27.767158203125" />
                  <Point X="-2.902742431641" Y="27.7731953125" />
                  <Point X="-2.886609863281" Y="27.78614453125" />
                  <Point X="-2.878900390625" Y="27.793056640625" />
                  <Point X="-2.863330566406" Y="27.808626953125" />
                  <Point X="-2.818176757812" Y="27.853779296875" />
                  <Point X="-2.811265869141" Y="27.86148828125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.750716552734" Y="28.066333984375" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059387695312" Y="28.699916015625" />
                  <Point X="-2.648383789062" Y="29.015029296875" />
                  <Point X="-2.496579101562" Y="29.099369140625" />
                  <Point X="-2.192523681641" Y="29.268296875" />
                  <Point X="-2.169838623047" Y="29.238732421875" />
                  <Point X="-2.118563720703" Y="29.17191015625" />
                  <Point X="-2.111821777344" Y="29.16405078125" />
                  <Point X="-2.097517333984" Y="29.149107421875" />
                  <Point X="-2.089955078125" Y="29.1420234375" />
                  <Point X="-2.073375" Y="29.128111328125" />
                  <Point X="-2.065090820313" Y="29.1218984375" />
                  <Point X="-2.047897949219" Y="29.11041015625" />
                  <Point X="-2.038989135742" Y="29.105134765625" />
                  <Point X="-2.014575195313" Y="29.092423828125" />
                  <Point X="-1.94377331543" Y="29.05556640625" />
                  <Point X="-1.934340332031" Y="29.051291015625" />
                  <Point X="-1.915063476562" Y="29.04379296875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.88431262207" Y="29.034966796875" />
                  <Point X="-1.874173950195" Y="29.032833984375" />
                  <Point X="-1.853724121094" Y="29.029685546875" />
                  <Point X="-1.833053955078" Y="29.028783203125" />
                  <Point X="-1.812407348633" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.780804443359" Y="29.03513671875" />
                  <Point X="-1.770713256836" Y="29.0374921875" />
                  <Point X="-1.750859619141" Y="29.04328125" />
                  <Point X="-1.741097167969" Y="29.04671484375" />
                  <Point X="-1.715668457031" Y="29.057248046875" />
                  <Point X="-1.641923217773" Y="29.087794921875" />
                  <Point X="-1.632588867188" Y="29.092271484375" />
                  <Point X="-1.614455322266" Y="29.10221875" />
                  <Point X="-1.60565625" Y="29.107689453125" />
                  <Point X="-1.587926879883" Y="29.120103515625" />
                  <Point X="-1.579778686523" Y="29.126498046875" />
                  <Point X="-1.5642265625" Y="29.14013671875" />
                  <Point X="-1.550251464844" Y="29.155388671875" />
                  <Point X="-1.538020141602" Y="29.1720703125" />
                  <Point X="-1.532360229492" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.516855712891" Y="29.208728515625" />
                  <Point X="-1.508525146484" Y="29.227662109375" />
                  <Point X="-1.504876953125" Y="29.23735546875" />
                  <Point X="-1.496600341797" Y="29.26360546875" />
                  <Point X="-1.47259753418" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-0.93116394043" Y="29.7163203125" />
                  <Point X="-0.747163696289" Y="29.737857421875" />
                  <Point X="-0.365222595215" Y="29.78255859375" />
                  <Point X="-0.275215332031" Y="29.446646484375" />
                  <Point X="-0.225666244507" Y="29.2617265625" />
                  <Point X="-0.220435317993" Y="29.247107421875" />
                  <Point X="-0.207661773682" Y="29.218916015625" />
                  <Point X="-0.200119155884" Y="29.20534375" />
                  <Point X="-0.182260925293" Y="29.1786171875" />
                  <Point X="-0.172608901978" Y="29.166455078125" />
                  <Point X="-0.151451248169" Y="29.143865234375" />
                  <Point X="-0.126896362305" Y="29.1250234375" />
                  <Point X="-0.09960043335" Y="29.11043359375" />
                  <Point X="-0.085354034424" Y="29.1042578125" />
                  <Point X="-0.054915912628" Y="29.09392578125" />
                  <Point X="-0.03985357666" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629491806" Y="29.08511328125" />
                  <Point X="0.052165405273" Y="29.090154296875" />
                  <Point X="0.067227592468" Y="29.09392578125" />
                  <Point X="0.097665863037" Y="29.1042578125" />
                  <Point X="0.111912406921" Y="29.11043359375" />
                  <Point X="0.139208343506" Y="29.1250234375" />
                  <Point X="0.16376322937" Y="29.143865234375" />
                  <Point X="0.184920883179" Y="29.166455078125" />
                  <Point X="0.194572906494" Y="29.1786171875" />
                  <Point X="0.212431137085" Y="29.20534375" />
                  <Point X="0.219973754883" Y="29.218916015625" />
                  <Point X="0.232747146606" Y="29.247107421875" />
                  <Point X="0.237978225708" Y="29.2617265625" />
                  <Point X="0.378190979004" Y="29.7850078125" />
                  <Point X="0.827865600586" Y="29.7379140625" />
                  <Point X="0.980124206543" Y="29.701154296875" />
                  <Point X="1.453606567383" Y="29.58683984375" />
                  <Point X="1.550621459961" Y="29.55165234375" />
                  <Point X="1.858246948242" Y="29.44007421875" />
                  <Point X="1.954086425781" Y="29.39525390625" />
                  <Point X="2.250439697266" Y="29.256658203125" />
                  <Point X="2.343086181641" Y="29.20268359375" />
                  <Point X="2.629419677734" Y="29.035865234375" />
                  <Point X="2.716747558594" Y="28.97376171875" />
                  <Point X="2.817778564453" Y="28.9019140625" />
                  <Point X="2.286234619141" Y="27.981251953125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181762695" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.035796630859" Y="27.520009765625" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017285888672" Y="27.44710546875" />
                  <Point X="2.014084228516" Y="27.420470703125" />
                  <Point X="2.013428344727" Y="27.4070390625" />
                  <Point X="2.014237182617" Y="27.3703359375" />
                  <Point X="2.015557495117" Y="27.35177734375" />
                  <Point X="2.021782348633" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045319091797" Y="27.22388671875" />
                  <Point X="2.055682128906" Y="27.203841796875" />
                  <Point X="2.072475585938" Y="27.177896484375" />
                  <Point X="2.104418701172" Y="27.1308203125" />
                  <Point X="2.112816894531" Y="27.12016796875" />
                  <Point X="2.131041992188" Y="27.100171875" />
                  <Point X="2.140868896484" Y="27.090828125" />
                  <Point X="2.170401367188" Y="27.06651171875" />
                  <Point X="2.184489257812" Y="27.05596484375" />
                  <Point X="2.231565185547" Y="27.024021484375" />
                  <Point X="2.241281982422" Y="27.0182421875" />
                  <Point X="2.261329589844" Y="27.00787890625" />
                  <Point X="2.271660400391" Y="27.003294921875" />
                  <Point X="2.293749511719" Y="26.995029296875" />
                  <Point X="2.304551757812" Y="26.991705078125" />
                  <Point X="2.326472900391" Y="26.98636328125" />
                  <Point X="2.355392822266" Y="26.98219921875" />
                  <Point X="2.407016601562" Y="26.975974609375" />
                  <Point X="2.416040039062" Y="26.9753203125" />
                  <Point X="2.447583740234" Y="26.97549609375" />
                  <Point X="2.484324462891" Y="26.97982421875" />
                  <Point X="2.497755859375" Y="26.9823984375" />
                  <Point X="2.518341796875" Y="26.987904296875" />
                  <Point X="2.578042236328" Y="27.003869140625" />
                  <Point X="2.584003417969" Y="27.005673828125" />
                  <Point X="2.604410888672" Y="27.0130703125" />
                  <Point X="2.627657470703" Y="27.023576171875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.74576171875" Y="27.668576171875" />
                  <Point X="3.940403564453" Y="27.780953125" />
                  <Point X="4.043955322266" Y="27.6370390625" />
                  <Point X="4.092630126953" Y="27.556603515625" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.463821289062" Y="26.967013671875" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168135742188" Y="26.7398671875" />
                  <Point X="3.152114257812" Y="26.7252109375" />
                  <Point X="3.134666748047" Y="26.706599609375" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.113760742188" Y="26.68009375" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065635742188" Y="26.6166015625" />
                  <Point X="3.049739257812" Y="26.58937109375" />
                  <Point X="3.034762939453" Y="26.555544921875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.024621337891" Y="26.5229375" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.3525703125" />
                  <Point X="3.009229248047" Y="26.33061328125" />
                  <Point X="3.022367919922" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034684326172" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.195369140625" />
                  <Point X="3.056920410156" Y="26.183521484375" />
                  <Point X="3.069242675781" Y="26.16479296875" />
                  <Point X="3.104978027344" Y="26.1104765625" />
                  <Point X="3.111743408203" Y="26.10141796875" />
                  <Point X="3.126296386719" Y="26.08417578125" />
                  <Point X="3.134083984375" Y="26.0759921875" />
                  <Point X="3.151328125" Y="26.05990234375" />
                  <Point X="3.160030761719" Y="26.05269921875" />
                  <Point X="3.178236083984" Y="26.039375" />
                  <Point X="3.187738769531" Y="26.03325390625" />
                  <Point X="3.205595214844" Y="26.023201171875" />
                  <Point X="3.257380859375" Y="25.99405078125" />
                  <Point X="3.265475341797" Y="25.989990234375" />
                  <Point X="3.294677978516" Y="25.978083984375" />
                  <Point X="3.330281494141" Y="25.96801953125" />
                  <Point X="3.343681640625" Y="25.965255859375" />
                  <Point X="3.367824951172" Y="25.96206640625" />
                  <Point X="3.437842285156" Y="25.9528125" />
                  <Point X="3.444034912109" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.554770507812" Y="26.091580078125" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.914236328125" />
                  <Point X="4.7680234375" Y="25.81570703125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.025334960938" Y="25.510673828125" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686021728516" Y="25.419541015625" />
                  <Point X="3.665627685547" Y="25.41213671875" />
                  <Point X="3.642389892578" Y="25.401623046875" />
                  <Point X="3.634012939453" Y="25.397322265625" />
                  <Point X="3.61029296875" Y="25.38361328125" />
                  <Point X="3.541502929688" Y="25.3438515625" />
                  <Point X="3.533884033203" Y="25.33894921875" />
                  <Point X="3.508775878906" Y="25.319873046875" />
                  <Point X="3.481991455078" Y="25.294349609375" />
                  <Point X="3.472791015625" Y="25.284220703125" />
                  <Point X="3.458559082031" Y="25.266083984375" />
                  <Point X="3.41728515625" Y="25.2134921875" />
                  <Point X="3.410849365234" Y="25.204201171875" />
                  <Point X="3.399128173828" Y="25.184923828125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005126953" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.365632568359" Y="25.085701171875" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.356618896484" Y="24.8988046875" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.81601171875" />
                  <Point X="3.380005859375" Y="24.79451171875" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.39912890625" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417291015625" Y="24.72394140625" />
                  <Point X="3.431522949219" Y="24.705806640625" />
                  <Point X="3.472796875" Y="24.653212890625" />
                  <Point X="3.478716796875" Y="24.6463671875" />
                  <Point X="3.501133056641" Y="24.624201171875" />
                  <Point X="3.530173339844" Y="24.60127734375" />
                  <Point X="3.541493408203" Y="24.593595703125" />
                  <Point X="3.565213623047" Y="24.579884765625" />
                  <Point X="3.634003417969" Y="24.540123046875" />
                  <Point X="3.639487304688" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.68302734375" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.658711425781" Y="24.257052734375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761613769531" Y="24.06894921875" />
                  <Point X="4.741960449219" Y="23.98282421875" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.823366455078" Y="24.0398515625" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097412109" Y="24.09195703125" />
                  <Point X="3.366719970703" Y="24.089158203125" />
                  <Point X="3.354479980469" Y="24.087322265625" />
                  <Point X="3.30792578125" Y="24.077203125" />
                  <Point X="3.172915771484" Y="24.047859375" />
                  <Point X="3.157872070312" Y="24.043255859375" />
                  <Point X="3.128751953125" Y="24.031630859375" />
                  <Point X="3.114675537109" Y="24.024609375" />
                  <Point X="3.086848876953" Y="24.007716796875" />
                  <Point X="3.074124755859" Y="23.99846875" />
                  <Point X="3.050374267578" Y="23.977998046875" />
                  <Point X="3.039347900391" Y="23.966775390625" />
                  <Point X="3.011208740234" Y="23.932931640625" />
                  <Point X="2.929603759766" Y="23.834787109375" />
                  <Point X="2.921324462891" Y="23.823150390625" />
                  <Point X="2.906605224609" Y="23.79876953125" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.871124267578" Y="23.65951171875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889159179688" Y="23.394515625" />
                  <Point X="2.896542236328" Y="23.380623046875" />
                  <Point X="2.922305908203" Y="23.34055078125" />
                  <Point X="2.997022460938" Y="23.224333984375" />
                  <Point X="3.001744384766" Y="23.217642578125" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.949916992188" Y="22.475333984375" />
                  <Point X="4.087169921875" Y="22.370015625" />
                  <Point X="4.045489990234" Y="22.3025703125" />
                  <Point X="4.004843994141" Y="22.2448203125" />
                  <Point X="4.001274414063" Y="22.239748046875" />
                  <Point X="3.192760009766" Y="22.70654296875" />
                  <Point X="2.848454589844" Y="22.905328125" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815025634766" Y="22.920484375" />
                  <Point X="2.783119873047" Y="22.930671875" />
                  <Point X="2.771106445313" Y="22.933662109375" />
                  <Point X="2.715699707031" Y="22.94366796875" />
                  <Point X="2.555016113281" Y="22.9726875" />
                  <Point X="2.539358398438" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444846679688" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400590087891" Y="22.948890625" />
                  <Point X="2.354560546875" Y="22.924666015625" />
                  <Point X="2.221072021484" Y="22.854412109375" />
                  <Point X="2.208968505859" Y="22.846828125" />
                  <Point X="2.186038818359" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.78883984375" />
                  <Point X="2.128047363281" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.096239013672" Y="22.707775390625" />
                  <Point X="2.025985107422" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337158203" Y="22.514736328125" />
                  <Point X="2.001379394531" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435515625" />
                  <Point X="2.002187866211" Y="22.41985546875" />
                  <Point X="2.012194213867" Y="22.36444921875" />
                  <Point X="2.041213378906" Y="22.203763671875" />
                  <Point X="2.04301550293" Y="22.1957734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.646037353516" Y="21.12791015625" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723753417969" Y="20.963916015625" />
                  <Point X="2.096651611328" Y="21.781169921875" />
                  <Point X="1.833914672852" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808835693359" Y="22.150369140625" />
                  <Point X="1.78325390625" Y="22.171994140625" />
                  <Point X="1.773299682617" Y="22.179353515625" />
                  <Point X="1.718653686523" Y="22.214486328125" />
                  <Point X="1.560176269531" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517469970703" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455385375977" Y="22.3513046875" />
                  <Point X="1.424121704102" Y="22.35362109375" />
                  <Point X="1.408399414062" Y="22.35348046875" />
                  <Point X="1.348634521484" Y="22.347982421875" />
                  <Point X="1.1753125" Y="22.332033203125" />
                  <Point X="1.161231811523" Y="22.32966015625" />
                  <Point X="1.133583618164" Y="22.322830078125" />
                  <Point X="1.120016357422" Y="22.318373046875" />
                  <Point X="1.092631347656" Y="22.30703125" />
                  <Point X="1.079884765625" Y="22.300591796875" />
                  <Point X="1.055496826172" Y="22.285869140625" />
                  <Point X="1.04385534668" Y="22.2775859375" />
                  <Point X="0.997706604004" Y="22.239212890625" />
                  <Point X="0.863871520996" Y="22.12793359375" />
                  <Point X="0.852647949219" Y="22.11690625" />
                  <Point X="0.83217767334" Y="22.093154296875" />
                  <Point X="0.822930969238" Y="22.0804296875" />
                  <Point X="0.806038635254" Y="22.0526015625" />
                  <Point X="0.799017822266" Y="22.03852734375" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.768993591309" Y="21.930884765625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584716797" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091247559" Y="20.847662109375" />
                  <Point X="0.757776306152" Y="21.128740234375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605224609" Y="21.51987890625" />
                  <Point X="0.642142944336" Y="21.546423828125" />
                  <Point X="0.626785705566" Y="21.576185546875" />
                  <Point X="0.62040625" Y="21.586791015625" />
                  <Point X="0.578425598145" Y="21.647275390625" />
                  <Point X="0.456678619385" Y="21.82269140625" />
                  <Point X="0.446670806885" Y="21.834826171875" />
                  <Point X="0.424791564941" Y="21.85727734375" />
                  <Point X="0.41291986084" Y="21.86759375" />
                  <Point X="0.386664154053" Y="21.88683984375" />
                  <Point X="0.373249145508" Y="21.89505859375" />
                  <Point X="0.345244293213" Y="21.909169921875" />
                  <Point X="0.330654724121" Y="21.9150625" />
                  <Point X="0.265692077637" Y="21.935224609375" />
                  <Point X="0.077295753479" Y="21.9936953125" />
                  <Point X="0.063377220154" Y="21.996890625" />
                  <Point X="0.035217639923" Y="22.00116015625" />
                  <Point X="0.02097659111" Y="22.002234375" />
                  <Point X="-0.008664463997" Y="22.002234375" />
                  <Point X="-0.022905660629" Y="22.00116015625" />
                  <Point X="-0.051065093994" Y="21.996890625" />
                  <Point X="-0.064983627319" Y="21.9936953125" />
                  <Point X="-0.129946411133" Y="21.973533203125" />
                  <Point X="-0.31834274292" Y="21.9150625" />
                  <Point X="-0.332927093506" Y="21.909173828125" />
                  <Point X="-0.360928405762" Y="21.89506640625" />
                  <Point X="-0.374345336914" Y="21.88684765625" />
                  <Point X="-0.400600921631" Y="21.867603515625" />
                  <Point X="-0.412478271484" Y="21.85728125" />
                  <Point X="-0.434361938477" Y="21.83482421875" />
                  <Point X="-0.444368286133" Y="21.822689453125" />
                  <Point X="-0.486348754883" Y="21.762203125" />
                  <Point X="-0.60809588623" Y="21.5867890625" />
                  <Point X="-0.612468933105" Y="21.57987109375" />
                  <Point X="-0.625977172852" Y="21.554736328125" />
                  <Point X="-0.638778198242" Y="21.52378515625" />
                  <Point X="-0.642753112793" Y="21.512064453125" />
                  <Point X="-0.94062298584" Y="20.400396484375" />
                  <Point X="-0.985424865723" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92935813536" Y="22.281268833048" />
                  <Point X="4.01096440573" Y="22.428490441924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.847073143146" Y="22.328775984816" />
                  <Point X="3.934758877446" Y="22.486965236948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.764788150933" Y="22.376283136585" />
                  <Point X="3.858553300273" Y="22.545439943774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.628186923881" Y="23.933895754826" />
                  <Point X="4.783494387102" Y="24.214077835227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.682503158719" Y="22.423790288353" />
                  <Point X="3.782347723099" Y="22.6039146506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526955697735" Y="23.947222995799" />
                  <Point X="4.693526371785" Y="24.247724446413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600218166505" Y="22.471297440122" />
                  <Point X="3.706142145926" Y="22.662389357427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.42572447159" Y="23.960550236773" />
                  <Point X="4.598954039384" Y="24.2730646497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.684442281733" Y="21.015147218131" />
                  <Point X="2.697519855757" Y="21.038739786194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.517933174291" Y="22.51880459189" />
                  <Point X="3.629936568753" Y="22.720864064253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.324493245444" Y="23.973877477746" />
                  <Point X="4.504381867376" Y="24.298405142341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.621379490744" Y="21.097332138871" />
                  <Point X="2.642104783682" Y="21.134721557074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435648182077" Y="22.566311743659" />
                  <Point X="3.55373099158" Y="22.779338771079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.223262019299" Y="23.987204718719" />
                  <Point X="4.409809695367" Y="24.323745634982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.558316699755" Y="21.179517059611" />
                  <Point X="2.586689657731" Y="21.230703230759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.353363189863" Y="22.613818895427" />
                  <Point X="3.477525414407" Y="22.837813477905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122030793153" Y="24.000531959693" />
                  <Point X="4.315237523359" Y="24.349086127624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.495253908766" Y="21.261701980351" />
                  <Point X="2.531274531781" Y="21.326684904445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.271078197649" Y="22.661326047196" />
                  <Point X="3.401319837234" Y="22.896288184731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020799567008" Y="24.013859200666" />
                  <Point X="4.22066535135" Y="24.374426620265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.432191117777" Y="21.343886901091" />
                  <Point X="2.475859405831" Y="21.422666578131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.188793209268" Y="22.70883320588" />
                  <Point X="3.32511426006" Y="22.954762891557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.919568340862" Y="24.02718644164" />
                  <Point X="4.126093179342" Y="24.399767112906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.369128326788" Y="21.426071821831" />
                  <Point X="2.42044427988" Y="21.518648251816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.106508296568" Y="22.756340501094" />
                  <Point X="3.248908682887" Y="23.013237598384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.818337115472" Y="24.040513683976" />
                  <Point X="4.031521007333" Y="24.425107605548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739304168951" Y="25.701982229484" />
                  <Point X="4.775549268246" Y="25.767370119506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.306065535799" Y="21.508256742571" />
                  <Point X="2.36502915393" Y="21.614629925502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024223383867" Y="22.803847796309" />
                  <Point X="3.172703105714" Y="23.07171230521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.717105904533" Y="24.053840952382" />
                  <Point X="3.936948835324" Y="24.450448098189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.611738543979" Y="25.667800957367" />
                  <Point X="4.751357722192" Y="25.919680622415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243002744811" Y="21.590441663311" />
                  <Point X="2.309614027979" Y="21.710611599188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941938471167" Y="22.851355091523" />
                  <Point X="3.096497528541" Y="23.130187012036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615874693593" Y="24.067168220788" />
                  <Point X="3.842376663316" Y="24.475788590831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.484172919007" Y="25.633619685251" />
                  <Point X="4.718211387111" Y="26.055836258282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.179939953822" Y="21.672626584051" />
                  <Point X="2.254198902029" Y="21.806593272873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859653558466" Y="22.898862386738" />
                  <Point X="3.022571809834" Y="23.19277469241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.514643482654" Y="24.080495489194" />
                  <Point X="3.747804491307" Y="24.501129083472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.356607294034" Y="25.599438413135" />
                  <Point X="4.635281592307" Y="26.102180155386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.116877162833" Y="21.754811504791" />
                  <Point X="2.198783776079" Y="21.902574946559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770395854949" Y="22.933790434331" />
                  <Point X="2.96180821751" Y="23.27910747734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.412261741016" Y="24.091747145276" />
                  <Point X="3.65511835225" Y="24.529872069625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.229041669062" Y="25.565257141019" />
                  <Point X="4.518112053358" Y="26.086753918922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053814437836" Y="21.836996544584" />
                  <Point X="2.143368650128" Y="21.998556620245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67166106622" Y="22.951621367622" />
                  <Point X="2.903479524479" Y="23.369832936876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.29389027687" Y="24.07415257836" />
                  <Point X="3.572036237277" Y="24.575941173869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10147604409" Y="25.531075868902" />
                  <Point X="4.400942891819" Y="26.071328363326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.990751743997" Y="21.919181640587" />
                  <Point X="2.087953524178" Y="22.094538293931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.572926587449" Y="22.969452860093" />
                  <Point X="2.861291156676" Y="23.489676313906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.170243859902" Y="24.047041744646" />
                  <Point X="3.494050598632" Y="24.631204564793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.973910336651" Y="25.496894448012" />
                  <Point X="4.28377373028" Y="26.055902807729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.927689050158" Y="22.00136673659" />
                  <Point X="2.040885591564" Y="22.205578703019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464142886614" Y="22.969155076055" />
                  <Point X="2.876926573019" Y="23.713836558927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.972702699241" Y="23.886621264445" />
                  <Point X="3.42874598599" Y="24.709345132213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.846344507108" Y="25.46271280684" />
                  <Point X="4.166604568741" Y="26.040477252133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.864626356319" Y="22.083551832594" />
                  <Point X="2.014193585274" Y="22.353378256251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.321103069354" Y="22.907057622078" />
                  <Point X="3.37538310736" Y="24.80902915807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.718778677565" Y="25.428531165668" />
                  <Point X="4.049435407203" Y="26.025051696537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.798083394713" Y="22.159458359343" />
                  <Point X="2.019594498672" Y="22.559074969208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.150607774783" Y="22.795429175888" />
                  <Point X="3.349313964783" Y="24.957952387186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573460579278" Y="25.362323583919" />
                  <Point X="3.932266245664" Y="26.00962614094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.719622130698" Y="22.213863699386" />
                  <Point X="3.815097084125" Y="25.994200585344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.639541396056" Y="22.265347437079" />
                  <Point X="3.697927922587" Y="25.978775029748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804413224984" Y="20.954689541958" />
                  <Point X="0.816201099247" Y="20.975955430062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.559426506782" Y="22.316769558184" />
                  <Point X="3.580758761048" Y="25.963349474151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769017593102" Y="21.08678733898" />
                  <Point X="0.795354561781" Y="21.134300488205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.468777389884" Y="22.349187429593" />
                  <Point X="3.465425183622" Y="25.951235399953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733622030739" Y="21.218885261415" />
                  <Point X="0.774508024315" Y="21.292645546348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.360074152846" Y="22.349034806088" />
                  <Point X="3.36315242703" Y="25.962683670262" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.698226500728" Y="21.350983242218" />
                  <Point X="0.753661486848" Y="21.450990604491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.245617485257" Y="22.338502719113" />
                  <Point X="3.268896818742" Y="25.988595258974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.06838417558" Y="27.430908630445" />
                  <Point X="4.116339606322" Y="27.517422517628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662830970718" Y="21.483081223021" />
                  <Point X="0.732814949382" Y="21.609335662635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.127137544025" Y="22.320712454352" />
                  <Point X="3.185744822634" Y="26.034538294314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87937239508" Y="27.285875559379" />
                  <Point X="4.059649566143" Y="27.611104185161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.615565545867" Y="21.593765346682" />
                  <Point X="0.747422551036" Y="21.831641680873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.952514345608" Y="22.201637072495" />
                  <Point X="3.113225106704" Y="26.09966247084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.69036061458" Y="27.140842488313" />
                  <Point X="3.999586865991" Y="27.698701413041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.555176323359" Y="21.680773512637" />
                  <Point X="3.053990815951" Y="26.188754188839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.50134883408" Y="26.995809417247" />
                  <Point X="3.934752614963" Y="27.777690535275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.494787747514" Y="21.767782845204" />
                  <Point X="3.013237659662" Y="26.31118675598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312335928587" Y="26.850774316639" />
                  <Point X="3.775011877497" Y="27.685463823688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.43179383721" Y="21.850092029989" />
                  <Point X="3.027296113235" Y="26.532502084854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.096953989019" Y="26.6581682193" />
                  <Point X="3.615271419974" Y="27.593237617133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353591228108" Y="21.904963995848" />
                  <Point X="3.455531025203" Y="27.501011523784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.262325495073" Y="21.936269462296" />
                  <Point X="3.295790630431" Y="27.408785430434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.16965027812" Y="21.965032152448" />
                  <Point X="3.13605023566" Y="27.316559337085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.076962321343" Y="21.993771859349" />
                  <Point X="2.976309840888" Y="27.224333243736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.005409803192" Y="20.237074064978" />
                  <Point X="-0.964712590793" Y="20.310493779652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.027988062125" Y="22.000389562903" />
                  <Point X="2.816569446117" Y="27.132107150387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.10209802025" Y="20.258597111298" />
                  <Point X="-0.863077124349" Y="20.689802222011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.155970483886" Y="21.965456369475" />
                  <Point X="2.656829051345" Y="27.039881057037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121346134186" Y="20.419825801823" />
                  <Point X="-0.761442241164" Y="21.069109612142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.287158022127" Y="21.924740992857" />
                  <Point X="2.519583247821" Y="26.988236280535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.139419254166" Y="20.583174237557" />
                  <Point X="-0.659807357979" Y="21.448417002274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.483985166732" Y="21.76560863172" />
                  <Point X="2.404346328038" Y="26.976296581339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.168103504404" Y="20.727379687568" />
                  <Point X="2.304309999844" Y="26.99177947528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.203377783747" Y="20.859696410365" />
                  <Point X="2.21848369366" Y="27.03289792753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.265282011512" Y="20.943971434487" />
                  <Point X="2.141629256817" Y="27.090202060526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.338371027677" Y="21.008068566206" />
                  <Point X="2.077508764194" Y="27.170478837006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.411459855762" Y="21.072166037227" />
                  <Point X="2.027100680468" Y="27.275493453979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.484548683848" Y="21.136263508248" />
                  <Point X="2.019247559728" Y="27.4572792564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.56593766274" Y="21.185387110838" />
                  <Point X="2.74147964209" Y="28.956173630635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.663368376635" Y="21.205570657407" />
                  <Point X="2.663571985958" Y="29.011577705736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.76817910197" Y="21.212440310903" />
                  <Point X="2.583307632678" Y="29.062730186638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.872990016705" Y="21.219309622712" />
                  <Point X="2.501203738785" Y="29.110564048426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.980072942591" Y="21.222080117903" />
                  <Point X="2.419099844892" Y="29.158397910214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.118082263867" Y="21.169057918914" />
                  <Point X="2.336995912914" Y="29.206231703295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.29059593722" Y="21.053788221011" />
                  <Point X="2.254891505585" Y="29.254064638818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.515032582133" Y="20.844847002821" />
                  <Point X="2.168858998565" Y="29.294811094913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.595897416553" Y="20.894916187069" />
                  <Point X="2.082601294357" Y="29.335151284527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.676762250973" Y="20.944985371317" />
                  <Point X="1.99634359015" Y="29.375491474142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757039949419" Y="20.996113776902" />
                  <Point X="1.910085685765" Y="29.415831302626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83316704424" Y="21.05473006964" />
                  <Point X="1.822160395945" Y="29.453163088159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.909294469229" Y="21.113345766739" />
                  <Point X="1.731724159021" Y="29.485965005206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311405145866" Y="22.387919865717" />
                  <Point X="1.641287922097" Y="29.518766922252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.346617718339" Y="22.520347910655" />
                  <Point X="1.550851685173" Y="29.551568839299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413225322973" Y="22.596137818295" />
                  <Point X="1.460415213974" Y="29.584370333703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.497043176562" Y="22.640879614941" />
                  <Point X="1.365020772432" Y="29.608227412838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601555632666" Y="22.648287360374" />
                  <Point X="1.269222672613" Y="29.631356273165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.743415951158" Y="22.5883177785" />
                  <Point X="1.173424572794" Y="29.654485133492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.903156504187" Y="22.496091399646" />
                  <Point X="1.077626472975" Y="29.677613993819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.062897057216" Y="22.403865020793" />
                  <Point X="0.981828373156" Y="29.700742854146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.222637610246" Y="22.31163864194" />
                  <Point X="0.886030108289" Y="29.723871416717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.382378163275" Y="22.219412263086" />
                  <Point X="0.787536614102" Y="29.742137656886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.542118716304" Y="22.127185884233" />
                  <Point X="0.68487753795" Y="29.752888988261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.701859201723" Y="22.03495962735" />
                  <Point X="0.582218461799" Y="29.763640319636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.820043102876" Y="22.017703433031" />
                  <Point X="-3.029988505944" Y="23.442999655169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.957270202439" Y="23.574186947373" />
                  <Point X="0.111397760785" Y="29.1102104981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.277833505982" Y="29.410468530621" />
                  <Point X="0.479559385647" Y="29.774391651011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882891994118" Y="22.10027423913" />
                  <Point X="-3.221865202903" Y="23.292798137994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.970726880207" Y="23.745863665318" />
                  <Point X="-0.010980341579" Y="29.085387764502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.94574088536" Y="22.182845045228" />
                  <Point X="-3.410877262958" Y="23.147764562599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.032995701548" Y="23.829480945218" />
                  <Point X="-0.10431809243" Y="29.112955211862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008113190062" Y="22.266275636204" />
                  <Point X="-3.599889733586" Y="23.002730246508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.11422596842" Y="23.878890871872" />
                  <Point X="-0.17889245116" Y="29.174372714658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.06440748001" Y="22.360671256053" />
                  <Point X="-3.788902204214" Y="22.857695930418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.211170656853" Y="23.899951231583" />
                  <Point X="-0.230040657956" Y="29.278052114266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.120701362454" Y="22.455067611059" />
                  <Point X="-3.977914674842" Y="22.712661614327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.325818392925" Y="23.88907444794" />
                  <Point X="-0.26543617254" Y="29.410150122899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17699517808" Y="22.549464086607" />
                  <Point X="-4.16692714547" Y="22.567627298237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442987576227" Y="23.873648853083" />
                  <Point X="-0.300831684601" Y="29.542248136084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.560156759528" Y="23.858223258226" />
                  <Point X="-0.336227195698" Y="29.674346151006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.67732594283" Y="23.842797663368" />
                  <Point X="-0.386225232701" Y="29.780100511848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794495126131" Y="23.827372068511" />
                  <Point X="-0.502379315144" Y="29.766506207416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.911664309433" Y="23.811946473653" />
                  <Point X="-3.387904313293" Y="24.75683451899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300278253477" Y="24.914916115504" />
                  <Point X="-0.618533397587" Y="29.752911902983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.028833492734" Y="23.796520878796" />
                  <Point X="-3.539395089191" Y="24.679491132052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.317377710433" Y="25.080021085831" />
                  <Point X="-0.73468748003" Y="29.739317598551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146002676035" Y="23.781095283939" />
                  <Point X="-3.666960455104" Y="24.64531032729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.376177147998" Y="25.169897299745" />
                  <Point X="-0.850842332666" Y="29.725721904654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.263171859337" Y="23.765669689081" />
                  <Point X="-3.794525821017" Y="24.611129522529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453514786382" Y="25.226329714085" />
                  <Point X="-0.970837941596" Y="29.705197302986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.380341042638" Y="23.750244094224" />
                  <Point X="-3.922091275741" Y="24.576948557549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.543932967967" Y="25.259164203826" />
                  <Point X="-1.09944252466" Y="29.669141700857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.497510027405" Y="23.734818857533" />
                  <Point X="-4.049656981284" Y="24.54276714008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.638505156107" Y="25.284504667365" />
                  <Point X="-1.228047107723" Y="29.633086098728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.614678667189" Y="23.719394243207" />
                  <Point X="-4.177222686826" Y="24.50858572261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.733077344247" Y="25.309845130904" />
                  <Point X="-1.637840158133" Y="29.089753073276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462503133229" Y="29.406069439469" />
                  <Point X="-1.356651690787" Y="29.597030496598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.681662237553" Y="23.794505890717" />
                  <Point X="-4.304788392369" Y="24.474404305141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.827649532388" Y="25.335185594443" />
                  <Point X="-1.776128078375" Y="29.036228268446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71592583641" Y="23.928645929376" />
                  <Point X="-4.432354097912" Y="24.440222887671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.922221720528" Y="25.360526057982" />
                  <Point X="-1.885299361668" Y="29.035231267147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746891536728" Y="24.06873553449" />
                  <Point X="-4.559919803455" Y="24.406041470202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.016793908668" Y="25.385866521521" />
                  <Point X="-3.510013009394" Y="26.300123465272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.442590942047" Y="26.421756094525" />
                  <Point X="-1.973940953009" Y="29.071270810527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.769168967896" Y="24.224499192064" />
                  <Point X="-4.687485508997" Y="24.371860052733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.111366096808" Y="25.411206985061" />
                  <Point X="-3.664109998559" Y="26.218078345139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441736690053" Y="26.619250413182" />
                  <Point X="-2.057359746023" Y="29.116732531508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.205938284949" Y="25.4365474486" />
                  <Point X="-3.779535273904" Y="26.205798843516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.495537351558" Y="26.718144657826" />
                  <Point X="-2.91563960031" Y="27.764307894252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750376734104" Y="28.06244999706" />
                  <Point X="-2.128334597336" Y="29.184643717581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.300510473089" Y="25.461887912139" />
                  <Point X="-3.880766636689" Y="26.219125837985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56614480063" Y="26.786718655086" />
                  <Point X="-3.046821086732" Y="27.723603435404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.77794522763" Y="28.208668325462" />
                  <Point X="-2.191397263637" Y="29.266828863263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.395082661229" Y="25.487228375678" />
                  <Point X="-3.981997999474" Y="26.232452832454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642350368703" Y="26.84519337833" />
                  <Point X="-3.151430243417" Y="27.73083672837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.832292368155" Y="28.306576695858" />
                  <Point X="-2.346675360202" Y="29.182652968977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.489654849369" Y="25.512568839217" />
                  <Point X="-4.083229285164" Y="26.245779966007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.718555936775" Y="26.903668101574" />
                  <Point X="-3.245098553095" Y="27.75780783182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.88770754596" Y="28.402558275996" />
                  <Point X="-2.503630574499" Y="29.095451474211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.58422703751" Y="25.537909302756" />
                  <Point X="-4.184460407442" Y="26.259107394361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794761504848" Y="26.962142824818" />
                  <Point X="-3.327651209047" Y="27.804832105422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.943122723765" Y="28.498539856134" />
                  <Point X="-2.66306888454" Y="29.003770356142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678799108176" Y="25.563249978223" />
                  <Point X="-4.285691529721" Y="26.272434822716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.870967072921" Y="27.020617548062" />
                  <Point X="-3.409936313563" Y="27.852339054592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.998537901569" Y="28.594521436272" />
                  <Point X="-2.85196565262" Y="28.858944772974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.77337113097" Y="25.588590740055" />
                  <Point X="-4.386922651999" Y="26.28576225107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.947172640993" Y="27.079092271306" />
                  <Point X="-3.492221227619" Y="27.899846347362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.053953079374" Y="28.69050301641" />
                  <Point X="-3.040862420701" Y="28.714119189805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750940745479" Y="25.825009433914" />
                  <Point X="-4.488153774278" Y="26.299089679425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023378209066" Y="27.13756699455" />
                  <Point X="-3.574506110063" Y="27.947353697161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678946203595" Y="26.150844232857" />
                  <Point X="-4.589384896556" Y="26.312417107779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.099583777139" Y="27.196041717794" />
                  <Point X="-3.656790992506" Y="27.99486104696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175789345211" Y="27.254516441038" />
                  <Point X="-3.784823019163" Y="27.959838363933" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.574250366211" Y="21.079564453125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.478455078125" />
                  <Point X="0.422338226318" Y="21.538939453125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.274335601807" Y="21.7336015625" />
                  <Point X="0.209372970581" Y="21.753763671875" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664604187" Y="21.812234375" />
                  <Point X="-0.073627388" Y="21.792072265625" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.330259765625" Y="21.65387109375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.757097167969" Y="20.351220703125" />
                  <Point X="-0.84774395752" Y="20.012923828125" />
                  <Point X="-0.878838134766" Y="20.018958984375" />
                  <Point X="-1.100258300781" Y="20.0619375" />
                  <Point X="-1.174605102539" Y="20.081064453125" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.321514160156" Y="20.355046875" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.325202880859" Y="20.543263671875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.446092041016" Y="20.849822265625" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.72862121582" Y="21.019439453125" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.056022460938" Y="20.982013671875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.426965087891" Y="20.624765625" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.531293945312" Y="20.63144140625" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-2.958801025391" Y="20.911669921875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.696910888672" Y="22.040271484375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.40241015625" />
                  <Point X="-2.513983398438" Y="22.431240234375" />
                  <Point X="-2.531328369141" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.550473876953" Y="21.902970703125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.905299560547" Y="21.8160078125" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.235522460938" Y="22.27665234375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.497760009766" Y="23.320587890625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.143846435547" Y="23.61161328125" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651611328" Y="23.649947265625" />
                  <Point X="-3.148656982422" Y="23.678931640625" />
                  <Point X="-3.167949707031" Y="23.693357421875" />
                  <Point X="-3.187642089844" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.429911132812" Y="23.552078125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.827111816406" Y="23.5962109375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.946923339844" Y="24.125361328125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.937909667969" Y="24.7694140625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.534781005859" Y="24.883513671875" />
                  <Point X="-3.514144042969" Y="24.8978359375" />
                  <Point X="-3.502324951172" Y="24.909353515625" />
                  <Point X="-3.492526855469" Y="24.931734375" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.48801953125" Y="24.991181640625" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.502326171875" Y="25.028087890625" />
                  <Point X="-3.521264648438" Y="25.044544921875" />
                  <Point X="-3.541894287109" Y="25.05886328125" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.660784179688" Y="25.36171875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.982274902344" Y="25.559654296875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.878330078125" Y="26.141501953125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.025979003906" Y="26.4298828125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704101562" Y="26.3958671875" />
                  <Point X="-3.715954101562" Y="26.40083203125" />
                  <Point X="-3.670278076172" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.632800292969" Y="26.45904296875" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.623941650391" Y="26.56016015625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.292835449219" Y="27.104837890625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.411143554688" Y="27.35676171875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.05587109375" Y="27.9208671875" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.326649414062" Y="28.02364453125" />
                  <Point X="-3.159156738281" Y="27.926943359375" />
                  <Point X="-3.138513916016" Y="27.92043359375" />
                  <Point X="-3.116578613281" Y="27.918513671875" />
                  <Point X="-3.052964599609" Y="27.91294921875" />
                  <Point X="-3.031506591797" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.997683105469" Y="27.942974609375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939993408203" Y="28.049775390625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.232510009766" Y="28.6197734375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.190263671875" Y="28.838990234375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.588853759766" Y="29.265458984375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.01910144043" Y="29.354396484375" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246582031" Y="29.273662109375" />
                  <Point X="-1.926832763672" Y="29.260951171875" />
                  <Point X="-1.856030639648" Y="29.22409375" />
                  <Point X="-1.835124023438" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.788379882812" Y="29.232783203125" />
                  <Point X="-1.714634643555" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.677806640625" Y="29.32073828125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.68380078125" Y="29.6606015625" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.534319335938" Y="29.744544921875" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.769251098633" Y="29.926568359375" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.091689376831" Y="29.495822265625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282129288" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594051361" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.198147842407" Y="29.847181640625" />
                  <Point X="0.236648452759" Y="29.990869140625" />
                  <Point X="0.365808441162" Y="29.977341796875" />
                  <Point X="0.860210083008" Y="29.925564453125" />
                  <Point X="1.024715209961" Y="29.88584765625" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.615405395508" Y="29.730265625" />
                  <Point X="1.931043823242" Y="29.61578125" />
                  <Point X="2.034575805664" Y="29.56736328125" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.438730224609" Y="29.36685546875" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.826861816406" Y="29.1286015625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.450779541016" Y="27.886251953125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.219347167969" Y="27.470927734375" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.204191162109" Y="27.3745234375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.229696777344" Y="27.284580078125" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.291172363281" Y="27.2131875" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.360337402344" Y="27.172978515625" />
                  <Point X="2.378138427734" Y="27.17083203125" />
                  <Point X="2.429762207031" Y="27.164607421875" />
                  <Point X="2.448664550781" Y="27.1659453125" />
                  <Point X="2.469250488281" Y="27.171451171875" />
                  <Point X="2.528950927734" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.65076171875" Y="27.833119140625" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.028323486328" Y="27.984072265625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.255184082031" Y="27.654970703125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.579485839844" Y="26.816275390625" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.264555664062" Y="26.56450390625" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.207600830078" Y="26.471765625" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.195309814453" Y="26.3690078125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.22796875" Y="26.2692265625" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280948242188" Y="26.1988203125" />
                  <Point X="3.2988046875" Y="26.188767578125" />
                  <Point X="3.350590332031" Y="26.1596171875" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.392708740234" Y="26.1504296875" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.529970703125" Y="26.279955078125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.864341308594" Y="26.258833984375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.95576171875" Y="25.8449375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.074510742188" Y="25.327146484375" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.7053671875" Y="25.219109375" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.608033203125" Y="25.148791015625" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.552240966797" Y="25.049962890625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.543227294922" Y="24.93454296875" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.580990722656" Y="24.823107421875" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.660297119141" Y="24.744380859375" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.707887207031" Y="24.440580078125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.990223144531" Y="24.31079296875" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.927198730469" Y="23.9405546875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.798566650391" Y="23.8514765625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.348282226562" Y="23.8915390625" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.157306396484" Y="23.811458984375" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.060324951172" Y="23.6421015625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.082124267578" Y="23.4433046875" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.06558203125" Y="22.626072265625" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.3219375" Y="22.388486328125" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.160219238281" Y="22.13546484375" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.097760009766" Y="22.541998046875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.681934082031" Y="22.75669140625" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.443048339844" Y="22.756529296875" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.264374755859" Y="22.619287109375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.199169433594" Y="22.39821875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.810582275391" Y="21.22291015625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.975084960938" Y="20.909634765625" />
                  <Point X="2.835296142578" Y="20.809787109375" />
                  <Point X="2.786202880859" Y="20.778009765625" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="1.945914428711" Y="21.665505859375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.615902954102" Y="22.054666015625" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.366040039062" Y="22.158783203125" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.165332885742" Y="22.1314921875" />
                  <Point X="1.119184082031" Y="22.093119140625" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.954658569336" Y="21.890529296875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.077302490234" Y="20.44833984375" />
                  <Point X="1.127642578125" Y="20.06596875" />
                  <Point X="1.126596191406" Y="20.065740234375" />
                  <Point X="0.994369506836" Y="20.0367578125" />
                  <Point X="0.948988586426" Y="20.028513671875" />
                  <Point X="0.860200500488" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#142" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.049548971452" Y="4.540043761932" Z="0.65" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="-0.781219238283" Y="5.007509184099" Z="0.65" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.65" />
                  <Point X="-1.553973044818" Y="4.823970338236" Z="0.65" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.65" />
                  <Point X="-1.739529051844" Y="4.685357439594" Z="0.65" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.65" />
                  <Point X="-1.731648567591" Y="4.367053989318" Z="0.65" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.65" />
                  <Point X="-1.813670313711" Y="4.310257594668" Z="0.65" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.65" />
                  <Point X="-1.909901257286" Y="4.33658213761" Z="0.65" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.65" />
                  <Point X="-1.985589778871" Y="4.41611368167" Z="0.65" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.65" />
                  <Point X="-2.619292722615" Y="4.34044628258" Z="0.65" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.65" />
                  <Point X="-3.226841738242" Y="3.909950934568" Z="0.65" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.65" />
                  <Point X="-3.281967312673" Y="3.626053758592" Z="0.65" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.65" />
                  <Point X="-2.995959124172" Y="3.076698881895" Z="0.65" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.65" />
                  <Point X="-3.039193704129" Y="3.009609863758" Z="0.65" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.65" />
                  <Point X="-3.118377583712" Y="2.999605575001" Z="0.65" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.65" />
                  <Point X="-3.307805589705" Y="3.098226636073" Z="0.65" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.65" />
                  <Point X="-4.101490492392" Y="2.982850552871" Z="0.65" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.65" />
                  <Point X="-4.460481739335" Y="2.413247024759" Z="0.65" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.65" />
                  <Point X="-4.329429504242" Y="2.096450160351" Z="0.65" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.65" />
                  <Point X="-3.674447896769" Y="1.568352761616" Z="0.65" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.65" />
                  <Point X="-3.685150159406" Y="1.50945733361" Z="0.65" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.65" />
                  <Point X="-3.737146063554" Y="1.47979848801" Z="0.65" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.65" />
                  <Point X="-4.025608932035" Y="1.510735874986" Z="0.65" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.65" />
                  <Point X="-4.932745647644" Y="1.185860937105" Z="0.65" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.65" />
                  <Point X="-5.037892524107" Y="0.598253431006" Z="0.65" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.65" />
                  <Point X="-4.679881185355" Y="0.344702860097" Z="0.65" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.65" />
                  <Point X="-3.555923849975" Y="0.034745854875" Z="0.65" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.65" />
                  <Point X="-3.54192877932" Y="0.007642666657" Z="0.65" />
                  <Point X="-3.539556741714" Y="0" Z="0.65" />
                  <Point X="-3.546435821815" Y="-0.022164284404" Z="0.65" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.65" />
                  <Point X="-3.569444745149" Y="-0.044130128259" Z="0.65" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.65" />
                  <Point X="-3.957006603418" Y="-0.151009196659" Z="0.65" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.65" />
                  <Point X="-5.002575542816" Y="-0.850435492984" Z="0.65" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.65" />
                  <Point X="-4.881712526674" Y="-1.384883157171" Z="0.65" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.65" />
                  <Point X="-4.429540672593" Y="-1.46621306238" Z="0.65" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.65" />
                  <Point X="-3.199466585888" Y="-1.318453339962" Z="0.65" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.65" />
                  <Point X="-3.198405326786" Y="-1.344569781307" Z="0.65" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.65" />
                  <Point X="-3.534354062682" Y="-1.608463855506" Z="0.65" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.65" />
                  <Point X="-4.284621619936" Y="-2.717676252689" Z="0.65" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.65" />
                  <Point X="-3.951241339277" Y="-3.182995139943" Z="0.65" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.65" />
                  <Point X="-3.531629769661" Y="-3.109048784063" Z="0.65" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.65" />
                  <Point X="-2.559939228856" Y="-2.568391240276" Z="0.65" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.65" />
                  <Point X="-2.746368229105" Y="-2.903448612831" Z="0.65" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.65" />
                  <Point X="-2.995460886088" Y="-4.096666913651" Z="0.65" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.65" />
                  <Point X="-2.563771506369" Y="-4.379789162672" Z="0.65" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.65" />
                  <Point X="-2.393453313158" Y="-4.374391832631" Z="0.65" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.65" />
                  <Point X="-2.034399973745" Y="-4.028280425266" Z="0.65" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.65" />
                  <Point X="-1.738047214159" Y="-3.99917306494" Z="0.65" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.65" />
                  <Point X="-1.485215352806" Y="-4.156489021572" Z="0.65" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.65" />
                  <Point X="-1.380398105731" Y="-4.435210154441" Z="0.65" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.65" />
                  <Point X="-1.377242541349" Y="-4.607146140631" Z="0.65" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.65" />
                  <Point X="-1.193220234479" Y="-4.936076158126" Z="0.65" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.65" />
                  <Point X="-0.894521469102" Y="-4.99884543735" Z="0.65" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="-0.714956825476" Y="-4.630439301874" Z="0.65" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="-0.295339764888" Y="-3.343358841085" Z="0.65" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="-0.06496266539" Y="-3.224401382429" Z="0.65" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.65" />
                  <Point X="0.188396413972" Y="-3.262710662859" Z="0.65" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.65" />
                  <Point X="0.375106094544" Y="-3.458286914683" Z="0.65" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.65" />
                  <Point X="0.519798034926" Y="-3.902096727941" Z="0.65" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.65" />
                  <Point X="0.951769454981" Y="-4.989401625327" Z="0.65" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.65" />
                  <Point X="1.131145409099" Y="-4.951818206771" Z="0.65" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.65" />
                  <Point X="1.120718839102" Y="-4.513855103699" Z="0.65" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.65" />
                  <Point X="0.997361750065" Y="-3.088808860535" Z="0.65" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.65" />
                  <Point X="1.144994928741" Y="-2.91404667236" Z="0.65" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.65" />
                  <Point X="1.364465739511" Y="-2.859726192809" Z="0.65" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.65" />
                  <Point X="1.582707871373" Y="-2.956112924329" Z="0.65" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.65" />
                  <Point X="1.900090776153" Y="-3.333650567081" Z="0.65" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.65" />
                  <Point X="2.807216322698" Y="-4.232685101368" Z="0.65" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.65" />
                  <Point X="2.997990673943" Y="-4.099773059617" Z="0.65" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.65" />
                  <Point X="2.847727636269" Y="-3.720809455595" Z="0.65" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.65" />
                  <Point X="2.242217539128" Y="-2.56161496227" Z="0.65" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.65" />
                  <Point X="2.302465238736" Y="-2.372719598348" Z="0.65" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.65" />
                  <Point X="2.460178780024" Y="-2.256436071231" Z="0.65" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.65" />
                  <Point X="2.666891841243" Y="-2.261230470363" Z="0.65" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.65" />
                  <Point X="3.066604141511" Y="-2.470021864005" Z="0.65" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.65" />
                  <Point X="4.194951983017" Y="-2.86203209931" Z="0.65" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.65" />
                  <Point X="4.358315380285" Y="-2.606518163858" Z="0.65" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.65" />
                  <Point X="4.089864080339" Y="-2.302978472258" Z="0.65" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.65" />
                  <Point X="3.118026214253" Y="-1.498376252097" Z="0.65" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.65" />
                  <Point X="3.103958467322" Y="-1.331199694576" Z="0.65" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.65" />
                  <Point X="3.189596543585" Y="-1.18922664658" Z="0.65" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.65" />
                  <Point X="3.352745764072" Y="-1.126039149801" Z="0.65" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.65" />
                  <Point X="3.785884220169" Y="-1.166815217164" Z="0.65" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.65" />
                  <Point X="4.969789648516" Y="-1.039290515755" Z="0.65" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.65" />
                  <Point X="5.033508532318" Y="-0.665380410605" Z="0.65" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.65" />
                  <Point X="4.714671942979" Y="-0.479842265538" Z="0.65" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.65" />
                  <Point X="3.679162785091" Y="-0.181048833227" Z="0.65" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.65" />
                  <Point X="3.614168765949" Y="-0.114745497486" Z="0.65" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.65" />
                  <Point X="3.586178740795" Y="-0.024771473102" Z="0.65" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.65" />
                  <Point X="3.595192709629" Y="0.071839058128" Z="0.65" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.65" />
                  <Point X="3.641210672452" Y="0.149203241089" Z="0.65" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.65" />
                  <Point X="3.724232629262" Y="0.207100011015" Z="0.65" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.65" />
                  <Point X="4.081295874327" Y="0.310129667231" Z="0.65" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.65" />
                  <Point X="4.999010580588" Y="0.883909437875" Z="0.65" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.65" />
                  <Point X="4.906766374431" Y="1.301941390364" Z="0.65" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.65" />
                  <Point X="4.517288583205" Y="1.360807866533" Z="0.65" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.65" />
                  <Point X="3.393103966223" Y="1.231277741839" Z="0.65" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.65" />
                  <Point X="3.317175162106" Y="1.263619306154" Z="0.65" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.65" />
                  <Point X="3.263583218331" Y="1.32798716744" Z="0.65" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.65" />
                  <Point X="3.238122452822" Y="1.410392482505" Z="0.65" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.65" />
                  <Point X="3.249597125735" Y="1.489579580898" Z="0.65" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.65" />
                  <Point X="3.298082491123" Y="1.565366894508" Z="0.65" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.65" />
                  <Point X="3.603768093658" Y="1.807887443937" Z="0.65" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.65" />
                  <Point X="4.291805523022" Y="2.712137222266" Z="0.65" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.65" />
                  <Point X="4.062753072433" Y="3.044556552108" Z="0.65" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.65" />
                  <Point X="3.619605867174" Y="2.907700460247" Z="0.65" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.65" />
                  <Point X="2.45017749676" Y="2.251034096881" Z="0.65" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.65" />
                  <Point X="2.377967707085" Y="2.251754066833" Z="0.65" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.65" />
                  <Point X="2.313090776206" Y="2.285843520977" Z="0.65" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.65" />
                  <Point X="2.264915079258" Y="2.343934084176" Z="0.65" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.65" />
                  <Point X="2.247675635926" Y="2.411790736747" Z="0.65" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.65" />
                  <Point X="2.261493845776" Y="2.489292079122" Z="0.65" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.65" />
                  <Point X="2.487924892612" Y="2.892533214781" Z="0.65" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.65" />
                  <Point X="2.849682918537" Y="4.200630486783" Z="0.65" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.65" />
                  <Point X="2.457744198355" Y="4.441338757343" Z="0.65" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.65" />
                  <Point X="2.049601461394" Y="4.643934783701" Z="0.65" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.65" />
                  <Point X="1.62629803108" Y="4.808550627943" Z="0.65" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.65" />
                  <Point X="1.03029374554" Y="4.965731550357" Z="0.65" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.65" />
                  <Point X="0.364860388841" Y="5.058350271797" Z="0.65" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.65" />
                  <Point X="0.143695590101" Y="4.891403744019" Z="0.65" />
                  <Point X="0" Y="4.355124473572" Z="0.65" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>