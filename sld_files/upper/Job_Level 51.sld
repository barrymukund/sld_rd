<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#217" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3620" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004717773438" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.55772265625" Y="21.50285546875" />
                  <Point X="0.542365539551" Y="21.53262109375" />
                  <Point X="0.381134399414" Y="21.764923828125" />
                  <Point X="0.378637786865" Y="21.768521484375" />
                  <Point X="0.356758331299" Y="21.79097265625" />
                  <Point X="0.330502929688" Y="21.81021875" />
                  <Point X="0.302497375488" Y="21.824330078125" />
                  <Point X="0.05300151825" Y="21.901763671875" />
                  <Point X="0.049138343811" Y="21.902962890625" />
                  <Point X="0.02097927475" Y="21.907232421875" />
                  <Point X="-0.008665021896" Y="21.907232421875" />
                  <Point X="-0.036825004578" Y="21.902962890625" />
                  <Point X="-0.286300628662" Y="21.825533203125" />
                  <Point X="-0.290081787109" Y="21.82436328125" />
                  <Point X="-0.318149261475" Y="21.8102421875" />
                  <Point X="-0.344424133301" Y="21.7909921875" />
                  <Point X="-0.366323669434" Y="21.7685234375" />
                  <Point X="-0.527554931641" Y="21.53621875" />
                  <Point X="-0.534241455078" Y="21.525013671875" />
                  <Point X="-0.543958190918" Y="21.505849609375" />
                  <Point X="-0.550990356445" Y="21.487474609375" />
                  <Point X="-0.557098510742" Y="21.464677734375" />
                  <Point X="-0.916584411621" Y="20.123056640625" />
                  <Point X="-1.079046508789" Y="20.154591796875" />
                  <Point X="-1.079332519531" Y="20.154646484375" />
                  <Point X="-1.24641796875" Y="20.19763671875" />
                  <Point X="-1.214962890625" Y="20.436560546875" />
                  <Point X="-1.214200805664" Y="20.452068359375" />
                  <Point X="-1.216508300781" Y="20.483771484375" />
                  <Point X="-1.276112792969" Y="20.783423828125" />
                  <Point X="-1.277035766602" Y="20.788064453125" />
                  <Point X="-1.287937133789" Y="20.817029296875" />
                  <Point X="-1.304009887695" Y="20.844869140625" />
                  <Point X="-1.323645141602" Y="20.868794921875" />
                  <Point X="-1.553349121094" Y="21.070240234375" />
                  <Point X="-1.556906005859" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612885986328" Y="21.102005859375" />
                  <Point X="-1.64302734375" Y="21.109033203125" />
                  <Point X="-1.947895996094" Y="21.129015625" />
                  <Point X="-1.952651977539" Y="21.129326171875" />
                  <Point X="-1.983429321289" Y="21.1262890625" />
                  <Point X="-2.014469482422" Y="21.117970703125" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.296690185547" Y="20.9354609375" />
                  <Point X="-2.296711669922" Y="20.9354453125" />
                  <Point X="-2.315645751953" Y="20.92038671875" />
                  <Point X="-2.330959960938" Y="20.90478515625" />
                  <Point X="-2.338531982422" Y="20.896068359375" />
                  <Point X="-2.4801484375" Y="20.711509765625" />
                  <Point X="-2.801285888672" Y="20.910349609375" />
                  <Point X="-2.801708251953" Y="20.910611328125" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575195312" Y="22.4148046875" />
                  <Point X="-2.414559326172" Y="22.444421875" />
                  <Point X="-2.428776367188" Y="22.473251953125" />
                  <Point X="-2.4468046875" Y="22.498412109375" />
                  <Point X="-2.463888671875" Y="22.51549609375" />
                  <Point X="-2.464393066406" Y="22.515998046875" />
                  <Point X="-2.489493164062" Y="22.533908203125" />
                  <Point X="-2.518302734375" Y="22.54806640625" />
                  <Point X="-2.547890136719" Y="22.557005859375" />
                  <Point X="-2.578780761719" Y="22.55597265625" />
                  <Point X="-2.610261230469" Y="22.5496953125" />
                  <Point X="-2.639185058594" Y="22.53880078125" />
                  <Point X="-2.658845703125" Y="22.52744921875" />
                  <Point X="-3.818024658203" Y="21.8581953125" />
                  <Point X="-4.082527099609" Y="22.20569921875" />
                  <Point X="-4.082853027344" Y="22.206126953125" />
                  <Point X="-4.306141601563" Y="22.580548828125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.58016796875" />
                  <Point X="-3.046270996094" Y="23.609455078125" />
                  <Point X="-3.046157226562" Y="23.60989453125" />
                  <Point X="-3.043348632812" Y="23.640326171875" />
                  <Point X="-3.045554199219" Y="23.672" />
                  <Point X="-3.052553710938" Y="23.70175" />
                  <Point X="-3.068636474609" Y="23.727736328125" />
                  <Point X="-3.089469970703" Y="23.7516953125" />
                  <Point X="-3.112969970703" Y="23.77123046875" />
                  <Point X="-3.139048828125" Y="23.786580078125" />
                  <Point X="-3.139772949219" Y="23.78700390625" />
                  <Point X="-3.1689375" Y="23.798126953125" />
                  <Point X="-3.200718994141" Y="23.804546875" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.256749023438" Y="23.80234765625" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.833946777344" Y="24.006833984375" />
                  <Point X="-4.834077148438" Y="24.00734375" />
                  <Point X="-4.892423828125" Y="24.41530078125" />
                  <Point X="-3.532876220703" Y="24.77958984375" />
                  <Point X="-3.5174921875" Y="24.785171875" />
                  <Point X="-3.487727783203" Y="24.80053125" />
                  <Point X="-3.460480957031" Y="24.819443359375" />
                  <Point X="-3.460102294922" Y="24.819703125" />
                  <Point X="-3.437595458984" Y="24.841603515625" />
                  <Point X="-3.418306640625" Y="24.867890625" />
                  <Point X="-3.404168212891" Y="24.89593359375" />
                  <Point X="-3.395069824219" Y="24.92525" />
                  <Point X="-3.394951660156" Y="24.92562890625" />
                  <Point X="-3.390655029297" Y="24.9538515625" />
                  <Point X="-3.390647705078" Y="24.983515625" />
                  <Point X="-3.394916748047" Y="25.011697265625" />
                  <Point X="-3.404026611328" Y="25.04105078125" />
                  <Point X="-3.404169921875" Y="25.041513671875" />
                  <Point X="-3.418267822266" Y="25.069490234375" />
                  <Point X="-3.437517089844" Y="25.0957578125" />
                  <Point X="-3.459980224609" Y="25.117650390625" />
                  <Point X="-3.487225341797" Y="25.13655859375" />
                  <Point X="-3.500895751953" Y="25.14444921875" />
                  <Point X="-3.532875" Y="25.15784765625" />
                  <Point X="-3.555499511719" Y="25.16391015625" />
                  <Point X="-4.89181640625" Y="25.521974609375" />
                  <Point X="-4.824570800781" Y="25.9764140625" />
                  <Point X="-4.824487304688" Y="25.976978515625" />
                  <Point X="-4.703551269531" Y="26.423267578125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423339844" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.642696777344" Y="26.3243203125" />
                  <Point X="-3.641760253906" Y="26.324615234375" />
                  <Point X="-3.622813720703" Y="26.3329453125" />
                  <Point X="-3.604059326172" Y="26.343767578125" />
                  <Point X="-3.587372070312" Y="26.355998046875" />
                  <Point X="-3.573728027344" Y="26.37155078125" />
                  <Point X="-3.561307128906" Y="26.38928515625" />
                  <Point X="-3.551351806641" Y="26.4074296875" />
                  <Point X="-3.527080078125" Y="26.466025390625" />
                  <Point X="-3.5266875" Y="26.46697265625" />
                  <Point X="-3.520902587891" Y="26.48683984375" />
                  <Point X="-3.517152587891" Y="26.50814453125" />
                  <Point X="-3.515805419922" Y="26.5287734375" />
                  <Point X="-3.518952392578" Y="26.549205078125" />
                  <Point X="-3.524552001953" Y="26.570099609375" />
                  <Point X="-3.532048095703" Y="26.589375" />
                  <Point X="-3.561334472656" Y="26.6456328125" />
                  <Point X="-3.561787841797" Y="26.64650390625" />
                  <Point X="-3.573277099609" Y="26.66369921875" />
                  <Point X="-3.587191650391" Y="26.680283203125" />
                  <Point X="-3.602132324219" Y="26.6945859375" />
                  <Point X="-3.615109863281" Y="26.704544921875" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.081476074219" Y="27.733107421875" />
                  <Point X="-4.081155761719" Y="27.73365625" />
                  <Point X="-3.750503417969" Y="28.158662109375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729736328" Y="27.836341796875" />
                  <Point X="-3.167087890625" Y="27.82983203125" />
                  <Point X="-3.146795654297" Y="27.825794921875" />
                  <Point X="-3.06255078125" Y="27.818423828125" />
                  <Point X="-3.061093261719" Y="27.818298828125" />
                  <Point X="-3.040485595703" Y="27.818765625" />
                  <Point X="-3.019043701172" Y="27.821599609375" />
                  <Point X="-2.998971191406" Y="27.82651953125" />
                  <Point X="-2.980437744141" Y="27.835666015625" />
                  <Point X="-2.962198486328" Y="27.8472890625" />
                  <Point X="-2.946077636719" Y="27.860228515625" />
                  <Point X="-2.886280029297" Y="27.920025390625" />
                  <Point X="-2.885340576172" Y="27.92096484375" />
                  <Point X="-2.872416748047" Y="27.9370703125" />
                  <Point X="-2.860783691406" Y="27.955326171875" />
                  <Point X="-2.851631835938" Y="27.97387890625" />
                  <Point X="-2.846713623047" Y="27.993974609375" />
                  <Point X="-2.843887451172" Y="28.015435546875" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.850806396484" Y="28.120365234375" />
                  <Point X="-2.850920166016" Y="28.121666015625" />
                  <Point X="-2.854954101562" Y="28.14194921875" />
                  <Point X="-2.861462890625" Y="28.162595703125" />
                  <Point X="-2.869798339844" Y="28.1815390625" />
                  <Point X="-2.875549072266" Y="28.191498046875" />
                  <Point X="-3.183332519531" Y="28.72459375" />
                  <Point X="-2.701181396484" Y="29.094255859375" />
                  <Point X="-2.700622070312" Y="29.09468359375" />
                  <Point X="-2.167035888672" Y="29.391134765625" />
                  <Point X="-2.04319519043" Y="29.2297421875" />
                  <Point X="-2.028896850586" Y="29.2148046875" />
                  <Point X="-2.012319335938" Y="29.200892578125" />
                  <Point X="-1.995116699219" Y="29.189396484375" />
                  <Point X="-1.901352416992" Y="29.140583984375" />
                  <Point X="-1.899900634766" Y="29.139828125" />
                  <Point X="-1.880626342773" Y="29.13233203125" />
                  <Point X="-1.859718505859" Y="29.126728515625" />
                  <Point X="-1.839269775391" Y="29.123580078125" />
                  <Point X="-1.818624389648" Y="29.12493359375" />
                  <Point X="-1.797307861328" Y="29.128693359375" />
                  <Point X="-1.777453735352" Y="29.134482421875" />
                  <Point X="-1.679791870117" Y="29.174935546875" />
                  <Point X="-1.678251953125" Y="29.175572265625" />
                  <Point X="-1.660111816406" Y="29.185529296875" />
                  <Point X="-1.642391601562" Y="29.197943359375" />
                  <Point X="-1.626848266602" Y="29.211580078125" />
                  <Point X="-1.614623535156" Y="29.228255859375" />
                  <Point X="-1.603807006836" Y="29.246994140625" />
                  <Point X="-1.595479980469" Y="29.265921875" />
                  <Point X="-1.563702270508" Y="29.366708984375" />
                  <Point X="-1.563209838867" Y="29.36826953125" />
                  <Point X="-1.559168457031" Y="29.3885703125" />
                  <Point X="-1.557279785156" Y="29.410138671875" />
                  <Point X="-1.557730712891" Y="29.430828125" />
                  <Point X="-1.558384521484" Y="29.43579296875" />
                  <Point X="-1.584202148438" Y="29.6318984375" />
                  <Point X="-0.950351806641" Y="29.809607421875" />
                  <Point X="-0.949633789063" Y="29.80980859375" />
                  <Point X="-0.294711364746" Y="29.88645703125" />
                  <Point X="-0.133903289795" Y="29.286314453125" />
                  <Point X="-0.121765380859" Y="29.256525390625" />
                  <Point X="-0.109663383484" Y="29.236763671875" />
                  <Point X="-0.093137512207" Y="29.220521484375" />
                  <Point X="-0.067945854187" Y="29.20119140625" />
                  <Point X="-0.040650428772" Y="29.1866015625" />
                  <Point X="-0.010113883972" Y="29.181560546875" />
                  <Point X="0.022425701141" Y="29.181560546875" />
                  <Point X="0.052962341309" Y="29.1866015625" />
                  <Point X="0.080257827759" Y="29.20119140625" />
                  <Point X="0.105449478149" Y="29.220521484375" />
                  <Point X="0.123928771973" Y="29.23930859375" />
                  <Point X="0.136536407471" Y="29.262447265625" />
                  <Point X="0.146317962646" Y="29.288455078125" />
                  <Point X="0.149161819458" Y="29.297310546875" />
                  <Point X="0.307419555664" Y="29.887939453125" />
                  <Point X="0.843407958984" Y="29.8318046875" />
                  <Point X="0.844043273926" Y="29.831736328125" />
                  <Point X="1.478888916016" Y="29.67846484375" />
                  <Point X="1.481035888672" Y="29.677947265625" />
                  <Point X="1.89423046875" Y="29.528078125" />
                  <Point X="1.894643554688" Y="29.527927734375" />
                  <Point X="2.294217285156" Y="29.341060546875" />
                  <Point X="2.294578369141" Y="29.340890625" />
                  <Point X="2.680596679688" Y="29.11599609375" />
                  <Point X="2.680991455078" Y="29.115765625" />
                  <Point X="2.943260498047" Y="28.929255859375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074707031" Y="27.539927734375" />
                  <Point X="2.133076171875" Y="27.516052734375" />
                  <Point X="2.111933837891" Y="27.436990234375" />
                  <Point X="2.109032226562" Y="27.420279296875" />
                  <Point X="2.107367919922" Y="27.400154296875" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.115968505859" Y="27.31261328125" />
                  <Point X="2.116095947266" Y="27.3115546875" />
                  <Point X="2.121437744141" Y="27.289623046875" />
                  <Point X="2.129704833984" Y="27.267525390625" />
                  <Point X="2.140070556641" Y="27.247470703125" />
                  <Point X="2.182373291016" Y="27.185126953125" />
                  <Point X="2.193074951172" Y="27.172037109375" />
                  <Point X="2.221598632812" Y="27.145591796875" />
                  <Point X="2.283921386719" Y="27.103302734375" />
                  <Point X="2.284850585938" Y="27.102671875" />
                  <Point X="2.304877197266" Y="27.092310546875" />
                  <Point X="2.327002929688" Y="27.08401953125" />
                  <Point X="2.348967285156" Y="27.078662109375" />
                  <Point X="2.417333496094" Y="27.070419921875" />
                  <Point X="2.434447998047" Y="27.06991015625" />
                  <Point X="2.473206787109" Y="27.074169921875" />
                  <Point X="2.552268798828" Y="27.0953125" />
                  <Point X="2.563049316406" Y="27.0988984375" />
                  <Point X="2.588530273438" Y="27.110142578125" />
                  <Point X="2.611286132812" Y="27.123279296875" />
                  <Point X="3.967326904297" Y="27.90619140625" />
                  <Point X="4.12304296875" Y="27.68978125" />
                  <Point X="4.123269042969" Y="27.689466796875" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221423339844" Y="26.660240234375" />
                  <Point X="3.203973388672" Y="26.641626953125" />
                  <Point X="3.147085693359" Y="26.567412109375" />
                  <Point X="3.138089111328" Y="26.553236328125" />
                  <Point X="3.128725341797" Y="26.535119140625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.100434082031" Y="26.441294921875" />
                  <Point X="3.100104248047" Y="26.440115234375" />
                  <Point X="3.096650878906" Y="26.4178125" />
                  <Point X="3.0958359375" Y="26.39425" />
                  <Point X="3.097739013672" Y="26.37176953125" />
                  <Point X="3.115138427734" Y="26.287443359375" />
                  <Point X="3.119963134766" Y="26.2713828125" />
                  <Point X="3.127431152344" Y="26.252697265625" />
                  <Point X="3.136282470703" Y="26.235740234375" />
                  <Point X="3.183607177734" Y="26.16380859375" />
                  <Point X="3.184333007812" Y="26.162705078125" />
                  <Point X="3.198913818359" Y="26.14542578125" />
                  <Point X="3.216149169922" Y="26.129349609375" />
                  <Point X="3.234345947266" Y="26.11603515625" />
                  <Point X="3.302926025391" Y="26.0774296875" />
                  <Point X="3.318428955078" Y="26.07044921875" />
                  <Point X="3.337467285156" Y="26.063853515625" />
                  <Point X="3.356119628906" Y="26.0594375" />
                  <Point X="3.448844482422" Y="26.04718359375" />
                  <Point X="3.460025146484" Y="26.046373046875" />
                  <Point X="3.488202880859" Y="26.046984375" />
                  <Point X="3.509819580078" Y="26.049830078125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845839355469" Y="25.933205078125" />
                  <Point X="4.845935546875" Y="25.932810546875" />
                  <Point X="4.890864746094" Y="25.64423828125" />
                  <Point X="3.716580078125" Y="25.32958984375" />
                  <Point X="3.704788818359" Y="25.325583984375" />
                  <Point X="3.681546630859" Y="25.31506640625" />
                  <Point X="3.590447265625" Y="25.26241015625" />
                  <Point X="3.5768125" Y="25.25284375" />
                  <Point X="3.561091308594" Y="25.239611328125" />
                  <Point X="3.547532226562" Y="25.225580078125" />
                  <Point X="3.492872558594" Y="25.1559296875" />
                  <Point X="3.492026367188" Y="25.1548515625" />
                  <Point X="3.480302978516" Y="25.135572265625" />
                  <Point X="3.470527587891" Y="25.11410546875" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.4454609375" Y="24.997466796875" />
                  <Point X="3.443774658203" Y="24.980919921875" />
                  <Point X="3.443492431641" Y="24.96063671875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.463398681641" Y="24.84630859375" />
                  <Point X="3.463681884766" Y="24.844830078125" />
                  <Point X="3.470531005859" Y="24.823322265625" />
                  <Point X="3.480305419922" Y="24.801861328125" />
                  <Point X="3.492026367188" Y="24.782587890625" />
                  <Point X="3.546686035156" Y="24.7129375" />
                  <Point X="3.558244628906" Y="24.700638671875" />
                  <Point X="3.573401367188" Y="24.687142578125" />
                  <Point X="3.589036376953" Y="24.67584375" />
                  <Point X="3.680111816406" Y="24.623201171875" />
                  <Point X="3.689981445312" Y="24.61823828125" />
                  <Point X="3.716584960938" Y="24.60784765625" />
                  <Point X="3.736408447266" Y="24.602537109375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855077148438" Y="24.05163671875" />
                  <Point X="4.855020996094" Y="24.051265625" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="3.424382324219" Y="23.99655859375" />
                  <Point X="3.40803515625" Y="23.9972890625" />
                  <Point X="3.374661132812" Y="23.994490234375" />
                  <Point X="3.195865234375" Y="23.95562890625" />
                  <Point X="3.193096923828" Y="23.95502734375" />
                  <Point X="3.163978759766" Y="23.943404296875" />
                  <Point X="3.136149902344" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.004332763672" Y="23.7760703125" />
                  <Point X="3.002675537109" Y="23.774078125" />
                  <Point X="2.987940917969" Y="23.749681640625" />
                  <Point X="2.976591064453" Y="23.72228515625" />
                  <Point X="2.969757080078" Y="23.694630859375" />
                  <Point X="2.954267822266" Y="23.526306640625" />
                  <Point X="2.954028076172" Y="23.52369921875" />
                  <Point X="2.95634765625" Y="23.492427734375" />
                  <Point X="2.964080078125" Y="23.46080859375" />
                  <Point X="2.976450927734" Y="23.432" />
                  <Point X="3.075399169922" Y="23.27809375" />
                  <Point X="3.083856933594" Y="23.266861328125" />
                  <Point X="3.110625" Y="23.23909375" />
                  <Point X="3.129021240234" Y="23.2249765625" />
                  <Point X="4.213123046875" Y="22.3931171875" />
                  <Point X="4.1249609375" Y="22.25045703125" />
                  <Point X="4.124810058594" Y="22.250212890625" />
                  <Point X="4.028979980469" Y="22.114052734375" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129638672" Y="22.829986328125" />
                  <Point X="2.754225341797" Y="22.840173828125" />
                  <Point X="2.541429931641" Y="22.87860546875" />
                  <Point X="2.538091064453" Y="22.87920703125" />
                  <Point X="2.506745361328" Y="22.879603515625" />
                  <Point X="2.474591796875" Y="22.874642578125" />
                  <Point X="2.444833984375" Y="22.864822265625" />
                  <Point X="2.268053222656" Y="22.77178515625" />
                  <Point X="2.265272460938" Y="22.7703203125" />
                  <Point X="2.242372314453" Y="22.75344140625" />
                  <Point X="2.221424560547" Y="22.7324921875" />
                  <Point X="2.204533935547" Y="22.709564453125" />
                  <Point X="2.111495361328" Y="22.532783203125" />
                  <Point X="2.110054931641" Y="22.530046875" />
                  <Point X="2.100232177734" Y="22.50027734375" />
                  <Point X="2.095271972656" Y="22.4681015625" />
                  <Point X="2.095675292969" Y="22.436744140625" />
                  <Point X="2.134105957031" Y="22.223947265625" />
                  <Point X="2.137507568359" Y="22.210671875" />
                  <Point X="2.144005615234" Y="22.19126171875" />
                  <Point X="2.151820068359" Y="22.17391796875" />
                  <Point X="2.163641601562" Y="22.153443359375" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.782035644531" Y="20.88848828125" />
                  <Point X="2.781881347656" Y="20.88837890625" />
                  <Point X="2.701763916016" Y="20.83651953125" />
                  <Point X="1.758545898438" Y="22.065744140625" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721924194336" Y="22.099443359375" />
                  <Point X="1.512050415039" Y="22.234373046875" />
                  <Point X="1.508764892578" Y="22.236484375" />
                  <Point X="1.47996081543" Y="22.24884375" />
                  <Point X="1.448352172852" Y="22.25656640625" />
                  <Point X="1.417100341797" Y="22.258880859375" />
                  <Point X="1.187579956055" Y="22.23776171875" />
                  <Point X="1.184048583984" Y="22.2374375" />
                  <Point X="1.15638293457" Y="22.230607421875" />
                  <Point X="1.128985351563" Y="22.21926171875" />
                  <Point X="1.104595581055" Y="22.2045390625" />
                  <Point X="0.927356262207" Y="22.057169921875" />
                  <Point X="0.924600952148" Y="22.05487890625" />
                  <Point X="0.904125793457" Y="22.03112109375" />
                  <Point X="0.887245361328" Y="22.00330859375" />
                  <Point X="0.875625244141" Y="21.9741953125" />
                  <Point X="0.822631652832" Y="21.7303828125" />
                  <Point X="0.820718566895" Y="21.717154296875" />
                  <Point X="0.819184020996" Y="21.69623046875" />
                  <Point X="0.81974230957" Y="21.6768828125" />
                  <Point X="0.823092224121" Y="21.651435546875" />
                  <Point X="1.022065612793" Y="20.140083984375" />
                  <Point X="0.97586328125" Y="20.12995703125" />
                  <Point X="0.975732971191" Y="20.129927734375" />
                  <Point X="0.929315368652" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058439453125" Y="20.247365234375" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451416016" Y="20.45896484375" />
                  <Point X="-1.121759033203" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.5023046875" />
                  <Point X="-1.182938232422" Y="20.80195703125" />
                  <Point X="-1.188124511719" Y="20.82152734375" />
                  <Point X="-1.199026000977" Y="20.8504921875" />
                  <Point X="-1.20566394043" Y="20.86452734375" />
                  <Point X="-1.221736694336" Y="20.8923671875" />
                  <Point X="-1.230573730469" Y="20.90513671875" />
                  <Point X="-1.250208984375" Y="20.9290625" />
                  <Point X="-1.261007202148" Y="20.94021875" />
                  <Point X="-1.490711181641" Y="21.1416640625" />
                  <Point X="-1.506735473633" Y="21.15403125" />
                  <Point X="-1.533018554688" Y="21.170376953125" />
                  <Point X="-1.546834228516" Y="21.177474609375" />
                  <Point X="-1.57653125" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.62145690918" Y="21.201552734375" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.941682617188" Y="21.2238125" />
                  <Point X="-1.941705932617" Y="21.223814453125" />
                  <Point X="-1.961981323242" Y="21.2238671875" />
                  <Point X="-1.992758666992" Y="21.220830078125" />
                  <Point X="-2.008020385742" Y="21.21805078125" />
                  <Point X="-2.039060546875" Y="21.209732421875" />
                  <Point X="-2.053676025391" Y="21.20450390625" />
                  <Point X="-2.081863769531" Y="21.191732421875" />
                  <Point X="-2.095436279297" Y="21.184189453125" />
                  <Point X="-2.349469238281" Y="21.014451171875" />
                  <Point X="-2.355845214844" Y="21.009796875" />
                  <Point X="-2.374779296875" Y="20.99473828125" />
                  <Point X="-2.383442382812" Y="20.986935546875" />
                  <Point X="-2.398756591797" Y="20.971333984375" />
                  <Point X="-2.413900634766" Y="20.953900390625" />
                  <Point X="-2.503201660156" Y="20.837521484375" />
                  <Point X="-2.747581298828" Y="20.988833984375" />
                  <Point X="-2.980862304688" Y="21.168453125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665771484" Y="22.442380859375" />
                  <Point X="-2.323649902344" Y="22.471998046875" />
                  <Point X="-2.329355957031" Y="22.4864375" />
                  <Point X="-2.343572998047" Y="22.515267578125" />
                  <Point X="-2.351554199219" Y="22.528583984375" />
                  <Point X="-2.369582519531" Y="22.553744140625" />
                  <Point X="-2.379629638672" Y="22.565587890625" />
                  <Point X="-2.396713623047" Y="22.582671875" />
                  <Point X="-2.409213134766" Y="22.593330078125" />
                  <Point X="-2.434313232422" Y="22.611240234375" />
                  <Point X="-2.447592773438" Y="22.61916796875" />
                  <Point X="-2.47640234375" Y="22.633326171875" />
                  <Point X="-2.490826416016" Y="22.639005859375" />
                  <Point X="-2.520413818359" Y="22.6479453125" />
                  <Point X="-2.551065917969" Y="22.651953125" />
                  <Point X="-2.581956542969" Y="22.650919921875" />
                  <Point X="-2.597358398438" Y="22.649138671875" />
                  <Point X="-2.628838867188" Y="22.642861328125" />
                  <Point X="-2.643747558594" Y="22.63859765625" />
                  <Point X="-2.672671386719" Y="22.627703125" />
                  <Point X="-2.686686523438" Y="22.621072265625" />
                  <Point X="-2.706347167969" Y="22.609720703125" />
                  <Point X="-3.79308984375" Y="21.982287109375" />
                  <Point X="-4.004007568359" Y="22.259392578125" />
                  <Point X="-4.181264160156" Y="22.556625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481201172" Y="23.436689453125" />
                  <Point X="-3.015102783203" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833740234" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890625" Y="23.556349609375" />
                  <Point X="-2.954305419922" Y="23.58563671875" />
                  <Point X="-2.951559326172" Y="23.6011640625" />
                  <Point X="-2.948750732422" Y="23.631595703125" />
                  <Point X="-2.948578125" Y="23.64692578125" />
                  <Point X="-2.950783691406" Y="23.678599609375" />
                  <Point X="-2.953079345703" Y="23.6937578125" />
                  <Point X="-2.960078857422" Y="23.7235078125" />
                  <Point X="-2.971772949219" Y="23.751744140625" />
                  <Point X="-2.987855712891" Y="23.77773046875" />
                  <Point X="-2.996948242188" Y="23.790072265625" />
                  <Point X="-3.017781738281" Y="23.81403125" />
                  <Point X="-3.028740966797" Y="23.82475" />
                  <Point X="-3.052240966797" Y="23.84428515625" />
                  <Point X="-3.064781738281" Y="23.8531015625" />
                  <Point X="-3.090860595703" Y="23.868451171875" />
                  <Point X="-3.105919433594" Y="23.875767578125" />
                  <Point X="-3.135083984375" Y="23.886890625" />
                  <Point X="-3.150127197266" Y="23.89124609375" />
                  <Point X="-3.181908691406" Y="23.897666015625" />
                  <Point X="-3.197468994141" Y="23.8994921875" />
                  <Point X="-3.228679199219" Y="23.900560546875" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.269148925781" Y="23.89653515625" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.34534375" />
                  <Point X="-3.508288330078" Y="24.687826171875" />
                  <Point X="-3.500472900391" Y="24.690287109375" />
                  <Point X="-3.473927490234" Y="24.70075" />
                  <Point X="-3.444163085938" Y="24.716109375" />
                  <Point X="-3.433558105469" Y="24.72248828125" />
                  <Point X="-3.406412597656" Y="24.741330078125" />
                  <Point X="-3.393850585938" Y="24.7516171875" />
                  <Point X="-3.37134375" Y="24.773517578125" />
                  <Point X="-3.361003173828" Y="24.78540234375" />
                  <Point X="-3.341714355469" Y="24.811689453125" />
                  <Point X="-3.333478027344" Y="24.825123046875" />
                  <Point X="-3.319339599609" Y="24.853166015625" />
                  <Point X="-3.313437255859" Y="24.867775390625" />
                  <Point X="-3.304377685547" Y="24.896966796875" />
                  <Point X="-3.301033691406" Y="24.911330078125" />
                  <Point X="-3.296737060547" Y="24.939552734375" />
                  <Point X="-3.295655029297" Y="24.953828125" />
                  <Point X="-3.295647705078" Y="24.9834921875" />
                  <Point X="-3.296719238281" Y="24.997744140625" />
                  <Point X="-3.30098828125" Y="25.02592578125" />
                  <Point X="-3.304185791016" Y="25.03985546875" />
                  <Point X="-3.313295654297" Y="25.069208984375" />
                  <Point X="-3.319332763672" Y="25.084265625" />
                  <Point X="-3.333430664062" Y="25.1122421875" />
                  <Point X="-3.341640380859" Y="25.12564453125" />
                  <Point X="-3.360889648438" Y="25.151912109375" />
                  <Point X="-3.371211425781" Y="25.163791015625" />
                  <Point X="-3.393674560547" Y="25.18568359375" />
                  <Point X="-3.405815917969" Y="25.195697265625" />
                  <Point X="-3.433061035156" Y="25.21460546875" />
                  <Point X="-3.439734375" Y="25.2188359375" />
                  <Point X="-3.464185302734" Y="25.2320703125" />
                  <Point X="-3.496164550781" Y="25.24546875" />
                  <Point X="-3.508286132812" Y="25.249609375" />
                  <Point X="-3.530910644531" Y="25.255671875" />
                  <Point X="-4.785446289062" Y="25.591822265625" />
                  <Point X="-4.731330078125" Y="25.95753515625" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736703857422" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704888916016" Y="26.2080546875" />
                  <Point X="-3.684603759766" Y="26.21208984375" />
                  <Point X="-3.674571777344" Y="26.21466015625" />
                  <Point X="-3.614130371094" Y="26.233716796875" />
                  <Point X="-3.603524658203" Y="26.237650390625" />
                  <Point X="-3.584578125" Y="26.24598046875" />
                  <Point X="-3.57533203125" Y="26.250662109375" />
                  <Point X="-3.556577636719" Y="26.261484375" />
                  <Point X="-3.547900146484" Y="26.26714453125" />
                  <Point X="-3.531212890625" Y="26.279375" />
                  <Point X="-3.515958007812" Y="26.29334765625" />
                  <Point X="-3.502313964844" Y="26.308900390625" />
                  <Point X="-3.495915039062" Y="26.31705078125" />
                  <Point X="-3.483494140625" Y="26.33478515625" />
                  <Point X="-3.478019775391" Y="26.343587890625" />
                  <Point X="-3.468064453125" Y="26.361732421875" />
                  <Point X="-3.463583496094" Y="26.37107421875" />
                  <Point X="-3.439311767578" Y="26.429669921875" />
                  <Point X="-3.435475585938" Y="26.4404140625" />
                  <Point X="-3.429690673828" Y="26.46028125" />
                  <Point X="-3.427340820312" Y="26.47037109375" />
                  <Point X="-3.423590820312" Y="26.49167578125" />
                  <Point X="-3.422354492188" Y="26.501953125" />
                  <Point X="-3.421007324219" Y="26.52258203125" />
                  <Point X="-3.421912597656" Y="26.543234375" />
                  <Point X="-3.425059570312" Y="26.563666015625" />
                  <Point X="-3.427190429688" Y="26.573796875" />
                  <Point X="-3.432790039062" Y="26.59469140625" />
                  <Point X="-3.43601171875" Y="26.604533203125" />
                  <Point X="-3.4435078125" Y="26.62380859375" />
                  <Point X="-3.447782226562" Y="26.6332421875" />
                  <Point X="-3.477068603516" Y="26.6895" />
                  <Point X="-3.482797607422" Y="26.69928125" />
                  <Point X="-3.494286865234" Y="26.7164765625" />
                  <Point X="-3.500500488281" Y="26.72476171875" />
                  <Point X="-3.514415039062" Y="26.741345703125" />
                  <Point X="-3.521497558594" Y="26.748908203125" />
                  <Point X="-3.536438232422" Y="26.7632109375" />
                  <Point X="-3.557273925781" Y="26.77991015625" />
                  <Point X="-4.227615234375" Y="27.29428125" />
                  <Point X="-4.002294677734" Y="27.680310546875" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244925292969" Y="27.757720703125" />
                  <Point X="-3.225998046875" Y="27.749390625" />
                  <Point X="-3.216302490234" Y="27.745740234375" />
                  <Point X="-3.195660644531" Y="27.73923046875" />
                  <Point X="-3.185624755859" Y="27.736658203125" />
                  <Point X="-3.165332519531" Y="27.73262109375" />
                  <Point X="-3.155076171875" Y="27.73115625" />
                  <Point X="-3.070831298828" Y="27.72378515625" />
                  <Point X="-3.058941894531" Y="27.723322265625" />
                  <Point X="-3.038334228516" Y="27.7237890625" />
                  <Point X="-3.028037597656" Y="27.724583984375" />
                  <Point X="-3.006595703125" Y="27.72741796875" />
                  <Point X="-2.996427978516" Y="27.729330078125" />
                  <Point X="-2.97635546875" Y="27.73425" />
                  <Point X="-2.956928710938" Y="27.741328125" />
                  <Point X="-2.938395263672" Y="27.750474609375" />
                  <Point X="-2.929383789062" Y="27.75555078125" />
                  <Point X="-2.91114453125" Y="27.767173828125" />
                  <Point X="-2.902732666016" Y="27.773203125" />
                  <Point X="-2.886611816406" Y="27.786142578125" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.819105224609" Y="27.852849609375" />
                  <Point X="-2.811246582031" Y="27.8615078125" />
                  <Point X="-2.798322753906" Y="27.87761328125" />
                  <Point X="-2.792300048828" Y="27.886017578125" />
                  <Point X="-2.780666992188" Y="27.9042734375" />
                  <Point X="-2.775585693359" Y="27.913298828125" />
                  <Point X="-2.766433837891" Y="27.9318515625" />
                  <Point X="-2.759355224609" Y="27.951294921875" />
                  <Point X="-2.754437011719" Y="27.971390625" />
                  <Point X="-2.752526855469" Y="27.9815703125" />
                  <Point X="-2.749700683594" Y="28.00303125" />
                  <Point X="-2.74891015625" Y="28.013361328125" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.75616796875" Y="28.12864453125" />
                  <Point X="-2.757745117188" Y="28.140197265625" />
                  <Point X="-2.761779052734" Y="28.16048046875" />
                  <Point X="-2.764349609375" Y="28.17051171875" />
                  <Point X="-2.770858398438" Y="28.191158203125" />
                  <Point X="-2.774508544922" Y="28.200857421875" />
                  <Point X="-2.782843994141" Y="28.21980078125" />
                  <Point X="-2.793280029297" Y="28.23900390625" />
                  <Point X="-3.05938671875" Y="28.6999140625" />
                  <Point X="-2.648375" Y="29.015033203125" />
                  <Point X="-2.1925234375" Y="29.268296875" />
                  <Point X="-2.118563720703" Y="29.17191015625" />
                  <Point X="-2.111822509766" Y="29.16405078125" />
                  <Point X="-2.097524169922" Y="29.14911328125" />
                  <Point X="-2.089966552734" Y="29.14203515625" />
                  <Point X="-2.073389160156" Y="29.128123046875" />
                  <Point X="-2.065103759766" Y="29.12190625" />
                  <Point X="-2.047901000977" Y="29.11041015625" />
                  <Point X="-2.038984130859" Y="29.105130859375" />
                  <Point X="-1.945219848633" Y="29.056318359375" />
                  <Point X="-1.934335205078" Y="29.0512890625" />
                  <Point X="-1.915060913086" Y="29.04379296875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.884311523438" Y="29.034966796875" />
                  <Point X="-1.874175048828" Y="29.032833984375" />
                  <Point X="-1.853726318359" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812409545898" Y="29.03013671875" />
                  <Point X="-1.802123291016" Y="29.031376953125" />
                  <Point X="-1.780806640625" Y="29.03513671875" />
                  <Point X="-1.770715209961" Y="29.0374921875" />
                  <Point X="-1.750861083984" Y="29.04328125" />
                  <Point X="-1.741098632812" Y="29.04671484375" />
                  <Point X="-1.64349230957" Y="29.08714453125" />
                  <Point X="-1.632540405273" Y="29.09229296875" />
                  <Point X="-1.614400146484" Y="29.10225" />
                  <Point X="-1.605603637695" Y="29.10772265625" />
                  <Point X="-1.587883422852" Y="29.12013671875" />
                  <Point X="-1.579739257812" Y="29.12653125" />
                  <Point X="-1.564195922852" Y="29.14016796875" />
                  <Point X="-1.55023059082" Y="29.155412109375" />
                  <Point X="-1.538005981445" Y="29.172087890625" />
                  <Point X="-1.532347290039" Y="29.18076171875" />
                  <Point X="-1.521530761719" Y="29.1995" />
                  <Point X="-1.516850097656" Y="29.20873828125" />
                  <Point X="-1.508523071289" Y="29.227666015625" />
                  <Point X="-1.504876831055" Y="29.23735546875" />
                  <Point X="-1.473105957031" Y="29.33812109375" />
                  <Point X="-1.470038208008" Y="29.349720703125" />
                  <Point X="-1.465996704102" Y="29.370021484375" />
                  <Point X="-1.464530639648" Y="29.380283203125" />
                  <Point X="-1.462641967773" Y="29.4018515625" />
                  <Point X="-1.462302368164" Y="29.412208984375" />
                  <Point X="-1.462753295898" Y="29.4328984375" />
                  <Point X="-1.464197631836" Y="29.4481953125" />
                  <Point X="-1.479266601562" Y="29.56265625" />
                  <Point X="-0.931176147461" Y="29.7163203125" />
                  <Point X="-0.365222595215" Y="29.782556640625" />
                  <Point X="-0.225666229248" Y="29.2617265625" />
                  <Point X="-0.221880355835" Y="29.250466796875" />
                  <Point X="-0.209742477417" Y="29.220677734375" />
                  <Point X="-0.202780822754" Y="29.206912109375" />
                  <Point X="-0.190678756714" Y="29.187150390625" />
                  <Point X="-0.176254455566" Y="29.169009765625" />
                  <Point X="-0.159728637695" Y="29.152767578125" />
                  <Point X="-0.150969543457" Y="29.14515234375" />
                  <Point X="-0.125777816772" Y="29.125822265625" />
                  <Point X="-0.112728874207" Y="29.117408203125" />
                  <Point X="-0.085433387756" Y="29.102818359375" />
                  <Point X="-0.056123767853" Y="29.09287109375" />
                  <Point X="-0.0255872612" Y="29.087830078125" />
                  <Point X="-0.010113833427" Y="29.086560546875" />
                  <Point X="0.022425662994" Y="29.086560546875" />
                  <Point X="0.03789894104" Y="29.087830078125" />
                  <Point X="0.068435592651" Y="29.09287109375" />
                  <Point X="0.09774521637" Y="29.102818359375" />
                  <Point X="0.12504070282" Y="29.117408203125" />
                  <Point X="0.138089950562" Y="29.125822265625" />
                  <Point X="0.163281524658" Y="29.14515234375" />
                  <Point X="0.173177139282" Y="29.153904296875" />
                  <Point X="0.191656463623" Y="29.17269140625" />
                  <Point X="0.207349258423" Y="29.19385546875" />
                  <Point X="0.219956939697" Y="29.216994140625" />
                  <Point X="0.225455535889" Y="29.22900390625" />
                  <Point X="0.235236999512" Y="29.25501171875" />
                  <Point X="0.240924804688" Y="29.27272265625" />
                  <Point X="0.378190673828" Y="29.7850078125" />
                  <Point X="0.827850219727" Y="29.737916015625" />
                  <Point X="1.453624633789" Y="29.586833984375" />
                  <Point X="1.858241455078" Y="29.440076171875" />
                  <Point X="2.250441162109" Y="29.256658203125" />
                  <Point X="2.629447021484" Y="29.03584765625" />
                  <Point X="2.817780029297" Y="28.90191796875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062371826172" Y="27.5931015625" />
                  <Point X="2.053179199219" Y="27.57343359375" />
                  <Point X="2.044180664062" Y="27.54955859375" />
                  <Point X="2.04130090332" Y="27.54059375" />
                  <Point X="2.020158569336" Y="27.46153125" />
                  <Point X="2.018334350586" Y="27.4532421875" />
                  <Point X="2.01435546875" Y="27.428109375" />
                  <Point X="2.012691162109" Y="27.407984375" />
                  <Point X="2.012384643555" Y="27.398373046875" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.021649658203" Y="27.3012578125" />
                  <Point X="2.023794311523" Y="27.289072265625" />
                  <Point X="2.029136108398" Y="27.267140625" />
                  <Point X="2.032460693359" Y="27.2563359375" />
                  <Point X="2.040727783203" Y="27.23423828125" />
                  <Point X="2.045311523438" Y="27.223904296875" />
                  <Point X="2.055677246094" Y="27.203849609375" />
                  <Point X="2.061459228516" Y="27.19412890625" />
                  <Point X="2.103761962891" Y="27.13178515625" />
                  <Point X="2.108824707031" Y="27.12499609375" />
                  <Point X="2.128485839844" Y="27.10237109375" />
                  <Point X="2.157009521484" Y="27.07592578125" />
                  <Point X="2.168257080078" Y="27.06698046875" />
                  <Point X="2.230559570312" Y="27.024705078125" />
                  <Point X="2.241196289062" Y="27.018296875" />
                  <Point X="2.261222900391" Y="27.007935546875" />
                  <Point X="2.271541992188" Y="27.0033515625" />
                  <Point X="2.293667724609" Y="26.995060546875" />
                  <Point X="2.304490966797" Y="26.991724609375" />
                  <Point X="2.326455322266" Y="26.9863671875" />
                  <Point X="2.337596435547" Y="26.984345703125" />
                  <Point X="2.405962646484" Y="26.976103515625" />
                  <Point X="2.414505126953" Y="26.975462890625" />
                  <Point X="2.444826416016" Y="26.975478515625" />
                  <Point X="2.483585205078" Y="26.97973828125" />
                  <Point X="2.497749023438" Y="26.98239453125" />
                  <Point X="2.576811035156" Y="27.003537109375" />
                  <Point X="2.582253417969" Y="27.00516796875" />
                  <Point X="2.60140234375" Y="27.011984375" />
                  <Point X="2.626883300781" Y="27.023228515625" />
                  <Point X="2.636026611328" Y="27.0278671875" />
                  <Point X="2.658782470703" Y="27.04100390625" />
                  <Point X="3.940405517578" Y="27.780951171875" />
                  <Point X="4.043950683594" Y="27.637046875" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.1521171875" Y="26.72521484375" />
                  <Point X="3.134667236328" Y="26.7066015625" />
                  <Point X="3.128575683594" Y="26.699421875" />
                  <Point X="3.071687988281" Y="26.62520703125" />
                  <Point X="3.066875488281" Y="26.61831640625" />
                  <Point X="3.053694824219" Y="26.59685546875" />
                  <Point X="3.044331054688" Y="26.57873828125" />
                  <Point X="3.040322265625" Y="26.56990234375" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.008944335938" Y="26.466880859375" />
                  <Point X="3.006222900391" Y="26.45465234375" />
                  <Point X="3.00276953125" Y="26.432349609375" />
                  <Point X="3.001707763672" Y="26.421095703125" />
                  <Point X="3.000892822266" Y="26.397533203125" />
                  <Point X="3.001174560547" Y="26.386236328125" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.004698974609" Y="26.352572265625" />
                  <Point X="3.022098388672" Y="26.26824609375" />
                  <Point X="3.024155029297" Y="26.260111328125" />
                  <Point X="3.031747802734" Y="26.236125" />
                  <Point X="3.039215820312" Y="26.217439453125" />
                  <Point X="3.043214111328" Y="26.208736328125" />
                  <Point X="3.056918457031" Y="26.183525390625" />
                  <Point X="3.104243164062" Y="26.11159375" />
                  <Point X="3.111728271484" Y="26.101439453125" />
                  <Point X="3.126309082031" Y="26.08416015625" />
                  <Point X="3.134115478516" Y="26.075955078125" />
                  <Point X="3.151350830078" Y="26.05987890625" />
                  <Point X="3.160051513672" Y="26.052681640625" />
                  <Point X="3.178248291016" Y="26.0393671875" />
                  <Point X="3.187744384766" Y="26.03325" />
                  <Point X="3.256324462891" Y="25.99464453125" />
                  <Point X="3.263922119141" Y="25.990806640625" />
                  <Point X="3.287330322266" Y="25.98068359375" />
                  <Point X="3.306368652344" Y="25.974087890625" />
                  <Point X="3.315580810547" Y="25.971408203125" />
                  <Point X="3.343673339844" Y="25.965255859375" />
                  <Point X="3.436398193359" Y="25.953001953125" />
                  <Point X="3.441975341797" Y="25.952431640625" />
                  <Point X="3.462085693359" Y="25.95139453125" />
                  <Point X="3.490263427734" Y="25.952005859375" />
                  <Point X="3.500602050781" Y="25.952796875" />
                  <Point X="3.52221875" Y="25.955642578125" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.914234375" />
                  <Point X="4.783870605469" Y="25.713921875" />
                  <Point X="3.6919921875" Y="25.421353515625" />
                  <Point X="3.686020996094" Y="25.419541015625" />
                  <Point X="3.665622802734" Y="25.412134765625" />
                  <Point X="3.642380615234" Y="25.4016171875" />
                  <Point X="3.634006103516" Y="25.397314453125" />
                  <Point X="3.542906738281" Y="25.344658203125" />
                  <Point X="3.535883789062" Y="25.340177734375" />
                  <Point X="3.515637207031" Y="25.325525390625" />
                  <Point X="3.499916015625" Y="25.31229296875" />
                  <Point X="3.492776611328" Y="25.305626953125" />
                  <Point X="3.472797851562" Y="25.28423046875" />
                  <Point X="3.418138183594" Y="25.214580078125" />
                  <Point X="3.41085546875" Y="25.2042109375" />
                  <Point X="3.399132080078" Y="25.184931640625" />
                  <Point X="3.393845214844" Y="25.174943359375" />
                  <Point X="3.384069824219" Y="25.1534765625" />
                  <Point X="3.380005859375" Y="25.1429296875" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.352156494141" Y="25.0153359375" />
                  <Point X="3.350950439453" Y="25.00709765625" />
                  <Point X="3.348783935547" Y="24.9822421875" />
                  <Point X="3.348501708984" Y="24.961958984375" />
                  <Point X="3.348856933594" Y="24.952322265625" />
                  <Point X="3.351874267578" Y="24.923576171875" />
                  <Point X="3.370094238281" Y="24.828439453125" />
                  <Point X="3.373160888672" Y="24.81600390625" />
                  <Point X="3.380010009766" Y="24.79449609375" />
                  <Point X="3.384075683594" Y="24.7839453125" />
                  <Point X="3.393850097656" Y="24.762484375" />
                  <Point X="3.399136474609" Y="24.7525" />
                  <Point X="3.410857421875" Y="24.7332265625" />
                  <Point X="3.417291992188" Y="24.7239375" />
                  <Point X="3.471951660156" Y="24.654287109375" />
                  <Point X="3.477459960938" Y="24.64787890625" />
                  <Point X="3.495068847656" Y="24.629689453125" />
                  <Point X="3.510225585938" Y="24.616193359375" />
                  <Point X="3.517757568359" Y="24.61014453125" />
                  <Point X="3.541495605469" Y="24.593595703125" />
                  <Point X="3.632571044922" Y="24.540953125" />
                  <Point X="3.655419677734" Y="24.529748046875" />
                  <Point X="3.682023193359" Y="24.519357421875" />
                  <Point X="3.692002197266" Y="24.516083984375" />
                  <Point X="3.711825683594" Y="24.5107734375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761612304688" Y="24.06894140625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096191406" Y="24.09195703125" />
                  <Point X="3.366722167969" Y="24.089158203125" />
                  <Point X="3.354483886719" Y="24.087322265625" />
                  <Point X="3.175687988281" Y="24.0484609375" />
                  <Point X="3.157878173828" Y="24.0432578125" />
                  <Point X="3.128760009766" Y="24.031634765625" />
                  <Point X="3.114683349609" Y="24.02461328125" />
                  <Point X="3.086854492188" Y="24.007720703125" />
                  <Point X="3.074127197266" Y="23.998470703125" />
                  <Point X="3.050374511719" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.931299072266" Y="23.83682421875" />
                  <Point X="2.921356201172" Y="23.82319140625" />
                  <Point X="2.906621582031" Y="23.798794921875" />
                  <Point X="2.900174560547" Y="23.786041015625" />
                  <Point X="2.888824707031" Y="23.75864453125" />
                  <Point X="2.884365478516" Y="23.745076171875" />
                  <Point X="2.877531494141" Y="23.717421875" />
                  <Point X="2.875156738281" Y="23.7033359375" />
                  <Point X="2.859667480469" Y="23.53501171875" />
                  <Point X="2.859288330078" Y="23.516671875" />
                  <Point X="2.861607910156" Y="23.485400390625" />
                  <Point X="2.864066894531" Y="23.469861328125" />
                  <Point X="2.871799316406" Y="23.4382421875" />
                  <Point X="2.876788085938" Y="23.42332421875" />
                  <Point X="2.889158935547" Y="23.394515625" />
                  <Point X="2.896541015625" Y="23.380625" />
                  <Point X="2.995489257812" Y="23.22671875" />
                  <Point X="2.9995078125" Y="23.22094921875" />
                  <Point X="3.015462158203" Y="23.200927734375" />
                  <Point X="3.042230224609" Y="23.17316015625" />
                  <Point X="3.052789306641" Y="23.163728515625" />
                  <Point X="3.071185546875" Y="23.149611328125" />
                  <Point X="4.087171875" Y="22.37001953125" />
                  <Point X="4.045494384766" Y="22.302578125" />
                  <Point X="4.001272460938" Y="22.23974609375" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841192138672" Y="22.90911328125" />
                  <Point X="2.815027099609" Y="22.920484375" />
                  <Point X="2.783122802734" Y="22.930671875" />
                  <Point X="2.771109619141" Y="22.933662109375" />
                  <Point X="2.558314208984" Y="22.97209375" />
                  <Point X="2.539292480469" Y="22.97419921875" />
                  <Point X="2.507946777344" Y="22.974595703125" />
                  <Point X="2.492259277344" Y="22.9734921875" />
                  <Point X="2.460105712891" Y="22.96853125" />
                  <Point X="2.4448203125" Y="22.964857421875" />
                  <Point X="2.4150625" Y="22.955037109375" />
                  <Point X="2.400590087891" Y="22.948890625" />
                  <Point X="2.223809326172" Y="22.855853515625" />
                  <Point X="2.208907470703" Y="22.84679296875" />
                  <Point X="2.186007324219" Y="22.8299140625" />
                  <Point X="2.175194824219" Y="22.82061328125" />
                  <Point X="2.154247070312" Y="22.7996640625" />
                  <Point X="2.144938720703" Y="22.788837890625" />
                  <Point X="2.128048095703" Y="22.76591015625" />
                  <Point X="2.120465820312" Y="22.75380859375" />
                  <Point X="2.027427368164" Y="22.57702734375" />
                  <Point X="2.019839233398" Y="22.559814453125" />
                  <Point X="2.010016357422" Y="22.530044921875" />
                  <Point X="2.006341308594" Y="22.514751953125" />
                  <Point X="2.001381103516" Y="22.482576171875" />
                  <Point X="2.000279785156" Y="22.46687890625" />
                  <Point X="2.000683227539" Y="22.435521484375" />
                  <Point X="2.002187744141" Y="22.419861328125" />
                  <Point X="2.040618286133" Y="22.207064453125" />
                  <Point X="2.042078979492" Y="22.2003671875" />
                  <Point X="2.04742175293" Y="22.180513671875" />
                  <Point X="2.053919677734" Y="22.161103515625" />
                  <Point X="2.057391357422" Y="22.152236328125" />
                  <Point X="2.069548583984" Y="22.126416015625" />
                  <Point X="2.081370117187" Y="22.10594140625" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723752929688" Y="20.96391796875" />
                  <Point X="1.833914428711" Y="22.123576171875" />
                  <Point X="1.828655151367" Y="22.1298515625" />
                  <Point X="1.808838256836" Y="22.1503671875" />
                  <Point X="1.783255371094" Y="22.171994140625" />
                  <Point X="1.773299072266" Y="22.179353515625" />
                  <Point X="1.563425292969" Y="22.314283203125" />
                  <Point X="1.546225097656" Y="22.323787109375" />
                  <Point X="1.517421020508" Y="22.336146484375" />
                  <Point X="1.502508056641" Y="22.34112890625" />
                  <Point X="1.470899536133" Y="22.3488515625" />
                  <Point X="1.455368530273" Y="22.351306640625" />
                  <Point X="1.424116699219" Y="22.35362109375" />
                  <Point X="1.408395629883" Y="22.35348046875" />
                  <Point X="1.17889440918" Y="22.33236328125" />
                  <Point X="1.161278808594" Y="22.32966796875" />
                  <Point X="1.133613037109" Y="22.322837890625" />
                  <Point X="1.120035400391" Y="22.31837890625" />
                  <Point X="1.092637817383" Y="22.307033203125" />
                  <Point X="1.07989074707" Y="22.300591796875" />
                  <Point X="1.055500976563" Y="22.285869140625" />
                  <Point X="1.043858398438" Y="22.277587890625" />
                  <Point X="0.866619140625" Y="22.13021875" />
                  <Point X="0.85263848877" Y="22.1168984375" />
                  <Point X="0.832163330078" Y="22.093140625" />
                  <Point X="0.822913635254" Y="22.080412109375" />
                  <Point X="0.806033203125" Y="22.052599609375" />
                  <Point X="0.79901373291" Y="22.038525390625" />
                  <Point X="0.787393615723" Y="22.009412109375" />
                  <Point X="0.782792724609" Y="21.994373046875" />
                  <Point X="0.729799133301" Y="21.750560546875" />
                  <Point X="0.728609863281" Y="21.74398046875" />
                  <Point X="0.725973022461" Y="21.724103515625" />
                  <Point X="0.724438476562" Y="21.7031796875" />
                  <Point X="0.724223571777" Y="21.693490234375" />
                  <Point X="0.725554931641" Y="21.664484375" />
                  <Point X="0.728904907227" Y="21.639037109375" />
                  <Point X="0.83309185791" Y="20.84766015625" />
                  <Point X="0.655065002441" Y="21.512064453125" />
                  <Point X="0.652606506348" Y="21.519875" />
                  <Point X="0.642148376465" Y="21.5464140625" />
                  <Point X="0.626791137695" Y="21.5761796875" />
                  <Point X="0.620409851074" Y="21.5867890625" />
                  <Point X="0.459178710938" Y="21.819091796875" />
                  <Point X="0.446673583984" Y="21.83482421875" />
                  <Point X="0.424794219971" Y="21.857275390625" />
                  <Point X="0.412922943115" Y="21.867591796875" />
                  <Point X="0.386667541504" Y="21.886837890625" />
                  <Point X="0.373251037598" Y="21.895056640625" />
                  <Point X="0.345245574951" Y="21.90916796875" />
                  <Point X="0.330656616211" Y="21.915060546875" />
                  <Point X="0.081160690308" Y="21.992494140625" />
                  <Point X="0.063379592896" Y="21.996888671875" />
                  <Point X="0.035220607758" Y="22.001158203125" />
                  <Point X="0.020979265213" Y="22.002232421875" />
                  <Point X="-0.008665058136" Y="22.002232421875" />
                  <Point X="-0.022905954361" Y="22.001158203125" />
                  <Point X="-0.051065830231" Y="21.996888671875" />
                  <Point X="-0.064984962463" Y="21.993693359375" />
                  <Point X="-0.314380828857" Y="21.9162890625" />
                  <Point X="-0.332778289795" Y="21.909228515625" />
                  <Point X="-0.360845733643" Y="21.895107421875" />
                  <Point X="-0.374294158936" Y="21.886876953125" />
                  <Point X="-0.400569061279" Y="21.867626953125" />
                  <Point X="-0.412455474854" Y="21.85730078125" />
                  <Point X="-0.434355072021" Y="21.83483203125" />
                  <Point X="-0.444368225098" Y="21.822689453125" />
                  <Point X="-0.60559954834" Y="21.590384765625" />
                  <Point X="-0.618972473145" Y="21.567974609375" />
                  <Point X="-0.628689147949" Y="21.548810546875" />
                  <Point X="-0.632682800293" Y="21.5398046875" />
                  <Point X="-0.642753479004" Y="21.512060546875" />
                  <Point X="-0.648861755371" Y="21.489263671875" />
                  <Point X="-0.985424499512" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.66311396324" Y="21.042944078312" />
                  <Point X="2.689539623397" Y="21.05256223203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.602474996792" Y="21.121970187873" />
                  <Point X="2.64130680976" Y="21.136103811936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541836030344" Y="21.200996297435" />
                  <Point X="2.593073996122" Y="21.219645391842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.481197063896" Y="21.280022406996" />
                  <Point X="2.544841182485" Y="21.303186971749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420558097448" Y="21.359048516558" />
                  <Point X="2.496608368848" Y="21.386728551655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.824621482506" Y="20.879271981328" />
                  <Point X="0.828733057141" Y="20.880768472111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.359919131001" Y="21.438074626119" />
                  <Point X="2.44837555521" Y="21.470270131561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135097043416" Y="20.267089658738" />
                  <Point X="-0.959186241151" Y="20.331115954648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799939718856" Y="20.971385442416" />
                  <Point X="0.816031980122" Y="20.977242546519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299280164553" Y="21.517100735681" />
                  <Point X="2.400142741573" Y="21.553811711468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127794851808" Y="20.370844327513" />
                  <Point X="-0.929170096224" Y="20.443137826334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775257955206" Y="21.063498903503" />
                  <Point X="0.803330903104" Y="21.073716620926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.238641198105" Y="21.596126845242" />
                  <Point X="2.351909927936" Y="21.637353291374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120586902951" Y="20.474564694733" />
                  <Point X="-0.899153951297" Y="20.55515969802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750576191556" Y="21.15561236459" />
                  <Point X="0.790629826085" Y="21.170190695333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.178002231657" Y="21.675152954804" />
                  <Point X="2.303677114299" Y="21.72089487128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.896076707295" Y="22.300480924189" />
                  <Point X="4.087159194054" Y="22.370029261658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136754793525" Y="20.569776952198" />
                  <Point X="-0.86913780637" Y="20.667181569706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725894427905" Y="21.247725825678" />
                  <Point X="0.777928749066" Y="21.26666476974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.117363265209" Y="21.754179064365" />
                  <Point X="2.255444300661" Y="21.804436451187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.788677735796" Y="22.362487783757" />
                  <Point X="3.997795359032" Y="22.438600374076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.155506597979" Y="20.664048741923" />
                  <Point X="-0.839121661443" Y="20.779203441392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.701212664255" Y="21.339839286765" />
                  <Point X="0.765227672047" Y="21.363138844148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.056724298761" Y="21.833205173927" />
                  <Point X="2.207211487024" Y="21.887978031093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.681278764297" Y="22.424494643326" />
                  <Point X="3.90843152401" Y="22.507171486493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.174258402433" Y="20.758320531648" />
                  <Point X="-0.809105516516" Y="20.891225313078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.676530900605" Y="21.431952747852" />
                  <Point X="0.752526595029" Y="21.459612918555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.996085332314" Y="21.912231283488" />
                  <Point X="2.158978673387" Y="21.971519610999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573879792797" Y="22.486501502895" />
                  <Point X="3.819067688988" Y="22.57574259891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.19899639551" Y="20.850413526898" />
                  <Point X="-0.779089371589" Y="21.003247184764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.651067066282" Y="21.523781558494" />
                  <Point X="0.73982551801" Y="21.556086992962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.935446365866" Y="21.99125739305" />
                  <Point X="2.110745859749" Y="22.055061190906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.466480821298" Y="22.548508362463" />
                  <Point X="3.729703853966" Y="22.644313711328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.252934619023" Y="20.931878507435" />
                  <Point X="-0.749073226663" Y="21.11526905645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.605487737411" Y="21.608288927872" />
                  <Point X="0.727124556046" Y="21.652561109246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.874807399418" Y="22.070283502611" />
                  <Point X="2.063620747947" Y="22.139005941308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.359081849798" Y="22.610515222032" />
                  <Point X="3.640340018944" Y="22.712884823745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.333386106393" Y="21.003693449116" />
                  <Point X="-0.719057081736" Y="21.227290928136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.549471425131" Y="21.688997545954" />
                  <Point X="0.73075997549" Y="21.754981182098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.810980579193" Y="22.148149328287" />
                  <Point X="2.036438395922" Y="22.230209262659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251682878299" Y="22.6725220816" />
                  <Point X="3.550976183922" Y="22.781455936162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.414853563387" Y="21.075138608094" />
                  <Point X="-0.689040936809" Y="21.339312799823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.493455112851" Y="21.769706164036" />
                  <Point X="0.754621466461" Y="21.864762942942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.717505123392" Y="22.215223933126" />
                  <Point X="2.019306673521" Y="22.325070714029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.1442839068" Y="22.734528941169" />
                  <Point X="3.4616123489" Y="22.85002704858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496840640567" Y="21.146394640791" />
                  <Point X="-0.659024791882" Y="21.451334671509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.433040259694" Y="21.848813844165" />
                  <Point X="0.778482957432" Y="21.974544703786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.617098926885" Y="22.279775954647" />
                  <Point X="2.002180736304" Y="22.419934271033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0368849353" Y="22.796535800738" />
                  <Point X="3.372248513878" Y="22.918598160997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.547635398757" Y="20.865033514872" />
                  <Point X="-2.456691585978" Y="20.898134355714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.622593399736" Y="21.201721267961" />
                  <Point X="-0.619572602558" Y="21.566790982484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.333800887035" Y="21.913790554835" />
                  <Point X="0.834723782176" Y="22.096111578329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.505321138888" Y="22.340189055349" />
                  <Point X="2.008393681685" Y="22.523292486604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.929485963801" Y="22.858542660306" />
                  <Point X="3.282884678857" Y="22.987169273415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.650466004261" Y="20.928703123682" />
                  <Point X="-2.271213152938" Y="21.066739872824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.855228463955" Y="21.218145917524" />
                  <Point X="-0.52890437483" Y="21.700888406956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.184315182973" Y="21.960479096493" />
                  <Point X="1.034744708588" Y="22.270010130158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.215249262353" Y="22.335708414878" />
                  <Point X="2.062769531659" Y="22.644180565842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.81816329684" Y="22.919121411519" />
                  <Point X="3.193520843835" Y="23.055740385832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752536547799" Y="20.992649372424" />
                  <Point X="-0.431988982703" Y="21.837259613318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.021220160271" Y="22.002214251258" />
                  <Point X="2.131195452263" Y="22.77018245258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643615358505" Y="22.956688045897" />
                  <Point X="3.104157008813" Y="23.124311498249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.841692021181" Y="21.061296322276" />
                  <Point X="3.021029544171" Y="23.195152463855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.930847494562" Y="21.129943272129" />
                  <Point X="2.964107081531" Y="23.275531270178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948420415163" Y="21.224644140486" />
                  <Point X="2.911435773032" Y="23.35745737007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.530515837058" Y="23.946754320269" />
                  <Point X="4.752136617023" Y="24.027417687471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.874523557588" Y="21.352637285434" />
                  <Point X="2.870477644897" Y="23.443646718963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.326535942361" Y="23.973608598596" />
                  <Point X="4.771665757768" Y="24.135622601788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.800626700013" Y="21.480630430383" />
                  <Point X="2.860219437685" Y="23.541009925265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122556047665" Y="24.000462876923" />
                  <Point X="4.755949425486" Y="24.230999213031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.726729842439" Y="21.608623575331" />
                  <Point X="2.869844790141" Y="23.645610155439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.918576152969" Y="24.02731715525" />
                  <Point X="4.595966110119" Y="24.273866936644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.652832984864" Y="21.736616720279" />
                  <Point X="2.886947230917" Y="23.7529318232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714596258272" Y="24.054171433577" />
                  <Point X="4.435982794753" Y="24.316734660256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.578936127289" Y="21.864609865227" />
                  <Point X="2.971058567049" Y="23.884642734302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.510616363576" Y="24.081025711904" />
                  <Point X="4.275999479386" Y="24.359602383869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.505039269715" Y="21.992603010175" />
                  <Point X="4.116016164019" Y="24.402470107481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.43114241214" Y="22.120596155124" />
                  <Point X="3.956032848653" Y="24.445337831094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.357245554566" Y="22.248589300072" />
                  <Point X="3.796049533286" Y="24.488205554706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313377561941" Y="22.365652832009" />
                  <Point X="3.645623844671" Y="24.534551969966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.321194786268" Y="22.463904483425" />
                  <Point X="3.537581252227" Y="24.596324570669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.821096085966" Y="22.019081944383" />
                  <Point X="-3.572882813898" Y="22.109424187165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.365971128027" Y="22.548704116211" />
                  <Point X="3.460202894685" Y="24.669258040133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.881352480373" Y="22.09824729878" />
                  <Point X="-3.099099807256" Y="22.382963987469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.448674342678" Y="22.619699496185" />
                  <Point X="3.40131312575" Y="24.748920805523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.94160887478" Y="22.177412653177" />
                  <Point X="3.368265317893" Y="24.83798927554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.001865269187" Y="22.256578007574" />
                  <Point X="3.350909331635" Y="24.932769101541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051783782221" Y="22.339506043075" />
                  <Point X="3.356064599387" Y="25.035742353938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.101321202947" Y="22.422572784834" />
                  <Point X="3.381181641279" Y="25.145981097945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150858623673" Y="22.505639526593" />
                  <Point X="3.468615746869" Y="25.278901398224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.32883162619" Y="25.59199437334" />
                  <Point X="4.777432661268" Y="25.75527179717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084464709927" Y="22.630901823318" />
                  <Point X="4.762536590785" Y="25.850946959291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.833825774877" Y="22.823223823609" />
                  <Point X="4.745029883411" Y="25.945671927292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.583186839828" Y="23.015545823901" />
                  <Point X="4.722421849748" Y="26.038540164369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.332547904779" Y="23.207867824192" />
                  <Point X="4.610574164968" Y="26.098927824723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.08190896973" Y="23.400189824483" />
                  <Point X="4.175406773421" Y="26.041636735662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966447879771" Y="23.543311112829" />
                  <Point X="3.740239381874" Y="25.9843456466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948848843168" Y="23.65081352669" />
                  <Point X="3.392353864424" Y="25.958822561701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968829897948" Y="23.744637905886" />
                  <Point X="3.239316867966" Y="26.004218538634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.028129167363" Y="23.82415162529" />
                  <Point X="3.141001980403" Y="26.069531734362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.134318607237" Y="23.886598718368" />
                  <Point X="3.080008843856" Y="26.148428936549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45030603534" Y="23.872685588521" />
                  <Point X="3.033200939669" Y="26.232489141082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.885470244853" Y="23.815395657626" />
                  <Point X="3.010333808004" Y="26.325263074198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.320634454366" Y="23.75810572673" />
                  <Point X="3.001915343995" Y="26.423295892266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.666071690993" Y="23.733473743176" />
                  <Point X="3.027647408353" Y="26.533758486144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.689698317386" Y="23.825971242818" />
                  <Point X="3.098986941821" Y="26.660820841237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.713324943779" Y="23.91846874246" />
                  <Point X="3.284561409495" Y="26.829461312096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736951570171" Y="24.010966242103" />
                  <Point X="3.53520126216" Y="27.021783646372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752289264224" Y="24.10648066639" />
                  <Point X="3.785841114824" Y="27.214105980648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766032987354" Y="24.202575248648" />
                  <Point X="4.036480967489" Y="27.406428314924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779776710485" Y="24.298669830906" />
                  <Point X="-4.275070867808" Y="24.482367734701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333512395007" Y="24.825066992621" />
                  <Point X="4.10683341554" Y="27.533131400308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296737661114" Y="24.939548789516" />
                  <Point X="2.326718481126" Y="26.986319438993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.968916884541" Y="27.220060542329" />
                  <Point X="4.056698539586" Y="27.615980686147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303778898925" Y="25.038082876926" />
                  <Point X="2.203977589704" Y="27.042742296374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442701617277" Y="27.49360097088" />
                  <Point X="4.000962149546" Y="27.696791187592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.34151755475" Y="25.12544401791" />
                  <Point X="2.119248910386" Y="27.113000467498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.916486350013" Y="27.767141399431" />
                  <Point X="3.943315810679" Y="27.776906524515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.412849127797" Y="25.200578336943" />
                  <Point X="2.062038899507" Y="27.193274614822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.535709480801" Y="25.256957713863" />
                  <Point X="2.025723025887" Y="27.281153606178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.695693595041" Y="25.299825146709" />
                  <Point X="2.013122957266" Y="27.377664444635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.85567770928" Y="25.342692579556" />
                  <Point X="2.026021539591" Y="27.483456033051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.01566182352" Y="25.385560012402" />
                  <Point X="2.065494681552" Y="27.598919970163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17564593776" Y="25.428427445249" />
                  <Point X="2.139391451713" Y="27.726913083295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.335630052" Y="25.471294878095" />
                  <Point X="2.213288221874" Y="27.854906196427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.495614166239" Y="25.514162310942" />
                  <Point X="2.287184992035" Y="27.982899309559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.655598280479" Y="25.557029743788" />
                  <Point X="2.361081762195" Y="28.110892422692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782467916617" Y="25.611949860987" />
                  <Point X="2.434978532356" Y="28.238885535824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76665657447" Y="25.718801607278" />
                  <Point X="2.508875302517" Y="28.366878648956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750845232323" Y="25.825653353568" />
                  <Point X="-3.694562452415" Y="26.210108844423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.561989472443" Y="26.258361463001" />
                  <Point X="2.582772072678" Y="28.494871762088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735033890176" Y="25.932505099859" />
                  <Point X="-3.92980958849" Y="26.22558277758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45177920169" Y="26.39957160945" />
                  <Point X="2.656668842838" Y="28.62286487522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708056546822" Y="26.043420938224" />
                  <Point X="-4.133789361587" Y="26.252437100166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421724001717" Y="26.511607696011" />
                  <Point X="2.730565612999" Y="28.750857988352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.677663414328" Y="26.155580022164" />
                  <Point X="-4.337769134685" Y="26.279291422751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437023886236" Y="26.607135881843" />
                  <Point X="2.80446238316" Y="28.878851101484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.647270281833" Y="26.267739106103" />
                  <Point X="-4.541748907782" Y="26.306145745337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4790758947" Y="26.692927090856" />
                  <Point X="2.740692268601" Y="28.956737566334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.544962773954" Y="26.770043116364" />
                  <Point X="2.646657621746" Y="29.023608642274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.633954872796" Y="26.838749529686" />
                  <Point X="2.542191658713" Y="29.086683029622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723318543868" Y="26.907320701776" />
                  <Point X="2.435388807415" Y="29.1489068592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81268221494" Y="26.975891873867" />
                  <Point X="2.328585956117" Y="29.211130688778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902045886012" Y="27.044463045957" />
                  <Point X="2.217822289246" Y="29.271912899384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.991409557084" Y="27.113034218047" />
                  <Point X="2.096258269999" Y="29.328764103206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.080773228156" Y="27.181605390137" />
                  <Point X="1.974694250753" Y="29.385615307027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.170136899228" Y="27.250176562228" />
                  <Point X="1.852392000358" Y="29.442197816685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.200881020696" Y="27.34008350552" />
                  <Point X="-3.131971658898" Y="27.729134696343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834577823054" Y="27.837377200444" />
                  <Point X="1.713270082452" Y="29.492658468018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125954180244" Y="27.468451533577" />
                  <Point X="-3.278972522811" Y="27.776727645852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755443905812" Y="27.967276479226" />
                  <Point X="1.574148164546" Y="29.543119119352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051027339792" Y="27.596819561635" />
                  <Point X="-3.386371547919" Y="27.838734485909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751035290882" Y="28.06997797222" />
                  <Point X="0.051791385901" Y="29.090123454377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.12138079491" Y="29.115451927876" />
                  <Point X="1.43130056125" Y="29.592223732101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963935918196" Y="27.729615135141" />
                  <Point X="-3.493770573027" Y="27.900741325966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763345732831" Y="28.166594226165" />
                  <Point X="-0.126456841334" Y="29.126343293738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.236319728148" Y="29.258383166718" />
                  <Point X="1.264309318281" Y="29.632540778662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.854213039603" Y="27.870647885352" />
                  <Point X="-3.601169598135" Y="27.962748166023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801768503333" Y="28.25370636977" />
                  <Point X="-0.199149385931" Y="29.200982259636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.267181045623" Y="29.370712656055" />
                  <Point X="1.097318075312" Y="29.672857825224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74449016101" Y="28.011680635563" />
                  <Point X="-3.708568623242" Y="28.024755006079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850001280379" Y="28.337247962995" />
                  <Point X="-0.233161629571" Y="29.289699703736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.297197091239" Y="29.482734491594" />
                  <Point X="0.930326832343" Y="29.713174871785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.898234057425" Y="28.420789556219" />
                  <Point X="-0.257843419455" Y="29.381813155275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.327213136854" Y="29.594756327134" />
                  <Point X="0.744518701864" Y="29.746643131391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.94646683447" Y="28.504331149444" />
                  <Point X="-0.282525209338" Y="29.473926606815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.357229182469" Y="29.706778162674" />
                  <Point X="0.528821368247" Y="29.769232610729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.994699611516" Y="28.587872742668" />
                  <Point X="-0.307206999222" Y="29.566040058354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.042932388562" Y="28.671414335893" />
                  <Point X="-1.961759943878" Y="29.064928923867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.507551750316" Y="29.230247186483" />
                  <Point X="-0.331888789106" Y="29.658153509893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.893991192438" Y="28.826721386323" />
                  <Point X="-2.070941935542" Y="29.126286817168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471420289207" Y="29.344494851232" />
                  <Point X="-0.35657057899" Y="29.750266961432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.636987202579" Y="29.021360077105" />
                  <Point X="-2.141440742475" Y="29.201724238278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464200805219" Y="29.448219416896" />
                  <Point X="-0.631114627465" Y="29.751437988177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.47690177989" Y="29.544693528555" />
                  <Point X="-1.254126333147" Y="29.625777160095" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001626953125" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464321105957" Y="21.478453125" />
                  <Point X="0.303089996338" Y="21.710755859375" />
                  <Point X="0.300593475342" Y="21.714353515625" />
                  <Point X="0.274338195801" Y="21.733599609375" />
                  <Point X="0.024842357635" Y="21.811033203125" />
                  <Point X="0.020979211807" Y="21.812232421875" />
                  <Point X="-0.008665060997" Y="21.812232421875" />
                  <Point X="-0.258140625" Y="21.734802734375" />
                  <Point X="-0.262004272461" Y="21.733607421875" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.449510406494" Y="21.482052734375" />
                  <Point X="-0.449509796143" Y="21.482052734375" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.465335235596" Y="21.440091796875" />
                  <Point X="-0.847744140625" Y="20.012921875" />
                  <Point X="-1.0971484375" Y="20.06133203125" />
                  <Point X="-1.100237426758" Y="20.061931640625" />
                  <Point X="-1.351589599609" Y="20.1266015625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309682861328" Y="20.46523828125" />
                  <Point X="-1.369287353516" Y="20.764890625" />
                  <Point X="-1.370210327148" Y="20.76953125" />
                  <Point X="-1.386283081055" Y="20.79737109375" />
                  <Point X="-1.615987182617" Y="20.99881640625" />
                  <Point X="-1.619543945312" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.954109375" Y="21.03421875" />
                  <Point X="-1.958838256836" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.243911132812" Y="20.856470703125" />
                  <Point X="-2.247849121094" Y="20.853837890625" />
                  <Point X="-2.263163330078" Y="20.838236328125" />
                  <Point X="-2.457095214844" Y="20.585498046875" />
                  <Point X="-2.851297119141" Y="20.829578125" />
                  <Point X="-2.855825683594" Y="20.8323828125" />
                  <Point X="-3.228580322266" Y="21.119392578125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979736328" Y="22.431236328125" />
                  <Point X="-2.531063720703" Y="22.4483203125" />
                  <Point X="-2.531393554688" Y="22.4486484375" />
                  <Point X="-2.560203125" Y="22.462806640625" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.611344238281" Y="22.445177734375" />
                  <Point X="-3.842959472656" Y="21.734103515625" />
                  <Point X="-4.158120605469" Y="22.148162109375" />
                  <Point X="-4.161700195312" Y="22.152865234375" />
                  <Point X="-4.431019042969" Y="22.60447265625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.138236572266" Y="23.6332734375" />
                  <Point X="-3.138119140625" Y="23.6337265625" />
                  <Point X="-3.140324707031" Y="23.665400390625" />
                  <Point X="-3.161158203125" Y="23.689359375" />
                  <Point X="-3.187237060547" Y="23.704708984375" />
                  <Point X="-3.187747802734" Y="23.7050078125" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.244349121094" Y="23.70816015625" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.925991699219" Y="23.983322265625" />
                  <Point X="-4.927392089844" Y="23.988802734375" />
                  <Point X="-4.998395996094" Y="24.4852578125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541895507813" Y="24.87857421875" />
                  <Point X="-3.514565673828" Y="24.89754296875" />
                  <Point X="-3.514187744141" Y="24.8978046875" />
                  <Point X="-3.494898925781" Y="24.924091796875" />
                  <Point X="-3.485796142578" Y="24.953421875" />
                  <Point X="-3.485655029297" Y="24.953875" />
                  <Point X="-3.485647705078" Y="24.9835390625" />
                  <Point X="-3.494757568359" Y="25.012892578125" />
                  <Point X="-3.494895263672" Y="25.0133359375" />
                  <Point X="-3.51414453125" Y="25.039603515625" />
                  <Point X="-3.541389648438" Y="25.05851171875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.580088378906" Y="25.0721484375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.918547363281" Y="25.9903203125" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731704589844" Y="26.3958671875" />
                  <Point X="-3.671231933594" Y="26.41493359375" />
                  <Point X="-3.670295410156" Y="26.415228515625" />
                  <Point X="-3.651541015625" Y="26.42605078125" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.614848388672" Y="26.502380859375" />
                  <Point X="-3.614464355469" Y="26.50330859375" />
                  <Point X="-3.610714355469" Y="26.52461328125" />
                  <Point X="-3.616313964844" Y="26.5455078125" />
                  <Point X="-3.645600341797" Y="26.601765625" />
                  <Point X="-3.646053710938" Y="26.60263671875" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.672945800781" Y="26.6291796875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.163522460938" Y="27.78099609375" />
                  <Point X="-4.160014160156" Y="27.787005859375" />
                  <Point X="-3.774670166016" Y="28.282310546875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515136719" Y="27.92043359375" />
                  <Point X="-3.054270263672" Y="27.9130625" />
                  <Point X="-3.05293359375" Y="27.912947265625" />
                  <Point X="-3.031491699219" Y="27.91578125" />
                  <Point X="-3.013252441406" Y="27.927404296875" />
                  <Point X="-2.953454833984" Y="27.987201171875" />
                  <Point X="-2.952533447266" Y="27.988123046875" />
                  <Point X="-2.940900390625" Y="28.00637890625" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.945444824219" Y="28.1120859375" />
                  <Point X="-2.94555859375" Y="28.11338671875" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-2.957818115234" Y="28.1439921875" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.758983642578" Y="29.1696484375" />
                  <Point X="-2.752871582031" Y="29.174333984375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951249267578" Y="29.273662109375" />
                  <Point X="-1.857484985352" Y="29.224849609375" />
                  <Point X="-1.856033203125" Y="29.22409375" />
                  <Point X="-1.835125488281" Y="29.218490234375" />
                  <Point X="-1.813808837891" Y="29.22225" />
                  <Point X="-1.716147094727" Y="29.262703125" />
                  <Point X="-1.714619995117" Y="29.2633359375" />
                  <Point X="-1.696899780273" Y="29.27575" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.654298828125" Y="29.395296875" />
                  <Point X="-1.653806396484" Y="29.396857421875" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.652571411133" Y="29.423390625" />
                  <Point X="-1.689137817383" Y="29.701140625" />
                  <Point X="-0.975997680664" Y="29.901080078125" />
                  <Point X="-0.968094665527" Y="29.903296875" />
                  <Point X="-0.224200042725" Y="29.990357421875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.035305446625" Y="29.295890625" />
                  <Point X="-0.010113825798" Y="29.276560546875" />
                  <Point X="0.022425748825" Y="29.276560546875" />
                  <Point X="0.047617370605" Y="29.295890625" />
                  <Point X="0.057398895264" Y="29.3218984375" />
                  <Point X="0.2366484375" Y="29.990869140625" />
                  <Point X="0.853303161621" Y="29.926287109375" />
                  <Point X="0.860210327148" Y="29.925564453125" />
                  <Point X="1.501184448242" Y="29.7708125" />
                  <Point X="1.508452514648" Y="29.76905859375" />
                  <Point X="1.926622680664" Y="29.617384765625" />
                  <Point X="1.931039306641" Y="29.615783203125" />
                  <Point X="2.334462158203" Y="29.427115234375" />
                  <Point X="2.338694824219" Y="29.42513671875" />
                  <Point X="2.728419677734" Y="29.19808203125" />
                  <Point X="2.732532958984" Y="29.195685546875" />
                  <Point X="3.068740966797" Y="28.95659375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224851318359" Y="27.49151171875" />
                  <Point X="2.203708984375" Y="27.41244921875" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.210287353516" Y="27.32396875" />
                  <Point X="2.210414794922" Y="27.32291015625" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.260984619141" Y="27.23846875" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.337283203125" Y="27.181900390625" />
                  <Point X="2.338212402344" Y="27.18126953125" />
                  <Point X="2.360338134766" Y="27.172978515625" />
                  <Point X="2.428704345703" Y="27.164736328125" />
                  <Point X="2.448664550781" Y="27.1659453125" />
                  <Point X="2.5277265625" Y="27.187087890625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.563789794922" Y="27.2055546875" />
                  <Point X="3.994248291016" Y="28.031431640625" />
                  <Point X="4.200155273438" Y="27.745267578125" />
                  <Point X="4.202586425781" Y="27.741888671875" />
                  <Point X="4.387513183594" Y="27.436294921875" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.222483398438" Y="26.5096171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.191923828125" Y="26.415708984375" />
                  <Point X="3.191593994141" Y="26.414529296875" />
                  <Point X="3.190779052734" Y="26.390966796875" />
                  <Point X="3.208178466797" Y="26.306640625" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.262971191406" Y="26.2160234375" />
                  <Point X="3.263712158203" Y="26.214896484375" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.349527587891" Y="26.16021484375" />
                  <Point X="3.349532958984" Y="26.1602109375" />
                  <Point X="3.368565917969" Y="26.153619140625" />
                  <Point X="3.461290771484" Y="26.141365234375" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.497420410156" Y="26.144017578125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.938143554688" Y="25.95567578125" />
                  <Point X="4.939187988281" Y="25.95138671875" />
                  <Point X="4.997858398438" Y="25.5745546875" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.637987792969" Y="25.180162109375" />
                  <Point X="3.637988037109" Y="25.180162109375" />
                  <Point X="3.622266601562" Y="25.1669296875" />
                  <Point X="3.567606933594" Y="25.097279296875" />
                  <Point X="3.566760742188" Y="25.096201171875" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.538765380859" Y="24.97959765625" />
                  <Point X="3.538764160156" Y="24.979591796875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.556703125" Y="24.864177734375" />
                  <Point X="3.556986328125" Y="24.86269921875" />
                  <Point X="3.566760742188" Y="24.84123828125" />
                  <Point X="3.621420410156" Y="24.771587890625" />
                  <Point X="3.636577148438" Y="24.758091796875" />
                  <Point X="3.727652587891" Y="24.70544921875" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.760991210938" Y="24.69430078125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.949015625" Y="24.037474609375" />
                  <Point X="4.948430664062" Y="24.03359375" />
                  <Point X="4.874545898438" Y="23.7098203125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394838378906" Y="23.901658203125" />
                  <Point X="3.216042480469" Y="23.862796875" />
                  <Point X="3.213274169922" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.077380859375" Y="23.715333984375" />
                  <Point X="3.075707275391" Y="23.713322265625" />
                  <Point X="3.064357421875" Y="23.68592578125" />
                  <Point X="3.048868164062" Y="23.5176015625" />
                  <Point X="3.048628417969" Y="23.514994140625" />
                  <Point X="3.056360839844" Y="23.483375" />
                  <Point X="3.155309082031" Y="23.32946875" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.186856933594" Y="23.300341796875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.205774414062" Y="22.200515625" />
                  <Point X="4.204128417969" Y="22.1978515625" />
                  <Point X="4.0566875" Y="21.988359375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737341064453" Y="22.746685546875" />
                  <Point X="2.524545654297" Y="22.7851171875" />
                  <Point X="2.521231445312" Y="22.78571484375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.312297119141" Y="22.687716796875" />
                  <Point X="2.309549804688" Y="22.68626953125" />
                  <Point X="2.288602050781" Y="22.6653203125" />
                  <Point X="2.195563476562" Y="22.4885390625" />
                  <Point X="2.194123046875" Y="22.485802734375" />
                  <Point X="2.189162841797" Y="22.453626953125" />
                  <Point X="2.227593505859" Y="22.240830078125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.245913085938" Y="22.2009453125" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.837253173828" Y="20.81118359375" />
                  <Point X="2.835305419922" Y="20.80979296875" />
                  <Point X="2.679775390625" Y="20.70912109375" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.460675537109" Y="22.154462890625" />
                  <Point X="1.457413696289" Y="22.15655859375" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.196284545898" Y="22.143162109375" />
                  <Point X="1.19273034668" Y="22.1428359375" />
                  <Point X="1.165332763672" Y="22.131490234375" />
                  <Point X="0.988093444824" Y="21.98412109375" />
                  <Point X="0.985338134766" Y="21.981830078125" />
                  <Point X="0.968457763672" Y="21.954017578125" />
                  <Point X="0.915464111328" Y="21.710205078125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.917279602051" Y="21.663833984375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="0.996203735352" Y="20.03716015625" />
                  <Point X="0.994347412109" Y="20.03675390625" />
                  <Point X="0.860200256348" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#216" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.190297965249" Y="5.0653261847" Z="2.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="-0.205301466071" Y="5.074911947802" Z="2.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.5" />
                  <Point X="-0.995652588278" Y="4.980503814645" Z="2.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.5" />
                  <Point X="-1.7082996835" Y="4.448146597217" Z="2.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.5" />
                  <Point X="-1.708138087686" Y="4.441619523077" Z="2.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.5" />
                  <Point X="-1.741437351441" Y="4.340177718811" Z="2.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.5" />
                  <Point X="-1.840550907092" Y="4.300480373425" Z="2.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.5" />
                  <Point X="-2.131240526435" Y="4.605929519272" Z="2.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.5" />
                  <Point X="-2.144235126693" Y="4.604377896997" Z="2.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.5" />
                  <Point X="-2.795559906076" Y="4.240609982665" Z="2.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.5" />
                  <Point X="-3.007275393598" Y="3.150273425835" Z="2.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.5" />
                  <Point X="-3.001410560372" Y="3.139008452092" Z="2.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.5" />
                  <Point X="-2.994965925961" Y="3.053837641926" Z="2.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.5" />
                  <Point X="-3.056068013515" Y="2.994154138801" Z="2.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.5" />
                  <Point X="-3.783585922462" Y="3.372918555147" Z="2.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.5" />
                  <Point X="-3.799861084417" Y="3.370552673632" Z="2.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.5" />
                  <Point X="-4.212859457823" Y="2.837483301517" Z="2.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.5" />
                  <Point X="-3.709539753124" Y="1.62079207813" Z="2.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.5" />
                  <Point X="-3.696108816359" Y="1.609963006463" Z="2.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.5" />
                  <Point X="-3.667198258572" Y="1.552797058877" Z="2.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.5" />
                  <Point X="-3.692406534916" Y="1.493904793318" Z="2.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.5" />
                  <Point X="-4.800278107478" Y="1.612723037554" Z="2.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.5" />
                  <Point X="-4.818879692203" Y="1.606061209653" Z="2.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.5" />
                  <Point X="-4.97416491715" Y="1.028918518768" Z="2.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.5" />
                  <Point X="-3.59918522931" Y="0.055131246957" Z="2.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.5" />
                  <Point X="-3.576137559381" Y="0.04877532354" Z="2.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.5" />
                  <Point X="-3.548666798223" Y="0.029352454173" Z="2.5" />
                  <Point X="-3.539556741714" Y="0" Z="2.5" />
                  <Point X="-3.539697802912" Y="-0.000454496888" Z="2.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.5" />
                  <Point X="-3.549231035742" Y="-0.030100659594" Z="2.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.5" />
                  <Point X="-5.037702559463" Y="-0.440580809799" Z="2.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.5" />
                  <Point X="-5.059142810836" Y="-0.454923121477" Z="2.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.5" />
                  <Point X="-4.980591677739" Y="-0.997774967371" Z="2.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.5" />
                  <Point X="-3.243978762234" Y="-1.310130905777" Z="2.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.5" />
                  <Point X="-3.218755081009" Y="-1.307100971095" Z="2.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.5" />
                  <Point X="-3.192793784061" Y="-1.3229033917" Z="2.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.5" />
                  <Point X="-4.483039756779" Y="-2.336415960968" Z="2.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.5" />
                  <Point X="-4.498424610343" Y="-2.359161273806" Z="2.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.5" />
                  <Point X="-4.204062047001" Y="-2.850840780243" Z="2.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.5" />
                  <Point X="-2.5925002819" Y="-2.566842129031" Z="2.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.5" />
                  <Point X="-2.572574967876" Y="-2.555755501255" Z="2.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.5" />
                  <Point X="-3.288574884137" Y="-3.842578100592" Z="2.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.5" />
                  <Point X="-3.293682734016" Y="-3.867046023603" Z="2.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.5" />
                  <Point X="-2.88377617712" Y="-4.181650216827" Z="2.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.5" />
                  <Point X="-2.229651486684" Y="-4.160921210869" Z="2.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.5" />
                  <Point X="-2.222288802539" Y="-4.153823911689" Z="2.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.5" />
                  <Point X="-1.963535376207" Y="-3.984393773903" Z="2.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.5" />
                  <Point X="-1.655110083492" Y="-4.007495138604" Z="2.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.5" />
                  <Point X="-1.42448305568" Y="-4.213580351293" Z="2.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.5" />
                  <Point X="-1.412363783299" Y="-4.873918343503" Z="2.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.5" />
                  <Point X="-1.408590254488" Y="-4.880663324148" Z="2.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.5" />
                  <Point X="-1.112831492951" Y="-4.956470692524" Z="2.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="-0.423194850433" Y="-3.541568709474" Z="2.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="-0.414590257354" Y="-3.515176065645" Z="2.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="-0.249495855884" Y="-3.281673674848" Z="2.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.5" />
                  <Point X="0.003863223476" Y="-3.20543837044" Z="2.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.5" />
                  <Point X="0.255855602078" Y="-3.286469690123" Z="2.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.5" />
                  <Point X="0.811560009968" Y="-4.990967320342" Z="2.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.5" />
                  <Point X="0.820417939353" Y="-5.013263401872" Z="2.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.5" />
                  <Point X="1.000740206392" Y="-4.980402952187" Z="2.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.5" />
                  <Point X="0.960695882631" Y="-3.298360218998" Z="2.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.5" />
                  <Point X="0.958166344025" Y="-3.269138473286" Z="2.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.5" />
                  <Point X="1.013904411061" Y="-3.023044139116" Z="2.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.5" />
                  <Point X="1.194697861121" Y="-2.875348387402" Z="2.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.5" />
                  <Point X="1.427480166139" Y="-2.856315852334" Z="2.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.5" />
                  <Point X="2.646422138372" Y="-4.306288336414" Z="2.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.5" />
                  <Point X="2.665023494066" Y="-4.324723779245" Z="2.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.5" />
                  <Point X="2.860159098462" Y="-4.198222944594" Z="2.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.5" />
                  <Point X="2.283058313457" Y="-2.742773908323" Z="2.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.5" />
                  <Point X="2.270641830715" Y="-2.719003671479" Z="2.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.5" />
                  <Point X="2.233651731628" Y="-2.503470888155" Z="2.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.5" />
                  <Point X="2.329427490217" Y="-2.325249578339" Z="2.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.5" />
                  <Point X="2.509503132034" Y="-2.232806178776" Z="2.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.5" />
                  <Point X="4.044639688783" Y="-3.034691186818" Z="2.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.5" />
                  <Point X="4.067777389652" Y="-3.04272967923" Z="2.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.5" />
                  <Point X="4.242154101566" Y="-2.794484614797" Z="2.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.5" />
                  <Point X="3.211139034035" Y="-1.628708938924" Z="2.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.5" />
                  <Point X="3.191210698985" Y="-1.612209908391" Z="2.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.5" />
                  <Point X="3.092502341785" Y="-1.455696150305" Z="2.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.5" />
                  <Point X="3.109664836429" Y="-1.285359675513" Z="2.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.5" />
                  <Point X="3.220504024901" Y="-1.154782438881" Z="2.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.5" />
                  <Point X="4.884017200855" Y="-1.311387155841" Z="2.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.5" />
                  <Point X="4.908294155754" Y="-1.308772156914" Z="2.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.5" />
                  <Point X="4.992301071731" Y="-0.938700788063" Z="2.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.5" />
                  <Point X="3.76777607314" Y="-0.226122293549" Z="2.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.5" />
                  <Point X="3.746542105864" Y="-0.219995288379" Z="2.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.5" />
                  <Point X="3.654596358413" Y="-0.166259750576" Z="2.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.5" />
                  <Point X="3.59965460495" Y="-0.095137412217" Z="2.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.5" />
                  <Point X="3.581716845474" Y="0.001473118956" Z="2.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.5" />
                  <Point X="3.600783079987" Y="0.097688987999" Z="2.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.5" />
                  <Point X="3.656853308488" Y="0.168153555863" Z="2.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.5" />
                  <Point X="5.028191744166" Y="0.56384963922" Z="2.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.5" />
                  <Point X="5.047010239293" Y="0.57561546551" Z="2.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.5" />
                  <Point X="4.980567842339" Y="0.998787150061" Z="2.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.5" />
                  <Point X="3.48473797933" Y="1.224869958804" Z="2.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.5" />
                  <Point X="3.461685648805" Y="1.222213837023" Z="2.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.5" />
                  <Point X="3.367898715572" Y="1.235066338931" Z="2.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.5" />
                  <Point X="3.298585769598" Y="1.274784828066" Z="2.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.5" />
                  <Point X="3.25099157923" Y="1.348022134343" Z="2.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.5" />
                  <Point X="3.233920230973" Y="1.433522760962" Z="2.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.5" />
                  <Point X="3.255997049412" Y="1.510463119403" Z="2.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.5" />
                  <Point X="4.430014318854" Y="2.441888449425" Z="2.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.5" />
                  <Point X="4.444123092444" Y="2.460430835126" Z="2.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.5" />
                  <Point X="4.234586620218" Y="2.805746901938" Z="2.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.5" />
                  <Point X="2.532633804763" Y="2.28013688321" Z="2.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.5" />
                  <Point X="2.508653713849" Y="2.266671399139" Z="2.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.5" />
                  <Point X="2.428533064189" Y="2.245656821337" Z="2.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.5" />
                  <Point X="2.359201455744" Y="2.254555545308" Z="2.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.5" />
                  <Point X="2.296203054927" Y="2.297823404638" Z="2.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.5" />
                  <Point X="2.253772881421" Y="2.361225379643" Z="2.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.5" />
                  <Point X="2.245856543518" Y="2.430815862033" Z="2.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.5" />
                  <Point X="3.115488469649" Y="3.979505277192" Z="2.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.5" />
                  <Point X="3.122906615617" Y="4.006328888027" Z="2.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.5" />
                  <Point X="2.747433167234" Y="4.272565293294" Z="2.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.5" />
                  <Point X="2.349484631596" Y="4.503689454734" Z="2.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.5" />
                  <Point X="1.937515857713" Y="4.695669622845" Z="2.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.5" />
                  <Point X="1.506762649233" Y="4.850697268768" Z="2.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.5" />
                  <Point X="0.852352950287" Y="5.007296780804" Z="2.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.5" />
                  <Point X="0.002946596304" Y="4.366121321251" Z="2.5" />
                  <Point X="0" Y="4.355124473572" Z="2.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>