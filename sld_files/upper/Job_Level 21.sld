<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#157" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1610" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.721211425781" Y="20.898150390625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721313477" Y="21.502859375" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.477821594238" Y="21.6256171875" />
                  <Point X="0.378635528564" Y="21.768525390625" />
                  <Point X="0.35674887085" Y="21.790982421875" />
                  <Point X="0.330493835449" Y="21.810224609375" />
                  <Point X="0.302494415283" Y="21.824330078125" />
                  <Point X="0.202620117188" Y="21.855326171875" />
                  <Point X="0.049135276794" Y="21.902962890625" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036824542999" Y="21.90296484375" />
                  <Point X="-0.13669883728" Y="21.871966796875" />
                  <Point X="-0.290183685303" Y="21.82433203125" />
                  <Point X="-0.318184295654" Y="21.810224609375" />
                  <Point X="-0.344439788818" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.768525390625" />
                  <Point X="-0.43086517334" Y="21.675533203125" />
                  <Point X="-0.530051208496" Y="21.532625" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.793661865234" Y="20.5818125" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-1.079328735352" Y="20.1546484375" />
                  <Point X="-1.1916796875" Y="20.1835546875" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.233971557617" Y="20.29217578125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.240368530273" Y="20.6037265625" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323645019531" Y="20.868796875" />
                  <Point X="-1.415596679688" Y="20.949435546875" />
                  <Point X="-1.556905883789" Y="21.073361328125" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643027954102" Y="21.109033203125" />
                  <Point X="-1.765068237305" Y="21.11703125" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.144348144531" Y="21.037251953125" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.471343994141" Y="20.722984375" />
                  <Point X="-2.480147949219" Y="20.71151171875" />
                  <Point X="-2.541824462891" Y="20.74969921875" />
                  <Point X="-2.801724853516" Y="20.910623046875" />
                  <Point X="-2.957263916016" Y="21.0303828125" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.717218261719" Y="21.81509765625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575195312" Y="22.4148046875" />
                  <Point X="-2.414559326172" Y="22.444421875" />
                  <Point X="-2.428776367188" Y="22.473251953125" />
                  <Point X="-2.446806396484" Y="22.4984140625" />
                  <Point X="-2.464155029297" Y="22.51576171875" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.420300537109" Y="22.087822265625" />
                  <Point X="-3.818022949219" Y="21.858197265625" />
                  <Point X="-3.877537109375" Y="21.93638671875" />
                  <Point X="-4.082862548828" Y="22.206142578125" />
                  <Point X="-4.19437890625" Y="22.393138671875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.619408935547" Y="23.107498046875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556884766" Y="23.672015625" />
                  <Point X="-3.052559326172" Y="23.701763671875" />
                  <Point X="-3.068644775391" Y="23.72775" />
                  <Point X="-3.089479003906" Y="23.751705078125" />
                  <Point X="-3.112975830078" Y="23.771234375" />
                  <Point X="-3.139458496094" Y="23.7868203125" />
                  <Point X="-3.168722412109" Y="23.79804296875" />
                  <Point X="-3.200607910156" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.218015625" Y="23.675794921875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.7537734375" Y="23.692958984375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.863582519531" Y="24.213640625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.117777832031" Y="24.622865234375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.511116210938" Y="24.78842578125" />
                  <Point X="-3.484609619141" Y="24.803240234375" />
                  <Point X="-3.476793212891" Y="24.80812109375" />
                  <Point X="-3.45998046875" Y="24.8197890625" />
                  <Point X="-3.436024658203" Y="24.841314453125" />
                  <Point X="-3.414675048828" Y="24.872712890625" />
                  <Point X="-3.403602294922" Y="24.899201171875" />
                  <Point X="-3.398850585938" Y="24.913775390625" />
                  <Point X="-3.390999267578" Y="24.94665234375" />
                  <Point X="-3.388404052734" Y="24.967951171875" />
                  <Point X="-3.390654785156" Y="24.9892890625" />
                  <Point X="-3.396548095703" Y="25.015859375" />
                  <Point X="-3.401007080078" Y="25.030365234375" />
                  <Point X="-3.414038085938" Y="25.0631640625" />
                  <Point X="-3.425299804688" Y="25.083693359375" />
                  <Point X="-3.441241210938" Y="25.100845703125" />
                  <Point X="-3.464002685547" Y="25.119955078125" />
                  <Point X="-3.470917724609" Y="25.125240234375" />
                  <Point X="-3.487725341797" Y="25.13690625" />
                  <Point X="-3.501916503906" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.431740234375" Y="25.39869921875" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.8762421875" Y="25.627224609375" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.765094726562" Y="26.19615234375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.184938476563" Y="26.354990234375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984619141" Y="26.299341796875" />
                  <Point X="-3.723423828125" Y="26.301228515625" />
                  <Point X="-3.703137939453" Y="26.305263671875" />
                  <Point X="-3.678923828125" Y="26.3128984375" />
                  <Point X="-3.641711914063" Y="26.324630859375" />
                  <Point X="-3.62278515625" Y="26.332958984375" />
                  <Point X="-3.604040771484" Y="26.343779296875" />
                  <Point X="-3.587355957031" Y="26.35601171875" />
                  <Point X="-3.573715087891" Y="26.37156640625" />
                  <Point X="-3.561300292969" Y="26.389296875" />
                  <Point X="-3.551351318359" Y="26.407431640625" />
                  <Point X="-3.541635253906" Y="26.430888671875" />
                  <Point X="-3.526703857422" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.515804443359" Y="26.52875" />
                  <Point X="-3.518951660156" Y="26.5491953125" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532049560547" Y="26.589376953125" />
                  <Point X="-3.543772949219" Y="26.6118984375" />
                  <Point X="-3.561789306641" Y="26.646505859375" />
                  <Point X="-3.573281494141" Y="26.663705078125" />
                  <Point X="-3.587193603516" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.1177265625" Y="27.090216796875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.28225" Y="27.3891328125" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-3.923825195313" Y="27.9358828125" />
                  <Point X="-3.750504150391" Y="28.158662109375" />
                  <Point X="-3.464162353516" Y="27.993341796875" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187728271484" Y="27.836341796875" />
                  <Point X="-3.167085205078" Y="27.82983203125" />
                  <Point X="-3.146795654297" Y="27.825794921875" />
                  <Point X="-3.113072021484" Y="27.82284375" />
                  <Point X="-3.061246337891" Y="27.818310546875" />
                  <Point X="-3.040559570312" Y="27.81876171875" />
                  <Point X="-3.0191015625" Y="27.821587890625" />
                  <Point X="-2.999012207031" Y="27.826505859375" />
                  <Point X="-2.980462890625" Y="27.83565234375" />
                  <Point X="-2.962209228516" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.922140136719" Y="27.88416796875" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846386230469" Y="28.06984375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.098268798828" Y="28.57726171875" />
                  <Point X="-3.183332275391" Y="28.724595703125" />
                  <Point X="-3.050867675781" Y="28.826154296875" />
                  <Point X="-2.700621582031" Y="29.094685546875" />
                  <Point X="-2.452840576172" Y="29.23234765625" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.122025634766" Y="29.332474609375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028890869141" Y="29.214798828125" />
                  <Point X="-2.012310791016" Y="29.20088671875" />
                  <Point X="-1.995116210938" Y="29.1893984375" />
                  <Point X="-1.95758215332" Y="29.169857421875" />
                  <Point X="-1.899900268555" Y="29.139830078125" />
                  <Point X="-1.880624755859" Y="29.13233203125" />
                  <Point X="-1.859717895508" Y="29.126728515625" />
                  <Point X="-1.839268066406" Y="29.123580078125" />
                  <Point X="-1.818621704102" Y="29.12493359375" />
                  <Point X="-1.797306274414" Y="29.128693359375" />
                  <Point X="-1.777453613281" Y="29.13448046875" />
                  <Point X="-1.738359008789" Y="29.150673828125" />
                  <Point X="-1.678279663086" Y="29.175560546875" />
                  <Point X="-1.660144897461" Y="29.185509765625" />
                  <Point X="-1.642415649414" Y="29.197923828125" />
                  <Point X="-1.626863525391" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.582755615234" Y="29.306279296875" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.583705322266" Y="29.628125" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.403044799805" Y="29.6826875" />
                  <Point X="-0.949623168945" Y="29.80980859375" />
                  <Point X="-0.649250427246" Y="29.84496484375" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.21008039856" Y="29.570611328125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425933838" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.263282714844" Y="29.723216796875" />
                  <Point X="0.307419403076" Y="29.8879375" />
                  <Point X="0.448141876221" Y="29.87319921875" />
                  <Point X="0.844045410156" Y="29.83173828125" />
                  <Point X="1.09256237793" Y="29.77173828125" />
                  <Point X="1.48102355957" Y="29.677951171875" />
                  <Point X="1.641892333984" Y="29.619603515625" />
                  <Point X="1.894645507812" Y="29.527927734375" />
                  <Point X="2.051066162109" Y="29.454775390625" />
                  <Point X="2.294559570312" Y="29.340900390625" />
                  <Point X="2.445714111328" Y="29.25283984375" />
                  <Point X="2.680972412109" Y="29.11577734375" />
                  <Point X="2.823495849609" Y="29.014421875" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.487235351562" Y="28.139396484375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.516056640625" />
                  <Point X="2.124613525391" Y="27.484408203125" />
                  <Point X="2.111607177734" Y="27.43576953125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.111028076172" Y="27.353583984375" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070556641" Y="27.24747265625" />
                  <Point X="2.157004638672" Y="27.222515625" />
                  <Point X="2.183028320312" Y="27.1841640625" />
                  <Point X="2.194464355469" Y="27.170330078125" />
                  <Point X="2.221598388672" Y="27.14559375" />
                  <Point X="2.2465546875" Y="27.12866015625" />
                  <Point X="2.284906982422" Y="27.102634765625" />
                  <Point X="2.304945800781" Y="27.0922734375" />
                  <Point X="2.327034423828" Y="27.084005859375" />
                  <Point X="2.348967041016" Y="27.078662109375" />
                  <Point X="2.376334472656" Y="27.07536328125" />
                  <Point X="2.418391845703" Y="27.070291015625" />
                  <Point X="2.436467529297" Y="27.06984375" />
                  <Point X="2.473209960938" Y="27.074171875" />
                  <Point X="2.504858886719" Y="27.08263671875" />
                  <Point X="2.553496337891" Y="27.095642578125" />
                  <Point X="2.565285644531" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.492618408203" Y="27.632119140625" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="3.983720214844" Y="27.88340625" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.202724121094" Y="27.558166015625" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.677970703125" Y="27.01158984375" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.181196533203" Y="26.611912109375" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.136606689453" Y="26.550912109375" />
                  <Point X="3.121629638672" Y="26.5170859375" />
                  <Point X="3.113144775391" Y="26.48674609375" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.104704589844" Y="26.33801171875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282470703" Y="26.23573828125" />
                  <Point X="3.155226806641" Y="26.206943359375" />
                  <Point X="3.184340087891" Y="26.162693359375" />
                  <Point X="3.198895507812" Y="26.145447265625" />
                  <Point X="3.216138183594" Y="26.129359375" />
                  <Point X="3.234347167969" Y="26.11603515625" />
                  <Point X="3.261800048828" Y="26.10058203125" />
                  <Point X="3.303989257812" Y="26.07683203125" />
                  <Point X="3.320520751953" Y="26.069501953125" />
                  <Point X="3.356121582031" Y="26.0594375" />
                  <Point X="3.393239746094" Y="26.054533203125" />
                  <Point X="3.450282226562" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.3470234375" Y="26.160048828125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.785999511719" Y="26.179009765625" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.870973632812" Y="25.77199609375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.229065429688" Y="25.46691015625" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704787597656" Y="25.325583984375" />
                  <Point X="3.681547851562" Y="25.315068359375" />
                  <Point X="3.645080322266" Y="25.293990234375" />
                  <Point X="3.589037841797" Y="25.26159765625" />
                  <Point X="3.574309814453" Y="25.251095703125" />
                  <Point X="3.547530517578" Y="25.225576171875" />
                  <Point X="3.525650146484" Y="25.1976953125" />
                  <Point X="3.492024658203" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.456387207031" Y="25.05451953125" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.45247265625" Y="24.903361328125" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.513905029297" Y="24.7547109375" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559993652344" Y="24.69876953125" />
                  <Point X="3.589036621094" Y="24.675841796875" />
                  <Point X="3.625504150391" Y="24.654763671875" />
                  <Point X="3.681546630859" Y="24.622369140625" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.504156738281" Y="24.396818359375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.888488769531" Y="24.27324609375" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.8229453125" Y="23.910705078125" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.018721435547" Y="23.9183125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.303086181641" Y="23.97893359375" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163973876953" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112396728516" Y="23.9060390625" />
                  <Point X="3.069135498047" Y="23.8540078125" />
                  <Point X="3.002652587891" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.963557128906" Y="23.62725390625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.016060058594" Y="23.370392578125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.841504150391" Y="22.678267578125" />
                  <Point X="4.21312109375" Y="22.393115234375" />
                  <Point X="4.124808105469" Y="22.2502109375" />
                  <Point X="4.058471435547" Y="22.15595703125" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.330293945312" Y="22.51744140625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786128417969" Y="22.82998828125" />
                  <Point X="2.754224853516" Y="22.840173828125" />
                  <Point X="2.669041748047" Y="22.85555859375" />
                  <Point X="2.538134521484" Y="22.87919921875" />
                  <Point X="2.506783203125" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.374067138672" Y="22.827578125" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242386962891" Y="22.753453125" />
                  <Point X="2.221426757812" Y="22.732494140625" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.167288330078" Y="22.638794921875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.111059082031" Y="22.35155859375" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.62148046875" Y="21.360443359375" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781848632813" Y="20.88835546875" />
                  <Point X="2.707680664062" Y="20.84034765625" />
                  <Point X="2.701763916016" Y="20.836517578125" />
                  <Point X="2.162480957031" Y="21.539326171875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721923217773" Y="22.099443359375" />
                  <Point X="1.637909912109" Y="22.153455078125" />
                  <Point X="1.508799926758" Y="22.2364609375" />
                  <Point X="1.479986694336" Y="22.248833984375" />
                  <Point X="1.448366088867" Y="22.256564453125" />
                  <Point X="1.417100219727" Y="22.258880859375" />
                  <Point X="1.325217163086" Y="22.25042578125" />
                  <Point X="1.184013305664" Y="22.23743359375" />
                  <Point X="1.156368164062" Y="22.230603515625" />
                  <Point X="1.128982543945" Y="22.21926171875" />
                  <Point X="1.104594604492" Y="22.2045390625" />
                  <Point X="1.033644897461" Y="22.145544921875" />
                  <Point X="0.924610900879" Y="22.05488671875" />
                  <Point X="0.904140014648" Y="22.031134765625" />
                  <Point X="0.887247680664" Y="22.003306640625" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.854410888672" Y="21.876591796875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.952840759277" Y="20.665896484375" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975723815918" Y="20.12992578125" />
                  <Point X="0.929315429688" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058426025391" Y="20.247365234375" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.139784301758" Y="20.279775390625" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.147193847656" Y="20.622259765625" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573486328" Y="20.905138671875" />
                  <Point X="-1.250208862305" Y="20.929064453125" />
                  <Point X="-1.261007446289" Y="20.94022265625" />
                  <Point X="-1.352959106445" Y="21.020861328125" />
                  <Point X="-1.494268310547" Y="21.144787109375" />
                  <Point X="-1.506739868164" Y="21.15403515625" />
                  <Point X="-1.533023071289" Y="21.17037890625" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576531494141" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621457885742" Y="21.201552734375" />
                  <Point X="-1.636815307617" Y="21.203830078125" />
                  <Point X="-1.75885559082" Y="21.211828125" />
                  <Point X="-1.946404296875" Y="21.22412109375" />
                  <Point X="-1.961928344727" Y="21.2238671875" />
                  <Point X="-1.992725585938" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047729492" Y="21.209736328125" />
                  <Point X="-2.053666992188" Y="21.204505859375" />
                  <Point X="-2.081861328125" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.197127197266" Y="21.1162421875" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410470947266" Y="20.958369140625" />
                  <Point X="-2.503201171875" Y="20.837521484375" />
                  <Point X="-2.747597167969" Y="20.988845703125" />
                  <Point X="-2.899306640625" Y="21.10565625" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.634945800781" Y="21.76759765625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665771484" Y="22.442380859375" />
                  <Point X="-2.323649902344" Y="22.471998046875" />
                  <Point X="-2.329355957031" Y="22.4864375" />
                  <Point X="-2.343572998047" Y="22.515267578125" />
                  <Point X="-2.3515546875" Y="22.5285859375" />
                  <Point X="-2.369584716797" Y="22.553748046875" />
                  <Point X="-2.379633056641" Y="22.565591796875" />
                  <Point X="-2.396981689453" Y="22.582939453125" />
                  <Point X="-2.408822021484" Y="22.592984375" />
                  <Point X="-2.433978027344" Y="22.611009765625" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.467800537109" Y="22.17009375" />
                  <Point X="-3.793086669922" Y="21.9822890625" />
                  <Point X="-3.801943603516" Y="21.99392578125" />
                  <Point X="-4.004021240234" Y="22.2594140625" />
                  <Point X="-4.112786132813" Y="22.441796875" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.561576660156" Y="23.03212890625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950787109375" Y="23.678625" />
                  <Point X="-2.953084228516" Y="23.693783203125" />
                  <Point X="-2.960086669922" Y="23.72353125" />
                  <Point X="-2.971782226562" Y="23.751763671875" />
                  <Point X="-2.987867675781" Y="23.77775" />
                  <Point X="-2.996962890625" Y="23.79009375" />
                  <Point X="-3.017797119141" Y="23.814048828125" />
                  <Point X="-3.028755859375" Y="23.824763671875" />
                  <Point X="-3.052252685547" Y="23.84429296875" />
                  <Point X="-3.064790771484" Y="23.853107421875" />
                  <Point X="-3.0912734375" Y="23.868693359375" />
                  <Point X="-3.105441894531" Y="23.875521484375" />
                  <Point X="-3.134705810547" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891138671875" />
                  <Point X="-3.181686767578" Y="23.897619140625" />
                  <Point X="-3.197298339844" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.230415527344" Y="23.769982421875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.661729003906" Y="23.716470703125" />
                  <Point X="-4.74076171875" Y="24.02587890625" />
                  <Point X="-4.769539550781" Y="24.227091796875" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.093189941406" Y="24.5311015625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497134033203" Y="24.6915703125" />
                  <Point X="-3.475374267578" Y="24.70040625" />
                  <Point X="-3.464768554688" Y="24.705498046875" />
                  <Point X="-3.438261962891" Y="24.7203125" />
                  <Point X="-3.422629150391" Y="24.73007421875" />
                  <Point X="-3.40581640625" Y="24.7417421875" />
                  <Point X="-3.396485595703" Y="24.749125" />
                  <Point X="-3.372529785156" Y="24.770650390625" />
                  <Point X="-3.357465087891" Y="24.787896484375" />
                  <Point X="-3.336115478516" Y="24.819294921875" />
                  <Point X="-3.327025146484" Y="24.836072265625" />
                  <Point X="-3.315952392578" Y="24.862560546875" />
                  <Point X="-3.313281494141" Y="24.86975390625" />
                  <Point X="-3.306448974609" Y="24.891708984375" />
                  <Point X="-3.29859765625" Y="24.9245859375" />
                  <Point X="-3.296696777344" Y="24.935162109375" />
                  <Point X="-3.2941015625" Y="24.9564609375" />
                  <Point X="-3.293928222656" Y="24.977916015625" />
                  <Point X="-3.296178955078" Y="24.99925390625" />
                  <Point X="-3.297908691406" Y="25.009859375" />
                  <Point X="-3.303802001953" Y="25.0364296875" />
                  <Point X="-3.305741455078" Y="25.0437734375" />
                  <Point X="-3.312719970703" Y="25.06544140625" />
                  <Point X="-3.325750976562" Y="25.098240234375" />
                  <Point X="-3.330747314453" Y="25.10885546875" />
                  <Point X="-3.342009033203" Y="25.129384765625" />
                  <Point X="-3.355713134766" Y="25.1483671875" />
                  <Point X="-3.371654541016" Y="25.16551953125" />
                  <Point X="-3.380157226562" Y="25.173603515625" />
                  <Point X="-3.402918701172" Y="25.192712890625" />
                  <Point X="-3.416748779297" Y="25.203283203125" />
                  <Point X="-3.433556396484" Y="25.21494921875" />
                  <Point X="-3.44048046875" Y="25.219326171875" />
                  <Point X="-3.465598388672" Y="25.23282421875" />
                  <Point X="-3.496557861328" Y="25.2456328125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.40715234375" Y="25.490462890625" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.782265625" Y="25.613318359375" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.673401855469" Y="26.1713046875" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.197338378906" Y="26.260802734375" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056640625" Y="26.204365234375" />
                  <Point X="-3.736703125" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704890136719" Y="26.2080546875" />
                  <Point X="-3.684604248047" Y="26.21208984375" />
                  <Point X="-3.674570556641" Y="26.21466015625" />
                  <Point X="-3.650356445312" Y="26.222294921875" />
                  <Point X="-3.61314453125" Y="26.23402734375" />
                  <Point X="-3.603450439453" Y="26.23767578125" />
                  <Point X="-3.584523681641" Y="26.24600390625" />
                  <Point X="-3.575291015625" Y="26.25068359375" />
                  <Point X="-3.556546630859" Y="26.26150390625" />
                  <Point X="-3.547870605469" Y="26.2671640625" />
                  <Point X="-3.531185791016" Y="26.279396484375" />
                  <Point X="-3.515930664062" Y="26.293375" />
                  <Point X="-3.502289794922" Y="26.3089296875" />
                  <Point X="-3.495895263672" Y="26.317078125" />
                  <Point X="-3.48348046875" Y="26.33480859375" />
                  <Point X="-3.478010986328" Y="26.343603515625" />
                  <Point X="-3.468062011719" Y="26.36173828125" />
                  <Point X="-3.463582519531" Y="26.371078125" />
                  <Point X="-3.453866455078" Y="26.39453515625" />
                  <Point X="-3.438935058594" Y="26.43058203125" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429711181641" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.501896484375" />
                  <Point X="-3.4210078125" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057617188" Y="26.5636484375" />
                  <Point X="-3.427189208984" Y="26.57378515625" />
                  <Point X="-3.432791503906" Y="26.59469140625" />
                  <Point X="-3.436012939453" Y="26.604533203125" />
                  <Point X="-3.443508544922" Y="26.62380859375" />
                  <Point X="-3.447782714844" Y="26.6332421875" />
                  <Point X="-3.459506103516" Y="26.655763671875" />
                  <Point X="-3.477522460938" Y="26.69037109375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494291992188" Y="26.716484375" />
                  <Point X="-3.500506835938" Y="26.72476953125" />
                  <Point X="-3.514418945312" Y="26.741349609375" />
                  <Point X="-3.521498779297" Y="26.748908203125" />
                  <Point X="-3.536441162109" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.059894287109" Y="27.1655859375" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.200203613281" Y="27.341244140625" />
                  <Point X="-4.002296386719" Y="27.6803046875" />
                  <Point X="-3.848844238281" Y="27.877548828125" />
                  <Point X="-3.726337646484" Y="28.035013671875" />
                  <Point X="-3.511662597656" Y="27.9110703125" />
                  <Point X="-3.254157470703" Y="27.762400390625" />
                  <Point X="-3.244922607422" Y="27.75771875" />
                  <Point X="-3.225993652344" Y="27.749388671875" />
                  <Point X="-3.216299560547" Y="27.745740234375" />
                  <Point X="-3.195656494141" Y="27.73923046875" />
                  <Point X="-3.185624267578" Y="27.736658203125" />
                  <Point X="-3.165334716797" Y="27.73262109375" />
                  <Point X="-3.155077392578" Y="27.73115625" />
                  <Point X="-3.121353759766" Y="27.728205078125" />
                  <Point X="-3.069528076172" Y="27.723671875" />
                  <Point X="-3.059174804688" Y="27.723333984375" />
                  <Point X="-3.038488037109" Y="27.72378515625" />
                  <Point X="-3.028154541016" Y="27.72457421875" />
                  <Point X="-3.006696533203" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976422851562" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.93844921875" Y="27.750447265625" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911165527344" Y="27.767158203125" />
                  <Point X="-2.902740722656" Y="27.773197265625" />
                  <Point X="-2.886608886719" Y="27.786146484375" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.854964599609" Y="27.816994140625" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751747802734" Y="28.078123046875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.015996337891" Y="28.62476171875" />
                  <Point X="-3.059386230469" Y="28.6999140625" />
                  <Point X="-2.993065917969" Y="28.75076171875" />
                  <Point X="-2.648371337891" Y="29.015037109375" />
                  <Point X="-2.406702880859" Y="29.149302734375" />
                  <Point X="-2.192524658203" Y="29.268294921875" />
                  <Point X="-2.118564453125" Y="29.17191015625" />
                  <Point X="-2.111821289062" Y="29.16405078125" />
                  <Point X="-2.097516845703" Y="29.149107421875" />
                  <Point X="-2.089955078125" Y="29.1420234375" />
                  <Point X="-2.073375" Y="29.128111328125" />
                  <Point X="-2.065087402344" Y="29.12189453125" />
                  <Point X="-2.047892944336" Y="29.11040625" />
                  <Point X="-2.038985839844" Y="29.105134765625" />
                  <Point X="-2.001451782227" Y="29.08559375" />
                  <Point X="-1.943769897461" Y="29.05556640625" />
                  <Point X="-1.934340698242" Y="29.05129296875" />
                  <Point X="-1.915065185547" Y="29.043794921875" />
                  <Point X="-1.90521875" Y="29.0405703125" />
                  <Point X="-1.884312011719" Y="29.034966796875" />
                  <Point X="-1.874173706055" Y="29.032833984375" />
                  <Point X="-1.853723999023" Y="29.029685546875" />
                  <Point X="-1.833053466797" Y="29.028783203125" />
                  <Point X="-1.812407104492" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.780804199219" Y="29.03513671875" />
                  <Point X="-1.770720092773" Y="29.037490234375" />
                  <Point X="-1.750867431641" Y="29.04327734375" />
                  <Point X="-1.741098999023" Y="29.0467109375" />
                  <Point X="-1.702004394531" Y="29.062904296875" />
                  <Point X="-1.641925048828" Y="29.087791015625" />
                  <Point X="-1.632585205078" Y="29.092271484375" />
                  <Point X="-1.614450439453" Y="29.102220703125" />
                  <Point X="-1.605655395508" Y="29.107689453125" />
                  <Point X="-1.587926147461" Y="29.120103515625" />
                  <Point X="-1.579778198242" Y="29.126498046875" />
                  <Point X="-1.564225952148" Y="29.14013671875" />
                  <Point X="-1.550250488281" Y="29.155388671875" />
                  <Point X="-1.53801953125" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516854370117" Y="29.20873046875" />
                  <Point X="-1.508524169922" Y="29.2276640625" />
                  <Point X="-1.504877441406" Y="29.237353515625" />
                  <Point X="-1.49215246582" Y="29.2777109375" />
                  <Point X="-1.472597900391" Y="29.33973046875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.37739855957" Y="29.59121484375" />
                  <Point X="-0.931162902832" Y="29.7163203125" />
                  <Point X="-0.638206787109" Y="29.750609375" />
                  <Point X="-0.36522265625" Y="29.78255859375" />
                  <Point X="-0.301843292236" Y="29.5460234375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.355045715332" Y="29.69862890625" />
                  <Point X="0.378190490723" Y="29.785005859375" />
                  <Point X="0.438246429443" Y="29.778716796875" />
                  <Point X="0.827865966797" Y="29.7379140625" />
                  <Point X="1.070266967773" Y="29.679390625" />
                  <Point X="1.453607910156" Y="29.58683984375" />
                  <Point X="1.609500366211" Y="29.530296875" />
                  <Point X="1.858248291016" Y="29.44007421875" />
                  <Point X="2.010821533203" Y="29.368720703125" />
                  <Point X="2.250435546875" Y="29.25666015625" />
                  <Point X="2.397892089844" Y="29.17075390625" />
                  <Point X="2.629424804688" Y="29.035861328125" />
                  <Point X="2.768439208984" Y="28.937001953125" />
                  <Point X="2.817778564453" Y="28.9019140625" />
                  <Point X="2.404962890625" Y="28.186896484375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053180664062" Y="27.573435546875" />
                  <Point X="2.044182373047" Y="27.549564453125" />
                  <Point X="2.041301757812" Y="27.540599609375" />
                  <Point X="2.032838378906" Y="27.508951171875" />
                  <Point X="2.01983203125" Y="27.4603125" />
                  <Point X="2.017912231445" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.016711303711" Y="27.3422109375" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.04532043457" Y="27.223884765625" />
                  <Point X="2.055681884766" Y="27.20384375" />
                  <Point X="2.061458740234" Y="27.1941328125" />
                  <Point X="2.078392822266" Y="27.16917578125" />
                  <Point X="2.104416503906" Y="27.13082421875" />
                  <Point X="2.109807617188" Y="27.123634765625" />
                  <Point X="2.130462646484" Y="27.100125" />
                  <Point X="2.157596679688" Y="27.075388671875" />
                  <Point X="2.168258056641" Y="27.066982421875" />
                  <Point X="2.193214355469" Y="27.050048828125" />
                  <Point X="2.231566650391" Y="27.0240234375" />
                  <Point X="2.241273681641" Y="27.018248046875" />
                  <Point X="2.2613125" Y="27.00788671875" />
                  <Point X="2.271644287109" Y="27.00330078125" />
                  <Point X="2.293732910156" Y="26.995033203125" />
                  <Point X="2.304546142578" Y="26.991705078125" />
                  <Point X="2.326478759766" Y="26.986361328125" />
                  <Point X="2.337598144531" Y="26.984345703125" />
                  <Point X="2.364965576172" Y="26.981046875" />
                  <Point X="2.407022949219" Y="26.975974609375" />
                  <Point X="2.416041992188" Y="26.9753203125" />
                  <Point X="2.447581298828" Y="26.97549609375" />
                  <Point X="2.484323730469" Y="26.97982421875" />
                  <Point X="2.497755859375" Y="26.9823984375" />
                  <Point X="2.529404785156" Y="26.99086328125" />
                  <Point X="2.578042236328" Y="27.003869140625" />
                  <Point X="2.583993164062" Y="27.005669921875" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627661132812" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.540118408203" Y="27.54984765625" />
                  <Point X="3.940403808594" Y="27.780953125" />
                  <Point X="4.043952392578" Y="27.63704296875" />
                  <Point X="4.121447265625" Y="27.508982421875" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.620138427734" Y="27.086958984375" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116210938" Y="26.725212890625" />
                  <Point X="3.134668212891" Y="26.7066015625" />
                  <Point X="3.128577392578" Y="26.699421875" />
                  <Point X="3.105799560547" Y="26.66970703125" />
                  <Point X="3.070795166016" Y="26.624041015625" />
                  <Point X="3.065636230469" Y="26.6166015625" />
                  <Point X="3.049740478516" Y="26.589373046875" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030139892578" Y="26.542671875" />
                  <Point X="3.021655029297" Y="26.51233203125" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.011664550781" Y="26.318814453125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682373047" Y="26.228611328125" />
                  <Point X="3.050285400391" Y="26.19537109375" />
                  <Point X="3.056918212891" Y="26.1835234375" />
                  <Point X="3.075862548828" Y="26.154728515625" />
                  <Point X="3.104975830078" Y="26.110478515625" />
                  <Point X="3.111740722656" Y="26.101419921875" />
                  <Point X="3.126296142578" Y="26.084173828125" />
                  <Point X="3.134086669922" Y="26.075986328125" />
                  <Point X="3.151329345703" Y="26.0598984375" />
                  <Point X="3.160038085938" Y="26.052693359375" />
                  <Point X="3.178247070312" Y="26.039369140625" />
                  <Point X="3.187747314453" Y="26.03325" />
                  <Point X="3.215200195312" Y="26.017796875" />
                  <Point X="3.257389404297" Y="25.994046875" />
                  <Point X="3.265481689453" Y="25.989986328125" />
                  <Point X="3.294676757813" Y="25.978083984375" />
                  <Point X="3.330277587891" Y="25.96801953125" />
                  <Point X="3.343677734375" Y="25.965255859375" />
                  <Point X="3.380795898438" Y="25.9603515625" />
                  <Point X="3.437838378906" Y="25.9528125" />
                  <Point X="3.444033935547" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.359423339844" Y="26.065861328125" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.777104492188" Y="25.757380859375" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.204477539062" Y="25.558673828125" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686022949219" Y="25.419541015625" />
                  <Point X="3.665624267578" Y="25.41213671875" />
                  <Point X="3.642384521484" Y="25.40162109375" />
                  <Point X="3.634008056641" Y="25.397318359375" />
                  <Point X="3.597540527344" Y="25.376240234375" />
                  <Point X="3.541498046875" Y="25.34384765625" />
                  <Point X="3.533883056641" Y="25.338947265625" />
                  <Point X="3.508771728516" Y="25.319869140625" />
                  <Point X="3.481992431641" Y="25.294349609375" />
                  <Point X="3.472796386719" Y="25.2842265625" />
                  <Point X="3.450916015625" Y="25.256345703125" />
                  <Point X="3.417290527344" Y="25.213498046875" />
                  <Point X="3.410854248047" Y="25.20420703125" />
                  <Point X="3.399130615234" Y="25.184927734375" />
                  <Point X="3.393843261719" Y="25.174939453125" />
                  <Point X="3.384069580078" Y="25.1534765625" />
                  <Point X="3.380005371094" Y="25.142927734375" />
                  <Point X="3.373158935547" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.363083007812" Y="25.072388671875" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.359168457031" Y="24.8854921875" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.81601171875" />
                  <Point X="3.380005859375" Y="24.79451171875" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399128417969" Y="24.752515625" />
                  <Point X="3.410852294922" Y="24.733234375" />
                  <Point X="3.417290527344" Y="24.72394140625" />
                  <Point X="3.439170898438" Y="24.696060546875" />
                  <Point X="3.472796386719" Y="24.653212890625" />
                  <Point X="3.47871875" Y="24.646365234375" />
                  <Point X="3.50112890625" Y="24.624205078125" />
                  <Point X="3.530171875" Y="24.60127734375" />
                  <Point X="3.541496826172" Y="24.593591796875" />
                  <Point X="3.577964355469" Y="24.572513671875" />
                  <Point X="3.634006835938" Y="24.540119140625" />
                  <Point X="3.639498535156" Y="24.537181640625" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.479568847656" Y="24.3050546875" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.06894921875" />
                  <Point X="4.730326171875" Y="23.93183984375" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.031121337891" Y="24.0125" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354481445312" Y="24.087322265625" />
                  <Point X="3.282908691406" Y="24.071765625" />
                  <Point X="3.172917236328" Y="24.047859375" />
                  <Point X="3.157873535156" Y="24.0432578125" />
                  <Point X="3.128752685547" Y="24.0316328125" />
                  <Point X="3.114675537109" Y="24.024609375" />
                  <Point X="3.086848876953" Y="24.007716796875" />
                  <Point X="3.074124755859" Y="23.99846875" />
                  <Point X="3.050374267578" Y="23.977998046875" />
                  <Point X="3.039347900391" Y="23.966775390625" />
                  <Point X="2.996086669922" Y="23.914744140625" />
                  <Point X="2.929603759766" Y="23.834787109375" />
                  <Point X="2.921324462891" Y="23.823150390625" />
                  <Point X="2.906605224609" Y="23.79876953125" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.868956787109" Y="23.635958984375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.936150146484" Y="23.319017578125" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.783671630859" Y="22.6028984375" />
                  <Point X="4.087169433594" Y="22.370015625" />
                  <Point X="4.045490722656" Y="22.30257421875" />
                  <Point X="4.001273193359" Y="22.23974609375" />
                  <Point X="3.377793945312" Y="22.599712890625" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841198730469" Y="22.909109375" />
                  <Point X="2.815021484375" Y="22.92048828125" />
                  <Point X="2.783117919922" Y="22.930673828125" />
                  <Point X="2.771109375" Y="22.933662109375" />
                  <Point X="2.685926269531" Y="22.949046875" />
                  <Point X="2.555019042969" Y="22.9726875" />
                  <Point X="2.539359619141" Y="22.97419140625" />
                  <Point X="2.508008300781" Y="22.974595703125" />
                  <Point X="2.49231640625" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444847167969" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400588867188" Y="22.948890625" />
                  <Point X="2.329822509766" Y="22.911646484375" />
                  <Point X="2.221070800781" Y="22.854412109375" />
                  <Point X="2.208970214844" Y="22.846830078125" />
                  <Point X="2.186041748047" Y="22.829939453125" />
                  <Point X="2.175213867187" Y="22.820630859375" />
                  <Point X="2.154253662109" Y="22.799671875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128045898438" Y="22.76590625" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.083220458984" Y="22.6830390625" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.017571289062" Y="22.33467578125" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.539208007812" Y="21.312943359375" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723753173828" Y="20.963916015625" />
                  <Point X="2.237849609375" Y="21.597158203125" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828654663086" Y="22.1298515625" />
                  <Point X="1.808836914062" Y="22.1503671875" />
                  <Point X="1.783252929688" Y="22.171994140625" />
                  <Point X="1.773297363281" Y="22.179353515625" />
                  <Point X="1.689284057617" Y="22.233365234375" />
                  <Point X="1.560174072266" Y="22.31637109375" />
                  <Point X="1.54628503418" Y="22.323751953125" />
                  <Point X="1.517471801758" Y="22.336125" />
                  <Point X="1.502547485352" Y="22.3411171875" />
                  <Point X="1.470927001953" Y="22.34884765625" />
                  <Point X="1.455385131836" Y="22.3513046875" />
                  <Point X="1.424119262695" Y="22.35362109375" />
                  <Point X="1.408395263672" Y="22.35348046875" />
                  <Point X="1.316512084961" Y="22.345025390625" />
                  <Point X="1.175308227539" Y="22.332033203125" />
                  <Point X="1.161227539062" Y="22.32966015625" />
                  <Point X="1.133582397461" Y="22.322830078125" />
                  <Point X="1.120017822266" Y="22.318373046875" />
                  <Point X="1.092632324219" Y="22.30703125" />
                  <Point X="1.079885253906" Y="22.300591796875" />
                  <Point X="1.055497314453" Y="22.285869140625" />
                  <Point X="1.043856323242" Y="22.2775859375" />
                  <Point X="0.972906799316" Y="22.218591796875" />
                  <Point X="0.86387286377" Y="22.12793359375" />
                  <Point X="0.852649597168" Y="22.116908203125" />
                  <Point X="0.832178710938" Y="22.09315625" />
                  <Point X="0.822930969238" Y="22.0804296875" />
                  <Point X="0.806038635254" Y="22.0526015625" />
                  <Point X="0.799017822266" Y="22.03852734375" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.761578369141" Y="21.89676953125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091247559" Y="20.84766015625" />
                  <Point X="0.81297442627" Y="20.92273828125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652606567383" Y="21.519875" />
                  <Point X="0.642145629883" Y="21.546419921875" />
                  <Point X="0.626787353516" Y="21.576185546875" />
                  <Point X="0.620407592773" Y="21.586791015625" />
                  <Point X="0.555866149902" Y="21.679783203125" />
                  <Point X="0.456679992676" Y="21.82269140625" />
                  <Point X="0.446669036865" Y="21.834830078125" />
                  <Point X="0.42478237915" Y="21.857287109375" />
                  <Point X="0.412906524658" Y="21.867607421875" />
                  <Point X="0.386651550293" Y="21.886849609375" />
                  <Point X="0.373235351562" Y="21.89506640625" />
                  <Point X="0.345235839844" Y="21.909171875" />
                  <Point X="0.330652832031" Y="21.915060546875" />
                  <Point X="0.230778533936" Y="21.946056640625" />
                  <Point X="0.07729372406" Y="21.993693359375" />
                  <Point X="0.063380096436" Y="21.996888671875" />
                  <Point X="0.03522794342" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022895795822" Y="22.001162109375" />
                  <Point X="-0.051062213898" Y="21.996892578125" />
                  <Point X="-0.064984611511" Y="21.9936953125" />
                  <Point X="-0.164858901978" Y="21.962697265625" />
                  <Point X="-0.318343719482" Y="21.9150625" />
                  <Point X="-0.332928375244" Y="21.909171875" />
                  <Point X="-0.360929077148" Y="21.895064453125" />
                  <Point X="-0.374345123291" Y="21.88684765625" />
                  <Point X="-0.400600524902" Y="21.867603515625" />
                  <Point X="-0.412474761963" Y="21.85728515625" />
                  <Point X="-0.434358581543" Y="21.834830078125" />
                  <Point X="-0.444368041992" Y="21.822693359375" />
                  <Point X="-0.508909637451" Y="21.729701171875" />
                  <Point X="-0.60809564209" Y="21.58679296875" />
                  <Point X="-0.612471252441" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.885424804688" Y="20.606400390625" />
                  <Point X="-0.985424987793" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.75260341899" Y="29.478392371478" />
                  <Point X="0.36174689643" Y="29.723637902699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121771709388" Y="29.316832513329" />
                  <Point X="0.337065154176" Y="29.63152443166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.357572496632" Y="29.754007791314" />
                  <Point X="-0.457946654969" Y="29.771706463595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.395613730812" Y="29.172081248371" />
                  <Point X="0.312383460967" Y="29.539410951974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.330442868801" Y="29.652758577822" />
                  <Point X="-0.786768035257" Y="29.733221016644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.632178655547" Y="29.033902941322" />
                  <Point X="0.287701767758" Y="29.447297472287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.30331324097" Y="29.551509364329" />
                  <Point X="-1.049634466953" Y="29.683105932745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.812548690291" Y="28.905633309556" />
                  <Point X="0.263020074548" Y="29.3551839926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276183653341" Y="29.450260157926" />
                  <Point X="-1.260864655877" Y="29.623885986064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.768695795526" Y="28.816900229956" />
                  <Point X="0.238338381339" Y="29.263070512913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.249054068015" Y="29.349010951928" />
                  <Point X="-1.472092352713" Y="29.56466559996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718147342151" Y="28.72934775799" />
                  <Point X="0.191582642349" Y="29.174849283071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.220584868126" Y="29.247525535739" />
                  <Point X="-1.466705887564" Y="29.467250292694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667598888776" Y="28.641795286024" />
                  <Point X="0.079607436811" Y="29.098128004849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.142999098733" Y="29.137379543147" />
                  <Point X="-1.465906380955" Y="29.370643789978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617050435401" Y="28.554242814058" />
                  <Point X="-1.491825334221" Y="29.278748472622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.566501982027" Y="28.466690342092" />
                  <Point X="-1.527808121312" Y="29.188627680698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.515953528652" Y="28.379137870126" />
                  <Point X="-1.607911587718" Y="29.106286554944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.140233556116" Y="29.200149280397" />
                  <Point X="-2.27303443305" Y="29.223565658062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.465405075277" Y="28.29158539816" />
                  <Point X="-1.768500644012" Y="29.038137210246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.982915441623" Y="29.075944324128" />
                  <Point X="-2.404835488294" Y="29.150340212058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.414856621902" Y="28.204032926194" />
                  <Point X="-2.536636150568" Y="29.077114696763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.364308196543" Y="28.116480449288" />
                  <Point X="-2.66394469968" Y="29.003097100718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972416798411" Y="27.73646198674" />
                  <Point X="3.888861442031" Y="27.751195050453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313759778002" Y="28.02892797118" />
                  <Point X="-2.766238808963" Y="28.924668784023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.05049643763" Y="27.626228911573" />
                  <Point X="3.7608685365" Y="27.677298124908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26321135946" Y="27.941375493072" />
                  <Point X="-2.868532918246" Y="28.846240467328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.115844672486" Y="27.518240726497" />
                  <Point X="3.632875630969" Y="27.603401199363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212662940919" Y="27.853823014963" />
                  <Point X="-2.97082702753" Y="28.767812150632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.067573693975" Y="27.430286674264" />
                  <Point X="3.504882617936" Y="27.529504292774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162114522377" Y="27.766270536855" />
                  <Point X="-3.051060662349" Y="28.685493977082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965347808109" Y="27.35184632794" />
                  <Point X="3.376889321907" Y="27.455607436084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.111566103836" Y="27.678718058747" />
                  <Point X="-2.989053227843" Y="28.578094865245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.863121922242" Y="27.273405981616" />
                  <Point X="3.248896025877" Y="27.381710579395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061432203022" Y="27.59109248998" />
                  <Point X="-2.92704640821" Y="28.470695861827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.760896036375" Y="27.194965635292" />
                  <Point X="3.120902729847" Y="27.307813722706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030467976459" Y="27.50008679043" />
                  <Point X="-2.865039588578" Y="28.363296858409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.658670150508" Y="27.116525288968" />
                  <Point X="2.992909433817" Y="27.233916866017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013319750146" Y="27.406644957271" />
                  <Point X="-2.803032768945" Y="28.25589785499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.556444515334" Y="27.03808489844" />
                  <Point X="2.864916137787" Y="27.160020009327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020731322942" Y="27.308872568889" />
                  <Point X="-2.760063322333" Y="28.151855654077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.454219031817" Y="26.959644481171" />
                  <Point X="2.736922841757" Y="27.086123152638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.054316270157" Y="27.20648510842" />
                  <Point X="-2.749597537699" Y="28.053544725743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351993548299" Y="26.881204063902" />
                  <Point X="2.604324488158" Y="27.013038291846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.135266186429" Y="27.095745925966" />
                  <Point X="-2.757594209423" Y="27.958489226595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.249768064781" Y="26.802763646633" />
                  <Point X="-2.804235602137" Y="27.870247834419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.72091563881" Y="28.031883257561" />
                  <Point X="-3.727825222745" Y="28.033101603634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150751678309" Y="26.723757378971" />
                  <Point X="-2.884595286403" Y="27.787951886787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480368582079" Y="27.8930027932" />
                  <Point X="-3.793821330538" Y="27.948272969931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.082510507806" Y="26.639324610397" />
                  <Point X="-3.066583392608" Y="27.72357577195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234696911901" Y="27.753218721223" />
                  <Point X="-3.859817283052" Y="27.863444308847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.033321784795" Y="26.551532381281" />
                  <Point X="-3.925812456945" Y="27.778615510472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007305815657" Y="26.45965417044" />
                  <Point X="-3.991807630839" Y="27.693786712097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.716768236186" Y="26.061764295065" />
                  <Point X="4.550707044626" Y="26.091045363585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003062562472" Y="26.363936842334" />
                  <Point X="-4.04523452764" Y="27.60674178737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.741305510708" Y="25.960972183404" />
                  <Point X="4.237489234337" Y="26.049808586249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023138400286" Y="26.263931402338" />
                  <Point X="-4.096286463222" Y="27.519278092901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.760965293203" Y="25.861040105186" />
                  <Point X="3.924267032951" Y="26.008572583181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073301857371" Y="26.158620703279" />
                  <Point X="-4.147338398803" Y="27.431814398432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.776408431506" Y="25.761851535108" />
                  <Point X="3.611044831565" Y="25.967336580113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.170538623127" Y="26.04500970983" />
                  <Point X="-4.198390334384" Y="27.344350703963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.671658425578" Y="25.683856259253" />
                  <Point X="-4.157828809928" Y="27.240733084694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.454530547525" Y="25.625676234289" />
                  <Point X="-3.99460354281" Y="27.115486538038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.237402669472" Y="25.567496209324" />
                  <Point X="-3.831379509468" Y="26.990240208931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020273462402" Y="25.509316418701" />
                  <Point X="-3.668155476126" Y="26.864993879824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.803144017778" Y="25.451136669966" />
                  <Point X="-3.514498429584" Y="26.741434468613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617007426462" Y="25.387492044983" />
                  <Point X="-3.447764481763" Y="26.633201944954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.500131080129" Y="25.311634970119" />
                  <Point X="-3.421425125582" Y="26.532092077676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.428512471299" Y="25.227797735047" />
                  <Point X="-3.436242155294" Y="26.438239191659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.379088132967" Y="25.140047051269" />
                  <Point X="-3.475242186487" Y="26.348650421277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358269204671" Y="25.047252461908" />
                  <Point X="-3.550492511798" Y="26.265453555807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.349574427785" Y="24.952320057535" />
                  <Point X="-3.751701249832" Y="26.204466556948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365378198048" Y="24.853067898312" />
                  <Point X="-4.647741842774" Y="26.265997161165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40039814889" Y="24.750427407986" />
                  <Point X="-4.672690249015" Y="26.173930710182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.486365835369" Y="24.638803457262" />
                  <Point X="-4.697638554279" Y="26.081864241393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801177025867" Y="24.486828222419" />
                  <Point X="-4.722586856577" Y="25.989797772082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783946127519" Y="24.217073985862" />
                  <Point X="-4.740366854419" Y="25.89646733729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.769779216458" Y="24.123106466386" />
                  <Point X="-4.754278297564" Y="25.802454771927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752648874135" Y="24.029661479797" />
                  <Point X="-4.768189740709" Y="25.708442206565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.731487128023" Y="23.936927338467" />
                  <Point X="-4.782101183854" Y="25.614429641203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.344175642664" Y="24.085082255853" />
                  <Point X="-3.985720309381" Y="25.377540677984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121301826728" Y="24.027915394767" />
                  <Point X="-3.381204423591" Y="25.174482688923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024210780416" Y="23.948569637688" />
                  <Point X="-3.312933627732" Y="25.06597917749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954258461663" Y="23.864438590718" />
                  <Point X="-3.294023046694" Y="24.966179203703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.896878518894" Y="23.77809069465" />
                  <Point X="-3.312291552622" Y="24.872934906066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.873537746423" Y="23.685740774458" />
                  <Point X="-3.360085417716" Y="24.784896725865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.864802757833" Y="23.590815460494" />
                  <Point X="-3.462875781221" Y="24.706555912179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860893458824" Y="23.495039247256" />
                  <Point X="-3.665235967761" Y="24.645771944758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.889702486745" Y="23.393493910216" />
                  <Point X="-3.882365087569" Y="24.587592138748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.958033506764" Y="23.284979779638" />
                  <Point X="-4.099494207971" Y="24.529412332844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.041583101386" Y="23.17378220375" />
                  <Point X="-4.316623348276" Y="24.471232530448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.202340180112" Y="23.048970865302" />
                  <Point X="-4.53375248858" Y="24.413052728053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.365563891458" Y="22.923724592971" />
                  <Point X="-4.750881628885" Y="24.354872925658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.528787602803" Y="22.79847832064" />
                  <Point X="2.788112016945" Y="22.929079410379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.531709697177" Y="22.97429005727" />
                  <Point X="-4.774617018709" Y="24.262592587152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.692011314149" Y="22.673232048309" />
                  <Point X="3.056304567811" Y="22.785324299507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.332350465451" Y="22.912976940548" />
                  <Point X="-3.093285262409" Y="23.869662906865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.255715238689" Y="23.898303694159" />
                  <Point X="-4.760463327219" Y="24.163631381337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.855234955802" Y="22.547985788267" />
                  <Point X="3.29685407507" Y="22.646443403052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.199590737061" Y="22.839920534485" />
                  <Point X="-2.971788788169" Y="23.751774272267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568936349768" Y="23.857067498841" />
                  <Point X="-4.746309718421" Y="24.064670190102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01845850819" Y="22.422739543965" />
                  <Point X="3.5374023119" Y="22.507562730608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.122496581474" Y="22.757048786041" />
                  <Point X="-2.948879056294" Y="23.651269140287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.882157460847" Y="23.815831303522" />
                  <Point X="-4.725073405327" Y="23.964460127004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056041730914" Y="22.319647079648" />
                  <Point X="3.777949904481" Y="22.368682171762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.075741015895" Y="22.668827525622" />
                  <Point X="-2.961705034677" Y="23.557065178201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.195378571926" Y="23.774595108204" />
                  <Point X="-4.699270902842" Y="23.863444921516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029283096124" Y="22.580553782216" />
                  <Point X="-3.007899607414" Y="23.468744999608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.508601908682" Y="23.733359305332" />
                  <Point X="-4.673468400356" Y="23.762429716029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002346274606" Y="22.488837942495" />
                  <Point X="-3.097656907752" Y="23.388106105244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.007309745432" Y="22.391497220541" />
                  <Point X="-3.199882660057" Y="23.309665735369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.025304015211" Y="22.291858817152" />
                  <Point X="-3.302108412363" Y="23.231225365495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.044129236331" Y="22.192073894621" />
                  <Point X="1.643681188288" Y="22.262683689863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.224638996367" Y="22.336572134354" />
                  <Point X="-3.404334164668" Y="23.15278499562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092232769671" Y="22.087126415697" />
                  <Point X="1.824287637162" Y="22.134372371908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.03829733454" Y="22.272963668835" />
                  <Point X="-3.506559916973" Y="23.074344625746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.154239684207" Y="21.979727395545" />
                  <Point X="1.911443418087" Y="22.022538928077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.942579651316" Y="22.193375750789" />
                  <Point X="-3.608785549846" Y="22.995904234812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.216246598744" Y="21.872328375392" />
                  <Point X="1.997046127649" Y="21.91097933263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.849551538709" Y="22.113313588877" />
                  <Point X="-3.711011043535" Y="22.917463819336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.27825351328" Y="21.76492935524" />
                  <Point X="2.082648837211" Y="21.799419737183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794255798645" Y="22.02659819164" />
                  <Point X="-2.406445455577" Y="22.590968179946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.654202064103" Y="22.634654354678" />
                  <Point X="-3.813236537223" Y="22.839023403861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.340260427816" Y="21.657530335087" />
                  <Point X="2.168251546773" Y="21.687860141737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769768584973" Y="21.934450419963" />
                  <Point X="-2.32700791773" Y="22.480495670614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.789056342319" Y="22.561967274263" />
                  <Point X="-3.915462030911" Y="22.760582988385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.402267342352" Y="21.550131314935" />
                  <Point X="2.253854242001" Y="21.576300548818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.74957515623" Y="21.841545538155" />
                  <Point X="0.328024200873" Y="21.915876345327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.08249221712" Y="21.988261465843" />
                  <Point X="-2.311620796529" Y="22.381316977861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.917049400826" Y="22.488070375692" />
                  <Point X="-4.017687524599" Y="22.682142572909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464274256889" Y="21.442732294783" />
                  <Point X="2.339456874891" Y="21.46474096689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729381809326" Y="21.748640641915" />
                  <Point X="0.477214960899" Y="21.793104460933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.280700195775" Y="21.926745352143" />
                  <Point X="-2.335273571943" Y="22.289022072206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.045042459332" Y="22.414173477121" />
                  <Point X="-4.119913018287" Y="22.603702157434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.526281171425" Y="21.33533327463" />
                  <Point X="2.425059507782" Y="21.353181384963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.727122575169" Y="21.652573477724" />
                  <Point X="0.553503597137" Y="21.683187187914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.415610010831" Y="21.85406806437" />
                  <Point X="-2.384576833001" Y="22.201250039239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.173035517839" Y="22.34027657855" />
                  <Point X="-4.15556031814" Y="22.513522210058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.588288187282" Y="21.227934236612" />
                  <Point X="2.510662140673" Y="21.241621803035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.740124301814" Y="21.553815394391" />
                  <Point X="0.628141564667" Y="21.573560972324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.48148301773" Y="21.769217724658" />
                  <Point X="-2.435125212643" Y="22.113697554272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.301028576345" Y="22.266379679979" />
                  <Point X="-4.091272063375" Y="22.405720928071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650295229825" Y="21.120535193889" />
                  <Point X="2.596264773563" Y="21.130062221108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.753126028458" Y="21.455057311059" />
                  <Point X="0.666234630627" Y="21.470378608888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.541135056691" Y="21.683270460452" />
                  <Point X="-2.485673592284" Y="22.026145069304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429021634852" Y="22.192482781407" />
                  <Point X="-4.026984285677" Y="22.297919730204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.712302272369" Y="21.013136151165" />
                  <Point X="2.681867406454" Y="21.01850263918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766127755103" Y="21.356299227727" />
                  <Point X="0.693364278817" Y="21.369129391806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.600787095937" Y="21.597323196296" />
                  <Point X="-2.536221971926" Y="21.938592584337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557014522794" Y="22.118585852761" />
                  <Point X="-3.949506346809" Y="22.187792751043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779129481748" Y="21.257541144394" />
                  <Point X="0.720493927008" Y="21.267880174724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.643727068344" Y="21.508429143853" />
                  <Point X="-2.586770351568" Y="21.85104009937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.685007336596" Y="22.044688911042" />
                  <Point X="-3.864698989119" Y="22.07637339759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.792131208392" Y="21.158783061062" />
                  <Point X="0.747623575198" Y="21.166630957642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.66840878545" Y="21.41631566838" />
                  <Point X="-2.63731873028" Y="21.763487614239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.805132935037" Y="21.060024977729" />
                  <Point X="0.774753223388" Y="21.065381740559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.693090502556" Y="21.324202192907" />
                  <Point X="-2.687867090123" Y="21.675935125781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818134661682" Y="20.961266894397" />
                  <Point X="0.801882871578" Y="20.964132523477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.717772219661" Y="21.232088717434" />
                  <Point X="-2.738415449966" Y="21.588382637322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831136388327" Y="20.862508811065" />
                  <Point X="0.829012265948" Y="20.86288335115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.742453936767" Y="21.13997524196" />
                  <Point X="-2.788963809809" Y="21.500830148864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.767135653873" Y="21.047861766487" />
                  <Point X="-1.582082897334" Y="21.191558953363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.660466393431" Y="21.205380078568" />
                  <Point X="-2.839512169652" Y="21.413277660406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.791817370979" Y="20.955748291014" />
                  <Point X="-1.401244407633" Y="21.063206720349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.09349388931" Y="21.18526898135" />
                  <Point X="-2.890060529495" Y="21.325725171948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.816499088085" Y="20.863634815541" />
                  <Point X="-1.263563642274" Y="20.942464358563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.207980180799" Y="21.108990475332" />
                  <Point X="-2.940608889338" Y="21.238172683489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.841180805191" Y="20.771521340068" />
                  <Point X="-1.192626251877" Y="20.833490654566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322207643764" Y="21.032666330862" />
                  <Point X="-2.947767766467" Y="21.142969458549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.865862522297" Y="20.679407864595" />
                  <Point X="-1.16920047395" Y="20.732894529744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.414935250932" Y="20.952551181733" />
                  <Point X="-2.785268034522" Y="21.017850843321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.890544251213" Y="20.587294391204" />
                  <Point X="-1.149314822539" Y="20.632922624742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.480134726879" Y="20.867582080341" />
                  <Point X="-2.580265373804" Y="20.88523781499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915226025256" Y="20.49518092577" />
                  <Point X="-1.129429214634" Y="20.532950727411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.939907799298" Y="20.403067460337" />
                  <Point X="-1.120009868131" Y="20.434824314353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.964589573341" Y="20.310953994903" />
                  <Point X="-1.131797876047" Y="20.340437330068" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.629448486328" Y="20.8735625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318664551" Y="21.47845703125" />
                  <Point X="0.399777099609" Y="21.57144921875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.174461669922" Y="21.764595703125" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.108538841248" Y="21.781236328125" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.352820739746" Y="21.621365234375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.701898864746" Y="20.557224609375" />
                  <Point X="-0.847744140625" Y="20.012921875" />
                  <Point X="-0.920139831543" Y="20.026974609375" />
                  <Point X="-1.10023059082" Y="20.061931640625" />
                  <Point X="-1.215351074219" Y="20.09155078125" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.328158813477" Y="20.304576171875" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.333543212891" Y="20.585193359375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.47823425293" Y="20.878009765625" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.771280883789" Y="21.022234375" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.091569091797" Y="20.95826171875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.395975585938" Y="20.66515234375" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.591835449219" Y="20.668927734375" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.015221435547" Y="20.955111328125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.799490722656" Y="21.86259765625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979736328" Y="22.431236328125" />
                  <Point X="-2.531328369141" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.372800537109" Y="22.00555078125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.953130371094" Y="21.87884765625" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.275971679688" Y="22.34448046875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.677241210938" Y="23.1828671875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326660156" Y="23.66540625" />
                  <Point X="-3.161160888672" Y="23.689361328125" />
                  <Point X="-3.187643554688" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.205615722656" Y="23.581607421875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.845818359375" Y="23.669447265625" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.957625488281" Y="24.200189453125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.142365722656" Y="24.71462890625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.530957275391" Y="24.88616796875" />
                  <Point X="-3.51414453125" Y="24.8978359375" />
                  <Point X="-3.502324951172" Y="24.909353515625" />
                  <Point X="-3.491252197266" Y="24.935841796875" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.489294189453" Y="24.9952890625" />
                  <Point X="-3.502325195312" Y="25.028087890625" />
                  <Point X="-3.525086669922" Y="25.047197265625" />
                  <Point X="-3.541894287109" Y="25.05886328125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.456328125" Y="25.306935546875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.97021875" Y="25.641130859375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.856787597656" Y="26.221" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.172538574219" Y="26.449177734375" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731705322266" Y="26.3958671875" />
                  <Point X="-3.707491210938" Y="26.403501953125" />
                  <Point X="-3.670279296875" Y="26.415234375" />
                  <Point X="-3.651534912109" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.629404052734" Y="26.4672421875" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.61631640625" Y="26.54551171875" />
                  <Point X="-3.628039794922" Y="26.568033203125" />
                  <Point X="-3.646056152344" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.175559082031" Y="27.01484765625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.364296386719" Y="27.437021484375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.998806152344" Y="27.994216796875" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.416662109375" Y="28.07561328125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138513916016" Y="27.92043359375" />
                  <Point X="-3.104790283203" Y="27.917482421875" />
                  <Point X="-3.052964599609" Y="27.91294921875" />
                  <Point X="-3.031506591797" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.989315673828" Y="27.951341796875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941024658203" Y="28.061564453125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.180541259766" Y="28.52976171875" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.108669677734" Y="28.901546875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.498978027344" Y="29.315392578125" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.046656860352" Y="29.390306640625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246582031" Y="29.273662109375" />
                  <Point X="-1.913712524414" Y="29.25412109375" />
                  <Point X="-1.856030639648" Y="29.22409375" />
                  <Point X="-1.835123779297" Y="29.218490234375" />
                  <Point X="-1.813808349609" Y="29.22225" />
                  <Point X="-1.774713867188" Y="29.238443359375" />
                  <Point X="-1.714634399414" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.673358886719" Y="29.334845703125" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.677892578125" Y="29.615724609375" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.428691040039" Y="29.77416015625" />
                  <Point X="-0.968083007812" Y="29.903296875" />
                  <Point X="-0.660293701172" Y="29.9393203125" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.118317489624" Y="29.59519921875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282114029" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594032288" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.171519744873" Y="29.7478046875" />
                  <Point X="0.236648361206" Y="29.9908671875" />
                  <Point X="0.458036865234" Y="29.967681640625" />
                  <Point X="0.860210266113" Y="29.925564453125" />
                  <Point X="1.114858032227" Y="29.864083984375" />
                  <Point X="1.508456298828" Y="29.769056640625" />
                  <Point X="1.674284423828" Y="29.70891015625" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="2.091310791016" Y="29.540830078125" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.493536376953" Y="29.33492578125" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.878552734375" Y="29.091841796875" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.5695078125" Y="28.091896484375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.216388671875" Y="27.459865234375" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.205344726562" Y="27.36495703125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.235616455078" Y="27.27585546875" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.299895019531" Y="27.207271484375" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.3603359375" Y="27.172978515625" />
                  <Point X="2.387703369141" Y="27.1696796875" />
                  <Point X="2.429760742188" Y="27.164607421875" />
                  <Point X="2.4486640625" Y="27.1659453125" />
                  <Point X="2.480312988281" Y="27.17441015625" />
                  <Point X="2.528950439453" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.445118408203" Y="27.714390625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.060832519531" Y="27.938892578125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.284000976562" Y="27.607349609375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.735802978516" Y="26.936220703125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.256593505859" Y="26.5541171875" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.204634521484" Y="26.46116015625" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.197744628906" Y="26.357208984375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.234591064453" Y="26.259158203125" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947021484" Y="26.1988203125" />
                  <Point X="3.308399902344" Y="26.1833671875" />
                  <Point X="3.350589111328" Y="26.1596171875" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.40568359375" Y="26.14871484375" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.334623535156" Y="26.254236328125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.878303710938" Y="26.20148046875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.964842773438" Y="25.786611328125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.253653320312" Y="25.375146484375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087646484" Y="25.232818359375" />
                  <Point X="3.692620117188" Y="25.211740234375" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.600384277344" Y="25.139044921875" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.54969140625" Y="25.036650390625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.545776855469" Y="24.92123046875" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.588639160156" Y="24.813361328125" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636576416016" Y="24.758091796875" />
                  <Point X="3.673043945312" Y="24.737013671875" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.528744628906" Y="24.48858203125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.982427246094" Y="24.259083984375" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.915564453125" Y="23.8895703125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.006321533203" Y="23.824125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.323263671875" Y="23.8861015625" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.142184326172" Y="23.793271484375" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.058157470703" Y="23.618548828125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.095969970703" Y="23.421767578125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.899336669922" Y="22.75363671875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.299961425781" Y="22.35292578125" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.136159179687" Y="22.101279296875" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.282793945312" Y="22.435169921875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.746685546875" />
                  <Point X="2.652157226562" Y="22.7620703125" />
                  <Point X="2.52125" Y="22.7857109375" />
                  <Point X="2.489078125" Y="22.78075390625" />
                  <Point X="2.418311767578" Y="22.743509765625" />
                  <Point X="2.309560058594" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.251356201172" Y="22.59455078125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.204546875" Y="22.36844140625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.703752929688" Y="21.407943359375" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.949008789062" Y="20.8910078125" />
                  <Point X="2.835296142578" Y="20.80978515625" />
                  <Point X="2.759301757812" Y="20.760595703125" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.087112304688" Y="21.481494140625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.586535766602" Y="22.073544921875" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425805297852" Y="22.16428125" />
                  <Point X="1.333922241211" Y="22.155826171875" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.1314921875" />
                  <Point X="1.094383056641" Y="22.072498046875" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.947243225098" Y="21.8564140625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.047027954102" Y="20.678296875" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="1.101924316406" Y="20.060333984375" />
                  <Point X="0.994362365723" Y="20.036755859375" />
                  <Point X="0.92413885498" Y="20.023998046875" />
                  <Point X="0.860200500488" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#156" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.076177159468" Y="4.639421517591" Z="1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="-0.672261821919" Y="5.020261058313" Z="1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1" />
                  <Point X="-1.448344850338" Y="4.853584779719" Z="1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1" />
                  <Point X="-1.733620792968" Y="4.640479712658" Z="1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1" />
                  <Point X="-1.72720063896" Y="4.381160982191" Z="1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1" />
                  <Point X="-1.800004618146" Y="4.315918158695" Z="1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1" />
                  <Point X="-1.896780920762" Y="4.329752074116" Z="1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1" />
                  <Point X="-2.013145325707" Y="4.452024786082" Z="1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1" />
                  <Point X="-2.529416961224" Y="4.390379290713" Z="1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1" />
                  <Point X="-3.145247878103" Y="3.972508051775" Z="1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1" />
                  <Point X="-3.229998571226" Y="3.536041263206" Z="1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1" />
                  <Point X="-2.996990476967" Y="3.088487178959" Z="1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1" />
                  <Point X="-3.030826286637" Y="3.017977281249" Z="1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1" />
                  <Point X="-3.106589286648" Y="2.998574222206" Z="1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1" />
                  <Point X="-3.397818085091" Y="3.150195377519" Z="1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1" />
                  <Point X="-4.044425469262" Y="3.056199602745" Z="1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1" />
                  <Point X="-4.413634280671" Y="2.493507941984" Z="1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1" />
                  <Point X="-4.212153064841" Y="2.006460793445" Z="1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1" />
                  <Point X="-3.678545908584" Y="1.576224970101" Z="1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1" />
                  <Point X="-3.681753853843" Y="1.517656741093" Z="1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1" />
                  <Point X="-3.728681828406" Y="1.482467248474" Z="1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1" />
                  <Point X="-4.172167965227" Y="1.53003074358" Z="1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1" />
                  <Point X="-4.911203439858" Y="1.265358285965" Z="1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1" />
                  <Point X="-5.025835949818" Y="0.679730609772" Z="1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1" />
                  <Point X="-4.475425193671" Y="0.289919041395" Z="1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1" />
                  <Point X="-3.559748065268" Y="0.037400078677" Z="1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1" />
                  <Point X="-3.543203539653" Y="0.011749923755" Z="1" />
                  <Point X="-3.539556741714" Y="0" Z="1" />
                  <Point X="-3.545161061482" Y="-0.018057027306" Z="1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1" />
                  <Point X="-3.565620529855" Y="-0.041475904457" Z="1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1" />
                  <Point X="-4.161462595102" Y="-0.205793015361" Z="1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1" />
                  <Point X="-5.013277458387" Y="-0.775608828104" Z="1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1" />
                  <Point X="-4.900419393092" Y="-1.311646472614" Z="1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1" />
                  <Point X="-4.205245176039" Y="-1.436684005725" Z="1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1" />
                  <Point X="-3.203115760641" Y="-1.316305594501" Z="1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1" />
                  <Point X="-3.197343683568" Y="-1.340470734625" Z="1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1" />
                  <Point X="-3.713835139944" Y="-1.746184524107" Z="1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1" />
                  <Point X="-4.325070834337" Y="-2.649849094522" Z="1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1" />
                  <Point X="-3.999072283982" Y="-3.120155125946" Z="1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1" />
                  <Point X="-3.353956623328" Y="-3.006469146625" Z="1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1" />
                  <Point X="-2.562329774076" Y="-2.566000695056" Z="1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1" />
                  <Point X="-2.848947866543" Y="-3.081121759164" Z="1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1" />
                  <Point X="-3.051881235696" Y="-4.053225123642" Z="1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1" />
                  <Point X="-2.624312930565" Y="-4.34230341616" Z="1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1" />
                  <Point X="-2.36246377842" Y="-4.334005498785" Z="1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1" />
                  <Point X="-2.069946508922" Y="-4.05203189567" Z="1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1" />
                  <Point X="-1.780707136709" Y="-3.996376982851" Z="1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1" />
                  <Point X="-1.517357599152" Y="-4.128300989659" Z="1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1" />
                  <Point X="-1.388738501668" Y="-4.393280191684" Z="1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1" />
                  <Point X="-1.383887100637" Y="-4.657616557391" Z="1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1" />
                  <Point X="-1.23396591394" Y="-4.925592648995" Z="1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1" />
                  <Point X="-0.935823365506" Y="-4.990828593734" Z="1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="-0.659758613981" Y="-4.424436757366" Z="1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="-0.317900668868" Y="-3.375864802488" Z="1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="-0.099874350078" Y="-3.235236680995" Z="1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1" />
                  <Point X="0.153484729283" Y="-3.251875364293" Z="1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1" />
                  <Point X="0.352545190564" Y="-3.42578095328" Z="1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1" />
                  <Point X="0.57499624642" Y="-4.108099272449" Z="1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1" />
                  <Point X="0.926919168241" Y="-4.993916015485" Z="1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1" />
                  <Point X="1.106474154533" Y="-4.957226131579" Z="1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1" />
                  <Point X="1.090444225716" Y="-4.283896611999" Z="1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1" />
                  <Point X="0.989946402976" Y="-3.122925273758" Z="1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1" />
                  <Point X="1.12019401999" Y="-2.934667814719" Z="1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1" />
                  <Point X="1.332347492248" Y="-2.862681743138" Z="1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1" />
                  <Point X="1.55334046768" Y="-2.937232397195" Z="1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1" />
                  <Point X="2.041288601438" Y="-3.517663118036" Z="1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1" />
                  <Point X="2.78031497674" Y="-4.250097824209" Z="1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1" />
                  <Point X="2.971914429933" Y="-4.118398713532" Z="1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1" />
                  <Point X="2.740898304926" Y="-3.535775703409" Z="1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1" />
                  <Point X="2.247595107807" Y="-2.591391204553" Z="1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1" />
                  <Point X="2.289446467121" Y="-2.397456328852" Z="1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1" />
                  <Point X="2.43544204952" Y="-2.269454842846" Z="1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1" />
                  <Point X="2.63711559896" Y="-2.255852901684" Z="1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1" />
                  <Point X="3.251637893698" Y="-2.576851195348" Z="1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1" />
                  <Point X="4.170891924813" Y="-2.896218127943" Z="1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1" />
                  <Point X="4.336338922149" Y="-2.642079384306" Z="1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1" />
                  <Point X="3.923618801309" Y="-2.175413965951" Z="1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1" />
                  <Point X="3.13187192758" Y="-1.519912349234" Z="1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1" />
                  <Point X="3.10179109222" Y="-1.354753078092" Z="1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1" />
                  <Point X="3.174474328718" Y="-1.207413976378" Z="1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1" />
                  <Point X="3.327727056661" Y="-1.131477069357" Z="1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1" />
                  <Point X="3.993639108407" Y="-1.194166665022" Z="1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1" />
                  <Point X="4.958155366102" Y="-1.090273528948" Z="1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1" />
                  <Point X="5.025712526261" Y="-0.717089671205" Z="1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1" />
                  <Point X="4.535529481118" Y="-0.431841189756" Z="1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1" />
                  <Point X="3.691910224156" Y="-0.188417081499" Z="1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1" />
                  <Point X="3.621817229388" Y="-0.12449143726" Z="1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1" />
                  <Point X="3.588728228608" Y="-0.038083948069" Z="1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1" />
                  <Point X="3.592643221816" Y="0.05852658315" Z="1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1" />
                  <Point X="3.633562209012" Y="0.139457301315" Z="1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1" />
                  <Point X="3.711485190197" Y="0.199731762743" Z="1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1" />
                  <Point X="4.260438336189" Y="0.358130743012" Z="1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1" />
                  <Point X="5.0080915971" Y="0.825583551211" Z="1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1" />
                  <Point X="4.920728814306" Y="1.244587885442" Z="1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1" />
                  <Point X="4.321941171661" Y="1.33508988399" Z="1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1" />
                  <Point X="3.406078879144" Y="1.229562949036" Z="1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1" />
                  <Point X="3.326771510059" Y="1.258217393436" Z="1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1" />
                  <Point X="3.270205322625" Y="1.317921859991" Z="1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1" />
                  <Point X="3.240557152412" Y="1.398592686907" Z="1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1" />
                  <Point X="3.246631226726" Y="1.478974236586" Z="1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1" />
                  <Point X="3.290120380529" Y="1.554979693812" Z="1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1" />
                  <Point X="3.760084947073" Y="1.92783358011" Z="1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1" />
                  <Point X="4.32062236048" Y="2.664517094969" Z="1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1" />
                  <Point X="4.095262122014" Y="2.999376348022" Z="1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1" />
                  <Point X="3.413962504015" Y="2.788972215943" Z="1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1" />
                  <Point X="2.461240564858" Y="2.253992505416" Z="1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1" />
                  <Point X="2.387534125997" Y="2.250600533901" Z="1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1" />
                  <Point X="2.321814418281" Y="2.279924174228" Z="1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1" />
                  <Point X="2.270834426007" Y="2.335210442101" Z="1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1" />
                  <Point X="2.248829168857" Y="2.402224317836" Z="1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1" />
                  <Point X="2.258535437241" Y="2.478229011024" Z="1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1" />
                  <Point X="2.606653136917" Y="3.09817657794" Z="1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1" />
                  <Point X="2.901373888255" Y="4.163870724856" Z="1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1" />
                  <Point X="2.512550219494" Y="4.409408642523" Z="1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1" />
                  <Point X="2.106336115216" Y="4.617401883626" Z="1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1" />
                  <Point X="1.685177079362" Y="4.787194762113" Z="1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1" />
                  <Point X="1.120436511103" Y="4.943968307894" Z="1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1" />
                  <Point X="0.457088711277" Y="5.04869150323" Z="1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1" />
                  <Point X="0.117067402085" Y="4.79202598836" Z="1" />
                  <Point X="0" Y="4.355124473572" Z="1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>