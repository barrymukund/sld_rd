<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#133" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="806" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.62658605957" Y="21.251296875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.516497192383" Y="21.569892578125" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.356752349854" Y="21.790978515625" />
                  <Point X="0.330497161865" Y="21.81022265625" />
                  <Point X="0.30249395752" Y="21.82433203125" />
                  <Point X="0.262468200684" Y="21.83675390625" />
                  <Point X="0.049134822845" Y="21.90296484375" />
                  <Point X="0.020976791382" Y="21.907234375" />
                  <Point X="-0.008664384842" Y="21.907234375" />
                  <Point X="-0.036826667786" Y="21.90296484375" />
                  <Point X="-0.076852432251" Y="21.890541015625" />
                  <Point X="-0.290185516357" Y="21.82433203125" />
                  <Point X="-0.318183990479" Y="21.810224609375" />
                  <Point X="-0.34443963623" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.768525390625" />
                  <Point X="-0.39218927002" Y="21.7312578125" />
                  <Point X="-0.530051208496" Y="21.532625" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.888287231445" Y="20.2286640625" />
                  <Point X="-0.916584411621" Y="20.12305859375" />
                  <Point X="-1.079368896484" Y="20.15465625" />
                  <Point X="-1.121832763672" Y="20.165580078125" />
                  <Point X="-1.24641809082" Y="20.197634765625" />
                  <Point X="-1.222580810547" Y="20.3786953125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.226070800781" Y="20.531845703125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323645507812" Y="20.868796875" />
                  <Point X="-1.36049609375" Y="20.90111328125" />
                  <Point X="-1.55690637207" Y="21.073361328125" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643028564453" Y="21.109033203125" />
                  <Point X="-1.69193762207" Y="21.11223828125" />
                  <Point X="-1.952617797852" Y="21.12932421875" />
                  <Point X="-1.983414672852" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657226562" Y="21.10519921875" />
                  <Point X="-2.083410888672" Y="21.07796875" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.4801484375" Y="20.711509765625" />
                  <Point X="-2.801714111328" Y="20.910615234375" />
                  <Point X="-2.860542236328" Y="20.955912109375" />
                  <Point X="-3.104721923828" Y="21.143921875" />
                  <Point X="-2.541367431641" Y="22.1196796875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.411279052734" Y="22.353029296875" />
                  <Point X="-2.405864013672" Y="22.375560546875" />
                  <Point X="-2.406064208984" Y="22.39873046875" />
                  <Point X="-2.410208984375" Y="22.4302109375" />
                  <Point X="-2.416571289062" Y="22.454029296875" />
                  <Point X="-2.42884765625" Y="22.475408203125" />
                  <Point X="-2.441172363281" Y="22.49157421875" />
                  <Point X="-2.449546875" Y="22.50115234375" />
                  <Point X="-2.464154785156" Y="22.515759765625" />
                  <Point X="-2.489311767578" Y="22.533787109375" />
                  <Point X="-2.518140869141" Y="22.54800390625" />
                  <Point X="-2.547759033203" Y="22.55698828125" />
                  <Point X="-2.578693359375" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-3.724883544922" Y="21.91197265625" />
                  <Point X="-3.818024169922" Y="21.858197265625" />
                  <Point X="-4.082861816406" Y="22.206142578125" />
                  <Point X="-4.125037597656" Y="22.27686328125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.311727050781" Y="23.34358984375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.064365966797" Y="23.553544921875" />
                  <Point X="-3.056435058594" Y="23.573314453125" />
                  <Point X="-3.052639404297" Y="23.584865234375" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037353516" Y="23.64139453125" />
                  <Point X="-3.042736328125" Y="23.66426171875" />
                  <Point X="-3.048882080078" Y="23.686298828125" />
                  <Point X="-3.060885253906" Y="23.715279296875" />
                  <Point X="-3.0731875" Y="23.736630859375" />
                  <Point X="-3.090567138672" Y="23.754099609375" />
                  <Point X="-3.107256591797" Y="23.76699609375" />
                  <Point X="-3.117160644531" Y="23.773697265625" />
                  <Point X="-3.139459472656" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608154297" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.602521484375" Y="23.625173828125" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.845236328125" Y="24.085365234375" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.767281738281" Y="24.71678125" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.514007568359" Y="24.786875" />
                  <Point X="-3.494055175781" Y="24.797138671875" />
                  <Point X="-3.483344970703" Y="24.803572265625" />
                  <Point X="-3.4599765625" Y="24.819791015625" />
                  <Point X="-3.436021240234" Y="24.841318359375" />
                  <Point X="-3.415918945313" Y="24.8698671875" />
                  <Point X="-3.407031982422" Y="24.889314453125" />
                  <Point X="-3.40270703125" Y="24.900640625" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390733886719" Y="24.957935546875" />
                  <Point X="-3.392195068359" Y="24.99228515625" />
                  <Point X="-3.396378173828" Y="25.01640625" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.417483886719" Y="25.070830078125" />
                  <Point X="-3.438826660156" Y="25.098748046875" />
                  <Point X="-3.455030029297" Y="25.11330859375" />
                  <Point X="-3.464361083984" Y="25.12069140625" />
                  <Point X="-3.487729492188" Y="25.13691015625" />
                  <Point X="-3.501922851562" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.782235839844" Y="25.49261328125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.824488769531" Y="25.97696875" />
                  <Point X="-4.802023925781" Y="26.05987109375" />
                  <Point X="-4.703550292969" Y="26.423267578125" />
                  <Point X="-3.933694091797" Y="26.3219140625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744991699219" Y="26.299341796875" />
                  <Point X="-3.723435058594" Y="26.3012265625" />
                  <Point X="-3.703150390625" Y="26.305259765625" />
                  <Point X="-3.693440673828" Y="26.3083203125" />
                  <Point X="-3.64171875" Y="26.32462890625" />
                  <Point X="-3.622782958984" Y="26.332958984375" />
                  <Point X="-3.6040390625" Y="26.343779296875" />
                  <Point X="-3.587354003906" Y="26.35601171875" />
                  <Point X="-3.573712890625" Y="26.371568359375" />
                  <Point X="-3.561298828125" Y="26.389298828125" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.547457763672" Y="26.416830078125" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532049804688" Y="26.589376953125" />
                  <Point X="-3.536748046875" Y="26.59840234375" />
                  <Point X="-3.561789550781" Y="26.646505859375" />
                  <Point X="-3.57328125" Y="26.663705078125" />
                  <Point X="-3.587193359375" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.318771972656" Y="27.244484375" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-4.021650878906" Y="27.810140625" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.309855224609" Y="27.90425390625" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187729736328" Y="27.836341796875" />
                  <Point X="-3.167087646484" Y="27.82983203125" />
                  <Point X="-3.146789550781" Y="27.825794921875" />
                  <Point X="-3.133274414062" Y="27.82461328125" />
                  <Point X="-3.061240234375" Y="27.818310546875" />
                  <Point X="-3.040561035156" Y="27.81876171875" />
                  <Point X="-3.019102539062" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946076171875" Y="27.86023046875" />
                  <Point X="-2.936482910156" Y="27.86982421875" />
                  <Point X="-2.885352539062" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435546875" Y="28.036119140625" />
                  <Point X="-2.844617919922" Y="28.049634765625" />
                  <Point X="-2.850920166016" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.183333007812" Y="28.724595703125" />
                  <Point X="-2.700628417969" Y="29.094681640625" />
                  <Point X="-2.606913330078" Y="29.146748046875" />
                  <Point X="-2.167037353516" Y="29.391134765625" />
                  <Point X="-2.074787109375" Y="29.27091015625" />
                  <Point X="-2.043195068359" Y="29.229740234375" />
                  <Point X="-2.02888684082" Y="29.214794921875" />
                  <Point X="-2.012307373047" Y="29.200884765625" />
                  <Point X="-1.995119873047" Y="29.189400390625" />
                  <Point X="-1.980077514648" Y="29.181568359375" />
                  <Point X="-1.899903686523" Y="29.13983203125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718261719" Y="29.126728515625" />
                  <Point X="-1.839268310547" Y="29.123580078125" />
                  <Point X="-1.818621826172" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.777451049805" Y="29.134482421875" />
                  <Point X="-1.761783447266" Y="29.14097265625" />
                  <Point X="-1.67827722168" Y="29.1755625" />
                  <Point X="-1.660145751953" Y="29.185509765625" />
                  <Point X="-1.642416259766" Y="29.197923828125" />
                  <Point X="-1.626864013672" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.590380859375" Y="29.282095703125" />
                  <Point X="-1.563201171875" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201904297" Y="29.6318984375" />
                  <Point X="-1.584122802734" Y="29.631919921875" />
                  <Point X="-0.94962487793" Y="29.80980859375" />
                  <Point X="-0.836033203125" Y="29.82310546875" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.164432159424" Y="29.40025" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113998413" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.307419525146" Y="29.8879375" />
                  <Point X="0.844044250488" Y="29.83173828125" />
                  <Point X="0.938031921387" Y="29.809046875" />
                  <Point X="1.4810234375" Y="29.677951171875" />
                  <Point X="1.540956665039" Y="29.656212890625" />
                  <Point X="1.894643432617" Y="29.527927734375" />
                  <Point X="1.953805786133" Y="29.500259765625" />
                  <Point X="2.294564453125" Y="29.3408984375" />
                  <Point X="2.351760986328" Y="29.307576171875" />
                  <Point X="2.680971679688" Y="29.115779296875" />
                  <Point X="2.734883056641" Y="29.077439453125" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.283701171875" Y="27.786865234375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.139879638672" Y="27.534087890625" />
                  <Point X="2.131486328125" Y="27.5093203125" />
                  <Point X="2.129684814453" Y="27.50337109375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108382324219" Y="27.411275390625" />
                  <Point X="2.1083671875" Y="27.38140625" />
                  <Point X="2.109050292969" Y="27.369984375" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.1400703125" Y="27.24747265625" />
                  <Point X="2.146856933594" Y="27.237470703125" />
                  <Point X="2.183028076172" Y="27.1841640625" />
                  <Point X="2.199609130859" Y="27.16555078125" />
                  <Point X="2.22291015625" Y="27.145462890625" />
                  <Point X="2.231599121094" Y="27.1388046875" />
                  <Point X="2.28490625" Y="27.1026328125" />
                  <Point X="2.304954101562" Y="27.09226953125" />
                  <Point X="2.327043212891" Y="27.08400390625" />
                  <Point X="2.348966308594" Y="27.078662109375" />
                  <Point X="2.359934082031" Y="27.07733984375" />
                  <Point X="2.418391113281" Y="27.070291015625" />
                  <Point X="2.443832519531" Y="27.070654296875" />
                  <Point X="2.475418701172" Y="27.075384765625" />
                  <Point X="2.485890380859" Y="27.0775625" />
                  <Point X="2.553493164062" Y="27.095640625" />
                  <Point X="2.565287841797" Y="27.099640625" />
                  <Point X="2.588533691406" Y="27.110146484375" />
                  <Point X="3.845149658203" Y="27.835654296875" />
                  <Point X="3.967324951172" Y="27.90619140625" />
                  <Point X="4.123274414062" Y="27.68945703125" />
                  <Point X="4.153323242187" Y="27.63980078125" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.409998779297" Y="26.80596875" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.216885742188" Y="26.655369140625" />
                  <Point X="3.198512695312" Y="26.6342109375" />
                  <Point X="3.194845458984" Y="26.62971875" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.133621582031" Y="26.544322265625" />
                  <Point X="3.121751464844" Y="26.515212890625" />
                  <Point X="3.118229248047" Y="26.504927734375" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.100530761719" Y="26.358240234375" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.123869873047" Y="26.262072265625" />
                  <Point X="3.138659912109" Y="26.23315625" />
                  <Point X="3.143874511719" Y="26.224201171875" />
                  <Point X="3.184340087891" Y="26.1626953125" />
                  <Point X="3.198892578125" Y="26.145451171875" />
                  <Point X="3.216135986328" Y="26.129361328125" />
                  <Point X="3.234345458984" Y="26.11603515625" />
                  <Point X="3.245347412109" Y="26.109841796875" />
                  <Point X="3.303987548828" Y="26.07683203125" />
                  <Point X="3.328210449219" Y="26.067291015625" />
                  <Point X="3.361061767578" Y="26.059328125" />
                  <Point X="3.370993652344" Y="26.05747265625" />
                  <Point X="3.450278808594" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203857422" Y="26.046984375" />
                  <Point X="4.681904785156" Y="26.20413671875" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.85540625" Y="25.871984375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.921963867188" Y="25.384623046875" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.698408447266" Y="25.32266015625" />
                  <Point X="3.671713867188" Y="25.309205078125" />
                  <Point X="3.666932861328" Y="25.30662109375" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.568669433594" Y="25.24578125" />
                  <Point X="3.545587890625" Y="25.2221875" />
                  <Point X="3.53876171875" Y="25.214404296875" />
                  <Point X="3.492024658203" Y="25.154849609375" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463681152344" Y="25.09260546875" />
                  <Point X="3.460758056641" Y="25.07734375" />
                  <Point X="3.445178955078" Y="24.995994140625" />
                  <Point X="3.443830566406" Y="24.970001953125" />
                  <Point X="3.446753662109" Y="24.935931640625" />
                  <Point X="3.4481015625" Y="24.92618359375" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480300292969" Y="24.80187109375" />
                  <Point X="3.492024169922" Y="24.782591796875" />
                  <Point X="3.50079296875" Y="24.77141796875" />
                  <Point X="3.547530029297" Y="24.71186328125" />
                  <Point X="3.566685546875" Y="24.693466796875" />
                  <Point X="3.595612060547" Y="24.672599609375" />
                  <Point X="3.603649902344" Y="24.667396484375" />
                  <Point X="3.681545166016" Y="24.62237109375" />
                  <Point X="3.692709472656" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.811258300781" Y="24.314529296875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.842889648438" Y="23.99810546875" />
                  <Point X="4.801173828125" Y="23.815302734375" />
                  <Point X="3.662569824219" Y="23.965201171875" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.345975341797" Y="23.988255859375" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.095059814453" Y="23.8851875" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.967272705078" Y="23.667630859375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976451660156" Y="23.432" />
                  <Point X="2.992325683594" Y="23.407310546875" />
                  <Point X="3.076931884766" Y="23.2757109375" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.12649609375" Y="22.4595859375" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.124810546875" Y="22.250212890625" />
                  <Point X="4.099716796875" Y="22.21455859375" />
                  <Point X="4.028981445312" Y="22.114052734375" />
                  <Point X="3.013093261719" Y="22.700576171875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754226318359" Y="22.840171875" />
                  <Point X="2.720088378906" Y="22.846337890625" />
                  <Point X="2.538135986328" Y="22.879197265625" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.416473388672" Y="22.849896484375" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204532470703" Y="22.7095625" />
                  <Point X="2.189606689453" Y="22.681203125" />
                  <Point X="2.110053466797" Y="22.530044921875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.101840576172" Y="22.402603515625" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.804616455078" Y="21.0432421875" />
                  <Point X="2.861283691406" Y="20.945091796875" />
                  <Point X="2.781862060547" Y="20.88836328125" />
                  <Point X="2.753798095703" Y="20.87019921875" />
                  <Point X="2.701764160156" Y="20.83651953125" />
                  <Point X="1.920427368164" Y="21.854775390625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.688254882813" Y="22.12108984375" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448366088867" Y="22.2565625" />
                  <Point X="1.417099487305" Y="22.258880859375" />
                  <Point X="1.380276245117" Y="22.2554921875" />
                  <Point X="1.184012695312" Y="22.23743359375" />
                  <Point X="1.156363037109" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104594604492" Y="22.204537109375" />
                  <Point X="1.076160522461" Y="22.18089453125" />
                  <Point X="0.924610778809" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.86712286377" Y="21.935076171875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="1.004739868164" Y="20.271681640625" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.975717712402" Y="20.12992578125" />
                  <Point X="0.949757873535" Y="20.125208984375" />
                  <Point X="0.929315551758" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058480224609" Y="20.247375" />
                  <Point X="-1.098164672852" Y="20.257583984375" />
                  <Point X="-1.141246459961" Y="20.26866796875" />
                  <Point X="-1.128393554688" Y="20.366294921875" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.132896240234" Y="20.55037890625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.23057434082" Y="20.905138671875" />
                  <Point X="-1.250210083008" Y="20.929064453125" />
                  <Point X="-1.261008422852" Y="20.94022265625" />
                  <Point X="-1.297859008789" Y="20.9725390625" />
                  <Point X="-1.49426940918" Y="21.144787109375" />
                  <Point X="-1.506739746094" Y="21.15403515625" />
                  <Point X="-1.533022460938" Y="21.17037890625" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531982422" Y="21.189775390625" />
                  <Point X="-1.59131652832" Y="21.194525390625" />
                  <Point X="-1.621458740234" Y="21.201552734375" />
                  <Point X="-1.63681640625" Y="21.203830078125" />
                  <Point X="-1.685725463867" Y="21.20703515625" />
                  <Point X="-1.946405639648" Y="21.22412109375" />
                  <Point X="-1.961929321289" Y="21.2238671875" />
                  <Point X="-1.992726196289" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048095703" Y="21.209736328125" />
                  <Point X="-2.053667724609" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436035156" Y="21.184189453125" />
                  <Point X="-2.136189697266" Y="21.156958984375" />
                  <Point X="-2.353402587891" Y="21.011822265625" />
                  <Point X="-2.359682373047" Y="21.007244140625" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201660156" Y="20.837521484375" />
                  <Point X="-2.747597412109" Y="20.98884375" />
                  <Point X="-2.802583984375" Y="21.03118359375" />
                  <Point X="-2.980862792969" Y="21.168451171875" />
                  <Point X="-2.459094970703" Y="22.0721796875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.336204345703" Y="22.28651953125" />
                  <Point X="-2.323722167969" Y="22.31616796875" />
                  <Point X="-2.318909179688" Y="22.330830078125" />
                  <Point X="-2.313494140625" Y="22.353361328125" />
                  <Point X="-2.310867675781" Y="22.376380859375" />
                  <Point X="-2.311067871094" Y="22.39955078125" />
                  <Point X="-2.311876953125" Y="22.411130859375" />
                  <Point X="-2.316021728516" Y="22.442611328125" />
                  <Point X="-2.318427001953" Y="22.4547265625" />
                  <Point X="-2.324789306641" Y="22.478544921875" />
                  <Point X="-2.334187744141" Y="22.5013359375" />
                  <Point X="-2.346464111328" Y="22.52271484375" />
                  <Point X="-2.353299072266" Y="22.533005859375" />
                  <Point X="-2.365623779297" Y="22.549171875" />
                  <Point X="-2.382372802734" Y="22.568328125" />
                  <Point X="-2.396980712891" Y="22.582935546875" />
                  <Point X="-2.408819091797" Y="22.59298046875" />
                  <Point X="-2.433976074219" Y="22.6110078125" />
                  <Point X="-2.447294677734" Y="22.618990234375" />
                  <Point X="-2.476123779297" Y="22.63320703125" />
                  <Point X="-2.490564453125" Y="22.6389140625" />
                  <Point X="-2.520182617188" Y="22.6478984375" />
                  <Point X="-2.550870361328" Y="22.6519375" />
                  <Point X="-2.5818046875" Y="22.650923828125" />
                  <Point X="-2.597228759766" Y="22.6491484375" />
                  <Point X="-2.628754638672" Y="22.642876953125" />
                  <Point X="-2.643685302734" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.686683837891" Y="22.621072265625" />
                  <Point X="-3.772383544922" Y="21.994244140625" />
                  <Point X="-3.793089111328" Y="21.9822890625" />
                  <Point X="-4.004012939453" Y="22.25940234375" />
                  <Point X="-4.0434453125" Y="22.3255234375" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.253894775391" Y="23.268220703125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.0028828125" Y="23.47316796875" />
                  <Point X="-2.984183837891" Y="23.502595703125" />
                  <Point X="-2.976196289062" Y="23.518173828125" />
                  <Point X="-2.968265380859" Y="23.537943359375" />
                  <Point X="-2.960674072266" Y="23.561044921875" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953369141" Y="23.597599609375" />
                  <Point X="-2.947838623047" Y="23.62908203125" />
                  <Point X="-2.947081787109" Y="23.644296875" />
                  <Point X="-2.947780761719" Y="23.6671640625" />
                  <Point X="-2.951228271484" Y="23.68978125" />
                  <Point X="-2.957374023438" Y="23.711818359375" />
                  <Point X="-2.961112548828" Y="23.722650390625" />
                  <Point X="-2.973115722656" Y="23.751630859375" />
                  <Point X="-2.978571044922" Y="23.76270703125" />
                  <Point X="-2.990873291016" Y="23.78405859375" />
                  <Point X="-3.005840820312" Y="23.803634765625" />
                  <Point X="-3.023220458984" Y="23.821103515625" />
                  <Point X="-3.032479492188" Y="23.829271484375" />
                  <Point X="-3.049168945312" Y="23.84216796875" />
                  <Point X="-3.068977050781" Y="23.8555703125" />
                  <Point X="-3.091275878906" Y="23.868693359375" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.13469921875" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891142578125" />
                  <Point X="-3.181687255859" Y="23.897623046875" />
                  <Point X="-3.197304443359" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.614921386719" Y="23.719361328125" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.751193359375" Y="24.098814453125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.742693847656" Y="24.625017578125" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.498658203125" Y="24.690966796875" />
                  <Point X="-3.479789794922" Y="24.698251953125" />
                  <Point X="-3.470551269531" Y="24.702396484375" />
                  <Point X="-3.450598876953" Y="24.71266015625" />
                  <Point X="-3.429178466797" Y="24.72552734375" />
                  <Point X="-3.405810058594" Y="24.74174609375" />
                  <Point X="-3.396477783203" Y="24.749130859375" />
                  <Point X="-3.372522460938" Y="24.770658203125" />
                  <Point X="-3.358345458984" Y="24.786623046875" />
                  <Point X="-3.338243164062" Y="24.815171875" />
                  <Point X="-3.329513427734" Y="24.830380859375" />
                  <Point X="-3.320626464844" Y="24.849828125" />
                  <Point X="-3.3119765625" Y="24.87248046875" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.300709472656" Y="24.913498046875" />
                  <Point X="-3.296525878906" Y="24.9456953125" />
                  <Point X="-3.295819824219" Y="24.96197265625" />
                  <Point X="-3.297281005859" Y="24.996322265625" />
                  <Point X="-3.298592285156" Y="25.008517578125" />
                  <Point X="-3.302775390625" Y="25.032638671875" />
                  <Point X="-3.305647216797" Y="25.044564453125" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.317668457031" Y="25.08078515625" />
                  <Point X="-3.330984619141" Y="25.110109375" />
                  <Point X="-3.34201171875" Y="25.12852734375" />
                  <Point X="-3.363354492188" Y="25.1564453125" />
                  <Point X="-3.375329101562" Y="25.16941015625" />
                  <Point X="-3.391532470703" Y="25.183970703125" />
                  <Point X="-3.410194580078" Y="25.198736328125" />
                  <Point X="-3.433562988281" Y="25.214955078125" />
                  <Point X="-3.440481445312" Y="25.219328125" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496565673828" Y="25.24563671875" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.757647949219" Y="25.584376953125" />
                  <Point X="-4.785446289062" Y="25.591826171875" />
                  <Point X="-4.731332519531" Y="25.95751953125" />
                  <Point X="-4.710330566406" Y="26.0350234375" />
                  <Point X="-4.633584960938" Y="26.318236328125" />
                  <Point X="-3.946093994141" Y="26.2277265625" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.767739013672" Y="26.20481640625" />
                  <Point X="-3.747064453125" Y="26.204365234375" />
                  <Point X="-3.736717041016" Y="26.204703125" />
                  <Point X="-3.715160400391" Y="26.206587890625" />
                  <Point X="-3.704908935547" Y="26.20805078125" />
                  <Point X="-3.684624267578" Y="26.212083984375" />
                  <Point X="-3.664881347656" Y="26.21771484375" />
                  <Point X="-3.613159423828" Y="26.2340234375" />
                  <Point X="-3.603465087891" Y="26.237671875" />
                  <Point X="-3.584529296875" Y="26.246001953125" />
                  <Point X="-3.575287841797" Y="26.25068359375" />
                  <Point X="-3.556543945313" Y="26.26150390625" />
                  <Point X="-3.547869384766" Y="26.2671640625" />
                  <Point X="-3.531184326172" Y="26.279396484375" />
                  <Point X="-3.515925292969" Y="26.29337890625" />
                  <Point X="-3.502284179688" Y="26.308935546875" />
                  <Point X="-3.495891601562" Y="26.31708203125" />
                  <Point X="-3.483477539062" Y="26.3348125" />
                  <Point X="-3.478010498047" Y="26.343603515625" />
                  <Point X="-3.468063232422" Y="26.361734375" />
                  <Point X="-3.459689208984" Y="26.380474609375" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012451172" Y="26.60453125" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.452481689453" Y="26.642267578125" />
                  <Point X="-3.477523193359" Y="26.69037109375" />
                  <Point X="-3.482799072266" Y="26.699283203125" />
                  <Point X="-3.494290771484" Y="26.716482421875" />
                  <Point X="-3.500506591797" Y="26.72476953125" />
                  <Point X="-3.514418701172" Y="26.741349609375" />
                  <Point X="-3.521498535156" Y="26.748908203125" />
                  <Point X="-3.536440917969" Y="26.763212890625" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.002299072266" Y="27.68030078125" />
                  <Point X="-3.946669433594" Y="27.751806640625" />
                  <Point X="-3.726336425781" Y="28.035013671875" />
                  <Point X="-3.357355224609" Y="27.821982421875" />
                  <Point X="-3.254157226562" Y="27.762400390625" />
                  <Point X="-3.244925048828" Y="27.757720703125" />
                  <Point X="-3.225997558594" Y="27.749390625" />
                  <Point X="-3.216302246094" Y="27.745740234375" />
                  <Point X="-3.19566015625" Y="27.73923046875" />
                  <Point X="-3.185619384766" Y="27.73665625" />
                  <Point X="-3.165321289062" Y="27.732619140625" />
                  <Point X="-3.141548828125" Y="27.729974609375" />
                  <Point X="-3.069514648438" Y="27.723671875" />
                  <Point X="-3.05916796875" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.02815625" Y="27.72457421875" />
                  <Point X="-3.006697753906" Y="27.727400390625" />
                  <Point X="-2.996520751953" Y="27.729310546875" />
                  <Point X="-2.976432128906" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902748291016" Y="27.77319140625" />
                  <Point X="-2.886613769531" Y="27.786140625" />
                  <Point X="-2.869306152344" Y="27.802650390625" />
                  <Point X="-2.81817578125" Y="27.853779296875" />
                  <Point X="-2.811264404297" Y="27.861490234375" />
                  <Point X="-2.798320800781" Y="27.8776171875" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.013365234375" />
                  <Point X="-2.748458251953" Y="28.034044921875" />
                  <Point X="-2.749979248047" Y="28.0579140625" />
                  <Point X="-2.756281494141" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761781005859" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059386962891" Y="28.699916015625" />
                  <Point X="-2.648377197266" Y="29.015033203125" />
                  <Point X="-2.560775634766" Y="29.063703125" />
                  <Point X="-2.192525634766" Y="29.268296875" />
                  <Point X="-2.150156005859" Y="29.213078125" />
                  <Point X="-2.118563964844" Y="29.171908203125" />
                  <Point X="-2.111816894531" Y="29.16404296875" />
                  <Point X="-2.097508544922" Y="29.14909765625" />
                  <Point X="-2.089947265625" Y="29.142017578125" />
                  <Point X="-2.073367919922" Y="29.128107421875" />
                  <Point X="-2.065086669922" Y="29.12189453125" />
                  <Point X="-2.047899169922" Y="29.11041015625" />
                  <Point X="-2.023950073242" Y="29.097306640625" />
                  <Point X="-1.943776245117" Y="29.0555703125" />
                  <Point X="-1.934347167969" Y="29.051296875" />
                  <Point X="-1.915068603516" Y="29.043796875" />
                  <Point X="-1.905219238281" Y="29.0405703125" />
                  <Point X="-1.88431237793" Y="29.034966796875" />
                  <Point X="-1.874174072266" Y="29.032833984375" />
                  <Point X="-1.853723999023" Y="29.029685546875" />
                  <Point X="-1.833053710938" Y="29.028783203125" />
                  <Point X="-1.812407226562" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.780804443359" Y="29.03513671875" />
                  <Point X="-1.770715332031" Y="29.037490234375" />
                  <Point X="-1.750859985352" Y="29.043279296875" />
                  <Point X="-1.725426147461" Y="29.053205078125" />
                  <Point X="-1.641919921875" Y="29.087794921875" />
                  <Point X="-1.632583374023" Y="29.0922734375" />
                  <Point X="-1.614451782227" Y="29.102220703125" />
                  <Point X="-1.605656738281" Y="29.107689453125" />
                  <Point X="-1.587927246094" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564226928711" Y="29.14013671875" />
                  <Point X="-1.550251708984" Y="29.155388671875" />
                  <Point X="-1.538020263672" Y="29.1720703125" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.516855102539" Y="29.20873046875" />
                  <Point X="-1.508524780273" Y="29.2276640625" />
                  <Point X="-1.499777709961" Y="29.253529296875" />
                  <Point X="-1.472598144531" Y="29.339732421875" />
                  <Point X="-1.470026977539" Y="29.349763671875" />
                  <Point X="-1.465991577148" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.931166809082" Y="29.7163203125" />
                  <Point X="-0.824988037109" Y="29.72875" />
                  <Point X="-0.36522253418" Y="29.78255859375" />
                  <Point X="-0.256195098877" Y="29.375662109375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190765381" Y="29.7850078125" />
                  <Point X="0.827864624023" Y="29.7379140625" />
                  <Point X="0.915736755371" Y="29.71669921875" />
                  <Point X="1.453606323242" Y="29.58683984375" />
                  <Point X="1.508564331055" Y="29.56690625" />
                  <Point X="1.858248535156" Y="29.440072265625" />
                  <Point X="1.913561523438" Y="29.414205078125" />
                  <Point X="2.250438232422" Y="29.256658203125" />
                  <Point X="2.303938720703" Y="29.225490234375" />
                  <Point X="2.629436279297" Y="29.035857421875" />
                  <Point X="2.679825683594" Y="29.000021484375" />
                  <Point X="2.817779541016" Y="28.901916015625" />
                  <Point X="2.201428710938" Y="27.834365234375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.061038330078" Y="27.59028125" />
                  <Point X="2.049905761719" Y="27.564578125" />
                  <Point X="2.041512207031" Y="27.539810546875" />
                  <Point X="2.037909545898" Y="27.527912109375" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017419799805" Y="27.44816796875" />
                  <Point X="2.01419519043" Y="27.42367578125" />
                  <Point X="2.013382202148" Y="27.41132421875" />
                  <Point X="2.01336730957" Y="27.381455078125" />
                  <Point X="2.014733520508" Y="27.358611328125" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045318847656" Y="27.223888671875" />
                  <Point X="2.055680664062" Y="27.203845703125" />
                  <Point X="2.068245117188" Y="27.184130859375" />
                  <Point X="2.104416259766" Y="27.13082421875" />
                  <Point X="2.112092285156" Y="27.12097265625" />
                  <Point X="2.128673339844" Y="27.102359375" />
                  <Point X="2.137578369141" Y="27.09359765625" />
                  <Point X="2.160879394531" Y="27.073509765625" />
                  <Point X="2.178257324219" Y="27.060193359375" />
                  <Point X="2.231564453125" Y="27.024021484375" />
                  <Point X="2.241281982422" Y="27.0182421875" />
                  <Point X="2.261329833984" Y="27.00787890625" />
                  <Point X="2.27166015625" Y="27.003294921875" />
                  <Point X="2.293749267578" Y="26.995029296875" />
                  <Point X="2.304553466797" Y="26.991705078125" />
                  <Point X="2.3264765625" Y="26.98636328125" />
                  <Point X="2.348563232422" Y="26.9830234375" />
                  <Point X="2.407020263672" Y="26.975974609375" />
                  <Point X="2.419747558594" Y="26.97530078125" />
                  <Point X="2.445188964844" Y="26.9756640625" />
                  <Point X="2.457903076172" Y="26.976701171875" />
                  <Point X="2.489489257812" Y="26.981431640625" />
                  <Point X="2.510432617188" Y="26.985787109375" />
                  <Point X="2.578035400391" Y="27.003865234375" />
                  <Point X="2.584004150391" Y="27.005673828125" />
                  <Point X="2.604412597656" Y="27.0130703125" />
                  <Point X="2.627658447266" Y="27.023576171875" />
                  <Point X="2.636033691406" Y="27.027875" />
                  <Point X="3.892649658203" Y="27.7533828125" />
                  <Point X="3.94040234375" Y="27.780953125" />
                  <Point X="4.043956787109" Y="27.63703515625" />
                  <Point X="4.072046142578" Y="27.5906171875" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.352166503906" Y="26.881337890625" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.165669921875" Y="26.737626953125" />
                  <Point X="3.145155517578" Y="26.71765625" />
                  <Point X="3.126782470703" Y="26.696498046875" />
                  <Point X="3.119447998047" Y="26.687513671875" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.063776855469" Y="26.613498046875" />
                  <Point X="3.051206787109" Y="26.59157421875" />
                  <Point X="3.045654052734" Y="26.580193359375" />
                  <Point X="3.033783935547" Y="26.551083984375" />
                  <Point X="3.026739501953" Y="26.530513671875" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.007490722656" Y="26.33904296875" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.025787841797" Y="26.25462109375" />
                  <Point X="3.034249267578" Y="26.23055859375" />
                  <Point X="3.039291259766" Y="26.2188125" />
                  <Point X="3.054081298828" Y="26.189896484375" />
                  <Point X="3.064510498047" Y="26.171986328125" />
                  <Point X="3.104976074219" Y="26.11048046875" />
                  <Point X="3.111738037109" Y="26.10142578125" />
                  <Point X="3.126290527344" Y="26.084181640625" />
                  <Point X="3.134081054688" Y="26.0759921875" />
                  <Point X="3.151324462891" Y="26.05990234375" />
                  <Point X="3.160031494141" Y="26.052697265625" />
                  <Point X="3.178240966797" Y="26.03937109375" />
                  <Point X="3.198745361328" Y="26.027056640625" />
                  <Point X="3.257385498047" Y="25.994046875" />
                  <Point X="3.269171875" Y="25.98844140625" />
                  <Point X="3.293394775391" Y="25.978900390625" />
                  <Point X="3.305831298828" Y="25.97496484375" />
                  <Point X="3.338682617188" Y="25.967001953125" />
                  <Point X="3.358546386719" Y="25.963291015625" />
                  <Point X="3.437831542969" Y="25.9528125" />
                  <Point X="3.444032226562" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491214111328" Y="25.95203125" />
                  <Point X="3.500603759766" Y="25.952796875" />
                  <Point X="4.6943046875" Y="26.10994921875" />
                  <Point X="4.704704589844" Y="26.111318359375" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.761537109375" Y="25.857369140625" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.897375976562" Y="25.47638671875" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.682728759766" Y="25.418353515625" />
                  <Point X="3.655649414062" Y="25.407494140625" />
                  <Point X="3.628954833984" Y="25.3940390625" />
                  <Point X="3.619392822266" Y="25.38887109375" />
                  <Point X="3.541497558594" Y="25.34384765625" />
                  <Point X="3.530771728516" Y="25.336630859375" />
                  <Point X="3.510403564453" Y="25.320814453125" />
                  <Point X="3.500761230469" Y="25.31221484375" />
                  <Point X="3.4776796875" Y="25.28862109375" />
                  <Point X="3.46402734375" Y="25.2730546875" />
                  <Point X="3.417290283203" Y="25.2135" />
                  <Point X="3.410853271484" Y="25.20420703125" />
                  <Point X="3.39912890625" Y="25.18492578125" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004394531" Y="25.14292578125" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.367454101562" Y="25.09521484375" />
                  <Point X="3.351875" Y="25.013865234375" />
                  <Point X="3.350306640625" Y="25.000916015625" />
                  <Point X="3.348958251953" Y="24.974923828125" />
                  <Point X="3.349178222656" Y="24.961880859375" />
                  <Point X="3.352101318359" Y="24.927810546875" />
                  <Point X="3.354797119141" Y="24.908314453125" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067626953" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.762501953125" />
                  <Point X="3.399130126953" Y="24.75251171875" />
                  <Point X="3.410854003906" Y="24.733232421875" />
                  <Point X="3.426058105469" Y="24.71276953125" />
                  <Point X="3.472795166016" Y="24.65321484375" />
                  <Point X="3.481726074219" Y="24.64334375" />
                  <Point X="3.500881591797" Y="24.624947265625" />
                  <Point X="3.511106201172" Y="24.616421875" />
                  <Point X="3.540032714844" Y="24.5955546875" />
                  <Point X="3.556108398438" Y="24.5851484375" />
                  <Point X="3.634003662109" Y="24.540123046875" />
                  <Point X="3.639490234375" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683026855469" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.784876464844" Y="24.22324609375" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.750270507812" Y="24.019240234375" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.674969726562" Y="24.059388671875" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354481689453" Y="24.087322265625" />
                  <Point X="3.325798095703" Y="24.081087890625" />
                  <Point X="3.172917480469" Y="24.047859375" />
                  <Point X="3.157873779297" Y="24.0432578125" />
                  <Point X="3.128752685547" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123291016" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="3.02201171875" Y="23.945923828125" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.872672363281" Y="23.6763359375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.423330078125" />
                  <Point X="2.889159179688" Y="23.394515625" />
                  <Point X="2.89654296875" Y="23.380623046875" />
                  <Point X="2.912416992188" Y="23.35593359375" />
                  <Point X="2.997023193359" Y="23.224333984375" />
                  <Point X="3.001745117188" Y="23.217642578125" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="4.068663818359" Y="22.384216796875" />
                  <Point X="4.087170654297" Y="22.370015625" />
                  <Point X="4.045493164062" Y="22.302576171875" />
                  <Point X="4.022029052734" Y="22.269236328125" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="3.060593261719" Y="22.78284765625" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815017578125" Y="22.920490234375" />
                  <Point X="2.783114990234" Y="22.930673828125" />
                  <Point X="2.771112060547" Y="22.933658203125" />
                  <Point X="2.736974121094" Y="22.93982421875" />
                  <Point X="2.555021728516" Y="22.97268359375" />
                  <Point X="2.539366943359" Y="22.974189453125" />
                  <Point X="2.508014648438" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444846923828" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589599609" Y="22.948890625" />
                  <Point X="2.372229248047" Y="22.93396484375" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.208967773438" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144943115234" Y="22.788841796875" />
                  <Point X="2.128049316406" Y="22.765912109375" />
                  <Point X="2.120465087891" Y="22.75380859375" />
                  <Point X="2.105539306641" Y="22.72544921875" />
                  <Point X="2.025985961914" Y="22.574291015625" />
                  <Point X="2.019836914062" Y="22.559810546875" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.008352905273" Y="22.385720703125" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.722343994141" Y="20.9957421875" />
                  <Point X="2.735893554688" Y="20.9722734375" />
                  <Point X="2.723754394531" Y="20.963916015625" />
                  <Point X="1.995795898438" Y="21.912607421875" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657714844" Y="22.12984765625" />
                  <Point X="1.808835571289" Y="22.150369140625" />
                  <Point X="1.783253417969" Y="22.171994140625" />
                  <Point X="1.773299560547" Y="22.179353515625" />
                  <Point X="1.739630004883" Y="22.201" />
                  <Point X="1.56017590332" Y="22.31637109375" />
                  <Point X="1.546279663086" Y="22.323755859375" />
                  <Point X="1.517465332031" Y="22.336126953125" />
                  <Point X="1.502547363281" Y="22.34111328125" />
                  <Point X="1.470927001953" Y="22.34884375" />
                  <Point X="1.45539074707" Y="22.351302734375" />
                  <Point X="1.424124267578" Y="22.35362109375" />
                  <Point X="1.408393920898" Y="22.35348046875" />
                  <Point X="1.371570556641" Y="22.350091796875" />
                  <Point X="1.175307128906" Y="22.332033203125" />
                  <Point X="1.161224121094" Y="22.32966015625" />
                  <Point X="1.133574462891" Y="22.322828125" />
                  <Point X="1.1200078125" Y="22.318369140625" />
                  <Point X="1.092621826172" Y="22.307025390625" />
                  <Point X="1.079876342773" Y="22.3005859375" />
                  <Point X="1.055493896484" Y="22.285865234375" />
                  <Point X="1.043856689453" Y="22.277583984375" />
                  <Point X="1.015422607422" Y="22.25394140625" />
                  <Point X="0.86387286377" Y="22.127931640625" />
                  <Point X="0.852652404785" Y="22.116908203125" />
                  <Point X="0.832182861328" Y="22.09316015625" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.774290405273" Y="21.95525390625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833090942383" Y="20.847662109375" />
                  <Point X="0.718349060059" Y="21.275884765625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407287598" Y="21.58679296875" />
                  <Point X="0.594541564941" Y="21.624060546875" />
                  <Point X="0.4566796875" Y="21.822693359375" />
                  <Point X="0.446668609619" Y="21.83483203125" />
                  <Point X="0.424785675049" Y="21.85728515625" />
                  <Point X="0.412913513184" Y="21.867599609375" />
                  <Point X="0.38665838623" Y="21.88684375" />
                  <Point X="0.373243530273" Y="21.8950625" />
                  <Point X="0.34524029541" Y="21.909171875" />
                  <Point X="0.330652069092" Y="21.9150625" />
                  <Point X="0.290626281738" Y="21.927484375" />
                  <Point X="0.077292984009" Y="21.9936953125" />
                  <Point X="0.063376678467" Y="21.996890625" />
                  <Point X="0.035218582153" Y="22.00116015625" />
                  <Point X="0.020976791382" Y="22.002234375" />
                  <Point X="-0.008664410591" Y="22.002234375" />
                  <Point X="-0.022904119492" Y="22.00116015625" />
                  <Point X="-0.051066375732" Y="21.996890625" />
                  <Point X="-0.064988777161" Y="21.9936953125" />
                  <Point X="-0.105014556885" Y="21.981271484375" />
                  <Point X="-0.318347717285" Y="21.9150625" />
                  <Point X="-0.332932830811" Y="21.909171875" />
                  <Point X="-0.360931304932" Y="21.895064453125" />
                  <Point X="-0.374344512939" Y="21.88684765625" />
                  <Point X="-0.400600250244" Y="21.867603515625" />
                  <Point X="-0.412474456787" Y="21.85728515625" />
                  <Point X="-0.434358306885" Y="21.834830078125" />
                  <Point X="-0.444368041992" Y="21.822693359375" />
                  <Point X="-0.470233642578" Y="21.78542578125" />
                  <Point X="-0.60809564209" Y="21.58679296875" />
                  <Point X="-0.612471252441" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.980050170898" Y="20.253251953125" />
                  <Point X="-0.985424621582" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.669035092634" Y="23.928518058544" />
                  <Point X="4.691396014669" Y="24.248294141756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.574671820505" Y="23.940941164429" />
                  <Point X="4.597915564493" Y="24.273342189762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.697023597045" Y="25.690653086638" />
                  <Point X="4.721589434538" Y="26.041960929948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.480308548376" Y="23.953364270314" />
                  <Point X="4.504435114318" Y="24.298390237768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.599973187259" Y="25.664648333701" />
                  <Point X="4.630524500689" Y="26.10155247078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.385945276247" Y="23.965787376199" />
                  <Point X="4.410954664143" Y="24.323438285774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.502922777473" Y="25.638643580765" />
                  <Point X="4.534407672747" Y="26.088898560211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.291582004119" Y="23.978210482084" />
                  <Point X="4.317474213968" Y="24.34848633378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.405872367687" Y="25.612638827829" />
                  <Point X="4.438290844805" Y="26.076244649643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.083476036914" Y="22.364037266553" />
                  <Point X="4.084060945688" Y="22.372401851721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.19721873199" Y="23.990633587969" />
                  <Point X="4.223993763792" Y="24.373534381786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.308821957901" Y="25.586634074892" />
                  <Point X="4.342174016862" Y="26.063590739075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.980395663173" Y="22.251800011544" />
                  <Point X="3.993678613992" Y="22.441755058116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.102855459861" Y="24.003056693854" />
                  <Point X="4.130513313617" Y="24.398582429793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.211771548115" Y="25.560629321956" />
                  <Point X="4.24605718892" Y="26.050936828506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.888859212947" Y="22.304648554035" />
                  <Point X="3.903296262698" Y="22.511107984259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008492187733" Y="24.01547979974" />
                  <Point X="4.037032863442" Y="24.423630477799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.114721138329" Y="25.534624569019" />
                  <Point X="4.149940360978" Y="26.038282917938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.797322762722" Y="22.357497096527" />
                  <Point X="3.812913911405" Y="22.580460910402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.914128915604" Y="24.027902905625" />
                  <Point X="3.943552413267" Y="24.448678525805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.017670728543" Y="25.508619816083" />
                  <Point X="4.053823533035" Y="26.025629007369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.705786312496" Y="22.410345639018" />
                  <Point X="3.722531560111" Y="22.649813836545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.819765643475" Y="24.04032601151" />
                  <Point X="3.850071963091" Y="24.473726573811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920620318757" Y="25.482615063146" />
                  <Point X="3.957706705093" Y="26.012975096801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056204794207" Y="27.421563396146" />
                  <Point X="4.068442594279" Y="27.596572090686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.614249862271" Y="22.46319418151" />
                  <Point X="3.632149208818" Y="22.719166762688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.725402371346" Y="24.052749117395" />
                  <Point X="3.756591512916" Y="24.498774621817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823569901708" Y="25.456610206342" />
                  <Point X="3.86158987715" Y="26.000321186233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955573276454" Y="27.344346413351" />
                  <Point X="3.982055787917" Y="27.723063971416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.522713412045" Y="22.516042724001" />
                  <Point X="3.541766857524" Y="22.78851968883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.631039093033" Y="24.065172134839" />
                  <Point X="3.663292741587" Y="24.526420798361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726519482371" Y="25.430605316826" />
                  <Point X="3.765473049208" Y="25.987667275664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854941758702" Y="27.267129430556" />
                  <Point X="3.888788003712" Y="27.751153284485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.43117696182" Y="22.568891266493" />
                  <Point X="3.45138450623" Y="22.857872614973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.536675807621" Y="24.077595050754" />
                  <Point X="3.571543519095" Y="24.576226555679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.628721730733" Y="25.393913077471" />
                  <Point X="3.669356221266" Y="25.975013365096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75431024095" Y="27.189912447761" />
                  <Point X="3.789549540636" Y="27.693857911702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.339640511595" Y="22.621739808984" />
                  <Point X="3.361002154937" Y="22.927225541116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.442312522208" Y="24.090017966668" />
                  <Point X="3.481056574642" Y="24.644083729959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.529410259801" Y="25.335573643701" />
                  <Point X="3.573239393323" Y="25.962359454527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653678723197" Y="27.112695464966" />
                  <Point X="3.690311077561" Y="27.63656253892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.248104061369" Y="22.674588351475" />
                  <Point X="3.270619803643" Y="22.996578467259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.346774907955" Y="24.085647197768" />
                  <Point X="3.394074440263" Y="24.762062023401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.426459033085" Y="25.225183277201" />
                  <Point X="3.477254248117" Y="25.951588694824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.553047205445" Y="27.035478482171" />
                  <Point X="3.591072614485" Y="27.579267166138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.156567611144" Y="22.727436893967" />
                  <Point X="3.18023745235" Y="23.065931393402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.250073200984" Y="24.064629127403" />
                  <Point X="3.382618110271" Y="25.960109639144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452415687692" Y="26.958261499376" />
                  <Point X="3.49183415141" Y="27.521971793356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065031160918" Y="22.780285436458" />
                  <Point X="3.089855101056" Y="23.135284319545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.15321679563" Y="24.041398767109" />
                  <Point X="3.288825946819" Y="25.980699979613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351784169698" Y="26.881044513126" />
                  <Point X="3.392595688334" Y="27.464676420573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.973494719416" Y="22.833134103695" />
                  <Point X="3.000505052677" Y="23.219399865138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.053755227231" Y="23.980912839555" />
                  <Point X="3.196912513021" Y="26.028157405855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251152588349" Y="26.803826620855" />
                  <Point X="3.293357225259" Y="27.407381047791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.881958278358" Y="22.885982777288" />
                  <Point X="2.914584669516" Y="23.352561908393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.950022238425" Y="23.859342754313" />
                  <Point X="3.107226408943" Y="26.107467131051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.150241188186" Y="26.722607133118" />
                  <Point X="3.194118762183" Y="27.350085675009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658288237629" Y="21.049232940679" />
                  <Point X="2.661871973824" Y="21.100482755968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.789704318578" Y="22.928570455096" />
                  <Point X="3.022989300117" Y="26.264701118789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.044925780178" Y="26.578407399001" />
                  <Point X="3.094880299108" Y="27.292790302227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.571009941374" Y="21.16297592198" />
                  <Point X="2.57692811667" Y="21.24760977173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69577949659" Y="22.947263690116" />
                  <Point X="2.995641836032" Y="27.235494929444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48373164512" Y="21.27671890328" />
                  <Point X="2.491984259515" Y="21.394736787491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601735137651" Y="22.964247467099" />
                  <Point X="2.896403372957" Y="27.178199556662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396453348865" Y="21.390461884581" />
                  <Point X="2.40704040236" Y="21.541863803253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.507222898204" Y="22.974540241081" />
                  <Point X="2.797164909881" Y="27.12090418388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.309175052611" Y="21.504204865881" />
                  <Point X="2.322096545205" Y="21.688990819015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.410491329858" Y="22.953095133162" />
                  <Point X="2.697926446806" Y="27.063608811098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221896756356" Y="21.617947847182" />
                  <Point X="2.237152688051" Y="21.836117834777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.311693819413" Y="22.902105676791" />
                  <Point X="2.599023904407" Y="27.011117327792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719315820755" Y="28.731371876865" />
                  <Point X="2.735340954565" Y="28.960541967214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.134618460102" Y="21.731690828482" />
                  <Point X="2.152208830896" Y="21.983244850539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212762731465" Y="22.849205973118" />
                  <Point X="2.50189652854" Y="26.984011908621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.610960151603" Y="28.543694382798" />
                  <Point X="2.644620427804" Y="29.025058758853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047340163848" Y="21.845433809783" />
                  <Point X="2.067306094583" Y="22.13095992174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.109388212111" Y="22.732762239873" />
                  <Point X="2.406110199056" Y="26.976084346201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.502604482452" Y="28.35601688873" />
                  <Point X="2.553247415367" Y="29.080244570616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.96006188801" Y="21.959177083059" />
                  <Point X="2.311846268925" Y="26.989928108844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.3942488133" Y="28.168339394663" />
                  <Point X="2.461743216991" Y="29.133554336037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.872783641623" Y="22.072920777494" />
                  <Point X="2.219567572779" Y="27.032162040153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.285893144149" Y="27.980661900596" />
                  <Point X="2.370239018615" Y="29.186864101458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.784411114041" Y="22.171015521771" />
                  <Point X="2.12920751618" Y="27.101833795278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177537465821" Y="27.792984275293" />
                  <Point X="2.278734786171" Y="29.240173379691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.693356139104" Y="22.23074948168" />
                  <Point X="2.042892760066" Y="27.229356042551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.069181755049" Y="27.605306186025" />
                  <Point X="2.186738679227" Y="29.286448524857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.602221217376" Y="22.289340149194" />
                  <Point X="2.09452241236" Y="29.329575236445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.510425432825" Y="22.338480038052" />
                  <Point X="2.002306145493" Y="29.372701948033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.416247294156" Y="22.35355067576" />
                  <Point X="1.910089874394" Y="29.415828599101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.320444494915" Y="22.34538758484" />
                  <Point X="1.81758454368" Y="29.454821505079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.22459581393" Y="22.336568354424" />
                  <Point X="1.724708200841" Y="29.488508690483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.128281371973" Y="22.32108843178" />
                  <Point X="1.631831858002" Y="29.522195875887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.029152303462" Y="22.265357474154" />
                  <Point X="1.538955515163" Y="29.555883061291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.928041488619" Y="22.181286223635" />
                  <Point X="1.446016382107" Y="29.588672304761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.758911952843" Y="21.124501945841" />
                  <Point X="0.772001147549" Y="21.311686150889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826057913306" Y="22.084733916899" />
                  <Point X="1.352365475258" Y="29.611282708766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.683389188219" Y="21.406356861656" />
                  <Point X="1.258714568409" Y="29.633893112771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.602571506233" Y="21.612490931426" />
                  <Point X="1.16506366156" Y="29.656503516776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.516056065986" Y="21.737143261888" />
                  <Point X="1.071412754712" Y="29.679113920781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.428928017037" Y="21.853034879755" />
                  <Point X="0.977761847863" Y="29.701724324786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.337830733937" Y="21.912163804732" />
                  <Point X="0.884110931755" Y="29.724334596386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.244667504231" Y="21.941748316811" />
                  <Point X="0.790105046539" Y="29.741868573237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.151458417657" Y="21.970677045103" />
                  <Point X="0.695565412368" Y="29.751769584406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.058115255238" Y="21.997688399489" />
                  <Point X="0.601025778196" Y="29.761670595575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.037023640565" Y="21.999019570072" />
                  <Point X="0.506486144024" Y="29.771571606744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.134128522902" Y="21.972235823358" />
                  <Point X="0.411946509853" Y="29.781472617912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.231473087254" Y="21.942024464148" />
                  <Point X="0.295340122312" Y="29.475804353781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.328888113531" Y="21.910805452461" />
                  <Point X="0.177956364909" Y="29.159019182695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.429052076426" Y="21.840274815635" />
                  <Point X="0.078438717804" Y="29.097731292286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.534604961265" Y="21.692679004615" />
                  <Point X="-0.017580219125" Y="29.086477288424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64239238843" Y="21.513127749546" />
                  <Point X="-0.110721355701" Y="29.11637774696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.771226591475" Y="21.03259357684" />
                  <Point X="-0.199768892193" Y="29.204819414105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900087523876" Y="20.55167715654" />
                  <Point X="-0.277495583558" Y="29.455156709136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.017159209482" Y="20.239354820063" />
                  <Point X="-0.353018333525" Y="29.73701183456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.110887589407" Y="20.26085730746" />
                  <Point X="-0.445724189666" Y="29.773137093336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.172094273175" Y="20.747441717703" />
                  <Point X="-0.541741967478" Y="29.761899665634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.254328378234" Y="20.933319993822" />
                  <Point X="-0.637759745289" Y="29.750662237932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.343988998564" Y="21.012994153602" />
                  <Point X="-0.7337775231" Y="29.73942481023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.433718375558" Y="21.09168504728" />
                  <Point X="-0.829795310709" Y="29.728187242422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.523846546728" Y="21.164672918541" />
                  <Point X="-0.925813284204" Y="29.716947016313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.616579189009" Y="21.20041511767" />
                  <Point X="-1.022887248752" Y="29.690605414586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.71123135101" Y="21.208706905904" />
                  <Point X="-1.120023567116" Y="29.663372111741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.806028849702" Y="21.214920282632" />
                  <Point X="-1.21715988548" Y="29.636138808897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900826348393" Y="21.221133659361" />
                  <Point X="-1.314296203845" Y="29.608905506053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.996122500764" Y="21.220215956256" />
                  <Point X="-1.411432522209" Y="29.581672203209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.093810538524" Y="21.185092698461" />
                  <Point X="-1.534980987008" Y="29.176727609085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.19369675101" Y="21.118534077541" />
                  <Point X="-1.636241272239" Y="29.090518832417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.293596392958" Y="21.051783406368" />
                  <Point X="-1.734325339468" Y="29.049732089363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.394128801286" Y="20.975983754377" />
                  <Point X="-1.831012852971" Y="29.028916995062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.498626030259" Y="20.843484525565" />
                  <Point X="-2.406314675301" Y="22.163598404525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.378336062356" Y="22.563711210578" />
                  <Point X="-1.924935902252" Y="29.047635580973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.590495469569" Y="20.891571102299" />
                  <Point X="-2.514670392876" Y="21.975920217978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.468955606894" Y="22.629672115297" />
                  <Point X="-2.016949382911" Y="29.093662270441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.681775349469" Y="20.948088771375" />
                  <Point X="-2.623026066704" Y="21.78824265702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562657649207" Y="22.651551248102" />
                  <Point X="-2.107570053514" Y="29.159607071677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772800435541" Y="21.008250161968" />
                  <Point X="-2.731381740533" Y="21.600565096063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659202769606" Y="22.632772470044" />
                  <Point X="-2.19530987053" Y="29.266749998596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.86316691698" Y="21.077830037598" />
                  <Point X="-2.839737414362" Y="21.412887535106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.758137656203" Y="22.579818443167" />
                  <Point X="-2.294391180226" Y="29.211702023835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953533483756" Y="21.147408692867" />
                  <Point X="-2.948093088191" Y="21.225209974148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857376111915" Y="22.522523175682" />
                  <Point X="-2.393472489923" Y="29.156654049074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956614567628" Y="22.465227908198" />
                  <Point X="-2.49255379962" Y="29.101606074313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.05585302334" Y="22.407932640714" />
                  <Point X="-2.978536558694" Y="23.513609597767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963513625515" Y="23.72844755135" />
                  <Point X="-2.591635106988" Y="29.046558132841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.155091479052" Y="22.350637373229" />
                  <Point X="-3.081687749766" Y="23.400359607743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.050720109196" Y="23.843217500297" />
                  <Point X="-2.764430155053" Y="27.937354587143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753325366851" Y="28.096160457082" />
                  <Point X="-2.691376830333" Y="28.98206580291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.254329934765" Y="22.293342105745" />
                  <Point X="-3.182319281074" Y="23.323142431099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.142744558743" Y="23.889087327364" />
                  <Point X="-2.869064685894" Y="27.802891850157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833676013259" Y="28.308973446769" />
                  <Point X="-2.79200363569" Y="28.904916210508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.353568390477" Y="22.236046838261" />
                  <Point X="-3.282950814268" Y="23.245925227484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237203261654" Y="23.900145709477" />
                  <Point X="-2.968906661543" Y="27.736965845474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.918619872824" Y="28.456100428073" />
                  <Point X="-2.892630441047" Y="28.827766618106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452806846189" Y="22.178751570776" />
                  <Point X="-3.383582352107" Y="23.168707957432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333278092922" Y="23.888092379331" />
                  <Point X="-3.065078377475" Y="27.723527000082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.003563732388" Y="28.603227409377" />
                  <Point X="-2.993257246404" Y="28.750617025703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552045301902" Y="22.121456303292" />
                  <Point X="-3.484213889947" Y="23.091490687379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.42939492603" Y="23.875438394898" />
                  <Point X="-3.366305503386" Y="24.777659172459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.341795646865" Y="25.128166450554" />
                  <Point X="-3.159718159585" Y="27.731995829011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651283757614" Y="22.064161035807" />
                  <Point X="-3.584845427786" Y="23.014273417327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.525511759137" Y="23.862784410464" />
                  <Point X="-3.466660405596" Y="24.704397976205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.431079313882" Y="25.213231293868" />
                  <Point X="-3.252869682975" Y="27.7617477492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.750522213326" Y="22.006865768323" />
                  <Point X="-3.685476965626" Y="22.937056147275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.621628592245" Y="23.850130426031" />
                  <Point X="-3.564096875305" Y="24.672872309166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.523482527714" Y="25.253684539288" />
                  <Point X="-3.441643212281" Y="26.424041275971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.430347944385" Y="26.585571132435" />
                  <Point X="-3.344412189882" Y="27.81450967711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842896904385" Y="22.04772690842" />
                  <Point X="-3.786108503465" Y="22.859838877222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.717745425352" Y="23.837476441598" />
                  <Point X="-3.661147265586" Y="24.646867835164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.616962986733" Y="25.278732460824" />
                  <Point X="-3.547843804615" Y="26.267182816237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514668205572" Y="26.741615986016" />
                  <Point X="-3.435948633331" Y="27.867358316497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.930116031938" Y="22.162316041564" />
                  <Point X="-3.886740041305" Y="22.78262160717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81386225846" Y="23.824822457164" />
                  <Point X="-3.758197658682" Y="24.620863320901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710443445752" Y="25.303780382359" />
                  <Point X="-3.646121285089" Y="26.223630134917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604678703882" Y="26.81628665757" />
                  <Point X="-3.527485082514" Y="27.920206873898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017032473097" Y="22.281233791825" />
                  <Point X="-3.987371579144" Y="22.705404337118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.909979091567" Y="23.812168472731" />
                  <Point X="-3.855248066586" Y="24.594858594873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803923904771" Y="25.328828303894" />
                  <Point X="-3.742690410654" Y="26.204508066991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.695061050037" Y="26.8856396572" />
                  <Point X="-3.619021531697" Y="27.973055431299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102269865899" Y="22.424163052263" />
                  <Point X="-4.088003116983" Y="22.628187067066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006095924675" Y="23.799514488298" />
                  <Point X="-3.95229847449" Y="24.568853868845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.89740436379" Y="25.35387622543" />
                  <Point X="-3.83730034562" Y="26.213403729971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785443396192" Y="26.95499265683" />
                  <Point X="-3.71055798088" Y="28.0259039887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102212757782" Y="23.786860503864" />
                  <Point X="-4.049348882395" Y="24.542849142817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990884822809" Y="25.378924146965" />
                  <Point X="-3.931663621361" Y="26.2258267842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875825742347" Y="27.02434565646" />
                  <Point X="-3.812936672249" Y="27.923701259127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.19832959089" Y="23.774206519431" />
                  <Point X="-4.146399290299" Y="24.516844416789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084365281828" Y="25.403972068501" />
                  <Point X="-4.026026891944" Y="26.238249912181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966208088502" Y="27.093698656089" />
                  <Point X="-3.91757352396" Y="27.789205332136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.294446423997" Y="23.761552534997" />
                  <Point X="-4.243449698203" Y="24.490839690761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.177845740847" Y="25.429019990036" />
                  <Point X="-4.120390161597" Y="26.250673053476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056590434657" Y="27.163051655719" />
                  <Point X="-4.022887347779" Y="27.645028252983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.390563257105" Y="23.748898550564" />
                  <Point X="-4.340500106108" Y="24.464834964733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.271326199866" Y="25.454067911571" />
                  <Point X="-4.214753431249" Y="26.263096194772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146972780811" Y="27.232404655349" />
                  <Point X="-4.131081037455" Y="27.459667173319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.486680090212" Y="23.736244566131" />
                  <Point X="-4.437550514012" Y="24.438830238705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.364806658885" Y="25.479115833107" />
                  <Point X="-4.309116700902" Y="26.275519336067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.58279692332" Y="23.723590581697" />
                  <Point X="-4.534600921916" Y="24.412825512677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.458287117904" Y="25.504163754642" />
                  <Point X="-4.403479970555" Y="26.287942477363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.674916489703" Y="23.768100174622" />
                  <Point X="-4.631651329821" Y="24.386820786649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.551767576923" Y="25.529211676178" />
                  <Point X="-4.497843240207" Y="26.300365618658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748392232964" Y="24.079228859772" />
                  <Point X="-4.728701737725" Y="24.360816060621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.645248035941" Y="25.554259597713" />
                  <Point X="-4.59220650986" Y="26.312788759953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73872849496" Y="25.579307519248" />
                  <Point X="-4.70565527697" Y="26.052276571771" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.534823059082" Y="21.226708984375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.438452972412" Y="21.515724609375" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.7336015625" />
                  <Point X="0.234310150146" Y="21.7460234375" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.04869020462" Y="21.799810546875" />
                  <Point X="-0.2620234375" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.314144866943" Y="21.67708984375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.796524353027" Y="20.204076171875" />
                  <Point X="-0.84774395752" Y="20.012923828125" />
                  <Point X="-0.849336730957" Y="20.013232421875" />
                  <Point X="-1.100259155273" Y="20.0619375" />
                  <Point X="-1.145501220703" Y="20.073576171875" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.316768188477" Y="20.391095703125" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.319245361328" Y="20.5133125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.423133178711" Y="20.8296875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.698149780273" Y="21.01744140625" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.030632080078" Y="20.998978515625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.449100341797" Y="20.59591796875" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.488050048828" Y="20.604666015625" />
                  <Point X="-2.855837646484" Y="20.832390625" />
                  <Point X="-2.918500732422" Y="20.880640625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.623639892578" Y="22.1671796875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.500251464844" Y="22.386330078125" />
                  <Point X="-2.504396240234" Y="22.417810546875" />
                  <Point X="-2.516720947266" Y="22.4339765625" />
                  <Point X="-2.531328857422" Y="22.448583984375" />
                  <Point X="-2.560157958984" Y="22.46280078125" />
                  <Point X="-2.591683837891" Y="22.456529296875" />
                  <Point X="-3.677383544922" Y="21.829701171875" />
                  <Point X="-3.842959472656" Y="21.73410546875" />
                  <Point X="-3.871134521484" Y="21.77112109375" />
                  <Point X="-4.161703613281" Y="22.15287109375" />
                  <Point X="-4.206629882812" Y="22.228205078125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.369559326172" Y="23.418958984375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.144604736328" Y="23.608685546875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651611328" Y="23.649947265625" />
                  <Point X="-3.148654785156" Y="23.678927734375" />
                  <Point X="-3.165344238281" Y="23.69182421875" />
                  <Point X="-3.187643066406" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.590121582031" Y="23.530986328125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.813749511719" Y="23.5438984375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.939279296875" Y="24.0719140625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.791869628906" Y="24.808544921875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.537511474609" Y="24.8816171875" />
                  <Point X="-3.514143066406" Y="24.8978359375" />
                  <Point X="-3.502324462891" Y="24.909353515625" />
                  <Point X="-3.4934375" Y="24.92880078125" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.487109130859" Y="24.988248046875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.50232421875" Y="25.0280859375" />
                  <Point X="-3.518527587891" Y="25.042646484375" />
                  <Point X="-3.541895996094" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.806823730469" Y="25.400849609375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.990887207031" Y="25.50145703125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.893717285156" Y="26.08471875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.921294189453" Y="26.4161015625" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731709716797" Y="26.395865234375" />
                  <Point X="-3.722" Y="26.39892578125" />
                  <Point X="-3.670278076172" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.635226318359" Y="26.453185546875" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.621014404297" Y="26.554537109375" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968017578" Y="26.619220703125" />
                  <Point X="-4.376604492188" Y="27.169115234375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.444605957031" Y="27.299431640625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.096631835938" Y="27.868474609375" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.262355224609" Y="27.986525390625" />
                  <Point X="-3.159157226562" Y="27.926943359375" />
                  <Point X="-3.138515136719" Y="27.92043359375" />
                  <Point X="-3.125" Y="27.919251953125" />
                  <Point X="-3.052965820312" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-3.003659667969" Y="27.936998046875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939256591797" Y="28.04135546875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.269630615234" Y="28.684068359375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.248544921875" Y="28.794306640625" />
                  <Point X="-2.752873046875" Y="29.174333984375" />
                  <Point X="-2.65305078125" Y="29.22979296875" />
                  <Point X="-2.141549316406" Y="29.51397265625" />
                  <Point X="-1.999418334961" Y="29.3287421875" />
                  <Point X="-1.967826293945" Y="29.287572265625" />
                  <Point X="-1.951246826172" Y="29.273662109375" />
                  <Point X="-1.936204589844" Y="29.265830078125" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124023438" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.798141113281" Y="29.228740234375" />
                  <Point X="-1.714634765625" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.680983886719" Y="29.310662109375" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.688020996094" Y="29.692658203125" />
                  <Point X="-1.689137573242" Y="29.701140625" />
                  <Point X="-1.609768066406" Y="29.723392578125" />
                  <Point X="-0.968082763672" Y="29.903296875" />
                  <Point X="-0.847077941895" Y="29.9174609375" />
                  <Point X="-0.224200027466" Y="29.990359375" />
                  <Point X="-0.072669212341" Y="29.424837890625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282129288" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594047546" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.217168014526" Y="29.918166015625" />
                  <Point X="0.236648391724" Y="29.9908671875" />
                  <Point X="0.299931152344" Y="29.984240234375" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="0.960327636719" Y="29.901392578125" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.573348999023" Y="29.74551953125" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="1.994051269531" Y="29.586314453125" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.399583007812" Y="29.389662109375" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.789939697266" Y="29.154859375" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.365973632812" Y="27.739365234375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.221460205078" Y="27.478830078125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.2033671875" Y="27.381357421875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.22546875" Y="27.290810546875" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.284940917969" Y="27.217416015625" />
                  <Point X="2.338248046875" Y="27.181244140625" />
                  <Point X="2.360337158203" Y="27.172978515625" />
                  <Point X="2.371304931641" Y="27.17165625" />
                  <Point X="2.429761962891" Y="27.164607421875" />
                  <Point X="2.461348144531" Y="27.169337890625" />
                  <Point X="2.528950927734" Y="27.187416015625" />
                  <Point X="2.541033691406" Y="27.19241796875" />
                  <Point X="3.797649658203" Y="27.91792578125" />
                  <Point X="3.994247314453" Y="28.0314296875" />
                  <Point X="4.005102783203" Y="28.01634375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.234600585938" Y="27.688984375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.467831054688" Y="26.730599609375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.270242919922" Y="26.571923828125" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.209718994141" Y="26.479341796875" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.193570800781" Y="26.3774375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.223238525391" Y="26.276416015625" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.291949462891" Y="26.192626953125" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.383440917969" Y="26.151654296875" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803955078" Y="26.141171875" />
                  <Point X="4.669504882812" Y="26.29832421875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.854368164063" Y="26.29980078125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.949275390625" Y="25.886599609375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.946551757812" Y="25.292859375" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.714472900391" Y="25.22437109375" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.61349609375" Y="25.15575390625" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.554062011719" Y="25.05947265625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.541406005859" Y="24.944052734375" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.575527832031" Y="24.83006640625" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.65119140625" Y="24.74964453125" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.74116796875" Y="24.699611328125" />
                  <Point X="4.835846191406" Y="24.40629296875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.995791503906" Y="24.3477265625" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.935508789062" Y="23.976970703125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.650169921875" Y="23.871013671875" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.366152587891" Y="23.895423828125" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.168107910156" Y="23.824451171875" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.061873046875" Y="23.65892578125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.072234375" Y="23.4586875" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.184328613281" Y="22.534955078125" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.337635742188" Y="22.41388671875" />
                  <Point X="4.204134277344" Y="22.197861328125" />
                  <Point X="4.177405273438" Y="22.1598828125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="2.965593261719" Y="22.6183046875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.703202636719" Y="22.7528515625" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.460717529297" Y="22.765828125" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.273674072266" Y="22.63695703125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.195328369141" Y="22.419486328125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.886888916016" Y="21.0907421875" />
                  <Point X="2.986673828125" Y="20.91791015625" />
                  <Point X="2.835297851563" Y="20.809787109375" />
                  <Point X="2.805418212891" Y="20.790447265625" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="1.845058837891" Y="21.796943359375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.636879760742" Y="22.0411796875" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805175781" Y="22.16428125" />
                  <Point X="1.388982055664" Y="22.160892578125" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.136898803711" Y="22.10784765625" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.95995526123" Y="21.9148984375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.098927124023" Y="20.28408203125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="0.994368530273" Y="20.0367578125" />
                  <Point X="0.966738586426" Y="20.03173828125" />
                  <Point X="0.860200500488" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#132" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.030528837156" Y="4.469059650747" Z="0.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="-0.859045964258" Y="4.998400702517" Z="0.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.4" />
                  <Point X="-1.629421755161" Y="4.802817165748" Z="0.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.4" />
                  <Point X="-1.743749236756" Y="4.717412958834" Z="0.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.4" />
                  <Point X="-1.73482565947" Y="4.356977565837" Z="0.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.4" />
                  <Point X="-1.823431524828" Y="4.306214334649" Z="0.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.4" />
                  <Point X="-1.919272926231" Y="4.341460754392" Z="0.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.4" />
                  <Point X="-1.965907245416" Y="4.390462892805" Z="0.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.4" />
                  <Point X="-2.683489695037" Y="4.304779848199" Z="0.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.4" />
                  <Point X="-3.285123066913" Y="3.865267279419" Z="0.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.4" />
                  <Point X="-3.319087842277" Y="3.690348398154" Z="0.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.4" />
                  <Point X="-2.995222443605" Y="3.068278669706" Z="0.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.4" />
                  <Point X="-3.045170430908" Y="3.003633136978" Z="0.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.4" />
                  <Point X="-3.126797795901" Y="3.000342255568" Z="0.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.4" />
                  <Point X="-3.243510950143" Y="3.061106106468" Z="0.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.4" />
                  <Point X="-4.1422512232" Y="2.93045837439" Z="0.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.4" />
                  <Point X="-4.49394420981" Y="2.355917798171" Z="0.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.4" />
                  <Point X="-4.413198389528" Y="2.16072827957" Z="0.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.4" />
                  <Point X="-3.671520745474" Y="1.562729755556" Z="0.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.4" />
                  <Point X="-3.687576091951" Y="1.50360061398" Z="0.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.4" />
                  <Point X="-3.743191945802" Y="1.477892230536" Z="0.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.4" />
                  <Point X="-3.920923908327" Y="1.496953825991" Z="0.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.4" />
                  <Point X="-4.94813293892" Y="1.12907711649" Z="0.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.4" />
                  <Point X="-5.046504362885" Y="0.540055446174" Z="0.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.4" />
                  <Point X="-4.825921179415" Y="0.38383415917" Z="0.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.4" />
                  <Point X="-3.553192267622" Y="0.032849980732" Z="0.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.4" />
                  <Point X="-3.541018236225" Y="0.004708911587" Z="0.4" />
                  <Point X="-3.539556741714" Y="0" Z="0.4" />
                  <Point X="-3.54734636491" Y="-0.025098039474" Z="0.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.4" />
                  <Point X="-3.572176327501" Y="-0.046026002402" Z="0.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.4" />
                  <Point X="-3.810966609357" Y="-0.111877897586" Z="0.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.4" />
                  <Point X="-4.994931317408" Y="-0.903883110755" Z="0.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.4" />
                  <Point X="-4.868350479233" Y="-1.437195074712" Z="0.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.4" />
                  <Point X="-4.589751741561" Y="-1.487305245705" Z="0.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.4" />
                  <Point X="-3.196860032493" Y="-1.319987443863" Z="0.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.4" />
                  <Point X="-3.199163643371" Y="-1.347497671794" Z="0.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.4" />
                  <Point X="-3.40615329321" Y="-1.510091949363" Z="0.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.4" />
                  <Point X="-4.255729323935" Y="-2.766124222808" Z="0.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.4" />
                  <Point X="-3.917076378774" Y="-3.227880864227" Z="0.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.4" />
                  <Point X="-3.658539159899" Y="-3.182319953662" Z="0.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.4" />
                  <Point X="-2.558231696556" Y="-2.570098772576" Z="0.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.4" />
                  <Point X="-2.673097059506" Y="-2.776539222593" Z="0.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.4" />
                  <Point X="-2.955160636368" Y="-4.127696763658" Z="0.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.4" />
                  <Point X="-2.520527631943" Y="-4.406564695894" Z="0.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.4" />
                  <Point X="-2.415588695114" Y="-4.403239213951" Z="0.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.4" />
                  <Point X="-2.009009591475" Y="-4.011315089263" Z="0.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.4" />
                  <Point X="-1.70757584091" Y="-4.001170266431" Z="0.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.4" />
                  <Point X="-1.462256605416" Y="-4.176623330081" Z="0.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.4" />
                  <Point X="-1.374440680063" Y="-4.46516012784" Z="0.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.4" />
                  <Point X="-1.372496427572" Y="-4.571095842946" Z="0.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.4" />
                  <Point X="-1.164116177721" Y="-4.943564378934" Z="0.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.4" />
                  <Point X="-0.865020114528" Y="-5.004571754218" Z="0.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="-0.7543841194" Y="-4.777583976523" Z="0.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="-0.279224833474" Y="-3.320140297226" Z="0.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="-0.040025747755" Y="-3.216661883454" Z="0.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.4" />
                  <Point X="0.213333331606" Y="-3.270450161834" Z="0.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.4" />
                  <Point X="0.391221025958" Y="-3.481505458542" Z="0.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.4" />
                  <Point X="0.480370741001" Y="-3.754952053292" Z="0.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.4" />
                  <Point X="0.969519659796" Y="-4.986177060929" Z="0.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.4" />
                  <Point X="1.148767733789" Y="-4.947955403336" Z="0.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.4" />
                  <Point X="1.14234356295" Y="-4.678111169199" Z="0.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.4" />
                  <Point X="1.002658426557" Y="-3.064439993946" Z="0.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.4" />
                  <Point X="1.162709863562" Y="-2.899317284961" Z="0.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.4" />
                  <Point X="1.387407344699" Y="-2.857615085432" Z="0.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.4" />
                  <Point X="1.603684588297" Y="-2.969599015139" Z="0.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.4" />
                  <Point X="1.799235186664" Y="-3.202213030684" Z="0.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.4" />
                  <Point X="2.82643156981" Y="-4.220247442195" Z="0.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.4" />
                  <Point X="3.016616562522" Y="-4.086469021107" Z="0.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.4" />
                  <Point X="2.924034301514" Y="-3.852976421443" Z="0.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.4" />
                  <Point X="2.238376418643" Y="-2.540346217782" Z="0.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.4" />
                  <Point X="2.311764361318" Y="-2.355050505131" Z="0.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.4" />
                  <Point X="2.477847873242" Y="-2.247136948649" Z="0.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.4" />
                  <Point X="2.688160585731" Y="-2.265071590848" Z="0.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.4" />
                  <Point X="2.934437175664" Y="-2.39371519876" Z="0.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.4" />
                  <Point X="4.212137738877" Y="-2.837613507429" Z="0.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.4" />
                  <Point X="4.374012850382" Y="-2.58111729211" Z="0.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.4" />
                  <Point X="4.208610708218" Y="-2.394095976762" Z="0.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.4" />
                  <Point X="3.108136419019" Y="-1.482993325571" Z="0.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.4" />
                  <Point X="3.105506592394" Y="-1.314375849207" Z="0.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.4" />
                  <Point X="3.200398125633" Y="-1.176235696724" Z="0.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.4" />
                  <Point X="3.370616269366" Y="-1.122154921547" Z="0.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.4" />
                  <Point X="3.637487871427" Y="-1.147278468694" Z="0.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.4" />
                  <Point X="4.978099850241" Y="-1.002874077761" Z="0.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.4" />
                  <Point X="5.039077108073" Y="-0.628445224462" Z="0.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.4" />
                  <Point X="4.842630844309" Y="-0.514128748239" Z="0.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.4" />
                  <Point X="3.670057471473" Y="-0.175785798747" Z="0.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.4" />
                  <Point X="3.608705577778" Y="-0.107784111933" Z="0.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.4" />
                  <Point X="3.584357678071" Y="-0.015262562411" Z="0.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.4" />
                  <Point X="3.597013772353" Y="0.081347968827" Z="0.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.4" />
                  <Point X="3.646673860622" Y="0.156164626642" Z="0.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.4" />
                  <Point X="3.73333794288" Y="0.212363045495" Z="0.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.4" />
                  <Point X="3.953336972998" Y="0.275843184529" Z="0.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.4" />
                  <Point X="4.992524140223" Y="0.925570785492" Z="0.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.4" />
                  <Point X="4.896793203092" Y="1.342908179594" Z="0.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.4" />
                  <Point X="4.656822448593" Y="1.379177854064" Z="0.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.4" />
                  <Point X="3.38383617128" Y="1.232502593841" Z="0.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.4" />
                  <Point X="3.310320627854" Y="1.267477815238" Z="0.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.4" />
                  <Point X="3.258853143836" Y="1.335176672761" Z="0.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.4" />
                  <Point X="3.236383381685" Y="1.418820907933" Z="0.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.4" />
                  <Point X="3.251715625027" Y="1.497154826835" Z="0.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.4" />
                  <Point X="3.303769712976" Y="1.572786323576" Z="0.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.4" />
                  <Point X="3.492113198361" Y="1.722211632384" Z="0.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.4" />
                  <Point X="4.271222067695" Y="2.746151598906" Z="0.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.4" />
                  <Point X="4.039532322732" Y="3.076828126456" Z="0.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.4" />
                  <Point X="3.766493983716" Y="2.992506349036" Z="0.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.4" />
                  <Point X="2.442275305261" Y="2.248920947927" Z="0.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.4" />
                  <Point X="2.37113455072" Y="2.252578018927" Z="0.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.4" />
                  <Point X="2.306859603296" Y="2.290071625797" Z="0.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.4" />
                  <Point X="2.260686974438" Y="2.350165257086" Z="0.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.4" />
                  <Point X="2.246851683832" Y="2.418623893113" Z="0.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.4" />
                  <Point X="2.26360699473" Y="2.497194270621" Z="0.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.4" />
                  <Point X="2.403119003823" Y="2.745645098239" Z="0.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.4" />
                  <Point X="2.81276079731" Y="4.226887459588" Z="0.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.4" />
                  <Point X="2.418597040398" Y="4.464145982215" Z="0.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.4" />
                  <Point X="2.009076708665" Y="4.662886855183" Z="0.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.4" />
                  <Point X="1.584241568022" Y="4.823804817821" Z="0.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.4" />
                  <Point X="0.965906055851" Y="4.981276723544" Z="0.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.4" />
                  <Point X="0.298983015673" Y="5.065249392201" Z="0.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.4" />
                  <Point X="0.162715724397" Y="4.962387855204" Z="0.4" />
                  <Point X="0" Y="4.355124473572" Z="0.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>