<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#173" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2146" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004717773438" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.784295166016" Y="20.66271875" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557719726562" Y="21.502861328125" />
                  <Point X="0.542363098145" Y="21.532623046875" />
                  <Point X="0.452037628174" Y="21.662763671875" />
                  <Point X="0.378635437012" Y="21.7685234375" />
                  <Point X="0.356752746582" Y="21.7909765625" />
                  <Point X="0.330497711182" Y="21.810220703125" />
                  <Point X="0.302494659424" Y="21.824330078125" />
                  <Point X="0.162721191406" Y="21.867708984375" />
                  <Point X="0.049135520935" Y="21.902962890625" />
                  <Point X="0.020983409882" Y="21.907232421875" />
                  <Point X="-0.008658220291" Y="21.907234375" />
                  <Point X="-0.036824142456" Y="21.90296484375" />
                  <Point X="-0.176597457886" Y="21.859583984375" />
                  <Point X="-0.290183258057" Y="21.82433203125" />
                  <Point X="-0.318184509277" Y="21.810224609375" />
                  <Point X="-0.344439849854" Y="21.79098046875" />
                  <Point X="-0.366323608398" Y="21.7685234375" />
                  <Point X="-0.456649078369" Y="21.638380859375" />
                  <Point X="-0.530051269531" Y="21.532623046875" />
                  <Point X="-0.538188781738" Y="21.518427734375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.730578063965" Y="20.817244140625" />
                  <Point X="-0.916584960938" Y="20.12305859375" />
                  <Point X="-0.949240966797" Y="20.129396484375" />
                  <Point X="-1.079353149414" Y="20.15465234375" />
                  <Point X="-1.238246337891" Y="20.195533203125" />
                  <Point X="-1.246418334961" Y="20.19763671875" />
                  <Point X="-1.241565551758" Y="20.23449609375" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.249900512695" Y="20.651646484375" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937744141" Y="20.817033203125" />
                  <Point X="-1.304009521484" Y="20.84487109375" />
                  <Point X="-1.323644897461" Y="20.868796875" />
                  <Point X="-1.452330566406" Y="20.981650390625" />
                  <Point X="-1.556905761719" Y="21.073361328125" />
                  <Point X="-1.583188720703" Y="21.089705078125" />
                  <Point X="-1.612885742188" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.813822387695" Y="21.1202265625" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463012695" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.184972412109" Y="21.010107421875" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.435927734375" Y="20.769138671875" />
                  <Point X="-2.480147216797" Y="20.71151171875" />
                  <Point X="-2.611014404297" Y="20.792541015625" />
                  <Point X="-2.801728759766" Y="20.910625" />
                  <Point X="-3.021744384766" Y="21.08003125" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.834452148438" Y="21.61204296875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575439453" Y="22.41480859375" />
                  <Point X="-2.414560546875" Y="22.444427734375" />
                  <Point X="-2.428778564453" Y="22.4732578125" />
                  <Point X="-2.446807373047" Y="22.498416015625" />
                  <Point X="-2.464156005859" Y="22.515763671875" />
                  <Point X="-2.489316650391" Y="22.533791015625" />
                  <Point X="-2.518145507812" Y="22.548005859375" />
                  <Point X="-2.547761962891" Y="22.55698828125" />
                  <Point X="-2.578693847656" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.217245605469" Y="22.205056640625" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.932200927734" Y="22.008203125" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.240606445313" Y="22.470654296875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.824530029297" Y="22.950103515625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556152344" Y="23.67201171875" />
                  <Point X="-3.052556640625" Y="23.701755859375" />
                  <Point X="-3.068637451172" Y="23.72773828125" />
                  <Point X="-3.089468994141" Y="23.7516953125" />
                  <Point X="-3.112970458984" Y="23.77123046875" />
                  <Point X="-3.139458251953" Y="23.7868203125" />
                  <Point X="-3.168728759766" Y="23.798046875" />
                  <Point X="-3.200612792969" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.961677734375" Y="23.70954296875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.775152832031" Y="23.776658203125" />
                  <Point X="-4.834078125" Y="24.007349609375" />
                  <Point X="-4.875812988281" Y="24.29915625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.351441894531" Y="24.560255859375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.509926513672" Y="24.789103515625" />
                  <Point X="-3.479049316406" Y="24.80694921875" />
                  <Point X="-3.465885498047" Y="24.81612109375" />
                  <Point X="-3.441624267578" Y="24.8362734375" />
                  <Point X="-3.4255546875" Y="24.85339453125" />
                  <Point X="-3.414175048828" Y="24.87393359375" />
                  <Point X="-3.401645507812" Y="24.9051171875" />
                  <Point X="-3.397150878906" Y="24.919513671875" />
                  <Point X="-3.390755859375" Y="24.947697265625" />
                  <Point X="-3.388401611328" Y="24.969087890625" />
                  <Point X="-3.390922363281" Y="24.9904609375" />
                  <Point X="-3.398272949219" Y="25.0217265625" />
                  <Point X="-3.40291015625" Y="25.036162109375" />
                  <Point X="-3.414484130859" Y="25.064263671875" />
                  <Point X="-3.426039794922" Y="25.084703125" />
                  <Point X="-3.442255126953" Y="25.10168359375" />
                  <Point X="-3.469386230469" Y="25.123828125" />
                  <Point X="-3.482654541016" Y="25.13290234375" />
                  <Point X="-3.510661865234" Y="25.1487578125" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.198076660156" Y="25.33608984375" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.862462890625" Y="25.720341796875" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.740475097656" Y="26.287005859375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.352434570312" Y="26.377041015625" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744985351562" Y="26.299341796875" />
                  <Point X="-3.723422851562" Y="26.301228515625" />
                  <Point X="-3.703139160156" Y="26.305263671875" />
                  <Point X="-3.669251708984" Y="26.315947265625" />
                  <Point X="-3.641713134766" Y="26.324630859375" />
                  <Point X="-3.622776123047" Y="26.332962890625" />
                  <Point X="-3.604032226562" Y="26.34378515625" />
                  <Point X="-3.587350585938" Y="26.356017578125" />
                  <Point X="-3.573712402344" Y="26.3715703125" />
                  <Point X="-3.561298339844" Y="26.38930078125" />
                  <Point X="-3.5513515625" Y="26.407431640625" />
                  <Point X="-3.53775390625" Y="26.4402578125" />
                  <Point X="-3.526704101563" Y="26.466935546875" />
                  <Point X="-3.520915283203" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.54845703125" Y="26.62089453125" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.983696289063" Y="26.987373046875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.228709960938" Y="27.480861328125" />
                  <Point X="-4.08115625" Y="27.733654296875" />
                  <Point X="-3.858607910156" Y="28.019708984375" />
                  <Point X="-3.750504638672" Y="28.15866015625" />
                  <Point X="-3.567033203125" Y="28.052734375" />
                  <Point X="-3.206656738281" Y="27.844669921875" />
                  <Point X="-3.187729003906" Y="27.836341796875" />
                  <Point X="-3.167086669922" Y="27.82983203125" />
                  <Point X="-3.146793945312" Y="27.825794921875" />
                  <Point X="-3.099597900391" Y="27.821666015625" />
                  <Point X="-3.061244628906" Y="27.818310546875" />
                  <Point X="-3.040560546875" Y="27.81876171875" />
                  <Point X="-3.019102294922" Y="27.821587890625" />
                  <Point X="-2.999013183594" Y="27.826505859375" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962210205078" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.912577880859" Y="27.893728515625" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.847564941406" Y="28.08331640625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.038876220703" Y="28.474388671875" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.957617431641" Y="28.8976484375" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.350125488281" Y="29.2894140625" />
                  <Point X="-2.167036621094" Y="29.391134765625" />
                  <Point X="-2.153517822266" Y="29.373515625" />
                  <Point X="-2.043195556641" Y="29.2297421875" />
                  <Point X="-2.028892944336" Y="29.21480078125" />
                  <Point X="-2.012313964844" Y="29.200888671875" />
                  <Point X="-1.995114746094" Y="29.189396484375" />
                  <Point X="-1.9425859375" Y="29.16205078125" />
                  <Point X="-1.899898681641" Y="29.139828125" />
                  <Point X="-1.880625854492" Y="29.13233203125" />
                  <Point X="-1.859719360352" Y="29.126728515625" />
                  <Point X="-1.83926940918" Y="29.123580078125" />
                  <Point X="-1.818622802734" Y="29.12493359375" />
                  <Point X="-1.797307495117" Y="29.128693359375" />
                  <Point X="-1.777452636719" Y="29.134482421875" />
                  <Point X="-1.722740112305" Y="29.157146484375" />
                  <Point X="-1.678278686523" Y="29.1755625" />
                  <Point X="-1.660147705078" Y="29.185509765625" />
                  <Point X="-1.642417602539" Y="29.197923828125" />
                  <Point X="-1.626865112305" Y="29.2115625" />
                  <Point X="-1.614633422852" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265919921875" />
                  <Point X="-1.577672363281" Y="29.3223984375" />
                  <Point X="-1.563200927734" Y="29.368296875" />
                  <Point X="-1.559165405273" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.57695300293" Y="29.5768359375" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.282327270508" Y="29.71653125" />
                  <Point X="-0.949623718262" Y="29.80980859375" />
                  <Point X="-0.524727539062" Y="29.8595390625" />
                  <Point X="-0.294711578369" Y="29.886458984375" />
                  <Point X="-0.240512741089" Y="29.684185546875" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271453857" Y="29.231396484375" />
                  <Point X="-0.082113960266" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155897141" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425964355" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.232850479126" Y="29.609640625" />
                  <Point X="0.307419616699" Y="29.887939453125" />
                  <Point X="0.553545349121" Y="29.862162109375" />
                  <Point X="0.84403125" Y="29.831740234375" />
                  <Point X="1.195583129883" Y="29.746865234375" />
                  <Point X="1.481039794922" Y="29.6779453125" />
                  <Point X="1.709182861328" Y="29.595197265625" />
                  <Point X="1.894646850586" Y="29.527927734375" />
                  <Point X="2.115905761719" Y="29.424453125" />
                  <Point X="2.294555908203" Y="29.34090234375" />
                  <Point X="2.508348876953" Y="29.21634765625" />
                  <Point X="2.680974609375" Y="29.115775390625" />
                  <Point X="2.882571289062" Y="28.97241015625" />
                  <Point X="2.943259033203" Y="28.929251953125" />
                  <Point X="2.622924804688" Y="28.37441796875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.121232421875" Y="27.47176171875" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.112346435547" Y="27.34265234375" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442138672" Y="27.289603515625" />
                  <Point X="2.129708007812" Y="27.267515625" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.163770019531" Y="27.212544921875" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.194465820313" Y="27.170328125" />
                  <Point X="2.221597167969" Y="27.14559375" />
                  <Point X="2.256523193359" Y="27.12189453125" />
                  <Point X="2.284905761719" Y="27.102634765625" />
                  <Point X="2.304945556641" Y="27.0922734375" />
                  <Point X="2.327033691406" Y="27.084005859375" />
                  <Point X="2.348965576172" Y="27.078662109375" />
                  <Point X="2.387265869141" Y="27.074044921875" />
                  <Point X="2.418390380859" Y="27.070291015625" />
                  <Point X="2.436467285156" Y="27.06984375" />
                  <Point X="2.473208496094" Y="27.074169921875" />
                  <Point X="2.517500976563" Y="27.086015625" />
                  <Point X="2.553494873047" Y="27.095640625" />
                  <Point X="2.565285644531" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.257597412109" Y="27.4964296875" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="4.020873535156" Y="27.831771484375" />
                  <Point X="4.123273925781" Y="27.689458984375" />
                  <Point X="4.235657714844" Y="27.503744140625" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.856618164062" Y="27.148671875" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221421386719" Y="26.66023828125" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.172096923828" Y="26.600041015625" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.136606689453" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.109755615234" Y="26.474626953125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.107487060547" Y="26.324525390625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136283203125" Y="26.235740234375" />
                  <Point X="3.162795654297" Y="26.195443359375" />
                  <Point X="3.184340820312" Y="26.1626953125" />
                  <Point X="3.198891113281" Y="26.145453125" />
                  <Point X="3.216133789062" Y="26.12936328125" />
                  <Point X="3.234346435547" Y="26.11603515625" />
                  <Point X="3.272766601562" Y="26.094408203125" />
                  <Point X="3.303988525391" Y="26.07683203125" />
                  <Point X="3.320522705078" Y="26.0695" />
                  <Point X="3.356119628906" Y="26.0594375" />
                  <Point X="3.408066162109" Y="26.052572265625" />
                  <Point X="3.450280273438" Y="26.046994140625" />
                  <Point X="3.462698974609" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.123769042969" Y="26.130658203125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.801956542969" Y="26.113462890625" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.881352050781" Y="25.705337890625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.433799804688" Y="25.52176953125" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788818359" Y="25.325583984375" />
                  <Point X="3.681547851562" Y="25.315068359375" />
                  <Point X="3.630511962891" Y="25.2855703125" />
                  <Point X="3.589037841797" Y="25.26159765625" />
                  <Point X="3.574308105469" Y="25.25109375" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.516908691406" Y="25.186556640625" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.48030078125" Y="25.13556640625" />
                  <Point X="3.47052734375" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.453473632812" Y="25.0393046875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443483154297" Y="24.978123046875" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.455386230469" Y="24.8881484375" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.522645996094" Y="24.743572265625" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559994873047" Y="24.698767578125" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.640071533203" Y="24.64634375" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692708496094" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.299422363281" Y="24.45167578125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.879579101562" Y="24.214150390625" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.809648925781" Y="23.852439453125" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.256155273438" Y="23.8870546875" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.274493652344" Y="23.97271875" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163974609375" Y="23.94340234375" />
                  <Point X="3.136147705078" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.051853271484" Y="23.83322265625" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.961080078125" Y="23.6003359375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.031883789062" Y="23.34578125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.651510009766" Y="22.824056640625" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.194031738281" Y="22.362224609375" />
                  <Point X="4.124815429687" Y="22.250220703125" />
                  <Point X="4.030974121094" Y="22.11688671875" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.541760986328" Y="22.395349609375" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224121094" Y="22.840173828125" />
                  <Point X="2.635010986328" Y="22.861703125" />
                  <Point X="2.538133789062" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.345796875" Y="22.812701171875" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242385009766" Y="22.753451171875" />
                  <Point X="2.221425537109" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.152409423828" Y="22.6105234375" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.436740234375" />
                  <Point X="2.117205322266" Y="22.31752734375" />
                  <Point X="2.134701171875" Y="22.2206484375" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.499389892578" Y="21.57191015625" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781837402344" Y="20.88834765625" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="2.323849853516" Y="21.329025390625" />
                  <Point X="1.758546264648" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.60434753418" Y="22.175033203125" />
                  <Point X="1.508800292969" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417100585938" Y="22.258880859375" />
                  <Point X="1.288510864258" Y="22.247048828125" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156362670898" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104595092773" Y="22.204537109375" />
                  <Point X="1.005301513672" Y="22.1219765625" />
                  <Point X="0.92461138916" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887248962402" Y="22.003310546875" />
                  <Point X="0.875624267578" Y="21.974189453125" />
                  <Point X="0.845936035156" Y="21.8376015625" />
                  <Point X="0.821809997559" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.918241210938" Y="20.92870703125" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.97570916748" Y="20.129923828125" />
                  <Point X="0.929315551758" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058442993164" Y="20.2473671875" />
                  <Point X="-1.141246459961" Y="20.268669921875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.156725952148" Y="20.6701796875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.82152734375" />
                  <Point X="-1.199026000977" Y="20.850494140625" />
                  <Point X="-1.205664794922" Y="20.864533203125" />
                  <Point X="-1.221736572266" Y="20.89237109375" />
                  <Point X="-1.230573486328" Y="20.905138671875" />
                  <Point X="-1.250208862305" Y="20.929064453125" />
                  <Point X="-1.261007324219" Y="20.94022265625" />
                  <Point X="-1.389692871094" Y="21.053076171875" />
                  <Point X="-1.494268188477" Y="21.144787109375" />
                  <Point X="-1.506739379883" Y="21.15403515625" />
                  <Point X="-1.533022338867" Y="21.17037890625" />
                  <Point X="-1.546834106445" Y="21.177474609375" />
                  <Point X="-1.57653112793" Y="21.189775390625" />
                  <Point X="-1.591315795898" Y="21.194525390625" />
                  <Point X="-1.621457763672" Y="21.201552734375" />
                  <Point X="-1.636815063477" Y="21.203830078125" />
                  <Point X="-1.807609619141" Y="21.2150234375" />
                  <Point X="-1.946404296875" Y="21.22412109375" />
                  <Point X="-1.961928344727" Y="21.2238671875" />
                  <Point X="-1.992725585938" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047607422" Y="21.209736328125" />
                  <Point X="-2.053666748047" Y="21.204505859375" />
                  <Point X="-2.081861328125" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.237751708984" Y="21.08909765625" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359685302734" Y="21.0072421875" />
                  <Point X="-2.380448242188" Y="20.98986328125" />
                  <Point X="-2.402761962891" Y="20.967224609375" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.503200195312" Y="20.837521484375" />
                  <Point X="-2.561003417969" Y="20.8733125" />
                  <Point X="-2.747615234375" Y="20.98885546875" />
                  <Point X="-2.963786621094" Y="21.155302734375" />
                  <Point X="-2.980862792969" Y="21.168451171875" />
                  <Point X="-2.7521796875" Y="21.56454296875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626220703" Y="22.411701171875" />
                  <Point X="-2.314666259766" Y="22.44238671875" />
                  <Point X="-2.323651367188" Y="22.472005859375" />
                  <Point X="-2.329358398438" Y="22.486447265625" />
                  <Point X="-2.343576416016" Y="22.51527734375" />
                  <Point X="-2.351559082031" Y="22.52859375" />
                  <Point X="-2.369587890625" Y="22.553751953125" />
                  <Point X="-2.379634033203" Y="22.56559375" />
                  <Point X="-2.396982666016" Y="22.58294140625" />
                  <Point X="-2.408825683594" Y="22.59298828125" />
                  <Point X="-2.433986328125" Y="22.611015625" />
                  <Point X="-2.447303955078" Y="22.61899609375" />
                  <Point X="-2.4761328125" Y="22.6332109375" />
                  <Point X="-2.490572998047" Y="22.638916015625" />
                  <Point X="-2.520189453125" Y="22.6478984375" />
                  <Point X="-2.550873535156" Y="22.6519375" />
                  <Point X="-2.581805419922" Y="22.650923828125" />
                  <Point X="-2.597229492188" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.264745605469" Y="22.287328125" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.856607666016" Y="22.0657421875" />
                  <Point X="-4.004022705078" Y="22.259416015625" />
                  <Point X="-4.159013671875" Y="22.5193125" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.766697753906" Y="22.874734375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950786376953" Y="23.67862109375" />
                  <Point X="-2.953082763672" Y="23.693775390625" />
                  <Point X="-2.960083251953" Y="23.72351953125" />
                  <Point X="-2.971776611328" Y="23.751751953125" />
                  <Point X="-2.987857421875" Y="23.777734375" />
                  <Point X="-2.996948974609" Y="23.79007421875" />
                  <Point X="-3.017780517578" Y="23.81403125" />
                  <Point X="-3.0287421875" Y="23.824751953125" />
                  <Point X="-3.052243652344" Y="23.844287109375" />
                  <Point X="-3.064783447266" Y="23.8531015625" />
                  <Point X="-3.091271240234" Y="23.86869140625" />
                  <Point X="-3.105437988281" Y="23.87551953125" />
                  <Point X="-3.134708496094" Y="23.88674609375" />
                  <Point X="-3.149812255859" Y="23.89114453125" />
                  <Point X="-3.181696289062" Y="23.897623046875" />
                  <Point X="-3.19730859375" Y="23.89946875" />
                  <Point X="-3.228625" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.974077636719" Y="23.80373046875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.683107910156" Y="23.800169921875" />
                  <Point X="-4.740762695312" Y="24.02588671875" />
                  <Point X="-4.781770019531" Y="24.312607421875" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.326854003906" Y="24.4684921875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.496495849609" Y="24.69183203125" />
                  <Point X="-3.473546386719" Y="24.701345703125" />
                  <Point X="-3.462389160156" Y="24.706853515625" />
                  <Point X="-3.431511962891" Y="24.72469921875" />
                  <Point X="-3.424740722656" Y="24.72900390625" />
                  <Point X="-3.405184326172" Y="24.74304296875" />
                  <Point X="-3.380923095703" Y="24.7631953125" />
                  <Point X="-3.372355712891" Y="24.771259765625" />
                  <Point X="-3.356286132812" Y="24.788380859375" />
                  <Point X="-3.342456542969" Y="24.807353515625" />
                  <Point X="-3.331076904297" Y="24.827892578125" />
                  <Point X="-3.326024658203" Y="24.838515625" />
                  <Point X="-3.313495117188" Y="24.86969921875" />
                  <Point X="-3.310962158203" Y="24.8768046875" />
                  <Point X="-3.304505859375" Y="24.8984921875" />
                  <Point X="-3.298110839844" Y="24.92667578125" />
                  <Point X="-3.296326171875" Y="24.9373046875" />
                  <Point X="-3.293971923828" Y="24.9586953125" />
                  <Point X="-3.294055419922" Y="24.98021484375" />
                  <Point X="-3.296576171875" Y="25.001587890625" />
                  <Point X="-3.298443847656" Y="25.012203125" />
                  <Point X="-3.305794433594" Y="25.04346875" />
                  <Point X="-3.307825195312" Y="25.05078125" />
                  <Point X="-3.315068847656" Y="25.07233984375" />
                  <Point X="-3.326642822266" Y="25.10044140625" />
                  <Point X="-3.331785644531" Y="25.111017578125" />
                  <Point X="-3.343341308594" Y="25.13145703125" />
                  <Point X="-3.357334472656" Y="25.1503125" />
                  <Point X="-3.373549804688" Y="25.16729296875" />
                  <Point X="-3.382184814453" Y="25.17528125" />
                  <Point X="-3.409315917969" Y="25.19742578125" />
                  <Point X="-3.4157578125" Y="25.202244140625" />
                  <Point X="-3.435852539062" Y="25.21557421875" />
                  <Point X="-3.463859863281" Y="25.2314296875" />
                  <Point X="-3.474677490234" Y="25.2366796875" />
                  <Point X="-3.496891601562" Y="25.245771484375" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.173488769531" Y="25.427853515625" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.768486328125" Y="25.706435546875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.648782226562" Y="26.262158203125" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-4.364834960938" Y="26.282853515625" />
                  <Point X="-3.778066894531" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747057373047" Y="26.204365234375" />
                  <Point X="-3.736704589844" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704887207031" Y="26.2080546875" />
                  <Point X="-3.684603515625" Y="26.21208984375" />
                  <Point X="-3.674574707031" Y="26.21466015625" />
                  <Point X="-3.640687255859" Y="26.22534375" />
                  <Point X="-3.613148681641" Y="26.23402734375" />
                  <Point X="-3.603453857422" Y="26.23767578125" />
                  <Point X="-3.584516845703" Y="26.2460078125" />
                  <Point X="-3.575274658203" Y="26.25069140625" />
                  <Point X="-3.556530761719" Y="26.261513671875" />
                  <Point X="-3.547854980469" Y="26.26717578125" />
                  <Point X="-3.531173339844" Y="26.279408203125" />
                  <Point X="-3.515923095703" Y="26.2933828125" />
                  <Point X="-3.502284912109" Y="26.308935546875" />
                  <Point X="-3.495891113281" Y="26.317083984375" />
                  <Point X="-3.483477050781" Y="26.334814453125" />
                  <Point X="-3.478009033203" Y="26.343607421875" />
                  <Point X="-3.468062255859" Y="26.36173828125" />
                  <Point X="-3.463583496094" Y="26.371076171875" />
                  <Point X="-3.449985839844" Y="26.40390234375" />
                  <Point X="-3.438936035156" Y="26.430580078125" />
                  <Point X="-3.435499755859" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358398438" Y="26.470298828125" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012695312" Y="26.60453125" />
                  <Point X="-3.443509277344" Y="26.62380859375" />
                  <Point X="-3.447783935547" Y="26.6332421875" />
                  <Point X="-3.464190917969" Y="26.664759765625" />
                  <Point X="-3.477523681641" Y="26.69037109375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494291748047" Y="26.716484375" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.925863769531" Y="27.0627421875" />
                  <Point X="-4.227614746094" Y="27.294283203125" />
                  <Point X="-4.146663574219" Y="27.43297265625" />
                  <Point X="-4.002295654297" Y="27.680306640625" />
                  <Point X="-3.783627197266" Y="27.961375" />
                  <Point X="-3.726338623047" Y="28.03501171875" />
                  <Point X="-3.614532714844" Y="27.9704609375" />
                  <Point X="-3.25415625" Y="27.762396484375" />
                  <Point X="-3.244916503906" Y="27.75771484375" />
                  <Point X="-3.225988769531" Y="27.74938671875" />
                  <Point X="-3.216301025391" Y="27.745740234375" />
                  <Point X="-3.195658691406" Y="27.73923046875" />
                  <Point X="-3.185623046875" Y="27.736658203125" />
                  <Point X="-3.165330322266" Y="27.73262109375" />
                  <Point X="-3.155073242188" Y="27.73115625" />
                  <Point X="-3.107877197266" Y="27.72702734375" />
                  <Point X="-3.069523925781" Y="27.723671875" />
                  <Point X="-3.059172851562" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155761719" Y="27.72457421875" />
                  <Point X="-3.006697509766" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423583984" Y="27.73423046875" />
                  <Point X="-2.956998779297" Y="27.74130078125" />
                  <Point X="-2.938449951172" Y="27.750447265625" />
                  <Point X="-2.929421630859" Y="27.755529296875" />
                  <Point X="-2.911167480469" Y="27.767158203125" />
                  <Point X="-2.902748291016" Y="27.77319140625" />
                  <Point X="-2.886615966797" Y="27.786138671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.845402832031" Y="27.826552734375" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752926513672" Y="28.091595703125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.956604003906" Y="28.521888671875" />
                  <Point X="-3.059387695312" Y="28.6999140625" />
                  <Point X="-2.899815185547" Y="28.8222578125" />
                  <Point X="-2.648378662109" Y="29.01503125" />
                  <Point X="-2.303987792969" Y="29.206369140625" />
                  <Point X="-2.192524658203" Y="29.268296875" />
                  <Point X="-2.118565917969" Y="29.171912109375" />
                  <Point X="-2.111821777344" Y="29.16405078125" />
                  <Point X="-2.097519042969" Y="29.149109375" />
                  <Point X="-2.089959472656" Y="29.14202734375" />
                  <Point X="-2.073380615234" Y="29.128115234375" />
                  <Point X="-2.065093261719" Y="29.1218984375" />
                  <Point X="-2.047893920898" Y="29.11040625" />
                  <Point X="-2.038982055664" Y="29.105130859375" />
                  <Point X="-1.98645324707" Y="29.07778515625" />
                  <Point X="-1.943765991211" Y="29.0555625" />
                  <Point X="-1.934335571289" Y="29.0512890625" />
                  <Point X="-1.9150625" Y="29.04379296875" />
                  <Point X="-1.905220336914" Y="29.0405703125" />
                  <Point X="-1.884313842773" Y="29.034966796875" />
                  <Point X="-1.874175048828" Y="29.032833984375" />
                  <Point X="-1.853725097656" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812408325195" Y="29.03013671875" />
                  <Point X="-1.802120727539" Y="29.031376953125" />
                  <Point X="-1.780805419922" Y="29.03513671875" />
                  <Point X="-1.770715698242" Y="29.037490234375" />
                  <Point X="-1.750860839844" Y="29.043279296875" />
                  <Point X="-1.741095825195" Y="29.04671484375" />
                  <Point X="-1.686383300781" Y="29.06937890625" />
                  <Point X="-1.641921875" Y="29.087794921875" />
                  <Point X="-1.632583862305" Y="29.0922734375" />
                  <Point X="-1.614452758789" Y="29.102220703125" />
                  <Point X="-1.605659790039" Y="29.107689453125" />
                  <Point X="-1.587929931641" Y="29.120103515625" />
                  <Point X="-1.579781005859" Y="29.126498046875" />
                  <Point X="-1.564228515625" Y="29.14013671875" />
                  <Point X="-1.550253173828" Y="29.15538671875" />
                  <Point X="-1.538021606445" Y="29.172068359375" />
                  <Point X="-1.532361450195" Y="29.180744140625" />
                  <Point X="-1.521539306641" Y="29.19948828125" />
                  <Point X="-1.516858276367" Y="29.208724609375" />
                  <Point X="-1.50852722168" Y="29.22765625" />
                  <Point X="-1.504877197266" Y="29.2373515625" />
                  <Point X="-1.487069335938" Y="29.293830078125" />
                  <Point X="-1.472597900391" Y="29.339728515625" />
                  <Point X="-1.470026123047" Y="29.349763671875" />
                  <Point X="-1.465990600586" Y="29.370052734375" />
                  <Point X="-1.464526733398" Y="29.380306640625" />
                  <Point X="-1.462640625" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354284668" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.256681518555" Y="29.62505859375" />
                  <Point X="-0.931165039062" Y="29.7163203125" />
                  <Point X="-0.513683959961" Y="29.76518359375" />
                  <Point X="-0.365222930908" Y="29.78255859375" />
                  <Point X="-0.332275756836" Y="29.65959765625" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.22043510437" Y="29.247107421875" />
                  <Point X="-0.207661560059" Y="29.218916015625" />
                  <Point X="-0.200119094849" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600669861" Y="29.11043359375" />
                  <Point X="-0.085354125977" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227790833" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912307739" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763122559" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.1945730896" Y="29.1786171875" />
                  <Point X="0.21243132019" Y="29.20534375" />
                  <Point X="0.219973648071" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978118896" Y="29.2617265625" />
                  <Point X="0.324613372803" Y="29.585052734375" />
                  <Point X="0.378190856934" Y="29.7850078125" />
                  <Point X="0.543649963379" Y="29.7676796875" />
                  <Point X="0.827852050781" Y="29.737916015625" />
                  <Point X="1.173287841797" Y="29.654517578125" />
                  <Point X="1.453622436523" Y="29.586833984375" />
                  <Point X="1.676790893555" Y="29.505890625" />
                  <Point X="1.858249633789" Y="29.44007421875" />
                  <Point X="2.075661132812" Y="29.3383984375" />
                  <Point X="2.250428955078" Y="29.2566640625" />
                  <Point X="2.460526367188" Y="29.13426171875" />
                  <Point X="2.6294296875" Y="29.035857421875" />
                  <Point X="2.817778808594" Y="28.9019140625" />
                  <Point X="2.54065234375" Y="28.42191796875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.02945715332" Y="27.496302734375" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017912719727" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.018029663086" Y="27.331279296875" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023800537109" Y="27.28903515625" />
                  <Point X="2.029143310547" Y="27.267111328125" />
                  <Point X="2.032468261719" Y="27.256306640625" />
                  <Point X="2.040734130859" Y="27.23421875" />
                  <Point X="2.045318847656" Y="27.22388671875" />
                  <Point X="2.055681640625" Y="27.203841796875" />
                  <Point X="2.061460205078" Y="27.19412890625" />
                  <Point X="2.085159179688" Y="27.159203125" />
                  <Point X="2.10441796875" Y="27.1308203125" />
                  <Point X="2.109810791016" Y="27.123630859375" />
                  <Point X="2.130463378906" Y="27.100123046875" />
                  <Point X="2.157594726562" Y="27.075388671875" />
                  <Point X="2.168255371094" Y="27.066982421875" />
                  <Point X="2.203181396484" Y="27.043283203125" />
                  <Point X="2.231563964844" Y="27.0240234375" />
                  <Point X="2.241274169922" Y="27.018248046875" />
                  <Point X="2.261313964844" Y="27.00788671875" />
                  <Point X="2.271643554688" Y="27.00330078125" />
                  <Point X="2.293731689453" Y="26.995033203125" />
                  <Point X="2.304544677734" Y="26.99170703125" />
                  <Point X="2.3264765625" Y="26.98636328125" />
                  <Point X="2.337595458984" Y="26.984345703125" />
                  <Point X="2.375895751953" Y="26.979728515625" />
                  <Point X="2.407020263672" Y="26.975974609375" />
                  <Point X="2.416040527344" Y="26.9753203125" />
                  <Point X="2.447576416016" Y="26.97549609375" />
                  <Point X="2.484317626953" Y="26.979822265625" />
                  <Point X="2.497752929688" Y="26.98239453125" />
                  <Point X="2.542045410156" Y="26.994240234375" />
                  <Point X="2.578039306641" Y="27.003865234375" />
                  <Point X="2.584001708984" Y="27.005671875" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627661132812" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.305097412109" Y="27.414158203125" />
                  <Point X="3.940402832031" Y="27.780951171875" />
                  <Point X="3.943760742188" Y="27.77628515625" />
                  <Point X="4.043958984375" Y="27.637033203125" />
                  <Point X="4.136885253906" Y="27.48347265625" />
                  <Point X="3.798786132812" Y="27.224041015625" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168135742188" Y="26.7398671875" />
                  <Point X="3.15211328125" Y="26.7252109375" />
                  <Point X="3.134666259766" Y="26.706599609375" />
                  <Point X="3.128577392578" Y="26.699421875" />
                  <Point X="3.096699951172" Y="26.6578359375" />
                  <Point X="3.070795166016" Y="26.624041015625" />
                  <Point X="3.065636230469" Y="26.6166015625" />
                  <Point X="3.049740234375" Y="26.589373046875" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.018266113281" Y="26.500212890625" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.014447021484" Y="26.305328125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034685058594" Y="26.22860546875" />
                  <Point X="3.050287841797" Y="26.195369140625" />
                  <Point X="3.056919921875" Y="26.183525390625" />
                  <Point X="3.083432373047" Y="26.143228515625" />
                  <Point X="3.104977539062" Y="26.11048046875" />
                  <Point X="3.111737792969" Y="26.101427734375" />
                  <Point X="3.126288085938" Y="26.084185546875" />
                  <Point X="3.134078125" Y="26.07599609375" />
                  <Point X="3.151320800781" Y="26.05990625" />
                  <Point X="3.160030517578" Y="26.05269921875" />
                  <Point X="3.178243164062" Y="26.03937109375" />
                  <Point X="3.18774609375" Y="26.03325" />
                  <Point X="3.226166259766" Y="26.011623046875" />
                  <Point X="3.257388183594" Y="25.994046875" />
                  <Point X="3.265477783203" Y="25.98998828125" />
                  <Point X="3.294680908203" Y="25.97808203125" />
                  <Point X="3.330277832031" Y="25.96801953125" />
                  <Point X="3.343672607422" Y="25.965255859375" />
                  <Point X="3.395619140625" Y="25.958390625" />
                  <Point X="3.437833251953" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465709228516" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.136168945312" Y="26.036470703125" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.70965234375" Y="26.0909921875" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.409211914062" Y="25.613533203125" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686020263672" Y="25.419541015625" />
                  <Point X="3.665627197266" Y="25.41213671875" />
                  <Point X="3.642386230469" Y="25.40162109375" />
                  <Point X="3.634008544922" Y="25.397318359375" />
                  <Point X="3.58297265625" Y="25.3678203125" />
                  <Point X="3.541498535156" Y="25.34384765625" />
                  <Point X="3.533880615234" Y="25.3389453125" />
                  <Point X="3.508770751953" Y="25.3198671875" />
                  <Point X="3.481992919922" Y="25.294349609375" />
                  <Point X="3.472795898438" Y="25.2842265625" />
                  <Point X="3.442174316406" Y="25.24520703125" />
                  <Point X="3.417290039063" Y="25.213498046875" />
                  <Point X="3.4108515625" Y="25.204203125" />
                  <Point X="3.399127929688" Y="25.184921875" />
                  <Point X="3.393842773438" Y="25.174935546875" />
                  <Point X="3.384069335938" Y="25.15347265625" />
                  <Point X="3.380006103516" Y="25.1429296875" />
                  <Point X="3.373159667969" Y="25.1214296875" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.360169189453" Y="25.057173828125" />
                  <Point X="3.351874267578" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584472656" Y="24.973736328125" />
                  <Point X="3.350280273438" Y="24.93705859375" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.36208203125" Y="24.870279296875" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.81601171875" />
                  <Point X="3.380005859375" Y="24.79451171875" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399127929688" Y="24.752517578125" />
                  <Point X="3.4108515625" Y="24.733236328125" />
                  <Point X="3.417290039063" Y="24.72394140625" />
                  <Point X="3.447911621094" Y="24.684921875" />
                  <Point X="3.472795898438" Y="24.653212890625" />
                  <Point X="3.478717529297" Y="24.6463671875" />
                  <Point X="3.501133300781" Y="24.62419921875" />
                  <Point X="3.530173828125" Y="24.601275390625" />
                  <Point X="3.541493896484" Y="24.593595703125" />
                  <Point X="3.592530029297" Y="24.564095703125" />
                  <Point X="3.63400390625" Y="24.540123046875" />
                  <Point X="3.639486816406" Y="24.5371875" />
                  <Point X="3.659156494141" Y="24.527982421875" />
                  <Point X="3.683028076172" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.274834472656" Y="24.359912109375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.068947265625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.268555175781" Y="23.9812421875" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481689453" Y="24.087322265625" />
                  <Point X="3.254316162109" Y="24.06555078125" />
                  <Point X="3.172917480469" Y="24.047859375" />
                  <Point X="3.157873291016" Y="24.043255859375" />
                  <Point X="3.128752929688" Y="24.031630859375" />
                  <Point X="3.114676757812" Y="24.024609375" />
                  <Point X="3.086849853516" Y="24.007716796875" />
                  <Point X="3.074125244141" Y="23.99846875" />
                  <Point X="3.050374755859" Y="23.977998046875" />
                  <Point X="3.039348876953" Y="23.966775390625" />
                  <Point X="2.978804931641" Y="23.893958984375" />
                  <Point X="2.929604736328" Y="23.834787109375" />
                  <Point X="2.921325683594" Y="23.82315234375" />
                  <Point X="2.906605957031" Y="23.798771484375" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.866479736328" Y="23.609041015625" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.42333203125" />
                  <Point X="2.889158203125" Y="23.39451953125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.951974121094" Y="23.29440625" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001742675781" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.593677734375" Y="22.7486875" />
                  <Point X="4.087170654297" Y="22.370015625" />
                  <Point X="4.045500488281" Y="22.3025859375" />
                  <Point X="4.001274414063" Y="22.239748046875" />
                  <Point X="3.589260742188" Y="22.477623046875" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815025878906" Y="22.920484375" />
                  <Point X="2.783120605469" Y="22.930671875" />
                  <Point X="2.771107421875" Y="22.933662109375" />
                  <Point X="2.651894287109" Y="22.95519140625" />
                  <Point X="2.555017089844" Y="22.9726875" />
                  <Point X="2.539358886719" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444846435547" Y="22.96486328125" />
                  <Point X="2.415069091797" Y="22.9550390625" />
                  <Point X="2.400590087891" Y="22.948890625" />
                  <Point X="2.301553222656" Y="22.89676953125" />
                  <Point X="2.221072021484" Y="22.854412109375" />
                  <Point X="2.208969726562" Y="22.846830078125" />
                  <Point X="2.1860390625" Y="22.8299375" />
                  <Point X="2.175210693359" Y="22.820626953125" />
                  <Point X="2.154251220703" Y="22.79966796875" />
                  <Point X="2.144940185547" Y="22.78883984375" />
                  <Point X="2.128046386719" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.068341308594" Y="22.654767578125" />
                  <Point X="2.025984619141" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435515625" />
                  <Point X="2.002187866211" Y="22.41985546875" />
                  <Point X="2.023717651367" Y="22.300642578125" />
                  <Point X="2.041213500977" Y="22.203763671875" />
                  <Point X="2.043015380859" Y="22.1957734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.417117431641" Y="21.52441015625" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.72375390625" Y="20.963916015625" />
                  <Point X="2.399218505859" Y="21.386857421875" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658569336" Y="22.12984765625" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251464844" Y="22.17199609375" />
                  <Point X="1.773298217773" Y="22.179353515625" />
                  <Point X="1.655722045898" Y="22.254943359375" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470581055" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455385009766" Y="22.3513046875" />
                  <Point X="1.424119750977" Y="22.35362109375" />
                  <Point X="1.408395996094" Y="22.35348046875" />
                  <Point X="1.279806274414" Y="22.3416484375" />
                  <Point X="1.175308959961" Y="22.332033203125" />
                  <Point X="1.161226196289" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120007080078" Y="22.318369140625" />
                  <Point X="1.092621459961" Y="22.307025390625" />
                  <Point X="1.079875610352" Y="22.300583984375" />
                  <Point X="1.055493652344" Y="22.28586328125" />
                  <Point X="1.043857543945" Y="22.277583984375" />
                  <Point X="0.944563964844" Y="22.1950234375" />
                  <Point X="0.863873901367" Y="22.127931640625" />
                  <Point X="0.852654052734" Y="22.11691015625" />
                  <Point X="0.832183776855" Y="22.093162109375" />
                  <Point X="0.822933654785" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799018859863" Y="22.03853125" />
                  <Point X="0.787394165039" Y="22.00941015625" />
                  <Point X="0.782791748047" Y="21.9943671875" />
                  <Point X="0.75310357666" Y="21.857779296875" />
                  <Point X="0.728977478027" Y="21.74678125" />
                  <Point X="0.727584594727" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.724742370605" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.824053955078" Y="20.916306640625" />
                  <Point X="0.833091308594" Y="20.84766015625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605102539" Y="21.51987890625" />
                  <Point X="0.642143676758" Y="21.546423828125" />
                  <Point X="0.62678692627" Y="21.576185546875" />
                  <Point X="0.620407287598" Y="21.586791015625" />
                  <Point X="0.530081726074" Y="21.716931640625" />
                  <Point X="0.4566796875" Y="21.82269140625" />
                  <Point X="0.446669189453" Y="21.834828125" />
                  <Point X="0.424786560059" Y="21.85728125" />
                  <Point X="0.412914123535" Y="21.86759765625" />
                  <Point X="0.386659118652" Y="21.886841796875" />
                  <Point X="0.373244140625" Y="21.895060546875" />
                  <Point X="0.345241210938" Y="21.909169921875" />
                  <Point X="0.330653106689" Y="21.915060546875" />
                  <Point X="0.190879669189" Y="21.958439453125" />
                  <Point X="0.077294021606" Y="21.993693359375" />
                  <Point X="0.063380245209" Y="21.996888671875" />
                  <Point X="0.035228092194" Y="22.001158203125" />
                  <Point X="0.020989723206" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022896093369" Y="22.001162109375" />
                  <Point X="-0.051062065125" Y="21.996892578125" />
                  <Point X="-0.06498387146" Y="21.9936953125" />
                  <Point X="-0.204757171631" Y="21.950314453125" />
                  <Point X="-0.318342956543" Y="21.9150625" />
                  <Point X="-0.332927185059" Y="21.909171875" />
                  <Point X="-0.360928466797" Y="21.895064453125" />
                  <Point X="-0.374345550537" Y="21.88684765625" />
                  <Point X="-0.400600830078" Y="21.867603515625" />
                  <Point X="-0.412477874756" Y="21.85728125" />
                  <Point X="-0.434361572266" Y="21.83482421875" />
                  <Point X="-0.44436819458" Y="21.822689453125" />
                  <Point X="-0.534693603516" Y="21.692546875" />
                  <Point X="-0.60809576416" Y="21.5867890625" />
                  <Point X="-0.612469482422" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777832031" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.822341064453" Y="20.84183203125" />
                  <Point X="-0.985425720215" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128849972531" Y="20.36282991384" />
                  <Point X="-0.961846678743" Y="20.321191316132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119464926043" Y="20.458398253731" />
                  <Point X="-0.937255065365" Y="20.41296823307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.675676346236" Y="20.944313633918" />
                  <Point X="-2.462114194756" Y="20.891066609206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134837697889" Y="20.560139411022" />
                  <Point X="-0.912663451987" Y="20.504745150008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884564827539" Y="21.094303676566" />
                  <Point X="-2.397261392001" Y="20.972805284205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.155329013435" Y="20.66315676459" />
                  <Point X="-0.888071838609" Y="20.596522066946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956753082575" Y="21.210210524811" />
                  <Point X="-2.301134920546" Y="21.046746557845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.175820354356" Y="20.766174124486" />
                  <Point X="-0.863480225231" Y="20.688298983884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907338972722" Y="21.295798498277" />
                  <Point X="-2.194423424323" Y="21.118048688399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.210379128053" Y="20.872698889301" />
                  <Point X="-0.838888611853" Y="20.780075900822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857924862868" Y="21.381386471743" />
                  <Point X="-2.08663214186" Y="21.189081598007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.329529625718" Y="21.00031473971" />
                  <Point X="-0.814297032178" Y="20.871852826163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808510753014" Y="21.46697444521" />
                  <Point X="-1.794560412928" Y="21.214168231934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.485521146689" Y="21.137116088882" />
                  <Point X="-0.789705521836" Y="20.96362976879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75909664316" Y="21.552562418676" />
                  <Point X="-0.765114011493" Y="21.055406711418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.709682454337" Y="21.638150372452" />
                  <Point X="-0.74052250115" Y="21.147183654046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.815533014041" Y="22.011778178735" />
                  <Point X="-3.764185283313" Y="21.998975751582" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.66026825266" Y="21.723738323025" />
                  <Point X="-0.715930990808" Y="21.238960596673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831631015176" Y="20.853110052445" />
                  <Point X="0.832399035939" Y="20.852918563363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.907511179161" Y="22.132619205738" />
                  <Point X="-3.645749705182" Y="22.067354740209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610854050983" Y="21.809326273597" />
                  <Point X="-0.691339480465" Y="21.330737539301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803518481926" Y="20.958027589004" />
                  <Point X="0.819071820514" Y="20.954149706156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.999489480487" Y="22.2534602667" />
                  <Point X="-3.527314127051" Y="22.135733728836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561439849307" Y="21.894914224169" />
                  <Point X="-0.666747970122" Y="21.422514481929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.775405948675" Y="21.062945125562" />
                  <Point X="0.805744482448" Y="21.055380879528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.069228604288" Y="22.368756477946" />
                  <Point X="-3.408878548919" Y="22.204112717463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51202564763" Y="21.980502174741" />
                  <Point X="-0.642010213412" Y="21.514254961241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747293415424" Y="21.167862662121" />
                  <Point X="0.792417144381" Y="21.156612052899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137814830863" Y="22.483765239629" />
                  <Point X="-3.290442970788" Y="22.272491706091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462611445954" Y="22.066090125313" />
                  <Point X="-0.598085649894" Y="21.601211632332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.719180882174" Y="21.272780198679" />
                  <Point X="0.779089806315" Y="21.257843226271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145971100136" Y="22.583707120745" />
                  <Point X="-3.172007203431" Y="22.340870647538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413197244277" Y="22.151678075885" />
                  <Point X="-0.540156125711" Y="21.68467647455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691068348923" Y="21.377697735238" />
                  <Point X="0.765762468249" Y="21.359074399642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.049666898187" Y="22.657604081196" />
                  <Point X="-3.05357138364" Y="22.409249575913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.363783042601" Y="22.237266026457" />
                  <Point X="-0.482227204419" Y="21.768141467085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662955815673" Y="21.482615271796" />
                  <Point X="0.752435130182" Y="21.460305573014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719119106341" Y="20.969956185014" />
                  <Point X="2.728961591822" Y="20.967502177766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.953362696238" Y="22.731501041647" />
                  <Point X="-2.935135563849" Y="22.477628504288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.322252840793" Y="22.324819678971" />
                  <Point X="-0.419237393231" Y="21.850344638051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.616763804234" Y="21.592040528544" />
                  <Point X="0.739107792116" Y="21.561536746385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.626217570157" Y="21.09102743428" />
                  <Point X="2.674244974505" Y="21.079052857473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.857058494289" Y="22.805398002098" />
                  <Point X="-2.816699744057" Y="22.546007432662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311732663076" Y="22.42010499886" />
                  <Point X="-0.303987294957" Y="21.919517856009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.534589234463" Y="21.710437244697" />
                  <Point X="0.725780454049" Y="21.662767919757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.533316033973" Y="21.212098683546" />
                  <Point X="2.608212087975" Y="21.193424999981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760754271457" Y="22.879294957342" />
                  <Point X="-2.698263924266" Y="22.614386361037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.351100527082" Y="22.527828804557" />
                  <Point X="-0.129053554653" Y="21.973810270697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.451405762204" Y="21.829085508493" />
                  <Point X="0.731678025995" Y="21.75920578471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44041449779" Y="21.333169932812" />
                  <Point X="2.542179201445" Y="21.30779714249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.664449731131" Y="22.953191833426" />
                  <Point X="0.751864996098" Y="21.852080902559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347513220481" Y="21.454241117534" />
                  <Point X="2.476146314916" Y="21.422169284999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568145190806" Y="23.02708870951" />
                  <Point X="0.772051945026" Y="21.944956025688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.254612149428" Y="21.575312250829" />
                  <Point X="2.41011343361" Y="21.536541426205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47184065048" Y="23.100985585595" />
                  <Point X="0.798150973224" Y="22.036357101899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.161711078376" Y="21.696383384125" />
                  <Point X="2.344080596337" Y="21.650913556432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375536110155" Y="23.174882461679" />
                  <Point X="0.855711822275" Y="22.119913865152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.068810007323" Y="21.817454517421" />
                  <Point X="2.278047759064" Y="21.76528568666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279231569829" Y="23.248779337763" />
                  <Point X="0.945161106662" Y="22.195519948508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.975908936271" Y="21.938525650717" />
                  <Point X="2.212014921791" Y="21.879657816887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182927029504" Y="23.322676213847" />
                  <Point X="1.03574922772" Y="22.270842087992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.883007865218" Y="22.059596784012" />
                  <Point X="2.145982084518" Y="21.994029947115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.681594309468" Y="23.794244228475" />
                  <Point X="-4.462004141214" Y="23.73949425038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.086622489178" Y="23.396573089931" />
                  <Point X="1.18093683267" Y="22.3325510472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.759310342899" Y="22.188346334997" />
                  <Point X="2.079949247245" Y="22.108402077343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708304032927" Y="23.89881200527" />
                  <Point X="-4.205013111549" Y="23.773327484994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.004079787855" Y="23.473901177849" />
                  <Point X="2.038905485691" Y="22.216543731228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735013753929" Y="24.003379781452" />
                  <Point X="-3.948022066227" Y="23.807160715703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960688327662" Y="23.560990766527" />
                  <Point X="2.020389945243" Y="22.319068468738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752158092639" Y="24.105562639971" />
                  <Point X="-3.691030882136" Y="23.840993911814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949211519249" Y="23.656037571595" />
                  <Point X="2.002024525938" Y="22.421555776843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766678921007" Y="24.207091383896" />
                  <Point X="-3.434039698045" Y="23.874827107924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977499642138" Y="23.760998887567" />
                  <Point X="2.007165005644" Y="22.518182406092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781199749375" Y="24.308620127821" />
                  <Point X="2.043250578658" Y="22.60709355703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665635266874" Y="24.377714960987" />
                  <Point X="2.088801937398" Y="22.693644622517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.476360148849" Y="24.428431668611" />
                  <Point X="2.137917363693" Y="22.779307066158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.772714943777" Y="22.371706250463" />
                  <Point X="4.046093385319" Y="22.303545349613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.287084781448" Y="24.479148314057" />
                  <Point X="2.223741421394" Y="22.855817020044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474232817331" Y="22.544034497722" />
                  <Point X="4.046251294916" Y="22.401414273116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.097808476549" Y="24.52986472576" />
                  <Point X="2.349973476152" Y="22.922252128724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.175752556496" Y="22.716362279833" />
                  <Point X="3.85723983662" Y="22.546448417316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908532171651" Y="24.580581137462" />
                  <Point X="2.534071654097" Y="22.974259592478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.87727229566" Y="22.888690061943" />
                  <Point X="3.668228378324" Y="22.691482561516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.719255866752" Y="24.631297549164" />
                  <Point X="3.479216341168" Y="22.836516850041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.529979561853" Y="24.682013960867" />
                  <Point X="3.290203926989" Y="22.981551232569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.399846815197" Y="24.747476517826" />
                  <Point X="3.10119151281" Y="23.126585615097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330929806132" Y="24.828201872383" />
                  <Point X="2.977016473299" Y="23.25545422449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299989591342" Y="24.91839590521" />
                  <Point X="2.902053455723" Y="23.372052898738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299371802166" Y="25.016150167857" />
                  <Point X="2.862490807916" Y="23.479825269491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.339105748672" Y="25.123965248172" />
                  <Point X="2.863574069177" Y="23.577463476912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.283857050785" Y="25.4574264983" />
                  <Point X="2.87238149844" Y="23.673175832952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772805943484" Y="25.677243443997" />
                  <Point X="2.891954720343" Y="23.766203975414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.758833682569" Y="25.771668062876" />
                  <Point X="2.943336077857" Y="23.85130145895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744861397348" Y="25.866092675695" />
                  <Point X="3.010765254387" Y="23.932397771821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73054459974" Y="25.960431391929" />
                  <Point X="3.091076036012" Y="24.01028233982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.705692364824" Y="26.052143328619" />
                  <Point X="3.258472127649" Y="24.066454101396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.680840129907" Y="26.143855265309" />
                  <Point X="3.684567884026" Y="24.058124792227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.65598789499" Y="26.235567201999" />
                  <Point X="4.51658018061" Y="23.948589122767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.551545871436" Y="26.307435175642" />
                  <Point X="4.743575209044" Y="23.98990120046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.745650373923" Y="26.204411155535" />
                  <Point X="4.763700580055" Y="24.082791676688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.564208287144" Y="26.257080857195" />
                  <Point X="4.77792725886" Y="24.177152862063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483452209112" Y="26.33485440033" />
                  <Point X="3.572699486169" Y="24.575558190387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44228351983" Y="26.42249818804" />
                  <Point X="3.428731886716" Y="24.709361639221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421487563994" Y="26.515221468692" />
                  <Point X="3.371772841831" Y="24.821471418914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441275060705" Y="26.618063340517" />
                  <Point X="3.351781572857" Y="24.92436409687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50684274822" Y="26.732319495884" />
                  <Point X="3.353407462851" Y="25.021867011753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678682296778" Y="26.873072202124" />
                  <Point X="3.371588026434" Y="25.115242382933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.86769338669" Y="27.018106254475" />
                  <Point X="3.410407023456" Y="25.203472014721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0567060143" Y="27.163140690218" />
                  <Point X="3.473979924377" Y="25.285529805088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222836274603" Y="27.302469911019" />
                  <Point X="3.569003655011" Y="25.359746022894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.172948635889" Y="27.38793982048" />
                  <Point X="3.702888151317" Y="25.424273163607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.123060655541" Y="27.473409644762" />
                  <Point X="3.89216381522" Y="25.474989735128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073172294731" Y="27.558879374185" />
                  <Point X="4.081439479122" Y="25.525706306648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.023283933922" Y="27.644349103607" />
                  <Point X="4.270715143025" Y="25.576422878169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.965338514903" Y="27.727809982797" />
                  <Point X="4.459990671436" Y="25.627139483471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901541770084" Y="27.809811962612" />
                  <Point X="3.167357216746" Y="26.047337495926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.530665380477" Y="25.956754597046" />
                  <Point X="4.649265830302" Y="25.677856180912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.837745025266" Y="27.891813942427" />
                  <Point X="3.065376190583" Y="26.170672516295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787655629797" Y="25.990588026221" />
                  <Point X="4.779289810149" Y="25.743345856483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773948291575" Y="27.973815925016" />
                  <Point X="-3.503590640291" Y="27.906408191768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920709281448" Y="27.761079546673" />
                  <Point X="3.019678388519" Y="26.279974552806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.044645879118" Y="26.024421455396" />
                  <Point X="4.763430569674" Y="25.845208304025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834469360097" Y="27.837485814106" />
                  <Point X="3.001498521729" Y="26.382415597473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.301635705283" Y="26.058254990075" />
                  <Point X="4.744503843114" Y="25.947835561747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.77232911252" Y="27.919900805069" />
                  <Point X="3.011973714788" Y="26.477712133297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.558625297393" Y="26.092088583111" />
                  <Point X="4.719127754306" Y="26.052070826078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.749014545913" Y="28.011996125528" />
                  <Point X="3.040501873744" Y="26.568507559188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75465125967" Y="28.1113098109" />
                  <Point X="3.093204357998" Y="26.653275648832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.781087088263" Y="28.215809298035" />
                  <Point X="3.161808113639" Y="26.734079106239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.845719632389" Y="28.329832295969" />
                  <Point X="2.147060249969" Y="27.084992459265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.52708977249" Y="26.990240457393" />
                  <Point X="3.256941365523" Y="26.808268017331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.911752800311" Y="28.444204508637" />
                  <Point X="2.054588337434" Y="27.205956591325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.676086963564" Y="27.050999580102" />
                  <Point X="3.353245557572" Y="26.88216498025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977786005486" Y="28.558576730593" />
                  <Point X="2.020304510323" Y="27.312412804256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.794522920235" Y="27.119378474349" />
                  <Point X="3.449549749621" Y="26.956061943169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.043819289543" Y="28.672948972216" />
                  <Point X="2.013449553916" Y="27.412030231635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.912958876906" Y="27.187757368595" />
                  <Point X="3.545853941669" Y="27.029958906089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985743001025" Y="28.756377221975" />
                  <Point X="2.031875041915" Y="27.505344536299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.031394833577" Y="27.256136262842" />
                  <Point X="3.642158133718" Y="27.103855869008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.889378843388" Y="28.830259233794" />
                  <Point X="2.063572670158" Y="27.595349724743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.149830790248" Y="27.324515157088" />
                  <Point X="3.738462325767" Y="27.177752831927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.79301395658" Y="28.90414106381" />
                  <Point X="2.112865108306" Y="27.680968034372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.26826674692" Y="27.392894051335" />
                  <Point X="3.834766673978" Y="27.251649755911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696649069771" Y="28.978022893826" />
                  <Point X="2.162279290248" Y="27.766555989865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386702711422" Y="27.461272943629" />
                  <Point X="3.931071284008" Y="27.325546614616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.587670406588" Y="29.048759756171" />
                  <Point X="2.21169347219" Y="27.852143945357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.505138679458" Y="27.529651835042" />
                  <Point X="4.027375894037" Y="27.399443473321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466031826123" Y="29.116340146623" />
                  <Point X="2.261107654133" Y="27.93773190085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.623574647495" Y="27.598030726455" />
                  <Point X="4.123680504067" Y="27.473340332026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.344393245659" Y="29.183920537075" />
                  <Point X="-2.046693484854" Y="29.109695650267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.764436452654" Y="29.03932106814" />
                  <Point X="2.310521836075" Y="28.023319856342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.742010615531" Y="27.666409617868" />
                  <Point X="4.076676148832" Y="27.582968128829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.222755257195" Y="29.25150107513" />
                  <Point X="-2.169435902588" Y="29.238207066933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.61744348677" Y="29.100579900313" />
                  <Point X="2.359936018017" Y="28.108907811835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.860446583567" Y="27.734788509281" />
                  <Point X="3.998361944116" Y="27.700402347874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.534308176984" Y="29.177760234346" />
                  <Point X="2.409350199959" Y="28.194495767327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.495821334192" Y="29.266072681485" />
                  <Point X="2.458764381902" Y="28.280083722819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468550679303" Y="29.357181638354" />
                  <Point X="2.508178563844" Y="28.365671678312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.464987765874" Y="29.454201599052" />
                  <Point X="-0.111308206189" Y="29.116691377947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.015344621959" Y="29.08511328125" />
                  <Point X="2.557592791508" Y="28.451259622405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.478315371774" Y="29.555432839202" />
                  <Point X="-0.217728156288" Y="29.241133146356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.165396700178" Y="29.145609391053" />
                  <Point X="2.607007106817" Y="28.536847544645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.30761126549" Y="29.610779820094" />
                  <Point X="-0.248434221725" Y="29.346697323114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.224449451312" Y="29.228794181339" />
                  <Point X="2.656421422126" Y="28.622435466885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.122769966536" Y="29.662602002971" />
                  <Point X="-0.276546845065" Y="29.451614882135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.253450903333" Y="29.319471602015" />
                  <Point X="2.705835737435" Y="28.708023389126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.937928436224" Y="29.714424128164" />
                  <Point X="-0.304659468405" Y="29.556532441155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.278042529143" Y="29.411248515854" />
                  <Point X="2.755250052745" Y="28.793611311366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.673705309157" Y="29.746454198376" />
                  <Point X="-0.332772089061" Y="29.661449999506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302634154952" Y="29.503025429692" />
                  <Point X="2.804664368054" Y="28.879199233606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.406464632933" Y="29.777731909082" />
                  <Point X="-0.360884560403" Y="29.766367520629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.327225758692" Y="29.594802349033" />
                  <Point X="2.662037866003" Y="29.012668309303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.351817176755" Y="29.686579314669" />
                  <Point X="2.380839204529" Y="29.180687304759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.376408594818" Y="29.778356280304" />
                  <Point X="2.001067315819" Y="29.373283366095" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001626953125" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.692532226562" Y="20.638130859375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.478455078125" />
                  <Point X="0.373993377686" Y="21.608595703125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.274336212158" Y="21.733599609375" />
                  <Point X="0.134562774658" Y="21.776978515625" />
                  <Point X="0.020977081299" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.14843788147" Y="21.768853515625" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.37860446167" Y="21.58421484375" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.638815185547" Y="20.79265625" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.96734185791" Y="20.03613671875" />
                  <Point X="-1.100258056641" Y="20.0619375" />
                  <Point X="-1.261917236328" Y="20.103529296875" />
                  <Point X="-1.35158996582" Y="20.1266015625" />
                  <Point X="-1.335752807617" Y="20.246896484375" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.343075073242" Y="20.63311328125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282470703" Y="20.79737109375" />
                  <Point X="-1.514968139648" Y="20.910224609375" />
                  <Point X="-1.619543334961" Y="21.001935546875" />
                  <Point X="-1.649240356445" Y="21.014236328125" />
                  <Point X="-1.82003503418" Y="21.0254296875" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.132193359375" Y="20.9311171875" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.360558837891" Y="20.711306640625" />
                  <Point X="-2.457094238281" Y="20.5855" />
                  <Point X="-2.661025634766" Y="20.71176953125" />
                  <Point X="-2.855839599609" Y="20.832392578125" />
                  <Point X="-3.079701904297" Y="21.004759765625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.916724609375" Y="21.65954296875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.513980712891" Y="22.43123828125" />
                  <Point X="-2.531329345703" Y="22.4485859375" />
                  <Point X="-2.560158203125" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.169745605469" Y="22.12278515625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.007794189453" Y="21.9506640625" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.32219921875" Y="22.42199609375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.882362304688" Y="23.02547265625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140325927734" Y="23.66540234375" />
                  <Point X="-3.161157470703" Y="23.689359375" />
                  <Point X="-3.187645263672" Y="23.70494921875" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.949277832031" Y="23.61535546875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.867197753906" Y="23.753146484375" />
                  <Point X="-4.927393066406" Y="23.98880859375" />
                  <Point X="-4.969855957031" Y="24.28570703125" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.376029785156" Y="24.65201953125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.526586669922" Y="24.88919921875" />
                  <Point X="-3.502325439453" Y="24.9093515625" />
                  <Point X="-3.489795898438" Y="24.94053515625" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.490751464844" Y="24.999984375" />
                  <Point X="-3.502325439453" Y="25.0280859375" />
                  <Point X="-3.529456542969" Y="25.05023046875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.222664550781" Y="25.244326171875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.956439453125" Y="25.734248046875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.83216796875" Y="26.311853515625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.340034179687" Y="26.471228515625" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731703613281" Y="26.3958671875" />
                  <Point X="-3.697816162109" Y="26.40655078125" />
                  <Point X="-3.670277587891" Y="26.415234375" />
                  <Point X="-3.651533691406" Y="26.426056640625" />
                  <Point X="-3.639119628906" Y="26.443787109375" />
                  <Point X="-3.625521972656" Y="26.47661328125" />
                  <Point X="-3.614472167969" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.632723144531" Y="26.577029296875" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.041528808594" Y="26.91200390625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.310756347656" Y="27.52875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.933588623047" Y="28.07804296875" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.519533447266" Y="28.1350078125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514648438" Y="27.92043359375" />
                  <Point X="-3.091318603516" Y="27.9163046875" />
                  <Point X="-3.052965332031" Y="27.91294921875" />
                  <Point X="-3.031507080078" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.979752929688" Y="27.960904296875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942203369141" Y="28.075037109375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.1211484375" Y="28.426888671875" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.015419677734" Y="28.973041015625" />
                  <Point X="-2.752872314453" Y="29.174333984375" />
                  <Point X="-2.396263183594" Y="29.372458984375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.078148925781" Y="29.43134765625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247558594" Y="29.273662109375" />
                  <Point X="-1.89871862793" Y="29.24631640625" />
                  <Point X="-1.85603137207" Y="29.22409375" />
                  <Point X="-1.83512487793" Y="29.218490234375" />
                  <Point X="-1.813809448242" Y="29.22225" />
                  <Point X="-1.759096923828" Y="29.2449140625" />
                  <Point X="-1.714635498047" Y="29.263330078125" />
                  <Point X="-1.696905395508" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.668275390625" Y="29.350966796875" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.671140258789" Y="29.564435546875" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.307973144531" Y="29.80800390625" />
                  <Point X="-0.968082885742" Y="29.903296875" />
                  <Point X="-0.535770996094" Y="29.95389453125" />
                  <Point X="-0.224200180054" Y="29.990359375" />
                  <Point X="-0.148749725342" Y="29.7087734375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282100677" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.03659405899" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.141087509155" Y="29.634228515625" />
                  <Point X="0.236648452759" Y="29.990869140625" />
                  <Point X="0.563440551758" Y="29.95664453125" />
                  <Point X="0.860209777832" Y="29.925564453125" />
                  <Point X="1.217878173828" Y="29.839212890625" />
                  <Point X="1.508455688477" Y="29.769056640625" />
                  <Point X="1.741574707031" Y="29.68450390625" />
                  <Point X="1.931043701172" Y="29.61578125" />
                  <Point X="2.156150146484" Y="29.5105078125" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.556171630859" Y="29.29843359375" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.937628173828" Y="29.049830078125" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.705197265625" Y="28.32691796875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.213007568359" Y="27.447220703125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.206663085938" Y="27.354025390625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.242380859375" Y="27.26588671875" />
                  <Point X="2.261639648438" Y="27.23750390625" />
                  <Point X="2.274938964844" Y="27.224205078125" />
                  <Point X="2.309864990234" Y="27.200505859375" />
                  <Point X="2.338247558594" Y="27.18124609375" />
                  <Point X="2.360335693359" Y="27.172978515625" />
                  <Point X="2.398635986328" Y="27.168361328125" />
                  <Point X="2.429760498047" Y="27.164607421875" />
                  <Point X="2.4486640625" Y="27.1659453125" />
                  <Point X="2.492956542969" Y="27.177791015625" />
                  <Point X="2.528950439453" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.210097412109" Y="27.578701171875" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.097985839844" Y="27.8872578125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.316934570312" Y="27.552927734375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.914450439453" Y="27.073302734375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.247493896484" Y="26.54224609375" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.201245117188" Y="26.449041015625" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.200527099609" Y="26.34372265625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.242158935547" Y="26.247658203125" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280946777344" Y="26.1988203125" />
                  <Point X="3.319366943359" Y="26.177193359375" />
                  <Point X="3.350588867188" Y="26.1596171875" />
                  <Point X="3.368566650391" Y="26.153619140625" />
                  <Point X="3.420513183594" Y="26.14675390625" />
                  <Point X="3.462727294922" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.111369140625" Y="26.224845703125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.894260742188" Y="26.13593359375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.975221191406" Y="25.719953125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.458387695312" Y="25.430005859375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.678051269531" Y="25.2033203125" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.591643066406" Y="25.12790625" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.546778076172" Y="25.021435546875" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.548690429688" Y="24.906017578125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.597380371094" Y="24.80222265625" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.687613037109" Y="24.728591796875" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.324010253906" Y="24.543439453125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.973517578125" Y="24.19998828125" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.902268066406" Y="23.8313046875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.243755371094" Y="23.7928671875" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.394836669922" Y="23.901658203125" />
                  <Point X="3.294671142578" Y="23.87988671875" />
                  <Point X="3.213272460938" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.124901611328" Y="23.772486328125" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.055680419922" Y="23.591630859375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.111793457031" Y="23.39715625" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.709342285156" Y="22.89942578125" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.274845214844" Y="22.312283203125" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.108662109375" Y="22.062208984375" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.494260986328" Y="22.313078125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.618127685547" Y="22.76821484375" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077392578" Y="22.78075390625" />
                  <Point X="2.390040527344" Y="22.7286328125" />
                  <Point X="2.309559326172" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.236477539062" Y="22.566279296875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.210692871094" Y="22.334412109375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.581662353516" Y="21.61941015625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.919207275391" Y="20.86972265625" />
                  <Point X="2.835291259766" Y="20.809783203125" />
                  <Point X="2.728557617188" Y="20.7406953125" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="2.248481201172" Y="21.271193359375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.552973022461" Y="22.095123046875" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425805053711" Y="22.16428125" />
                  <Point X="1.297215454102" Y="22.15244921875" />
                  <Point X="1.192718261719" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.0660390625" Y="22.0489296875" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.938768493652" Y="21.817423828125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.012428405762" Y="20.941107421875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.073729248047" Y="20.05415234375" />
                  <Point X="0.994369689941" Y="20.0367578125" />
                  <Point X="0.895737976074" Y="20.01883984375" />
                  <Point X="0.860200378418" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#172" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.106609374343" Y="4.752996095487" Z="1.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="-0.547739060359" Y="5.034834628843" Z="1.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.4" />
                  <Point X="-1.327626913789" Y="4.887429855699" Z="1.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.4" />
                  <Point X="-1.72686849711" Y="4.589190881874" Z="1.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.4" />
                  <Point X="-1.722117291954" Y="4.397283259761" Z="1.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.4" />
                  <Point X="-1.784386680358" Y="4.322387374726" Z="1.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.4" />
                  <Point X="-1.88178625045" Y="4.321946287265" Z="1.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.4" />
                  <Point X="-2.044637379235" Y="4.493066048266" Z="1.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.4" />
                  <Point X="-2.426701805349" Y="4.447445585722" Z="1.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.4" />
                  <Point X="-3.051997752229" Y="4.044001900013" Z="1.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.4" />
                  <Point X="-3.170605723859" Y="3.433169839907" Z="1.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.4" />
                  <Point X="-2.998169165875" Y="3.101959518461" Z="1.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.4" />
                  <Point X="-3.02126352379" Y="3.027540044096" Z="1.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.4" />
                  <Point X="-3.093116947145" Y="2.997395533298" Z="1.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.4" />
                  <Point X="-3.50068950839" Y="3.209588224887" Z="1.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.4" />
                  <Point X="-3.97920829997" Y="3.140027088315" Z="1.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.4" />
                  <Point X="-4.360094327911" Y="2.585234704526" Z="1.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.4" />
                  <Point X="-4.078122848383" Y="1.903615802694" Z="1.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.4" />
                  <Point X="-3.683229350657" Y="1.585221779797" Z="1.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.4" />
                  <Point X="-3.67787236177" Y="1.527027492502" Z="1.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.4" />
                  <Point X="-3.719008416809" Y="1.485517260433" Z="1.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.4" />
                  <Point X="-4.339664003161" Y="1.552082021973" Z="1.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.4" />
                  <Point X="-4.886583773817" Y="1.356212398948" Z="1.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.4" />
                  <Point X="-5.012057007773" Y="0.772847385504" Z="1.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.4" />
                  <Point X="-4.241761203174" Y="0.227308962878" Z="1.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.4" />
                  <Point X="-3.564118597031" Y="0.040433477307" Z="1.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.4" />
                  <Point X="-3.544660408605" Y="0.016443931866" Z="1.4" />
                  <Point X="-3.539556741714" Y="0" Z="1.4" />
                  <Point X="-3.54370419253" Y="-0.013363019195" Z="1.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.4" />
                  <Point X="-3.561249998092" Y="-0.038442505827" Z="1.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.4" />
                  <Point X="-4.395126585598" Y="-0.268403093878" Z="1.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.4" />
                  <Point X="-5.02550821904" Y="-0.69009263967" Z="1.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.4" />
                  <Point X="-4.921798668998" Y="-1.227947404549" Z="1.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.4" />
                  <Point X="-3.948907465691" Y="-1.402936512406" Z="1.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.4" />
                  <Point X="-3.207286246072" Y="-1.313851028259" Z="1.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.4" />
                  <Point X="-3.196130377033" Y="-1.335786109845" Z="1.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.4" />
                  <Point X="-3.9189563711" Y="-1.903579573936" Z="1.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.4" />
                  <Point X="-4.371298507939" Y="-2.572332342331" Z="1.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.4" />
                  <Point X="-4.053736220787" Y="-3.048337967092" Z="1.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.4" />
                  <Point X="-3.150901598947" Y="-2.889235275267" Z="1.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.4" />
                  <Point X="-2.565061825756" Y="-2.563268643375" Z="1.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.4" />
                  <Point X="-2.966181737902" Y="-3.284176783545" Z="1.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.4" />
                  <Point X="-3.116361635248" Y="-4.003577363632" Z="1.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.4" />
                  <Point X="-2.693503129646" Y="-4.299462563005" Z="1.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.4" />
                  <Point X="-2.327047167291" Y="-4.287849688674" Z="1.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.4" />
                  <Point X="-2.110571120553" Y="-4.079176433275" Z="1.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.4" />
                  <Point X="-1.829461333909" Y="-3.993181460465" Z="1.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.4" />
                  <Point X="-1.554091594976" Y="-4.096086096045" Z="1.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.4" />
                  <Point X="-1.398270382738" Y="-4.345360234246" Z="1.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.4" />
                  <Point X="-1.39148088268" Y="-4.715297033687" Z="1.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.4" />
                  <Point X="-1.280532404753" Y="-4.913611495702" Z="1.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.4" />
                  <Point X="-0.983025532825" Y="-4.981666486745" Z="1.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="-0.596674943702" Y="-4.189005277928" Z="1.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="-0.343684559131" Y="-3.413014472663" Z="1.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="-0.139773418293" Y="-3.247619879356" Z="1.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.4" />
                  <Point X="0.113585661068" Y="-3.239492165932" Z="1.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.4" />
                  <Point X="0.326761300301" Y="-3.388631283105" Z="1.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.4" />
                  <Point X="0.6380799167" Y="-4.343530751887" Z="1.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.4" />
                  <Point X="0.898518840537" Y="-4.999075318521" Z="1.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.4" />
                  <Point X="1.078278435028" Y="-4.963406617075" Z="1.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.4" />
                  <Point X="1.05584466756" Y="-4.021086907199" Z="1.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.4" />
                  <Point X="0.981471720589" Y="-3.161915460299" Z="1.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.4" />
                  <Point X="1.091850124276" Y="-2.958234834558" Z="1.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.4" />
                  <Point X="1.295640923948" Y="-2.866059514941" Z="1.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.4" />
                  <Point X="1.519777720602" Y="-2.915654651899" Z="1.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.4" />
                  <Point X="2.20265754462" Y="-3.72796317627" Z="1.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.4" />
                  <Point X="2.74957058136" Y="-4.269998078885" Z="1.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.4" />
                  <Point X="2.942113008207" Y="-4.139685175149" Z="1.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.4" />
                  <Point X="2.618807640535" Y="-3.324308558053" Z="1.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.4" />
                  <Point X="2.253740900582" Y="-2.625421195733" Z="1.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.4" />
                  <Point X="2.274567870989" Y="-2.425726877999" Z="1.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.4" />
                  <Point X="2.407171500373" Y="-2.284333438978" Z="1.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.4" />
                  <Point X="2.60308560778" Y="-2.249707108909" Z="1.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.4" />
                  <Point X="3.463105039054" Y="-2.69894185974" Z="1.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.4" />
                  <Point X="4.143394715437" Y="-2.935287874953" Z="1.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.4" />
                  <Point X="4.311222969993" Y="-2.682720779103" Z="1.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.4" />
                  <Point X="3.733624196702" Y="-2.029625958744" Z="1.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.4" />
                  <Point X="3.147695599955" Y="-1.544525031676" Z="1.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.4" />
                  <Point X="3.099314092104" Y="-1.381671230682" Z="1.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.4" />
                  <Point X="3.157191797441" Y="-1.228199496147" Z="1.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.4" />
                  <Point X="3.299134248192" Y="-1.137691834563" Z="1.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.4" />
                  <Point X="4.231073266393" Y="-1.225425462574" Z="1.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.4" />
                  <Point X="4.944859043342" Y="-1.148539829739" Z="1.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.4" />
                  <Point X="5.016802805053" Y="-0.776185969034" Z="1.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.4" />
                  <Point X="4.33079523899" Y="-0.376982817434" Z="1.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.4" />
                  <Point X="3.706478725945" Y="-0.196837936667" Z="1.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.4" />
                  <Point X="3.630558330461" Y="-0.135629654144" Z="1.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.4" />
                  <Point X="3.591641928966" Y="-0.053298205175" Z="1.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.4" />
                  <Point X="3.589729521458" Y="0.043312326031" Z="1.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.4" />
                  <Point X="3.624821107939" Y="0.128319084431" Z="1.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.4" />
                  <Point X="3.696916688408" Y="0.191310907575" Z="1.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.4" />
                  <Point X="4.465172578316" Y="0.412989115334" Z="1.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.4" />
                  <Point X="5.018469901685" Y="0.758925395024" Z="1.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.4" />
                  <Point X="4.936685888448" Y="1.179041022674" Z="1.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.4" />
                  <Point X="4.098686987039" Y="1.30569790394" Z="1.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.4" />
                  <Point X="3.420907351054" Y="1.227603185832" Z="1.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.4" />
                  <Point X="3.337738764863" Y="1.252043778901" Z="1.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.4" />
                  <Point X="3.277773441818" Y="1.306418651478" Z="1.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.4" />
                  <Point X="3.24333966623" Y="1.385107206223" Z="1.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.4" />
                  <Point X="3.243241627859" Y="1.466853843086" Z="1.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.4" />
                  <Point X="3.281020825565" Y="1.543108607303" Z="1.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.4" />
                  <Point X="3.938732779548" Y="2.064914878594" Z="1.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.4" />
                  <Point X="4.353555889004" Y="2.610094092344" Z="1.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.4" />
                  <Point X="4.132415321535" Y="2.947741829066" Z="1.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.4" />
                  <Point X="3.178941517548" Y="2.653282793881" Z="1.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.4" />
                  <Point X="2.473884071255" Y="2.257373543742" Z="1.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.4" />
                  <Point X="2.398467176181" Y="2.249282210551" Z="1.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.4" />
                  <Point X="2.331784294938" Y="2.273159206516" Z="1.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.4" />
                  <Point X="2.277599393719" Y="2.325240565444" Z="1.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.4" />
                  <Point X="2.250147492208" Y="2.391291267651" Z="1.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.4" />
                  <Point X="2.255154398915" Y="2.465585504627" Z="1.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.4" />
                  <Point X="2.742342558979" Y="3.333197564407" Z="1.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.4" />
                  <Point X="2.960449282218" Y="4.121859568368" Z="1.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.4" />
                  <Point X="2.575185672225" Y="4.372917082729" Z="1.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.4" />
                  <Point X="2.171175719584" Y="4.587078569254" Z="1.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.4" />
                  <Point X="1.752467420256" Y="4.762788058308" Z="1.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.4" />
                  <Point X="1.223456814604" Y="4.919096030794" Z="1.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.4" />
                  <Point X="0.562492508346" Y="5.037652910583" Z="1.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.4" />
                  <Point X="0.08663518721" Y="4.678451410464" Z="1.4" />
                  <Point X="0" Y="4.355124473572" Z="1.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>