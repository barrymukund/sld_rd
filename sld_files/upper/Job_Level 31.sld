<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#177" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2280" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.800066101074" Y="20.603859375" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557720153809" Y="21.502861328125" />
                  <Point X="0.542363037109" Y="21.532623046875" />
                  <Point X="0.445591644287" Y="21.67205078125" />
                  <Point X="0.378635375977" Y="21.7685234375" />
                  <Point X="0.356752960205" Y="21.7909765625" />
                  <Point X="0.330498077393" Y="21.810220703125" />
                  <Point X="0.302494873047" Y="21.824330078125" />
                  <Point X="0.152746505737" Y="21.8708046875" />
                  <Point X="0.049135734558" Y="21.902962890625" />
                  <Point X="0.020983469009" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036824085236" Y="21.90296484375" />
                  <Point X="-0.186572158813" Y="21.85648828125" />
                  <Point X="-0.290183227539" Y="21.82433203125" />
                  <Point X="-0.318184448242" Y="21.810224609375" />
                  <Point X="-0.344439941406" Y="21.79098046875" />
                  <Point X="-0.366323425293" Y="21.7685234375" />
                  <Point X="-0.463094970703" Y="21.62909375" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.714807189941" Y="20.8761015625" />
                  <Point X="-0.916584777832" Y="20.12305859375" />
                  <Point X="-0.961041259766" Y="20.1316875" />
                  <Point X="-1.079320922852" Y="20.154646484375" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.243463867188" Y="20.220076171875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.252283569336" Y="20.663626953125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.461513793945" Y="20.989703125" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583188720703" Y="21.089705078125" />
                  <Point X="-1.612885620117" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.826010864258" Y="21.121025390625" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.9834140625" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.19512890625" Y="21.003322265625" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312791259766" Y="20.923173828125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.427073486328" Y="20.780677734375" />
                  <Point X="-2.480147460938" Y="20.711509765625" />
                  <Point X="-2.628312255859" Y="20.80325" />
                  <Point X="-2.801726074219" Y="20.910623046875" />
                  <Point X="-3.037864501953" Y="21.092443359375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.863760498047" Y="21.561279296875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414561767578" Y="22.4444296875" />
                  <Point X="-2.428780761719" Y="22.473259765625" />
                  <Point X="-2.44680859375" Y="22.498416015625" />
                  <Point X="-2.464157226562" Y="22.515763671875" />
                  <Point X="-2.489317626953" Y="22.533791015625" />
                  <Point X="-2.518146240234" Y="22.548005859375" />
                  <Point X="-2.547762939453" Y="22.55698828125" />
                  <Point X="-2.5786953125" Y="22.555974609375" />
                  <Point X="-2.610219726562" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.166481933594" Y="22.234365234375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.9458671875" Y="22.026158203125" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.252163085938" Y="22.490033203125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.875810546875" Y="22.91075390625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577148438" Y="23.524404296875" />
                  <Point X="-3.066612548828" Y="23.55153515625" />
                  <Point X="-3.053856689453" Y="23.580166015625" />
                  <Point X="-3.046152099609" Y="23.6099140625" />
                  <Point X="-3.043347900391" Y="23.640341796875" />
                  <Point X="-3.045556884766" Y="23.672013671875" />
                  <Point X="-3.052559082031" Y="23.70176171875" />
                  <Point X="-3.068644042969" Y="23.72774609375" />
                  <Point X="-3.089479492188" Y="23.751703125" />
                  <Point X="-3.112977050781" Y="23.771234375" />
                  <Point X="-3.139459716797" Y="23.7868203125" />
                  <Point X="-3.168723144531" Y="23.79804296875" />
                  <Point X="-3.200607666016" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.897593017578" Y="23.71798046875" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.780498046875" Y="23.797583984375" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.87887109375" Y="24.32053515625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.409857910156" Y="24.544603515625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.509680664062" Y="24.78924609375" />
                  <Point X="-3.477709960938" Y="24.8078515625" />
                  <Point X="-3.464513427734" Y="24.81711328125" />
                  <Point X="-3.441344970703" Y="24.8365078125" />
                  <Point X="-3.425367919922" Y="24.85365234375" />
                  <Point X="-3.414073730469" Y="24.874185546875" />
                  <Point X="-3.401180175781" Y="24.906541015625" />
                  <Point X="-3.396713867188" Y="24.9210078125" />
                  <Point X="-3.39068359375" Y="24.948017578125" />
                  <Point X="-3.388403076172" Y="24.969375" />
                  <Point X="-3.390978515625" Y="24.99069921875" />
                  <Point X="-3.398693359375" Y="25.023138671875" />
                  <Point X="-3.403410888672" Y="25.037666015625" />
                  <Point X="-3.414619873047" Y="25.06459375" />
                  <Point X="-3.426225830078" Y="25.084953125" />
                  <Point X="-3.442462646484" Y="25.101853515625" />
                  <Point X="-3.470684814453" Y="25.124755859375" />
                  <Point X="-3.484072753906" Y="25.13384375" />
                  <Point X="-3.510989501953" Y="25.14894140625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.139661132812" Y="25.320435546875" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.859018554688" Y="25.74362109375" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.7343203125" Y="26.30971875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.394308105469" Y="26.3825546875" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423828125" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.666832275391" Y="26.3167109375" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.62278515625" Y="26.332958984375" />
                  <Point X="-3.604040771484" Y="26.343779296875" />
                  <Point X="-3.587355957031" Y="26.35601171875" />
                  <Point X="-3.573715087891" Y="26.37156640625" />
                  <Point X="-3.561300292969" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.536783691406" Y="26.442599609375" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532050048828" Y="26.58937890625" />
                  <Point X="-3.549627685547" Y="26.623146484375" />
                  <Point X="-3.561789794922" Y="26.6465078125" />
                  <Point X="-3.573284667969" Y="26.6637109375" />
                  <Point X="-3.587196533203" Y="26.6802890625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.950188720703" Y="26.961662109375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.215324707031" Y="27.50379296875" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.842303710938" Y="28.040666015625" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.592751464844" Y="28.067583984375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167086181641" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.096229980469" Y="27.82137109375" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040560302734" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.910187255859" Y="27.896119140625" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.847859619141" Y="28.08668359375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.024027832031" Y="28.448671875" />
                  <Point X="-3.183333496094" Y="28.724595703125" />
                  <Point X="-2.934304931641" Y="28.915525390625" />
                  <Point X="-2.700630371094" Y="29.094681640625" />
                  <Point X="-2.324446533203" Y="29.3036796875" />
                  <Point X="-2.167037109375" Y="29.3911328125" />
                  <Point X="-2.161391113281" Y="29.383775390625" />
                  <Point X="-2.043195800781" Y="29.2297421875" />
                  <Point X="-2.028893432617" Y="29.21480078125" />
                  <Point X="-2.012314697266" Y="29.200888671875" />
                  <Point X="-1.995114624023" Y="29.189396484375" />
                  <Point X="-1.938836791992" Y="29.160099609375" />
                  <Point X="-1.89989855957" Y="29.139828125" />
                  <Point X="-1.880625976562" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.83926953125" Y="29.123580078125" />
                  <Point X="-1.818623291016" Y="29.12493359375" />
                  <Point X="-1.797307495117" Y="29.128693359375" />
                  <Point X="-1.777452636719" Y="29.134482421875" />
                  <Point X="-1.718835571289" Y="29.158763671875" />
                  <Point X="-1.678278686523" Y="29.1755625" />
                  <Point X="-1.660147338867" Y="29.185509765625" />
                  <Point X="-1.642417358398" Y="29.197923828125" />
                  <Point X="-1.626864990234" Y="29.2115625" />
                  <Point X="-1.614633422852" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.576401367188" Y="29.326431640625" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.575265014648" Y="29.564013671875" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-1.252147705078" Y="29.724994140625" />
                  <Point X="-0.949623535156" Y="29.80980859375" />
                  <Point X="-0.493597106934" Y="29.863181640625" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.248120742798" Y="29.712580078125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155908585" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.225242355347" Y="29.581248046875" />
                  <Point X="0.307419616699" Y="29.887939453125" />
                  <Point X="0.57989642334" Y="29.85940234375" />
                  <Point X="0.84403125" Y="29.831740234375" />
                  <Point X="1.221337890625" Y="29.740646484375" />
                  <Point X="1.481039794922" Y="29.6779453125" />
                  <Point X="1.726005249023" Y="29.589095703125" />
                  <Point X="1.894645141602" Y="29.527927734375" />
                  <Point X="2.132115234375" Y="29.41687109375" />
                  <Point X="2.294558105469" Y="29.340900390625" />
                  <Point X="2.5240078125" Y="29.207224609375" />
                  <Point X="2.680975097656" Y="29.115775390625" />
                  <Point X="2.897340332031" Y="28.961908203125" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.656847167969" Y="28.433171875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.516056640625" />
                  <Point X="2.120387207031" Y="27.468603515625" />
                  <Point X="2.111607177734" Y="27.43576953125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.112676025391" Y="27.33991796875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.1654609375" Y="27.21005078125" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.194469238281" Y="27.170322265625" />
                  <Point X="2.221596923828" Y="27.14559375" />
                  <Point X="2.259015380859" Y="27.120203125" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304946533203" Y="27.0922734375" />
                  <Point X="2.327035400391" Y="27.084005859375" />
                  <Point X="2.348965087891" Y="27.078662109375" />
                  <Point X="2.389998779297" Y="27.07371484375" />
                  <Point X="2.418389892578" Y="27.070291015625" />
                  <Point X="2.436467529297" Y="27.06984375" />
                  <Point X="2.473207519531" Y="27.074169921875" />
                  <Point X="2.520660888672" Y="27.086859375" />
                  <Point X="2.553493896484" Y="27.095640625" />
                  <Point X="2.565292480469" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.198842285156" Y="27.4625078125" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="4.030161621094" Y="27.81886328125" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.243891113281" Y="27.49013671875" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.901280517578" Y="27.18294140625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221421630859" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.169821533203" Y="26.597072265625" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606933594" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.108908203125" Y="26.471595703125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.108182617188" Y="26.321154296875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282714844" Y="26.23573828125" />
                  <Point X="3.164687255859" Y="26.192564453125" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198894042969" Y="26.14544921875" />
                  <Point X="3.216135986328" Y="26.129361328125" />
                  <Point X="3.234346435547" Y="26.11603515625" />
                  <Point X="3.275508300781" Y="26.092865234375" />
                  <Point X="3.303988525391" Y="26.07683203125" />
                  <Point X="3.320523681641" Y="26.0695" />
                  <Point X="3.356119628906" Y="26.0594375" />
                  <Point X="3.4117734375" Y="26.05208203125" />
                  <Point X="3.450280273438" Y="26.046994140625" />
                  <Point X="3.462699462891" Y="26.04617578125" />
                  <Point X="3.488203857422" Y="26.046984375" />
                  <Point X="4.067955810547" Y="26.123310546875" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.805945800781" Y="26.097076171875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.883946289062" Y="25.688673828125" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.484983398438" Y="25.535484375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788818359" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.626869384766" Y="25.28346484375" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574312988281" Y="25.25109765625" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.514723388672" Y="25.183771484375" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.452745117188" Y="25.035501953125" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.456114746094" Y="24.88434375" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.524831298828" Y="24.740787109375" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.560000732422" Y="24.69876171875" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.643713623047" Y="24.64423828125" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692708251953" Y="24.616861328125" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.248238769531" Y="24.465390625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.8773515625" Y="24.199376953125" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.806324707031" Y="23.837873046875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.315514160156" Y="23.87923828125" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.267345458984" Y="23.971166015625" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163973144531" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112397460938" Y="23.9060390625" />
                  <Point X="3.047532958984" Y="23.82802734375" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.9604609375" Y="23.59360546875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.035839599609" Y="23.339626953125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.604011230469" Y="22.86050390625" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.187752929688" Y="22.352064453125" />
                  <Point X="4.124814453125" Y="22.25021875" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.594627685547" Y="22.364828125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.626504150391" Y="22.863240234375" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.338729248047" Y="22.80898046875" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.148689941406" Y="22.60345703125" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.118741699219" Y="22.309021484375" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.4688671875" Y="21.62477734375" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.856539306641" Y="20.941705078125" />
                  <Point X="2.781833251953" Y="20.88834375" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="2.364192138672" Y="21.276451171875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.595956787109" Y="22.180427734375" />
                  <Point X="1.508800292969" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417100830078" Y="22.258880859375" />
                  <Point X="1.279334472656" Y="22.246205078125" />
                  <Point X="1.184013916016" Y="22.23743359375" />
                  <Point X="1.156362548828" Y="22.2306015625" />
                  <Point X="1.128976806641" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.2045390625" />
                  <Point X="0.998215576172" Y="22.116087890625" />
                  <Point X="0.924611633301" Y="22.05488671875" />
                  <Point X="0.904140441895" Y="22.031134765625" />
                  <Point X="0.887248596191" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.843817443848" Y="21.827853515625" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.909591247559" Y="20.99441015625" />
                  <Point X="1.022065551758" Y="20.140083984375" />
                  <Point X="0.975707824707" Y="20.129921875" />
                  <Point X="0.929315673828" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.05841394043" Y="20.247361328125" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.159109008789" Y="20.68216015625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250210327148" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.398875854492" Y="21.061126953125" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735229492" Y="21.15403125" />
                  <Point X="-1.533018432617" Y="21.170376953125" />
                  <Point X="-1.546833984375" Y="21.177474609375" />
                  <Point X="-1.576530883789" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.621457763672" Y="21.201552734375" />
                  <Point X="-1.636815063477" Y="21.203830078125" />
                  <Point X="-1.819798217773" Y="21.215822265625" />
                  <Point X="-1.946404296875" Y="21.22412109375" />
                  <Point X="-1.961928344727" Y="21.2238671875" />
                  <Point X="-1.992725585938" Y="21.220833984375" />
                  <Point X="-2.007998535156" Y="21.2180546875" />
                  <Point X="-2.039047607422" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436035156" Y="21.184189453125" />
                  <Point X="-2.247907714844" Y="21.0823125" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359687744141" Y="21.007240234375" />
                  <Point X="-2.380450683594" Y="20.989861328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.502442138672" Y="20.838509765625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.578301025391" Y="20.884021484375" />
                  <Point X="-2.747613525391" Y="20.98885546875" />
                  <Point X="-2.979906982422" Y="21.16771484375" />
                  <Point X="-2.980862792969" Y="21.168451171875" />
                  <Point X="-2.781488037109" Y="21.513779296875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314667480469" Y="22.442390625" />
                  <Point X="-2.323653320313" Y="22.472009765625" />
                  <Point X="-2.329360595703" Y="22.486451171875" />
                  <Point X="-2.343579589844" Y="22.51528125" />
                  <Point X="-2.351562011719" Y="22.52859765625" />
                  <Point X="-2.36958984375" Y="22.55375390625" />
                  <Point X="-2.379635253906" Y="22.56559375" />
                  <Point X="-2.396983886719" Y="22.58294140625" />
                  <Point X="-2.408826660156" Y="22.59298828125" />
                  <Point X="-2.433987060547" Y="22.611015625" />
                  <Point X="-2.4473046875" Y="22.61899609375" />
                  <Point X="-2.476133300781" Y="22.6332109375" />
                  <Point X="-2.490573974609" Y="22.638916015625" />
                  <Point X="-2.520190673828" Y="22.6478984375" />
                  <Point X="-2.550874511719" Y="22.6519375" />
                  <Point X="-2.581806884766" Y="22.650923828125" />
                  <Point X="-2.597231445312" Y="22.6491484375" />
                  <Point X="-2.628755859375" Y="22.642876953125" />
                  <Point X="-2.643686523438" Y="22.63861328125" />
                  <Point X="-2.672650390625" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.213981933594" Y="22.31663671875" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.870273681641" Y="22.083697265625" />
                  <Point X="-4.004021484375" Y="22.2594140625" />
                  <Point X="-4.1705703125" Y="22.53869140625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.817978271484" Y="22.835384765625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036482421875" Y="23.4366875" />
                  <Point X="-3.015104980469" Y="23.459607421875" />
                  <Point X="-3.005367431641" Y="23.471955078125" />
                  <Point X="-2.987402832031" Y="23.4990859375" />
                  <Point X="-2.979835449219" Y="23.512873046875" />
                  <Point X="-2.967079589844" Y="23.54150390625" />
                  <Point X="-2.961891113281" Y="23.55634765625" />
                  <Point X="-2.954186523438" Y="23.586095703125" />
                  <Point X="-2.951552978516" Y="23.6011953125" />
                  <Point X="-2.948748779297" Y="23.631623046875" />
                  <Point X="-2.948578125" Y="23.646951171875" />
                  <Point X="-2.950787109375" Y="23.678623046875" />
                  <Point X="-2.953083984375" Y="23.693779296875" />
                  <Point X="-2.960086181641" Y="23.72352734375" />
                  <Point X="-2.971782958984" Y="23.751763671875" />
                  <Point X="-2.987867919922" Y="23.777748046875" />
                  <Point X="-2.996961425781" Y="23.790087890625" />
                  <Point X="-3.017796875" Y="23.814044921875" />
                  <Point X="-3.02875390625" Y="23.824759765625" />
                  <Point X="-3.052251464844" Y="23.844291015625" />
                  <Point X="-3.064791992188" Y="23.853107421875" />
                  <Point X="-3.091274658203" Y="23.868693359375" />
                  <Point X="-3.105442626953" Y="23.875521484375" />
                  <Point X="-3.134706054688" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891138671875" />
                  <Point X="-3.181686035156" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-3.909992675781" Y="23.81216796875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.688453125" Y="23.821095703125" />
                  <Point X="-4.740762207031" Y="24.025880859375" />
                  <Point X="-4.784828125" Y="24.333986328125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.385270019531" Y="24.45283984375" />
                  <Point X="-3.508287841797" Y="24.687826171875" />
                  <Point X="-3.496364746094" Y="24.69188671875" />
                  <Point X="-3.473169433594" Y="24.70154296875" />
                  <Point X="-3.461897460938" Y="24.707138671875" />
                  <Point X="-3.429926757812" Y="24.725744140625" />
                  <Point X="-3.423135498047" Y="24.730091796875" />
                  <Point X="-3.403533691406" Y="24.744267578125" />
                  <Point X="-3.380365234375" Y="24.763662109375" />
                  <Point X="-3.371845214844" Y="24.771740234375" />
                  <Point X="-3.355868164062" Y="24.788884765625" />
                  <Point X="-3.34212890625" Y="24.8078671875" />
                  <Point X="-3.330834716797" Y="24.828400390625" />
                  <Point X="-3.325822753906" Y="24.839017578125" />
                  <Point X="-3.312929199219" Y="24.871373046875" />
                  <Point X="-3.310407714844" Y="24.878517578125" />
                  <Point X="-3.303996582031" Y="24.900306640625" />
                  <Point X="-3.297966308594" Y="24.92731640625" />
                  <Point X="-3.296220703125" Y="24.937931640625" />
                  <Point X="-3.293940185547" Y="24.9592890625" />
                  <Point X="-3.294088378906" Y="24.980765625" />
                  <Point X="-3.296663818359" Y="25.00208984375" />
                  <Point X="-3.298556152344" Y="25.0126796875" />
                  <Point X="-3.306270996094" Y="25.045119140625" />
                  <Point X="-3.308338134766" Y="25.05248046875" />
                  <Point X="-3.315706054688" Y="25.074173828125" />
                  <Point X="-3.326915039062" Y="25.1011015625" />
                  <Point X="-3.332087890625" Y="25.111640625" />
                  <Point X="-3.343693847656" Y="25.132" />
                  <Point X="-3.357719238281" Y="25.15076953125" />
                  <Point X="-3.373956054688" Y="25.167669921875" />
                  <Point X="-3.382600585938" Y="25.17562109375" />
                  <Point X="-3.410822753906" Y="25.1985234375" />
                  <Point X="-3.417329345703" Y="25.203357421875" />
                  <Point X="-3.437598632812" Y="25.21669921875" />
                  <Point X="-3.464515380859" Y="25.231796875" />
                  <Point X="-3.475175537109" Y="25.236931640625" />
                  <Point X="-3.497062011719" Y="25.24583984375" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.115073730469" Y="25.41219921875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.765041992188" Y="25.72971484375" />
                  <Point X="-4.73133203125" Y="25.957521484375" />
                  <Point X="-4.642626953125" Y="26.28487109375" />
                  <Point X="-4.633585449219" Y="26.318236328125" />
                  <Point X="-4.406708007812" Y="26.2883671875" />
                  <Point X="-3.778066650391" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703369141" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704889892578" Y="26.2080546875" />
                  <Point X="-3.684604248047" Y="26.21208984375" />
                  <Point X="-3.674571044922" Y="26.21466015625" />
                  <Point X="-3.638265136719" Y="26.226107421875" />
                  <Point X="-3.613145019531" Y="26.23402734375" />
                  <Point X="-3.603451171875" Y="26.23767578125" />
                  <Point X="-3.584524169922" Y="26.24600390625" />
                  <Point X="-3.575291015625" Y="26.25068359375" />
                  <Point X="-3.556546630859" Y="26.26150390625" />
                  <Point X="-3.547870605469" Y="26.2671640625" />
                  <Point X="-3.531185791016" Y="26.279396484375" />
                  <Point X="-3.515930664062" Y="26.293375" />
                  <Point X="-3.502289794922" Y="26.3089296875" />
                  <Point X="-3.495895263672" Y="26.317078125" />
                  <Point X="-3.48348046875" Y="26.33480859375" />
                  <Point X="-3.478012695312" Y="26.343599609375" />
                  <Point X="-3.468063964844" Y="26.361732421875" />
                  <Point X="-3.463583007813" Y="26.37107421875" />
                  <Point X="-3.449015136719" Y="26.406244140625" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432791259766" Y="26.594689453125" />
                  <Point X="-3.436012451172" Y="26.60453125" />
                  <Point X="-3.443508544922" Y="26.62380859375" />
                  <Point X="-3.447783447266" Y="26.633244140625" />
                  <Point X="-3.465361083984" Y="26.66701171875" />
                  <Point X="-3.477523193359" Y="26.690373046875" />
                  <Point X="-3.482800537109" Y="26.699287109375" />
                  <Point X="-3.494295410156" Y="26.716490234375" />
                  <Point X="-3.500512939453" Y="26.724779296875" />
                  <Point X="-3.514424804688" Y="26.741357421875" />
                  <Point X="-3.521503662109" Y="26.7489140625" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.892356201172" Y="27.03703125" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.133278320312" Y="27.455904296875" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.767322998047" Y="27.98233203125" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.640251464844" Y="27.9853125" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923583984" Y="27.757720703125" />
                  <Point X="-3.225995361328" Y="27.749390625" />
                  <Point X="-3.216300537109" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165331298828" Y="27.73262109375" />
                  <Point X="-3.155073974609" Y="27.73115625" />
                  <Point X="-3.104509765625" Y="27.726732421875" />
                  <Point X="-3.069524658203" Y="27.723671875" />
                  <Point X="-3.059173339844" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155517578" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.90274609375" Y="27.773193359375" />
                  <Point X="-2.886614501953" Y="27.786140625" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.843012451172" Y="27.828943359375" />
                  <Point X="-2.8181796875" Y="27.853775390625" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753221191406" Y="28.094962890625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.941755371094" Y="28.496171875" />
                  <Point X="-3.059388183594" Y="28.699916015625" />
                  <Point X="-2.876502441406" Y="28.840134765625" />
                  <Point X="-2.648385498047" Y="29.015029296875" />
                  <Point X="-2.278309326172" Y="29.220634765625" />
                  <Point X="-2.192524658203" Y="29.268294921875" />
                  <Point X="-2.118562255859" Y="29.17190625" />
                  <Point X="-2.111822509766" Y="29.16405078125" />
                  <Point X="-2.097520019531" Y="29.149109375" />
                  <Point X="-2.089960449219" Y="29.142029296875" />
                  <Point X="-2.073381835938" Y="29.1281171875" />
                  <Point X="-2.065092285156" Y="29.1218984375" />
                  <Point X="-2.047892089844" Y="29.11040625" />
                  <Point X="-2.038981323242" Y="29.105130859375" />
                  <Point X="-1.982703491211" Y="29.075833984375" />
                  <Point X="-1.943765258789" Y="29.0555625" />
                  <Point X="-1.934335693359" Y="29.0512890625" />
                  <Point X="-1.915063110352" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313476562" Y="29.034966796875" />
                  <Point X="-1.874175170898" Y="29.032833984375" />
                  <Point X="-1.853725463867" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812408691406" Y="29.03013671875" />
                  <Point X="-1.802121582031" Y="29.031376953125" />
                  <Point X="-1.780805786133" Y="29.03513671875" />
                  <Point X="-1.770715698242" Y="29.037490234375" />
                  <Point X="-1.750860839844" Y="29.043279296875" />
                  <Point X="-1.741096069336" Y="29.04671484375" />
                  <Point X="-1.682479003906" Y="29.07099609375" />
                  <Point X="-1.64192199707" Y="29.087794921875" />
                  <Point X="-1.632584472656" Y="29.0922734375" />
                  <Point X="-1.614453125" Y="29.102220703125" />
                  <Point X="-1.605659301758" Y="29.107689453125" />
                  <Point X="-1.587929321289" Y="29.120103515625" />
                  <Point X="-1.579780639648" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252929688" Y="29.155388671875" />
                  <Point X="-1.538021362305" Y="29.1720703125" />
                  <Point X="-1.532361450195" Y="29.180744140625" />
                  <Point X="-1.521539306641" Y="29.19948828125" />
                  <Point X="-1.516856811523" Y="29.2087265625" />
                  <Point X="-1.508525756836" Y="29.22766015625" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.485798339844" Y="29.297865234375" />
                  <Point X="-1.472597900391" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266235352" Y="29.562654296875" />
                  <Point X="-1.226501586914" Y="29.633521484375" />
                  <Point X="-0.931164306641" Y="29.7163203125" />
                  <Point X="-0.482553741455" Y="29.768826171875" />
                  <Point X="-0.365222564697" Y="29.78255859375" />
                  <Point X="-0.339883758545" Y="29.6879921875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.317005371094" Y="29.55666015625" />
                  <Point X="0.378190765381" Y="29.7850078125" />
                  <Point X="0.570000915527" Y="29.764919921875" />
                  <Point X="0.827852783203" Y="29.737916015625" />
                  <Point X="1.199042480469" Y="29.648298828125" />
                  <Point X="1.453624023438" Y="29.586833984375" />
                  <Point X="1.69361328125" Y="29.4997890625" />
                  <Point X="1.858245483398" Y="29.44007421875" />
                  <Point X="2.091870605469" Y="29.33081640625" />
                  <Point X="2.250431884766" Y="29.25666015625" />
                  <Point X="2.476185302734" Y="29.125138671875" />
                  <Point X="2.629430419922" Y="29.035857421875" />
                  <Point X="2.817778808594" Y="28.9019140625" />
                  <Point X="2.574574707031" Y="28.480671875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053180664062" Y="27.573435546875" />
                  <Point X="2.044182373047" Y="27.549564453125" />
                  <Point X="2.041301757812" Y="27.540599609375" />
                  <Point X="2.028612060547" Y="27.493146484375" />
                  <Point X="2.01983203125" Y="27.4603125" />
                  <Point X="2.017912231445" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.01835925293" Y="27.328544921875" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040735717773" Y="27.234216796875" />
                  <Point X="2.045319091797" Y="27.22388671875" />
                  <Point X="2.055680664062" Y="27.20384375" />
                  <Point X="2.061458740234" Y="27.194130859375" />
                  <Point X="2.086848876953" Y="27.1567109375" />
                  <Point X="2.104416503906" Y="27.130822265625" />
                  <Point X="2.109807373047" Y="27.123634765625" />
                  <Point X="2.130470458984" Y="27.100115234375" />
                  <Point X="2.157598144531" Y="27.07538671875" />
                  <Point X="2.168254882812" Y="27.066982421875" />
                  <Point X="2.205673339844" Y="27.041591796875" />
                  <Point X="2.231563476562" Y="27.0240234375" />
                  <Point X="2.241276123047" Y="27.01824609375" />
                  <Point X="2.261317138672" Y="27.007884765625" />
                  <Point X="2.271645507812" Y="27.00330078125" />
                  <Point X="2.293734375" Y="26.995033203125" />
                  <Point X="2.304544189453" Y="26.99170703125" />
                  <Point X="2.326473876953" Y="26.98636328125" />
                  <Point X="2.33759375" Y="26.984345703125" />
                  <Point X="2.378627441406" Y="26.9793984375" />
                  <Point X="2.407018554688" Y="26.975974609375" />
                  <Point X="2.416040283203" Y="26.9753203125" />
                  <Point X="2.447577148438" Y="26.97549609375" />
                  <Point X="2.484317138672" Y="26.979822265625" />
                  <Point X="2.497749023438" Y="26.98239453125" />
                  <Point X="2.545202392578" Y="26.995083984375" />
                  <Point X="2.578035400391" Y="27.003865234375" />
                  <Point X="2.583995849609" Y="27.005669921875" />
                  <Point X="2.604423339844" Y="27.01307421875" />
                  <Point X="2.627664794922" Y="27.023580078125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.246342285156" Y="27.380236328125" />
                  <Point X="3.940403076172" Y="27.780951171875" />
                  <Point X="3.953049072266" Y="27.763376953125" />
                  <Point X="4.043958007812" Y="27.63703515625" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.843448242188" Y="27.258310546875" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168135986328" Y="26.7398671875" />
                  <Point X="3.152114990234" Y="26.725212890625" />
                  <Point X="3.134667236328" Y="26.7066015625" />
                  <Point X="3.128576171875" Y="26.699421875" />
                  <Point X="3.094423828125" Y="26.6548671875" />
                  <Point X="3.070793945312" Y="26.624041015625" />
                  <Point X="3.065634033203" Y="26.616599609375" />
                  <Point X="3.049740722656" Y="26.589373046875" />
                  <Point X="3.034763671875" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.017418457031" Y="26.497181640625" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.015142578125" Y="26.30195703125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682617188" Y="26.228611328125" />
                  <Point X="3.050285888672" Y="26.19537109375" />
                  <Point X="3.056918701172" Y="26.1835234375" />
                  <Point X="3.085323242188" Y="26.140349609375" />
                  <Point X="3.104976318359" Y="26.110478515625" />
                  <Point X="3.111740966797" Y="26.101419921875" />
                  <Point X="3.126294677734" Y="26.08417578125" />
                  <Point X="3.134083740234" Y="26.075990234375" />
                  <Point X="3.151325683594" Y="26.05990234375" />
                  <Point X="3.160033447266" Y="26.052697265625" />
                  <Point X="3.178243896484" Y="26.03937109375" />
                  <Point X="3.187746582031" Y="26.03325" />
                  <Point X="3.228908447266" Y="26.010080078125" />
                  <Point X="3.257388671875" Y="25.994046875" />
                  <Point X="3.265479736328" Y="25.989986328125" />
                  <Point X="3.294681152344" Y="25.97808203125" />
                  <Point X="3.330277099609" Y="25.96801953125" />
                  <Point X="3.343672119141" Y="25.965255859375" />
                  <Point X="3.399325927734" Y="25.957900390625" />
                  <Point X="3.437832763672" Y="25.9528125" />
                  <Point X="3.444033935547" Y="25.95219921875" />
                  <Point X="3.465709960938" Y="25.95122265625" />
                  <Point X="3.491214355469" Y="25.95203125" />
                  <Point X="3.500604003906" Y="25.952796875" />
                  <Point X="4.080355957031" Y="26.029123046875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.713641601562" Y="26.07460546875" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.460395507812" Y="25.627248046875" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686020263672" Y="25.419541015625" />
                  <Point X="3.665627441406" Y="25.41213671875" />
                  <Point X="3.642386230469" Y="25.40162109375" />
                  <Point X="3.634008056641" Y="25.397318359375" />
                  <Point X="3.579329833984" Y="25.36571484375" />
                  <Point X="3.541498046875" Y="25.34384765625" />
                  <Point X="3.533881103516" Y="25.3389453125" />
                  <Point X="3.508776611328" Y="25.319873046875" />
                  <Point X="3.481993896484" Y="25.2943515625" />
                  <Point X="3.472795654297" Y="25.284224609375" />
                  <Point X="3.439988769531" Y="25.242419921875" />
                  <Point X="3.417289794922" Y="25.21349609375" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130615234" Y="25.1849296875" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.359440917969" Y="25.05337109375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.362810546875" Y="24.866474609375" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.816013671875" />
                  <Point X="3.380004394531" Y="24.794515625" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417289794922" Y="24.723943359375" />
                  <Point X="3.450096679688" Y="24.682138671875" />
                  <Point X="3.472795654297" Y="24.65321484375" />
                  <Point X="3.478718261719" Y="24.646365234375" />
                  <Point X="3.501141113281" Y="24.624193359375" />
                  <Point X="3.53017578125" Y="24.601275390625" />
                  <Point X="3.541493652344" Y="24.593595703125" />
                  <Point X="3.596171875" Y="24.561990234375" />
                  <Point X="3.634003662109" Y="24.540123046875" />
                  <Point X="3.639498046875" Y="24.53718359375" />
                  <Point X="3.659150146484" Y="24.527986328125" />
                  <Point X="3.683021972656" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.223650878906" Y="24.373626953125" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.783413085938" Y="24.2135390625" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.727801757812" Y="23.920779296875" />
                  <Point X="4.3279140625" Y="23.97342578125" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354482421875" Y="24.087322265625" />
                  <Point X="3.247168701172" Y="24.063998046875" />
                  <Point X="3.172918212891" Y="24.047859375" />
                  <Point X="3.157874755859" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114674560547" Y="24.024609375" />
                  <Point X="3.086848144531" Y="24.007716796875" />
                  <Point X="3.074122314453" Y="23.99846875" />
                  <Point X="3.050373046875" Y="23.977998046875" />
                  <Point X="3.039349609375" Y="23.966775390625" />
                  <Point X="2.974485107422" Y="23.888763671875" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921327148438" Y="23.82315234375" />
                  <Point X="2.906606689453" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.865860595703" Y="23.602310546875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.9559296875" Y="23.288251953125" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.546178955078" Y="22.785134765625" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045498291016" Y="22.30258203125" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="3.642127685547" Y="22.447099609375" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.643388183594" Y="22.956728515625" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444846923828" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589599609" Y="22.948890625" />
                  <Point X="2.294485107422" Y="22.893048828125" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.208967773438" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.78883984375" />
                  <Point X="2.128047363281" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.064622070313" Y="22.647701171875" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.02525390625" Y="22.292138671875" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.386594726562" Y="21.57727734375" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.963916015625" />
                  <Point X="2.439560791016" Y="21.334283203125" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251464844" Y="22.17199609375" />
                  <Point X="1.773298217773" Y="22.179353515625" />
                  <Point X="1.647331298828" Y="22.260337890625" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470581055" Y="22.336126953125" />
                  <Point X="1.502546875" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384765625" Y="22.3513046875" />
                  <Point X="1.424120117188" Y="22.35362109375" />
                  <Point X="1.408396728516" Y="22.35348046875" />
                  <Point X="1.270630371094" Y="22.3408046875" />
                  <Point X="1.175309814453" Y="22.332033203125" />
                  <Point X="1.161226806641" Y="22.32966015625" />
                  <Point X="1.133575439453" Y="22.322828125" />
                  <Point X="1.120007080078" Y="22.318369140625" />
                  <Point X="1.092621337891" Y="22.307025390625" />
                  <Point X="1.079879638672" Y="22.300587890625" />
                  <Point X="1.055498046875" Y="22.285869140625" />
                  <Point X="1.043858154297" Y="22.277587890625" />
                  <Point X="0.937478637695" Y="22.18913671875" />
                  <Point X="0.863874633789" Y="22.127935546875" />
                  <Point X="0.852650756836" Y="22.116908203125" />
                  <Point X="0.832179626465" Y="22.09315625" />
                  <Point X="0.822932312012" Y="22.080431640625" />
                  <Point X="0.806040405273" Y="22.05260546875" />
                  <Point X="0.799018859863" Y="22.038529296875" />
                  <Point X="0.78739465332" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.750984985352" Y="21.84803125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.815403930664" Y="20.982009765625" />
                  <Point X="0.83309161377" Y="20.847658203125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143554688" Y="21.546423828125" />
                  <Point X="0.626786315918" Y="21.576185546875" />
                  <Point X="0.620407165527" Y="21.586791015625" />
                  <Point X="0.523635803223" Y="21.72621875" />
                  <Point X="0.456679534912" Y="21.82269140625" />
                  <Point X="0.446669647217" Y="21.834828125" />
                  <Point X="0.424787139893" Y="21.85728125" />
                  <Point X="0.412914550781" Y="21.86759765625" />
                  <Point X="0.386659729004" Y="21.886841796875" />
                  <Point X="0.373244415283" Y="21.895060546875" />
                  <Point X="0.345241210938" Y="21.909169921875" />
                  <Point X="0.330653411865" Y="21.915060546875" />
                  <Point X="0.180904998779" Y="21.96153515625" />
                  <Point X="0.077294174194" Y="21.993693359375" />
                  <Point X="0.063380393982" Y="21.996888671875" />
                  <Point X="0.035228092194" Y="22.001158203125" />
                  <Point X="0.020989723206" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022896093369" Y="22.001162109375" />
                  <Point X="-0.051062065125" Y="21.996892578125" />
                  <Point X="-0.064983718872" Y="21.9936953125" />
                  <Point X="-0.214731704712" Y="21.94721875" />
                  <Point X="-0.318342803955" Y="21.9150625" />
                  <Point X="-0.332927185059" Y="21.909171875" />
                  <Point X="-0.360928466797" Y="21.895064453125" />
                  <Point X="-0.374345275879" Y="21.88684765625" />
                  <Point X="-0.40060067749" Y="21.867603515625" />
                  <Point X="-0.41247833252" Y="21.85728125" />
                  <Point X="-0.434361724854" Y="21.83482421875" />
                  <Point X="-0.444367889404" Y="21.822689453125" />
                  <Point X="-0.541139404297" Y="21.683259765625" />
                  <Point X="-0.60809564209" Y="21.5867890625" />
                  <Point X="-0.612468444824" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.806570068359" Y="20.900689453125" />
                  <Point X="-0.985425292969" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.060865679287" Y="27.425139623008" />
                  <Point X="3.876679055064" Y="27.744160214236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.984846592948" Y="27.366808542891" />
                  <Point X="3.794406572885" Y="27.696660333435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767043292241" Y="25.822004118204" />
                  <Point X="4.607402369065" Y="26.098510308112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.90882750661" Y="27.308477462774" />
                  <Point X="3.712134090705" Y="27.649160452635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.728337946176" Y="25.699043744112" />
                  <Point X="4.505454841046" Y="26.085088606346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.8328084443" Y="27.250146341038" />
                  <Point X="3.629861608526" Y="27.601660571835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.633338076875" Y="25.673588344454" />
                  <Point X="4.403507313028" Y="26.07166690458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.75678952964" Y="27.191814963566" />
                  <Point X="3.547589126346" Y="27.554160691034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.793621090938" Y="28.860071635645" />
                  <Point X="2.735807513548" Y="28.960207689051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.538338207574" Y="25.648132944796" />
                  <Point X="4.301559785009" Y="26.058245202814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.68077061498" Y="27.133483586093" />
                  <Point X="3.465316644167" Y="27.506660810234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.738772836652" Y="28.765071598774" />
                  <Point X="2.558614427363" Y="29.077115117074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.44333833053" Y="25.622677558549" />
                  <Point X="4.199612256991" Y="26.044823501048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.60475170032" Y="27.075152208621" />
                  <Point X="3.383044161988" Y="27.459160929434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.683924582367" Y="28.670071561902" />
                  <Point X="2.393318580267" Y="29.173415922524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.348338418105" Y="25.597222233584" />
                  <Point X="4.097664728972" Y="26.031401799283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52873278566" Y="27.016820831148" />
                  <Point X="3.300771679808" Y="27.411661048633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629076328082" Y="28.575071525031" />
                  <Point X="2.230060283981" Y="29.266187586448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.25333850568" Y="25.571766908619" />
                  <Point X="3.995717206127" Y="26.017980088556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.452713871" Y="26.958489453676" />
                  <Point X="3.218499213772" Y="27.364161139872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574228073551" Y="28.480071488585" />
                  <Point X="2.079787860938" Y="29.336467058135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.158338593255" Y="25.546311583653" />
                  <Point X="3.89376968434" Y="26.004558375998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.37669495634" Y="26.900158076204" />
                  <Point X="3.136226779292" Y="27.316661176453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.519379780442" Y="28.385071518958" />
                  <Point X="1.929517516251" Y="29.406742930005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.06333868083" Y="25.520856258688" />
                  <Point X="3.791822162552" Y="25.991136663439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.300676041679" Y="26.841826698731" />
                  <Point X="3.053954344813" Y="27.269161213033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464531487333" Y="28.290071549331" />
                  <Point X="1.785301631512" Y="29.466532169631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767885223244" Y="24.11054585093" />
                  <Point X="4.687798627374" Y="24.249259903981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.968338768405" Y="25.495400933723" />
                  <Point X="3.689874640765" Y="25.97771495088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.224657127019" Y="26.783495321259" />
                  <Point X="2.971681910334" Y="27.221661249613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.409683194224" Y="28.195071579704" />
                  <Point X="1.646548517546" Y="29.516859612728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.739118615145" Y="23.970371077719" />
                  <Point X="4.558026398339" Y="24.284031998081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87333885598" Y="25.469945608758" />
                  <Point X="3.587927118978" Y="25.964293238322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.149945834449" Y="26.72289907589" />
                  <Point X="2.889409475855" Y="27.174161286194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.354834901116" Y="28.100071610077" />
                  <Point X="1.507796291365" Y="29.567185518137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.652316237996" Y="23.930717205158" />
                  <Point X="4.428254169305" Y="24.318804092181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.778338943554" Y="25.444490283793" />
                  <Point X="3.48541616235" Y="25.951847423534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0859270759" Y="26.643782818333" />
                  <Point X="2.807137041376" Y="27.126661322774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299986608007" Y="28.00507164045" />
                  <Point X="1.375925041816" Y="29.605593222414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.533595742522" Y="23.946347135219" />
                  <Point X="4.29848194027" Y="24.35357618628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.683562145441" Y="25.418648513504" />
                  <Point X="3.369986147614" Y="25.961778073775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.031853052793" Y="26.547441773726" />
                  <Point X="2.724864606896" Y="27.079161359354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.245138314898" Y="27.910071670823" />
                  <Point X="1.248460960553" Y="29.636367487301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.414875247048" Y="23.96197706528" />
                  <Point X="4.168709662867" Y="24.388348364156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.598147385491" Y="25.376591217454" />
                  <Point X="3.234084893821" Y="26.007165950156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001335188848" Y="26.410300264617" />
                  <Point X="2.642592172417" Y="27.031661395935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.190290021789" Y="27.815071701196" />
                  <Point X="1.120997009426" Y="29.667141526787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.296154753501" Y="23.977606992003" />
                  <Point X="4.038937319586" Y="24.423120656136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.517410203576" Y="25.326432118591" />
                  <Point X="2.552835019629" Y="26.997125344907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.13544172868" Y="27.720071731569" />
                  <Point X="0.9935331407" Y="29.69791542355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.17743426523" Y="23.993236909587" />
                  <Point X="3.909164976305" Y="24.457892948117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449339100457" Y="25.25433472772" />
                  <Point X="2.455113756758" Y="26.97638353718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.080593435571" Y="27.625071761942" />
                  <Point X="0.866069271974" Y="29.728689320313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.05871377696" Y="24.008866827171" />
                  <Point X="3.779392633023" Y="24.492665240097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.390155829361" Y="25.166843160215" />
                  <Point X="2.341061642738" Y="26.983927593373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03445312103" Y="27.514989131005" />
                  <Point X="0.746102766351" Y="29.746477403258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.939993288689" Y="24.024496744756" />
                  <Point X="3.645657770227" Y="24.534300817203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356086177357" Y="25.035853528484" />
                  <Point X="2.193177008963" Y="27.050071292729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.014652470008" Y="27.359284864596" />
                  <Point X="0.629346695511" Y="29.758704850045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.821272800418" Y="24.04012666234" />
                  <Point X="3.452027347284" Y="24.679678547632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.373037375944" Y="24.816493191282" />
                  <Point X="0.512590569886" Y="29.770932391721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.702552312148" Y="24.055756579924" />
                  <Point X="0.395834387631" Y="29.783160031485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583831823877" Y="24.071386497508" />
                  <Point X="0.348673046698" Y="29.674845870134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.465111335606" Y="24.087016415092" />
                  <Point X="0.313900698668" Y="29.54507334362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355177959812" Y="24.087426607416" />
                  <Point X="0.279128320969" Y="29.415300868496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.257687895137" Y="24.066284352666" />
                  <Point X="0.244355943269" Y="29.285528393372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.160773752809" Y="24.04414457115" />
                  <Point X="0.19554483786" Y="29.180071707914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.051221659694" Y="22.311843554932" />
                  <Point X="3.962328932682" Y="22.465810274541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.07646516296" Y="24.000171332282" />
                  <Point X="0.122719001232" Y="29.116209757058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974089023593" Y="22.25544119958" />
                  <Point X="3.765380697035" Y="22.616934625141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007892618164" Y="23.928942463874" />
                  <Point X="0.03016269864" Y="29.086521975708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.359618014791" Y="29.76164197518" />
                  <Point X="-0.37128460033" Y="29.781849094084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.809544007175" Y="22.350441528149" />
                  <Point X="3.568432461389" Y="22.768058975742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.943150989809" Y="23.851078253549" />
                  <Point X="-0.091249045632" Y="29.106813285422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.264618417331" Y="29.407097845682" />
                  <Point X="-0.474037810668" Y="29.769822875031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.644998990757" Y="22.445441856717" />
                  <Point X="3.371484830847" Y="22.919182278271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.88809666012" Y="23.756435149747" />
                  <Point X="-0.576790958335" Y="29.757796547427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.480454304142" Y="22.540441614049" />
                  <Point X="3.174537277386" Y="23.070305447292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866070018854" Y="23.60458641154" />
                  <Point X="-0.679544100337" Y="29.745770210013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.315909623385" Y="22.635441361236" />
                  <Point X="-0.78229724234" Y="29.733743872599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.151364942627" Y="22.730441108424" />
                  <Point X="-0.885050384343" Y="29.721717535186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986820261869" Y="22.825440855611" />
                  <Point X="-0.983207090833" Y="29.701729937931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.824669245616" Y="22.91629465426" />
                  <Point X="-1.077621519955" Y="29.675260526137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.697243701683" Y="22.947002170536" />
                  <Point X="-1.172035949077" Y="29.648791114344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574777968096" Y="22.969119043294" />
                  <Point X="-1.266450084163" Y="29.622321193265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.464985642084" Y="22.969284930227" />
                  <Point X="-1.360863818362" Y="29.595850577831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37487671262" Y="22.935358174274" />
                  <Point X="-1.455277552562" Y="29.569379962397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290744168595" Y="22.891080015097" />
                  <Point X="-1.463380295917" Y="29.393414325568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207304095822" Y="22.845602460527" />
                  <Point X="-1.497141349166" Y="29.261890185114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136777487231" Y="22.777758129892" />
                  <Point X="-1.547509445479" Y="29.159130287008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08252728233" Y="22.681722241101" />
                  <Point X="-1.621968695837" Y="29.09809749172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030216442812" Y="22.582327272933" />
                  <Point X="-1.709545142672" Y="29.059784347186" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000578447307" Y="22.443661786982" />
                  <Point X="-1.802793880845" Y="29.031295899442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.054970816972" Y="22.159451439179" />
                  <Point X="-1.921050052394" Y="29.046121596875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.36736099704" Y="21.428375775515" />
                  <Point X="-2.082508598125" Y="29.135776001397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.924290707424" Y="22.005796028454" />
                  <Point X="-2.250211228333" Y="29.236245477481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.679618808642" Y="22.239580188329" />
                  <Point X="-2.333266695222" Y="29.190101765979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.513393935347" Y="22.337490114358" />
                  <Point X="-2.416322197437" Y="29.143958115663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.395168075647" Y="22.352263310126" />
                  <Point X="-2.499377699652" Y="29.097814465348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.291004839531" Y="22.34267932736" />
                  <Point X="-2.582433201867" Y="29.051670815032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.186842138346" Y="22.333094418065" />
                  <Point X="-2.664043698618" Y="29.003024341834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.092292474949" Y="22.306859238909" />
                  <Point X="-2.740082057128" Y="28.944726642098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.013884531694" Y="22.252665780343" />
                  <Point X="-2.816120415638" Y="28.886428942362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.939767660037" Y="22.19103996775" />
                  <Point X="-2.89215869057" Y="28.828131097864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.865651470714" Y="22.129412973322" />
                  <Point X="-2.968196643163" Y="28.76983269506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803379518405" Y="22.047271158607" />
                  <Point X="-3.044234595757" Y="28.711534292255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766774181886" Y="21.920673461287" />
                  <Point X="-2.749135988763" Y="28.010408511699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736771649419" Y="21.782639371875" />
                  <Point X="-2.789526966599" Y="27.890367737479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.734493584658" Y="21.596585095783" />
                  <Point X="-2.856165989358" Y="27.815789910663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.766896299344" Y="21.350461947645" />
                  <Point X="-2.930666217169" Y="27.754828090408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799299014029" Y="21.104338799506" />
                  <Point X="0.734017028334" Y="21.21741051555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331403458473" Y="21.914757674367" />
                  <Point X="-3.023267382444" Y="27.725218013504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.831701694292" Y="20.858215710989" />
                  <Point X="0.829016584454" Y="20.862866457654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.197711290154" Y="21.956319302468" />
                  <Point X="-3.135398530366" Y="27.729434858815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.064777204388" Y="21.996567893073" />
                  <Point X="-3.269113130996" Y="27.771035340822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.045586050766" Y="21.997722658288" />
                  <Point X="-3.433658089114" Y="27.866035568411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.139588944974" Y="21.970540447115" />
                  <Point X="-3.598203047232" Y="27.961035796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.232616113555" Y="21.941668229582" />
                  <Point X="-3.740270577724" Y="28.017103976919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.325323905838" Y="21.912242836074" />
                  <Point X="-3.803238505566" Y="27.936167627188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.406358813221" Y="21.862599412848" />
                  <Point X="-3.866206370785" Y="27.855231168991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.470923199003" Y="21.784428209381" />
                  <Point X="-3.929174236004" Y="27.774294710795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.530806002822" Y="21.698148268095" />
                  <Point X="-3.992142101223" Y="27.693358252599" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.590688978312" Y="21.611868624152" />
                  <Point X="-3.421503210015" Y="26.514982700251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.588129913385" Y="26.803588616386" />
                  <Point X="-4.048551054907" Y="27.601061426381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.642756342455" Y="21.512051944264" />
                  <Point X="-3.457121084448" Y="26.386674668427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785078539629" Y="26.954713643521" />
                  <Point X="-4.103698985358" Y="27.506580443854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.677528703638" Y="21.382279440533" />
                  <Point X="-3.514133930984" Y="26.295423815311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.982025971" Y="27.105836601076" />
                  <Point X="-4.15884659558" Y="27.412098906675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.712301064822" Y="21.252506936803" />
                  <Point X="-3.593115363948" Y="26.24222367006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.178971972888" Y="27.256957082694" />
                  <Point X="-4.213993835343" Y="27.317616727842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.747073426006" Y="21.122734433072" />
                  <Point X="-3.685330711852" Y="26.211945337867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.78184578719" Y="20.992961929341" />
                  <Point X="-3.79246104357" Y="26.207500515434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.816618170562" Y="20.863189464043" />
                  <Point X="-3.911181431504" Y="26.22313025923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.851390608533" Y="20.733417093311" />
                  <Point X="-3.293973344154" Y="24.964094493098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440876122598" Y="25.218537569135" />
                  <Point X="-4.029901819438" Y="26.238760003026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.886163046503" Y="20.603644722579" />
                  <Point X="-3.328357098027" Y="24.83364890176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579536213838" Y="25.268703892146" />
                  <Point X="-4.148622207372" Y="26.254389746822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.920935484474" Y="20.473872351846" />
                  <Point X="-3.392015918489" Y="24.75390921315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.709308375874" Y="25.3034758702" />
                  <Point X="-4.267342595306" Y="26.270019490618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.955707922444" Y="20.344099981114" />
                  <Point X="-1.163430556944" Y="20.70388613795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.339743469589" Y="21.009269060682" />
                  <Point X="-3.471855417026" Y="24.702195281068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.83908053791" Y="25.338247848254" />
                  <Point X="-4.38606298324" Y="26.285649234414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.003385293475" Y="20.236679610111" />
                  <Point X="-1.119924226325" Y="20.438530962868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.546435279663" Y="21.177269777238" />
                  <Point X="-2.950773367479" Y="23.609654696339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103820187286" Y="23.874739564183" />
                  <Point X="-3.564551892676" Y="24.672750286575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.968852699946" Y="25.373019826308" />
                  <Point X="-4.504783461453" Y="26.301279134578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129862704048" Y="20.265744911233" />
                  <Point X="-1.139446325214" Y="20.282344230013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.672829122952" Y="21.206190335579" />
                  <Point X="-2.32106714311" Y="22.32897152189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502017484883" Y="22.642386707489" />
                  <Point X="-2.992333650325" Y="23.491639217805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.228418185273" Y="23.900549627217" />
                  <Point X="-3.659551910992" Y="24.64729514502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.098624861982" Y="25.407791804363" />
                  <Point X="-4.62350395867" Y="26.316909067658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.786839587144" Y="21.213662252154" />
                  <Point X="-2.370800268597" Y="22.225111822053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.613723654239" Y="22.645867468332" />
                  <Point X="-3.059263978273" Y="23.41756594638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.331089012191" Y="23.888380715894" />
                  <Point X="-3.754551929309" Y="24.621840003464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.228397393383" Y="25.442564422176" />
                  <Point X="-4.665650122524" Y="26.199908364797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.900850601181" Y="21.221135121088" />
                  <Point X="-2.42564852705" Y="22.1301117924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703499418785" Y="22.611363653813" />
                  <Point X="-3.135282928969" Y="23.359234631323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.433036698842" Y="23.874959288888" />
                  <Point X="-3.849551947625" Y="24.596384861908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.358169978397" Y="25.477337132851" />
                  <Point X="-4.700690183411" Y="26.070599530554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.00866549862" Y="21.217876001266" />
                  <Point X="-2.480496785503" Y="22.035111762747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.785771851844" Y="22.563863687933" />
                  <Point X="-3.211301879665" Y="23.300903316266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534984385493" Y="23.861537861881" />
                  <Point X="-3.944551965942" Y="24.570929720353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.487942563412" Y="25.512109843525" />
                  <Point X="-4.734141102323" Y="25.938538221668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.097945168802" Y="21.182512926105" />
                  <Point X="-2.535345043955" Y="21.940111733094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868044284902" Y="22.516363722054" />
                  <Point X="-3.28732083036" Y="23.242572001208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.636932072144" Y="23.848116434875" />
                  <Point X="-4.039551984259" Y="24.545474578797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.617715148426" Y="25.5468825542" />
                  <Point X="-4.756520650639" Y="25.787300736402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.177104563175" Y="21.129621019054" />
                  <Point X="-2.590193302408" Y="21.845111703441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950316717961" Y="22.468863756174" />
                  <Point X="-3.363339781056" Y="23.184240686151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.738879758795" Y="23.834695007868" />
                  <Point X="-4.134552002575" Y="24.520019437241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747487733441" Y="25.581655264874" />
                  <Point X="-4.77889996484" Y="25.636062845637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.256263904881" Y="21.076729020784" />
                  <Point X="-2.645041560861" Y="21.750111673788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.03258915102" Y="22.421363790294" />
                  <Point X="-3.439358731751" Y="23.125909371094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840827445446" Y="23.821273580862" />
                  <Point X="-4.229552020892" Y="24.494564295686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335422800347" Y="21.023836249601" />
                  <Point X="-2.699889819314" Y="21.655111644134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.114861584079" Y="22.373863824415" />
                  <Point X="-3.515377682447" Y="23.067578056037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.942775094891" Y="23.807852089413" />
                  <Point X="-4.324552039209" Y="24.46910915413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.408577766665" Y="20.96054436809" />
                  <Point X="-2.754738077767" Y="21.560111614481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197134017138" Y="22.326363858535" />
                  <Point X="-3.591396633142" Y="23.00924674098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.044722665838" Y="23.794430462003" />
                  <Point X="-4.419551981724" Y="24.443653881283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.471271273196" Y="20.879132706706" />
                  <Point X="-2.809586307638" Y="21.465111535324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.279406380772" Y="22.278863772408" />
                  <Point X="-3.667415583838" Y="22.950915425922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.146670236786" Y="23.781008834592" />
                  <Point X="-4.514551789985" Y="24.4181983759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586844480737" Y="20.88931137416" />
                  <Point X="-2.8644345103" Y="21.370111409038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361678726528" Y="22.231363655316" />
                  <Point X="-3.743434534533" Y="22.892584110865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.248617807734" Y="23.767587207182" />
                  <Point X="-4.609551598246" Y="24.392742870518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75913434549" Y="20.997726173542" />
                  <Point X="-2.919282712962" Y="21.275111282753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443951072284" Y="22.183863538223" />
                  <Point X="-3.81945348471" Y="22.834252794909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.350565378681" Y="23.754165579771" />
                  <Point X="-4.704551406508" Y="24.367287365136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.956623291508" Y="21.149787061979" />
                  <Point X="-2.974130915624" Y="21.180111156467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526223418041" Y="22.136363421131" />
                  <Point X="-3.895472408658" Y="22.775921433523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.452512949629" Y="23.740743952361" />
                  <Point X="-4.781471463462" Y="24.310516811902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.608495763797" Y="22.088863304038" />
                  <Point X="-3.971491332605" Y="22.717590072137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.554460520576" Y="23.72732232495" />
                  <Point X="-4.745348907046" Y="24.05795070889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.690768109553" Y="22.041363186946" />
                  <Point X="-4.047510256553" Y="22.659258710751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.656408091524" Y="23.71390069754" />
                  <Point X="-4.664772920428" Y="23.728389006199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773040455309" Y="21.993863069854" />
                  <Point X="-4.123529180501" Y="22.600927349365" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.708303161621" Y="20.579271484375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318969727" Y="21.478455078125" />
                  <Point X="0.367547454834" Y="21.6178828125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.274336364746" Y="21.733599609375" />
                  <Point X="0.124588058472" Y="21.78007421875" />
                  <Point X="0.020977233887" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.158412597656" Y="21.7657578125" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.385050537109" Y="21.574927734375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.623044250488" Y="20.851513671875" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.979142578125" Y="20.038427734375" />
                  <Point X="-1.10023059082" Y="20.061931640625" />
                  <Point X="-1.273559204102" Y="20.10652734375" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.337651123047" Y="20.2324765625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.345458129883" Y="20.64509375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.524151855469" Y="20.918279296875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240356445" Y="21.014236328125" />
                  <Point X="-1.832223510742" Y="21.026228515625" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878662109" Y="21.026208984375" />
                  <Point X="-2.142350097656" Y="20.92433203125" />
                  <Point X="-2.247845214844" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.351704833984" Y="20.722845703125" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.678323242188" Y="20.722478515625" />
                  <Point X="-2.855839599609" Y="20.832392578125" />
                  <Point X="-3.095822021484" Y="21.017171875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.946032958984" Y="21.608779296875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513981933594" Y="22.43123828125" />
                  <Point X="-2.531330566406" Y="22.4485859375" />
                  <Point X="-2.560159179688" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.118981933594" Y="22.15209375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.021460449219" Y="21.968619140625" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.333755859375" Y="22.441375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.933642822266" Y="22.986123046875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822265625" Y="23.603984375" />
                  <Point X="-3.138117675781" Y="23.633732421875" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.161162109375" Y="23.689361328125" />
                  <Point X="-3.187644775391" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.885193359375" Y="23.62379296875" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.87254296875" Y="23.774072265625" />
                  <Point X="-4.927393066406" Y="23.988806640625" />
                  <Point X="-4.9729140625" Y="24.307083984375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.434445800781" Y="24.6363671875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.525493164062" Y="24.889958984375" />
                  <Point X="-3.502324707031" Y="24.909353515625" />
                  <Point X="-3.489431152344" Y="24.941708984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.491115722656" Y="25.001158203125" />
                  <Point X="-3.502324707031" Y="25.0280859375" />
                  <Point X="-3.530546875" Y="25.05098828125" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.164248535156" Y="25.228671875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.952995117188" Y="25.75752734375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.826013183594" Y="26.33456640625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.381908203125" Y="26.4767421875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731705322266" Y="26.3958671875" />
                  <Point X="-3.695399414062" Y="26.407314453125" />
                  <Point X="-3.670279296875" Y="26.415234375" />
                  <Point X="-3.651534912109" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.624552246094" Y="26.478955078125" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316650391" Y="26.545513671875" />
                  <Point X="-3.633894287109" Y="26.57928125" />
                  <Point X="-3.646056396484" Y="26.602642578125" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.008021240234" Y="26.88629296875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.29737109375" Y="27.551681640625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.917284423828" Y="28.099" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.545251464844" Y="28.14985546875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514404297" Y="27.92043359375" />
                  <Point X="-3.087950195312" Y="27.916009765625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.977362060547" Y="27.963294921875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942498046875" Y="28.078404296875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.106300292969" Y="28.401171875" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-2.992107177734" Y="28.990916015625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.370583984375" Y="29.386724609375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.086021972656" Y="29.441607421875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247802734" Y="29.273662109375" />
                  <Point X="-1.894970092773" Y="29.244365234375" />
                  <Point X="-1.856031738281" Y="29.22409375" />
                  <Point X="-1.835125" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.755192260742" Y="29.24653125" />
                  <Point X="-1.714635375977" Y="29.263330078125" />
                  <Point X="-1.696905395508" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.667004516602" Y="29.354998046875" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.669452148438" Y="29.55161328125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.277793823242" Y="29.816466796875" />
                  <Point X="-0.968083312988" Y="29.903296875" />
                  <Point X="-0.504640258789" Y="29.957537109375" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.156357818604" Y="29.73716796875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282119751" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.133479400635" Y="29.6058359375" />
                  <Point X="0.236648422241" Y="29.990869140625" />
                  <Point X="0.589791503906" Y="29.953884765625" />
                  <Point X="0.860210021973" Y="29.925564453125" />
                  <Point X="1.243633300781" Y="29.832994140625" />
                  <Point X="1.508455322266" Y="29.769056640625" />
                  <Point X="1.758397094727" Y="29.67840234375" />
                  <Point X="1.931045410156" Y="29.61578125" />
                  <Point X="2.172360107422" Y="29.50292578125" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.571830566406" Y="29.289310546875" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.952396972656" Y="29.039328125" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.739119628906" Y="28.385671875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.212162353516" Y="27.444060546875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.206992675781" Y="27.351291015625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682861328" Y="27.300810546875" />
                  <Point X="2.244072998047" Y="27.263390625" />
                  <Point X="2.261640625" Y="27.237501953125" />
                  <Point X="2.274938964844" Y="27.224205078125" />
                  <Point X="2.312357421875" Y="27.198814453125" />
                  <Point X="2.338247558594" Y="27.18124609375" />
                  <Point X="2.360336425781" Y="27.172978515625" />
                  <Point X="2.401370117188" Y="27.16803125" />
                  <Point X="2.429761230469" Y="27.164607421875" />
                  <Point X="2.448666015625" Y="27.1659453125" />
                  <Point X="2.496119384766" Y="27.178634765625" />
                  <Point X="2.528952392578" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.151342285156" Y="27.544779296875" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.107273925781" Y="27.874349609375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.32516796875" Y="27.5393203125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.959112792969" Y="27.107572265625" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.245219238281" Y="26.53927734375" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.200397949219" Y="26.446009765625" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.20122265625" Y="26.3403515625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.244051269531" Y="26.244779296875" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280946289062" Y="26.1988203125" />
                  <Point X="3.322108154297" Y="26.175650390625" />
                  <Point X="3.350588378906" Y="26.1596171875" />
                  <Point X="3.368567138672" Y="26.153619140625" />
                  <Point X="3.424220947266" Y="26.146263671875" />
                  <Point X="3.462727783203" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.055555664062" Y="26.217498046875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.89825" Y="26.119546875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.977815429688" Y="25.7032890625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.509571289062" Y="25.443720703125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.674408935547" Y="25.20121484375" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.589458007812" Y="25.125123046875" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.546049316406" Y="25.0176328125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.549418945312" Y="24.902212890625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.599565917969" Y="24.799435546875" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636577148438" Y="24.758091796875" />
                  <Point X="3.691255371094" Y="24.726486328125" />
                  <Point X="3.729087158203" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.272826660156" Y="24.557154296875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.971290039062" Y="24.18521484375" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.898943847656" Y="23.81673828125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.303114257812" Y="23.78505078125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.287522216797" Y="23.878333984375" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.120580810547" Y="23.767291015625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.055061279297" Y="23.584900390625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.115749511719" Y="23.391001953125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.661843505859" Y="22.935873046875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.26856640625" Y="22.302123046875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.101787597656" Y="22.05244140625" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.547127685547" Y="22.282556640625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.609620117188" Y="22.769751953125" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.382973388672" Y="22.724912109375" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.2327578125" Y="22.559212890625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.212229248047" Y="22.325904296875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.551139648438" Y="21.67227734375" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.911756835938" Y="20.864400390625" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.720871337891" Y="20.735720703125" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.288823486328" Y="21.218619140625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.544582275391" Y="22.100517578125" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.288038574219" Y="22.15160546875" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.058952636719" Y="22.0430390625" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.936649841309" Y="21.80767578125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.003778442383" Y="21.006810546875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.066680175781" Y="20.052607421875" />
                  <Point X="0.994364868164" Y="20.036755859375" />
                  <Point X="0.88863873291" Y="20.017548828125" />
                  <Point X="0.860200683594" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#176" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.114217428061" Y="4.781389739961" Z="1.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="-0.51660836997" Y="5.038478021476" Z="1.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.5" />
                  <Point X="-1.297447429651" Y="4.895891124694" Z="1.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.5" />
                  <Point X="-1.725180423146" Y="4.576368674178" Z="1.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.5" />
                  <Point X="-1.720846455202" Y="4.401313829153" Z="1.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.5" />
                  <Point X="-1.780482195911" Y="4.324004678734" Z="1.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.5" />
                  <Point X="-1.878037582872" Y="4.319994840552" Z="1.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.5" />
                  <Point X="-2.052510392617" Y="4.503326363812" Z="1.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.5" />
                  <Point X="-2.401023016381" Y="4.461712159474" Z="1.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.5" />
                  <Point X="-3.028685220761" Y="4.061875362072" Z="1.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.5" />
                  <Point X="-3.155757512017" Y="3.407451984082" Z="1.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.5" />
                  <Point X="-2.998463838102" Y="3.105327603337" Z="1.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.5" />
                  <Point X="-3.018872833079" Y="3.029930734808" Z="1.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.5" />
                  <Point X="-3.08974886227" Y="2.997100861071" Z="1.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.5" />
                  <Point X="-3.526407364215" Y="3.224436436729" Z="1.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.5" />
                  <Point X="-3.962904007647" Y="3.160983959707" Z="1.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.5" />
                  <Point X="-4.346709339722" Y="2.608166395162" Z="1.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.5" />
                  <Point X="-4.044615294269" Y="1.877904555007" Z="1.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.5" />
                  <Point X="-3.684400211176" Y="1.587470982221" Z="1.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.5" />
                  <Point X="-3.676901988752" Y="1.529370180355" Z="1.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.5" />
                  <Point X="-3.716590063909" Y="1.486279763422" Z="1.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.5" />
                  <Point X="-4.381538012644" Y="1.557594841571" Z="1.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.5" />
                  <Point X="-4.880428857307" Y="1.378925927194" Z="1.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.5" />
                  <Point X="-5.008612272262" Y="0.796126579437" Z="1.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.5" />
                  <Point X="-4.18334520555" Y="0.211656443249" Z="1.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.5" />
                  <Point X="-3.565211229972" Y="0.041191826964" Z="1.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.5" />
                  <Point X="-3.545024625843" Y="0.017617433894" Z="1.5" />
                  <Point X="-3.539556741714" Y="0" Z="1.5" />
                  <Point X="-3.543339975292" Y="-0.012189517167" Z="1.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.5" />
                  <Point X="-3.560157365151" Y="-0.037684156169" Z="1.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.5" />
                  <Point X="-4.453542583222" Y="-0.284055613507" Z="1.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.5" />
                  <Point X="-5.028565909203" Y="-0.668713592562" Z="1.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.5" />
                  <Point X="-4.927143487974" Y="-1.207022637533" Z="1.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.5" />
                  <Point X="-3.884823038104" Y="-1.394499639076" Z="1.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.5" />
                  <Point X="-3.20832886743" Y="-1.313237386699" Z="1.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.5" />
                  <Point X="-3.195827050399" Y="-1.33461495365" Z="1.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.5" />
                  <Point X="-3.970236678889" Y="-1.942928336394" Z="1.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.5" />
                  <Point X="-4.382855426339" Y="-2.552953154283" Z="1.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.5" />
                  <Point X="-4.067402204988" Y="-3.030383677378" Z="1.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.5" />
                  <Point X="-3.100137842852" Y="-2.859926807427" Z="1.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.5" />
                  <Point X="-2.565744838676" Y="-2.562585630455" Z="1.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.5" />
                  <Point X="-2.995490205741" Y="-3.33494053964" Z="1.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.5" />
                  <Point X="-3.132481735136" Y="-3.991165423629" Z="1.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.5" />
                  <Point X="-2.710800679417" Y="-4.288752349716" Z="1.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.5" />
                  <Point X="-2.318193014508" Y="-4.276310736146" Z="1.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.5" />
                  <Point X="-2.120727273461" Y="-4.085962567676" Z="1.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.5" />
                  <Point X="-1.841649883209" Y="-3.992382579868" Z="1.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.5" />
                  <Point X="-1.563275093932" Y="-4.088032372641" Z="1.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.5" />
                  <Point X="-1.400653353005" Y="-4.333380244887" Z="1.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.5" />
                  <Point X="-1.393379328191" Y="-4.729717152762" Z="1.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.5" />
                  <Point X="-1.292174027456" Y="-4.910616207379" Z="1.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.5" />
                  <Point X="-0.994826074654" Y="-4.979375959997" Z="1.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="-0.580904026132" Y="-4.130147408069" Z="1.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="-0.350130531697" Y="-3.422301890207" Z="1.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="-0.149748185347" Y="-3.250715678946" Z="1.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.5" />
                  <Point X="0.103610894014" Y="-3.236396366342" Z="1.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.5" />
                  <Point X="0.320315327735" Y="-3.379343865561" Z="1.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.5" />
                  <Point X="0.653850834269" Y="-4.402388621747" Z="1.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.5" />
                  <Point X="0.891418758611" Y="-5.000365144281" Z="1.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.5" />
                  <Point X="1.071229505152" Y="-4.964951738448" Z="1.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.5" />
                  <Point X="1.047194778021" Y="-3.955384480999" Z="1.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.5" />
                  <Point X="0.979353049992" Y="-3.171663006934" Z="1.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.5" />
                  <Point X="1.084764150347" Y="-2.964126589518" Z="1.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.5" />
                  <Point X="1.286464281873" Y="-2.866903957892" Z="1.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.5" />
                  <Point X="1.511387033833" Y="-2.910260215575" Z="1.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.5" />
                  <Point X="2.242999780416" Y="-3.780538190829" Z="1.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.5" />
                  <Point X="2.741884482515" Y="-4.274973142554" Z="1.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.5" />
                  <Point X="2.934662652776" Y="-4.145006790553" Z="1.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.5" />
                  <Point X="2.588284974437" Y="-3.271441771714" Z="1.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.5" />
                  <Point X="2.255277348776" Y="-2.633928693528" Z="1.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.5" />
                  <Point X="2.270848221957" Y="-2.432794515286" Z="1.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.5" />
                  <Point X="2.400103863086" Y="-2.28805308801" Z="1.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.5" />
                  <Point X="2.594578109985" Y="-2.248170660715" Z="1.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.5" />
                  <Point X="3.515971825393" Y="-2.729464525838" Z="1.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.5" />
                  <Point X="4.136520413093" Y="-2.945055311705" Z="1.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.5" />
                  <Point X="4.304943981954" Y="-2.692881127803" Z="1.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.5" />
                  <Point X="3.686125545551" Y="-1.993178956942" Z="1.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.5" />
                  <Point X="3.151651518049" Y="-1.550678202286" Z="1.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.5" />
                  <Point X="3.098694842075" Y="-1.38840076883" Z="1.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.5" />
                  <Point X="3.152871164621" Y="-1.23339587609" Z="1.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.5" />
                  <Point X="3.291986046074" Y="-1.139245525865" Z="1.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.5" />
                  <Point X="4.290431805889" Y="-1.233240161961" Z="1.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.5" />
                  <Point X="4.941534962652" Y="-1.163106404936" Z="1.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.5" />
                  <Point X="5.014575374751" Y="-0.790960043491" Z="1.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.5" />
                  <Point X="4.279611678459" Y="-0.363268224354" Z="1.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.5" />
                  <Point X="3.710120851392" Y="-0.198943150459" Z="1.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.5" />
                  <Point X="3.63274360573" Y="-0.138414208365" Z="1.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.5" />
                  <Point X="3.592370354055" Y="-0.057101769452" Z="1.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.5" />
                  <Point X="3.589001096369" Y="0.039508761752" Z="1.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.5" />
                  <Point X="3.622635832671" Y="0.12553453021" Z="1.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.5" />
                  <Point X="3.69327456296" Y="0.189205693783" Z="1.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.5" />
                  <Point X="4.516356138848" Y="0.426703708415" Z="1.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.5" />
                  <Point X="5.021064477831" Y="0.742260855977" Z="1.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.5" />
                  <Point X="4.940675156983" Y="1.162654306982" Z="1.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.5" />
                  <Point X="4.042873440884" Y="1.298349908928" Z="1.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.5" />
                  <Point X="3.424614469031" Y="1.227113245031" Z="1.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.5" />
                  <Point X="3.340480578563" Y="1.250500375268" Z="1.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.5" />
                  <Point X="3.279665471616" Y="1.30354284935" Z="1.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.5" />
                  <Point X="3.244035294685" Y="1.381735836052" Z="1.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.5" />
                  <Point X="3.242394228142" Y="1.463823744711" Z="1.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.5" />
                  <Point X="3.278745936824" Y="1.540140835676" Z="1.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.5" />
                  <Point X="3.983394737667" Y="2.099185203215" Z="1.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.5" />
                  <Point X="4.361789271135" Y="2.596488341688" Z="1.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.5" />
                  <Point X="4.141703621415" Y="2.934833199327" Z="1.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.5" />
                  <Point X="3.120186270931" Y="2.619360438365" Z="1.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.5" />
                  <Point X="2.477044947855" Y="2.258218803324" Z="1.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.5" />
                  <Point X="2.401200438728" Y="2.248952629713" Z="1.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.5" />
                  <Point X="2.334276764102" Y="2.271467964588" Z="1.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.5" />
                  <Point X="2.279290635647" Y="2.32274809628" Z="1.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.5" />
                  <Point X="2.250477073045" Y="2.388558005105" Z="1.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.5" />
                  <Point X="2.254309139334" Y="2.462424628027" Z="1.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.5" />
                  <Point X="2.776264914494" Y="3.391952811024" Z="1.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.5" />
                  <Point X="2.975218130709" Y="4.111356779246" Z="1.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.5" />
                  <Point X="2.590844535407" Y="4.36379419278" Z="1.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.5" />
                  <Point X="2.187385620676" Y="4.579497740662" Z="1.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.5" />
                  <Point X="1.769290005479" Y="4.756686382357" Z="1.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.5" />
                  <Point X="1.24921189048" Y="4.912877961519" Z="1.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.5" />
                  <Point X="0.588843457614" Y="5.034893262422" Z="1.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.5" />
                  <Point X="0.079027133492" Y="4.65005776599" Z="1.5" />
                  <Point X="0" Y="4.355124473572" Z="1.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>