<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#129" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="672" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.00471484375" />
                  <Width Value="9.783896972656" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.610815063477" Y="21.310154296875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542361694336" Y="21.532626953125" />
                  <Point X="0.522941894531" Y="21.56060546875" />
                  <Point X="0.378634155273" Y="21.76852734375" />
                  <Point X="0.356752044678" Y="21.79098046875" />
                  <Point X="0.330496551514" Y="21.810224609375" />
                  <Point X="0.302493804932" Y="21.82433203125" />
                  <Point X="0.272442962646" Y="21.833658203125" />
                  <Point X="0.049134822845" Y="21.90296484375" />
                  <Point X="0.020976791382" Y="21.907234375" />
                  <Point X="-0.00866468811" Y="21.907234375" />
                  <Point X="-0.036822719574" Y="21.90296484375" />
                  <Point X="-0.066873733521" Y="21.893638671875" />
                  <Point X="-0.290181854248" Y="21.82433203125" />
                  <Point X="-0.318185058594" Y="21.81022265625" />
                  <Point X="-0.344440246582" Y="21.790978515625" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.385743499756" Y="21.74054296875" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.904058227539" Y="20.169806640625" />
                  <Point X="-0.916584472656" Y="20.12305859375" />
                  <Point X="-1.079328369141" Y="20.1546484375" />
                  <Point X="-1.110188354492" Y="20.162587890625" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.220682617188" Y="20.393115234375" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.223687866211" Y="20.5198671875" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.32364453125" Y="20.868794921875" />
                  <Point X="-1.351311523438" Y="20.89305859375" />
                  <Point X="-1.556905273438" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643028808594" Y="21.109033203125" />
                  <Point X="-1.679749267578" Y="21.111439453125" />
                  <Point X="-1.952617919922" Y="21.12932421875" />
                  <Point X="-1.983414550781" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042655273438" Y="21.105201171875" />
                  <Point X="-2.073252929688" Y="21.0847578125" />
                  <Point X="-2.300621826172" Y="20.932833984375" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480148193359" Y="20.711509765625" />
                  <Point X="-2.801707275391" Y="20.910609375" />
                  <Point X="-2.844421875" Y="20.9435" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-2.512058837891" Y="22.170443359375" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412857666016" Y="22.352353515625" />
                  <Point X="-2.406587646484" Y="22.38388671875" />
                  <Point X="-2.405751953125" Y="22.41608203125" />
                  <Point X="-2.415720458984" Y="22.446705078125" />
                  <Point X="-2.431993896484" Y="22.477583984375" />
                  <Point X="-2.448861328125" Y="22.500466796875" />
                  <Point X="-2.464152099609" Y="22.5157578125" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518140380859" Y="22.54800390625" />
                  <Point X="-2.547758300781" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.61021875" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-3.775647216797" Y="21.8826640625" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.082864257812" Y="22.20614453125" />
                  <Point X="-4.113480957031" Y="22.257484375" />
                  <Point X="-4.306143066406" Y="22.580548828125" />
                  <Point X="-3.260446777344" Y="23.382939453125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.064640625" Y="23.5528671875" />
                  <Point X="-3.057013183594" Y="23.57146484375" />
                  <Point X="-3.052942871094" Y="23.583693359375" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.043347167969" Y="23.6403359375" />
                  <Point X="-3.045553955078" Y="23.672001953125" />
                  <Point X="-3.053162597656" Y="23.703185546875" />
                  <Point X="-3.070721191406" Y="23.7300546875" />
                  <Point X="-3.09469921875" Y="23.7558671875" />
                  <Point X="-3.116119628906" Y="23.7730859375" />
                  <Point X="-3.139461181641" Y="23.786822265625" />
                  <Point X="-3.168722412109" Y="23.79804296875" />
                  <Point X="-3.200607421875" Y="23.8045234375" />
                  <Point X="-3.231928710938" Y="23.805615234375" />
                  <Point X="-4.666605957031" Y="23.616736328125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.842178710938" Y="24.063986328125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.708865722656" Y="24.73243359375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.514703857422" Y="24.786521484375" />
                  <Point X="-3.495844970703" Y="24.79602734375" />
                  <Point X="-3.484439697266" Y="24.802814453125" />
                  <Point X="-3.459978515625" Y="24.819791015625" />
                  <Point X="-3.436022949219" Y="24.84131640625" />
                  <Point X="-3.416229003906" Y="24.869197265625" />
                  <Point X="-3.407706054688" Y="24.887470703125" />
                  <Point X="-3.403071289062" Y="24.899466796875" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390699951172" Y="24.9570390625" />
                  <Point X="-3.391797119141" Y="24.990216796875" />
                  <Point X="-3.396014648438" Y="25.015236328125" />
                  <Point X="-3.404168457031" Y="25.0415078125" />
                  <Point X="-3.417491943359" Y="25.07084375" />
                  <Point X="-3.438275390625" Y="25.09825" />
                  <Point X="-3.453393554688" Y="25.112052734375" />
                  <Point X="-3.463278076172" Y="25.1199375" />
                  <Point X="-3.487725585938" Y="25.13690625" />
                  <Point X="-3.501916015625" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.84065234375" Y="25.508267578125" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.824489257812" Y="25.97696875" />
                  <Point X="-4.808178710938" Y="26.037158203125" />
                  <Point X="-4.703550292969" Y="26.423267578125" />
                  <Point X="-3.891820068359" Y="26.316400390625" />
                  <Point X="-3.765666015625" Y="26.29979296875" />
                  <Point X="-3.744984375" Y="26.299341796875" />
                  <Point X="-3.723423339844" Y="26.301228515625" />
                  <Point X="-3.703140869141" Y="26.305263671875" />
                  <Point X="-3.695855224609" Y="26.307560546875" />
                  <Point X="-3.64171484375" Y="26.324630859375" />
                  <Point X="-3.62278515625" Y="26.332958984375" />
                  <Point X="-3.604040527344" Y="26.343779296875" />
                  <Point X="-3.587354980469" Y="26.35601171875" />
                  <Point X="-3.573713378906" Y="26.37156640625" />
                  <Point X="-3.561299072266" Y="26.389296875" />
                  <Point X="-3.551354003906" Y="26.407423828125" />
                  <Point X="-3.548430419922" Y="26.41448046875" />
                  <Point X="-3.526706542969" Y="26.466927734375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532051513672" Y="26.589380859375" />
                  <Point X="-3.535578857422" Y="26.59615625" />
                  <Point X="-3.561791259766" Y="26.646509765625" />
                  <Point X="-3.573281494141" Y="26.663705078125" />
                  <Point X="-3.587193847656" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.081153808594" Y="27.73365625" />
                  <Point X="-4.037955078125" Y="27.78918359375" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.284136962891" Y="27.889404296875" />
                  <Point X="-3.206656738281" Y="27.844671875" />
                  <Point X="-3.187727294922" Y="27.836341796875" />
                  <Point X="-3.16708984375" Y="27.829833984375" />
                  <Point X="-3.146821289063" Y="27.825798828125" />
                  <Point X="-3.136645263672" Y="27.82490625" />
                  <Point X="-3.061242919922" Y="27.818310546875" />
                  <Point X="-3.040559570312" Y="27.81876171875" />
                  <Point X="-3.0191015625" Y="27.821587890625" />
                  <Point X="-2.999012207031" Y="27.826505859375" />
                  <Point X="-2.980462890625" Y="27.83565234375" />
                  <Point X="-2.962209228516" Y="27.84728125" />
                  <Point X="-2.946074462891" Y="27.860232421875" />
                  <Point X="-2.938872070312" Y="27.867435546875" />
                  <Point X="-2.885350830078" Y="27.920955078125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435546875" Y="28.0361171875" />
                  <Point X="-2.844323242188" Y="28.046265625" />
                  <Point X="-2.850920166016" Y="28.12166796875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.183333251953" Y="28.724595703125" />
                  <Point X="-2.700623291016" Y="29.094685546875" />
                  <Point X="-2.632593017578" Y="29.13248046875" />
                  <Point X="-2.167036865234" Y="29.3911328125" />
                  <Point X="-2.066913818359" Y="29.260650390625" />
                  <Point X="-2.043194824219" Y="29.229740234375" />
                  <Point X="-2.028893676758" Y="29.21480078125" />
                  <Point X="-2.012319702148" Y="29.200892578125" />
                  <Point X="-1.995123291016" Y="29.189400390625" />
                  <Point X="-1.983823974609" Y="29.183517578125" />
                  <Point X="-1.899901489258" Y="29.139830078125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718383789" Y="29.126728515625" />
                  <Point X="-1.839268432617" Y="29.123580078125" />
                  <Point X="-1.818621826172" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777449951172" Y="29.134482421875" />
                  <Point X="-1.765686889648" Y="29.13935546875" />
                  <Point X="-1.678276000977" Y="29.1755625" />
                  <Point X="-1.660146118164" Y="29.185509765625" />
                  <Point X="-1.642416381836" Y="29.197923828125" />
                  <Point X="-1.626864501953" Y="29.2115625" />
                  <Point X="-1.614633056641" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265919921875" />
                  <Point X="-1.591651855469" Y="29.2780625" />
                  <Point X="-1.563201171875" Y="29.368296875" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584202026367" Y="29.6318984375" />
                  <Point X="-0.949624633789" Y="29.80980859375" />
                  <Point X="-0.867164550781" Y="29.8194609375" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.156824111938" Y="29.37185546875" />
                  <Point X="-0.133903167725" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082114044189" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155899525" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.09442590332" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441650391" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.307418823242" Y="29.887935546875" />
                  <Point X="0.844044921875" Y="29.831736328125" />
                  <Point X="0.91227746582" Y="29.815263671875" />
                  <Point X="1.481037963867" Y="29.6779453125" />
                  <Point X="1.524133789062" Y="29.662314453125" />
                  <Point X="1.894652099609" Y="29.52792578125" />
                  <Point X="1.93759753418" Y="29.507841796875" />
                  <Point X="2.294561035156" Y="29.340900390625" />
                  <Point X="2.336103027344" Y="29.31669921875" />
                  <Point X="2.680977783203" Y="29.115775390625" />
                  <Point X="2.72011328125" Y="29.087943359375" />
                  <Point X="2.943260009766" Y="28.92925390625" />
                  <Point X="2.249778808594" Y="27.728109375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.140168212891" Y="27.534927734375" />
                  <Point X="2.132620117188" Y="27.5133203125" />
                  <Point X="2.130530273438" Y="27.50653125" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108389892578" Y="27.41243359375" />
                  <Point X="2.108045166016" Y="27.385296875" />
                  <Point X="2.108720703125" Y="27.372716796875" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140068847656" Y="27.247474609375" />
                  <Point X="2.1451640625" Y="27.23996484375" />
                  <Point X="2.183026611328" Y="27.184166015625" />
                  <Point X="2.198716552734" Y="27.166330078125" />
                  <Point X="2.219524902344" Y="27.14793359375" />
                  <Point X="2.229106445312" Y="27.14049609375" />
                  <Point X="2.284906005859" Y="27.1026328125" />
                  <Point X="2.304953613281" Y="27.09226953125" />
                  <Point X="2.327042236328" Y="27.08400390625" />
                  <Point X="2.348972412109" Y="27.07866015625" />
                  <Point X="2.35720703125" Y="27.07766796875" />
                  <Point X="2.418397216797" Y="27.0702890625" />
                  <Point X="2.442624755859" Y="27.070482421875" />
                  <Point X="2.471051025391" Y="27.0743671875" />
                  <Point X="2.48273046875" Y="27.076716796875" />
                  <Point X="2.553493896484" Y="27.095640625" />
                  <Point X="2.565288085938" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.903905273438" Y="27.869576171875" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="4.123271484375" Y="27.689462890625" />
                  <Point X="4.14508984375" Y="27.65340625" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.365336914062" Y="26.771697265625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.21747265625" Y="26.6560390625" />
                  <Point X="3.201374511719" Y="26.63784765625" />
                  <Point X="3.197120849609" Y="26.632685546875" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.134083984375" Y="26.545435546875" />
                  <Point X="3.123061279297" Y="26.51935546875" />
                  <Point X="3.119076904297" Y="26.50795703125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739746094" Y="26.371765625" />
                  <Point X="3.099835449219" Y="26.361609375" />
                  <Point X="3.115408691406" Y="26.2861328125" />
                  <Point X="3.123317626953" Y="26.26316796875" />
                  <Point X="3.136216064453" Y="26.237125" />
                  <Point X="3.141983154297" Y="26.22707421875" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198896972656" Y="26.1454453125" />
                  <Point X="3.216140380859" Y="26.129357421875" />
                  <Point X="3.234342529297" Y="26.116037109375" />
                  <Point X="3.242602783203" Y="26.11138671875" />
                  <Point X="3.303984619141" Y="26.076833984375" />
                  <Point X="3.326999267578" Y="26.06759375" />
                  <Point X="3.356149414062" Y="26.06012109375" />
                  <Point X="3.367291503906" Y="26.057962890625" />
                  <Point X="3.450277587891" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203369141" Y="26.046984375" />
                  <Point X="4.737718261719" Y="26.211486328125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.852811523438" Y="25.8886484375" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="3.870780273438" Y="25.370908203125" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.699205810547" Y="25.323056640625" />
                  <Point X="3.676153076172" Y="25.31170703125" />
                  <Point X="3.670574462891" Y="25.3087265625" />
                  <Point X="3.589037109375" Y="25.26159765625" />
                  <Point X="3.569538330078" Y="25.246658203125" />
                  <Point X="3.548639404297" Y="25.22584375" />
                  <Point X="3.540944824219" Y="25.21718359375" />
                  <Point X="3.492025634766" Y="25.154849609375" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.461486328125" Y="25.08114453125" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443731689453" Y="24.971251953125" />
                  <Point X="3.445926513672" Y="24.940984375" />
                  <Point X="3.447373535156" Y="24.929986328125" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492025634766" Y="24.782591796875" />
                  <Point X="3.498609130859" Y="24.774203125" />
                  <Point X="3.547531494141" Y="24.71186328125" />
                  <Point X="3.565651367188" Y="24.694224609375" />
                  <Point X="3.590936035156" Y="24.6754609375" />
                  <Point X="3.600007568359" Y="24.669501953125" />
                  <Point X="3.681544921875" Y="24.62237109375" />
                  <Point X="3.692708251953" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.86244140625" Y="24.30081640625" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.846213867188" Y="24.012671875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.603211425781" Y="23.973015625" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374663085938" Y="23.9944921875" />
                  <Point X="3.353127685547" Y="23.9898125" />
                  <Point X="3.193098876953" Y="23.955029296875" />
                  <Point X="3.163972900391" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112395751953" Y="23.906037109375" />
                  <Point X="3.09937890625" Y="23.890380859375" />
                  <Point X="3.002651611328" Y="23.774048828125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.967891845703" Y="23.674359375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976451171875" Y="23.432001953125" />
                  <Point X="2.988369140625" Y="23.41346484375" />
                  <Point X="3.076931396484" Y="23.275712890625" />
                  <Point X="3.086931640625" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.173994628906" Y="22.423138671875" />
                  <Point X="4.213122558594" Y="22.393115234375" />
                  <Point X="4.124819335938" Y="22.2502265625" />
                  <Point X="4.106591308594" Y="22.224328125" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="2.960226318359" Y="22.731099609375" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224853516" Y="22.840173828125" />
                  <Point X="2.728594238281" Y="22.844802734375" />
                  <Point X="2.538134521484" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444836669922" Y="22.86482421875" />
                  <Point X="2.423543945312" Y="22.853619140625" />
                  <Point X="2.265318603516" Y="22.770345703125" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.193325439453" Y="22.688267578125" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.100303955078" Y="22.411111328125" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.835139160156" Y="20.990376953125" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781837158203" Y="20.88834765625" />
                  <Point X="2.761482666016" Y="20.875171875" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="1.880085205078" Y="21.9073515625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721924072266" Y="22.099443359375" />
                  <Point X="1.696645507812" Y="22.1156953125" />
                  <Point X="1.508800537109" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417103393555" Y="22.2588828125" />
                  <Point X="1.38945690918" Y="22.25633984375" />
                  <Point X="1.184016601562" Y="22.237435546875" />
                  <Point X="1.156362548828" Y="22.2306015625" />
                  <Point X="1.128976928711" Y="22.2192578125" />
                  <Point X="1.104595825195" Y="22.2045390625" />
                  <Point X="1.083247802734" Y="22.1867890625" />
                  <Point X="0.924612121582" Y="22.05488671875" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.869241577148" Y="21.94482421875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="1.013389953613" Y="20.20598046875" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975690063477" Y="20.129919921875" />
                  <Point X="0.956852539062" Y="20.12649609375" />
                  <Point X="0.929316040039" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058430297852" Y="20.247365234375" />
                  <Point X="-1.086518188477" Y="20.254591796875" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.126495361328" Y="20.38071484375" />
                  <Point X="-1.120775756836" Y="20.424158203125" />
                  <Point X="-1.120077392578" Y="20.4318984375" />
                  <Point X="-1.119451660156" Y="20.458962890625" />
                  <Point X="-1.121759033203" Y="20.49066796875" />
                  <Point X="-1.123333862305" Y="20.502306640625" />
                  <Point X="-1.130513183594" Y="20.538400390625" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026611328" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575195312" Y="20.905140625" />
                  <Point X="-1.250209716797" Y="20.929064453125" />
                  <Point X="-1.261006103516" Y="20.94021875" />
                  <Point X="-1.288673217773" Y="20.964482421875" />
                  <Point X="-1.494266845703" Y="21.144783203125" />
                  <Point X="-1.506735717773" Y="21.15403125" />
                  <Point X="-1.53301953125" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531982422" Y="21.189775390625" />
                  <Point X="-1.591316650391" Y="21.194525390625" />
                  <Point X="-1.621458984375" Y="21.201552734375" />
                  <Point X="-1.636816894531" Y="21.203830078125" />
                  <Point X="-1.673537353516" Y="21.206236328125" />
                  <Point X="-1.946405883789" Y="21.22412109375" />
                  <Point X="-1.96192956543" Y="21.2238671875" />
                  <Point X="-1.992726196289" Y="21.220833984375" />
                  <Point X="-2.007999267578" Y="21.2180546875" />
                  <Point X="-2.039047973633" Y="21.209736328125" />
                  <Point X="-2.053665039062" Y="21.2045078125" />
                  <Point X="-2.081856933594" Y="21.191736328125" />
                  <Point X="-2.095432128906" Y="21.184193359375" />
                  <Point X="-2.126029785156" Y="21.16375" />
                  <Point X="-2.353398681641" Y="21.011826171875" />
                  <Point X="-2.359681884766" Y="21.007244140625" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201660156" Y="20.83751953125" />
                  <Point X="-2.747583007812" Y="20.988833984375" />
                  <Point X="-2.786462646484" Y="21.018771484375" />
                  <Point X="-2.980862304688" Y="21.168453125" />
                  <Point X="-2.429786376953" Y="22.122943359375" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849121094" Y="22.289919921875" />
                  <Point X="-2.323945556641" Y="22.318892578125" />
                  <Point X="-2.319681640625" Y="22.333826171875" />
                  <Point X="-2.313411621094" Y="22.365359375" />
                  <Point X="-2.311619628906" Y="22.381421875" />
                  <Point X="-2.310783935547" Y="22.4136171875" />
                  <Point X="-2.315417724609" Y="22.44548828125" />
                  <Point X="-2.325386230469" Y="22.476111328125" />
                  <Point X="-2.331677246094" Y="22.49099609375" />
                  <Point X="-2.347950683594" Y="22.521875" />
                  <Point X="-2.355523925781" Y="22.533951171875" />
                  <Point X="-2.372391357422" Y="22.556833984375" />
                  <Point X="-2.381685546875" Y="22.567640625" />
                  <Point X="-2.396976318359" Y="22.582931640625" />
                  <Point X="-2.408815429688" Y="22.5929765625" />
                  <Point X="-2.433974365234" Y="22.611005859375" />
                  <Point X="-2.447294189453" Y="22.618990234375" />
                  <Point X="-2.476123535156" Y="22.63320703125" />
                  <Point X="-2.490563720703" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869628906" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753662109" Y="22.642876953125" />
                  <Point X="-2.643684326172" Y="22.63861328125" />
                  <Point X="-2.672649414062" Y="22.6277109375" />
                  <Point X="-2.686683837891" Y="22.621072265625" />
                  <Point X="-3.793087646484" Y="21.982291015625" />
                  <Point X="-4.004022949219" Y="22.259416015625" />
                  <Point X="-4.031888183594" Y="22.306142578125" />
                  <Point X="-4.181265625" Y="22.556625" />
                  <Point X="-3.202614501953" Y="23.3075703125" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.003079833984" Y="23.472859375" />
                  <Point X="-2.984655517578" Y="23.501609375" />
                  <Point X="-2.976745605469" Y="23.516818359375" />
                  <Point X="-2.969118164062" Y="23.535416015625" />
                  <Point X="-2.960977539062" Y="23.559873046875" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951553222656" Y="23.60119140625" />
                  <Point X="-2.948748291016" Y="23.631615234375" />
                  <Point X="-2.948576904297" Y="23.646939453125" />
                  <Point X="-2.950783691406" Y="23.67860546875" />
                  <Point X="-2.953261474609" Y="23.694521484375" />
                  <Point X="-2.960870117188" Y="23.725705078125" />
                  <Point X="-2.973637451172" Y="23.755154296875" />
                  <Point X="-2.991196044922" Y="23.7820234375" />
                  <Point X="-3.001118164062" Y="23.7947109375" />
                  <Point X="-3.025096191406" Y="23.8205234375" />
                  <Point X="-3.0351796875" Y="23.82991015625" />
                  <Point X="-3.056600097656" Y="23.84712890625" />
                  <Point X="-3.067937011719" Y="23.8549609375" />
                  <Point X="-3.091278564453" Y="23.868697265625" />
                  <Point X="-3.105447021484" Y="23.8755234375" />
                  <Point X="-3.134708251953" Y="23.886744140625" />
                  <Point X="-3.149801025391" Y="23.891138671875" />
                  <Point X="-3.181686035156" Y="23.897619140625" />
                  <Point X="-3.197297851562" Y="23.89946484375" />
                  <Point X="-3.228619140625" Y="23.900556640625" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-4.660919921875" Y="23.7133046875" />
                  <Point X="-4.740762695312" Y="24.02588671875" />
                  <Point X="-4.748135742188" Y="24.0774375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.684277832031" Y="24.640669921875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.499018310547" Y="24.690828125" />
                  <Point X="-3.471943847656" Y="24.701689453125" />
                  <Point X="-3.453084960938" Y="24.7111953125" />
                  <Point X="-3.430274414062" Y="24.72476953125" />
                  <Point X="-3.405813232422" Y="24.74174609375" />
                  <Point X="-3.396483398438" Y="24.749126953125" />
                  <Point X="-3.372527832031" Y="24.77065234375" />
                  <Point X="-3.358559570312" Y="24.786322265625" />
                  <Point X="-3.338765625" Y="24.814203125" />
                  <Point X="-3.330133300781" Y="24.829041015625" />
                  <Point X="-3.321610351562" Y="24.847314453125" />
                  <Point X="-3.312340820312" Y="24.871306640625" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.300768310547" Y="24.913052734375" />
                  <Point X="-3.29655078125" Y="24.944353515625" />
                  <Point X="-3.295751953125" Y="24.9601796875" />
                  <Point X="-3.296849121094" Y="24.993357421875" />
                  <Point X="-3.298118652344" Y="25.0060078125" />
                  <Point X="-3.302336181641" Y="25.03102734375" />
                  <Point X="-3.305284179688" Y="25.043396484375" />
                  <Point X="-3.313437988281" Y="25.06966796875" />
                  <Point X="-3.317671386719" Y="25.08079296875" />
                  <Point X="-3.330994873047" Y="25.11012890625" />
                  <Point X="-3.341796386719" Y="25.128248046875" />
                  <Point X="-3.362579833984" Y="25.155654296875" />
                  <Point X="-3.374221923828" Y="25.168408203125" />
                  <Point X="-3.389340087891" Y="25.1822109375" />
                  <Point X="-3.409109130859" Y="25.19798046875" />
                  <Point X="-3.433556640625" Y="25.21494921875" />
                  <Point X="-3.440478759766" Y="25.21932421875" />
                  <Point X="-3.465598388672" Y="25.23282421875" />
                  <Point X="-3.496558349609" Y="25.2456328125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.73133203125" Y="25.9575234375" />
                  <Point X="-4.716485839844" Y="26.012310546875" />
                  <Point X="-4.633585449219" Y="26.318236328125" />
                  <Point X="-3.904220214844" Y="26.222212890625" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.767738037109" Y="26.20481640625" />
                  <Point X="-3.747056396484" Y="26.204365234375" />
                  <Point X="-3.736702880859" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704886474609" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.667291259766" Y="26.21695703125" />
                  <Point X="-3.613150878906" Y="26.23402734375" />
                  <Point X="-3.603458251953" Y="26.237673828125" />
                  <Point X="-3.584528564453" Y="26.246001953125" />
                  <Point X="-3.575291503906" Y="26.25068359375" />
                  <Point X="-3.556546875" Y="26.26150390625" />
                  <Point X="-3.547871826172" Y="26.267162109375" />
                  <Point X="-3.531186279297" Y="26.27939453125" />
                  <Point X="-3.515931396484" Y="26.293373046875" />
                  <Point X="-3.502289794922" Y="26.308927734375" />
                  <Point X="-3.495892578125" Y="26.317078125" />
                  <Point X="-3.483478271484" Y="26.33480859375" />
                  <Point X="-3.478010498047" Y="26.3436015625" />
                  <Point X="-3.468065429688" Y="26.361728515625" />
                  <Point X="-3.460664550781" Y="26.378119140625" />
                  <Point X="-3.438940673828" Y="26.43056640625" />
                  <Point X="-3.435502197266" Y="26.44034375" />
                  <Point X="-3.429711181641" Y="26.4602109375" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012939453" Y="26.604533203125" />
                  <Point X="-3.443511474609" Y="26.623814453125" />
                  <Point X="-3.451314453125" Y="26.640025390625" />
                  <Point X="-3.477526855469" Y="26.69037890625" />
                  <Point X="-3.482803222656" Y="26.699291015625" />
                  <Point X="-3.494293457031" Y="26.716486328125" />
                  <Point X="-3.500507324219" Y="26.72476953125" />
                  <Point X="-3.514419677734" Y="26.741349609375" />
                  <Point X="-3.521497802734" Y="26.748908203125" />
                  <Point X="-3.536439697266" Y="26.763212890625" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.002296875" Y="27.680302734375" />
                  <Point X="-3.962973632812" Y="27.730849609375" />
                  <Point X="-3.726336425781" Y="28.035013671875" />
                  <Point X="-3.331636962891" Y="27.8071328125" />
                  <Point X="-3.254156738281" Y="27.762400390625" />
                  <Point X="-3.244921142578" Y="27.75771875" />
                  <Point X="-3.225991699219" Y="27.749388671875" />
                  <Point X="-3.216297851562" Y="27.745740234375" />
                  <Point X="-3.195660400391" Y="27.739232421875" />
                  <Point X="-3.185638916016" Y="27.736662109375" />
                  <Point X="-3.165370361328" Y="27.732626953125" />
                  <Point X="-3.144923583984" Y="27.730267578125" />
                  <Point X="-3.069521240234" Y="27.723671875" />
                  <Point X="-3.059171142578" Y="27.723333984375" />
                  <Point X="-3.038487792969" Y="27.72378515625" />
                  <Point X="-3.028154541016" Y="27.72457421875" />
                  <Point X="-3.006696533203" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976422851562" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.93844921875" Y="27.750447265625" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911165527344" Y="27.767158203125" />
                  <Point X="-2.902741943359" Y="27.7731953125" />
                  <Point X="-2.886607177734" Y="27.786146484375" />
                  <Point X="-2.871693603516" Y="27.800263671875" />
                  <Point X="-2.818172363281" Y="27.853783203125" />
                  <Point X="-2.811262207031" Y="27.8614921875" />
                  <Point X="-2.7983203125" Y="27.8776171875" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.013365234375" />
                  <Point X="-2.748458251953" Y="28.03404296875" />
                  <Point X="-2.749684570312" Y="28.05454296875" />
                  <Point X="-2.756281494141" Y="28.1299453125" />
                  <Point X="-2.757745361328" Y="28.14019921875" />
                  <Point X="-2.761780761719" Y="28.16048828125" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059387207031" Y="28.699916015625" />
                  <Point X="-2.648369384766" Y="29.0150390625" />
                  <Point X="-2.586456542969" Y="29.049435546875" />
                  <Point X="-2.192524414062" Y="29.268294921875" />
                  <Point X="-2.142282226562" Y="29.202818359375" />
                  <Point X="-2.118563232422" Y="29.171908203125" />
                  <Point X="-2.111820068359" Y="29.164046875" />
                  <Point X="-2.097518798828" Y="29.149107421875" />
                  <Point X="-2.0899609375" Y="29.142029296875" />
                  <Point X="-2.073386962891" Y="29.12812109375" />
                  <Point X="-2.065104980469" Y="29.12190625" />
                  <Point X="-2.047908569336" Y="29.1104140625" />
                  <Point X="-2.027694580078" Y="29.09925390625" />
                  <Point X="-1.943772094727" Y="29.05556640625" />
                  <Point X="-1.934340576172" Y="29.05129296875" />
                  <Point X="-1.915064208984" Y="29.043794921875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.88431262207" Y="29.034966796875" />
                  <Point X="-1.874173950195" Y="29.032833984375" />
                  <Point X="-1.853724121094" Y="29.029685546875" />
                  <Point X="-1.833053955078" Y="29.028783203125" />
                  <Point X="-1.812407348633" Y="29.03013671875" />
                  <Point X="-1.802119506836" Y="29.031376953125" />
                  <Point X="-1.780804443359" Y="29.03513671875" />
                  <Point X="-1.770717407227" Y="29.037490234375" />
                  <Point X="-1.750860595703" Y="29.043279296875" />
                  <Point X="-1.72932800293" Y="29.051587890625" />
                  <Point X="-1.641917114258" Y="29.087794921875" />
                  <Point X="-1.632578979492" Y="29.092275390625" />
                  <Point X="-1.61444909668" Y="29.10222265625" />
                  <Point X="-1.605657592773" Y="29.107689453125" />
                  <Point X="-1.587927978516" Y="29.120103515625" />
                  <Point X="-1.579778442383" Y="29.1265" />
                  <Point X="-1.5642265625" Y="29.140138671875" />
                  <Point X="-1.550252197266" Y="29.155388671875" />
                  <Point X="-1.538020751953" Y="29.1720703125" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153894043" Y="29.19948828125" />
                  <Point X="-1.516856689453" Y="29.2087265625" />
                  <Point X="-1.508526489258" Y="29.227658203125" />
                  <Point X="-1.501048950195" Y="29.249494140625" />
                  <Point X="-1.472598388672" Y="29.339728515625" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.56265625" />
                  <Point X="-0.931166931152" Y="29.7163203125" />
                  <Point X="-0.856119689941" Y="29.72510546875" />
                  <Point X="-0.365222595215" Y="29.78255859375" />
                  <Point X="-0.248587020874" Y="29.347267578125" />
                  <Point X="-0.225666091919" Y="29.2617265625" />
                  <Point X="-0.220435317993" Y="29.247107421875" />
                  <Point X="-0.207661911011" Y="29.218916015625" />
                  <Point X="-0.200119155884" Y="29.20534375" />
                  <Point X="-0.182260925293" Y="29.1786171875" />
                  <Point X="-0.172608901978" Y="29.166455078125" />
                  <Point X="-0.151451400757" Y="29.143865234375" />
                  <Point X="-0.126896362305" Y="29.1250234375" />
                  <Point X="-0.09960043335" Y="29.11043359375" />
                  <Point X="-0.085354034424" Y="29.1042578125" />
                  <Point X="-0.054915912628" Y="29.09392578125" />
                  <Point X="-0.03985357666" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629491806" Y="29.08511328125" />
                  <Point X="0.052165405273" Y="29.090154296875" />
                  <Point X="0.067227592468" Y="29.09392578125" />
                  <Point X="0.097665863037" Y="29.1042578125" />
                  <Point X="0.111912406921" Y="29.11043359375" />
                  <Point X="0.139208343506" Y="29.1250234375" />
                  <Point X="0.163763076782" Y="29.143865234375" />
                  <Point X="0.18492074585" Y="29.166455078125" />
                  <Point X="0.194573059082" Y="29.1786171875" />
                  <Point X="0.212431289673" Y="29.20534375" />
                  <Point X="0.219973449707" Y="29.218916015625" />
                  <Point X="0.232746994019" Y="29.247107421875" />
                  <Point X="0.237978225708" Y="29.2617265625" />
                  <Point X="0.378190002441" Y="29.785005859375" />
                  <Point X="0.827868530273" Y="29.737912109375" />
                  <Point X="0.889983032227" Y="29.722916015625" />
                  <Point X="1.453619628906" Y="29.586833984375" />
                  <Point X="1.49174206543" Y="29.5730078125" />
                  <Point X="1.858253662109" Y="29.440072265625" />
                  <Point X="1.897353027344" Y="29.421787109375" />
                  <Point X="2.25044140625" Y="29.256658203125" />
                  <Point X="2.288281982422" Y="29.23461328125" />
                  <Point X="2.629434570312" Y="29.035857421875" />
                  <Point X="2.665055419922" Y="29.010525390625" />
                  <Point X="2.817780029297" Y="28.901916015625" />
                  <Point X="2.167506347656" Y="27.775609375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.061223144531" Y="27.5906875" />
                  <Point X="2.050482910156" Y="27.5662578125" />
                  <Point X="2.042934814453" Y="27.544650390625" />
                  <Point X="2.038755004883" Y="27.531072265625" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017497558594" Y="27.448744140625" />
                  <Point X="2.014280273438" Y="27.42541015625" />
                  <Point X="2.013397583008" Y="27.413640625" />
                  <Point X="2.013052856445" Y="27.38650390625" />
                  <Point X="2.014403930664" Y="27.36134375" />
                  <Point X="2.021782348633" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.02914453125" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045319091797" Y="27.22388671875" />
                  <Point X="2.055678955078" Y="27.20384765625" />
                  <Point X="2.066550537109" Y="27.186626953125" />
                  <Point X="2.104413085937" Y="27.130828125" />
                  <Point X="2.111697509766" Y="27.121419921875" />
                  <Point X="2.127387451172" Y="27.103583984375" />
                  <Point X="2.13579296875" Y="27.09515625" />
                  <Point X="2.156601318359" Y="27.076759765625" />
                  <Point X="2.175764404297" Y="27.061884765625" />
                  <Point X="2.231563964844" Y="27.024021484375" />
                  <Point X="2.24128125" Y="27.0182421875" />
                  <Point X="2.261328857422" Y="27.00787890625" />
                  <Point X="2.271659179688" Y="27.003294921875" />
                  <Point X="2.293747802734" Y="26.995029296875" />
                  <Point X="2.304551513672" Y="26.991705078125" />
                  <Point X="2.326481689453" Y="26.986361328125" />
                  <Point X="2.345842773438" Y="26.983349609375" />
                  <Point X="2.407032958984" Y="26.975970703125" />
                  <Point X="2.419155273438" Y="26.97529296875" />
                  <Point X="2.4433828125" Y="26.975486328125" />
                  <Point X="2.455488037109" Y="26.976357421875" />
                  <Point X="2.483914306641" Y="26.9802421875" />
                  <Point X="2.507273193359" Y="26.98494140625" />
                  <Point X="2.578036621094" Y="27.003865234375" />
                  <Point X="2.584006103516" Y="27.005673828125" />
                  <Point X="2.604412841797" Y="27.0130703125" />
                  <Point X="2.627658691406" Y="27.023576171875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.940403320312" Y="27.780953125" />
                  <Point X="4.043955078125" Y="27.6370390625" />
                  <Point X="4.063812011719" Y="27.604224609375" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.307504638672" Y="26.84706640625" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.165996582031" Y="26.737931640625" />
                  <Point X="3.146329101562" Y="26.71899609375" />
                  <Point X="3.130230957031" Y="26.7008046875" />
                  <Point X="3.121723632812" Y="26.69048046875" />
                  <Point X="3.070794921875" Y="26.624041015625" />
                  <Point X="3.064079345703" Y="26.614021484375" />
                  <Point X="3.051971191406" Y="26.5932109375" />
                  <Point X="3.046578613281" Y="26.582419921875" />
                  <Point X="3.035555908203" Y="26.55633984375" />
                  <Point X="3.027587158203" Y="26.53354296875" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.006795654297" Y="26.34241015625" />
                  <Point X="3.022368896484" Y="26.26693359375" />
                  <Point X="3.025586181641" Y="26.25519921875" />
                  <Point X="3.033495117188" Y="26.232234375" />
                  <Point X="3.038186767578" Y="26.22100390625" />
                  <Point X="3.051085205078" Y="26.1949609375" />
                  <Point X="3.062619384766" Y="26.174859375" />
                  <Point X="3.104976806641" Y="26.110478515625" />
                  <Point X="3.111739746094" Y="26.101421875" />
                  <Point X="3.126296142578" Y="26.084173828125" />
                  <Point X="3.134089599609" Y="26.075982421875" />
                  <Point X="3.151333007812" Y="26.05989453125" />
                  <Point X="3.160037353516" Y="26.052693359375" />
                  <Point X="3.178239501953" Y="26.039373046875" />
                  <Point X="3.195997558594" Y="26.028603515625" />
                  <Point X="3.257379394531" Y="25.99405078125" />
                  <Point X="3.268589111328" Y="25.988673828125" />
                  <Point X="3.291603759766" Y="25.97943359375" />
                  <Point X="3.303408691406" Y="25.9755703125" />
                  <Point X="3.332558837891" Y="25.96809765625" />
                  <Point X="3.354843017578" Y="25.96378125" />
                  <Point X="3.437829101562" Y="25.9528125" />
                  <Point X="3.444031738281" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603271484" Y="25.952796875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.914236328125" />
                  <Point X="4.758942382812" Y="25.874033203125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.846192382812" Y="25.462671875" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.683141845703" Y="25.418509765625" />
                  <Point X="3.657244140625" Y="25.408287109375" />
                  <Point X="3.63419140625" Y="25.3969375" />
                  <Point X="3.623034179688" Y="25.3909765625" />
                  <Point X="3.541496826172" Y="25.34384765625" />
                  <Point X="3.531259521484" Y="25.3370078125" />
                  <Point X="3.511760742188" Y="25.322068359375" />
                  <Point X="3.502499267578" Y="25.31396875" />
                  <Point X="3.481600341797" Y="25.293154296875" />
                  <Point X="3.466211181641" Y="25.275833984375" />
                  <Point X="3.417291992188" Y="25.2135" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130126953" Y="25.184927734375" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005126953" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.368182128906" Y="25.099013671875" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350340820313" Y="25.0015390625" />
                  <Point X="3.348893798828" Y="24.976798828125" />
                  <Point X="3.34898046875" Y="24.964380859375" />
                  <Point X="3.351175292969" Y="24.93411328125" />
                  <Point X="3.354069335938" Y="24.9121171875" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.81601171875" />
                  <Point X="3.380005859375" Y="24.79451171875" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399130371094" Y="24.752513671875" />
                  <Point X="3.410855224609" Y="24.733232421875" />
                  <Point X="3.423875976562" Y="24.715552734375" />
                  <Point X="3.472798339844" Y="24.653212890625" />
                  <Point X="3.481266357422" Y="24.643791015625" />
                  <Point X="3.499386230469" Y="24.62615234375" />
                  <Point X="3.509038085938" Y="24.617935546875" />
                  <Point X="3.534322753906" Y="24.599171875" />
                  <Point X="3.552465820312" Y="24.58725390625" />
                  <Point X="3.634003173828" Y="24.540123046875" />
                  <Point X="3.639487060547" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027832031" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761613769531" Y="24.06894921875" />
                  <Point X="4.753594726562" Y="24.033806640625" />
                  <Point X="4.727802246094" Y="23.920779296875" />
                  <Point X="3.615611328125" Y="24.067203125" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400101318359" Y="24.09195703125" />
                  <Point X="3.366729003906" Y="24.08916015625" />
                  <Point X="3.354490234375" Y="24.087326171875" />
                  <Point X="3.332954833984" Y="24.082646484375" />
                  <Point X="3.172926025391" Y="24.04786328125" />
                  <Point X="3.157877929688" Y="24.043259765625" />
                  <Point X="3.128751953125" Y="24.0316328125" />
                  <Point X="3.114674072266" Y="24.024609375" />
                  <Point X="3.086847900391" Y="24.007716796875" />
                  <Point X="3.074121582031" Y="23.998466796875" />
                  <Point X="3.050370605469" Y="23.977994140625" />
                  <Point X="3.039345947266" Y="23.966771484375" />
                  <Point X="3.026329101562" Y="23.951115234375" />
                  <Point X="2.929601806641" Y="23.834783203125" />
                  <Point X="2.921323974609" Y="23.8231484375" />
                  <Point X="2.906605712891" Y="23.79876953125" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.873291503906" Y="23.683064453125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787353516" Y="23.423326171875" />
                  <Point X="2.889159423828" Y="23.394515625" />
                  <Point X="2.896541748047" Y="23.380626953125" />
                  <Point X="2.908459716797" Y="23.36208984375" />
                  <Point X="2.997021972656" Y="23.224337890625" />
                  <Point X="3.001742431641" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.195556640625" />
                  <Point X="3.043485839844" Y="23.1718828125" />
                  <Point X="3.052795898438" Y="23.163720703125" />
                  <Point X="4.087170654297" Y="22.370015625" />
                  <Point X="4.045495849609" Y="22.302580078125" />
                  <Point X="4.028904296875" Y="22.279005859375" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="3.007726318359" Y="22.81337109375" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815026611328" Y="22.920484375" />
                  <Point X="2.783122070312" Y="22.930671875" />
                  <Point X="2.771108886719" Y="22.933662109375" />
                  <Point X="2.745478271484" Y="22.938291015625" />
                  <Point X="2.555018554688" Y="22.9726875" />
                  <Point X="2.539359619141" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444849609375" Y="22.96486328125" />
                  <Point X="2.415074951172" Y="22.955041015625" />
                  <Point X="2.400595703125" Y="22.94889453125" />
                  <Point X="2.379302978516" Y="22.937689453125" />
                  <Point X="2.221077636719" Y="22.854416015625" />
                  <Point X="2.208972412109" Y="22.84683203125" />
                  <Point X="2.186039306641" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.109257324219" Y="22.73251171875" />
                  <Point X="2.025984619141" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337158203" Y="22.514736328125" />
                  <Point X="2.001379394531" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.00218737793" Y="22.419859375" />
                  <Point X="2.006816162109" Y="22.394228515625" />
                  <Point X="2.041213134766" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723753173828" Y="20.963916015625" />
                  <Point X="1.955453735352" Y="21.96518359375" />
                  <Point X="1.833914672852" Y="22.123576171875" />
                  <Point X="1.828658691406" Y="22.12984765625" />
                  <Point X="1.808835083008" Y="22.15037109375" />
                  <Point X="1.783252319336" Y="22.17199609375" />
                  <Point X="1.773299438477" Y="22.179353515625" />
                  <Point X="1.748020751953" Y="22.19560546875" />
                  <Point X="1.56017578125" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470336914" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455391479492" Y="22.3513046875" />
                  <Point X="1.424129150391" Y="22.353623046875" />
                  <Point X="1.408401855469" Y="22.353484375" />
                  <Point X="1.380755371094" Y="22.35094140625" />
                  <Point X="1.175315063477" Y="22.332037109375" />
                  <Point X="1.161225463867" Y="22.329662109375" />
                  <Point X="1.133571411133" Y="22.322828125" />
                  <Point X="1.120006835938" Y="22.318369140625" />
                  <Point X="1.092621337891" Y="22.307025390625" />
                  <Point X="1.07987902832" Y="22.3005859375" />
                  <Point X="1.055497802734" Y="22.2858671875" />
                  <Point X="1.04385925293" Y="22.277587890625" />
                  <Point X="1.022511169434" Y="22.259837890625" />
                  <Point X="0.863875366211" Y="22.127935546875" />
                  <Point X="0.852653320312" Y="22.11691015625" />
                  <Point X="0.832182434082" Y="22.09316015625" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041259766" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394287109" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.776409118652" Y="21.965001953125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584716797" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.702578063965" Y="21.3347421875" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605957031" Y="21.519876953125" />
                  <Point X="0.642146362305" Y="21.54641796875" />
                  <Point X="0.626786437988" Y="21.5761875" />
                  <Point X="0.620404602051" Y="21.586796875" />
                  <Point X="0.600984802246" Y="21.614775390625" />
                  <Point X="0.456676971436" Y="21.822697265625" />
                  <Point X="0.44666885376" Y="21.83483203125" />
                  <Point X="0.424786682129" Y="21.85728515625" />
                  <Point X="0.412912872314" Y="21.867603515625" />
                  <Point X="0.386657318115" Y="21.88684765625" />
                  <Point X="0.373238739014" Y="21.89506640625" />
                  <Point X="0.345235961914" Y="21.909173828125" />
                  <Point X="0.330651885986" Y="21.9150625" />
                  <Point X="0.300601074219" Y="21.924388671875" />
                  <Point X="0.077292930603" Y="21.9936953125" />
                  <Point X="0.063376625061" Y="21.996890625" />
                  <Point X="0.035218677521" Y="22.00116015625" />
                  <Point X="0.020976739883" Y="22.002234375" />
                  <Point X="-0.008664761543" Y="22.002234375" />
                  <Point X="-0.022906551361" Y="22.00116015625" />
                  <Point X="-0.051064498901" Y="21.996890625" />
                  <Point X="-0.064980659485" Y="21.9936953125" />
                  <Point X="-0.095031616211" Y="21.984369140625" />
                  <Point X="-0.318339752197" Y="21.9150625" />
                  <Point X="-0.332928131104" Y="21.909171875" />
                  <Point X="-0.360931365967" Y="21.8950625" />
                  <Point X="-0.374346221924" Y="21.88684375" />
                  <Point X="-0.400601501465" Y="21.867599609375" />
                  <Point X="-0.412475738525" Y="21.85728125" />
                  <Point X="-0.43435925293" Y="21.834826171875" />
                  <Point X="-0.444368408203" Y="21.822689453125" />
                  <Point X="-0.463788238525" Y="21.794708984375" />
                  <Point X="-0.608096008301" Y="21.5867890625" />
                  <Point X="-0.612468933105" Y="21.57987109375" />
                  <Point X="-0.625977172852" Y="21.554736328125" />
                  <Point X="-0.638778198242" Y="21.52378515625" />
                  <Point X="-0.642753112793" Y="21.512064453125" />
                  <Point X="-0.985424804688" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.179069712473" Y="22.552942800849" />
                  <Point X="-3.708977160519" Y="22.03085212952" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.961955536308" Y="21.201200564824" />
                  <Point X="-2.823085994822" Y="21.046970314121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.106326102758" Y="22.614128109763" />
                  <Point X="-3.624866674553" Y="22.079413243416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912011040125" Y="21.287706854553" />
                  <Point X="-2.511018479973" Y="20.842359498895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0307244765" Y="22.672139269736" />
                  <Point X="-3.540756188587" Y="22.127974357311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862066543942" Y="21.374213144283" />
                  <Point X="-2.44597618327" Y="20.91209798242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.955122850241" Y="22.730150429708" />
                  <Point X="-3.456645702622" Y="22.176535471206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.812122047759" Y="21.460719434012" />
                  <Point X="-2.38447942655" Y="20.985774187024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.879521223982" Y="22.788161589681" />
                  <Point X="-3.372535216656" Y="22.225096585101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762177551576" Y="21.547225723741" />
                  <Point X="-2.307634756925" Y="21.042404807477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69110985298" Y="23.831497350176" />
                  <Point X="-4.592767527766" Y="23.722277133056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803919597723" Y="22.846172749654" />
                  <Point X="-3.28842473069" Y="22.273657698997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.712233055393" Y="21.633732013471" />
                  <Point X="-2.227819318671" Y="21.095736055113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741227053454" Y="24.029133412468" />
                  <Point X="-4.478480117594" Y="23.737323377269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728317971464" Y="22.904183909626" />
                  <Point X="-3.204314244724" Y="22.322218812892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.66228855921" Y="21.7202383032" />
                  <Point X="-2.148003880417" Y="21.149067302748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.765367367724" Y="24.197919219845" />
                  <Point X="-4.364192707422" Y="23.752369621482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.652716345205" Y="22.962195069599" />
                  <Point X="-3.120203758759" Y="22.370779926787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612344063027" Y="21.80674459293" />
                  <Point X="-2.065329633118" Y="21.199223521281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.773417041046" Y="24.348834560014" />
                  <Point X="-4.249905297251" Y="23.767415865695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577114718947" Y="23.020206229571" />
                  <Point X="-3.036093272793" Y="22.419341040682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562399566844" Y="21.893250882659" />
                  <Point X="-1.959716374337" Y="21.223903386584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135936472794" Y="20.309003118466" />
                  <Point X="-1.087072790956" Y="20.254734501896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.670429166061" Y="24.376430209417" />
                  <Point X="-4.135617887079" Y="23.782462109909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501513092688" Y="23.078217389544" />
                  <Point X="-2.951982786827" Y="22.467902154578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.512455070661" Y="21.979757172389" />
                  <Point X="-1.824906950173" Y="21.216157625228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120044332177" Y="20.433328380447" />
                  <Point X="-0.974971278843" Y="20.272208431849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.567441291076" Y="24.404025858821" />
                  <Point X="-4.021330476908" Y="23.797508354122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425911466429" Y="23.136228549517" />
                  <Point X="-2.867872300862" Y="22.516463268473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462510574478" Y="22.066263462118" />
                  <Point X="-1.689054414348" Y="21.207253371006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.142903657385" Y="20.60069150534" />
                  <Point X="-0.945653694846" Y="20.381623228395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.464453416092" Y="24.431621508224" />
                  <Point X="-3.907043066736" Y="23.812554598335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.35030984017" Y="23.194239709489" />
                  <Point X="-2.783761814896" Y="22.565024382368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.412566199141" Y="22.152769886061" />
                  <Point X="-1.521648204963" Y="21.16330521204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.179152107509" Y="20.782924759928" />
                  <Point X="-0.91633611085" Y="20.491038024941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.361465541107" Y="24.459217157628" />
                  <Point X="-3.792755656565" Y="23.827600842548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.274708213911" Y="23.252250869462" />
                  <Point X="-2.69965132893" Y="22.613585496263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.362622053453" Y="22.239276565055" />
                  <Point X="-0.887018526854" Y="20.600452821487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.258477666122" Y="24.486812807031" />
                  <Point X="-3.678468246393" Y="23.842647086761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.199106574518" Y="23.310262014847" />
                  <Point X="-2.602833109858" Y="22.648033242735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.319738978235" Y="22.33362535728" />
                  <Point X="-0.857700942857" Y="20.709867618033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155489791137" Y="24.514408456434" />
                  <Point X="-3.564180836222" Y="23.857693330975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123504665192" Y="23.368272860442" />
                  <Point X="-2.450088128565" Y="22.620368027172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.329975290059" Y="22.486969205535" />
                  <Point X="-0.828383358861" Y="20.819282414579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052501916152" Y="24.542004105838" />
                  <Point X="-3.44989342605" Y="23.872739575188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047914357321" Y="23.426296590758" />
                  <Point X="-0.799065774864" Y="20.928697211125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.949514041168" Y="24.569599755241" />
                  <Point X="-3.335606015879" Y="23.887785819401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.98600089511" Y="23.499509997027" />
                  <Point X="-0.769748190868" Y="21.038112007672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.77932711911" Y="25.633175816811" />
                  <Point X="-4.728311348293" Y="25.576517063287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.846526166183" Y="24.597195404645" />
                  <Point X="-3.218966856484" Y="23.900220181495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.951382960935" Y="23.603038158332" />
                  <Point X="-0.740430606871" Y="21.147526804218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.761283725994" Y="25.755111870842" />
                  <Point X="-4.559827442409" Y="25.531372001103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.743538291198" Y="24.624791054048" />
                  <Point X="-0.711113022875" Y="21.256941600764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.743240332877" Y="25.877047924874" />
                  <Point X="-4.391343536526" Y="25.486226938918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640550364845" Y="24.652386646402" />
                  <Point X="-0.681795438878" Y="21.36635639731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.721276836756" Y="25.99463026345" />
                  <Point X="-4.222859630642" Y="25.441081876733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537562368878" Y="24.67998216144" />
                  <Point X="-0.652477854882" Y="21.475771193856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691704195778" Y="26.103761790521" />
                  <Point X="-4.054375724758" Y="25.395936814548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44315327457" Y="24.717105512025" />
                  <Point X="-0.61467638839" Y="21.575763684328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662131522316" Y="26.212893281513" />
                  <Point X="-3.885891818874" Y="25.350791752364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368052478266" Y="24.775672900013" />
                  <Point X="-0.559636102809" Y="21.65661052658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.628550918471" Y="26.317573514865" />
                  <Point X="-3.717407912991" Y="25.305646690179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31654005357" Y="24.860437828714" />
                  <Point X="-0.503990543704" Y="21.73678514448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.483524149765" Y="26.298480242792" />
                  <Point X="-3.548924007107" Y="25.260501627994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296409236543" Y="24.980055563627" />
                  <Point X="-0.448345048183" Y="21.816959832998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.338497381059" Y="26.279386970719" />
                  <Point X="-0.379820729935" Y="21.882831139818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193470612353" Y="26.260293698645" />
                  <Point X="-0.289160951581" Y="21.924118527624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.048443843646" Y="26.241200426572" />
                  <Point X="-0.189247022751" Y="21.955128140096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.903417082694" Y="26.222107163111" />
                  <Point X="-0.089333021729" Y="21.986137672392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789595695828" Y="21.009988439031" />
                  <Point X="0.815509548992" Y="20.981208189399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.759858533646" Y="26.204644514164" />
                  <Point X="0.024229457417" Y="22.001989034074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735436279548" Y="21.212113836785" />
                  <Point X="0.793617139967" Y="21.147497445079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.648456700089" Y="26.222895515878" />
                  <Point X="0.191427413607" Y="21.958272163714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.681277057796" Y="21.414239018493" />
                  <Point X="0.771724730943" Y="21.313786700759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.555814720279" Y="26.26198144594" />
                  <Point X="0.379781504009" Y="21.89105902593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.508278506001" Y="21.7483486474" />
                  <Point X="0.749832321918" Y="21.480075956439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487885652662" Y="26.328513845561" />
                  <Point X="0.727939912893" Y="21.646365212118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443036186724" Y="26.420678739643" />
                  <Point X="0.736052441395" Y="21.779330608674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.178130347319" Y="27.379058786215" />
                  <Point X="-3.820563419349" Y="26.981940481123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421726382229" Y="26.53898707632" />
                  <Point X="0.760910791598" Y="21.893697886078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127853091493" Y="27.465195508921" />
                  <Point X="0.786674992236" Y="22.007059114652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.077575835668" Y="27.551332231627" />
                  <Point X="0.834530991322" Y="22.095884915394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.027298579842" Y="27.637468954333" />
                  <Point X="0.903695533793" Y="22.161045181181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972508343022" Y="27.718593503868" />
                  <Point X="0.976799848897" Y="22.221829886176" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91325278482" Y="27.794758811592" />
                  <Point X="1.050302928688" Y="22.282171718118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853996989599" Y="27.870923856081" />
                  <Point X="1.140082133622" Y="22.324437081784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.794741194378" Y="27.947088900569" />
                  <Point X="1.254512352832" Y="22.339324720492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.735485399157" Y="28.023253945057" />
                  <Point X="1.372566250847" Y="22.35018785617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501202989351" Y="27.905032240961" />
                  <Point X="1.511162685119" Y="22.338236193993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580450391303" Y="21.150671885553" />
                  <Point X="2.726625024045" Y="20.988328509079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.237973242039" Y="27.754661261558" />
                  <Point X="1.805976922961" Y="22.152787084133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.843348720372" Y="22.111281498228" />
                  <Point X="2.498161664237" Y="21.384038047898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.083322142809" Y="27.724879087558" />
                  <Point X="2.26969830443" Y="21.779747586718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966996464959" Y="27.737661606178" />
                  <Point X="2.054393528581" Y="22.160843037514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884561183062" Y="27.788083222677" />
                  <Point X="2.015529575957" Y="22.345981101912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817030048541" Y="27.855057571774" />
                  <Point X="2.004156854333" Y="22.500587061112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764103518008" Y="27.938251976836" />
                  <Point X="2.040610180778" Y="22.602076812793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750621688623" Y="28.065254160635" />
                  <Point X="2.08776723196" Y="22.691678873824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.818384372046" Y="28.28248751712" />
                  <Point X="2.137370458125" Y="22.778564182307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.046847754811" Y="28.678197081436" />
                  <Point X="2.205824789654" Y="22.844513217254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.987910795745" Y="28.754716229348" />
                  <Point X="2.291412928844" Y="22.891433230986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912283501127" Y="28.81269888172" />
                  <Point X="2.378146690033" Y="22.937080902588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.836656206509" Y="28.870681534092" />
                  <Point X="2.475523317782" Y="22.970908473395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761028911891" Y="28.928664186463" />
                  <Point X="2.610832515727" Y="22.962607657023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685401617272" Y="28.986646838835" />
                  <Point X="2.763491646574" Y="22.935037788039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.604883817442" Y="29.039198034913" />
                  <Point X="2.993459281759" Y="22.821608126634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519674078588" Y="29.086538304794" />
                  <Point X="3.259698397756" Y="22.667894904707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.434464487354" Y="29.133878738623" />
                  <Point X="3.525937601646" Y="22.514181585166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.349254896119" Y="29.181219172452" />
                  <Point X="2.95376940852" Y="23.291614013277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.104977889794" Y="23.123679981624" />
                  <Point X="3.792176805536" Y="22.360468265624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.264045304885" Y="29.228559606281" />
                  <Point X="2.859925604045" Y="23.537813389203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518557332584" Y="22.806328748823" />
                  <Point X="4.013310788809" Y="22.256849368585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.012802611816" Y="29.091501599336" />
                  <Point X="2.87177870711" Y="23.666624456836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932136775373" Y="22.488977516023" />
                  <Point X="4.067666968366" Y="22.338455987548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.828749674448" Y="29.029065375941" />
                  <Point X="2.897625485476" Y="23.779893973552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.723403532115" Y="29.054041904114" />
                  <Point X="2.951895886945" Y="23.861595858733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.630848769982" Y="29.09322469922" />
                  <Point X="3.013269552726" Y="23.935408769673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554657610072" Y="29.150581115741" />
                  <Point X="3.080290023558" Y="24.002950268254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.505136509684" Y="29.237557634139" />
                  <Point X="3.168817088208" Y="24.04660627459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471866809425" Y="29.342583160904" />
                  <Point X="3.275454691479" Y="24.070148490082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468490859493" Y="29.480809060898" />
                  <Point X="3.384807400723" Y="24.090675274902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.427435246081" Y="29.577187455075" />
                  <Point X="3.522771883947" Y="24.079425465469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.325366038059" Y="29.605803387505" />
                  <Point X="3.667798358849" Y="24.060332519699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.223296830038" Y="29.634419319935" />
                  <Point X="3.81282513753" Y="24.041239236547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121227622017" Y="29.663035252365" />
                  <Point X="3.957851916211" Y="24.022145953396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.019158413995" Y="29.691651184795" />
                  <Point X="3.374403110873" Y="24.812106770604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603338839213" Y="24.557847885618" />
                  <Point X="4.102878694892" Y="24.003052670245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.915216687557" Y="29.718187474836" />
                  <Point X="3.349202135116" Y="24.982070561902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793194530199" Y="24.488967051434" />
                  <Point X="4.247905473573" Y="23.983959387093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.799569826038" Y="29.73172389537" />
                  <Point X="3.368886203814" Y="25.102184461101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.961678210039" Y="24.443822240296" />
                  <Point X="4.392932252254" Y="23.964866103942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.683921776562" Y="29.745258996543" />
                  <Point X="-0.203891125258" Y="29.212130947703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.124125757471" Y="29.123542531989" />
                  <Point X="3.408437411569" Y="25.200233667029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.130161889879" Y="24.398677429159" />
                  <Point X="4.537959030935" Y="23.945772820791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.568273727085" Y="29.758794097716" />
                  <Point X="-0.270131779327" Y="29.42767391934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.036152465708" Y="29.08751080371" />
                  <Point X="3.467199814742" Y="25.276946678901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.298645569719" Y="24.353532618021" />
                  <Point X="4.682985809616" Y="23.926679537639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.452625677609" Y="29.772329198889" />
                  <Point X="-0.324290883387" Y="29.629798970339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.133134307711" Y="29.121776828507" />
                  <Point X="3.537313590842" Y="25.341052713939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.467129249559" Y="24.308387806883" />
                  <Point X="4.745662338519" Y="23.999045472491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.201055189219" Y="29.188318319723" />
                  <Point X="3.621168193372" Y="25.38989801518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.6356129294" Y="24.263242995746" />
                  <Point X="4.768633418025" Y="24.115508776349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.24366926125" Y="29.282965870255" />
                  <Point X="2.020666161218" Y="27.309410874337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.307341980192" Y="26.991025122086" />
                  <Point X="3.715104366663" Y="25.427546597765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.27298678381" Y="29.392380735032" />
                  <Point X="2.018513462035" Y="27.453776961228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.448816233644" Y="26.975877317913" />
                  <Point X="3.003822446768" Y="26.35948047181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.360823356081" Y="25.962990794121" />
                  <Point X="3.818092138025" Y="25.455142362253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302304306371" Y="29.50179559981" />
                  <Point X="2.048969978377" Y="27.561926845258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.556610173651" Y="26.998135291356" />
                  <Point X="3.015108346831" Y="26.488921482196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.498026307652" Y="25.952586751272" />
                  <Point X="3.92107994803" Y="25.482738083824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331621828931" Y="29.611210464588" />
                  <Point X="2.095969483093" Y="27.651703879366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650267980686" Y="27.036093030988" />
                  <Point X="3.050925293489" Y="26.591118005232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.612417648626" Y="25.967517568636" />
                  <Point X="4.024067772535" Y="25.510333789291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360939351492" Y="29.720625329366" />
                  <Point X="2.145913935896" Y="27.738210217274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734378404515" Y="27.084654213894" />
                  <Point X="3.106810932463" Y="26.671025987425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726705033408" Y="25.982563841046" />
                  <Point X="4.12705559704" Y="25.537929494758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.436284076769" Y="29.778921806783" />
                  <Point X="2.195858396678" Y="27.82471654632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.818488828343" Y="27.133215396799" />
                  <Point X="3.170767117182" Y="26.741970720512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840992418191" Y="25.997610113456" />
                  <Point X="4.230043421545" Y="25.565525200224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.577428723841" Y="29.764140067581" />
                  <Point X="2.245802863537" Y="27.911222868617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.902599252172" Y="27.181776579705" />
                  <Point X="3.246276450424" Y="26.800084382264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.955279802974" Y="26.012656385866" />
                  <Point X="4.33303124605" Y="25.593120905691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.718573370912" Y="29.749358328379" />
                  <Point X="2.295747330396" Y="27.997729190914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986709676001" Y="27.23033776261" />
                  <Point X="3.32187809152" Y="26.858095525759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.069567187757" Y="26.027702658277" />
                  <Point X="4.436019070556" Y="25.620716611158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.864727065753" Y="29.729013478038" />
                  <Point X="2.345691797255" Y="28.084235513212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.070820099829" Y="27.278898945516" />
                  <Point X="3.397479765993" Y="26.916106632184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.18385457254" Y="26.042748930687" />
                  <Point X="4.539006895061" Y="25.648312316625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.028071334554" Y="29.68957656112" />
                  <Point X="2.395636264113" Y="28.170741835509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.154930523658" Y="27.327460128421" />
                  <Point X="3.473081440466" Y="26.974117738609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.298141957323" Y="26.057795203097" />
                  <Point X="4.641994719566" Y="25.675908022091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.191415869237" Y="29.650139348908" />
                  <Point X="2.445580730972" Y="28.257248157806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.239040947487" Y="27.376021311327" />
                  <Point X="3.54868311494" Y="27.032128845034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.412429342106" Y="26.072841475507" />
                  <Point X="4.744982544071" Y="25.703503727558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.354760403921" Y="29.610702136697" />
                  <Point X="2.495525197831" Y="28.343754480104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.323151371315" Y="27.424582494232" />
                  <Point X="3.624284789413" Y="27.090139951459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.526716726889" Y="26.087887747917" />
                  <Point X="4.767236150016" Y="25.820763866533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.528559511592" Y="29.559653944889" />
                  <Point X="2.54546966469" Y="28.430260802401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.407261795144" Y="27.473143677137" />
                  <Point X="3.699886463886" Y="27.148151057884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.641004111671" Y="26.102934020327" />
                  <Point X="4.731107051702" Y="26.002864567507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.718389467533" Y="29.490801692368" />
                  <Point X="2.595414131548" Y="28.516767124698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491372218973" Y="27.521704860043" />
                  <Point X="3.77548813836" Y="27.206162164309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.91637601801" Y="29.412890623877" />
                  <Point X="2.645358598407" Y="28.603273446996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.575482642801" Y="27.570266042948" />
                  <Point X="3.851089812833" Y="27.264173270734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.1371971786" Y="29.309619151624" />
                  <Point X="2.695303065266" Y="28.689779769293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.65959306663" Y="27.618827225854" />
                  <Point X="3.926691487306" Y="27.322184377159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.381432364387" Y="29.180343769964" />
                  <Point X="2.745247532125" Y="28.77628609159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.743703490459" Y="27.667388408759" />
                  <Point X="4.00229316178" Y="27.380195483584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.657040342938" Y="29.016225372036" />
                  <Point X="2.795191998983" Y="28.862792413888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.827813914287" Y="27.715949591665" />
                  <Point X="4.077894836253" Y="27.43820659001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.911924338116" Y="27.76451077457" />
                  <Point X="4.079316640477" Y="27.578602788682" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.519052062988" Y="21.28556640625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.444899047852" Y="21.506435546875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.244284881592" Y="21.742927734375" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.038715789795" Y="21.802908203125" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.307698944092" Y="21.686376953125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.812295349121" Y="20.14521875" />
                  <Point X="-0.84774407959" Y="20.012921875" />
                  <Point X="-1.100230712891" Y="20.061931640625" />
                  <Point X="-1.133859741211" Y="20.070583984375" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.314869750977" Y="20.405515625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.316862548828" Y="20.501333984375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.413949829102" Y="20.821634765625" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.685961181641" Y="21.016642578125" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.020476196289" Y="21.005765625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.457094238281" Y="20.5855" />
                  <Point X="-2.470752685547" Y="20.59395703125" />
                  <Point X="-2.855837646484" Y="20.832390625" />
                  <Point X="-2.902380615234" Y="20.868228515625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.594331298828" Y="22.217943359375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763671875" Y="22.4024140625" />
                  <Point X="-2.516037109375" Y="22.43329296875" />
                  <Point X="-2.531327880859" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.591683837891" Y="22.456529296875" />
                  <Point X="-3.728147216797" Y="21.800392578125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.857468505859" Y="21.75316796875" />
                  <Point X="-4.161703613281" Y="22.15287109375" />
                  <Point X="-4.195073242188" Y="22.208826171875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.318279052734" Y="23.45830859375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.144908203125" Y="23.607513671875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.14032421875" Y="23.6653984375" />
                  <Point X="-3.164302246094" Y="23.6912109375" />
                  <Point X="-3.187643798828" Y="23.704947265625" />
                  <Point X="-3.219528808594" Y="23.711427734375" />
                  <Point X="-4.654206054688" Y="23.522548828125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.808404785156" Y="23.522974609375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.936221679688" Y="24.05053515625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.733453613281" Y="24.824197265625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.538604980469" Y="24.880859375" />
                  <Point X="-3.514143798828" Y="24.8978359375" />
                  <Point X="-3.502324707031" Y="24.909353515625" />
                  <Point X="-3.493801757812" Y="24.927626953125" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.486745117188" Y="24.987076171875" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.502328857422" Y="25.028091796875" />
                  <Point X="-3.517447021484" Y="25.04189453125" />
                  <Point X="-3.54189453125" Y="25.05886328125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.865240234375" Y="25.41650390625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.994331542969" Y="25.478177734375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.899872070312" Y="26.062005859375" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.879419921875" Y="26.410587890625" />
                  <Point X="-3.753265869141" Y="26.39398046875" />
                  <Point X="-3.731704833984" Y="26.3958671875" />
                  <Point X="-3.724419189453" Y="26.3981640625" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.636196289062" Y="26.450841796875" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.619843261719" Y="26.552287109375" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968017578" Y="26.619220703125" />
                  <Point X="-4.410111816406" Y="27.194826171875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.457991210938" Y="27.2765" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.112936035156" Y="27.847517578125" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.236636962891" Y="27.97167578125" />
                  <Point X="-3.159156738281" Y="27.926943359375" />
                  <Point X="-3.138519287109" Y="27.920435546875" />
                  <Point X="-3.128366943359" Y="27.919544921875" />
                  <Point X="-3.052964599609" Y="27.91294921875" />
                  <Point X="-3.031506591797" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-3.006050537109" Y="27.934607421875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.938961914062" Y="28.03798828125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.284478759766" Y="28.70978515625" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.271857421875" Y="28.77643359375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.678729492188" Y="29.215525390625" />
                  <Point X="-2.141549072266" Y="29.513970703125" />
                  <Point X="-1.991545410156" Y="29.318482421875" />
                  <Point X="-1.967826293945" Y="29.287572265625" />
                  <Point X="-1.951252319336" Y="29.2736640625" />
                  <Point X="-1.939953125" Y="29.26778125" />
                  <Point X="-1.856030639648" Y="29.22409375" />
                  <Point X="-1.835124023438" Y="29.218490234375" />
                  <Point X="-1.813808837891" Y="29.22225" />
                  <Point X="-1.802045776367" Y="29.227123046875" />
                  <Point X="-1.714634887695" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.682254638672" Y="29.306630859375" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.639947631836" Y="29.714931640625" />
                  <Point X="-0.968083007812" Y="29.903296875" />
                  <Point X="-0.878208557129" Y="29.91381640625" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.06506111145" Y="29.396443359375" />
                  <Point X="-0.042140228271" Y="29.31090234375" />
                  <Point X="-0.024282068253" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594009399" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.2247759552" Y="29.946560546875" />
                  <Point X="0.236648223877" Y="29.9908671875" />
                  <Point X="0.273580200195" Y="29.987" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="0.934572509766" Y="29.907611328125" />
                  <Point X="1.508456054688" Y="29.769056640625" />
                  <Point X="1.556526367188" Y="29.75162109375" />
                  <Point X="1.931044189453" Y="29.61578125" />
                  <Point X="1.977841308594" Y="29.593896484375" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.383924316406" Y="29.39878515625" />
                  <Point X="2.732519775391" Y="29.195693359375" />
                  <Point X="2.775170898438" Y="29.165361328125" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.332051269531" Y="27.680609375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.222305419922" Y="27.481990234375" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.203037597656" Y="27.38408984375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.223777587891" Y="27.293302734375" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.282448486328" Y="27.219107421875" />
                  <Point X="2.338248046875" Y="27.181244140625" />
                  <Point X="2.360336669922" Y="27.172978515625" />
                  <Point X="2.368571289062" Y="27.171986328125" />
                  <Point X="2.429761474609" Y="27.164607421875" />
                  <Point X="2.458187744141" Y="27.1684921875" />
                  <Point X="2.528951171875" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.856405273438" Y="27.95184765625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="3.995814208984" Y="28.02925390625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.2263671875" Y="27.70258984375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.423169189453" Y="26.696328125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.272518066406" Y="26.574890625" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.210566650391" Y="26.48237109375" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.192875244141" Y="26.38080859375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.221346923828" Y="26.2792890625" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.289208007812" Y="26.194169921875" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.379739990234" Y="26.15214453125" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803466797" Y="26.141171875" />
                  <Point X="4.725318359375" Y="26.305673828125" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.85037890625" Y="26.3161875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.946680664062" Y="25.903263671875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.895368164062" Y="25.27914453125" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.718114746094" Y="25.2264765625" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.615678466797" Y="25.158533203125" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.554790527344" Y="25.063275390625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.540677734375" Y="24.94785546875" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.573342285156" Y="24.832853515625" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.647549316406" Y="24.75175" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167480469" Y="24.699611328125" />
                  <Point X="4.887029296875" Y="24.392580078125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.998019042969" Y="24.362501953125" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.938833007812" Y="23.991537109375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.590811523438" Y="23.878828125" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.373300537109" Y="23.896978515625" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.172428710938" Y="23.829646484375" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.0624921875" Y="23.665654296875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.068278564453" Y="23.46483984375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.1684609375" Y="23.314458984375" />
                  <Point X="4.231827148438" Y="22.4985078125" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.204134277344" Y="22.197861328125" />
                  <Point X="4.184279296875" Y="22.169650390625" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="2.912726318359" Y="22.648828125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.711710205078" Y="22.751314453125" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.467784912109" Y="22.769548828125" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.277393554688" Y="22.6440234375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.193791748047" Y="22.427994140625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.917411621094" Y="21.037876953125" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.813104492188" Y="20.795421875" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.804716552734" Y="21.84951953125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.645270385742" Y="22.03578515625" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.398158447266" Y="22.16173828125" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.143984619141" Y="22.113740234375" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.962073913574" Y="21.924646484375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.107577148438" Y="20.218380859375" />
                  <Point X="1.127642578125" Y="20.065970703125" />
                  <Point X="0.994366210938" Y="20.0367578125" />
                  <Point X="0.973839050293" Y="20.03302734375" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#128" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.022920783437" Y="4.440666006273" Z="0.3" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="-0.890176654647" Y="4.994757309884" Z="0.3" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.3" />
                  <Point X="-1.659601239299" Y="4.794355896753" Z="0.3" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.3" />
                  <Point X="-1.74543731072" Y="4.73023516653" Z="0.3" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.3" />
                  <Point X="-1.736096496222" Y="4.352946996444" Z="0.3" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.3" />
                  <Point X="-1.827336009275" Y="4.304597030641" Z="0.3" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.3" />
                  <Point X="-1.923021593809" Y="4.343412201105" Z="0.3" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.3" />
                  <Point X="-1.958034232034" Y="4.380202577259" Z="0.3" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.3" />
                  <Point X="-2.709168484006" Y="4.290513274447" Z="0.3" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.3" />
                  <Point X="-3.308435598382" Y="3.84739381736" Z="0.3" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.3" />
                  <Point X="-3.333936054119" Y="3.716066253979" Z="0.3" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.3" />
                  <Point X="-2.994927771378" Y="3.064910584831" Z="0.3" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.3" />
                  <Point X="-3.04756112162" Y="3.001242446267" Z="0.3" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.3" />
                  <Point X="-3.130165880776" Y="3.000636927795" Z="0.3" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.3" />
                  <Point X="-3.217793094318" Y="3.046257894626" Z="0.3" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.3" />
                  <Point X="-4.158555515522" Y="2.909501502997" Z="0.3" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.3" />
                  <Point X="-4.507329198" Y="2.332986107535" Z="0.3" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.3" />
                  <Point X="-4.446705943642" Y="2.186439527258" Z="0.3" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.3" />
                  <Point X="-3.670349884955" Y="1.560480553132" Z="0.3" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.3" />
                  <Point X="-3.688546464969" Y="1.501257926128" Z="0.3" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.3" />
                  <Point X="-3.745610298701" Y="1.477129727547" Z="0.3" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.3" />
                  <Point X="-3.879049898843" Y="1.491441006393" Z="0.3" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.3" />
                  <Point X="-4.95428785543" Y="1.106363588244" Z="0.3" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.3" />
                  <Point X="-5.049949098396" Y="0.516776252241" Z="0.3" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.3" />
                  <Point X="-4.884337177039" Y="0.399486678799" Z="0.3" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.3" />
                  <Point X="-3.552099634682" Y="0.032091631074" Z="0.3" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.3" />
                  <Point X="-3.540654018987" Y="0.003535409559" Z="0.3" />
                  <Point X="-3.539556741714" Y="0" Z="0.3" />
                  <Point X="-3.547710582148" Y="-0.026271541502" Z="0.3" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.3" />
                  <Point X="-3.573268960442" Y="-0.04678435206" Z="0.3" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.3" />
                  <Point X="-3.752550611733" Y="-0.096225377957" Z="0.3" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.3" />
                  <Point X="-4.991873627245" Y="-0.925262157864" Z="0.3" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.3" />
                  <Point X="-4.863005660257" Y="-1.458119841728" Z="0.3" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.3" />
                  <Point X="-4.653836169148" Y="-1.495742119034" Z="0.3" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.3" />
                  <Point X="-3.195817411136" Y="-1.320601085424" Z="0.3" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.3" />
                  <Point X="-3.199466970005" Y="-1.348668827989" Z="0.3" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.3" />
                  <Point X="-3.354872985421" Y="-1.470743186905" Z="0.3" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.3" />
                  <Point X="-4.244172405535" Y="-2.785503410855" Z="0.3" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.3" />
                  <Point X="-3.903410394573" Y="-3.24583515394" Z="0.3" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.3" />
                  <Point X="-3.709302915994" Y="-3.211628421502" Z="0.3" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.3" />
                  <Point X="-2.557548683636" Y="-2.570781785496" Z="0.3" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.3" />
                  <Point X="-2.643788591666" Y="-2.725775466498" Z="0.3" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.3" />
                  <Point X="-2.93904053648" Y="-4.14010870366" Z="0.3" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.3" />
                  <Point X="-2.503230082172" Y="-4.417274909183" Z="0.3" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.3" />
                  <Point X="-2.424442847897" Y="-4.414778166478" Z="0.3" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.3" />
                  <Point X="-1.998853438567" Y="-4.004528954861" Z="0.3" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.3" />
                  <Point X="-1.69538729161" Y="-4.001969147028" Z="0.3" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.3" />
                  <Point X="-1.45307310646" Y="-4.184677053485" Z="0.3" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.3" />
                  <Point X="-1.372057709795" Y="-4.477140117199" Z="0.3" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.3" />
                  <Point X="-1.370597982061" Y="-4.556675723872" Z="0.3" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.3" />
                  <Point X="-1.152474555018" Y="-4.946559667257" Z="0.3" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.3" />
                  <Point X="-0.853219572698" Y="-5.006862280966" Z="0.3" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="-0.77015503697" Y="-4.836441846383" Z="0.3" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="-0.272778860908" Y="-3.310852879682" Z="0.3" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="-0.030050980702" Y="-3.213566083864" Z="0.3" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.3" />
                  <Point X="0.22330809866" Y="-3.273545961424" Z="0.3" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.3" />
                  <Point X="0.397666998524" Y="-3.490792876086" Z="0.3" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.3" />
                  <Point X="0.464599823431" Y="-3.696094183433" Z="0.3" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.3" />
                  <Point X="0.976619741722" Y="-4.98488723517" Z="0.3" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.3" />
                  <Point X="1.155816663665" Y="-4.946410281962" Z="0.3" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.3" />
                  <Point X="1.150993452488" Y="-4.743813595399" Z="0.3" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.3" />
                  <Point X="1.004777097153" Y="-3.054692447311" Z="0.3" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.3" />
                  <Point X="1.169795837491" Y="-2.893425530001" Z="0.3" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.3" />
                  <Point X="1.396583986774" Y="-2.856770642481" Z="0.3" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.3" />
                  <Point X="1.612075275066" Y="-2.974993451463" Z="0.3" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.3" />
                  <Point X="1.758892950868" Y="-3.149638016126" Z="0.3" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.3" />
                  <Point X="2.834117668655" Y="-4.215272378526" Z="0.3" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.3" />
                  <Point X="3.024066917953" Y="-4.081147405703" Z="0.3" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.3" />
                  <Point X="2.954556967612" Y="-3.905843207782" Z="0.3" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.3" />
                  <Point X="2.23683997045" Y="-2.531838719987" Z="0.3" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.3" />
                  <Point X="2.31548401035" Y="-2.347982867844" Z="0.3" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.3" />
                  <Point X="2.484915510528" Y="-2.243417299617" Z="0.3" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.3" />
                  <Point X="2.696668083526" Y="-2.266608039041" Z="0.3" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.3" />
                  <Point X="2.881570389325" Y="-2.363192532662" Z="0.3" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.3" />
                  <Point X="4.219012041221" Y="-2.827846070676" Z="0.3" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.3" />
                  <Point X="4.380291838421" Y="-2.57095694341" Z="0.3" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.3" />
                  <Point X="4.25610935937" Y="-2.430542978564" Z="0.3" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.3" />
                  <Point X="3.104180500925" Y="-1.476840154961" Z="0.3" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.3" />
                  <Point X="3.106125842423" Y="-1.307646311059" Z="0.3" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.3" />
                  <Point X="3.204718758452" Y="-1.171039316781" Z="0.3" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.3" />
                  <Point X="3.377764471483" Y="-1.120601230245" Z="0.3" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.3" />
                  <Point X="3.578129331931" Y="-1.139463769306" Z="0.3" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.3" />
                  <Point X="4.981423930931" Y="-0.988307502563" Z="0.3" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.3" />
                  <Point X="5.041304538375" Y="-0.613671150005" Z="0.3" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.3" />
                  <Point X="4.893814404841" Y="-0.52784334132" Z="0.3" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.3" />
                  <Point X="3.666415346025" Y="-0.173680584955" Z="0.3" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.3" />
                  <Point X="3.60652030251" Y="-0.104999557712" Z="0.3" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.3" />
                  <Point X="3.583629252982" Y="-0.011458998134" Z="0.3" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.3" />
                  <Point X="3.597742197442" Y="0.085151533107" Z="0.3" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.3" />
                  <Point X="3.648859135891" Y="0.158949180863" Z="0.3" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.3" />
                  <Point X="3.736980068327" Y="0.214468259287" Z="0.3" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.3" />
                  <Point X="3.902153412466" Y="0.262128591449" Z="0.3" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.3" />
                  <Point X="4.989929564077" Y="0.942235324539" Z="0.3" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.3" />
                  <Point X="4.892803934557" Y="1.359294895286" Z="0.3" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.3" />
                  <Point X="4.712635994749" Y="1.386525849076" Z="0.3" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.3" />
                  <Point X="3.380129053303" Y="1.232992534642" Z="0.3" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.3" />
                  <Point X="3.307578814153" Y="1.269021218871" Z="0.3" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.3" />
                  <Point X="3.256961114038" Y="1.33805247489" Z="0.3" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.3" />
                  <Point X="3.235687753231" Y="1.422192278104" Z="0.3" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.3" />
                  <Point X="3.252563024744" Y="1.50018492521" Z="0.3" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.3" />
                  <Point X="3.306044601717" Y="1.575754095203" Z="0.3" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.3" />
                  <Point X="3.447451240243" Y="1.687941307763" Z="0.3" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.3" />
                  <Point X="4.262988685564" Y="2.759757349562" Z="0.3" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.3" />
                  <Point X="4.030244022852" Y="3.089736756195" Z="0.3" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.3" />
                  <Point X="3.825249230333" Y="3.026428704552" Z="0.3" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.3" />
                  <Point X="2.439114428662" Y="2.248075688346" Z="0.3" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.3" />
                  <Point X="2.368401288174" Y="2.252907599765" Z="0.3" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.3" />
                  <Point X="2.304367134131" Y="2.291762867725" Z="0.3" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.3" />
                  <Point X="2.25899573251" Y="2.352657726251" Z="0.3" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.3" />
                  <Point X="2.246522102994" Y="2.421357155659" Z="0.3" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.3" />
                  <Point X="2.264452254312" Y="2.50035514722" Z="0.3" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.3" />
                  <Point X="2.369196648308" Y="2.686889851622" Z="0.3" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.3" />
                  <Point X="2.797991948819" Y="4.23739024871" Z="0.3" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.3" />
                  <Point X="2.402938177215" Y="4.473268872164" Z="0.3" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.3" />
                  <Point X="1.992866807573" Y="4.670467683775" Z="0.3" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.3" />
                  <Point X="1.567418982799" Y="4.829906493772" Z="0.3" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.3" />
                  <Point X="0.940150979976" Y="4.98749479282" Z="0.3" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.3" />
                  <Point X="0.272632066406" Y="5.068009040363" Z="0.3" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.3" />
                  <Point X="0.170323778116" Y="4.990781499678" Z="0.3" />
                  <Point X="0" Y="4.355124473572" Z="0.3" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>