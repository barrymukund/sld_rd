<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#161" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1744" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.73698248291" Y="20.839291015625" />
                  <Point X="0.563301940918" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.54236315918" Y="21.532625" />
                  <Point X="0.471375518799" Y="21.634904296875" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.35674887085" Y="21.790982421875" />
                  <Point X="0.330493835449" Y="21.810224609375" />
                  <Point X="0.302494567871" Y="21.824330078125" />
                  <Point X="0.192645370483" Y="21.858421875" />
                  <Point X="0.049135429382" Y="21.902962890625" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036824390411" Y="21.90296484375" />
                  <Point X="-0.146673446655" Y="21.86887109375" />
                  <Point X="-0.290183532715" Y="21.82433203125" />
                  <Point X="-0.318184448242" Y="21.810224609375" />
                  <Point X="-0.344439941406" Y="21.79098046875" />
                  <Point X="-0.366323883057" Y="21.7685234375" />
                  <Point X="-0.437311401367" Y="21.6662421875" />
                  <Point X="-0.530051513672" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.777890808105" Y="20.640669921875" />
                  <Point X="-0.916584838867" Y="20.12305859375" />
                  <Point X="-1.079318481445" Y="20.154646484375" />
                  <Point X="-1.203320800781" Y="20.18655078125" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.235870117188" Y="20.277755859375" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.242751586914" Y="20.61570703125" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.424779907227" Y="20.95748828125" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643027954102" Y="21.109033203125" />
                  <Point X="-1.777256835938" Y="21.117830078125" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.9834140625" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657226562" Y="21.10519921875" />
                  <Point X="-2.15450390625" Y="21.030466796875" />
                  <Point X="-2.300623535156" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.462489746094" Y="20.7345234375" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.559121826172" Y="20.76041015625" />
                  <Point X="-2.801725585938" Y="20.910623046875" />
                  <Point X="-2.973384033203" Y="21.042794921875" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.746526611328" Y="21.764333984375" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561279297" Y="22.4444296875" />
                  <Point X="-2.428779052734" Y="22.4732578125" />
                  <Point X="-2.446807128906" Y="22.4984140625" />
                  <Point X="-2.464155761719" Y="22.51576171875" />
                  <Point X="-2.489311767578" Y="22.533787109375" />
                  <Point X="-2.518140625" Y="22.54800390625" />
                  <Point X="-2.547758789062" Y="22.55698828125" />
                  <Point X="-2.578693359375" Y="22.555974609375" />
                  <Point X="-2.610218994141" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.369536865234" Y="22.117130859375" />
                  <Point X="-3.818022705078" Y="21.858197265625" />
                  <Point X="-3.891202880859" Y="21.954341796875" />
                  <Point X="-4.08286328125" Y="22.20614453125" />
                  <Point X="-4.205935546875" Y="22.412517578125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.670689208984" Y="23.0681484375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64034375" />
                  <Point X="-3.045556884766" Y="23.672015625" />
                  <Point X="-3.052558349609" Y="23.701759765625" />
                  <Point X="-3.068641113281" Y="23.727744140625" />
                  <Point X="-3.089474609375" Y="23.751701171875" />
                  <Point X="-3.112975097656" Y="23.771234375" />
                  <Point X="-3.139457763672" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608642578" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.153931152344" Y="23.684232421875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.759118652344" Y="23.713884765625" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.866640136719" Y="24.23501953125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.176193847656" Y="24.607212890625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.510781982422" Y="24.78861328125" />
                  <Point X="-3.483181640625" Y="24.804185546875" />
                  <Point X="-3.469872802734" Y="24.813263671875" />
                  <Point X="-3.442333984375" Y="24.83569140625" />
                  <Point X="-3.426107910156" Y="24.85264453125" />
                  <Point X="-3.414532226562" Y="24.873056640625" />
                  <Point X="-3.403095458984" Y="24.90071875" />
                  <Point X="-3.398431152344" Y="24.915181640625" />
                  <Point X="-3.390944091797" Y="24.946884765625" />
                  <Point X="-3.388402099609" Y="24.968240234375" />
                  <Point X="-3.390729003906" Y="24.98962109375" />
                  <Point X="-3.396986816406" Y="25.017365234375" />
                  <Point X="-3.401469726562" Y="25.03178515625" />
                  <Point X="-3.41413671875" Y="25.06341015625" />
                  <Point X="-3.425486572266" Y="25.083951171875" />
                  <Point X="-3.441525390625" Y="25.101083984375" />
                  <Point X="-3.465372802734" Y="25.120947265625" />
                  <Point X="-3.478537109375" Y="25.13014453125" />
                  <Point X="-3.509827880859" Y="25.148279296875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.37332421875" Y="25.383046875" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.872797363281" Y="25.65050390625" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.758939941406" Y="26.218865234375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.2268125" Y="26.36050390625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985351562" Y="26.299341796875" />
                  <Point X="-3.723423339844" Y="26.301228515625" />
                  <Point X="-3.703139648438" Y="26.305263671875" />
                  <Point X="-3.676507080078" Y="26.31366015625" />
                  <Point X="-3.641713623047" Y="26.324630859375" />
                  <Point X="-3.622783691406" Y="26.332958984375" />
                  <Point X="-3.604039550781" Y="26.343779296875" />
                  <Point X="-3.587354492188" Y="26.35601171875" />
                  <Point X="-3.573713378906" Y="26.37156640625" />
                  <Point X="-3.561299072266" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.540665039062" Y="26.433228515625" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.544944580078" Y="26.6141484375" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.084219482422" Y="27.064505859375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.268864746094" Y="27.412064453125" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.907520751953" Y="27.956837890625" />
                  <Point X="-3.750503417969" Y="28.158662109375" />
                  <Point X="-3.489879882812" Y="28.00819140625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187730224609" Y="27.836341796875" />
                  <Point X="-3.167088867188" Y="27.82983203125" />
                  <Point X="-3.14679296875" Y="27.825794921875" />
                  <Point X="-3.109701171875" Y="27.82255078125" />
                  <Point X="-3.061243652344" Y="27.818310546875" />
                  <Point X="-3.040561767578" Y="27.81876171875" />
                  <Point X="-3.019102783203" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.919749755859" Y="27.886556640625" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846680908203" Y="28.0732109375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.083420654297" Y="28.55154296875" />
                  <Point X="-3.183333496094" Y="28.724595703125" />
                  <Point X="-3.0275546875" Y="28.84403125" />
                  <Point X="-2.70062890625" Y="29.094681640625" />
                  <Point X="-2.427162109375" Y="29.24661328125" />
                  <Point X="-2.167036621094" Y="29.391134765625" />
                  <Point X="-2.129898681641" Y="29.342734375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028891479492" Y="29.21480078125" />
                  <Point X="-2.012311645508" Y="29.200888671875" />
                  <Point X="-1.995115722656" Y="29.1893984375" />
                  <Point X="-1.953832885742" Y="29.16790625" />
                  <Point X="-1.899899780273" Y="29.139830078125" />
                  <Point X="-1.880625488281" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.839269042969" Y="29.123580078125" />
                  <Point X="-1.818622070312" Y="29.12493359375" />
                  <Point X="-1.797307128906" Y="29.128693359375" />
                  <Point X="-1.777451660156" Y="29.134482421875" />
                  <Point X="-1.734452636719" Y="29.152294921875" />
                  <Point X="-1.678277587891" Y="29.1755625" />
                  <Point X="-1.660148193359" Y="29.185509765625" />
                  <Point X="-1.64241796875" Y="29.197923828125" />
                  <Point X="-1.626865234375" Y="29.2115625" />
                  <Point X="-1.614633422852" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265919921875" />
                  <Point X="-1.581484863281" Y="29.310306640625" />
                  <Point X="-1.563201171875" Y="29.368296875" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.582017089844" Y="29.615302734375" />
                  <Point X="-1.584201660156" Y="29.631896484375" />
                  <Point X="-1.372865356445" Y="29.6911484375" />
                  <Point X="-0.94962322998" Y="29.80980859375" />
                  <Point X="-0.618119873047" Y="29.848607421875" />
                  <Point X="-0.294711151123" Y="29.886458984375" />
                  <Point X="-0.217688583374" Y="29.599005859375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113998413" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.255674667358" Y="29.694822265625" />
                  <Point X="0.307419555664" Y="29.8879375" />
                  <Point X="0.474492675781" Y="29.870439453125" />
                  <Point X="0.844044921875" Y="29.83173828125" />
                  <Point X="1.118317382812" Y="29.76551953125" />
                  <Point X="1.481025024414" Y="29.677951171875" />
                  <Point X="1.658715209961" Y="29.613501953125" />
                  <Point X="1.894646972656" Y="29.527927734375" />
                  <Point X="2.067275634766" Y="29.4471953125" />
                  <Point X="2.294559082031" Y="29.340900390625" />
                  <Point X="2.461372558594" Y="29.243716796875" />
                  <Point X="2.680988525391" Y="29.115767578125" />
                  <Point X="2.838264648438" Y="29.003921875" />
                  <Point X="2.943260498047" Y="28.92925390625" />
                  <Point X="2.521157714844" Y="28.198150390625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.123768066406" Y="27.481244140625" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.111357666016" Y="27.3508515625" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070800781" Y="27.24747265625" />
                  <Point X="2.158696044922" Y="27.2200234375" />
                  <Point X="2.183028564453" Y="27.1841640625" />
                  <Point X="2.194464355469" Y="27.170330078125" />
                  <Point X="2.221597900391" Y="27.14559375" />
                  <Point X="2.249046630859" Y="27.12696875" />
                  <Point X="2.284906494141" Y="27.102634765625" />
                  <Point X="2.304946533203" Y="27.0922734375" />
                  <Point X="2.327035644531" Y="27.084005859375" />
                  <Point X="2.348965576172" Y="27.078662109375" />
                  <Point X="2.379066162109" Y="27.075033203125" />
                  <Point X="2.418390380859" Y="27.070291015625" />
                  <Point X="2.436467773438" Y="27.06984375" />
                  <Point X="2.473207519531" Y="27.074169921875" />
                  <Point X="2.508017333984" Y="27.083478515625" />
                  <Point X="2.553493896484" Y="27.095640625" />
                  <Point X="2.565290771484" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.43386328125" Y="27.598197265625" />
                  <Point X="3.967325927734" Y="27.90619140625" />
                  <Point X="3.993008789062" Y="27.870498046875" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.210957519531" Y="27.544560546875" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.722632324219" Y="27.045861328125" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221420410156" Y="26.660236328125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.178921142578" Y="26.608943359375" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136607421875" Y="26.5509140625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.112297607422" Y="26.483716796875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.105400146484" Y="26.334640625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.235740234375" />
                  <Point X="3.157119384766" Y="26.2040703125" />
                  <Point X="3.184340576172" Y="26.1626953125" />
                  <Point X="3.198889648438" Y="26.145455078125" />
                  <Point X="3.216133789062" Y="26.12936328125" />
                  <Point X="3.234345214844" Y="26.11603515625" />
                  <Point X="3.264539794922" Y="26.099037109375" />
                  <Point X="3.303987304688" Y="26.07683203125" />
                  <Point X="3.320529296875" Y="26.06949609375" />
                  <Point X="3.356120849609" Y="26.0594375" />
                  <Point X="3.396946044922" Y="26.05404296875" />
                  <Point X="3.450281494141" Y="26.046994140625" />
                  <Point X="3.462697998047" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.291209960938" Y="26.152701171875" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.789988769531" Y="26.162623046875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.873568359375" Y="25.75533203125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.280249023438" Y="25.480625" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704787597656" Y="25.325583984375" />
                  <Point X="3.681547119141" Y="25.315068359375" />
                  <Point X="3.6414375" Y="25.291884765625" />
                  <Point X="3.589037109375" Y="25.26159765625" />
                  <Point X="3.574314697266" Y="25.251099609375" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.523464599609" Y="25.19491015625" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.455658935547" Y="25.050716796875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.453200927734" Y="24.89955859375" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.516090087891" Y="24.75192578125" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559999023438" Y="24.69876171875" />
                  <Point X="3.589035888672" Y="24.675841796875" />
                  <Point X="3.629145507812" Y="24.652658203125" />
                  <Point X="3.681545898438" Y="24.622369140625" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.452973632812" Y="24.410533203125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.886261230469" Y="24.25847265625" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.81962109375" Y="23.896138671875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.078079833984" Y="23.910498046875" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659423828" Y="23.994490234375" />
                  <Point X="3.295938476562" Y="23.977380859375" />
                  <Point X="3.193095214844" Y="23.95502734375" />
                  <Point X="3.163973144531" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.064815185547" Y="23.8488125" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.962937988281" Y="23.6205234375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.020016357422" Y="23.364240234375" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.794005615234" Y="22.71471484375" />
                  <Point X="4.213122070312" Y="22.3931171875" />
                  <Point X="4.212868652344" Y="22.39270703125" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.051597167969" Y="22.146189453125" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.383160644531" Y="22.48691796875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224121094" Y="22.840173828125" />
                  <Point X="2.660533691406" Y="22.85709375" />
                  <Point X="2.538133789062" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.366999755859" Y="22.823859375" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.163568359375" Y="22.6317265625" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.112595947266" Y="22.34305078125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.590957763672" Y="21.413310546875" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781836425781" Y="20.888345703125" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="2.202823242188" Y="21.48675" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721923461914" Y="22.099443359375" />
                  <Point X="1.629519287109" Y="22.158849609375" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.316040893555" Y="22.24958203125" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156362792969" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="1.026559448242" Y="22.13965234375" />
                  <Point X="0.92461151123" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.852292053223" Y="21.86684375" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.944190795898" Y="20.731599609375" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.975697937012" Y="20.129921875" />
                  <Point X="0.929315795898" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058414794922" Y="20.24736328125" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.149576904297" Y="20.634240234375" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250210327148" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.362141967773" Y="21.028912109375" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735717773" Y="21.15403125" />
                  <Point X="-1.53301940918" Y="21.170376953125" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576531494141" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621457885742" Y="21.201552734375" />
                  <Point X="-1.636815307617" Y="21.203830078125" />
                  <Point X="-1.771044189453" Y="21.212626953125" />
                  <Point X="-1.946404296875" Y="21.22412109375" />
                  <Point X="-1.961928344727" Y="21.2238671875" />
                  <Point X="-1.992725585938" Y="21.220833984375" />
                  <Point X="-2.007998535156" Y="21.2180546875" />
                  <Point X="-2.039047607422" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095435791016" Y="21.184189453125" />
                  <Point X="-2.207282470703" Y="21.10945703125" />
                  <Point X="-2.353402099609" Y="21.011822265625" />
                  <Point X="-2.359681396484" Y="21.007244140625" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410470947266" Y="20.958369140625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.509111083984" Y="20.841181640625" />
                  <Point X="-2.747611328125" Y="20.988853515625" />
                  <Point X="-2.915426513672" Y="21.11806640625" />
                  <Point X="-2.98086328125" Y="21.16844921875" />
                  <Point X="-2.664254150391" Y="21.716833984375" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314666992188" Y="22.442388671875" />
                  <Point X="-2.323652587891" Y="22.4720078125" />
                  <Point X="-2.329359863281" Y="22.486451171875" />
                  <Point X="-2.343577636719" Y="22.515279296875" />
                  <Point X="-2.351560546875" Y="22.528595703125" />
                  <Point X="-2.369588623047" Y="22.553751953125" />
                  <Point X="-2.379633789062" Y="22.565591796875" />
                  <Point X="-2.396982421875" Y="22.582939453125" />
                  <Point X="-2.408822753906" Y="22.592984375" />
                  <Point X="-2.433978759766" Y="22.611009765625" />
                  <Point X="-2.447294433594" Y="22.618990234375" />
                  <Point X="-2.476123291016" Y="22.63320703125" />
                  <Point X="-2.490564208984" Y="22.6389140625" />
                  <Point X="-2.520182373047" Y="22.6478984375" />
                  <Point X="-2.550870117188" Y="22.6519375" />
                  <Point X="-2.5818046875" Y="22.650923828125" />
                  <Point X="-2.597228759766" Y="22.6491484375" />
                  <Point X="-2.628754394531" Y="22.642876953125" />
                  <Point X="-2.643685058594" Y="22.63861328125" />
                  <Point X="-2.672649658203" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.417036865234" Y="22.19940234375" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.815609375" Y="22.01187890625" />
                  <Point X="-4.004018066406" Y="22.25941015625" />
                  <Point X="-4.124342773438" Y="22.46117578125" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.612856933594" Y="22.992779296875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748535156" Y="23.631626953125" />
                  <Point X="-2.948577880859" Y="23.646955078125" />
                  <Point X="-2.950787109375" Y="23.678626953125" />
                  <Point X="-2.953084228516" Y="23.693783203125" />
                  <Point X="-2.960085693359" Y="23.72352734375" />
                  <Point X="-2.971779296875" Y="23.7517578125" />
                  <Point X="-2.987862060547" Y="23.7777421875" />
                  <Point X="-2.996955566406" Y="23.790083984375" />
                  <Point X="-3.0177890625" Y="23.814041015625" />
                  <Point X="-3.028749755859" Y="23.824759765625" />
                  <Point X="-3.052250244141" Y="23.84429296875" />
                  <Point X="-3.064790039063" Y="23.853107421875" />
                  <Point X="-3.091272705078" Y="23.868693359375" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.134700927734" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891142578125" />
                  <Point X="-3.181687988281" Y="23.897623046875" />
                  <Point X="-3.197304931641" Y="23.89946875" />
                  <Point X="-3.228625488281" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.166331054688" Y="23.778419921875" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.66707421875" Y="23.737396484375" />
                  <Point X="-4.740762207031" Y="24.025880859375" />
                  <Point X="-4.772597167969" Y="24.24846875" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.151605957031" Y="24.51544921875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.49695703125" Y="24.691642578125" />
                  <Point X="-3.474863037109" Y="24.700666015625" />
                  <Point X="-3.464100097656" Y="24.705873046875" />
                  <Point X="-3.436499755859" Y="24.7214453125" />
                  <Point X="-3.429648925781" Y="24.725705078125" />
                  <Point X="-3.409882080078" Y="24.7396015625" />
                  <Point X="-3.382343261719" Y="24.762029296875" />
                  <Point X="-3.373703369141" Y="24.77000390625" />
                  <Point X="-3.357477294922" Y="24.78695703125" />
                  <Point X="-3.343471191406" Y="24.80578125" />
                  <Point X="-3.331895507812" Y="24.826193359375" />
                  <Point X="-3.326739746094" Y="24.836759765625" />
                  <Point X="-3.315302978516" Y="24.864421875" />
                  <Point X="-3.312681152344" Y="24.871560546875" />
                  <Point X="-3.305974365234" Y="24.89334765625" />
                  <Point X="-3.298487304688" Y="24.92505078125" />
                  <Point X="-3.296610107422" Y="24.93565625" />
                  <Point X="-3.294068115234" Y="24.95701171875" />
                  <Point X="-3.293959716797" Y="24.978517578125" />
                  <Point X="-3.296286621094" Y="24.9998984375" />
                  <Point X="-3.298057128906" Y="25.0105234375" />
                  <Point X="-3.304314941406" Y="25.038267578125" />
                  <Point X="-3.30626953125" Y="25.045568359375" />
                  <Point X="-3.313280761719" Y="25.067107421875" />
                  <Point X="-3.325947753906" Y="25.098732421875" />
                  <Point X="-3.330985839844" Y="25.10935546875" />
                  <Point X="-3.342335693359" Y="25.129896484375" />
                  <Point X="-3.356133789062" Y="25.148875" />
                  <Point X="-3.372172607422" Y="25.1660078125" />
                  <Point X="-3.380725097656" Y="25.174080078125" />
                  <Point X="-3.404572509766" Y="25.193943359375" />
                  <Point X="-3.410964355469" Y="25.19882421875" />
                  <Point X="-3.430901123047" Y="25.212337890625" />
                  <Point X="-3.462191894531" Y="25.23047265625" />
                  <Point X="-3.473396728516" Y="25.236015625" />
                  <Point X="-3.496444824219" Y="25.2455859375" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.348736328125" Y="25.474810546875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.778820800781" Y="25.63659765625" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.667247070312" Y="26.194017578125" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.239212402344" Y="26.26631640625" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057373047" Y="26.204365234375" />
                  <Point X="-3.736704345703" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704887695313" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.674574951172" Y="26.21466015625" />
                  <Point X="-3.647942382812" Y="26.223056640625" />
                  <Point X="-3.613148925781" Y="26.23402734375" />
                  <Point X="-3.603457519531" Y="26.237673828125" />
                  <Point X="-3.584527587891" Y="26.246001953125" />
                  <Point X="-3.5752890625" Y="26.25068359375" />
                  <Point X="-3.556544921875" Y="26.26150390625" />
                  <Point X="-3.547869873047" Y="26.2671640625" />
                  <Point X="-3.531184814453" Y="26.279396484375" />
                  <Point X="-3.5159296875" Y="26.293373046875" />
                  <Point X="-3.502288574219" Y="26.308927734375" />
                  <Point X="-3.495892578125" Y="26.317078125" />
                  <Point X="-3.483478271484" Y="26.33480859375" />
                  <Point X="-3.478009033203" Y="26.34360546875" />
                  <Point X="-3.468061523438" Y="26.36173828125" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.452896728516" Y="26.396873046875" />
                  <Point X="-3.438935791016" Y="26.430578125" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.447784423828" Y="26.63324609375" />
                  <Point X="-3.460678710938" Y="26.658015625" />
                  <Point X="-3.477524169922" Y="26.690375" />
                  <Point X="-3.482802246094" Y="26.6992890625" />
                  <Point X="-3.494293945312" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.026387207031" Y="27.139875" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.186818359375" Y="27.36417578125" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.832540039062" Y="27.89850390625" />
                  <Point X="-3.726337646484" Y="28.03501171875" />
                  <Point X="-3.537379882812" Y="27.92591796875" />
                  <Point X="-3.254156982422" Y="27.7623984375" />
                  <Point X="-3.244926025391" Y="27.757720703125" />
                  <Point X="-3.225999267578" Y="27.749390625" />
                  <Point X="-3.216303466797" Y="27.745740234375" />
                  <Point X="-3.195662109375" Y="27.73923046875" />
                  <Point X="-3.185622558594" Y="27.736658203125" />
                  <Point X="-3.165326660156" Y="27.73262109375" />
                  <Point X="-3.1550703125" Y="27.73115625" />
                  <Point X="-3.117978515625" Y="27.727912109375" />
                  <Point X="-3.069520996094" Y="27.723671875" />
                  <Point X="-3.059171630859" Y="27.723333984375" />
                  <Point X="-3.038489746094" Y="27.72378515625" />
                  <Point X="-3.028157226562" Y="27.72457421875" />
                  <Point X="-3.006698242188" Y="27.727400390625" />
                  <Point X="-2.996521240234" Y="27.729310546875" />
                  <Point X="-2.976432373047" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902749755859" Y="27.77319140625" />
                  <Point X="-2.886616943359" Y="27.786138671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.852574707031" Y="27.819380859375" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752042480469" Y="28.081490234375" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.001148193359" Y="28.59904296875" />
                  <Point X="-3.059388183594" Y="28.699916015625" />
                  <Point X="-2.969752197266" Y="28.768640625" />
                  <Point X="-2.648384521484" Y="29.015029296875" />
                  <Point X="-2.381024658203" Y="29.163568359375" />
                  <Point X="-2.192525146484" Y="29.268294921875" />
                  <Point X="-2.118564697266" Y="29.17191015625" />
                  <Point X="-2.111818359375" Y="29.164046875" />
                  <Point X="-2.097514404297" Y="29.14910546875" />
                  <Point X="-2.089956298828" Y="29.14202734375" />
                  <Point X="-2.073376464844" Y="29.128115234375" />
                  <Point X="-2.065091796875" Y="29.121900390625" />
                  <Point X="-2.047895751953" Y="29.11041015625" />
                  <Point X="-2.038984619141" Y="29.105134765625" />
                  <Point X="-1.997701782227" Y="29.083642578125" />
                  <Point X="-1.943768676758" Y="29.05556640625" />
                  <Point X="-1.934342163086" Y="29.05129296875" />
                  <Point X="-1.915067871094" Y="29.043794921875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313964844" Y="29.034966796875" />
                  <Point X="-1.874174804688" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.8330546875" Y="29.028783203125" />
                  <Point X="-1.812407714844" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.7808046875" Y="29.03513671875" />
                  <Point X="-1.770716064453" Y="29.037490234375" />
                  <Point X="-1.750860595703" Y="29.043279296875" />
                  <Point X="-1.74109375" Y="29.04671484375" />
                  <Point X="-1.698094726562" Y="29.06452734375" />
                  <Point X="-1.641919677734" Y="29.087794921875" />
                  <Point X="-1.632579589844" Y="29.092275390625" />
                  <Point X="-1.614450195312" Y="29.10222265625" />
                  <Point X="-1.605660766602" Y="29.107689453125" />
                  <Point X="-1.587930664062" Y="29.120103515625" />
                  <Point X="-1.579781860352" Y="29.126498046875" />
                  <Point X="-1.564229248047" Y="29.14013671875" />
                  <Point X="-1.550253662109" Y="29.15538671875" />
                  <Point X="-1.538021850586" Y="29.172068359375" />
                  <Point X="-1.532361450195" Y="29.180744140625" />
                  <Point X="-1.521539306641" Y="29.19948828125" />
                  <Point X="-1.516857910156" Y="29.208724609375" />
                  <Point X="-1.508527099609" Y="29.22765625" />
                  <Point X="-1.504877563477" Y="29.2373515625" />
                  <Point X="-1.490882080078" Y="29.28173828125" />
                  <Point X="-1.472598388672" Y="29.339728515625" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.347219238281" Y="29.59967578125" />
                  <Point X="-0.931163818359" Y="29.7163203125" />
                  <Point X="-0.607076538086" Y="29.754251953125" />
                  <Point X="-0.365222381592" Y="29.78255859375" />
                  <Point X="-0.309451599121" Y="29.57441796875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.347437683105" Y="29.670234375" />
                  <Point X="0.378190704346" Y="29.785005859375" />
                  <Point X="0.464597137451" Y="29.77595703125" />
                  <Point X="0.827880065918" Y="29.737912109375" />
                  <Point X="1.096021728516" Y="29.673173828125" />
                  <Point X="1.453592041016" Y="29.586845703125" />
                  <Point X="1.626322998047" Y="29.5241953125" />
                  <Point X="1.858248657227" Y="29.44007421875" />
                  <Point X="2.027031005859" Y="29.361140625" />
                  <Point X="2.250436035156" Y="29.25666015625" />
                  <Point X="2.413550537109" Y="29.161630859375" />
                  <Point X="2.629442626953" Y="29.0358515625" />
                  <Point X="2.783208251953" Y="28.926501953125" />
                  <Point X="2.817780273438" Y="28.901916015625" />
                  <Point X="2.438885253906" Y="28.245650390625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301269531" Y="27.540595703125" />
                  <Point X="2.031992675781" Y="27.50578515625" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.017040893555" Y="27.339478515625" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045320922852" Y="27.2238828125" />
                  <Point X="2.055682373047" Y="27.203841796875" />
                  <Point X="2.061459228516" Y="27.1941328125" />
                  <Point X="2.080084472656" Y="27.16668359375" />
                  <Point X="2.104416992188" Y="27.13082421875" />
                  <Point X="2.109807373047" Y="27.12363671875" />
                  <Point X="2.130462158203" Y="27.100125" />
                  <Point X="2.157595703125" Y="27.075388671875" />
                  <Point X="2.168257080078" Y="27.066982421875" />
                  <Point X="2.195705810547" Y="27.048357421875" />
                  <Point X="2.231565673828" Y="27.0240234375" />
                  <Point X="2.241275390625" Y="27.01824609375" />
                  <Point X="2.261315429688" Y="27.007884765625" />
                  <Point X="2.271645751953" Y="27.00330078125" />
                  <Point X="2.293734863281" Y="26.995033203125" />
                  <Point X="2.304544677734" Y="26.99170703125" />
                  <Point X="2.326474609375" Y="26.98636328125" />
                  <Point X="2.337594726562" Y="26.984345703125" />
                  <Point X="2.3676953125" Y="26.980716796875" />
                  <Point X="2.40701953125" Y="26.975974609375" />
                  <Point X="2.416040527344" Y="26.9753203125" />
                  <Point X="2.447577392578" Y="26.97549609375" />
                  <Point X="2.484317138672" Y="26.979822265625" />
                  <Point X="2.497749511719" Y="26.98239453125" />
                  <Point X="2.532559326172" Y="26.991703125" />
                  <Point X="2.578035888672" Y="27.003865234375" />
                  <Point X="2.583999755859" Y="27.005671875" />
                  <Point X="2.604419189453" Y="27.013072265625" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.48136328125" Y="27.51592578125" />
                  <Point X="3.940403320312" Y="27.780953125" />
                  <Point X="4.043959716797" Y="27.63703125" />
                  <Point X="4.129680664062" Y="27.495376953125" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="3.664800292969" Y="27.12123046875" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168130859375" Y="26.73986328125" />
                  <Point X="3.152114746094" Y="26.7252109375" />
                  <Point X="3.134668212891" Y="26.7066015625" />
                  <Point X="3.128576171875" Y="26.699421875" />
                  <Point X="3.1035234375" Y="26.66673828125" />
                  <Point X="3.070793945312" Y="26.624041015625" />
                  <Point X="3.065635742188" Y="26.6166015625" />
                  <Point X="3.049740966797" Y="26.589375" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.020808105469" Y="26.509302734375" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.012360107422" Y="26.315443359375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034684814453" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.19537109375" />
                  <Point X="3.056919433594" Y="26.183525390625" />
                  <Point X="3.077755859375" Y="26.15185546875" />
                  <Point X="3.104977050781" Y="26.11048046875" />
                  <Point X="3.11173828125" Y="26.10142578125" />
                  <Point X="3.126287353516" Y="26.084185546875" />
                  <Point X="3.134075195312" Y="26.076" />
                  <Point X="3.151319335938" Y="26.059908203125" />
                  <Point X="3.160028076172" Y="26.052701171875" />
                  <Point X="3.178239501953" Y="26.039373046875" />
                  <Point X="3.1877421875" Y="26.033251953125" />
                  <Point X="3.217936767578" Y="26.01625390625" />
                  <Point X="3.257384277344" Y="25.994048828125" />
                  <Point X="3.265474609375" Y="25.98998828125" />
                  <Point X="3.294693115234" Y="25.978076171875" />
                  <Point X="3.330284667969" Y="25.968017578125" />
                  <Point X="3.343676025391" Y="25.965255859375" />
                  <Point X="3.384501220703" Y="25.959861328125" />
                  <Point X="3.437836669922" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465708251953" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.303609863281" Y="26.058513671875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.77969921875" Y="25.740716796875" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.255661132813" Y="25.572388671875" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686022949219" Y="25.419541015625" />
                  <Point X="3.665625244141" Y="25.41213671875" />
                  <Point X="3.642384765625" Y="25.40162109375" />
                  <Point X="3.634006591797" Y="25.397318359375" />
                  <Point X="3.593896972656" Y="25.374134765625" />
                  <Point X="3.541496582031" Y="25.34384765625" />
                  <Point X="3.533882080078" Y="25.338947265625" />
                  <Point X="3.508778076172" Y="25.319875" />
                  <Point X="3.481993652344" Y="25.2943515625" />
                  <Point X="3.472795654297" Y="25.284224609375" />
                  <Point X="3.448729980469" Y="25.25355859375" />
                  <Point X="3.417289794922" Y="25.21349609375" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130615234" Y="25.1849296875" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.362354736328" Y="25.0685859375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.359896728516" Y="24.881689453125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.816013671875" />
                  <Point X="3.380004394531" Y="24.794515625" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417289794922" Y="24.723943359375" />
                  <Point X="3.44135546875" Y="24.69327734375" />
                  <Point X="3.472795654297" Y="24.65321484375" />
                  <Point X="3.478713867188" Y="24.64637109375" />
                  <Point X="3.501138916016" Y="24.624193359375" />
                  <Point X="3.53017578125" Y="24.6012734375" />
                  <Point X="3.541495361328" Y="24.593591796875" />
                  <Point X="3.581604980469" Y="24.570408203125" />
                  <Point X="3.634005371094" Y="24.540119140625" />
                  <Point X="3.6395" Y="24.5371796875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.428386230469" Y="24.31876953125" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.090479736328" Y="24.004685546875" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720947266" Y="24.089158203125" />
                  <Point X="3.354482910156" Y="24.087322265625" />
                  <Point X="3.275761962891" Y="24.070212890625" />
                  <Point X="3.172918701172" Y="24.047859375" />
                  <Point X="3.157875244141" Y="24.0432578125" />
                  <Point X="3.128753173828" Y="24.0316328125" />
                  <Point X="3.114674560547" Y="24.024609375" />
                  <Point X="3.086848144531" Y="24.007716796875" />
                  <Point X="3.074122802734" Y="23.99846875" />
                  <Point X="3.050373291016" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.991767089844" Y="23.909548828125" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.868337646484" Y="23.629228515625" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158935547" Y="23.394517578125" />
                  <Point X="2.896541259766" Y="23.380626953125" />
                  <Point X="2.940106689453" Y="23.312865234375" />
                  <Point X="2.997021484375" Y="23.224337890625" />
                  <Point X="3.001740966797" Y="23.2176484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.736173095703" Y="22.639345703125" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.045498535156" Y="22.302583984375" />
                  <Point X="4.001273193359" Y="22.23974609375" />
                  <Point X="3.430660644531" Y="22.569189453125" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815025878906" Y="22.920484375" />
                  <Point X="2.783120605469" Y="22.930671875" />
                  <Point X="2.771107421875" Y="22.933662109375" />
                  <Point X="2.677416992188" Y="22.95058203125" />
                  <Point X="2.555017089844" Y="22.9726875" />
                  <Point X="2.539358886719" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444846923828" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.40058984375" Y="22.948890625" />
                  <Point X="2.322755859375" Y="22.907927734375" />
                  <Point X="2.221071777344" Y="22.854412109375" />
                  <Point X="2.208968994141" Y="22.846830078125" />
                  <Point X="2.186038818359" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.079500244141" Y="22.675970703125" />
                  <Point X="2.025984619141" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.019108276367" Y="22.32616796875" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.508685302734" Y="21.365810546875" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.9639140625" />
                  <Point X="2.278191894531" Y="21.54458203125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828654663086" Y="22.1298515625" />
                  <Point X="1.808837280273" Y="22.1503671875" />
                  <Point X="1.783253540039" Y="22.171994140625" />
                  <Point X="1.773297607422" Y="22.179353515625" />
                  <Point X="1.680893310547" Y="22.238759765625" />
                  <Point X="1.560174316406" Y="22.31637109375" />
                  <Point X="1.546284667969" Y="22.32375390625" />
                  <Point X="1.517470947266" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384765625" Y="22.3513046875" />
                  <Point X="1.424119995117" Y="22.35362109375" />
                  <Point X="1.408396240234" Y="22.35348046875" />
                  <Point X="1.307336425781" Y="22.344181640625" />
                  <Point X="1.175309204102" Y="22.332033203125" />
                  <Point X="1.161226196289" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120007324219" Y="22.318369140625" />
                  <Point X="1.092621582031" Y="22.307025390625" />
                  <Point X="1.079875488281" Y="22.300583984375" />
                  <Point X="1.055493652344" Y="22.28586328125" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.96582208252" Y="22.21269921875" />
                  <Point X="0.863874206543" Y="22.127931640625" />
                  <Point X="0.85265435791" Y="22.11691015625" />
                  <Point X="0.832184082031" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.759459594727" Y="21.887021484375" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.847658203125" />
                  <Point X="0.828745361328" Y="20.86387890625" />
                  <Point X="0.655064819336" Y="21.512064453125" />
                  <Point X="0.652606018066" Y="21.519876953125" />
                  <Point X="0.642146972656" Y="21.546416015625" />
                  <Point X="0.626788391113" Y="21.57618359375" />
                  <Point X="0.620407409668" Y="21.58679296875" />
                  <Point X="0.549419921875" Y="21.689072265625" />
                  <Point X="0.4566796875" Y="21.822693359375" />
                  <Point X="0.446669189453" Y="21.834830078125" />
                  <Point X="0.424782684326" Y="21.857287109375" />
                  <Point X="0.412906524658" Y="21.867607421875" />
                  <Point X="0.386651550293" Y="21.886849609375" />
                  <Point X="0.37323550415" Y="21.89506640625" />
                  <Point X="0.345236297607" Y="21.909171875" />
                  <Point X="0.330652954102" Y="21.915060546875" />
                  <Point X="0.220803848267" Y="21.94915234375" />
                  <Point X="0.077293876648" Y="21.993693359375" />
                  <Point X="0.063380096436" Y="21.996888671875" />
                  <Point X="0.035227794647" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022895944595" Y="22.001162109375" />
                  <Point X="-0.051062213898" Y="21.996892578125" />
                  <Point X="-0.064984313965" Y="21.9936953125" />
                  <Point X="-0.174833435059" Y="21.9596015625" />
                  <Point X="-0.318343414307" Y="21.9150625" />
                  <Point X="-0.33292791748" Y="21.909171875" />
                  <Point X="-0.360928771973" Y="21.895064453125" />
                  <Point X="-0.374345275879" Y="21.88684765625" />
                  <Point X="-0.40060067749" Y="21.867603515625" />
                  <Point X="-0.412477600098" Y="21.85728125" />
                  <Point X="-0.434361572266" Y="21.83482421875" />
                  <Point X="-0.444368804932" Y="21.822689453125" />
                  <Point X="-0.515356323242" Y="21.720408203125" />
                  <Point X="-0.608096374512" Y="21.5867890625" />
                  <Point X="-0.61247052002" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.869653808594" Y="20.6652578125" />
                  <Point X="-0.985425231934" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121061018339" Y="22.602821175172" />
                  <Point X="-3.727894709244" Y="22.019928151174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.045551571919" Y="22.660761523903" />
                  <Point X="-3.645421437881" Y="22.067544204853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.673116836659" Y="23.761052997348" />
                  <Point X="-4.642543427531" Y="23.715726054301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.970042125499" Y="22.718701872635" />
                  <Point X="-3.562948166519" Y="22.115160258532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.954815168014" Y="21.213566011285" />
                  <Point X="-2.863357939703" Y="21.077975094303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741732958546" Y="24.032668288216" />
                  <Point X="-4.537298451726" Y="23.729581667788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894532679079" Y="22.776642221366" />
                  <Point X="-3.480474895156" Y="22.162776312212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.901966662289" Y="21.3051025862" />
                  <Point X="-2.646373599538" Y="20.926170287542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.772569091658" Y="24.248272442335" />
                  <Point X="-4.432053475922" Y="23.743437281275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.819023232659" Y="22.834582570097" />
                  <Point X="-3.398001617244" Y="22.210392356182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849118156564" Y="21.396639161115" />
                  <Point X="-2.486589952098" Y="20.859168995189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.733095842968" Y="24.359638651274" />
                  <Point X="-4.326808500117" Y="23.757292894762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.743513786239" Y="22.892522918828" />
                  <Point X="-3.315528317507" Y="22.258008367795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.796269650839" Y="21.48817573603" />
                  <Point X="-2.425606177885" Y="20.938644538576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.636045523967" Y="24.385643343089" />
                  <Point X="-4.221563524312" Y="23.77114850825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.66800433982" Y="22.950463267559" />
                  <Point X="-3.23305501777" Y="22.305624379408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.743421145114" Y="21.579712310945" />
                  <Point X="-2.358075985348" Y="21.008414617671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.538995204966" Y="24.411648034904" />
                  <Point X="-4.116318557597" Y="23.785004135213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.592494901036" Y="23.008403627612" />
                  <Point X="-3.150581718034" Y="22.353240391021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.690572639389" Y="21.67124888586" />
                  <Point X="-2.279218150764" Y="21.061390776802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.441944885965" Y="24.437652726719" />
                  <Point X="-4.01107360092" Y="23.798859777058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.516985482934" Y="23.066344018326" />
                  <Point X="-3.068108418297" Y="22.400856402634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637724111654" Y="21.762785428145" />
                  <Point X="-2.200227924132" Y="21.114170656652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.344894566964" Y="24.463657418533" />
                  <Point X="-3.905828644243" Y="23.812715418903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441476064833" Y="23.124284409041" />
                  <Point X="-2.98563511856" Y="22.448472414248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.584875562085" Y="21.854321938059" />
                  <Point X="-2.121237184618" Y="21.166949776122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.247844247963" Y="24.489662110348" />
                  <Point X="-3.800583687566" Y="23.826571060748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.365966646731" Y="23.182224799756" />
                  <Point X="-2.903161818824" Y="22.496088425861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.532027012516" Y="21.945858447972" />
                  <Point X="-2.036048360114" Y="21.210539856705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150793928631" Y="24.515666801671" />
                  <Point X="-3.695338730889" Y="23.840426702593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29045722863" Y="23.24016519047" />
                  <Point X="-2.820688519087" Y="22.543704437474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.479178462947" Y="22.037394957886" />
                  <Point X="-1.92988811336" Y="21.223038525207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781242373435" Y="25.620232765345" />
                  <Point X="-4.756927709477" Y="25.584184793598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053743569947" Y="24.541671434654" />
                  <Point X="-3.590093774212" Y="23.854282344438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.214947810528" Y="23.298105581185" />
                  <Point X="-2.73821521935" Y="22.591320449087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.426329913378" Y="22.1289314678" />
                  <Point X="-1.809996861926" Y="21.215180142113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760626198714" Y="25.759555736131" />
                  <Point X="-4.617057830201" Y="25.54670687666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.956693211264" Y="24.567676067638" />
                  <Point X="-3.484848817534" Y="23.868137986283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.139438392426" Y="23.3560459719" />
                  <Point X="-2.653126800557" Y="22.63505938726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.373481363809" Y="22.220467977713" />
                  <Point X="-1.69010615575" Y="21.207322567396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.127484359426" Y="20.373201452132" />
                  <Point X="-1.040223709498" Y="20.243832218462" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740010018964" Y="25.89887869946" />
                  <Point X="-4.477187950925" Y="25.509228959722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.859642852581" Y="24.593680700621" />
                  <Point X="-3.379603860857" Y="23.881993628128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.063928974325" Y="23.413986362615" />
                  <Point X="-2.549828010594" Y="22.651800339913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.324422613358" Y="22.317623095878" />
                  <Point X="-1.558696989804" Y="21.182388173807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133102717574" Y="20.551418717376" />
                  <Point X="-0.966385863935" Y="20.304250817378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71231442661" Y="26.027706001985" />
                  <Point X="-4.337318066455" Y="25.471751035085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762592493898" Y="24.619685333605" />
                  <Point X="-3.27435890418" Y="23.895849269973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.996981765438" Y="23.484620750516" />
                  <Point X="-2.361326276358" Y="22.542222732985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.34697286479" Y="22.520942925229" />
                  <Point X="-1.309367924664" Y="20.982630340263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181028864669" Y="20.792359859178" />
                  <Point X="-0.9338065712" Y="20.425837736335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.679472240441" Y="26.148903165396" />
                  <Point X="-4.197448123559" Y="25.434273023825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.665542135215" Y="24.645689966588" />
                  <Point X="-3.157672480388" Y="23.89274223925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.953498192049" Y="23.590041408585" />
                  <Point X="-0.901227278466" Y="20.547424655292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.646629987571" Y="26.270100229921" />
                  <Point X="-4.057578180662" Y="25.396795012566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568491776532" Y="24.671694599572" />
                  <Point X="-0.868647990691" Y="20.669011581603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.557775513572" Y="26.30825576164" />
                  <Point X="-3.917708237766" Y="25.359317001307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47379203134" Y="24.701184160369" />
                  <Point X="-0.836068858606" Y="20.790598738733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.432017475704" Y="26.291699509967" />
                  <Point X="-3.777838294869" Y="25.321838990048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393895811347" Y="24.752620849823" />
                  <Point X="-0.803489726521" Y="20.912185895863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.306259437835" Y="26.275143258293" />
                  <Point X="-3.637968351973" Y="25.284360978789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.330650895612" Y="24.828744113046" />
                  <Point X="-0.770910594436" Y="21.033773052992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.180501373613" Y="26.25858696755" />
                  <Point X="-3.49745533003" Y="25.245929563636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295344578717" Y="24.946288052423" />
                  <Point X="-0.738331462351" Y="21.155360210122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054743279296" Y="26.242030632188" />
                  <Point X="-0.705752330266" Y="21.276947367252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928985184979" Y="26.225474296826" />
                  <Point X="-0.673173198181" Y="21.398534524382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803227090662" Y="26.208917961464" />
                  <Point X="-0.640212212267" Y="21.51955555993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.69004566342" Y="26.211007301822" />
                  <Point X="-0.589268503277" Y="21.613916112138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.595733757024" Y="26.241071857281" />
                  <Point X="-0.531154516315" Y="21.697646290091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210880073995" Y="27.322951483493" />
                  <Point X="-4.15284233169" Y="27.236906992051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.516233129626" Y="26.293095036875" />
                  <Point X="-0.473041138782" Y="21.781377371559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.157720662633" Y="27.414027121846" />
                  <Point X="-3.915315380343" Y="27.054646511762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.460028488599" Y="26.379655936587" />
                  <Point X="-0.410693888517" Y="21.858831478569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.104560999807" Y="27.505102387388" />
                  <Point X="-3.677787692608" Y="26.872384939732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423199393379" Y="26.494942264255" />
                  <Point X="-0.33067262268" Y="21.910082779936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.051401336981" Y="27.59617765293" />
                  <Point X="-0.236560902964" Y="21.940444124352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997614908269" Y="27.686323699834" />
                  <Point X="-0.141806189932" Y="21.969852191976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.936237481811" Y="27.765215629767" />
                  <Point X="-0.045974630978" Y="21.997663769866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.874860055353" Y="27.8441075597" />
                  <Point X="0.07019443574" Y="21.995323752548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825701633456" Y="20.875238269785" />
                  <Point X="0.830372108567" Y="20.868314005681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813482614645" Y="27.922999468506" />
                  <Point X="0.214637179471" Y="21.951066285256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.750179043797" Y="21.157092820201" />
                  <Point X="0.802581831832" Y="21.07940249202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.752105142292" Y="28.001891330396" />
                  <Point X="0.363790991035" Y="21.899824372673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.674656454139" Y="21.438947370617" />
                  <Point X="0.774791555097" Y="21.290490978359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.617451592873" Y="27.972146940504" />
                  <Point X="0.747001278362" Y="21.501579464698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.429774263893" Y="27.863791564631" />
                  <Point X="0.724727569952" Y="21.704489302157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.243093990725" Y="27.756914384788" />
                  <Point X="0.748916544301" Y="21.838515379666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.108374130983" Y="27.727071685798" />
                  <Point X="0.776843432675" Y="21.966999771739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9954674901" Y="27.729568413686" />
                  <Point X="0.819137960744" Y="22.074183261988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.907840224927" Y="27.76954335731" />
                  <Point X="0.88540153718" Y="22.145831176678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837221150221" Y="27.834733980265" />
                  <Point X="0.958817582598" Y="22.206875120026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775602980638" Y="27.913268993839" />
                  <Point X="1.032233787022" Y="22.267918827638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.74876355288" Y="28.043365612575" />
                  <Point X="1.11437028916" Y="22.316034162226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95459632119" Y="28.518412947659" />
                  <Point X="1.215664845761" Y="22.335746513034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.995480763471" Y="28.748914332752" />
                  <Point X="1.323559144671" Y="22.345674343491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.919950126136" Y="28.806823264659" />
                  <Point X="1.433245752601" Y="22.352944966552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.844419259931" Y="28.864731857253" />
                  <Point X="1.581947915238" Y="22.30237265104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.768888393726" Y="28.922640449847" />
                  <Point X="1.786107109726" Y="22.169581904678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.693357527522" Y="28.980549042441" />
                  <Point X="2.642549842106" Y="21.069741044632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.614661475773" Y="29.033765054491" />
                  <Point X="2.218968966575" Y="21.86761322445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.531307116546" Y="29.080074841692" />
                  <Point X="2.017224347122" Y="22.336599629606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.447952757319" Y="29.126384628894" />
                  <Point X="2.007735195231" Y="22.520555582571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.36459842027" Y="29.172694448975" />
                  <Point X="2.052325126975" Y="22.624335996925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.281244173581" Y="29.219004403022" />
                  <Point X="2.10254852847" Y="22.719764448911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.197889926892" Y="29.265314357069" />
                  <Point X="2.159621670628" Y="22.805037742744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.940965180311" Y="29.05429546269" />
                  <Point X="2.235711870757" Y="22.862117088694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.810254298151" Y="29.030396317388" />
                  <Point X="2.32028133461" Y="22.906625409205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.714188404511" Y="29.057860479818" />
                  <Point X="2.405078238817" Y="22.950796535524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.625449593316" Y="29.096187488696" />
                  <Point X="2.503814480854" Y="22.97430174365" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.550562190925" Y="29.155050055625" />
                  <Point X="2.628437868929" Y="22.959427679472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.500611194528" Y="29.250882364775" />
                  <Point X="2.758923523861" Y="22.935862447267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.466084460661" Y="29.369582083521" />
                  <Point X="2.923231329649" Y="22.862153814332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.478670219606" Y="29.558128945239" />
                  <Point X="3.110908821157" Y="22.7537981975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.384965104228" Y="29.589093105377" />
                  <Point X="3.298586312666" Y="22.645442580668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.288598010721" Y="29.616110720642" />
                  <Point X="2.868030211379" Y="23.453655957938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.075178896029" Y="23.146545403397" />
                  <Point X="3.486263820569" Y="22.537086939531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.192230738864" Y="29.643128071493" />
                  <Point X="2.867840418062" Y="23.62382504485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312706858934" Y="22.964283423412" />
                  <Point X="3.673941367411" Y="22.428731240663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.095863467008" Y="29.670145422344" />
                  <Point X="2.889839241787" Y="23.761098154189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550234821839" Y="22.782021443426" />
                  <Point X="3.861618914254" Y="22.320375541795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.999496195152" Y="29.697162773195" />
                  <Point X="2.943474680664" Y="23.851468052728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787762309675" Y="22.599760167759" />
                  <Point X="4.016245605382" Y="22.261019751586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.900266607534" Y="29.719936566419" />
                  <Point X="3.006741649091" Y="23.927558621489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.02528808528" Y="22.417501430577" />
                  <Point X="4.072896446112" Y="22.346919133033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794060398864" Y="29.732367093578" />
                  <Point X="3.073730783549" Y="23.998130852175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.687854190194" Y="29.744797620737" />
                  <Point X="3.157881648267" Y="24.043259771424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.58164796068" Y="29.757228116994" />
                  <Point X="3.257037412327" Y="24.066143012574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.475441664953" Y="29.769658515086" />
                  <Point X="-0.286846607621" Y="29.490054844231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.014260448886" Y="29.085929244734" />
                  <Point X="3.357079758252" Y="24.087711842054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.369235369225" Y="29.782088913177" />
                  <Point X="-0.362368793949" Y="29.771908796686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.089774314299" Y="29.101579072215" />
                  <Point X="3.472824414354" Y="24.086001039351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.170785460756" Y="29.151362815211" />
                  <Point X="3.598582475981" Y="24.069444752454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.227944789591" Y="29.236508332041" />
                  <Point X="3.724340537608" Y="24.052888465557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.262868770589" Y="29.354619107696" />
                  <Point X="3.850098599235" Y="24.03633217866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.295447952953" Y="29.476206190284" />
                  <Point X="3.349775133994" Y="24.947979926604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.61904967742" Y="24.548763998707" />
                  <Point X="3.975856660862" Y="24.019775891763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.328027135318" Y="29.597793272871" />
                  <Point X="3.366923858702" Y="25.09244360344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.76973370357" Y="24.495253449707" />
                  <Point X="4.101614717082" Y="24.003219612883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.360606340401" Y="29.719380321776" />
                  <Point X="2.050180160257" Y="27.214484123037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.116914061912" Y="27.115547045167" />
                  <Point X="3.408613630777" Y="25.200523681323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.909603407441" Y="24.457775792817" />
                  <Point X="4.227372717638" Y="23.986663416528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.434940726182" Y="29.779062769547" />
                  <Point X="2.015726454493" Y="27.435451549171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.31709940723" Y="26.988647772478" />
                  <Point X="3.469543004893" Y="25.280079876169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.049473111312" Y="24.420298135928" />
                  <Point X="4.353130718193" Y="23.970107220173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.558241112777" Y="29.766150135725" />
                  <Point X="2.047468343405" Y="27.55827997035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.440587311548" Y="26.975457132199" />
                  <Point X="3.541235243052" Y="25.343679468877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.189342815183" Y="24.382820479038" />
                  <Point X="4.478888718749" Y="23.953551023819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.681541527634" Y="29.753237460004" />
                  <Point X="2.097387381772" Y="27.65415965923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.542437964033" Y="26.994345036955" />
                  <Point X="3.017647426142" Y="26.289818036564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192516195988" Y="26.030564423779" />
                  <Point X="3.623674306331" Y="25.391346238126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.329212519054" Y="24.345342822148" />
                  <Point X="4.604646719305" Y="23.936994827464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804841942492" Y="29.740324784283" />
                  <Point X="2.150236018899" Y="27.745696039332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.634829429" Y="27.027256763919" />
                  <Point X="3.010076924491" Y="26.470929473571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351890194295" Y="25.96417046124" />
                  <Point X="3.714040001294" Y="25.427261292629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.469082306749" Y="24.307865040986" />
                  <Point X="4.728401717111" Y="23.923408204805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.939186053053" Y="29.711039156361" />
                  <Point X="2.203084656027" Y="27.837232419435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.717340229866" Y="27.074817177821" />
                  <Point X="3.047773075082" Y="26.584930338789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.47501528571" Y="25.951517713211" />
                  <Point X="3.811090354209" Y="25.453265934165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.608952298716" Y="24.270386956976" />
                  <Point X="4.757369646581" Y="24.050349189981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.076067767944" Y="29.677991375309" />
                  <Point X="2.255933293155" Y="27.928768799537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.799813560369" Y="27.122433143821" />
                  <Point X="3.105470389564" Y="26.669278259097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.581554726243" Y="25.963454203818" />
                  <Point X="3.908140707123" Y="25.479270575701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.748822290683" Y="24.232908872967" />
                  <Point X="4.779480700689" Y="24.187455910935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.212949167922" Y="29.644944061133" />
                  <Point X="2.308781930283" Y="28.02030517964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.882286890872" Y="27.170049109822" />
                  <Point X="3.170911426481" Y="26.742145638773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686799689589" Y="25.977309835775" />
                  <Point X="4.005191060038" Y="25.505275217236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.34983051416" Y="29.611896826631" />
                  <Point X="2.36163056741" Y="28.111841559743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.964760221375" Y="27.217665075822" />
                  <Point X="3.246372227087" Y="26.800158107888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792044652936" Y="25.991165467732" />
                  <Point X="4.102241412952" Y="25.531279858772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.490298585019" Y="29.573532054201" />
                  <Point X="2.414479204538" Y="28.203377939845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.047233551878" Y="27.265281041823" />
                  <Point X="3.321881537358" Y="26.858098658469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.897289616283" Y="26.005021099689" />
                  <Point X="4.199291765867" Y="25.557284500308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.642003582925" Y="29.518507852323" />
                  <Point X="2.467327812791" Y="28.294914362757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.129706882381" Y="27.312897007823" />
                  <Point X="3.397390847628" Y="26.91603920905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.002534579629" Y="26.018876731646" />
                  <Point X="4.296342061582" Y="25.583289226646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.79370883518" Y="29.463483273359" />
                  <Point X="2.520176396267" Y="28.386450822402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212180212883" Y="27.360512973824" />
                  <Point X="3.472900157899" Y="26.973979759631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107779542976" Y="26.032732363603" />
                  <Point X="4.393392278038" Y="25.609294070489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.954428503645" Y="29.395094272769" />
                  <Point X="2.573024979742" Y="28.477987282047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.294653543386" Y="27.408128939824" />
                  <Point X="3.54840946817" Y="27.031920310211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.213024506322" Y="26.046587995559" />
                  <Point X="4.490442494495" Y="25.635298914331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.121823444166" Y="29.316808774372" />
                  <Point X="2.625873563218" Y="28.569523741692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.377126873889" Y="27.455744905825" />
                  <Point X="3.62391877844" Y="27.089860860792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.318269422595" Y="26.060443697307" />
                  <Point X="4.587492710952" Y="25.661303758174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294171256823" Y="29.231180341065" />
                  <Point X="2.678722146694" Y="28.661060201337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.459600204392" Y="27.503360871825" />
                  <Point X="3.699428164933" Y="27.147801298369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.423514047985" Y="26.074299830306" />
                  <Point X="4.684542927409" Y="25.687308602017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.482942778474" Y="29.121202757846" />
                  <Point X="2.731570730169" Y="28.752596660982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.542073493775" Y="27.550976898788" />
                  <Point X="3.774937641413" Y="27.205741602534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.528758673374" Y="26.088155963304" />
                  <Point X="4.781593143866" Y="25.713313445859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.678759638319" Y="29.000780031211" />
                  <Point X="2.784419313645" Y="28.844133120627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.624546768418" Y="27.598592947605" />
                  <Point X="3.850447117893" Y="27.263681906699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.634003298764" Y="26.102012096303" />
                  <Point X="4.748179004944" Y="25.932739650769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70702004306" Y="27.646208996422" />
                  <Point X="3.925956594374" Y="27.321622210864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.789493317703" Y="27.693825045239" />
                  <Point X="4.001466070854" Y="27.379562515028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.871966592345" Y="27.741441094056" />
                  <Point X="4.076975547334" Y="27.437502819193" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.645219482422" Y="20.814703125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.393331176758" Y="21.580736328125" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.164486938477" Y="21.76769140625" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.118513565063" Y="21.778140625" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.35926651001" Y="21.612076171875" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.686127929688" Y="20.61608203125" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.931940429688" Y="20.029265625" />
                  <Point X="-1.10023046875" Y="20.061931640625" />
                  <Point X="-1.226992797852" Y="20.094546875" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.330057373047" Y="20.29015625" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.335926147461" Y="20.597173828125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.48741784668" Y="20.886064453125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.783469482422" Y="21.023033203125" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878662109" Y="21.026208984375" />
                  <Point X="-2.101725341797" Y="20.9514765625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.387121337891" Y="20.67669140625" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.609133056641" Y="20.679638671875" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.031341552734" Y="20.9675234375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.828799072266" Y="21.811833984375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.51398046875" Y="22.431236328125" />
                  <Point X="-2.531329101562" Y="22.448583984375" />
                  <Point X="-2.560157958984" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.322036865234" Y="22.034859375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.966796386719" Y="21.896802734375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.287528320312" Y="22.363859375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.728521484375" Y="23.143517578125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.16116015625" Y="23.689361328125" />
                  <Point X="-3.187642822266" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.14153125" Y="23.590044921875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.851163574219" Y="23.690373046875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.960683105469" Y="24.221568359375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.200781738281" Y="24.6989765625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.529863525391" Y="24.88692578125" />
                  <Point X="-3.502324707031" Y="24.909353515625" />
                  <Point X="-3.490887939453" Y="24.937015625" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.489658691406" Y="24.996462890625" />
                  <Point X="-3.502325683594" Y="25.028087890625" />
                  <Point X="-3.526173095703" Y="25.047951171875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.397912109375" Y="25.291283203125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.966773925781" Y="25.66441015625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.8506328125" Y="26.243712890625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.214412597656" Y="26.45469140625" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731704345703" Y="26.3958671875" />
                  <Point X="-3.705071777344" Y="26.404263671875" />
                  <Point X="-3.670278320312" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.628433349609" Y="26.469583984375" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.629210449219" Y="26.57028125" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.142051757812" Y="26.98913671875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.350911132813" Y="27.459953125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.982501464844" Y="28.015171875" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.442379882813" Y="28.090462890625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515625" Y="27.92043359375" />
                  <Point X="-3.101423828125" Y="27.917189453125" />
                  <Point X="-3.052966308594" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.986924804688" Y="27.953732421875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941319335938" Y="28.064931640625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.165693115234" Y="28.50404296875" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.085357177734" Y="28.919421875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.473299316406" Y="29.329658203125" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.054529785156" Y="29.40056640625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246826172" Y="29.273662109375" />
                  <Point X="-1.909963989258" Y="29.252169921875" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124511719" Y="29.218490234375" />
                  <Point X="-1.813809570312" Y="29.22225" />
                  <Point X="-1.770810668945" Y="29.2400625" />
                  <Point X="-1.714635620117" Y="29.263330078125" />
                  <Point X="-1.696905395508" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.672087768555" Y="29.338875" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.676204345703" Y="29.60290234375" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.398511474609" Y="29.78262109375" />
                  <Point X="-0.968083129883" Y="29.903296875" />
                  <Point X="-0.629162963867" Y="29.942962890625" />
                  <Point X="-0.224199966431" Y="29.990359375" />
                  <Point X="-0.125925582886" Y="29.62359375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282125473" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594043732" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.163911636353" Y="29.71941015625" />
                  <Point X="0.236648391724" Y="29.9908671875" />
                  <Point X="0.484387817383" Y="29.964921875" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="1.14061315918" Y="29.857865234375" />
                  <Point X="1.508455566406" Y="29.769056640625" />
                  <Point X="1.691106933594" Y="29.70280859375" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="2.107520263672" Y="29.53325" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.509195068359" Y="29.325802734375" />
                  <Point X="2.732533691406" Y="29.195685546875" />
                  <Point X="2.893321289062" Y="29.081341796875" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.603430175781" Y="28.150650390625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.215543457031" Y="27.456703125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.205674316406" Y="27.362224609375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.237307617188" Y="27.27336328125" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.302387451172" Y="27.205580078125" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360336425781" Y="27.172978515625" />
                  <Point X="2.390437011719" Y="27.169349609375" />
                  <Point X="2.429761230469" Y="27.164607421875" />
                  <Point X="2.448665527344" Y="27.1659453125" />
                  <Point X="2.483475341797" Y="27.17525390625" />
                  <Point X="2.528951904297" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.38636328125" Y="27.68046875" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.070120849609" Y="27.925984375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.292234375" Y="27.593744140625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.780464599609" Y="26.9704921875" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.254318847656" Y="26.5511484375" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.203787109375" Y="26.458130859375" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.198440185547" Y="26.353837890625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.236482910156" Y="26.25628515625" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280948242188" Y="26.198818359375" />
                  <Point X="3.311142822266" Y="26.1818203125" />
                  <Point X="3.350590332031" Y="26.159615234375" />
                  <Point X="3.368565673828" Y="26.153619140625" />
                  <Point X="3.409390869141" Y="26.148224609375" />
                  <Point X="3.462726318359" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.278810058594" Y="26.246888671875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.88229296875" Y="26.18509375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.9674375" Y="25.769947265625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.304836914062" Y="25.388861328125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087646484" Y="25.232818359375" />
                  <Point X="3.688978027344" Y="25.209634765625" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.59819921875" Y="25.13626171875" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.548963134766" Y="25.03284765625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.546505126953" Y="24.917427734375" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.590824707031" Y="24.81057421875" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636576416016" Y="24.758091796875" />
                  <Point X="3.676686035156" Y="24.734908203125" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.477561035156" Y="24.502296875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.980199707031" Y="24.244310546875" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.912240234375" Y="23.87500390625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.065679931641" Y="23.816310546875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.316114990234" Y="23.884548828125" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.13786328125" Y="23.788076171875" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.057538330078" Y="23.611818359375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.099926025391" Y="23.415615234375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.851838134766" Y="22.790083984375" />
                  <Point X="4.339073730469" Y="22.416216796875" />
                  <Point X="4.293682128906" Y="22.342765625" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.12928515625" Y="22.09151171875" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.335660644531" Y="22.404646484375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.643650390625" Y="22.76360546875" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.411243652344" Y="22.739791015625" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.247636474609" Y="22.587482421875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.206083496094" Y="22.35993359375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.673230224609" Y="21.460810546875" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.941558349609" Y="20.8856875" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.751615722656" Y="20.75562109375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.127454589844" Y="21.42891796875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.578145141602" Y="22.078939453125" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425805053711" Y="22.16428125" />
                  <Point X="1.324745361328" Y="22.154982421875" />
                  <Point X="1.192718261719" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.087296875" Y="22.06660546875" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.945124511719" Y="21.846666015625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.038378051758" Y="20.744" />
                  <Point X="1.127642578125" Y="20.065970703125" />
                  <Point X="1.094876220703" Y="20.058787109375" />
                  <Point X="0.994368896484" Y="20.0367578125" />
                  <Point X="0.917038391113" Y="20.022708984375" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#160" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.083785213187" Y="4.667815162065" Z="1.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="-0.641131131529" Y="5.023904450945" Z="1.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.1" />
                  <Point X="-1.4181653662" Y="4.862046048714" Z="1.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.1" />
                  <Point X="-1.731932719004" Y="4.627657504962" Z="1.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.1" />
                  <Point X="-1.725929802209" Y="4.385191551583" Z="1.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.1" />
                  <Point X="-1.796100133699" Y="4.317535462703" Z="1.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.1" />
                  <Point X="-1.893032253184" Y="4.327800627403" Z="1.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.1" />
                  <Point X="-2.021018339089" Y="4.462285101628" Z="1.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.1" />
                  <Point X="-2.503738172256" Y="4.404645864465" Z="1.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.1" />
                  <Point X="-3.121935346634" Y="3.990381513835" Z="1.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.1" />
                  <Point X="-3.215150359384" Y="3.510323407381" Z="1.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.1" />
                  <Point X="-2.997285149194" Y="3.091855263835" Z="1.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.1" />
                  <Point X="-3.028435595926" Y="3.020367971961" Z="1.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.1" />
                  <Point X="-3.103221201772" Y="2.998279549979" Z="1.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.1" />
                  <Point X="-3.423535940916" Y="3.165043589361" Z="1.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.1" />
                  <Point X="-4.028121176939" Y="3.077156474137" Z="1.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.1" />
                  <Point X="-4.400249292481" Y="2.516439632619" Z="1.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.1" />
                  <Point X="-4.178645510727" Y="1.980749545757" Z="1.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.1" />
                  <Point X="-3.679716769102" Y="1.578474172525" Z="1.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.1" />
                  <Point X="-3.680783480825" Y="1.519999428946" Z="1.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.1" />
                  <Point X="-3.726263475507" Y="1.483229751464" Z="1.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.1" />
                  <Point X="-4.214041974711" Y="1.535543563178" Z="1.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.1" />
                  <Point X="-4.905048523348" Y="1.288071814211" Z="1.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.1" />
                  <Point X="-5.022391214307" Y="0.703009803705" Z="1.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.1" />
                  <Point X="-4.417009196047" Y="0.274266521766" Z="1.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.1" />
                  <Point X="-3.560840698209" Y="0.038158428334" Z="1.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.1" />
                  <Point X="-3.543567756891" Y="0.012923425782" Z="1.1" />
                  <Point X="-3.539556741714" Y="0" Z="1.1" />
                  <Point X="-3.544796844244" Y="-0.016883525278" Z="1.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.1" />
                  <Point X="-3.564527896915" Y="-0.0407175548" Z="1.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.1" />
                  <Point X="-4.219878592726" Y="-0.22144553499" Z="1.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.1" />
                  <Point X="-5.016335148551" Y="-0.754229780996" Z="1.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.1" />
                  <Point X="-4.905764212068" Y="-1.290721705598" Z="1.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.1" />
                  <Point X="-4.141160748452" Y="-1.428247132395" Z="1.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.1" />
                  <Point X="-3.204158381999" Y="-1.315691952941" Z="1.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.1" />
                  <Point X="-3.197040356934" Y="-1.33929957843" Z="1.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.1" />
                  <Point X="-3.765115447733" Y="-1.785533286564" Z="1.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.1" />
                  <Point X="-4.336627752738" Y="-2.630469906474" Z="1.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.1" />
                  <Point X="-4.012738268183" Y="-3.102200836232" Z="1.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.1" />
                  <Point X="-3.303192867233" Y="-2.977160678785" Z="1.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.1" />
                  <Point X="-2.563012786996" Y="-2.565317682136" Z="1.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.1" />
                  <Point X="-2.878256334383" Y="-3.131885515259" Z="1.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.1" />
                  <Point X="-3.068001335584" Y="-4.040813183639" Z="1.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.1" />
                  <Point X="-2.641610480335" Y="-4.331593202872" Z="1.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.1" />
                  <Point X="-2.353609625638" Y="-4.322466546257" Z="1.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.1" />
                  <Point X="-2.08010266183" Y="-4.058818030071" Z="1.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.1" />
                  <Point X="-1.792895686009" Y="-3.995578102255" Z="1.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.1" />
                  <Point X="-1.526541098108" Y="-4.120247266256" Z="1.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.1" />
                  <Point X="-1.391121471935" Y="-4.381300202324" Z="1.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.1" />
                  <Point X="-1.385785546148" Y="-4.672036676465" Z="1.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.1" />
                  <Point X="-1.245607536643" Y="-4.922597360672" Z="1.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.1" />
                  <Point X="-0.947623907336" Y="-4.988538066987" Z="1.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="-0.643987696411" Y="-4.365578887507" Z="1.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="-0.324346641434" Y="-3.385152220032" Z="1.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="-0.109849117132" Y="-3.238332480585" Z="1.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.1" />
                  <Point X="0.143509962229" Y="-3.248779564703" Z="1.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.1" />
                  <Point X="0.346099217998" Y="-3.416493535736" Z="1.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.1" />
                  <Point X="0.59076716399" Y="-4.166957142309" Z="1.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.1" />
                  <Point X="0.919819086315" Y="-4.995205841244" Z="1.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.1" />
                  <Point X="1.099425224657" Y="-4.958771252953" Z="1.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.1" />
                  <Point X="1.081794336177" Y="-4.218194185799" Z="1.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.1" />
                  <Point X="0.987827732379" Y="-3.132672820393" Z="1.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.1" />
                  <Point X="1.113108046062" Y="-2.940559569679" Z="1.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.1" />
                  <Point X="1.323170850173" Y="-2.863526186089" Z="1.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.1" />
                  <Point X="1.544949780911" Y="-2.931837960871" Z="1.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.1" />
                  <Point X="2.081630837233" Y="-3.570238132594" Z="1.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.1" />
                  <Point X="2.772628877895" Y="-4.255072887878" Z="1.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.1" />
                  <Point X="2.964464074502" Y="-4.123720328936" Z="1.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.1" />
                  <Point X="2.710375638829" Y="-3.48290891707" Z="1.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.1" />
                  <Point X="2.249131556001" Y="-2.599898702348" Z="1.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.1" />
                  <Point X="2.285726818088" Y="-2.404523966139" Z="1.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.1" />
                  <Point X="2.428374412234" Y="-2.273174491879" Z="1.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.1" />
                  <Point X="2.628608101165" Y="-2.25431645349" Z="1.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.1" />
                  <Point X="3.304504680037" Y="-2.607373861446" Z="1.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.1" />
                  <Point X="4.164017622469" Y="-2.905985564696" Z="1.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.1" />
                  <Point X="4.33005993411" Y="-2.652239733005" Z="1.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.1" />
                  <Point X="3.876120150157" Y="-2.138966964149" Z="1.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.1" />
                  <Point X="3.135827845674" Y="-1.526065519844" Z="1.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.1" />
                  <Point X="3.101171842191" Y="-1.36148261624" Z="1.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.1" />
                  <Point X="3.170153695898" Y="-1.21261035632" Z="1.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.1" />
                  <Point X="3.320578854544" Y="-1.133030760658" Z="1.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.1" />
                  <Point X="4.052997647903" Y="-1.20198136441" Z="1.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.1" />
                  <Point X="4.954831285412" Y="-1.104840104145" Z="1.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.1" />
                  <Point X="5.023485095959" Y="-0.731863745663" Z="1.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.1" />
                  <Point X="4.484345920586" Y="-0.418126596676" Z="1.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.1" />
                  <Point X="3.695552349603" Y="-0.190522295291" Z="1.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.1" />
                  <Point X="3.624002504656" Y="-0.127275991481" Z="1.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.1" />
                  <Point X="3.589456653697" Y="-0.041887512346" Z="1.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.1" />
                  <Point X="3.591914796727" Y="0.05472301887" Z="1.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.1" />
                  <Point X="3.631376933744" Y="0.136672747094" Z="1.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.1" />
                  <Point X="3.707843064749" Y="0.197626548951" Z="1.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.1" />
                  <Point X="4.311621896721" Y="0.371845336093" Z="1.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.1" />
                  <Point X="5.010686173246" Y="0.808919012165" Z="1.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.1" />
                  <Point X="4.924718082841" Y="1.22820116975" Z="1.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.1" />
                  <Point X="4.266127625506" Y="1.327741888977" Z="1.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.1" />
                  <Point X="3.409785997122" Y="1.229073008235" Z="1.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.1" />
                  <Point X="3.32951332376" Y="1.256673989802" Z="1.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.1" />
                  <Point X="3.272097352423" Y="1.315046057863" Z="1.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.1" />
                  <Point X="3.241252780867" Y="1.395221316736" Z="1.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.1" />
                  <Point X="3.245783827009" Y="1.475944138211" Z="1.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.1" />
                  <Point X="3.287845491788" Y="1.552011922185" Z="1.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.1" />
                  <Point X="3.804746905192" Y="1.962103904731" Z="1.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.1" />
                  <Point X="4.328855742611" Y="2.650911344313" Z="1.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.1" />
                  <Point X="4.104550421894" Y="2.986467718283" Z="1.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.1" />
                  <Point X="3.355207257399" Y="2.755049860427" Z="1.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.1" />
                  <Point X="2.464401441457" Y="2.254837764998" Z="1.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.1" />
                  <Point X="2.390267388543" Y="2.250270953064" Z="1.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.1" />
                  <Point X="2.324306887445" Y="2.2782329323" Z="1.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.1" />
                  <Point X="2.272525667935" Y="2.332717972937" Z="1.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.1" />
                  <Point X="2.249158749695" Y="2.39949105529" Z="1.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.1" />
                  <Point X="2.25769017766" Y="2.475068134425" Z="1.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.1" />
                  <Point X="2.640575492432" Y="3.156931824556" Z="1.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.1" />
                  <Point X="2.916142736746" Y="4.153367935734" Z="1.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.1" />
                  <Point X="2.528209082677" Y="4.400285752575" Z="1.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.1" />
                  <Point X="2.122546016308" Y="4.609821055033" Z="1.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.1" />
                  <Point X="1.701999664586" Y="4.781093086162" Z="1.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.1" />
                  <Point X="1.146191586978" Y="4.937750238619" Z="1.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.1" />
                  <Point X="0.483439660544" Y="5.045931855069" Z="1.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.1" />
                  <Point X="0.109459348366" Y="4.763632343886" Z="1.1" />
                  <Point X="0" Y="4.355124473572" Z="1.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>