<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#153" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1476" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.705440429688" Y="20.9570078125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557719848633" Y="21.502861328125" />
                  <Point X="0.542362731934" Y="21.532623046875" />
                  <Point X="0.484267242432" Y="21.616326171875" />
                  <Point X="0.378635223389" Y="21.7685234375" />
                  <Point X="0.356752197266" Y="21.7909765625" />
                  <Point X="0.330497009277" Y="21.810220703125" />
                  <Point X="0.302494262695" Y="21.824330078125" />
                  <Point X="0.21259487915" Y="21.85223046875" />
                  <Point X="0.049135276794" Y="21.902962890625" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036824691772" Y="21.90296484375" />
                  <Point X="-0.126724235535" Y="21.8750625" />
                  <Point X="-0.290183685303" Y="21.82433203125" />
                  <Point X="-0.318183990479" Y="21.810224609375" />
                  <Point X="-0.34443963623" Y="21.79098046875" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.424419219971" Y="21.684818359375" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.809432861328" Y="20.522955078125" />
                  <Point X="-0.916584655762" Y="20.12305859375" />
                  <Point X="-1.079360229492" Y="20.154654296875" />
                  <Point X="-1.180038818359" Y="20.180556640625" />
                  <Point X="-1.24641809082" Y="20.197634765625" />
                  <Point X="-1.232073120117" Y="20.306595703125" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.237985473633" Y="20.59174609375" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.406412963867" Y="20.941380859375" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643028198242" Y="21.109033203125" />
                  <Point X="-1.752879882812" Y="21.116232421875" />
                  <Point X="-1.9526171875" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657104492" Y="21.10519921875" />
                  <Point X="-2.13419140625" Y="21.0440390625" />
                  <Point X="-2.300623535156" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.480147949219" Y="20.711509765625" />
                  <Point X="-2.524526855469" Y="20.73898828125" />
                  <Point X="-2.801713134766" Y="20.910615234375" />
                  <Point X="-2.941144042969" Y="21.01797265625" />
                  <Point X="-3.104721435547" Y="21.143921875" />
                  <Point X="-2.687909667969" Y="21.865861328125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858398438" Y="22.352349609375" />
                  <Point X="-2.406587890625" Y="22.38387890625" />
                  <Point X="-2.405576660156" Y="22.41481640625" />
                  <Point X="-2.414564208984" Y="22.444435546875" />
                  <Point X="-2.428784912109" Y="22.473265625" />
                  <Point X="-2.446808349609" Y="22.4984140625" />
                  <Point X="-2.464151855469" Y="22.5157578125" />
                  <Point X="-2.489310546875" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547757568359" Y="22.55698828125" />
                  <Point X="-2.57869140625" Y="22.555974609375" />
                  <Point X="-2.610218261719" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.471064453125" Y="22.058513671875" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-3.863871337891" Y="21.918431640625" />
                  <Point X="-4.082862304687" Y="22.206142578125" />
                  <Point X="-4.182822265625" Y="22.3737578125" />
                  <Point X="-4.306143066406" Y="22.580548828125" />
                  <Point X="-3.568128662109" Y="23.146845703125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.043347412109" Y="23.64033984375" />
                  <Point X="-3.045555664062" Y="23.672009765625" />
                  <Point X="-3.052555908203" Y="23.70175390625" />
                  <Point X="-3.06863671875" Y="23.72773828125" />
                  <Point X="-3.089468017578" Y="23.7516953125" />
                  <Point X="-3.112973144531" Y="23.771232421875" />
                  <Point X="-3.139459960938" Y="23.7868203125" />
                  <Point X="-3.168722412109" Y="23.79804296875" />
                  <Point X="-3.200607666016" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.282100097656" Y="23.667357421875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.748428710938" Y="23.67203515625" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.860524902344" Y="24.19226171875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.059361816406" Y="24.638517578125" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.511491943359" Y="24.788216796875" />
                  <Point X="-3.486075439453" Y="24.802271484375" />
                  <Point X="-3.47787890625" Y="24.807365234375" />
                  <Point X="-3.459973632812" Y="24.81979296875" />
                  <Point X="-3.436019042969" Y="24.8413203125" />
                  <Point X="-3.414829589844" Y="24.87234375" />
                  <Point X="-3.404121826172" Y="24.897658203125" />
                  <Point X="-3.400885986328" Y="24.9065078125" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.389474365234" Y="24.9544765625" />
                  <Point X="-3.390575195312" Y="24.988927734375" />
                  <Point X="-3.396104736328" Y="25.014326171875" />
                  <Point X="-3.398199951172" Y="25.02227734375" />
                  <Point X="-3.404168457031" Y="25.0415078125" />
                  <Point X="-3.417483398438" Y="25.070830078125" />
                  <Point X="-3.440922363281" Y="25.100576171875" />
                  <Point X="-3.462587890625" Y="25.118927734375" />
                  <Point X="-3.469821289062" Y="25.12448046875" />
                  <Point X="-3.4877265625" Y="25.136908203125" />
                  <Point X="-3.501924560547" Y="25.145046875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-4.49015625" Y="25.4143515625" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.879686523438" Y="25.6039453125" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.771249511719" Y="26.1734375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.143064453125" Y="26.349478515625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985595703" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703141113281" Y="26.305263671875" />
                  <Point X="-3.681345214844" Y="26.312134765625" />
                  <Point X="-3.641715087891" Y="26.324630859375" />
                  <Point X="-3.622775878906" Y="26.332962890625" />
                  <Point X="-3.604032226562" Y="26.34378515625" />
                  <Point X="-3.587353271484" Y="26.356015625" />
                  <Point X="-3.573716064453" Y="26.371564453125" />
                  <Point X="-3.561301757812" Y="26.38929296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.542605957031" Y="26.42854296875" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532049804688" Y="26.589376953125" />
                  <Point X="-3.542602294922" Y="26.6096484375" />
                  <Point X="-3.561789550781" Y="26.646505859375" />
                  <Point X="-3.57328125" Y="26.663705078125" />
                  <Point X="-3.587193359375" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.151234375" Y="27.115927734375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.295634765625" Y="27.366201171875" />
                  <Point X="-4.081156738281" Y="27.73365234375" />
                  <Point X="-3.940129394531" Y="27.91492578125" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.438444335938" Y="27.978494140625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728515625" Y="27.836341796875" />
                  <Point X="-3.167085693359" Y="27.82983203125" />
                  <Point X="-3.146795410156" Y="27.825794921875" />
                  <Point X="-3.116439941406" Y="27.823138671875" />
                  <Point X="-3.06124609375" Y="27.818310546875" />
                  <Point X="-3.040559814453" Y="27.81876171875" />
                  <Point X="-3.019101806641" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.924530761719" Y="27.88177734375" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846091552734" Y="28.066474609375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.1131171875" Y="28.602978515625" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-3.074180175781" Y="28.80828125" />
                  <Point X="-2.700620605469" Y="29.094685546875" />
                  <Point X="-2.47851953125" Y="29.218080078125" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.114152587891" Y="29.32221484375" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028893066406" Y="29.21480078125" />
                  <Point X="-2.012314086914" Y="29.200888671875" />
                  <Point X="-1.995114257812" Y="29.189396484375" />
                  <Point X="-1.961328491211" Y="29.17180859375" />
                  <Point X="-1.899897949219" Y="29.139828125" />
                  <Point X="-1.880625366211" Y="29.13233203125" />
                  <Point X="-1.859718383789" Y="29.126728515625" />
                  <Point X="-1.839268798828" Y="29.123580078125" />
                  <Point X="-1.818622680664" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777453491211" Y="29.134482421875" />
                  <Point X="-1.742263549805" Y="29.14905859375" />
                  <Point X="-1.678279296875" Y="29.1755625" />
                  <Point X="-1.660145141602" Y="29.185509765625" />
                  <Point X="-1.642415771484" Y="29.197923828125" />
                  <Point X="-1.626863647461" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.584026489258" Y="29.302248046875" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584202026367" Y="29.6318984375" />
                  <Point X="-1.433224975586" Y="29.6742265625" />
                  <Point X="-0.949624206543" Y="29.80980859375" />
                  <Point X="-0.68038079834" Y="29.841322265625" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.202472351074" Y="29.54221875" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082114006042" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425956726" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.270890594482" Y="29.751609375" />
                  <Point X="0.307419433594" Y="29.8879375" />
                  <Point X="0.421790893555" Y="29.875958984375" />
                  <Point X="0.844031311035" Y="29.831740234375" />
                  <Point X="1.066807495117" Y="29.777955078125" />
                  <Point X="1.481040283203" Y="29.6779453125" />
                  <Point X="1.625069946289" Y="29.625705078125" />
                  <Point X="1.894648681641" Y="29.52792578125" />
                  <Point X="2.034856445313" Y="29.462357421875" />
                  <Point X="2.294552978516" Y="29.34090625" />
                  <Point X="2.430054443359" Y="29.261962890625" />
                  <Point X="2.680994140625" Y="29.115763671875" />
                  <Point X="2.808727539062" Y="29.024927734375" />
                  <Point X="2.943260742188" Y="28.92925390625" />
                  <Point X="2.453312988281" Y="28.080640625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.125458740234" Y="27.48756640625" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.110698486328" Y="27.356318359375" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070068359" Y="27.24747265625" />
                  <Point X="2.155312744141" Y="27.2250078125" />
                  <Point X="2.183027832031" Y="27.1841640625" />
                  <Point X="2.194462646484" Y="27.17033203125" />
                  <Point X="2.221595458984" Y="27.14559375" />
                  <Point X="2.244059326172" Y="27.130349609375" />
                  <Point X="2.284904052734" Y="27.102634765625" />
                  <Point X="2.304951660156" Y="27.09226953125" />
                  <Point X="2.327038574219" Y="27.08400390625" />
                  <Point X="2.34896875" Y="27.07866015625" />
                  <Point X="2.373602783203" Y="27.07569140625" />
                  <Point X="2.418393554688" Y="27.0702890625" />
                  <Point X="2.436466796875" Y="27.06984375" />
                  <Point X="2.473209472656" Y="27.074171875" />
                  <Point X="2.501697509766" Y="27.081791015625" />
                  <Point X="2.553495849609" Y="27.095642578125" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.11014453125" />
                  <Point X="3.551373779297" Y="27.6660390625" />
                  <Point X="3.967326660156" Y="27.906189453125" />
                  <Point X="3.974432128906" Y="27.896314453125" />
                  <Point X="4.123271972656" Y="27.689462890625" />
                  <Point X="4.194490234375" Y="27.571771484375" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.633308349609" Y="26.9773203125" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221420410156" Y="26.660236328125" />
                  <Point X="3.203973632812" Y="26.641626953125" />
                  <Point X="3.183470703125" Y="26.61487890625" />
                  <Point X="3.14619140625" Y="26.56624609375" />
                  <Point X="3.136607421875" Y="26.5509140625" />
                  <Point X="3.121630126953" Y="26.517087890625" />
                  <Point X="3.113992675781" Y="26.489779296875" />
                  <Point X="3.100106201172" Y="26.440123046875" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.104009033203" Y="26.3413828125" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136283203125" Y="26.235740234375" />
                  <Point X="3.153335449219" Y="26.209822265625" />
                  <Point X="3.184340820312" Y="26.1626953125" />
                  <Point X="3.198892578125" Y="26.145451171875" />
                  <Point X="3.216135986328" Y="26.129361328125" />
                  <Point X="3.234346679688" Y="26.11603515625" />
                  <Point X="3.259057617188" Y="26.102125" />
                  <Point X="3.303988769531" Y="26.07683203125" />
                  <Point X="3.320522216797" Y="26.0695" />
                  <Point X="3.356118652344" Y="26.0594375" />
                  <Point X="3.389529785156" Y="26.055021484375" />
                  <Point X="3.450279296875" Y="26.046994140625" />
                  <Point X="3.462698974609" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.402836914062" Y="26.1673984375" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.782010253906" Y="26.195396484375" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.86837890625" Y="25.78866015625" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.177881835938" Y="25.4531953125" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788085938" Y="25.325583984375" />
                  <Point X="3.681548095703" Y="25.315068359375" />
                  <Point X="3.64872265625" Y="25.296095703125" />
                  <Point X="3.589038085938" Y="25.26159765625" />
                  <Point X="3.574313232422" Y="25.25109765625" />
                  <Point X="3.547529541016" Y="25.225576171875" />
                  <Point X="3.527834472656" Y="25.200478515625" />
                  <Point X="3.492023681641" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.457115478516" Y="25.058322265625" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.451743896484" Y="24.907166015625" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526855469" Y="24.8233359375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492023681641" Y="24.782591796875" />
                  <Point X="3.51171875" Y="24.757494140625" />
                  <Point X="3.547529541016" Y="24.71186328125" />
                  <Point X="3.559998046875" Y="24.698763671875" />
                  <Point X="3.589036865234" Y="24.675841796875" />
                  <Point X="3.621862304688" Y="24.656869140625" />
                  <Point X="3.681546875" Y="24.622369140625" />
                  <Point X="3.692709716797" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.555340332031" Y="24.383103515625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.890715820312" Y="24.28801953125" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.826269042969" Y="23.925271484375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.959362792969" Y="23.926126953125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659667969" Y="23.994490234375" />
                  <Point X="3.310235107422" Y="23.98048828125" />
                  <Point X="3.193095458984" Y="23.95502734375" />
                  <Point X="3.163973144531" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.073456542969" Y="23.859205078125" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.964176513672" Y="23.633982421875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.012104492188" Y="23.376546875" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.889003173828" Y="22.641822265625" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.124814453125" Y="22.250220703125" />
                  <Point X="4.065345947266" Y="22.165724609375" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.277427001953" Y="22.547962890625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.677549072266" Y="22.854021484375" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783691406" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444833984375" Y="22.864822265625" />
                  <Point X="2.381135498047" Y="22.831298828125" />
                  <Point X="2.265315917969" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.171007568359" Y="22.645861328125" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.109522949219" Y="22.36006640625" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.652003173828" Y="21.307576171875" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781858398438" Y="20.88836328125" />
                  <Point X="2.715366210938" Y="20.84532421875" />
                  <Point X="2.701763671875" Y="20.83651953125" />
                  <Point X="2.122138671875" Y="21.591900390625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721923217773" Y="22.099443359375" />
                  <Point X="1.646300415039" Y="22.148060546875" />
                  <Point X="1.508799804688" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417101318359" Y="22.258880859375" />
                  <Point X="1.334394775391" Y="22.251271484375" />
                  <Point X="1.184014282227" Y="22.23743359375" />
                  <Point X="1.156362060547" Y="22.2306015625" />
                  <Point X="1.12897668457" Y="22.2192578125" />
                  <Point X="1.104594970703" Y="22.204537109375" />
                  <Point X="1.040731201172" Y="22.151435546875" />
                  <Point X="0.924611206055" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.856529418945" Y="21.886337890625" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.961490661621" Y="20.6001953125" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="0.975727905273" Y="20.129927734375" />
                  <Point X="0.929315429688" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058460327148" Y="20.24737109375" />
                  <Point X="-1.141246337891" Y="20.268669921875" />
                  <Point X="-1.137885864258" Y="20.2941953125" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.144810791016" Y="20.610279296875" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250210327148" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.343775024414" Y="21.0128046875" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735717773" Y="21.15403125" />
                  <Point X="-1.53301940918" Y="21.170376953125" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576531494141" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621458251953" Y="21.201552734375" />
                  <Point X="-1.636815673828" Y="21.203830078125" />
                  <Point X="-1.746667358398" Y="21.211029296875" />
                  <Point X="-1.946404541016" Y="21.22412109375" />
                  <Point X="-1.961928710938" Y="21.2238671875" />
                  <Point X="-1.992725708008" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047729492" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095435546875" Y="21.184189453125" />
                  <Point X="-2.186969970703" Y="21.123029296875" />
                  <Point X="-2.353402099609" Y="21.011822265625" />
                  <Point X="-2.359681396484" Y="21.007244140625" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503201904297" Y="20.83751953125" />
                  <Point X="-2.747588378906" Y="20.988837890625" />
                  <Point X="-2.883186523438" Y="21.093244140625" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.605637207031" Y="21.818361328125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947021484" Y="22.31888671875" />
                  <Point X="-2.319683105469" Y="22.333818359375" />
                  <Point X="-2.313412597656" Y="22.36534765625" />
                  <Point X="-2.311638671875" Y="22.380775390625" />
                  <Point X="-2.310627441406" Y="22.411712890625" />
                  <Point X="-2.314669677734" Y="22.442400390625" />
                  <Point X="-2.323657226562" Y="22.47201953125" />
                  <Point X="-2.329365234375" Y="22.4864609375" />
                  <Point X="-2.3435859375" Y="22.515291015625" />
                  <Point X="-2.351567871094" Y="22.52860546875" />
                  <Point X="-2.369591308594" Y="22.55375390625" />
                  <Point X="-2.3796328125" Y="22.565587890625" />
                  <Point X="-2.396976318359" Y="22.582931640625" />
                  <Point X="-2.408814697266" Y="22.5929765625" />
                  <Point X="-2.433973388672" Y="22.611005859375" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476123046875" Y="22.63320703125" />
                  <Point X="-2.490562988281" Y="22.6389140625" />
                  <Point X="-2.520180664062" Y="22.6478984375" />
                  <Point X="-2.550868896484" Y="22.6519375" />
                  <Point X="-2.581802734375" Y="22.650923828125" />
                  <Point X="-2.597226074219" Y="22.6491484375" />
                  <Point X="-2.628752929688" Y="22.642876953125" />
                  <Point X="-2.64368359375" Y="22.63861328125" />
                  <Point X="-2.672648925781" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.518564453125" Y="22.14078515625" />
                  <Point X="-3.793087646484" Y="21.9822890625" />
                  <Point X="-4.004022705078" Y="22.259416015625" />
                  <Point X="-4.101229980469" Y="22.422416015625" />
                  <Point X="-4.181266113281" Y="22.556625" />
                  <Point X="-3.510296386719" Y="23.0714765625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552978516" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.63162109375" />
                  <Point X="-2.948577392578" Y="23.646947265625" />
                  <Point X="-2.950785644531" Y="23.6786171875" />
                  <Point X="-2.953082275391" Y="23.6937734375" />
                  <Point X="-2.960082519531" Y="23.723517578125" />
                  <Point X="-2.971774169922" Y="23.75174609375" />
                  <Point X="-2.987854980469" Y="23.77773046875" />
                  <Point X="-2.996947753906" Y="23.79007421875" />
                  <Point X="-3.017779052734" Y="23.81403125" />
                  <Point X="-3.028743164062" Y="23.82475390625" />
                  <Point X="-3.052248291016" Y="23.844291015625" />
                  <Point X="-3.064789306641" Y="23.85310546875" />
                  <Point X="-3.091276123047" Y="23.868693359375" />
                  <Point X="-3.105441894531" Y="23.875521484375" />
                  <Point X="-3.134704345703" Y="23.886744140625" />
                  <Point X="-3.149801025391" Y="23.891138671875" />
                  <Point X="-3.181686279297" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.2945" Y="23.761544921875" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.766481933594" Y="24.205712890625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.034773925781" Y="24.54675390625" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497333496094" Y="24.69148828125" />
                  <Point X="-3.475949462891" Y="24.700115234375" />
                  <Point X="-3.465520019531" Y="24.705080078125" />
                  <Point X="-3.440103515625" Y="24.719134765625" />
                  <Point X="-3.423710449219" Y="24.729322265625" />
                  <Point X="-3.405805175781" Y="24.74175" />
                  <Point X="-3.396473876953" Y="24.7491328125" />
                  <Point X="-3.372519287109" Y="24.77066015625" />
                  <Point X="-3.357571044922" Y="24.787740234375" />
                  <Point X="-3.336381591797" Y="24.818763671875" />
                  <Point X="-3.327334960938" Y="24.835333984375" />
                  <Point X="-3.316627197266" Y="24.8606484375" />
                  <Point X="-3.310155517578" Y="24.87834765625" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.301576904297" Y="24.90805859375" />
                  <Point X="-3.296133789062" Y="24.936796875" />
                  <Point X="-3.294522705078" Y="24.957509765625" />
                  <Point X="-3.295623535156" Y="24.9919609375" />
                  <Point X="-3.297749511719" Y="25.00913671875" />
                  <Point X="-3.303279052734" Y="25.03453515625" />
                  <Point X="-3.307469482422" Y="25.0504375" />
                  <Point X="-3.313437988281" Y="25.06966796875" />
                  <Point X="-3.317668701172" Y="25.080787109375" />
                  <Point X="-3.330983642578" Y="25.110109375" />
                  <Point X="-3.342864990234" Y="25.129626953125" />
                  <Point X="-3.366303955078" Y="25.159373046875" />
                  <Point X="-3.379520507812" Y="25.17306640625" />
                  <Point X="-3.401186035156" Y="25.19141796875" />
                  <Point X="-3.415652832031" Y="25.2025234375" />
                  <Point X="-3.433558105469" Y="25.214951171875" />
                  <Point X="-3.440481689453" Y="25.219328125" />
                  <Point X="-3.465612792969" Y="25.232833984375" />
                  <Point X="-3.496564453125" Y="25.24563671875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-4.465568359375" Y="25.506115234375" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.957525390625" />
                  <Point X="-4.679556640625" Y="26.14858984375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.155464355469" Y="26.255291015625" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057617188" Y="26.204365234375" />
                  <Point X="-3.736704833984" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704885986328" Y="26.2080546875" />
                  <Point X="-3.684604003906" Y="26.21208984375" />
                  <Point X="-3.674578369141" Y="26.21466015625" />
                  <Point X="-3.652782470703" Y="26.22153125" />
                  <Point X="-3.61315234375" Y="26.23402734375" />
                  <Point X="-3.603459716797" Y="26.237673828125" />
                  <Point X="-3.584520507812" Y="26.246005859375" />
                  <Point X="-3.575273925781" Y="26.25069140625" />
                  <Point X="-3.556530273438" Y="26.261513671875" />
                  <Point X="-3.547854980469" Y="26.26717578125" />
                  <Point X="-3.531176025391" Y="26.27940625" />
                  <Point X="-3.515931396484" Y="26.293375" />
                  <Point X="-3.502294189453" Y="26.308923828125" />
                  <Point X="-3.495897949219" Y="26.317072265625" />
                  <Point X="-3.483483642578" Y="26.33480078125" />
                  <Point X="-3.478012695312" Y="26.343599609375" />
                  <Point X="-3.4680625" Y="26.361736328125" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.454837646484" Y="26.3921875" />
                  <Point X="-3.438935791016" Y="26.430578125" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012451172" Y="26.60453125" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.447783447266" Y="26.6332421875" />
                  <Point X="-3.4583359375" Y="26.653513671875" />
                  <Point X="-3.477523193359" Y="26.69037109375" />
                  <Point X="-3.482799072266" Y="26.699283203125" />
                  <Point X="-3.494290771484" Y="26.716482421875" />
                  <Point X="-3.500506591797" Y="26.72476953125" />
                  <Point X="-3.514418701172" Y="26.741349609375" />
                  <Point X="-3.521498535156" Y="26.748908203125" />
                  <Point X="-3.536440917969" Y="26.763212890625" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.093402099609" Y="27.191296875" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.213588378906" Y="27.3183125" />
                  <Point X="-4.002297607422" Y="27.680302734375" />
                  <Point X="-3.8651484375" Y="27.856591796875" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.485944335938" Y="27.89622265625" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923095703" Y="27.75771875" />
                  <Point X="-3.225994628906" Y="27.749388671875" />
                  <Point X="-3.216300048828" Y="27.745740234375" />
                  <Point X="-3.195657226562" Y="27.73923046875" />
                  <Point X="-3.185624267578" Y="27.736658203125" />
                  <Point X="-3.165333984375" Y="27.73262109375" />
                  <Point X="-3.155076660156" Y="27.73115625" />
                  <Point X="-3.124721191406" Y="27.7285" />
                  <Point X="-3.06952734375" Y="27.723671875" />
                  <Point X="-3.059174560547" Y="27.723333984375" />
                  <Point X="-3.03848828125" Y="27.72378515625" />
                  <Point X="-3.028154785156" Y="27.72457421875" />
                  <Point X="-3.006696777344" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976423095703" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902742431641" Y="27.7731953125" />
                  <Point X="-2.886610107422" Y="27.78614453125" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.857355224609" Y="27.814603515625" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751453125" Y="28.07475390625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.030844726562" Y="28.650478515625" />
                  <Point X="-3.059386474609" Y="28.6999140625" />
                  <Point X="-3.016378173828" Y="28.732888671875" />
                  <Point X="-2.648368896484" Y="29.015037109375" />
                  <Point X="-2.432382080078" Y="29.13503515625" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.189521484375" Y="29.2643828125" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111822265625" Y="29.16405078125" />
                  <Point X="-2.097520019531" Y="29.149109375" />
                  <Point X="-2.089959716797" Y="29.14202734375" />
                  <Point X="-2.073380615234" Y="29.128115234375" />
                  <Point X="-2.065092041016" Y="29.1218984375" />
                  <Point X="-2.047892211914" Y="29.11040625" />
                  <Point X="-2.03898059082" Y="29.105130859375" />
                  <Point X="-2.005194702148" Y="29.08754296875" />
                  <Point X="-1.943764282227" Y="29.0555625" />
                  <Point X="-1.934335083008" Y="29.0512890625" />
                  <Point X="-1.9150625" Y="29.04379296875" />
                  <Point X="-1.905219360352" Y="29.0405703125" />
                  <Point X="-1.88431237793" Y="29.034966796875" />
                  <Point X="-1.874174316406" Y="29.032833984375" />
                  <Point X="-1.853724731445" Y="29.029685546875" />
                  <Point X="-1.833054199219" Y="29.028783203125" />
                  <Point X="-1.812408081055" Y="29.03013671875" />
                  <Point X="-1.80212109375" Y="29.031376953125" />
                  <Point X="-1.780805053711" Y="29.03513671875" />
                  <Point X="-1.770713012695" Y="29.0374921875" />
                  <Point X="-1.750859863281" Y="29.04328125" />
                  <Point X="-1.741098510742" Y="29.04671484375" />
                  <Point X="-1.705908569336" Y="29.061291015625" />
                  <Point X="-1.641924316406" Y="29.087794921875" />
                  <Point X="-1.632590576172" Y="29.092271484375" />
                  <Point X="-1.614456420898" Y="29.10221875" />
                  <Point X="-1.605655883789" Y="29.107689453125" />
                  <Point X="-1.587926513672" Y="29.120103515625" />
                  <Point X="-1.579778320312" Y="29.126498046875" />
                  <Point X="-1.564226196289" Y="29.14013671875" />
                  <Point X="-1.550251098633" Y="29.155388671875" />
                  <Point X="-1.538019775391" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516855102539" Y="29.208728515625" />
                  <Point X="-1.508524658203" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.493423339844" Y="29.273681640625" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266235352" Y="29.56265625" />
                  <Point X="-1.407579345703" Y="29.58275390625" />
                  <Point X="-0.93116607666" Y="29.7163203125" />
                  <Point X="-0.669336914063" Y="29.746966796875" />
                  <Point X="-0.36522253418" Y="29.78255859375" />
                  <Point X="-0.294235290527" Y="29.517630859375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896308899" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.362653564453" Y="29.727021484375" />
                  <Point X="0.378190551758" Y="29.785005859375" />
                  <Point X="0.411895294189" Y="29.7814765625" />
                  <Point X="0.827852966309" Y="29.737916015625" />
                  <Point X="1.044512084961" Y="29.685607421875" />
                  <Point X="1.453624145508" Y="29.586833984375" />
                  <Point X="1.592677734375" Y="29.5363984375" />
                  <Point X="1.858255493164" Y="29.4400703125" />
                  <Point X="1.994612792969" Y="29.376302734375" />
                  <Point X="2.250419921875" Y="29.256669921875" />
                  <Point X="2.382231445312" Y="29.179876953125" />
                  <Point X="2.629453857422" Y="29.03584375" />
                  <Point X="2.753671386719" Y="28.9475078125" />
                  <Point X="2.817780761719" Y="28.901916015625" />
                  <Point X="2.371040527344" Y="28.128140625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.033683349609" Y="27.512107421875" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017912719727" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411376953" Y="27.369578125" />
                  <Point X="2.016381713867" Y="27.3449453125" />
                  <Point X="2.021782714844" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045319580078" Y="27.22388671875" />
                  <Point X="2.055680419922" Y="27.203845703125" />
                  <Point X="2.061457763672" Y="27.1941328125" />
                  <Point X="2.076700439453" Y="27.17166796875" />
                  <Point X="2.104415527344" Y="27.13082421875" />
                  <Point X="2.109808105469" Y="27.123634765625" />
                  <Point X="2.130456787109" Y="27.100130859375" />
                  <Point X="2.157589599609" Y="27.075392578125" />
                  <Point X="2.168250976563" Y="27.066984375" />
                  <Point X="2.19071484375" Y="27.051740234375" />
                  <Point X="2.231559570312" Y="27.024025390625" />
                  <Point X="2.241272949219" Y="27.01824609375" />
                  <Point X="2.261320556641" Y="27.007880859375" />
                  <Point X="2.271654785156" Y="27.003294921875" />
                  <Point X="2.293741699219" Y="26.995029296875" />
                  <Point X="2.304547851562" Y="26.991705078125" />
                  <Point X="2.326478027344" Y="26.986361328125" />
                  <Point X="2.337602050781" Y="26.984341796875" />
                  <Point X="2.362236083984" Y="26.981373046875" />
                  <Point X="2.407026855469" Y="26.975970703125" />
                  <Point X="2.416053466797" Y="26.975318359375" />
                  <Point X="2.447580566406" Y="26.97549609375" />
                  <Point X="2.484323242188" Y="26.97982421875" />
                  <Point X="2.497754638672" Y="26.9823984375" />
                  <Point X="2.526242675781" Y="26.990017578125" />
                  <Point X="2.578041015625" Y="27.003869140625" />
                  <Point X="2.583989746094" Y="27.005669921875" />
                  <Point X="2.604408691406" Y="27.013068359375" />
                  <Point X="2.62765625" Y="27.02357421875" />
                  <Point X="2.636033935547" Y="27.02787109375" />
                  <Point X="3.598873779297" Y="27.583765625" />
                  <Point X="3.940405517578" Y="27.78094921875" />
                  <Point X="4.043953369141" Y="27.63704296875" />
                  <Point X="4.113212890625" Y="27.522587890625" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.575476074219" Y="27.052689453125" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168130859375" Y="26.73986328125" />
                  <Point X="3.152115234375" Y="26.7252109375" />
                  <Point X="3.134668457031" Y="26.7066015625" />
                  <Point X="3.128575683594" Y="26.699421875" />
                  <Point X="3.108072753906" Y="26.672673828125" />
                  <Point X="3.070793457031" Y="26.624041015625" />
                  <Point X="3.065635009766" Y="26.6166015625" />
                  <Point X="3.049741455078" Y="26.589375" />
                  <Point X="3.034764160156" Y="26.555548828125" />
                  <Point X="3.030140869141" Y="26.54267578125" />
                  <Point X="3.022503417969" Y="26.5153671875" />
                  <Point X="3.008616943359" Y="26.4657109375" />
                  <Point X="3.006225585938" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.010968994141" Y="26.322185546875" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034685058594" Y="26.22860546875" />
                  <Point X="3.050287841797" Y="26.195369140625" />
                  <Point X="3.056919921875" Y="26.183525390625" />
                  <Point X="3.073972167969" Y="26.157607421875" />
                  <Point X="3.104977539062" Y="26.11048046875" />
                  <Point X="3.111737304688" Y="26.101427734375" />
                  <Point X="3.1262890625" Y="26.08418359375" />
                  <Point X="3.134081054688" Y="26.0759921875" />
                  <Point X="3.151324462891" Y="26.05990234375" />
                  <Point X="3.160033935547" Y="26.0526953125" />
                  <Point X="3.178244628906" Y="26.039369140625" />
                  <Point X="3.187745849609" Y="26.03325" />
                  <Point X="3.212456787109" Y="26.01933984375" />
                  <Point X="3.257387939453" Y="25.994046875" />
                  <Point X="3.2654765625" Y="25.98998828125" />
                  <Point X="3.294679931641" Y="25.97808203125" />
                  <Point X="3.330276367188" Y="25.96801953125" />
                  <Point X="3.343670654297" Y="25.965255859375" />
                  <Point X="3.377081787109" Y="25.96083984375" />
                  <Point X="3.437831298828" Y="25.9528125" />
                  <Point X="3.444033203125" Y="25.95219921875" />
                  <Point X="3.465709228516" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.415236816406" Y="26.0732109375" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.774509765625" Y="25.774044921875" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.153293945312" Y="25.544958984375" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686021972656" Y="25.419541015625" />
                  <Point X="3.665625" Y="25.41213671875" />
                  <Point X="3.642385009766" Y="25.40162109375" />
                  <Point X="3.634008789062" Y="25.397318359375" />
                  <Point X="3.601183349609" Y="25.378345703125" />
                  <Point X="3.541498779297" Y="25.34384765625" />
                  <Point X="3.533882324219" Y="25.338947265625" />
                  <Point X="3.508778320312" Y="25.319873046875" />
                  <Point X="3.481994628906" Y="25.2943515625" />
                  <Point X="3.472793945312" Y="25.284224609375" />
                  <Point X="3.453098876953" Y="25.259126953125" />
                  <Point X="3.417288085938" Y="25.21349609375" />
                  <Point X="3.410851318359" Y="25.204203125" />
                  <Point X="3.399128662109" Y="25.184923828125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005371094" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.363811035156" Y="25.07619140625" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.358439697266" Y="24.889296875" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380005126953" Y="24.79451171875" />
                  <Point X="3.384068603516" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.762501953125" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410851318359" Y="24.733236328125" />
                  <Point X="3.417288085938" Y="24.723943359375" />
                  <Point X="3.436983154297" Y="24.698845703125" />
                  <Point X="3.472793945312" Y="24.65321484375" />
                  <Point X="3.478717285156" Y="24.646365234375" />
                  <Point X="3.501137451172" Y="24.6241953125" />
                  <Point X="3.530176269531" Y="24.6012734375" />
                  <Point X="3.541497558594" Y="24.593591796875" />
                  <Point X="3.574322998047" Y="24.574619140625" />
                  <Point X="3.634007568359" Y="24.540119140625" />
                  <Point X="3.639499511719" Y="24.537181640625" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026367188" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.530752441406" Y="24.29133984375" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.068947265625" />
                  <Point X="4.733649902344" Y="23.94640625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.971762695312" Y="24.020314453125" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366720947266" Y="24.089158203125" />
                  <Point X="3.354483398438" Y="24.087322265625" />
                  <Point X="3.290058837891" Y="24.0733203125" />
                  <Point X="3.172919189453" Y="24.047859375" />
                  <Point X="3.157875732422" Y="24.0432578125" />
                  <Point X="3.128753417969" Y="24.0316328125" />
                  <Point X="3.114674560547" Y="24.024609375" />
                  <Point X="3.086848144531" Y="24.007716796875" />
                  <Point X="3.074122802734" Y="23.99846875" />
                  <Point X="3.050373291016" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="3.000408447266" Y="23.91994140625" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.869576171875" Y="23.6426875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786376953" Y="23.423330078125" />
                  <Point X="2.889158691406" Y="23.394517578125" />
                  <Point X="2.896541503906" Y="23.380626953125" />
                  <Point X="2.932195068359" Y="23.325171875" />
                  <Point X="2.997021728516" Y="23.224337890625" />
                  <Point X="3.0017421875" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.831170898438" Y="22.566453125" />
                  <Point X="4.087170898438" Y="22.370017578125" />
                  <Point X="4.045498046875" Y="22.302583984375" />
                  <Point X="4.001273681641" Y="22.239748046875" />
                  <Point X="3.324927001953" Y="22.630236328125" />
                  <Point X="2.848454589844" Y="22.905328125" />
                  <Point X="2.841199951172" Y="22.909109375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.694433105469" Y="22.947509765625" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508008789062" Y="22.974595703125" />
                  <Point X="2.492317138672" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415069091797" Y="22.9550390625" />
                  <Point X="2.400590332031" Y="22.948890625" />
                  <Point X="2.336891845703" Y="22.9153671875" />
                  <Point X="2.221072265625" Y="22.854412109375" />
                  <Point X="2.208969726562" Y="22.846830078125" />
                  <Point X="2.186039306641" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.086939453125" Y="22.69010546875" />
                  <Point X="2.025984619141" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.01603527832" Y="22.34318359375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.569730712891" Y="21.260076171875" />
                  <Point X="2.735892822266" Y="20.972275390625" />
                  <Point X="2.723751953125" Y="20.96391796875" />
                  <Point X="2.197507080078" Y="21.649732421875" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828654663086" Y="22.1298515625" />
                  <Point X="1.808836914062" Y="22.1503671875" />
                  <Point X="1.783252929688" Y="22.171994140625" />
                  <Point X="1.773297119141" Y="22.179353515625" />
                  <Point X="1.697674316406" Y="22.227970703125" />
                  <Point X="1.560173706055" Y="22.31637109375" />
                  <Point X="1.546284912109" Y="22.323751953125" />
                  <Point X="1.517471435547" Y="22.336125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384887695" Y="22.3513046875" />
                  <Point X="1.424120727539" Y="22.35362109375" />
                  <Point X="1.408397705078" Y="22.35348046875" />
                  <Point X="1.325691162109" Y="22.34587109375" />
                  <Point X="1.175310668945" Y="22.332033203125" />
                  <Point X="1.161227783203" Y="22.32966015625" />
                  <Point X="1.133575561523" Y="22.322828125" />
                  <Point X="1.120006103516" Y="22.318369140625" />
                  <Point X="1.092620727539" Y="22.307025390625" />
                  <Point X="1.07987487793" Y="22.300583984375" />
                  <Point X="1.055493041992" Y="22.28586328125" />
                  <Point X="1.043857299805" Y="22.277583984375" />
                  <Point X="0.979993530273" Y="22.224482421875" />
                  <Point X="0.863873596191" Y="22.127931640625" />
                  <Point X="0.852653625488" Y="22.11691015625" />
                  <Point X="0.83218359375" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.763697021484" Y="21.906515625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.797203430176" Y="20.981595703125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605224609" Y="21.51987890625" />
                  <Point X="0.642143249512" Y="21.546423828125" />
                  <Point X="0.626786010742" Y="21.576185546875" />
                  <Point X="0.620406677246" Y="21.586791015625" />
                  <Point X="0.562311157227" Y="21.670494140625" />
                  <Point X="0.456679229736" Y="21.82269140625" />
                  <Point X="0.446668457031" Y="21.834830078125" />
                  <Point X="0.424785522461" Y="21.857283203125" />
                  <Point X="0.412913360596" Y="21.86759765625" />
                  <Point X="0.386658233643" Y="21.886841796875" />
                  <Point X="0.373243835449" Y="21.895060546875" />
                  <Point X="0.34524105835" Y="21.909169921875" />
                  <Point X="0.330652679443" Y="21.915060546875" />
                  <Point X="0.240753356934" Y="21.9429609375" />
                  <Point X="0.07729372406" Y="21.993693359375" />
                  <Point X="0.063380096436" Y="21.996888671875" />
                  <Point X="0.03522794342" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022895795822" Y="22.001162109375" />
                  <Point X="-0.051062362671" Y="21.996892578125" />
                  <Point X="-0.064984909058" Y="21.9936953125" />
                  <Point X="-0.154884384155" Y="21.96579296875" />
                  <Point X="-0.318343841553" Y="21.9150625" />
                  <Point X="-0.33292880249" Y="21.909171875" />
                  <Point X="-0.360929077148" Y="21.895064453125" />
                  <Point X="-0.374344512939" Y="21.88684765625" />
                  <Point X="-0.400600250244" Y="21.867603515625" />
                  <Point X="-0.412477142334" Y="21.85728125" />
                  <Point X="-0.434361114502" Y="21.83482421875" />
                  <Point X="-0.444368347168" Y="21.822689453125" />
                  <Point X="-0.50246383667" Y="21.738984375" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.901195739746" Y="20.54754296875" />
                  <Point X="-0.985425048828" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.666128046827" Y="21.039014772175" />
                  <Point X="2.68813112819" Y="21.055000946532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608275820626" Y="21.114409127362" />
                  <Point X="2.640369434115" Y="21.137726502439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550423594425" Y="21.18980348255" />
                  <Point X="2.592607740039" Y="21.220452058346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998083443224" Y="22.241589928788" />
                  <Point X="4.007264917632" Y="22.248260660416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492571368224" Y="21.265197837738" />
                  <Point X="2.544846062449" Y="21.303177626231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.908025007815" Y="22.29358510332" />
                  <Point X="4.051207478848" Y="22.397613257791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.434719142023" Y="21.340592192925" />
                  <Point X="2.497084400014" Y="21.385903205126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.817966572406" Y="22.345580277853" />
                  <Point X="3.972601880191" Y="22.45792940529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.376866915822" Y="21.415986548113" />
                  <Point X="2.449322737579" Y="21.468628784021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.727908136998" Y="22.397575452385" />
                  <Point X="3.893996281534" Y="22.518245552788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.319014689621" Y="21.4913809033" />
                  <Point X="2.401561075144" Y="21.551354362916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.637849701589" Y="22.449570626918" />
                  <Point X="3.815390684422" Y="22.57856170141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26116246342" Y="21.566775258488" />
                  <Point X="2.353799412709" Y="21.634079941811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.54779126618" Y="22.50156580145" />
                  <Point X="3.736785093465" Y="22.638877854503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.203310237219" Y="21.642169613676" />
                  <Point X="2.306037750274" Y="21.716805520706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.457732830772" Y="22.553560975983" />
                  <Point X="3.658179502508" Y="22.699194007596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.145458122577" Y="21.717564049916" />
                  <Point X="2.258276087839" Y="21.799531099602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.367674395363" Y="22.605556150515" />
                  <Point X="3.579573911551" Y="22.759510160689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825501588397" Y="20.875985950578" />
                  <Point X="0.829025282342" Y="20.878546064085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087606020374" Y="21.792958495194" />
                  <Point X="2.210514425404" Y="21.882256678497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.277616019647" Y="22.657551368417" />
                  <Point X="3.500968320593" Y="22.819826313782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.799164393638" Y="20.97427731638" />
                  <Point X="0.814915412493" Y="20.985721101437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029753918171" Y="21.868352940471" />
                  <Point X="2.162752762969" Y="21.964982257392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.187557697865" Y="22.709546625504" />
                  <Point X="3.422362729636" Y="22.880142466875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.772827304891" Y="21.072568759204" />
                  <Point X="0.800805542644" Y="21.09289613879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.971901815968" Y="21.943747385748" />
                  <Point X="2.114991100534" Y="22.047707836287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.097499376083" Y="22.761541882591" />
                  <Point X="3.343757138679" Y="22.940458619968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698385795636" Y="23.924653948903" />
                  <Point X="4.734708145924" Y="23.951043681104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746490224671" Y="21.170860208223" />
                  <Point X="0.786695672794" Y="21.200071176142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.914049713765" Y="22.019141831026" />
                  <Point X="2.067477122263" Y="22.130613368261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.007441054301" Y="22.813537139678" />
                  <Point X="3.265151547721" Y="23.000774773061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.56155595138" Y="23.942667705812" />
                  <Point X="4.764842144224" Y="24.09036377027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.720153144452" Y="21.269151657243" />
                  <Point X="0.772585802945" Y="21.307246213494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.856197611561" Y="22.094536276303" />
                  <Point X="2.037191824786" Y="22.226036269534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.917382732519" Y="22.865532396765" />
                  <Point X="3.186545956764" Y="23.061090926154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.424726107123" Y="23.960681462722" />
                  <Point X="4.784723740727" Y="24.222235053517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693816064233" Y="21.367443106262" />
                  <Point X="0.758475933096" Y="21.414421250847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.791541646279" Y="22.164987425699" />
                  <Point X="2.018444712826" Y="22.32984215328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.825268701111" Y="22.916034093384" />
                  <Point X="3.107940365807" Y="23.121407079247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.287896262867" Y="23.978695219632" />
                  <Point X="4.667706389883" Y="24.254643429477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.01978620375" Y="20.239864640349" />
                  <Point X="-0.974899634873" Y="20.272476641574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.667478984013" Y="21.465734555282" />
                  <Point X="0.744366063247" Y="21.521596288199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.707718930216" Y="22.221513115528" />
                  <Point X="2.000787044145" Y="22.4344395639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.704472136168" Y="22.945696709578" />
                  <Point X="3.031823866055" Y="23.183531662958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15106641861" Y="23.996708976542" />
                  <Point X="4.549629284059" Y="24.286281848374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.141191714087" Y="20.269084831817" />
                  <Point X="-0.935829156712" Y="20.41828946341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.635264031232" Y="21.559755479911" />
                  <Point X="0.730256193398" Y="21.628771325552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.621971728085" Y="22.276640584385" />
                  <Point X="2.023457976975" Y="22.568337418613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.575025062709" Y="22.969074363446" />
                  <Point X="2.974525940247" Y="23.259328740953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.014236574354" Y="24.014722733451" />
                  <Point X="4.431552276065" Y="24.317920338349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.124096858951" Y="20.398931428946" />
                  <Point X="-0.896758685726" Y="20.564102280034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.583681060186" Y="21.639704715588" />
                  <Point X="0.728679210462" Y="21.745052038246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.533027202793" Y="22.329445061989" />
                  <Point X="2.123267134058" Y="22.758279473781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.346281045456" Y="22.920308564748" />
                  <Point X="2.923067639973" Y="23.339368555248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.877406937226" Y="24.032736640849" />
                  <Point X="4.313475286687" Y="24.34955884185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125872140756" Y="20.515068069078" />
                  <Point X="-0.857688270742" Y="20.709915055969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.52950138041" Y="21.71776733194" />
                  <Point X="0.75889725278" Y="21.884433188965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.403918192913" Y="22.353068333426" />
                  <Point X="2.87683886582" Y="23.423207842671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740577393337" Y="24.050750615989" />
                  <Point X="4.195398297309" Y="24.381197345351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.146280319763" Y="20.617667116973" />
                  <Point X="-0.818617855758" Y="20.855727831904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.475322113331" Y="21.795830248133" />
                  <Point X="0.794790224879" Y="22.027937417514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.218857530672" Y="22.33604034991" />
                  <Point X="2.859388697985" Y="23.52795601148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603747849448" Y="24.068764591128" />
                  <Point X="4.077321307932" Y="24.412835848851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.166688533693" Y="20.720266139495" />
                  <Point X="-0.779547440774" Y="21.001540607839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.412696550246" Y="21.867756571074" />
                  <Point X="2.870571793938" Y="23.653507464148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.466918305559" Y="24.086778566268" />
                  <Point X="3.959244318554" Y="24.444474352352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188298727382" Y="20.821991872604" />
                  <Point X="-0.74047702579" Y="21.147353383774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.320512664784" Y="21.918207515751" />
                  <Point X="2.905282858821" Y="23.79615298684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.285367464421" Y="24.072300617049" />
                  <Point X="3.841167329176" Y="24.476112855853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.23240682907" Y="20.907371918761" />
                  <Point X="-0.701406610806" Y="21.293166159709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.207264882692" Y="21.953354643722" />
                  <Point X="3.723090339799" Y="24.507751359354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.300794623096" Y="20.975111735867" />
                  <Point X="-0.662336195822" Y="21.438978935644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.094018266566" Y="21.988502618816" />
                  <Point X="3.618419230927" Y="24.549129805167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.374024992984" Y="21.039333215664" />
                  <Point X="-0.595461693116" Y="21.604992563762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.05836620904" Y="21.995215274543" />
                  <Point X="3.529404078211" Y="24.601882968945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.447255441529" Y="21.103554638313" />
                  <Point X="-0.418871971087" Y="21.850718964687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.350837469409" Y="21.900148923528" />
                  <Point X="3.460316251405" Y="24.669114182465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.524337490804" Y="21.164977709232" />
                  <Point X="3.403527507457" Y="24.745281202737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.709217066997" Y="25.693920196116" />
                  <Point X="4.779081883654" Y="25.744679956629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.633219446753" Y="21.203296795565" />
                  <Point X="3.368417200578" Y="24.837198529481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.453157072874" Y="25.625308178527" />
                  <Point X="4.762657123933" Y="25.850173128041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.781094387052" Y="21.213285820474" />
                  <Point X="3.35008369977" Y="24.94130491932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.197097078751" Y="25.556696160939" />
                  <Point X="4.743143446553" Y="25.953422069409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.929343711099" Y="21.223002839668" />
                  <Point X="3.362145524276" Y="25.067494805651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.941038895786" Y="25.488085459233" />
                  <Point X="4.718852748054" Y="26.053200301778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.567091455365" Y="20.877078439182" />
                  <Point X="3.438264969278" Y="25.240225277515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.684123213557" Y="25.418851747845" />
                  <Point X="4.622290737578" Y="26.100470352439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.654350724466" Y="20.93110732708" />
                  <Point X="4.424898464524" Y="26.074482929228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.741609993566" Y="20.985136214979" />
                  <Point X="4.227506466182" Y="26.048495705608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820679225478" Y="21.0451155132" />
                  <Point X="4.03011448198" Y="26.02250849226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899146105571" Y="21.105532445636" />
                  <Point X="3.832722497777" Y="25.996521278912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.977613154801" Y="21.165949255185" />
                  <Point X="3.635330513574" Y="25.970534065563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868915703793" Y="21.362349033891" />
                  <Point X="3.448211983564" Y="25.952010953595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752132767967" Y="21.564623261176" />
                  <Point X="3.314687539858" Y="25.972426224577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.635349832142" Y="21.766897488461" />
                  <Point X="3.215374382387" Y="26.017697449947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.518567045016" Y="21.969171607709" />
                  <Point X="3.134042433689" Y="26.076032788194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.401784308634" Y="22.17144569009" />
                  <Point X="3.07744057059" Y="26.152335585351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316264924731" Y="22.351005617327" />
                  <Point X="3.031930287719" Y="26.236696887246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.321429972434" Y="22.464679448374" />
                  <Point X="3.007970851845" Y="26.336715796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.366462329574" Y="22.549387983638" />
                  <Point X="3.005913580214" Y="26.45264755853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.439070787447" Y="22.614061308963" />
                  <Point X="3.062454733838" Y="26.611153569083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.548916094199" Y="22.651680479968" />
                  <Point X="4.126069667039" Y="27.501341509338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.00802001717" Y="22.435548413018" />
                  <Point X="4.076712020712" Y="27.582907538061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.793235066826" Y="21.982482743676" />
                  <Point X="4.025293207182" Y="27.662976041155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.850787329329" Y="22.058095035247" />
                  <Point X="3.969806490523" Y="27.740089039625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.908339591832" Y="22.133707326818" />
                  <Point X="3.570377368256" Y="27.567313253237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.965891854335" Y="22.209319618389" />
                  <Point X="2.783303115722" Y="27.112896793935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.020510531369" Y="22.287063284563" />
                  <Point X="2.432446189333" Y="26.975410773531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.069369580264" Y="22.368991565525" />
                  <Point X="2.29661046335" Y="26.994146799644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.118228447291" Y="22.450919978622" />
                  <Point X="2.202888242055" Y="27.043480077916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167086973446" Y="22.532848639378" />
                  <Point X="2.126085776038" Y="27.105106277962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979845090028" Y="23.512856816572" />
                  <Point X="2.069890528941" Y="27.181704498937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948979121631" Y="23.652708713143" />
                  <Point X="2.028643302122" Y="27.269163092353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972554353282" Y="23.753006762604" />
                  <Point X="2.013135651214" Y="27.375322582321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.031860367452" Y="23.827344879006" />
                  <Point X="2.032213212727" Y="27.506609699954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119666122274" Y="23.880976721786" />
                  <Point X="2.114521264026" Y="27.683836457482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.25782323144" Y="23.898026164293" />
                  <Point X="2.231304551452" Y="27.88611094022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.455215155468" Y="23.872038994664" />
                  <Point X="2.348087838878" Y="28.088385422958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.652607079496" Y="23.846051825036" />
                  <Point X="2.464871333751" Y="28.290660056415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.849999003524" Y="23.820064655407" />
                  <Point X="2.58165487937" Y="28.49293472674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.047390927552" Y="23.794077485779" />
                  <Point X="2.698438424988" Y="28.695209397065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.24478285158" Y="23.76809031615" />
                  <Point X="2.815221970606" Y="28.897484067391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.442173507394" Y="23.742104067932" />
                  <Point X="2.737893487034" Y="28.958728093312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.639563736244" Y="23.716118129923" />
                  <Point X="2.656215834258" Y="29.016812262845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.68348203756" Y="23.801636074121" />
                  <Point X="2.569146513571" Y="29.070979156344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708781355221" Y="23.900681501774" />
                  <Point X="-3.693779290021" Y="24.638123668154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.298278835585" Y="24.925471568148" />
                  <Point X="2.479449679167" Y="29.123237049385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.734080672881" Y="23.999726929427" />
                  <Point X="-3.94983807393" Y="24.569512529838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.304325174369" Y="25.038505103745" />
                  <Point X="2.389752844764" Y="29.175494942426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.751958107329" Y="24.104164670871" />
                  <Point X="-4.205896550142" Y="24.500901615076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.342149064607" Y="25.128450896775" />
                  <Point X="2.300055446039" Y="29.227752425463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.767171980793" Y="24.210537602647" />
                  <Point X="-4.461954873631" Y="24.432290811274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.40892700471" Y="25.19736034122" />
                  <Point X="2.20650250505" Y="29.277208693077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782385813965" Y="24.316910563695" />
                  <Point X="-4.71801319712" Y="24.363680007472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.501704400703" Y="25.247380075256" />
                  <Point X="2.10817272691" Y="29.323194385351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.619309725043" Y="25.279361263466" />
                  <Point X="2.009842948769" Y="29.369180077626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737386631899" Y="25.310999826922" />
                  <Point X="1.91151196128" Y="29.415164891256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.855463538756" Y="25.342638390378" />
                  <Point X="1.808838237909" Y="29.457994522581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.973540445612" Y="25.374276953834" />
                  <Point X="1.701033770226" Y="29.497096449963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091617352469" Y="25.405915517289" />
                  <Point X="1.593229302544" Y="29.536198377345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.209694259326" Y="25.437554080745" />
                  <Point X="1.485424269639" Y="29.57529989407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.327771166182" Y="25.469192644201" />
                  <Point X="1.368097019126" Y="29.60748311474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.445848073039" Y="25.500831207657" />
                  <Point X="1.24678571544" Y="29.636771751347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.563924887974" Y="25.532469837897" />
                  <Point X="-3.57561695451" Y="26.250517582324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514706453395" Y="26.294771651787" />
                  <Point X="1.125474411753" Y="29.666060387953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.68200168448" Y="25.564108481528" />
                  <Point X="-3.795836722038" Y="26.20794501357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.426274936454" Y="26.476447367522" />
                  <Point X="1.004163066052" Y="29.695348994034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.783032521643" Y="25.608131739551" />
                  <Point X="-3.932666238004" Y="26.225959008997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.431546454561" Y="26.590043843293" />
                  <Point X="0.002622010163" Y="29.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.235262313289" Y="29.254136355199" />
                  <Point X="0.882851636046" Y="29.724637538864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763563422024" Y="25.739703326269" />
                  <Point X="-4.069495753969" Y="26.243973004424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.471364552036" Y="26.678540759955" />
                  <Point X="-0.113745169369" Y="29.117994034318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.275179763448" Y="29.400564538212" />
                  <Point X="0.75063447581" Y="29.746002606883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744094322405" Y="25.871274912987" />
                  <Point X="-4.20632536356" Y="26.261986931829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.527814089077" Y="26.754954228472" />
                  <Point X="-0.185420261548" Y="29.183345489514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.314250138057" Y="29.546377284812" />
                  <Point X="0.609372289356" Y="29.760796078688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.717683730599" Y="26.007889788986" />
                  <Point X="-4.343155131403" Y="26.280000744256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604845234957" Y="26.816414282871" />
                  <Point X="-0.227865427144" Y="29.269933729463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.353320512665" Y="29.692190031413" />
                  <Point X="0.468110102903" Y="29.775589550493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678062817489" Y="26.154102525221" />
                  <Point X="-4.479984899246" Y="26.298014556683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683450829955" Y="26.876730433029" />
                  <Point X="-0.254202408899" Y="29.368225250021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.638442244396" Y="26.30031501442" />
                  <Point X="-4.61681466709" Y="26.316028369111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762056424953" Y="26.937046583186" />
                  <Point X="-0.280539390654" Y="29.46651677058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.840662019951" Y="26.997362733343" />
                  <Point X="-0.306876444005" Y="29.56480823912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.919267614949" Y="27.0576788835" />
                  <Point X="-2.993995117585" Y="27.729928702829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808955163276" Y="27.864368099015" />
                  <Point X="-0.333213574929" Y="29.663099651301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997873209948" Y="27.117995033657" />
                  <Point X="-3.15405254226" Y="27.731066634742" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748642471017" Y="28.025614292782" />
                  <Point X="-0.359550705852" Y="29.761391063482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.076478804946" Y="27.178311183815" />
                  <Point X="-3.26440520141" Y="27.768317192654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757257245052" Y="28.136781750939" />
                  <Point X="-0.516390460338" Y="29.764866769628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.155084564583" Y="27.238627214354" />
                  <Point X="-3.354463636936" Y="27.820312367101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.788818342463" Y="28.231277729302" />
                  <Point X="-0.709049022848" Y="29.742318588443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.218416697992" Y="27.310040183906" />
                  <Point X="-3.444522072461" Y="27.872307541549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.83658008268" Y="28.314003251685" />
                  <Point X="-1.851652423469" Y="29.029595083111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.486805052554" Y="29.294672214311" />
                  <Point X="-0.901710945528" Y="29.719767965951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.0994065036" Y="27.51393260926" />
                  <Point X="-3.53458044768" Y="27.924302759812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.884341822897" Y="28.396728774068" />
                  <Point X="-1.963437995752" Y="29.065804568692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462681522267" Y="29.429625442853" />
                  <Point X="-1.154109192779" Y="29.653816363191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963628042926" Y="27.730007893189" />
                  <Point X="-3.624638771535" Y="27.976298015392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.932103563114" Y="28.479454296451" />
                  <Point X="-2.056112761214" Y="29.115898868174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475943386613" Y="29.537416592267" />
                  <Point X="-1.417288385895" Y="29.580031944769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.753497220213" Y="28.000103330198" />
                  <Point X="-3.714697095391" Y="28.028293270973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.979865303331" Y="28.562179818835" />
                  <Point X="-2.126467384098" Y="29.182209700469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027627043548" Y="28.644905341218" />
                  <Point X="-2.184319811631" Y="29.25760390938" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.61367755127" Y="20.932419921875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.478455078125" />
                  <Point X="0.406223327637" Y="21.562158203125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.274335906982" Y="21.733599609375" />
                  <Point X="0.184436386108" Y="21.7615" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.098563964844" Y="21.78433203125" />
                  <Point X="-0.2620234375" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.346374664307" Y="21.63065234375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.71766986084" Y="20.4983671875" />
                  <Point X="-0.84774420166" Y="20.012921875" />
                  <Point X="-0.908339111328" Y="20.02468359375" />
                  <Point X="-1.100258789062" Y="20.0619375" />
                  <Point X="-1.203709228516" Y="20.088552734375" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.326260375977" Y="20.31899609375" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.33116015625" Y="20.573212890625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.46905090332" Y="20.86995703125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.759092407227" Y="21.021435546875" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.081412841797" Y="20.965048828125" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.404829589844" Y="20.65361328125" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.574537597656" Y="20.658216796875" />
                  <Point X="-2.855839355469" Y="20.832392578125" />
                  <Point X="-2.999101318359" Y="20.94269921875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.770182128906" Y="21.913361328125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.40241015625" />
                  <Point X="-2.513983886719" Y="22.431240234375" />
                  <Point X="-2.531327392578" Y="22.448583984375" />
                  <Point X="-2.560156738281" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.423564453125" Y="21.9762421875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.939464355469" Y="21.860892578125" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.264414550781" Y="22.325099609375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.6259609375" Y="23.22221484375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140325683594" Y="23.66540234375" />
                  <Point X="-3.161156982422" Y="23.689359375" />
                  <Point X="-3.187643798828" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.269700195312" Y="23.573169921875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.840473632812" Y="23.6485234375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.954567871094" Y="24.178810546875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.083949707031" Y="24.73028125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.532047363281" Y="24.885408203125" />
                  <Point X="-3.514142089844" Y="24.8978359375" />
                  <Point X="-3.50232421875" Y="24.909353515625" />
                  <Point X="-3.491616455078" Y="24.93466796875" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.488930419922" Y="24.9941171875" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.50232421875" Y="25.0280859375" />
                  <Point X="-3.523989746094" Y="25.0464375" />
                  <Point X="-3.541895019531" Y="25.058865234375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.514744140625" Y="25.322587890625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.973663085938" Y="25.6178515625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.862942382812" Y="26.19828515625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.130664550781" Y="26.443666015625" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731703857422" Y="26.3958671875" />
                  <Point X="-3.709907958984" Y="26.40273828125" />
                  <Point X="-3.670277832031" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.630374267578" Y="26.4648984375" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.626868652344" Y="26.565783203125" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968017578" Y="26.619220703125" />
                  <Point X="-4.209066894531" Y="27.04055859375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.377681152344" Y="27.41408984375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.015110351562" Y="27.973259765625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.390944335938" Y="28.060765625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514160156" Y="27.92043359375" />
                  <Point X="-3.108158691406" Y="27.91777734375" />
                  <Point X="-3.05296484375" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.991706298828" Y="27.948951171875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.940729980469" Y="28.0581953125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.195389648438" Y="28.555478515625" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.131982421875" Y="28.883673828125" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.524656738281" Y="29.301125" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.038783935547" Y="29.380046875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247558594" Y="29.273662109375" />
                  <Point X="-1.917462036133" Y="29.25607421875" />
                  <Point X="-1.85603137207" Y="29.22409375" />
                  <Point X="-1.835124389648" Y="29.218490234375" />
                  <Point X="-1.81380847168" Y="29.22225" />
                  <Point X="-1.778618530273" Y="29.236826171875" />
                  <Point X="-1.714634399414" Y="29.263330078125" />
                  <Point X="-1.696905029297" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.674629638672" Y="29.330814453125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.679580566406" Y="29.628546875" />
                  <Point X="-1.689137573242" Y="29.701140625" />
                  <Point X="-1.458870727539" Y="29.76569921875" />
                  <Point X="-0.968083190918" Y="29.903296875" />
                  <Point X="-0.691424438477" Y="29.935677734375" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.110709388733" Y="29.566806640625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282131195" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594051361" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.179127685547" Y="29.776197265625" />
                  <Point X="0.236648376465" Y="29.9908671875" />
                  <Point X="0.431685913086" Y="29.97044140625" />
                  <Point X="0.860210021973" Y="29.925564453125" />
                  <Point X="1.089102905273" Y="29.870302734375" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.657461914062" Y="29.71501171875" />
                  <Point X="1.931043334961" Y="29.61578125" />
                  <Point X="2.075100585938" Y="29.548412109375" />
                  <Point X="2.338683837891" Y="29.425142578125" />
                  <Point X="2.477877197266" Y="29.344048828125" />
                  <Point X="2.73253515625" Y="29.19568359375" />
                  <Point X="2.863783691406" Y="29.10234765625" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.535585449219" Y="28.033140625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.217233886719" Y="27.463025390625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.205015136719" Y="27.36769140625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.233925048828" Y="27.27834765625" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.297403808594" Y="27.208958984375" />
                  <Point X="2.338248535156" Y="27.181244140625" />
                  <Point X="2.360335449219" Y="27.172978515625" />
                  <Point X="2.384969482422" Y="27.170009765625" />
                  <Point X="2.429760253906" Y="27.164607421875" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.47715234375" Y="27.173564453125" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.503873779297" Y="27.7483125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.051544433594" Y="27.95180078125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.275767578125" Y="27.620955078125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.691140625" Y="26.901951171875" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.258868652344" Y="26.557083984375" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.205481933594" Y="26.46419140625" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.197049072266" Y="26.360580078125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.232698730469" Y="26.262037109375" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.305658447266" Y="26.18491015625" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.368566650391" Y="26.153619140625" />
                  <Point X="3.401977783203" Y="26.149203125" />
                  <Point X="3.462727294922" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.390437011719" Y="26.2615859375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.874314453125" Y="26.2178671875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.962248046875" Y="25.803275390625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.202469726562" Y="25.361431640625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087402344" Y="25.232818359375" />
                  <Point X="3.696261962891" Y="25.213845703125" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.602570068359" Y="25.141830078125" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.550419921875" Y="25.040453125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.545048095703" Y="24.92503515625" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759277344" Y="24.841240234375" />
                  <Point X="3.586454345703" Y="24.816142578125" />
                  <Point X="3.622265136719" Y="24.77051171875" />
                  <Point X="3.636576171875" Y="24.758091796875" />
                  <Point X="3.669401611328" Y="24.739119140625" />
                  <Point X="3.729086181641" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.579928222656" Y="24.4748671875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.984654296875" Y="24.273857421875" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.918888183594" Y="23.90413671875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.946962890625" Y="23.831939453125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.330411376953" Y="23.88765625" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.146504638672" Y="23.79846875" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.058776855469" Y="23.62527734375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.092013916016" Y="23.427921875" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.946835449219" Y="22.71719140625" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.306240234375" Y="22.3630859375" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.143033691406" Y="22.111046875" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.229927001953" Y="22.46569140625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.660665039062" Y="22.760533203125" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.425379150391" Y="22.74723046875" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.255075683594" Y="22.6016171875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.203010498047" Y="22.37694921875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.734275634766" Y="21.355076171875" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.956458984375" Y="20.896330078125" />
                  <Point X="2.835296142578" Y="20.809787109375" />
                  <Point X="2.766987548828" Y="20.765572265625" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.046770019531" Y="21.534068359375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.594926513672" Y="22.068150390625" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.343098388672" Y="22.156671875" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.165332641602" Y="22.131490234375" />
                  <Point X="1.10146887207" Y="22.078388671875" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.949361877441" Y="21.86616015625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.055677978516" Y="20.612595703125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.108973266602" Y="20.06187890625" />
                  <Point X="0.994366638184" Y="20.0367578125" />
                  <Point X="0.93123828125" Y="20.0252890625" />
                  <Point X="0.860200317383" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#152" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.068569105749" Y="4.611027873117" Z="0.9" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="-0.703392512309" Y="5.01661766568" Z="0.9" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.9" />
                  <Point X="-1.478524334475" Y="4.845123510723" Z="0.9" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.9" />
                  <Point X="-1.735308866933" Y="4.653301920354" Z="0.9" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.9" />
                  <Point X="-1.728471475712" Y="4.377130412799" Z="0.9" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.9" />
                  <Point X="-1.803909102593" Y="4.314300854688" Z="0.9" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.9" />
                  <Point X="-1.90052958834" Y="4.331703520828" Z="0.9" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.9" />
                  <Point X="-2.005272312325" Y="4.441764470535" Z="0.9" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.9" />
                  <Point X="-2.555095750193" Y="4.376112716961" Z="0.9" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.9" />
                  <Point X="-3.168560409571" Y="3.954634589716" Z="0.9" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.9" />
                  <Point X="-3.244846783068" Y="3.561759119031" Z="0.9" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.9" />
                  <Point X="-2.99669580474" Y="3.085119094084" Z="0.9" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.9" />
                  <Point X="-3.033216977349" Y="3.015586590537" Z="0.9" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.9" />
                  <Point X="-3.109957371523" Y="2.998868894433" Z="0.9" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.9" />
                  <Point X="-3.372100229266" Y="3.135347165678" Z="0.9" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.9" />
                  <Point X="-4.060729761585" Y="3.035242731352" Z="0.9" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.9" />
                  <Point X="-4.427019268861" Y="2.470576251348" Z="0.9" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.9" />
                  <Point X="-4.245660618956" Y="2.032172041132" Z="0.9" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.9" />
                  <Point X="-3.677375048065" Y="1.573975767677" Z="0.9" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.9" />
                  <Point X="-3.682724226861" Y="1.515314053241" Z="0.9" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.9" />
                  <Point X="-3.731100181305" Y="1.481704745484" Z="0.9" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.9" />
                  <Point X="-4.130293955744" Y="1.524517923982" Z="0.9" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.9" />
                  <Point X="-4.917358356369" Y="1.242644757719" Z="0.9" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.9" />
                  <Point X="-5.029280685329" Y="0.656451415839" Z="0.9" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.9" />
                  <Point X="-4.533841191295" Y="0.305571561024" Z="0.9" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.9" />
                  <Point X="-3.558655432327" Y="0.036641729019" Z="0.9" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.9" />
                  <Point X="-3.542839322415" Y="0.010576421727" Z="0.9" />
                  <Point X="-3.539556741714" Y="0" Z="0.9" />
                  <Point X="-3.54552527872" Y="-0.019230529334" Z="0.9" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.9" />
                  <Point X="-3.566713162796" Y="-0.042234254115" Z="0.9" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.9" />
                  <Point X="-4.103046597478" Y="-0.190140495732" Z="0.9" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.9" />
                  <Point X="-5.010219768224" Y="-0.796987875213" Z="0.9" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.9" />
                  <Point X="-4.895074574115" Y="-1.332571239631" Z="0.9" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.9" />
                  <Point X="-4.269329603626" Y="-1.445120879055" Z="0.9" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.9" />
                  <Point X="-3.202073139283" Y="-1.316919236061" Z="0.9" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.9" />
                  <Point X="-3.197647010202" Y="-1.341641890819" Z="0.9" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.9" />
                  <Point X="-3.662554832155" Y="-1.70683576165" Z="0.9" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.9" />
                  <Point X="-4.313513915937" Y="-2.669228282569" Z="0.9" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.9" />
                  <Point X="-3.985406299781" Y="-3.138109415659" Z="0.9" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.9" />
                  <Point X="-3.404720379423" Y="-3.035777614465" Z="0.9" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.9" />
                  <Point X="-2.561646761156" Y="-2.566683707976" Z="0.9" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.9" />
                  <Point X="-2.819639398704" Y="-3.030358003069" Z="0.9" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.9" />
                  <Point X="-3.035761135808" Y="-4.065637063645" Z="0.9" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.9" />
                  <Point X="-2.607015380794" Y="-4.353013629449" Z="0.9" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.9" />
                  <Point X="-2.371317931202" Y="-4.345544451312" Z="0.9" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.9" />
                  <Point X="-2.059790356014" Y="-4.045245761269" Z="0.9" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.9" />
                  <Point X="-1.768518587409" Y="-3.997175863448" Z="0.9" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.9" />
                  <Point X="-1.508174100196" Y="-4.136354713063" Z="0.9" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.9" />
                  <Point X="-1.3863555314" Y="-4.405260181043" Z="0.9" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.9" />
                  <Point X="-1.381988655126" Y="-4.643196438317" Z="0.9" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.9" />
                  <Point X="-1.222324291237" Y="-4.928587937318" Z="0.9" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.9" />
                  <Point X="-0.924022823676" Y="-4.993119120482" Z="0.9" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="-0.675529531551" Y="-4.483294627226" Z="0.9" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="-0.311454696303" Y="-3.366577384944" Z="0.9" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="-0.089899583024" Y="-3.232140881405" Z="0.9" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.9" />
                  <Point X="0.163459496337" Y="-3.254971163883" Z="0.9" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.9" />
                  <Point X="0.358991163129" Y="-3.435068370823" Z="0.9" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.9" />
                  <Point X="0.55922532885" Y="-4.04924140259" Z="0.9" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.9" />
                  <Point X="0.934019250167" Y="-4.992626189725" Z="0.9" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.9" />
                  <Point X="1.113523084409" Y="-4.955681010205" Z="0.9" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.9" />
                  <Point X="1.099094115255" Y="-4.349599038199" Z="0.9" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.9" />
                  <Point X="0.992065073573" Y="-3.113177727123" Z="0.9" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.9" />
                  <Point X="1.127279993919" Y="-2.928776059759" Z="0.9" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.9" />
                  <Point X="1.341524134323" Y="-2.861837300187" Z="0.9" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.9" />
                  <Point X="1.56173115445" Y="-2.942626833519" Z="0.9" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.9" />
                  <Point X="2.000946365642" Y="-3.465088103477" Z="0.9" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.9" />
                  <Point X="2.788001075585" Y="-4.24512276054" Z="0.9" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.9" />
                  <Point X="2.979364785364" Y="-4.113077098128" Z="0.9" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.9" />
                  <Point X="2.771420971024" Y="-3.588642489748" Z="0.9" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.9" />
                  <Point X="2.246058659613" Y="-2.582883706757" Z="0.9" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.9" />
                  <Point X="2.293166116154" Y="-2.390388691565" Z="0.9" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.9" />
                  <Point X="2.442509686807" Y="-2.265735193814" Z="0.9" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.9" />
                  <Point X="2.645623096755" Y="-2.257389349878" Z="0.9" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.9" />
                  <Point X="3.198771107359" Y="-2.54632852925" Z="0.9" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.9" />
                  <Point X="4.177766227157" Y="-2.886450691191" Z="0.9" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.9" />
                  <Point X="4.342617910188" Y="-2.631919035607" Z="0.9" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.9" />
                  <Point X="3.97111745246" Y="-2.211860967753" Z="0.9" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.9" />
                  <Point X="3.127916009487" Y="-1.513759178623" Z="0.9" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.9" />
                  <Point X="3.102410342249" Y="-1.348023539944" Z="0.9" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.9" />
                  <Point X="3.178794961537" Y="-1.202217596435" Z="0.9" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.9" />
                  <Point X="3.334875258779" Y="-1.129923378055" Z="0.9" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.9" />
                  <Point X="3.93428056891" Y="-1.186351965634" Z="0.9" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.9" />
                  <Point X="4.961479446792" Y="-1.07570695375" Z="0.9" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.9" />
                  <Point X="5.027939956563" Y="-0.702315596748" Z="0.9" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.9" />
                  <Point X="4.58671304165" Y="-0.445555782837" Z="0.9" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.9" />
                  <Point X="3.688268098709" Y="-0.186311867707" Z="0.9" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.9" />
                  <Point X="3.61963195412" Y="-0.121706883039" Z="0.9" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.9" />
                  <Point X="3.587999803519" Y="-0.034280383793" Z="0.9" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.9" />
                  <Point X="3.593371646906" Y="0.062330147429" Z="0.9" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.9" />
                  <Point X="3.635747484281" Y="0.142241855536" Z="0.9" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.9" />
                  <Point X="3.715127315644" Y="0.201836976535" Z="0.9" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.9" />
                  <Point X="4.209254775657" Y="0.344416149932" Z="0.9" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.9" />
                  <Point X="5.005497020954" Y="0.842248090258" Z="0.9" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.9" />
                  <Point X="4.91673954577" Y="1.260974601134" Z="0.9" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.9" />
                  <Point X="4.377754717816" Y="1.342437879002" Z="0.9" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.9" />
                  <Point X="3.402371761167" Y="1.230052889837" Z="0.9" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.9" />
                  <Point X="3.324029696358" Y="1.25976079707" Z="0.9" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.9" />
                  <Point X="3.268313292827" Y="1.32079766212" Z="0.9" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.9" />
                  <Point X="3.239861523958" Y="1.401964057078" Z="0.9" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.9" />
                  <Point X="3.247478626443" Y="1.482004334961" Z="0.9" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.9" />
                  <Point X="3.29239526927" Y="1.55794746544" Z="0.9" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.9" />
                  <Point X="3.715422988955" Y="1.893563255489" Z="0.9" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.9" />
                  <Point X="4.31238897835" Y="2.678122845625" Z="0.9" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.9" />
                  <Point X="4.085973822133" Y="3.012284977761" Z="0.9" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.9" />
                  <Point X="3.472717750632" Y="2.822894571458" Z="0.9" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.9" />
                  <Point X="2.458079688258" Y="2.253147245835" Z="0.9" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.9" />
                  <Point X="2.384800863451" Y="2.250930114739" Z="0.9" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.9" />
                  <Point X="2.319321949117" Y="2.281615416156" Z="0.9" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.9" />
                  <Point X="2.269143184079" Y="2.337702911265" Z="0.9" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.9" />
                  <Point X="2.24849958802" Y="2.404957580382" Z="0.9" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.9" />
                  <Point X="2.259380696823" Y="2.481389887624" Z="0.9" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.9" />
                  <Point X="2.572730781401" Y="3.039421331323" Z="0.9" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.9" />
                  <Point X="2.886605039764" Y="4.174373513978" Z="0.9" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.9" />
                  <Point X="2.496891356311" Y="4.418531532472" Z="0.9" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.9" />
                  <Point X="2.090126214124" Y="4.624982712218" Z="0.9" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.9" />
                  <Point X="1.668354494139" Y="4.793296438064" Z="0.9" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.9" />
                  <Point X="1.094681435228" Y="4.950186377169" Z="0.9" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.9" />
                  <Point X="0.43073776201" Y="5.051451151392" Z="0.9" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.9" />
                  <Point X="0.124675455804" Y="4.820419632834" Z="0.9" />
                  <Point X="0" Y="4.355124473572" Z="0.9" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>