<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#195" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2883" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.87103527832" Y="20.339" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721313477" Y="21.502859375" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.41658480835" Y="21.71384765625" />
                  <Point X="0.378635528564" Y="21.768525390625" />
                  <Point X="0.356749328613" Y="21.790982421875" />
                  <Point X="0.330494598389" Y="21.810224609375" />
                  <Point X="0.302495330811" Y="21.824330078125" />
                  <Point X="0.107860710144" Y="21.884736328125" />
                  <Point X="0.049136188507" Y="21.902962890625" />
                  <Point X="0.020983469009" Y="21.907232421875" />
                  <Point X="-0.008657554626" Y="21.907234375" />
                  <Point X="-0.036823631287" Y="21.90296484375" />
                  <Point X="-0.231458251953" Y="21.842556640625" />
                  <Point X="-0.290182769775" Y="21.82433203125" />
                  <Point X="-0.318182800293" Y="21.810224609375" />
                  <Point X="-0.344439025879" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.768525390625" />
                  <Point X="-0.492101806641" Y="21.587302734375" />
                  <Point X="-0.530051208496" Y="21.532625" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.643838195801" Y="21.1409609375" />
                  <Point X="-0.916584838867" Y="20.12305859375" />
                  <Point X="-1.014143188477" Y="20.141994140625" />
                  <Point X="-1.079352416992" Y="20.15465234375" />
                  <Point X="-1.24641784668" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.263006958008" Y="20.717537109375" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323644897461" Y="20.868794921875" />
                  <Point X="-1.50283972168" Y="21.0259453125" />
                  <Point X="-1.556905761719" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886230469" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.88085925293" Y="21.12462109375" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983415161133" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657348633" Y="21.10519921875" />
                  <Point X="-2.240831298828" Y="20.972783203125" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312788330078" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.387229736328" Y="20.832603515625" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.706151123047" Y="20.851447265625" />
                  <Point X="-2.801726806641" Y="20.910623046875" />
                  <Point X="-3.104721923828" Y="21.143921875" />
                  <Point X="-2.995648681641" Y="21.332841796875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859375" Y="22.352345703125" />
                  <Point X="-2.406588134766" Y="22.38387109375" />
                  <Point X="-2.405574951172" Y="22.4148046875" />
                  <Point X="-2.414559082031" Y="22.444421875" />
                  <Point X="-2.428775146484" Y="22.47325" />
                  <Point X="-2.446805175781" Y="22.498412109375" />
                  <Point X="-2.464153808594" Y="22.515759765625" />
                  <Point X="-2.489305664062" Y="22.533783203125" />
                  <Point X="-2.518135009766" Y="22.548001953125" />
                  <Point X="-2.547754882812" Y="22.556986328125" />
                  <Point X="-2.578691162109" Y="22.555974609375" />
                  <Point X="-2.610218261719" Y="22.549703125" />
                  <Point X="-2.639183349609" Y="22.538802734375" />
                  <Point X="-2.938044677734" Y="22.366255859375" />
                  <Point X="-3.818024169922" Y="21.85819921875" />
                  <Point X="-4.007364013672" Y="22.106953125" />
                  <Point X="-4.082860839844" Y="22.206140625" />
                  <Point X="-4.304169433594" Y="22.577240234375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.106571777344" Y="22.733685546875" />
                  <Point X="-3.105954345703" Y="23.501484375" />
                  <Point X="-3.084577148438" Y="23.524404296875" />
                  <Point X="-3.066612548828" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.047937744141" Y="23.60301953125" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64033984375" />
                  <Point X="-3.045555664062" Y="23.672009765625" />
                  <Point X="-3.052556152344" Y="23.70175390625" />
                  <Point X="-3.068637939453" Y="23.72773828125" />
                  <Point X="-3.089469970703" Y="23.7516953125" />
                  <Point X="-3.112968994141" Y="23.77123046875" />
                  <Point X="-3.133313476562" Y="23.783205078125" />
                  <Point X="-3.139459960938" Y="23.786822265625" />
                  <Point X="-3.168735595703" Y="23.798048828125" />
                  <Point X="-3.200616210938" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.609213378906" Y="23.7559453125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.804549316406" Y="23.891744140625" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.672729980469" Y="24.474166015625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.517493896484" Y="24.785171875" />
                  <Point X="-3.487729248047" Y="24.800529296875" />
                  <Point X="-3.466408935547" Y="24.815326171875" />
                  <Point X="-3.459976318359" Y="24.819791015625" />
                  <Point X="-3.437517578125" Y="24.841677734375" />
                  <Point X="-3.418274658203" Y="24.86793359375" />
                  <Point X="-3.404168457031" Y="24.895931640625" />
                  <Point X="-3.397061523438" Y="24.918830078125" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390647949219" Y="24.953896484375" />
                  <Point X="-3.390647216797" Y="24.98353515625" />
                  <Point X="-3.394916503906" Y="25.0116953125" />
                  <Point X="-3.402023193359" Y="25.03459375" />
                  <Point X="-3.404167480469" Y="25.04150390625" />
                  <Point X="-3.418272949219" Y="25.069501953125" />
                  <Point X="-3.437516601563" Y="25.095759765625" />
                  <Point X="-3.459976318359" Y="25.1176484375" />
                  <Point X="-3.481296630859" Y="25.1324453125" />
                  <Point X="-3.490900878906" Y="25.13830078125" />
                  <Point X="-3.512902587891" Y="25.149986328125" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.876788574219" Y="25.25" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.843517089844" Y="25.848376953125" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.706623046875" Y="26.4119296875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.5827421875" Y="26.407361328125" />
                  <Point X="-3.765666748047" Y="26.29979296875" />
                  <Point X="-3.744984375" Y="26.299341796875" />
                  <Point X="-3.723424316406" Y="26.301228515625" />
                  <Point X="-3.703141601563" Y="26.305263671875" />
                  <Point X="-3.655953125" Y="26.320140625" />
                  <Point X="-3.641710693359" Y="26.324630859375" />
                  <Point X="-3.622770751953" Y="26.33296484375" />
                  <Point X="-3.604028320312" Y="26.343787109375" />
                  <Point X="-3.587347167969" Y="26.35601953125" />
                  <Point X="-3.573709228516" Y="26.371572265625" />
                  <Point X="-3.561296142578" Y="26.389302734375" />
                  <Point X="-3.551351318359" Y="26.407431640625" />
                  <Point X="-3.532416748047" Y="26.453142578125" />
                  <Point X="-3.526703857422" Y="26.466935546875" />
                  <Point X="-3.520915283203" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.515804443359" Y="26.52875" />
                  <Point X="-3.518951660156" Y="26.5491953125" />
                  <Point X="-3.524554443359" Y="26.570103515625" />
                  <Point X="-3.532050537109" Y="26.58937890625" />
                  <Point X="-3.554897216797" Y="26.633267578125" />
                  <Point X="-3.561790283203" Y="26.6465078125" />
                  <Point X="-3.573284423828" Y="26.663708984375" />
                  <Point X="-3.587196044922" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.799404785156" Y="26.8459609375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.155092285156" Y="27.606984375" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.768934570312" Y="28.13497265625" />
                  <Point X="-3.750504150391" Y="28.158662109375" />
                  <Point X="-3.708481933594" Y="28.134400390625" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187727783203" Y="27.836341796875" />
                  <Point X="-3.167084472656" Y="27.82983203125" />
                  <Point X="-3.146793701172" Y="27.825794921875" />
                  <Point X="-3.081073242188" Y="27.820044921875" />
                  <Point X="-3.061244384766" Y="27.818310546875" />
                  <Point X="-3.040559082031" Y="27.81876171875" />
                  <Point X="-3.019101318359" Y="27.821587890625" />
                  <Point X="-2.999012207031" Y="27.826505859375" />
                  <Point X="-2.980462890625" Y="27.83565234375" />
                  <Point X="-2.962209228516" Y="27.84728125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.899429199219" Y="27.906876953125" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.849185546875" Y="28.10183984375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.957211181641" Y="28.33294140625" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.8293984375" Y="28.995953125" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.208892089844" Y="29.36787890625" />
                  <Point X="-2.167035888672" Y="29.391134765625" />
                  <Point X="-2.043196289063" Y="29.229744140625" />
                  <Point X="-2.028896118164" Y="29.2148046875" />
                  <Point X="-2.012318481445" Y="29.200892578125" />
                  <Point X="-1.995115600586" Y="29.189396484375" />
                  <Point X="-1.921969116211" Y="29.151318359375" />
                  <Point X="-1.899899780273" Y="29.139828125" />
                  <Point X="-1.880626342773" Y="29.13233203125" />
                  <Point X="-1.85971862793" Y="29.126728515625" />
                  <Point X="-1.839269775391" Y="29.123580078125" />
                  <Point X="-1.818624267578" Y="29.12493359375" />
                  <Point X="-1.797307739258" Y="29.128693359375" />
                  <Point X="-1.777453125" Y="29.134482421875" />
                  <Point X="-1.701266235352" Y="29.166041015625" />
                  <Point X="-1.678279174805" Y="29.1755625" />
                  <Point X="-1.660146728516" Y="29.185509765625" />
                  <Point X="-1.642416870117" Y="29.197923828125" />
                  <Point X="-1.626864501953" Y="29.2115625" />
                  <Point X="-1.6146328125" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265919921875" />
                  <Point X="-1.570682617188" Y="29.34456640625" />
                  <Point X="-1.563200927734" Y="29.368296875" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.567668579102" Y="29.506314453125" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.11633996582" Y="29.763068359375" />
                  <Point X="-0.949623596191" Y="29.80980859375" />
                  <Point X="-0.35350881958" Y="29.879576171875" />
                  <Point X="-0.294711242676" Y="29.886458984375" />
                  <Point X="-0.282357055664" Y="29.8403515625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155908585" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.19100617981" Y="29.4534765625" />
                  <Point X="0.307419555664" Y="29.8879375" />
                  <Point X="0.698476135254" Y="29.846982421875" />
                  <Point X="0.844045532227" Y="29.83173828125" />
                  <Point X="1.337235717773" Y="29.712666015625" />
                  <Point X="1.4810234375" Y="29.677951171875" />
                  <Point X="1.801706787109" Y="29.561638671875" />
                  <Point X="1.894647216797" Y="29.52792578125" />
                  <Point X="2.205060058594" Y="29.3827578125" />
                  <Point X="2.294556640625" Y="29.34090234375" />
                  <Point X="2.59447265625" Y="29.166171875" />
                  <Point X="2.680990234375" Y="29.115767578125" />
                  <Point X="2.943260253906" Y="28.929255859375" />
                  <Point X="2.809497802734" Y="28.6975703125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.116583496094" Y="27.454376953125" />
                  <Point X="2.114676513672" Y="27.44560546875" />
                  <Point X="2.108362548828" Y="27.408095703125" />
                  <Point X="2.107728027344" Y="27.380953125" />
                  <Point X="2.114159179688" Y="27.32762109375" />
                  <Point X="2.116099365234" Y="27.311529296875" />
                  <Point X="2.121441894531" Y="27.289607421875" />
                  <Point X="2.129708251953" Y="27.267517578125" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.173071777344" Y="27.1988359375" />
                  <Point X="2.178630371094" Y="27.191447265625" />
                  <Point X="2.20188671875" Y="27.16347265625" />
                  <Point X="2.221597167969" Y="27.145591796875" />
                  <Point X="2.270231933594" Y="27.11258984375" />
                  <Point X="2.284905761719" Y="27.1026328125" />
                  <Point X="2.304949707031" Y="27.092271484375" />
                  <Point X="2.327035400391" Y="27.084005859375" />
                  <Point X="2.348963134766" Y="27.078662109375" />
                  <Point X="2.402296386719" Y="27.072232421875" />
                  <Point X="2.412029052734" Y="27.0715625" />
                  <Point X="2.447027099609" Y="27.070958984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.534885253906" Y="27.0906640625" />
                  <Point X="2.539715332031" Y="27.09209375" />
                  <Point X="2.570406982422" Y="27.102072265625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="2.934443603516" Y="27.309857421875" />
                  <Point X="3.967325195312" Y="27.90619140625" />
                  <Point X="4.071958740234" Y="27.7607734375" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.26219921875" Y="27.459884765625" />
                  <Point X="4.102259277344" Y="27.337158203125" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221425048828" Y="26.6602421875" />
                  <Point X="3.203973632812" Y="26.641626953125" />
                  <Point X="3.159584228516" Y="26.58371875" />
                  <Point X="3.14619140625" Y="26.56624609375" />
                  <Point X="3.136602539062" Y="26.55090625" />
                  <Point X="3.121629882812" Y="26.517083984375" />
                  <Point X="3.105094726562" Y="26.457958984375" />
                  <Point X="3.100105957031" Y="26.440119140625" />
                  <Point X="3.09665234375" Y="26.417818359375" />
                  <Point X="3.095836669922" Y="26.39425" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.111312988281" Y="26.305982421875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679931641" Y="26.2689765625" />
                  <Point X="3.136283203125" Y="26.23573828125" />
                  <Point X="3.173201904297" Y="26.179623046875" />
                  <Point X="3.184340820312" Y="26.162693359375" />
                  <Point X="3.198891601562" Y="26.145451171875" />
                  <Point X="3.216132080078" Y="26.12936328125" />
                  <Point X="3.234345214844" Y="26.11603515625" />
                  <Point X="3.287845214844" Y="26.085919921875" />
                  <Point X="3.296028564453" Y="26.081818359375" />
                  <Point X="3.330147949219" Y="26.066732421875" />
                  <Point X="3.356119384766" Y="26.0594375" />
                  <Point X="3.428455078125" Y="26.04987890625" />
                  <Point X="3.450280029297" Y="26.046994140625" />
                  <Point X="3.462697021484" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="3.816794677734" Y="26.090244140625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.823897460938" Y="26.0233359375" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.715309082031" Y="25.59719921875" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704795166016" Y="25.325587890625" />
                  <Point X="3.681548339844" Y="25.315068359375" />
                  <Point X="3.610480712891" Y="25.273990234375" />
                  <Point X="3.589038330078" Y="25.26159765625" />
                  <Point X="3.574313232422" Y="25.251099609375" />
                  <Point X="3.547530761719" Y="25.225576171875" />
                  <Point X="3.504890136719" Y="25.171240234375" />
                  <Point X="3.492024902344" Y="25.15484765625" />
                  <Point X="3.480301757812" Y="25.1355703125" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.449467285156" Y="25.018384765625" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443483154297" Y="24.978123046875" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.459392578125" Y="24.867228515625" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526123047" Y="24.823337890625" />
                  <Point X="3.480298828125" Y="24.801875" />
                  <Point X="3.492024169922" Y="24.782591796875" />
                  <Point X="3.534664794922" Y="24.7282578125" />
                  <Point X="3.547530029297" Y="24.71186328125" />
                  <Point X="3.559994140625" Y="24.698767578125" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.660103271484" Y="24.634765625" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692709716797" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.017912841797" Y="24.52710546875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.867328125" Y="24.132892578125" />
                  <Point X="4.855021484375" Y="24.051267578125" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.582626953125" Y="23.844072265625" />
                  <Point X="3.424381591797" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659423828" Y="23.994490234375" />
                  <Point X="3.235178710938" Y="23.964173828125" />
                  <Point X="3.193095214844" Y="23.95502734375" />
                  <Point X="3.163974121094" Y="23.94340234375" />
                  <Point X="3.136147460938" Y="23.926509765625" />
                  <Point X="3.112397705078" Y="23.9060390625" />
                  <Point X="3.028090332031" Y="23.804642578125" />
                  <Point X="3.002653564453" Y="23.77405078125" />
                  <Point X="2.987933837891" Y="23.749671875" />
                  <Point X="2.976590087891" Y="23.722287109375" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.957674316406" Y="23.563322265625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.95634765625" Y="23.49243359375" />
                  <Point X="2.964079589844" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.053641601562" Y="23.3119375" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930419922" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.390267333984" Y="23.024515625" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.159497558594" Y="22.30634375" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.028981445312" Y="22.1140546875" />
                  <Point X="3.832528076172" Y="22.2274765625" />
                  <Point X="2.800954345703" Y="22.823056640625" />
                  <Point X="2.786137939453" Y="22.829984375" />
                  <Point X="2.754224365234" Y="22.84017578125" />
                  <Point X="2.588220214844" Y="22.87015625" />
                  <Point X="2.538134033203" Y="22.879201171875" />
                  <Point X="2.506777587891" Y="22.879603515625" />
                  <Point X="2.47460546875" Y="22.87464453125" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.306924804688" Y="22.7922421875" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.131951660156" Y="22.57165234375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.436740234375" />
                  <Point X="2.125655761719" Y="22.270736328125" />
                  <Point X="2.134701171875" Y="22.2206484375" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.331515136719" Y="21.862677734375" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.823012695312" Y="20.9177578125" />
                  <Point X="2.781869140625" Y="20.88837109375" />
                  <Point X="2.701763183594" Y="20.83651953125" />
                  <Point X="2.545732177734" Y="21.03986328125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.558198608398" Y="22.204703125" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986694336" Y="22.24883203125" />
                  <Point X="1.448366943359" Y="22.2565625" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.238039550781" Y="22.242404296875" />
                  <Point X="1.184013916016" Y="22.23743359375" />
                  <Point X="1.156364135742" Y="22.230603515625" />
                  <Point X="1.128977661133" Y="22.219259765625" />
                  <Point X="1.104594970703" Y="22.204537109375" />
                  <Point X="0.966328552246" Y="22.089572265625" />
                  <Point X="0.924611328125" Y="22.054884765625" />
                  <Point X="0.904140441895" Y="22.031134765625" />
                  <Point X="0.887248596191" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.834283325195" Y="21.783990234375" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.870666687012" Y="21.2900703125" />
                  <Point X="1.022065551758" Y="20.140083984375" />
                  <Point X="1.01461895752" Y="20.138451171875" />
                  <Point X="0.975710571289" Y="20.129921875" />
                  <Point X="0.929315551758" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-0.996041992188" Y="20.23525390625" />
                  <Point X="-1.058443847656" Y="20.2473671875" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.169832397461" Y="20.7360703125" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575317383" Y="20.905138671875" />
                  <Point X="-1.250209594727" Y="20.9290625" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.440201660156" Y="21.097369140625" />
                  <Point X="-1.494267700195" Y="21.144783203125" />
                  <Point X="-1.506735717773" Y="21.15403125" />
                  <Point X="-1.533019165039" Y="21.170376953125" />
                  <Point X="-1.546834594727" Y="21.177474609375" />
                  <Point X="-1.576531738281" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.621457397461" Y="21.201552734375" />
                  <Point X="-1.636814575195" Y="21.203830078125" />
                  <Point X="-1.874646240234" Y="21.21941796875" />
                  <Point X="-1.946403686523" Y="21.22412109375" />
                  <Point X="-1.961927978516" Y="21.2238671875" />
                  <Point X="-1.992726318359" Y="21.220833984375" />
                  <Point X="-2.008000366211" Y="21.2180546875" />
                  <Point X="-2.039048583984" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.293610595703" Y="21.0517734375" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359688232422" Y="21.00723828125" />
                  <Point X="-2.380446289062" Y="20.989865234375" />
                  <Point X="-2.402760742188" Y="20.9672265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.462598388672" Y="20.890435546875" />
                  <Point X="-2.503200439453" Y="20.837521484375" />
                  <Point X="-2.656140136719" Y="20.93221875" />
                  <Point X="-2.747613037109" Y="20.988853515625" />
                  <Point X="-2.98086328125" Y="21.168451171875" />
                  <Point X="-2.913376220703" Y="21.285341796875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850341797" Y="22.289916015625" />
                  <Point X="-2.323948486328" Y="22.318880859375" />
                  <Point X="-2.319685058594" Y="22.333810546875" />
                  <Point X="-2.313413818359" Y="22.3653359375" />
                  <Point X="-2.311639160156" Y="22.38076171875" />
                  <Point X="-2.310625976562" Y="22.4116953125" />
                  <Point X="-2.314665527344" Y="22.442380859375" />
                  <Point X="-2.323649658203" Y="22.471998046875" />
                  <Point X="-2.329355712891" Y="22.4864375" />
                  <Point X="-2.343571777344" Y="22.515265625" />
                  <Point X="-2.351553466797" Y="22.528583984375" />
                  <Point X="-2.369583496094" Y="22.55374609375" />
                  <Point X="-2.379631835938" Y="22.56558984375" />
                  <Point X="-2.39698046875" Y="22.5829375" />
                  <Point X="-2.408818603516" Y="22.59298046875" />
                  <Point X="-2.433970458984" Y="22.61100390625" />
                  <Point X="-2.447284179688" Y="22.618984375" />
                  <Point X="-2.476113525391" Y="22.633203125" />
                  <Point X="-2.490560058594" Y="22.638912109375" />
                  <Point X="-2.520179931641" Y="22.647896484375" />
                  <Point X="-2.550860107422" Y="22.651935546875" />
                  <Point X="-2.581796386719" Y="22.650923828125" />
                  <Point X="-2.597225830078" Y="22.6491484375" />
                  <Point X="-2.628752929688" Y="22.642876953125" />
                  <Point X="-2.643678466797" Y="22.638615234375" />
                  <Point X="-2.672643554688" Y="22.62771484375" />
                  <Point X="-2.686683105469" Y="22.621076171875" />
                  <Point X="-2.985544433594" Y="22.448529296875" />
                  <Point X="-3.793089111328" Y="21.98229296875" />
                  <Point X="-3.931770507813" Y="22.1644921875" />
                  <Point X="-4.004020751953" Y="22.2594140625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-4.048739257812" Y="22.65831640625" />
                  <Point X="-3.048121826172" Y="23.426115234375" />
                  <Point X="-3.036481933594" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005367431641" Y="23.471955078125" />
                  <Point X="-2.987402832031" Y="23.4990859375" />
                  <Point X="-2.979835693359" Y="23.512873046875" />
                  <Point X="-2.967079589844" Y="23.54150390625" />
                  <Point X="-2.961890625" Y="23.55634765625" />
                  <Point X="-2.955971923828" Y="23.579201171875" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.63162109375" />
                  <Point X="-2.948577636719" Y="23.646947265625" />
                  <Point X="-2.950785644531" Y="23.6786171875" />
                  <Point X="-2.953082275391" Y="23.6937734375" />
                  <Point X="-2.960082763672" Y="23.723517578125" />
                  <Point X="-2.971775634766" Y="23.75175" />
                  <Point X="-2.987857421875" Y="23.777734375" />
                  <Point X="-2.996950195312" Y="23.79007421875" />
                  <Point X="-3.017782226563" Y="23.81403125" />
                  <Point X="-3.028739501953" Y="23.824748046875" />
                  <Point X="-3.052238525391" Y="23.844283203125" />
                  <Point X="-3.064780273438" Y="23.8531015625" />
                  <Point X="-3.085124755859" Y="23.865076171875" />
                  <Point X="-3.105444824219" Y="23.8755234375" />
                  <Point X="-3.134720458984" Y="23.88675" />
                  <Point X="-3.149822509766" Y="23.891146484375" />
                  <Point X="-3.181703125" Y="23.897623046875" />
                  <Point X="-3.197311767578" Y="23.89946875" />
                  <Point X="-3.228624755859" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.62161328125" Y="23.8501328125" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.712504394531" Y="23.915255859375" />
                  <Point X="-4.740762695312" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.648142089844" Y="24.38240234375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.500468994141" Y="24.6902890625" />
                  <Point X="-3.473934082031" Y="24.700748046875" />
                  <Point X="-3.444169433594" Y="24.71610546875" />
                  <Point X="-3.433563720703" Y="24.722484375" />
                  <Point X="-3.412243408203" Y="24.73728125" />
                  <Point X="-3.393673095703" Y="24.751755859375" />
                  <Point X="-3.371214355469" Y="24.773642578125" />
                  <Point X="-3.360893310547" Y="24.78551953125" />
                  <Point X="-3.341650390625" Y="24.811775390625" />
                  <Point X="-3.333434326172" Y="24.825189453125" />
                  <Point X="-3.319328125" Y="24.8531875" />
                  <Point X="-3.313437988281" Y="24.867771484375" />
                  <Point X="-3.306331054688" Y="24.890669921875" />
                  <Point X="-3.300990966797" Y="24.91149609375" />
                  <Point X="-3.296721435547" Y="24.939654296875" />
                  <Point X="-3.295647949219" Y="24.95389453125" />
                  <Point X="-3.295647216797" Y="24.983533203125" />
                  <Point X="-3.296720458984" Y="24.997775390625" />
                  <Point X="-3.300989746094" Y="25.025935546875" />
                  <Point X="-3.304185791016" Y="25.039853515625" />
                  <Point X="-3.311292480469" Y="25.062751953125" />
                  <Point X="-3.319326416016" Y="25.08424609375" />
                  <Point X="-3.333431884766" Y="25.112244140625" />
                  <Point X="-3.341647705078" Y="25.125658203125" />
                  <Point X="-3.360891357422" Y="25.151916015625" />
                  <Point X="-3.371211914062" Y="25.163794921875" />
                  <Point X="-3.393671630859" Y="25.18568359375" />
                  <Point X="-3.405810791016" Y="25.195693359375" />
                  <Point X="-3.427131103516" Y="25.210490234375" />
                  <Point X="-3.446339599609" Y="25.222201171875" />
                  <Point X="-3.468341308594" Y="25.23388671875" />
                  <Point X="-3.478102294922" Y="25.2383828125" />
                  <Point X="-3.498075927734" Y="25.24624609375" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.852200927734" Y="25.341763671875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.749540527344" Y="25.834470703125" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-4.595143554688" Y="26.313173828125" />
                  <Point X="-3.778067871094" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747056152344" Y="26.204365234375" />
                  <Point X="-3.736702636719" Y="26.204703125" />
                  <Point X="-3.715142578125" Y="26.20658984375" />
                  <Point X="-3.704887695313" Y="26.2080546875" />
                  <Point X="-3.684604980469" Y="26.21208984375" />
                  <Point X="-3.674577148438" Y="26.21466015625" />
                  <Point X="-3.627388671875" Y="26.229537109375" />
                  <Point X="-3.603448974609" Y="26.23767578125" />
                  <Point X="-3.584509033203" Y="26.246009765625" />
                  <Point X="-3.575266357422" Y="26.2506953125" />
                  <Point X="-3.556523925781" Y="26.261517578125" />
                  <Point X="-3.547850097656" Y="26.267177734375" />
                  <Point X="-3.531168945312" Y="26.27941015625" />
                  <Point X="-3.515919189453" Y="26.293384765625" />
                  <Point X="-3.50228125" Y="26.3089375" />
                  <Point X="-3.495885742188" Y="26.317087890625" />
                  <Point X="-3.48347265625" Y="26.334818359375" />
                  <Point X="-3.478005126953" Y="26.34361328125" />
                  <Point X="-3.468060302734" Y="26.3617421875" />
                  <Point X="-3.463583007813" Y="26.371076171875" />
                  <Point X="-3.4446484375" Y="26.416787109375" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429710693359" Y="26.4602109375" />
                  <Point X="-3.427358398438" Y="26.470298828125" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.501896484375" />
                  <Point X="-3.4210078125" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057617188" Y="26.5636484375" />
                  <Point X="-3.427189208984" Y="26.57378515625" />
                  <Point X="-3.432791992188" Y="26.594693359375" />
                  <Point X="-3.436014160156" Y="26.604537109375" />
                  <Point X="-3.443510253906" Y="26.6238125" />
                  <Point X="-3.447784179688" Y="26.633244140625" />
                  <Point X="-3.470630859375" Y="26.6771328125" />
                  <Point X="-3.482802001953" Y="26.6992890625" />
                  <Point X="-3.494296142578" Y="26.716490234375" />
                  <Point X="-3.500512207031" Y="26.724775390625" />
                  <Point X="-3.514423828125" Y="26.741353515625" />
                  <Point X="-3.521499755859" Y="26.74891015625" />
                  <Point X="-3.536439453125" Y="26.763212890625" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.741572265625" Y="26.921330078125" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.073045898438" Y="27.559095703125" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.726337646484" Y="28.035013671875" />
                  <Point X="-3.254157470703" Y="27.762400390625" />
                  <Point X="-3.244921630859" Y="27.75771875" />
                  <Point X="-3.2259921875" Y="27.749388671875" />
                  <Point X="-3.216298583984" Y="27.745740234375" />
                  <Point X="-3.195655273438" Y="27.73923046875" />
                  <Point X="-3.185622558594" Y="27.736658203125" />
                  <Point X="-3.165331787109" Y="27.73262109375" />
                  <Point X="-3.155073730469" Y="27.73115625" />
                  <Point X="-3.089353271484" Y="27.72540625" />
                  <Point X="-3.059172851562" Y="27.723333984375" />
                  <Point X="-3.038487548828" Y="27.72378515625" />
                  <Point X="-3.028153808594" Y="27.72457421875" />
                  <Point X="-3.006696044922" Y="27.727400390625" />
                  <Point X="-2.99651171875" Y="27.7293125" />
                  <Point X="-2.976422607422" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.93844921875" Y="27.750447265625" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911165527344" Y="27.767158203125" />
                  <Point X="-2.902744628906" Y="27.773193359375" />
                  <Point X="-2.886613525391" Y="27.786140625" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.832254394531" Y="27.839701171875" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.754547119141" Y="28.110119140625" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.874938964844" Y="28.38044140625" />
                  <Point X="-3.059386962891" Y="28.6999140625" />
                  <Point X="-2.771596191406" Y="28.920560546875" />
                  <Point X="-2.648368652344" Y="29.015037109375" />
                  <Point X="-2.192522460938" Y="29.268294921875" />
                  <Point X="-2.118564697266" Y="29.171912109375" />
                  <Point X="-2.111823730469" Y="29.164052734375" />
                  <Point X="-2.0975234375" Y="29.14911328125" />
                  <Point X="-2.089965576172" Y="29.14203515625" />
                  <Point X="-2.073387939453" Y="29.128123046875" />
                  <Point X="-2.065102294922" Y="29.12190625" />
                  <Point X="-2.047899414062" Y="29.11041015625" />
                  <Point X="-2.038982299805" Y="29.105130859375" />
                  <Point X="-1.965835693359" Y="29.067052734375" />
                  <Point X="-1.943766357422" Y="29.0555625" />
                  <Point X="-1.934335693359" Y="29.0512890625" />
                  <Point X="-1.915062255859" Y="29.04379296875" />
                  <Point X="-1.905219604492" Y="29.0405703125" />
                  <Point X="-1.884311767578" Y="29.034966796875" />
                  <Point X="-1.874175048828" Y="29.032833984375" />
                  <Point X="-1.853726196289" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812409423828" Y="29.03013671875" />
                  <Point X="-1.802123168945" Y="29.031376953125" />
                  <Point X="-1.780806640625" Y="29.03513671875" />
                  <Point X="-1.770715698242" Y="29.037490234375" />
                  <Point X="-1.750861206055" Y="29.043279296875" />
                  <Point X="-1.741097167969" Y="29.04671484375" />
                  <Point X="-1.664910400391" Y="29.0782734375" />
                  <Point X="-1.641923339844" Y="29.087794921875" />
                  <Point X="-1.632587158203" Y="29.092271484375" />
                  <Point X="-1.614454711914" Y="29.10221875" />
                  <Point X="-1.605658447266" Y="29.107689453125" />
                  <Point X="-1.587928588867" Y="29.120103515625" />
                  <Point X="-1.579780029297" Y="29.126498046875" />
                  <Point X="-1.564227661133" Y="29.14013671875" />
                  <Point X="-1.550252685547" Y="29.15538671875" />
                  <Point X="-1.538020996094" Y="29.172068359375" />
                  <Point X="-1.532360351562" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.516857543945" Y="29.208724609375" />
                  <Point X="-1.508526733398" Y="29.22765625" />
                  <Point X="-1.504877075195" Y="29.2373515625" />
                  <Point X="-1.480079589844" Y="29.315998046875" />
                  <Point X="-1.472597900391" Y="29.339728515625" />
                  <Point X="-1.470026000977" Y="29.349765625" />
                  <Point X="-1.465990600586" Y="29.3700546875" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.473481323242" Y="29.51871484375" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.090694213867" Y="29.671595703125" />
                  <Point X="-0.931164001465" Y="29.7163203125" />
                  <Point X="-0.365222473145" Y="29.782556640625" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.282769195557" Y="29.428888671875" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.688581054688" Y="29.7525" />
                  <Point X="0.827865783691" Y="29.7379140625" />
                  <Point X="1.314940185547" Y="29.620318359375" />
                  <Point X="1.453608642578" Y="29.58683984375" />
                  <Point X="1.769314941406" Y="29.47233203125" />
                  <Point X="1.858249755859" Y="29.440072265625" />
                  <Point X="2.164815673828" Y="29.296703125" />
                  <Point X="2.250428222656" Y="29.2566640625" />
                  <Point X="2.546649902344" Y="29.0840859375" />
                  <Point X="2.629447509766" Y="29.035849609375" />
                  <Point X="2.817780029297" Y="28.90191796875" />
                  <Point X="2.727225097656" Y="28.7450703125" />
                  <Point X="2.065308349609" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.024808227539" Y="27.47891796875" />
                  <Point X="2.020994628906" Y="27.461375" />
                  <Point X="2.014680541992" Y="27.423865234375" />
                  <Point X="2.013388549805" Y="27.41031640625" />
                  <Point X="2.01275402832" Y="27.383173828125" />
                  <Point X="2.013411254883" Y="27.369580078125" />
                  <Point X="2.019842407227" Y="27.316248046875" />
                  <Point X="2.02380078125" Y="27.28903515625" />
                  <Point X="2.029143310547" Y="27.26711328125" />
                  <Point X="2.032467651367" Y="27.2563125" />
                  <Point X="2.040734130859" Y="27.23422265625" />
                  <Point X="2.045316772461" Y="27.223892578125" />
                  <Point X="2.0556796875" Y="27.203845703125" />
                  <Point X="2.061459716797" Y="27.19412890625" />
                  <Point X="2.094460449219" Y="27.145494140625" />
                  <Point X="2.105577636719" Y="27.130716796875" />
                  <Point X="2.128833984375" Y="27.1027421875" />
                  <Point X="2.138056640625" Y="27.093111328125" />
                  <Point X="2.157767089844" Y="27.07523046875" />
                  <Point X="2.168254882812" Y="27.06698046875" />
                  <Point X="2.216889648438" Y="27.033978515625" />
                  <Point X="2.24128125" Y="27.0182421875" />
                  <Point X="2.261325195312" Y="27.007880859375" />
                  <Point X="2.271651367188" Y="27.003298828125" />
                  <Point X="2.293737060547" Y="26.995033203125" />
                  <Point X="2.304542236328" Y="26.99170703125" />
                  <Point X="2.326469970703" Y="26.98636328125" />
                  <Point X="2.337592529297" Y="26.984345703125" />
                  <Point X="2.39092578125" Y="26.977916015625" />
                  <Point X="2.410391113281" Y="26.976576171875" />
                  <Point X="2.445389160156" Y="26.97597265625" />
                  <Point X="2.458591552734" Y="26.976666015625" />
                  <Point X="2.484772460938" Y="26.979876953125" />
                  <Point X="2.497750976562" Y="26.98239453125" />
                  <Point X="2.559428222656" Y="26.998888671875" />
                  <Point X="2.569088378906" Y="27.001748046875" />
                  <Point X="2.599780029297" Y="27.0117265625" />
                  <Point X="2.609061279297" Y="27.015291015625" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="2.981943603516" Y="27.2275859375" />
                  <Point X="3.94040234375" Y="27.780951171875" />
                  <Point X="3.994845947266" Y="27.705287109375" />
                  <Point X="4.043961181641" Y="27.637029296875" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="4.044426757813" Y="27.41252734375" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168139892578" Y="26.73987109375" />
                  <Point X="3.152118408203" Y="26.725216796875" />
                  <Point X="3.134666992188" Y="26.7066015625" />
                  <Point X="3.128576660156" Y="26.699421875" />
                  <Point X="3.084187255859" Y="26.641513671875" />
                  <Point X="3.070794433594" Y="26.624041015625" />
                  <Point X="3.065635009766" Y="26.6166015625" />
                  <Point X="3.049733886719" Y="26.589361328125" />
                  <Point X="3.034761230469" Y="26.5555390625" />
                  <Point X="3.030140380859" Y="26.542669921875" />
                  <Point X="3.013605224609" Y="26.483544921875" />
                  <Point X="3.006225097656" Y="26.454658203125" />
                  <Point X="3.002771484375" Y="26.432357421875" />
                  <Point X="3.001709228516" Y="26.421103515625" />
                  <Point X="3.000893554688" Y="26.39753515625" />
                  <Point X="3.001175048828" Y="26.38623828125" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.00469921875" Y="26.3525703125" />
                  <Point X="3.018272705078" Y="26.28678515625" />
                  <Point X="3.022368164062" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034684082031" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.195369140625" />
                  <Point X="3.056919189453" Y="26.1835234375" />
                  <Point X="3.093837890625" Y="26.127408203125" />
                  <Point X="3.104976806641" Y="26.110478515625" />
                  <Point X="3.111738769531" Y="26.101423828125" />
                  <Point X="3.126289550781" Y="26.084181640625" />
                  <Point X="3.134078369141" Y="26.075994140625" />
                  <Point X="3.151318847656" Y="26.05990625" />
                  <Point X="3.160029785156" Y="26.05269921875" />
                  <Point X="3.178242919922" Y="26.03937109375" />
                  <Point X="3.187745117188" Y="26.03325" />
                  <Point X="3.241245117188" Y="26.003134765625" />
                  <Point X="3.257611816406" Y="25.994931640625" />
                  <Point X="3.291731201172" Y="25.979845703125" />
                  <Point X="3.304458251953" Y="25.975271484375" />
                  <Point X="3.3304296875" Y="25.9679765625" />
                  <Point X="3.343674072266" Y="25.965255859375" />
                  <Point X="3.416009765625" Y="25.955697265625" />
                  <Point X="3.437834716797" Y="25.9528125" />
                  <Point X="3.444032470703" Y="25.95219921875" />
                  <Point X="3.46570703125" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="3.829194580078" Y="25.996056640625" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.731593261719" Y="26.000865234375" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.690720703125" Y="25.688962890625" />
                  <Point X="3.691991455078" Y="25.421353515625" />
                  <Point X="3.686032226563" Y="25.419544921875" />
                  <Point X="3.665629638672" Y="25.412138671875" />
                  <Point X="3.6423828125" Y="25.401619140625" />
                  <Point X="3.634007324219" Y="25.39731640625" />
                  <Point X="3.562939697266" Y="25.35623828125" />
                  <Point X="3.541497314453" Y="25.343845703125" />
                  <Point X="3.533889892578" Y="25.338951171875" />
                  <Point X="3.508773925781" Y="25.31987109375" />
                  <Point X="3.481991455078" Y="25.29434765625" />
                  <Point X="3.472795898438" Y="25.284224609375" />
                  <Point X="3.430155273438" Y="25.229888671875" />
                  <Point X="3.417290039063" Y="25.21349609375" />
                  <Point X="3.410855712891" Y="25.204208984375" />
                  <Point X="3.399132568359" Y="25.184931640625" />
                  <Point X="3.39384375" Y="25.17494140625" />
                  <Point X="3.384069335938" Y="25.1534765625" />
                  <Point X="3.380005371094" Y="25.142927734375" />
                  <Point X="3.373158935547" Y="25.12142578125" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.356162841797" Y="25.03625390625" />
                  <Point X="3.351874267578" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584472656" Y="24.973736328125" />
                  <Point X="3.350280273438" Y="24.93705859375" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.366088378906" Y="24.849359375" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.816013671875" />
                  <Point X="3.38000390625" Y="24.794515625" />
                  <Point X="3.384066894531" Y="24.783970703125" />
                  <Point X="3.393839599609" Y="24.7625078125" />
                  <Point X="3.399126953125" Y="24.752517578125" />
                  <Point X="3.410852294922" Y="24.733234375" />
                  <Point X="3.417290283203" Y="24.72394140625" />
                  <Point X="3.459930908203" Y="24.669607421875" />
                  <Point X="3.472796142578" Y="24.653212890625" />
                  <Point X="3.478716064453" Y="24.6463671875" />
                  <Point X="3.501133544922" Y="24.62419921875" />
                  <Point X="3.530174804688" Y="24.601275390625" />
                  <Point X="3.541494628906" Y="24.593595703125" />
                  <Point X="3.6125625" Y="24.552517578125" />
                  <Point X="3.634004638672" Y="24.540123046875" />
                  <Point X="3.639490478516" Y="24.5371875" />
                  <Point X="3.659155761719" Y="24.527982421875" />
                  <Point X="3.683025878906" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="3.993324951172" Y="24.435341796875" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.773389648438" Y="24.1470546875" />
                  <Point X="4.761612304688" Y="24.068939453125" />
                  <Point X="4.727802246094" Y="23.920779296875" />
                  <Point X="4.595026855469" Y="23.938259765625" />
                  <Point X="3.43678125" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720947266" Y="24.089158203125" />
                  <Point X="3.354482177734" Y="24.087322265625" />
                  <Point X="3.215001464844" Y="24.057005859375" />
                  <Point X="3.17291796875" Y="24.047859375" />
                  <Point X="3.157874267578" Y="24.0432578125" />
                  <Point X="3.128753173828" Y="24.0316328125" />
                  <Point X="3.11467578125" Y="24.024609375" />
                  <Point X="3.086849121094" Y="24.007716796875" />
                  <Point X="3.074123779297" Y="23.99846875" />
                  <Point X="3.050374023438" Y="23.977998046875" />
                  <Point X="3.039349609375" Y="23.966775390625" />
                  <Point X="2.955042236328" Y="23.86537890625" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921328125" Y="23.823154296875" />
                  <Point X="2.906608398438" Y="23.798775390625" />
                  <Point X="2.900166015625" Y="23.786029296875" />
                  <Point X="2.888822265625" Y="23.75864453125" />
                  <Point X="2.884363769531" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.863073974609" Y="23.57202734375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607910156" Y="23.485408203125" />
                  <Point X="2.86406640625" Y="23.469869140625" />
                  <Point X="2.871798339844" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.973731689453" Y="23.2605625" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001739501953" Y="23.217650390625" />
                  <Point X="3.019789550781" Y="23.1955546875" />
                  <Point X="3.043487548828" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.332435058594" Y="22.949146484375" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.078684082031" Y="22.35628515625" />
                  <Point X="4.045497314453" Y="22.30258203125" />
                  <Point X="4.001274658203" Y="22.239748046875" />
                  <Point X="3.880027832031" Y="22.30975" />
                  <Point X="2.848454101562" Y="22.905330078125" />
                  <Point X="2.841192382812" Y="22.90911328125" />
                  <Point X="2.815037841797" Y="22.920482421875" />
                  <Point X="2.783124267578" Y="22.930673828125" />
                  <Point X="2.771108398438" Y="22.9336640625" />
                  <Point X="2.605104248047" Y="22.96364453125" />
                  <Point X="2.555018066406" Y="22.972689453125" />
                  <Point X="2.539353027344" Y="22.974193359375" />
                  <Point X="2.507996582031" Y="22.974595703125" />
                  <Point X="2.492305175781" Y="22.973494140625" />
                  <Point X="2.460133056641" Y="22.96853515625" />
                  <Point X="2.444841308594" Y="22.964861328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589111328" Y="22.948890625" />
                  <Point X="2.262680419922" Y="22.876310546875" />
                  <Point X="2.221071044922" Y="22.854412109375" />
                  <Point X="2.208967285156" Y="22.846828125" />
                  <Point X="2.186038085938" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.78883984375" />
                  <Point X="2.128047363281" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.047883789062" Y="22.615896484375" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435515625" />
                  <Point X="2.002187866211" Y="22.41985546875" />
                  <Point X="2.03216809082" Y="22.2538515625" />
                  <Point X="2.041213500977" Y="22.203763671875" />
                  <Point X="2.043015380859" Y="22.1957734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.249242675781" Y="21.815177734375" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723752441406" Y="20.96391796875" />
                  <Point X="2.621100830078" Y="21.0976953125" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808834838867" Y="22.15037109375" />
                  <Point X="1.783252319336" Y="22.17199609375" />
                  <Point X="1.773298461914" Y="22.179353515625" />
                  <Point X="1.609573364258" Y="22.28461328125" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.323755859375" />
                  <Point X="1.517466552734" Y="22.336126953125" />
                  <Point X="1.502547973633" Y="22.34111328125" />
                  <Point X="1.470928344727" Y="22.34884375" />
                  <Point X="1.455391845703" Y="22.351302734375" />
                  <Point X="1.424125610352" Y="22.35362109375" />
                  <Point X="1.408395751953" Y="22.35348046875" />
                  <Point X="1.229334838867" Y="22.33700390625" />
                  <Point X="1.175308959961" Y="22.332033203125" />
                  <Point X="1.161231811523" Y="22.329662109375" />
                  <Point X="1.13358190918" Y="22.32283203125" />
                  <Point X="1.120009521484" Y="22.318373046875" />
                  <Point X="1.092623168945" Y="22.307029296875" />
                  <Point X="1.079872558594" Y="22.300583984375" />
                  <Point X="1.055489868164" Y="22.285861328125" />
                  <Point X="1.043857666016" Y="22.277583984375" />
                  <Point X="0.905591308594" Y="22.162619140625" />
                  <Point X="0.863874023438" Y="22.127931640625" />
                  <Point X="0.852652587891" Y="22.116908203125" />
                  <Point X="0.83218170166" Y="22.093158203125" />
                  <Point X="0.822932312012" Y="22.080431640625" />
                  <Point X="0.806040405273" Y="22.05260546875" />
                  <Point X="0.799018859863" Y="22.038529296875" />
                  <Point X="0.78739465332" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.741450805664" Y="21.80416796875" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.776479492188" Y="21.277669921875" />
                  <Point X="0.833091247559" Y="20.847662109375" />
                  <Point X="0.655065124512" Y="21.512064453125" />
                  <Point X="0.652606567383" Y="21.519875" />
                  <Point X="0.642145629883" Y="21.546419921875" />
                  <Point X="0.626787353516" Y="21.576185546875" />
                  <Point X="0.620407409668" Y="21.58679296875" />
                  <Point X="0.494629211426" Y="21.768015625" />
                  <Point X="0.456679992676" Y="21.822693359375" />
                  <Point X="0.446669799805" Y="21.834830078125" />
                  <Point X="0.424783569336" Y="21.857287109375" />
                  <Point X="0.412907409668" Y="21.867607421875" />
                  <Point X="0.386652740479" Y="21.886849609375" />
                  <Point X="0.37323626709" Y="21.89506640625" />
                  <Point X="0.345237030029" Y="21.909171875" />
                  <Point X="0.330654296875" Y="21.915060546875" />
                  <Point X="0.136019683838" Y="21.975466796875" />
                  <Point X="0.077295059204" Y="21.993693359375" />
                  <Point X="0.063380687714" Y="21.996888671875" />
                  <Point X="0.03522794342" Y="22.001158203125" />
                  <Point X="0.020989723206" Y="22.002232421875" />
                  <Point X="-0.008651331902" Y="22.002234375" />
                  <Point X="-0.02289535141" Y="22.001162109375" />
                  <Point X="-0.051061470032" Y="21.996892578125" />
                  <Point X="-0.064983421326" Y="21.9936953125" />
                  <Point X="-0.259618041992" Y="21.933287109375" />
                  <Point X="-0.318342529297" Y="21.9150625" />
                  <Point X="-0.332928222656" Y="21.909171875" />
                  <Point X="-0.360928192139" Y="21.895064453125" />
                  <Point X="-0.374342590332" Y="21.88684765625" />
                  <Point X="-0.400598754883" Y="21.867603515625" />
                  <Point X="-0.412472839355" Y="21.85728515625" />
                  <Point X="-0.434357421875" Y="21.834830078125" />
                  <Point X="-0.444368041992" Y="21.822693359375" />
                  <Point X="-0.57014630127" Y="21.641470703125" />
                  <Point X="-0.60809564209" Y="21.58679296875" />
                  <Point X="-0.612471252441" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.735601135254" Y="21.165548828125" />
                  <Point X="-0.985425598145" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134420774308" Y="20.320515225938" />
                  <Point X="-0.95957238589" Y="20.329678641686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121809587122" Y="20.416306523123" />
                  <Point X="-0.933719173635" Y="20.426163923998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.125115754384" Y="20.511263627108" />
                  <Point X="-0.907865961381" Y="20.522649206309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.143843059945" Y="20.605412543482" />
                  <Point X="-0.882012749126" Y="20.619134488621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.162570365507" Y="20.699561459855" />
                  <Point X="-0.856159536872" Y="20.715619770932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181297640305" Y="20.793710377841" />
                  <Point X="-0.830306324617" Y="20.812105053243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.819451798148" Y="20.898565212812" />
                  <Point X="0.826342167734" Y="20.89892632178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.698341717335" Y="20.997033660994" />
                  <Point X="2.720915817959" Y="20.998216719477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.218572281647" Y="20.886887269534" />
                  <Point X="-0.804453112363" Y="20.908590335555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794314675394" Y="20.9923782049" />
                  <Point X="0.81390380289" Y="20.99340482757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.628167166472" Y="21.08848634149" />
                  <Point X="2.667605268979" Y="21.090553204862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.618109619556" Y="20.9086709985" />
                  <Point X="-2.44150396395" Y="20.917926508719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.303588680816" Y="20.977562121721" />
                  <Point X="-0.778599900108" Y="21.005075617866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76917755264" Y="21.086191196989" />
                  <Point X="0.801465438046" Y="21.087883333361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557992999539" Y="21.179939042108" />
                  <Point X="2.614294719999" Y="21.182889690247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.757536439462" Y="20.996494321366" />
                  <Point X="-2.343899656792" Y="21.018172106576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.405946651196" Y="21.067328140671" />
                  <Point X="-0.752746687853" Y="21.101560900178" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.744040429886" Y="21.180004189078" />
                  <Point X="0.789027073202" Y="21.182361839151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.487818875595" Y="21.271391744979" />
                  <Point X="2.56098417102" Y="21.275226175633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.873212471063" Y="21.085562370304" />
                  <Point X="-2.189408840347" Y="21.121399000055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.511399949216" Y="21.156931940374" />
                  <Point X="-0.726893554695" Y="21.198046178344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.718903307131" Y="21.273817181166" />
                  <Point X="0.776588708358" Y="21.276840344942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.417644751652" Y="21.36284444785" />
                  <Point X="2.50767362204" Y="21.367562661018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976933961655" Y="21.175256930188" />
                  <Point X="-0.701040577279" Y="21.294531448348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.693766184377" Y="21.367630173255" />
                  <Point X="0.764150394631" Y="21.371318853411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.347470627708" Y="21.454297150721" />
                  <Point X="2.45436307306" Y="21.459899146404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920296509022" Y="21.273355546174" />
                  <Point X="-0.675187599863" Y="21.391016718352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.668629061623" Y="21.461443165344" />
                  <Point X="0.751712081357" Y="21.465797361904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.277296503764" Y="21.545749853591" />
                  <Point X="2.40105252408" Y="21.552235631789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.863659277334" Y="21.371454150582" />
                  <Point X="-0.649334622447" Y="21.487501988356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.637741853926" Y="21.55495480825" />
                  <Point X="0.739273768084" Y="21.560275870397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207122379821" Y="21.637202556462" />
                  <Point X="2.3477419751" Y="21.644572117174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.807022076399" Y="21.469552753377" />
                  <Point X="-0.609402449563" Y="21.584725117728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.578629380439" Y="21.646987227656" />
                  <Point X="0.72683545481" Y="21.65475437889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136948255877" Y="21.728655259333" />
                  <Point X="2.29443142612" Y="21.73690860256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750384875465" Y="21.567651356173" />
                  <Point X="-0.541017785542" Y="21.683439378977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.514921133819" Y="21.738778792799" />
                  <Point X="0.729684670175" Y="21.75003407281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.066774131934" Y="21.820107962204" />
                  <Point X="2.241120872701" Y="21.829245087713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.69374767453" Y="21.665749958968" />
                  <Point X="-0.472499940707" Y="21.782160619935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.450225822442" Y="21.830518628069" />
                  <Point X="0.750599925356" Y="21.846260567757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.99660000799" Y="21.911560665075" />
                  <Point X="2.187810294579" Y="21.921581571571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.637110473596" Y="21.763848561764" />
                  <Point X="-0.380826438637" Y="21.882095397468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.318740312874" Y="21.918758137375" />
                  <Point X="0.771515427343" Y="21.942487075639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.926425884046" Y="22.003013367945" />
                  <Point X="2.134499716458" Y="22.013918055429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.580473272661" Y="21.861947164559" />
                  <Point X="-0.064514650608" Y="21.993802968709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.045655796252" Y="21.999576757172" />
                  <Point X="0.799290101211" Y="22.039073057486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.856251760103" Y="22.094466070816" />
                  <Point X="2.081189138337" Y="22.106254539287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.523836071727" Y="21.960045767355" />
                  <Point X="0.876268724575" Y="22.138237709059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.764819507029" Y="22.184804682347" />
                  <Point X="2.04221060323" Y="22.199342133692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.902404575898" Y="22.296830768835" />
                  <Point X="4.046613618524" Y="22.304388444512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.797747591431" Y="21.988413266474" />
                  <Point X="-3.780965049026" Y="21.989292802252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467198870792" Y="22.058144370151" />
                  <Point X="0.998376358274" Y="22.239767471844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.628002944155" Y="22.272764802988" />
                  <Point X="2.024994911522" Y="22.293570270391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.751346129215" Y="22.384044503973" />
                  <Point X="4.048588970257" Y="22.399622341179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.8673787084" Y="22.079894427135" />
                  <Point X="-3.599744569021" Y="22.09392053804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.410561669858" Y="22.156242972946" />
                  <Point X="2.007975498193" Y="22.387808693603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.600287687725" Y="22.471258239382" />
                  <Point X="3.932538767487" Y="22.488670780637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937009840622" Y="22.171375586996" />
                  <Point X="-3.418524089016" Y="22.198548273827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.353924468923" Y="22.254341575742" />
                  <Point X="2.001384068497" Y="22.48259362428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449229246234" Y="22.558471974791" />
                  <Point X="3.816488564717" Y="22.577719220094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.006091015651" Y="22.262885568892" />
                  <Point X="-3.237303609011" Y="22.303176009615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316176049731" Y="22.351450259433" />
                  <Point X="2.028542895399" Y="22.579147330956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.298170804744" Y="22.645685710201" />
                  <Point X="3.700438361947" Y="22.666767659551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.061103507324" Y="22.35513285924" />
                  <Point X="-3.056083129006" Y="22.407803745402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315943195796" Y="22.44659283566" />
                  <Point X="2.080029603435" Y="22.676976007857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147112363253" Y="22.73289944561" />
                  <Point X="3.584388159177" Y="22.755816099009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.116115998996" Y="22.447380149589" />
                  <Point X="-2.874862039485" Y="22.512431513133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.359338803286" Y="22.539448941111" />
                  <Point X="2.134725521489" Y="22.774972872328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.996053921763" Y="22.820113181019" />
                  <Point X="3.468337956407" Y="22.844864538466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.171128490668" Y="22.539627439937" />
                  <Point X="-2.693640561513" Y="22.617059301222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.467419221777" Y="22.628915059264" />
                  <Point X="2.263732431401" Y="22.876864210858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.844655449477" Y="22.90730909617" />
                  <Point X="3.352287753637" Y="22.933912977923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.072720030112" Y="22.639915181687" />
                  <Point X="3.236237451468" Y="23.022961412171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939655410323" Y="22.742019175781" />
                  <Point X="3.120187128785" Y="23.112009845344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.806590542838" Y="22.844123182857" />
                  <Point X="3.01483561199" Y="23.201618979174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.673525675352" Y="22.946227189932" />
                  <Point X="2.95256490753" Y="23.293485882709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.540460807867" Y="23.048331197007" />
                  <Point X="2.893928119313" Y="23.385543231724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.407395940381" Y="23.150435204083" />
                  <Point X="2.862616613333" Y="23.4790326381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.274331072896" Y="23.252539211158" />
                  <Point X="2.863273665328" Y="23.574197445605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141266205411" Y="23.354643218233" />
                  <Point X="2.872069892121" Y="23.669788809187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.018264294791" Y="23.456219848086" />
                  <Point X="2.891850879527" Y="23.765955859679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.962618135315" Y="23.5542665126" />
                  <Point X="2.954189419411" Y="23.864353256988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.529362959904" Y="23.946904604231" />
                  <Point X="4.736238133386" Y="23.957746472662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948798915932" Y="23.650121120069" />
                  <Point X="3.036890491528" Y="23.963817809392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.01252035054" Y="24.014948403705" />
                  <Point X="4.758209593289" Y="24.054028320953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.96865323593" Y="23.744210972118" />
                  <Point X="3.283368372083" Y="24.071865540624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495677741177" Y="24.082992203179" />
                  <Point X="4.773830282553" Y="24.149977339458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.670333497669" Y="23.750160061421" />
                  <Point X="-4.189629411103" Y="23.77535269509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.041689574615" Y="23.835513672671" />
                  <Point X="4.71483392862" Y="24.242015844432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.694311932369" Y="23.844033777777" />
                  <Point X="4.417880466684" Y="24.321583545811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71829037505" Y="23.937907493715" />
                  <Point X="4.120927004747" Y="24.401151247191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.741610948856" Y="24.0318156871" />
                  <Point X="3.823975299295" Y="24.480719040623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755115482849" Y="24.126238317333" />
                  <Point X="3.593127595239" Y="24.563751197971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.768620016843" Y="24.220660947566" />
                  <Point X="3.473323631258" Y="24.652602911139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782124550836" Y="24.315083577799" />
                  <Point X="3.404237657681" Y="24.744112641554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.486534134564" Y="24.425705187963" />
                  <Point X="3.368385687413" Y="24.837364092279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.04518026536" Y="24.543965936996" />
                  <Point X="3.350928244857" Y="24.931579559353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.603826396157" Y="24.662226686029" />
                  <Point X="3.354369542559" Y="25.026890282993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.375655023015" Y="24.769315013861" />
                  <Point X="3.373670426428" Y="25.123032172325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.313464989933" Y="24.867704628258" />
                  <Point X="3.422980526043" Y="25.220746778012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29564770521" Y="24.963768765453" />
                  <Point X="3.509484118019" Y="25.320410612037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.309865478799" Y="25.058154016383" />
                  <Point X="3.709399996761" Y="25.426018132155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.359969782558" Y="25.15065853396" />
                  <Point X="4.150754032563" Y="25.544278889919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.480826026366" Y="25.239455099479" />
                  <Point X="4.592108068366" Y="25.662539647683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769039666596" Y="25.319480835506" />
                  <Point X="4.77556309745" Y="25.767284491225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.065991102795" Y="25.399048643049" />
                  <Point X="4.760871637018" Y="25.861644917279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.362941970162" Y="25.478616480404" />
                  <Point X="4.742560846315" Y="25.955815662271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.659892837528" Y="25.558184317759" />
                  <Point X="3.303713291321" Y="25.975539230057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.917804023279" Y="26.007722361597" />
                  <Point X="4.719693349183" Y="26.049747600399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777257214438" Y="25.647163884268" />
                  <Point X="3.148501449495" Y="26.062535294978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763070297569" Y="25.743037761946" />
                  <Point X="3.076416690122" Y="26.153887865689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74888339147" Y="25.838911639059" />
                  <Point X="3.028588621371" Y="26.246511675688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.734696707112" Y="25.934785504552" />
                  <Point X="3.007185492261" Y="26.340520358092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.711384591922" Y="26.031137613609" />
                  <Point X="3.00324955852" Y="26.435444457415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.685234744877" Y="26.127638441891" />
                  <Point X="3.027107490515" Y="26.531825171519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.659084897832" Y="26.224139270173" />
                  <Point X="-4.129619426522" Y="26.251887379731" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.526723578883" Y="26.283483812245" />
                  <Point X="3.074950302263" Y="26.629462879907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.458990186698" Y="26.382163941783" />
                  <Point X="3.156085511557" Y="26.728845368917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.425818473871" Y="26.479032770457" />
                  <Point X="3.286305862487" Y="26.830800301197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.427270111967" Y="26.574087066198" />
                  <Point X="3.419370267663" Y="26.932904284044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.465468333524" Y="26.667215555103" />
                  <Point X="2.430776326416" Y="26.97622464384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473062135328" Y="26.97844074918" />
                  <Point X="3.552434672839" Y="27.03500826689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.531897437116" Y="26.758864526174" />
                  <Point X="2.181092010118" Y="27.058269616171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.739354096935" Y="27.087526892399" />
                  <Point X="3.685499078015" Y="27.137112249737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646025294762" Y="26.84801371147" />
                  <Point X="2.092255116978" Y="27.148744244753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.920574760826" Y="27.192154637823" />
                  <Point X="3.818563483191" Y="27.239216232584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.762074854094" Y="26.937062184649" />
                  <Point X="2.038182639808" Y="27.241040799174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.101795637564" Y="27.296782394403" />
                  <Point X="3.951627888366" Y="27.34132021543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.878125613547" Y="27.026110594931" />
                  <Point X="2.017570200748" Y="27.335090919887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.283016623289" Y="27.401410156694" />
                  <Point X="4.084692453935" Y="27.443424206683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.994176373" Y="27.115159005214" />
                  <Point X="2.015734272464" Y="27.430125075832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464237609013" Y="27.506037918985" />
                  <Point X="4.102972635845" Y="27.539512603292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.110227132453" Y="27.204207415497" />
                  <Point X="2.037504556959" Y="27.526396380967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.645458594738" Y="27.610665681276" />
                  <Point X="4.047174824976" Y="27.631718736805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.226277891906" Y="27.29325582578" />
                  <Point X="2.079827059449" Y="27.623744782206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.826679580462" Y="27.715293443567" />
                  <Point X="3.981796539533" Y="27.723422778921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17099549291" Y="27.391283426414" />
                  <Point X="2.136464355683" Y="27.721843389996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.113717147421" Y="27.489415630172" />
                  <Point X="2.193101651917" Y="27.819941997786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.05643863537" Y="27.587547842659" />
                  <Point X="2.249738948151" Y="27.918040605576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.998071285335" Y="27.685737118727" />
                  <Point X="-3.146213792098" Y="27.730381078213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960218640807" Y="27.74012867105" />
                  <Point X="2.306376244386" Y="28.016139213366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.920914939415" Y="27.784911084344" />
                  <Point X="-3.345389310781" Y="27.815073104461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.829957138715" Y="27.84208575997" />
                  <Point X="2.36301354062" Y="28.114237821156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.843758593496" Y="27.884085049961" />
                  <Point X="-3.496447952436" Y="27.90228682938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.763207250676" Y="27.940714346239" />
                  <Point X="2.419650836854" Y="28.212336428946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.766602247576" Y="27.983259015578" />
                  <Point X="-3.647506594092" Y="27.989500554299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748542509071" Y="28.036613265651" />
                  <Point X="2.476288133088" Y="28.310435036736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756800193765" Y="28.131310871604" />
                  <Point X="2.532925429322" Y="28.408533644526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.785448367143" Y="28.224939857326" />
                  <Point X="2.589562725556" Y="28.506632252316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.838478884324" Y="28.317291018556" />
                  <Point X="2.64620002179" Y="28.604730860106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.89178959546" Y="28.409627495443" />
                  <Point X="2.702837318024" Y="28.702829467896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945100196659" Y="28.501963978092" />
                  <Point X="2.759474185813" Y="28.800928053232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.998410797858" Y="28.594300460741" />
                  <Point X="2.816110729603" Y="28.899026621588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.051721399056" Y="28.68663694339" />
                  <Point X="2.696862956332" Y="28.987907483477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945354377766" Y="28.787341775634" />
                  <Point X="2.560698042162" Y="29.075901755579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.812171319535" Y="28.889451976824" />
                  <Point X="2.410887398852" Y="29.16318088532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.678987382189" Y="28.991562224086" />
                  <Point X="-1.877624912039" Y="29.033559851548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.763722701698" Y="29.039529213447" />
                  <Point X="-0.127248033712" Y="29.125293216649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.159118373969" Y="29.140301044137" />
                  <Point X="2.261077125096" Y="29.250460034428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.502771315848" Y="29.095927689667" />
                  <Point X="-2.060882215603" Y="29.1190861161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.559435763582" Y="29.14536581108" />
                  <Point X="-0.2062001891" Y="29.216285882385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.229119062312" Y="29.239099997631" />
                  <Point X="2.0805145669" Y="29.336127524601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313709057091" Y="29.200966395664" />
                  <Point X="-2.147541334996" Y="29.209674876968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.502952212783" Y="29.243456361413" />
                  <Point X="-0.23852671269" Y="29.309722093942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257808470344" Y="29.335733918665" />
                  <Point X="1.897595985234" Y="29.421671540816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.472481277617" Y="29.340183648328" />
                  <Point X="-0.263663852819" Y="29.40353508512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.283661603974" Y="29.432219196856" />
                  <Point X="1.678382822237" Y="29.505313438624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462975847769" Y="29.435812179667" />
                  <Point X="-0.288800992948" Y="29.497348076298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.30951466666" Y="29.528704471328" />
                  <Point X="1.447400926365" Y="29.588338563276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475008095089" Y="29.530311969175" />
                  <Point X="-0.313938133077" Y="29.591161067476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.335367729345" Y="29.625189745801" />
                  <Point X="1.123650688346" Y="29.666501905129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.204805434711" Y="29.639603063432" />
                  <Point X="-0.339075273206" Y="29.684974058654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.361220792031" Y="29.721675020274" />
                  <Point X="0.775572228309" Y="29.743390258892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.424367574383" Y="29.77563445143" />
                  <Point X="-0.364212413335" Y="29.778787049832" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.779272277832" Y="20.314412109375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318664551" Y="21.47845703125" />
                  <Point X="0.338540405273" Y="21.6596796875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274336364746" Y="21.733599609375" />
                  <Point X="0.079701728821" Y="21.794005859375" />
                  <Point X="0.020977233887" Y="21.812232421875" />
                  <Point X="-0.008663844109" Y="21.812234375" />
                  <Point X="-0.203298477173" Y="21.751826171875" />
                  <Point X="-0.262022979736" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.414057434082" Y="21.533134765625" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.552075134277" Y="21.116373046875" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.032244384766" Y="20.048734375" />
                  <Point X="-1.100259155273" Y="20.0619375" />
                  <Point X="-1.325946166992" Y="20.12000390625" />
                  <Point X="-1.35158984375" Y="20.1266015625" />
                  <Point X="-1.346193969727" Y="20.1675859375" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683349609" Y="20.465240234375" />
                  <Point X="-1.356181518555" Y="20.69900390625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.565477783203" Y="20.954521484375" />
                  <Point X="-1.619543823242" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.887072387695" Y="21.02982421875" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878051758" Y="21.026208984375" />
                  <Point X="-2.188052001953" Y="20.89379296875" />
                  <Point X="-2.247844482422" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.311861083984" Y="20.774771484375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.756162353516" Y="20.77067578125" />
                  <Point X="-2.855838378906" Y="20.832390625" />
                  <Point X="-3.168362548828" Y="21.073025390625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.077921142578" Y="21.380341796875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762451172" Y="22.40240625" />
                  <Point X="-2.513978515625" Y="22.431234375" />
                  <Point X="-2.531327148438" Y="22.44858203125" />
                  <Point X="-2.560156494141" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.890544921875" Y="22.283982421875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.082957275391" Y="22.0494140625" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.385762207031" Y="22.52858203125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.164404296875" Y="22.8090546875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822265625" Y="23.603984375" />
                  <Point X="-3.139903564453" Y="23.626837890625" />
                  <Point X="-3.138117675781" Y="23.633732421875" />
                  <Point X="-3.140325683594" Y="23.66540234375" />
                  <Point X="-3.161157714844" Y="23.689359375" />
                  <Point X="-3.181502197266" Y="23.701333984375" />
                  <Point X="-3.187648681641" Y="23.704951171875" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.596813476562" Y="23.6617578125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.896594238281" Y="23.868232421875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.986673339844" Y="24.4032890625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.697317871094" Y="24.5659296875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541894775391" Y="24.87857421875" />
                  <Point X="-3.520574462891" Y="24.89337109375" />
                  <Point X="-3.514141845703" Y="24.8978359375" />
                  <Point X="-3.494898925781" Y="24.924091796875" />
                  <Point X="-3.487791992188" Y="24.946990234375" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.49275390625" Y="25.006435546875" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514141845703" Y="25.039603515625" />
                  <Point X="-3.535462158203" Y="25.054400390625" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.901376220703" Y="25.158236328125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.937493652344" Y="25.862283203125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.798315917969" Y="26.43677734375" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.570341308594" Y="26.501548828125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731706054688" Y="26.3958671875" />
                  <Point X="-3.684517578125" Y="26.410744140625" />
                  <Point X="-3.670275146484" Y="26.415234375" />
                  <Point X="-3.651532714844" Y="26.426056640625" />
                  <Point X="-3.639119628906" Y="26.443787109375" />
                  <Point X="-3.620185058594" Y="26.489498046875" />
                  <Point X="-3.614472167969" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316894531" Y="26.545513671875" />
                  <Point X="-3.639163574219" Y="26.58940234375" />
                  <Point X="-3.646056640625" Y="26.602642578125" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.857237304688" Y="26.770591796875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.237138671875" Y="27.654873046875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.843915283203" Y="28.193306640625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.660981689453" Y="28.216671875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138513671875" Y="27.92043359375" />
                  <Point X="-3.072793212891" Y="27.91468359375" />
                  <Point X="-3.052964355469" Y="27.91294921875" />
                  <Point X="-3.031506591797" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.966604003906" Y="27.974052734375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.943823974609" Y="28.093560546875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.039483398438" Y="28.28544140625" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.887200683594" Y="29.071345703125" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.255029541016" Y="29.450923828125" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.121450439453" Y="29.487779296875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951249023438" Y="29.273662109375" />
                  <Point X="-1.878102539063" Y="29.235583984375" />
                  <Point X="-1.856033081055" Y="29.22409375" />
                  <Point X="-1.835125488281" Y="29.218490234375" />
                  <Point X="-1.813808959961" Y="29.22225" />
                  <Point X="-1.737621948242" Y="29.25380859375" />
                  <Point X="-1.714635009766" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083129883" Y="29.29448828125" />
                  <Point X="-1.661285644531" Y="29.373134765625" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.661855834961" Y="29.4939140625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.141985839844" Y="29.854541015625" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.36455178833" Y="29.973931640625" />
                  <Point X="-0.224199951172" Y="29.990359375" />
                  <Point X="-0.190594024658" Y="29.864939453125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282119751" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.099243202209" Y="29.478064453125" />
                  <Point X="0.236648406982" Y="29.9908671875" />
                  <Point X="0.708371154785" Y="29.94146484375" />
                  <Point X="0.860210083008" Y="29.925564453125" />
                  <Point X="1.35953125" Y="29.80501171875" />
                  <Point X="1.508454223633" Y="29.769056640625" />
                  <Point X="1.834098388672" Y="29.6509453125" />
                  <Point X="1.931046142578" Y="29.615779296875" />
                  <Point X="2.245304443359" Y="29.4688125" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.642295410156" Y="29.2482578125" />
                  <Point X="2.732532958984" Y="29.195685546875" />
                  <Point X="3.018856445312" Y="28.992068359375" />
                  <Point X="3.068740722656" Y="28.95659375" />
                  <Point X="2.891770263672" Y="28.6500703125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.208358642578" Y="27.4298359375" />
                  <Point X="2.202044677734" Y="27.392326171875" />
                  <Point X="2.208475830078" Y="27.338994140625" />
                  <Point X="2.210416015625" Y="27.32290234375" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.251683105469" Y="27.252177734375" />
                  <Point X="2.274939453125" Y="27.224203125" />
                  <Point X="2.32357421875" Y="27.191201171875" />
                  <Point X="2.338248046875" Y="27.181244140625" />
                  <Point X="2.360333740234" Y="27.172978515625" />
                  <Point X="2.413666992188" Y="27.166548828125" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.510342285156" Y="27.182439453125" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.886943603516" Y="27.39212890625" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.149071289063" Y="27.816259765625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.362218261719" Y="27.478095703125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.160091308594" Y="27.2617890625" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279370605469" Y="26.58383203125" />
                  <Point X="3.234981201172" Y="26.525923828125" />
                  <Point X="3.221588378906" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.491498046875" />
                  <Point X="3.196584228516" Y="26.432373046875" />
                  <Point X="3.191595458984" Y="26.414533203125" />
                  <Point X="3.190779785156" Y="26.39096484375" />
                  <Point X="3.204353271484" Y="26.3251796875" />
                  <Point X="3.208448730469" Y="26.30533203125" />
                  <Point X="3.215647216797" Y="26.287953125" />
                  <Point X="3.252565917969" Y="26.231837890625" />
                  <Point X="3.263704833984" Y="26.214908203125" />
                  <Point X="3.2809453125" Y="26.1988203125" />
                  <Point X="3.3344453125" Y="26.168705078125" />
                  <Point X="3.368564697266" Y="26.153619140625" />
                  <Point X="3.440900390625" Y="26.144060546875" />
                  <Point X="3.462725341797" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.804394775391" Y="26.184431640625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.916201660156" Y="26.045806640625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.989491210938" Y="25.628298828125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.739896972656" Y="25.505435546875" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729089355469" Y="25.2328203125" />
                  <Point X="3.658021728516" Y="25.1917421875" />
                  <Point X="3.636579345703" Y="25.179349609375" />
                  <Point X="3.622265625" Y="25.166927734375" />
                  <Point X="3.579625" Y="25.112591796875" />
                  <Point X="3.566759765625" Y="25.09619921875" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.542771728516" Y="25.000515625" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.552696777344" Y="24.88509765625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758056641" Y="24.8412421875" />
                  <Point X="3.609398681641" Y="24.786908203125" />
                  <Point X="3.622263916016" Y="24.770513671875" />
                  <Point X="3.636576171875" Y="24.758091796875" />
                  <Point X="3.707644042969" Y="24.717013671875" />
                  <Point X="3.729086181641" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.042500732422" Y="24.618869140625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.961266601562" Y="24.11873046875" />
                  <Point X="4.948430664062" Y="24.033595703125" />
                  <Point X="4.883985351562" Y="23.7511875" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="4.570227539062" Y="23.749884765625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836669922" Y="23.901658203125" />
                  <Point X="3.255355957031" Y="23.871341796875" />
                  <Point X="3.213272460938" Y="23.8621953125" />
                  <Point X="3.185445800781" Y="23.845302734375" />
                  <Point X="3.101138427734" Y="23.74390625" />
                  <Point X="3.075701660156" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.052274658203" Y="23.5546171875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360839844" Y="23.483376953125" />
                  <Point X="3.133551513672" Y="23.3633125" />
                  <Point X="3.156841064453" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.448099609375" Y="23.099884765625" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.240311035156" Y="22.25640234375" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.070853759766" Y="22.00848828125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.785028320312" Y="22.145203125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.7466875" />
                  <Point X="2.571336181641" Y="22.77666796875" />
                  <Point X="2.52125" Y="22.785712890625" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.351169189453" Y="22.708173828125" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.21601953125" Y="22.527408203125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.219143310547" Y="22.28762109375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.413787597656" Y="21.910177734375" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.878230224609" Y="20.840453125" />
                  <Point X="2.835301757812" Y="20.809791015625" />
                  <Point X="2.686283203125" Y="20.713333984375" />
                  <Point X="2.679774658203" Y="20.70912109375" />
                  <Point X="2.470363525391" Y="20.98203125" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.506823852539" Y="22.12479296875" />
                  <Point X="1.457425415039" Y="22.15655078125" />
                  <Point X="1.425805541992" Y="22.16428125" />
                  <Point X="1.246744384766" Y="22.1478046875" />
                  <Point X="1.19271875" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.027065795898" Y="22.016525390625" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.927115783691" Y="21.7638125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.964854003906" Y="21.302470703125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.034960449219" Y="20.045654296875" />
                  <Point X="0.99437322998" Y="20.0367578125" />
                  <Point X="0.860200317383" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#194" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.148453669796" Y="4.909161140093" Z="1.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="-0.376520263215" Y="5.054873288322" Z="1.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.95" />
                  <Point X="-1.161639751034" Y="4.933966835172" Z="1.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.95" />
                  <Point X="-1.717584090305" Y="4.518668739546" Z="1.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.95" />
                  <Point X="-1.71512768982" Y="4.419451391419" Z="1.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.95" />
                  <Point X="-1.762912015899" Y="4.331282546769" Z="1.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.95" />
                  <Point X="-1.861168578771" Y="4.311213330345" Z="1.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.95" />
                  <Point X="-2.087938952835" Y="4.549497783769" Z="1.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.95" />
                  <Point X="-2.285468466021" Y="4.525911741359" Z="1.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.95" />
                  <Point X="-2.923778829153" Y="4.142305941339" Z="1.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.95" />
                  <Point X="-3.088940558729" Y="3.291721632871" Z="1.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.95" />
                  <Point X="-2.999789863124" Y="3.120483985277" Z="1.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.95" />
                  <Point X="-3.008114724876" Y="3.040688843011" Z="1.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.95" />
                  <Point X="-3.07459248033" Y="2.995774836049" Z="1.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.95" />
                  <Point X="-3.642137715426" Y="3.291253390017" Z="1.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.95" />
                  <Point X="-3.889534692194" Y="3.255289880974" Z="1.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.95" />
                  <Point X="-4.286476892867" Y="2.711359003022" Z="1.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.95" />
                  <Point X="-3.893831300754" Y="1.762203940412" Z="1.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.95" />
                  <Point X="-3.689669083508" Y="1.59759239313" Z="1.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.95" />
                  <Point X="-3.672535310171" Y="1.53991227569" Z="1.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.95" />
                  <Point X="-3.705707475863" Y="1.489711026875" Z="1.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.95" />
                  <Point X="-4.56997105532" Y="1.582402529763" Z="1.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.95" />
                  <Point X="-4.85273173301" Y="1.481136804301" Z="1.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.95" />
                  <Point X="-4.993110962462" Y="0.900882952136" Z="1.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.95" />
                  <Point X="-3.920473216242" Y="0.141220104918" Z="1.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.95" />
                  <Point X="-3.570128078206" Y="0.044604400423" Z="1.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.95" />
                  <Point X="-3.546663603414" Y="0.02289819302" Z="1.95" />
                  <Point X="-3.539556741714" Y="0" Z="1.95" />
                  <Point X="-3.541700997721" Y="-0.006908758041" Z="1.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.95" />
                  <Point X="-3.555240516917" Y="-0.034271582711" Z="1.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.95" />
                  <Point X="-4.716414572531" Y="-0.354491951838" Z="1.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.95" />
                  <Point X="-5.042325514938" Y="-0.572507880574" Z="1.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.95" />
                  <Point X="-4.951195173368" Y="-1.11286118596" Z="1.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.95" />
                  <Point X="-3.596443113962" Y="-1.356533709091" Z="1.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.95" />
                  <Point X="-3.21302066354" Y="-1.310475999677" Z="1.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.95" />
                  <Point X="-3.194462080547" Y="-1.329344750772" Z="1.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.95" />
                  <Point X="-4.200998063939" Y="-2.119997767452" Z="1.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.95" />
                  <Point X="-4.434861559141" Y="-2.465746808069" Z="1.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.95" />
                  <Point X="-4.128899133894" Y="-2.949589373668" Z="1.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.95" />
                  <Point X="-2.871700940424" Y="-2.728038702149" Z="1.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.95" />
                  <Point X="-2.568818396816" Y="-2.559512072315" Z="1.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.95" />
                  <Point X="-3.127378311019" Y="-3.563377442068" Z="1.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.95" />
                  <Point X="-3.205022184632" Y="-3.935311693617" Z="1.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.95" />
                  <Point X="-2.788639653383" Y="-4.240556389916" Z="1.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.95" />
                  <Point X="-2.278349326987" Y="-4.224385449772" Z="1.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.95" />
                  <Point X="-2.166429961546" Y="-4.116500172482" Z="1.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.95" />
                  <Point X="-1.896498355058" Y="-3.988787617184" Z="1.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.95" />
                  <Point X="-1.604600839234" Y="-4.051790617324" Z="1.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.95" />
                  <Point X="-1.411376719209" Y="-4.279470292769" Z="1.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.95" />
                  <Point X="-1.40192233299" Y="-4.794607688595" Z="1.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.95" />
                  <Point X="-1.34456132962" Y="-4.897137409925" Z="1.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.95" />
                  <Point X="-1.047928512888" Y="-4.969068589634" Z="1.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="-0.509934897067" Y="-3.865286993701" Z="1.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="-0.379137408243" Y="-3.464095269154" Z="1.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="-0.194634637089" Y="-3.264646777102" Z="1.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.95" />
                  <Point X="0.058724442272" Y="-3.222465268186" Z="1.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.95" />
                  <Point X="0.291308451189" Y="-3.337550486614" Z="1.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.95" />
                  <Point X="0.724819963334" Y="-4.667249036115" Z="1.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.95" />
                  <Point X="0.859468389945" Y="-5.006169360197" Z="1.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.95" />
                  <Point X="1.03950932071" Y="-4.971904784631" Z="1.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.95" />
                  <Point X="1.008270275095" Y="-3.659723563098" Z="1.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.95" />
                  <Point X="0.969819032307" Y="-3.215526966793" Z="1.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.95" />
                  <Point X="1.052877267668" Y="-2.990639486837" Z="1.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.95" />
                  <Point X="1.245169392534" Y="-2.870703951172" Z="1.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.95" />
                  <Point X="1.473628943371" Y="-2.885985252117" Z="1.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.95" />
                  <Point X="2.424539841496" Y="-4.017125756342" Z="1.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.95" />
                  <Point X="2.707297037713" Y="-4.297360929065" Z="1.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.95" />
                  <Point X="2.901136053335" Y="-4.168954059871" Z="1.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.95" />
                  <Point X="2.450932976996" Y="-3.033541233188" Z="1.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.95" />
                  <Point X="2.262191365649" Y="-2.672212433606" Z="1.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.95" />
                  <Point X="2.254109801309" Y="-2.464598883077" Z="1.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.95" />
                  <Point X="2.368299495295" Y="-2.304791508658" Z="1.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.95" />
                  <Point X="2.556294369907" Y="-2.241256643842" Z="1.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.95" />
                  <Point X="3.753872363919" Y="-2.866816523279" Z="1.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.95" />
                  <Point X="4.105586052545" Y="-2.989008777091" Z="1.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.95" />
                  <Point X="4.276688535779" Y="-2.73860269695" Z="1.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.95" />
                  <Point X="3.472381615369" Y="-1.829167448834" Z="1.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.95" />
                  <Point X="3.16945314947" Y="-1.578367470033" Z="1.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.95" />
                  <Point X="3.095908216945" Y="-1.418683690494" Z="1.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.95" />
                  <Point X="3.133428316935" Y="-1.25677958583" Z="1.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.95" />
                  <Point X="3.259819136546" Y="-1.146237136722" Z="1.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.95" />
                  <Point X="4.557545233624" Y="-1.268406309207" Z="1.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.95" />
                  <Point X="4.926576599548" Y="-1.228655993326" Z="1.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.95" />
                  <Point X="5.004551938392" Y="-0.857443378549" Z="1.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.95" />
                  <Point X="4.049285656065" Y="-0.301552555492" Z="1.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.95" />
                  <Point X="3.726510415904" Y="-0.208416612523" Z="1.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.95" />
                  <Point X="3.642577344437" Y="-0.15094470236" Z="1.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.95" />
                  <Point X="3.595648266958" Y="-0.074217808696" Z="1.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.95" />
                  <Point X="3.585723183466" Y="0.022392722494" Z="1.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.95" />
                  <Point X="3.612802093963" Y="0.113004036215" Z="1.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.95" />
                  <Point X="3.676884998448" Y="0.179732231719" Z="1.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.95" />
                  <Point X="4.746682161241" Y="0.488419377277" Z="1.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.95" />
                  <Point X="5.032740070489" Y="0.667270430267" Z="1.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.95" />
                  <Point X="4.958626865393" Y="1.088914086368" Z="1.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.95" />
                  <Point X="3.791712483185" Y="1.265283931372" Z="1.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.95" />
                  <Point X="3.44129649993" Y="1.224908511428" Z="1.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.95" />
                  <Point X="3.352818740217" Y="1.243555058916" Z="1.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.95" />
                  <Point X="3.288179605708" Y="1.290601739772" Z="1.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.95" />
                  <Point X="3.24716562273" Y="1.366564670283" Z="1.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.95" />
                  <Point X="3.238580929416" Y="1.450188302024" Z="1.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.95" />
                  <Point X="3.268508937489" Y="1.526785863353" Z="1.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.95" />
                  <Point X="4.184373549201" Y="2.25340166401" Z="1.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.95" />
                  <Point X="4.398839490724" Y="2.535262463735" Z="1.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.95" />
                  <Point X="4.183500970876" Y="2.876744365502" Z="1.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.95" />
                  <Point X="2.855787661156" Y="2.466709838545" Z="1.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.95" />
                  <Point X="2.491268892552" Y="2.262022471441" Z="1.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.95" />
                  <Point X="2.413500120185" Y="2.247469515944" Z="1.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.95" />
                  <Point X="2.345492875341" Y="2.263857375912" Z="1.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.95" />
                  <Point X="2.286901224323" Y="2.311531985041" Z="1.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.95" />
                  <Point X="2.251960186815" Y="2.376258323647" Z="1.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.95" />
                  <Point X="2.250505471217" Y="2.44820068333" Z="1.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.95" />
                  <Point X="2.928915514314" Y="3.656351420799" Z="1.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.95" />
                  <Point X="3.041677948918" Y="4.064094228197" Z="1.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.95" />
                  <Point X="2.661309419729" Y="4.322741188011" Z="1.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.95" />
                  <Point X="2.26033017559" Y="4.545384011994" Z="1.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.95" />
                  <Point X="1.844991638984" Y="4.729228840577" Z="1.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.95" />
                  <Point X="1.365109731919" Y="4.884896649781" Z="1.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.95" />
                  <Point X="0.707422729317" Y="5.022474845694" Z="1.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.95" />
                  <Point X="0.044790891757" Y="4.522286365857" Z="1.95" />
                  <Point X="0" Y="4.355124473572" Z="1.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>