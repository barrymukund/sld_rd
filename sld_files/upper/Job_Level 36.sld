<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#187" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2615" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.839493225098" Y="20.45671484375" />
                  <Point X="0.563301940918" Y="21.4874765625" />
                  <Point X="0.557720153809" Y="21.502861328125" />
                  <Point X="0.54236315918" Y="21.532623046875" />
                  <Point X="0.429476806641" Y="21.69526953125" />
                  <Point X="0.378635528564" Y="21.7685234375" />
                  <Point X="0.356752349854" Y="21.7909765625" />
                  <Point X="0.330497161865" Y="21.810220703125" />
                  <Point X="0.302495025635" Y="21.824330078125" />
                  <Point X="0.127810066223" Y="21.878544921875" />
                  <Point X="0.049135883331" Y="21.902962890625" />
                  <Point X="0.020983165741" Y="21.907232421875" />
                  <Point X="-0.008657706261" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.211508895874" Y="21.848748046875" />
                  <Point X="-0.290182922363" Y="21.82433203125" />
                  <Point X="-0.318183105469" Y="21.810224609375" />
                  <Point X="-0.344439025879" Y="21.79098046875" />
                  <Point X="-0.366323425293" Y="21.7685234375" />
                  <Point X="-0.479209960938" Y="21.605875" />
                  <Point X="-0.530051086426" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.675380065918" Y="21.02324609375" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-0.990542358398" Y="20.1374140625" />
                  <Point X="-1.07932409668" Y="20.154646484375" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.258240966797" Y="20.693576171875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323644897461" Y="20.868794921875" />
                  <Point X="-1.48447265625" Y="21.009837890625" />
                  <Point X="-1.556905639648" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.856482177734" Y="21.1230234375" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.220519287109" Y="20.98635546875" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.404938232422" Y="20.809525390625" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.671556152344" Y="20.830025390625" />
                  <Point X="-2.801724609375" Y="20.910623046875" />
                  <Point X="-3.078164550781" Y="21.12347265625" />
                  <Point X="-3.104721923828" Y="21.143919921875" />
                  <Point X="-2.937031738281" Y="21.434369140625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561035156" Y="22.444427734375" />
                  <Point X="-2.428779541016" Y="22.4732578125" />
                  <Point X="-2.446807861328" Y="22.498416015625" />
                  <Point X="-2.464156494141" Y="22.515763671875" />
                  <Point X="-2.489316894531" Y="22.533791015625" />
                  <Point X="-2.518145507812" Y="22.548005859375" />
                  <Point X="-2.547761962891" Y="22.55698828125" />
                  <Point X="-2.578694091797" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.538802734375" />
                  <Point X="-3.039572509766" Y="22.307638671875" />
                  <Point X="-3.818024169922" Y="21.85819921875" />
                  <Point X="-3.980031738281" Y="22.07104296875" />
                  <Point X="-4.082862548828" Y="22.206142578125" />
                  <Point X="-4.281055664063" Y="22.538482421875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.004011230469" Y="22.8123828125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084578613281" Y="23.52440234375" />
                  <Point X="-3.066614257812" Y="23.55153125" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.048544433594" Y="23.600677734375" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.043347900391" Y="23.640337890625" />
                  <Point X="-3.045556152344" Y="23.672009765625" />
                  <Point X="-3.052556640625" Y="23.70175390625" />
                  <Point X="-3.068637695312" Y="23.72773828125" />
                  <Point X="-3.089470947266" Y="23.751697265625" />
                  <Point X="-3.112974609375" Y="23.771234375" />
                  <Point X="-3.131233886719" Y="23.78198046875" />
                  <Point X="-3.139457275391" Y="23.7868203125" />
                  <Point X="-3.168721923828" Y="23.798044921875" />
                  <Point X="-3.200609130859" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.737382080078" Y="23.739072265625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.793859863281" Y="23.84989453125" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.886515136719" Y="24.373982421875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.555897460938" Y="24.50547265625" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517494873047" Y="24.785169921875" />
                  <Point X="-3.487729980469" Y="24.800529296875" />
                  <Point X="-3.468594970703" Y="24.813810546875" />
                  <Point X="-3.459977050781" Y="24.819791015625" />
                  <Point X="-3.437524658203" Y="24.841671875" />
                  <Point X="-3.418277832031" Y="24.8679296875" />
                  <Point X="-3.404168212891" Y="24.895931640625" />
                  <Point X="-3.397789794922" Y="24.916482421875" />
                  <Point X="-3.394917236328" Y="24.92573828125" />
                  <Point X="-3.390647705078" Y="24.9538984375" />
                  <Point X="-3.390647705078" Y="24.9835390625" />
                  <Point X="-3.394916503906" Y="25.011697265625" />
                  <Point X="-3.401294921875" Y="25.03225" />
                  <Point X="-3.404167480469" Y="25.041505859375" />
                  <Point X="-3.418277099609" Y="25.069509765625" />
                  <Point X="-3.4375234375" Y="25.095767578125" />
                  <Point X="-3.459981933594" Y="25.11765234375" />
                  <Point X="-3.479116943359" Y="25.130931640625" />
                  <Point X="-3.487762451172" Y="25.13626953125" />
                  <Point X="-3.511946289062" Y="25.149470703125" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.993620605469" Y="25.2813046875" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.85040625" Y="25.8018203125" />
                  <Point X="-4.824487792969" Y="25.976974609375" />
                  <Point X="-4.71893359375" Y="26.36650390625" />
                  <Point X="-4.703551757812" Y="26.423267578125" />
                  <Point X="-4.498993652344" Y="26.3963359375" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744984619141" Y="26.299341796875" />
                  <Point X="-3.723424072266" Y="26.301228515625" />
                  <Point X="-3.703138427734" Y="26.305263671875" />
                  <Point X="-3.660786621094" Y="26.3186171875" />
                  <Point X="-3.641712402344" Y="26.324630859375" />
                  <Point X="-3.622785400391" Y="26.332958984375" />
                  <Point X="-3.604041015625" Y="26.343779296875" />
                  <Point X="-3.587356201172" Y="26.35601171875" />
                  <Point X="-3.573715332031" Y="26.37156640625" />
                  <Point X="-3.561300537109" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.534357666016" Y="26.44845703125" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520916503906" Y="26.486791015625" />
                  <Point X="-3.517157714844" Y="26.50810546875" />
                  <Point X="-3.515804443359" Y="26.528744140625" />
                  <Point X="-3.518950683594" Y="26.549189453125" />
                  <Point X="-3.524552490234" Y="26.57009765625" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.552555175781" Y="26.628767578125" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.866419921875" Y="26.8973828125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.181862304688" Y="27.56112109375" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.801543212891" Y="28.09305859375" />
                  <Point X="-3.750504638672" Y="28.158662109375" />
                  <Point X="-3.657045898438" Y="28.104703125" />
                  <Point X="-3.206656738281" Y="27.844669921875" />
                  <Point X="-3.187729492188" Y="27.836341796875" />
                  <Point X="-3.167087402344" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.087809814453" Y="27.820634765625" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040560791016" Y="27.81876171875" />
                  <Point X="-3.019102294922" Y="27.821587890625" />
                  <Point X="-2.999013427734" Y="27.826505859375" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962210205078" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.904210449219" Y="27.902095703125" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848596191406" Y="28.095103515625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-2.986907226562" Y="28.384376953125" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-2.8760234375" Y="28.96020703125" />
                  <Point X="-2.70062109375" Y="29.094685546875" />
                  <Point X="-2.260249755859" Y="29.339345703125" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028893432617" Y="29.21480078125" />
                  <Point X="-2.012314697266" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.929465576172" Y="29.155220703125" />
                  <Point X="-1.899898925781" Y="29.139828125" />
                  <Point X="-1.880625976562" Y="29.13233203125" />
                  <Point X="-1.859719116211" Y="29.126728515625" />
                  <Point X="-1.83926953125" Y="29.123580078125" />
                  <Point X="-1.818623291016" Y="29.12493359375" />
                  <Point X="-1.797307495117" Y="29.128693359375" />
                  <Point X="-1.777453125" Y="29.134482421875" />
                  <Point X="-1.709075073242" Y="29.162806640625" />
                  <Point X="-1.678279174805" Y="29.1755625" />
                  <Point X="-1.660147216797" Y="29.185509765625" />
                  <Point X="-1.642417236328" Y="29.197923828125" />
                  <Point X="-1.626864746094" Y="29.2115625" />
                  <Point X="-1.614633056641" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.573224487305" Y="29.3365078125" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.571044677734" Y="29.531958984375" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.176698974609" Y="29.746146484375" />
                  <Point X="-0.949623535156" Y="29.80980859375" />
                  <Point X="-0.415770202637" Y="29.872291015625" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.267140838623" Y="29.783564453125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.206222259521" Y="29.510263671875" />
                  <Point X="0.307419586182" Y="29.8879375" />
                  <Point X="0.645774169922" Y="29.852501953125" />
                  <Point X="0.84403112793" Y="29.831740234375" />
                  <Point X="1.285725585938" Y="29.7251015625" />
                  <Point X="1.481038574219" Y="29.6779453125" />
                  <Point X="1.768061645508" Y="29.573841796875" />
                  <Point X="1.894646240234" Y="29.527927734375" />
                  <Point X="2.172640136719" Y="29.397919921875" />
                  <Point X="2.294558837891" Y="29.34090234375" />
                  <Point X="2.563155273438" Y="29.18441796875" />
                  <Point X="2.680977050781" Y="29.1157734375" />
                  <Point X="2.934262695312" Y="28.935650390625" />
                  <Point X="2.943259033203" Y="28.929251953125" />
                  <Point X="2.741653076172" Y="28.580060546875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.118274169922" Y="27.46069921875" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107727783203" Y="27.380951171875" />
                  <Point X="2.113499755859" Y="27.333083984375" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070800781" Y="27.24747265625" />
                  <Point X="2.169689208984" Y="27.203822265625" />
                  <Point X="2.183028564453" Y="27.1841640625" />
                  <Point X="2.194465576172" Y="27.170328125" />
                  <Point X="2.221596923828" Y="27.14559375" />
                  <Point X="2.265246582031" Y="27.115974609375" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304947021484" Y="27.0922734375" />
                  <Point X="2.327036132812" Y="27.084005859375" />
                  <Point X="2.34896484375" Y="27.078662109375" />
                  <Point X="2.396831542969" Y="27.072890625" />
                  <Point X="2.418389648438" Y="27.070291015625" />
                  <Point X="2.436468261719" Y="27.06984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.528563476563" Y="27.08897265625" />
                  <Point X="2.553494384766" Y="27.095640625" />
                  <Point X="2.565293457031" Y="27.099640625" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="3.051953857422" Y="27.37769921875" />
                  <Point X="3.967326660156" Y="27.906189453125" />
                  <Point X="4.0533828125" Y="27.786591796875" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.262198730469" Y="27.459884765625" />
                  <Point X="4.012935302734" Y="27.2686171875" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221424316406" Y="26.660240234375" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.164134277344" Y="26.589654296875" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136603271484" Y="26.55090625" />
                  <Point X="3.121629882812" Y="26.517083984375" />
                  <Point X="3.106789550781" Y="26.46401953125" />
                  <Point X="3.100105957031" Y="26.440119140625" />
                  <Point X="3.09665234375" Y="26.4178203125" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.109921875" Y="26.3127265625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.1206796875" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.169417480469" Y="26.185375" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198895751953" Y="26.145447265625" />
                  <Point X="3.216138427734" Y="26.129359375" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.282362548828" Y="26.089005859375" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320523193359" Y="26.0695" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.421041748047" Y="26.050857421875" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462699462891" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="3.928421875" Y="26.104939453125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.815918945312" Y="26.056109375" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.890433105469" Y="25.64701171875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.612942382812" Y="25.56976953125" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704786132812" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.617764160156" Y="25.278201171875" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574311523438" Y="25.251095703125" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.509260253906" Y="25.176810546875" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.450923828125" Y="25.0259921875" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.457935546875" Y="24.8748359375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.823333984375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.530294433594" Y="24.733826171875" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559992431641" Y="24.69876953125" />
                  <Point X="3.589035400391" Y="24.675841796875" />
                  <Point X="3.652818847656" Y="24.638974609375" />
                  <Point X="3.681545410156" Y="24.622369140625" />
                  <Point X="3.692710205078" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.120280273438" Y="24.499677734375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.871782714844" Y="24.162439453125" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.46391015625" Y="23.859703125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.249474853516" Y="23.96728125" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112397460938" Y="23.9060390625" />
                  <Point X="3.036731445313" Y="23.81503515625" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987933837891" Y="23.749671875" />
                  <Point X="2.976590087891" Y="23.722287109375" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.958912841797" Y="23.57678125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.045729492188" Y="23.324244140625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.485264648438" Y="22.95162109375" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.172055175781" Y="22.3266640625" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.726794677734" Y="22.288521484375" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786137207031" Y="22.829984375" />
                  <Point X="2.754224121094" Y="22.84017578125" />
                  <Point X="2.605234863281" Y="22.867083984375" />
                  <Point X="2.538133789062" Y="22.879201171875" />
                  <Point X="2.506777099609" Y="22.879603515625" />
                  <Point X="2.47460546875" Y="22.87464453125" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.321060302734" Y="22.799681640625" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.139390625" Y="22.585787109375" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.122582763672" Y="22.287751953125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.392560791016" Y="21.7569453125" />
                  <Point X="2.861283935547" Y="20.94509375" />
                  <Point X="2.837913330078" Y="20.928400390625" />
                  <Point X="2.781859619141" Y="20.88836328125" />
                  <Point X="2.701764160156" Y="20.83651953125" />
                  <Point X="2.465047607422" Y="21.145013671875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721923583984" Y="22.099443359375" />
                  <Point X="1.574979980469" Y="22.1939140625" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365722656" Y="22.256564453125" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.256392700195" Y="22.24409375" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156362548828" Y="22.2306015625" />
                  <Point X="1.128976928711" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="0.980500610352" Y="22.10135546875" />
                  <Point X="0.92461151123" Y="22.054884765625" />
                  <Point X="0.904141235352" Y="22.03113671875" />
                  <Point X="0.887249084473" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.838520690918" Y="21.803484375" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.887966430664" Y="21.158666015625" />
                  <Point X="1.022065551758" Y="20.140083984375" />
                  <Point X="0.975716918945" Y="20.12992578125" />
                  <Point X="0.929315368652" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058416870117" Y="20.247361328125" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.16506640625" Y="20.712109375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575317383" Y="20.905138671875" />
                  <Point X="-1.250209594727" Y="20.9290625" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.421834594727" Y="21.08126171875" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735839844" Y="21.15403125" />
                  <Point X="-1.53301940918" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531860352" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.621457275391" Y="21.201552734375" />
                  <Point X="-1.636814575195" Y="21.203830078125" />
                  <Point X="-1.850269042969" Y="21.2178203125" />
                  <Point X="-1.946403686523" Y="21.22412109375" />
                  <Point X="-1.961927978516" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.273298583984" Y="21.065345703125" />
                  <Point X="-2.353403320312" Y="21.011822265625" />
                  <Point X="-2.359686279297" Y="21.007240234375" />
                  <Point X="-2.380448242188" Y="20.98986328125" />
                  <Point X="-2.402761962891" Y="20.967224609375" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.480307128906" Y="20.867357421875" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.621545410156" Y="20.910796875" />
                  <Point X="-2.747598388672" Y="20.988845703125" />
                  <Point X="-2.980862060547" Y="21.168451171875" />
                  <Point X="-2.854759277344" Y="21.386869140625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314667236328" Y="22.442390625" />
                  <Point X="-2.323652587891" Y="22.4720078125" />
                  <Point X="-2.329359375" Y="22.486447265625" />
                  <Point X="-2.343577880859" Y="22.51527734375" />
                  <Point X="-2.351559326172" Y="22.52859375" />
                  <Point X="-2.369587646484" Y="22.553751953125" />
                  <Point X="-2.379634521484" Y="22.56559375" />
                  <Point X="-2.396983154297" Y="22.58294140625" />
                  <Point X="-2.408825927734" Y="22.59298828125" />
                  <Point X="-2.433986328125" Y="22.611015625" />
                  <Point X="-2.447303955078" Y="22.61899609375" />
                  <Point X="-2.476132568359" Y="22.6332109375" />
                  <Point X="-2.490572998047" Y="22.638916015625" />
                  <Point X="-2.520189453125" Y="22.6478984375" />
                  <Point X="-2.550873535156" Y="22.6519375" />
                  <Point X="-2.581805664062" Y="22.650923828125" />
                  <Point X="-2.597229736328" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643680175781" Y="22.638615234375" />
                  <Point X="-2.67264453125" Y="22.62771484375" />
                  <Point X="-2.68668359375" Y="22.621076171875" />
                  <Point X="-3.087072509766" Y="22.389912109375" />
                  <Point X="-3.793089355469" Y="21.98229296875" />
                  <Point X="-3.904438476562" Y="22.12858203125" />
                  <Point X="-4.004020019531" Y="22.259412109375" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.946178710938" Y="22.737013671875" />
                  <Point X="-3.048122070312" Y="23.426115234375" />
                  <Point X="-3.036482910156" Y="23.4366875" />
                  <Point X="-3.015106933594" Y="23.45960546875" />
                  <Point X="-3.005370361328" Y="23.471951171875" />
                  <Point X="-2.987406005859" Y="23.499080078125" />
                  <Point X="-2.979837402344" Y="23.512869140625" />
                  <Point X="-2.967079589844" Y="23.54150390625" />
                  <Point X="-2.961890380859" Y="23.556349609375" />
                  <Point X="-2.956578369141" Y="23.576861328125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748779297" Y="23.631619140625" />
                  <Point X="-2.948577880859" Y="23.6469453125" />
                  <Point X="-2.950786132812" Y="23.6786171875" />
                  <Point X="-2.953082763672" Y="23.6937734375" />
                  <Point X="-2.960083251953" Y="23.723517578125" />
                  <Point X="-2.971775146484" Y="23.751748046875" />
                  <Point X="-2.987856201172" Y="23.777732421875" />
                  <Point X="-2.99694921875" Y="23.79007421875" />
                  <Point X="-3.017782470703" Y="23.814033203125" />
                  <Point X="-3.028743896484" Y="23.82475390625" />
                  <Point X="-3.052247558594" Y="23.844291015625" />
                  <Point X="-3.064789794922" Y="23.853107421875" />
                  <Point X="-3.083049072266" Y="23.863853515625" />
                  <Point X="-3.105436279297" Y="23.87551953125" />
                  <Point X="-3.134700927734" Y="23.886744140625" />
                  <Point X="-3.149801757812" Y="23.891142578125" />
                  <Point X="-3.181688964844" Y="23.897623046875" />
                  <Point X="-3.197305419922" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-3.749781738281" Y="23.833259765625" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.701814941406" Y="23.87340625" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.531309082031" Y="24.413708984375" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.5004765625" Y="24.69028515625" />
                  <Point X="-3.473930908203" Y="24.700748046875" />
                  <Point X="-3.444166015625" Y="24.716107421875" />
                  <Point X="-3.433561523438" Y="24.722486328125" />
                  <Point X="-3.414426513672" Y="24.735767578125" />
                  <Point X="-3.393673339844" Y="24.751755859375" />
                  <Point X="-3.371220947266" Y="24.77363671875" />
                  <Point X="-3.360903808594" Y="24.785509765625" />
                  <Point X="-3.341656982422" Y="24.811767578125" />
                  <Point X="-3.333439208984" Y="24.825181640625" />
                  <Point X="-3.319329589844" Y="24.85318359375" />
                  <Point X="-3.313437744141" Y="24.867771484375" />
                  <Point X="-3.307059326172" Y="24.888322265625" />
                  <Point X="-3.300990722656" Y="24.911498046875" />
                  <Point X="-3.296721191406" Y="24.939658203125" />
                  <Point X="-3.295647705078" Y="24.9538984375" />
                  <Point X="-3.295647705078" Y="24.9835390625" />
                  <Point X="-3.296720947266" Y="24.997779296875" />
                  <Point X="-3.300989746094" Y="25.0259375" />
                  <Point X="-3.304185302734" Y="25.03985546875" />
                  <Point X="-3.310563720703" Y="25.060408203125" />
                  <Point X="-3.319327880859" Y="25.084251953125" />
                  <Point X="-3.3334375" Y="25.112255859375" />
                  <Point X="-3.341655517578" Y="25.125671875" />
                  <Point X="-3.360901855469" Y="25.1519296875" />
                  <Point X="-3.371222900391" Y="25.163806640625" />
                  <Point X="-3.393681396484" Y="25.18569140625" />
                  <Point X="-3.405818847656" Y="25.19569921875" />
                  <Point X="-3.424953857422" Y="25.208978515625" />
                  <Point X="-3.442244873047" Y="25.219654296875" />
                  <Point X="-3.466428710938" Y="25.23285546875" />
                  <Point X="-3.476638916016" Y="25.237666015625" />
                  <Point X="-3.497568847656" Y="25.246044921875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.969032958984" Y="25.373068359375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.7564296875" Y="25.7879140625" />
                  <Point X="-4.731331054688" Y="25.95752734375" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-4.511394042969" Y="26.3021484375" />
                  <Point X="-3.778066894531" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747056640625" Y="26.204365234375" />
                  <Point X="-3.736703125" Y="26.204703125" />
                  <Point X="-3.715142578125" Y="26.20658984375" />
                  <Point X="-3.704890136719" Y="26.2080546875" />
                  <Point X="-3.684604492188" Y="26.21208984375" />
                  <Point X="-3.674571289062" Y="26.21466015625" />
                  <Point X="-3.632219482422" Y="26.228013671875" />
                  <Point X="-3.603451416016" Y="26.23767578125" />
                  <Point X="-3.584524414062" Y="26.24600390625" />
                  <Point X="-3.575291259766" Y="26.25068359375" />
                  <Point X="-3.556546875" Y="26.26150390625" />
                  <Point X="-3.547870849609" Y="26.2671640625" />
                  <Point X="-3.531186035156" Y="26.279396484375" />
                  <Point X="-3.515930908203" Y="26.293375" />
                  <Point X="-3.502290039063" Y="26.3089296875" />
                  <Point X="-3.495895507812" Y="26.317078125" />
                  <Point X="-3.483480712891" Y="26.33480859375" />
                  <Point X="-3.478013427734" Y="26.343599609375" />
                  <Point X="-3.468064453125" Y="26.361732421875" />
                  <Point X="-3.463582763672" Y="26.37107421875" />
                  <Point X="-3.446588867188" Y="26.4121015625" />
                  <Point X="-3.435499023438" Y="26.4403515625" />
                  <Point X="-3.429711425781" Y="26.460208984375" />
                  <Point X="-3.427360107422" Y="26.47029296875" />
                  <Point X="-3.423601318359" Y="26.491607421875" />
                  <Point X="-3.422361328125" Y="26.501890625" />
                  <Point X="-3.421008056641" Y="26.522529296875" />
                  <Point X="-3.421909667969" Y="26.543193359375" />
                  <Point X="-3.425055908203" Y="26.563638671875" />
                  <Point X="-3.427187255859" Y="26.573775390625" />
                  <Point X="-3.4327890625" Y="26.59468359375" />
                  <Point X="-3.436011230469" Y="26.60452734375" />
                  <Point X="-3.443509033203" Y="26.62380859375" />
                  <Point X="-3.447784667969" Y="26.63324609375" />
                  <Point X="-3.468289550781" Y="26.672634765625" />
                  <Point X="-3.482800048828" Y="26.699287109375" />
                  <Point X="-3.494291992188" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521501708984" Y="26.748912109375" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.808587402344" Y="26.972751953125" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.099815917969" Y="27.513232421875" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.7265625" Y="28.034724609375" />
                  <Point X="-3.726337890625" Y="28.035013671875" />
                  <Point X="-3.704546386719" Y="28.022431640625" />
                  <Point X="-3.254157226562" Y="27.7623984375" />
                  <Point X="-3.244917480469" Y="27.75771484375" />
                  <Point X="-3.225990234375" Y="27.74938671875" />
                  <Point X="-3.216302001953" Y="27.745740234375" />
                  <Point X="-3.195659912109" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165330078125" Y="27.73262109375" />
                  <Point X="-3.155073486328" Y="27.73115625" />
                  <Point X="-3.096089111328" Y="27.72599609375" />
                  <Point X="-3.069524169922" Y="27.723671875" />
                  <Point X="-3.059173095703" Y="27.723333984375" />
                  <Point X="-3.038489013672" Y="27.72378515625" />
                  <Point X="-3.028156005859" Y="27.72457421875" />
                  <Point X="-3.006697509766" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423583984" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938450195312" Y="27.750447265625" />
                  <Point X="-2.929421630859" Y="27.755529296875" />
                  <Point X="-2.911167480469" Y="27.767158203125" />
                  <Point X="-2.902748291016" Y="27.77319140625" />
                  <Point X="-2.886615966797" Y="27.786138671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.837035400391" Y="27.834919921875" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753957763672" Y="28.1033828125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.904634765625" Y="28.431876953125" />
                  <Point X="-3.05938671875" Y="28.6999140625" />
                  <Point X="-2.818221191406" Y="28.884814453125" />
                  <Point X="-2.648368408203" Y="29.015037109375" />
                  <Point X="-2.214112304688" Y="29.25630078125" />
                  <Point X="-2.192523193359" Y="29.268294921875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111822998047" Y="29.16405078125" />
                  <Point X="-2.097520996094" Y="29.149109375" />
                  <Point X="-2.089960449219" Y="29.142029296875" />
                  <Point X="-2.073381835938" Y="29.1281171875" />
                  <Point X="-2.065092773438" Y="29.1218984375" />
                  <Point X="-2.047892822266" Y="29.11040625" />
                  <Point X="-2.038981933594" Y="29.105130859375" />
                  <Point X="-1.973332641602" Y="29.070955078125" />
                  <Point X="-1.943765991211" Y="29.0555625" />
                  <Point X="-1.934335571289" Y="29.0512890625" />
                  <Point X="-1.9150625" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313232422" Y="29.034966796875" />
                  <Point X="-1.874175048828" Y="29.032833984375" />
                  <Point X="-1.853725463867" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812408691406" Y="29.03013671875" />
                  <Point X="-1.802121582031" Y="29.031376953125" />
                  <Point X="-1.780805786133" Y="29.03513671875" />
                  <Point X="-1.770715087891" Y="29.037490234375" />
                  <Point X="-1.750860717773" Y="29.043279296875" />
                  <Point X="-1.741097045898" Y="29.04671484375" />
                  <Point X="-1.672718994141" Y="29.0750390625" />
                  <Point X="-1.641923095703" Y="29.087794921875" />
                  <Point X="-1.632586181641" Y="29.0922734375" />
                  <Point X="-1.614454101562" Y="29.102220703125" />
                  <Point X="-1.605659301758" Y="29.107689453125" />
                  <Point X="-1.587929077148" Y="29.120103515625" />
                  <Point X="-1.579780639648" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252929688" Y="29.15538671875" />
                  <Point X="-1.538021240234" Y="29.172068359375" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153894043" Y="29.19948828125" />
                  <Point X="-1.516855834961" Y="29.208728515625" />
                  <Point X="-1.508525146484" Y="29.227662109375" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.482621459961" Y="29.30794140625" />
                  <Point X="-1.472597900391" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.476857421875" Y="29.544359375" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.151053100586" Y="29.654673828125" />
                  <Point X="-0.931164855957" Y="29.7163203125" />
                  <Point X="-0.404726745605" Y="29.777935546875" />
                  <Point X="-0.36522265625" Y="29.78255859375" />
                  <Point X="-0.358903778076" Y="29.7589765625" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.297985198975" Y="29.48567578125" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.635879089355" Y="29.75801953125" />
                  <Point X="0.827852722168" Y="29.737916015625" />
                  <Point X="1.263430419922" Y="29.63275390625" />
                  <Point X="1.453622924805" Y="29.586833984375" />
                  <Point X="1.735669799805" Y="29.48453515625" />
                  <Point X="1.858247314453" Y="29.44007421875" />
                  <Point X="2.132395507812" Y="29.311865234375" />
                  <Point X="2.250430664063" Y="29.2566640625" />
                  <Point X="2.515332519531" Y="29.10233203125" />
                  <Point X="2.629432617188" Y="29.03585546875" />
                  <Point X="2.817778808594" Y="28.9019140625" />
                  <Point X="2.659380615234" Y="28.627560546875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.026498779297" Y="27.485240234375" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017912719727" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755371094" Y="27.383240234375" />
                  <Point X="2.013410888672" Y="27.369578125" />
                  <Point X="2.019182983398" Y="27.3217109375" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.02914440918" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045320922852" Y="27.2238828125" />
                  <Point X="2.055682373047" Y="27.203841796875" />
                  <Point X="2.061459228516" Y="27.1941328125" />
                  <Point X="2.091077636719" Y="27.150482421875" />
                  <Point X="2.104416992188" Y="27.13082421875" />
                  <Point X="2.109806152344" Y="27.12363671875" />
                  <Point X="2.130463134766" Y="27.100123046875" />
                  <Point X="2.157594482422" Y="27.075388671875" />
                  <Point X="2.168254638672" Y="27.066982421875" />
                  <Point X="2.211904296875" Y="27.03736328125" />
                  <Point X="2.231563232422" Y="27.0240234375" />
                  <Point X="2.241276855469" Y="27.01824609375" />
                  <Point X="2.261318359375" Y="27.007884765625" />
                  <Point X="2.271646240234" Y="27.00330078125" />
                  <Point X="2.293735351562" Y="26.995033203125" />
                  <Point X="2.304543945312" Y="26.99170703125" />
                  <Point X="2.32647265625" Y="26.98636328125" />
                  <Point X="2.337592773438" Y="26.984345703125" />
                  <Point X="2.385459472656" Y="26.97857421875" />
                  <Point X="2.407017578125" Y="26.975974609375" />
                  <Point X="2.416040039062" Y="26.9753203125" />
                  <Point X="2.447577880859" Y="26.97549609375" />
                  <Point X="2.484317626953" Y="26.979822265625" />
                  <Point X="2.497749755859" Y="26.98239453125" />
                  <Point X="2.553105224609" Y="26.997197265625" />
                  <Point X="2.578036132812" Y="27.003865234375" />
                  <Point X="2.583995361328" Y="27.005669921875" />
                  <Point X="2.604419921875" Y="27.013072265625" />
                  <Point X="2.62766015625" Y="27.023576171875" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="3.099453613281" Y="27.29542578125" />
                  <Point X="3.940405517578" Y="27.78094921875" />
                  <Point X="3.976270507812" Y="27.73110546875" />
                  <Point X="4.043958251953" Y="27.63703515625" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="3.955102783203" Y="27.343986328125" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168134033203" Y="26.739865234375" />
                  <Point X="3.152119140625" Y="26.72521484375" />
                  <Point X="3.134668701172" Y="26.7066015625" />
                  <Point X="3.128576904297" Y="26.699421875" />
                  <Point X="3.088737304688" Y="26.64744921875" />
                  <Point X="3.070794677734" Y="26.624041015625" />
                  <Point X="3.065634033203" Y="26.616599609375" />
                  <Point X="3.049735351562" Y="26.58936328125" />
                  <Point X="3.034761962891" Y="26.555541015625" />
                  <Point X="3.030140380859" Y="26.542669921875" />
                  <Point X="3.015300048828" Y="26.48960546875" />
                  <Point X="3.008616455078" Y="26.465705078125" />
                  <Point X="3.006225341797" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421107421875" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.016881835938" Y="26.293529296875" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024597167969" Y="26.258236328125" />
                  <Point X="3.034683837891" Y="26.228607421875" />
                  <Point X="3.050287109375" Y="26.195369140625" />
                  <Point X="3.056918945312" Y="26.1835234375" />
                  <Point X="3.090053466797" Y="26.13316015625" />
                  <Point X="3.1049765625" Y="26.110478515625" />
                  <Point X="3.111740722656" Y="26.101421875" />
                  <Point X="3.126295898438" Y="26.08417578125" />
                  <Point X="3.134086914063" Y="26.075986328125" />
                  <Point X="3.151329589844" Y="26.0598984375" />
                  <Point X="3.160035888672" Y="26.0526953125" />
                  <Point X="3.178243652344" Y="26.03937109375" />
                  <Point X="3.187745117188" Y="26.03325" />
                  <Point X="3.235761474609" Y="26.006220703125" />
                  <Point X="3.257387207031" Y="25.994046875" />
                  <Point X="3.265479003906" Y="25.989986328125" />
                  <Point X="3.294681396484" Y="25.97808203125" />
                  <Point X="3.330278320312" Y="25.96801953125" />
                  <Point X="3.343673095703" Y="25.965255859375" />
                  <Point X="3.408594726563" Y="25.95667578125" />
                  <Point X="3.437833740234" Y="25.9528125" />
                  <Point X="3.444034179688" Y="25.95219921875" />
                  <Point X="3.465709960938" Y="25.95122265625" />
                  <Point X="3.491214111328" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="3.940821777344" Y="26.010751953125" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.723614746094" Y="26.033638671875" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.588354003906" Y="25.661533203125" />
                  <Point X="3.691991210938" Y="25.421353515625" />
                  <Point X="3.686025634766" Y="25.41954296875" />
                  <Point X="3.66562109375" Y="25.412134765625" />
                  <Point X="3.642382568359" Y="25.401619140625" />
                  <Point X="3.634007080078" Y="25.397318359375" />
                  <Point X="3.570223632812" Y="25.360451171875" />
                  <Point X="3.541497070312" Y="25.34384765625" />
                  <Point X="3.533877929688" Y="25.338943359375" />
                  <Point X="3.508775878906" Y="25.31987109375" />
                  <Point X="3.481994628906" Y="25.2943515625" />
                  <Point X="3.472795898438" Y="25.2842265625" />
                  <Point X="3.434525878906" Y="25.2354609375" />
                  <Point X="3.417290039063" Y="25.213498046875" />
                  <Point X="3.410853515625" Y="25.20420703125" />
                  <Point X="3.399130126953" Y="25.184927734375" />
                  <Point X="3.393843261719" Y="25.174939453125" />
                  <Point X="3.384069580078" Y="25.1534765625" />
                  <Point X="3.380005615234" Y="25.1429296875" />
                  <Point X="3.373158935547" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.357619384766" Y="25.043861328125" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.364631347656" Y="24.856966796875" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.816013671875" />
                  <Point X="3.380005371094" Y="24.79451171875" />
                  <Point X="3.384069580078" Y="24.783962890625" />
                  <Point X="3.393843261719" Y="24.7625" />
                  <Point X="3.399130126953" Y="24.75251171875" />
                  <Point X="3.410853515625" Y="24.733232421875" />
                  <Point X="3.417290039063" Y="24.72394140625" />
                  <Point X="3.455560058594" Y="24.67517578125" />
                  <Point X="3.472795898438" Y="24.653212890625" />
                  <Point X="3.478716064453" Y="24.646369140625" />
                  <Point X="3.501127685547" Y="24.624205078125" />
                  <Point X="3.530170654297" Y="24.60127734375" />
                  <Point X="3.541494873047" Y="24.593591796875" />
                  <Point X="3.605278320313" Y="24.556724609375" />
                  <Point X="3.634004882812" Y="24.540119140625" />
                  <Point X="3.639504150391" Y="24.537177734375" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683025878906" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.095692626953" Y="24.4079140625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.777844238281" Y="24.1766015625" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="4.476310546875" Y="23.953890625" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481933594" Y="24.087322265625" />
                  <Point X="3.229297607422" Y="24.06011328125" />
                  <Point X="3.172917724609" Y="24.047859375" />
                  <Point X="3.157874267578" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114675048828" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123535156" Y="23.99846875" />
                  <Point X="3.050373779297" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.963683105469" Y="23.875771484375" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921327392578" Y="23.823154296875" />
                  <Point X="2.906607910156" Y="23.798775390625" />
                  <Point X="2.900166015625" Y="23.786029296875" />
                  <Point X="2.888822265625" Y="23.75864453125" />
                  <Point X="2.884363769531" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.8643125" Y="23.585486328125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.965819580078" Y="23.272869140625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.427432373047" Y="22.876251953125" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.045496337891" Y="22.302580078125" />
                  <Point X="4.001273193359" Y="22.23974609375" />
                  <Point X="3.774294677734" Y="22.37079296875" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841199707031" Y="22.909109375" />
                  <Point X="2.815037353516" Y="22.920482421875" />
                  <Point X="2.783124267578" Y="22.930673828125" />
                  <Point X="2.771108398438" Y="22.9336640625" />
                  <Point X="2.622119140625" Y="22.960572265625" />
                  <Point X="2.555018066406" Y="22.972689453125" />
                  <Point X="2.539352539062" Y="22.974193359375" />
                  <Point X="2.507995849609" Y="22.974595703125" />
                  <Point X="2.4923046875" Y="22.973494140625" />
                  <Point X="2.460133056641" Y="22.96853515625" />
                  <Point X="2.444841064453" Y="22.964861328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400589599609" Y="22.948890625" />
                  <Point X="2.276816162109" Y="22.88375" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.208967773438" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144942138672" Y="22.78883984375" />
                  <Point X="2.128047607422" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.055322509766" Y="22.63003125" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.029095092773" Y="22.270869140625" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236816406" Y="22.168453125" />
                  <Point X="2.0640703125" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.310288574219" Y="21.7094453125" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723752929688" Y="20.96391796875" />
                  <Point X="2.540416015625" Y="21.202845703125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251831055" Y="22.17199609375" />
                  <Point X="1.773298217773" Y="22.179353515625" />
                  <Point X="1.626354614258" Y="22.27382421875" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470703125" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926513672" Y="22.34884765625" />
                  <Point X="1.455385009766" Y="22.3513046875" />
                  <Point X="1.424119995117" Y="22.35362109375" />
                  <Point X="1.408396362305" Y="22.35348046875" />
                  <Point X="1.247688354492" Y="22.338693359375" />
                  <Point X="1.175309326172" Y="22.332033203125" />
                  <Point X="1.161226196289" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120007080078" Y="22.318369140625" />
                  <Point X="1.092621337891" Y="22.307025390625" />
                  <Point X="1.07987512207" Y="22.300583984375" />
                  <Point X="1.055493408203" Y="22.28586328125" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.919763244629" Y="22.17440234375" />
                  <Point X="0.863874206543" Y="22.127931640625" />
                  <Point X="0.85265435791" Y="22.11691015625" />
                  <Point X="0.832184082031" Y="22.093162109375" />
                  <Point X="0.82293347168" Y="22.080435546875" />
                  <Point X="0.806041320801" Y="22.052609375" />
                  <Point X="0.799019165039" Y="22.03853125" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.745688232422" Y="21.823662109375" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.793779174805" Y="21.146265625" />
                  <Point X="0.83309173584" Y="20.847658203125" />
                  <Point X="0.655064819336" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143676758" Y="21.546423828125" />
                  <Point X="0.626786621094" Y="21.576185546875" />
                  <Point X="0.620407287598" Y="21.586791015625" />
                  <Point X="0.507521057129" Y="21.7494375" />
                  <Point X="0.4566796875" Y="21.82269140625" />
                  <Point X="0.446668609619" Y="21.834830078125" />
                  <Point X="0.424785369873" Y="21.857283203125" />
                  <Point X="0.412913513184" Y="21.86759765625" />
                  <Point X="0.38665838623" Y="21.886841796875" />
                  <Point X="0.373244720459" Y="21.895060546875" />
                  <Point X="0.345242675781" Y="21.909169921875" />
                  <Point X="0.330654022217" Y="21.915060546875" />
                  <Point X="0.155969039917" Y="21.969275390625" />
                  <Point X="0.077294914246" Y="21.993693359375" />
                  <Point X="0.063380393982" Y="21.996888671875" />
                  <Point X="0.035227645874" Y="22.001158203125" />
                  <Point X="0.020989425659" Y="22.002232421875" />
                  <Point X="-0.008651480675" Y="22.002234375" />
                  <Point X="-0.022895498276" Y="22.001162109375" />
                  <Point X="-0.051061767578" Y="21.996892578125" />
                  <Point X="-0.06498387146" Y="21.9936953125" />
                  <Point X="-0.239668838501" Y="21.939478515625" />
                  <Point X="-0.318342803955" Y="21.9150625" />
                  <Point X="-0.332928222656" Y="21.909171875" />
                  <Point X="-0.360928344727" Y="21.895064453125" />
                  <Point X="-0.374343322754" Y="21.88684765625" />
                  <Point X="-0.400599212646" Y="21.867603515625" />
                  <Point X="-0.412475952148" Y="21.857283203125" />
                  <Point X="-0.43436038208" Y="21.834826171875" />
                  <Point X="-0.444367889404" Y="21.822689453125" />
                  <Point X="-0.557254455566" Y="21.660041015625" />
                  <Point X="-0.60809564209" Y="21.5867890625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.767142944336" Y="21.047833984375" />
                  <Point X="-0.985425109863" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.690692629394" Y="25.688955790865" />
                  <Point X="4.654320849023" Y="26.104687142852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.743866137932" Y="23.991177048812" />
                  <Point X="4.722090691411" Y="24.240071541471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.597514165038" Y="25.663987753605" />
                  <Point X="4.560043870727" Y="26.092275177378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.6538098432" Y="23.930522449459" />
                  <Point X="4.624438600634" Y="24.26623728819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.504335628004" Y="25.639020547059" />
                  <Point X="4.465766892432" Y="26.079863211905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.55733576281" Y="23.943223475843" />
                  <Point X="4.526786509856" Y="24.292403034909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.411157083047" Y="25.614053431082" />
                  <Point X="4.371489914136" Y="26.067451246431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.460861683389" Y="23.955924491142" />
                  <Point X="4.429134419079" Y="24.318568781627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.317978538089" Y="25.589086315105" />
                  <Point X="4.277212935841" Y="26.055039280958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.364387609055" Y="23.968625448304" />
                  <Point X="4.331482328302" Y="24.344734528346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.224799993131" Y="25.564119199128" />
                  <Point X="4.182935957545" Y="26.042627315484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.061911466644" Y="27.425943576399" />
                  <Point X="4.043372073594" Y="27.637849808624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267913534721" Y="23.981326405466" />
                  <Point X="4.233830237525" Y="24.370900275065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.131621448174" Y="25.539152083151" />
                  <Point X="4.088658979249" Y="26.030215350011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.972547786159" Y="27.357372359975" />
                  <Point X="3.935725983474" Y="27.778247490539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.171439460386" Y="23.994027362628" />
                  <Point X="4.136178146747" Y="24.397066021784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.038442903216" Y="25.514184967173" />
                  <Point X="3.994382000954" Y="26.017803384537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.883184103428" Y="27.288801169215" />
                  <Point X="3.8449484159" Y="27.725837077492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.074965386052" Y="24.006728319791" />
                  <Point X="4.038526068706" Y="24.423231622935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945264358258" Y="25.489217851196" />
                  <Point X="3.90010501042" Y="26.005391558945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.793820420152" Y="27.220229984681" />
                  <Point X="3.754170848326" Y="27.673426664446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.978491311718" Y="24.019429276953" />
                  <Point X="3.940873999683" Y="24.449397120995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.852085813301" Y="25.464250735219" />
                  <Point X="3.805828003788" Y="25.992979917357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.704456736877" Y="27.151658800147" />
                  <Point X="3.663393280751" Y="27.621016251399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.034678060306" Y="22.287209043525" />
                  <Point X="4.023134345591" Y="22.419154306486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.882017237384" Y="24.032130234115" />
                  <Point X="3.843221930661" Y="24.475562619054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.758907268343" Y="25.439283619242" />
                  <Point X="3.711550997156" Y="25.98056827577" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615093053602" Y="27.083087615613" />
                  <Point X="3.572615713177" Y="27.568605838352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.940392443467" Y="22.274895817058" />
                  <Point X="3.920908823397" Y="22.49759461351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.785543163049" Y="24.044831191277" />
                  <Point X="3.745569861639" Y="24.501728117114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.665910410774" Y="25.412239806913" />
                  <Point X="3.617273990524" Y="25.968156634182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.525729370326" Y="27.014516431079" />
                  <Point X="3.481838145603" Y="27.516195425306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.839956340572" Y="22.332882967891" />
                  <Point X="3.818683301202" Y="22.576034920535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689069088715" Y="24.05753214844" />
                  <Point X="3.647429996841" Y="24.533469146397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.574844759233" Y="25.363122208665" />
                  <Point X="3.522996983892" Y="25.955744992595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436365687051" Y="26.945945246545" />
                  <Point X="3.391060578028" Y="27.463785012259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.739520251359" Y="22.390869962348" />
                  <Point X="3.716457779007" Y="22.65447522756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.592595014381" Y="24.070233105602" />
                  <Point X="3.547090001627" Y="24.590357781417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.485228895798" Y="25.297433456549" />
                  <Point X="3.427774376268" Y="25.954141619807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.347002003776" Y="26.877374062011" />
                  <Point X="3.300283010454" Y="27.411374599212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.639084187978" Y="22.448856661531" />
                  <Point X="3.614232256812" Y="22.732915534585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.496120940047" Y="24.082934062764" />
                  <Point X="3.442894516708" Y="24.69131486541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.399636177702" Y="25.185759942783" />
                  <Point X="3.331214225868" Y="25.967826430897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.2576383205" Y="26.808802877477" />
                  <Point X="3.20950544288" Y="27.358964186166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.538648124598" Y="22.506843360714" />
                  <Point X="3.512006734617" Y="22.81135584161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.399969584043" Y="24.091946332529" />
                  <Point X="3.232322933042" Y="26.008156321857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.168295127092" Y="26.739997492687" />
                  <Point X="3.118727875305" Y="27.306553773119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.438212061218" Y="22.564830059897" />
                  <Point X="3.409781209737" Y="22.889796179338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.305934421743" Y="24.076770397577" />
                  <Point X="3.130715661778" Y="26.079529988398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.081818834206" Y="26.638423284991" />
                  <Point X="3.027950296606" Y="27.25414348723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.337775997837" Y="22.62281675908" />
                  <Point X="3.307555671985" Y="22.968236664174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212351088412" Y="24.056430033878" />
                  <Point X="3.016445072799" Y="26.295646038759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003865934306" Y="26.439426249661" />
                  <Point X="2.937172714908" Y="27.201733235617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.792288789032" Y="28.857764086216" />
                  <Point X="2.786478768771" Y="28.924172921669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.237339934457" Y="22.680803458263" />
                  <Point X="3.205330134234" Y="23.046677149011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.119558977125" Y="24.027045960839" />
                  <Point X="2.84639513321" Y="27.149322984005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.709475058172" Y="28.714326602988" />
                  <Point X="2.684789043999" Y="28.996489036136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.136903871076" Y="22.738790157445" />
                  <Point X="3.103104596483" Y="23.125117633848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.030409715824" Y="23.956023921919" />
                  <Point X="2.755617551512" Y="27.096912732392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.626661330358" Y="28.570889084951" />
                  <Point X="2.583648310982" Y="29.062530146121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036467807696" Y="22.796776856628" />
                  <Point X="2.99934979282" Y="23.22103770804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.944125809963" Y="23.852250720454" />
                  <Point X="2.664839969814" Y="27.04450248078" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.543847607206" Y="28.42745151362" />
                  <Point X="2.483163528152" Y="29.12107371116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.936031744316" Y="22.854763555811" />
                  <Point X="2.888720071165" Y="23.395538454449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868305427606" Y="23.628878898077" />
                  <Point X="2.573146791931" Y="27.002557541453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.461033884055" Y="28.284013942289" />
                  <Point X="2.382678842193" Y="29.179616168955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835705250209" Y="22.911497872463" />
                  <Point X="2.479819327513" Y="26.979292582691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.378220160904" Y="28.140576370958" />
                  <Point X="2.282194156235" Y="29.23815862675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.73787800555" Y="22.93966563721" />
                  <Point X="2.384509268066" Y="26.978688788817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.295406437752" Y="27.997138799627" />
                  <Point X="2.182429967978" Y="29.288465758126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.640984111313" Y="22.95716515782" />
                  <Point X="2.287512667532" Y="26.997362247766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.212592714601" Y="27.853701228297" />
                  <Point X="2.082998818258" Y="29.334966241614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.719525795551" Y="20.969426840694" />
                  <Point X="2.716308806764" Y="21.006197190791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544171930211" Y="22.973730693018" />
                  <Point X="2.187182492437" Y="27.054138638316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129778991449" Y="27.710263656966" />
                  <Point X="1.983567687443" Y="29.381466509009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.611890715378" Y="21.109698678346" />
                  <Point X="2.603914218718" Y="21.200870452362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449487355374" Y="22.965977577339" />
                  <Point X="2.082252764819" Y="27.163488154762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.047666827767" Y="27.558807224201" />
                  <Point X="1.884136556629" Y="29.427966776403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.504255605198" Y="21.24997085898" />
                  <Point X="2.491519630672" Y="21.395543713933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357598865408" Y="22.926265065335" />
                  <Point X="1.785402787743" Y="29.466496160479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.396620435705" Y="21.390243717555" />
                  <Point X="2.379125042627" Y="21.590216975504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.266433625652" Y="22.878285765598" />
                  <Point X="1.686914581774" Y="29.502218747566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.288985266213" Y="21.530516576129" />
                  <Point X="2.266730493558" Y="21.78488979157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.176052084164" Y="22.821348753672" />
                  <Point X="1.588426445003" Y="29.537940543725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.181350096721" Y="21.670789434703" />
                  <Point X="2.154336006085" Y="21.979561903585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.091425679844" Y="22.698630222908" />
                  <Point X="1.489938308232" Y="29.573662339883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073714927229" Y="21.811062293278" />
                  <Point X="2.037613862465" Y="22.223699351715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01064528029" Y="22.531951656502" />
                  <Point X="1.392124000573" Y="29.601682234036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.966079757736" Y="21.951335151852" />
                  <Point X="1.294703279515" Y="29.625203412763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.858444588244" Y="22.091608010426" />
                  <Point X="1.197282604762" Y="29.648724062226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.754338544716" Y="22.191542774646" />
                  <Point X="1.0998619519" Y="29.672244461467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.653292116346" Y="22.256505977589" />
                  <Point X="1.002441299038" Y="29.695764860708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.552326770504" Y="22.320542402993" />
                  <Point X="0.905020646177" Y="29.719285259949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.454265276688" Y="22.351387647855" />
                  <Point X="0.807844469494" Y="29.74001128368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.35911600244" Y="22.348946070737" />
                  <Point X="0.711599807735" Y="29.750090043107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.264514661595" Y="22.340241586168" />
                  <Point X="0.615355124145" Y="29.760169052058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.169948940749" Y="22.33112996315" />
                  <Point X="0.519110360015" Y="29.770248981594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.077389764957" Y="22.299083425209" />
                  <Point X="0.422865595885" Y="29.78032891113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.987973155059" Y="22.231117194748" />
                  <Point X="0.339670663729" Y="29.641248578662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.899077002032" Y="22.157202115021" />
                  <Point X="0.267780763887" Y="29.372951135554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812005278725" Y="22.062433708173" />
                  <Point X="0.189931559446" Y="29.172768855709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.817990852202" Y="20.90401553193" />
                  <Point X="0.802774403326" Y="21.077940338448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.739872930425" Y="21.796907463628" />
                  <Point X="0.100456769077" Y="29.105467631063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.6763952241" Y="21.432458208639" />
                  <Point X="0.006874659372" Y="29.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.559903207177" Y="21.673965296579" />
                  <Point X="-0.090352736376" Y="29.106424741578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.450917174562" Y="21.829678591302" />
                  <Point X="-0.193503713891" Y="29.19544305132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.348754511503" Y="21.907400415123" />
                  <Point X="-0.328117168252" Y="29.644079116984" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.250546304399" Y="21.939922600541" />
                  <Point X="-0.434882186515" Y="29.77440610149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.152521777445" Y="21.970345312236" />
                  <Point X="-0.529278469593" Y="29.763357795921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.054721765624" Y="21.998201804228" />
                  <Point X="-0.623674752671" Y="29.752309490353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.040664463524" Y="21.998468634013" />
                  <Point X="-0.71807103575" Y="29.741261184784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.133742675934" Y="21.972354711768" />
                  <Point X="-0.812467318828" Y="29.730212879216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.226584553675" Y="21.943539471898" />
                  <Point X="-0.906863601906" Y="29.719164573648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.319418033733" Y="21.914628246069" />
                  <Point X="-1.000282347847" Y="29.696942967472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.409955491193" Y="21.859473361855" />
                  <Point X="-1.09336219132" Y="29.670847688363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.495637428801" Y="21.748819631783" />
                  <Point X="-1.18644199952" Y="29.644752006087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.580325057996" Y="21.626800904539" />
                  <Point X="-1.27952175022" Y="29.618655666568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.660013897866" Y="21.447645753854" />
                  <Point X="-1.372601500919" Y="29.592559327049" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.731903810751" Y="21.179348459838" />
                  <Point X="-1.465681251618" Y="29.56646298753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.80379373454" Y="20.911051290442" />
                  <Point X="-1.527964354481" Y="29.188359352499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.875683668811" Y="20.642754240866" />
                  <Point X="-1.615729852401" Y="29.101520825758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.947573603082" Y="20.374457191291" />
                  <Point X="-1.70751486651" Y="29.060625579295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.031357522085" Y="20.242109009288" />
                  <Point X="-1.800346224473" Y="29.031690097816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128763527043" Y="20.265461982218" />
                  <Point X="-1.133915768545" Y="20.324352372065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.170018040546" Y="20.737003229279" />
                  <Point X="-1.896276304641" Y="29.038173173202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.285000941763" Y="20.961261045777" />
                  <Point X="-1.99551764922" Y="29.082504174001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.388288681153" Y="21.051842550898" />
                  <Point X="-2.096635158163" Y="29.148279831601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.491576333858" Y="21.142423065206" />
                  <Point X="-2.202035644358" Y="29.263010143209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.591501363903" Y="21.194568626645" />
                  <Point X="-2.292978114515" Y="29.212484575305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.687967842662" Y="21.207182765976" />
                  <Point X="-2.383920569679" Y="29.161958836036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.783880709124" Y="21.213469087811" />
                  <Point X="-2.474863024844" Y="29.111433096767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.879793573525" Y="21.219755386077" />
                  <Point X="-2.565805480008" Y="29.060907357498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.975400111861" Y="21.222540361408" />
                  <Point X="-2.656602902808" Y="29.008723890716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.068593551774" Y="21.197743495549" />
                  <Point X="-2.745971331256" Y="28.940206943757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.159051791756" Y="21.141683151422" />
                  <Point X="-2.75601106678" Y="27.964958887569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777180903801" Y="28.20693123196" />
                  <Point X="-2.835339738104" Y="28.871689749913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.249147824799" Y="21.081482763034" />
                  <Point X="-2.352044548052" Y="22.257597691605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.378916689509" Y="22.564747673937" />
                  <Point X="-2.839758754785" Y="27.832196583361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890007449009" Y="28.406541786485" />
                  <Point X="-2.92470805379" Y="28.80317151407" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.339243923433" Y="21.021283124354" />
                  <Point X="-2.4348582571" Y="22.114159959071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.480417438931" Y="22.634903790268" />
                  <Point X="-2.928467310113" Y="27.756137252121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.002401941036" Y="28.601213950557" />
                  <Point X="-3.014076369476" Y="28.734653278227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.427195727443" Y="20.936574085977" />
                  <Point X="-2.517671966149" Y="21.970722226538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.577195114002" Y="22.651074919727" />
                  <Point X="-3.021149520115" Y="27.725497001636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.514504976958" Y="20.844520616109" />
                  <Point X="-2.600485675197" Y="21.827284494005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.670582160939" Y="22.628490992283" />
                  <Point X="-3.116713928002" Y="27.727800423739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.615329568302" Y="20.906948210232" />
                  <Point X="-2.683299384245" Y="21.683846761471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761516416493" Y="22.577871531033" />
                  <Point X="-3.213570992019" Y="27.744878973004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.716154198907" Y="20.969376253126" />
                  <Point X="-2.766113093293" Y="21.540409028938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.852293991797" Y="22.525461206342" />
                  <Point X="-2.948913707745" Y="23.629829613108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.955351371908" Y="23.703412451191" />
                  <Point X="-3.31346223243" Y="27.796638317145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.817960256834" Y="21.043022061629" />
                  <Point X="-2.848926802341" Y="21.396971296404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.943071567101" Y="22.47305088165" />
                  <Point X="-3.028159574259" Y="23.445611253796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.063746721365" Y="23.852374206523" />
                  <Point X="-3.413898322514" Y="27.854625321549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.920211110215" Y="21.121751905431" />
                  <Point X="-2.93174045268" Y="21.253532892829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.033849142406" Y="22.420640556959" />
                  <Point X="-3.117180715031" Y="23.373124790536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.162731291331" Y="23.893770260061" />
                  <Point X="-3.514334412597" Y="27.912612325954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.124626712581" Y="22.368230173637" />
                  <Point X="-3.206544412352" Y="23.304553766539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.25845919448" Y="23.897942441547" />
                  <Point X="-3.338814472575" Y="24.816407472986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368982392331" Y="25.161228373657" />
                  <Point X="-3.473194823015" Y="26.35238190698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506387883096" Y="26.73178031979" />
                  <Point X="-3.614770502681" Y="27.970599330359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.215404275486" Y="22.315819707223" />
                  <Point X="-3.295908109672" Y="23.235982742543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.352736206437" Y="23.885530860829" />
                  <Point X="-3.426395474533" Y="24.727460147746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.470791678821" Y="25.234911084808" />
                  <Point X="-3.560411724798" Y="26.259272897705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.609465480188" Y="26.81995988746" />
                  <Point X="-3.715206622382" Y="28.028586673285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.306181838391" Y="22.263409240809" />
                  <Point X="-3.385271806993" Y="23.167411718546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.447013218394" Y="23.87311928011" />
                  <Point X="-3.51806170631" Y="24.685207213021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.568860792557" Y="25.265843425761" />
                  <Point X="-3.652480867973" Y="26.221625261321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.711691032522" Y="26.898400538975" />
                  <Point X="-3.802560204822" Y="27.937039931071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.396959401297" Y="22.210998774395" />
                  <Point X="-3.474635504313" Y="23.09884069455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.541290230351" Y="23.860707699391" />
                  <Point X="-3.611240255894" Y="24.660240149926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.666512868872" Y="25.292009007174" />
                  <Point X="-3.746335754141" Y="26.204388760759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813916582177" Y="26.976841159873" />
                  <Point X="-3.888283139289" Y="27.82685479724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487736964202" Y="22.158588307981" />
                  <Point X="-3.563999201634" Y="23.030269670554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.635567242309" Y="23.848296118672" />
                  <Point X="-3.704418805479" Y="24.635273086832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.764164945187" Y="25.318174588588" />
                  <Point X="-3.842547773514" Y="26.214094416006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.916142083127" Y="27.055281224069" />
                  <Point X="-3.974006073757" Y="27.716669663409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.578514527107" Y="22.106177841567" />
                  <Point X="-3.653362898954" Y="22.961698646557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.729844254266" Y="23.835884537953" />
                  <Point X="-3.797597355063" Y="24.610306023737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861817021502" Y="25.344340170002" />
                  <Point X="-3.939021840226" Y="26.226795286044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.018367584077" Y="27.133721288266" />
                  <Point X="-4.057859320527" Y="27.58511390141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.669292090013" Y="22.053767375153" />
                  <Point X="-3.742726596275" Y="22.893127622561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.824121254918" Y="23.823472828019" />
                  <Point X="-3.890775904647" Y="24.585338960643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.959469097817" Y="25.370505751416" />
                  <Point X="-4.035495906938" Y="26.239496156083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.120593085028" Y="27.212161352463" />
                  <Point X="-4.140791538069" Y="27.443030727157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760069652918" Y="22.001356908739" />
                  <Point X="-3.832090293596" Y="22.824556598565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.918398252539" Y="23.811061083431" />
                  <Point X="-3.983954454232" Y="24.560371897549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057121197683" Y="25.396671602022" />
                  <Point X="-4.13196997365" Y="26.252197026121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.222818585978" Y="27.29060141666" />
                  <Point X="-4.223723688677" Y="27.300946787851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861644488397" Y="22.072359832566" />
                  <Point X="-3.921453990916" Y="22.755985574568" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.012675250159" Y="23.798649338842" />
                  <Point X="-4.077133003816" Y="24.535404834454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.154773300106" Y="25.422837481854" />
                  <Point X="-4.228444040362" Y="26.264897896159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.96939210905" Y="22.213918013798" />
                  <Point X="-4.010817659174" Y="22.687414218382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.10695224778" Y="23.786237594253" />
                  <Point X="-4.1703115534" Y="24.51043777136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252425402529" Y="25.449003361686" />
                  <Point X="-4.324918107074" Y="26.277598766197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079861584529" Y="22.386587138044" />
                  <Point X="-4.100181316315" Y="22.618842735133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.2012292454" Y="23.773825849665" />
                  <Point X="-4.263490102985" Y="24.485470708265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.350077504952" Y="25.475169241519" />
                  <Point X="-4.421392173786" Y="26.290299636235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.295506243021" Y="23.761414105076" />
                  <Point X="-4.356668652569" Y="24.460503645171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.447729607376" Y="25.501335121351" />
                  <Point X="-4.517866246069" Y="26.303000569959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.389783240642" Y="23.749002360487" />
                  <Point X="-4.449847202153" Y="24.435536582076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.545381709799" Y="25.527501001184" />
                  <Point X="-4.614340395833" Y="26.315702389285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.484060238262" Y="23.736590615899" />
                  <Point X="-4.543025744056" Y="24.410569431179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.643033812222" Y="25.553666881016" />
                  <Point X="-4.691293573182" Y="26.105278472914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.578337235883" Y="23.72417887131" />
                  <Point X="-4.63620423255" Y="24.385601669823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.740685914645" Y="25.579832760849" />
                  <Point X="-4.757976269513" Y="25.777462421318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.678910895081" Y="23.783738297887" />
                  <Point X="-4.729382721044" Y="24.360633908466" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.74773034668" Y="20.432126953125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318969727" Y="21.478455078125" />
                  <Point X="0.351432556152" Y="21.6411015625" />
                  <Point X="0.300591339111" Y="21.71435546875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.099651023865" Y="21.787814453125" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="-0.008663995743" Y="21.812234375" />
                  <Point X="-0.183349029541" Y="21.758017578125" />
                  <Point X="-0.262022979736" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.401165435791" Y="21.551708984375" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.58361706543" Y="20.998658203125" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.008643981934" Y="20.044154296875" />
                  <Point X="-1.10023046875" Y="20.061931640625" />
                  <Point X="-1.302663208008" Y="20.114015625" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.342397216797" Y="20.19642578125" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.351415527344" Y="20.67504296875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.547110717773" Y="20.9384140625" />
                  <Point X="-1.619543701172" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.8626953125" Y="21.0282265625" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878295898" Y="21.026208984375" />
                  <Point X="-2.167739990234" Y="20.907365234375" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.329569335938" Y="20.751693359375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.721567138672" Y="20.74925390625" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-3.136122070312" Y="21.048201171875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.019304199219" Y="21.481869140625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.513981201172" Y="22.43123828125" />
                  <Point X="-2.531329833984" Y="22.4485859375" />
                  <Point X="-2.560158447266" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.992072509766" Y="22.225365234375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.055625" Y="22.01350390625" />
                  <Point X="-4.161703613281" Y="22.15287109375" />
                  <Point X="-4.3626484375" Y="22.48982421875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.061843505859" Y="22.887751953125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822509766" Y="23.603982421875" />
                  <Point X="-3.140510498047" Y="23.624494140625" />
                  <Point X="-3.138117919922" Y="23.63373046875" />
                  <Point X="-3.140326171875" Y="23.66540234375" />
                  <Point X="-3.161159423828" Y="23.689361328125" />
                  <Point X="-3.179418701172" Y="23.700107421875" />
                  <Point X="-3.187642089844" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.724982421875" Y="23.644884765625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.885904785156" Y="23.8263828125" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.980558105469" Y="24.36053125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.580485839844" Y="24.597236328125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.5418984375" Y="24.878572265625" />
                  <Point X="-3.522763427734" Y="24.891853515625" />
                  <Point X="-3.514145507812" Y="24.897833984375" />
                  <Point X="-3.494898681641" Y="24.924091796875" />
                  <Point X="-3.488520263672" Y="24.944642578125" />
                  <Point X="-3.485647705078" Y="24.9538984375" />
                  <Point X="-3.485647705078" Y="24.9835390625" />
                  <Point X="-3.492026123047" Y="25.004091796875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.514145019531" Y="25.03960546875" />
                  <Point X="-3.533280029297" Y="25.052884765625" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.018208251953" Y="25.189541015625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.9443828125" Y="25.8157265625" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.810626464844" Y="26.3913515625" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.486593261719" Y="26.4905234375" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731705566406" Y="26.3958671875" />
                  <Point X="-3.689353759766" Y="26.409220703125" />
                  <Point X="-3.670279541016" Y="26.415234375" />
                  <Point X="-3.65153515625" Y="26.4260546875" />
                  <Point X="-3.639120361328" Y="26.44378515625" />
                  <Point X="-3.622126464844" Y="26.4848125" />
                  <Point X="-3.614472900391" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.524603515625" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.636820800781" Y="26.584900390625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.924252441406" Y="26.822013671875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.263908691406" Y="27.609009765625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.876523925781" Y="28.151392578125" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.609546142578" Y="28.1869765625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514892578" Y="27.92043359375" />
                  <Point X="-3.079530517578" Y="27.9152734375" />
                  <Point X="-3.052965576172" Y="27.91294921875" />
                  <Point X="-3.031507080078" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.971385498047" Y="27.969271484375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.943234619141" Y="28.08682421875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.0691796875" Y="28.336876953125" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-2.933825683594" Y="29.035599609375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.306387207031" Y="29.422390625" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.105704589844" Y="29.467259765625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247802734" Y="29.273662109375" />
                  <Point X="-1.885598510742" Y="29.239486328125" />
                  <Point X="-1.856031860352" Y="29.22409375" />
                  <Point X="-1.835125" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.745431152344" Y="29.25057421875" />
                  <Point X="-1.714635253906" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.663827514648" Y="29.36507421875" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.665232055664" Y="29.51955859375" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.202345092773" Y="29.837619140625" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.426813598633" Y="29.966646484375" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.175377838135" Y="29.80815234375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282119751" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594039917" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.114459243774" Y="29.5348515625" />
                  <Point X="0.236648406982" Y="29.9908671875" />
                  <Point X="0.655669128418" Y="29.946984375" />
                  <Point X="0.860209655762" Y="29.925564453125" />
                  <Point X="1.308020874023" Y="29.81744921875" />
                  <Point X="1.508455322266" Y="29.769056640625" />
                  <Point X="1.800453491211" Y="29.6631484375" />
                  <Point X="1.931044555664" Y="29.61578125" />
                  <Point X="2.212884765625" Y="29.483974609375" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.610977783203" Y="29.26650390625" />
                  <Point X="2.732521728516" Y="29.19569140625" />
                  <Point X="2.989319335938" Y="29.0130703125" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.823925537109" Y="28.532560546875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.210049316406" Y="27.436158203125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.207816650391" Y="27.34445703125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.24830078125" Y="27.257162109375" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274939208984" Y="27.224205078125" />
                  <Point X="2.318588867188" Y="27.1945859375" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360336914062" Y="27.172978515625" />
                  <Point X="2.408203613281" Y="27.16720703125" />
                  <Point X="2.42976171875" Y="27.164607421875" />
                  <Point X="2.448666259766" Y="27.1659453125" />
                  <Point X="2.504021728516" Y="27.180748046875" />
                  <Point X="2.528952636719" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.004454101562" Y="27.45997265625" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.130495117188" Y="27.842078125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.345751464844" Y="27.505306640625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.070767578125" Y="27.193248046875" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279370849609" Y="26.58383203125" />
                  <Point X="3.23953125" Y="26.531859375" />
                  <Point X="3.221588623047" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.491498046875" />
                  <Point X="3.198279052734" Y="26.43843359375" />
                  <Point X="3.191595458984" Y="26.414533203125" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.202961914062" Y="26.331923828125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646972656" Y="26.287953125" />
                  <Point X="3.248781494141" Y="26.23758984375" />
                  <Point X="3.263704589844" Y="26.214908203125" />
                  <Point X="3.280947265625" Y="26.1988203125" />
                  <Point X="3.328963623047" Y="26.171791015625" />
                  <Point X="3.350589355469" Y="26.1596171875" />
                  <Point X="3.368567138672" Y="26.153619140625" />
                  <Point X="3.433488769531" Y="26.1450390625" />
                  <Point X="3.462727783203" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.916021972656" Y="26.199126953125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.908223144531" Y="26.078580078125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.984302246094" Y="25.661626953125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.637530273438" Y="25.478005859375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088134766" Y="25.232818359375" />
                  <Point X="3.6653046875" Y="25.195951171875" />
                  <Point X="3.636578125" Y="25.17934765625" />
                  <Point X="3.622264648438" Y="25.16692578125" />
                  <Point X="3.583994628906" Y="25.11816015625" />
                  <Point X="3.566758789062" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.544228271484" Y="25.008123046875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.551239746094" Y="24.892705078125" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.605028808594" Y="24.7924765625" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636575927734" Y="24.758091796875" />
                  <Point X="3.700359375" Y="24.721224609375" />
                  <Point X="3.7290859375" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.144867675781" Y="24.59144140625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.965721191406" Y="24.14827734375" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.890633789062" Y="23.780322265625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.451510253906" Y="23.765515625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.269652099609" Y="23.87444921875" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185445800781" Y="23.845302734375" />
                  <Point X="3.109779785156" Y="23.754298828125" />
                  <Point X="3.075701660156" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.053513183594" Y="23.568076171875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.125639404297" Y="23.375619140625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.543096923828" Y="23.026990234375" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.252868652344" Y="22.27672265625" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.084602050781" Y="22.0280234375" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.679294677734" Y="22.20625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.73733984375" Y="22.7466875" />
                  <Point X="2.588350585938" Y="22.773595703125" />
                  <Point X="2.521249511719" Y="22.785712890625" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.365304443359" Y="22.71561328125" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.223458740234" Y="22.54154296875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.2160703125" Y="22.304634765625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.474833007813" Y="21.8044453125" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.893131103516" Y="20.851095703125" />
                  <Point X="2.835297851563" Y="20.809787109375" />
                  <Point X="2.701655761719" Y="20.723283203125" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.389678955078" Y="21.087181640625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.52360534668" Y="22.11400390625" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.265097045898" Y="22.149494140625" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.041238037109" Y="22.02830859375" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.931353149414" Y="21.783306640625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.982153747559" Y="21.17106640625" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.04905847168" Y="20.048744140625" />
                  <Point X="0.99436730957" Y="20.0367578125" />
                  <Point X="0.87088873291" Y="20.01432421875" />
                  <Point X="0.860200744629" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#186" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.133237562358" Y="4.852373851146" Z="1.75" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="-0.438781643995" Y="5.047586503057" Z="1.75" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.75" />
                  <Point X="-1.221998719308" Y="4.917044297182" Z="1.75" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.75" />
                  <Point X="-1.720960238234" Y="4.544313154938" Z="1.75" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.75" />
                  <Point X="-1.717669363323" Y="4.411390252634" Z="1.75" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.75" />
                  <Point X="-1.770720984793" Y="4.328047938753" Z="1.75" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.75" />
                  <Point X="-1.868665913927" Y="4.31511622377" Z="1.75" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.75" />
                  <Point X="-2.072192926071" Y="4.528977152677" Z="1.75" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.75" />
                  <Point X="-2.336826043959" Y="4.497378593855" Z="1.75" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.75" />
                  <Point X="-2.97040389209" Y="4.10655901722" Z="1.75" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.75" />
                  <Point X="-3.118636982412" Y="3.343157344521" Z="1.75" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.75" />
                  <Point X="-2.99920051867" Y="3.113747815525" Z="1.75" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.75" />
                  <Point X="-3.012896106299" Y="3.035907461587" Z="1.75" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.75" />
                  <Point X="-3.081328650081" Y="2.996364180503" Z="1.75" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.75" />
                  <Point X="-3.590702003777" Y="3.261556966333" Z="1.75" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.75" />
                  <Point X="-3.92214327684" Y="3.213376138189" Z="1.75" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.75" />
                  <Point X="-4.313246869247" Y="2.665495621751" Z="1.75" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.75" />
                  <Point X="-3.960846408983" Y="1.813626435788" Z="1.75" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.75" />
                  <Point X="-3.687327362472" Y="1.593093988282" Z="1.75" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.75" />
                  <Point X="-3.674476056207" Y="1.535226899985" Z="1.75" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.75" />
                  <Point X="-3.710544181661" Y="1.488186020896" Z="1.75" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.75" />
                  <Point X="-4.486223036353" Y="1.571376890567" Z="1.75" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.75" />
                  <Point X="-4.865041566031" Y="1.435709747809" Z="1.75" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.75" />
                  <Point X="-5.000000433484" Y="0.85432456427" Z="1.75" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.75" />
                  <Point X="-4.03730521149" Y="0.172525144176" Z="1.75" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.75" />
                  <Point X="-3.567942812324" Y="0.043087701108" Z="1.75" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.75" />
                  <Point X="-3.545935168938" Y="0.020551188964" Z="1.75" />
                  <Point X="-3.539556741714" Y="0" Z="1.75" />
                  <Point X="-3.542429432197" Y="-0.009255762097" Z="1.75" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.75" />
                  <Point X="-3.557425782799" Y="-0.035788282026" Z="1.75" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.75" />
                  <Point X="-4.599582577282" Y="-0.32318691258" Z="1.75" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.75" />
                  <Point X="-5.036210134611" Y="-0.615265974791" Z="1.75" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.75" />
                  <Point X="-4.940505535415" Y="-1.154710719993" Z="1.75" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.75" />
                  <Point X="-3.724611969136" Y="-1.373407455751" Z="1.75" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.75" />
                  <Point X="-3.210935420825" Y="-1.311703282798" Z="1.75" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.75" />
                  <Point X="-3.195068733814" Y="-1.331687063162" Z="1.75" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.75" />
                  <Point X="-4.098437448361" Y="-2.041300242537" Z="1.75" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.75" />
                  <Point X="-4.41174772234" Y="-2.504505184164" Z="1.75" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.75" />
                  <Point X="-4.101567165492" Y="-2.985497953095" Z="1.75" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.75" />
                  <Point X="-2.973228452614" Y="-2.786655637828" Z="1.75" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.75" />
                  <Point X="-2.567452370976" Y="-2.560878098155" Z="1.75" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.75" />
                  <Point X="-3.06876137534" Y="-3.461849929878" Z="1.75" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.75" />
                  <Point X="-3.172781984856" Y="-3.960135573622" Z="1.75" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.75" />
                  <Point X="-2.754044553842" Y="-4.261976816494" Z="1.75" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.75" />
                  <Point X="-2.296057632552" Y="-4.247463354827" Z="1.75" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.75" />
                  <Point X="-2.14611765573" Y="-4.10292790368" Z="1.75" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.75" />
                  <Point X="-1.872121256458" Y="-3.990385378377" Z="1.75" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.75" />
                  <Point X="-1.586233841322" Y="-4.067898064132" Z="1.75" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.75" />
                  <Point X="-1.406610778674" Y="-4.303430271488" Z="1.75" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.75" />
                  <Point X="-1.398125441968" Y="-4.765767450447" Z="1.75" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.75" />
                  <Point X="-1.321278084214" Y="-4.903127986571" Z="1.75" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.75" />
                  <Point X="-1.024327429229" Y="-4.973649643129" Z="1.75" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="-0.541476732207" Y="-3.98300273342" Z="1.75" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="-0.366245463111" Y="-3.445520434067" Z="1.75" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="-0.174685102981" Y="-3.258455177922" Z="1.75" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.75" />
                  <Point X="0.07867397638" Y="-3.228656867366" Z="1.75" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.75" />
                  <Point X="0.304200396321" Y="-3.356125321701" Z="1.75" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.75" />
                  <Point X="0.693278128194" Y="-4.549533296396" Z="1.75" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.75" />
                  <Point X="0.873668553797" Y="-5.003589708679" Z="1.75" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.75" />
                  <Point X="1.053607180462" Y="-4.968814541883" Z="1.75" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.75" />
                  <Point X="1.025570054173" Y="-3.791128415499" Z="1.75" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.75" />
                  <Point X="0.9740563735" Y="-3.196031873522" Z="1.75" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.75" />
                  <Point X="1.067049215526" Y="-2.978855976917" Z="1.75" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.75" />
                  <Point X="1.263522676685" Y="-2.86901506527" Z="1.75" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.75" />
                  <Point X="1.490410316909" Y="-2.896774124765" Z="1.75" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.75" />
                  <Point X="2.343855369905" Y="-3.911975727225" Z="1.75" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.75" />
                  <Point X="2.722669235403" Y="-4.287410801727" Z="1.75" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.75" />
                  <Point X="2.916036764197" Y="-4.158310829063" Z="1.75" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.75" />
                  <Point X="2.511978309192" Y="-3.139274805866" Z="1.75" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.75" />
                  <Point X="2.259118469261" Y="-2.655197438016" Z="1.75" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.75" />
                  <Point X="2.261549099374" Y="-2.450463608503" Z="1.75" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.75" />
                  <Point X="2.382434769869" Y="-2.297352210593" Z="1.75" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.75" />
                  <Point X="2.573309365497" Y="-2.24432954023" Z="1.75" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.75" />
                  <Point X="3.648138791241" Y="-2.805771191083" Z="1.75" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.75" />
                  <Point X="4.119334657233" Y="-2.969473903586" Z="1.75" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.75" />
                  <Point X="4.289246511857" Y="-2.718281999551" Z="1.75" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.75" />
                  <Point X="3.567378917672" Y="-1.902061452437" Z="1.75" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.75" />
                  <Point X="3.161541313283" Y="-1.566061128812" Z="1.75" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.75" />
                  <Point X="3.097146717003" Y="-1.405224614199" Z="1.75" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.75" />
                  <Point X="3.142069582573" Y="-1.246386825945" Z="1.75" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.75" />
                  <Point X="3.274115540781" Y="-1.143129754119" Z="1.75" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.75" />
                  <Point X="4.438828154631" Y="-1.252776910431" Z="1.75" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.75" />
                  <Point X="4.933224760928" Y="-1.199522842931" Z="1.75" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.75" />
                  <Point X="5.009006798996" Y="-0.827895229634" Z="1.75" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.75" />
                  <Point X="4.151652777129" Y="-0.328981741653" Z="1.75" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.75" />
                  <Point X="3.71922616501" Y="-0.204206184939" Z="1.75" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.75" />
                  <Point X="3.6382067939" Y="-0.145375593918" Z="1.75" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.75" />
                  <Point X="3.594191416779" Y="-0.066610680143" Z="1.75" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.75" />
                  <Point X="3.587180033645" Y="0.029999851053" Z="1.75" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.75" />
                  <Point X="3.6171726445" Y="0.118573144657" Z="1.75" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.75" />
                  <Point X="3.684169249342" Y="0.183942659303" Z="1.75" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.75" />
                  <Point X="4.644315040178" Y="0.460990191116" Z="1.75" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.75" />
                  <Point X="5.027550918197" Y="0.700599508361" Z="1.75" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.75" />
                  <Point X="4.950648328322" Y="1.121687517752" Z="1.75" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.75" />
                  <Point X="3.903339575496" Y="1.279979921397" Z="1.75" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.75" />
                  <Point X="3.433882263975" Y="1.225888393029" Z="1.75" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.75" />
                  <Point X="3.347335112815" Y="1.246641866184" Z="1.75" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.75" />
                  <Point X="3.284395546111" Y="1.296353344029" Z="1.75" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.75" />
                  <Point X="3.245774365821" Y="1.373307410625" Z="1.75" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.75" />
                  <Point X="3.24027572885" Y="1.456248498774" Z="1.75" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.75" />
                  <Point X="3.273058714971" Y="1.532721406608" Z="1.75" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.75" />
                  <Point X="4.095049632964" Y="2.184861014768" Z="1.75" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.75" />
                  <Point X="4.382372726462" Y="2.562473965047" Z="1.75" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.75" />
                  <Point X="4.164924371116" Y="2.90256162498" Z="1.75" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.75" />
                  <Point X="2.973298154389" Y="2.534554549576" Z="1.75" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.75" />
                  <Point X="2.484947139353" Y="2.260331952278" Z="1.75" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.75" />
                  <Point X="2.408033595093" Y="2.248128677619" Z="1.75" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.75" />
                  <Point X="2.340507937013" Y="2.267239859768" Z="1.75" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.75" />
                  <Point X="2.283518740467" Y="2.316516923369" Z="1.75" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.75" />
                  <Point X="2.251301025139" Y="2.381724848739" Z="1.75" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.75" />
                  <Point X="2.25219599038" Y="2.454522436529" Z="1.75" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.75" />
                  <Point X="2.861070803283" Y="3.538840927566" Z="1.75" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.75" />
                  <Point X="3.012140251936" Y="4.085099806441" Z="1.75" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.75" />
                  <Point X="2.629991693364" Y="4.340986967909" Z="1.75" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.75" />
                  <Point X="2.227910373406" Y="4.56054566918" Z="1.75" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.75" />
                  <Point X="1.811346468538" Y="4.741432192479" Z="1.75" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.75" />
                  <Point X="1.313599580168" Y="4.897332788331" Z="1.75" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.75" />
                  <Point X="0.654720830782" Y="5.027994142017" Z="1.75" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.75" />
                  <Point X="0.060006999195" Y="4.579073654805" Z="1.75" />
                  <Point X="0" Y="4.355124473572" Z="1.75" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>