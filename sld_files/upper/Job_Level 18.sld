<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#151" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1409" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.697555053711" Y="20.986435546875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.487490509033" Y="21.611685546875" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.356752044678" Y="21.790978515625" />
                  <Point X="0.330496856689" Y="21.81022265625" />
                  <Point X="0.302495025635" Y="21.82433203125" />
                  <Point X="0.217582870483" Y="21.850685546875" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.00866468811" Y="21.907234375" />
                  <Point X="-0.036824085236" Y="21.90296484375" />
                  <Point X="-0.121736251831" Y="21.876611328125" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318184906006" Y="21.81022265625" />
                  <Point X="-0.344440093994" Y="21.790978515625" />
                  <Point X="-0.366323883057" Y="21.7685234375" />
                  <Point X="-0.421196563721" Y="21.6894609375" />
                  <Point X="-0.530051513672" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.817318237305" Y="20.493525390625" />
                  <Point X="-0.916584960938" Y="20.12305859375" />
                  <Point X="-1.079317504883" Y="20.15464453125" />
                  <Point X="-1.174216796875" Y="20.1790625" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.231123901367" Y="20.313806640625" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.236793945312" Y="20.585755859375" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323644897461" Y="20.868796875" />
                  <Point X="-1.401821411133" Y="20.93735546875" />
                  <Point X="-1.556905761719" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.64302734375" Y="21.109033203125" />
                  <Point X="-1.746784790039" Y="21.115833984375" />
                  <Point X="-1.952616577148" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657226562" Y="21.10519921875" />
                  <Point X="-2.129113769531" Y="21.047431640625" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.4801484375" Y="20.711509765625" />
                  <Point X="-2.515877929688" Y="20.7336328125" />
                  <Point X="-2.801711181641" Y="20.910611328125" />
                  <Point X="-2.933083496094" Y="21.011765625" />
                  <Point X="-3.104721923828" Y="21.143919921875" />
                  <Point X="-2.673255371094" Y="21.891244140625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858154297" Y="22.3523515625" />
                  <Point X="-2.406587646484" Y="22.3838828125" />
                  <Point X="-2.405576904297" Y="22.4148203125" />
                  <Point X="-2.414565185547" Y="22.444439453125" />
                  <Point X="-2.428786865234" Y="22.47326953125" />
                  <Point X="-2.446814453125" Y="22.498421875" />
                  <Point X="-2.464156738281" Y="22.51576171875" />
                  <Point X="-2.489305664062" Y="22.533783203125" />
                  <Point X="-2.518135009766" Y="22.548001953125" />
                  <Point X="-2.547754882812" Y="22.556986328125" />
                  <Point X="-2.578691162109" Y="22.555974609375" />
                  <Point X="-2.610218261719" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.496446289062" Y="22.043859375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.857038085938" Y="21.909455078125" />
                  <Point X="-4.082863037109" Y="22.206142578125" />
                  <Point X="-4.177043945312" Y="22.364068359375" />
                  <Point X="-4.306143066406" Y="22.580548828125" />
                  <Point X="-3.542488525391" Y="23.166521484375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083064941406" Y="23.5241171875" />
                  <Point X="-3.063474853516" Y="23.555853515625" />
                  <Point X="-3.054179199219" Y="23.580892578125" />
                  <Point X="-3.051274658203" Y="23.590134765625" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.042037353516" Y="23.64139453125" />
                  <Point X="-3.042736328125" Y="23.66426171875" />
                  <Point X="-3.048882080078" Y="23.686298828125" />
                  <Point X="-3.060885253906" Y="23.715279296875" />
                  <Point X="-3.073938476562" Y="23.737599609375" />
                  <Point X="-3.092526855469" Y="23.75557421875" />
                  <Point X="-3.113909179688" Y="23.771232421875" />
                  <Point X="-3.121855712891" Y="23.7764609375" />
                  <Point X="-3.139462890625" Y="23.786822265625" />
                  <Point X="-3.168722900391" Y="23.79804296875" />
                  <Point X="-3.200607910156" Y="23.8045234375" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.314142089844" Y="23.663140625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.745756347656" Y="23.661572265625" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.85899609375" Y="24.181572265625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.030153808594" Y="24.64634375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.511687988281" Y="24.788109375" />
                  <Point X="-3.486818359375" Y="24.80178515625" />
                  <Point X="-3.478426757812" Y="24.806984375" />
                  <Point X="-3.459975097656" Y="24.819791015625" />
                  <Point X="-3.436020019531" Y="24.841318359375" />
                  <Point X="-3.414914550781" Y="24.87214453125" />
                  <Point X="-3.404388916016" Y="24.89687109375" />
                  <Point X="-3.401068603516" Y="24.905919921875" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.389474365234" Y="24.954478515625" />
                  <Point X="-3.390533203125" Y="24.988734375" />
                  <Point X="-3.395880371094" Y="25.013544921875" />
                  <Point X="-3.398017089844" Y="25.0216875" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.417483886719" Y="25.070830078125" />
                  <Point X="-3.440755859375" Y="25.100435546875" />
                  <Point X="-3.461875488281" Y="25.118408203125" />
                  <Point X="-3.469276367188" Y="25.124103515625" />
                  <Point X="-3.487728027344" Y="25.13691015625" />
                  <Point X="-3.501923828125" Y="25.145046875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-4.519364257812" Y="25.422177734375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.881409179688" Y="25.592306640625" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.774327148438" Y="26.16208203125" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.122127929688" Y="26.346720703125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703140136719" Y="26.305263671875" />
                  <Point X="-3.682553466797" Y="26.31175390625" />
                  <Point X="-3.641714111328" Y="26.324630859375" />
                  <Point X="-3.622783691406" Y="26.332958984375" />
                  <Point X="-3.604039550781" Y="26.343779296875" />
                  <Point X="-3.587354492188" Y="26.35601171875" />
                  <Point X="-3.573713378906" Y="26.37156640625" />
                  <Point X="-3.561299072266" Y="26.389296875" />
                  <Point X="-3.551352050781" Y="26.407427734375" />
                  <Point X="-3.543091552734" Y="26.427369140625" />
                  <Point X="-3.526704589844" Y="26.466931640625" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.542017089844" Y="26.6085234375" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.167987792969" Y="27.12878515625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.302327148438" Y="27.354736328125" />
                  <Point X="-4.081156982422" Y="27.73365234375" />
                  <Point X="-3.948281494141" Y="27.904447265625" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.425585449219" Y="27.9710703125" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729736328" Y="27.836341796875" />
                  <Point X="-3.167087890625" Y="27.82983203125" />
                  <Point X="-3.14679296875" Y="27.825794921875" />
                  <Point X="-3.118121337891" Y="27.823287109375" />
                  <Point X="-3.061243652344" Y="27.818310546875" />
                  <Point X="-3.040561035156" Y="27.81876171875" />
                  <Point X="-3.019102539062" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.925726074219" Y="27.88058203125" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.845944335938" Y="28.064791015625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.120541259766" Y="28.615837890625" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-3.085836669922" Y="28.799345703125" />
                  <Point X="-2.700621582031" Y="29.094685546875" />
                  <Point X="-2.491358886719" Y="29.210947265625" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.110216064453" Y="29.317083984375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028891723633" Y="29.21480078125" />
                  <Point X="-2.012312011719" Y="29.200888671875" />
                  <Point X="-1.995115844727" Y="29.1893984375" />
                  <Point X="-1.963204467773" Y="29.17278515625" />
                  <Point X="-1.899899780273" Y="29.139830078125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718261719" Y="29.126728515625" />
                  <Point X="-1.839268554688" Y="29.123580078125" />
                  <Point X="-1.818622070312" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.777453491211" Y="29.134482421875" />
                  <Point X="-1.744215698242" Y="29.14825" />
                  <Point X="-1.678279296875" Y="29.1755625" />
                  <Point X="-1.660145141602" Y="29.185509765625" />
                  <Point X="-1.642415771484" Y="29.197923828125" />
                  <Point X="-1.626863647461" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.584662109375" Y="29.300232421875" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584202148438" Y="29.6318984375" />
                  <Point X="-1.448314697266" Y="29.66999609375" />
                  <Point X="-0.949624084473" Y="29.80980859375" />
                  <Point X="-0.695946166992" Y="29.8395" />
                  <Point X="-0.294711303711" Y="29.886458984375" />
                  <Point X="-0.198668334961" Y="29.528021484375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113990784" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907154" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.274694763184" Y="29.765806640625" />
                  <Point X="0.307419616699" Y="29.8879375" />
                  <Point X="0.408614898682" Y="29.87733984375" />
                  <Point X="0.844031921387" Y="29.831740234375" />
                  <Point X="1.053930053711" Y="29.781064453125" />
                  <Point X="1.481038696289" Y="29.6779453125" />
                  <Point X="1.616658691406" Y="29.628755859375" />
                  <Point X="1.894644897461" Y="29.527927734375" />
                  <Point X="2.026751098633" Y="29.466146484375" />
                  <Point X="2.294559082031" Y="29.340900390625" />
                  <Point X="2.422225341797" Y="29.2665234375" />
                  <Point X="2.680972167969" Y="29.11577734375" />
                  <Point X="2.801342285156" Y="29.03017578125" />
                  <Point X="2.943259033203" Y="28.929251953125" />
                  <Point X="2.436351806641" Y="28.051263671875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.125881103516" Y="27.489146484375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107727783203" Y="27.380951171875" />
                  <Point X="2.110533447266" Y="27.35768359375" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070068359" Y="27.24747265625" />
                  <Point X="2.154467285156" Y="27.22625390625" />
                  <Point X="2.183027832031" Y="27.1841640625" />
                  <Point X="2.194462646484" Y="27.17033203125" />
                  <Point X="2.221595214844" Y="27.14559375" />
                  <Point X="2.242812744141" Y="27.1311953125" />
                  <Point X="2.284903808594" Y="27.102634765625" />
                  <Point X="2.304953125" Y="27.09226953125" />
                  <Point X="2.327041015625" Y="27.08400390625" />
                  <Point X="2.348967285156" Y="27.078662109375" />
                  <Point X="2.372234619141" Y="27.075857421875" />
                  <Point X="2.418392089844" Y="27.070291015625" />
                  <Point X="2.436467773438" Y="27.06984375" />
                  <Point X="2.473206787109" Y="27.074169921875" />
                  <Point X="2.500114501953" Y="27.081365234375" />
                  <Point X="2.553493164062" Y="27.095640625" />
                  <Point X="2.565289794922" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.580751220703" Y="27.68300390625" />
                  <Point X="3.967325927734" Y="27.90619140625" />
                  <Point X="3.969788085938" Y="27.90276953125" />
                  <Point X="4.123270996094" Y="27.689462890625" />
                  <Point X="4.190373535156" Y="27.57857421875" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.610977539062" Y="26.960185546875" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221421386719" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.184608398438" Y="26.61636328125" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606201172" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.114416259766" Y="26.491291015625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.103661132812" Y="26.343068359375" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120678955078" Y="26.26898046875" />
                  <Point X="3.136282714844" Y="26.23573828125" />
                  <Point X="3.152388916016" Y="26.2112578125" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198896728516" Y="26.1454453125" />
                  <Point X="3.216140136719" Y="26.129357421875" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.257686279297" Y="26.102896484375" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320521240234" Y="26.069501953125" />
                  <Point X="3.35612109375" Y="26.0594375" />
                  <Point X="3.387678466797" Y="26.055267578125" />
                  <Point X="3.450281738281" Y="26.046994140625" />
                  <Point X="3.462698242188" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.430743652344" Y="26.171072265625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.780015625" Y="26.20358984375" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.867081542969" Y="25.796994140625" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.152290039062" Y="25.44633984375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704787841797" Y="25.325583984375" />
                  <Point X="3.681548339844" Y="25.315068359375" />
                  <Point X="3.650543945312" Y="25.2971484375" />
                  <Point X="3.589038330078" Y="25.26159765625" />
                  <Point X="3.574312988281" Y="25.25109765625" />
                  <Point X="3.547529541016" Y="25.225576171875" />
                  <Point X="3.528927001953" Y="25.20187109375" />
                  <Point X="3.492023681641" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.457479736328" Y="25.060224609375" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.451379638672" Y="24.909068359375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480300292969" Y="24.80187109375" />
                  <Point X="3.492023681641" Y="24.782591796875" />
                  <Point X="3.510626220703" Y="24.75888671875" />
                  <Point X="3.547529541016" Y="24.71186328125" />
                  <Point X="3.559998291016" Y="24.698763671875" />
                  <Point X="3.589037353516" Y="24.675841796875" />
                  <Point X="3.620041748047" Y="24.657921875" />
                  <Point X="3.681547363281" Y="24.622369140625" />
                  <Point X="3.692709716797" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.580932128906" Y="24.37624609375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.827931152344" Y="23.9325546875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.929683349609" Y="23.93003515625" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374658447266" Y="23.994490234375" />
                  <Point X="3.313807861328" Y="23.981263671875" />
                  <Point X="3.193094238281" Y="23.95502734375" />
                  <Point X="3.163974609375" Y="23.94340234375" />
                  <Point X="3.136147705078" Y="23.926509765625" />
                  <Point X="3.112396972656" Y="23.9060390625" />
                  <Point X="3.075616699219" Y="23.861802734375" />
                  <Point X="3.002652832031" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.964486083984" Y="23.63734765625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.010126464844" Y="23.379623046875" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.912752441406" Y="22.62359765625" />
                  <Point X="4.213121582031" Y="22.393115234375" />
                  <Point X="4.124808105469" Y="22.2502109375" />
                  <Point X="4.068782714844" Y="22.170607421875" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.250993652344" Y="22.563224609375" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786128417969" Y="22.82998828125" />
                  <Point X="2.754225097656" Y="22.840173828125" />
                  <Point X="2.681803222656" Y="22.85325390625" />
                  <Point X="2.538134765625" Y="22.87919921875" />
                  <Point X="2.506783203125" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.384668945312" Y="22.833158203125" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242386230469" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.172867675781" Y="22.649396484375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.108754394531" Y="22.3643203125" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.667264648438" Y="21.28114453125" />
                  <Point X="2.861284423828" Y="20.94509375" />
                  <Point X="2.781868896484" Y="20.8883671875" />
                  <Point X="2.719209960938" Y="20.847810546875" />
                  <Point X="2.701763671875" Y="20.836517578125" />
                  <Point X="2.101967529297" Y="21.6181875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721923095703" Y="22.099443359375" />
                  <Point X="1.650495727539" Y="22.14536328125" />
                  <Point X="1.508799682617" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417100952148" Y="22.258880859375" />
                  <Point X="1.338982910156" Y="22.251693359375" />
                  <Point X="1.184014160156" Y="22.23743359375" />
                  <Point X="1.156367553711" Y="22.230603515625" />
                  <Point X="1.128982055664" Y="22.21926171875" />
                  <Point X="1.104594604492" Y="22.2045390625" />
                  <Point X="1.044273925781" Y="22.1543828125" />
                  <Point X="0.924610900879" Y="22.05488671875" />
                  <Point X="0.904140014648" Y="22.031134765625" />
                  <Point X="0.887247680664" Y="22.003306640625" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.857588745117" Y="21.891212890625" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.965815612793" Y="20.56734375" />
                  <Point X="1.022065612793" Y="20.140083984375" />
                  <Point X="0.975746765137" Y="20.1299296875" />
                  <Point X="0.929315612793" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058404907227" Y="20.247359375" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.136936645508" Y="20.30140625" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759155273" Y="20.490669921875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.143619384766" Y="20.6042890625" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573364258" Y="20.905138671875" />
                  <Point X="-1.250208496094" Y="20.929064453125" />
                  <Point X="-1.261007202148" Y="20.94022265625" />
                  <Point X="-1.33918371582" Y="21.00878125" />
                  <Point X="-1.494268066406" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.53302355957" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315795898" Y="21.194525390625" />
                  <Point X="-1.621456665039" Y="21.201552734375" />
                  <Point X="-1.636813842773" Y="21.203830078125" />
                  <Point X="-1.740571289062" Y="21.210630859375" />
                  <Point X="-1.946403076172" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.05366796875" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095435791016" Y="21.184189453125" />
                  <Point X="-2.181892333984" Y="21.126421875" />
                  <Point X="-2.35340234375" Y="21.011822265625" />
                  <Point X="-2.359682373047" Y="21.007244140625" />
                  <Point X="-2.380451416016" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503202392578" Y="20.83751953125" />
                  <Point X="-2.747585693359" Y="20.988833984375" />
                  <Point X="-2.875125488281" Y="21.087037109375" />
                  <Point X="-2.980862792969" Y="21.168451171875" />
                  <Point X="-2.590982910156" Y="21.843744140625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849365234" Y="22.289919921875" />
                  <Point X="-2.323946289062" Y="22.318890625" />
                  <Point X="-2.319682617188" Y="22.333822265625" />
                  <Point X="-2.313412109375" Y="22.365353515625" />
                  <Point X="-2.311638183594" Y="22.38078125" />
                  <Point X="-2.310627441406" Y="22.41171875" />
                  <Point X="-2.314670410156" Y="22.44240625" />
                  <Point X="-2.323658691406" Y="22.472025390625" />
                  <Point X="-2.3293671875" Y="22.486466796875" />
                  <Point X="-2.343588867188" Y="22.515296875" />
                  <Point X="-2.351571777344" Y="22.52861328125" />
                  <Point X="-2.369599365234" Y="22.553765625" />
                  <Point X="-2.379644042969" Y="22.5656015625" />
                  <Point X="-2.396986328125" Y="22.58294140625" />
                  <Point X="-2.408821289063" Y="22.592982421875" />
                  <Point X="-2.433970214844" Y="22.61100390625" />
                  <Point X="-2.447284179688" Y="22.618984375" />
                  <Point X="-2.476113525391" Y="22.633203125" />
                  <Point X="-2.490560058594" Y="22.638912109375" />
                  <Point X="-2.520179931641" Y="22.647896484375" />
                  <Point X="-2.550860107422" Y="22.651935546875" />
                  <Point X="-2.581796386719" Y="22.650923828125" />
                  <Point X="-2.597225830078" Y="22.6491484375" />
                  <Point X="-2.628752929688" Y="22.642876953125" />
                  <Point X="-2.64368359375" Y="22.63861328125" />
                  <Point X="-2.672648925781" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.543946289062" Y="22.126130859375" />
                  <Point X="-3.793087890625" Y="21.9822890625" />
                  <Point X="-4.004017822266" Y="22.259408203125" />
                  <Point X="-4.095451416016" Y="22.4127265625" />
                  <Point X="-4.181266113281" Y="22.556625" />
                  <Point X="-3.48465625" Y="23.09115234375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039159423828" Y="23.433931640625" />
                  <Point X="-3.016269775391" Y="23.456564453125" />
                  <Point X="-3.002225830078" Y="23.474216796875" />
                  <Point X="-2.982635742188" Y="23.505953125" />
                  <Point X="-2.9744140625" Y="23.522791015625" />
                  <Point X="-2.965118408203" Y="23.547830078125" />
                  <Point X="-2.959309326172" Y="23.566314453125" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951953369141" Y="23.597599609375" />
                  <Point X="-2.947838623047" Y="23.62908203125" />
                  <Point X="-2.947081787109" Y="23.644296875" />
                  <Point X="-2.947780761719" Y="23.6671640625" />
                  <Point X="-2.951228271484" Y="23.68978125" />
                  <Point X="-2.957374023438" Y="23.711818359375" />
                  <Point X="-2.961112548828" Y="23.722650390625" />
                  <Point X="-2.973115722656" Y="23.751630859375" />
                  <Point X="-2.978879150391" Y="23.76323828125" />
                  <Point X="-2.991932373047" Y="23.78555859375" />
                  <Point X="-3.007900390625" Y="23.805892578125" />
                  <Point X="-3.026488769531" Y="23.8238671875" />
                  <Point X="-3.036398925781" Y="23.832220703125" />
                  <Point X="-3.05778125" Y="23.84787890625" />
                  <Point X="-3.073674316406" Y="23.8583359375" />
                  <Point X="-3.091281494141" Y="23.868697265625" />
                  <Point X="-3.105447509766" Y="23.8755234375" />
                  <Point X="-3.134707519531" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891138671875" />
                  <Point X="-3.181686523438" Y="23.897619140625" />
                  <Point X="-3.197298339844" Y="23.89946484375" />
                  <Point X="-3.228619628906" Y="23.900556640625" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.326541992188" Y="23.757328125" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.74076171875" Y="24.02587890625" />
                  <Point X="-4.764953125" Y="24.1950234375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.005565917969" Y="24.554580078125" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497434814453" Y="24.69144921875" />
                  <Point X="-3.476246826172" Y="24.69996875" />
                  <Point X="-3.465912109375" Y="24.704865234375" />
                  <Point X="-3.441042480469" Y="24.718541015625" />
                  <Point X="-3.424259277344" Y="24.728939453125" />
                  <Point X="-3.405807617188" Y="24.74174609375" />
                  <Point X="-3.396476074219" Y="24.749130859375" />
                  <Point X="-3.372520996094" Y="24.770658203125" />
                  <Point X="-3.357632324219" Y="24.7876484375" />
                  <Point X="-3.336526855469" Y="24.818474609375" />
                  <Point X="-3.327504638672" Y="24.834935546875" />
                  <Point X="-3.316979003906" Y="24.859662109375" />
                  <Point X="-3.310338378906" Y="24.877759765625" />
                  <Point X="-3.304187744141" Y="24.897576171875" />
                  <Point X="-3.301577392578" Y="24.90805859375" />
                  <Point X="-3.296133789062" Y="24.93680078125" />
                  <Point X="-3.294519775391" Y="24.9574140625" />
                  <Point X="-3.295578613281" Y="24.991669921875" />
                  <Point X="-3.297665527344" Y="25.00875" />
                  <Point X="-3.303012695312" Y="25.033560546875" />
                  <Point X="-3.307286132812" Y="25.049845703125" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.317668457031" Y="25.08078515625" />
                  <Point X="-3.330984619141" Y="25.110109375" />
                  <Point X="-3.342796630859" Y="25.1295390625" />
                  <Point X="-3.366068603516" Y="25.15914453125" />
                  <Point X="-3.3791875" Y="25.17278515625" />
                  <Point X="-3.400307128906" Y="25.1907578125" />
                  <Point X="-3.415108886719" Y="25.2021484375" />
                  <Point X="-3.433560546875" Y="25.214955078125" />
                  <Point X="-3.440486083984" Y="25.21933203125" />
                  <Point X="-3.465612792969" Y="25.232833984375" />
                  <Point X="-3.496565185547" Y="25.24563671875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-4.494776367188" Y="25.51394140625" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.682634277344" Y="26.137234375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.134528320313" Y="26.252533203125" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056884766" Y="26.204365234375" />
                  <Point X="-3.736703613281" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.70488671875" Y="26.2080546875" />
                  <Point X="-3.684603759766" Y="26.21208984375" />
                  <Point X="-3.674575927734" Y="26.21466015625" />
                  <Point X="-3.653989257812" Y="26.221150390625" />
                  <Point X="-3.613149902344" Y="26.23402734375" />
                  <Point X="-3.603458740234" Y="26.237673828125" />
                  <Point X="-3.584528320312" Y="26.246001953125" />
                  <Point X="-3.5752890625" Y="26.25068359375" />
                  <Point X="-3.556544921875" Y="26.26150390625" />
                  <Point X="-3.547869873047" Y="26.2671640625" />
                  <Point X="-3.531184814453" Y="26.279396484375" />
                  <Point X="-3.5159296875" Y="26.293373046875" />
                  <Point X="-3.502288574219" Y="26.308927734375" />
                  <Point X="-3.495892578125" Y="26.317078125" />
                  <Point X="-3.483478271484" Y="26.33480859375" />
                  <Point X="-3.478010253906" Y="26.343603515625" />
                  <Point X="-3.468063232422" Y="26.361734375" />
                  <Point X="-3.463584228516" Y="26.3710703125" />
                  <Point X="-3.455323730469" Y="26.39101171875" />
                  <Point X="-3.438936767578" Y="26.43057421875" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012695312" Y="26.60453125" />
                  <Point X="-3.443509277344" Y="26.62380859375" />
                  <Point X="-3.447783935547" Y="26.6332421875" />
                  <Point X="-3.457750976562" Y="26.652388671875" />
                  <Point X="-3.477523681641" Y="26.69037109375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494291748047" Y="26.716484375" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-4.110155273438" Y="27.204154296875" />
                  <Point X="-4.227614257812" Y="27.294283203125" />
                  <Point X="-4.220280273438" Y="27.30684765625" />
                  <Point X="-4.002296630859" Y="27.6803046875" />
                  <Point X="-3.873300537109" Y="27.84611328125" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.473085449219" Y="27.888798828125" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244925292969" Y="27.757720703125" />
                  <Point X="-3.225998046875" Y="27.749390625" />
                  <Point X="-3.216302490234" Y="27.745740234375" />
                  <Point X="-3.195660644531" Y="27.73923046875" />
                  <Point X="-3.185622314453" Y="27.736658203125" />
                  <Point X="-3.165327392578" Y="27.73262109375" />
                  <Point X="-3.155070800781" Y="27.73115625" />
                  <Point X="-3.126399169922" Y="27.7286484375" />
                  <Point X="-3.069521484375" Y="27.723671875" />
                  <Point X="-3.059171875" Y="27.723333984375" />
                  <Point X="-3.038489257812" Y="27.72378515625" />
                  <Point X="-3.02815625" Y="27.72457421875" />
                  <Point X="-3.006697753906" Y="27.727400390625" />
                  <Point X="-2.996520751953" Y="27.729310546875" />
                  <Point X="-2.976432128906" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886612304688" Y="27.786142578125" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.858550537109" Y="27.813408203125" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751305908203" Y="28.0730703125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.038268798828" Y="28.663337890625" />
                  <Point X="-3.059387695312" Y="28.699916015625" />
                  <Point X="-3.028034179688" Y="28.723955078125" />
                  <Point X="-2.648377441406" Y="29.015033203125" />
                  <Point X="-2.445221435547" Y="29.12790234375" />
                  <Point X="-2.192524658203" Y="29.268296875" />
                  <Point X="-2.185584960938" Y="29.259251953125" />
                  <Point X="-2.118564453125" Y="29.17191015625" />
                  <Point X="-2.111819091797" Y="29.164046875" />
                  <Point X="-2.097515380859" Y="29.14910546875" />
                  <Point X="-2.089956787109" Y="29.14202734375" />
                  <Point X="-2.073376953125" Y="29.128115234375" />
                  <Point X="-2.065091552734" Y="29.1218984375" />
                  <Point X="-2.047895507812" Y="29.110408203125" />
                  <Point X="-2.038984619141" Y="29.1051328125" />
                  <Point X="-2.007073242188" Y="29.08851953125" />
                  <Point X="-1.943768676758" Y="29.055564453125" />
                  <Point X="-1.934341674805" Y="29.05129296875" />
                  <Point X="-1.915066894531" Y="29.043794921875" />
                  <Point X="-1.905219238281" Y="29.0405703125" />
                  <Point X="-1.88431237793" Y="29.034966796875" />
                  <Point X="-1.874174194336" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.833053955078" Y="29.028783203125" />
                  <Point X="-1.812407470703" Y="29.03013671875" />
                  <Point X="-1.802120117188" Y="29.031376953125" />
                  <Point X="-1.780804443359" Y="29.03513671875" />
                  <Point X="-1.770712402344" Y="29.0374921875" />
                  <Point X="-1.750859375" Y="29.04328125" />
                  <Point X="-1.741098510742" Y="29.04671484375" />
                  <Point X="-1.707860717773" Y="29.060482421875" />
                  <Point X="-1.641924316406" Y="29.087794921875" />
                  <Point X="-1.632590576172" Y="29.092271484375" />
                  <Point X="-1.614456420898" Y="29.10221875" />
                  <Point X="-1.605655883789" Y="29.107689453125" />
                  <Point X="-1.587926513672" Y="29.120103515625" />
                  <Point X="-1.579778320312" Y="29.126498046875" />
                  <Point X="-1.564226196289" Y="29.14013671875" />
                  <Point X="-1.550251098633" Y="29.155388671875" />
                  <Point X="-1.538019775391" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516854858398" Y="29.20873046875" />
                  <Point X="-1.508524536133" Y="29.2276640625" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.494059082031" Y="29.271666015625" />
                  <Point X="-1.472597900391" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266357422" Y="29.56265625" />
                  <Point X="-1.422669189453" Y="29.5785234375" />
                  <Point X="-0.931165039062" Y="29.7163203125" />
                  <Point X="-0.68490246582" Y="29.74514453125" />
                  <Point X="-0.36522253418" Y="29.78255859375" />
                  <Point X="-0.290431274414" Y="29.50343359375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.366457702637" Y="29.74121875" />
                  <Point X="0.378190826416" Y="29.7850078125" />
                  <Point X="0.398720184326" Y="29.782857421875" />
                  <Point X="0.827853210449" Y="29.737916015625" />
                  <Point X="1.031634765625" Y="29.688716796875" />
                  <Point X="1.453622436523" Y="29.586833984375" />
                  <Point X="1.584266967773" Y="29.53944921875" />
                  <Point X="1.858247192383" Y="29.44007421875" />
                  <Point X="1.986506591797" Y="29.380091796875" />
                  <Point X="2.250434082031" Y="29.25666015625" />
                  <Point X="2.374403076172" Y="29.1844375" />
                  <Point X="2.629425048828" Y="29.035861328125" />
                  <Point X="2.746285400391" Y="28.952755859375" />
                  <Point X="2.817778564453" Y="28.9019140625" />
                  <Point X="2.354079345703" Y="28.098763671875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.034105834961" Y="27.5136875" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755371094" Y="27.383240234375" />
                  <Point X="2.013410888672" Y="27.369578125" />
                  <Point X="2.016216674805" Y="27.346310546875" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.02914440918" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045319580078" Y="27.22388671875" />
                  <Point X="2.055680419922" Y="27.203845703125" />
                  <Point X="2.061457763672" Y="27.1941328125" />
                  <Point X="2.075854980469" Y="27.1729140625" />
                  <Point X="2.104415527344" Y="27.13082421875" />
                  <Point X="2.109808105469" Y="27.123634765625" />
                  <Point X="2.130456298828" Y="27.100130859375" />
                  <Point X="2.157588867188" Y="27.075392578125" />
                  <Point X="2.168250488281" Y="27.066984375" />
                  <Point X="2.189468017578" Y="27.0525859375" />
                  <Point X="2.231559082031" Y="27.024025390625" />
                  <Point X="2.241275634766" Y="27.01824609375" />
                  <Point X="2.261324951172" Y="27.007880859375" />
                  <Point X="2.271657714844" Y="27.003294921875" />
                  <Point X="2.293745605469" Y="26.995029296875" />
                  <Point X="2.304554199219" Y="26.991703125" />
                  <Point X="2.32648046875" Y="26.986361328125" />
                  <Point X="2.337598144531" Y="26.984345703125" />
                  <Point X="2.360865478516" Y="26.981541015625" />
                  <Point X="2.407022949219" Y="26.975974609375" />
                  <Point X="2.416042236328" Y="26.9753203125" />
                  <Point X="2.447577636719" Y="26.97549609375" />
                  <Point X="2.484316650391" Y="26.979822265625" />
                  <Point X="2.497748291016" Y="26.98239453125" />
                  <Point X="2.524656005859" Y="26.98958984375" />
                  <Point X="2.578034667969" Y="27.003865234375" />
                  <Point X="2.583999755859" Y="27.005671875" />
                  <Point X="2.604416748047" Y="27.013072265625" />
                  <Point X="2.627660888672" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.628251220703" Y="27.600732421875" />
                  <Point X="3.940404296875" Y="27.780953125" />
                  <Point X="4.043947998047" Y="27.63705078125" />
                  <Point X="4.109096191406" Y="27.529390625" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.553145263672" Y="27.0355546875" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.168135742188" Y="26.7398671875" />
                  <Point X="3.152114257812" Y="26.7252109375" />
                  <Point X="3.134666748047" Y="26.706599609375" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.1092109375" Y="26.674158203125" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065635742188" Y="26.6166015625" />
                  <Point X="3.049739257812" Y="26.58937109375" />
                  <Point X="3.034762939453" Y="26.555544921875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.022926513672" Y="26.516876953125" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.01062109375" Y="26.32387109375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024597900391" Y="26.258234375" />
                  <Point X="3.034681640625" Y="26.22861328125" />
                  <Point X="3.050285400391" Y="26.19537109375" />
                  <Point X="3.056918945312" Y="26.1835234375" />
                  <Point X="3.073025146484" Y="26.15904296875" />
                  <Point X="3.1049765625" Y="26.110478515625" />
                  <Point X="3.111739501953" Y="26.101421875" />
                  <Point X="3.126295898438" Y="26.084173828125" />
                  <Point X="3.134089355469" Y="26.075982421875" />
                  <Point X="3.151332763672" Y="26.05989453125" />
                  <Point X="3.160039550781" Y="26.05269140625" />
                  <Point X="3.178245605469" Y="26.039369140625" />
                  <Point X="3.187744873047" Y="26.03325" />
                  <Point X="3.211084960938" Y="26.020111328125" />
                  <Point X="3.257386962891" Y="25.994046875" />
                  <Point X="3.265483642578" Y="25.989984375" />
                  <Point X="3.294676757813" Y="25.9780859375" />
                  <Point X="3.330276611328" Y="25.968021484375" />
                  <Point X="3.343676269531" Y="25.965255859375" />
                  <Point X="3.375233642578" Y="25.9610859375" />
                  <Point X="3.437836914063" Y="25.9528125" />
                  <Point X="3.444033935547" Y="25.95219921875" />
                  <Point X="3.465708496094" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.443143554688" Y="26.076884765625" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.914236328125" />
                  <Point X="4.773212402344" Y="25.78237890625" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.127702148438" Y="25.538103515625" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686022460938" Y="25.419541015625" />
                  <Point X="3.665624023438" Y="25.412134765625" />
                  <Point X="3.642384521484" Y="25.401619140625" />
                  <Point X="3.634009521484" Y="25.397318359375" />
                  <Point X="3.603005126953" Y="25.3793984375" />
                  <Point X="3.541499511719" Y="25.34384765625" />
                  <Point X="3.533883789062" Y="25.338947265625" />
                  <Point X="3.508777587891" Y="25.319873046875" />
                  <Point X="3.481994140625" Y="25.2943515625" />
                  <Point X="3.472794189453" Y="25.284224609375" />
                  <Point X="3.454191650391" Y="25.26051953125" />
                  <Point X="3.417288330078" Y="25.21349609375" />
                  <Point X="3.410852783203" Y="25.20420703125" />
                  <Point X="3.399129394531" Y="25.184927734375" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004150391" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.364175292969" Y="25.07809375" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.358075439453" Y="24.89119921875" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816013671875" />
                  <Point X="3.38000390625" Y="24.794513671875" />
                  <Point X="3.384067626953" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.762501953125" />
                  <Point X="3.399129394531" Y="24.75251171875" />
                  <Point X="3.410852783203" Y="24.733232421875" />
                  <Point X="3.417288330078" Y="24.723943359375" />
                  <Point X="3.435890869141" Y="24.70023828125" />
                  <Point X="3.472794189453" Y="24.65321484375" />
                  <Point X="3.478718017578" Y="24.646365234375" />
                  <Point X="3.501137939453" Y="24.6241953125" />
                  <Point X="3.530177001953" Y="24.6012734375" />
                  <Point X="3.541498535156" Y="24.593591796875" />
                  <Point X="3.572502929688" Y="24.575671875" />
                  <Point X="3.634008544922" Y="24.540119140625" />
                  <Point X="3.639498535156" Y="24.537181640625" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026367188" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.556344238281" Y="24.284482421875" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.735312011719" Y="23.953689453125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.942083496094" Y="24.02422265625" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400096923828" Y="24.09195703125" />
                  <Point X="3.366719970703" Y="24.089158203125" />
                  <Point X="3.354480224609" Y="24.087322265625" />
                  <Point X="3.293629638672" Y="24.074095703125" />
                  <Point X="3.172916015625" Y="24.047859375" />
                  <Point X="3.157871826172" Y="24.043255859375" />
                  <Point X="3.128752197266" Y="24.031630859375" />
                  <Point X="3.114676757812" Y="24.024609375" />
                  <Point X="3.086849853516" Y="24.007716796875" />
                  <Point X="3.074125488281" Y="23.998470703125" />
                  <Point X="3.050374755859" Y="23.978" />
                  <Point X="3.039348388672" Y="23.966775390625" />
                  <Point X="3.002568115234" Y="23.9225390625" />
                  <Point X="2.929604248047" Y="23.834787109375" />
                  <Point X="2.921325195312" Y="23.823150390625" />
                  <Point X="2.906605712891" Y="23.79876953125" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.869885742188" Y="23.646052734375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158935547" Y="23.394517578125" />
                  <Point X="2.896541259766" Y="23.380626953125" />
                  <Point X="2.930216796875" Y="23.328248046875" />
                  <Point X="2.997021484375" Y="23.224337890625" />
                  <Point X="3.001740966797" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.854920166016" Y="22.548228515625" />
                  <Point X="4.087169921875" Y="22.370015625" />
                  <Point X="4.045492919922" Y="22.302576171875" />
                  <Point X="4.001273193359" Y="22.239748046875" />
                  <Point X="3.298493652344" Y="22.64549609375" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841198730469" Y="22.909109375" />
                  <Point X="2.815021484375" Y="22.92048828125" />
                  <Point X="2.783118164062" Y="22.930673828125" />
                  <Point X="2.771109863281" Y="22.933662109375" />
                  <Point X="2.698687988281" Y="22.9467421875" />
                  <Point X="2.55501953125" Y="22.9726875" />
                  <Point X="2.539359863281" Y="22.97419140625" />
                  <Point X="2.508008300781" Y="22.974595703125" />
                  <Point X="2.49231640625" Y="22.97349609375" />
                  <Point X="2.460144287109" Y="22.9685390625" />
                  <Point X="2.444846435547" Y="22.96486328125" />
                  <Point X="2.415069091797" Y="22.9550390625" />
                  <Point X="2.400589599609" Y="22.948890625" />
                  <Point X="2.340424804688" Y="22.9172265625" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.208967773438" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175212646484" Y="22.820626953125" />
                  <Point X="2.154252685547" Y="22.79966796875" />
                  <Point X="2.144941650391" Y="22.78883984375" />
                  <Point X="2.128047363281" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.088799804688" Y="22.693640625" />
                  <Point X="2.025985229492" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.015266601563" Y="22.3474375" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.5849921875" Y="21.23364453125" />
                  <Point X="2.73589453125" Y="20.972275390625" />
                  <Point X="2.723753173828" Y="20.9639140625" />
                  <Point X="2.177336181641" Y="21.67601953125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828654663086" Y="22.1298515625" />
                  <Point X="1.808836669922" Y="22.1503671875" />
                  <Point X="1.783252685547" Y="22.171994140625" />
                  <Point X="1.773296875" Y="22.179353515625" />
                  <Point X="1.701869506836" Y="22.2252734375" />
                  <Point X="1.560173461914" Y="22.31637109375" />
                  <Point X="1.546284912109" Y="22.323751953125" />
                  <Point X="1.517471435547" Y="22.336125" />
                  <Point X="1.502547119141" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384887695" Y="22.3513046875" />
                  <Point X="1.424120361328" Y="22.35362109375" />
                  <Point X="1.408396972656" Y="22.35348046875" />
                  <Point X="1.330278808594" Y="22.34629296875" />
                  <Point X="1.175310180664" Y="22.332033203125" />
                  <Point X="1.161229492188" Y="22.32966015625" />
                  <Point X="1.133582885742" Y="22.322830078125" />
                  <Point X="1.120017089844" Y="22.318373046875" />
                  <Point X="1.092631591797" Y="22.30703125" />
                  <Point X="1.079884033203" Y="22.30058984375" />
                  <Point X="1.055496459961" Y="22.2858671875" />
                  <Point X="1.043856323242" Y="22.2775859375" />
                  <Point X="0.983535827637" Y="22.2274296875" />
                  <Point X="0.863872680664" Y="22.12793359375" />
                  <Point X="0.852649597168" Y="22.116908203125" />
                  <Point X="0.832178710938" Y="22.09315625" />
                  <Point X="0.822930969238" Y="22.0804296875" />
                  <Point X="0.806038635254" Y="22.0526015625" />
                  <Point X="0.799017822266" Y="22.03852734375" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.764756408691" Y="21.911390625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.83309173584" Y="20.847658203125" />
                  <Point X="0.789317932129" Y="21.0110234375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407287598" Y="21.58679296875" />
                  <Point X="0.565534790039" Y="21.665853515625" />
                  <Point X="0.4566796875" Y="21.822693359375" />
                  <Point X="0.446668151855" Y="21.83483203125" />
                  <Point X="0.424784912109" Y="21.85728515625" />
                  <Point X="0.412913208008" Y="21.867599609375" />
                  <Point X="0.386658081055" Y="21.88684375" />
                  <Point X="0.373244873047" Y="21.895060546875" />
                  <Point X="0.345242980957" Y="21.909169921875" />
                  <Point X="0.330654449463" Y="21.9150625" />
                  <Point X="0.245742172241" Y="21.941416015625" />
                  <Point X="0.077295211792" Y="21.9936953125" />
                  <Point X="0.063377120972" Y="21.996890625" />
                  <Point X="0.035217838287" Y="22.00116015625" />
                  <Point X="0.020976644516" Y="22.002234375" />
                  <Point X="-0.008664708138" Y="22.002234375" />
                  <Point X="-0.022905902863" Y="22.00116015625" />
                  <Point X="-0.051065185547" Y="21.996890625" />
                  <Point X="-0.064983421326" Y="21.9936953125" />
                  <Point X="-0.149895553589" Y="21.967341796875" />
                  <Point X="-0.318342376709" Y="21.9150625" />
                  <Point X="-0.332931030273" Y="21.909169921875" />
                  <Point X="-0.360932922363" Y="21.895060546875" />
                  <Point X="-0.374346160889" Y="21.88684375" />
                  <Point X="-0.400601287842" Y="21.867599609375" />
                  <Point X="-0.412475067139" Y="21.857283203125" />
                  <Point X="-0.434358886719" Y="21.834828125" />
                  <Point X="-0.444368804932" Y="21.822689453125" />
                  <Point X="-0.499241424561" Y="21.743626953125" />
                  <Point X="-0.608096374512" Y="21.5867890625" />
                  <Point X="-0.61247052002" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.909081237793" Y="20.51811328125" />
                  <Point X="-0.985425598145" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.952183706944" Y="21.218124894467" />
                  <Point X="-2.444995882285" Y="20.913375704179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.904676534189" Y="21.300409877973" />
                  <Point X="-2.383198371416" Y="20.98707418628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.857169361434" Y="21.382694861478" />
                  <Point X="-2.299677650664" Y="21.047720047042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.131120960004" Y="20.345580350524" />
                  <Point X="-0.979691700831" Y="20.254592472118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.896312450607" Y="22.117905194032" />
                  <Point X="-3.730626575045" Y="22.018351076278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.809662188679" Y="21.464979844984" />
                  <Point X="-2.212343845048" Y="21.106074775274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.11966994465" Y="20.449530059085" />
                  <Point X="-0.954112930885" Y="20.35005336931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.035657347868" Y="22.312462227994" />
                  <Point X="-3.636560020844" Y="22.072660361027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762155015925" Y="21.54726482849" />
                  <Point X="-2.125009531097" Y="21.164429198066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.136946205542" Y="20.570740856634" />
                  <Point X="-0.928534160939" Y="20.445514266502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138661607899" Y="22.485183604175" />
                  <Point X="-3.542493468307" Y="22.126969646775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.71464784317" Y="21.629549811996" />
                  <Point X="-2.023080570234" Y="21.214014272281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.161984168526" Y="20.696615355308" />
                  <Point X="-0.902955414849" Y="20.540975178028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133766662186" Y="22.593072596799" />
                  <Point X="-3.448427021819" Y="22.181278996244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.667140670415" Y="21.711834795502" />
                  <Point X="-1.8443130908" Y="21.217430106661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.188913803015" Y="20.823626484893" />
                  <Point X="-0.877376744515" Y="20.636436135072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.05276154568" Y="22.655229985086" />
                  <Point X="-3.35436057533" Y="22.235588345712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.61963349766" Y="21.794119779007" />
                  <Point X="-1.637276981404" Y="21.203860434544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.397009015048" Y="21.059492875547" />
                  <Point X="-0.851798074182" Y="20.731897092117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.971756429173" Y="22.717387373373" />
                  <Point X="-3.260294128841" Y="22.289897695181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.572126306156" Y="21.876404751247" />
                  <Point X="-0.826219403848" Y="20.827358049162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.890751312667" Y="22.77954476166" />
                  <Point X="-3.166227682353" Y="22.344207044649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.524619086163" Y="21.95868970637" />
                  <Point X="-0.800640733514" Y="20.922819006207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80974619616" Y="22.841702149947" />
                  <Point X="-3.072161235864" Y="22.398516394118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.47711186617" Y="22.040974661492" />
                  <Point X="-0.77506206318" Y="21.018279963251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.728741079653" Y="22.903859538234" />
                  <Point X="-2.978094789375" Y="22.452825743586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.429604646178" Y="22.123259616614" />
                  <Point X="-0.749483392846" Y="21.113740920296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.647735963147" Y="22.966016926521" />
                  <Point X="-2.884028342886" Y="22.507135093055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.382097426185" Y="22.205544571737" />
                  <Point X="-0.723904722512" Y="21.209201877341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.56673084664" Y="23.028174314807" />
                  <Point X="-2.789961896398" Y="22.561444442524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335561856982" Y="22.288413353554" />
                  <Point X="-0.698326052178" Y="21.304662834386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.686011873143" Y="23.811536377994" />
                  <Point X="-4.547402559719" Y="23.728251500126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.485725730133" Y="23.090331703094" />
                  <Point X="-2.695895449909" Y="22.615753791992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311507214635" Y="22.384790038998" />
                  <Point X="-0.672747381844" Y="21.40012379143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719454062531" Y="23.942460645346" />
                  <Point X="-4.396101072757" Y="23.748170567746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.404720446172" Y="23.152488990765" />
                  <Point X="-2.570585990212" Y="22.651290445204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.343308384262" Y="22.514728282202" />
                  <Point X="-0.64716871151" Y="21.495584748475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747054072" Y="24.069874576856" />
                  <Point X="-4.244799550701" Y="23.768089614279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.323715159971" Y="23.214646277089" />
                  <Point X="-0.609860646724" Y="21.583997974308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764395494711" Y="24.191124527576" />
                  <Point X="-4.093497998781" Y="23.788008642868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.24270987377" Y="23.276803563412" />
                  <Point X="-0.555698264612" Y="21.6622841046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.781737067607" Y="24.312374568538" />
                  <Point X="-3.942196446861" Y="23.807927671457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.161704587568" Y="23.338960849736" />
                  <Point X="-0.501413859108" Y="21.740496915841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804033327664" Y="20.956105111089" />
                  <Point X="0.820084117851" Y="20.946460823361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.693571063738" Y="24.370229261611" />
                  <Point X="-3.790894894941" Y="23.827846700046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.080699301367" Y="23.40111813606" />
                  <Point X="-0.447130473013" Y="21.818710339604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.768637797449" Y="21.08820306402" />
                  <Point X="0.804239669208" Y="21.066811301316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.566005630719" Y="24.404410389296" />
                  <Point X="-3.639593343021" Y="23.847765728635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.007296409562" Y="23.467843401788" />
                  <Point X="-0.375123221218" Y="21.886274190452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733242426992" Y="21.220300920959" />
                  <Point X="0.788395220566" Y="21.187161779271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.438440197701" Y="24.438591516982" />
                  <Point X="-3.4882917911" Y="23.867684757224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963664369054" Y="23.552456799654" />
                  <Point X="-0.265748696509" Y="21.931385518564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.697847056536" Y="21.352398777898" />
                  <Point X="0.772550771924" Y="21.307512257226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.310874764683" Y="24.472772644668" />
                  <Point X="-3.33699023918" Y="23.887603785813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947362852813" Y="23.65349203325" />
                  <Point X="-0.144120562905" Y="21.969134135651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.662451686079" Y="21.484496634837" />
                  <Point X="0.756706323282" Y="21.42786273518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.183309331665" Y="24.506953772353" />
                  <Point X="-3.162826773593" Y="23.893785990803" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000175044747" Y="23.796054972323" />
                  <Point X="-0.014076809374" Y="22.001826138139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.580170482643" Y="21.644766342403" />
                  <Point X="0.74086187464" Y="21.548213213135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.055743898647" Y="24.541134900039" />
                  <Point X="0.297808800513" Y="21.925256530253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.44436142619" Y="21.837198828868" />
                  <Point X="0.725297318989" Y="21.668395514414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92817857596" Y="24.575316094018" />
                  <Point X="0.734789902305" Y="21.773521967662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.800613324812" Y="24.609497330982" />
                  <Point X="0.756096762989" Y="21.871549686898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.673048073664" Y="24.643678567947" />
                  <Point X="0.777403693745" Y="21.969577364029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.545482822516" Y="24.677859804911" />
                  <Point X="0.810753591743" Y="22.060368896409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434983267713" Y="24.722295146745" />
                  <Point X="0.871795656939" Y="22.134521296064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.641404585734" Y="21.071232979672" />
                  <Point X="2.698604034253" Y="21.036864083627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778279026071" Y="25.640258840385" />
                  <Point X="-4.627028379647" Y="25.549378283346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358206518646" Y="24.786993194509" />
                  <Point X="0.949173264806" Y="22.198858311438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483609239345" Y="21.276876161918" />
                  <Point X="2.600626635243" Y="21.206565016982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763218134374" Y="25.742039516412" />
                  <Point X="-4.294116611221" Y="25.460174884824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31295355443" Y="24.870632643153" />
                  <Point X="1.026550036292" Y="22.26319582936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.325813892956" Y="21.482519344165" />
                  <Point X="2.502649551348" Y="21.376265760997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748157242678" Y="25.84382019244" />
                  <Point X="-3.961205208783" Y="25.370971706209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29492834315" Y="24.970632176281" />
                  <Point X="1.119516883511" Y="22.318165884726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.168018555897" Y="21.688162520805" />
                  <Point X="2.404672527284" Y="21.545966469062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.733096350981" Y="25.945600868467" />
                  <Point X="-3.628293806344" Y="25.281768527594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326624246082" Y="25.100507168873" />
                  <Point X="1.266868700171" Y="22.340458153688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.010223367521" Y="21.893805608108" />
                  <Point X="2.306695503221" Y="21.715667177127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708530363745" Y="26.041670306905" />
                  <Point X="1.430158863612" Y="22.353173697737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.852428179144" Y="22.09944869541" />
                  <Point X="2.208718479157" Y="21.885367885191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.682702751721" Y="26.136981684692" />
                  <Point X="2.110741455093" Y="22.055068593256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.656875060905" Y="26.232293015135" />
                  <Point X="2.040421576663" Y="22.208151211675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.610369403038" Y="26.315179769497" />
                  <Point X="2.01796934915" Y="22.332472043733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.374161600739" Y="26.284081975923" />
                  <Point X="2.000447213221" Y="22.453830577909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137953798439" Y="26.25298418235" />
                  <Point X="2.017964687522" Y="22.554135190192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.901748836253" Y="26.221888095289" />
                  <Point X="2.060315748648" Y="22.639518278123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.696913654869" Y="26.20964087414" />
                  <Point X="2.10463113016" Y="22.723721083291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.578261305926" Y="26.24917752304" />
                  <Point X="2.157418355452" Y="22.802833491161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.499392315062" Y="26.312618425103" />
                  <Point X="2.23970689859" Y="22.864219718928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453347760204" Y="26.395782238103" />
                  <Point X="2.33803462382" Y="22.915968633813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.424059288934" Y="26.489014121861" />
                  <Point X="2.442436114902" Y="22.96406806199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.437278982554" Y="26.607787487889" />
                  <Point X="2.637261594351" Y="22.957835276541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.622187331418" Y="26.829721805586" />
                  <Point X="4.005891412436" Y="22.246309689562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.223272950075" Y="27.301720655236" />
                  <Point X="4.059358904278" Y="22.325013352052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.175379351754" Y="27.383773450837" />
                  <Point X="3.792119882384" Y="22.596416928911" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.127485820391" Y="27.46582628667" />
                  <Point X="3.126342591551" Y="23.107286456751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079592289029" Y="27.547879122504" />
                  <Point X="2.922136611855" Y="23.340815960856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.031698757666" Y="27.629931958337" />
                  <Point X="2.86140529304" Y="23.488137191409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.979610800487" Y="27.709464528878" />
                  <Point X="2.865335653929" Y="23.596605765067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.920853526255" Y="27.784989769447" />
                  <Point X="2.87499980873" Y="23.701629127767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.862096133368" Y="27.860514938719" />
                  <Point X="2.904550779074" Y="23.794703286169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803338236881" Y="27.936039805399" />
                  <Point X="2.960483954158" Y="23.871925416699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.744580340394" Y="28.011564672078" />
                  <Point X="3.021935053306" Y="23.94583204396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.082949193013" Y="27.72484674403" />
                  <Point X="3.095079433548" Y="24.012712639105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.939907727492" Y="27.749728933246" />
                  <Point X="3.208254994358" Y="24.055540074313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.85963333964" Y="27.812325387605" />
                  <Point X="3.343709306926" Y="24.084981084949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.793963100896" Y="27.883696900037" />
                  <Point X="3.541514396307" Y="24.076957969233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754568001563" Y="27.970856109001" />
                  <Point X="3.777720481169" Y="24.0458612076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75192051033" Y="28.080095508515" />
                  <Point X="4.013926715279" Y="24.01476435629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.776685900802" Y="28.2058062291" />
                  <Point X="4.250133290839" Y="23.983667299816" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870722171583" Y="28.373139093708" />
                  <Point X="4.4863398664" Y="23.952570243341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.968699020308" Y="28.542839696418" />
                  <Point X="3.444302898715" Y="24.689519393529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.765911354735" Y="24.496277537561" />
                  <Point X="4.72254644196" Y="23.921473186867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.053358679205" Y="28.704538524206" />
                  <Point X="3.36652406362" Y="24.847083805268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.098822313685" Y="24.40707462542" />
                  <Point X="4.749548958675" Y="24.016078610694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972316127767" Y="28.766673419317" />
                  <Point X="3.348824055462" Y="24.968549215862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.431733272636" Y="24.31787171328" />
                  <Point X="4.768624132096" Y="24.11544726292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.891273157042" Y="28.828808062494" />
                  <Point X="3.362811899812" Y="25.070974643782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764643033968" Y="24.228669520741" />
                  <Point X="4.783945281331" Y="24.217071560442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.810230186317" Y="28.890942705672" />
                  <Point X="3.389630896457" Y="25.165690337591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.729187215592" Y="28.95307734885" />
                  <Point X="3.442037039913" Y="25.245031722529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.64810167464" Y="29.015186413254" />
                  <Point X="3.506130484955" Y="25.317350668201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.552264240577" Y="29.068431646033" />
                  <Point X="3.595009538554" Y="25.374776917773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.456426806514" Y="29.121676878812" />
                  <Point X="3.698875657772" Y="25.423198029819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.360589834493" Y="29.174922389212" />
                  <Point X="3.826440567344" Y="25.457379472023" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.264752923646" Y="29.228167936371" />
                  <Point X="3.954005476916" Y="25.491560914227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.765494862041" Y="29.039013601156" />
                  <Point X="3.102734541647" Y="26.113886268087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351907174443" Y="25.964168245701" />
                  <Point X="4.081570386487" Y="25.52574235643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.653942833235" Y="29.082816552809" />
                  <Point X="3.020939234298" Y="26.27386401983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545456993877" Y="25.958701954119" />
                  <Point X="4.209135611751" Y="25.559923608948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.564494960853" Y="29.139901021575" />
                  <Point X="3.000913545906" Y="26.396726840089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696758243892" Y="25.97862116411" />
                  <Point X="4.336701015854" Y="25.594104754007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512200370085" Y="29.219309434129" />
                  <Point X="3.017528184312" Y="26.497573930908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848059493908" Y="25.998540374102" />
                  <Point X="4.464266419956" Y="25.628285899067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.481448444451" Y="29.311661985792" />
                  <Point X="2.074360751125" Y="27.175116271394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.405478067039" Y="26.976160915983" />
                  <Point X="3.049626876225" Y="26.58911726375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.999360743924" Y="26.018459584093" />
                  <Point X="4.591831824059" Y="25.662467044127" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462341294865" Y="29.411011424799" />
                  <Point X="2.019522250156" Y="27.318896739768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.554360033382" Y="26.99753377826" />
                  <Point X="3.10395621821" Y="26.667303074429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.150661993939" Y="26.038378794084" />
                  <Point X="4.719397228161" Y="25.696648189187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474885282191" Y="29.529378785524" />
                  <Point X="2.015291905292" Y="27.432268760138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662696654692" Y="27.043268741652" />
                  <Point X="3.167939859725" Y="26.739687996716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.301963243955" Y="26.058298004076" />
                  <Point X="4.77445528942" Y="25.774396141152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.388272566995" Y="29.588166788592" />
                  <Point X="2.038277525306" Y="27.529287779003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.75676311253" Y="27.097578084301" />
                  <Point X="3.248735128368" Y="26.80197147432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.453264468989" Y="26.078217229078" />
                  <Point X="4.755418915344" Y="25.896664521399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262503282068" Y="29.623427150931" />
                  <Point X="2.076144142677" Y="27.617365392584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.850829570368" Y="27.15188742695" />
                  <Point X="3.329740013371" Y="26.864129001708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.604565345539" Y="26.09813666347" />
                  <Point X="4.725620850243" Y="26.025399177977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13673399714" Y="29.65868751327" />
                  <Point X="2.123651349945" Y="27.699650355352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.944896028206" Y="27.206196769599" />
                  <Point X="3.410744898375" Y="26.926286529096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.010964712212" Y="29.693947875609" />
                  <Point X="-0.20128650254" Y="29.207444125332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.002306211325" Y="29.08511328125" />
                  <Point X="2.171158557213" Y="27.78193531812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.038962486044" Y="27.260506112248" />
                  <Point X="3.491749783378" Y="26.988444056484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.874738179492" Y="29.722924889567" />
                  <Point X="-0.24840410199" Y="29.34658540804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.129221211095" Y="29.11968522866" />
                  <Point X="2.218665764482" Y="27.864220280888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133028943882" Y="27.314815454897" />
                  <Point X="3.572754776905" Y="27.050601518665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.720358677373" Y="29.740994499094" />
                  <Point X="-0.283799470712" Y="29.478683263937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.200594971553" Y="29.187629719504" />
                  <Point X="2.26617297175" Y="27.946505243656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.22709540172" Y="27.369124797547" />
                  <Point X="3.653760110212" Y="27.112758776685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.565977405662" Y="29.759063045342" />
                  <Point X="-0.319194953114" Y="29.610781188139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.241271938204" Y="29.274018704878" />
                  <Point X="2.313680179019" Y="28.028790206424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.321161859558" Y="27.423434140196" />
                  <Point X="3.734765443519" Y="27.174916034705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.411595606358" Y="29.777131274581" />
                  <Point X="-0.354590461726" Y="29.74287912809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.266850602909" Y="29.369479665304" />
                  <Point X="2.361187391941" Y="28.111075165795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415228317396" Y="27.477743482845" />
                  <Point X="3.815770776825" Y="27.237073292726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.292429267615" Y="29.464940625731" />
                  <Point X="2.408694636995" Y="28.193360105859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.509294775234" Y="27.532052825494" />
                  <Point X="3.896776110132" Y="27.299230550746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.31800793232" Y="29.560401586158" />
                  <Point X="2.456201882048" Y="28.275645045923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.603361233072" Y="27.586362168143" />
                  <Point X="3.977781443439" Y="27.361387808766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.343586597025" Y="29.655862546584" />
                  <Point X="2.503709127102" Y="28.357929985987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.697427901265" Y="27.640671384398" />
                  <Point X="4.058786776745" Y="27.423545066787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.369165239905" Y="29.751323520126" />
                  <Point X="2.551216372156" Y="28.440214926051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.791494645145" Y="27.694980555176" />
                  <Point X="4.133100179892" Y="27.489723242105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.522754816223" Y="29.769867764958" />
                  <Point X="2.59872361721" Y="28.522499866115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.885561389025" Y="27.749289725954" />
                  <Point X="4.022309793937" Y="27.667122994728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746142137716" Y="29.746473293418" />
                  <Point X="2.646230862264" Y="28.604784806179" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.023414493634" Y="29.690701426738" />
                  <Point X="2.693738107318" Y="28.687069746243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.331768409575" Y="29.616253874661" />
                  <Point X="2.741245352372" Y="28.769354686307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.735088270007" Y="29.484745026392" />
                  <Point X="2.788752597426" Y="28.851639626371" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.605792175293" Y="20.96184765625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.40944619751" Y="21.557517578125" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.189423522949" Y="21.759955078125" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.093576835632" Y="21.785880859375" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.343151611328" Y="21.635294921875" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.725555236816" Y="20.4689375" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-0.902438964844" Y="20.0235390625" />
                  <Point X="-1.10023046875" Y="20.061931640625" />
                  <Point X="-1.197888793945" Y="20.08705859375" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.325311157227" Y="20.32620703125" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.32996862793" Y="20.56722265625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.464458984375" Y="20.8659296875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.752998291016" Y="21.021037109375" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.076334960938" Y="20.96844140625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.409256835938" Y="20.64784375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.565889160156" Y="20.65286328125" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-2.991041259766" Y="20.936494140625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.755527832031" Y="21.938744140625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.402412109375" />
                  <Point X="-2.513984863281" Y="22.4312421875" />
                  <Point X="-2.531327148438" Y="22.44858203125" />
                  <Point X="-2.560156494141" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.448946289062" Y="21.961587890625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.932631347656" Y="21.851916015625" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.258636230469" Y="22.31541015625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.600320800781" Y="23.241890625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535644531" Y="23.588916015625" />
                  <Point X="-3.143239990234" Y="23.613955078125" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651611328" Y="23.649947265625" />
                  <Point X="-3.148654785156" Y="23.678927734375" />
                  <Point X="-3.170037109375" Y="23.6945859375" />
                  <Point X="-3.187644287109" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.3017421875" Y="23.568953125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.837801269531" Y="23.638060546875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.9530390625" Y="24.16812109375" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.054741699219" Y="24.738107421875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.532594238281" Y="24.885029296875" />
                  <Point X="-3.514142578125" Y="24.8978359375" />
                  <Point X="-3.502324462891" Y="24.909353515625" />
                  <Point X="-3.491798828125" Y="24.934080078125" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.488748046875" Y="24.993529296875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.50232421875" Y="25.0280859375" />
                  <Point X="-3.523443847656" Y="25.04605859375" />
                  <Point X="-3.541895507813" Y="25.058865234375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.543952148438" Y="25.3304140625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.975385742188" Y="25.606212890625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.866020019531" Y="26.1869296875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.109727539062" Y="26.440908203125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731704345703" Y="26.3958671875" />
                  <Point X="-3.711117675781" Y="26.402357421875" />
                  <Point X="-3.670278320312" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.630859375" Y="26.4637265625" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.626283203125" Y="26.564658203125" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.2258203125" Y="27.053416015625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.384373535156" Y="27.402625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.023262451172" Y="27.96278125" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.378085449219" Y="28.053341796875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515136719" Y="27.92043359375" />
                  <Point X="-3.109843505859" Y="27.91792578125" />
                  <Point X="-3.052965820312" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.992901611328" Y="27.947755859375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.940582763672" Y="28.05651171875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.202813720703" Y="28.568337890625" />
                  <Point X="-3.307278808594" Y="28.74927734375" />
                  <Point X="-3.143638671875" Y="28.87473828125" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.537496337891" Y="29.2939921875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.034847290039" Y="29.374916015625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246948242" Y="29.273662109375" />
                  <Point X="-1.919335693359" Y="29.257048828125" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124023438" Y="29.218490234375" />
                  <Point X="-1.81380847168" Y="29.22225" />
                  <Point X="-1.780570678711" Y="29.236017578125" />
                  <Point X="-1.714634399414" Y="29.263330078125" />
                  <Point X="-1.696905029297" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.675265136719" Y="29.328798828125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.680424682617" Y="29.63495703125" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.473960449219" Y="29.76146875" />
                  <Point X="-0.968083251953" Y="29.903296875" />
                  <Point X="-0.706989746094" Y="29.93385546875" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.106905418396" Y="29.552609375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282123566" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594043732" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.182931808472" Y="29.79039453125" />
                  <Point X="0.2366484375" Y="29.990869140625" />
                  <Point X="0.418510375977" Y="29.971822265625" />
                  <Point X="0.860210021973" Y="29.925564453125" />
                  <Point X="1.076225341797" Y="29.873412109375" />
                  <Point X="1.508456176758" Y="29.769056640625" />
                  <Point X="1.64905065918" Y="29.7180625" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="2.066995849609" Y="29.552201171875" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.470047851562" Y="29.348609375" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.856399414062" Y="29.107595703125" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.518624267578" Y="28.003763671875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.217656494141" Y="27.46460546875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.204850341797" Y="27.369056640625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.233079589844" Y="27.27959375" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.296157470703" Y="27.2098046875" />
                  <Point X="2.338248535156" Y="27.181244140625" />
                  <Point X="2.360336425781" Y="27.172978515625" />
                  <Point X="2.383603759766" Y="27.170173828125" />
                  <Point X="2.429761230469" Y="27.164607421875" />
                  <Point X="2.448665283203" Y="27.1659453125" />
                  <Point X="2.475572998047" Y="27.173140625" />
                  <Point X="2.528951660156" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.533251220703" Y="27.765275390625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.046900146484" Y="27.958255859375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.271650878906" Y="27.6277578125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.668809814453" Y="26.88481640625" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.260005859375" Y="26.558568359375" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.205906005859" Y="26.465705078125" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.196701171875" Y="26.362265625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287953125" />
                  <Point X="3.231752685547" Y="26.26347265625" />
                  <Point X="3.263704101562" Y="26.214908203125" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.304287597656" Y="26.185681640625" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.368565917969" Y="26.153619140625" />
                  <Point X="3.400123291016" Y="26.14944921875" />
                  <Point X="3.4627265625" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.41834375" Y="26.265259765625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.872319824219" Y="26.226060546875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.960950683594" Y="25.811609375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.176877929688" Y="25.354576171875" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.698082763672" Y="25.2148984375" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.603662353516" Y="25.14322265625" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.550784179688" Y="25.04235546875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.544683837891" Y="24.9269375" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.585361572266" Y="24.81753515625" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636576171875" Y="24.758091796875" />
                  <Point X="3.667580566406" Y="24.740171875" />
                  <Point X="3.729086181641" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.605520019531" Y="24.468009765625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.985768066406" Y="24.281244140625" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.920550292969" Y="23.911419921875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.917283447266" Y="23.83584765625" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.394836669922" Y="23.901658203125" />
                  <Point X="3.333986083984" Y="23.888431640625" />
                  <Point X="3.213272460938" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.148665283203" Y="23.80106640625" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.059086425781" Y="23.628642578125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.090036132812" Y="23.430998046875" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.970584716797" Y="22.698966796875" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.309379882813" Y="22.368166015625" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.146470703125" Y="22.1159296875" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.203493652344" Y="22.480953125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.746685546875" />
                  <Point X="2.664918457031" Y="22.759765625" />
                  <Point X="2.52125" Y="22.7857109375" />
                  <Point X="2.489077880859" Y="22.78075390625" />
                  <Point X="2.428913085938" Y="22.74908984375" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.256935546875" Y="22.60515234375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.2022421875" Y="22.381203125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.749537109375" Y="21.32864453125" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.960184326172" Y="20.898990234375" />
                  <Point X="2.835296142578" Y="20.80978515625" />
                  <Point X="2.770830810547" Y="20.76805859375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.026598999023" Y="21.56035546875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.599121948242" Y="22.065453125" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.347686889648" Y="22.15709375" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.1314921875" />
                  <Point X="1.105012084961" Y="22.0813359375" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.950421203613" Y="21.87103515625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.060002807617" Y="20.579744140625" />
                  <Point X="1.127642578125" Y="20.065970703125" />
                  <Point X="1.112498291016" Y="20.062650390625" />
                  <Point X="0.994367858887" Y="20.0367578125" />
                  <Point X="0.934788513184" Y="20.02593359375" />
                  <Point X="0.860200500488" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#150" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.06476507889" Y="4.59683105088" Z="0.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="-0.718957857503" Y="5.014795969364" Z="0.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.85" />
                  <Point X="-1.493614076544" Y="4.840892876226" Z="0.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.85" />
                  <Point X="-1.736152903915" Y="4.659713024202" Z="0.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.85" />
                  <Point X="-1.729106894088" Y="4.375115128102" Z="0.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.85" />
                  <Point X="-1.805861344817" Y="4.313492202684" Z="0.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.85" />
                  <Point X="-1.90240392213" Y="4.332679244185" Z="0.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.85" />
                  <Point X="-2.001335805635" Y="4.436634312762" Z="0.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.85" />
                  <Point X="-2.567935144678" Y="4.368979430084" Z="0.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.85" />
                  <Point X="-3.180216675305" Y="3.945697858686" Z="0.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.85" />
                  <Point X="-3.252270888989" Y="3.574618046943" Z="0.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.85" />
                  <Point X="-2.996548468626" Y="3.083435051646" Z="0.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.85" />
                  <Point X="-3.034412322705" Y="3.014391245181" Z="0.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.85" />
                  <Point X="-3.111641413961" Y="2.999016230547" Z="0.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.85" />
                  <Point X="-3.359241301354" Y="3.127923059757" Z="0.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.85" />
                  <Point X="-4.068881907746" Y="3.024764295656" Z="0.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.85" />
                  <Point X="-4.433711762955" Y="2.459110406031" Z="0.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.85" />
                  <Point X="-4.262414396013" Y="2.045027664976" Z="0.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.85" />
                  <Point X="-3.676789617806" Y="1.572851166465" Z="0.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.85" />
                  <Point X="-3.68320941337" Y="1.514142709315" Z="0.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.85" />
                  <Point X="-3.732309357755" Y="1.48132349399" Z="0.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.85" />
                  <Point X="-4.109356951002" Y="1.521761514183" Z="0.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.85" />
                  <Point X="-4.920435814624" Y="1.231287993596" Z="0.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.85" />
                  <Point X="-5.031003053085" Y="0.644811818872" Z="0.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.85" />
                  <Point X="-4.563049190107" Y="0.313397820839" Z="0.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.85" />
                  <Point X="-3.558109115856" Y="0.03626255419" Z="0.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.85" />
                  <Point X="-3.542657213796" Y="0.009989670713" Z="0.85" />
                  <Point X="-3.539556741714" Y="0" Z="0.85" />
                  <Point X="-3.545707387339" Y="-0.019817280348" Z="0.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.85" />
                  <Point X="-3.567259479267" Y="-0.042613428943" Z="0.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.85" />
                  <Point X="-4.073838598666" Y="-0.182314235917" Z="0.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.85" />
                  <Point X="-5.008690923142" Y="-0.807677398767" Z="0.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.85" />
                  <Point X="-4.892402164627" Y="-1.343033623139" Z="0.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.85" />
                  <Point X="-4.301371817419" Y="-1.44933931572" Z="0.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.85" />
                  <Point X="-3.201551828604" Y="-1.317226056841" Z="0.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.85" />
                  <Point X="-3.197798673519" Y="-1.342227468917" Z="0.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.85" />
                  <Point X="-3.63691467826" Y="-1.687161380421" Z="0.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.85" />
                  <Point X="-4.307735456737" Y="-2.678917876593" Z="0.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.85" />
                  <Point X="-3.97857330768" Y="-3.147086560516" Z="0.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.85" />
                  <Point X="-3.430102257471" Y="-3.050431848384" Z="0.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.85" />
                  <Point X="-2.561305254696" Y="-2.567025214436" Z="0.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.85" />
                  <Point X="-2.804985164784" Y="-3.004976125021" Z="0.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.85" />
                  <Point X="-3.027701085864" Y="-4.071843033646" Z="0.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.85" />
                  <Point X="-2.598366605909" Y="-4.358368736094" Z="0.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.85" />
                  <Point X="-2.375745007594" Y="-4.351313927576" Z="0.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.85" />
                  <Point X="-2.05471227956" Y="-4.041852694068" Z="0.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.85" />
                  <Point X="-1.762424312759" Y="-3.997575303746" Z="0.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.85" />
                  <Point X="-1.503582350718" Y="-4.140381574765" Z="0.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.85" />
                  <Point X="-1.385164046266" Y="-4.411250175723" Z="0.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.85" />
                  <Point X="-1.38103943237" Y="-4.63598637878" Z="0.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.85" />
                  <Point X="-1.216503479886" Y="-4.93008558148" Z="0.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.85" />
                  <Point X="-0.918122552761" Y="-4.994264383855" Z="0.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="-0.683414990336" Y="-4.512723562155" Z="0.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="-0.30823171002" Y="-3.361933676173" Z="0.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="-0.084912199497" Y="-3.23059298161" Z="0.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.85" />
                  <Point X="0.168446879864" Y="-3.256519063678" Z="0.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.85" />
                  <Point X="0.362214149412" Y="-3.439712079595" Z="0.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.85" />
                  <Point X="0.551339870065" Y="-4.01981246766" Z="0.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.85" />
                  <Point X="0.93756929113" Y="-4.991981276846" Z="0.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.85" />
                  <Point X="1.117047549347" Y="-4.954908449519" Z="0.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.85" />
                  <Point X="1.103419060024" Y="-4.382450251299" Z="0.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.85" />
                  <Point X="0.993124408871" Y="-3.108303953805" Z="0.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.85" />
                  <Point X="1.130822980883" Y="-2.925830182279" Z="0.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.85" />
                  <Point X="1.346112455361" Y="-2.861415078711" Z="0.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.85" />
                  <Point X="1.565926497834" Y="-2.945324051681" Z="0.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.85" />
                  <Point X="1.980775247744" Y="-3.438800596198" Z="0.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.85" />
                  <Point X="2.791844125008" Y="-4.242635228706" Z="0.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.85" />
                  <Point X="2.98308996308" Y="-4.110416290426" Z="0.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.85" />
                  <Point X="2.786682304073" Y="-3.615075882917" Z="0.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.85" />
                  <Point X="2.245290435516" Y="-2.57862995786" Z="0.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.85" />
                  <Point X="2.29502594067" Y="-2.386854872922" Z="0.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.85" />
                  <Point X="2.446043505451" Y="-2.263875369297" Z="0.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.85" />
                  <Point X="2.649876845653" Y="-2.258157573975" Z="0.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.85" />
                  <Point X="3.17233771419" Y="-2.531067196201" Z="0.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.85" />
                  <Point X="4.181203378329" Y="-2.881566972815" Z="0.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.85" />
                  <Point X="4.345757404207" Y="-2.626838861257" Z="0.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.85" />
                  <Point X="3.994866778036" Y="-2.230084468654" Z="0.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.85" />
                  <Point X="3.12593805044" Y="-1.510682593318" Z="0.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.85" />
                  <Point X="3.102719967264" Y="-1.344658770871" Z="0.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.85" />
                  <Point X="3.180955277946" Y="-1.199619406464" Z="0.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.85" />
                  <Point X="3.338449359837" Y="-1.129146532404" Z="0.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.85" />
                  <Point X="3.904601299162" Y="-1.18244461594" Z="0.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.85" />
                  <Point X="4.963141487137" Y="-1.068423666151" Z="0.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.85" />
                  <Point X="5.029053671714" Y="-0.69492855952" Z="0.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.85" />
                  <Point X="4.612304821916" Y="-0.452413079377" Z="0.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.85" />
                  <Point X="3.686447035985" Y="-0.185259260811" Z="0.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.85" />
                  <Point X="3.618539316485" Y="-0.120314605928" Z="0.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.85" />
                  <Point X="3.587635590974" Y="-0.032378601655" Z="0.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.85" />
                  <Point X="3.59373585945" Y="0.064231929569" Z="0.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.85" />
                  <Point X="3.636840121915" Y="0.143634132647" Z="0.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.85" />
                  <Point X="3.716948378367" Y="0.202889583431" Z="0.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.85" />
                  <Point X="4.183662995391" Y="0.337558853392" Z="0.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.85" />
                  <Point X="5.004199732881" Y="0.850580359781" Z="0.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.85" />
                  <Point X="4.914744911502" Y="1.26916795898" Z="0.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.85" />
                  <Point X="4.405661490894" Y="1.346111876508" Z="0.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.85" />
                  <Point X="3.400518202178" Y="1.230297860237" Z="0.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.85" />
                  <Point X="3.322658789508" Y="1.260532498886" Z="0.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.85" />
                  <Point X="3.267367277928" Y="1.322235563184" Z="0.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.85" />
                  <Point X="3.239513709731" Y="1.403649742164" Z="0.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.85" />
                  <Point X="3.247902326301" Y="1.483519384148" Z="0.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.85" />
                  <Point X="3.293532713641" Y="1.559431351253" Z="0.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.85" />
                  <Point X="3.693092009895" Y="1.876428093179" Z="0.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.85" />
                  <Point X="4.308272287284" Y="2.684925720953" Z="0.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.85" />
                  <Point X="4.081329672193" Y="3.01873929263" Z="0.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.85" />
                  <Point X="3.502095373941" Y="2.839855749216" Z="0.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.85" />
                  <Point X="2.456499249959" Y="2.252724616044" Z="0.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.85" />
                  <Point X="2.383434232177" Y="2.251094905158" Z="0.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.85" />
                  <Point X="2.318075714535" Y="2.28246103712" Z="0.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.85" />
                  <Point X="2.268297563115" Y="2.338949145847" Z="0.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.85" />
                  <Point X="2.248334797601" Y="2.406324211655" Z="0.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.85" />
                  <Point X="2.259803326613" Y="2.482970325923" Z="0.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.85" />
                  <Point X="2.555769603643" Y="3.010043708014" Z="0.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.85" />
                  <Point X="2.879220615519" Y="4.179624908539" Z="0.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.85" />
                  <Point X="2.48906192472" Y="4.423092977446" Z="0.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.85" />
                  <Point X="2.082021263578" Y="4.628773126515" Z="0.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.85" />
                  <Point X="1.659943201527" Y="4.79634727604" Z="0.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.85" />
                  <Point X="1.08180389729" Y="4.953295411807" Z="0.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.85" />
                  <Point X="0.417562287376" Y="5.052830975473" Z="0.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.85" />
                  <Point X="0.128479482663" Y="4.834616455071" Z="0.85" />
                  <Point X="0" Y="4.355124473572" Z="0.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>