<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#189" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2682" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.84737890625" Y="20.427287109375" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.54236315918" Y="21.532625" />
                  <Point X="0.426253845215" Y="21.699916015625" />
                  <Point X="0.378635528564" Y="21.768525390625" />
                  <Point X="0.35675189209" Y="21.79098046875" />
                  <Point X="0.330496246338" Y="21.810224609375" />
                  <Point X="0.302495025635" Y="21.82433203125" />
                  <Point X="0.122822387695" Y="21.880095703125" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976488113" Y="21.907234375" />
                  <Point X="-0.008664384842" Y="21.907234375" />
                  <Point X="-0.036823783875" Y="21.90296484375" />
                  <Point X="-0.216496414185" Y="21.847201171875" />
                  <Point X="-0.290182922363" Y="21.82433203125" />
                  <Point X="-0.318184753418" Y="21.81022265625" />
                  <Point X="-0.344439941406" Y="21.790978515625" />
                  <Point X="-0.366323425293" Y="21.7685234375" />
                  <Point X="-0.482432769775" Y="21.60123046875" />
                  <Point X="-0.530051086426" Y="21.532623046875" />
                  <Point X="-0.538188293457" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.667494384766" Y="21.05267578125" />
                  <Point X="-0.916584838867" Y="20.12305859375" />
                  <Point X="-0.996442626953" Y="20.13855859375" />
                  <Point X="-1.079320678711" Y="20.154646484375" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.259432373047" Y="20.69956640625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.2879375" Y="20.81703125" />
                  <Point X="-1.304009277344" Y="20.844869140625" />
                  <Point X="-1.32364453125" Y="20.868796875" />
                  <Point X="-1.489064208984" Y="21.013865234375" />
                  <Point X="-1.556905517578" Y="21.073361328125" />
                  <Point X="-1.583189697266" Y="21.089705078125" />
                  <Point X="-1.612887939453" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.862576416016" Y="21.123423828125" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983415771484" Y="21.126291015625" />
                  <Point X="-2.014463745117" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.225597167969" Y="20.982962890625" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.400510986328" Y="20.815294921875" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.680204833984" Y="20.835380859375" />
                  <Point X="-2.8017265625" Y="20.910623046875" />
                  <Point X="-3.086224853516" Y="21.129677734375" />
                  <Point X="-3.104722412109" Y="21.143919921875" />
                  <Point X="-2.951685791016" Y="21.40898828125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405576171875" Y="22.4148125" />
                  <Point X="-2.414562255859" Y="22.4444296875" />
                  <Point X="-2.428781494141" Y="22.473259765625" />
                  <Point X="-2.446808837891" Y="22.498416015625" />
                  <Point X="-2.464157470703" Y="22.515763671875" />
                  <Point X="-2.489317626953" Y="22.533791015625" />
                  <Point X="-2.518145996094" Y="22.548005859375" />
                  <Point X="-2.547762695312" Y="22.55698828125" />
                  <Point X="-2.5786953125" Y="22.555974609375" />
                  <Point X="-2.610219726562" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.538802734375" />
                  <Point X="-3.014190673828" Y="22.32229296875" />
                  <Point X="-3.818024169922" Y="21.85819921875" />
                  <Point X="-3.986864990234" Y="22.080021484375" />
                  <Point X="-4.082862060547" Y="22.206142578125" />
                  <Point X="-4.286833984375" Y="22.548171875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.029651611328" Y="22.79270703125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576416016" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.580166015625" />
                  <Point X="-3.048392333984" Y="23.60126171875" />
                  <Point X="-3.046151611328" Y="23.6099140625" />
                  <Point X="-3.043347412109" Y="23.640345703125" />
                  <Point X="-3.045556884766" Y="23.672015625" />
                  <Point X="-3.05255859375" Y="23.70176171875" />
                  <Point X="-3.068641601562" Y="23.727744140625" />
                  <Point X="-3.089473876953" Y="23.75169921875" />
                  <Point X="-3.112974365234" Y="23.771232421875" />
                  <Point X="-3.131754882812" Y="23.78228515625" />
                  <Point X="-3.13945703125" Y="23.786818359375" />
                  <Point X="-3.168716064453" Y="23.798041015625" />
                  <Point X="-3.200604003906" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.70533984375" Y="23.743291015625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.796532226562" Y="23.860357421875" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.888043945312" Y="24.384671875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.58510546875" Y="24.497646484375" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517496582031" Y="24.785169921875" />
                  <Point X="-3.487730224609" Y="24.80052734375" />
                  <Point X="-3.468048828125" Y="24.8141875" />
                  <Point X="-3.459977294922" Y="24.8197890625" />
                  <Point X="-3.437524169922" Y="24.841671875" />
                  <Point X="-3.418278564453" Y="24.867927734375" />
                  <Point X="-3.404168457031" Y="24.895931640625" />
                  <Point X="-3.397607910156" Y="24.9170703125" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390648193359" Y="24.953892578125" />
                  <Point X="-3.390647216797" Y="24.983533203125" />
                  <Point X="-3.394916503906" Y="25.0116953125" />
                  <Point X="-3.401477050781" Y="25.032833984375" />
                  <Point X="-3.404167480469" Y="25.04150390625" />
                  <Point X="-3.418275634766" Y="25.069505859375" />
                  <Point X="-3.437522705078" Y="25.095765625" />
                  <Point X="-3.459982910156" Y="25.11765234375" />
                  <Point X="-3.479664306641" Y="25.131310546875" />
                  <Point X="-3.488528564453" Y="25.13676953125" />
                  <Point X="-3.512165771484" Y="25.149591796875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.964412597656" Y="25.273478515625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.848684082031" Y="25.813458984375" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.71585546875" Y="26.377859375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.519930175781" Y="26.39909375" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723423339844" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.659577148438" Y="26.318998046875" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.622777832031" Y="26.332962890625" />
                  <Point X="-3.604033447266" Y="26.34378515625" />
                  <Point X="-3.587351806641" Y="26.356015625" />
                  <Point X="-3.573713378906" Y="26.371568359375" />
                  <Point X="-3.561299072266" Y="26.389298828125" />
                  <Point X="-3.5513515625" Y="26.407431640625" />
                  <Point X="-3.533872558594" Y="26.44962890625" />
                  <Point X="-3.526704101563" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.486794921875" />
                  <Point X="-3.517157470703" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.528748046875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.58937890625" />
                  <Point X="-3.553140380859" Y="26.629892578125" />
                  <Point X="-3.561789794922" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.849666259766" Y="26.88452734375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.175169921875" Y="27.572587890625" />
                  <Point X="-4.08115625" Y="27.733654296875" />
                  <Point X="-3.793391113281" Y="28.103537109375" />
                  <Point X="-3.750504882813" Y="28.158662109375" />
                  <Point X="-3.669904785156" Y="28.112126953125" />
                  <Point X="-3.206656738281" Y="27.844669921875" />
                  <Point X="-3.1877265625" Y="27.836341796875" />
                  <Point X="-3.167082275391" Y="27.82983203125" />
                  <Point X="-3.146793701172" Y="27.825794921875" />
                  <Point X="-3.086125244141" Y="27.820486328125" />
                  <Point X="-3.061244384766" Y="27.818310546875" />
                  <Point X="-3.040557861328" Y="27.81876171875" />
                  <Point X="-3.019100585938" Y="27.821587890625" />
                  <Point X="-2.999010986328" Y="27.826505859375" />
                  <Point X="-2.980461425781" Y="27.835654296875" />
                  <Point X="-2.962208251953" Y="27.847283203125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.903015136719" Y="27.903291015625" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848743652344" Y="28.096787109375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.979483398438" Y="28.371517578125" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.8643671875" Y="28.969142578125" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.247409912109" Y="29.346478515625" />
                  <Point X="-2.167036621094" Y="29.3911328125" />
                  <Point X="-2.043196044922" Y="29.2297421875" />
                  <Point X="-2.028891479492" Y="29.21480078125" />
                  <Point X="-2.012311645508" Y="29.200888671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.927591186523" Y="29.154244140625" />
                  <Point X="-1.899898681641" Y="29.139828125" />
                  <Point X="-1.880625366211" Y="29.13233203125" />
                  <Point X="-1.859718994141" Y="29.126728515625" />
                  <Point X="-1.839268920898" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797307006836" Y="29.128693359375" />
                  <Point X="-1.777453125" Y="29.134482421875" />
                  <Point X="-1.707122924805" Y="29.163615234375" />
                  <Point X="-1.678279174805" Y="29.1755625" />
                  <Point X="-1.660147216797" Y="29.185509765625" />
                  <Point X="-1.642417236328" Y="29.197923828125" />
                  <Point X="-1.626864746094" Y="29.2115625" />
                  <Point X="-1.614633056641" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.572588867188" Y="29.3385234375" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.570200805664" Y="29.525546875" />
                  <Point X="-1.584202026367" Y="29.631896484375" />
                  <Point X="-1.161609375" Y="29.750376953125" />
                  <Point X="-0.949623962402" Y="29.80980859375" />
                  <Point X="-0.400204864502" Y="29.874111328125" />
                  <Point X="-0.29471105957" Y="29.886458984375" />
                  <Point X="-0.270944854736" Y="29.79776171875" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113998413" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.202418243408" Y="29.49606640625" />
                  <Point X="0.307419616699" Y="29.887939453125" />
                  <Point X="0.658949157715" Y="29.851123046875" />
                  <Point X="0.844031066895" Y="29.831740234375" />
                  <Point X="1.298603027344" Y="29.7219921875" />
                  <Point X="1.481039794922" Y="29.6779453125" />
                  <Point X="1.776472900391" Y="29.570791015625" />
                  <Point X="1.894646118164" Y="29.527927734375" />
                  <Point X="2.180745117188" Y="29.39412890625" />
                  <Point X="2.294558105469" Y="29.34090234375" />
                  <Point X="2.570984375" Y="29.17985546875" />
                  <Point X="2.6809921875" Y="29.115765625" />
                  <Point X="2.941646972656" Y="28.93040234375" />
                  <Point X="2.943260986328" Y="28.92925390625" />
                  <Point X="2.758614257812" Y="28.6094375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.1178515625" Y="27.459119140625" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.113664794922" Y="27.33171875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121443115234" Y="27.2896015625" />
                  <Point X="2.129709716797" Y="27.267513671875" />
                  <Point X="2.140071044922" Y="27.24747265625" />
                  <Point X="2.170534912109" Y="27.202576171875" />
                  <Point X="2.183028808594" Y="27.1841640625" />
                  <Point X="2.194465332031" Y="27.170328125" />
                  <Point X="2.221596923828" Y="27.14559375" />
                  <Point X="2.266492919922" Y="27.11512890625" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304944335938" Y="27.092275390625" />
                  <Point X="2.327031494141" Y="27.0840078125" />
                  <Point X="2.348964111328" Y="27.078662109375" />
                  <Point X="2.398197509766" Y="27.0727265625" />
                  <Point X="2.418388916016" Y="27.070291015625" />
                  <Point X="2.436466308594" Y="27.06984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.530144042969" Y="27.089396484375" />
                  <Point X="2.553494384766" Y="27.095640625" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.11014453125" />
                  <Point X="3.022576416016" Y="27.36073828125" />
                  <Point X="3.967326416016" Y="27.906189453125" />
                  <Point X="4.058026611328" Y="27.78013671875" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="4.035266357422" Y="27.285751953125" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221423339844" Y="26.660240234375" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.162996826172" Y="26.588169921875" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136605957031" Y="26.550912109375" />
                  <Point X="3.121629638672" Y="26.5170859375" />
                  <Point X="3.106365722656" Y="26.462505859375" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.11026953125" Y="26.311041015625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679443359" Y="26.268978515625" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.170363525391" Y="26.1839375" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198895019531" Y="26.145447265625" />
                  <Point X="3.216137451172" Y="26.129359375" />
                  <Point X="3.234345947266" Y="26.11603515625" />
                  <Point X="3.283733398438" Y="26.088234375" />
                  <Point X="3.303988037109" Y="26.07683203125" />
                  <Point X="3.320521484375" Y="26.0695" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.422895263672" Y="26.05061328125" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462698242188" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="3.900515136719" Y="26.101265625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.817913574219" Y="26.047916015625" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.638534179688" Y="25.576626953125" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704785644531" Y="25.325583984375" />
                  <Point X="3.681547851562" Y="25.31506640625" />
                  <Point X="3.615943359375" Y="25.277146484375" />
                  <Point X="3.589037841797" Y="25.261595703125" />
                  <Point X="3.574317382812" Y="25.2511015625" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.508167724609" Y="25.17541796875" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.450559814453" Y="25.024091796875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.458300048828" Y="24.87293359375" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.531386962891" Y="24.73243359375" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559997802734" Y="24.698763671875" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.654639892578" Y="24.637923828125" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692710205078" Y="24.616859375" />
                  <Point X="3.716580322266" Y="24.60784765625" />
                  <Point X="4.094688476562" Y="24.50653515625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.870668945313" Y="24.155052734375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.49358984375" Y="23.855794921875" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.245900878906" Y="23.96650390625" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163975097656" Y="23.943404296875" />
                  <Point X="3.136147949219" Y="23.92651171875" />
                  <Point X="3.112397460938" Y="23.9060390625" />
                  <Point X="3.034571044922" Y="23.8124375" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.958603271484" Y="23.57341796875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.047707519531" Y="23.32116796875" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.461515136719" Y="22.96984375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.168916015625" Y="22.321583984375" />
                  <Point X="4.124810546875" Y="22.250212890625" />
                  <Point X="4.028981201172" Y="22.1140546875" />
                  <Point X="3.753228271484" Y="22.273259765625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786128417969" Y="22.82998828125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.600981445312" Y="22.867849609375" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506783447266" Y="22.879603515625" />
                  <Point X="2.474610839844" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.317526123047" Y="22.797822265625" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242385009766" Y="22.753451171875" />
                  <Point X="2.221425537109" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.137531005859" Y="22.58225390625" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.123351074219" Y="22.283498046875" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.377299560547" Y="21.78337890625" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.834188232422" Y="20.925740234375" />
                  <Point X="2.781831054688" Y="20.88834375" />
                  <Point X="2.701764892578" Y="20.836517578125" />
                  <Point X="2.48521875" Y="21.1187265625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506347656" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.570784912109" Y="22.196611328125" />
                  <Point X="1.508800292969" Y="22.2364609375" />
                  <Point X="1.479987060547" Y="22.24883203125" />
                  <Point X="1.448367797852" Y="22.2565625" />
                  <Point X="1.417100830078" Y="22.258880859375" />
                  <Point X="1.2518046875" Y="22.243669921875" />
                  <Point X="1.184014038086" Y="22.23743359375" />
                  <Point X="1.156365356445" Y="22.230603515625" />
                  <Point X="1.128978271484" Y="22.219259765625" />
                  <Point X="1.104594970703" Y="22.204537109375" />
                  <Point X="0.976957641602" Y="22.09841015625" />
                  <Point X="0.924611328125" Y="22.054884765625" />
                  <Point X="0.904140441895" Y="22.031134765625" />
                  <Point X="0.887248596191" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.837461364746" Y="21.798611328125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.883641601562" Y="21.191515625" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="0.975710266113" Y="20.129921875" />
                  <Point X="0.929315551758" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058412475586" Y="20.247361328125" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.1662578125" Y="20.718099609375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.18812487793" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.850494140625" />
                  <Point X="-1.205664428711" Y="20.86453125" />
                  <Point X="-1.221736328125" Y="20.892369140625" />
                  <Point X="-1.230570678711" Y="20.9051328125" />
                  <Point X="-1.250205932617" Y="20.929060546875" />
                  <Point X="-1.261006958008" Y="20.94022265625" />
                  <Point X="-1.426426513672" Y="21.085291015625" />
                  <Point X="-1.494267822266" Y="21.144787109375" />
                  <Point X="-1.506740844727" Y="21.154037109375" />
                  <Point X="-1.533025024414" Y="21.170380859375" />
                  <Point X="-1.546836425781" Y="21.177474609375" />
                  <Point X="-1.576534667969" Y="21.189775390625" />
                  <Point X="-1.59131640625" Y="21.1945234375" />
                  <Point X="-1.621456176758" Y="21.20155078125" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.856362792969" Y="21.218220703125" />
                  <Point X="-1.946403198242" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.99272668457" Y="21.220833984375" />
                  <Point X="-2.008001098633" Y="21.2180546875" />
                  <Point X="-2.039049072266" Y="21.209736328125" />
                  <Point X="-2.053668212891" Y="21.204505859375" />
                  <Point X="-2.081862060547" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.278376464844" Y="21.061953125" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359685302734" Y="21.0072421875" />
                  <Point X="-2.380448730469" Y="20.98986328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.475879638672" Y="20.873126953125" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.630194091797" Y="20.91615234375" />
                  <Point X="-2.747611816406" Y="20.988853515625" />
                  <Point X="-2.980863525391" Y="21.16844921875" />
                  <Point X="-2.869413330078" Y="21.36148828125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.38076953125" />
                  <Point X="-2.310626953125" Y="22.41170703125" />
                  <Point X="-2.314668457031" Y="22.44239453125" />
                  <Point X="-2.323654541016" Y="22.47201171875" />
                  <Point X="-2.329361572266" Y="22.486451171875" />
                  <Point X="-2.343580810547" Y="22.51528125" />
                  <Point X="-2.351562011719" Y="22.528595703125" />
                  <Point X="-2.369589355469" Y="22.553751953125" />
                  <Point X="-2.379635498047" Y="22.56559375" />
                  <Point X="-2.396984130859" Y="22.58294140625" />
                  <Point X="-2.408826416016" Y="22.592986328125" />
                  <Point X="-2.433986572266" Y="22.611013671875" />
                  <Point X="-2.447304443359" Y="22.61899609375" />
                  <Point X="-2.4761328125" Y="22.6332109375" />
                  <Point X="-2.490573730469" Y="22.638916015625" />
                  <Point X="-2.520190429688" Y="22.6478984375" />
                  <Point X="-2.550874267578" Y="22.6519375" />
                  <Point X="-2.581806884766" Y="22.650923828125" />
                  <Point X="-2.597231445312" Y="22.6491484375" />
                  <Point X="-2.628755859375" Y="22.642876953125" />
                  <Point X="-2.643681152344" Y="22.638615234375" />
                  <Point X="-2.672645019531" Y="22.62771484375" />
                  <Point X="-2.68668359375" Y="22.621076171875" />
                  <Point X="-3.061690673828" Y="22.40456640625" />
                  <Point X="-3.793089355469" Y="21.98229296875" />
                  <Point X="-3.911271728516" Y="22.137560546875" />
                  <Point X="-4.004021484375" Y="22.259416015625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.971819335938" Y="22.717337890625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036480712891" Y="23.436689453125" />
                  <Point X="-3.015102539062" Y="23.459611328125" />
                  <Point X="-3.005365966797" Y="23.471958984375" />
                  <Point X="-2.987400634766" Y="23.499091796875" />
                  <Point X="-2.979834716797" Y="23.512875" />
                  <Point X="-2.967079833984" Y="23.541501953125" />
                  <Point X="-2.961890869141" Y="23.556345703125" />
                  <Point X="-2.956427001953" Y="23.57744140625" />
                  <Point X="-2.951552490234" Y="23.601197265625" />
                  <Point X="-2.948748291016" Y="23.63162890625" />
                  <Point X="-2.948577880859" Y="23.64695703125" />
                  <Point X="-2.950787353516" Y="23.678626953125" />
                  <Point X="-2.953083984375" Y="23.69378125" />
                  <Point X="-2.960085693359" Y="23.72352734375" />
                  <Point X="-2.971781494141" Y="23.75176171875" />
                  <Point X="-2.987864501953" Y="23.777744140625" />
                  <Point X="-2.996956787109" Y="23.790083984375" />
                  <Point X="-3.0177890625" Y="23.8140390625" />
                  <Point X="-3.028749023438" Y="23.8247578125" />
                  <Point X="-3.052249511719" Y="23.844291015625" />
                  <Point X="-3.064790039063" Y="23.85310546875" />
                  <Point X="-3.083570556641" Y="23.864158203125" />
                  <Point X="-3.105435302734" Y="23.875517578125" />
                  <Point X="-3.134694335938" Y="23.886740234375" />
                  <Point X="-3.149790771484" Y="23.89113671875" />
                  <Point X="-3.181678710938" Y="23.897619140625" />
                  <Point X="-3.197294921875" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-3.717739501953" Y="23.837478515625" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.704487304688" Y="23.883869140625" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.560517089844" Y="24.4058828125" />
                  <Point X="-3.508287353516" Y="24.687826171875" />
                  <Point X="-3.500473388672" Y="24.690287109375" />
                  <Point X="-3.473938720703" Y="24.700744140625" />
                  <Point X="-3.444172363281" Y="24.7161015625" />
                  <Point X="-3.433562744141" Y="24.722482421875" />
                  <Point X="-3.413881347656" Y="24.736142578125" />
                  <Point X="-3.393671630859" Y="24.751755859375" />
                  <Point X="-3.371218505859" Y="24.773638671875" />
                  <Point X="-3.360903564453" Y="24.785509765625" />
                  <Point X="-3.341657958984" Y="24.811765625" />
                  <Point X="-3.333439453125" Y="24.8251796875" />
                  <Point X="-3.319329345703" Y="24.85318359375" />
                  <Point X="-3.313437744141" Y="24.8677734375" />
                  <Point X="-3.306877197266" Y="24.888912109375" />
                  <Point X="-3.300991210938" Y="24.91149609375" />
                  <Point X="-3.296721923828" Y="24.939650390625" />
                  <Point X="-3.295648193359" Y="24.953888671875" />
                  <Point X="-3.295647216797" Y="24.983529296875" />
                  <Point X="-3.296720458984" Y="24.997771484375" />
                  <Point X="-3.300989746094" Y="25.02593359375" />
                  <Point X="-3.304185791016" Y="25.039853515625" />
                  <Point X="-3.310746337891" Y="25.0609921875" />
                  <Point X="-3.319327148438" Y="25.084248046875" />
                  <Point X="-3.333435302734" Y="25.11225" />
                  <Point X="-3.341653076172" Y="25.125666015625" />
                  <Point X="-3.360900146484" Y="25.15192578125" />
                  <Point X="-3.371221679688" Y="25.1638046875" />
                  <Point X="-3.393681884766" Y="25.18569140625" />
                  <Point X="-3.405820556641" Y="25.19569921875" />
                  <Point X="-3.425501953125" Y="25.209357421875" />
                  <Point X="-3.44323046875" Y="25.220275390625" />
                  <Point X="-3.466867675781" Y="25.23309765625" />
                  <Point X="-3.47698046875" Y="25.2378359375" />
                  <Point X="-3.497690917969" Y="25.24609375" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.939824951172" Y="25.3652421875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.754707519531" Y="25.799552734375" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.532330078125" Y="26.30490625" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736703857422" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704888916016" Y="26.2080546875" />
                  <Point X="-3.684603759766" Y="26.21208984375" />
                  <Point X="-3.674571777344" Y="26.21466015625" />
                  <Point X="-3.631010742188" Y="26.22839453125" />
                  <Point X="-3.603448486328" Y="26.237677734375" />
                  <Point X="-3.584514160156" Y="26.246009765625" />
                  <Point X="-3.575277099609" Y="26.25069140625" />
                  <Point X="-3.556532714844" Y="26.261513671875" />
                  <Point X="-3.547862060547" Y="26.267169921875" />
                  <Point X="-3.531180419922" Y="26.279400390625" />
                  <Point X="-3.515924804688" Y="26.293380859375" />
                  <Point X="-3.502286376953" Y="26.30893359375" />
                  <Point X="-3.495892578125" Y="26.317080078125" />
                  <Point X="-3.483478271484" Y="26.334810546875" />
                  <Point X="-3.478009033203" Y="26.343607421875" />
                  <Point X="-3.468061523438" Y="26.361740234375" />
                  <Point X="-3.463583251953" Y="26.371076171875" />
                  <Point X="-3.446104248047" Y="26.4132734375" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.4297109375" Y="26.4602109375" />
                  <Point X="-3.427359130859" Y="26.470298828125" />
                  <Point X="-3.423600830078" Y="26.49161328125" />
                  <Point X="-3.422360839844" Y="26.501896484375" />
                  <Point X="-3.421008056641" Y="26.52253515625" />
                  <Point X="-3.42191015625" Y="26.543197265625" />
                  <Point X="-3.425056640625" Y="26.563642578125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443509033203" Y="26.62380859375" />
                  <Point X="-3.447784179688" Y="26.63324609375" />
                  <Point X="-3.468874511719" Y="26.673759765625" />
                  <Point X="-3.482800292969" Y="26.699287109375" />
                  <Point X="-3.494292480469" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521501708984" Y="26.748912109375" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.791833740234" Y="26.959896484375" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.093123535156" Y="27.52469921875" />
                  <Point X="-4.002296386719" Y="27.680306640625" />
                  <Point X="-3.726338134766" Y="28.035013671875" />
                  <Point X="-3.717405273438" Y="28.02985546875" />
                  <Point X="-3.254157226562" Y="27.7623984375" />
                  <Point X="-3.244912353516" Y="27.757712890625" />
                  <Point X="-3.225982177734" Y="27.749384765625" />
                  <Point X="-3.216296142578" Y="27.745740234375" />
                  <Point X="-3.195651855469" Y="27.73923046875" />
                  <Point X="-3.185622314453" Y="27.736658203125" />
                  <Point X="-3.165333740234" Y="27.73262109375" />
                  <Point X="-3.155074707031" Y="27.73115625" />
                  <Point X="-3.09440625" Y="27.72584765625" />
                  <Point X="-3.069525390625" Y="27.723671875" />
                  <Point X="-3.059172851562" Y="27.723333984375" />
                  <Point X="-3.038486328125" Y="27.72378515625" />
                  <Point X="-3.02815234375" Y="27.72457421875" />
                  <Point X="-3.006695068359" Y="27.727400390625" />
                  <Point X="-2.996511474609" Y="27.7293125" />
                  <Point X="-2.976421875" Y="27.73423046875" />
                  <Point X="-2.956990478516" Y="27.7413046875" />
                  <Point X="-2.938440917969" Y="27.750453125" />
                  <Point X="-2.929416748047" Y="27.755533203125" />
                  <Point X="-2.911163574219" Y="27.767162109375" />
                  <Point X="-2.902746826172" Y="27.773193359375" />
                  <Point X="-2.886616699219" Y="27.786138671875" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.835840332031" Y="27.836115234375" />
                  <Point X="-2.8181796875" Y="27.853775390625" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.754105224609" Y="28.10506640625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.897211181641" Y="28.419017578125" />
                  <Point X="-3.059386962891" Y="28.6999140625" />
                  <Point X="-2.806564941406" Y="28.89375" />
                  <Point X="-2.648368652344" Y="29.015037109375" />
                  <Point X="-2.201272460938" Y="29.26343359375" />
                  <Point X="-2.1925234375" Y="29.268294921875" />
                  <Point X="-2.118564453125" Y="29.17191015625" />
                  <Point X="-2.111817626953" Y="29.164044921875" />
                  <Point X="-2.097513183594" Y="29.149103515625" />
                  <Point X="-2.089956298828" Y="29.14202734375" />
                  <Point X="-2.073376464844" Y="29.128115234375" />
                  <Point X="-2.065096191406" Y="29.12190234375" />
                  <Point X="-2.047899291992" Y="29.11041015625" />
                  <Point X="-2.038982666016" Y="29.105130859375" />
                  <Point X="-1.971458984375" Y="29.069978515625" />
                  <Point X="-1.943766479492" Y="29.0555625" />
                  <Point X="-1.934334838867" Y="29.0512890625" />
                  <Point X="-1.915061401367" Y="29.04379296875" />
                  <Point X="-1.905219970703" Y="29.0405703125" />
                  <Point X="-1.884313720703" Y="29.034966796875" />
                  <Point X="-1.874174560547" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.83305456543" Y="29.028783203125" />
                  <Point X="-1.812407714844" Y="29.03013671875" />
                  <Point X="-1.802119995117" Y="29.031376953125" />
                  <Point X="-1.78080480957" Y="29.03513671875" />
                  <Point X="-1.770713989258" Y="29.0374921875" />
                  <Point X="-1.750860107422" Y="29.04328125" />
                  <Point X="-1.741097045898" Y="29.04671484375" />
                  <Point X="-1.670766723633" Y="29.07584765625" />
                  <Point X="-1.641923095703" Y="29.087794921875" />
                  <Point X="-1.632586181641" Y="29.0922734375" />
                  <Point X="-1.614454101562" Y="29.102220703125" />
                  <Point X="-1.605659301758" Y="29.107689453125" />
                  <Point X="-1.587929077148" Y="29.120103515625" />
                  <Point X="-1.579780639648" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252929688" Y="29.15538671875" />
                  <Point X="-1.538021240234" Y="29.172068359375" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153894043" Y="29.19948828125" />
                  <Point X="-1.516856079102" Y="29.208728515625" />
                  <Point X="-1.508525268555" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.481985717773" Y="29.30995703125" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.476013549805" Y="29.537947265625" />
                  <Point X="-1.479266235352" Y="29.562654296875" />
                  <Point X="-1.135963500977" Y="29.658904296875" />
                  <Point X="-0.931164916992" Y="29.7163203125" />
                  <Point X="-0.389161621094" Y="29.779755859375" />
                  <Point X="-0.365222229004" Y="29.78255859375" />
                  <Point X="-0.362707794189" Y="29.773173828125" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.294181213379" Y="29.471478515625" />
                  <Point X="0.378190765381" Y="29.7850078125" />
                  <Point X="0.649053771973" Y="29.756640625" />
                  <Point X="0.827852722168" Y="29.737916015625" />
                  <Point X="1.276307617188" Y="29.62964453125" />
                  <Point X="1.453624023438" Y="29.586833984375" />
                  <Point X="1.744080932617" Y="29.481484375" />
                  <Point X="1.858246459961" Y="29.44007421875" />
                  <Point X="2.140500488281" Y="29.30807421875" />
                  <Point X="2.250428466797" Y="29.2566640625" />
                  <Point X="2.523161376953" Y="29.09776953125" />
                  <Point X="2.629451416016" Y="29.035845703125" />
                  <Point X="2.817780761719" Y="28.901916015625" />
                  <Point X="2.676341796875" Y="28.6569375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.026076293945" Y="27.48366015625" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017912719727" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.019348022461" Y="27.320345703125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023801025391" Y="27.289033203125" />
                  <Point X="2.029144775391" Y="27.267107421875" />
                  <Point X="2.032470214844" Y="27.256302734375" />
                  <Point X="2.040736694336" Y="27.23421484375" />
                  <Point X="2.045320922852" Y="27.223884765625" />
                  <Point X="2.055682373047" Y="27.20384375" />
                  <Point X="2.061459472656" Y="27.1941328125" />
                  <Point X="2.091923339844" Y="27.149236328125" />
                  <Point X="2.104417236328" Y="27.13082421875" />
                  <Point X="2.109805175781" Y="27.123638671875" />
                  <Point X="2.130463378906" Y="27.100123046875" />
                  <Point X="2.157594970703" Y="27.075388671875" />
                  <Point X="2.168254638672" Y="27.066982421875" />
                  <Point X="2.213150634766" Y="27.036517578125" />
                  <Point X="2.231563232422" Y="27.0240234375" />
                  <Point X="2.241278808594" Y="27.018244140625" />
                  <Point X="2.261317626953" Y="27.007884765625" />
                  <Point X="2.271640869141" Y="27.0033046875" />
                  <Point X="2.293728027344" Y="26.995037109375" />
                  <Point X="2.304535400391" Y="26.991708984375" />
                  <Point X="2.326468017578" Y="26.98636328125" />
                  <Point X="2.337593261719" Y="26.984345703125" />
                  <Point X="2.386826660156" Y="26.97841015625" />
                  <Point X="2.407018066406" Y="26.975974609375" />
                  <Point X="2.4160390625" Y="26.9753203125" />
                  <Point X="2.447575439453" Y="26.97549609375" />
                  <Point X="2.484317138672" Y="26.979822265625" />
                  <Point X="2.497751708984" Y="26.98239453125" />
                  <Point X="2.554687744141" Y="26.99762109375" />
                  <Point X="2.578038085938" Y="27.003865234375" />
                  <Point X="2.583998291016" Y="27.005671875" />
                  <Point X="2.604408691406" Y="27.013068359375" />
                  <Point X="2.62765625" Y="27.02357421875" />
                  <Point X="2.636033935547" Y="27.02787109375" />
                  <Point X="3.070076416016" Y="27.27846484375" />
                  <Point X="3.940405029297" Y="27.78094921875" />
                  <Point X="3.9809140625" Y="27.724650390625" />
                  <Point X="4.043958007812" Y="27.63703515625" />
                  <Point X="4.136884765625" Y="27.483470703125" />
                  <Point X="3.977434082031" Y="27.36112109375" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.152116455078" Y="26.725212890625" />
                  <Point X="3.134666992188" Y="26.706599609375" />
                  <Point X="3.128576660156" Y="26.699421875" />
                  <Point X="3.087599609375" Y="26.64596484375" />
                  <Point X="3.070794433594" Y="26.624041015625" />
                  <Point X="3.065636474609" Y="26.616603515625" />
                  <Point X="3.049739013672" Y="26.58937109375" />
                  <Point X="3.034762695312" Y="26.555544921875" />
                  <Point X="3.030139892578" Y="26.542671875" />
                  <Point X="3.014875976562" Y="26.488091796875" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.017229492188" Y="26.29184375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034682861328" Y="26.228609375" />
                  <Point X="3.050286376953" Y="26.195369140625" />
                  <Point X="3.056919189453" Y="26.1835234375" />
                  <Point X="3.090999755859" Y="26.13172265625" />
                  <Point X="3.104976806641" Y="26.110478515625" />
                  <Point X="3.111739257813" Y="26.101423828125" />
                  <Point X="3.126293701172" Y="26.084177734375" />
                  <Point X="3.134085693359" Y="26.075986328125" />
                  <Point X="3.151328125" Y="26.0598984375" />
                  <Point X="3.160036376953" Y="26.052693359375" />
                  <Point X="3.178244873047" Y="26.039369140625" />
                  <Point X="3.187745117188" Y="26.03325" />
                  <Point X="3.237132568359" Y="26.00544921875" />
                  <Point X="3.257387207031" Y="25.994046875" />
                  <Point X="3.265475830078" Y="25.98998828125" />
                  <Point X="3.294680664062" Y="25.97808203125" />
                  <Point X="3.330279296875" Y="25.96801953125" />
                  <Point X="3.343674316406" Y="25.965255859375" />
                  <Point X="3.410449462891" Y="25.956431640625" />
                  <Point X="3.437834960938" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708496094" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="3.912915039063" Y="26.007078125" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.725609375" Y="26.0254453125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.613945800781" Y="25.668390625" />
                  <Point X="3.691991210938" Y="25.421353515625" />
                  <Point X="3.686026855469" Y="25.41954296875" />
                  <Point X="3.665613525391" Y="25.4121328125" />
                  <Point X="3.642375732422" Y="25.401615234375" />
                  <Point X="3.634007324219" Y="25.397314453125" />
                  <Point X="3.568402832031" Y="25.35939453125" />
                  <Point X="3.541497314453" Y="25.34384375" />
                  <Point X="3.533891357422" Y="25.338951171875" />
                  <Point X="3.508781494141" Y="25.319876953125" />
                  <Point X="3.481994384766" Y="25.2943515625" />
                  <Point X="3.472795654297" Y="25.284224609375" />
                  <Point X="3.433433105469" Y="25.23406640625" />
                  <Point X="3.417289794922" Y="25.21349609375" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130615234" Y="25.1849296875" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.357255615234" Y="25.0419609375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.364995849609" Y="24.855064453125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.816013671875" />
                  <Point X="3.380004394531" Y="24.794515625" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417289794922" Y="24.723943359375" />
                  <Point X="3.45665234375" Y="24.67378515625" />
                  <Point X="3.472795654297" Y="24.65321484375" />
                  <Point X="3.478715332031" Y="24.646369140625" />
                  <Point X="3.501138671875" Y="24.624193359375" />
                  <Point X="3.530176269531" Y="24.6012734375" />
                  <Point X="3.541494873047" Y="24.593595703125" />
                  <Point X="3.607099365234" Y="24.55567578125" />
                  <Point X="3.634004882812" Y="24.540123046875" />
                  <Point X="3.6394921875" Y="24.537185546875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683026367188" Y="24.518970703125" />
                  <Point X="3.691992919922" Y="24.516083984375" />
                  <Point X="4.070101074219" Y="24.414771484375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.77673046875" Y="24.16921484375" />
                  <Point X="4.761614257812" Y="24.068953125" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="4.505989746094" Y="23.949982421875" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481445312" Y="24.087322265625" />
                  <Point X="3.225723144531" Y="24.0593359375" />
                  <Point X="3.172917236328" Y="24.047859375" />
                  <Point X="3.157877929688" Y="24.0432578125" />
                  <Point X="3.128758056641" Y="24.031634765625" />
                  <Point X="3.114677490234" Y="24.02461328125" />
                  <Point X="3.086850341797" Y="24.007720703125" />
                  <Point X="3.074122070312" Y="23.99846875" />
                  <Point X="3.050371582031" Y="23.97799609375" />
                  <Point X="3.039349365234" Y="23.966775390625" />
                  <Point X="2.961522949219" Y="23.873173828125" />
                  <Point X="2.929605224609" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606445312" Y="23.798771484375" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.864002929688" Y="23.582123046875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.967797607422" Y="23.26979296875" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.403682617188" Y="22.894474609375" />
                  <Point X="4.087170166016" Y="22.370015625" />
                  <Point X="4.045495361328" Y="22.302578125" />
                  <Point X="4.001274902344" Y="22.239748046875" />
                  <Point X="3.800728027344" Y="22.355533203125" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841198730469" Y="22.909109375" />
                  <Point X="2.815020996094" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108398438" Y="22.933662109375" />
                  <Point X="2.617865478516" Y="22.961337890625" />
                  <Point X="2.555018066406" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508008544922" Y="22.974595703125" />
                  <Point X="2.492316894531" Y="22.97349609375" />
                  <Point X="2.460144287109" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.400589599609" Y="22.948890625" />
                  <Point X="2.273282226562" Y="22.881890625" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.208969238281" Y="22.846830078125" />
                  <Point X="2.186038818359" Y="22.8299375" />
                  <Point X="2.175210693359" Y="22.820626953125" />
                  <Point X="2.154251220703" Y="22.79966796875" />
                  <Point X="2.144939697266" Y="22.788837890625" />
                  <Point X="2.128046142578" Y="22.76590625" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.053463134766" Y="22.626498046875" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.02986340332" Y="22.266615234375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236816406" Y="22.168453125" />
                  <Point X="2.0640703125" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.29502734375" Y="21.73587890625" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.72375390625" Y="20.963916015625" />
                  <Point X="2.560587402344" Y="21.17655859375" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828657348633" Y="22.129849609375" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783252075195" Y="22.17199609375" />
                  <Point X="1.773298461914" Y="22.179353515625" />
                  <Point X="1.622159667969" Y="22.276521484375" />
                  <Point X="1.560175048828" Y="22.31637109375" />
                  <Point X="1.546280395508" Y="22.323755859375" />
                  <Point X="1.517467163086" Y="22.336126953125" />
                  <Point X="1.502548706055" Y="22.34111328125" />
                  <Point X="1.470929443359" Y="22.34884375" />
                  <Point X="1.455392456055" Y="22.351302734375" />
                  <Point X="1.424125488281" Y="22.35362109375" />
                  <Point X="1.408395507812" Y="22.35348046875" />
                  <Point X="1.243099365234" Y="22.33826953125" />
                  <Point X="1.17530871582" Y="22.332033203125" />
                  <Point X="1.161230957031" Y="22.329662109375" />
                  <Point X="1.133582275391" Y="22.32283203125" />
                  <Point X="1.120011352539" Y="22.318373046875" />
                  <Point X="1.092624267578" Y="22.307029296875" />
                  <Point X="1.079874145508" Y="22.300583984375" />
                  <Point X="1.055490844727" Y="22.285861328125" />
                  <Point X="1.043857666016" Y="22.277583984375" />
                  <Point X="0.916220336914" Y="22.17145703125" />
                  <Point X="0.863874023438" Y="22.127931640625" />
                  <Point X="0.852652587891" Y="22.116908203125" />
                  <Point X="0.83218170166" Y="22.093158203125" />
                  <Point X="0.822932312012" Y="22.080431640625" />
                  <Point X="0.806040405273" Y="22.05260546875" />
                  <Point X="0.799018859863" Y="22.038529296875" />
                  <Point X="0.78739465332" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.744628845215" Y="21.8187890625" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.789454406738" Y="21.179115234375" />
                  <Point X="0.833091064453" Y="20.847662109375" />
                  <Point X="0.655065124512" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146972656" Y="21.546416015625" />
                  <Point X="0.626788391113" Y="21.57618359375" />
                  <Point X="0.620407592773" Y="21.58679296875" />
                  <Point X="0.504298156738" Y="21.754083984375" />
                  <Point X="0.456679840088" Y="21.822693359375" />
                  <Point X="0.446670837402" Y="21.834830078125" />
                  <Point X="0.424787139893" Y="21.85728515625" />
                  <Point X="0.412912475586" Y="21.867603515625" />
                  <Point X="0.386656890869" Y="21.88684765625" />
                  <Point X="0.373240264893" Y="21.895064453125" />
                  <Point X="0.345238983154" Y="21.909171875" />
                  <Point X="0.330654449463" Y="21.9150625" />
                  <Point X="0.1509818573" Y="21.970826171875" />
                  <Point X="0.077295211792" Y="21.9936953125" />
                  <Point X="0.063376972198" Y="21.996890625" />
                  <Point X="0.035217689514" Y="22.00116015625" />
                  <Point X="0.020976495743" Y="22.002234375" />
                  <Point X="-0.008664410591" Y="22.002234375" />
                  <Point X="-0.022905605316" Y="22.00116015625" />
                  <Point X="-0.051064888" Y="21.996890625" />
                  <Point X="-0.064983123779" Y="21.9936953125" />
                  <Point X="-0.244655883789" Y="21.937931640625" />
                  <Point X="-0.318342376709" Y="21.9150625" />
                  <Point X="-0.332930908203" Y="21.909169921875" />
                  <Point X="-0.360932800293" Y="21.895060546875" />
                  <Point X="-0.374346008301" Y="21.88684375" />
                  <Point X="-0.400601135254" Y="21.867599609375" />
                  <Point X="-0.412475372314" Y="21.85728125" />
                  <Point X="-0.434358886719" Y="21.834826171875" />
                  <Point X="-0.444368041992" Y="21.822689453125" />
                  <Point X="-0.560477478027" Y="21.655396484375" />
                  <Point X="-0.60809564209" Y="21.5867890625" />
                  <Point X="-0.612470092773" Y="21.579869140625" />
                  <Point X="-0.625975402832" Y="21.55473828125" />
                  <Point X="-0.63877722168" Y="21.523787109375" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.759257324219" Y="21.077263671875" />
                  <Point X="-0.985425537109" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.361482005455" Y="29.191964133333" />
                  <Point X="0.611473497277" Y="29.760576366215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.709298409829" Y="28.979062821576" />
                  <Point X="0.366230607765" Y="29.740371700076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.789588646688" Y="28.853086030903" />
                  <Point X="0.341609155226" Y="29.648482783653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.741027308586" Y="28.768975654825" />
                  <Point X="0.316987702687" Y="29.55659386723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.363959906693" Y="29.77784715762" />
                  <Point X="-0.374953727075" Y="29.781419266399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.692465970484" Y="28.684865278747" />
                  <Point X="0.292366240555" Y="29.464704953924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.334642512611" Y="29.668432447538" />
                  <Point X="-0.600966641405" Y="29.754966402565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643904720851" Y="28.600754873924" />
                  <Point X="0.267744657885" Y="29.372816079783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.305325010648" Y="29.559017702403" />
                  <Point X="-0.826980752023" Y="29.728513927429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.595343515195" Y="28.516644454812" />
                  <Point X="0.243123075215" Y="29.280927205643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276007508684" Y="29.449602957268" />
                  <Point X="-1.020122767112" Y="29.691380660999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.546782309539" Y="28.432534035699" />
                  <Point X="0.204562432297" Y="29.193567406723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.246690006721" Y="29.340188212133" />
                  <Point X="-1.185152950992" Y="29.645113306912" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.498221103883" Y="28.348423616587" />
                  <Point X="0.127444575661" Y="29.118735605973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212285504151" Y="29.229120600307" />
                  <Point X="-1.3501809884" Y="29.598845255394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.941352075702" Y="27.779633028297" />
                  <Point X="3.939287318889" Y="27.780303908453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.449659898227" Y="28.264313197475" />
                  <Point X="-1.47627411433" Y="29.539926484265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.035158216157" Y="27.649264654333" />
                  <Point X="3.828578854864" Y="27.716386357652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.401098692571" Y="28.180202778363" />
                  <Point X="-1.462968357144" Y="29.43571427038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112139531632" Y="27.52436299739" />
                  <Point X="3.717870390838" Y="27.652468806851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.352537486915" Y="28.096092359251" />
                  <Point X="-1.472820251526" Y="29.339026433607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.075509166935" Y="27.436376013058" />
                  <Point X="3.607161926813" Y="27.58855125605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.303976281259" Y="28.011981940139" />
                  <Point X="-1.501388449468" Y="29.248419892502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.984055832301" Y="27.366202091464" />
                  <Point X="3.496453462787" Y="27.524633705249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.255415075603" Y="27.927871521026" />
                  <Point X="-1.544910854729" Y="29.162672267896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.892603055601" Y="27.296027988586" />
                  <Point X="3.385744998762" Y="27.460716154448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.206853869947" Y="27.843761101914" />
                  <Point X="-1.63383235295" Y="29.091675702784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801150322453" Y="27.225853871558" />
                  <Point X="3.275036534736" Y="27.396798603647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.15829266429" Y="27.759650682802" />
                  <Point X="-1.77291666475" Y="29.036978023822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.085994837604" Y="29.138703288643" />
                  <Point X="-2.300391864467" Y="29.208365105484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709697589305" Y="27.15567975453" />
                  <Point X="3.164328070711" Y="27.332881052846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.109731458634" Y="27.67554026369" />
                  <Point X="-2.413837923217" Y="29.145337053129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618244856156" Y="27.085505637501" />
                  <Point X="3.053619575128" Y="27.268963512298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.061534537466" Y="27.591311481373" />
                  <Point X="-2.527283981966" Y="29.082309000775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.526792123008" Y="27.015331520473" />
                  <Point X="2.942910898811" Y="27.205046030475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.030820729358" Y="27.501402091271" />
                  <Point X="-2.640730040716" Y="29.01928094842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.435339389859" Y="26.945157403444" />
                  <Point X="2.832202222494" Y="27.141128548652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013333014899" Y="27.407195282838" />
                  <Point X="-2.733713583674" Y="28.949604221649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.343886656711" Y="26.874983286416" />
                  <Point X="2.721493546176" Y="27.077211066828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.021229599308" Y="27.304740615728" />
                  <Point X="-2.825219834125" Y="28.879447493447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.252433923563" Y="26.804809169388" />
                  <Point X="2.60736553586" Y="27.014404593969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.063507261689" Y="27.191114859207" />
                  <Point X="-2.916725961386" Y="28.809290725217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.162040162174" Y="26.734290971577" />
                  <Point X="2.420157477212" Y="26.975343268215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.195940471373" Y="27.048195789643" />
                  <Point X="-3.008232088647" Y="28.739133956988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.09534712319" Y="26.656071942242" />
                  <Point X="-3.028083674586" Y="28.645695216958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725336197272" Y="26.026567476125" />
                  <Point X="4.533758940136" Y="26.088814700319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.042629032362" Y="26.573312176997" />
                  <Point X="-2.957095973606" Y="28.522741003417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.751743356474" Y="25.918098358676" />
                  <Point X="4.314979551799" Y="26.060011521417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01342727847" Y="26.482911490698" />
                  <Point X="-2.886108195258" Y="28.399786764737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.76848106303" Y="25.812771036844" />
                  <Point X="4.096200163463" Y="26.031208342514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001155630076" Y="26.387009879664" />
                  <Point X="-2.815119999613" Y="28.276832390469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.773679775963" Y="25.711192961314" />
                  <Point X="3.877420547691" Y="26.00240523751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.01942900743" Y="26.281183588143" />
                  <Point X="-2.761594117916" Y="28.159551865945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.605198741794" Y="25.666046856455" />
                  <Point X="3.658639757491" Y="25.973602514101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.06897750342" Y="26.165195394574" />
                  <Point X="-2.749797515122" Y="28.055830006046" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.436714797036" Y="25.620901697303" />
                  <Point X="3.39967959662" Y="25.957854859605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.215197509022" Y="26.017796723468" />
                  <Point X="-2.757597571604" Y="27.958475486726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.268230852278" Y="25.575756538151" />
                  <Point X="-2.802011728278" Y="27.873017609719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.09974690752" Y="25.530611379" />
                  <Point X="-2.875084522779" Y="27.796871488608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.931262962762" Y="25.485466219848" />
                  <Point X="-2.98400696754" Y="27.73237362497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.482940957134" Y="27.894487105309" />
                  <Point X="-3.764494142971" Y="27.985969280925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762779018003" Y="25.440321060696" />
                  <Point X="-3.826525982534" Y="27.90623573609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.617348355815" Y="25.387685435975" />
                  <Point X="-3.888557822098" Y="27.826502191255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.511727492571" Y="25.322114823473" />
                  <Point X="-3.950589661662" Y="27.74676864642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441936763179" Y="25.244902294765" />
                  <Point X="-4.010453984612" Y="27.666330832746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.388170495654" Y="25.162483102774" />
                  <Point X="-4.059463688414" Y="27.582366139515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.362786775815" Y="25.07084186201" />
                  <Point X="-4.108473160124" Y="27.498401370873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.34870043336" Y="24.97552988082" />
                  <Point X="-4.157482122889" Y="27.414436436865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361874930328" Y="24.871360315964" />
                  <Point X="-4.206491085655" Y="27.330471502857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.394749294181" Y="24.760789876344" />
                  <Point X="-4.099145606173" Y="27.195703930969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.782860899488" Y="24.209876163908" />
                  <Point X="4.538664003149" Y="24.289220545288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.497916575145" Y="24.62737988345" />
                  <Point X="-3.873358586867" Y="27.02245236994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.768504169546" Y="24.114652036937" />
                  <Point X="-3.647573746184" Y="26.849201516788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.75057741431" Y="24.0205878815" />
                  <Point X="-3.480471231101" Y="26.695017707045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729356432031" Y="23.927594085313" />
                  <Point X="-3.428372467781" Y="26.578200881391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.248827866175" Y="23.98383836966" />
                  <Point X="-3.426077675446" Y="26.47756634686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.731984866055" Y="24.051881928956" />
                  <Point X="-3.456724194885" Y="26.387635093344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.331115171749" Y="24.082243476956" />
                  <Point X="-3.506642784786" Y="26.303965715108" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.151504112727" Y="24.040713736391" />
                  <Point X="-3.606692796178" Y="26.236585023115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.047161194733" Y="23.974727894307" />
                  <Point X="-3.846503950965" Y="26.214615479379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980827998872" Y="23.896391944854" />
                  <Point X="-4.363347720774" Y="26.282659288763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.917633725668" Y="23.817036097604" />
                  <Point X="-4.645455880401" Y="26.274432874991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880448359597" Y="23.72922944415" />
                  <Point X="-4.670333553868" Y="26.182627209794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868699143225" Y="23.633158084662" />
                  <Point X="-4.695211227336" Y="26.090821544597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859774290299" Y="23.536169033861" />
                  <Point X="-4.720088900803" Y="25.999015879401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.874001531735" Y="23.431657411593" />
                  <Point X="-4.739061053052" Y="25.905291394044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.942198706555" Y="23.309609894964" />
                  <Point X="-4.753163861007" Y="25.809984762818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.036173103188" Y="23.179186851253" />
                  <Point X="-4.767266808926" Y="25.714678177069" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.255828351249" Y="23.007927623475" />
                  <Point X="-3.368052081124" Y="25.160156841445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.278689840431" Y="25.456040985577" />
                  <Point X="-4.781369774048" Y="25.61937159691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.481612161298" Y="22.834677105197" />
                  <Point X="-3.30410217703" Y="25.039489346729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.707396713341" Y="22.66142634583" />
                  <Point X="-3.297075696963" Y="24.937317393658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.933181265384" Y="22.488175586464" />
                  <Point X="3.104776482595" Y="22.757340616845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.457057311576" Y="22.967797333137" />
                  <Point X="-3.323020244909" Y="24.845858376993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.07082364907" Y="22.343563953665" />
                  <Point X="3.500485831193" Y="22.5288779442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.3270059963" Y="22.910164655689" />
                  <Point X="-3.380459337837" Y="24.764632558316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.016468742612" Y="22.261336022057" />
                  <Point X="3.896196162934" Y="22.300414952112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.21085919563" Y="22.848014127578" />
                  <Point X="-3.481765048623" Y="24.697659867789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.133471064209" Y="22.773270144429" />
                  <Point X="-3.64564941693" Y="24.651020215654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.086218024381" Y="22.688734676473" />
                  <Point X="-3.814133601376" Y="24.605875134381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041324049913" Y="22.603432701717" />
                  <Point X="-3.982617785821" Y="24.560730053109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.006376136395" Y="22.514899055859" />
                  <Point X="-4.151101970266" Y="24.515584971836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.002857131707" Y="22.41615353849" />
                  <Point X="-4.319586154711" Y="24.470439890563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02202139179" Y="22.310037781623" />
                  <Point X="-4.488070339156" Y="24.425294809291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.041185605668" Y="22.203922039769" />
                  <Point X="-3.116399447705" Y="23.879723008606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.154429442238" Y="23.892079702878" />
                  <Point X="-4.656553046745" Y="24.380149248158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.092005741211" Y="22.087520665463" />
                  <Point X="1.736437003312" Y="22.203051951771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.303245702057" Y="22.343804337786" />
                  <Point X="-2.96272665226" Y="23.729902779288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.416048396469" Y="23.877195942712" />
                  <Point X="-4.783021079527" Y="24.32135229165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162993867207" Y="21.964566313825" />
                  <Point X="1.88715544472" Y="22.054191650269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.099823611668" Y="22.310011270299" />
                  <Point X="-2.949299139627" Y="23.62565100466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.634830024342" Y="23.848393491479" />
                  <Point X="-4.768038326205" Y="24.21659518869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.233981993204" Y="21.841611962188" />
                  <Point X="1.98925951937" Y="21.921127114047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.001244805548" Y="22.242152554736" />
                  <Point X="-2.970957356176" Y="23.532799274499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.853610762542" Y="23.819590751174" />
                  <Point X="-4.753055572883" Y="24.111838085729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.304970030363" Y="21.718657639415" />
                  <Point X="2.091363594021" Y="21.788062577825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.914865052121" Y="22.170330126678" />
                  <Point X="-3.023947338856" Y="23.450127852272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.072390957862" Y="23.790787834477" />
                  <Point X="-4.735767880604" Y="24.006332062702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375957522093" Y="21.595703493863" />
                  <Point X="2.193467668671" Y="21.654998041603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.834975082788" Y="22.096399039943" />
                  <Point X="-3.110398894947" Y="23.378328754313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.291171153182" Y="23.761984917779" />
                  <Point X="-4.707944161739" Y="23.897402677118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.446945013823" Y="21.472749348312" />
                  <Point X="2.295571743321" Y="21.521933505381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.788299184966" Y="22.011676047182" />
                  <Point X="-3.201851625721" Y="23.308154636514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.509951348502" Y="23.733182001082" />
                  <Point X="-4.680119965861" Y="23.788473136543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.517932505553" Y="21.34979520276" />
                  <Point X="2.397675817971" Y="21.388868969159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.76638991091" Y="21.91890589055" />
                  <Point X="-3.293304356495" Y="23.237980518714" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.588919997283" Y="21.226841057208" />
                  <Point X="2.499779892621" Y="21.255804432937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746110581292" Y="21.825606132867" />
                  <Point X="-3.38475708727" Y="23.167806400914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.659907489013" Y="21.103886911656" />
                  <Point X="2.601883971069" Y="21.122739895481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726904357975" Y="21.73195770181" />
                  <Point X="0.459309458468" Y="21.818904555271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.071968635543" Y="21.991527272193" />
                  <Point X="-3.476209818044" Y="23.097632283114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730894980743" Y="20.980932766104" />
                  <Point X="2.703988055107" Y="20.989675356208" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.729952448651" Y="21.631078405811" />
                  <Point X="0.54882430104" Y="21.689930508512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.229204152471" Y="21.942727277287" />
                  <Point X="-3.567662548818" Y="23.027458165315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.743690720105" Y="21.526725659521" />
                  <Point X="0.63390077054" Y="21.562398576583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.370981708434" Y="21.888904686401" />
                  <Point X="-2.355147650659" Y="22.533599281624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.659746047596" Y="22.63256930023" />
                  <Point X="-3.659115279593" Y="22.957284047515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.757428991558" Y="21.422372913231" />
                  <Point X="0.671627873581" Y="21.45025138642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.449934550259" Y="21.814669108481" />
                  <Point X="-2.311664035084" Y="22.419581687158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.774953629438" Y="22.570113601423" />
                  <Point X="-3.750568010367" Y="22.887109929715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.771167263011" Y="21.318020166941" />
                  <Point X="0.7009454511" Y="21.340836616736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.506505078827" Y="21.733161076136" />
                  <Point X="-2.322693552004" Y="22.323276483142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.885662301802" Y="22.506196118315" />
                  <Point X="-3.842020741142" Y="22.816935811915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784905534464" Y="21.213667420651" />
                  <Point X="0.730263028619" Y="21.231421847052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.563075664606" Y="21.65165306238" />
                  <Point X="-2.364039570955" Y="22.236821707758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.996370974166" Y="22.442278635208" />
                  <Point X="-3.933473471916" Y="22.746761694116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.798643831355" Y="21.109314666096" />
                  <Point X="0.759580606138" Y="21.122007077367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.617985756152" Y="21.569605521343" />
                  <Point X="-2.412600730577" Y="22.152711273688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.107079541684" Y="22.378361118033" />
                  <Point X="-4.024926221255" Y="22.676587582348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812382140839" Y="21.004961907449" />
                  <Point X="0.788898183656" Y="21.012592307683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.651207681407" Y="21.480511067903" />
                  <Point X="-2.461161890199" Y="22.068600839619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.217787958318" Y="22.314443551834" />
                  <Point X="-4.116378984" Y="22.606413474936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826120450323" Y="20.900609148802" />
                  <Point X="0.818215761175" Y="20.903177537999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.675829197765" Y="21.388622172216" />
                  <Point X="-2.509723049821" Y="21.984490405549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.328496374951" Y="22.250525985634" />
                  <Point X="-4.159800558849" Y="22.520633088543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.700450714122" Y="21.296733276529" />
                  <Point X="-2.558284209443" Y="21.90037997148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439204791585" Y="22.186608419434" />
                  <Point X="-4.085914044408" Y="22.396736993413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.725072230479" Y="21.204844380842" />
                  <Point X="-2.606845369065" Y="21.81626953741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549913208218" Y="22.122690853235" />
                  <Point X="-4.012027529967" Y="22.272840898282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.749693746836" Y="21.112955485154" />
                  <Point X="-2.655406528688" Y="21.732159103341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.660621624852" Y="22.058773287035" />
                  <Point X="-3.913955442007" Y="22.141086433951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.77431530073" Y="21.021066601664" />
                  <Point X="-2.70396768831" Y="21.648048669271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.771330041486" Y="21.994855720836" />
                  <Point X="-3.812942974617" Y="22.008376582428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.798936878463" Y="20.929177725919" />
                  <Point X="-1.523986529539" Y="21.1647606383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.646103985897" Y="21.204439005125" />
                  <Point X="-2.752528847932" Y="21.563938235202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.823558456197" Y="20.837288850174" />
                  <Point X="-1.332018002552" Y="21.002497371523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.999946080814" Y="21.219520359817" />
                  <Point X="-2.801090007554" Y="21.479827801132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.84818003393" Y="20.74539997443" />
                  <Point X="-1.204000694125" Y="20.861013115253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.129201009617" Y="21.161628920718" />
                  <Point X="-2.849651167176" Y="21.395717367063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.872801611664" Y="20.653511098685" />
                  <Point X="-1.172799335955" Y="20.750986268132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.229784150423" Y="21.094421452972" />
                  <Point X="-2.898212129102" Y="21.311606868758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.897423189397" Y="20.56162222294" />
                  <Point X="-1.151557422451" Y="20.644195440746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330367494312" Y="21.027214051212" />
                  <Point X="-2.946772955367" Y="21.227496326374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.922044767131" Y="20.469733347195" />
                  <Point X="-1.130315496526" Y="20.537404609325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.413552626708" Y="20.954353627858" />
                  <Point X="-2.913982488467" Y="21.116953146527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.946666344864" Y="20.377844471451" />
                  <Point X="-1.120024996056" Y="20.434172111735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.474903663659" Y="20.874398876845" />
                  <Point X="-2.659772795979" Y="20.934466499162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.971287922597" Y="20.285955595706" />
                  <Point X="-1.132092069342" Y="20.338204030219" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.755615905762" Y="20.40269921875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.348209503174" Y="21.645748046875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335601807" Y="21.7336015625" />
                  <Point X="0.094662979126" Y="21.789365234375" />
                  <Point X="0.020976472855" Y="21.812234375" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.188337081909" Y="21.756470703125" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278839111" Y="21.714357421875" />
                  <Point X="-0.404388153076" Y="21.547064453125" />
                  <Point X="-0.452006500244" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.575731506348" Y="21.028087890625" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-1.014543823242" Y="20.045298828125" />
                  <Point X="-1.100231689453" Y="20.061931640625" />
                  <Point X="-1.308484130859" Y="20.115513671875" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.343346435547" Y="20.189216796875" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.352606933594" Y="20.681033203125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282348633" Y="20.79737109375" />
                  <Point X="-1.551702026367" Y="20.942439453125" />
                  <Point X="-1.619543212891" Y="21.001935546875" />
                  <Point X="-1.649241333008" Y="21.014236328125" />
                  <Point X="-1.868790039062" Y="21.028626953125" />
                  <Point X="-1.958830444336" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.172818115234" Y="20.90397265625" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.325142333984" Y="20.757462890625" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.730215820312" Y="20.754609375" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-3.144182128906" Y="21.05440625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.033958251953" Y="21.45648828125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513982177734" Y="22.43123828125" />
                  <Point X="-2.531330810547" Y="22.4485859375" />
                  <Point X="-2.560159179688" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.966690673828" Y="22.24001953125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.062458251953" Y="22.022482421875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.368426757812" Y="22.499513671875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.087483886719" Y="22.868076171875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821533203" Y="23.603986328125" />
                  <Point X="-3.140357666016" Y="23.62508203125" />
                  <Point X="-3.138116943359" Y="23.633734375" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161158691406" Y="23.689359375" />
                  <Point X="-3.179939208984" Y="23.700412109375" />
                  <Point X="-3.187641357422" Y="23.7049453125" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.692940185547" Y="23.649103515625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.888577148438" Y="23.836845703125" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.982086914062" Y="24.371220703125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.609693847656" Y="24.58941015625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541897705078" Y="24.878572265625" />
                  <Point X="-3.522216308594" Y="24.892232421875" />
                  <Point X="-3.514144775391" Y="24.897833984375" />
                  <Point X="-3.494899169922" Y="24.92408984375" />
                  <Point X="-3.488338623047" Y="24.945228515625" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.492207763672" Y="25.00467578125" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514145263672" Y="25.03960546875" />
                  <Point X="-3.533826660156" Y="25.053263671875" />
                  <Point X="-3.533826904297" Y="25.053263671875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.989000244141" Y="25.18171484375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.942660644531" Y="25.827365234375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.807548339844" Y="26.40270703125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.507530273438" Y="26.49328125" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731704589844" Y="26.3958671875" />
                  <Point X="-3.688143554688" Y="26.4096015625" />
                  <Point X="-3.670278564453" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.443787109375" />
                  <Point X="-3.621640869141" Y="26.485984375" />
                  <Point X="-3.614472412109" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.63740625" Y="26.586025390625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.907498779297" Y="26.809158203125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.257216308594" Y="27.6204765625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.868371826172" Y="28.16187109375" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.622405029297" Y="28.194400390625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138512695312" Y="27.92043359375" />
                  <Point X="-3.077844238281" Y="27.915125" />
                  <Point X="-3.052963378906" Y="27.91294921875" />
                  <Point X="-3.031506103516" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.970189941406" Y="27.970466796875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.943382080078" Y="28.0885078125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.061755615234" Y="28.324017578125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.922169433594" Y="29.04453515625" />
                  <Point X="-2.752873779297" Y="29.17433203125" />
                  <Point X="-2.293547363281" Y="29.4295234375" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.109640869141" Y="29.472388671875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246826172" Y="29.273662109375" />
                  <Point X="-1.883723266602" Y="29.238509765625" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124389648" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.743478881836" Y="29.2513828125" />
                  <Point X="-1.714635253906" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.663192016602" Y="29.36708984375" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.664388061523" Y="29.513146484375" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.187255249023" Y="29.841849609375" />
                  <Point X="-0.968083251953" Y="29.903296875" />
                  <Point X="-0.41124798584" Y="29.968466796875" />
                  <Point X="-0.224199935913" Y="29.990359375" />
                  <Point X="-0.17918196106" Y="29.822349609375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282125473" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594043732" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.110655265808" Y="29.520654296875" />
                  <Point X="0.236648422241" Y="29.990869140625" />
                  <Point X="0.668844360352" Y="29.94560546875" />
                  <Point X="0.860210021973" Y="29.925564453125" />
                  <Point X="1.3208984375" Y="29.81433984375" />
                  <Point X="1.508455322266" Y="29.769056640625" />
                  <Point X="1.808864746094" Y="29.66009765625" />
                  <Point X="1.931045166016" Y="29.61578125" />
                  <Point X="2.220989990234" Y="29.48018359375" />
                  <Point X="2.338686279297" Y="29.425140625" />
                  <Point X="2.618807373047" Y="29.26194140625" />
                  <Point X="2.732533203125" Y="29.195685546875" />
                  <Point X="2.996703369141" Y="29.007822265625" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.84088671875" Y="28.5619375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.209626708984" Y="27.434578125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.207981445312" Y="27.343091796875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682617188" Y="27.3008125" />
                  <Point X="2.249146484375" Y="27.255916015625" />
                  <Point X="2.261640380859" Y="27.23750390625" />
                  <Point X="2.274939208984" Y="27.224205078125" />
                  <Point X="2.319835205078" Y="27.193740234375" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360334960938" Y="27.172978515625" />
                  <Point X="2.409568359375" Y="27.16704296875" />
                  <Point X="2.429759765625" Y="27.164607421875" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.505600341797" Y="27.181171875" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.975076416016" Y="27.44301171875" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.135139160156" Y="27.835623046875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.349868164063" Y="27.49850390625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.093098632813" Y="27.2103828125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.238394042969" Y="26.530375" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.19785546875" Y="26.436919921875" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.203309570312" Y="26.33023828125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646728516" Y="26.287953125" />
                  <Point X="3.249727294922" Y="26.23615234375" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280946777344" Y="26.1988203125" />
                  <Point X="3.330334228516" Y="26.17101953125" />
                  <Point X="3.350588867188" Y="26.1596171875" />
                  <Point X="3.368565917969" Y="26.153619140625" />
                  <Point X="3.435341064453" Y="26.144794921875" />
                  <Point X="3.4627265625" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.888115234375" Y="26.195453125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.910217773438" Y="26.07038671875" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.985599609375" Y="25.653294921875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.663122070312" Y="25.48486328125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088378906" Y="25.232818359375" />
                  <Point X="3.663483886719" Y="25.1948984375" />
                  <Point X="3.636578369141" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.58290234375" Y="25.11676953125" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.543864013672" Y="25.00622265625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.551604248047" Y="24.890802734375" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.606121582031" Y="24.79108203125" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636575927734" Y="24.758091796875" />
                  <Point X="3.702180419922" Y="24.720171875" />
                  <Point X="3.7290859375" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.119275878906" Y="24.598298828125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.964607421875" Y="24.140890625" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.888971679688" Y="23.7730390625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.481189941406" Y="23.761607421875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836914062" Y="23.901658203125" />
                  <Point X="3.266078613281" Y="23.873671875" />
                  <Point X="3.213272705078" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.107619140625" Y="23.751701171875" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.053203613281" Y="23.564712890625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.127617431641" Y="23.37254296875" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.51934765625" Y="23.045212890625" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.249729492188" Y="22.271642578125" />
                  <Point X="4.204126953125" Y="22.197849609375" />
                  <Point X="4.081165283203" Y="22.023138671875" />
                  <Point X="4.056688476562" Y="21.988361328125" />
                  <Point X="3.705728271484" Y="22.19098828125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.746685546875" />
                  <Point X="2.584097412109" Y="22.774361328125" />
                  <Point X="2.52125" Y="22.7857109375" />
                  <Point X="2.489077392578" Y="22.78075390625" />
                  <Point X="2.361770019531" Y="22.71375390625" />
                  <Point X="2.309559326172" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.221598876953" Y="22.538009765625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.216838623047" Y="22.300380859375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.459571777344" Y="21.83087890625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.889405761719" Y="20.848435546875" />
                  <Point X="2.835291503906" Y="20.809783203125" />
                  <Point X="2.697813232422" Y="20.720794921875" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="2.409850097656" Y="21.06089453125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.51941015625" Y="22.116701171875" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425806152344" Y="22.16428125" />
                  <Point X="1.260510009766" Y="22.1490703125" />
                  <Point X="1.192719360352" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.037694946289" Y="22.02536328125" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.930293762207" Y="21.77843359375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.977828918457" Y="21.203916015625" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.045533203125" Y="20.04797265625" />
                  <Point X="0.994362976074" Y="20.036755859375" />
                  <Point X="0.867338500977" Y="20.0136796875" />
                  <Point X="0.860200622559" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#188" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.137041589218" Y="4.866570673383" Z="1.8" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="-0.4232162988" Y="5.049408199373" Z="1.8" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.8" />
                  <Point X="-1.206908977239" Y="4.921274931679" Z="1.8" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.8" />
                  <Point X="-1.720116201252" Y="4.53790205109" Z="1.8" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.8" />
                  <Point X="-1.717033944948" Y="4.41340553733" Z="1.8" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.8" />
                  <Point X="-1.76876874257" Y="4.328856590757" Z="1.8" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.8" />
                  <Point X="-1.866791580138" Y="4.314140500414" Z="1.8" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.8" />
                  <Point X="-2.076129432762" Y="4.53410731045" Z="1.8" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.8" />
                  <Point X="-2.323986649474" Y="4.504511880731" Z="1.8" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.8" />
                  <Point X="-2.958747626355" Y="4.11549574825" Z="1.8" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.8" />
                  <Point X="-3.111212876491" Y="3.330298416608" Z="1.8" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.8" />
                  <Point X="-2.999347854783" Y="3.115431857963" Z="1.8" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.8" />
                  <Point X="-3.011700760943" Y="3.037102806943" Z="1.8" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.8" />
                  <Point X="-3.079644607643" Y="2.99621684439" Z="1.8" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.8" />
                  <Point X="-3.603560931689" Y="3.268981072254" Z="1.8" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.8" />
                  <Point X="-3.913991130678" Y="3.223854573885" Z="1.8" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.8" />
                  <Point X="-4.306554375152" Y="2.676961467068" Z="1.8" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.8" />
                  <Point X="-3.944092631926" Y="1.800770811944" Z="1.8" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.8" />
                  <Point X="-3.687912792731" Y="1.594218589494" Z="1.8" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.8" />
                  <Point X="-3.673990869698" Y="1.536398243911" Z="1.8" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.8" />
                  <Point X="-3.709335005212" Y="1.488567272391" Z="1.8" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.8" />
                  <Point X="-4.507160041094" Y="1.574133300366" Z="1.8" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.8" />
                  <Point X="-4.861964107776" Y="1.447066511932" Z="1.8" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.8" />
                  <Point X="-4.998278065728" Y="0.865964161236" Z="1.8" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.8" />
                  <Point X="-4.008097212678" Y="0.164698884362" Z="1.8" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.8" />
                  <Point X="-3.568489128795" Y="0.043466875937" Z="1.8" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.8" />
                  <Point X="-3.546117277557" Y="0.021137939978" Z="1.8" />
                  <Point X="-3.539556741714" Y="0" Z="1.8" />
                  <Point X="-3.542247323578" Y="-0.008669011083" Z="1.8" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.8" />
                  <Point X="-3.556879466328" Y="-0.035409107197" Z="1.8" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.8" />
                  <Point X="-4.628790576095" Y="-0.331013172394" Z="1.8" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.8" />
                  <Point X="-5.037738979693" Y="-0.604576451236" Z="1.8" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.8" />
                  <Point X="-4.943177944904" Y="-1.144248336485" Z="1.8" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.8" />
                  <Point X="-3.692569755343" Y="-1.369189019086" Z="1.8" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.8" />
                  <Point X="-3.211456731504" Y="-1.311396462018" Z="1.8" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.8" />
                  <Point X="-3.194917070497" Y="-1.331101485065" Z="1.8" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.8" />
                  <Point X="-4.124077602256" Y="-2.060974623766" Z="1.8" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.8" />
                  <Point X="-4.417526181541" Y="-2.49481559014" Z="1.8" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.8" />
                  <Point X="-4.108400157592" Y="-2.976520808238" Z="1.8" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.8" />
                  <Point X="-2.947846574567" Y="-2.772001403908" Z="1.8" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.8" />
                  <Point X="-2.567793877436" Y="-2.560536591695" Z="1.8" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.8" />
                  <Point X="-3.08341560926" Y="-3.487231807926" Z="1.8" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.8" />
                  <Point X="-3.1808420348" Y="-3.953929603621" Z="1.8" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.8" />
                  <Point X="-2.762693328728" Y="-4.256621709849" Z="1.8" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.8" />
                  <Point X="-2.291630556161" Y="-4.241693878563" Z="1.8" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.8" />
                  <Point X="-2.151195732184" Y="-4.10632097088" Z="1.8" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.8" />
                  <Point X="-1.878215531108" Y="-3.989985938079" Z="1.8" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.8" />
                  <Point X="-1.5908255908" Y="-4.06387120243" Z="1.8" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.8" />
                  <Point X="-1.407802263808" Y="-4.297440276808" Z="1.8" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.8" />
                  <Point X="-1.399074664723" Y="-4.772977509984" Z="1.8" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.8" />
                  <Point X="-1.327098895566" Y="-4.90163034241" Z="1.8" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.8" />
                  <Point X="-1.030227700143" Y="-4.972504379755" Z="1.8" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="-0.533591273422" Y="-3.95357379849" Z="1.8" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="-0.369468449394" Y="-3.450164142838" Z="1.8" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="-0.179672486508" Y="-3.260003077717" Z="1.8" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.8" />
                  <Point X="0.073686592853" Y="-3.227108967571" Z="1.8" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.8" />
                  <Point X="0.300977410038" Y="-3.351481612929" Z="1.8" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.8" />
                  <Point X="0.701163586979" Y="-4.578962231325" Z="1.8" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.8" />
                  <Point X="0.870118512834" Y="-5.004234621558" Z="1.8" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.8" />
                  <Point X="1.050082715524" Y="-4.96958710257" Z="1.8" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.8" />
                  <Point X="1.021245109404" Y="-3.758277202398" Z="1.8" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.8" />
                  <Point X="0.972997038202" Y="-3.20090564684" Z="1.8" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.8" />
                  <Point X="1.063506228561" Y="-2.981801854397" Z="1.8" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.8" />
                  <Point X="1.258934355647" Y="-2.869437286745" Z="1.8" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.8" />
                  <Point X="1.486214973525" Y="-2.894076906603" Z="1.8" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.8" />
                  <Point X="2.364026487802" Y="-3.938263234504" Z="1.8" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.8" />
                  <Point X="2.718826185981" Y="-4.289898333562" Z="1.8" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.8" />
                  <Point X="2.912311586482" Y="-4.160971636765" Z="1.8" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.8" />
                  <Point X="2.496716976143" Y="-3.112841412697" Z="1.8" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.8" />
                  <Point X="2.259886693358" Y="-2.659451186913" Z="1.8" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.8" />
                  <Point X="2.259689274858" Y="-2.453997427147" Z="1.8" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.8" />
                  <Point X="2.378900951225" Y="-2.299212035109" Z="1.8" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.8" />
                  <Point X="2.569055616599" Y="-2.243561316133" Z="1.8" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.8" />
                  <Point X="3.67457218441" Y="-2.821032524132" Z="1.8" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.8" />
                  <Point X="4.115897506061" Y="-2.974357621963" Z="1.8" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.8" />
                  <Point X="4.286107017838" Y="-2.723362173901" Z="1.8" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.8" />
                  <Point X="3.543629592096" Y="-1.883837951537" Z="1.8" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.8" />
                  <Point X="3.16351927233" Y="-1.569137714118" Z="1.8" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.8" />
                  <Point X="3.096837091988" Y="-1.408589383272" Z="1.8" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.8" />
                  <Point X="3.139909266164" Y="-1.248985015917" Z="1.8" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.8" />
                  <Point X="3.270541439722" Y="-1.14390659977" Z="1.8" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.8" />
                  <Point X="4.468507424379" Y="-1.256684260125" Z="1.8" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.8" />
                  <Point X="4.931562720583" Y="-1.20680613053" Z="1.8" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.8" />
                  <Point X="5.007893083845" Y="-0.835282266863" Z="1.8" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.8" />
                  <Point X="4.126060996863" Y="-0.322124445112" Z="1.8" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.8" />
                  <Point X="3.721047227734" Y="-0.205258791835" Z="1.8" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.8" />
                  <Point X="3.639299431535" Y="-0.146767871028" Z="1.8" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.8" />
                  <Point X="3.594555629324" Y="-0.068512462281" Z="1.8" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.8" />
                  <Point X="3.586815821101" Y="0.028098068913" Z="1.8" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.8" />
                  <Point X="3.616080006866" Y="0.117180867547" Z="1.8" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.8" />
                  <Point X="3.682348186619" Y="0.182890052407" Z="1.8" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.8" />
                  <Point X="4.669906820444" Y="0.467847487656" Z="1.8" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.8" />
                  <Point X="5.02884820627" Y="0.692267238837" Z="1.8" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.8" />
                  <Point X="4.95264296259" Y="1.113494159906" Z="1.8" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.8" />
                  <Point X="3.875432802418" Y="1.276305923891" Z="1.8" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.8" />
                  <Point X="3.435735822964" Y="1.225643422629" Z="1.8" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.8" />
                  <Point X="3.348706019666" Y="1.245870164367" Z="1.8" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.8" />
                  <Point X="3.28534156101" Y="1.294915442964" Z="1.8" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.8" />
                  <Point X="3.246122180048" Y="1.37162172554" Z="1.8" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.8" />
                  <Point X="3.239852028991" Y="1.454733449587" Z="1.8" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.8" />
                  <Point X="3.2719212706" Y="1.531237520794" Z="1.8" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.8" />
                  <Point X="4.117380612023" Y="2.201996177078" Z="1.8" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.8" />
                  <Point X="4.386489417528" Y="2.555671089719" Z="1.8" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.8" />
                  <Point X="4.169568521056" Y="2.89610731011" Z="1.8" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.8" />
                  <Point X="2.943920531081" Y="2.517593371819" Z="1.8" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.8" />
                  <Point X="2.486527577653" Y="2.260754582069" Z="1.8" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.8" />
                  <Point X="2.409400226366" Y="2.247963887201" Z="1.8" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.8" />
                  <Point X="2.341754171595" Y="2.266394238804" Z="1.8" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.8" />
                  <Point X="2.284364361431" Y="2.315270688787" Z="1.8" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.8" />
                  <Point X="2.251465815558" Y="2.380358217466" Z="1.8" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.8" />
                  <Point X="2.251773360589" Y="2.452941998229" Z="1.8" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.8" />
                  <Point X="2.878031981041" Y="3.568218550874" Z="1.8" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.8" />
                  <Point X="3.019524676182" Y="4.07984841188" Z="1.8" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.8" />
                  <Point X="2.637821124955" Y="4.336425522934" Z="1.8" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.8" />
                  <Point X="2.236015323952" Y="4.556755254883" Z="1.8" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.8" />
                  <Point X="1.819757761149" Y="4.738381354503" Z="1.8" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.8" />
                  <Point X="1.326477118106" Y="4.894223753694" Z="1.8" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.8" />
                  <Point X="0.667896305416" Y="5.026614317936" Z="1.8" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.8" />
                  <Point X="0.056202972335" Y="4.564876832568" Z="1.8" />
                  <Point X="0" Y="4.355124473572" Z="1.8" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>