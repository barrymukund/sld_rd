<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#121" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="404" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="25.004715820312" />
                  <Width Value="9.783895507812" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.579273193359" Y="21.42787109375" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.554761169434" Y="21.508705078125" />
                  <Point X="0.541013244629" Y="21.533677734375" />
                  <Point X="0.535835510254" Y="21.54202734375" />
                  <Point X="0.378635742188" Y="21.7685234375" />
                  <Point X="0.357108215332" Y="21.79248046875" />
                  <Point X="0.326243469238" Y="21.8136015625" />
                  <Point X="0.301404327393" Y="21.824162109375" />
                  <Point X="0.292393768311" Y="21.827466796875" />
                  <Point X="0.049135974884" Y="21.90296484375" />
                  <Point X="0.02039081955" Y="21.908408203125" />
                  <Point X="-0.013893167496" Y="21.90733984375" />
                  <Point X="-0.038815330505" Y="21.901958984375" />
                  <Point X="-0.046925643921" Y="21.899830078125" />
                  <Point X="-0.290183258057" Y="21.82433203125" />
                  <Point X="-0.319509185791" Y="21.811013671875" />
                  <Point X="-0.349137908936" Y="21.7877109375" />
                  <Point X="-0.367183349609" Y="21.766486328125" />
                  <Point X="-0.372851196289" Y="21.759119140625" />
                  <Point X="-0.530051147461" Y="21.532625" />
                  <Point X="-0.538189208984" Y="21.518427734375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.916584838867" Y="20.12305859375" />
                  <Point X="-1.079319580078" Y="20.154646484375" />
                  <Point X="-1.086903442383" Y="20.15659765625" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.216885742188" Y="20.42195703125" />
                  <Point X="-1.214963134766" Y="20.43655859375" />
                  <Point X="-1.214656982422" Y="20.458755859375" />
                  <Point X="-1.217603027344" Y="20.487169921875" />
                  <Point X="-1.21892175293" Y="20.49590625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287037231445" Y="20.8182421875" />
                  <Point X="-1.306946899414" Y="20.850076171875" />
                  <Point X="-1.326581176758" Y="20.87082421875" />
                  <Point X="-1.332945068359" Y="20.876951171875" />
                  <Point X="-1.556905639648" Y="21.073359375" />
                  <Point X="-1.583204223633" Y="21.091208984375" />
                  <Point X="-1.618391357422" Y="21.104349609375" />
                  <Point X="-1.646598754883" Y="21.10885546875" />
                  <Point X="-1.655370605469" Y="21.109841796875" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.984347045898" Y="21.127474609375" />
                  <Point X="-2.020250976562" Y="21.116484375" />
                  <Point X="-2.045373657227" Y="21.102890625" />
                  <Point X="-2.052943115234" Y="21.098328125" />
                  <Point X="-2.300624267578" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480147949219" Y="20.71151171875" />
                  <Point X="-2.801719482422" Y="20.910619140625" />
                  <Point X="-2.8121796875" Y="20.918673828125" />
                  <Point X="-3.104721435547" Y="21.143919921875" />
                  <Point X="-2.45344140625" Y="22.271970703125" />
                  <Point X="-2.423760742188" Y="22.323380859375" />
                  <Point X="-2.412857421875" Y="22.352353515625" />
                  <Point X="-2.406587646484" Y="22.383884765625" />
                  <Point X="-2.405637695312" Y="22.41526953125" />
                  <Point X="-2.414969970703" Y="22.44525" />
                  <Point X="-2.429884277344" Y="22.474771484375" />
                  <Point X="-2.447504638672" Y="22.499111328125" />
                  <Point X="-2.464156005859" Y="22.51576171875" />
                  <Point X="-2.489312011719" Y="22.533787109375" />
                  <Point X="-2.518141113281" Y="22.54800390625" />
                  <Point X="-2.547759277344" Y="22.55698828125" />
                  <Point X="-2.578693603516" Y="22.555974609375" />
                  <Point X="-2.610219482422" Y="22.549703125" />
                  <Point X="-2.639184082031" Y="22.53880078125" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-4.082861083984" Y="22.206140625" />
                  <Point X="-4.090367431641" Y="22.2187265625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.157886474609" Y="23.46163671875" />
                  <Point X="-3.105954833984" Y="23.501484375" />
                  <Point X="-3.083961914062" Y="23.52534765625" />
                  <Point X="-3.065689697266" Y="23.553666015625" />
                  <Point X="-3.053549560547" Y="23.5813515625" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.043348144531" Y="23.64034765625" />
                  <Point X="-3.045559326172" Y="23.672025390625" />
                  <Point X="-3.052778808594" Y="23.7022890625" />
                  <Point X="-3.069388427734" Y="23.728595703125" />
                  <Point X="-3.091274902344" Y="23.75316796875" />
                  <Point X="-3.114029296875" Y="23.77185546875" />
                  <Point X="-3.139456054688" Y="23.7868203125" />
                  <Point X="-3.168721191406" Y="23.798044921875" />
                  <Point X="-3.200607666016" Y="23.804525390625" />
                  <Point X="-3.231928222656" Y="23.805615234375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.834076660156" Y="24.007341796875" />
                  <Point X="-4.8360625" Y="24.021228515625" />
                  <Point X="-4.892423339844" Y="24.415298828125" />
                  <Point X="-3.592033691406" Y="24.76373828125" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.516418457031" Y="24.785677734375" />
                  <Point X="-3.486622070312" Y="24.801296875" />
                  <Point X="-3.459975585938" Y="24.819791015625" />
                  <Point X="-3.4368515625" Y="24.842599609375" />
                  <Point X="-3.417239013672" Y="24.87004296875" />
                  <Point X="-3.403800048828" Y="24.897119140625" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.390655029297" Y="24.95503125" />
                  <Point X="-3.3910234375" Y="24.98586328125" />
                  <Point X="-3.395285888672" Y="25.012888671875" />
                  <Point X="-3.404168212891" Y="25.0415078125" />
                  <Point X="-3.419010742188" Y="25.07049609375" />
                  <Point X="-3.439361083984" Y="25.09751953125" />
                  <Point X="-3.46108203125" Y="25.118416015625" />
                  <Point X="-3.487728515625" Y="25.13691015625" />
                  <Point X="-3.501923095703" Y="25.145046875" />
                  <Point X="-3.532875732422" Y="25.157849609375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.820488769531" Y="25.991732421875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.808071533203" Y="26.305375" />
                  <Point X="-3.765665527344" Y="26.29979296875" />
                  <Point X="-3.744996337891" Y="26.299341796875" />
                  <Point X="-3.723447509766" Y="26.301224609375" />
                  <Point X="-3.703167480469" Y="26.30525390625" />
                  <Point X="-3.700688232422" Y="26.30603515625" />
                  <Point X="-3.641711181641" Y="26.324630859375" />
                  <Point X="-3.622774658203" Y="26.33296484375" />
                  <Point X="-3.604028564453" Y="26.3437890625" />
                  <Point X="-3.587347167969" Y="26.356021484375" />
                  <Point X="-3.573708740234" Y="26.37157421875" />
                  <Point X="-3.561294433594" Y="26.389306640625" />
                  <Point X="-3.551352050781" Y="26.4074296875" />
                  <Point X="-3.550371337891" Y="26.409796875" />
                  <Point X="-3.526706542969" Y="26.466927734375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532054199219" Y="26.58938671875" />
                  <Point X="-3.533239990234" Y="26.5916640625" />
                  <Point X="-3.561793945312" Y="26.646515625" />
                  <Point X="-3.573282958984" Y="26.66370703125" />
                  <Point X="-3.587196044922" Y="26.680287109375" />
                  <Point X="-3.602136230469" Y="26.69458984375" />
                  <Point X="-4.351858886719" Y="27.269875" />
                  <Point X="-4.081150878906" Y="27.733662109375" />
                  <Point X="-4.070562988281" Y="27.747271484375" />
                  <Point X="-3.750504150391" Y="28.158662109375" />
                  <Point X="-3.232701904297" Y="27.859708984375" />
                  <Point X="-3.206657470703" Y="27.844671875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167092041016" Y="27.829833984375" />
                  <Point X="-3.146850341797" Y="27.82580078125" />
                  <Point X="-3.143382568359" Y="27.82549609375" />
                  <Point X="-3.061244140625" Y="27.818310546875" />
                  <Point X="-3.040565429688" Y="27.81876171875" />
                  <Point X="-3.019112792969" Y="27.8215859375" />
                  <Point X="-2.999028076172" Y="27.8265" />
                  <Point X="-2.980482421875" Y="27.835642578125" />
                  <Point X="-2.962231201172" Y="27.847265625" />
                  <Point X="-2.946119873047" Y="27.8601875" />
                  <Point X="-2.943655761719" Y="27.8626484375" />
                  <Point X="-2.885353271484" Y="27.920951171875" />
                  <Point X="-2.872406738281" Y="27.937083984375" />
                  <Point X="-2.860777587891" Y="27.955337890625" />
                  <Point X="-2.851628662109" Y="27.973888671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435302734" Y="28.036115234375" />
                  <Point X="-2.843733642578" Y="28.03952734375" />
                  <Point X="-2.850919921875" Y="28.121666015625" />
                  <Point X="-2.854955322266" Y="28.14195703125" />
                  <Point X="-2.861463623047" Y="28.162599609375" />
                  <Point X="-2.869794677734" Y="28.181533203125" />
                  <Point X="-3.183333251953" Y="28.724595703125" />
                  <Point X="-2.700614746094" Y="29.094689453125" />
                  <Point X="-2.68394921875" Y="29.10394921875" />
                  <Point X="-2.167036865234" Y="29.391134765625" />
                  <Point X="-2.051168212891" Y="29.2401328125" />
                  <Point X="-2.04319519043" Y="29.2297421875" />
                  <Point X="-2.028892211914" Y="29.21480078125" />
                  <Point X="-2.012312744141" Y="29.200888671875" />
                  <Point X="-1.995119506836" Y="29.1893984375" />
                  <Point X="-1.991323242188" Y="29.187421875" />
                  <Point X="-1.899903442383" Y="29.139830078125" />
                  <Point X="-1.880612548828" Y="29.132328125" />
                  <Point X="-1.859701293945" Y="29.1267265625" />
                  <Point X="-1.839259277344" Y="29.12358203125" />
                  <Point X="-1.81862097168" Y="29.124935546875" />
                  <Point X="-1.797301513672" Y="29.1286953125" />
                  <Point X="-1.777387207031" Y="29.1345078125" />
                  <Point X="-1.773492797852" Y="29.136123046875" />
                  <Point X="-1.678280639648" Y="29.175560546875" />
                  <Point X="-1.660146484375" Y="29.185509765625" />
                  <Point X="-1.642416748047" Y="29.197923828125" />
                  <Point X="-1.626864257812" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480712891" Y="29.265919921875" />
                  <Point X="-1.594193725586" Y="29.270001953125" />
                  <Point X="-1.563201293945" Y="29.368296875" />
                  <Point X="-1.559165405273" Y="29.3885859375" />
                  <Point X="-1.557279052734" Y="29.4101484375" />
                  <Point X="-1.557730224609" Y="29.430828125" />
                  <Point X="-1.584201416016" Y="29.631896484375" />
                  <Point X="-0.949624206543" Y="29.80980859375" />
                  <Point X="-0.929424560547" Y="29.812173828125" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.141608093262" Y="29.315068359375" />
                  <Point X="-0.133903213501" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082114028931" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425842285" Y="29.208806640625" />
                  <Point X="0.115583435059" Y="29.231396484375" />
                  <Point X="0.133441574097" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.307419464111" Y="29.8879375" />
                  <Point X="0.844042358398" Y="29.83173828125" />
                  <Point X="0.860765625" Y="29.827701171875" />
                  <Point X="1.481020141602" Y="29.677951171875" />
                  <Point X="1.490496704102" Y="29.674515625" />
                  <Point X="1.894663574219" Y="29.527919921875" />
                  <Point X="1.905181762695" Y="29.523001953125" />
                  <Point X="2.294558105469" Y="29.34090234375" />
                  <Point X="2.304783935547" Y="29.3349453125" />
                  <Point X="2.680972900391" Y="29.11577734375" />
                  <Point X="2.690571044922" Y="29.108951171875" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.181933837891" Y="27.610599609375" />
                  <Point X="2.147580810547" Y="27.55109765625" />
                  <Point X="2.141143310547" Y="27.53758984375" />
                  <Point X="2.132220947266" Y="27.512853515625" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108484130859" Y="27.415623046875" />
                  <Point X="2.107480224609" Y="27.393953125" />
                  <Point X="2.108061523438" Y="27.37818359375" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121439208984" Y="27.289611328125" />
                  <Point X="2.129699951172" Y="27.26753125" />
                  <Point X="2.140060058594" Y="27.24748828125" />
                  <Point X="2.141784423828" Y="27.2449453125" />
                  <Point X="2.183029296875" Y="27.184162109375" />
                  <Point X="2.196255615234" Y="27.168583984375" />
                  <Point X="2.212078857422" Y="27.153572265625" />
                  <Point X="2.224121337891" Y="27.143880859375" />
                  <Point X="2.284906005859" Y="27.102634765625" />
                  <Point X="2.304974121094" Y="27.092263671875" />
                  <Point X="2.327093994141" Y="27.0839921875" />
                  <Point X="2.349053710938" Y="27.078650390625" />
                  <Point X="2.351821777344" Y="27.078318359375" />
                  <Point X="2.418385986328" Y="27.070291015625" />
                  <Point X="2.439149414062" Y="27.070072265625" />
                  <Point X="2.46125390625" Y="27.072267578125" />
                  <Point X="2.476406738281" Y="27.07502734375" />
                  <Point X="2.553491943359" Y="27.095640625" />
                  <Point X="2.565284912109" Y="27.099638671875" />
                  <Point X="2.588534179688" Y="27.110146484375" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.123270507813" Y="27.689462890625" />
                  <Point X="4.128623046875" Y="27.6806171875" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.276011962891" Y="26.70315625" />
                  <Point X="3.230782958984" Y="26.66844921875" />
                  <Point X="3.219477783203" Y="26.658234375" />
                  <Point X="3.201670166016" Y="26.63862109375" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.135477783203" Y="26.54857421875" />
                  <Point X="3.126149902344" Y="26.5285546875" />
                  <Point X="3.120771484375" Y="26.514017578125" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739013672" Y="26.37176953125" />
                  <Point X="3.098443359375" Y="26.36835546875" />
                  <Point X="3.115407958984" Y="26.28613671875" />
                  <Point X="3.121788818359" Y="26.266408203125" />
                  <Point X="3.130902832031" Y="26.2461171875" />
                  <Point X="3.138198730469" Y="26.232826171875" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198900634766" Y="26.145443359375" />
                  <Point X="3.216150146484" Y="26.1293515625" />
                  <Point X="3.234364013672" Y="26.116025390625" />
                  <Point X="3.237128662109" Y="26.11446875" />
                  <Point X="3.303987792969" Y="26.07683203125" />
                  <Point X="3.323387939453" Y="26.068595703125" />
                  <Point X="3.345118896484" Y="26.0621015625" />
                  <Point X="3.359874267578" Y="26.05894140625" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845936523438" Y="25.932810546875" />
                  <Point X="4.847622558594" Y="25.921978515625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.768413330078" Y="25.343478515625" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.701988769531" Y="25.32437109375" />
                  <Point X="3.677857910156" Y="25.312935546875" />
                  <Point X="3.589036376953" Y="25.261595703125" />
                  <Point X="3.572171875" Y="25.24918359375" />
                  <Point X="3.555646728516" Y="25.233943359375" />
                  <Point X="3.545317871094" Y="25.2227578125" />
                  <Point X="3.492024902344" Y="25.154849609375" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463681640625" Y="25.092607421875" />
                  <Point X="3.462943847656" Y="25.088755859375" />
                  <Point X="3.445179443359" Y="24.99599609375" />
                  <Point X="3.443533203125" Y="24.97503125" />
                  <Point X="3.444271240234" Y="24.95237109375" />
                  <Point X="3.445916503906" Y="24.93759375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492019287109" Y="24.78259765625" />
                  <Point X="3.494232177734" Y="24.77977734375" />
                  <Point X="3.547525146484" Y="24.711869140625" />
                  <Point X="3.562542236328" Y="24.6966328125" />
                  <Point X="3.580542236328" Y="24.68208203125" />
                  <Point X="3.59272265625" Y="24.673712890625" />
                  <Point X="3.681544189453" Y="24.62237109375" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855021972656" Y="24.0512734375" />
                  <Point X="4.852861328125" Y="24.041802734375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.484494384766" Y="23.98864453125" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.403118408203" Y="23.99695703125" />
                  <Point X="3.378733154297" Y="23.994671875" />
                  <Point X="3.367419189453" Y="23.99291796875" />
                  <Point X="3.193093994141" Y="23.95502734375" />
                  <Point X="3.16272265625" Y="23.944306640625" />
                  <Point X="3.132492919922" Y="23.92493359375" />
                  <Point X="3.115616699219" Y="23.908892578125" />
                  <Point X="3.108018554688" Y="23.9007734375" />
                  <Point X="3.002653564453" Y="23.774052734375" />
                  <Point X="2.986627929688" Y="23.7495859375" />
                  <Point X="2.974756347656" Y="23.71890234375" />
                  <Point X="2.970533447266" Y="23.69753515625" />
                  <Point X="2.969130371094" Y="23.6878203125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.955109130859" Y="23.491515625" />
                  <Point X="2.964593994141" Y="23.45662890625" />
                  <Point X="2.975111572266" Y="23.435234375" />
                  <Point X="2.980456542969" Y="23.425771484375" />
                  <Point X="3.076930664062" Y="23.275712890625" />
                  <Point X="3.086930175781" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="4.213122070312" Y="22.3931171875" />
                  <Point X="4.124818847656" Y="22.2502265625" />
                  <Point X="4.120342285156" Y="22.2438671875" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="2.854492675781" Y="22.79214453125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.780887939453" Y="22.831736328125" />
                  <Point X="2.756158447266" Y="22.8391953125" />
                  <Point X="2.745608398438" Y="22.84173046875" />
                  <Point X="2.538133544922" Y="22.87919921875" />
                  <Point X="2.505978515625" Y="22.880916015625" />
                  <Point X="2.470131103516" Y="22.874390625" />
                  <Point X="2.447298583984" Y="22.865455078125" />
                  <Point X="2.437675537109" Y="22.861056640625" />
                  <Point X="2.265314941406" Y="22.77034375" />
                  <Point X="2.241145996094" Y="22.75387109375" />
                  <Point X="2.218142578125" Y="22.729623046875" />
                  <Point X="2.205484863281" Y="22.710396484375" />
                  <Point X="2.200765136719" Y="22.70240234375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.098733398438" Y="22.499890625" />
                  <Point X="2.094185058594" Y="22.463455078125" />
                  <Point X="2.095953613281" Y="22.438337890625" />
                  <Point X="2.097231445312" Y="22.428126953125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138986328125" Y="22.204853515625" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.7818359375" Y="20.888345703125" />
                  <Point X="2.776842773438" Y="20.88511328125" />
                  <Point X="2.701764892578" Y="20.836515625" />
                  <Point X="1.799400634766" Y="22.012501953125" />
                  <Point X="1.758546264648" Y="22.065744140625" />
                  <Point X="1.74291394043" Y="22.08178125" />
                  <Point X="1.721788330078" Y="22.098865234375" />
                  <Point X="1.713426391602" Y="22.10490625" />
                  <Point X="1.508800048828" Y="22.2364609375" />
                  <Point X="1.479751831055" Y="22.2503515625" />
                  <Point X="1.443620605469" Y="22.25805078125" />
                  <Point X="1.417865844727" Y="22.25841796875" />
                  <Point X="1.407806396484" Y="22.25802734375" />
                  <Point X="1.184012817383" Y="22.23743359375" />
                  <Point X="1.155377685547" Y="22.23146484375" />
                  <Point X="1.124454589844" Y="22.21759765625" />
                  <Point X="1.104446044922" Y="22.2038828125" />
                  <Point X="1.097419311523" Y="22.1985703125" />
                  <Point X="0.924611572266" Y="22.054884765625" />
                  <Point X="0.902615600586" Y="22.031361328125" />
                  <Point X="0.884305603027" Y="21.998943359375" />
                  <Point X="0.876041809082" Y="21.97374609375" />
                  <Point X="0.873478820801" Y="21.964318359375" />
                  <Point X="0.821809997559" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975713745117" Y="20.129923828125" />
                  <Point X="0.971048217773" Y="20.129076171875" />
                  <Point X="0.929315551758" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058419555664" Y="20.24736328125" />
                  <Point X="-1.063232788086" Y="20.2486015625" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.122698364258" Y="20.409556640625" />
                  <Point X="-1.119972290039" Y="20.435248046875" />
                  <Point X="-1.119666015625" Y="20.4574453125" />
                  <Point X="-1.120163452148" Y="20.468552734375" />
                  <Point X="-1.123109619141" Y="20.496966796875" />
                  <Point X="-1.125747070312" Y="20.514439453125" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.186859863281" Y="20.817953125" />
                  <Point X="-1.196860961914" Y="20.84812890625" />
                  <Point X="-1.206492553711" Y="20.8686171875" />
                  <Point X="-1.22640222168" Y="20.900451171875" />
                  <Point X="-1.2379453125" Y="20.915373046875" />
                  <Point X="-1.257579589844" Y="20.93612109375" />
                  <Point X="-1.270307250977" Y="20.948375" />
                  <Point X="-1.494267822266" Y="21.144783203125" />
                  <Point X="-1.50355456543" Y="21.151962890625" />
                  <Point X="-1.529853149414" Y="21.1698125" />
                  <Point X="-1.549968505859" Y="21.180205078125" />
                  <Point X="-1.585155639648" Y="21.193345703125" />
                  <Point X="-1.60340612793" Y="21.19816015625" />
                  <Point X="-1.63161340332" Y="21.202666015625" />
                  <Point X="-1.649157348633" Y="21.204638671875" />
                  <Point X="-1.946403442383" Y="21.22412109375" />
                  <Point X="-1.958145019531" Y="21.2241640625" />
                  <Point X="-1.989875366211" Y="21.222314453125" />
                  <Point X="-2.012153076172" Y="21.218314453125" />
                  <Point X="-2.048057128906" Y="21.20732421875" />
                  <Point X="-2.0654609375" Y="21.200037109375" />
                  <Point X="-2.090583496094" Y="21.186443359375" />
                  <Point X="-2.105722412109" Y="21.177318359375" />
                  <Point X="-2.353403564453" Y="21.011822265625" />
                  <Point X="-2.359684326172" Y="21.0072421875" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.747602294922" Y="20.98884765625" />
                  <Point X="-2.754219238281" Y="20.993943359375" />
                  <Point X="-2.980862304688" Y="21.16844921875" />
                  <Point X="-2.371168945312" Y="22.224470703125" />
                  <Point X="-2.34148828125" Y="22.275880859375" />
                  <Point X="-2.334848388672" Y="22.289919921875" />
                  <Point X="-2.323945068359" Y="22.318892578125" />
                  <Point X="-2.319681640625" Y="22.333826171875" />
                  <Point X="-2.313411865234" Y="22.365357421875" />
                  <Point X="-2.311631103516" Y="22.381009765625" />
                  <Point X="-2.310681152344" Y="22.41239453125" />
                  <Point X="-2.314930664062" Y="22.44350390625" />
                  <Point X="-2.324262939453" Y="22.473484375" />
                  <Point X="-2.330176513672" Y="22.488087890625" />
                  <Point X="-2.345090820312" Y="22.517609375" />
                  <Point X="-2.352932128906" Y="22.530478515625" />
                  <Point X="-2.370552490234" Y="22.554818359375" />
                  <Point X="-2.380331542969" Y="22.5662890625" />
                  <Point X="-2.396982910156" Y="22.582939453125" />
                  <Point X="-2.408822998047" Y="22.592984375" />
                  <Point X="-2.433979003906" Y="22.611009765625" />
                  <Point X="-2.447294921875" Y="22.618990234375" />
                  <Point X="-2.476124023438" Y="22.63320703125" />
                  <Point X="-2.490564697266" Y="22.6389140625" />
                  <Point X="-2.520182861328" Y="22.6478984375" />
                  <Point X="-2.550870605469" Y="22.6519375" />
                  <Point X="-2.581804931641" Y="22.650923828125" />
                  <Point X="-2.597229003906" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672650146484" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-3.793088134766" Y="21.9822890625" />
                  <Point X="-4.004014404297" Y="22.259404296875" />
                  <Point X="-4.008776611328" Y="22.267388671875" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.100054199219" Y="23.386267578125" />
                  <Point X="-3.048122558594" Y="23.426115234375" />
                  <Point X="-3.036097900391" Y="23.437103515625" />
                  <Point X="-3.014104980469" Y="23.460966796875" />
                  <Point X="-3.00413671875" Y="23.473841796875" />
                  <Point X="-2.985864501953" Y="23.50216015625" />
                  <Point X="-2.978686767578" Y="23.515515625" />
                  <Point X="-2.966546630859" Y="23.543201171875" />
                  <Point X="-2.961584228516" Y="23.55753125" />
                  <Point X="-2.954186767578" Y="23.586091796875" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748779297" Y="23.6316328125" />
                  <Point X="-2.948578857422" Y="23.646962890625" />
                  <Point X="-2.950790039062" Y="23.678640625" />
                  <Point X="-2.95315234375" Y="23.6940703125" />
                  <Point X="-2.960371826172" Y="23.724333984375" />
                  <Point X="-2.972450439453" Y="23.7530078125" />
                  <Point X="-2.989060058594" Y="23.779314453125" />
                  <Point X="-2.998448242188" Y="23.79178125" />
                  <Point X="-3.020334716797" Y="23.816353515625" />
                  <Point X="-3.030981689453" Y="23.82658203125" />
                  <Point X="-3.053736083984" Y="23.84526953125" />
                  <Point X="-3.065843505859" Y="23.853728515625" />
                  <Point X="-3.091270263672" Y="23.868693359375" />
                  <Point X="-3.105435546875" Y="23.87551953125" />
                  <Point X="-3.134700683594" Y="23.886744140625" />
                  <Point X="-3.149800537109" Y="23.891142578125" />
                  <Point X="-3.181687011719" Y="23.897623046875" />
                  <Point X="-3.197303955078" Y="23.89946875" />
                  <Point X="-3.228624511719" Y="23.90055859375" />
                  <Point X="-3.244328125" Y="23.899802734375" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.740760253906" Y="24.025876953125" />
                  <Point X="-4.742019042969" Y="24.034677734375" />
                  <Point X="-4.786451171875" Y="24.345341796875" />
                  <Point X="-3.567445800781" Y="24.671974609375" />
                  <Point X="-3.508287841797" Y="24.687826171875" />
                  <Point X="-3.499916015625" Y="24.690490234375" />
                  <Point X="-3.472312255859" Y="24.701537109375" />
                  <Point X="-3.442515869141" Y="24.71715625" />
                  <Point X="-3.432454833984" Y="24.723251953125" />
                  <Point X="-3.405808349609" Y="24.74174609375" />
                  <Point X="-3.393263427734" Y="24.75215625" />
                  <Point X="-3.370139404297" Y="24.77496484375" />
                  <Point X="-3.359560302734" Y="24.78736328125" />
                  <Point X="-3.339947753906" Y="24.814806640625" />
                  <Point X="-3.332144287109" Y="24.827806640625" />
                  <Point X="-3.318705322266" Y="24.8548828125" />
                  <Point X="-3.313069824219" Y="24.868958984375" />
                  <Point X="-3.304187744141" Y="24.897576171875" />
                  <Point X="-3.300908203125" Y="24.912056640625" />
                  <Point X="-3.296645263672" Y="24.9413515625" />
                  <Point X="-3.295661865234" Y="24.956166015625" />
                  <Point X="-3.296030273438" Y="24.986998046875" />
                  <Point X="-3.297183349609" Y="25.0006640625" />
                  <Point X="-3.301445800781" Y="25.027689453125" />
                  <Point X="-3.304555175781" Y="25.041048828125" />
                  <Point X="-3.3134375" Y="25.06966796875" />
                  <Point X="-3.319607910156" Y="25.0848046875" />
                  <Point X="-3.334450439453" Y="25.11379296875" />
                  <Point X="-3.343122558594" Y="25.12764453125" />
                  <Point X="-3.363472900391" Y="25.15466796875" />
                  <Point X="-3.373497802734" Y="25.16598046875" />
                  <Point X="-3.39521875" Y="25.186876953125" />
                  <Point X="-3.406914794922" Y="25.1964609375" />
                  <Point X="-3.433561279297" Y="25.214955078125" />
                  <Point X="-3.440483642578" Y="25.219330078125" />
                  <Point X="-3.465612304688" Y="25.232833984375" />
                  <Point X="-3.496564941406" Y="25.24563671875" />
                  <Point X="-3.508287841797" Y="25.24961328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331054688" Y="25.957529296875" />
                  <Point X="-4.728795410156" Y="25.96688671875" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-3.820471679688" Y="26.2111875" />
                  <Point X="-3.778065673828" Y="26.20560546875" />
                  <Point X="-3.767738769531" Y="26.20481640625" />
                  <Point X="-3.747069580078" Y="26.204365234375" />
                  <Point X="-3.736727294922" Y="26.204703125" />
                  <Point X="-3.715178466797" Y="26.2065859375" />
                  <Point X="-3.704934570312" Y="26.208046875" />
                  <Point X="-3.684654541016" Y="26.212076171875" />
                  <Point X="-3.672120849609" Y="26.215431640625" />
                  <Point X="-3.613143798828" Y="26.23402734375" />
                  <Point X="-3.603443603516" Y="26.2376796875" />
                  <Point X="-3.584507080078" Y="26.246013671875" />
                  <Point X="-3.575270751953" Y="26.2506953125" />
                  <Point X="-3.556524658203" Y="26.26151953125" />
                  <Point X="-3.547850830078" Y="26.2671796875" />
                  <Point X="-3.531169433594" Y="26.279412109375" />
                  <Point X="-3.515920166016" Y="26.29338671875" />
                  <Point X="-3.502281738281" Y="26.308939453125" />
                  <Point X="-3.495885009766" Y="26.31708984375" />
                  <Point X="-3.483470703125" Y="26.334822265625" />
                  <Point X="-3.478004882812" Y="26.34361328125" />
                  <Point X="-3.4680625" Y="26.361736328125" />
                  <Point X="-3.462605224609" Y="26.373435546875" />
                  <Point X="-3.438940429688" Y="26.43056640625" />
                  <Point X="-3.435502441406" Y="26.440341796875" />
                  <Point X="-3.429711669922" Y="26.46020703125" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436013671875" Y="26.60453515625" />
                  <Point X="-3.443514892578" Y="26.623822265625" />
                  <Point X="-3.448978271484" Y="26.6355390625" />
                  <Point X="-3.477532226562" Y="26.690390625" />
                  <Point X="-3.482808837891" Y="26.69930078125" />
                  <Point X="-3.494297851562" Y="26.7164921875" />
                  <Point X="-3.500510253906" Y="26.7247734375" />
                  <Point X="-3.514423339844" Y="26.741353515625" />
                  <Point X="-3.521500732422" Y="26.74891015625" />
                  <Point X="-3.536440917969" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.22761328125" Y="27.294283203125" />
                  <Point X="-4.002292480469" Y="27.6803125" />
                  <Point X="-3.99558203125" Y="27.6889375" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.280201904297" Y="27.7774375" />
                  <Point X="-3.254157470703" Y="27.762400390625" />
                  <Point X="-3.244923095703" Y="27.75771875" />
                  <Point X="-3.225994384766" Y="27.749388671875" />
                  <Point X="-3.216300048828" Y="27.745740234375" />
                  <Point X="-3.195663330078" Y="27.739232421875" />
                  <Point X="-3.185656005859" Y="27.736666015625" />
                  <Point X="-3.165414306641" Y="27.7326328125" />
                  <Point X="-3.151661621094" Y="27.730857421875" />
                  <Point X="-3.069523193359" Y="27.723671875" />
                  <Point X="-3.059171875" Y="27.723333984375" />
                  <Point X="-3.038493164062" Y="27.72378515625" />
                  <Point X="-3.028165771484" Y="27.72457421875" />
                  <Point X="-3.006713134766" Y="27.7273984375" />
                  <Point X="-2.996535400391" Y="27.72930859375" />
                  <Point X="-2.976450683594" Y="27.73422265625" />
                  <Point X="-2.957022216797" Y="27.741291015625" />
                  <Point X="-2.9384765625" Y="27.75043359375" />
                  <Point X="-2.929452392578" Y="27.75551171875" />
                  <Point X="-2.911201171875" Y="27.767134765625" />
                  <Point X="-2.902793212891" Y="27.77315625" />
                  <Point X="-2.886681884766" Y="27.786078125" />
                  <Point X="-2.87648046875" Y="27.79547265625" />
                  <Point X="-2.818177978516" Y="27.853775390625" />
                  <Point X="-2.811260986328" Y="27.8614921875" />
                  <Point X="-2.798314453125" Y="27.877625" />
                  <Point X="-2.792284912109" Y="27.886041015625" />
                  <Point X="-2.780655761719" Y="27.904294921875" />
                  <Point X="-2.775575927734" Y="27.913318359375" />
                  <Point X="-2.766427001953" Y="27.931869140625" />
                  <Point X="-2.759351318359" Y="27.95130859375" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.013365234375" />
                  <Point X="-2.748458007813" Y="28.034041015625" />
                  <Point X="-2.749094726562" Y="28.047802734375" />
                  <Point X="-2.756281005859" Y="28.12994140625" />
                  <Point X="-2.757744628906" Y="28.140197265625" />
                  <Point X="-2.761780029297" Y="28.16048828125" />
                  <Point X="-2.764351806641" Y="28.1705234375" />
                  <Point X="-2.770860107422" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200861328125" />
                  <Point X="-2.782840332031" Y="28.219794921875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059387207031" Y="28.6999140625" />
                  <Point X="-2.648360351563" Y="29.01504296875" />
                  <Point X="-2.637808837891" Y="29.02090625" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.126536621094" Y="29.18230078125" />
                  <Point X="-2.111820556641" Y="29.164048828125" />
                  <Point X="-2.097517578125" Y="29.149107421875" />
                  <Point X="-2.089957763672" Y="29.14202734375" />
                  <Point X="-2.073378417969" Y="29.128115234375" />
                  <Point X="-2.065098632812" Y="29.121904296875" />
                  <Point X="-2.047905273437" Y="29.1104140625" />
                  <Point X="-2.035195556641" Y="29.103158203125" />
                  <Point X="-1.943775756836" Y="29.05556640625" />
                  <Point X="-1.934335571289" Y="29.0512890625" />
                  <Point X="-1.915044677734" Y="29.043787109375" />
                  <Point X="-1.905193969727" Y="29.0405625" />
                  <Point X="-1.884282470703" Y="29.0349609375" />
                  <Point X="-1.874144897461" Y="29.032830078125" />
                  <Point X="-1.853702880859" Y="29.029685546875" />
                  <Point X="-1.833042236328" Y="29.02878515625" />
                  <Point X="-1.812403930664" Y="29.030138671875" />
                  <Point X="-1.802121948242" Y="29.03137890625" />
                  <Point X="-1.780802490234" Y="29.035138671875" />
                  <Point X="-1.770683959961" Y="29.0375" />
                  <Point X="-1.75076965332" Y="29.0433125" />
                  <Point X="-1.737138427734" Y="29.048353515625" />
                  <Point X="-1.641926269531" Y="29.087791015625" />
                  <Point X="-1.632584960938" Y="29.092271484375" />
                  <Point X="-1.614450805664" Y="29.102220703125" />
                  <Point X="-1.605657958984" Y="29.107689453125" />
                  <Point X="-1.587928222656" Y="29.120103515625" />
                  <Point X="-1.579780029297" Y="29.126498046875" />
                  <Point X="-1.564227539062" Y="29.14013671875" />
                  <Point X="-1.550252441406" Y="29.15538671875" />
                  <Point X="-1.538020751953" Y="29.172068359375" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.516856079102" Y="29.2087265625" />
                  <Point X="-1.508526000977" Y="29.227658203125" />
                  <Point X="-1.503590209961" Y="29.241435546875" />
                  <Point X="-1.47259777832" Y="29.33973046875" />
                  <Point X="-1.470026855469" Y="29.34976171875" />
                  <Point X="-1.465990966797" Y="29.37005078125" />
                  <Point X="-1.464526855469" Y="29.380306640625" />
                  <Point X="-1.462640380859" Y="29.401869140625" />
                  <Point X="-1.462301513672" Y="29.412220703125" />
                  <Point X="-1.462752807617" Y="29.432900390625" />
                  <Point X="-1.46354296875" Y="29.443228515625" />
                  <Point X="-1.479265625" Y="29.562654296875" />
                  <Point X="-0.931165893555" Y="29.7163203125" />
                  <Point X="-0.918376281738" Y="29.717818359375" />
                  <Point X="-0.365222473145" Y="29.78255859375" />
                  <Point X="-0.233371032715" Y="29.29048046875" />
                  <Point X="-0.225666122437" Y="29.2617265625" />
                  <Point X="-0.220435180664" Y="29.247107421875" />
                  <Point X="-0.207661636353" Y="29.218916015625" />
                  <Point X="-0.20011932373" Y="29.20534375" />
                  <Point X="-0.182261245728" Y="29.1786171875" />
                  <Point X="-0.172608932495" Y="29.166455078125" />
                  <Point X="-0.151451278687" Y="29.143865234375" />
                  <Point X="-0.126896400452" Y="29.1250234375" />
                  <Point X="-0.099600616455" Y="29.11043359375" />
                  <Point X="-0.085354072571" Y="29.1042578125" />
                  <Point X="-0.054915958405" Y="29.09392578125" />
                  <Point X="-0.039853622437" Y="29.090154296875" />
                  <Point X="-0.009317713737" Y="29.08511328125" />
                  <Point X="0.021629436493" Y="29.08511328125" />
                  <Point X="0.052165493011" Y="29.090154296875" />
                  <Point X="0.067227828979" Y="29.09392578125" />
                  <Point X="0.09766594696" Y="29.1042578125" />
                  <Point X="0.111912338257" Y="29.11043359375" />
                  <Point X="0.139208267212" Y="29.1250234375" />
                  <Point X="0.163763153076" Y="29.143865234375" />
                  <Point X="0.184920654297" Y="29.166455078125" />
                  <Point X="0.194572967529" Y="29.1786171875" />
                  <Point X="0.21243119812" Y="29.20534375" />
                  <Point X="0.219973373413" Y="29.218916015625" />
                  <Point X="0.232746902466" Y="29.247107421875" />
                  <Point X="0.237978134155" Y="29.2617265625" />
                  <Point X="0.378190612793" Y="29.7850078125" />
                  <Point X="0.827868164062" Y="29.7379140625" />
                  <Point X="0.838472412109" Y="29.735353515625" />
                  <Point X="1.453598388672" Y="29.586841796875" />
                  <Point X="1.458118530273" Y="29.585203125" />
                  <Point X="1.858264160156" Y="29.44006640625" />
                  <Point X="1.864943969727" Y="29.436943359375" />
                  <Point X="2.250436767578" Y="29.25666015625" />
                  <Point X="2.256964355469" Y="29.252857421875" />
                  <Point X="2.629430419922" Y="29.035857421875" />
                  <Point X="2.635511962891" Y="29.031533203125" />
                  <Point X="2.817778808594" Y="28.901916015625" />
                  <Point X="2.099661376953" Y="27.658099609375" />
                  <Point X="2.065308349609" Y="27.59859765625" />
                  <Point X="2.061822021484" Y="27.59196875" />
                  <Point X="2.051778808594" Y="27.56982421875" />
                  <Point X="2.042856567383" Y="27.545087890625" />
                  <Point X="2.040445800781" Y="27.53739453125" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017728637695" Y="27.450322265625" />
                  <Point X="2.01460559082" Y="27.430177734375" />
                  <Point X="2.01358581543" Y="27.42001953125" />
                  <Point X="2.01258203125" Y="27.398349609375" />
                  <Point X="2.012544677734" Y="27.390453125" />
                  <Point X="2.013744750977" Y="27.366810546875" />
                  <Point X="2.021782470703" Y="27.300154296875" />
                  <Point X="2.023799560547" Y="27.289037109375" />
                  <Point X="2.029139648438" Y="27.26712109375" />
                  <Point X="2.032462402344" Y="27.256322265625" />
                  <Point X="2.040723022461" Y="27.2342421875" />
                  <Point X="2.045307373047" Y="27.223908203125" />
                  <Point X="2.055667480469" Y="27.203865234375" />
                  <Point X="2.063173583984" Y="27.191603515625" />
                  <Point X="2.104418457031" Y="27.1308203125" />
                  <Point X="2.110610351562" Y="27.12267578125" />
                  <Point X="2.123836669922" Y="27.10709765625" />
                  <Point X="2.13087109375" Y="27.0996640625" />
                  <Point X="2.146694335938" Y="27.08465234375" />
                  <Point X="2.152517822266" Y="27.0795625" />
                  <Point X="2.170779296875" Y="27.06526953125" />
                  <Point X="2.231563964844" Y="27.0240234375" />
                  <Point X="2.241290527344" Y="27.01823828125" />
                  <Point X="2.261358642578" Y="27.0078671875" />
                  <Point X="2.271700195312" Y="27.00328125" />
                  <Point X="2.293820068359" Y="26.995009765625" />
                  <Point X="2.304639648438" Y="26.99168359375" />
                  <Point X="2.326599365234" Y="26.986341796875" />
                  <Point X="2.3405078125" Y="26.983994140625" />
                  <Point X="2.40701171875" Y="26.975974609375" />
                  <Point X="2.417385253906" Y="26.975296875" />
                  <Point X="2.438148681641" Y="26.975078125" />
                  <Point X="2.448538085938" Y="26.975537109375" />
                  <Point X="2.470642578125" Y="26.977732421875" />
                  <Point X="2.478276123047" Y="26.9788046875" />
                  <Point X="2.500948242188" Y="26.983251953125" />
                  <Point X="2.578033447266" Y="27.003865234375" />
                  <Point X="2.583993652344" Y="27.005669921875" />
                  <Point X="2.604410888672" Y="27.0130703125" />
                  <Point X="2.62766015625" Y="27.023578125" />
                  <Point X="2.636034179688" Y="27.027875" />
                  <Point X="3.940403076172" Y="27.780951171875" />
                  <Point X="4.043952392578" Y="27.637041015625" />
                  <Point X="4.047344726562" Y="27.631435546875" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.2181796875" Y="26.778525390625" />
                  <Point X="3.172950683594" Y="26.743818359375" />
                  <Point X="3.167093017578" Y="26.7389375" />
                  <Point X="3.149143066406" Y="26.72209375" />
                  <Point X="3.131335449219" Y="26.70248046875" />
                  <Point X="3.126272949219" Y="26.696416015625" />
                  <Point X="3.070794677734" Y="26.624041015625" />
                  <Point X="3.064956054688" Y="26.615498046875" />
                  <Point X="3.054241943359" Y="26.597826171875" />
                  <Point X="3.049366455078" Y="26.588697265625" />
                  <Point X="3.040038574219" Y="26.568677734375" />
                  <Point X="3.037052490234" Y="26.56151953125" />
                  <Point X="3.029281738281" Y="26.539603515625" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077392578" Y="26.3637578125" />
                  <Point X="3.005402832031" Y="26.34916015625" />
                  <Point X="3.022367431641" Y="26.26694140625" />
                  <Point X="3.025018066406" Y="26.25690234375" />
                  <Point X="3.031398925781" Y="26.237173828125" />
                  <Point X="3.035129150391" Y="26.227484375" />
                  <Point X="3.044243164063" Y="26.207193359375" />
                  <Point X="3.047625" Y="26.20040234375" />
                  <Point X="3.058834960938" Y="26.180611328125" />
                  <Point X="3.1049765625" Y="26.110478515625" />
                  <Point X="3.111744384766" Y="26.101416015625" />
                  <Point X="3.1263046875" Y="26.084166015625" />
                  <Point X="3.134097167969" Y="26.075978515625" />
                  <Point X="3.151346679688" Y="26.05988671875" />
                  <Point X="3.1600546875" Y="26.052681640625" />
                  <Point X="3.178268554688" Y="26.03935546875" />
                  <Point X="3.190519042969" Y="26.031689453125" />
                  <Point X="3.257378173828" Y="25.994052734375" />
                  <Point X="3.266862792969" Y="25.98938671875" />
                  <Point X="3.286262939453" Y="25.981150390625" />
                  <Point X="3.296186523438" Y="25.97757421875" />
                  <Point X="3.317917480469" Y="25.971080078125" />
                  <Point X="3.325223876953" Y="25.969208984375" />
                  <Point X="3.347428222656" Y="25.964759765625" />
                  <Point X="3.437834716797" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.75268359375" Y="25.914236328125" />
                  <Point X="4.753752929688" Y="25.9073671875" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="3.743825439453" Y="25.4352421875" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.684586181641" Y="25.419041015625" />
                  <Point X="3.661305664062" Y="25.41021875" />
                  <Point X="3.637174804688" Y="25.398783203125" />
                  <Point X="3.630317138672" Y="25.39518359375" />
                  <Point X="3.541495605469" Y="25.34384375" />
                  <Point X="3.532724609375" Y="25.338107421875" />
                  <Point X="3.515860107422" Y="25.3256953125" />
                  <Point X="3.507766601562" Y="25.31901953125" />
                  <Point X="3.491241455078" Y="25.303779296875" />
                  <Point X="3.485852050781" Y="25.298392578125" />
                  <Point X="3.470583740234" Y="25.281408203125" />
                  <Point X="3.417290771484" Y="25.2135" />
                  <Point X="3.410854003906" Y="25.204208984375" />
                  <Point X="3.399129394531" Y="25.184927734375" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004638672" Y="25.142927734375" />
                  <Point X="3.373159912109" Y="25.121431640625" />
                  <Point X="3.369640380859" Y="25.10662890625" />
                  <Point X="3.351875976562" Y="25.013869140625" />
                  <Point X="3.350470947266" Y="25.00343359375" />
                  <Point X="3.348824707031" Y="24.98246875" />
                  <Point X="3.348583496094" Y="24.971939453125" />
                  <Point X="3.349321533203" Y="24.949279296875" />
                  <Point X="3.349854736328" Y="24.941859375" />
                  <Point X="3.352612060547" Y="24.919724609375" />
                  <Point X="3.370376464844" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380004638672" Y="24.794513671875" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399125732422" Y="24.75251953125" />
                  <Point X="3.410844970703" Y="24.733244140625" />
                  <Point X="3.419492431641" Y="24.721134765625" />
                  <Point X="3.472785400391" Y="24.6532265625" />
                  <Point X="3.479864990234" Y="24.645181640625" />
                  <Point X="3.494882080078" Y="24.6299453125" />
                  <Point X="3.502819580078" Y="24.62275390625" />
                  <Point X="3.520819580078" Y="24.608203125" />
                  <Point X="3.526743408203" Y="24.603783203125" />
                  <Point X="3.545180419922" Y="24.59146484375" />
                  <Point X="3.634001953125" Y="24.540123046875" />
                  <Point X="3.639491699219" Y="24.537185546875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.761612792969" Y="24.0689453125" />
                  <Point X="4.760241210938" Y="24.06293359375" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="3.496894287109" Y="24.08283203125" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.426161865234" Y="24.091541015625" />
                  <Point X="3.404898193359" Y="24.091939453125" />
                  <Point X="3.394254638672" Y="24.09154296875" />
                  <Point X="3.369869384766" Y="24.0892578125" />
                  <Point X="3.347241455078" Y="24.08575" />
                  <Point X="3.172916259766" Y="24.047859375" />
                  <Point X="3.161472412109" Y="24.044609375" />
                  <Point X="3.131101074219" Y="24.033888671875" />
                  <Point X="3.111463623047" Y="24.024291015625" />
                  <Point X="3.081233886719" Y="24.00491796875" />
                  <Point X="3.067043212891" Y="23.993791015625" />
                  <Point X="3.050166992188" Y="23.97775" />
                  <Point X="3.034970703125" Y="23.96151171875" />
                  <Point X="2.929605712891" Y="23.834791015625" />
                  <Point X="2.923183349609" Y="23.82610546875" />
                  <Point X="2.907157714844" Y="23.801638671875" />
                  <Point X="2.898028076172" Y="23.783865234375" />
                  <Point X="2.886156494141" Y="23.753181640625" />
                  <Point X="2.881559082031" Y="23.737322265625" />
                  <Point X="2.877336181641" Y="23.715955078125" />
                  <Point X="2.874530029297" Y="23.696525390625" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.85908203125" Y="23.520515625" />
                  <Point X="2.860162597656" Y="23.488328125" />
                  <Point X="2.863436767578" Y="23.466591796875" />
                  <Point X="2.872921630859" Y="23.431705078125" />
                  <Point X="2.879338867188" Y="23.414716796875" />
                  <Point X="2.889856445312" Y="23.393322265625" />
                  <Point X="2.900546386719" Y="23.374396484375" />
                  <Point X="2.997020507812" Y="23.224337890625" />
                  <Point X="3.001739746094" Y="23.217650390625" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043487304688" Y="23.171880859375" />
                  <Point X="3.052795898438" Y="23.163720703125" />
                  <Point X="4.087170410156" Y="22.370017578125" />
                  <Point X="4.045497558594" Y="22.302583984375" />
                  <Point X="4.042658935547" Y="22.29855078125" />
                  <Point X="4.001272949219" Y="22.23974609375" />
                  <Point X="2.901992675781" Y="22.874416015625" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.838676513672" Y="22.910244140625" />
                  <Point X="2.818609863281" Y="22.91892578125" />
                  <Point X="2.808321289062" Y="22.922689453125" />
                  <Point X="2.783591796875" Y="22.9301484375" />
                  <Point X="2.762491699219" Y="22.93521875" />
                  <Point X="2.555016845703" Y="22.9726875" />
                  <Point X="2.543198486328" Y="22.974064453125" />
                  <Point X="2.511043457031" Y="22.97578125" />
                  <Point X="2.488965087891" Y="22.974380859375" />
                  <Point X="2.453117675781" Y="22.96785546875" />
                  <Point X="2.435509521484" Y="22.962857421875" />
                  <Point X="2.412677001953" Y="22.953921875" />
                  <Point X="2.393430908203" Y="22.945125" />
                  <Point X="2.2210703125" Y="22.854412109375" />
                  <Point X="2.211811523438" Y="22.84884375" />
                  <Point X="2.187642578125" Y="22.83237109375" />
                  <Point X="2.172225341797" Y="22.81925390625" />
                  <Point X="2.149221923828" Y="22.795005859375" />
                  <Point X="2.138794433594" Y="22.781861328125" />
                  <Point X="2.12613671875" Y="22.762634765625" />
                  <Point X="2.116697265625" Y="22.746646484375" />
                  <Point X="2.025985229492" Y="22.574287109375" />
                  <Point X="2.021113769531" Y="22.563431640625" />
                  <Point X="2.009794189453" Y="22.533279296875" />
                  <Point X="2.004465087891" Y="22.511658203125" />
                  <Point X="1.999916748047" Y="22.47522265625" />
                  <Point X="1.999419677734" Y="22.456783203125" />
                  <Point X="2.001188232422" Y="22.431666015625" />
                  <Point X="2.003743774414" Y="22.411244140625" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051237792969" Y="22.168451171875" />
                  <Point X="2.064070800781" Y="22.137517578125" />
                  <Point X="2.069546875" Y="22.126419921875" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723753417969" Y="20.9639140625" />
                  <Point X="1.874769287109" Y="22.070333984375" />
                  <Point X="1.833914916992" Y="22.123576171875" />
                  <Point X="1.826574462891" Y="22.1320546875" />
                  <Point X="1.810942138672" Y="22.148091796875" />
                  <Point X="1.802650512695" Y="22.155650390625" />
                  <Point X="1.781524780273" Y="22.172734375" />
                  <Point X="1.764801025391" Y="22.18481640625" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.549783569336" Y="22.322166015625" />
                  <Point X="1.520735229492" Y="22.336056640625" />
                  <Point X="1.49955090332" Y="22.343265625" />
                  <Point X="1.463419555664" Y="22.35096484375" />
                  <Point X="1.444974975586" Y="22.353041015625" />
                  <Point X="1.419220092773" Y="22.353408203125" />
                  <Point X="1.399101074219" Y="22.352626953125" />
                  <Point X="1.175307617188" Y="22.332033203125" />
                  <Point X="1.164627563477" Y="22.330435546875" />
                  <Point X="1.135992431641" Y="22.324466796875" />
                  <Point X="1.116505615234" Y="22.3181484375" />
                  <Point X="1.085582275391" Y="22.30428125" />
                  <Point X="1.070743530273" Y="22.29595703125" />
                  <Point X="1.050734985352" Y="22.2822421875" />
                  <Point X="1.036681884766" Y="22.2716171875" />
                  <Point X="0.863874145508" Y="22.127931640625" />
                  <Point X="0.855221313477" Y="22.11976953125" />
                  <Point X="0.833225280762" Y="22.09624609375" />
                  <Point X="0.819897644043" Y="22.07808203125" />
                  <Point X="0.801587768555" Y="22.0456640625" />
                  <Point X="0.794036254883" Y="22.028548828125" />
                  <Point X="0.785772521973" Y="22.0033515625" />
                  <Point X="0.780646362305" Y="21.98449609375" />
                  <Point X="0.728977478027" Y="21.74678125" />
                  <Point X="0.727584716797" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.72474230957" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.671036071777" Y="21.452458984375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.651436279297" Y="21.522935546875" />
                  <Point X="0.642895568848" Y="21.5441640625" />
                  <Point X="0.637983398438" Y="21.554521484375" />
                  <Point X="0.624235473633" Y="21.579494140625" />
                  <Point X="0.613880065918" Y="21.596193359375" />
                  <Point X="0.456680267334" Y="21.822689453125" />
                  <Point X="0.449298187256" Y="21.83201953125" />
                  <Point X="0.427770751953" Y="21.8559765625" />
                  <Point X="0.410758636475" Y="21.870880859375" />
                  <Point X="0.379893829346" Y="21.892001953125" />
                  <Point X="0.363413482666" Y="21.90102734375" />
                  <Point X="0.338574279785" Y="21.911587890625" />
                  <Point X="0.320553161621" Y="21.918197265625" />
                  <Point X="0.077295394897" Y="21.9936953125" />
                  <Point X="0.066811683655" Y="21.996306640625" />
                  <Point X="0.038066532135" Y="22.00175" />
                  <Point X="0.017431879044" Y="22.003361328125" />
                  <Point X="-0.016852149963" Y="22.00229296875" />
                  <Point X="-0.033942295074" Y="22.00019921875" />
                  <Point X="-0.058864421844" Y="21.994818359375" />
                  <Point X="-0.075084976196" Y="21.990560546875" />
                  <Point X="-0.318342590332" Y="21.9150625" />
                  <Point X="-0.329466125488" Y="21.910830078125" />
                  <Point X="-0.35879208374" Y="21.89751171875" />
                  <Point X="-0.378238067627" Y="21.885685546875" />
                  <Point X="-0.407866790771" Y="21.8623828125" />
                  <Point X="-0.421514526367" Y="21.84924609375" />
                  <Point X="-0.439560028076" Y="21.828021484375" />
                  <Point X="-0.450895477295" Y="21.813287109375" />
                  <Point X="-0.608095397949" Y="21.58679296875" />
                  <Point X="-0.612470825195" Y="21.579869140625" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778137207" Y="21.52378515625" />
                  <Point X="-0.642753051758" Y="21.512064453125" />
                  <Point X="-0.985425292969" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667028768286" Y="21.037839216328" />
                  <Point X="2.686939126373" Y="21.057066425635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.945177322135" Y="22.272132927316" />
                  <Point X="4.064529826629" Y="22.387390301151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608822341109" Y="21.113695464128" />
                  <Point X="2.6379849422" Y="21.141857460646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.859589385008" Y="22.32154715832" />
                  <Point X="3.988324191327" Y="22.445864915709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550615913932" Y="21.189551711929" />
                  <Point X="2.589030758027" Y="21.226648495656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774001447881" Y="22.370961389324" />
                  <Point X="3.912118556025" Y="22.504339530268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.492409486756" Y="21.265407959729" />
                  <Point X="2.540076573853" Y="21.311439530667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.688413510754" Y="22.420375620329" />
                  <Point X="3.835912920723" Y="22.562814144826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.434203059579" Y="21.34126420753" />
                  <Point X="2.49112238968" Y="21.396230565677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.602825573627" Y="22.469789851333" />
                  <Point X="3.759707285421" Y="22.621288759385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375996632403" Y="21.417120455331" />
                  <Point X="2.442168205506" Y="21.481021600688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.5172376365" Y="22.519204082337" />
                  <Point X="3.683501650119" Y="22.679763373943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317790205226" Y="21.492976703131" />
                  <Point X="2.393214021333" Y="21.565812635698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.431649699373" Y="22.568618313341" />
                  <Point X="3.607296014817" Y="22.738237988501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.25958377805" Y="21.568832950932" />
                  <Point X="2.34425983716" Y="21.650603670709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.346061762246" Y="22.618032544345" />
                  <Point X="3.531090379515" Y="22.79671260306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.699021169624" Y="23.92457025682" />
                  <Point X="4.737046038447" Y="23.961290445805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.201377350873" Y="21.644689198732" />
                  <Point X="2.295305652986" Y="21.735394705719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.260473825119" Y="22.667446775349" />
                  <Point X="3.454884744213" Y="22.855187217618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.578670584244" Y="23.940414588623" />
                  <Point X="4.770106487878" Y="24.125282091858" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.143170923697" Y="21.720545446533" />
                  <Point X="2.246351468813" Y="21.82018574073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.174885887992" Y="22.716861006353" />
                  <Point X="3.378679108911" Y="22.913661832177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.458319998863" Y="23.956258920426" />
                  <Point X="4.74567303513" Y="24.233752521956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08496449652" Y="21.796401694334" />
                  <Point X="2.197397284639" Y="21.90497677574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.089297950865" Y="22.766275237357" />
                  <Point X="3.302473473609" Y="22.972136446735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.337969413483" Y="23.972103252229" />
                  <Point X="4.638619193292" Y="24.26243736974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.026758069344" Y="21.872257942134" />
                  <Point X="2.148443100466" Y="21.989767810751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003710013738" Y="22.815689468361" />
                  <Point X="3.226267838307" Y="23.030611061294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.217618828102" Y="23.987947584032" />
                  <Point X="4.531565351454" Y="24.291122217523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.83225004162" Y="20.850800489491" />
                  <Point X="0.832629780478" Y="20.851167199043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.968551642167" Y="21.948114189935" />
                  <Point X="2.099488916292" Y="22.074558845761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.918122076611" Y="22.865103699365" />
                  <Point X="3.150062203005" Y="23.089085675852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.097268242722" Y="24.003791915835" />
                  <Point X="4.424511509616" Y="24.319807065307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.804137461262" Y="20.955718027355" />
                  <Point X="0.817204154974" Y="20.968336386797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.910345214991" Y="22.023970437735" />
                  <Point X="2.053732447719" Y="22.162437878832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.831378223088" Y="22.913401674882" />
                  <Point X="3.073856567703" Y="23.14756029041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976917657341" Y="24.019636247639" />
                  <Point X="4.317457667778" Y="24.348491913091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.776024880905" Y="21.06063556522" />
                  <Point X="0.801778529471" Y="21.08550557455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.852138696738" Y="22.099826597585" />
                  <Point X="2.029119384216" Y="22.27073486084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.72434621062" Y="22.942107603043" />
                  <Point X="3.005240871178" Y="23.213364423647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.85656707196" Y="24.035480579442" />
                  <Point X="4.21040382594" Y="24.377176760874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.747912300547" Y="21.165553103084" />
                  <Point X="0.786352903967" Y="21.202674762303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.786615771699" Y="22.168617385528" />
                  <Point X="2.008810567112" Y="22.38318840528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6091342194" Y="22.962914217545" />
                  <Point X="2.952138119992" Y="23.294149234062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.73621648658" Y="24.051324911245" />
                  <Point X="4.103349984102" Y="24.405861608658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.71979972019" Y="21.270470640948" />
                  <Point X="0.770927278463" Y="21.319843950057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.705867275326" Y="22.222705010145" />
                  <Point X="2.004379831222" Y="22.510975234513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483155248128" Y="22.973323280273" />
                  <Point X="2.899816617983" Y="23.375688488038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.615865901199" Y="24.067169243048" />
                  <Point X="3.996296142264" Y="24.434546456442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.691687139832" Y="21.375388178813" />
                  <Point X="0.75550165296" Y="21.43701313781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.623767108082" Y="22.275487341374" />
                  <Point X="2.123369548187" Y="22.75794780965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.225885534793" Y="22.856946347154" />
                  <Point X="2.862643726085" Y="23.471856584751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.495515319771" Y="24.083013578668" />
                  <Point X="3.889242300426" Y="24.463231304225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.66357460141" Y="21.480305757174" />
                  <Point X="0.740076027456" Y="21.554182325563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.540127776578" Y="22.326783318955" />
                  <Point X="2.866337011525" Y="23.607488690189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.364335132107" Y="24.08839988511" />
                  <Point X="3.782188458588" Y="24.491916152009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.626072968086" Y="21.576156391983" />
                  <Point X="0.725075800653" Y="21.671762316067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.430770258396" Y="22.353243532353" />
                  <Point X="2.88956122422" Y="23.761981592839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.18927930741" Y="24.051415981382" />
                  <Point X="3.67613913088" Y="24.521571047812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.572220478442" Y="21.656217188385" />
                  <Point X="0.745707502811" Y="21.823751660392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.282237919875" Y="22.341873061493" />
                  <Point X="3.586828859297" Y="24.567390662215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770345829687" Y="25.710299715315" />
                  <Point X="4.782593827783" Y="25.72212746959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.029300964452" Y="20.241711449516" />
                  <Point X="-0.967030579081" Y="20.301845261671" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.517341924049" Y="21.735287125576" />
                  <Point X="0.782512167055" Y="21.99135905266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.123140042781" Y="22.320299568635" />
                  <Point X="3.505313580881" Y="24.620737814021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.581068979847" Y="25.659582727241" />
                  <Point X="4.764719361334" Y="25.836931839131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.138792261114" Y="20.268042474537" />
                  <Point X="-0.91929073549" Y="20.480012633885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.462463369656" Y="21.814357062768" />
                  <Point X="3.442353809334" Y="24.692003810519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.391792130008" Y="25.608865739166" />
                  <Point X="4.744180699095" Y="25.949163424704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121978026928" Y="20.416345332893" />
                  <Point X="-0.871550891899" Y="20.658180006099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.395236767181" Y="21.881502628536" />
                  <Point X="3.389187094833" Y="24.77272685228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.202515280169" Y="25.558148751092" />
                  <Point X="4.718149520846" Y="26.056090949221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.130808242314" Y="20.539883634163" />
                  <Point X="-0.823811048307" Y="20.836347378312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.302333057075" Y="21.923852099695" />
                  <Point X="3.360733983417" Y="24.877315543123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.013238430329" Y="25.507431763018" />
                  <Point X="4.628146130977" Y="26.101241227076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.152844806787" Y="20.650668712362" />
                  <Point X="-0.776071204716" Y="21.014514750526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.198837561622" Y="21.95597320264" />
                  <Point X="3.350134188941" Y="24.999144981729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.82396158049" Y="25.456714774944" />
                  <Point X="4.469800958134" Y="26.080394612263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.174881371261" Y="20.761453790561" />
                  <Point X="-0.728331361125" Y="21.19268212274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.095342066169" Y="21.988094305585" />
                  <Point X="3.392606822913" Y="25.172225868739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613301815811" Y="25.385348546037" />
                  <Point X="4.311455785291" Y="26.059547997451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.2046740507" Y="20.864748875602" />
                  <Point X="-0.680591517534" Y="21.370849494953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.02814537407" Y="22.000909411863" />
                  <Point X="4.153110612448" Y="26.038701382638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.262559214149" Y="20.940915364178" />
                  <Point X="-0.624950431875" Y="21.556647007939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.223233107004" Y="21.944580919212" />
                  <Point X="3.994765439606" Y="26.017854767825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.333869365004" Y="21.004117493114" />
                  <Point X="3.836420266763" Y="25.997008153013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.405540291077" Y="21.066971225472" />
                  <Point X="3.67807509392" Y="25.9761615382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.477211217151" Y="21.129824957829" />
                  <Point X="3.519729921078" Y="25.955314923388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.558499754843" Y="21.183391070606" />
                  <Point X="3.387296641614" Y="25.959491133146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.671723485719" Y="21.206117725804" />
                  <Point X="3.277027710382" Y="25.985071205192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.799789295229" Y="21.21451155227" />
                  <Point X="3.189320870888" Y="26.032439235965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.927855104739" Y="21.222905378736" />
                  <Point X="3.117243774401" Y="26.094900734113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.125513890335" Y="21.16409404939" />
                  <Point X="3.062878106119" Y="26.174465959665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.562455375039" Y="20.874210103511" />
                  <Point X="3.022286671734" Y="26.267332808273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645784869614" Y="20.925805287137" />
                  <Point X="3.001740375407" Y="26.379557021693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.729114364189" Y="20.977400470762" />
                  <Point X="3.028751273351" Y="26.537706683781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.510016809405" Y="27.00245940965" />
                  <Point X="4.089390683922" Y="27.561954256688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.806810069747" Y="21.0344361412" />
                  <Point X="4.038391050667" Y="27.64477002448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.882900130309" Y="21.093022364987" />
                  <Point X="3.98232328432" Y="27.722691553038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958990190871" Y="21.151608588775" />
                  <Point X="3.854591484882" Y="27.73140792928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.858070558328" Y="21.381131086226" />
                  <Point X="3.51451412432" Y="27.535064580766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685743275984" Y="21.679611149525" />
                  <Point X="3.174436763758" Y="27.338721232252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51341599364" Y="21.978091212825" />
                  <Point X="2.834359403196" Y="27.142377883738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.341223141017" Y="22.276441458851" />
                  <Point X="2.545027917722" Y="26.995039257165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.313771345619" Y="22.435016890662" />
                  <Point X="2.390579898068" Y="26.97795607944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.352466741371" Y="22.529714722494" />
                  <Point X="2.277717108183" Y="27.001031291301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.417367167739" Y="22.599106650417" />
                  <Point X="2.19233401888" Y="27.05064334155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.507563599296" Y="22.644070510081" />
                  <Point X="2.119421414877" Y="27.112297999469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.653988403141" Y="22.634735261801" />
                  <Point X="2.063812212855" Y="27.190662358446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.980641094242" Y="22.451355965891" />
                  <Point X="2.02477573981" Y="27.285030815766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320719919145" Y="22.25501120328" />
                  <Point X="2.012920079826" Y="27.405647479147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.660798744047" Y="22.058666440669" />
                  <Point X="2.056147822006" Y="27.579457565678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.828487514648" Y="22.028796818384" />
                  <Point X="2.224202981595" Y="27.873812087987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.886423951888" Y="22.104913792435" />
                  <Point X="2.396530691727" Y="28.172292564397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944360389129" Y="22.181030766487" />
                  <Point X="2.568858401859" Y="28.470773040807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002296826369" Y="22.257147740538" />
                  <Point X="2.741186111992" Y="28.769253517216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052510014742" Y="22.340722969325" />
                  <Point X="2.774024929306" Y="28.933031135621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.102486806397" Y="22.42452648377" />
                  <Point X="-3.131798418827" Y="23.361909363482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.985109240453" Y="23.503565456423" />
                  <Point X="2.695265805043" Y="28.989039874553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.152463598051" Y="22.508329998216" />
                  <Point X="-3.797581508927" Y="22.851035648063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950144730425" Y="23.669395832421" />
                  <Point X="2.615433383953" Y="29.044012142788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.983006666031" Y="23.769726971234" />
                  <Point X="2.530135826895" Y="29.093706790564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.047144088431" Y="23.839855723524" />
                  <Point X="2.444838269836" Y="29.143401438341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.13519767122" Y="23.886888908189" />
                  <Point X="2.359540712778" Y="29.193096086118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.260832970104" Y="23.897629851484" />
                  <Point X="2.274243155719" Y="29.242790733894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.419177806495" Y="23.87678356158" />
                  <Point X="2.184014741579" Y="29.287723708337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577522642885" Y="23.855937271676" />
                  <Point X="2.091877591738" Y="29.330813438139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.735867479275" Y="23.835090981772" />
                  <Point X="1.999740441897" Y="29.373903167942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.894212315665" Y="23.814244691868" />
                  <Point X="1.907603292055" Y="29.416992897744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.052557152055" Y="23.793398401964" />
                  <Point X="1.812083901774" Y="29.456816435919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210901988446" Y="23.77255211206" />
                  <Point X="1.712666792124" Y="29.492875990253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.369246824836" Y="23.751705822155" />
                  <Point X="1.613249682474" Y="29.528935544587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.527591661226" Y="23.730859532251" />
                  <Point X="-3.547505586506" Y="24.677317652953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300332998575" Y="24.916009446558" />
                  <Point X="1.513832572824" Y="29.564995098921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665195319048" Y="23.730042765667" />
                  <Point X="-3.736782224112" Y="24.626600869831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.305259341601" Y="25.043317673544" />
                  <Point X="1.410478159907" Y="29.597252443687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692254296603" Y="23.83597775593" />
                  <Point X="-3.926058778772" Y="24.575884166808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.347994638364" Y="25.134114218318" />
                  <Point X="1.301072843618" Y="29.623666498989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.719313274158" Y="23.941912746194" />
                  <Point X="-4.115335333432" Y="24.525167463785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.414639049737" Y="25.201821999498" />
                  <Point X="1.191667527329" Y="29.65008055429" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744202354216" Y="24.049943182114" />
                  <Point X="-4.304611888092" Y="24.474450760762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.503566163071" Y="25.248011625522" />
                  <Point X="1.082262211039" Y="29.676494609592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760798573981" Y="24.16598194013" />
                  <Point X="-4.493888442752" Y="24.42373405774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.610347166183" Y="25.276959950601" />
                  <Point X="0.97285689475" Y="29.702908664894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777394793745" Y="24.282020698145" />
                  <Point X="-4.683164997412" Y="24.373017354717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.717400929941" Y="25.305644873786" />
                  <Point X="0.863451578461" Y="29.729322720196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.824454693699" Y="25.33432979697" />
                  <Point X="0.069530214046" Y="29.094707311648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.235599116387" Y="29.255078186483" />
                  <Point X="0.744618618646" Y="29.746632605973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931508457457" Y="25.363014720155" />
                  <Point X="-0.064624396698" Y="29.09722125111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.28414496763" Y="29.434023911238" />
                  <Point X="0.621240915339" Y="29.759553683973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.038562221215" Y="25.391699643339" />
                  <Point X="-0.152225267073" Y="29.144691615072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331884614875" Y="29.612191093843" />
                  <Point X="0.497863212031" Y="29.772474761974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.145615984973" Y="25.420384566524" />
                  <Point X="-0.2090191097" Y="29.221911979915" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252669748732" Y="25.449069489708" />
                  <Point X="-0.241881626689" Y="29.322242557294" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.35972351249" Y="25.477754412893" />
                  <Point X="-0.269994114478" Y="29.427160184551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.466777276248" Y="25.506439336077" />
                  <Point X="-3.743948252289" Y="26.204467210619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421442624457" Y="26.515907275228" />
                  <Point X="-0.298106602267" Y="29.532077811808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.573831040006" Y="25.535124259262" />
                  <Point X="-3.867355379964" Y="26.217359873839" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.444426577519" Y="26.625777470902" />
                  <Point X="-0.326219090056" Y="29.636995439065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.680884803764" Y="25.563809182446" />
                  <Point X="-3.987705811764" Y="26.233204353953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.491532116016" Y="26.712353722291" />
                  <Point X="-0.354331577845" Y="29.741913066321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784914887823" Y="25.595414039175" />
                  <Point X="-4.108056243564" Y="26.249048834067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557863481253" Y="26.78036380861" />
                  <Point X="-0.460553601697" Y="29.771401191397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.762114427002" Y="25.749497729396" />
                  <Point X="-4.228406675364" Y="26.264893314181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.634068950665" Y="26.838838583367" />
                  <Point X="-0.616171915681" Y="29.753187873575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.739313966182" Y="25.903581419618" />
                  <Point X="-4.348757107163" Y="26.280737794296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710274420076" Y="26.897313358125" />
                  <Point X="-0.771790229665" Y="29.734974555753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699830369899" Y="26.073775826484" />
                  <Point X="-4.469107538963" Y="26.29658227441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.786479889487" Y="26.955788132882" />
                  <Point X="-2.981723182809" Y="27.732932650972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761390936285" Y="27.945705028168" />
                  <Point X="-0.927409518409" Y="29.716760296616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.651358679303" Y="26.252649935135" />
                  <Point X="-4.589457970763" Y="26.312426754524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.862685358898" Y="27.014262907639" />
                  <Point X="-3.123207639183" Y="27.728368240788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752470285799" Y="28.086385141353" />
                  <Point X="-1.766209686388" Y="29.038805931239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.480479775127" Y="29.31473209917" />
                  <Point X="-1.119219141225" Y="29.663597438109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93889082831" Y="27.072737682397" />
                  <Point X="-3.234377793485" Y="27.753078011832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.773489702869" Y="28.198152467382" />
                  <Point X="-1.902027043187" Y="29.0397141755" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465870234947" Y="29.460905909274" />
                  <Point X="-1.311923458411" Y="29.609570583293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.015096297721" Y="27.131212457154" />
                  <Point X="-3.321324801629" Y="27.801179803211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820006638018" Y="28.285297126417" />
                  <Point X="-1.994841069404" Y="29.082150253385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091301767132" Y="27.189687231911" />
                  <Point X="-3.406912763875" Y="27.850594009958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868960965208" Y="28.370088023318" />
                  <Point X="-2.079062071359" Y="29.13288451834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167507236543" Y="27.248162006668" />
                  <Point X="-3.49250072612" Y="27.900008216705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.917915292397" Y="28.454878920219" />
                  <Point X="-2.14275749694" Y="29.203440101997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.190290757933" Y="27.358225756959" />
                  <Point X="-3.578088688366" Y="27.949422423453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.966869619587" Y="28.539669817121" />
                  <Point X="-2.239218085563" Y="29.242354735499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.013627277018" Y="27.660893238543" />
                  <Point X="-3.663676650612" Y="27.9988366302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.015823946777" Y="28.624460714022" />
                  <Point X="-2.561242614299" Y="29.063444804032" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.487510314941" Y="21.403283203125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.457791046143" Y="21.487861328125" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.289073425293" Y="21.72617578125" />
                  <Point X="0.264234313965" Y="21.736736328125" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="0.006155959129" Y="21.81448046875" />
                  <Point X="-0.018766191483" Y="21.809099609375" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.276761383057" Y="21.72617578125" />
                  <Point X="-0.294806915283" Y="21.704951171875" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.843837219238" Y="20.02750390625" />
                  <Point X="-0.847744384766" Y="20.012921875" />
                  <Point X="-1.100230712891" Y="20.061931640625" />
                  <Point X="-1.110576660156" Y="20.06459375" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.311072998047" Y="20.434357421875" />
                  <Point X="-1.309150390625" Y="20.448958984375" />
                  <Point X="-1.312096435547" Y="20.477373046875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.375948730469" Y="20.784779296875" />
                  <Point X="-1.395582885742" Y="20.80552734375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.633376708984" Y="21.0105390625" />
                  <Point X="-1.661583984375" Y="21.015044921875" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.975041015625" Y="21.032931640625" />
                  <Point X="-2.000163696289" Y="21.019337890625" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.855837646484" Y="20.832390625" />
                  <Point X="-2.870140380859" Y="20.843404296875" />
                  <Point X="-3.228580566406" Y="21.119390625" />
                  <Point X="-2.535713867188" Y="22.319470703125" />
                  <Point X="-2.506033203125" Y="22.370880859375" />
                  <Point X="-2.499763427734" Y="22.402412109375" />
                  <Point X="-2.514677734375" Y="22.43193359375" />
                  <Point X="-2.531329101562" Y="22.448583984375" />
                  <Point X="-2.560158203125" Y="22.46280078125" />
                  <Point X="-2.591684082031" Y="22.456529296875" />
                  <Point X="-3.829675048828" Y="21.741775390625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.161704589844" Y="22.15287109375" />
                  <Point X="-4.171959472656" Y="22.17006640625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.21571875" Y="23.537005859375" />
                  <Point X="-3.163787109375" Y="23.576853515625" />
                  <Point X="-3.145514892578" Y="23.605171875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140328613281" Y="23.66541015625" />
                  <Point X="-3.162215087891" Y="23.689982421875" />
                  <Point X="-3.187641845703" Y="23.704947265625" />
                  <Point X="-3.219528320312" Y="23.711427734375" />
                  <Point X="-4.782374511719" Y="23.50567578125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.927393066406" Y="23.98880859375" />
                  <Point X="-4.930105957031" Y="24.007779296875" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-3.616621582031" Y="24.855501953125" />
                  <Point X="-3.557463623047" Y="24.871353515625" />
                  <Point X="-3.540789306641" Y="24.879341796875" />
                  <Point X="-3.514142822266" Y="24.8978359375" />
                  <Point X="-3.494530273438" Y="24.925279296875" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.486016601562" Y="24.984728515625" />
                  <Point X="-3.494898925781" Y="25.01334765625" />
                  <Point X="-3.515249267578" Y="25.04037109375" />
                  <Point X="-3.541895751953" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.982071777344" Y="25.44780859375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.912182128906" Y="26.016578125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.795671386719" Y="26.3995625" />
                  <Point X="-3.753265380859" Y="26.39398046875" />
                  <Point X="-3.731716552734" Y="26.39586328125" />
                  <Point X="-3.729255615234" Y="26.396638671875" />
                  <Point X="-3.670278564453" Y="26.415234375" />
                  <Point X="-3.651532470703" Y="26.42605859375" />
                  <Point X="-3.639118164062" Y="26.443791015625" />
                  <Point X="-3.638137451172" Y="26.446158203125" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.617501708984" Y="26.5477890625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.65996875" Y="26.619220703125" />
                  <Point X="-4.47610546875" Y="27.245466796875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.145544921875" Y="27.805603515625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.185201904297" Y="27.94198046875" />
                  <Point X="-3.159157470703" Y="27.926943359375" />
                  <Point X="-3.138520751953" Y="27.920435546875" />
                  <Point X="-3.135103515625" Y="27.920134765625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031512451172" Y="27.9157734375" />
                  <Point X="-3.013261230469" Y="27.927396484375" />
                  <Point X="-3.010831054688" Y="27.92982421875" />
                  <Point X="-2.952528564453" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.938372558594" Y="28.031251953125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067138672" Y="28.134033203125" />
                  <Point X="-3.307279296875" Y="28.74927734375" />
                  <Point X="-2.752874267578" Y="29.17433203125" />
                  <Point X="-2.730087158203" Y="29.1869921875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-1.975799682617" Y="29.29796484375" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.947450927734" Y="29.271685546875" />
                  <Point X="-1.85603112793" Y="29.22409375" />
                  <Point X="-1.835119995117" Y="29.2184921875" />
                  <Point X="-1.813800537109" Y="29.222251953125" />
                  <Point X="-1.809847167969" Y="29.223892578125" />
                  <Point X="-1.714635009766" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.684796386719" Y="29.2985703125" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.418427734375" />
                  <Point X="-1.689137207031" Y="29.701138671875" />
                  <Point X="-0.968083190918" Y="29.903296875" />
                  <Point X="-0.940469909668" Y="29.906529296875" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.0498450737" Y="29.33965625" />
                  <Point X="-0.049844921112" Y="29.33965625" />
                  <Point X="-0.042140228271" Y="29.31090234375" />
                  <Point X="-0.024282043457" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036593975067" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.236648239136" Y="29.9908671875" />
                  <Point X="0.860210388184" Y="29.925564453125" />
                  <Point X="0.883062438965" Y="29.920046875" />
                  <Point X="1.508456542969" Y="29.769056640625" />
                  <Point X="1.522881103516" Y="29.763826171875" />
                  <Point X="1.931044433594" Y="29.61578125" />
                  <Point X="1.945421508789" Y="29.60905859375" />
                  <Point X="2.338687011719" Y="29.425140625" />
                  <Point X="2.352606445312" Y="29.41703125" />
                  <Point X="2.732520751953" Y="29.195693359375" />
                  <Point X="2.745633056641" Y="29.1863671875" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.264206298828" Y="27.563099609375" />
                  <Point X="2.229853271484" Y="27.50359765625" />
                  <Point X="2.22399609375" Y="27.4883125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202378417969" Y="27.389556640625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218676757812" Y="27.3008203125" />
                  <Point X="2.220395263672" Y="27.298287109375" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.277463378906" Y="27.2224921875" />
                  <Point X="2.338248046875" Y="27.18124609375" />
                  <Point X="2.360367919922" Y="27.172974609375" />
                  <Point X="2.363135986328" Y="27.172642578125" />
                  <Point X="2.429760742188" Y="27.164607421875" />
                  <Point X="2.451865234375" Y="27.166802734375" />
                  <Point X="2.528950439453" Y="27.187416015625" />
                  <Point X="2.541034179688" Y="27.19241796875" />
                  <Point X="3.973916015625" Y="28.01969140625" />
                  <Point X="3.994248046875" Y="28.0314296875" />
                  <Point X="4.202590332031" Y="27.741880859375" />
                  <Point X="4.209900390625" Y="27.72980078125" />
                  <Point X="4.387513183594" Y="27.436294921875" />
                  <Point X="3.333844238281" Y="26.627787109375" />
                  <Point X="3.288615234375" Y="26.593080078125" />
                  <Point X="3.277067382812" Y="26.580826171875" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.212261230469" Y="26.488431640625" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.191483886719" Y="26.38755078125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.2175625" Y="26.285041015625" />
                  <Point X="3.263704101562" Y="26.214908203125" />
                  <Point X="3.280953613281" Y="26.19881640625" />
                  <Point X="3.283730224609" Y="26.19725390625" />
                  <Point X="3.350589355469" Y="26.1596171875" />
                  <Point X="3.3723203125" Y="26.153123046875" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.8369453125" Y="26.320369140625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.941491699219" Y="25.936591796875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.793001220703" Y="25.25171484375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.725398681641" Y="25.2306875" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.620052001953" Y="25.164107421875" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.556247314453" Y="25.0708828125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.539220947266" Y="24.955462890625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.568971923828" Y="24.838419921875" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.640264892578" Y="24.7559609375" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.989396484375" Y="24.365150390625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.945480957031" Y="24.020669921875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.472094482422" Y="23.89445703125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.387596923828" Y="23.9000859375" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.197942626953" Y="23.856076171875" />
                  <Point X="3.18106640625" Y="23.84003515625" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.067953613281" Y="23.700482421875" />
                  <Point X="3.063730712891" Y="23.679115234375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.049849121094" Y="23.498541015625" />
                  <Point X="3.060366699219" Y="23.477146484375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460449219" Y="23.314458984375" />
                  <Point X="4.326824707031" Y="22.425615234375" />
                  <Point X="4.339073730469" Y="22.416216796875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.198027832031" Y="22.189185546875" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="2.806992675781" Y="22.709873046875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.728725097656" Y="22.7482421875" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.504752685547" Y="22.785923828125" />
                  <Point X="2.481920166016" Y="22.77698828125" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.297490722656" Y="22.677384765625" />
                  <Point X="2.284833007812" Y="22.658158203125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.188950439453" Y="22.470126953125" />
                  <Point X="2.190718994141" Y="22.445009765625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091796875" Y="22.221419921875" />
                  <Point X="2.978457275391" Y="20.932142578125" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.835296630859" Y="20.809787109375" />
                  <Point X="2.8284765625" Y="20.80537109375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.724031982422" Y="21.954669921875" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.662051879883" Y="22.02499609375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.442266357422" Y="22.163060546875" />
                  <Point X="1.41651159668" Y="22.163427734375" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.178165405273" Y="22.13923828125" />
                  <Point X="1.158156738281" Y="22.1255234375" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.974574951172" Y="21.969337890625" />
                  <Point X="0.966311279297" Y="21.944140625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.124876953125" Y="20.0869765625" />
                  <Point X="1.127642333984" Y="20.065970703125" />
                  <Point X="0.994367492676" Y="20.0367578125" />
                  <Point X="0.988039123535" Y="20.035607421875" />
                  <Point X="0.860200378418" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#120" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.007704675999" Y="4.383878717325" Z="0.1" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="-0.952438035427" Y="4.987470524619" Z="0.1" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.1" />
                  <Point X="-1.719960207573" Y="4.777433358763" Z="0.1" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.1" />
                  <Point X="-1.74881345865" Y="4.755879581922" Z="0.1" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.1" />
                  <Point X="-1.738638169725" Y="4.34488585766" Z="0.1" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.1" />
                  <Point X="-1.835144978169" Y="4.301362422626" Z="0.1" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.1" />
                  <Point X="-1.930518928965" Y="4.34731509453" Z="0.1" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.1" />
                  <Point X="-1.942288205271" Y="4.359681946167" Z="0.1" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.1" />
                  <Point X="-2.760526061943" Y="4.261980126942" Z="0.1" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.1" />
                  <Point X="-3.355060661319" Y="3.811646893241" Z="0.1" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.1" />
                  <Point X="-3.363632477803" Y="3.767501965628" Z="0.1" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.1" />
                  <Point X="-2.994338426924" Y="3.05817441508" Z="0.1" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.1" />
                  <Point X="-3.052342503043" Y="2.996461064843" Z="0.1" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.1" />
                  <Point X="-3.136902050527" Y="3.001226272249" Z="0.1" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.1" />
                  <Point X="-3.166357382669" Y="3.016561470943" Z="0.1" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.1" />
                  <Point X="-4.191164100168" Y="2.867587760212" Z="0.1" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.1" />
                  <Point X="-4.534099174379" Y="2.287122726264" Z="0.1" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.1" />
                  <Point X="-4.513721051871" Y="2.237862022633" Z="0.1" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.1" />
                  <Point X="-3.668008163918" Y="1.555982148284" Z="0.1" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.1" />
                  <Point X="-3.690487211005" Y="1.496572550423" Z="0.1" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.1" />
                  <Point X="-3.7504470045" Y="1.475604721567" Z="0.1" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.1" />
                  <Point X="-3.795301879876" Y="1.480415367196" Z="0.1" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.1" />
                  <Point X="-4.966597688451" Y="1.060936531753" Z="0.1" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.1" />
                  <Point X="-5.056838569419" Y="0.470217864375" Z="0.1" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.1" />
                  <Point X="-5.001169172287" Y="0.430791718058" Z="0.1" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.1" />
                  <Point X="-3.5499143688" Y="0.030574931759" Z="0.1" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.1" />
                  <Point X="-3.539925584511" Y="0.001188405503" Z="0.1" />
                  <Point X="-3.539556741714" Y="0" Z="0.1" />
                  <Point X="-3.548439016624" Y="-0.028618545557" Z="0.1" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.1" />
                  <Point X="-3.575454226323" Y="-0.048301051375" Z="0.1" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.1" />
                  <Point X="-3.635718616485" Y="-0.064920338698" Z="0.1" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.1" />
                  <Point X="-4.985758246918" Y="-0.968020252081" Z="0.1" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.1" />
                  <Point X="-4.852316022304" Y="-1.49996937576" Z="0.1" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.1" />
                  <Point X="-4.782005024322" Y="-1.512615865694" Z="0.1" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.1" />
                  <Point X="-3.19373216842" Y="-1.321828368544" Z="0.1" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.1" />
                  <Point X="-3.200073623273" Y="-1.351011140379" Z="0.1" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.1" />
                  <Point X="-3.252312369843" Y="-1.392045661991" Z="0.1" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.1" />
                  <Point X="-4.221058568734" Y="-2.824261786951" Z="0.1" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.1" />
                  <Point X="-3.87607842617" Y="-3.281743733367" Z="0.1" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.1" />
                  <Point X="-3.810830428184" Y="-3.270245357181" Z="0.1" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.1" />
                  <Point X="-2.556182657796" Y="-2.572147811336" Z="0.1" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.1" />
                  <Point X="-2.585171655987" Y="-2.624247954308" Z="0.1" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.1" />
                  <Point X="-2.906800336704" Y="-4.164932583666" Z="0.1" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.1" />
                  <Point X="-2.468634982632" Y="-4.43869533576" Z="0.1" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.1" />
                  <Point X="-2.442151153462" Y="-4.437856071534" Z="0.1" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.1" />
                  <Point X="-1.978541132752" Y="-3.990956686059" Z="0.1" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.1" />
                  <Point X="-1.67101019301" Y="-4.003566908221" Z="0.1" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.1" />
                  <Point X="-1.434706108548" Y="-4.200784500292" Z="0.1" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.1" />
                  <Point X="-1.36729176926" Y="-4.501100095918" Z="0.1" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.1" />
                  <Point X="-1.366801091039" Y="-4.527835485724" Z="0.1" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.1" />
                  <Point X="-1.129191309612" Y="-4.952550243903" Z="0.1" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.1" />
                  <Point X="-0.829618489039" Y="-5.011443334461" Z="0.1" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="-0.80169687211" Y="-4.954157586102" Z="0.1" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="-0.259886915777" Y="-3.292278044594" Z="0.1" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="-0.010101446594" Y="-3.207374484683" Z="0.1" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.1" />
                  <Point X="0.243257632767" Y="-3.279737560605" Z="0.1" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.1" />
                  <Point X="0.410558943655" Y="-3.509367711174" Z="0.1" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.1" />
                  <Point X="0.433057988291" Y="-3.578378443714" Z="0.1" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.1" />
                  <Point X="0.990819905574" Y="-4.982307583652" Z="0.1" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.1" />
                  <Point X="1.169914523417" Y="-4.943320039215" Z="0.1" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.1" />
                  <Point X="1.168293231566" Y="-4.8752184478" Z="0.1" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.1" />
                  <Point X="1.009014438347" Y="-3.035197354041" Z="0.1" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.1" />
                  <Point X="1.183967785348" Y="-2.881642020081" Z="0.1" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.1" />
                  <Point X="1.414937270924" Y="-2.855081756579" Z="0.1" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.1" />
                  <Point X="1.628856648605" Y="-2.985782324111" Z="0.1" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.1" />
                  <Point X="1.678208479277" Y="-3.044487987009" Z="0.1" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.1" />
                  <Point X="2.849489866345" Y="-4.205322251188" Z="0.1" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.1" />
                  <Point X="3.038967628816" Y="-4.070504174894" Z="0.1" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.1" />
                  <Point X="3.015602299808" Y="-4.01157678046" Z="0.1" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.1" />
                  <Point X="2.233767074062" Y="-2.514823724397" Z="0.1" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.1" />
                  <Point X="2.322923308416" Y="-2.33384759327" Z="0.1" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.1" />
                  <Point X="2.499050785102" Y="-2.235978001551" Z="0.1" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.1" />
                  <Point X="2.713683079116" Y="-2.269680935429" Z="0.1" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.1" />
                  <Point X="2.775836816647" Y="-2.302147200466" Z="0.1" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.1" />
                  <Point X="4.23276064591" Y="-2.808311197171" Z="0.1" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.1" />
                  <Point X="4.392849814499" Y="-2.550636246012" Z="0.1" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.1" />
                  <Point X="4.351106661673" Y="-2.503436982168" Z="0.1" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.1" />
                  <Point X="3.096268664738" Y="-1.46453381374" Z="0.1" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.1" />
                  <Point X="3.107364342481" Y="-1.294187234764" Z="0.1" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.1" />
                  <Point X="3.213360024091" Y="-1.160646556897" Z="0.1" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.1" />
                  <Point X="3.392060875718" Y="-1.117493847642" Z="0.1" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.1" />
                  <Point X="3.459412252937" Y="-1.12383437053" Z="0.1" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.1" />
                  <Point X="4.98807209231" Y="-0.959174352167" Z="0.1" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.1" />
                  <Point X="5.045759398979" Y="-0.584123001091" Z="0.1" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.1" />
                  <Point X="4.996181525904" Y="-0.555272527481" Z="0.1" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.1" />
                  <Point X="3.659131095131" Y="-0.169470157371" Z="0.1" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.1" />
                  <Point X="3.602149751973" Y="-0.09943044927" Z="0.1" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.1" />
                  <Point X="3.582172402803" Y="-0.003851869581" Z="0.1" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.1" />
                  <Point X="3.599199047621" Y="0.092758661666" Z="0.1" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.1" />
                  <Point X="3.653229686427" Y="0.164518289305" Z="0.1" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.1" />
                  <Point X="3.744264319222" Y="0.218678686871" Z="0.1" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.1" />
                  <Point X="3.799786291402" Y="0.234699405288" Z="0.1" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.1" />
                  <Point X="4.984740411784" Y="0.975564402632" Z="0.1" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.1" />
                  <Point X="4.884825397486" Y="1.39206832667" Z="0.1" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.1" />
                  <Point X="4.82426308706" Y="1.401221839101" Z="0.1" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.1" />
                  <Point X="3.372714817348" Y="1.233972416243" Z="0.1" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.1" />
                  <Point X="3.302095186752" Y="1.272108026139" Z="0.1" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.1" />
                  <Point X="3.253177054441" Y="1.343804079146" Z="0.1" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.1" />
                  <Point X="3.234296496322" Y="1.428935018446" Z="0.1" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.1" />
                  <Point X="3.254257824177" Y="1.50624512196" Z="0.1" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.1" />
                  <Point X="3.310594379199" Y="1.581689638458" Z="0.1" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.1" />
                  <Point X="3.358127324005" Y="1.619400658521" Z="0.1" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.1" />
                  <Point X="4.246521921302" Y="2.786968850875" Z="0.1" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.1" />
                  <Point X="4.011667423091" Y="3.115554015673" Z="0.1" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.1" />
                  <Point X="3.942759723567" Y="3.094273415583" Z="0.1" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.1" />
                  <Point X="2.432792675463" Y="2.246385169183" Z="0.1" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.1" />
                  <Point X="2.362934763081" Y="2.25356676144" Z="0.1" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.1" />
                  <Point X="2.299382195803" Y="2.295145351581" Z="0.1" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.1" />
                  <Point X="2.255613248654" Y="2.357642664579" Z="0.1" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.1" />
                  <Point X="2.245862941319" Y="2.426823680751" Z="0.1" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.1" />
                  <Point X="2.266142773475" Y="2.506676900419" Z="0.1" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.1" />
                  <Point X="2.301351937277" Y="2.569379358388" Z="0.1" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.1" />
                  <Point X="2.768454251838" Y="4.258395826954" Z="0.1" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.1" />
                  <Point X="2.37162045085" Y="4.491514652061" Z="0.1" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.1" />
                  <Point X="1.960447005389" Y="4.685629340961" Z="0.1" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.1" />
                  <Point X="1.533773812352" Y="4.842109845674" Z="0.1" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.1" />
                  <Point X="0.888640828225" Y="4.99993093137" Z="0.1" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.1" />
                  <Point X="0.219930167871" Y="5.073528336686" Z="0.1" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.1" />
                  <Point X="0.185539885554" Y="5.047568788626" Z="0.1" />
                  <Point X="0" Y="4.355124473572" Z="0.1" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>