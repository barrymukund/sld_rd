<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#203" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3151" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004717773438" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.902576843262" Y="20.221283203125" />
                  <Point X="0.563301940918" Y="21.4874765625" />
                  <Point X="0.557720153809" Y="21.502861328125" />
                  <Point X="0.542363342285" Y="21.532623046875" />
                  <Point X="0.403693084717" Y="21.732419921875" />
                  <Point X="0.378635528564" Y="21.7685234375" />
                  <Point X="0.356753570557" Y="21.7909765625" />
                  <Point X="0.330499145508" Y="21.810220703125" />
                  <Point X="0.302495788574" Y="21.824330078125" />
                  <Point X="0.087911506653" Y="21.890927734375" />
                  <Point X="0.049136642456" Y="21.902962890625" />
                  <Point X="0.020983924866" Y="21.907232421875" />
                  <Point X="-0.008657250404" Y="21.907234375" />
                  <Point X="-0.036823177338" Y="21.90296484375" />
                  <Point X="-0.251407455444" Y="21.836365234375" />
                  <Point X="-0.290182312012" Y="21.82433203125" />
                  <Point X="-0.318182952881" Y="21.810224609375" />
                  <Point X="-0.344438720703" Y="21.79098046875" />
                  <Point X="-0.366323120117" Y="21.7685234375" />
                  <Point X="-0.504993377686" Y="21.568724609375" />
                  <Point X="-0.53005090332" Y="21.532623046875" />
                  <Point X="-0.538187805176" Y="21.5184296875" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.612296447754" Y="21.258677734375" />
                  <Point X="-0.916584716797" Y="20.12305859375" />
                  <Point X="-1.037744262695" Y="20.146576171875" />
                  <Point X="-1.079352172852" Y="20.154654296875" />
                  <Point X="-1.24641796875" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.267772705078" Y="20.74149609375" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323645019531" Y="20.868794921875" />
                  <Point X="-1.52120690918" Y="21.042052734375" />
                  <Point X="-1.556905883789" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612885864258" Y="21.102005859375" />
                  <Point X="-1.643027587891" Y="21.109033203125" />
                  <Point X="-1.905236206055" Y="21.12621875" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983415161133" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657104492" Y="21.10519921875" />
                  <Point X="-2.261143310547" Y="20.9592109375" />
                  <Point X="-2.300623535156" Y="20.93283203125" />
                  <Point X="-2.312787353516" Y="20.923177734375" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.369521484375" Y="20.855681640625" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.740746582031" Y="20.8728671875" />
                  <Point X="-2.801726806641" Y="20.910625" />
                  <Point X="-3.104721923828" Y="21.143921875" />
                  <Point X="-3.054265625" Y="21.231314453125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412859130859" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383873046875" />
                  <Point X="-2.405575439453" Y="22.414806640625" />
                  <Point X="-2.414559570312" Y="22.444423828125" />
                  <Point X="-2.428776855469" Y="22.47325390625" />
                  <Point X="-2.446805419922" Y="22.498412109375" />
                  <Point X="-2.461499023438" Y="22.51310546875" />
                  <Point X="-2.464176513672" Y="22.51578125" />
                  <Point X="-2.489323486328" Y="22.533794921875" />
                  <Point X="-2.518150878906" Y="22.5480078125" />
                  <Point X="-2.547766601562" Y="22.55698828125" />
                  <Point X="-2.578697509766" Y="22.555974609375" />
                  <Point X="-2.610220458984" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-2.836517822266" Y="22.424869140625" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.034696289063" Y="22.142861328125" />
                  <Point X="-4.082857910156" Y="22.206134765625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.2091328125" Y="22.654986328125" />
                  <Point X="-3.105954833984" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.047331054688" Y="23.605361328125" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556396484" Y="23.672013671875" />
                  <Point X="-3.052558105469" Y="23.70176171875" />
                  <Point X="-3.068642089844" Y="23.72774609375" />
                  <Point X="-3.089474853516" Y="23.751701171875" />
                  <Point X="-3.112972412109" Y="23.771232421875" />
                  <Point X="-3.135402099609" Y="23.78443359375" />
                  <Point X="-3.139460205078" Y="23.786822265625" />
                  <Point X="-3.168729248047" Y="23.798046875" />
                  <Point X="-3.200611572266" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.481044677734" Y="23.772818359375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.815239257812" Y="23.93359375" />
                  <Point X="-4.834076660156" Y="24.007341796875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.789562011719" Y="24.442861328125" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.5174921875" Y="24.785171875" />
                  <Point X="-3.487727783203" Y="24.800529296875" />
                  <Point X="-3.464222167969" Y="24.81684375" />
                  <Point X="-3.459979003906" Y="24.8197890625" />
                  <Point X="-3.437529785156" Y="24.841666015625" />
                  <Point X="-3.418282958984" Y="24.867919921875" />
                  <Point X="-3.404168945312" Y="24.8959296875" />
                  <Point X="-3.396333740234" Y="24.92117578125" />
                  <Point X="-3.39491796875" Y="24.925736328125" />
                  <Point X="-3.390648925781" Y="24.953888671875" />
                  <Point X="-3.390647216797" Y="24.98353125" />
                  <Point X="-3.394916259766" Y="25.0116953125" />
                  <Point X="-3.402751464844" Y="25.03694140625" />
                  <Point X="-3.404167236328" Y="25.04150390625" />
                  <Point X="-3.418274902344" Y="25.069505859375" />
                  <Point X="-3.437521484375" Y="25.095765625" />
                  <Point X="-3.459981445312" Y="25.11765234375" />
                  <Point X="-3.483487060547" Y="25.13396484375" />
                  <Point X="-3.494275878906" Y="25.1404375" />
                  <Point X="-3.514089355469" Y="25.15060546875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.759956787109" Y="25.2186953125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.836627441406" Y="25.894935546875" />
                  <Point X="-4.824487792969" Y="25.976974609375" />
                  <Point X="-4.703551757812" Y="26.423267578125" />
                  <Point X="-4.666489257812" Y="26.418388671875" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744986572266" Y="26.299341796875" />
                  <Point X="-3.723421386719" Y="26.301228515625" />
                  <Point X="-3.703135986328" Y="26.305263671875" />
                  <Point X="-3.651110839844" Y="26.321666015625" />
                  <Point X="-3.641709960938" Y="26.324630859375" />
                  <Point X="-3.622771728516" Y="26.33296484375" />
                  <Point X="-3.604028808594" Y="26.343787109375" />
                  <Point X="-3.587347167969" Y="26.35601953125" />
                  <Point X="-3.573708740234" Y="26.371572265625" />
                  <Point X="-3.561295654297" Y="26.389302734375" />
                  <Point X="-3.551350830078" Y="26.407431640625" />
                  <Point X="-3.530475341797" Y="26.457828125" />
                  <Point X="-3.526703369141" Y="26.466935546875" />
                  <Point X="-3.5209140625" Y="26.486798828125" />
                  <Point X="-3.517156494141" Y="26.508115234375" />
                  <Point X="-3.5158046875" Y="26.528755859375" />
                  <Point X="-3.518952392578" Y="26.549201171875" />
                  <Point X="-3.524555419922" Y="26.570107421875" />
                  <Point X="-3.53205078125" Y="26.58937890625" />
                  <Point X="-3.557239013672" Y="26.637765625" />
                  <Point X="-3.561790527344" Y="26.6465078125" />
                  <Point X="-3.573284179688" Y="26.663708984375" />
                  <Point X="-3.587195556641" Y="26.680287109375" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-3.732389892578" Y="26.794537109375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.128322265625" Y="27.65284765625" />
                  <Point X="-4.081155761719" Y="27.73365625" />
                  <Point X="-3.750504882813" Y="28.158662109375" />
                  <Point X="-3.206656494141" Y="27.844669921875" />
                  <Point X="-3.187724853516" Y="27.83633984375" />
                  <Point X="-3.167079101562" Y="27.829830078125" />
                  <Point X="-3.146791992188" Y="27.825794921875" />
                  <Point X="-3.074335449219" Y="27.819455078125" />
                  <Point X="-3.061242675781" Y="27.818310546875" />
                  <Point X="-3.040555908203" Y="27.818763671875" />
                  <Point X="-3.019099609375" Y="27.82158984375" />
                  <Point X="-2.999009521484" Y="27.826505859375" />
                  <Point X="-2.980459716797" Y="27.835654296875" />
                  <Point X="-2.962207275391" Y="27.847283203125" />
                  <Point X="-2.946078369141" Y="27.860228515625" />
                  <Point X="-2.894648193359" Y="27.911658203125" />
                  <Point X="-2.885354736328" Y="27.920951171875" />
                  <Point X="-2.872409179688" Y="27.937080078125" />
                  <Point X="-2.860779296875" Y="27.955333984375" />
                  <Point X="-2.851629638672" Y="27.97388671875" />
                  <Point X="-2.846712646484" Y="27.99398046875" />
                  <Point X="-2.843887207031" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.849774902344" Y="28.108576171875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.927514648438" Y="28.281505859375" />
                  <Point X="-3.183332519531" Y="28.72459375" />
                  <Point X="-2.7827734375" Y="29.03169921875" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.16703515625" Y="29.3911328125" />
                  <Point X="-2.04319921875" Y="29.229748046875" />
                  <Point X="-2.028891723633" Y="29.21480078125" />
                  <Point X="-2.012312011719" Y="29.200888671875" />
                  <Point X="-1.995114624023" Y="29.189396484375" />
                  <Point X="-1.914470825195" Y="29.1474140625" />
                  <Point X="-1.89989855957" Y="29.139828125" />
                  <Point X="-1.88062487793" Y="29.13233203125" />
                  <Point X="-1.859718017578" Y="29.126728515625" />
                  <Point X="-1.839268310547" Y="29.123580078125" />
                  <Point X="-1.818621826172" Y="29.12493359375" />
                  <Point X="-1.797306396484" Y="29.128693359375" />
                  <Point X="-1.777452880859" Y="29.134482421875" />
                  <Point X="-1.693456665039" Y="29.169275390625" />
                  <Point X="-1.678278930664" Y="29.1755625" />
                  <Point X="-1.660144897461" Y="29.185509765625" />
                  <Point X="-1.642415649414" Y="29.197923828125" />
                  <Point X="-1.626863525391" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.568141235352" Y="29.352630859375" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.564292602539" Y="29.480669921875" />
                  <Point X="-1.584202026367" Y="29.6318984375" />
                  <Point X="-1.055981567383" Y="29.7799921875" />
                  <Point X="-0.949625427246" Y="29.80980859375" />
                  <Point X="-0.294711212158" Y="29.88645703125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907154" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.17578994751" Y="29.396689453125" />
                  <Point X="0.307419647217" Y="29.887939453125" />
                  <Point X="0.751177307129" Y="29.84146484375" />
                  <Point X="0.844030517578" Y="29.831740234375" />
                  <Point X="1.388745849609" Y="29.700228515625" />
                  <Point X="1.481038818359" Y="29.6779453125" />
                  <Point X="1.835352050781" Y="29.54943359375" />
                  <Point X="1.894646972656" Y="29.527927734375" />
                  <Point X="2.237479736328" Y="29.367595703125" />
                  <Point X="2.294558349609" Y="29.34090234375" />
                  <Point X="2.625790527344" Y="29.14792578125" />
                  <Point X="2.680990966797" Y="29.115765625" />
                  <Point X="2.943260253906" Y="28.929255859375" />
                  <Point X="2.877342529297" Y="28.81508203125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142076416016" Y="27.539931640625" />
                  <Point X="2.133076904297" Y="27.516056640625" />
                  <Point X="2.114893066406" Y="27.448056640625" />
                  <Point X="2.112695556641" Y="27.437447265625" />
                  <Point X="2.108071533203" Y="27.4062578125" />
                  <Point X="2.107727783203" Y="27.380953125" />
                  <Point X="2.114818115234" Y="27.322154296875" />
                  <Point X="2.116099121094" Y="27.311529296875" />
                  <Point X="2.121442138672" Y="27.28960546875" />
                  <Point X="2.129710449219" Y="27.26751171875" />
                  <Point X="2.140072265625" Y="27.24746875" />
                  <Point X="2.176455566406" Y="27.19384765625" />
                  <Point X="2.183204345703" Y="27.185052734375" />
                  <Point X="2.203077148438" Y="27.162068359375" />
                  <Point X="2.221599609375" Y="27.145591796875" />
                  <Point X="2.275209472656" Y="27.10921484375" />
                  <Point X="2.284892578125" Y="27.102642578125" />
                  <Point X="2.30494140625" Y="27.092275390625" />
                  <Point X="2.327034912109" Y="27.084005859375" />
                  <Point X="2.348964355469" Y="27.078662109375" />
                  <Point X="2.407764160156" Y="27.071572265625" />
                  <Point X="2.419318603516" Y="27.070888671875" />
                  <Point X="2.448846435547" Y="27.0709453125" />
                  <Point X="2.473205810547" Y="27.074169921875" />
                  <Point X="2.541204833984" Y="27.092353515625" />
                  <Point X="2.547254150391" Y="27.094189453125" />
                  <Point X="2.571624755859" Y="27.102478515625" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="2.816932861328" Y="27.242009765625" />
                  <Point X="3.967326416016" Y="27.906189453125" />
                  <Point X="4.090535400391" Y="27.73495703125" />
                  <Point X="4.123274414062" Y="27.689458984375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="4.191583496094" Y="27.40569921875" />
                  <Point X="3.230784179688" Y="26.668451171875" />
                  <Point X="3.221424316406" Y="26.660240234375" />
                  <Point X="3.203973632812" Y="26.641626953125" />
                  <Point X="3.155034667969" Y="26.577783203125" />
                  <Point X="3.14924609375" Y="26.569322265625" />
                  <Point X="3.131933349609" Y="26.54083203125" />
                  <Point X="3.121629638672" Y="26.517083984375" />
                  <Point X="3.103399658203" Y="26.4518984375" />
                  <Point X="3.100105712891" Y="26.440119140625" />
                  <Point X="3.096652099609" Y="26.417818359375" />
                  <Point X="3.095836425781" Y="26.39425" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.112704345703" Y="26.299240234375" />
                  <Point X="3.115391601562" Y="26.289087890625" />
                  <Point X="3.125293212891" Y="26.25860546875" />
                  <Point X="3.136282470703" Y="26.235740234375" />
                  <Point X="3.176985107422" Y="26.173875" />
                  <Point X="3.184340087891" Y="26.1626953125" />
                  <Point X="3.198892578125" Y="26.145451171875" />
                  <Point X="3.216136230469" Y="26.129361328125" />
                  <Point X="3.234345947266" Y="26.11603515625" />
                  <Point X="3.293329589844" Y="26.08283203125" />
                  <Point X="3.303218994141" Y="26.077998046875" />
                  <Point X="3.331854492188" Y="26.066" />
                  <Point X="3.356120361328" Y="26.0594375" />
                  <Point X="3.435870361328" Y="26.0488984375" />
                  <Point X="3.441737304688" Y="26.04830859375" />
                  <Point X="3.469224609375" Y="26.046400390625" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.705167236328" Y="26.075546875" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.831875976562" Y="25.990560546875" />
                  <Point X="4.845937011719" Y="25.932806640625" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.817676269531" Y="25.62462890625" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788818359" Y="25.325583984375" />
                  <Point X="3.681547363281" Y="25.315068359375" />
                  <Point X="3.603195556641" Y="25.26978125" />
                  <Point X="3.595040039062" Y="25.2644921875" />
                  <Point X="3.566569580078" Y="25.243888671875" />
                  <Point X="3.547530517578" Y="25.225578125" />
                  <Point X="3.500519287109" Y="25.165673828125" />
                  <Point X="3.492024658203" Y="25.154849609375" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.448010253906" Y="25.010779296875" />
                  <Point X="3.446650390625" Y="25.000888671875" />
                  <Point X="3.443818847656" Y="24.967291015625" />
                  <Point X="3.445178710938" Y="24.941443359375" />
                  <Point X="3.460849121094" Y="24.859619140625" />
                  <Point X="3.463680908203" Y="24.844833984375" />
                  <Point X="3.470528320312" Y="24.823330078125" />
                  <Point X="3.480302490234" Y="24.8018671875" />
                  <Point X="3.492024902344" Y="24.782591796875" />
                  <Point X="3.539035888672" Y="24.7226875" />
                  <Point X="3.545959960938" Y="24.714802734375" />
                  <Point X="3.568766357422" Y="24.69155859375" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.667387695312" Y="24.6305546875" />
                  <Point X="3.672257324219" Y="24.62792578125" />
                  <Point X="3.69849609375" Y="24.614734375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="3.915545898438" Y="24.55453515625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.862873046875" Y="24.10334375" />
                  <Point X="4.855022460938" Y="24.05126953125" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="4.701344238281" Y="23.828443359375" />
                  <Point X="3.424381591797" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374659667969" Y="23.994490234375" />
                  <Point X="3.220882568359" Y="23.96106640625" />
                  <Point X="3.193095458984" Y="23.95502734375" />
                  <Point X="3.163975341797" Y="23.943404296875" />
                  <Point X="3.136148193359" Y="23.92651171875" />
                  <Point X="3.112397705078" Y="23.9060390625" />
                  <Point X="3.01944921875" Y="23.79425" />
                  <Point X="3.002653564453" Y="23.77405078125" />
                  <Point X="2.987933837891" Y="23.749671875" />
                  <Point X="2.976590087891" Y="23.722287109375" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.956435791016" Y="23.54986328125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079589844" Y="23.460810546875" />
                  <Point X="2.976450927734" Y="23.432" />
                  <Point X="3.061553466797" Y="23.29962890625" />
                  <Point X="3.076931152344" Y="23.2757109375" />
                  <Point X="3.086935058594" Y="23.2627578125" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.295269775391" Y="23.097408203125" />
                  <Point X="4.213121582031" Y="22.3931171875" />
                  <Point X="4.146938964844" Y="22.2860234375" />
                  <Point X="4.124807128906" Y="22.250208984375" />
                  <Point X="4.028981689453" Y="22.1140546875" />
                  <Point X="3.93826171875" Y="22.166431640625" />
                  <Point X="2.800954345703" Y="22.823056640625" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.571205078125" Y="22.8732265625" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611572266" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.292789550781" Y="22.784802734375" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242385986328" Y="22.753451171875" />
                  <Point X="2.221426269531" Y="22.7324921875" />
                  <Point X="2.204532226563" Y="22.709560546875" />
                  <Point X="2.124512451172" Y="22.557515625" />
                  <Point X="2.110053222656" Y="22.53004296875" />
                  <Point X="2.100229736328" Y="22.500267578125" />
                  <Point X="2.095271484375" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.128728515625" Y="22.25372265625" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.270470214844" Y="21.968412109375" />
                  <Point X="2.861284423828" Y="20.94509375" />
                  <Point X="2.808111572266" Y="20.90711328125" />
                  <Point X="2.781883789062" Y="20.88837890625" />
                  <Point X="2.701763671875" Y="20.83651953125" />
                  <Point X="2.626416503906" Y="20.934712890625" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.747504516602" Y="22.0778203125" />
                  <Point X="1.721923095703" Y="22.099443359375" />
                  <Point X="1.541416748047" Y="22.2154921875" />
                  <Point X="1.508799804688" Y="22.2364609375" />
                  <Point X="1.479987426758" Y="22.24883203125" />
                  <Point X="1.448369140625" Y="22.2565625" />
                  <Point X="1.41710168457" Y="22.258880859375" />
                  <Point X="1.219687011719" Y="22.24071484375" />
                  <Point X="1.184014770508" Y="22.23743359375" />
                  <Point X="1.1563671875" Y="22.230603515625" />
                  <Point X="1.128978881836" Y="22.219259765625" />
                  <Point X="1.104594604492" Y="22.2045390625" />
                  <Point X="0.95215612793" Y="22.077791015625" />
                  <Point X="0.924610900879" Y="22.05488671875" />
                  <Point X="0.904138916016" Y="22.0311328125" />
                  <Point X="0.887247680664" Y="22.003306640625" />
                  <Point X="0.875624511719" Y="21.974189453125" />
                  <Point X="0.830046081543" Y="21.764494140625" />
                  <Point X="0.821810241699" Y="21.726603515625" />
                  <Point X="0.81972479248" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.853367004395" Y="21.421474609375" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="1.00052142334" Y="20.135361328125" />
                  <Point X="0.97571270752" Y="20.129923828125" />
                  <Point X="0.929315246582" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.019642272949" Y="20.2398359375" />
                  <Point X="-1.05844934082" Y="20.24737109375" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.174598144531" Y="20.760029296875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905138671875" />
                  <Point X="-1.250209838867" Y="20.9290625" />
                  <Point X="-1.261007080078" Y="20.94021875" />
                  <Point X="-1.458568969727" Y="21.1134765625" />
                  <Point X="-1.494268066406" Y="21.144783203125" />
                  <Point X="-1.506735595703" Y="21.15403125" />
                  <Point X="-1.533018798828" Y="21.170376953125" />
                  <Point X="-1.546834350586" Y="21.177474609375" />
                  <Point X="-1.576530883789" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.621457397461" Y="21.201552734375" />
                  <Point X="-1.636814453125" Y="21.203830078125" />
                  <Point X="-1.899023071289" Y="21.221015625" />
                  <Point X="-1.946403564453" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.992726196289" Y="21.220833984375" />
                  <Point X="-2.008000488281" Y="21.2180546875" />
                  <Point X="-2.039048583984" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095436279297" Y="21.184189453125" />
                  <Point X="-2.313922607422" Y="21.038201171875" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359682861328" Y="21.0072421875" />
                  <Point X="-2.380446533203" Y="20.989865234375" />
                  <Point X="-2.402761962891" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.444890136719" Y="20.913513671875" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.690735595703" Y="20.953638671875" />
                  <Point X="-2.747613037109" Y="20.98885546875" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.971993164062" Y="21.183814453125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684570312" Y="22.333814453125" />
                  <Point X="-2.313413574219" Y="22.36533984375" />
                  <Point X="-2.311638916016" Y="22.380763671875" />
                  <Point X="-2.310626220703" Y="22.411697265625" />
                  <Point X="-2.314666015625" Y="22.4423828125" />
                  <Point X="-2.323650146484" Y="22.472" />
                  <Point X="-2.329356445312" Y="22.48644140625" />
                  <Point X="-2.343573730469" Y="22.515271484375" />
                  <Point X="-2.351557128906" Y="22.52858984375" />
                  <Point X="-2.369585693359" Y="22.553748046875" />
                  <Point X="-2.379630859375" Y="22.565587890625" />
                  <Point X="-2.394324462891" Y="22.58028125" />
                  <Point X="-2.408854003906" Y="22.59301171875" />
                  <Point X="-2.434000976562" Y="22.611025390625" />
                  <Point X="-2.447313720703" Y="22.619001953125" />
                  <Point X="-2.476141113281" Y="22.63321484375" />
                  <Point X="-2.490583251953" Y="22.638919921875" />
                  <Point X="-2.520198974609" Y="22.647900390625" />
                  <Point X="-2.550878173828" Y="22.6519375" />
                  <Point X="-2.581809082031" Y="22.650923828125" />
                  <Point X="-2.597234375" Y="22.6491484375" />
                  <Point X="-2.628757324219" Y="22.642876953125" />
                  <Point X="-2.643687744141" Y="22.63861328125" />
                  <Point X="-2.672651123047" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-2.884018066406" Y="22.507140625" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.959103027344" Y="22.200400390625" />
                  <Point X="-4.004018066406" Y="22.259408203125" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-4.15130078125" Y="22.5796171875" />
                  <Point X="-3.048122802734" Y="23.426115234375" />
                  <Point X="-3.036482421875" Y="23.4366875" />
                  <Point X="-3.015105224609" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.955365478516" Y="23.58154296875" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950786621094" Y="23.678623046875" />
                  <Point X="-2.953083251953" Y="23.693779296875" />
                  <Point X="-2.960084960938" Y="23.72352734375" />
                  <Point X="-2.971780761719" Y="23.75176171875" />
                  <Point X="-2.987864746094" Y="23.77774609375" />
                  <Point X="-2.996958007812" Y="23.790087890625" />
                  <Point X="-3.017790771484" Y="23.81404296875" />
                  <Point X="-3.028749267578" Y="23.8247578125" />
                  <Point X="-3.052246826172" Y="23.8442890625" />
                  <Point X="-3.064785888672" Y="23.85310546875" />
                  <Point X="-3.087215576172" Y="23.866306640625" />
                  <Point X="-3.105443603516" Y="23.8755234375" />
                  <Point X="-3.134712646484" Y="23.886748046875" />
                  <Point X="-3.149811767578" Y="23.89114453125" />
                  <Point X="-3.181694091797" Y="23.897623046875" />
                  <Point X="-3.197307617188" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244329345703" Y="23.899802734375" />
                  <Point X="-3.493444824219" Y="23.867005859375" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.723194335938" Y="23.95710546875" />
                  <Point X="-4.740761230469" Y="24.025880859375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.764973632813" Y="24.35109765625" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.500472412109" Y="24.690287109375" />
                  <Point X="-3.473931884766" Y="24.700748046875" />
                  <Point X="-3.444167480469" Y="24.71610546875" />
                  <Point X="-3.433560058594" Y="24.722484375" />
                  <Point X="-3.410054443359" Y="24.738798828125" />
                  <Point X="-3.393676757812" Y="24.751751953125" />
                  <Point X="-3.371227539062" Y="24.77362890625" />
                  <Point X="-3.360912841797" Y="24.785498046875" />
                  <Point X="-3.341666015625" Y="24.811751953125" />
                  <Point X="-3.333445068359" Y="24.825169921875" />
                  <Point X="-3.319331054688" Y="24.8531796875" />
                  <Point X="-3.313437988281" Y="24.867771484375" />
                  <Point X="-3.305602783203" Y="24.893017578125" />
                  <Point X="-3.300991699219" Y="24.911494140625" />
                  <Point X="-3.29672265625" Y="24.939646484375" />
                  <Point X="-3.295648925781" Y="24.9538828125" />
                  <Point X="-3.295647216797" Y="24.983525390625" />
                  <Point X="-3.296720214844" Y="24.997767578125" />
                  <Point X="-3.300989257812" Y="25.025931640625" />
                  <Point X="-3.304185302734" Y="25.039853515625" />
                  <Point X="-3.312020507812" Y="25.065099609375" />
                  <Point X="-3.319326416016" Y="25.084248046875" />
                  <Point X="-3.333434082031" Y="25.11225" />
                  <Point X="-3.341651611328" Y="25.125666015625" />
                  <Point X="-3.360898193359" Y="25.15192578125" />
                  <Point X="-3.371220214844" Y="25.163802734375" />
                  <Point X="-3.393680175781" Y="25.185689453125" />
                  <Point X="-3.405818115234" Y="25.19569921875" />
                  <Point X="-3.429323730469" Y="25.21201171875" />
                  <Point X="-3.450901367188" Y="25.22495703125" />
                  <Point X="-3.47071484375" Y="25.235125" />
                  <Point X="-3.479910644531" Y="25.239244140625" />
                  <Point X="-3.498697509766" Y="25.24648828125" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.735369140625" Y="25.310458984375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.742650878906" Y="25.881029296875" />
                  <Point X="-4.731331054688" Y="25.95752734375" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-3.778065185547" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.74705859375" Y="26.204365234375" />
                  <Point X="-3.736706787109" Y="26.204703125" />
                  <Point X="-3.715141601563" Y="26.20658984375" />
                  <Point X="-3.704887207031" Y="26.2080546875" />
                  <Point X="-3.684601806641" Y="26.21208984375" />
                  <Point X="-3.674570800781" Y="26.21466015625" />
                  <Point X="-3.622545654297" Y="26.2310625" />
                  <Point X="-3.6034453125" Y="26.237677734375" />
                  <Point X="-3.584507080078" Y="26.24601171875" />
                  <Point X="-3.575268310547" Y="26.2506953125" />
                  <Point X="-3.556525390625" Y="26.261517578125" />
                  <Point X="-3.5478515625" Y="26.267177734375" />
                  <Point X="-3.531169921875" Y="26.27941015625" />
                  <Point X="-3.515920166016" Y="26.293384765625" />
                  <Point X="-3.502281738281" Y="26.3089375" />
                  <Point X="-3.495885253906" Y="26.317087890625" />
                  <Point X="-3.483472167969" Y="26.334818359375" />
                  <Point X="-3.478004638672" Y="26.34361328125" />
                  <Point X="-3.468059814453" Y="26.3617421875" />
                  <Point X="-3.463582519531" Y="26.371076171875" />
                  <Point X="-3.44270703125" Y="26.42147265625" />
                  <Point X="-3.435498291016" Y="26.440353515625" />
                  <Point X="-3.429708984375" Y="26.460216796875" />
                  <Point X="-3.427356445313" Y="26.470306640625" />
                  <Point X="-3.423598876953" Y="26.491623046875" />
                  <Point X="-3.422359619141" Y="26.50190625" />
                  <Point X="-3.4210078125" Y="26.522546875" />
                  <Point X="-3.421910888672" Y="26.5432109375" />
                  <Point X="-3.42505859375" Y="26.56365625" />
                  <Point X="-3.427190673828" Y="26.573794921875" />
                  <Point X="-3.432793701172" Y="26.594701171875" />
                  <Point X="-3.436016357422" Y="26.60454296875" />
                  <Point X="-3.44351171875" Y="26.623814453125" />
                  <Point X="-3.447784423828" Y="26.633244140625" />
                  <Point X="-3.47297265625" Y="26.681630859375" />
                  <Point X="-3.482801269531" Y="26.699287109375" />
                  <Point X="-3.494294921875" Y="26.71648828125" />
                  <Point X="-3.500511474609" Y="26.724775390625" />
                  <Point X="-3.514422851562" Y="26.741353515625" />
                  <Point X="-3.521500976563" Y="26.74891015625" />
                  <Point X="-3.53644140625" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-3.674557617188" Y="26.86990625" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.046275878906" Y="27.604958984375" />
                  <Point X="-4.002295654297" Y="27.68030859375" />
                  <Point X="-3.726338378906" Y="28.035013671875" />
                  <Point X="-3.254156738281" Y="27.7623984375" />
                  <Point X="-3.244917236328" Y="27.75771484375" />
                  <Point X="-3.225985595703" Y="27.749384765625" />
                  <Point X="-3.216292724609" Y="27.745736328125" />
                  <Point X="-3.195646972656" Y="27.7392265625" />
                  <Point X="-3.185611816406" Y="27.73665625" />
                  <Point X="-3.165324707031" Y="27.73262109375" />
                  <Point X="-3.155072753906" Y="27.73115625" />
                  <Point X="-3.082616210938" Y="27.72481640625" />
                  <Point X="-3.059162353516" Y="27.723333984375" />
                  <Point X="-3.038475585938" Y="27.723787109375" />
                  <Point X="-3.028149902344" Y="27.724578125" />
                  <Point X="-3.006693603516" Y="27.727404296875" />
                  <Point X="-2.99651953125" Y="27.7293125" />
                  <Point X="-2.976429443359" Y="27.734228515625" />
                  <Point X="-2.956989501953" Y="27.7413046875" />
                  <Point X="-2.938439697266" Y="27.750453125" />
                  <Point X="-2.929413818359" Y="27.755533203125" />
                  <Point X="-2.911161376953" Y="27.767162109375" />
                  <Point X="-2.902743164062" Y="27.7731953125" />
                  <Point X="-2.886614257812" Y="27.786140625" />
                  <Point X="-2.878903564453" Y="27.793052734375" />
                  <Point X="-2.827473388672" Y="27.844482421875" />
                  <Point X="-2.811267333984" Y="27.861486328125" />
                  <Point X="-2.798321777344" Y="27.877615234375" />
                  <Point X="-2.792288818359" Y="27.886033203125" />
                  <Point X="-2.780658935547" Y="27.904287109375" />
                  <Point X="-2.775577148438" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759352294922" Y="27.951306640625" />
                  <Point X="-2.754435302734" Y="27.971400390625" />
                  <Point X="-2.752525634766" Y="27.981580078125" />
                  <Point X="-2.749700195312" Y="28.0030390625" />
                  <Point X="-2.748909912109" Y="28.013365234375" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.755136474609" Y="28.11685546875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.845242431641" Y="28.329005859375" />
                  <Point X="-3.059386474609" Y="28.699912109375" />
                  <Point X="-2.724971191406" Y="28.956306640625" />
                  <Point X="-2.648368408203" Y="29.015037109375" />
                  <Point X="-2.192522460938" Y="29.268294921875" />
                  <Point X="-2.118567626953" Y="29.171916015625" />
                  <Point X="-2.111826904297" Y="29.16405859375" />
                  <Point X="-2.09751953125" Y="29.149111328125" />
                  <Point X="-2.089956787109" Y="29.14202734375" />
                  <Point X="-2.073376953125" Y="29.128115234375" />
                  <Point X="-2.065095214844" Y="29.12190234375" />
                  <Point X="-2.047897827148" Y="29.11041015625" />
                  <Point X="-2.038982299805" Y="29.105130859375" />
                  <Point X="-1.958338623047" Y="29.0631484375" />
                  <Point X="-1.934333984375" Y="29.0512890625" />
                  <Point X="-1.915060302734" Y="29.04379296875" />
                  <Point X="-1.905218994141" Y="29.0405703125" />
                  <Point X="-1.884312133789" Y="29.034966796875" />
                  <Point X="-1.874174072266" Y="29.032833984375" />
                  <Point X="-1.853724243164" Y="29.029685546875" />
                  <Point X="-1.833053710938" Y="29.028783203125" />
                  <Point X="-1.812407226562" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.780804321289" Y="29.03513671875" />
                  <Point X="-1.770712890625" Y="29.0374921875" />
                  <Point X="-1.750859375" Y="29.04328125" />
                  <Point X="-1.741097412109" Y="29.04671484375" />
                  <Point X="-1.657101196289" Y="29.0815078125" />
                  <Point X="-1.63258996582" Y="29.092271484375" />
                  <Point X="-1.614455932617" Y="29.10221875" />
                  <Point X="-1.605655395508" Y="29.107689453125" />
                  <Point X="-1.587926147461" Y="29.120103515625" />
                  <Point X="-1.579778198242" Y="29.126498046875" />
                  <Point X="-1.564225952148" Y="29.14013671875" />
                  <Point X="-1.550250488281" Y="29.155388671875" />
                  <Point X="-1.53801953125" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516854370117" Y="29.20873046875" />
                  <Point X="-1.508524169922" Y="29.2276640625" />
                  <Point X="-1.504877319336" Y="29.23735546875" />
                  <Point X="-1.477538085938" Y="29.324064453125" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.47010534668" Y="29.4930703125" />
                  <Point X="-1.479266357422" Y="29.56265625" />
                  <Point X="-1.0303359375" Y="29.68851953125" />
                  <Point X="-0.931167419434" Y="29.7163203125" />
                  <Point X="-0.365222381592" Y="29.782556640625" />
                  <Point X="-0.225666183472" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.267552886963" Y="29.3721015625" />
                  <Point X="0.378190856934" Y="29.785009765625" />
                  <Point X="0.741282165527" Y="29.746982421875" />
                  <Point X="0.827851318359" Y="29.737916015625" />
                  <Point X="1.366450439453" Y="29.607880859375" />
                  <Point X="1.453621948242" Y="29.586833984375" />
                  <Point X="1.802959838867" Y="29.460126953125" />
                  <Point X="1.858248535156" Y="29.44007421875" />
                  <Point X="2.197234863281" Y="29.281541015625" />
                  <Point X="2.250431396484" Y="29.2566640625" />
                  <Point X="2.577967529297" Y="29.06583984375" />
                  <Point X="2.629450683594" Y="29.035845703125" />
                  <Point X="2.817779785156" Y="28.90191796875" />
                  <Point X="2.795069824219" Y="28.86258203125" />
                  <Point X="2.065308349609" Y="27.59859765625" />
                  <Point X="2.062372558594" Y="27.593103515625" />
                  <Point X="2.053182128906" Y="27.573439453125" />
                  <Point X="2.044182373047" Y="27.549564453125" />
                  <Point X="2.041301635742" Y="27.54059765625" />
                  <Point X="2.023117797852" Y="27.47259765625" />
                  <Point X="2.01872277832" Y="27.45137890625" />
                  <Point X="2.014098632812" Y="27.420189453125" />
                  <Point X="2.013080200195" Y="27.407548828125" />
                  <Point X="2.012736694336" Y="27.382244140625" />
                  <Point X="2.013410888672" Y="27.369580078125" />
                  <Point X="2.020501342773" Y="27.31078125" />
                  <Point X="2.023800537109" Y="27.28903515625" />
                  <Point X="2.029143554687" Y="27.267111328125" />
                  <Point X="2.032468505859" Y="27.25630859375" />
                  <Point X="2.040736938477" Y="27.23421484375" />
                  <Point X="2.045320678711" Y="27.223884765625" />
                  <Point X="2.055682617188" Y="27.203841796875" />
                  <Point X="2.061460449219" Y="27.19412890625" />
                  <Point X="2.09784375" Y="27.1405078125" />
                  <Point X="2.111341308594" Y="27.12291796875" />
                  <Point X="2.131214111328" Y="27.09993359375" />
                  <Point X="2.139936523438" Y="27.091087890625" />
                  <Point X="2.158458984375" Y="27.074611328125" />
                  <Point X="2.168258056641" Y="27.06698046875" />
                  <Point X="2.221867919922" Y="27.030603515625" />
                  <Point X="2.221868408203" Y="27.030603515625" />
                  <Point X="2.241257080078" Y="27.0182578125" />
                  <Point X="2.261305908203" Y="27.007890625" />
                  <Point X="2.271639648438" Y="27.003302734375" />
                  <Point X="2.293733154297" Y="26.995033203125" />
                  <Point X="2.304543457031" Y="26.99170703125" />
                  <Point X="2.326472900391" Y="26.98636328125" />
                  <Point X="2.337592041016" Y="26.984345703125" />
                  <Point X="2.396391845703" Y="26.977255859375" />
                  <Point X="2.419500732422" Y="26.975888671875" />
                  <Point X="2.449028564453" Y="26.9759453125" />
                  <Point X="2.461313476562" Y="26.976767578125" />
                  <Point X="2.485672851562" Y="26.9799921875" />
                  <Point X="2.497747314453" Y="26.98239453125" />
                  <Point X="2.565746337891" Y="27.000578125" />
                  <Point X="2.577844970703" Y="27.00425" />
                  <Point X="2.602215576172" Y="27.0125390625" />
                  <Point X="2.610851806641" Y="27.015955078125" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="2.864432617188" Y="27.159736328125" />
                  <Point X="3.940405029297" Y="27.78094921875" />
                  <Point X="4.013422851562" Y="27.679470703125" />
                  <Point X="4.043957763672" Y="27.63703515625" />
                  <Point X="4.136884277344" Y="27.48347265625" />
                  <Point X="4.133751953125" Y="27.481068359375" />
                  <Point X="3.172952880859" Y="26.7438203125" />
                  <Point X="3.168135253906" Y="26.7398671875" />
                  <Point X="3.152119628906" Y="26.725216796875" />
                  <Point X="3.134668945312" Y="26.706603515625" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.079637451172" Y="26.635578125" />
                  <Point X="3.068060302734" Y="26.61865625" />
                  <Point X="3.050747558594" Y="26.590166015625" />
                  <Point X="3.044782714844" Y="26.57864453125" />
                  <Point X="3.034479003906" Y="26.554896484375" />
                  <Point X="3.030140136719" Y="26.542669921875" />
                  <Point X="3.01191015625" Y="26.477484375" />
                  <Point X="3.006224853516" Y="26.454658203125" />
                  <Point X="3.002771240234" Y="26.432357421875" />
                  <Point X="3.001708984375" Y="26.421103515625" />
                  <Point X="3.000893310547" Y="26.39753515625" />
                  <Point X="3.001175048828" Y="26.386236328125" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.019664306641" Y="26.28004296875" />
                  <Point X="3.025038818359" Y="26.25973828125" />
                  <Point X="3.034940429688" Y="26.229255859375" />
                  <Point X="3.039668945312" Y="26.217453125" />
                  <Point X="3.050658203125" Y="26.194587890625" />
                  <Point X="3.056918945312" Y="26.183525390625" />
                  <Point X="3.097621582031" Y="26.12166015625" />
                  <Point X="3.111738037109" Y="26.10142578125" />
                  <Point X="3.126290527344" Y="26.084181640625" />
                  <Point X="3.134081542969" Y="26.0759921875" />
                  <Point X="3.151325195312" Y="26.05990234375" />
                  <Point X="3.160032226562" Y="26.052697265625" />
                  <Point X="3.178241943359" Y="26.03937109375" />
                  <Point X="3.187744628906" Y="26.03325" />
                  <Point X="3.246728271484" Y="26.000046875" />
                  <Point X="3.266507080078" Y="25.99037890625" />
                  <Point X="3.295142578125" Y="25.978380859375" />
                  <Point X="3.307053466797" Y="25.974294921875" />
                  <Point X="3.331319335938" Y="25.967732421875" />
                  <Point X="3.343674316406" Y="25.965255859375" />
                  <Point X="3.423424316406" Y="25.954716796875" />
                  <Point X="3.435158203125" Y="25.953537109375" />
                  <Point X="3.462645507813" Y="25.95162890625" />
                  <Point X="3.472146484375" Y="25.9514453125" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.717566650391" Y="25.981359375" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.739571777344" Y="25.96808984375" />
                  <Point X="4.752684570312" Y="25.91423046875" />
                  <Point X="4.783870605469" Y="25.713921875" />
                  <Point X="3.691991210938" Y="25.421353515625" />
                  <Point X="3.686020263672" Y="25.419541015625" />
                  <Point X="3.665627929688" Y="25.41213671875" />
                  <Point X="3.642386474609" Y="25.40162109375" />
                  <Point X="3.634007568359" Y="25.397318359375" />
                  <Point X="3.555655761719" Y="25.35203125" />
                  <Point X="3.539344726562" Y="25.341453125" />
                  <Point X="3.510874267578" Y="25.320849609375" />
                  <Point X="3.500717285156" Y="25.312361328125" />
                  <Point X="3.481678222656" Y="25.29405078125" />
                  <Point X="3.472796142578" Y="25.284228515625" />
                  <Point X="3.425784912109" Y="25.22432421875" />
                  <Point X="3.410853271484" Y="25.20420703125" />
                  <Point X="3.39912890625" Y="25.18492578125" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004150391" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.354705810547" Y="25.0286484375" />
                  <Point X="3.351986083984" Y="25.0088671875" />
                  <Point X="3.349154541016" Y="24.97526953125" />
                  <Point X="3.348949951172" Y="24.96230078125" />
                  <Point X="3.350309814453" Y="24.936453125" />
                  <Point X="3.351874267578" Y="24.92357421875" />
                  <Point X="3.367544677734" Y="24.84175" />
                  <Point X="3.373159423828" Y="24.816009765625" />
                  <Point X="3.380006835938" Y="24.794505859375" />
                  <Point X="3.384071289062" Y="24.78395703125" />
                  <Point X="3.393845458984" Y="24.762494140625" />
                  <Point X="3.399134033203" Y="24.75250390625" />
                  <Point X="3.410856445312" Y="24.733228515625" />
                  <Point X="3.417290283203" Y="24.723943359375" />
                  <Point X="3.464301269531" Y="24.6640390625" />
                  <Point X="3.478149414062" Y="24.64826953125" />
                  <Point X="3.500955810547" Y="24.625025390625" />
                  <Point X="3.510557861328" Y="24.61648046875" />
                  <Point X="3.530827148438" Y="24.600765625" />
                  <Point X="3.541494384766" Y="24.593595703125" />
                  <Point X="3.619846435547" Y="24.548306640625" />
                  <Point X="3.629585693359" Y="24.543048828125" />
                  <Point X="3.655824462891" Y="24.529857421875" />
                  <Point X="3.664687011719" Y="24.525953125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="3.890958251953" Y="24.462771484375" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.768934570312" Y="24.117505859375" />
                  <Point X="4.76161328125" Y="24.06894140625" />
                  <Point X="4.727802734375" Y="23.920779296875" />
                  <Point X="4.713744140625" Y="23.922630859375" />
                  <Point X="3.43678125" Y="24.09074609375" />
                  <Point X="3.428622558594" Y="24.09146484375" />
                  <Point X="3.400096679688" Y="24.09195703125" />
                  <Point X="3.366720947266" Y="24.089158203125" />
                  <Point X="3.354482177734" Y="24.087322265625" />
                  <Point X="3.200705078125" Y="24.0538984375" />
                  <Point X="3.17291796875" Y="24.047859375" />
                  <Point X="3.157878662109" Y="24.0432578125" />
                  <Point X="3.128758544922" Y="24.031634765625" />
                  <Point X="3.114677734375" Y="24.02461328125" />
                  <Point X="3.086850585938" Y="24.007720703125" />
                  <Point X="3.074122314453" Y="23.99846875" />
                  <Point X="3.050371826172" Y="23.97799609375" />
                  <Point X="3.039349609375" Y="23.966775390625" />
                  <Point X="2.946401123047" Y="23.854986328125" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921328125" Y="23.823154296875" />
                  <Point X="2.906608398438" Y="23.798775390625" />
                  <Point X="2.900166015625" Y="23.786029296875" />
                  <Point X="2.888822265625" Y="23.75864453125" />
                  <Point X="2.884363769531" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.861835449219" Y="23.558568359375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871798095703" Y="23.43824609375" />
                  <Point X="2.876787109375" Y="23.423326171875" />
                  <Point X="2.889158447266" Y="23.394515625" />
                  <Point X="2.896540771484" Y="23.380625" />
                  <Point X="2.981643310547" Y="23.24825390625" />
                  <Point X="2.997020996094" Y="23.2243359375" />
                  <Point X="3.001744140625" Y="23.217642578125" />
                  <Point X="3.019795654297" Y="23.195546875" />
                  <Point X="3.043488769531" Y="23.17187890625" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.237437255859" Y="23.0220390625" />
                  <Point X="4.087169677734" Y="22.370017578125" />
                  <Point X="4.066125488281" Y="22.33596484375" />
                  <Point X="4.045490234375" Y="22.302572265625" />
                  <Point X="4.001274902344" Y="22.239748046875" />
                  <Point X="3.985761474609" Y="22.248705078125" />
                  <Point X="2.848454101562" Y="22.905330078125" />
                  <Point X="2.841182617188" Y="22.909119140625" />
                  <Point X="2.815026123047" Y="22.920484375" />
                  <Point X="2.78312109375" Y="22.930671875" />
                  <Point X="2.771107910156" Y="22.933662109375" />
                  <Point X="2.588088623047" Y="22.96671484375" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444847412109" Y="22.96486328125" />
                  <Point X="2.415069580078" Y="22.9550390625" />
                  <Point X="2.400589355469" Y="22.948890625" />
                  <Point X="2.248545166016" Y="22.86887109375" />
                  <Point X="2.221071289062" Y="22.854412109375" />
                  <Point X="2.208968261719" Y="22.846828125" />
                  <Point X="2.186038574219" Y="22.829935546875" />
                  <Point X="2.175211914062" Y="22.820626953125" />
                  <Point X="2.154252197266" Y="22.79966796875" />
                  <Point X="2.144941162109" Y="22.78883984375" />
                  <Point X="2.128047119141" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.040444213867" Y="22.601759765625" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836303711" Y="22.559806640625" />
                  <Point X="2.010012817383" Y="22.53003125" />
                  <Point X="2.006338134766" Y="22.514736328125" />
                  <Point X="2.001379882812" Y="22.4825625" />
                  <Point X="2.000279418945" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.035240844727" Y="22.23683984375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236816406" Y="22.168453125" />
                  <Point X="2.0640703125" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.188197998047" Y="21.920912109375" />
                  <Point X="2.73589453125" Y="20.972275390625" />
                  <Point X="2.723752197266" Y="20.963916015625" />
                  <Point X="2.701784912109" Y="20.992544921875" />
                  <Point X="1.833914306641" Y="22.123576171875" />
                  <Point X="1.828657470703" Y="22.12984765625" />
                  <Point X="1.808831420898" Y="22.150373046875" />
                  <Point X="1.78325" Y="22.17199609375" />
                  <Point X="1.773297851562" Y="22.179353515625" />
                  <Point X="1.592791503906" Y="22.29540234375" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.546280883789" Y="22.32375390625" />
                  <Point X="1.517468505859" Y="22.336125" />
                  <Point X="1.502549804688" Y="22.34111328125" />
                  <Point X="1.470931518555" Y="22.34884375" />
                  <Point X="1.455393676758" Y="22.351302734375" />
                  <Point X="1.424126220703" Y="22.35362109375" />
                  <Point X="1.408396484375" Y="22.35348046875" />
                  <Point X="1.210981933594" Y="22.335314453125" />
                  <Point X="1.175309814453" Y="22.332033203125" />
                  <Point X="1.161230834961" Y="22.32966015625" />
                  <Point X="1.133583251953" Y="22.322830078125" />
                  <Point X="1.120014648438" Y="22.318373046875" />
                  <Point X="1.092626342773" Y="22.307029296875" />
                  <Point X="1.079880981445" Y="22.300587890625" />
                  <Point X="1.055496459961" Y="22.2858671875" />
                  <Point X="1.043857421875" Y="22.277587890625" />
                  <Point X="0.891418945312" Y="22.15083984375" />
                  <Point X="0.863873718262" Y="22.127935546875" />
                  <Point X="0.852648681641" Y="22.11690625" />
                  <Point X="0.832176635742" Y="22.09315234375" />
                  <Point X="0.822929931641" Y="22.0804296875" />
                  <Point X="0.806038635254" Y="22.052603515625" />
                  <Point X="0.799017700195" Y="22.03852734375" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782792053223" Y="21.9943671875" />
                  <Point X="0.737213562012" Y="21.784671875" />
                  <Point X="0.728977783203" Y="21.74678125" />
                  <Point X="0.727584899902" Y="21.7387109375" />
                  <Point X="0.72472479248" Y="21.71032421875" />
                  <Point X="0.7247421875" Y="21.67683203125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.759179748535" Y="21.40907421875" />
                  <Point X="0.833091674805" Y="20.847658203125" />
                  <Point X="0.655064819336" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143859863" Y="21.546423828125" />
                  <Point X="0.62678692627" Y="21.576185546875" />
                  <Point X="0.620407592773" Y="21.586791015625" />
                  <Point X="0.481737365723" Y="21.786587890625" />
                  <Point X="0.456679840088" Y="21.82269140625" />
                  <Point X="0.446670379639" Y="21.834828125" />
                  <Point X="0.424788482666" Y="21.85728125" />
                  <Point X="0.412915893555" Y="21.86759765625" />
                  <Point X="0.386661346436" Y="21.886841796875" />
                  <Point X="0.373245330811" Y="21.895060546875" />
                  <Point X="0.345241943359" Y="21.909169921875" />
                  <Point X="0.330654754639" Y="21.915060546875" />
                  <Point X="0.116070480347" Y="21.981658203125" />
                  <Point X="0.077295509338" Y="21.993693359375" />
                  <Point X="0.063381134033" Y="21.996888671875" />
                  <Point X="0.03522838974" Y="22.001158203125" />
                  <Point X="0.020990167618" Y="22.002232421875" />
                  <Point X="-0.008651034355" Y="22.002234375" />
                  <Point X="-0.022895202637" Y="22.001162109375" />
                  <Point X="-0.051061172485" Y="21.996892578125" />
                  <Point X="-0.064982826233" Y="21.9936953125" />
                  <Point X="-0.279567108154" Y="21.927095703125" />
                  <Point X="-0.318341918945" Y="21.9150625" />
                  <Point X="-0.332927032471" Y="21.909171875" />
                  <Point X="-0.360927581787" Y="21.895064453125" />
                  <Point X="-0.374343322754" Y="21.88684765625" />
                  <Point X="-0.400599060059" Y="21.867603515625" />
                  <Point X="-0.412475646973" Y="21.857283203125" />
                  <Point X="-0.434360076904" Y="21.834826171875" />
                  <Point X="-0.444367767334" Y="21.822689453125" />
                  <Point X="-0.583037963867" Y="21.622890625" />
                  <Point X="-0.60809552002" Y="21.5867890625" />
                  <Point X="-0.612467834473" Y="21.57987109375" />
                  <Point X="-0.625975280762" Y="21.55473828125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.704059387207" Y="21.283265625" />
                  <Point X="-0.985425170898" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.720289706671" Y="25.998273454035" />
                  <Point X="-4.712855633645" Y="25.572375695598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691880010715" Y="24.370683062773" />
                  <Point X="-4.681834672862" Y="23.795186042589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.630853928999" Y="26.317876590171" />
                  <Point X="-4.617394684897" Y="25.54679701198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.597307860834" Y="24.396023632142" />
                  <Point X="-4.585564157164" Y="23.723227299479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.535620612659" Y="26.305338958425" />
                  <Point X="-4.52193373615" Y="25.521218328361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.502735710954" Y="24.421364201511" />
                  <Point X="-4.49076752639" Y="23.735707367036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.440387296319" Y="26.29280132668" />
                  <Point X="-4.426472787402" Y="25.495639644743" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.408163561073" Y="24.44670477088" />
                  <Point X="-4.395970895615" Y="23.748187434594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.345153979978" Y="26.280263694934" />
                  <Point X="-4.331011838655" Y="25.470060961125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.313591411193" Y="24.472045340249" />
                  <Point X="-4.30117426484" Y="23.760667502151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.249920663638" Y="26.267726063189" />
                  <Point X="-4.235550889907" Y="25.444482277506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.219019261312" Y="24.497385909617" />
                  <Point X="-4.206377634066" Y="23.773147569708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.174415676391" Y="27.385423648248" />
                  <Point X="-4.172080982023" Y="27.251669097497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.154687347298" Y="26.255188431443" />
                  <Point X="-4.14008994116" Y="25.418903593888" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.124447111432" Y="24.522726478986" />
                  <Point X="-4.111581003291" Y="23.785627637265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.091333202707" Y="22.625631918669" />
                  <Point X="-4.087380729977" Y="22.399194907649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.082160096047" Y="27.543480397501" />
                  <Point X="-4.075776636129" Y="27.177772223674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.059454030957" Y="26.242650799698" />
                  <Point X="-4.044628992412" Y="25.393324910269" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.029874961551" Y="24.548067048355" />
                  <Point X="-4.016784372516" Y="23.798107704823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.997574511026" Y="22.697575477125" />
                  <Point X="-3.989595537455" Y="22.240460387385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.989814009152" Y="27.696352029858" />
                  <Point X="-3.979472290234" Y="27.103875349852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.964220714617" Y="26.230113167952" />
                  <Point X="-3.949168043665" Y="25.367746226651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.935302811671" Y="24.573407617724" />
                  <Point X="-3.921987741742" Y="23.81058777238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.903815819346" Y="22.769519035581" />
                  <Point X="-3.892351030923" Y="22.112701746696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.896884511846" Y="27.815800102228" />
                  <Point X="-3.883167944339" Y="27.029978476029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.868987398277" Y="26.217575536206" />
                  <Point X="-3.853707094917" Y="25.342167543033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84073066179" Y="24.598748187093" />
                  <Point X="-3.827191110967" Y="23.823067839937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.810057127666" Y="22.841462594037" />
                  <Point X="-3.795106506452" Y="21.984942078328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803955014541" Y="27.935248174598" />
                  <Point X="-3.786863598444" Y="26.956081602206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.77375824458" Y="26.205276382171" />
                  <Point X="-3.75824614617" Y="25.316588859414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.74615851191" Y="24.624088756462" />
                  <Point X="-3.732394480192" Y="23.835547907495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716298435986" Y="22.913406152493" />
                  <Point X="-3.700974014593" Y="22.035470638891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710522569232" Y="28.025882375166" />
                  <Point X="-3.69055925255" Y="26.882184728384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.678888257418" Y="26.213553865124" />
                  <Point X="-3.662785199384" Y="25.291010288213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.651586362029" Y="24.64942932583" />
                  <Point X="-3.637597849418" Y="23.848027975052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.622539744306" Y="22.985349710949" />
                  <Point X="-3.606907516601" Y="22.089779985558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.614540820421" Y="27.970467075847" />
                  <Point X="-3.594254906704" Y="26.808287857384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.584440925587" Y="26.246045255766" />
                  <Point X="-3.567324253218" Y="25.265431752443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.557014212149" Y="24.674769895199" />
                  <Point X="-3.542801218643" Y="23.860508042609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.528781052626" Y="23.057293269405" />
                  <Point X="-3.512841018609" Y="22.144089332224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.518559071609" Y="27.915051776529" />
                  <Point X="-3.497717712376" Y="26.721051105771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490793458281" Y="26.324360854308" />
                  <Point X="-3.471789175678" Y="25.235606233162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.462555648207" Y="24.706617798624" />
                  <Point X="-3.44800458972" Y="23.872988216231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.435022360945" Y="23.129236827862" />
                  <Point X="-3.418774520617" Y="22.19839867889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422577322797" Y="27.85963647721" />
                  <Point X="-3.375595796508" Y="25.16806663873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.368760413903" Y="24.776467831591" />
                  <Point X="-3.353207962808" Y="23.885468505058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.341263669265" Y="23.201180386318" />
                  <Point X="-3.324708022626" Y="22.252708025556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326595573985" Y="27.804221177891" />
                  <Point X="-3.258411335895" Y="23.897948793886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.247504977585" Y="23.273123944774" />
                  <Point X="-3.230641524634" Y="22.307017372223" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.230659829542" Y="27.751441467092" />
                  <Point X="-3.163326029406" Y="23.893890640844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.153746285905" Y="23.34506750323" />
                  <Point X="-3.136575026642" Y="22.361326718889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.135261020271" Y="27.729422751678" />
                  <Point X="-3.067628857134" Y="23.854778720564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.059987594225" Y="23.417011061686" />
                  <Point X="-3.04250852865" Y="22.415636065555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.057215556626" Y="28.701576541375" />
                  <Point X="-3.057117918868" Y="28.69598287795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.040147539354" Y="27.723750486742" />
                  <Point X="-2.970773785985" Y="23.749330818034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.967143653498" Y="23.541360667168" />
                  <Point X="-2.948442030658" Y="22.469945412221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.927342459597" Y="21.261151795712" />
                  <Point X="-2.924973210998" Y="21.125417634392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963455844619" Y="28.773461645369" />
                  <Point X="-2.959141332633" Y="28.526283419191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945538058015" Y="27.746952338293" />
                  <Point X="-2.854375534616" Y="22.524254870627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.835116270227" Y="21.420892352733" />
                  <Point X="-2.828664359331" Y="21.051262625035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869696132613" Y="28.845346749362" />
                  <Point X="-2.861164746398" Y="28.356583960432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.851801338874" Y="27.820154702644" />
                  <Point X="-2.760309042814" Y="22.578564571882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.742890080858" Y="21.580632909755" />
                  <Point X="-2.732396107958" Y="20.979433605007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.775936420606" Y="28.917231853356" />
                  <Point X="-2.762796630665" Y="28.164453791787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.759094520785" Y="27.952360058824" />
                  <Point X="-2.666194845012" Y="22.630141198234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.650663891488" Y="21.740373466777" />
                  <Point X="-2.636343527937" Y="20.919960388401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.682176703755" Y="28.989116679824" />
                  <Point X="-2.571549005173" Y="22.651260072741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.558437702118" Y="21.900114023799" />
                  <Point X="-2.540290943117" Y="20.8604868969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588198184758" Y="29.048466339708" />
                  <Point X="-2.476220097986" Y="22.633246045079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466211512749" Y="22.05985458082" />
                  <Point X="-2.44617287795" Y="20.911841962083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.494096280331" Y="29.100747253052" />
                  <Point X="-2.380031644966" Y="22.565988669556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.373985323379" Y="22.219595137842" />
                  <Point X="-2.352909325093" Y="21.012152004713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399994375904" Y="29.153028166396" />
                  <Point X="-2.258990227442" Y="21.074905911284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.305892471476" Y="29.20530907974" />
                  <Point X="-2.16507114693" Y="21.137660799694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.211790567049" Y="29.257589993085" />
                  <Point X="-2.071085715795" Y="21.196614463479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.115212382725" Y="29.168004926156" />
                  <Point X="-1.976521854252" Y="21.222429871411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.018918118219" Y="29.094685614691" />
                  <Point X="-1.881462607607" Y="21.219864685821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.923069686568" Y="29.046908050405" />
                  <Point X="-1.786339312427" Y="21.213630162106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.82774491985" Y="29.029131230012" />
                  <Point X="-1.691216017246" Y="21.207395638391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.733095229575" Y="29.050029513157" />
                  <Point X="-1.595995941576" Y="21.195616564133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.638770718782" Y="29.089557316398" />
                  <Point X="-1.500170594691" Y="21.149161525161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.545029629783" Y="29.162509331771" />
                  <Point X="-1.40369321633" Y="21.065351628032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.457108185537" Y="29.568868571768" />
                  <Point X="-1.30720167762" Y="20.980730485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.362556426728" Y="29.595377344806" />
                  <Point X="-1.210299044476" Y="20.872557757588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.268004667918" Y="29.621886117845" />
                  <Point X="-1.104579049248" Y="20.259238694756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.173452909109" Y="29.648394890883" />
                  <Point X="-1.009190485208" Y="20.237806928249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.078901150299" Y="29.674903663921" />
                  <Point X="-0.918457946088" Y="20.483118650775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.984349372494" Y="29.701411348725" />
                  <Point X="-0.829254478102" Y="20.816030799868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.889679892418" Y="29.721175874911" />
                  <Point X="-0.740051010116" Y="21.148942948962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794859128964" Y="29.732273382233" />
                  <Point X="-0.650847542884" Y="21.481855141293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.700038365511" Y="29.743370889555" />
                  <Point X="-0.558901868654" Y="21.657666399894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.605217602058" Y="29.754468396876" />
                  <Point X="-0.466218351894" Y="21.791206688277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.510396838604" Y="29.765565904198" />
                  <Point X="-0.372888849488" Y="21.88773848378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.415576075151" Y="29.77666341152" />
                  <Point X="-0.278566780373" Y="21.927406170597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.317559567396" Y="29.604676850437" />
                  <Point X="-0.184064272212" Y="21.956736511441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.216138157135" Y="29.237623555355" />
                  <Point X="-0.089561764052" Y="21.986066852284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.11908536741" Y="29.120848363225" />
                  <Point X="0.005170517851" Y="22.002233464267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.023487970481" Y="29.087452568524" />
                  <Point X="0.100459554905" Y="21.986503594933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.071388855229" Y="29.095338271351" />
                  <Point X="0.195991558793" Y="21.956854165052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.165523480583" Y="29.145744604031" />
                  <Point X="0.291523555006" Y="21.927205174856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257257553471" Y="29.333678495427" />
                  <Point X="0.38725010428" Y="21.886410247296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.346461000452" Y="29.6665918479" />
                  <Point X="0.484065533079" Y="21.783233453467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.439520596498" Y="29.778586568392" />
                  <Point X="0.58153120345" Y="21.64280434497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.534709082398" Y="29.768617270894" />
                  <Point X="0.680483611547" Y="21.41720008916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.629897568297" Y="29.758647973396" />
                  <Point X="0.766714707552" Y="21.920399315045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.778155851594" Y="21.264936611886" />
                  <Point X="0.78211894204" Y="21.037891312278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725086054196" Y="29.748678675898" />
                  <Point X="0.858203862242" Y="22.122364560571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.820274537387" Y="29.738709533548" />
                  <Point X="0.951844319918" Y="22.201081740584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.915672958667" Y="29.716713046158" />
                  <Point X="1.045502939757" Y="22.278758410986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.011089535731" Y="29.693676414605" />
                  <Point X="1.139721667578" Y="22.324346516588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.106506112794" Y="29.670639783052" />
                  <Point X="1.23450690668" Y="22.337479212647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.201922689858" Y="29.647603151499" />
                  <Point X="1.329369009503" Y="22.34620838907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.297339266922" Y="29.624566519947" />
                  <Point X="1.424254257133" Y="22.35361160035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.392755848578" Y="29.601529625292" />
                  <Point X="1.519589856792" Y="22.335214161216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.488246040344" Y="29.574275610269" />
                  <Point X="1.615554694032" Y="22.28076772523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.583865886577" Y="29.539593695796" />
                  <Point X="1.711647517582" Y="22.218988958394" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.67948573281" Y="29.504911781323" />
                  <Point X="1.807845132101" Y="22.151206721009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.775105579043" Y="29.47022986685" />
                  <Point X="1.904957990095" Y="22.030990220011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.870748458159" Y="29.434228399411" />
                  <Point X="2.002184151146" Y="21.904282591278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.96654493346" Y="29.389427412417" />
                  <Point X="2.083593031671" Y="22.683746356962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.094062366151" Y="22.083958586296" />
                  <Point X="2.099410312197" Y="21.777574962545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.062341408761" Y="29.344626425423" />
                  <Point X="2.092011146773" Y="27.64484827314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100883590156" Y="27.136546332133" />
                  <Point X="2.176203363867" Y="22.821479386222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.192038947629" Y="21.914259400097" />
                  <Point X="2.196636473247" Y="21.650867333813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.158137884062" Y="29.299825438429" />
                  <Point X="2.18423733712" Y="27.804588774141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.197458188075" Y="27.047166730244" />
                  <Point X="2.27019175443" Y="22.880263504498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.290015546419" Y="21.744559222068" />
                  <Point X="2.293862634298" Y="21.52415970508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.253941434864" Y="29.254619096267" />
                  <Point X="2.276463527468" Y="27.964329275141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293380348325" Y="26.995165257314" />
                  <Point X="2.36434132541" Y="22.929813602823" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.387992145209" Y="21.574859044039" />
                  <Point X="2.391088795348" Y="21.397452076347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.349932074167" Y="29.198694461037" />
                  <Point X="2.368689717815" Y="28.124069776142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.388691221987" Y="26.978184369584" />
                  <Point X="2.458685960724" Y="22.968188472969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.485968743999" Y="21.40515886601" />
                  <Point X="2.488314956399" Y="21.270744447614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44592271347" Y="29.142769825806" />
                  <Point X="2.460915908163" Y="28.283810277143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.483678745236" Y="26.979728214637" />
                  <Point X="2.553619557352" Y="22.972821772073" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.583945342789" Y="21.235458687981" />
                  <Point X="2.58554111745" Y="21.144036818882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.541913352773" Y="29.086845190576" />
                  <Point X="2.55314209851" Y="28.443550778143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578262706981" Y="27.004392082728" />
                  <Point X="2.648932425234" Y="22.955726635546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.681921941579" Y="21.065758509952" />
                  <Point X="2.6827672785" Y="21.017329190149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.637923192017" Y="29.029820591508" />
                  <Point X="2.645368288858" Y="28.603291279144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.672499856478" Y="27.048924811236" />
                  <Point X="2.744247360756" Y="22.938513044042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734131895773" Y="28.961403052135" />
                  <Point X="2.737594479205" Y="28.763031780144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.766566362708" Y="27.103233685893" />
                  <Point X="2.839764146183" Y="22.909735479165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860632868939" Y="27.157542560549" />
                  <Point X="2.918904886445" Y="23.819140913521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.927396960095" Y="23.332630339907" />
                  <Point X="2.935735104869" Y="22.854938345727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.954699367844" Y="27.211851854946" />
                  <Point X="3.011918259792" Y="23.933783730683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.024893114097" Y="23.190454825345" />
                  <Point X="3.031716850384" Y="22.799523235317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.048765866439" Y="27.266161167012" />
                  <Point X="3.060291267984" Y="26.605871354717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.067956167331" Y="26.166749565222" />
                  <Point X="3.105445125498" Y="24.019008590309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.121291656398" Y="23.111161443068" />
                  <Point X="3.127698595898" Y="22.744108124906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.142832365035" Y="27.320470479078" />
                  <Point X="3.15320522289" Y="26.726209850591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.16502520867" Y="26.049043318753" />
                  <Point X="3.199853821412" Y="24.053713431121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.217596013847" Y="23.037263907262" />
                  <Point X="3.223680341413" Y="22.688693014495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236898863631" Y="27.374779791145" />
                  <Point X="3.246921583224" Y="26.800578570207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.26101682751" Y="25.993062565885" />
                  <Point X="3.294509179804" Y="24.07428698807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313900361015" Y="22.963366960506" />
                  <Point X="3.319662086927" Y="22.633277904084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.330965362227" Y="27.429089103211" />
                  <Point X="3.340680272967" Y="26.872522239685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.356546358594" Y="25.963554802859" />
                  <Point X="3.371368758208" Y="25.114380097688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.376775026916" Y="24.804655170837" />
                  <Point X="3.389231123674" Y="24.091045865515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.410204705515" Y="22.889470166603" />
                  <Point X="3.415643832441" Y="22.577862793673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.425031860823" Y="27.483398415277" />
                  <Point X="3.434438962709" Y="26.944465909164" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.451755801429" Y="25.952384883309" />
                  <Point X="3.463622555018" Y="25.272539025536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.474445599214" Y="24.652487238819" />
                  <Point X="3.484360163778" Y="24.084482215371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.506509050015" Y="22.815573372699" />
                  <Point X="3.511625577956" Y="22.522447683263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.519098359419" Y="27.537707727343" />
                  <Point X="3.528197652451" Y="27.016409578642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.546657252453" Y="25.958859802842" />
                  <Point X="3.557233565816" Y="25.352943216039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.570783535294" Y="24.576665984558" />
                  <Point X="3.579593481225" Y="24.071944520188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.602813394514" Y="22.741676578796" />
                  <Point X="3.60760732347" Y="22.467032572852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.613164858014" Y="27.592017039409" />
                  <Point X="3.621956342193" Y="27.08835324812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.641453891093" Y="25.971339419745" />
                  <Point X="3.651327747177" Y="25.405666583545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666695876391" Y="24.525227050543" />
                  <Point X="3.674826798673" Y="24.059406825005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.699117739014" Y="22.667779784892" />
                  <Point X="3.703589068985" Y="22.411617462441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.70723135661" Y="27.646326351475" />
                  <Point X="3.715715031936" Y="27.160296917598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.736250527061" Y="25.983819189817" />
                  <Point X="3.745816656382" Y="25.435776008022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.762198296731" Y="24.497272460985" />
                  <Point X="3.770060116121" Y="24.046869129821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.795422083514" Y="22.593882990989" />
                  <Point X="3.799570814499" Y="22.35620235203" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.801297855206" Y="27.700635663541" />
                  <Point X="3.809473721678" Y="27.232240587076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.831047152137" Y="25.996299583856" />
                  <Point X="3.840388806689" Y="25.461116552955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.857659243545" Y="24.471693888147" />
                  <Point X="3.865293433568" Y="24.034331434638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.891726428014" Y="22.519986197085" />
                  <Point X="3.895552560013" Y="22.30078724162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.895364353802" Y="27.754944975607" />
                  <Point X="3.90323241142" Y="27.304184256555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925843777212" Y="26.008779977894" />
                  <Point X="3.934960956997" Y="25.486457097887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953120190129" Y="24.446115328477" />
                  <Point X="3.960526751016" Y="24.021793739455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.988030772513" Y="22.446089403181" />
                  <Point X="3.991534307796" Y="22.245372001249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.991156071174" Y="27.710416570195" />
                  <Point X="3.996991101162" Y="27.376127926033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020640402288" Y="26.021260371933" />
                  <Point X="4.029533107304" Y="25.51179764282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.04858113659" Y="24.420536775861" />
                  <Point X="4.055760068464" Y="24.009256044272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.084335117013" Y="22.372192609278" />
                  <Point X="4.084449902322" Y="22.365616563305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088743246387" Y="27.563026453913" />
                  <Point X="4.090749790905" Y="27.448071595511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.115437027364" Y="26.033740765971" />
                  <Point X="4.124105257611" Y="25.537138187752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.144042083051" Y="24.394958223244" />
                  <Point X="4.150993385911" Y="23.996718349088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.21023365244" Y="26.046221160009" />
                  <Point X="4.218677407918" Y="25.562478732684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.239503029512" Y="24.369379670628" />
                  <Point X="4.246226703359" Y="23.984180653905" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.305030277516" Y="26.058701554048" />
                  <Point X="4.313249558225" Y="25.587819277617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.334963975973" Y="24.343801118011" />
                  <Point X="4.341460020807" Y="23.971642958722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.399826902592" Y="26.071181948086" />
                  <Point X="4.407821708532" Y="25.613159822549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430424922433" Y="24.318222565395" />
                  <Point X="4.436693338254" Y="23.959105263538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.494623527668" Y="26.083662342124" />
                  <Point X="4.502393858839" Y="25.638500367482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.525885868894" Y="24.292644012778" />
                  <Point X="4.531926655702" Y="23.946567568355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.589420152744" Y="26.096142736163" />
                  <Point X="4.596966009146" Y="25.663840912414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.621346815355" Y="24.267065460162" />
                  <Point X="4.627159973149" Y="23.934029873172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.68421677782" Y="26.108623130201" />
                  <Point X="4.691538159453" Y="25.689181457347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.716807761816" Y="24.241486907545" />
                  <Point X="4.722393298291" Y="23.921491737193" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.810814025879" Y="20.1966953125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318969727" Y="21.478455078125" />
                  <Point X="0.325648681641" Y="21.678251953125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.27433682251" Y="21.733599609375" />
                  <Point X="0.059752590179" Y="21.800197265625" />
                  <Point X="0.020977689743" Y="21.812232421875" />
                  <Point X="-0.008663539886" Y="21.812234375" />
                  <Point X="-0.223247772217" Y="21.745634765625" />
                  <Point X="-0.262022674561" Y="21.7336015625" />
                  <Point X="-0.288278564453" Y="21.714357421875" />
                  <Point X="-0.426948822021" Y="21.51455859375" />
                  <Point X="-0.452006347656" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.52053338623" Y="21.23408984375" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.055845947266" Y="20.05331640625" />
                  <Point X="-1.100256103516" Y="20.0619375" />
                  <Point X="-1.349228881836" Y="20.125994140625" />
                  <Point X="-1.349990966797" Y="20.13874609375" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.360947265625" Y="20.722962890625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.583844848633" Y="20.97062890625" />
                  <Point X="-1.619543823242" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.91144934082" Y="21.031421875" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989877807617" Y="21.026208984375" />
                  <Point X="-2.208364013672" Y="20.880220703125" />
                  <Point X="-2.247844238281" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.294152832031" Y="20.797849609375" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.790757568359" Y="20.792095703125" />
                  <Point X="-2.855840087891" Y="20.832392578125" />
                  <Point X="-3.200603027344" Y="21.097849609375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.136538085938" Y="21.278814453125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.40240625" />
                  <Point X="-2.513979980469" Y="22.431236328125" />
                  <Point X="-2.528673583984" Y="22.4459296875" />
                  <Point X="-2.531333251953" Y="22.448587890625" />
                  <Point X="-2.560160644531" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.789017578125" Y="22.34259765625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.110289550781" Y="22.085322265625" />
                  <Point X="-4.161699707031" Y="22.15286328125" />
                  <Point X="-4.408875488281" Y="22.56733984375" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.26696484375" Y="22.73035546875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.139296630859" Y="23.6291796875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326171875" Y="23.665404296875" />
                  <Point X="-3.161158935547" Y="23.689359375" />
                  <Point X="-3.183588623047" Y="23.702560546875" />
                  <Point X="-3.187646728516" Y="23.70494921875" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-3.46864453125" Y="23.678630859375" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.907284179688" Y="23.91008203125" />
                  <Point X="-4.927392578125" Y="23.988806640625" />
                  <Point X="-4.992788574219" Y="24.446046875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.814149902344" Y="24.534625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541895507813" Y="24.87857421875" />
                  <Point X="-3.518389892578" Y="24.894888671875" />
                  <Point X="-3.514146728516" Y="24.897833984375" />
                  <Point X="-3.494899902344" Y="24.924087890625" />
                  <Point X="-3.487064697266" Y="24.949333984375" />
                  <Point X="-3.485648925781" Y="24.95389453125" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.493482421875" Y="25.008783203125" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514144775391" Y="25.03960546875" />
                  <Point X="-3.537650390625" Y="25.05591796875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.784544433594" Y="25.126931640625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.930604003906" Y="25.908841796875" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.786006835938" Y="26.482205078125" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.654089355469" Y="26.512576171875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731701171875" Y="26.3958671875" />
                  <Point X="-3.679676025391" Y="26.41226953125" />
                  <Point X="-3.670275146484" Y="26.415234375" />
                  <Point X="-3.651532226562" Y="26.426056640625" />
                  <Point X="-3.639119140625" Y="26.443787109375" />
                  <Point X="-3.618243652344" Y="26.49418359375" />
                  <Point X="-3.614471679688" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.524607421875" />
                  <Point X="-3.616317138672" Y="26.545513671875" />
                  <Point X="-3.641505371094" Y="26.593900390625" />
                  <Point X="-3.646056884766" Y="26.602642578125" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.790222167969" Y="26.71916796875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.210368652344" Y="27.700736328125" />
                  <Point X="-4.160016113281" Y="27.78700390625" />
                  <Point X="-3.811306640625" Y="28.235220703125" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.712417480469" Y="28.246369140625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138511230469" Y="27.92043359375" />
                  <Point X="-3.0660546875" Y="27.91409375" />
                  <Point X="-3.052961914062" Y="27.91294921875" />
                  <Point X="-3.031505615234" Y="27.915775390625" />
                  <Point X="-3.013253173828" Y="27.927404296875" />
                  <Point X="-2.961822998047" Y="27.978833984375" />
                  <Point X="-2.952529541016" Y="27.988126953125" />
                  <Point X="-2.940899658203" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.944413330078" Y="28.100296875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.009786865234" Y="28.234005859375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.840575439453" Y="29.107091796875" />
                  <Point X="-2.752872314453" Y="29.174333984375" />
                  <Point X="-2.203671630859" Y="29.47945703125" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.137196777344" Y="29.50830078125" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246948242" Y="29.273662109375" />
                  <Point X="-1.870603149414" Y="29.2316796875" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835123901367" Y="29.218490234375" />
                  <Point X="-1.813808349609" Y="29.22225" />
                  <Point X="-1.729812255859" Y="29.25704296875" />
                  <Point X="-1.714634399414" Y="29.263330078125" />
                  <Point X="-1.696905151367" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.658744384766" Y="29.381197265625" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.658479858398" Y="29.46826953125" />
                  <Point X="-1.689137695313" Y="29.701140625" />
                  <Point X="-1.081627319336" Y="29.87146484375" />
                  <Point X="-0.96808404541" Y="29.903296875" />
                  <Point X="-0.302290283203" Y="29.98121875" />
                  <Point X="-0.224200027466" Y="29.990359375" />
                  <Point X="-0.205810073853" Y="29.9217265625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282117844" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.084027008057" Y="29.42127734375" />
                  <Point X="0.236648406982" Y="29.990869140625" />
                  <Point X="0.76107232666" Y="29.935947265625" />
                  <Point X="0.860210083008" Y="29.925564453125" />
                  <Point X="1.411041259766" Y="29.792576171875" />
                  <Point X="1.50845703125" Y="29.769056640625" />
                  <Point X="1.867744384766" Y="29.638740234375" />
                  <Point X="1.931044067383" Y="29.61578125" />
                  <Point X="2.277724609375" Y="29.453650390625" />
                  <Point X="2.338686279297" Y="29.425140625" />
                  <Point X="2.673613525391" Y="29.23001171875" />
                  <Point X="2.732533203125" Y="29.195685546875" />
                  <Point X="3.048394287109" Y="28.9710625" />
                  <Point X="3.068740722656" Y="28.95659375" />
                  <Point X="2.959614990234" Y="28.76758203125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852294922" Y="27.491515625" />
                  <Point X="2.206668457031" Y="27.423515625" />
                  <Point X="2.202044433594" Y="27.392326171875" />
                  <Point X="2.209134765625" Y="27.33352734375" />
                  <Point X="2.210415771484" Y="27.32290234375" />
                  <Point X="2.218684082031" Y="27.30080859375" />
                  <Point X="2.255067382812" Y="27.2471875" />
                  <Point X="2.274940185547" Y="27.224203125" />
                  <Point X="2.328559814453" Y="27.1878203125" />
                  <Point X="2.338243164062" Y="27.181248046875" />
                  <Point X="2.360336669922" Y="27.172978515625" />
                  <Point X="2.419136474609" Y="27.165888671875" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.516663330078" Y="27.18412890625" />
                  <Point X="2.516668212891" Y="27.184130859375" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.769433105469" Y="27.324283203125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.167647949219" Y="27.790443359375" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.378685058594" Y="27.4508828125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.249415527344" Y="27.330330078125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279370849609" Y="26.58383203125" />
                  <Point X="3.230431884766" Y="26.51998828125" />
                  <Point X="3.213119140625" Y="26.491498046875" />
                  <Point X="3.194889160156" Y="26.4263125" />
                  <Point X="3.191595214844" Y="26.414533203125" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.205744384766" Y="26.3184375" />
                  <Point X="3.215645996094" Y="26.287955078125" />
                  <Point X="3.256348632812" Y="26.22608984375" />
                  <Point X="3.263703613281" Y="26.21491015625" />
                  <Point X="3.280947265625" Y="26.1988203125" />
                  <Point X="3.339930908203" Y="26.1656171875" />
                  <Point X="3.36856640625" Y="26.153619140625" />
                  <Point X="3.44831640625" Y="26.143080078125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.692767822266" Y="26.169734375" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.924180175781" Y="26.01303125" />
                  <Point X="4.939189453125" Y="25.951380859375" />
                  <Point X="4.994680175781" Y="25.59496875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.842264160156" Y="25.532865234375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.650735351562" Y="25.18753125" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.575253662109" Y="25.1070234375" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.541314697266" Y="24.99291015625" />
                  <Point X="3.538483154297" Y="24.9593125" />
                  <Point X="3.554153564453" Y="24.87748828125" />
                  <Point X="3.556985351562" Y="24.862703125" />
                  <Point X="3.566759521484" Y="24.841240234375" />
                  <Point X="3.613770507812" Y="24.7813359375" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.714928955078" Y="24.712802734375" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.940133544922" Y="24.646298828125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.956811523438" Y="24.089181640625" />
                  <Point X="4.948431640625" Y="24.03359765625" />
                  <Point X="4.877337890625" Y="23.7220546875" />
                  <Point X="4.874546386719" Y="23.709822265625" />
                  <Point X="4.688944824219" Y="23.734255859375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394837158203" Y="23.901658203125" />
                  <Point X="3.241060058594" Y="23.868234375" />
                  <Point X="3.213272949219" Y="23.8621953125" />
                  <Point X="3.185445800781" Y="23.845302734375" />
                  <Point X="3.092497314453" Y="23.733513671875" />
                  <Point X="3.075701660156" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.051036132812" Y="23.541158203125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056361083984" Y="23.483375" />
                  <Point X="3.141463623047" Y="23.35100390625" />
                  <Point X="3.156841308594" Y="23.3270859375" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.353102294922" Y="23.17277734375" />
                  <Point X="4.339073730469" Y="22.416216796875" />
                  <Point X="4.227752441406" Y="22.23608203125" />
                  <Point X="4.204123046875" Y="22.19784375" />
                  <Point X="4.057105224609" Y="21.988953125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.890761962891" Y="22.084158203125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.554321533203" Y="22.77973828125" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489078125" Y="22.78075390625" />
                  <Point X="2.337033935547" Y="22.700734375" />
                  <Point X="2.309560058594" Y="22.686275390625" />
                  <Point X="2.288600341797" Y="22.66531640625" />
                  <Point X="2.208580566406" Y="22.513271484375" />
                  <Point X="2.194121337891" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.222216064453" Y="22.27060546875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.352742431641" Y="22.015912109375" />
                  <Point X="2.986674072266" Y="20.917912109375" />
                  <Point X="2.863329101562" Y="20.82980859375" />
                  <Point X="2.835318603516" Y="20.80980078125" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.551048095703" Y="20.876880859375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548339844" Y="22.019533203125" />
                  <Point X="1.490041870117" Y="22.13558203125" />
                  <Point X="1.457425048828" Y="22.15655078125" />
                  <Point X="1.425806762695" Y="22.16428125" />
                  <Point X="1.228392089844" Y="22.146115234375" />
                  <Point X="1.192719848633" Y="22.142833984375" />
                  <Point X="1.165331665039" Y="22.131490234375" />
                  <Point X="1.012893371582" Y="22.0047421875" />
                  <Point X="0.985348022461" Y="21.981837890625" />
                  <Point X="0.96845690918" Y="21.95401171875" />
                  <Point X="0.922878601074" Y="21.74431640625" />
                  <Point X="0.914642700195" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.947554260254" Y="21.433875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.020862487793" Y="20.042564453125" />
                  <Point X="0.99436920166" Y="20.0367578125" />
                  <Point X="0.860200500488" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#202" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.163669777233" Y="4.965948429041" Z="2.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="-0.314258882436" Y="5.062160073588" Z="2.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.15" />
                  <Point X="-1.101280782759" Y="4.950889373162" Z="2.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.15" />
                  <Point X="-1.714207942376" Y="4.493024324154" Z="2.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.15" />
                  <Point X="-1.712586016317" Y="4.427512530203" Z="2.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.15" />
                  <Point X="-1.755103047005" Y="4.334517154784" Z="2.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.15" />
                  <Point X="-1.853671243615" Y="4.30731043692" Z="2.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.15" />
                  <Point X="-2.103684979599" Y="4.570018414861" Z="2.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.15" />
                  <Point X="-2.234110888084" Y="4.554444888864" Z="2.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.15" />
                  <Point X="-2.877153766216" Y="4.178052865458" Z="2.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.15" />
                  <Point X="-3.059244135045" Y="3.240285921222" Z="2.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.15" />
                  <Point X="-3.000379207578" Y="3.127220155028" Z="2.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.15" />
                  <Point X="-3.003333343452" Y="3.045470224434" Z="2.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.15" />
                  <Point X="-3.067856310579" Y="2.995185491595" Z="2.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.15" />
                  <Point X="-3.693573427075" Y="3.320949813701" Z="2.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.15" />
                  <Point X="-3.856926107548" Y="3.297203623759" Z="2.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.15" />
                  <Point X="-4.259706916488" Y="2.757222384293" Z="2.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.15" />
                  <Point X="-3.826816192525" Y="1.710781445037" Z="2.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.15" />
                  <Point X="-3.692010804545" Y="1.602090797978" Z="2.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.15" />
                  <Point X="-3.670594564135" Y="1.544597651394" Z="2.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.15" />
                  <Point X="-3.700870770064" Y="1.491236032855" Z="2.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.15" />
                  <Point X="-4.653719074286" Y="1.59342816896" Z="2.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.15" />
                  <Point X="-4.84042189999" Y="1.526563860792" Z="2.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.15" />
                  <Point X="-4.986221491439" Y="0.947441340002" Z="2.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.15" />
                  <Point X="-3.803641220994" Y="0.10991506566" Z="2.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.15" />
                  <Point X="-3.572313344088" Y="0.046121099738" Z="2.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.15" />
                  <Point X="-3.54739203789" Y="0.025245197075" Z="2.15" />
                  <Point X="-3.539556741714" Y="0" Z="2.15" />
                  <Point X="-3.540972563245" Y="-0.004561753986" Z="2.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.15" />
                  <Point X="-3.553055251035" Y="-0.032754883395" Z="2.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.15" />
                  <Point X="-4.833246567779" Y="-0.385796991096" Z="2.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.15" />
                  <Point X="-5.048440895264" Y="-0.529749786357" Z="2.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.15" />
                  <Point X="-4.961884811321" Y="-1.071011651928" Z="2.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.15" />
                  <Point X="-3.468274258788" Y="-1.339659962431" Z="2.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.15" />
                  <Point X="-3.215105906256" Y="-1.309248716557" Z="2.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.15" />
                  <Point X="-3.193855427279" Y="-1.327002438382" Z="2.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.15" />
                  <Point X="-4.303558679517" Y="-2.198695292367" Z="2.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.15" />
                  <Point X="-4.457975395942" Y="-2.426988431973" Z="2.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.15" />
                  <Point X="-4.156231102297" Y="-2.913680794241" Z="2.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.15" />
                  <Point X="-2.770173428233" Y="-2.66942176647" Z="2.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.15" />
                  <Point X="-2.570184422656" Y="-2.558146046475" Z="2.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.15" />
                  <Point X="-3.185995246699" Y="-3.664904954259" Z="2.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.15" />
                  <Point X="-3.237262384408" Y="-3.910487813612" Z="2.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.15" />
                  <Point X="-2.823234752924" Y="-4.219135963338" Z="2.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.15" />
                  <Point X="-2.260641021423" Y="-4.201307544716" Z="2.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.15" />
                  <Point X="-2.186742267362" Y="-4.130072441285" Z="2.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.15" />
                  <Point X="-1.920875453658" Y="-3.987189855991" Z="2.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.15" />
                  <Point X="-1.622967837146" Y="-4.035683170517" Z="2.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.15" />
                  <Point X="-1.416142659744" Y="-4.255510314051" Z="2.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.15" />
                  <Point X="-1.405719224011" Y="-4.823447926743" Z="2.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.15" />
                  <Point X="-1.367844575027" Y="-4.891146833279" Z="2.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.15" />
                  <Point X="-1.071529596547" Y="-4.964487536139" Z="2.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="-0.478393061928" Y="-3.747571253982" Z="2.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="-0.392029353374" Y="-3.482670104242" Z="2.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="-0.214584171196" Y="-3.270838376283" Z="2.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.15" />
                  <Point X="0.038774908164" Y="-3.216273669005" Z="2.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.15" />
                  <Point X="0.278416506058" Y="-3.318975651526" Z="2.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.15" />
                  <Point X="0.756361798474" Y="-4.784964775834" Z="2.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.15" />
                  <Point X="0.845268226093" Y="-5.008749011715" Z="2.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.15" />
                  <Point X="1.025411460958" Y="-4.974995027378" Z="2.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.15" />
                  <Point X="0.990970496017" Y="-3.528318710698" Z="2.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.15" />
                  <Point X="0.965581691113" Y="-3.235022060063" Z="2.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.15" />
                  <Point X="1.038705319811" Y="-3.002422996756" Z="2.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.15" />
                  <Point X="1.226816108384" Y="-2.872392837073" Z="2.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.15" />
                  <Point X="1.456847569832" Y="-2.875196379468" Z="2.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.15" />
                  <Point X="2.505224313087" Y="-4.122275785459" Z="2.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.15" />
                  <Point X="2.691924840023" Y="-4.307311056403" Z="2.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.15" />
                  <Point X="2.886235342472" Y="-4.17959729068" Z="2.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.15" />
                  <Point X="2.3898876448" Y="-2.92780766051" Z="2.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.15" />
                  <Point X="2.265264262037" Y="-2.689227429196" Z="2.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.15" />
                  <Point X="2.246670503243" Y="-2.478734157651" Z="2.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.15" />
                  <Point X="2.354164220721" Y="-2.312230806724" Z="2.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.15" />
                  <Point X="2.539279374316" Y="-2.238183747454" Z="2.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.15" />
                  <Point X="3.859605936597" Y="-2.927861855475" Z="2.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.15" />
                  <Point X="4.091837447857" Y="-3.008543650596" Z="2.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.15" />
                  <Point X="4.264130559702" Y="-2.758923394349" Z="2.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.15" />
                  <Point X="3.377384313066" Y="-1.75627344523" Z="2.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.15" />
                  <Point X="3.177364985657" Y="-1.590673811254" Z="2.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.15" />
                  <Point X="3.094669716887" Y="-1.432142766789" Z="2.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.15" />
                  <Point X="3.124787051296" Y="-1.267172345715" Z="2.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.15" />
                  <Point X="3.245522732311" Y="-1.149344519326" Z="2.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.15" />
                  <Point X="4.676262312617" Y="-1.284035707983" Z="2.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.15" />
                  <Point X="4.919928438168" Y="-1.257789143722" Z="2.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.15" />
                  <Point X="5.000097077788" Y="-0.886991527463" Z="2.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.15" />
                  <Point X="3.946918535001" Y="-0.274123369331" Z="2.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.15" />
                  <Point X="3.733794666799" Y="-0.212627040107" Z="2.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.15" />
                  <Point X="3.646947894974" Y="-0.156513810802" Z="2.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.15" />
                  <Point X="3.597105117137" Y="-0.081824937249" Z="2.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.15" />
                  <Point X="3.584266333288" Y="0.014785593935" Z="2.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.15" />
                  <Point X="3.608431543426" Y="0.107434927773" Z="2.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.15" />
                  <Point X="3.669600747553" Y="0.175521804135" Z="2.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.15" />
                  <Point X="4.849049282305" Y="0.515848563438" Z="2.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.15" />
                  <Point X="5.037929222781" Y="0.633941352173" Z="2.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.15" />
                  <Point X="4.966605402464" Y="1.056140654983" Z="2.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.15" />
                  <Point X="3.680085390874" Y="1.250587941347" Z="2.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.15" />
                  <Point X="3.448710735884" Y="1.223928629826" Z="2.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.15" />
                  <Point X="3.358302367619" Y="1.240468251649" Z="2.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.15" />
                  <Point X="3.291963665304" Y="1.284850135515" Z="2.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.15" />
                  <Point X="3.248556879639" Y="1.359821929941" Z="2.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.15" />
                  <Point X="3.236886129982" Y="1.444128105274" Z="2.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.15" />
                  <Point X="3.263959160006" Y="1.520850320099" Z="2.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.15" />
                  <Point X="4.273697465438" Y="2.321942313252" Z="2.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.15" />
                  <Point X="4.415306254986" Y="2.508050962422" Z="2.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.15" />
                  <Point X="4.202077570637" Y="2.850927106024" Z="2.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.15" />
                  <Point X="2.738277167922" Y="2.398865127514" Z="2.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.15" />
                  <Point X="2.497590645751" Y="2.263712990604" Z="2.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.15" />
                  <Point X="2.418966645278" Y="2.246810354269" Z="2.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.15" />
                  <Point X="2.350477813669" Y="2.260474892056" Z="2.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.15" />
                  <Point X="2.290283708179" Y="2.306547046713" Z="2.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.15" />
                  <Point X="2.25261934849" Y="2.370791798555" Z="2.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.15" />
                  <Point X="2.248814952054" Y="2.441878930131" Z="2.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.15" />
                  <Point X="2.996760225345" Y="3.773861914033" Z="2.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.15" />
                  <Point X="3.071215645899" Y="4.043088649953" Z="2.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.15" />
                  <Point X="2.692627146094" Y="4.304495408114" Z="2.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.15" />
                  <Point X="2.292749977774" Y="4.530222354808" Z="2.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.15" />
                  <Point X="1.878636809431" Y="4.717025488674" Z="2.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.15" />
                  <Point X="1.416619883669" Y="4.872460511231" Z="2.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.15" />
                  <Point X="0.760124627852" Y="5.01695554937" Z="2.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.15" />
                  <Point X="0.02957478432" Y="4.46549907691" Z="2.15" />
                  <Point X="0" Y="4.355124473572" Z="2.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>