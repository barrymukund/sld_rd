<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#123" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="471" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.587158630371" Y="21.39844140625" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.553965576172" Y="21.51012109375" />
                  <Point X="0.536994506836" Y="21.53973828125" />
                  <Point X="0.532612304688" Y="21.546673828125" />
                  <Point X="0.378635528564" Y="21.768525390625" />
                  <Point X="0.357108612061" Y="21.79248046875" />
                  <Point X="0.324800323486" Y="21.814201171875" />
                  <Point X="0.294973968506" Y="21.826306640625" />
                  <Point X="0.287406921387" Y="21.82901171875" />
                  <Point X="0.049136642456" Y="21.902962890625" />
                  <Point X="0.020403022766" Y="21.90840625" />
                  <Point X="-0.015286241531" Y="21.907029296875" />
                  <Point X="-0.045195781708" Y="21.900099609375" />
                  <Point X="-0.051912509918" Y="21.89828125" />
                  <Point X="-0.290182769775" Y="21.82433203125" />
                  <Point X="-0.319508972168" Y="21.811015625" />
                  <Point X="-0.350144073486" Y="21.7865078125" />
                  <Point X="-0.371412506104" Y="21.760638671875" />
                  <Point X="-0.376073974609" Y="21.754474609375" />
                  <Point X="-0.53005078125" Y="21.532625" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.91658416748" Y="20.12305859375" />
                  <Point X="-1.079317993164" Y="20.154646484375" />
                  <Point X="-1.092730712891" Y="20.158095703125" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.217834960938" Y="20.41474609375" />
                  <Point X="-1.214963134766" Y="20.43655859375" />
                  <Point X="-1.214830078125" Y="20.460302734375" />
                  <Point X="-1.218967651367" Y="20.49470703125" />
                  <Point X="-1.22011340332" Y="20.501896484375" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287037231445" Y="20.8182421875" />
                  <Point X="-1.308026977539" Y="20.85119921875" />
                  <Point X="-1.332252563477" Y="20.87597265625" />
                  <Point X="-1.337536499023" Y="20.8809765625" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583204223633" Y="21.091208984375" />
                  <Point X="-1.619925537109" Y="21.10458203125" />
                  <Point X="-1.654227294922" Y="21.10948828125" />
                  <Point X="-1.661465576172" Y="21.1102421875" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.984347045898" Y="21.127474609375" />
                  <Point X="-2.021616699219" Y="21.11573046875" />
                  <Point X="-2.051817382813" Y="21.0987421875" />
                  <Point X="-2.058020751953" Y="21.09493359375" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312788574219" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.480148193359" Y="20.71151171875" />
                  <Point X="-2.801719726562" Y="20.910619140625" />
                  <Point X="-2.820241943359" Y="20.924880859375" />
                  <Point X="-3.104721923828" Y="21.143919921875" />
                  <Point X="-2.468096191406" Y="22.24658984375" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405664794922" Y="22.415470703125" />
                  <Point X="-2.415158447266" Y="22.445619140625" />
                  <Point X="-2.430409667969" Y="22.47548046875" />
                  <Point X="-2.447840820312" Y="22.499447265625" />
                  <Point X="-2.46415625" Y="22.51576171875" />
                  <Point X="-2.489312011719" Y="22.533787109375" />
                  <Point X="-2.518140869141" Y="22.54800390625" />
                  <Point X="-2.547759277344" Y="22.55698828125" />
                  <Point X="-2.578693603516" Y="22.555974609375" />
                  <Point X="-2.610219482422" Y="22.549703125" />
                  <Point X="-2.639184082031" Y="22.53880078125" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.082863037109" Y="22.206142578125" />
                  <Point X="-4.096145996094" Y="22.228416015625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.183526855469" Y="23.441962890625" />
                  <Point X="-3.105955078125" Y="23.501484375" />
                  <Point X="-3.083679199219" Y="23.525787109375" />
                  <Point X="-3.065255126953" Y="23.554689453125" />
                  <Point X="-3.053397705078" Y="23.5819375" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.043348144531" Y="23.64034765625" />
                  <Point X="-3.045559326172" Y="23.6720234375" />
                  <Point X="-3.052883544922" Y="23.702533203125" />
                  <Point X="-3.069747314453" Y="23.728994140625" />
                  <Point X="-3.092166015625" Y="23.75387890625" />
                  <Point X="-3.114559570312" Y="23.7721640625" />
                  <Point X="-3.139454101562" Y="23.78681640625" />
                  <Point X="-3.168716064453" Y="23.798041015625" />
                  <Point X="-3.200603027344" Y="23.8045234375" />
                  <Point X="-3.231928710938" Y="23.805615234375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.837592285156" Y="24.03191796875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.621241943359" Y="24.755912109375" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.515947509766" Y="24.78590625" />
                  <Point X="-3.486074951172" Y="24.801677734375" />
                  <Point X="-3.459974853516" Y="24.81979296875" />
                  <Point X="-3.436541015625" Y="24.843037109375" />
                  <Point X="-3.416746337891" Y="24.871068359375" />
                  <Point X="-3.403617431641" Y="24.89770703125" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390662597656" Y="24.9555625" />
                  <Point X="-3.391213134766" Y="24.986978515625" />
                  <Point X="-3.395467529297" Y="25.01347265625" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.419357666016" Y="25.070955078125" />
                  <Point X="-3.440254394531" Y="25.098357421875" />
                  <Point X="-3.461627685547" Y="25.11879296875" />
                  <Point X="-3.487727783203" Y="25.136908203125" />
                  <Point X="-3.501923339844" Y="25.145046875" />
                  <Point X="-3.532875732422" Y="25.157849609375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.817411132812" Y="26.00308984375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.829008300781" Y="26.308130859375" />
                  <Point X="-3.765665283203" Y="26.29979296875" />
                  <Point X="-3.744995605469" Y="26.299341796875" />
                  <Point X="-3.723447998047" Y="26.301224609375" />
                  <Point X="-3.703182128906" Y="26.30525" />
                  <Point X="-3.699477783203" Y="26.306416015625" />
                  <Point X="-3.641709960938" Y="26.324630859375" />
                  <Point X="-3.622778808594" Y="26.3329609375" />
                  <Point X="-3.604037109375" Y="26.34378125" />
                  <Point X="-3.587358398438" Y="26.356009765625" />
                  <Point X="-3.573721191406" Y="26.37155859375" />
                  <Point X="-3.561306884766" Y="26.38928515625" />
                  <Point X="-3.551362792969" Y="26.407404296875" />
                  <Point X="-3.549883056641" Y="26.410974609375" />
                  <Point X="-3.526703613281" Y="26.466935546875" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.53205078125" Y="26.58937890625" />
                  <Point X="-3.533822021484" Y="26.59278125" />
                  <Point X="-3.561790527344" Y="26.6465078125" />
                  <Point X="-3.573282470703" Y="26.66370703125" />
                  <Point X="-3.5871953125" Y="26.680287109375" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.351859375" Y="27.269875" />
                  <Point X="-4.081154541016" Y="27.73365625" />
                  <Point X="-4.062411132812" Y="27.757748046875" />
                  <Point X="-3.750503173828" Y="28.158662109375" />
                  <Point X="-3.245560302734" Y="27.8671328125" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729492188" Y="27.836341796875" />
                  <Point X="-3.167087402344" Y="27.82983203125" />
                  <Point X="-3.146786621094" Y="27.825794921875" />
                  <Point X="-3.141691650391" Y="27.825349609375" />
                  <Point X="-3.061237304688" Y="27.818310546875" />
                  <Point X="-3.040567138672" Y="27.81876171875" />
                  <Point X="-3.019112304688" Y="27.8215859375" />
                  <Point X="-2.999024902344" Y="27.8265" />
                  <Point X="-2.980476806641" Y="27.83564453125" />
                  <Point X="-2.962224365234" Y="27.84726953125" />
                  <Point X="-2.946096679688" Y="27.860208984375" />
                  <Point X="-2.942460449219" Y="27.86384375" />
                  <Point X="-2.885353271484" Y="27.920951171875" />
                  <Point X="-2.872406738281" Y="27.937083984375" />
                  <Point X="-2.860777587891" Y="27.955337890625" />
                  <Point X="-2.851628662109" Y="27.973888671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.843881591797" Y="28.04121484375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854956054688" Y="28.141958984375" />
                  <Point X="-2.86146484375" Y="28.1626015625" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-2.700622558594" Y="29.09468359375" />
                  <Point X="-2.671110351562" Y="29.111080078125" />
                  <Point X="-2.167036621094" Y="29.391134765625" />
                  <Point X="-2.055104003906" Y="29.245259765625" />
                  <Point X="-2.043194458008" Y="29.229740234375" />
                  <Point X="-2.028893310547" Y="29.21480078125" />
                  <Point X="-2.012321777344" Y="29.20089453125" />
                  <Point X="-1.995135742188" Y="29.189408203125" />
                  <Point X="-1.989442993164" Y="29.186443359375" />
                  <Point X="-1.899897460938" Y="29.139828125" />
                  <Point X="-1.880614257812" Y="29.132330078125" />
                  <Point X="-1.859704956055" Y="29.126728515625" />
                  <Point X="-1.839261108398" Y="29.12358203125" />
                  <Point X="-1.81862109375" Y="29.124935546875" />
                  <Point X="-1.797303466797" Y="29.1286953125" />
                  <Point X="-1.777439208984" Y="29.13448828125" />
                  <Point X="-1.771532836914" Y="29.136935546875" />
                  <Point X="-1.678279785156" Y="29.1755625" />
                  <Point X="-1.660146118164" Y="29.185509765625" />
                  <Point X="-1.642416381836" Y="29.197923828125" />
                  <Point X="-1.626864135742" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595479980469" Y="29.265921875" />
                  <Point X="-1.593557495117" Y="29.27201953125" />
                  <Point X="-1.563200561523" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-0.949625366211" Y="29.80980859375" />
                  <Point X="-0.913858764648" Y="29.81399609375" />
                  <Point X="-0.294711273193" Y="29.886458984375" />
                  <Point X="-0.145412063599" Y="29.329265625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113998413" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.307419464111" Y="29.8879375" />
                  <Point X="0.844041748047" Y="29.83173828125" />
                  <Point X="0.873642700195" Y="29.824591796875" />
                  <Point X="1.481026855469" Y="29.67794921875" />
                  <Point X="1.498905029297" Y="29.671466796875" />
                  <Point X="1.894653442383" Y="29.52792578125" />
                  <Point X="1.913282592773" Y="29.519212890625" />
                  <Point X="2.294550048828" Y="29.340908203125" />
                  <Point X="2.312609619141" Y="29.330384765625" />
                  <Point X="2.680980957031" Y="29.115771484375" />
                  <Point X="2.697958984375" Y="29.103697265625" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.198895263672" Y="27.6399765625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.140825439453" Y="27.536748046875" />
                  <Point X="2.131798095703" Y="27.5112734375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.1084453125" Y="27.414685546875" />
                  <Point X="2.107606201172" Y="27.3916484375" />
                  <Point X="2.1082265625" Y="27.37681640625" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121440185547" Y="27.289609375" />
                  <Point X="2.129702392578" Y="27.26752734375" />
                  <Point X="2.140061279297" Y="27.247486328125" />
                  <Point X="2.142630126953" Y="27.24369921875" />
                  <Point X="2.183029541016" Y="27.184162109375" />
                  <Point X="2.196981689453" Y="27.16790234375" />
                  <Point X="2.214051269531" Y="27.152044921875" />
                  <Point X="2.225367675781" Y="27.14303515625" />
                  <Point X="2.284906005859" Y="27.102634765625" />
                  <Point X="2.304955078125" Y="27.092271484375" />
                  <Point X="2.327050048828" Y="27.08400390625" />
                  <Point X="2.34896484375" Y="27.078662109375" />
                  <Point X="2.353093261719" Y="27.0781640625" />
                  <Point X="2.418383544922" Y="27.070291015625" />
                  <Point X="2.440196044922" Y="27.070181640625" />
                  <Point X="2.463880371094" Y="27.072798828125" />
                  <Point X="2.477988525391" Y="27.07544921875" />
                  <Point X="2.553493408203" Y="27.095640625" />
                  <Point X="2.565287353516" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.967325195312" Y="27.90619140625" />
                  <Point X="4.123274414062" Y="27.689458984375" />
                  <Point X="4.132739257812" Y="27.67381640625" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.298343505859" Y="26.72029296875" />
                  <Point X="3.230783447266" Y="26.668451171875" />
                  <Point X="3.218820800781" Y="26.65753125" />
                  <Point X="3.200531982422" Y="26.637138671875" />
                  <Point X="3.146191162109" Y="26.56624609375" />
                  <Point X="3.135047363281" Y="26.54763671875" />
                  <Point X="3.125296142578" Y="26.5261015625" />
                  <Point X="3.120347900391" Y="26.512501953125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.098791748047" Y="26.36666796875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.122241455078" Y="26.265416015625" />
                  <Point X="3.132301513672" Y="26.243689453125" />
                  <Point X="3.13914453125" Y="26.231390625" />
                  <Point X="3.184340087891" Y="26.1626953125" />
                  <Point X="3.198895751953" Y="26.14544921875" />
                  <Point X="3.216145019531" Y="26.12935546875" />
                  <Point X="3.234364990234" Y="26.1160234375" />
                  <Point X="3.238512695312" Y="26.113689453125" />
                  <Point X="3.303987548828" Y="26.07683203125" />
                  <Point X="3.324498535156" Y="26.06826953125" />
                  <Point X="3.348090087891" Y="26.06153125" />
                  <Point X="3.361732910156" Y="26.058697265625" />
                  <Point X="3.450278320312" Y="26.046994140625" />
                  <Point X="3.462697998047" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.848919921875" Y="25.91364453125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.794005126953" Y="25.3503359375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.701063232422" Y="25.3239453125" />
                  <Point X="3.676036865234" Y="25.3118828125" />
                  <Point X="3.589036376953" Y="25.261595703125" />
                  <Point X="3.571361083984" Y="25.24842578125" />
                  <Point X="3.553743164062" Y="25.23179296875" />
                  <Point X="3.544225097656" Y="25.221365234375" />
                  <Point X="3.492024902344" Y="25.154849609375" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463681152344" Y="25.09260546875" />
                  <Point X="3.462579101562" Y="25.0868515625" />
                  <Point X="3.445178955078" Y="24.995994140625" />
                  <Point X="3.443578369141" Y="24.973865234375" />
                  <Point X="3.444680419922" Y="24.9493046875" />
                  <Point X="3.446280517578" Y="24.935693359375" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480300292969" Y="24.80187109375" />
                  <Point X="3.492020263672" Y="24.782595703125" />
                  <Point X="3.495325927734" Y="24.7783828125" />
                  <Point X="3.547526123047" Y="24.7118671875" />
                  <Point X="3.563505371094" Y="24.69586328125" />
                  <Point X="3.583333007812" Y="24.680255859375" />
                  <Point X="3.594550537109" Y="24.67265625" />
                  <Point X="3.681544433594" Y="24.62237109375" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.851199707031" Y="24.034521484375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.514173339844" Y="23.98473828125" />
                  <Point X="3.424381591797" Y="23.99655859375" />
                  <Point X="3.401637451172" Y="23.996806640625" />
                  <Point X="3.373678955078" Y="23.993744140625" />
                  <Point X="3.363845703125" Y="23.992140625" />
                  <Point X="3.193094482422" Y="23.95502734375" />
                  <Point X="3.162729980469" Y="23.944310546875" />
                  <Point X="3.131485595703" Y="23.9239609375" />
                  <Point X="3.112449707031" Y="23.90532421875" />
                  <Point X="3.105861572266" Y="23.898177734375" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.986627197266" Y="23.749583984375" />
                  <Point X="2.974509765625" Y="23.717607421875" />
                  <Point X="2.969977294922" Y="23.692875" />
                  <Point X="2.968820800781" Y="23.684455078125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.955109130859" Y="23.491515625" />
                  <Point X="2.965241455078" Y="23.455337890625" />
                  <Point X="2.977737060547" Y="23.4308671875" />
                  <Point X="2.982434570312" Y="23.4226953125" />
                  <Point X="3.076930664062" Y="23.275712890625" />
                  <Point X="3.086931640625" Y="23.262763671875" />
                  <Point X="3.110628662109" Y="23.23908984375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.124814941406" Y="22.250220703125" />
                  <Point X="4.116907226562" Y="22.238986328125" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="2.880926269531" Y="22.7768828125" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.77939453125" Y="22.832173828125" />
                  <Point X="2.750411376953" Y="22.840400390625" />
                  <Point X="2.741354980469" Y="22.842498046875" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.505974853516" Y="22.880916015625" />
                  <Point X="2.468744628906" Y="22.8738359375" />
                  <Point X="2.442381835938" Y="22.8630390625" />
                  <Point X="2.434142333984" Y="22.8591953125" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.241146240234" Y="22.75387109375" />
                  <Point X="2.217397949219" Y="22.728474609375" />
                  <Point X="2.202880126953" Y="22.70571484375" />
                  <Point X="2.198905273438" Y="22.698869140625" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.098733398438" Y="22.499890625" />
                  <Point X="2.094302734375" Y="22.461953125" />
                  <Point X="2.096839355469" Y="22.43258203125" />
                  <Point X="2.097999511719" Y="22.423873046875" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138986328125" Y="22.204853515625" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781839599609" Y="20.88834765625" />
                  <Point X="2.773014892578" Y="20.88263671875" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="1.819571655273" Y="21.986212890625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.741662109375" Y="22.082775390625" />
                  <Point X="1.716340820313" Y="22.102556640625" />
                  <Point X="1.709230957031" Y="22.107603515625" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479748535156" Y="22.250353515625" />
                  <Point X="1.442078735352" Y="22.2580625" />
                  <Point X="1.411740234375" Y="22.25800390625" />
                  <Point X="1.403219116211" Y="22.257603515625" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.155378173828" Y="22.23146484375" />
                  <Point X="1.123302978516" Y="22.216794921875" />
                  <Point X="1.099751220703" Y="22.200134765625" />
                  <Point X="1.093876831055" Y="22.195626953125" />
                  <Point X="0.924611938477" Y="22.054888671875" />
                  <Point X="0.90261114502" Y="22.03135546875" />
                  <Point X="0.88383605957" Y="21.997470703125" />
                  <Point X="0.874513061523" Y="21.967400390625" />
                  <Point X="0.872419433594" Y="21.9594453125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.67687890625" />
                  <Point X="1.022065002441" Y="20.140083984375" />
                  <Point X="0.97567364502" Y="20.129916015625" />
                  <Point X="0.967506103516" Y="20.128431640625" />
                  <Point X="0.929315917969" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058434570312" Y="20.2473671875" />
                  <Point X="-1.06905859375" Y="20.250099609375" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.123647705078" Y="20.402345703125" />
                  <Point X="-1.120775878906" Y="20.424158203125" />
                  <Point X="-1.119964599609" Y="20.436025390625" />
                  <Point X="-1.119831542969" Y="20.45976953125" />
                  <Point X="-1.120509765625" Y="20.471646484375" />
                  <Point X="-1.124647338867" Y="20.50605078125" />
                  <Point X="-1.126938842773" Y="20.5204296875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.186859863281" Y="20.817953125" />
                  <Point X="-1.196860961914" Y="20.84812890625" />
                  <Point X="-1.206908203125" Y="20.869275390625" />
                  <Point X="-1.227897827148" Y="20.902232421875" />
                  <Point X="-1.240104980469" Y="20.917619140625" />
                  <Point X="-1.264330566406" Y="20.942392578125" />
                  <Point X="-1.2748984375" Y="20.952400390625" />
                  <Point X="-1.494267456055" Y="21.144783203125" />
                  <Point X="-1.50355456543" Y="21.15196484375" />
                  <Point X="-1.529853271484" Y="21.169814453125" />
                  <Point X="-1.550695922852" Y="21.180474609375" />
                  <Point X="-1.587417480469" Y="21.19384765625" />
                  <Point X="-1.606474243164" Y="21.198625" />
                  <Point X="-1.640776123047" Y="21.20353125" />
                  <Point X="-1.655252685547" Y="21.2050390625" />
                  <Point X="-1.946404052734" Y="21.22412109375" />
                  <Point X="-1.958145263672" Y="21.2241640625" />
                  <Point X="-1.989875366211" Y="21.222314453125" />
                  <Point X="-2.012898803711" Y="21.21808203125" />
                  <Point X="-2.050168457031" Y="21.206337890625" />
                  <Point X="-2.068192382813" Y="21.198529296875" />
                  <Point X="-2.098393066406" Y="21.181541015625" />
                  <Point X="-2.110799804687" Y="21.173923828125" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.3596875" Y="21.007240234375" />
                  <Point X="-2.380446533203" Y="20.989865234375" />
                  <Point X="-2.402760986328" Y="20.9672265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503201171875" Y="20.837521484375" />
                  <Point X="-2.747603271484" Y="20.98884765625" />
                  <Point X="-2.762283935547" Y="21.00015234375" />
                  <Point X="-2.980862792969" Y="21.16844921875" />
                  <Point X="-2.385823730469" Y="22.19908984375" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311628417969" Y="22.3811015625" />
                  <Point X="-2.310705322266" Y="22.412697265625" />
                  <Point X="-2.315051269531" Y="22.44400390625" />
                  <Point X="-2.324544921875" Y="22.47415234375" />
                  <Point X="-2.330554199219" Y="22.488830078125" />
                  <Point X="-2.345805419922" Y="22.51869140625" />
                  <Point X="-2.353580810547" Y="22.531357421875" />
                  <Point X="-2.371011962891" Y="22.55532421875" />
                  <Point X="-2.380667724609" Y="22.566625" />
                  <Point X="-2.396983154297" Y="22.582939453125" />
                  <Point X="-2.408822753906" Y="22.592984375" />
                  <Point X="-2.433978515625" Y="22.611009765625" />
                  <Point X="-2.447294677734" Y="22.618990234375" />
                  <Point X="-2.476123535156" Y="22.63320703125" />
                  <Point X="-2.490564697266" Y="22.6389140625" />
                  <Point X="-2.520183105469" Y="22.6478984375" />
                  <Point X="-2.550870605469" Y="22.6519375" />
                  <Point X="-2.581804931641" Y="22.650923828125" />
                  <Point X="-2.597229003906" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672650146484" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-4.004022216797" Y="22.2594140625" />
                  <Point X="-4.014553466797" Y="22.27707421875" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.125694580078" Y="23.36659375" />
                  <Point X="-3.048122802734" Y="23.426115234375" />
                  <Point X="-3.035923095703" Y="23.43729296875" />
                  <Point X="-3.013647216797" Y="23.461595703125" />
                  <Point X="-3.003571044922" Y="23.474720703125" />
                  <Point X="-2.985146972656" Y="23.503623046875" />
                  <Point X="-2.978145751953" Y="23.516783203125" />
                  <Point X="-2.966288330078" Y="23.54403125" />
                  <Point X="-2.961432128906" Y="23.558119140625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552490234" Y="23.60119921875" />
                  <Point X="-2.948748779297" Y="23.6316328125" />
                  <Point X="-2.948578857422" Y="23.646962890625" />
                  <Point X="-2.950790039062" Y="23.678638671875" />
                  <Point X="-2.953183837891" Y="23.69419921875" />
                  <Point X="-2.960508056641" Y="23.724708984375" />
                  <Point X="-2.972770019531" Y="23.75358984375" />
                  <Point X="-2.989633789062" Y="23.78005078125" />
                  <Point X="-2.999166015625" Y="23.792580078125" />
                  <Point X="-3.021584716797" Y="23.81746484375" />
                  <Point X="-3.032081054688" Y="23.82746484375" />
                  <Point X="-3.054474609375" Y="23.84575" />
                  <Point X="-3.066371826172" Y="23.85403515625" />
                  <Point X="-3.091266357422" Y="23.8686875" />
                  <Point X="-3.105430175781" Y="23.875515625" />
                  <Point X="-3.134692138672" Y="23.886740234375" />
                  <Point X="-3.149790283203" Y="23.89113671875" />
                  <Point X="-3.181677246094" Y="23.897619140625" />
                  <Point X="-3.197293945312" Y="23.89946484375" />
                  <Point X="-3.228619628906" Y="23.900556640625" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.025884765625" />
                  <Point X="-4.743549316406" Y="24.045369140625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.596654052734" Y="24.6641484375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.499665771484" Y="24.690583984375" />
                  <Point X="-3.471593505859" Y="24.701896484375" />
                  <Point X="-3.441720947266" Y="24.71766796875" />
                  <Point X="-3.431907226562" Y="24.723634765625" />
                  <Point X="-3.405807128906" Y="24.74175" />
                  <Point X="-3.393073242188" Y="24.752345703125" />
                  <Point X="-3.369639404297" Y="24.77558984375" />
                  <Point X="-3.358939453125" Y="24.78823828125" />
                  <Point X="-3.339144775391" Y="24.81626953125" />
                  <Point X="-3.331533447266" Y="24.8290703125" />
                  <Point X="-3.318404541016" Y="24.855708984375" />
                  <Point X="-3.312886962891" Y="24.869546875" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.300869873047" Y="24.9123203125" />
                  <Point X="-3.296614990234" Y="24.94214453125" />
                  <Point X="-3.295677246094" Y="24.9572265625" />
                  <Point X="-3.296227783203" Y="24.988642578125" />
                  <Point X="-3.297414794922" Y="25.002041015625" />
                  <Point X="-3.301669189453" Y="25.02853515625" />
                  <Point X="-3.304736572266" Y="25.041630859375" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.319737548828" Y="25.0850546875" />
                  <Point X="-3.334927490234" Y="25.11450390625" />
                  <Point X="-3.343816650391" Y="25.1285625" />
                  <Point X="-3.364713378906" Y="25.15596484375" />
                  <Point X="-3.374602539062" Y="25.167021484375" />
                  <Point X="-3.395975830078" Y="25.18745703125" />
                  <Point X="-3.407459960938" Y="25.1968359375" />
                  <Point X="-3.433560058594" Y="25.214951171875" />
                  <Point X="-3.440476806641" Y="25.21932421875" />
                  <Point X="-3.465612304688" Y="25.232833984375" />
                  <Point X="-3.496564697266" Y="25.24563671875" />
                  <Point X="-3.508287841797" Y="25.24961328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.73133203125" Y="25.9575234375" />
                  <Point X="-4.725717773438" Y="25.9782421875" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-3.841408447266" Y="26.213943359375" />
                  <Point X="-3.778065429688" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747068847656" Y="26.204365234375" />
                  <Point X="-3.736726074219" Y="26.204703125" />
                  <Point X="-3.715178466797" Y="26.2065859375" />
                  <Point X="-3.704939697266" Y="26.208044921875" />
                  <Point X="-3.684673828125" Y="26.2120703125" />
                  <Point X="-3.670909667969" Y="26.2158125" />
                  <Point X="-3.613141845703" Y="26.23402734375" />
                  <Point X="-3.603448486328" Y="26.23767578125" />
                  <Point X="-3.584517333984" Y="26.246005859375" />
                  <Point X="-3.575279541016" Y="26.2506875" />
                  <Point X="-3.556537841797" Y="26.2615078125" />
                  <Point X="-3.547865234375" Y="26.26716796875" />
                  <Point X="-3.531186523438" Y="26.279396484375" />
                  <Point X="-3.515936523438" Y="26.293369140625" />
                  <Point X="-3.502299316406" Y="26.30891796875" />
                  <Point X="-3.495906005859" Y="26.3170625" />
                  <Point X="-3.483491699219" Y="26.3347890625" />
                  <Point X="-3.478024902344" Y="26.343578125" />
                  <Point X="-3.468080810547" Y="26.361697265625" />
                  <Point X="-3.462114257812" Y="26.37462109375" />
                  <Point X="-3.438934814453" Y="26.43058203125" />
                  <Point X="-3.435498291016" Y="26.440353515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012939453" Y="26.604533203125" />
                  <Point X="-3.443510742188" Y="26.6238125" />
                  <Point X="-3.449556884766" Y="26.6366484375" />
                  <Point X="-3.477525390625" Y="26.690375" />
                  <Point X="-3.482800537109" Y="26.699287109375" />
                  <Point X="-3.494292480469" Y="26.716486328125" />
                  <Point X="-3.500509277344" Y="26.7247734375" />
                  <Point X="-3.514422119141" Y="26.741353515625" />
                  <Point X="-3.521501220703" Y="26.748912109375" />
                  <Point X="-3.536441894531" Y="26.76321484375" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.227613769531" Y="27.294283203125" />
                  <Point X="-4.002294677734" Y="27.68030859375" />
                  <Point X="-3.987430664062" Y="27.6994140625" />
                  <Point X="-3.726336669922" Y="28.035013671875" />
                  <Point X="-3.293060302734" Y="27.784861328125" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244924804688" Y="27.757720703125" />
                  <Point X="-3.225997314453" Y="27.749390625" />
                  <Point X="-3.216302001953" Y="27.745740234375" />
                  <Point X="-3.195659912109" Y="27.73923046875" />
                  <Point X="-3.185616699219" Y="27.73665625" />
                  <Point X="-3.165315917969" Y="27.732619140625" />
                  <Point X="-3.149963378906" Y="27.7307109375" />
                  <Point X="-3.069509033203" Y="27.723671875" />
                  <Point X="-3.059164306641" Y="27.723333984375" />
                  <Point X="-3.038494140625" Y="27.72378515625" />
                  <Point X="-3.028168701172" Y="27.72457421875" />
                  <Point X="-3.006713867188" Y="27.7273984375" />
                  <Point X="-2.996537841797" Y="27.729306640625" />
                  <Point X="-2.976450439453" Y="27.734220703125" />
                  <Point X="-2.957016357422" Y="27.74129296875" />
                  <Point X="-2.938468261719" Y="27.7504375" />
                  <Point X="-2.929442871094" Y="27.755515625" />
                  <Point X="-2.911190429688" Y="27.767140625" />
                  <Point X="-2.902773925781" Y="27.773169921875" />
                  <Point X="-2.886646240234" Y="27.786109375" />
                  <Point X="-2.87528515625" Y="27.79666796875" />
                  <Point X="-2.818177978516" Y="27.853775390625" />
                  <Point X="-2.811260986328" Y="27.8614921875" />
                  <Point X="-2.798314453125" Y="27.877625" />
                  <Point X="-2.792284912109" Y="27.886041015625" />
                  <Point X="-2.780655761719" Y="27.904294921875" />
                  <Point X="-2.775575927734" Y="27.913318359375" />
                  <Point X="-2.766427001953" Y="27.931869140625" />
                  <Point X="-2.759351318359" Y="27.95130859375" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.749243164062" Y="28.049494140625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.76178125" Y="28.1604921875" />
                  <Point X="-2.764353271484" Y="28.17052734375" />
                  <Point X="-2.770862060547" Y="28.191169921875" />
                  <Point X="-2.774510498047" Y="28.20086328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522705078" Y="28.229033203125" />
                  <Point X="-3.059386474609" Y="28.6999140625" />
                  <Point X="-2.648372070312" Y="29.01503515625" />
                  <Point X="-2.624972412109" Y="29.02803515625" />
                  <Point X="-2.192524414062" Y="29.268294921875" />
                  <Point X="-2.130472900391" Y="29.187427734375" />
                  <Point X="-2.111819580078" Y="29.164046875" />
                  <Point X="-2.097518554688" Y="29.149107421875" />
                  <Point X="-2.0899609375" Y="29.142029296875" />
                  <Point X="-2.073389404297" Y="29.128123046875" />
                  <Point X="-2.065110351563" Y="29.121912109375" />
                  <Point X="-2.047924438477" Y="29.11042578125" />
                  <Point X="-2.033309814453" Y="29.102177734375" />
                  <Point X="-1.943764160156" Y="29.0555625" />
                  <Point X="-1.934325805664" Y="29.051287109375" />
                  <Point X="-1.915042724609" Y="29.0437890625" />
                  <Point X="-1.905197753906" Y="29.04056640625" />
                  <Point X="-1.884288452148" Y="29.03496484375" />
                  <Point X="-1.87415612793" Y="29.032833984375" />
                  <Point X="-1.853712158203" Y="29.0296875" />
                  <Point X="-1.833044677734" Y="29.02878515625" />
                  <Point X="-1.812404663086" Y="29.030138671875" />
                  <Point X="-1.802120727539" Y="29.03137890625" />
                  <Point X="-1.780803100586" Y="29.035138671875" />
                  <Point X="-1.770706787109" Y="29.037494140625" />
                  <Point X="-1.750842529297" Y="29.043287109375" />
                  <Point X="-1.735177734375" Y="29.04916796875" />
                  <Point X="-1.641924682617" Y="29.087794921875" />
                  <Point X="-1.63258996582" Y="29.092271484375" />
                  <Point X="-1.614456420898" Y="29.10221875" />
                  <Point X="-1.605657592773" Y="29.107689453125" />
                  <Point X="-1.587927734375" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564227050781" Y="29.14013671875" />
                  <Point X="-1.550252075195" Y="29.155388671875" />
                  <Point X="-1.538020507812" Y="29.1720703125" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.516856079102" Y="29.208728515625" />
                  <Point X="-1.508525146484" Y="29.227662109375" />
                  <Point X="-1.502953979492" Y="29.243453125" />
                  <Point X="-1.472597045898" Y="29.339732421875" />
                  <Point X="-1.470025878906" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.37005078125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.931167358398" Y="29.7163203125" />
                  <Point X="-0.902811767578" Y="29.719640625" />
                  <Point X="-0.365222473145" Y="29.78255859375" />
                  <Point X="-0.237175094604" Y="29.304677734375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190612793" Y="29.7850078125" />
                  <Point X="0.827863647461" Y="29.7379140625" />
                  <Point X="0.85134765625" Y="29.732244140625" />
                  <Point X="1.453608886719" Y="29.586837890625" />
                  <Point X="1.466522216797" Y="29.58215625" />
                  <Point X="1.858260986328" Y="29.4400703125" />
                  <Point X="1.87303527832" Y="29.43316015625" />
                  <Point X="2.250414306641" Y="29.256673828125" />
                  <Point X="2.264780273438" Y="29.248302734375" />
                  <Point X="2.629445800781" Y="29.03584765625" />
                  <Point X="2.642901367188" Y="29.026279296875" />
                  <Point X="2.817779541016" Y="28.901916015625" />
                  <Point X="2.116622802734" Y="27.6874765625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.061629882812" Y="27.5915625" />
                  <Point X="2.051281494141" Y="27.568478515625" />
                  <Point X="2.042254150391" Y="27.54300390625" />
                  <Point X="2.040022827148" Y="27.535814453125" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017657592773" Y="27.449857421875" />
                  <Point X="2.01449609375" Y="27.428775390625" />
                  <Point X="2.013508300781" Y="27.41814453125" />
                  <Point X="2.012669189453" Y="27.395107421875" />
                  <Point X="2.012689208984" Y="27.387677734375" />
                  <Point X="2.013909667969" Y="27.365443359375" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023799926758" Y="27.289037109375" />
                  <Point X="2.029140869141" Y="27.267119140625" />
                  <Point X="2.032464355469" Y="27.256318359375" />
                  <Point X="2.04072644043" Y="27.234236328125" />
                  <Point X="2.045309448242" Y="27.22390625" />
                  <Point X="2.055668212891" Y="27.203865234375" />
                  <Point X="2.06401953125" Y="27.190357421875" />
                  <Point X="2.104418945313" Y="27.1308203125" />
                  <Point X="2.11093359375" Y="27.122298828125" />
                  <Point X="2.124885742188" Y="27.1060390625" />
                  <Point X="2.132323242188" Y="27.09830078125" />
                  <Point X="2.149392822266" Y="27.082443359375" />
                  <Point X="2.154879150391" Y="27.077724609375" />
                  <Point X="2.172025634766" Y="27.064423828125" />
                  <Point X="2.231563964844" Y="27.0240234375" />
                  <Point X="2.241283935547" Y="27.0182421875" />
                  <Point X="2.261333007812" Y="27.00787890625" />
                  <Point X="2.271662109375" Y="27.003296875" />
                  <Point X="2.293757080078" Y="26.995029296875" />
                  <Point X="2.304552246094" Y="26.99170703125" />
                  <Point X="2.326467041016" Y="26.986365234375" />
                  <Point X="2.341715087891" Y="26.98384765625" />
                  <Point X="2.407005371094" Y="26.975974609375" />
                  <Point X="2.417907226562" Y="26.97529296875" />
                  <Point X="2.439719726562" Y="26.97518359375" />
                  <Point X="2.450630371094" Y="26.975755859375" />
                  <Point X="2.474314697266" Y="26.978373046875" />
                  <Point X="2.481420410156" Y="26.979431640625" />
                  <Point X="2.502531005859" Y="26.983673828125" />
                  <Point X="2.578035888672" Y="27.003865234375" />
                  <Point X="2.584006103516" Y="27.005673828125" />
                  <Point X="2.604410888672" Y="27.0130703125" />
                  <Point X="2.627657470703" Y="27.023576171875" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.940403076172" Y="27.780953125" />
                  <Point X="4.043955078125" Y="27.6370390625" />
                  <Point X="4.051459716797" Y="27.62463671875" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.240511230469" Y="26.795662109375" />
                  <Point X="3.172951171875" Y="26.7438203125" />
                  <Point X="3.166735839844" Y="26.738615234375" />
                  <Point X="3.148096679688" Y="26.720958984375" />
                  <Point X="3.129807861328" Y="26.70056640625" />
                  <Point X="3.125134277344" Y="26.69493359375" />
                  <Point X="3.070793457031" Y="26.624041015625" />
                  <Point X="3.064687255859" Y="26.615052734375" />
                  <Point X="3.053543457031" Y="26.596443359375" />
                  <Point X="3.048505859375" Y="26.586822265625" />
                  <Point X="3.038754638672" Y="26.565287109375" />
                  <Point X="3.036021972656" Y="26.558583984375" />
                  <Point X="3.028858154297" Y="26.538087890625" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.005751708984" Y="26.347470703125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.025188232422" Y="26.256380859375" />
                  <Point X="3.032021240234" Y="26.235662109375" />
                  <Point X="3.036034423828" Y="26.2255" />
                  <Point X="3.046094482422" Y="26.2037734375" />
                  <Point X="3.049286132812" Y="26.1975" />
                  <Point X="3.059780517578" Y="26.17917578125" />
                  <Point X="3.104976074219" Y="26.11048046875" />
                  <Point X="3.111741210938" Y="26.101421875" />
                  <Point X="3.126296875" Y="26.08417578125" />
                  <Point X="3.134087402344" Y="26.07598828125" />
                  <Point X="3.151336669922" Y="26.05989453125" />
                  <Point X="3.160045654297" Y="26.0526875" />
                  <Point X="3.178265625" Y="26.03935546875" />
                  <Point X="3.191924560547" Y="26.030896484375" />
                  <Point X="3.257385986328" Y="25.994046875" />
                  <Point X="3.267389892578" Y="25.9891640625" />
                  <Point X="3.287900878906" Y="25.9806015625" />
                  <Point X="3.298407714844" Y="25.976921875" />
                  <Point X="3.321999267578" Y="25.97018359375" />
                  <Point X="3.328768554688" Y="25.968517578125" />
                  <Point X="3.349284912109" Y="25.964515625" />
                  <Point X="3.437830322266" Y="25.9528125" />
                  <Point X="3.444032226562" Y="25.95219921875" />
                  <Point X="3.465708251953" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.75505078125" Y="25.899029296875" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.769417236328" Y="25.442099609375" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.684103515625" Y="25.4188671875" />
                  <Point X="3.659815429688" Y="25.4095234375" />
                  <Point X="3.6347890625" Y="25.3974609375" />
                  <Point X="3.62849609375" Y="25.394130859375" />
                  <Point X="3.541495605469" Y="25.34384375" />
                  <Point X="3.532275390625" Y="25.3377734375" />
                  <Point X="3.514600097656" Y="25.324603515625" />
                  <Point X="3.506145019531" Y="25.31750390625" />
                  <Point X="3.488527099609" Y="25.30087109375" />
                  <Point X="3.483577392578" Y="25.295837890625" />
                  <Point X="3.469490966797" Y="25.280015625" />
                  <Point X="3.417290771484" Y="25.2135" />
                  <Point X="3.410854003906" Y="25.204208984375" />
                  <Point X="3.399129394531" Y="25.184927734375" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004394531" Y="25.14292578125" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.369275146484" Y="25.10472265625" />
                  <Point X="3.351875" Y="25.013865234375" />
                  <Point X="3.350426513672" Y="25.00284765625" />
                  <Point X="3.348825927734" Y="24.98071875" />
                  <Point X="3.348673828125" Y="24.969607421875" />
                  <Point X="3.349775878906" Y="24.945046875" />
                  <Point X="3.350330078125" Y="24.938212890625" />
                  <Point X="3.352976074219" Y="24.91782421875" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067626953" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.762501953125" />
                  <Point X="3.399127441406" Y="24.752515625" />
                  <Point X="3.410847412109" Y="24.733240234375" />
                  <Point X="3.420587158203" Y="24.71973828125" />
                  <Point X="3.472787353516" Y="24.65322265625" />
                  <Point X="3.480299316406" Y="24.644744140625" />
                  <Point X="3.496278564453" Y="24.628740234375" />
                  <Point X="3.504745849609" Y="24.62121484375" />
                  <Point X="3.524573486328" Y="24.605607421875" />
                  <Point X="3.530049316406" Y="24.60160546875" />
                  <Point X="3.547008544922" Y="24.590408203125" />
                  <Point X="3.634002441406" Y="24.540123046875" />
                  <Point X="3.639491210938" Y="24.537185546875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.758580566406" Y="24.05565625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.526573486328" Y="24.07892578125" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.425417480469" Y="24.091552734375" />
                  <Point X="3.402673339844" Y="24.09180078125" />
                  <Point X="3.391293212891" Y="24.0912421875" />
                  <Point X="3.363334716797" Y="24.0881796875" />
                  <Point X="3.343668212891" Y="24.08497265625" />
                  <Point X="3.172916992188" Y="24.047859375" />
                  <Point X="3.161476806641" Y="24.044611328125" />
                  <Point X="3.131112304688" Y="24.03389453125" />
                  <Point X="3.110883056641" Y="24.023916015625" />
                  <Point X="3.079638671875" Y="24.00356640625" />
                  <Point X="3.065025878906" Y="23.99184375" />
                  <Point X="3.045989990234" Y="23.97320703125" />
                  <Point X="3.032813720703" Y="23.9589140625" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.923183837891" Y="23.82610546875" />
                  <Point X="2.907157714844" Y="23.801638671875" />
                  <Point X="2.897791748047" Y="23.783248046875" />
                  <Point X="2.885674316406" Y="23.751271484375" />
                  <Point X="2.881065917969" Y="23.734732421875" />
                  <Point X="2.876533447266" Y="23.71" />
                  <Point X="2.874220458984" Y="23.69316015625" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.85908203125" Y="23.520515625" />
                  <Point X="2.860162597656" Y="23.488328125" />
                  <Point X="2.863629150391" Y="23.46589453125" />
                  <Point X="2.873761474609" Y="23.429716796875" />
                  <Point X="2.880633789062" Y="23.412134765625" />
                  <Point X="2.893129394531" Y="23.3876640625" />
                  <Point X="2.902524414062" Y="23.3713203125" />
                  <Point X="2.997020507812" Y="23.224337890625" />
                  <Point X="3.001743896484" Y="23.21764453125" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045499023438" Y="22.3025859375" />
                  <Point X="4.039222167969" Y="22.29366796875" />
                  <Point X="4.001272705078" Y="22.239748046875" />
                  <Point X="2.928426269531" Y="22.859154296875" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.837962158203" Y="22.91055078125" />
                  <Point X="2.816402099609" Y="22.919669921875" />
                  <Point X="2.805334472656" Y="22.923564453125" />
                  <Point X="2.776351318359" Y="22.931791015625" />
                  <Point X="2.758238525391" Y="22.935986328125" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.543198242188" Y="22.974064453125" />
                  <Point X="2.5110390625" Y="22.97578125" />
                  <Point X="2.488226806641" Y="22.974244140625" />
                  <Point X="2.450996582031" Y="22.9671640625" />
                  <Point X="2.432739990234" Y="22.961748046875" />
                  <Point X="2.406377197266" Y="22.950951171875" />
                  <Point X="2.389898193359" Y="22.943263671875" />
                  <Point X="2.221071533203" Y="22.854412109375" />
                  <Point X="2.211812988281" Y="22.848845703125" />
                  <Point X="2.187643554688" Y="22.832373046875" />
                  <Point X="2.171757324219" Y="22.8187578125" />
                  <Point X="2.148009033203" Y="22.793361328125" />
                  <Point X="2.137304931641" Y="22.779564453125" />
                  <Point X="2.122787109375" Y="22.7568046875" />
                  <Point X="2.114837402344" Y="22.74311328125" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.021113769531" Y="22.563431640625" />
                  <Point X="2.009794189453" Y="22.533279296875" />
                  <Point X="2.00437487793" Y="22.51091015625" />
                  <Point X="1.999944091797" Y="22.47297265625" />
                  <Point X="1.999655029297" Y="22.453779296875" />
                  <Point X="2.002191650391" Y="22.424408203125" />
                  <Point X="2.00451184082" Y="22.406990234375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051237792969" Y="22.168451171875" />
                  <Point X="2.064070800781" Y="22.137517578125" />
                  <Point X="2.069546875" Y="22.126419921875" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723753417969" Y="20.963916015625" />
                  <Point X="1.894940185547" Y="22.044044921875" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.826012207031" Y="22.132626953125" />
                  <Point X="1.809128173828" Y="22.149658203125" />
                  <Point X="1.800146484375" Y="22.157638671875" />
                  <Point X="1.774825317383" Y="22.177419921875" />
                  <Point X="1.76060546875" Y="22.187513671875" />
                  <Point X="1.560174682617" Y="22.31637109375" />
                  <Point X="1.549784423828" Y="22.322166015625" />
                  <Point X="1.520732666016" Y="22.33605859375" />
                  <Point X="1.498795288086" Y="22.343423828125" />
                  <Point X="1.461125366211" Y="22.3511328125" />
                  <Point X="1.441895263672" Y="22.3530625" />
                  <Point X="1.411556762695" Y="22.35300390625" />
                  <Point X="1.394514526367" Y="22.352203125" />
                  <Point X="1.175308959961" Y="22.332033203125" />
                  <Point X="1.164628662109" Y="22.330435546875" />
                  <Point X="1.135993164062" Y="22.324466796875" />
                  <Point X="1.115865478516" Y="22.317857421875" />
                  <Point X="1.083790283203" Y="22.3031875" />
                  <Point X="1.068440185547" Y="22.2943515625" />
                  <Point X="1.044888549805" Y="22.27769140625" />
                  <Point X="1.033139770508" Y="22.26867578125" />
                  <Point X="0.863874938965" Y="22.1279375" />
                  <Point X="0.855215270996" Y="22.119765625" />
                  <Point X="0.833214477539" Y="22.096232421875" />
                  <Point X="0.819514404297" Y="22.0773984375" />
                  <Point X="0.800739318848" Y="22.043513671875" />
                  <Point X="0.793097167969" Y="22.025603515625" />
                  <Point X="0.783774169922" Y="21.995533203125" />
                  <Point X="0.779587036133" Y="21.979623046875" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676828125" />
                  <Point X="0.725554992676" Y="21.664478515625" />
                  <Point X="0.833090820312" Y="20.847662109375" />
                  <Point X="0.678921630859" Y="21.423029296875" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.651129699707" Y="21.5236875" />
                  <Point X="0.641793212891" Y="21.54633203125" />
                  <Point X="0.636392272949" Y="21.557353515625" />
                  <Point X="0.619421325684" Y="21.586970703125" />
                  <Point X="0.610656738281" Y="21.600841796875" />
                  <Point X="0.456679992676" Y="21.822693359375" />
                  <Point X="0.449296386719" Y="21.8320234375" />
                  <Point X="0.42776940918" Y="21.855978515625" />
                  <Point X="0.410111968994" Y="21.8713203125" />
                  <Point X="0.377803588867" Y="21.893041015625" />
                  <Point X="0.360527069092" Y="21.9022265625" />
                  <Point X="0.330700683594" Y="21.91433203125" />
                  <Point X="0.315566711426" Y="21.9197421875" />
                  <Point X="0.077296401978" Y="21.993693359375" />
                  <Point X="0.066819076538" Y="21.996302734375" />
                  <Point X="0.038085517883" Y="22.00174609375" />
                  <Point X="0.016740444183" Y="22.0033359375" />
                  <Point X="-0.018948818207" Y="22.001958984375" />
                  <Point X="-0.03672857666" Y="21.999578125" />
                  <Point X="-0.066638191223" Y="21.9926484375" />
                  <Point X="-0.080071624756" Y="21.98901171875" />
                  <Point X="-0.318341766357" Y="21.9150625" />
                  <Point X="-0.329460540771" Y="21.91083203125" />
                  <Point X="-0.358786834717" Y="21.897515625" />
                  <Point X="-0.378854644775" Y="21.88519921875" />
                  <Point X="-0.409489685059" Y="21.86069140625" />
                  <Point X="-0.423526947021" Y="21.84683984375" />
                  <Point X="-0.444795349121" Y="21.820970703125" />
                  <Point X="-0.454118164062" Y="21.808642578125" />
                  <Point X="-0.608095031738" Y="21.58679296875" />
                  <Point X="-0.612470825195" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.985424560547" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.758137848789" Y="25.707028699484" />
                  <Point X="4.607840021339" Y="26.098567926222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.090027290064" Y="27.447516210026" />
                  <Point X="3.986767654227" Y="27.716516758168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.665869510765" Y="25.682305267474" />
                  <Point X="4.510976429397" Y="26.085815539943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.011421656159" Y="27.387200216919" />
                  <Point X="3.874810884942" Y="27.743083443138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.57360117274" Y="25.657581835465" />
                  <Point X="4.414112837454" Y="26.073063153664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932816022255" Y="27.326884223812" />
                  <Point X="3.791512940628" Y="27.694991336566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.481332834716" Y="25.632858403455" />
                  <Point X="4.317249245512" Y="26.060310767386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.854210388351" Y="27.266568230705" />
                  <Point X="3.708214996315" Y="27.646899229994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.389064496692" Y="25.608134971445" />
                  <Point X="4.22038565357" Y="26.047558381107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.775604754447" Y="27.206252237598" />
                  <Point X="3.624917052001" Y="27.598807123422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.296796158667" Y="25.583411539436" />
                  <Point X="4.123522061627" Y="26.034805994829" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696999120542" Y="27.145936244491" />
                  <Point X="3.541619107688" Y="27.55071501685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765779284041" Y="24.096578057584" />
                  <Point X="4.709391147233" Y="24.243474176161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204527820643" Y="25.558688107426" />
                  <Point X="4.026658469685" Y="26.02205360855" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618393486638" Y="27.085620251384" />
                  <Point X="3.458321163374" Y="27.502622910279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.729181862999" Y="23.926826928523" />
                  <Point X="4.595965963578" Y="24.273866211347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112259482618" Y="25.533964675417" />
                  <Point X="3.929794877743" Y="26.009301222272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.539787852734" Y="27.025304258277" />
                  <Point X="3.375023219061" Y="27.454530803707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624524502828" Y="23.934378002629" />
                  <Point X="4.482540779923" Y="24.304258246534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.019991144594" Y="25.509241243407" />
                  <Point X="3.8329312858" Y="25.996548835993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.461182218829" Y="26.96498826517" />
                  <Point X="3.291725274747" Y="27.406438697135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.75764626258" Y="28.797762091415" />
                  <Point X="2.680074254593" Y="28.999844081147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.517349468319" Y="23.948487842621" />
                  <Point X="4.369115596268" Y="24.33465028172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.92772280657" Y="25.484517811398" />
                  <Point X="3.736067693858" Y="25.983796449715" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382576584925" Y="26.904672272063" />
                  <Point X="3.208427330434" Y="27.358346590563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.696525190402" Y="28.691897257752" />
                  <Point X="2.545785111005" Y="29.084588590201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410174433811" Y="23.962597682613" />
                  <Point X="4.255690412613" Y="24.365042316906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.835454468545" Y="25.459794379388" />
                  <Point X="3.639204101916" Y="25.971044063436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.303970951021" Y="26.844356278956" />
                  <Point X="3.12512938612" Y="27.310254483991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.635404118225" Y="28.58603242409" />
                  <Point X="2.414713446101" Y="29.160951280719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.302999399302" Y="23.976707522605" />
                  <Point X="4.142265228958" Y="24.395434352093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.743186115982" Y="25.435070985252" />
                  <Point X="3.542340509973" Y="25.958291677158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.225365406148" Y="26.784040053914" />
                  <Point X="3.041831441807" Y="27.262162377419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.574283046047" Y="28.480167590427" />
                  <Point X="2.283641781197" Y="29.237313971237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.195824364794" Y="23.990817362597" />
                  <Point X="4.028840045302" Y="24.425826387279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.652573941191" Y="25.406033100515" />
                  <Point X="3.442876542924" Y="25.952313499634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147903759367" Y="26.720743872462" />
                  <Point X="2.958533497493" Y="27.214070270848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.51316197387" Y="28.374302756764" />
                  <Point X="2.157830975082" Y="29.299971656055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.088649330285" Y="24.004927202589" />
                  <Point X="3.915414861647" Y="24.456218422466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.568660217515" Y="25.359545154027" />
                  <Point X="3.335393675586" Y="25.967225271565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.079103954623" Y="26.634882821038" />
                  <Point X="2.87523555318" Y="27.165978164276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.452040901692" Y="28.268437923102" />
                  <Point X="2.033807639678" Y="29.357972820466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981474295777" Y="24.019037042581" />
                  <Point X="3.801989677992" Y="24.486610457652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.489185646936" Y="25.301492818349" />
                  <Point X="3.213954059307" Y="26.018495617538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.022807076719" Y="26.516450531629" />
                  <Point X="2.791937608866" Y="27.117886057704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390919829515" Y="28.162573089439" />
                  <Point X="1.909784304275" Y="29.415973984878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.874299261268" Y="24.033146882573" />
                  <Point X="3.688483358169" Y="24.517213859783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.419918725749" Y="25.216848646865" />
                  <Point X="2.708639664553" Y="27.069793951132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.329798757338" Y="28.056708255776" />
                  <Point X="1.789154080861" Y="29.465135790351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.76712422676" Y="24.047256722565" />
                  <Point X="3.561941256276" Y="24.581776635233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.366587736916" Y="25.090689952267" />
                  <Point X="2.625070944467" Y="27.022407239562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.26867768516" Y="27.950843422114" />
                  <Point X="1.670935808934" Y="29.508014247381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.659949192251" Y="24.061366562557" />
                  <Point X="2.534861710438" Y="26.992319658251" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.207556612983" Y="27.844978588451" />
                  <Point X="1.552717537006" Y="29.550892704411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.552774157743" Y="24.075476402549" />
                  <Point X="2.439680779942" Y="26.975183789041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.146435540805" Y="27.739113754789" />
                  <Point X="1.435480610076" Y="29.591214670322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.445599479739" Y="24.089585313814" />
                  <Point X="2.334114542702" Y="26.98510256886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085314477478" Y="27.63324889807" />
                  <Point X="1.323327759179" Y="29.618292165353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.035343977747" Y="22.288157700676" />
                  <Point X="3.969162424645" Y="22.460566540945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.345496870241" Y="24.085270856749" />
                  <Point X="2.212432060481" Y="27.037005602244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032194573671" Y="27.50654030818" />
                  <Point X="1.211174908282" Y="29.645369660385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.938186570091" Y="22.276170730499" />
                  <Point X="3.824915830939" Y="22.571251094415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.251539250294" Y="24.064948154603" />
                  <Point X="1.099022057385" Y="29.672447155416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.807454527078" Y="22.351648675743" />
                  <Point X="3.680669237233" Y="22.681935647884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.158051159933" Y="24.043402286067" />
                  <Point X="0.986869206488" Y="29.699524650448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.676722484065" Y="22.427126620988" />
                  <Point X="3.536422643526" Y="22.792620201353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073480356668" Y="23.998626090432" />
                  <Point X="0.874716355591" Y="29.72660214548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545990441052" Y="22.502604566232" />
                  <Point X="3.39217604982" Y="22.903304754822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001446719651" Y="23.921189460101" />
                  <Point X="0.766133697381" Y="29.744378970583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.415258398039" Y="22.578082511476" />
                  <Point X="3.247929456114" Y="23.013989308291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.931828431207" Y="23.837460631613" />
                  <Point X="0.660112704381" Y="29.755482429662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284526355025" Y="22.65356045672" />
                  <Point X="3.103682862407" Y="23.124673861761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.877329586127" Y="23.714344306556" />
                  <Point X="0.554091711381" Y="29.76658588874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.153794312012" Y="22.729038401964" />
                  <Point X="2.914241168859" Y="23.353095675606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859954852271" Y="23.494516365312" />
                  <Point X="0.448070718381" Y="29.777689347818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.023062268999" Y="22.804516347208" />
                  <Point X="0.363931014877" Y="29.731790098907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.892330148535" Y="22.879994494219" />
                  <Point X="0.322099813735" Y="29.575673433152" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.770135935478" Y="22.933230632008" />
                  <Point X="0.280268612592" Y="29.419556767397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.660546891427" Y="22.953629181862" />
                  <Point X="0.23843741145" Y="29.263440101642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.551306347231" Y="22.973119858552" />
                  <Point X="0.177109259546" Y="29.158114729111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.451776830967" Y="22.967312442572" />
                  <Point X="0.096213470666" Y="29.103764793685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.364400618906" Y="22.929844586714" />
                  <Point X="0.001614341094" Y="29.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.27974437784" Y="22.885290964157" />
                  <Point X="-0.11251343122" Y="29.117335622468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.359105729393" Y="29.759730521877" />
                  <Point X="-0.36775483529" Y="29.782262213067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.196089295452" Y="22.838129234079" />
                  <Point X="-0.465138512781" Y="29.770864695965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.124510795515" Y="22.759506931117" />
                  <Point X="-0.562522190272" Y="29.759467178863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.70026392773" Y="20.994528072005" />
                  <Point X="2.65506610584" Y="21.11227242356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.065228328477" Y="22.648852367312" />
                  <Point X="-0.659905867764" Y="29.748069661761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496640232889" Y="21.259895262335" />
                  <Point X="2.351425503451" Y="21.638192566025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.009035699657" Y="22.530148499753" />
                  <Point X="-0.757289545255" Y="29.736672144659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293016538048" Y="21.525262452664" />
                  <Point X="-0.854673222746" Y="29.725274627557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.089392843206" Y="21.790629642993" />
                  <Point X="-0.950873920132" Y="29.710795341916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.885769009048" Y="22.055997196254" />
                  <Point X="-1.042745404587" Y="29.685038071015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.72465324509" Y="22.210627440677" />
                  <Point X="-1.134616889043" Y="29.659280800114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.589553630263" Y="22.297483299494" />
                  <Point X="-1.226488373499" Y="29.633523529213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.467718682487" Y="22.349783519228" />
                  <Point X="-1.318359857955" Y="29.607766258312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.36603695787" Y="22.349582797692" />
                  <Point X="-1.410231342411" Y="29.582008987411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.267749762105" Y="22.340539026165" />
                  <Point X="-1.466062501604" Y="29.462363459279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.169584761497" Y="22.331176925371" />
                  <Point X="-1.494311405258" Y="29.270863698862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.07951483527" Y="22.300726434828" />
                  <Point X="-1.551303137716" Y="29.154241567453" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.000482421158" Y="22.241522242173" />
                  <Point X="-1.629850646915" Y="29.093774154311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.923343974205" Y="22.177384096384" />
                  <Point X="-1.717325298551" Y="29.056562742312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.847053918965" Y="22.111035814619" />
                  <Point X="-1.80909408169" Y="29.030537925335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.786391677156" Y="22.003975686981" />
                  <Point X="-1.916096828715" Y="29.044198941086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.748638399167" Y="21.837235668213" />
                  <Point X="-2.041992631986" Y="29.107078051065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.73200686745" Y="21.615471619203" />
                  <Point X="-2.203331883456" Y="29.26229050036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.785124101626" Y="21.212005822889" />
                  <Point X="-2.287203559191" Y="29.215693015239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.442429666261" Y="21.839664678577" />
                  <Point X="-2.371075234925" Y="29.169095530118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.30916991011" Y="21.921727541675" />
                  <Point X="-2.45494691066" Y="29.122498044998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.193648017104" Y="21.957581691463" />
                  <Point X="-2.538818586394" Y="29.075900559877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.078126124098" Y="21.993435841251" />
                  <Point X="-2.622690262129" Y="29.029303074756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.026515434458" Y="22.000945750743" />
                  <Point X="-2.702918732373" Y="28.973214714851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.11904951107" Y="21.976914591422" />
                  <Point X="-2.781539121584" Y="28.912937160634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.209975738876" Y="21.948694842758" />
                  <Point X="-2.860159510796" Y="28.852659606416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.300901966681" Y="21.920475094094" />
                  <Point X="-2.938779900007" Y="28.792382052198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.386707943653" Y="21.878916635975" />
                  <Point X="-2.748511338173" Y="28.031624831995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.897231392639" Y="28.419053819586" />
                  <Point X="-3.017400289218" Y="28.73210449798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.458865450082" Y="21.801802696493" />
                  <Point X="-2.793652807893" Y="27.884131710712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.524386470921" Y="21.707400120974" />
                  <Point X="-2.865567624337" Y="27.806385542206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.58990749176" Y="21.612997545455" />
                  <Point X="-2.944675271087" Y="27.747377337273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.646933161855" Y="21.496463824611" />
                  <Point X="-3.037409668251" Y="27.723868030832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.688764413962" Y="21.340347291624" />
                  <Point X="-3.14151132799" Y="27.729971455821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.73059566607" Y="21.184230758638" />
                  <Point X="-3.256162984173" Y="27.763558561177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.772426918177" Y="21.028114225651" />
                  <Point X="-3.38689519997" Y="27.839036956538" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.814258170285" Y="20.871997692665" />
                  <Point X="-3.517627376205" Y="27.914515248837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.856089422392" Y="20.715881159678" />
                  <Point X="-3.64835955244" Y="27.989993541136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.8979206745" Y="20.559764626691" />
                  <Point X="-3.753833105656" Y="27.99967087082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.939751926607" Y="20.403648093705" />
                  <Point X="-3.821971952759" Y="27.912087965873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.981583178715" Y="20.247531560718" />
                  <Point X="-1.170603124824" Y="20.739945355337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.237587737958" Y="20.914446238514" />
                  <Point X="-3.890110799861" Y="27.824505060927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086001154964" Y="20.254459018389" />
                  <Point X="-1.128531065131" Y="20.365253222287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.39401559347" Y="21.056864063907" />
                  <Point X="-3.448203221991" Y="26.408205791798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604922659698" Y="26.816473885192" />
                  <Point X="-3.958249646964" Y="27.73692215598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.541398676517" Y="21.17571945146" />
                  <Point X="-3.508941756156" Y="26.30134441254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.749169322536" Y="26.927158618757" />
                  <Point X="-4.024001200494" Y="27.643120138654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.654377191529" Y="21.20494787505" />
                  <Point X="-3.58874411447" Y="26.244145993107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.893415985375" Y="27.037843352321" />
                  <Point X="-4.085388543957" Y="27.537948965405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.758775437358" Y="21.211823933216" />
                  <Point X="-3.678802905251" Y="26.213666493737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.037662648213" Y="27.148528085886" />
                  <Point X="-4.14677588742" Y="27.432777792156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.863160370721" Y="21.218665311224" />
                  <Point X="-2.311469241601" Y="22.386549848359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.380533162179" Y="22.566467512623" />
                  <Point X="-3.296037212918" Y="24.951437103885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.381221671346" Y="25.173350205018" />
                  <Point X="-3.777449269825" Y="26.205558388945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.181909311052" Y="27.25921281945" />
                  <Point X="-4.208163230883" Y="27.327606618908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.966835464463" Y="21.223657493798" />
                  <Point X="-2.359062077669" Y="22.245442754744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.512676237481" Y="22.645621322653" />
                  <Point X="-3.343527572711" Y="24.810063050446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.512709936392" Y="25.250798176011" />
                  <Point X="-3.88461001283" Y="26.219630998298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.06026652422" Y="21.201963055459" />
                  <Point X="-2.420183035621" Y="22.139577623514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.614472208878" Y="22.645718224154" />
                  <Point X="-2.962983031284" Y="23.553619956531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.081783674351" Y="23.863106212663" />
                  <Point X="-3.41627460038" Y="24.7344848663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.626135163714" Y="25.281190324952" />
                  <Point X="-3.991785100872" Y="26.233740977748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.143002433885" Y="21.15240679857" />
                  <Point X="-2.481304050197" Y="22.033712639795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703126291643" Y="22.611579335292" />
                  <Point X="-3.024762161577" Y="23.449469422868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197502003158" Y="23.899472095209" />
                  <Point X="-3.501015668323" Y="24.690152225316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.739560391035" Y="25.311582473894" />
                  <Point X="-4.098960188913" Y="26.247850957198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.22398904851" Y="21.098293472302" />
                  <Point X="-2.542425064774" Y="21.927847656076" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.786424215215" Y="22.563487174687" />
                  <Point X="-3.101758214173" Y="23.384960327098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296739072536" Y="23.892902829044" />
                  <Point X="-3.593152664952" Y="24.665086637272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.852985618357" Y="25.341974622836" />
                  <Point X="-4.206135276954" Y="26.261960936648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.304975663135" Y="21.044180146034" />
                  <Point X="-2.60354607935" Y="21.821982672358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869722138788" Y="22.515395014082" />
                  <Point X="-3.18036389576" Y="23.324644458209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393602725879" Y="23.88015060272" />
                  <Point X="-3.685421041245" Y="24.640363304957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966410845679" Y="25.372366771777" />
                  <Point X="-4.313310364996" Y="26.276070916098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.384360597634" Y="20.985894300386" />
                  <Point X="-2.664667093926" Y="21.716117688639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.95302006236" Y="22.467302853477" />
                  <Point X="-3.258969439599" Y="23.264328230473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.490466379223" Y="23.867398376397" />
                  <Point X="-3.777689418307" Y="24.615639974643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079836073001" Y="25.402758920719" />
                  <Point X="-4.420485453037" Y="26.290180895547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.453851899596" Y="20.901834660803" />
                  <Point X="-2.725788108503" Y="21.61025270492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.036317985932" Y="22.419210692872" />
                  <Point X="-3.337574983438" Y="23.204012002737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587330032566" Y="23.854646150073" />
                  <Point X="-3.869957795369" Y="24.590916644329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.193261300322" Y="25.433151069661" />
                  <Point X="-4.527660541079" Y="26.304290874997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.539566301058" Y="20.860037640324" />
                  <Point X="-2.786909123079" Y="21.504387721201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.119615909504" Y="22.371118532267" />
                  <Point X="-3.416180527276" Y="23.143695775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684193685909" Y="23.84189392375" />
                  <Point X="-3.96222617243" Y="24.566193314015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.306686527644" Y="25.463543218603" />
                  <Point X="-4.634076936782" Y="26.316424393334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.673051358081" Y="20.942687432261" />
                  <Point X="-2.848030137655" Y="21.398522737483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.202913833076" Y="22.323026371662" />
                  <Point X="-3.494786071115" Y="23.083379547264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.781057339253" Y="23.829141697427" />
                  <Point X="-4.054494549492" Y="24.541469983701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.420111754966" Y="25.493935367544" />
                  <Point X="-4.676185638181" Y="26.161030640464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81137952721" Y="21.037953962583" />
                  <Point X="-2.909151152231" Y="21.292657753764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.286211756648" Y="22.274934211057" />
                  <Point X="-3.573391614953" Y="23.023063319528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.877920992596" Y="23.816389471103" />
                  <Point X="-4.146762926554" Y="24.516746653387" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.533536982287" Y="25.524327516486" />
                  <Point X="-4.718294339581" Y="26.005636887593" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.955832910934" Y="21.149177222465" />
                  <Point X="-2.970272166808" Y="21.186792770045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.36950968022" Y="22.226842050452" />
                  <Point X="-3.651997158792" Y="22.962747091792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97478464594" Y="23.80363724478" />
                  <Point X="-4.239031303615" Y="24.492023323074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.646962209609" Y="25.554719665428" />
                  <Point X="-4.7508783762" Y="25.825430534658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452807603793" Y="22.178749889847" />
                  <Point X="-3.73060270263" Y="22.902430864055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071648299283" Y="23.790885018457" />
                  <Point X="-4.331299680677" Y="24.46729999276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.760387436931" Y="25.585111814369" />
                  <Point X="-4.779190885703" Y="25.634096473144" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.536105527365" Y="22.130657729242" />
                  <Point X="-3.809208246469" Y="22.842114636319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.168511952626" Y="23.778132792133" />
                  <Point X="-4.423568057739" Y="24.442576662446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.619403450937" Y="22.082565568637" />
                  <Point X="-3.887813790308" Y="22.781798408583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.26537560597" Y="23.76538056581" />
                  <Point X="-4.515836434801" Y="24.417853332132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.702701374509" Y="22.034473408032" />
                  <Point X="-3.966419334146" Y="22.721482180847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.362239259313" Y="23.752628339487" />
                  <Point X="-4.608104811862" Y="24.393130001818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785999298081" Y="21.986381247428" />
                  <Point X="-4.045024877985" Y="22.66116595311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.459102912656" Y="23.739876113163" />
                  <Point X="-4.700373188924" Y="24.368406671504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.98090928681" Y="22.22904845725" />
                  <Point X="-4.123630421823" Y="22.600849725374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.555966566" Y="23.72712388684" />
                  <Point X="-4.782398541541" Y="24.316999350219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.652830219343" Y="23.714371660517" />
                  <Point X="-4.677822411638" Y="23.779478547367" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.495395721436" Y="21.373853515625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.454567871094" Y="21.492505859375" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.289073425293" Y="21.72617578125" />
                  <Point X="0.259247192383" Y="21.73828125" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="0.006156158924" Y="21.81448046875" />
                  <Point X="-0.023753477097" Y="21.80755078125" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.276761291504" Y="21.72617578125" />
                  <Point X="-0.298029815674" Y="21.700306640625" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.835951660156" Y="20.056931640625" />
                  <Point X="-0.847743896484" Y="20.012923828125" />
                  <Point X="-1.100231933594" Y="20.061931640625" />
                  <Point X="-1.116397216797" Y="20.06608984375" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.312022216797" Y="20.427146484375" />
                  <Point X="-1.309150390625" Y="20.448958984375" />
                  <Point X="-1.313287963867" Y="20.48336328125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.375948730469" Y="20.784779296875" />
                  <Point X="-1.400174438477" Y="20.809552734375" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.633376708984" Y="21.0105390625" />
                  <Point X="-1.667678466797" Y="21.0154453125" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.975041015625" Y="21.032931640625" />
                  <Point X="-2.005241699219" Y="21.015943359375" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734375" Y="20.842705078125" />
                  <Point X="-2.457094970703" Y="20.5855" />
                  <Point X="-2.855838134766" Y="20.832390625" />
                  <Point X="-2.878200439453" Y="20.849609375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.550368652344" Y="22.29408984375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.515013916016" Y="22.43226953125" />
                  <Point X="-2.531329345703" Y="22.448583984375" />
                  <Point X="-2.560158203125" Y="22.46280078125" />
                  <Point X="-2.591684082031" Y="22.456529296875" />
                  <Point X="-3.80429296875" Y="21.7564296875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.177737792969" Y="22.179755859375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.241359130859" Y="23.51733203125" />
                  <Point X="-3.163787353516" Y="23.576853515625" />
                  <Point X="-3.14536328125" Y="23.605755859375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140328613281" Y="23.665408203125" />
                  <Point X="-3.162747314453" Y="23.69029296875" />
                  <Point X="-3.187641845703" Y="23.7049453125" />
                  <Point X="-3.219528808594" Y="23.711427734375" />
                  <Point X="-4.750332519531" Y="23.50989453125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.931635253906" Y="24.018466796875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.645829833984" Y="24.84767578125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.540242675781" Y="24.879720703125" />
                  <Point X="-3.514142578125" Y="24.8978359375" />
                  <Point X="-3.494347900391" Y="24.9258671875" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.486198486328" Y="24.985314453125" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.515795410156" Y="25.04075" />
                  <Point X="-3.541895507813" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.952864257812" Y="25.439982421875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.909104492188" Y="26.027935546875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.816608154297" Y="26.402318359375" />
                  <Point X="-3.753265136719" Y="26.39398046875" />
                  <Point X="-3.731717529297" Y="26.39586328125" />
                  <Point X="-3.728045898438" Y="26.39701953125" />
                  <Point X="-3.670278076172" Y="26.415234375" />
                  <Point X="-3.651536376953" Y="26.4260546875" />
                  <Point X="-3.639122070312" Y="26.44378125" />
                  <Point X="-3.637651855469" Y="26.447328125" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.618087158203" Y="26.5489140625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968505859" Y="26.619220703125" />
                  <Point X="-4.460373535156" Y="27.23339453125" />
                  <Point X="-4.47610546875" Y="27.245466796875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.137392578125" Y="27.81608203125" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.198060302734" Y="27.949404296875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514892578" Y="27.92043359375" />
                  <Point X="-3.133419921875" Y="27.91998828125" />
                  <Point X="-3.052965576172" Y="27.91294921875" />
                  <Point X="-3.031510742188" Y="27.9157734375" />
                  <Point X="-3.013258300781" Y="27.9273984375" />
                  <Point X="-3.009635742188" Y="27.93101953125" />
                  <Point X="-2.952528564453" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.938520019531" Y="28.032935546875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067626953" Y="28.134033203125" />
                  <Point X="-3.306751464844" Y="28.74836328125" />
                  <Point X="-3.306826416016" Y="28.749623046875" />
                  <Point X="-2.752873779297" Y="29.17433203125" />
                  <Point X="-2.717247802734" Y="29.194125" />
                  <Point X="-2.141548828125" Y="29.51397265625" />
                  <Point X="-1.979735229492" Y="29.303091796875" />
                  <Point X="-1.967825683594" Y="29.287572265625" />
                  <Point X="-1.951254150391" Y="29.273666015625" />
                  <Point X="-1.945576293945" Y="29.270708984375" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835121459961" Y="29.2184921875" />
                  <Point X="-1.813803833008" Y="29.222251953125" />
                  <Point X="-1.807897460938" Y="29.22469921875" />
                  <Point X="-1.714634887695" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.684161010742" Y="29.3005859375" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.685216674805" Y="29.70223828125" />
                  <Point X="-0.968082763672" Y="29.903296875" />
                  <Point X="-0.924904602051" Y="29.9083515625" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.053649044037" Y="29.353853515625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.02428212738" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594047546" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.236188186646" Y="29.989150390625" />
                  <Point X="0.236648147583" Y="29.9908671875" />
                  <Point X="0.860210083008" Y="29.925564453125" />
                  <Point X="0.895939819336" Y="29.9169375" />
                  <Point X="1.508456665039" Y="29.769056640625" />
                  <Point X="1.531292358398" Y="29.760775390625" />
                  <Point X="1.931044799805" Y="29.61578125" />
                  <Point X="1.953526489258" Y="29.605267578125" />
                  <Point X="2.338684326172" Y="29.425142578125" />
                  <Point X="2.360436035156" Y="29.41246875" />
                  <Point X="2.732519287109" Y="29.195693359375" />
                  <Point X="2.753017578125" Y="29.181115234375" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.281167724609" Y="27.5924765625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.223573486328" Y="27.486732421875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202543212891" Y="27.388189453125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218678222656" Y="27.300818359375" />
                  <Point X="2.221240722656" Y="27.297041015625" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.278709716797" Y="27.221646484375" />
                  <Point X="2.338248046875" Y="27.18124609375" />
                  <Point X="2.360343017578" Y="27.172978515625" />
                  <Point X="2.364471435547" Y="27.17248046875" />
                  <Point X="2.42976171875" Y="27.164607421875" />
                  <Point X="2.453446044922" Y="27.167224609375" />
                  <Point X="2.528950927734" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.944538085938" Y="28.00273046875" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.202590820313" Y="27.741880859375" />
                  <Point X="4.214017089844" Y="27.722998046875" />
                  <Point X="4.387512207031" Y="27.436294921875" />
                  <Point X="3.35617578125" Y="26.644923828125" />
                  <Point X="3.288615722656" Y="26.59308203125" />
                  <Point X="3.2759296875" Y="26.57934375" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.211837646484" Y="26.486916015625" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.191831787109" Y="26.385865234375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.218508544922" Y="26.28360546875" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280953369141" Y="26.19881640625" />
                  <Point X="3.285101074219" Y="26.196482421875" />
                  <Point X="3.350589355469" Y="26.1596171875" />
                  <Point X="3.374180908203" Y="26.15287890625" />
                  <Point X="3.462726318359" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.809038574219" Y="26.3166953125" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.9427890625" Y="25.928259765625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.818593017578" Y="25.258572265625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.723577636719" Y="25.229634765625" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.618959228516" Y="25.16271484375" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.555883056641" Y="25.06898046875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.539584960938" Y="24.9535625" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.570064697266" Y="24.83702734375" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.642092529297" Y="24.754904296875" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.9638046875" Y="24.3720078125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.943818847656" Y="24.01338671875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.5017734375" Y="23.89055078125" />
                  <Point X="3.411981689453" Y="23.90237109375" />
                  <Point X="3.384023193359" Y="23.89930859375" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1979453125" Y="23.856078125" />
                  <Point X="3.178909423828" Y="23.83744140625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.067953613281" Y="23.700482421875" />
                  <Point X="3.063421142578" Y="23.67575" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.049849121094" Y="23.498541015625" />
                  <Point X="3.062344726562" Y="23.4740703125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.1684609375" Y="23.314458984375" />
                  <Point X="4.303075195312" Y="22.443837890625" />
                  <Point X="4.339073730469" Y="22.41621484375" />
                  <Point X="4.204130859375" Y="22.19785546875" />
                  <Point X="4.194590820313" Y="22.184302734375" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="2.833426269531" Y="22.694611328125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.724471435547" Y="22.749009765625" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.504749267578" Y="22.785923828125" />
                  <Point X="2.478386474609" Y="22.775126953125" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.297490966797" Y="22.677384765625" />
                  <Point X="2.282973144531" Y="22.654625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.188950439453" Y="22.470126953125" />
                  <Point X="2.191487060547" Y="22.440755859375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091796875" Y="22.221419921875" />
                  <Point X="2.963195800781" Y="20.958576171875" />
                  <Point X="2.986673828125" Y="20.917912109375" />
                  <Point X="2.835294921875" Y="20.80978515625" />
                  <Point X="2.824633544922" Y="20.802884765625" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.74420324707" Y="21.928380859375" />
                  <Point X="1.683177734375" Y="22.007912109375" />
                  <Point X="1.657856445312" Y="22.027693359375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.442262207031" Y="22.1630625" />
                  <Point X="1.411923706055" Y="22.16300390625" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.178165649414" Y="22.13923828125" />
                  <Point X="1.154613891602" Y="22.122578125" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.974574951172" Y="21.969337890625" />
                  <Point X="0.965251953125" Y="21.939267578125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.689279296875" />
                  <Point X="1.120552001953" Y="20.11982421875" />
                  <Point X="1.127642089844" Y="20.065970703125" />
                  <Point X="0.994367736816" Y="20.0367578125" />
                  <Point X="0.984488891602" Y="20.034962890625" />
                  <Point X="0.860200439453" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#122" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.011508702859" Y="4.398075539562" Z="0.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="-0.936872690232" Y="4.989292220936" Z="0.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.15" />
                  <Point X="-1.704870465505" Y="4.78166399326" Z="0.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.15" />
                  <Point X="-1.747969421667" Y="4.749468478074" Z="0.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.15" />
                  <Point X="-1.738002751349" Y="4.346901142356" Z="0.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.15" />
                  <Point X="-1.833192735946" Y="4.30217107463" Z="0.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.15" />
                  <Point X="-1.928644595176" Y="4.346339371174" Z="0.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.15" />
                  <Point X="-1.946224711962" Y="4.36481210394" Z="0.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.15" />
                  <Point X="-2.747686667459" Y="4.269113413819" Z="0.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.15" />
                  <Point X="-3.343404395585" Y="3.820583624271" Z="0.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.15" />
                  <Point X="-3.356208371882" Y="3.754643037716" Z="0.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.15" />
                  <Point X="-2.994485763037" Y="3.059858457517" Z="0.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.15" />
                  <Point X="-3.051147157687" Y="2.997656410199" Z="0.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.15" />
                  <Point X="-3.135218008089" Y="3.001078936136" Z="0.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.15" />
                  <Point X="-3.179216310581" Y="3.023985576864" Z="0.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.15" />
                  <Point X="-4.183011954007" Y="2.878066195908" Z="0.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.15" />
                  <Point X="-4.527406680284" Y="2.298588571582" Z="0.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.15" />
                  <Point X="-4.496967274814" Y="2.225006398789" Z="0.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.15" />
                  <Point X="-3.668593594178" Y="1.557106749496" Z="0.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.15" />
                  <Point X="-3.690002024496" Y="1.497743894349" Z="0.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.15" />
                  <Point X="-3.74923782805" Y="1.475985973062" Z="0.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.15" />
                  <Point X="-3.816238884618" Y="1.483171776995" Z="0.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.15" />
                  <Point X="-4.963520230196" Y="1.072293295875" Z="0.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.15" />
                  <Point X="-5.055116201663" Y="0.481857461341" Z="0.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.15" />
                  <Point X="-4.971961173475" Y="0.422965458243" Z="0.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.15" />
                  <Point X="-3.55046068527" Y="0.030954106588" Z="0.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.15" />
                  <Point X="-3.54010769313" Y="0.001775156517" Z="0.15" />
                  <Point X="-3.539556741714" Y="0" Z="0.15" />
                  <Point X="-3.548256908005" Y="-0.028031794544" Z="0.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.15" />
                  <Point X="-3.574907909853" Y="-0.047921876546" Z="0.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.15" />
                  <Point X="-3.664926615297" Y="-0.072746598513" Z="0.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.15" />
                  <Point X="-4.987287092" Y="-0.957330728527" Z="0.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.15" />
                  <Point X="-4.854988431792" Y="-1.489506992252" Z="0.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.15" />
                  <Point X="-4.749962810529" Y="-1.508397429029" Z="0.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.15" />
                  <Point X="-3.194253479099" Y="-1.321521547764" Z="0.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.15" />
                  <Point X="-3.199921959956" Y="-1.350425562282" Z="0.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.15" />
                  <Point X="-3.277952523737" Y="-1.411720043219" Z="0.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.15" />
                  <Point X="-4.226837027934" Y="-2.814572192927" Z="0.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.15" />
                  <Point X="-3.882911418271" Y="-3.272766588511" Z="0.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.15" />
                  <Point X="-3.785448550137" Y="-3.255591123261" Z="0.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.15" />
                  <Point X="-2.556524164256" Y="-2.571806304876" Z="0.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.15" />
                  <Point X="-2.599825889907" Y="-2.649629832355" Z="0.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.15" />
                  <Point X="-2.914860386648" Y="-4.158726613664" Z="0.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.15" />
                  <Point X="-2.477283757517" Y="-4.433340229116" Z="0.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.15" />
                  <Point X="-2.43772407707" Y="-4.43208659527" Z="0.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.15" />
                  <Point X="-1.983619209206" Y="-3.99434975326" Z="0.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.15" />
                  <Point X="-1.67710446766" Y="-4.003167467923" Z="0.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.15" />
                  <Point X="-1.439297858026" Y="-4.19675763859" Z="0.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.15" />
                  <Point X="-1.368483254394" Y="-4.495110101238" Z="0.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.15" />
                  <Point X="-1.367750313795" Y="-4.535045545261" Z="0.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.15" />
                  <Point X="-1.135012120963" Y="-4.951052599742" Z="0.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.15" />
                  <Point X="-0.835518759953" Y="-5.010298071087" Z="0.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="-0.793811413325" Y="-4.924728651172" Z="0.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="-0.26310990206" Y="-3.296921753366" Z="0.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="-0.015088830121" Y="-3.208922384478" Z="0.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.15" />
                  <Point X="0.238270249241" Y="-3.27818966081" Z="0.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.15" />
                  <Point X="0.407335957372" Y="-3.504724002402" Z="0.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.15" />
                  <Point X="0.440943447076" Y="-3.607807378644" Z="0.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.15" />
                  <Point X="0.987269864611" Y="-4.982952496531" Z="0.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.15" />
                  <Point X="1.166390058479" Y="-4.944092599902" Z="0.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.15" />
                  <Point X="1.163968286797" Y="-4.8423672347" Z="0.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.15" />
                  <Point X="1.007955103049" Y="-3.040071127358" Z="0.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.15" />
                  <Point X="1.180424798384" Y="-2.884587897561" Z="0.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.15" />
                  <Point X="1.410348949887" Y="-2.855503978055" Z="0.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.15" />
                  <Point X="1.62466130522" Y="-2.983085105949" Z="0.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.15" />
                  <Point X="1.698379597175" Y="-3.070775494288" Z="0.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.15" />
                  <Point X="2.845646816922" Y="-4.207809783023" Z="0.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.15" />
                  <Point X="3.0352424511" Y="-4.073164982596" Z="0.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.15" />
                  <Point X="3.000340966759" Y="-3.985143387291" Z="0.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.15" />
                  <Point X="2.234535298159" Y="-2.519077473294" Z="0.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.15" />
                  <Point X="2.3210634839" Y="-2.337381411913" Z="0.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.15" />
                  <Point X="2.495516966459" Y="-2.237837826067" Z="0.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.15" />
                  <Point X="2.709429330218" Y="-2.268912711332" Z="0.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.15" />
                  <Point X="2.802270209816" Y="-2.317408533515" Z="0.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.15" />
                  <Point X="4.229323494738" Y="-2.813194915548" Z="0.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.15" />
                  <Point X="4.38971032048" Y="-2.555716420361" Z="0.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.15" />
                  <Point X="4.327357336097" Y="-2.485213481267" Z="0.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.15" />
                  <Point X="3.098246623784" Y="-1.467610399045" Z="0.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.15" />
                  <Point X="3.107054717467" Y="-1.297552003838" Z="0.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.15" />
                  <Point X="3.211199707681" Y="-1.163244746868" Z="0.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.15" />
                  <Point X="3.388486774659" Y="-1.118270693293" Z="0.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.15" />
                  <Point X="3.489091522686" Y="-1.127741720224" Z="0.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.15" />
                  <Point X="4.986410051966" Y="-0.966457639766" Z="0.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.15" />
                  <Point X="5.044645683828" Y="-0.591510038319" Z="0.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.15" />
                  <Point X="4.970589745638" Y="-0.548415230941" Z="0.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.15" />
                  <Point X="3.660952157854" Y="-0.170522764267" Z="0.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.15" />
                  <Point X="3.603242389607" Y="-0.100822726381" Z="0.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.15" />
                  <Point X="3.582536615348" Y="-0.005753651719" Z="0.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.15" />
                  <Point X="3.598834835076" Y="0.090856879526" Z="0.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.15" />
                  <Point X="3.652137048793" Y="0.163126012194" Z="0.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.15" />
                  <Point X="3.742443256498" Y="0.217626079975" Z="0.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.15" />
                  <Point X="3.825378071668" Y="0.241556701828" Z="0.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.15" />
                  <Point X="4.986037699857" Y="0.967232133109" Z="0.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.15" />
                  <Point X="4.886820031753" Y="1.383874968824" Z="0.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.15" />
                  <Point X="4.796356313982" Y="1.397547841595" Z="0.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.15" />
                  <Point X="3.374568376336" Y="1.233727445843" Z="0.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.15" />
                  <Point X="3.303466093602" Y="1.271336324322" Z="0.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.15" />
                  <Point X="3.25412306934" Y="1.342366178082" Z="0.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.15" />
                  <Point X="3.234644310549" Y="1.42724933336" Z="0.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.15" />
                  <Point X="3.253834124319" Y="1.504730072772" Z="0.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.15" />
                  <Point X="3.309456934829" Y="1.580205752644" Z="0.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.15" />
                  <Point X="3.380458303065" Y="1.636535820832" Z="0.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.15" />
                  <Point X="4.250638612368" Y="2.780165975547" Z="0.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.15" />
                  <Point X="4.016311573031" Y="3.109099700803" Z="0.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.15" />
                  <Point X="3.913382100258" Y="3.077312237825" Z="0.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.15" />
                  <Point X="2.434373113763" Y="2.246807798973" Z="0.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.15" />
                  <Point X="2.364301394354" Y="2.253401971021" Z="0.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.15" />
                  <Point X="2.300628430385" Y="2.294299730617" Z="0.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.15" />
                  <Point X="2.256458869618" Y="2.356396429997" Z="0.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.15" />
                  <Point X="2.246027731738" Y="2.425457049478" Z="0.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.15" />
                  <Point X="2.265720143684" Y="2.505096462119" Z="0.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.15" />
                  <Point X="2.318313115035" Y="2.598756981697" Z="0.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.15" />
                  <Point X="2.775838676083" Y="4.253144432393" Z="0.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.15" />
                  <Point X="2.379449882441" Y="4.486953207086" Z="0.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.15" />
                  <Point X="1.968551955935" Y="4.681838926665" Z="0.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.15" />
                  <Point X="1.542185104964" Y="4.839059007699" Z="0.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.15" />
                  <Point X="0.901518366163" Y="4.996821896732" Z="0.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.15" />
                  <Point X="0.233105642505" Y="5.072148512605" Z="0.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.15" />
                  <Point X="0.181735858694" Y="5.033371966389" Z="0.15" />
                  <Point X="0" Y="4.355124473572" Z="0.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>