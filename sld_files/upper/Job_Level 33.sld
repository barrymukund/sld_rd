<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#181" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2414" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004717773438" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.815836975098" Y="20.545001953125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557720153809" Y="21.502861328125" />
                  <Point X="0.542363037109" Y="21.532623046875" />
                  <Point X="0.439145568848" Y="21.681337890625" />
                  <Point X="0.378635375977" Y="21.7685234375" />
                  <Point X="0.356752807617" Y="21.7909765625" />
                  <Point X="0.330497924805" Y="21.810220703125" />
                  <Point X="0.302494873047" Y="21.824330078125" />
                  <Point X="0.14277204895" Y="21.873900390625" />
                  <Point X="0.049135883331" Y="21.902962890625" />
                  <Point X="0.020983621597" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.196546905518" Y="21.853392578125" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318184295654" Y="21.810224609375" />
                  <Point X="-0.344439788818" Y="21.79098046875" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.469540893555" Y="21.619806640625" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.699036315918" Y="20.934958984375" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-0.97284161377" Y="20.133978515625" />
                  <Point X="-1.07935546875" Y="20.154654296875" />
                  <Point X="-1.246418212891" Y="20.197634765625" />
                  <Point X="-1.245362304688" Y="20.20565625" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.254666625977" Y="20.67560546875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937744141" Y="20.817033203125" />
                  <Point X="-1.304009643555" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868796875" />
                  <Point X="-1.470697265625" Y="20.9977578125" />
                  <Point X="-1.556905639648" Y="21.073361328125" />
                  <Point X="-1.583189697266" Y="21.089705078125" />
                  <Point X="-1.612887451172" Y="21.102005859375" />
                  <Point X="-1.643027587891" Y="21.109033203125" />
                  <Point X="-1.83819909668" Y="21.121826171875" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983415771484" Y="21.126291015625" />
                  <Point X="-2.014463745117" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.205284912109" Y="20.99653515625" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.418219482422" Y="20.792216796875" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.645609863281" Y="20.8139609375" />
                  <Point X="-2.801726318359" Y="20.910623046875" />
                  <Point X="-3.053984619141" Y="21.10485546875" />
                  <Point X="-3.104721923828" Y="21.143919921875" />
                  <Point X="-2.893069091797" Y="21.510515625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414561767578" Y="22.4444296875" />
                  <Point X="-2.428780761719" Y="22.473259765625" />
                  <Point X="-2.446809082031" Y="22.498416015625" />
                  <Point X="-2.464154052734" Y="22.515759765625" />
                  <Point X="-2.489305664062" Y="22.533783203125" />
                  <Point X="-2.518135253906" Y="22.548001953125" />
                  <Point X="-2.547754882812" Y="22.55698828125" />
                  <Point X="-2.578691162109" Y="22.555974609375" />
                  <Point X="-2.610218261719" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.115718017578" Y="22.263673828125" />
                  <Point X="-3.818022705078" Y="21.858197265625" />
                  <Point X="-3.959532714844" Y="22.04411328125" />
                  <Point X="-4.082861328125" Y="22.206140625" />
                  <Point X="-4.263720214844" Y="22.5094140625" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.927090820312" Y="22.87140625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.58016796875" />
                  <Point X="-3.046151611328" Y="23.609916015625" />
                  <Point X="-3.043347412109" Y="23.640345703125" />
                  <Point X="-3.045556884766" Y="23.672017578125" />
                  <Point X="-3.052559814453" Y="23.701765625" />
                  <Point X="-3.068645019531" Y="23.72775" />
                  <Point X="-3.089479492188" Y="23.751705078125" />
                  <Point X="-3.112976074219" Y="23.771234375" />
                  <Point X="-3.139458740234" Y="23.7868203125" />
                  <Point X="-3.168722412109" Y="23.79804296875" />
                  <Point X="-3.200607666016" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.833508789062" Y="23.726416015625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.785842773438" Y="23.8185078125" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.881928710938" Y="24.3419140625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.468273925781" Y="24.528951171875" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517493896484" Y="24.785171875" />
                  <Point X="-3.487729736328" Y="24.800529296875" />
                  <Point X="-3.470233642578" Y="24.812671875" />
                  <Point X="-3.459976806641" Y="24.819791015625" />
                  <Point X="-3.437517578125" Y="24.841677734375" />
                  <Point X="-3.418274658203" Y="24.86793359375" />
                  <Point X="-3.404168457031" Y="24.895931640625" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390647949219" Y="24.953896484375" />
                  <Point X="-3.390647216797" Y="24.98353515625" />
                  <Point X="-3.394916259766" Y="25.0116953125" />
                  <Point X="-3.404167236328" Y="25.04150390625" />
                  <Point X="-3.418272949219" Y="25.069501953125" />
                  <Point X="-3.437516601563" Y="25.095759765625" />
                  <Point X="-3.459976806641" Y="25.1176484375" />
                  <Point X="-3.477472900391" Y="25.129791015625" />
                  <Point X="-3.485521240234" Y="25.134802734375" />
                  <Point X="-3.511347167969" Y="25.149142578125" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-4.081244873047" Y="25.304783203125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.855573730469" Y="25.766900390625" />
                  <Point X="-4.824488769531" Y="25.976970703125" />
                  <Point X="-4.728165527344" Y="26.33243359375" />
                  <Point X="-4.703551269531" Y="26.423267578125" />
                  <Point X="-4.436182617188" Y="26.38806640625" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723422851562" Y="26.301228515625" />
                  <Point X="-3.703138427734" Y="26.305263671875" />
                  <Point X="-3.6644140625" Y="26.31747265625" />
                  <Point X="-3.641712402344" Y="26.324630859375" />
                  <Point X="-3.622776367188" Y="26.332962890625" />
                  <Point X="-3.604032470703" Y="26.34378515625" />
                  <Point X="-3.587351318359" Y="26.356015625" />
                  <Point X="-3.573713134766" Y="26.371568359375" />
                  <Point X="-3.561298828125" Y="26.389298828125" />
                  <Point X="-3.5513515625" Y="26.407431640625" />
                  <Point X="-3.535813232422" Y="26.444943359375" />
                  <Point X="-3.526704101563" Y="26.466935546875" />
                  <Point X="-3.520915283203" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.550798583984" Y="26.625392578125" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-3.916681396484" Y="26.93594921875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.201939941406" Y="27.526724609375" />
                  <Point X="-4.08115625" Y="27.733654296875" />
                  <Point X="-3.825999755859" Y="28.061623046875" />
                  <Point X="-3.750504150391" Y="28.158662109375" />
                  <Point X="-3.618469238281" Y="28.082431640625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728271484" Y="27.836341796875" />
                  <Point X="-3.167085205078" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.092862060547" Y="27.821076171875" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040559570312" Y="27.81876171875" />
                  <Point X="-3.0191015625" Y="27.821587890625" />
                  <Point X="-2.999012207031" Y="27.826505859375" />
                  <Point X="-2.980462890625" Y="27.83565234375" />
                  <Point X="-2.962209228516" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.907796386719" Y="27.898509765625" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848154296875" Y="28.090052734375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.009179931641" Y="28.422953125" />
                  <Point X="-3.183333007812" Y="28.72459375" />
                  <Point X="-2.910992431641" Y="28.933396484375" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.298768066406" Y="29.3179453125" />
                  <Point X="-2.167036376953" Y="29.3911328125" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.02889453125" Y="29.214802734375" />
                  <Point X="-2.012316162109" Y="29.200890625" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.935088745117" Y="29.1581484375" />
                  <Point X="-1.899898803711" Y="29.139828125" />
                  <Point X="-1.880626342773" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.839269775391" Y="29.123580078125" />
                  <Point X="-1.818623657227" Y="29.12493359375" />
                  <Point X="-1.797307617188" Y="29.128693359375" />
                  <Point X="-1.777452880859" Y="29.134482421875" />
                  <Point X="-1.714931518555" Y="29.160380859375" />
                  <Point X="-1.678278930664" Y="29.1755625" />
                  <Point X="-1.660147094727" Y="29.185509765625" />
                  <Point X="-1.642417236328" Y="29.197923828125" />
                  <Point X="-1.626864868164" Y="29.2115625" />
                  <Point X="-1.614633300781" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.575130737305" Y="29.330462890625" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.573577026367" Y="29.55119140625" />
                  <Point X="-1.584202026367" Y="29.631896484375" />
                  <Point X="-1.221968139648" Y="29.733455078125" />
                  <Point X="-0.949623657227" Y="29.80980859375" />
                  <Point X="-0.462466247559" Y="29.866826171875" />
                  <Point X="-0.294711608887" Y="29.886458984375" />
                  <Point X="-0.255728775024" Y="29.74097265625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113960266" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155894279" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425964355" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.21763432312" Y="29.552853515625" />
                  <Point X="0.307419647217" Y="29.887939453125" />
                  <Point X="0.606247375488" Y="29.856642578125" />
                  <Point X="0.844030944824" Y="29.831740234375" />
                  <Point X="1.247093017578" Y="29.734427734375" />
                  <Point X="1.481039916992" Y="29.6779453125" />
                  <Point X="1.742827880859" Y="29.582994140625" />
                  <Point X="1.894645751953" Y="29.527927734375" />
                  <Point X="2.148325195313" Y="29.409291015625" />
                  <Point X="2.294557373047" Y="29.34090234375" />
                  <Point X="2.539666748047" Y="29.1981015625" />
                  <Point X="2.680990478516" Y="29.115767578125" />
                  <Point X="2.912109130859" Y="28.951408203125" />
                  <Point X="2.943260742188" Y="28.92925390625" />
                  <Point X="2.69076953125" Y="28.491927734375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142077148438" Y="27.53993359375" />
                  <Point X="2.133076660156" Y="27.516056640625" />
                  <Point X="2.119541748047" Y="27.46544140625" />
                  <Point X="2.111606933594" Y="27.43576953125" />
                  <Point X="2.108619140625" Y="27.41793359375" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.113005615234" Y="27.337185546875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121443115234" Y="27.2896015625" />
                  <Point X="2.129709716797" Y="27.267513671875" />
                  <Point X="2.140070800781" Y="27.24747265625" />
                  <Point X="2.16715234375" Y="27.207560546875" />
                  <Point X="2.183028564453" Y="27.1841640625" />
                  <Point X="2.194464599609" Y="27.170330078125" />
                  <Point X="2.221596923828" Y="27.14559375" />
                  <Point X="2.261508056641" Y="27.11851171875" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304947509766" Y="27.0922734375" />
                  <Point X="2.327037109375" Y="27.084005859375" />
                  <Point X="2.348964599609" Y="27.078662109375" />
                  <Point X="2.392731445312" Y="27.073384765625" />
                  <Point X="2.418389404297" Y="27.070291015625" />
                  <Point X="2.43646875" Y="27.06984375" />
                  <Point X="2.473207763672" Y="27.074169921875" />
                  <Point X="2.523822021484" Y="27.087705078125" />
                  <Point X="2.553494140625" Y="27.095640625" />
                  <Point X="2.565290771484" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.140086914062" Y="27.4285859375" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="4.039450195312" Y="27.805955078125" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.252124511719" Y="27.47653125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.945942382812" Y="27.217212890625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221421386719" Y="26.66023828125" />
                  <Point X="3.203974121094" Y="26.641626953125" />
                  <Point X="3.167546875" Y="26.59410546875" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.136606689453" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.108060791016" Y="26.46856640625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.108878417969" Y="26.317783203125" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120678955078" Y="26.26898046875" />
                  <Point X="3.136282714844" Y="26.23573828125" />
                  <Point X="3.166579101562" Y="26.189689453125" />
                  <Point X="3.184340332031" Y="26.162693359375" />
                  <Point X="3.198897705078" Y="26.1454453125" />
                  <Point X="3.216141601562" Y="26.129357421875" />
                  <Point X="3.234345947266" Y="26.11603515625" />
                  <Point X="3.278249755859" Y="26.0913203125" />
                  <Point X="3.303988037109" Y="26.07683203125" />
                  <Point X="3.32051953125" Y="26.069501953125" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.415480957031" Y="26.05159375" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462697265625" Y="26.04617578125" />
                  <Point X="3.488203369141" Y="26.046984375" />
                  <Point X="4.012141845703" Y="26.1159609375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.809935058594" Y="26.080689453125" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.886541015625" Y="25.672009765625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.536166992188" Y="25.54919921875" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704787353516" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.623227294922" Y="25.281359375" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574312744141" Y="25.25109765625" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.512538085938" Y="25.180986328125" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480301269531" Y="25.135568359375" />
                  <Point X="3.470527099609" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.452016845703" Y="25.031697265625" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443483154297" Y="24.978123046875" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.456843017578" Y="24.880541015625" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480299316406" Y="24.801873046875" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.527016845703" Y="24.73800390625" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559998291016" Y="24.698763671875" />
                  <Point X="3.589035888672" Y="24.67584375" />
                  <Point X="3.647356201172" Y="24.642134765625" />
                  <Point X="3.681545898438" Y="24.62237109375" />
                  <Point X="3.692698486328" Y="24.616865234375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.197055175781" Y="24.47910546875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.875124023438" Y="24.1846015625" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.803000488281" Y="23.823306640625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.374872558594" Y="23.871423828125" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.260197021484" Y="23.969611328125" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163974609375" Y="23.94340234375" />
                  <Point X="3.136147705078" Y="23.926509765625" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.043212158203" Y="23.822830078125" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.959841552734" Y="23.586876953125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.039795654297" Y="23.333474609375" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.556512695312" Y="22.896951171875" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.181473632812" Y="22.341904296875" />
                  <Point X="4.124810546875" Y="22.250212890625" />
                  <Point X="4.028980957031" Y="22.114052734375" />
                  <Point X="3.647494628906" Y="22.3343046875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786137451172" Y="22.829984375" />
                  <Point X="2.754224365234" Y="22.84017578125" />
                  <Point X="2.617996337891" Y="22.864779296875" />
                  <Point X="2.538134033203" Y="22.879201171875" />
                  <Point X="2.506777587891" Y="22.879603515625" />
                  <Point X="2.474605224609" Y="22.87464453125" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.331661621094" Y="22.80526171875" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242385009766" Y="22.753451171875" />
                  <Point X="2.221425537109" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.144970214844" Y="22.596388671875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.120278076172" Y="22.300513671875" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.438344482422" Y="21.67764453125" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.849088623047" Y="20.9363828125" />
                  <Point X="2.781858642578" Y="20.88836328125" />
                  <Point X="2.701763916016" Y="20.83651953125" />
                  <Point X="2.404534179688" Y="21.223875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.587566162109" Y="22.185822265625" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986694336" Y="22.24883203125" />
                  <Point X="1.448366699219" Y="22.2565625" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.270157714844" Y="22.245359375" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156363769531" Y="22.2306015625" />
                  <Point X="1.128977539062" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="0.991129577637" Y="22.110193359375" />
                  <Point X="0.924611328125" Y="22.054884765625" />
                  <Point X="0.90414074707" Y="22.031134765625" />
                  <Point X="0.887248779297" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.841698730469" Y="21.81810546875" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.900941162109" Y="21.060111328125" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.975713806152" Y="20.12992578125" />
                  <Point X="0.929315246582" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058457763672" Y="20.24737109375" />
                  <Point X="-1.141246459961" Y="20.268669921875" />
                  <Point X="-1.120775512695" Y="20.424162109375" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.1614921875" Y="20.694138671875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.82152734375" />
                  <Point X="-1.199026000977" Y="20.850494140625" />
                  <Point X="-1.205664916992" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573242188" Y="20.905138671875" />
                  <Point X="-1.250208374023" Y="20.929064453125" />
                  <Point X="-1.261007080078" Y="20.94022265625" />
                  <Point X="-1.408059570312" Y="21.06918359375" />
                  <Point X="-1.494268066406" Y="21.144787109375" />
                  <Point X="-1.506740844727" Y="21.154037109375" />
                  <Point X="-1.533024902344" Y="21.170380859375" />
                  <Point X="-1.54683581543" Y="21.177474609375" />
                  <Point X="-1.576533569336" Y="21.189775390625" />
                  <Point X="-1.59131628418" Y="21.1945234375" />
                  <Point X="-1.621456420898" Y="21.20155078125" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.831985473633" Y="21.216623046875" />
                  <Point X="-1.946403198242" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.99272668457" Y="21.220833984375" />
                  <Point X="-2.008001098633" Y="21.2180546875" />
                  <Point X="-2.039049072266" Y="21.209736328125" />
                  <Point X="-2.053668212891" Y="21.204505859375" />
                  <Point X="-2.081862060547" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.258063964844" Y="21.075525390625" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359685302734" Y="21.0072421875" />
                  <Point X="-2.380448242188" Y="20.98986328125" />
                  <Point X="-2.402761962891" Y="20.967224609375" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.493588378906" Y="20.850048828125" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.595599121094" Y="20.894732421875" />
                  <Point X="-2.747611572266" Y="20.988853515625" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.810796630859" Y="21.463015625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314667480469" Y="22.442390625" />
                  <Point X="-2.323653320313" Y="22.472009765625" />
                  <Point X="-2.329360595703" Y="22.486451171875" />
                  <Point X="-2.343579589844" Y="22.51528125" />
                  <Point X="-2.351562744141" Y="22.52859765625" />
                  <Point X="-2.369591064453" Y="22.55375390625" />
                  <Point X="-2.379636230469" Y="22.56559375" />
                  <Point X="-2.396981201172" Y="22.5829375" />
                  <Point X="-2.408818603516" Y="22.59298046875" />
                  <Point X="-2.433970214844" Y="22.61100390625" />
                  <Point X="-2.447284423828" Y="22.618984375" />
                  <Point X="-2.476114013672" Y="22.633203125" />
                  <Point X="-2.490554443359" Y="22.63891015625" />
                  <Point X="-2.520174072266" Y="22.647896484375" />
                  <Point X="-2.550865966797" Y="22.6519375" />
                  <Point X="-2.581802246094" Y="22.650923828125" />
                  <Point X="-2.597225830078" Y="22.6491484375" />
                  <Point X="-2.628752929688" Y="22.642876953125" />
                  <Point X="-2.64368359375" Y="22.63861328125" />
                  <Point X="-2.672648925781" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.163218017578" Y="22.3459453125" />
                  <Point X="-3.793087646484" Y="21.9822890625" />
                  <Point X="-3.883939208984" Y="22.101650390625" />
                  <Point X="-4.004014892578" Y="22.259404296875" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.869258300781" Y="22.796037109375" />
                  <Point X="-3.048122070312" Y="23.426115234375" />
                  <Point X="-3.036481201172" Y="23.436689453125" />
                  <Point X="-3.015102783203" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833740234" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890625" Y="23.556349609375" />
                  <Point X="-2.954186035156" Y="23.58609765625" />
                  <Point X="-2.951552490234" Y="23.60119921875" />
                  <Point X="-2.948748291016" Y="23.63162890625" />
                  <Point X="-2.948577636719" Y="23.64695703125" />
                  <Point X="-2.950787109375" Y="23.67862890625" />
                  <Point X="-2.953084716797" Y="23.693787109375" />
                  <Point X="-2.960087646484" Y="23.72353515625" />
                  <Point X="-2.971784179688" Y="23.751767578125" />
                  <Point X="-2.987869384766" Y="23.777751953125" />
                  <Point X="-2.996963378906" Y="23.79009375" />
                  <Point X="-3.017797851562" Y="23.814048828125" />
                  <Point X="-3.028755859375" Y="23.824763671875" />
                  <Point X="-3.052252441406" Y="23.84429296875" />
                  <Point X="-3.064791015625" Y="23.853107421875" />
                  <Point X="-3.091273681641" Y="23.868693359375" />
                  <Point X="-3.105441894531" Y="23.875521484375" />
                  <Point X="-3.134705566406" Y="23.886744140625" />
                  <Point X="-3.149801025391" Y="23.891138671875" />
                  <Point X="-3.181686279297" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-3.845908691406" Y="23.820603515625" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.693798339844" Y="23.84201953125" />
                  <Point X="-4.74076171875" Y="24.02587890625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.443686035156" Y="24.4371875" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.500468505859" Y="24.6902890625" />
                  <Point X="-3.473933349609" Y="24.700748046875" />
                  <Point X="-3.444169189453" Y="24.71610546875" />
                  <Point X="-3.433564697266" Y="24.722484375" />
                  <Point X="-3.416068603516" Y="24.734626953125" />
                  <Point X="-3.393674316406" Y="24.75175390625" />
                  <Point X="-3.371215087891" Y="24.773640625" />
                  <Point X="-3.360893310547" Y="24.78551953125" />
                  <Point X="-3.341650390625" Y="24.811775390625" />
                  <Point X="-3.333434326172" Y="24.825189453125" />
                  <Point X="-3.319328125" Y="24.8531875" />
                  <Point X="-3.313437988281" Y="24.867771484375" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.300990966797" Y="24.91149609375" />
                  <Point X="-3.296721435547" Y="24.939654296875" />
                  <Point X="-3.295647949219" Y="24.95389453125" />
                  <Point X="-3.295647216797" Y="24.983533203125" />
                  <Point X="-3.296720458984" Y="24.9977734375" />
                  <Point X="-3.300989501953" Y="25.02593359375" />
                  <Point X="-3.304185302734" Y="25.039853515625" />
                  <Point X="-3.313436279297" Y="25.069662109375" />
                  <Point X="-3.319326416016" Y="25.084248046875" />
                  <Point X="-3.333432128906" Y="25.11224609375" />
                  <Point X="-3.341647705078" Y="25.125658203125" />
                  <Point X="-3.360891357422" Y="25.151916015625" />
                  <Point X="-3.371212646484" Y="25.163794921875" />
                  <Point X="-3.393672851562" Y="25.18568359375" />
                  <Point X="-3.405811767578" Y="25.195693359375" />
                  <Point X="-3.423307861328" Y="25.2078359375" />
                  <Point X="-3.439404541016" Y="25.217859375" />
                  <Point X="-3.46523046875" Y="25.23219921875" />
                  <Point X="-3.475728759766" Y="25.237212890625" />
                  <Point X="-3.4972578125" Y="25.245919921875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-4.056657226562" Y="25.396546875" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.761597167969" Y="25.752994140625" />
                  <Point X="-4.731332519531" Y="25.957521484375" />
                  <Point X="-4.636472167969" Y="26.3075859375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.448583007812" Y="26.29387890625" />
                  <Point X="-3.778066894531" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736704101562" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704887695313" Y="26.2080546875" />
                  <Point X="-3.684603271484" Y="26.21208984375" />
                  <Point X="-3.674572998047" Y="26.21466015625" />
                  <Point X="-3.635848632812" Y="26.226869140625" />
                  <Point X="-3.613146972656" Y="26.23402734375" />
                  <Point X="-3.603451660156" Y="26.23767578125" />
                  <Point X="-3.584515625" Y="26.2460078125" />
                  <Point X="-3.575274902344" Y="26.25069140625" />
                  <Point X="-3.556531005859" Y="26.261513671875" />
                  <Point X="-3.547860107422" Y="26.267171875" />
                  <Point X="-3.531178955078" Y="26.27940234375" />
                  <Point X="-3.515923828125" Y="26.293380859375" />
                  <Point X="-3.502285644531" Y="26.30893359375" />
                  <Point X="-3.495892333984" Y="26.317080078125" />
                  <Point X="-3.483478027344" Y="26.334810546875" />
                  <Point X="-3.478008300781" Y="26.343607421875" />
                  <Point X="-3.468061035156" Y="26.361740234375" />
                  <Point X="-3.463583496094" Y="26.371076171875" />
                  <Point X="-3.448045166016" Y="26.408587890625" />
                  <Point X="-3.438936035156" Y="26.430580078125" />
                  <Point X="-3.435499755859" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358398438" Y="26.470298828125" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012695312" Y="26.60453125" />
                  <Point X="-3.443509277344" Y="26.62380859375" />
                  <Point X="-3.447783935547" Y="26.6332421875" />
                  <Point X="-3.466532470703" Y="26.6692578125" />
                  <Point X="-3.477523681641" Y="26.69037109375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494291748047" Y="26.716484375" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-3.858849121094" Y="27.011318359375" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.119893554687" Y="27.4788359375" />
                  <Point X="-4.002296386719" Y="27.680306640625" />
                  <Point X="-3.751019042969" Y="28.0032890625" />
                  <Point X="-3.726337402344" Y="28.035013671875" />
                  <Point X="-3.665969238281" Y="28.00016015625" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244922607422" Y="27.75771875" />
                  <Point X="-3.225993896484" Y="27.749388671875" />
                  <Point X="-3.216299560547" Y="27.745740234375" />
                  <Point X="-3.195656494141" Y="27.73923046875" />
                  <Point X="-3.185623046875" Y="27.736658203125" />
                  <Point X="-3.16533203125" Y="27.73262109375" />
                  <Point X="-3.155074462891" Y="27.73115625" />
                  <Point X="-3.101142333984" Y="27.7264375" />
                  <Point X="-3.069525146484" Y="27.723671875" />
                  <Point X="-3.059173339844" Y="27.723333984375" />
                  <Point X="-3.038488037109" Y="27.72378515625" />
                  <Point X="-3.028154541016" Y="27.72457421875" />
                  <Point X="-3.006696533203" Y="27.727400390625" />
                  <Point X="-2.996512207031" Y="27.7293125" />
                  <Point X="-2.976422851562" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.93844921875" Y="27.750447265625" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911165527344" Y="27.767158203125" />
                  <Point X="-2.902745117188" Y="27.773193359375" />
                  <Point X="-2.886613769531" Y="27.786140625" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.840621337891" Y="27.831333984375" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753515869141" Y="28.09833203125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.926907714844" Y="28.470453125" />
                  <Point X="-3.059387939453" Y="28.6999140625" />
                  <Point X="-2.853189941406" Y="28.858005859375" />
                  <Point X="-2.648377441406" Y="29.015033203125" />
                  <Point X="-2.252630615234" Y="29.234900390625" />
                  <Point X="-2.192523193359" Y="29.268294921875" />
                  <Point X="-2.118563720703" Y="29.17191015625" />
                  <Point X="-2.111821289062" Y="29.164048828125" />
                  <Point X="-2.097520263672" Y="29.149109375" />
                  <Point X="-2.089962402344" Y="29.14203125" />
                  <Point X="-2.073384033203" Y="29.128119140625" />
                  <Point X="-2.065097167969" Y="29.12190234375" />
                  <Point X="-2.047895996094" Y="29.110408203125" />
                  <Point X="-2.038981445312" Y="29.105130859375" />
                  <Point X="-1.978955444336" Y="29.0738828125" />
                  <Point X="-1.943765380859" Y="29.0555625" />
                  <Point X="-1.93433605957" Y="29.0512890625" />
                  <Point X="-1.915063720703" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313110352" Y="29.034966796875" />
                  <Point X="-1.874175292969" Y="29.032833984375" />
                  <Point X="-1.853725830078" Y="29.029685546875" />
                  <Point X="-1.833055053711" Y="29.028783203125" />
                  <Point X="-1.812409057617" Y="29.03013671875" />
                  <Point X="-1.802122070312" Y="29.031376953125" />
                  <Point X="-1.780806030273" Y="29.03513671875" />
                  <Point X="-1.770715698242" Y="29.037490234375" />
                  <Point X="-1.750860961914" Y="29.043279296875" />
                  <Point X="-1.741096435547" Y="29.04671484375" />
                  <Point X="-1.678575073242" Y="29.07261328125" />
                  <Point X="-1.641922485352" Y="29.087794921875" />
                  <Point X="-1.632585693359" Y="29.0922734375" />
                  <Point X="-1.614453857422" Y="29.102220703125" />
                  <Point X="-1.605658813477" Y="29.107689453125" />
                  <Point X="-1.587928955078" Y="29.120103515625" />
                  <Point X="-1.579780395508" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252807617" Y="29.155388671875" />
                  <Point X="-1.538021240234" Y="29.1720703125" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153918457" Y="29.19948828125" />
                  <Point X="-1.516856445312" Y="29.208728515625" />
                  <Point X="-1.508525512695" Y="29.227662109375" />
                  <Point X="-1.504877319336" Y="29.23735546875" />
                  <Point X="-1.484527587891" Y="29.301896484375" />
                  <Point X="-1.47259777832" Y="29.339732421875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266357422" Y="29.562654296875" />
                  <Point X="-1.196322143555" Y="29.641982421875" />
                  <Point X="-0.931164794922" Y="29.7163203125" />
                  <Point X="-0.451422729492" Y="29.772470703125" />
                  <Point X="-0.365222991943" Y="29.78255859375" />
                  <Point X="-0.347491790771" Y="29.716384765625" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.309397216797" Y="29.528265625" />
                  <Point X="0.378190795898" Y="29.7850078125" />
                  <Point X="0.596351928711" Y="29.76216015625" />
                  <Point X="0.827866333008" Y="29.7379140625" />
                  <Point X="1.224797485352" Y="29.64208203125" />
                  <Point X="1.453607421875" Y="29.58683984375" />
                  <Point X="1.710435913086" Y="29.4936875" />
                  <Point X="1.858248168945" Y="29.44007421875" />
                  <Point X="2.108080566406" Y="29.323236328125" />
                  <Point X="2.250429199219" Y="29.2566640625" />
                  <Point X="2.491843994141" Y="29.116015625" />
                  <Point X="2.629446533203" Y="29.035849609375" />
                  <Point X="2.817780761719" Y="28.901916015625" />
                  <Point X="2.608497070312" Y="28.539427734375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062373291016" Y="27.59310546875" />
                  <Point X="2.053183105469" Y="27.573443359375" />
                  <Point X="2.044182617188" Y="27.54956640625" />
                  <Point X="2.041301269531" Y="27.54059765625" />
                  <Point X="2.027766235352" Y="27.489982421875" />
                  <Point X="2.019831665039" Y="27.460310546875" />
                  <Point X="2.017912475586" Y="27.45146484375" />
                  <Point X="2.013646606445" Y="27.42022265625" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.018688842773" Y="27.3258125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023801025391" Y="27.289033203125" />
                  <Point X="2.029144775391" Y="27.267107421875" />
                  <Point X="2.032470214844" Y="27.256302734375" />
                  <Point X="2.040736694336" Y="27.23421484375" />
                  <Point X="2.045320556641" Y="27.223884765625" />
                  <Point X="2.055681640625" Y="27.20384375" />
                  <Point X="2.061458984375" Y="27.1941328125" />
                  <Point X="2.088540527344" Y="27.154220703125" />
                  <Point X="2.104416748047" Y="27.13082421875" />
                  <Point X="2.109807861328" Y="27.123634765625" />
                  <Point X="2.130460693359" Y="27.100126953125" />
                  <Point X="2.157593017578" Y="27.075390625" />
                  <Point X="2.168254882812" Y="27.066982421875" />
                  <Point X="2.208166015625" Y="27.039900390625" />
                  <Point X="2.231563476562" Y="27.0240234375" />
                  <Point X="2.241277587891" Y="27.01824609375" />
                  <Point X="2.261319580078" Y="27.007884765625" />
                  <Point X="2.271647460938" Y="27.00330078125" />
                  <Point X="2.293737060547" Y="26.995033203125" />
                  <Point X="2.304543945312" Y="26.99170703125" />
                  <Point X="2.326471435547" Y="26.98636328125" />
                  <Point X="2.337592041016" Y="26.984345703125" />
                  <Point X="2.381358886719" Y="26.979068359375" />
                  <Point X="2.407016845703" Y="26.975974609375" />
                  <Point X="2.416039794922" Y="26.9753203125" />
                  <Point X="2.447578613281" Y="26.97549609375" />
                  <Point X="2.484317626953" Y="26.979822265625" />
                  <Point X="2.49775" Y="26.98239453125" />
                  <Point X="2.548364257812" Y="26.9959296875" />
                  <Point X="2.578036376953" Y="27.003865234375" />
                  <Point X="2.584000732422" Y="27.005671875" />
                  <Point X="2.604419189453" Y="27.013072265625" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.187586914062" Y="27.346314453125" />
                  <Point X="3.940403564453" Y="27.780953125" />
                  <Point X="3.962337890625" Y="27.75046875" />
                  <Point X="4.043955810547" Y="27.637037109375" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="3.888110351562" Y="27.29258203125" />
                  <Point X="3.172951904297" Y="26.7438203125" />
                  <Point X="3.16813671875" Y="26.7398671875" />
                  <Point X="3.152113769531" Y="26.7252109375" />
                  <Point X="3.134666503906" Y="26.706599609375" />
                  <Point X="3.128576904297" Y="26.699421875" />
                  <Point X="3.092149658203" Y="26.651900390625" />
                  <Point X="3.070794677734" Y="26.624041015625" />
                  <Point X="3.065635498047" Y="26.6166015625" />
                  <Point X="3.049740234375" Y="26.589373046875" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.016571289062" Y="26.49415234375" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.015838378906" Y="26.2985859375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024597900391" Y="26.258234375" />
                  <Point X="3.034681640625" Y="26.22861328125" />
                  <Point X="3.050285400391" Y="26.19537109375" />
                  <Point X="3.056918945312" Y="26.1835234375" />
                  <Point X="3.087215332031" Y="26.137474609375" />
                  <Point X="3.1049765625" Y="26.110478515625" />
                  <Point X="3.111741699219" Y="26.101419921875" />
                  <Point X="3.126299072266" Y="26.084171875" />
                  <Point X="3.134091308594" Y="26.075982421875" />
                  <Point X="3.151335205078" Y="26.05989453125" />
                  <Point X="3.160037597656" Y="26.052693359375" />
                  <Point X="3.178241943359" Y="26.03937109375" />
                  <Point X="3.187743896484" Y="26.03325" />
                  <Point X="3.231647705078" Y="26.00853515625" />
                  <Point X="3.257385986328" Y="25.994046875" />
                  <Point X="3.26548046875" Y="25.989986328125" />
                  <Point X="3.294675537109" Y="25.978083984375" />
                  <Point X="3.330276123047" Y="25.96801953125" />
                  <Point X="3.343675292969" Y="25.965255859375" />
                  <Point X="3.403036132813" Y="25.957412109375" />
                  <Point X="3.4378359375" Y="25.9528125" />
                  <Point X="3.444032958984" Y="25.95219921875" />
                  <Point X="3.465707519531" Y="25.95122265625" />
                  <Point X="3.491213623047" Y="25.95203125" />
                  <Point X="3.500603027344" Y="25.952796875" />
                  <Point X="4.024541503906" Y="26.0217734375" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.717630859375" Y="26.05821875" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.511579101562" Y="25.640962890625" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686023681641" Y="25.419541015625" />
                  <Point X="3.665624023438" Y="25.41213671875" />
                  <Point X="3.642384277344" Y="25.40162109375" />
                  <Point X="3.6340078125" Y="25.397318359375" />
                  <Point X="3.5756875" Y="25.363609375" />
                  <Point X="3.541497802734" Y="25.34384765625" />
                  <Point X="3.533881835938" Y="25.338947265625" />
                  <Point X="3.508776123047" Y="25.319873046875" />
                  <Point X="3.481993652344" Y="25.2943515625" />
                  <Point X="3.472795166016" Y="25.284224609375" />
                  <Point X="3.437802978516" Y="25.239634765625" />
                  <Point X="3.417289306641" Y="25.21349609375" />
                  <Point X="3.410853027344" Y="25.204205078125" />
                  <Point X="3.399129882813" Y="25.18492578125" />
                  <Point X="3.393843017578" Y="25.1749375" />
                  <Point X="3.384068847656" Y="25.15347265625" />
                  <Point X="3.380005615234" Y="25.142927734375" />
                  <Point X="3.373159423828" Y="25.121427734375" />
                  <Point X="3.370376464844" Y="25.11047265625" />
                  <Point X="3.358712402344" Y="25.04956640625" />
                  <Point X="3.351874267578" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584472656" Y="24.973736328125" />
                  <Point X="3.350280273438" Y="24.93705859375" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.363538818359" Y="24.862671875" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816015625" />
                  <Point X="3.380003662109" Y="24.794515625" />
                  <Point X="3.384067382812" Y="24.783966796875" />
                  <Point X="3.393840332031" Y="24.76250390625" />
                  <Point X="3.399129638672" Y="24.75251171875" />
                  <Point X="3.410854980469" Y="24.73323046875" />
                  <Point X="3.417291015625" Y="24.72394140625" />
                  <Point X="3.452283203125" Y="24.679353515625" />
                  <Point X="3.472796875" Y="24.653212890625" />
                  <Point X="3.478716308594" Y="24.646369140625" />
                  <Point X="3.501139160156" Y="24.624193359375" />
                  <Point X="3.530176757813" Y="24.6012734375" />
                  <Point X="3.54149609375" Y="24.59359375" />
                  <Point X="3.59981640625" Y="24.559884765625" />
                  <Point X="3.634006103516" Y="24.54012109375" />
                  <Point X="3.639491455078" Y="24.537185546875" />
                  <Point X="3.659139648438" Y="24.527990234375" />
                  <Point X="3.683021240234" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.172467285156" Y="24.387341796875" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.781185546875" Y="24.198763671875" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.920779296875" />
                  <Point X="4.387272460938" Y="23.965611328125" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354481201172" Y="24.087322265625" />
                  <Point X="3.240019287109" Y="24.062443359375" />
                  <Point X="3.172916992188" Y="24.047859375" />
                  <Point X="3.157872802734" Y="24.043255859375" />
                  <Point X="3.128752685547" Y="24.031630859375" />
                  <Point X="3.114676757812" Y="24.024609375" />
                  <Point X="3.086849853516" Y="24.007716796875" />
                  <Point X="3.074125244141" Y="23.99846875" />
                  <Point X="3.050374755859" Y="23.977998046875" />
                  <Point X="3.039348876953" Y="23.966775390625" />
                  <Point X="2.970163818359" Y="23.88356640625" />
                  <Point X="2.929604736328" Y="23.834787109375" />
                  <Point X="2.921325683594" Y="23.82315234375" />
                  <Point X="2.906605957031" Y="23.798771484375" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.865241210938" Y="23.59558203125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.42333203125" />
                  <Point X="2.889158203125" Y="23.39451953125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.959885986328" Y="23.282099609375" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001742675781" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.498680419922" Y="22.82158203125" />
                  <Point X="4.087170898438" Y="22.370017578125" />
                  <Point X="4.045493896484" Y="22.302576171875" />
                  <Point X="4.0012734375" Y="22.23974609375" />
                  <Point X="3.694994628906" Y="22.416576171875" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841200439453" Y="22.909109375" />
                  <Point X="2.815037597656" Y="22.920482421875" />
                  <Point X="2.783124511719" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.9336640625" />
                  <Point X="2.634880615234" Y="22.958267578125" />
                  <Point X="2.555018310547" Y="22.972689453125" />
                  <Point X="2.539353027344" Y="22.974193359375" />
                  <Point X="2.507996582031" Y="22.974595703125" />
                  <Point X="2.492305419922" Y="22.973494140625" />
                  <Point X="2.460133056641" Y="22.96853515625" />
                  <Point X="2.444840820312" Y="22.964861328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400590087891" Y="22.948890625" />
                  <Point X="2.28741796875" Y="22.889330078125" />
                  <Point X="2.221072021484" Y="22.854412109375" />
                  <Point X="2.208969726562" Y="22.846830078125" />
                  <Point X="2.1860390625" Y="22.8299375" />
                  <Point X="2.175210693359" Y="22.820626953125" />
                  <Point X="2.154251220703" Y="22.79966796875" />
                  <Point X="2.144939697266" Y="22.788837890625" />
                  <Point X="2.128046142578" Y="22.76590625" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.06090234375" Y="22.6406328125" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.026790405273" Y="22.283630859375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.356072021484" Y="21.63014453125" />
                  <Point X="2.735892822266" Y="20.97227734375" />
                  <Point X="2.723752197266" Y="20.96391796875" />
                  <Point X="2.479902587891" Y="21.28170703125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657714844" Y="22.12984765625" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251831055" Y="22.17199609375" />
                  <Point X="1.773298339844" Y="22.179353515625" />
                  <Point X="1.638940795898" Y="22.265732421875" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.323755859375" />
                  <Point X="1.517466552734" Y="22.336126953125" />
                  <Point X="1.502547851562" Y="22.34111328125" />
                  <Point X="1.470927856445" Y="22.34884375" />
                  <Point X="1.455391601563" Y="22.351302734375" />
                  <Point X="1.424125610352" Y="22.35362109375" />
                  <Point X="1.408395751953" Y="22.35348046875" />
                  <Point X="1.261452758789" Y="22.339958984375" />
                  <Point X="1.17530871582" Y="22.332033203125" />
                  <Point X="1.161225463867" Y="22.32966015625" />
                  <Point X="1.133575561523" Y="22.322828125" />
                  <Point X="1.120008789062" Y="22.318369140625" />
                  <Point X="1.092622558594" Y="22.307025390625" />
                  <Point X="1.079876708984" Y="22.3005859375" />
                  <Point X="1.055494384766" Y="22.285865234375" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.930392272949" Y="22.183240234375" />
                  <Point X="0.863874023438" Y="22.127931640625" />
                  <Point X="0.852651977539" Y="22.116908203125" />
                  <Point X="0.832181396484" Y="22.093158203125" />
                  <Point X="0.822932739258" Y="22.080431640625" />
                  <Point X="0.806040710449" Y="22.05260546875" />
                  <Point X="0.799019287109" Y="22.038529296875" />
                  <Point X="0.787394775391" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.748866210938" Y="21.838283203125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.806753967285" Y="21.0477109375" />
                  <Point X="0.833091125488" Y="20.84766015625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143554688" Y="21.546423828125" />
                  <Point X="0.626786315918" Y="21.576185546875" />
                  <Point X="0.620407165527" Y="21.586791015625" />
                  <Point X="0.517189575195" Y="21.735505859375" />
                  <Point X="0.456679534912" Y="21.82269140625" />
                  <Point X="0.446669342041" Y="21.834828125" />
                  <Point X="0.424786834717" Y="21.85728125" />
                  <Point X="0.412914398193" Y="21.86759765625" />
                  <Point X="0.386659576416" Y="21.886841796875" />
                  <Point X="0.373244415283" Y="21.895060546875" />
                  <Point X="0.345241333008" Y="21.909169921875" />
                  <Point X="0.330653411865" Y="21.915060546875" />
                  <Point X="0.170930618286" Y="21.964630859375" />
                  <Point X="0.077294471741" Y="21.993693359375" />
                  <Point X="0.06338053894" Y="21.996888671875" />
                  <Point X="0.035228240967" Y="22.001158203125" />
                  <Point X="0.020989871979" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022896093369" Y="22.001162109375" />
                  <Point X="-0.051061916351" Y="21.996892578125" />
                  <Point X="-0.064983421326" Y="21.9936953125" />
                  <Point X="-0.224706375122" Y="21.944123046875" />
                  <Point X="-0.318342529297" Y="21.9150625" />
                  <Point X="-0.332927032471" Y="21.909171875" />
                  <Point X="-0.360928344727" Y="21.895064453125" />
                  <Point X="-0.374345123291" Y="21.88684765625" />
                  <Point X="-0.400600524902" Y="21.867603515625" />
                  <Point X="-0.412477752686" Y="21.85728125" />
                  <Point X="-0.434361419678" Y="21.83482421875" />
                  <Point X="-0.444368041992" Y="21.822689453125" />
                  <Point X="-0.547585327148" Y="21.67397265625" />
                  <Point X="-0.60809564209" Y="21.5867890625" />
                  <Point X="-0.612468444824" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.790799255371" Y="20.959546875" />
                  <Point X="-0.985424865723" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.538352808235" Y="26.305697967193" />
                  <Point X="-4.746077090621" Y="25.581277304465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.086947264963" Y="27.535280427371" />
                  <Point X="-4.168958590522" Y="27.249272946057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.443119664376" Y="26.293159657158" />
                  <Point X="-4.654300192148" Y="25.556685634347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.913983642234" Y="27.793820512073" />
                  <Point X="-4.087953443599" Y="27.187115713998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.347886280704" Y="26.28062218345" />
                  <Point X="-4.562523293676" Y="25.532093964229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75746793447" Y="27.994999900555" />
                  <Point X="-4.006948296677" Y="27.124958481939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.252652897032" Y="26.268084709741" />
                  <Point X="-4.470746395204" Y="25.507502294111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.658411079957" Y="27.995796454281" />
                  <Point X="-3.925943149755" Y="27.062801249879" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.157419513361" Y="26.255547236033" />
                  <Point X="-4.378969496731" Y="25.482910623993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.698396369511" Y="24.368936734112" />
                  <Point X="-4.759399608483" Y="24.156193157397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.573620001773" Y="27.946842333588" />
                  <Point X="-3.844938013044" Y="27.000643982207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062186129689" Y="26.243009762324" />
                  <Point X="-4.287192598259" Y="25.458318953875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.591342468725" Y="24.397622302518" />
                  <Point X="-4.720586622686" Y="23.946894373212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488828923589" Y="27.897888212895" />
                  <Point X="-3.763932925586" Y="26.938486542774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.966952746017" Y="26.230472288615" />
                  <Point X="-4.195415699787" Y="25.433727283757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.48428856794" Y="24.426307870924" />
                  <Point X="-4.674026352641" Y="23.764613580015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.404037845405" Y="27.848934092202" />
                  <Point X="-3.682927838127" Y="26.87632910334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871719362345" Y="26.217934814907" />
                  <Point X="-4.103638801314" Y="25.409135613639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.377234788427" Y="24.454993016406" />
                  <Point X="-4.587124089147" Y="23.723022037466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.31924676722" Y="27.799979971509" />
                  <Point X="-3.601922750668" Y="26.814171663907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776461468573" Y="26.205482818078" />
                  <Point X="-4.011861857457" Y="25.384544101796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.270181083012" Y="24.483677903475" />
                  <Point X="-4.484418484359" Y="23.736543295607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.93502564619" Y="28.795262507159" />
                  <Point X="-2.994562300881" Y="28.587633517651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.233924394122" Y="27.752878696373" />
                  <Point X="-3.521741475279" Y="26.749141250363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.675035443304" Y="26.21454165212" />
                  <Point X="-3.920084866" Y="25.359952755955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.163127377597" Y="24.512362790544" />
                  <Point X="-4.38171287957" Y="23.750064553747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.808347470119" Y="28.892386056645" />
                  <Point X="-2.928529383003" Y="28.473261917764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.141661276593" Y="27.729982673616" />
                  <Point X="-3.453176272516" Y="26.64360077736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.563969502599" Y="26.257218866489" />
                  <Point X="-3.828307874542" Y="25.335361410114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.056073672182" Y="24.541047677614" />
                  <Point X="-4.279007274782" Y="23.763585811887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.681669616136" Y="28.989508482878" />
                  <Point X="-2.862496469458" Y="28.35889030277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.044648468692" Y="27.723650789666" />
                  <Point X="-3.736530883085" Y="25.310770064272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.949019966767" Y="24.569732564683" />
                  <Point X="-4.176301669994" Y="23.777107070028" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.561716184128" Y="29.063180062791" />
                  <Point X="-2.796463556021" Y="28.244518687396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.938076032333" Y="27.750657292079" />
                  <Point X="-3.644753891628" Y="25.286178718431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.841966261352" Y="24.598417451752" />
                  <Point X="-4.073596065206" Y="23.790628328168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.44416002152" Y="29.12849137077" />
                  <Point X="-2.750251766603" Y="28.061022597827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.805356490768" Y="27.868849586849" />
                  <Point X="-3.552976900171" Y="25.261587372589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.734912555937" Y="24.627102338822" />
                  <Point X="-3.970890460417" Y="23.804149586308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.326603858912" Y="29.193802678749" />
                  <Point X="-3.462940042312" Y="25.230927459703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.627858850522" Y="24.655787225891" />
                  <Point X="-3.868184855629" Y="23.817670844448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.20904761509" Y="29.259114269958" />
                  <Point X="-3.38070806013" Y="25.173048710649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.520805145107" Y="24.68447211296" />
                  <Point X="-3.765479216924" Y="23.83119222087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11676335822" Y="22.606118832624" />
                  <Point X="-4.147290980773" Y="22.499656360794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.130692243066" Y="29.187716174645" />
                  <Point X="-3.312442994261" Y="25.066461535909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.405214783228" Y="24.742927859083" />
                  <Point X="-3.662773568826" Y="23.844713630052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.990055576998" Y="22.703345627541" />
                  <Point X="-4.080552346009" Y="22.387745888173" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.053044922413" Y="29.113848810754" />
                  <Point X="-3.560067920727" Y="23.858235039233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.863347809199" Y="22.800572375651" />
                  <Point X="-4.013813711244" Y="22.275835415551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.967401364908" Y="29.067867638759" />
                  <Point X="-3.457362272629" Y="23.871756448414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.736640315707" Y="22.897798167134" />
                  <Point X="-3.942769373708" Y="22.178940712968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.878365857193" Y="29.033715602915" />
                  <Point X="-3.35465662453" Y="23.885277857596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.609932822216" Y="22.995023958617" />
                  <Point X="-3.870984114831" Y="22.084629910167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.779009775601" Y="29.035555685482" />
                  <Point X="-3.251950976431" Y="23.898799266777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.483225328725" Y="23.092249750099" />
                  <Point X="-3.79919908531" Y="21.990318307511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.668339551161" Y="29.076852873235" />
                  <Point X="-3.155015287653" Y="23.892198436484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.356517835234" Y="23.189475541582" />
                  <Point X="-3.684734870137" Y="22.044846713346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.425921162715" Y="29.577610511091" />
                  <Point X="-1.463830078758" Y="29.445406409734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.544896733984" Y="29.162693385385" />
                  <Point X="-3.067019869529" Y="23.85441917718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.229810341742" Y="23.286701333065" />
                  <Point X="-3.566299024519" Y="22.11322584056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.318452931809" Y="29.607741020347" />
                  <Point X="-2.989529397321" Y="23.780004817758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.103102848251" Y="23.383927124547" />
                  <Point X="-3.447863178902" Y="22.181604967773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.210984700902" Y="29.637871529603" />
                  <Point X="-3.329427333284" Y="22.249984094986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.103516839999" Y="29.668000748505" />
                  <Point X="-3.210991487667" Y="22.3183632222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.996049037553" Y="29.698129763542" />
                  <Point X="-3.092555691373" Y="22.386742177401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.890644617041" Y="29.721062910617" />
                  <Point X="-2.974119928426" Y="22.455121016308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.788384155716" Y="29.733031769016" />
                  <Point X="-2.855684165479" Y="22.523499855215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.68612369439" Y="29.745000627414" />
                  <Point X="-2.737248402532" Y="22.591878694122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.583863233065" Y="29.756969485812" />
                  <Point X="-2.623496618439" Y="22.643922557518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.481602771739" Y="29.768938344211" />
                  <Point X="-2.523406624964" Y="22.648322094985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.915082384457" Y="21.282386394029" />
                  <Point X="-2.953740894273" Y="21.147568148518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.379342601808" Y="29.780906186399" />
                  <Point X="-2.435087043491" Y="22.611673327627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.718737430204" Y="21.622466872003" />
                  <Point X="-2.872786197916" Y="21.08523497443" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324074895128" Y="29.62899183349" />
                  <Point X="-2.357646105734" Y="22.537086221043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.522393027708" Y="21.962545425772" />
                  <Point X="-2.791831501558" Y="21.022901800342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.276335073947" Y="29.450824623961" />
                  <Point X="-2.709527767363" Y="20.965273280296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.228595252766" Y="29.272657414432" />
                  <Point X="-2.625600112218" Y="20.913308045624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.163123749717" Y="29.156327928363" />
                  <Point X="-2.541672581273" Y="20.861342377814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.07976970986" Y="29.102362259453" />
                  <Point X="-2.417737149683" Y="20.948900840585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.014112671638" Y="29.08511328125" />
                  <Point X="-2.288414808023" Y="21.055245691338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.121702018983" Y="29.115666173722" />
                  <Point X="-2.166163349148" Y="21.136931443337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.411461946764" Y="29.781523379649" />
                  <Point X="-2.047305482992" Y="21.206782331074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.507409065489" Y="29.771474995876" />
                  <Point X="-1.943558677945" Y="21.223934686036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.603356184456" Y="29.761426612944" />
                  <Point X="-1.846553069755" Y="21.217577693709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.699303306486" Y="29.751378240694" />
                  <Point X="-1.749547811981" Y="21.211219479336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.795250428515" Y="29.741329868443" />
                  <Point X="-1.652542616129" Y="21.204861049016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888875749613" Y="29.723184414086" />
                  <Point X="-1.56000321705" Y="21.182928534526" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.981305331225" Y="29.700868920577" />
                  <Point X="-1.476563770049" Y="21.129260715725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.073734912837" Y="29.678553427069" />
                  <Point X="-1.397593931453" Y="21.060005520011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.16616449445" Y="29.65623793356" />
                  <Point X="-1.318623898757" Y="20.990751001202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.258594073652" Y="29.633922431645" />
                  <Point X="-1.240778594628" Y="20.917574087746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.351023648671" Y="29.611606915145" />
                  <Point X="-1.179728971852" Y="20.785823672545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.443453223691" Y="29.589291398645" />
                  <Point X="-1.139251679127" Y="20.582329016378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.533291312305" Y="29.557938295025" />
                  <Point X="-1.131111210403" Y="20.266062453126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.622809566875" Y="29.525469797538" />
                  <Point X="-1.038739876423" Y="20.243543825982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.712327816784" Y="29.493001283796" />
                  <Point X="-0.517181210192" Y="21.717779300444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.801845850827" Y="29.460532017238" />
                  <Point X="-0.368920586739" Y="21.890169788666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.890485117156" Y="29.424998123466" />
                  <Point X="-0.257543579218" Y="21.933931821944" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977627676116" Y="29.384244590794" />
                  <Point X="-0.149060726464" Y="21.967600738087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.064770235076" Y="29.343491058121" />
                  <Point X="-0.0414136021" Y="21.99835512297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.151912733351" Y="29.302737313817" />
                  <Point X="0.0572604908" Y="21.997816828319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239055171664" Y="29.2619833604" />
                  <Point X="0.148563654575" Y="21.971573048976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324058041434" Y="29.213767844744" />
                  <Point X="0.239315729136" Y="21.943407393148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.408739728388" Y="29.164432231494" />
                  <Point X="0.330067949654" Y="21.915242246331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.493421419075" Y="29.115096631261" />
                  <Point X="2.045562573675" Y="27.553227225012" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.032452178531" Y="27.507505843622" />
                  <Point X="0.414771745828" Y="21.865983737093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.578103306388" Y="29.065761716747" />
                  <Point X="2.248269909262" Y="27.915495963549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.04830068947" Y="27.218120418122" />
                  <Point X="0.488174145808" Y="21.777312575535" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661764385024" Y="29.012866819307" />
                  <Point X="2.444614278203" Y="28.2555744003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.117519013315" Y="27.114857649015" />
                  <Point X="0.558109188261" Y="21.676549301256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.74385336656" Y="28.954489367735" />
                  <Point X="2.640958495131" Y="28.595652306918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.19702211846" Y="27.047462174769" />
                  <Point X="0.627668538874" Y="21.574475833827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.282069030173" Y="26.999400251618" />
                  <Point X="0.891873637548" Y="22.151212759617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733972218926" Y="21.600545071612" />
                  <Point X="0.680925795597" Y="21.415550208698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375277660804" Y="26.979801624911" />
                  <Point X="1.021641607865" Y="22.259111702185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765069368703" Y="21.364337969446" />
                  <Point X="0.72866552817" Y="21.237382690157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473755386911" Y="26.978578517869" />
                  <Point X="1.139134292211" Y="22.324201635159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796166518481" Y="21.12813086728" />
                  <Point X="0.776405260743" Y="21.059215171616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580005735083" Y="27.004461765286" />
                  <Point X="1.241967006096" Y="22.338166175401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827263701497" Y="20.891923881031" />
                  <Point X="0.824144993316" Y="20.881047653075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.695371270039" Y="27.062133446752" />
                  <Point X="1.343473729405" Y="22.347506436953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813807074926" Y="27.130512431923" />
                  <Point X="1.443640596948" Y="22.352174066153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932242879814" Y="27.198891417094" />
                  <Point X="1.53563127787" Y="22.328327944037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.050678684701" Y="27.267270402264" />
                  <Point X="1.62000207507" Y="22.277908129373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.169114489589" Y="27.335649387435" />
                  <Point X="1.7034474254" Y="22.224260897921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.287550272271" Y="27.404028295168" />
                  <Point X="3.058017261094" Y="26.603551556649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.001160925621" Y="26.405269951097" />
                  <Point X="1.78650150848" Y="22.169249155412" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.40598605085" Y="27.472407188591" />
                  <Point X="3.203869965524" Y="26.767544633291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.043777436289" Y="26.209235634486" />
                  <Point X="1.861809319592" Y="22.087222952159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52442182943" Y="27.540786082014" />
                  <Point X="3.330577652218" Y="26.864771098552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.111704505368" Y="26.101469724858" />
                  <Point X="2.169220302153" Y="22.814636701473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.022990368695" Y="22.30467231941" />
                  <Point X="1.93375286736" Y="21.993464168322" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.642857608009" Y="27.609164975437" />
                  <Point X="3.457285338912" Y="26.961997563813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.190522709811" Y="26.031685718008" />
                  <Point X="2.289830856593" Y="22.890599939647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.070321111711" Y="22.125078484782" />
                  <Point X="2.005696415127" Y="21.899705384486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.761293386588" Y="27.67754386886" />
                  <Point X="3.583993025606" Y="27.059224029074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.276147104712" Y="25.985637718068" />
                  <Point X="2.406037136856" Y="22.95120364844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.136353932779" Y="22.010706547279" />
                  <Point X="2.077639962894" Y="21.805946600649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879729165167" Y="27.745922762283" />
                  <Point X="3.7107007123" Y="27.156450494334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.368201845649" Y="25.962014999776" />
                  <Point X="2.511560035941" Y="22.974549979402" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.202386753846" Y="21.896334609776" />
                  <Point X="2.149583510662" Y="21.712187816812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974867793201" Y="27.733054836395" />
                  <Point X="3.837408398994" Y="27.253676959595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463958237226" Y="25.951301471389" />
                  <Point X="2.60715524575" Y="22.963274343389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.268419574914" Y="21.781962672274" />
                  <Point X="2.221527058429" Y="21.618429032976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.045453789811" Y="27.634561709041" />
                  <Point X="3.964115955006" Y="27.350902969115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.565671835386" Y="25.961363191286" />
                  <Point X="2.969720212426" Y="23.883032893546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860394200892" Y="23.501767781834" />
                  <Point X="2.701117752269" Y="22.946304794344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.334452395982" Y="21.667590734771" />
                  <Point X="2.293470606196" Y="21.524670249139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.112508644661" Y="27.523754026916" />
                  <Point X="4.090823423844" Y="27.448128674619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668377424349" Y="25.974884394239" />
                  <Point X="3.468916778911" Y="25.279282458359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.357899335823" Y="24.892118623816" />
                  <Point X="3.107979209931" Y="24.020543566974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.915866696594" Y="23.350567613119" />
                  <Point X="2.794428925704" Y="22.927063776889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.400485222007" Y="21.553218814556" />
                  <Point X="2.365414153964" Y="21.430911465302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.771083013313" Y="25.988405597191" />
                  <Point X="3.595151506295" Y="25.374859518491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.411075905588" Y="24.732911609827" />
                  <Point X="3.217413404535" Y="24.057530206425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.98421232835" Y="23.244261405019" />
                  <Point X="2.881545870435" Y="22.886220916787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.466518050444" Y="21.438846902755" />
                  <Point X="2.437357701731" Y="21.337152681466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.873788602276" Y="26.001926800144" />
                  <Point X="3.708586948748" Y="25.425800167485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483680830281" Y="24.641459321433" />
                  <Point X="3.322810702483" Y="24.080438514167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.05865663268" Y="23.159223795739" />
                  <Point X="2.966336943906" Y="22.837266779656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532550878882" Y="21.324474990955" />
                  <Point X="2.50930130904" Y="21.243394105276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.976494191239" Y="26.015448003096" />
                  <Point X="3.815640742187" Y="25.454485361528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.564904397912" Y="24.580063812909" />
                  <Point X="3.424819714056" Y="24.091530463068" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.139661747509" Y="23.097066451758" />
                  <Point X="3.051128017377" Y="22.788312642525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.59858370732" Y="21.210103079154" />
                  <Point X="2.581245002516" Y="21.149635829586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.079199862886" Y="26.028969494398" />
                  <Point X="3.922694535625" Y="25.483170555572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.650024239454" Y="24.532256226298" />
                  <Point X="3.52027144037" Y="24.079754440645" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.220666862339" Y="23.034909107776" />
                  <Point X="3.135919090847" Y="22.739358505395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.664616535757" Y="21.095731167354" />
                  <Point X="2.653188695992" Y="21.055877553897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.181905607214" Y="26.042491239174" />
                  <Point X="4.029748329063" Y="25.511855749615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.740489225648" Y="24.503089374352" />
                  <Point X="3.615504749999" Y="24.067216708718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.301671977168" Y="22.972751763795" />
                  <Point X="3.220710164318" Y="22.690404368264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730649364195" Y="20.981359255553" />
                  <Point X="2.726114577693" Y="20.965544575608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.284611351543" Y="26.05601298395" />
                  <Point X="4.136802122502" Y="25.540540943658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.832266190863" Y="24.478497936991" />
                  <Point X="3.710738059628" Y="24.05467897679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.382677091997" Y="22.910594419814" />
                  <Point X="3.305501237789" Y="22.641450231133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.387317095872" Y="26.069534728726" />
                  <Point X="4.24385591594" Y="25.569226137701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.924043156077" Y="24.45390649963" />
                  <Point X="3.805971369257" Y="24.042141244863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463682206826" Y="22.848437075832" />
                  <Point X="3.39029231126" Y="22.592496094003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.490022840201" Y="26.083056473502" />
                  <Point X="4.350909709378" Y="25.597911331744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.015820121291" Y="24.429315062269" />
                  <Point X="3.901204678885" Y="24.029603512936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.544687315922" Y="22.786279711857" />
                  <Point X="3.475083384731" Y="22.543541956872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.592728584529" Y="26.096578218278" />
                  <Point X="4.457963502817" Y="25.626596525788" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107597086506" Y="24.404723624908" />
                  <Point X="3.996437988514" Y="24.017065781008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.625692420657" Y="22.724122332672" />
                  <Point X="3.559874458201" Y="22.494587819741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.695434328858" Y="26.110099963054" />
                  <Point X="4.565017302238" Y="25.655281740694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.199374052689" Y="24.380132190927" />
                  <Point X="4.091671298143" Y="24.004528049081" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.706697525391" Y="22.661964953487" />
                  <Point X="3.644665531672" Y="22.44563368261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.745987112013" Y="25.941742717741" />
                  <Point X="4.672071107661" Y="25.683966976534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.29115102121" Y="24.355540765096" />
                  <Point X="4.186904607772" Y="23.991990317154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.787702630126" Y="22.599807574302" />
                  <Point X="3.729456606008" Y="22.396679548497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.782328922036" Y="25.723825919469" />
                  <Point X="4.779124913084" Y="25.712652212373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.38292798973" Y="24.330949339264" />
                  <Point X="4.282137917401" Y="23.979452585226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.868707734861" Y="22.537650195117" />
                  <Point X="3.814247681608" Y="22.347725418789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.474704958251" Y="24.306357913433" />
                  <Point X="4.37737122703" Y="23.966914853299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.949712839596" Y="22.475492815932" />
                  <Point X="3.899038757207" Y="22.298771289082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.566481926771" Y="24.281766487602" />
                  <Point X="4.472604509266" Y="23.954377025842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.03071794433" Y="22.413335436747" />
                  <Point X="3.983829832806" Y="22.249817159374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.658258895292" Y="24.257175061771" />
                  <Point X="4.567837788323" Y="23.941839187301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.750035863812" Y="24.23258363594" />
                  <Point X="4.663071067381" Y="23.92930134876" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.724073974609" Y="20.5204140625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318969727" Y="21.478455078125" />
                  <Point X="0.361101531982" Y="21.627169921875" />
                  <Point X="0.300591339111" Y="21.71435546875" />
                  <Point X="0.274336364746" Y="21.733599609375" />
                  <Point X="0.114613487244" Y="21.783169921875" />
                  <Point X="0.020977386475" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.168387481689" Y="21.762662109375" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288279144287" Y="21.714357421875" />
                  <Point X="-0.391496459961" Y="21.565640625" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.6072734375" Y="20.91037109375" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.99094329834" Y="20.04071875" />
                  <Point X="-1.100256713867" Y="20.0619375" />
                  <Point X="-1.285200195312" Y="20.10951953125" />
                  <Point X="-1.35158996582" Y="20.1266015625" />
                  <Point X="-1.339549560547" Y="20.218056640625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.347841186523" Y="20.657072265625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282470703" Y="20.79737109375" />
                  <Point X="-1.533335083008" Y="20.92633203125" />
                  <Point X="-1.619543334961" Y="21.001935546875" />
                  <Point X="-1.649241210938" Y="21.014236328125" />
                  <Point X="-1.844412719727" Y="21.027029296875" />
                  <Point X="-1.958830444336" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.152505859375" Y="20.917544921875" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.342850585938" Y="20.734384765625" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.695620849609" Y="20.733189453125" />
                  <Point X="-2.855839599609" Y="20.832392578125" />
                  <Point X="-3.111942138672" Y="21.029583984375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.975341552734" Y="21.558015625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513981933594" Y="22.43123828125" />
                  <Point X="-2.531326904297" Y="22.44858203125" />
                  <Point X="-2.560156494141" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.068218017578" Y="22.18140234375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.035126220703" Y="21.98657421875" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.345312988281" Y="22.460755859375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.984923095703" Y="22.946775390625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.140326660156" Y="23.66540625" />
                  <Point X="-3.161161132812" Y="23.689361328125" />
                  <Point X="-3.187643798828" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-3.821108886719" Y="23.632228515625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.877887695312" Y="23.79499609375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.975971679688" Y="24.328462890625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.492861816406" Y="24.62071484375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541894775391" Y="24.87857421875" />
                  <Point X="-3.524398681641" Y="24.890716796875" />
                  <Point X="-3.514141845703" Y="24.8978359375" />
                  <Point X="-3.494898925781" Y="24.924091796875" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514141845703" Y="25.039603515625" />
                  <Point X="-3.531637939453" Y="25.05174609375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.105832519531" Y="25.21301953125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.949550292969" Y="25.780806640625" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.819858886719" Y="26.35728125" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.423782226562" Y="26.48225390625" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731703857422" Y="26.3958671875" />
                  <Point X="-3.692979492188" Y="26.408076171875" />
                  <Point X="-3.670277832031" Y="26.415234375" />
                  <Point X="-3.651533935547" Y="26.426056640625" />
                  <Point X="-3.639119628906" Y="26.443787109375" />
                  <Point X="-3.623581298828" Y="26.481298828125" />
                  <Point X="-3.614472167969" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.635064697266" Y="26.58152734375" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.974513671875" Y="26.860580078125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.283986328125" Y="27.57461328125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.90098046875" Y="28.11995703125" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.570969238281" Y="28.164703125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138513916016" Y="27.92043359375" />
                  <Point X="-3.084581787109" Y="27.91571484375" />
                  <Point X="-3.052964599609" Y="27.91294921875" />
                  <Point X="-3.031506591797" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.974971435547" Y="27.965685546875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942792724609" Y="28.0817734375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.091452148438" Y="28.375453125" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-2.968794677734" Y="29.0087890625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.344905273438" Y="29.400990234375" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.093895263672" Y="29.451869140625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951248291016" Y="29.273662109375" />
                  <Point X="-1.891222045898" Y="29.2424140625" />
                  <Point X="-1.856032226562" Y="29.22409375" />
                  <Point X="-1.835125244141" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.751287963867" Y="29.2481484375" />
                  <Point X="-1.714635375977" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.665733886719" Y="29.359029296875" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.667764282227" Y="29.538791015625" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.247614257812" Y="29.824927734375" />
                  <Point X="-0.968083129883" Y="29.903296875" />
                  <Point X="-0.473509643555" Y="29.961181640625" />
                  <Point X="-0.224200210571" Y="29.990359375" />
                  <Point X="-0.163965759277" Y="29.765560546875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282100677" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594055176" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.125871307373" Y="29.57744140625" />
                  <Point X="0.2366484375" Y="29.990869140625" />
                  <Point X="0.616142456055" Y="29.951125" />
                  <Point X="0.860210266113" Y="29.925564453125" />
                  <Point X="1.269388549805" Y="29.826775390625" />
                  <Point X="1.508455078125" Y="29.769056640625" />
                  <Point X="1.775219726562" Y="29.67230078125" />
                  <Point X="1.931044067383" Y="29.61578125" />
                  <Point X="2.188569824219" Y="29.495345703125" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.587489501953" Y="29.2801875" />
                  <Point X="2.732533935547" Y="29.195685546875" />
                  <Point X="2.967165771484" Y="29.028828125" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.773041992188" Y="28.444427734375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491515625" />
                  <Point X="2.211317138672" Y="27.440900390625" />
                  <Point X="2.203382324219" Y="27.411228515625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.207322265625" Y="27.34855859375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682617188" Y="27.3008125" />
                  <Point X="2.245764160156" Y="27.260900390625" />
                  <Point X="2.261640380859" Y="27.23750390625" />
                  <Point X="2.274938964844" Y="27.224205078125" />
                  <Point X="2.314850097656" Y="27.197123046875" />
                  <Point X="2.338247558594" Y="27.18124609375" />
                  <Point X="2.360337158203" Y="27.172978515625" />
                  <Point X="2.404104003906" Y="27.167701171875" />
                  <Point X="2.429761962891" Y="27.164607421875" />
                  <Point X="2.448665527344" Y="27.1659453125" />
                  <Point X="2.499279785156" Y="27.17948046875" />
                  <Point X="2.528951904297" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.092586914062" Y="27.510857421875" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.1165625" Y="27.86144140625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.333401367188" Y="27.52571484375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.003774414062" Y="27.14184375" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.242944091797" Y="26.536310546875" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.199550292969" Y="26.44298046875" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.201918457031" Y="26.33698046875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287953125" />
                  <Point X="3.245942871094" Y="26.241904296875" />
                  <Point X="3.263704101562" Y="26.214908203125" />
                  <Point X="3.280947998047" Y="26.1988203125" />
                  <Point X="3.324851806641" Y="26.17410546875" />
                  <Point X="3.350590087891" Y="26.1596171875" />
                  <Point X="3.368564941406" Y="26.153619140625" />
                  <Point X="3.42792578125" Y="26.145775390625" />
                  <Point X="3.462725585938" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.9997421875" Y="26.2101484375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.902239257812" Y="26.10316015625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.98041015625" Y="25.686625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.560754882812" Y="25.457435546875" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087402344" Y="25.232818359375" />
                  <Point X="3.670767089844" Y="25.199109375" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.622265380859" Y="25.166927734375" />
                  <Point X="3.587273193359" Y="25.122337890625" />
                  <Point X="3.566759521484" Y="25.09619921875" />
                  <Point X="3.556985351562" Y="25.074734375" />
                  <Point X="3.545321289062" Y="25.013828125" />
                  <Point X="3.538483154297" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.550147216797" Y="24.89841015625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758300781" Y="24.8412421875" />
                  <Point X="3.601750488281" Y="24.796654296875" />
                  <Point X="3.622264160156" Y="24.770513671875" />
                  <Point X="3.636575683594" Y="24.75809375" />
                  <Point X="3.694895996094" Y="24.724384765625" />
                  <Point X="3.729085693359" Y="24.70462109375" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.221643066406" Y="24.570869140625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.9690625" Y="24.170439453125" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.895619628906" Y="23.802171875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.36247265625" Y="23.777236328125" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836669922" Y="23.901658203125" />
                  <Point X="3.280374755859" Y="23.876779296875" />
                  <Point X="3.213272460938" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.116260498047" Y="23.76209375" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.054441894531" Y="23.578171875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.119705322266" Y="23.384849609375" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.614344970703" Y="22.9723203125" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.262287109375" Y="22.291962890625" />
                  <Point X="4.204126953125" Y="22.197849609375" />
                  <Point X="4.094913818359" Y="22.042673828125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.599994628906" Y="22.252033203125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340087891" Y="22.7466875" />
                  <Point X="2.601112060547" Y="22.771291015625" />
                  <Point X="2.521249755859" Y="22.785712890625" />
                  <Point X="2.489077392578" Y="22.78075390625" />
                  <Point X="2.375905273438" Y="22.721193359375" />
                  <Point X="2.309559326172" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.229038085938" Y="22.55214453125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.213765625" Y="22.317396484375" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.520616943359" Y="21.72514453125" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.904306640625" Y="20.859078125" />
                  <Point X="2.835297851563" Y="20.809787109375" />
                  <Point X="2.713184814453" Y="20.73074609375" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.329165771484" Y="21.16604296875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.53619152832" Y="22.105912109375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805541992" Y="22.16428125" />
                  <Point X="1.278862670898" Y="22.150759765625" />
                  <Point X="1.19271875" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.051866943359" Y="22.037146484375" />
                  <Point X="0.985348754883" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.93453112793" Y="21.797927734375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.995128479004" Y="21.07251171875" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.059631225586" Y="20.0510625" />
                  <Point X="0.994366333008" Y="20.0367578125" />
                  <Point X="0.881538085938" Y="20.016259765625" />
                  <Point X="0.860200561523" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#180" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.12182548178" Y="4.809783384435" Z="1.6" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="-0.48547767958" Y="5.042121414108" Z="1.6" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.6" />
                  <Point X="-1.267267945514" Y="4.904352393689" Z="1.6" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.6" />
                  <Point X="-1.723492349181" Y="4.563546466482" Z="1.6" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.6" />
                  <Point X="-1.719575618451" Y="4.405344398545" Z="1.6" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.6" />
                  <Point X="-1.776577711464" Y="4.325621982742" Z="1.6" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.6" />
                  <Point X="-1.874288915294" Y="4.318043393839" Z="1.6" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.6" />
                  <Point X="-2.060383405998" Y="4.513586679358" Z="1.6" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.6" />
                  <Point X="-2.375344227412" Y="4.475978733226" Z="1.6" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.6" />
                  <Point X="-3.005372689292" Y="4.079748824131" Z="1.6" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.6" />
                  <Point X="-3.140909300175" Y="3.381734128258" Z="1.6" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.6" />
                  <Point X="-2.998758510329" Y="3.108695688212" Z="1.6" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.6" />
                  <Point X="-3.016482142367" Y="3.03232142552" Z="1.6" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.6" />
                  <Point X="-3.086380777394" Y="2.996806188844" Z="1.6" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.6" />
                  <Point X="-3.552125220039" Y="3.239284648571" Z="1.6" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.6" />
                  <Point X="-3.946599715324" Y="3.1819408311" Z="1.6" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.6" />
                  <Point X="-4.333324351532" Y="2.631098085797" Z="1.6" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.6" />
                  <Point X="-4.011107740154" Y="1.852193307319" Z="1.6" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.6" />
                  <Point X="-3.685571071694" Y="1.589720184646" Z="1.6" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.6" />
                  <Point X="-3.675931615734" Y="1.531712868207" Z="1.6" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.6" />
                  <Point X="-3.71417171101" Y="1.487042266412" Z="1.6" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.6" />
                  <Point X="-4.423412022128" Y="1.56310766117" Z="1.6" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.6" />
                  <Point X="-4.874273940796" Y="1.40163945544" Z="1.6" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.6" />
                  <Point X="-5.005167536751" Y="0.81940577337" Z="1.6" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.6" />
                  <Point X="-4.124929207926" Y="0.19600392362" Z="1.6" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.6" />
                  <Point X="-3.566303862913" Y="0.041950176622" Z="1.6" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.6" />
                  <Point X="-3.545388843081" Y="0.018790935922" Z="1.6" />
                  <Point X="-3.539556741714" Y="0" Z="1.6" />
                  <Point X="-3.542975758054" Y="-0.011016015139" Z="1.6" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.6" />
                  <Point X="-3.55906473221" Y="-0.036925806512" Z="1.6" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.6" />
                  <Point X="-4.511958580846" Y="-0.299708133136" Z="1.6" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.6" />
                  <Point X="-5.031623599367" Y="-0.647334545453" Z="1.6" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.6" />
                  <Point X="-4.932488306951" Y="-1.186097870517" Z="1.6" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.6" />
                  <Point X="-3.820738610517" Y="-1.386062765746" Z="1.6" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.6" />
                  <Point X="-3.209371488788" Y="-1.312623745139" Z="1.6" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.6" />
                  <Point X="-3.195523723765" Y="-1.333443797455" Z="1.6" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.6" />
                  <Point X="-4.021516986678" Y="-1.982277098851" Z="1.6" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.6" />
                  <Point X="-4.39441234474" Y="-2.533573966236" Z="1.6" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.6" />
                  <Point X="-4.08106818919" Y="-3.012429387665" Z="1.6" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.6" />
                  <Point X="-3.049374086757" Y="-2.830618339587" Z="1.6" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.6" />
                  <Point X="-2.566427851596" Y="-2.561902617535" Z="1.6" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.6" />
                  <Point X="-3.024798673581" Y="-3.385704295735" Z="1.6" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.6" />
                  <Point X="-3.148601835024" Y="-3.978753483626" Z="1.6" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.6" />
                  <Point X="-2.728098229187" Y="-4.278042136427" Z="1.6" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.6" />
                  <Point X="-2.309338861726" Y="-4.264771783619" Z="1.6" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.6" />
                  <Point X="-2.130883426369" Y="-4.092748702078" Z="1.6" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.6" />
                  <Point X="-1.853838432508" Y="-3.991583699272" Z="1.6" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.6" />
                  <Point X="-1.572458592888" Y="-4.079978649237" Z="1.6" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.6" />
                  <Point X="-1.403036323273" Y="-4.321400255527" Z="1.6" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.6" />
                  <Point X="-1.395277773702" Y="-4.744137271836" Z="1.6" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.6" />
                  <Point X="-1.303815650159" Y="-4.907620919056" Z="1.6" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.6" />
                  <Point X="-1.006626616484" Y="-4.97708543325" Z="1.6" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="-0.565133108562" Y="-4.071289538209" Z="1.6" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="-0.356576504263" Y="-3.431589307751" Z="1.6" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="-0.1597229524" Y="-3.253811478536" Z="1.6" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.6" />
                  <Point X="0.09363612696" Y="-3.233300566752" Z="1.6" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.6" />
                  <Point X="0.313869355169" Y="-3.370056448017" Z="1.6" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.6" />
                  <Point X="0.669621751839" Y="-4.461246491606" Z="1.6" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.6" />
                  <Point X="0.884318676686" Y="-5.00165497004" Z="1.6" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.6" />
                  <Point X="1.064180575276" Y="-4.966496859822" Z="1.6" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.6" />
                  <Point X="1.038544888482" Y="-3.889682054799" Z="1.6" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.6" />
                  <Point X="0.977234379396" Y="-3.181410553569" Z="1.6" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.6" />
                  <Point X="1.077678176419" Y="-2.970018344478" Z="1.6" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.6" />
                  <Point X="1.277287639797" Y="-2.867748400843" Z="1.6" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.6" />
                  <Point X="1.502996347064" Y="-2.904865779251" Z="1.6" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.6" />
                  <Point X="2.283342016211" Y="-3.833113205387" Z="1.6" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.6" />
                  <Point X="2.73419838367" Y="-4.279948206223" Z="1.6" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.6" />
                  <Point X="2.927212297344" Y="-4.150328405957" Z="1.6" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.6" />
                  <Point X="2.557762308339" Y="-3.218574985375" Z="1.6" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.6" />
                  <Point X="2.25681379697" Y="-2.642436191323" Z="1.6" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.6" />
                  <Point X="2.267128572924" Y="-2.439862152573" Z="1.6" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.6" />
                  <Point X="2.393036225799" Y="-2.291772737043" Z="1.6" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.6" />
                  <Point X="2.586070612189" Y="-2.246634212521" Z="1.6" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.6" />
                  <Point X="3.568838611732" Y="-2.759987191936" Z="1.6" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.6" />
                  <Point X="4.129646110749" Y="-2.954822748458" Z="1.6" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.6" />
                  <Point X="4.298664993916" Y="-2.703041476502" Z="1.6" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.6" />
                  <Point X="3.638626894399" Y="-1.95673195514" Z="1.6" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.6" />
                  <Point X="3.155607436142" Y="-1.556831372897" Z="1.6" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.6" />
                  <Point X="3.098075592046" Y="-1.395130306977" Z="1.6" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.6" />
                  <Point X="3.148550531802" Y="-1.238592256032" Z="1.6" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.6" />
                  <Point X="3.284837843957" Y="-1.140799217167" Z="1.6" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.6" />
                  <Point X="4.349790345386" Y="-1.241054861349" Z="1.6" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.6" />
                  <Point X="4.938210881963" Y="-1.177672980134" Z="1.6" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.6" />
                  <Point X="5.012347944449" Y="-0.805734117949" Z="1.6" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.6" />
                  <Point X="4.228428117927" Y="-0.349553631273" Z="1.6" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.6" />
                  <Point X="3.713762976839" Y="-0.201048364251" Z="1.6" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.6" />
                  <Point X="3.634928880998" Y="-0.141198762586" Z="1.6" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.6" />
                  <Point X="3.593098779145" Y="-0.060905333728" Z="1.6" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.6" />
                  <Point X="3.588272671279" Y="0.035705197472" Z="1.6" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.6" />
                  <Point X="3.620450557402" Y="0.122749975989" Z="1.6" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.6" />
                  <Point X="3.689632437513" Y="0.187100479991" Z="1.6" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.6" />
                  <Point X="4.56753969938" Y="0.440418301495" Z="1.6" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.6" />
                  <Point X="5.023659053977" Y="0.725596316931" Z="1.6" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.6" />
                  <Point X="4.944664425519" Y="1.14626759129" Z="1.6" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.6" />
                  <Point X="3.987059894729" Y="1.291001913915" Z="1.6" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.6" />
                  <Point X="3.428321587009" Y="1.226623304231" Z="1.6" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.6" />
                  <Point X="3.343222392264" Y="1.248956971634" Z="1.6" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.6" />
                  <Point X="3.281557501414" Y="1.300667047221" Z="1.6" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.6" />
                  <Point X="3.244730923139" Y="1.378364465881" Z="1.6" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.6" />
                  <Point X="3.241546828425" Y="1.460793646336" Z="1.6" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.6" />
                  <Point X="3.276471048083" Y="1.537173064049" Z="1.6" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.6" />
                  <Point X="4.028056695786" Y="2.133455527836" Z="1.6" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.6" />
                  <Point X="4.370022653266" Y="2.582882591032" Z="1.6" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.6" />
                  <Point X="4.150991921295" Y="2.921924569588" Z="1.6" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.6" />
                  <Point X="3.061431024315" Y="2.58543808285" Z="1.6" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.6" />
                  <Point X="2.480205824454" Y="2.259064062905" Z="1.6" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.6" />
                  <Point X="2.403933701274" Y="2.248623048876" Z="1.6" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.6" />
                  <Point X="2.336769233266" Y="2.26977672266" Z="1.6" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.6" />
                  <Point X="2.280981877575" Y="2.320255627116" Z="1.6" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.6" />
                  <Point X="2.250806653883" Y="2.385824742559" Z="1.6" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.6" />
                  <Point X="2.253463879752" Y="2.459263751428" Z="1.6" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.6" />
                  <Point X="2.81018727001" Y="3.45070805764" Z="1.6" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.6" />
                  <Point X="2.9899869792" Y="4.100853990124" Z="1.6" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.6" />
                  <Point X="2.60650339859" Y="4.354671302832" Z="1.6" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.6" />
                  <Point X="2.203595521768" Y="4.571916912069" Z="1.6" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.6" />
                  <Point X="1.786112590703" Y="4.750584706406" Z="1.6" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.6" />
                  <Point X="1.274966966355" Y="4.906659892244" Z="1.6" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.6" />
                  <Point X="0.615194406881" Y="5.03213361426" Z="1.6" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.6" />
                  <Point X="0.071419079773" Y="4.621664121516" Z="1.6" />
                  <Point X="0" Y="4.355124473572" Z="1.6" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>