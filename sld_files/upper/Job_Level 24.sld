<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#163" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1811" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.744867858887" Y="20.80986328125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557719848633" Y="21.502861328125" />
                  <Point X="0.542362854004" Y="21.532623046875" />
                  <Point X="0.468152404785" Y="21.639544921875" />
                  <Point X="0.378635223389" Y="21.7685234375" />
                  <Point X="0.356755706787" Y="21.790974609375" />
                  <Point X="0.330500366211" Y="21.810220703125" />
                  <Point X="0.302494873047" Y="21.82433203125" />
                  <Point X="0.187658294678" Y="21.85997265625" />
                  <Point X="0.049135734558" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.00866468811" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.151660507202" Y="21.86732421875" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318185211182" Y="21.81022265625" />
                  <Point X="-0.34444039917" Y="21.790978515625" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.440534057617" Y="21.661599609375" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.770005432129" Y="20.670099609375" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-0.919738342285" Y="20.123669921875" />
                  <Point X="-1.079358276367" Y="20.154654296875" />
                  <Point X="-1.209142578125" Y="20.188044921875" />
                  <Point X="-1.24641809082" Y="20.197634765625" />
                  <Point X="-1.236819458008" Y="20.270544921875" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.243942993164" Y="20.621697265625" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.429371582031" Y="20.961515625" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643027587891" Y="21.109033203125" />
                  <Point X="-1.783350585938" Y="21.11823046875" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.159582275391" Y="21.027072265625" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790039062" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.458062988281" Y="20.740291015625" />
                  <Point X="-2.480147460938" Y="20.711509765625" />
                  <Point X="-2.567770751953" Y="20.765763671875" />
                  <Point X="-2.80171484375" Y="20.9106171875" />
                  <Point X="-2.981444580078" Y="21.049001953125" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-2.761180908203" Y="21.738951171875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414561279297" Y="22.444427734375" />
                  <Point X="-2.428779785156" Y="22.4732578125" />
                  <Point X="-2.446808105469" Y="22.498416015625" />
                  <Point X="-2.464156738281" Y="22.515763671875" />
                  <Point X="-2.489316894531" Y="22.533791015625" />
                  <Point X="-2.518145263672" Y="22.548005859375" />
                  <Point X="-2.54776171875" Y="22.55698828125" />
                  <Point X="-2.578694091797" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.344155029297" Y="22.13178515625" />
                  <Point X="-3.818022949219" Y="21.858197265625" />
                  <Point X="-3.898036132812" Y="21.963318359375" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.211714355469" Y="22.42220703125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.696329345703" Y="23.048474609375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556640625" Y="23.672013671875" />
                  <Point X="-3.052558105469" Y="23.701759765625" />
                  <Point X="-3.068640869141" Y="23.727744140625" />
                  <Point X="-3.089474365234" Y="23.751701171875" />
                  <Point X="-3.112974853516" Y="23.771234375" />
                  <Point X="-3.139457519531" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608886719" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.121888671875" Y="23.688451171875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.761791015625" Y="23.72434765625" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.868168945312" Y="24.245708984375" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.205401855469" Y="24.59938671875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.510624755859" Y="24.788703125" />
                  <Point X="-3.482478027344" Y="24.804654296875" />
                  <Point X="-3.469218505859" Y="24.81373046875" />
                  <Point X="-3.442225830078" Y="24.835779296875" />
                  <Point X="-3.426014404297" Y="24.85276953125" />
                  <Point X="-3.414465820312" Y="24.873216796875" />
                  <Point X="-3.402847167969" Y="24.90146484375" />
                  <Point X="-3.398219970703" Y="24.915890625" />
                  <Point X="-3.390915039062" Y="24.9470078125" />
                  <Point X="-3.388401367188" Y="24.9683828125" />
                  <Point X="-3.390763916016" Y="24.989775390625" />
                  <Point X="-3.397204101562" Y="25.018107421875" />
                  <Point X="-3.401703857422" Y="25.032501953125" />
                  <Point X="-3.414187255859" Y="25.063537109375" />
                  <Point X="-3.425576171875" Y="25.08407421875" />
                  <Point X="-3.441654785156" Y="25.101189453125" />
                  <Point X="-3.466052734375" Y="25.1214375" />
                  <Point X="-3.479215576172" Y="25.1306015625" />
                  <Point X="-3.50995703125" Y="25.148353515625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.344116210938" Y="25.375220703125" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.871074707031" Y="25.66214453125" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.755862304688" Y="26.23022265625" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.247749511719" Y="26.363259765625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985107422" Y="26.299341796875" />
                  <Point X="-3.723422851562" Y="26.301228515625" />
                  <Point X="-3.703139892578" Y="26.305263671875" />
                  <Point X="-3.675298339844" Y="26.314041015625" />
                  <Point X="-3.641713867188" Y="26.324630859375" />
                  <Point X="-3.622775878906" Y="26.332962890625" />
                  <Point X="-3.604032226562" Y="26.34378515625" />
                  <Point X="-3.587352783203" Y="26.356015625" />
                  <Point X="-3.573715576172" Y="26.37156640625" />
                  <Point X="-3.561301513672" Y="26.389294921875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.540179931641" Y="26.434400390625" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.545529785156" Y="26.615271484375" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.067465576172" Y="27.051650390625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.262172363281" Y="27.42353125" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.899368652344" Y="27.96731640625" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.502739013672" Y="28.015615234375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167086181641" Y="27.82983203125" />
                  <Point X="-3.146794433594" Y="27.825794921875" />
                  <Point X="-3.108018554688" Y="27.82240234375" />
                  <Point X="-3.061245117188" Y="27.818310546875" />
                  <Point X="-3.040560302734" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.918554443359" Y="27.887751953125" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846828369141" Y="28.07489453125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.075996826172" Y="28.53868359375" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-3.015898681641" Y="28.85296484375" />
                  <Point X="-2.700620605469" Y="29.0946875" />
                  <Point X="-2.414322753906" Y="29.25374609375" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.133834960938" Y="29.347865234375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028893066406" Y="29.21480078125" />
                  <Point X="-2.012314086914" Y="29.200888671875" />
                  <Point X="-1.995114501953" Y="29.189396484375" />
                  <Point X="-1.951957275391" Y="29.1669296875" />
                  <Point X="-1.89989831543" Y="29.139828125" />
                  <Point X="-1.880625976562" Y="29.13233203125" />
                  <Point X="-1.859719604492" Y="29.126728515625" />
                  <Point X="-1.83926965332" Y="29.123580078125" />
                  <Point X="-1.818622924805" Y="29.12493359375" />
                  <Point X="-1.797307495117" Y="29.128693359375" />
                  <Point X="-1.777451782227" Y="29.134482421875" />
                  <Point X="-1.732500610352" Y="29.153103515625" />
                  <Point X="-1.678277832031" Y="29.1755625" />
                  <Point X="-1.660147827148" Y="29.185509765625" />
                  <Point X="-1.642417602539" Y="29.197923828125" />
                  <Point X="-1.626865356445" Y="29.2115625" />
                  <Point X="-1.614633789062" Y="29.2282421875" />
                  <Point X="-1.603811523438" Y="29.246986328125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.580849487305" Y="29.31232421875" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.581173217773" Y="29.608892578125" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.357775878906" Y="29.69537890625" />
                  <Point X="-0.949623352051" Y="29.80980859375" />
                  <Point X="-0.602554382324" Y="29.8504296875" />
                  <Point X="-0.294711120605" Y="29.886458984375" />
                  <Point X="-0.221492446899" Y="29.613203125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082114006042" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155906677" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425956726" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.251870498657" Y="29.680625" />
                  <Point X="0.307419647217" Y="29.8879375" />
                  <Point X="0.487668548584" Y="29.869060546875" />
                  <Point X="0.844031066895" Y="29.831740234375" />
                  <Point X="1.13119519043" Y="29.76241015625" />
                  <Point X="1.481039550781" Y="29.6779453125" />
                  <Point X="1.667126220703" Y="29.610451171875" />
                  <Point X="1.894645385742" Y="29.527927734375" />
                  <Point X="2.075380859375" Y="29.443404296875" />
                  <Point X="2.294556884766" Y="29.34090234375" />
                  <Point X="2.469201904297" Y="29.239154296875" />
                  <Point X="2.680975097656" Y="29.115775390625" />
                  <Point X="2.845649414062" Y="28.99866796875" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.538118896484" Y="28.227529296875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.123345458984" Y="27.4796640625" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.111522460938" Y="27.349484375" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.159541748047" Y="27.218775390625" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.19446484375" Y="27.170328125" />
                  <Point X="2.22159765625" Y="27.14559375" />
                  <Point X="2.250292480469" Y="27.126123046875" />
                  <Point X="2.28490625" Y="27.102634765625" />
                  <Point X="2.3049453125" Y="27.0922734375" />
                  <Point X="2.327033447266" Y="27.084005859375" />
                  <Point X="2.348966796875" Y="27.078662109375" />
                  <Point X="2.380434082031" Y="27.074869140625" />
                  <Point X="2.418391601562" Y="27.070291015625" />
                  <Point X="2.436466796875" Y="27.06984375" />
                  <Point X="2.473208984375" Y="27.074169921875" />
                  <Point X="2.509599365234" Y="27.08390234375" />
                  <Point X="2.553495361328" Y="27.095640625" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.404485595703" Y="27.581236328125" />
                  <Point X="3.967325683594" Y="27.90619140625" />
                  <Point X="3.997652832031" Y="27.86404296875" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.21507421875" Y="27.5377578125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.744963378906" Y="27.06299609375" />
                  <Point X="3.230783691406" Y="26.668451171875" />
                  <Point X="3.221422119141" Y="26.66023828125" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.177784179688" Y="26.6074609375" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.13660546875" Y="26.55091015625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.111874023438" Y="26.482201171875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.105748046875" Y="26.332955078125" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.235740234375" />
                  <Point X="3.158065185547" Y="26.2026328125" />
                  <Point X="3.184340576172" Y="26.1626953125" />
                  <Point X="3.198893554688" Y="26.145451171875" />
                  <Point X="3.216137451172" Y="26.129361328125" />
                  <Point X="3.234345703125" Y="26.116037109375" />
                  <Point X="3.265911376953" Y="26.098267578125" />
                  <Point X="3.303987792969" Y="26.076833984375" />
                  <Point X="3.320521972656" Y="26.069501953125" />
                  <Point X="3.356119140625" Y="26.0594375" />
                  <Point X="3.398797851562" Y="26.053796875" />
                  <Point X="3.450279785156" Y="26.046994140625" />
                  <Point X="3.462698974609" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.263303222656" Y="26.14902734375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.791983398438" Y="26.1544296875" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.874865722656" Y="25.747" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.305840820313" Y="25.487482421875" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704786865234" Y="25.325583984375" />
                  <Point X="3.681547119141" Y="25.31506640625" />
                  <Point X="3.639616455078" Y="25.290830078125" />
                  <Point X="3.589037109375" Y="25.261595703125" />
                  <Point X="3.574315673828" Y="25.251099609375" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.522371826172" Y="25.193517578125" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.455294677734" Y="25.048814453125" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.453565185547" Y="24.89765625" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.517182861328" Y="24.750533203125" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559999511719" Y="24.69876171875" />
                  <Point X="3.589035888672" Y="24.67584375" />
                  <Point X="3.630966552734" Y="24.651607421875" />
                  <Point X="3.681545898438" Y="24.62237109375" />
                  <Point X="3.692708984375" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.427381347656" Y="24.417388671875" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.885147460938" Y="24.251083984375" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.817958984375" Y="23.88885546875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.107758789063" Y="23.90658984375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.292364013672" Y="23.976603515625" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112397460938" Y="23.9060390625" />
                  <Point X="3.062655273438" Y="23.84621484375" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.962628173828" Y="23.61716015625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.021993896484" Y="23.3611640625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.770256591797" Y="22.732939453125" />
                  <Point X="4.213122070312" Y="22.3931171875" />
                  <Point X="4.209729003906" Y="22.387626953125" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.048159667969" Y="22.1413046875" />
                  <Point X="4.02898046875" Y="22.1140546875" />
                  <Point X="3.409593994141" Y="22.47165625" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786128417969" Y="22.82998828125" />
                  <Point X="2.754224853516" Y="22.840173828125" />
                  <Point X="2.656280517578" Y="22.85786328125" />
                  <Point X="2.538134521484" Y="22.87919921875" />
                  <Point X="2.506783447266" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444833984375" Y="22.864822265625" />
                  <Point X="2.363466308594" Y="22.822" />
                  <Point X="2.265315917969" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.161708740234" Y="22.628193359375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.113363769531" Y="22.338796875" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.575696533203" Y="21.439744140625" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.781849609375" Y="20.88835546875" />
                  <Point X="2.701763916016" Y="20.836517578125" />
                  <Point X="2.222994384766" Y="21.460462890625" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923461914" Y="22.099443359375" />
                  <Point X="1.625323974609" Y="22.161546875" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.311452758789" Y="22.24916015625" />
                  <Point X="1.184013916016" Y="22.23743359375" />
                  <Point X="1.156362792969" Y="22.2306015625" />
                  <Point X="1.128976928711" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.2045390625" />
                  <Point X="1.023016601562" Y="22.136708984375" />
                  <Point X="0.92461151123" Y="22.05488671875" />
                  <Point X="0.90414074707" Y="22.031134765625" />
                  <Point X="0.887248779297" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.851232849121" Y="21.861970703125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.939865844727" Y="20.764451171875" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="0.975713684082" Y="20.129923828125" />
                  <Point X="0.929315612793" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058460327148" Y="20.24737109375" />
                  <Point X="-1.141246337891" Y="20.268669921875" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.150768188477" Y="20.64023046875" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575195312" Y="20.905138671875" />
                  <Point X="-1.250209350586" Y="20.9290625" />
                  <Point X="-1.261006591797" Y="20.94021875" />
                  <Point X="-1.366733398438" Y="21.032939453125" />
                  <Point X="-1.494267333984" Y="21.144783203125" />
                  <Point X="-1.506735839844" Y="21.15403125" />
                  <Point X="-1.53301953125" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531860352" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.62145715332" Y="21.201552734375" />
                  <Point X="-1.636814331055" Y="21.203830078125" />
                  <Point X="-1.777137329102" Y="21.21302734375" />
                  <Point X="-1.946403442383" Y="21.22412109375" />
                  <Point X="-1.961927856445" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095437011719" Y="21.184189453125" />
                  <Point X="-2.212361572266" Y="21.1060625" />
                  <Point X="-2.353403320312" Y="21.011822265625" />
                  <Point X="-2.359684082031" Y="21.0072421875" />
                  <Point X="-2.380450439453" Y="20.989861328125" />
                  <Point X="-2.402763183594" Y="20.96722265625" />
                  <Point X="-2.410471679688" Y="20.958369140625" />
                  <Point X="-2.503201171875" Y="20.837521484375" />
                  <Point X="-2.517759765625" Y="20.84653515625" />
                  <Point X="-2.747591308594" Y="20.988841796875" />
                  <Point X="-2.923487548828" Y="21.124275390625" />
                  <Point X="-2.980862304688" Y="21.168453125" />
                  <Point X="-2.678908447266" Y="21.691451171875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314667480469" Y="22.442390625" />
                  <Point X="-2.323652832031" Y="22.4720078125" />
                  <Point X="-2.329359619141" Y="22.486447265625" />
                  <Point X="-2.343578125" Y="22.51527734375" />
                  <Point X="-2.351559570312" Y="22.52859375" />
                  <Point X="-2.369587890625" Y="22.553751953125" />
                  <Point X="-2.379634765625" Y="22.56559375" />
                  <Point X="-2.396983398438" Y="22.58294140625" />
                  <Point X="-2.408825683594" Y="22.592986328125" />
                  <Point X="-2.433985839844" Y="22.611013671875" />
                  <Point X="-2.447303710938" Y="22.61899609375" />
                  <Point X="-2.476132080078" Y="22.6332109375" />
                  <Point X="-2.490572753906" Y="22.638916015625" />
                  <Point X="-2.520189208984" Y="22.6478984375" />
                  <Point X="-2.550873291016" Y="22.6519375" />
                  <Point X="-2.581805664062" Y="22.650923828125" />
                  <Point X="-2.597229736328" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.391655029297" Y="22.214056640625" />
                  <Point X="-3.793086914062" Y="21.9822890625" />
                  <Point X="-3.822442626953" Y="22.020857421875" />
                  <Point X="-4.004021972656" Y="22.259416015625" />
                  <Point X="-4.130121582031" Y="22.470865234375" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.638497070312" Y="22.97310546875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950786865234" Y="23.678623046875" />
                  <Point X="-2.953083740234" Y="23.693779296875" />
                  <Point X="-2.960085205078" Y="23.723525390625" />
                  <Point X="-2.971779052734" Y="23.7517578125" />
                  <Point X="-2.987861816406" Y="23.7777421875" />
                  <Point X="-2.996955322266" Y="23.790083984375" />
                  <Point X="-3.017788818359" Y="23.814041015625" />
                  <Point X="-3.028749511719" Y="23.824759765625" />
                  <Point X="-3.05225" Y="23.84429296875" />
                  <Point X="-3.064789794922" Y="23.853107421875" />
                  <Point X="-3.091272460938" Y="23.868693359375" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.134701171875" Y="23.886744140625" />
                  <Point X="-3.149801757812" Y="23.891142578125" />
                  <Point X="-3.181688476562" Y="23.897623046875" />
                  <Point X="-3.197305175781" Y="23.89946875" />
                  <Point X="-3.228625488281" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.134288574219" Y="23.782638671875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.66974609375" Y="23.747859375" />
                  <Point X="-4.740763183594" Y="24.02588671875" />
                  <Point X="-4.774125976562" Y="24.259158203125" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.180813964844" Y="24.507623046875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.496870361328" Y="24.691677734375" />
                  <Point X="-3.474619140625" Y="24.700791015625" />
                  <Point X="-3.463785644531" Y="24.706052734375" />
                  <Point X="-3.435638916016" Y="24.72200390625" />
                  <Point X="-3.428817382812" Y="24.72626171875" />
                  <Point X="-3.409119873047" Y="24.74015625" />
                  <Point X="-3.382127197266" Y="24.762205078125" />
                  <Point X="-3.373493896484" Y="24.770197265625" />
                  <Point X="-3.357282470703" Y="24.7871875" />
                  <Point X="-3.343296142578" Y="24.80605078125" />
                  <Point X="-3.331747558594" Y="24.826498046875" />
                  <Point X="-3.326607177734" Y="24.837080078125" />
                  <Point X="-3.314988525391" Y="24.865328125" />
                  <Point X="-3.31238671875" Y="24.87244921875" />
                  <Point X="-3.305734130859" Y="24.8941796875" />
                  <Point X="-3.298429199219" Y="24.925296875" />
                  <Point X="-3.296565185547" Y="24.935912109375" />
                  <Point X="-3.294051513672" Y="24.957287109375" />
                  <Point X="-3.293975341797" Y="24.978810546875" />
                  <Point X="-3.296337890625" Y="25.000203125" />
                  <Point X="-3.298126953125" Y="25.01083203125" />
                  <Point X="-3.304567138672" Y="25.0391640625" />
                  <Point X="-3.30653125" Y="25.046451171875" />
                  <Point X="-3.313566650391" Y="25.067953125" />
                  <Point X="-3.326050048828" Y="25.09898828125" />
                  <Point X="-3.331106933594" Y="25.109609375" />
                  <Point X="-3.342495849609" Y="25.130146484375" />
                  <Point X="-3.356336914062" Y="25.149119140625" />
                  <Point X="-3.372415527344" Y="25.166234375" />
                  <Point X="-3.380985107422" Y="25.17429296875" />
                  <Point X="-3.405383056641" Y="25.194541015625" />
                  <Point X="-3.411772460938" Y="25.19940234375" />
                  <Point X="-3.431708740234" Y="25.212869140625" />
                  <Point X="-3.462450195312" Y="25.23062109375" />
                  <Point X="-3.473593017578" Y="25.236119140625" />
                  <Point X="-3.496511962891" Y="25.245615234375" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.319528320312" Y="25.466984375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.777098144531" Y="25.64823828125" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.664169433594" Y="26.205375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.260149414062" Y="26.269072265625" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057128906" Y="26.204365234375" />
                  <Point X="-3.736704101562" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704886474609" Y="26.2080546875" />
                  <Point X="-3.684603515625" Y="26.21208984375" />
                  <Point X="-3.674575927734" Y="26.21466015625" />
                  <Point X="-3.646734375" Y="26.2234375" />
                  <Point X="-3.613149902344" Y="26.23402734375" />
                  <Point X="-3.603456298828" Y="26.237673828125" />
                  <Point X="-3.584518310547" Y="26.246005859375" />
                  <Point X="-3.575273925781" Y="26.25069140625" />
                  <Point X="-3.556530273438" Y="26.261513671875" />
                  <Point X="-3.547855957031" Y="26.267173828125" />
                  <Point X="-3.531176513672" Y="26.279404296875" />
                  <Point X="-3.515927001953" Y="26.29337890625" />
                  <Point X="-3.502289794922" Y="26.3089296875" />
                  <Point X="-3.495896972656" Y="26.317076171875" />
                  <Point X="-3.483482910156" Y="26.3348046875" />
                  <Point X="-3.478014160156" Y="26.34359765625" />
                  <Point X="-3.468064208984" Y="26.361732421875" />
                  <Point X="-3.463583007813" Y="26.37107421875" />
                  <Point X="-3.452411376953" Y="26.398044921875" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.5225390625" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436012695312" Y="26.60453125" />
                  <Point X="-3.443509277344" Y="26.62380859375" />
                  <Point X="-3.447783935547" Y="26.6332421875" />
                  <Point X="-3.461263671875" Y="26.65913671875" />
                  <Point X="-3.477523681641" Y="26.69037109375" />
                  <Point X="-3.482799804688" Y="26.69928515625" />
                  <Point X="-3.494291748047" Y="26.716484375" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.009633300781" Y="27.12701953125" />
                  <Point X="-4.227614746094" Y="27.29428125" />
                  <Point X="-4.180125976563" Y="27.375642578125" />
                  <Point X="-4.002294921875" Y="27.68030859375" />
                  <Point X="-3.824387939453" Y="27.908982421875" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.550239013672" Y="27.93334375" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244923583984" Y="27.757720703125" />
                  <Point X="-3.225995361328" Y="27.749390625" />
                  <Point X="-3.216300537109" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.73923046875" />
                  <Point X="-3.185623535156" Y="27.736658203125" />
                  <Point X="-3.165331787109" Y="27.73262109375" />
                  <Point X="-3.155074462891" Y="27.73115625" />
                  <Point X="-3.116298583984" Y="27.727763671875" />
                  <Point X="-3.069525146484" Y="27.723671875" />
                  <Point X="-3.059173583984" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155517578" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902746826172" Y="27.773193359375" />
                  <Point X="-2.886614990234" Y="27.786140625" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.851379394531" Y="27.820576171875" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752189941406" Y="28.083173828125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.993724609375" Y="28.58618359375" />
                  <Point X="-3.059387451172" Y="28.6999140625" />
                  <Point X="-2.958096191406" Y="28.77757421875" />
                  <Point X="-2.648374755859" Y="29.015037109375" />
                  <Point X="-2.368185791016" Y="29.170701171875" />
                  <Point X="-2.192524414062" Y="29.268296875" />
                  <Point X="-2.118564697266" Y="29.17191015625" />
                  <Point X="-2.111822021484" Y="29.16405078125" />
                  <Point X="-2.097519775391" Y="29.149109375" />
                  <Point X="-2.089959716797" Y="29.14202734375" />
                  <Point X="-2.073380615234" Y="29.128115234375" />
                  <Point X="-2.065092529297" Y="29.1218984375" />
                  <Point X="-2.047893066406" Y="29.11040625" />
                  <Point X="-2.038981445312" Y="29.105130859375" />
                  <Point X="-1.99582421875" Y="29.0826640625" />
                  <Point X="-1.943765258789" Y="29.0555625" />
                  <Point X="-1.9343359375" Y="29.0512890625" />
                  <Point X="-1.915063598633" Y="29.04379296875" />
                  <Point X="-1.905220581055" Y="29.0405703125" />
                  <Point X="-1.884314208984" Y="29.034966796875" />
                  <Point X="-1.874175292969" Y="29.032833984375" />
                  <Point X="-1.853725341797" Y="29.029685546875" />
                  <Point X="-1.833055053711" Y="29.028783203125" />
                  <Point X="-1.812408447266" Y="29.03013671875" />
                  <Point X="-1.802120849609" Y="29.031376953125" />
                  <Point X="-1.780805419922" Y="29.03513671875" />
                  <Point X="-1.770716918945" Y="29.037490234375" />
                  <Point X="-1.750861206055" Y="29.043279296875" />
                  <Point X="-1.741093994141" Y="29.04671484375" />
                  <Point X="-1.696142822266" Y="29.0653359375" />
                  <Point X="-1.641920166016" Y="29.087794921875" />
                  <Point X="-1.632581054688" Y="29.092275390625" />
                  <Point X="-1.614451049805" Y="29.10222265625" />
                  <Point X="-1.605660400391" Y="29.107689453125" />
                  <Point X="-1.587930175781" Y="29.120103515625" />
                  <Point X="-1.579780639648" Y="29.126498046875" />
                  <Point X="-1.564228271484" Y="29.14013671875" />
                  <Point X="-1.550256469727" Y="29.1553828125" />
                  <Point X="-1.538024780273" Y="29.1720625" />
                  <Point X="-1.532361938477" Y="29.180740234375" />
                  <Point X="-1.521539672852" Y="29.199484375" />
                  <Point X="-1.516856201172" Y="29.2087265625" />
                  <Point X="-1.508524780273" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.490246337891" Y="29.2837578125" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.332129882812" Y="29.60390625" />
                  <Point X="-0.931164245605" Y="29.7163203125" />
                  <Point X="-0.591510986328" Y="29.75607421875" />
                  <Point X="-0.365222351074" Y="29.78255859375" />
                  <Point X="-0.313255462646" Y="29.588615234375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896308899" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.343633392334" Y="29.656037109375" />
                  <Point X="0.378190765381" Y="29.7850078125" />
                  <Point X="0.477773590088" Y="29.774578125" />
                  <Point X="0.827851379395" Y="29.737916015625" />
                  <Point X="1.108899902344" Y="29.6700625" />
                  <Point X="1.453622436523" Y="29.586833984375" />
                  <Point X="1.63473425293" Y="29.52114453125" />
                  <Point X="1.858248046875" Y="29.44007421875" />
                  <Point X="2.035136230469" Y="29.357349609375" />
                  <Point X="2.250428710938" Y="29.2566640625" />
                  <Point X="2.421379150391" Y="29.157068359375" />
                  <Point X="2.629431152344" Y="29.035857421875" />
                  <Point X="2.790592773438" Y="28.921248046875" />
                  <Point X="2.817778808594" Y="28.901916015625" />
                  <Point X="2.455846435547" Y="28.275029296875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.03157019043" Y="27.504205078125" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.017205688477" Y="27.338111328125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045317871094" Y="27.223888671875" />
                  <Point X="2.055680175781" Y="27.20384375" />
                  <Point X="2.061459472656" Y="27.19412890625" />
                  <Point X="2.080930419922" Y="27.16543359375" />
                  <Point X="2.104417236328" Y="27.1308203125" />
                  <Point X="2.10980859375" Y="27.1236328125" />
                  <Point X="2.130464355469" Y="27.10012109375" />
                  <Point X="2.157597167969" Y="27.07538671875" />
                  <Point X="2.168256591797" Y="27.066982421875" />
                  <Point X="2.196951416016" Y="27.04751171875" />
                  <Point X="2.231565185547" Y="27.0240234375" />
                  <Point X="2.2412734375" Y="27.018248046875" />
                  <Point X="2.2613125" Y="27.00788671875" />
                  <Point X="2.271643310547" Y="27.00330078125" />
                  <Point X="2.293731445312" Y="26.995033203125" />
                  <Point X="2.304545898438" Y="26.991705078125" />
                  <Point X="2.326479248047" Y="26.986361328125" />
                  <Point X="2.337598144531" Y="26.984345703125" />
                  <Point X="2.369065429688" Y="26.980552734375" />
                  <Point X="2.407022949219" Y="26.975974609375" />
                  <Point X="2.416041503906" Y="26.9753203125" />
                  <Point X="2.447575683594" Y="26.97549609375" />
                  <Point X="2.484317871094" Y="26.979822265625" />
                  <Point X="2.497753662109" Y="26.98239453125" />
                  <Point X="2.534144042969" Y="26.992126953125" />
                  <Point X="2.578040039062" Y="27.003865234375" />
                  <Point X="2.584001464844" Y="27.005671875" />
                  <Point X="2.604414794922" Y="27.0130703125" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.451985595703" Y="27.49896484375" />
                  <Point X="3.940403808594" Y="27.780953125" />
                  <Point X="4.043953369141" Y="27.63704296875" />
                  <Point X="4.133797363281" Y="27.48857421875" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="3.687131103516" Y="27.138365234375" />
                  <Point X="3.172951416016" Y="26.7438203125" />
                  <Point X="3.1681328125" Y="26.739865234375" />
                  <Point X="3.152115478516" Y="26.725212890625" />
                  <Point X="3.134667724609" Y="26.7066015625" />
                  <Point X="3.128577636719" Y="26.699421875" />
                  <Point X="3.102387451172" Y="26.665255859375" />
                  <Point X="3.070795410156" Y="26.624041015625" />
                  <Point X="3.065636230469" Y="26.6166015625" />
                  <Point X="3.049738769531" Y="26.58937109375" />
                  <Point X="3.034763183594" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.020384277344" Y="26.507787109375" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.012708007812" Y="26.3137578125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034684814453" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.19537109375" />
                  <Point X="3.056919433594" Y="26.183525390625" />
                  <Point X="3.078701660156" Y="26.15041796875" />
                  <Point X="3.104977050781" Y="26.11048046875" />
                  <Point X="3.111739501953" Y="26.101423828125" />
                  <Point X="3.126292480469" Y="26.0841796875" />
                  <Point X="3.134083007812" Y="26.0759921875" />
                  <Point X="3.151326904297" Y="26.05990234375" />
                  <Point X="3.160035888672" Y="26.0526953125" />
                  <Point X="3.178244140625" Y="26.03937109375" />
                  <Point X="3.187743408203" Y="26.03325390625" />
                  <Point X="3.219309082031" Y="26.015484375" />
                  <Point X="3.257385498047" Y="25.99405078125" />
                  <Point X="3.265477050781" Y="25.989990234375" />
                  <Point X="3.294675537109" Y="25.9780859375" />
                  <Point X="3.330272705078" Y="25.968021484375" />
                  <Point X="3.343671630859" Y="25.965255859375" />
                  <Point X="3.386350341797" Y="25.959615234375" />
                  <Point X="3.437832275391" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465709228516" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.275703125" Y="26.05483984375" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.780996582031" Y="25.732384765625" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.281252929687" Y="25.57924609375" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686024658203" Y="25.419541015625" />
                  <Point X="3.665617431641" Y="25.4121328125" />
                  <Point X="3.642377685547" Y="25.401615234375" />
                  <Point X="3.634006347656" Y="25.397314453125" />
                  <Point X="3.592075683594" Y="25.373078125" />
                  <Point X="3.541496337891" Y="25.34384375" />
                  <Point X="3.533886474609" Y="25.338947265625" />
                  <Point X="3.508780273438" Y="25.319875" />
                  <Point X="3.481994873047" Y="25.2943515625" />
                  <Point X="3.472795654297" Y="25.284224609375" />
                  <Point X="3.447637207031" Y="25.252166015625" />
                  <Point X="3.417289794922" Y="25.21349609375" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130615234" Y="25.1849296875" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.361990478516" Y="25.06668359375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.360260986328" Y="24.879787109375" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.816013671875" />
                  <Point X="3.380004394531" Y="24.794515625" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417289794922" Y="24.723943359375" />
                  <Point X="3.442448242188" Y="24.691884765625" />
                  <Point X="3.472795654297" Y="24.65321484375" />
                  <Point X="3.478715087891" Y="24.646369140625" />
                  <Point X="3.501142089844" Y="24.62419140625" />
                  <Point X="3.530178466797" Y="24.6012734375" />
                  <Point X="3.541495117188" Y="24.593595703125" />
                  <Point X="3.58342578125" Y="24.569359375" />
                  <Point X="3.634005126953" Y="24.540123046875" />
                  <Point X="3.639487304688" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026855469" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.402793457031" Y="24.325625" />
                  <Point X="4.784876953125" Y="24.22324609375" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727802246094" Y="23.920779296875" />
                  <Point X="4.120158691406" Y="24.00077734375" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354482177734" Y="24.087322265625" />
                  <Point X="3.272187011719" Y="24.069435546875" />
                  <Point X="3.17291796875" Y="24.047859375" />
                  <Point X="3.157874267578" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123046875" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.039349609375" Y="23.966775390625" />
                  <Point X="2.989607421875" Y="23.906951171875" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921327148438" Y="23.82315234375" />
                  <Point X="2.906606689453" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.868027832031" Y="23.625865234375" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.42333203125" />
                  <Point X="2.889158203125" Y="23.39451953125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.942084228516" Y="23.3097890625" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001742675781" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.712424316406" Y="22.6575703125" />
                  <Point X="4.087170410156" Y="22.370017578125" />
                  <Point X="4.04549609375" Y="22.302580078125" />
                  <Point X="4.001274169922" Y="22.239748046875" />
                  <Point X="3.45709375" Y="22.5539296875" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841198730469" Y="22.909109375" />
                  <Point X="2.815021484375" Y="22.92048828125" />
                  <Point X="2.783117919922" Y="22.930673828125" />
                  <Point X="2.771109375" Y="22.933662109375" />
                  <Point X="2.673165039062" Y="22.9513515625" />
                  <Point X="2.555019042969" Y="22.9726875" />
                  <Point X="2.539359619141" Y="22.97419140625" />
                  <Point X="2.508008544922" Y="22.974595703125" />
                  <Point X="2.492316894531" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415069091797" Y="22.9550390625" />
                  <Point X="2.400590332031" Y="22.948890625" />
                  <Point X="2.31922265625" Y="22.906068359375" />
                  <Point X="2.221072265625" Y="22.854412109375" />
                  <Point X="2.208969726562" Y="22.846830078125" />
                  <Point X="2.186039306641" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940429687" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.077640869141" Y="22.6724375" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.019876098633" Y="22.3219140625" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.493424072266" Y="21.392244140625" />
                  <Point X="2.735893798828" Y="20.972275390625" />
                  <Point X="2.723752685547" Y="20.963916015625" />
                  <Point X="2.298363037109" Y="21.518294921875" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808834228516" Y="22.15037109375" />
                  <Point X="1.783250854492" Y="22.17199609375" />
                  <Point X="1.773297729492" Y="22.179353515625" />
                  <Point X="1.676698242188" Y="22.24145703125" />
                  <Point X="1.560174438477" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470703125" Y="22.336126953125" />
                  <Point X="1.502546875" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384765625" Y="22.3513046875" />
                  <Point X="1.424119995117" Y="22.35362109375" />
                  <Point X="1.408396484375" Y="22.35348046875" />
                  <Point X="1.302748535156" Y="22.343759765625" />
                  <Point X="1.175309814453" Y="22.332033203125" />
                  <Point X="1.1612265625" Y="22.32966015625" />
                  <Point X="1.133575439453" Y="22.322828125" />
                  <Point X="1.120007446289" Y="22.318369140625" />
                  <Point X="1.092621582031" Y="22.307025390625" />
                  <Point X="1.079880004883" Y="22.300587890625" />
                  <Point X="1.055498291016" Y="22.285869140625" />
                  <Point X="1.043858154297" Y="22.277587890625" />
                  <Point X="0.96227947998" Y="22.2097578125" />
                  <Point X="0.863874328613" Y="22.127935546875" />
                  <Point X="0.852650024414" Y="22.11690625" />
                  <Point X="0.832179138184" Y="22.093154296875" />
                  <Point X="0.822932739258" Y="22.080431640625" />
                  <Point X="0.806040710449" Y="22.05260546875" />
                  <Point X="0.799019287109" Y="22.038529296875" />
                  <Point X="0.787394775391" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.758400390625" Y="21.8821484375" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091247559" Y="20.847662109375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605224609" Y="21.51987890625" />
                  <Point X="0.642143371582" Y="21.546423828125" />
                  <Point X="0.626786315918" Y="21.576185546875" />
                  <Point X="0.620406982422" Y="21.586791015625" />
                  <Point X="0.546196594238" Y="21.693712890625" />
                  <Point X="0.456679382324" Y="21.82269140625" />
                  <Point X="0.44667098999" Y="21.834826171875" />
                  <Point X="0.424791442871" Y="21.85727734375" />
                  <Point X="0.412920349121" Y="21.86759375" />
                  <Point X="0.38666506958" Y="21.88683984375" />
                  <Point X="0.373248596191" Y="21.89505859375" />
                  <Point X="0.345243133545" Y="21.909169921875" />
                  <Point X="0.330654022217" Y="21.9150625" />
                  <Point X="0.215817398071" Y="21.950703125" />
                  <Point X="0.077294914246" Y="21.9936953125" />
                  <Point X="0.063376972198" Y="21.996890625" />
                  <Point X="0.035217987061" Y="22.00116015625" />
                  <Point X="0.020976644516" Y="22.002234375" />
                  <Point X="-0.008664708138" Y="22.002234375" />
                  <Point X="-0.022905902863" Y="22.00116015625" />
                  <Point X="-0.051065185547" Y="21.996890625" />
                  <Point X="-0.064983123779" Y="21.9936953125" />
                  <Point X="-0.179819580078" Y="21.9580546875" />
                  <Point X="-0.318342224121" Y="21.9150625" />
                  <Point X="-0.332930603027" Y="21.909171875" />
                  <Point X="-0.360932800293" Y="21.8950625" />
                  <Point X="-0.374346466064" Y="21.88684375" />
                  <Point X="-0.400601593018" Y="21.867599609375" />
                  <Point X="-0.412476409912" Y="21.85728125" />
                  <Point X="-0.43435949707" Y="21.834826171875" />
                  <Point X="-0.44436819458" Y="21.822689453125" />
                  <Point X="-0.518578613281" Y="21.715765625" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.612468994141" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.861768310547" Y="20.6946875" />
                  <Point X="-0.985425048828" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.137055855627" Y="20.30049935542" />
                  <Point X="-0.958068539315" Y="20.335290965297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665627603321" Y="21.039666141724" />
                  <Point X="2.693821537711" Y="21.045146487406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.123979978162" Y="20.399819134545" />
                  <Point X="-0.930712029802" Y="20.437386618094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601005761865" Y="21.123883014225" />
                  <Point X="2.643584433209" Y="21.132159469524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.122597669472" Y="20.496865914156" />
                  <Point X="-0.903355520289" Y="20.53948227089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.825615945699" Y="20.875560278939" />
                  <Point X="0.829323502336" Y="20.876280954945" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.53638392041" Y="21.208099886727" />
                  <Point X="2.593347328707" Y="21.219172451641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.140797967174" Y="20.590106220683" />
                  <Point X="-0.875999010776" Y="20.641577923687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.800968064605" Y="20.967547302214" />
                  <Point X="0.816900333614" Y="20.970644221589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.471762078954" Y="21.292316759228" />
                  <Point X="2.543110224205" Y="21.306185433758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.159331681585" Y="20.683281717567" />
                  <Point X="-0.848642543585" Y="20.743673568257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77632018351" Y="21.059534325488" />
                  <Point X="0.804477164892" Y="21.065007488233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.407140237499" Y="21.37653363173" />
                  <Point X="2.492873120041" Y="21.393198415942" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.177865578812" Y="20.776457178916" />
                  <Point X="-0.82128612228" Y="20.845769203908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.751672302416" Y="21.151521348762" />
                  <Point X="0.792053996171" Y="21.159370754877" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.342518396043" Y="21.460750504231" />
                  <Point X="2.44263604642" Y="21.480211404062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207378112359" Y="20.867498609542" />
                  <Point X="-0.793929700975" Y="20.947864839559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.727024421322" Y="21.243508372036" />
                  <Point X="0.779630827449" Y="21.253734021521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.277896559404" Y="21.544967377669" />
                  <Point X="2.392398972799" Y="21.567224392182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.273732073503" Y="20.951378792083" />
                  <Point X="-0.76657327967" Y="21.04996047521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702376540228" Y="21.33549539531" />
                  <Point X="0.767207658728" Y="21.348097288166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.213274733156" Y="21.629184253127" />
                  <Point X="2.342161899178" Y="21.654237380302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.364063716745" Y="21.030598185366" />
                  <Point X="-0.739216858365" Y="21.152056110861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.677728659134" Y="21.427482418584" />
                  <Point X="0.754784490006" Y="21.44246055481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148652906908" Y="21.713401128584" />
                  <Point X="2.291924825557" Y="21.741250368422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.587312639955" Y="20.88960076754" />
                  <Point X="-2.441489444939" Y="20.917945925267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.454396292358" Y="21.109817397414" />
                  <Point X="-0.71186043706" Y="21.254151746512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.652754097365" Y="21.519405941568" />
                  <Point X="0.742361321284" Y="21.536823821454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.08403108066" Y="21.797618004042" />
                  <Point X="2.241687751936" Y="21.828263356542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.706269181952" Y="20.963256044154" />
                  <Point X="-2.311140173944" Y="21.04006134288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565160196453" Y="21.185065161515" />
                  <Point X="-0.684504015755" Y="21.356247382163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.606276763417" Y="21.607149749047" />
                  <Point X="0.729938152563" Y="21.631187088098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.019409254411" Y="21.8818348795" />
                  <Point X="2.191450678315" Y="21.915276344662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965870081971" Y="22.260188536885" />
                  <Point X="4.023551683162" Y="22.271400704356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.813086787323" Y="21.039270891021" />
                  <Point X="-2.106879711217" Y="21.17654364079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.883368706676" Y="21.219989778948" />
                  <Point X="-0.65714759445" Y="21.458343017814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.547091579862" Y="21.692423400793" />
                  <Point X="0.726433453727" Y="21.727283929676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.954787428163" Y="21.966051754957" />
                  <Point X="2.141213604694" Y="22.002289332782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.840465782056" Y="22.332590496321" />
                  <Point X="4.076223792855" Y="22.378417211342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.913443254626" Y="21.116541655904" />
                  <Point X="-0.622110218051" Y="21.561931679891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.487907456469" Y="21.777697258612" />
                  <Point X="0.746628238856" Y="21.827987484273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.890165601915" Y="22.050268630415" />
                  <Point X="2.090976531073" Y="22.089302320903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.71506148214" Y="22.404992455757" />
                  <Point X="3.97559223727" Y="22.455634504479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960206530077" Y="21.204229881986" />
                  <Point X="-0.548208567053" Y="21.673074791678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.420155919396" Y="21.861305779912" />
                  <Point X="0.768591471651" Y="21.929034790274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.824394650606" Y="22.134262138588" />
                  <Point X="2.048421581901" Y="22.177808562748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.589657182224" Y="22.477394415193" />
                  <Point X="3.874960681684" Y="22.532851797616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.897268226409" Y="21.31324193493" />
                  <Point X="-0.470564457117" Y="21.784945363791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.279887341161" Y="21.930818416333" />
                  <Point X="0.796075523328" Y="22.031155234756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.723428378241" Y="22.211414369374" />
                  <Point X="2.029101616666" Y="22.270831227954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.464252882308" Y="22.549796374629" />
                  <Point X="3.774329126099" Y="22.610069090753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.834329922741" Y="21.422253987875" />
                  <Point X="-0.335176627672" Y="21.908040177953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.088149528469" Y="21.990326447049" />
                  <Point X="0.884556261416" Y="22.145132233999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.607842605639" Y="22.285724857184" />
                  <Point X="2.0122165348" Y="22.364327186542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.338848962975" Y="22.622198408042" />
                  <Point X="3.673697569973" Y="22.687286383785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.771391619073" Y="21.531266040819" />
                  <Point X="1.036461342855" Y="22.271437676709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.449561202682" Y="22.351736155167" />
                  <Point X="2.00038310274" Y="22.45880508638" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.213445066685" Y="22.694600445935" />
                  <Point X="3.573066012982" Y="22.764503676649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.708453315405" Y="21.640278093763" />
                  <Point X="2.019679998199" Y="22.559334108906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.088041170394" Y="22.767002483828" />
                  <Point X="3.472434455992" Y="22.841720969513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.645515217661" Y="21.74929010668" />
                  <Point X="2.074675019668" Y="22.666802144201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.962637274104" Y="22.839404521721" />
                  <Point X="3.371802899002" Y="22.918938262377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.582577302109" Y="21.858302084182" />
                  <Point X="2.134965187976" Y="22.775299451775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.835685852074" Y="22.911505751082" />
                  <Point X="3.271171342012" Y="22.996155555241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519639386557" Y="21.967314061684" />
                  <Point X="2.324721897003" Y="22.908962505557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.605582471776" Y="22.963556270907" />
                  <Point X="3.170539785021" Y="23.073372848105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.456701471005" Y="22.076326039187" />
                  <Point X="3.069908228031" Y="23.150590140969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.393763555453" Y="22.185338016689" />
                  <Point X="2.991956408124" Y="23.232215928138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.81153670768" Y="22.006528919093" />
                  <Point X="-3.720428490528" Y="22.024238562508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.333367997349" Y="22.293855809964" />
                  <Point X="2.936648812064" Y="23.318243306539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875705350013" Y="22.09083388458" />
                  <Point X="-3.467725640382" Y="22.17013710666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311174941376" Y="22.394947789066" />
                  <Point X="2.884691431794" Y="23.404921900921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.939874209231" Y="22.175138807909" />
                  <Point X="-3.215022019704" Y="22.316035800588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.330144428567" Y="22.488038580302" />
                  <Point X="2.860744400632" Y="23.497045155622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004038975556" Y="22.259444526815" />
                  <Point X="-2.962318067179" Y="22.461934559021" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.387675271515" Y="22.573633803286" />
                  <Point X="2.865158382524" Y="23.594681232807" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.617496844713" Y="23.935301324801" />
                  <Point X="4.736389770342" Y="23.958411768439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.055758135558" Y="22.346169426527" />
                  <Point X="-2.709614114654" Y="22.607833317454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.51366955557" Y="22.645921081422" />
                  <Point X="2.874226151076" Y="23.693221914482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.320661804402" Y="23.974380523923" />
                  <Point X="4.759499047627" Y="24.059681842923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.107477295559" Y="22.432894326238" />
                  <Point X="2.905218175365" Y="23.796024239765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.023826855923" Y="24.013459740896" />
                  <Point X="4.775269940761" Y="24.159525480026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.159196435653" Y="22.519619229819" />
                  <Point X="2.991843611132" Y="23.909640604769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726992098584" Y="24.052538995022" />
                  <Point X="4.709335812721" Y="24.243487269856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.084427825683" Y="22.630930861359" />
                  <Point X="3.135580445968" Y="24.03435830118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429135771098" Y="24.091419676028" />
                  <Point X="4.50000866087" Y="24.299576279389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91551486057" Y="22.760542301756" />
                  <Point X="4.290681645704" Y="24.355665315491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.746601895456" Y="22.890153742153" />
                  <Point X="4.081354749059" Y="24.411754374631" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.577688824187" Y="23.019765203185" />
                  <Point X="3.872027852415" Y="24.467843433771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.408775564194" Y="23.1493767009" />
                  <Point X="3.667442413251" Y="24.524854138882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.239862304201" Y="23.278988198616" />
                  <Point X="3.537422315932" Y="24.596358878192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.070949044208" Y="23.408599696332" />
                  <Point X="3.454165959439" Y="24.6769535679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.974853262869" Y="23.524056910036" />
                  <Point X="3.394081101331" Y="24.762052340627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949284720801" Y="23.625805017168" />
                  <Point X="3.365344324613" Y="24.853244563106" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959400592286" Y="23.720616776962" />
                  <Point X="3.349820215623" Y="24.947005068022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.011841022789" Y="23.80720147589" />
                  <Point X="3.357905818331" Y="25.045354835996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.123201321353" Y="23.882333312651" />
                  <Point X="3.381462708063" Y="25.146711917525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.133288627134" Y="23.782770315918" />
                  <Point X="3.451514470515" Y="25.257106686787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676416853155" Y="23.773974969463" />
                  <Point X="3.613265355921" Y="25.385325959917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.699967751815" Y="23.866175224522" />
                  <Point X="4.725736575928" Y="25.698346545589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.723518650474" Y="23.958375479581" />
                  <Point X="4.769892056284" Y="25.803707587532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744369307105" Y="24.05110060852" />
                  <Point X="4.755266914775" Y="25.897642834025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.757836247586" Y="24.145260986487" />
                  <Point X="4.734161622331" Y="25.990318466776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.771303188067" Y="24.239421364454" />
                  <Point X="4.711665829732" Y="26.082723813677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784770197033" Y="24.333581729108" />
                  <Point X="3.639321787777" Y="25.97105933332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.63528881229" Y="24.653796362044" />
                  <Point X="3.258037971815" Y="25.993723353325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.342483223868" Y="24.807490088859" />
                  <Point X="3.142584590753" Y="26.068059575444" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.30149545495" Y="24.912235390073" />
                  <Point X="3.077533778863" Y="26.15219306454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.297937270406" Y="25.009705117105" />
                  <Point X="3.03084168851" Y="26.239895127604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.326934847701" Y="25.100846645087" />
                  <Point X="3.008861257245" Y="26.3324006506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.393506444741" Y="25.184684523495" />
                  <Point X="3.002351818669" Y="26.427913429938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.528918478685" Y="25.255141176497" />
                  <Point X="3.026420117185" Y="26.529369919265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73824560198" Y="25.311230191581" />
                  <Point X="3.080462608099" Y="26.636652801376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.947572725276" Y="25.367319206664" />
                  <Point X="3.186196196446" Y="26.753983414986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.156899848572" Y="25.423408221748" />
                  <Point X="3.355108541378" Y="26.883594734832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.366227053045" Y="25.479497221052" />
                  <Point X="3.524020886309" Y="27.013206054677" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.575554540224" Y="25.535586165404" />
                  <Point X="3.692933266149" Y="27.142817381308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784882027402" Y="25.591675109756" />
                  <Point X="2.363638944124" Y="26.981206826079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.652432219273" Y="27.037342552179" />
                  <Point X="3.861846627321" Y="27.272428898691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770740650253" Y="25.691202001039" />
                  <Point X="2.199159506622" Y="27.046013348191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.905136391482" Y="27.183241353315" />
                  <Point X="4.030759988494" Y="27.402040416074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.755995949006" Y="25.790846166646" />
                  <Point X="2.10863640027" Y="27.125195524815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.157840563692" Y="27.32914015445" />
                  <Point X="4.11740624295" Y="27.515660827822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.74125124776" Y="25.890490332253" />
                  <Point X="2.051999107703" Y="27.210964436398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.410544735901" Y="27.475038955585" />
                  <Point X="4.065005888845" Y="27.602253316813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.722272593169" Y="25.990957495021" />
                  <Point X="2.02158056385" Y="27.301829756461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.663248981979" Y="27.620937771079" />
                  <Point X="4.007406199977" Y="27.687835157505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.694589358653" Y="26.093116656724" />
                  <Point X="-3.979473954189" Y="26.232121010113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.488567361547" Y="26.327543585349" />
                  <Point X="2.013086229849" Y="27.396956711213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.915953242546" Y="27.766836589389" />
                  <Point X="3.946314751643" Y="27.772738268913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.666906124138" Y="26.195275818428" />
                  <Point X="-4.276310483101" Y="26.271199919881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.43765556098" Y="26.434217922902" />
                  <Point X="2.029630250705" Y="27.496950629122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.639222740366" Y="26.297435009144" />
                  <Point X="-4.573145390231" Y="26.310279144891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421514356187" Y="26.534133541301" />
                  <Point X="2.066652331152" Y="27.600925078585" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.444686855469" Y="26.626407349747" />
                  <Point X="2.129590421469" Y="27.709937090058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.49255246308" Y="26.713881304164" />
                  <Point X="2.192528511785" Y="27.818949101531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.576164695724" Y="26.794406818555" />
                  <Point X="2.255466602102" Y="27.927961113004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.676796101758" Y="26.871624140762" />
                  <Point X="2.318404692418" Y="28.036973124477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.777427507792" Y="26.948841462969" />
                  <Point X="2.381342782735" Y="28.14598513595" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.878058913826" Y="27.026058785176" />
                  <Point X="2.444280873051" Y="28.254997147423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97869031986" Y="27.103276107383" />
                  <Point X="2.507218850554" Y="28.364009136967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.079322259614" Y="27.180493325845" />
                  <Point X="2.570156802658" Y="28.473021121574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.179954436349" Y="27.257710498243" />
                  <Point X="2.633094754762" Y="28.582033106182" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.194075156922" Y="27.351743794234" />
                  <Point X="2.696032706866" Y="28.691045090789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.130357652771" Y="27.460907308409" />
                  <Point X="2.758970658971" Y="28.800057075396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066639760822" Y="27.570070897964" />
                  <Point X="-3.195936811092" Y="27.7393184065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.869108092564" Y="27.802847473842" />
                  <Point X="2.810765645207" Y="28.906903086853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.002921868873" Y="27.67923448752" />
                  <Point X="-3.33498721214" Y="27.809067832579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.773127316317" Y="27.918282332821" />
                  <Point X="2.703889656912" Y="28.98290658523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.914459934843" Y="27.793207831624" />
                  <Point X="-3.460391017117" Y="27.881469888221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748769428955" Y="28.019795112517" />
                  <Point X="2.591651544885" Y="29.057867792338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.825752114452" Y="27.907228971195" />
                  <Point X="-3.585794936691" Y="27.953871921588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.755005762632" Y="28.11536097807" />
                  <Point X="2.467094744698" Y="29.130434489033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.73704491847" Y="28.021249989394" />
                  <Point X="-3.711199145846" Y="28.026273898666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777544399484" Y="28.207757996892" />
                  <Point X="2.342538008387" Y="29.203001198145" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.825709364336" Y="28.295173762155" />
                  <Point X="2.212348464692" Y="29.274473000415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.875946518862" Y="28.382186734549" />
                  <Point X="2.066168816607" Y="29.342836641252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.926183673387" Y="28.469199706943" />
                  <Point X="1.919988724819" Y="29.411200195841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976420827913" Y="28.556212679337" />
                  <Point X="-0.106427768726" Y="29.114082817405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.188918457032" Y="29.17149230807" />
                  <Point X="1.757899757192" Y="29.476471378226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.026658106341" Y="28.643225627646" />
                  <Point X="-0.192611916574" Y="29.194108402124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.242523658619" Y="29.278690189747" />
                  <Point X="1.584177935174" Y="29.539481362779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000456008153" Y="28.745096885613" />
                  <Point X="-0.231456502966" Y="29.283335865433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.269880083342" Y="29.380785826062" />
                  <Point X="1.398443151146" Y="29.600156264063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.831357784825" Y="28.874744336559" />
                  <Point X="-1.930596513862" Y="29.049834590868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.595169114304" Y="29.115035072487" />
                  <Point X="-0.256104238415" Y="29.375322917018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.297236508065" Y="29.482881462377" />
                  <Point X="1.176381634216" Y="29.653769963775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.662259326186" Y="29.004391833244" />
                  <Point X="-2.06343565723" Y="29.120791363135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.508121613653" Y="29.228733478594" />
                  <Point X="-0.280751973864" Y="29.467309968603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.324592932788" Y="29.584977098693" />
                  <Point X="0.954317863827" Y="29.70738322546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.40243154024" Y="29.15167532462" />
                  <Point X="-2.141897356429" Y="29.20231803981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475054879143" Y="29.331939086691" />
                  <Point X="-0.305399709313" Y="29.559297020187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.351949306933" Y="29.687072725177" />
                  <Point X="0.688560582684" Y="29.752503329016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462714041728" Y="29.431115988504" />
                  <Point X="-0.33004749896" Y="29.651284061237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474390920498" Y="29.525624319219" />
                  <Point X="-0.354695313963" Y="29.743271097358" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.653104858398" Y="20.785275390625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.478455078125" />
                  <Point X="0.390108276367" Y="21.585376953125" />
                  <Point X="0.300591186523" Y="21.71435546875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.159499206543" Y="21.7692421875" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.123501304626" Y="21.77659375" />
                  <Point X="-0.262023895264" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.362489562988" Y="21.60743359375" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.678242553711" Y="20.64551171875" />
                  <Point X="-0.847744140625" Y="20.012921875" />
                  <Point X="-0.937840576172" Y="20.03041015625" />
                  <Point X="-1.100258789062" Y="20.0619375" />
                  <Point X="-1.232813232422" Y="20.096041015625" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.331006591797" Y="20.2829453125" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.337117675781" Y="20.6031640625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.492009765625" Y="20.890091796875" />
                  <Point X="-1.619543701172" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.789563842773" Y="21.02343359375" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878295898" Y="21.026208984375" />
                  <Point X="-2.106802978516" Y="20.94808203125" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259733886719" Y="20.842705078125" />
                  <Point X="-2.382694091797" Y="20.682458984375" />
                  <Point X="-2.457094238281" Y="20.5855" />
                  <Point X="-2.617781494141" Y="20.6849921875" />
                  <Point X="-2.855839355469" Y="20.832392578125" />
                  <Point X="-3.039401611328" Y="20.973728515625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.843453369141" Y="21.786451171875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513981445312" Y="22.43123828125" />
                  <Point X="-2.531330078125" Y="22.4485859375" />
                  <Point X="-2.560158447266" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.296655029297" Y="22.049513671875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.973629394531" Y="21.905779296875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.293307128906" Y="22.373548828125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.754161621094" Y="23.12384375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161159912109" Y="23.689361328125" />
                  <Point X="-3.187642578125" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.109488769531" Y="23.594263671875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.8538359375" Y="23.7008359375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.962211914062" Y="24.2322578125" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.229989746094" Y="24.691150390625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.529317138672" Y="24.8873046875" />
                  <Point X="-3.502324462891" Y="24.909353515625" />
                  <Point X="-3.490705810547" Y="24.9376015625" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.489841064453" Y="24.99705078125" />
                  <Point X="-3.502324462891" Y="25.0280859375" />
                  <Point X="-3.526722412109" Y="25.048333984375" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.368704101562" Y="25.28345703125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.965051269531" Y="25.67605078125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.847555175781" Y="26.2550703125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.235349609375" Y="26.457447265625" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731703857422" Y="26.3958671875" />
                  <Point X="-3.703862304688" Y="26.40464453125" />
                  <Point X="-3.670277832031" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.627948486328" Y="26.470755859375" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.629795898438" Y="26.57140625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.125297851563" Y="26.97628125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.34421875" Y="27.471419921875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.974349365234" Y="28.025650390625" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.455239013672" Y="28.09788671875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514404297" Y="27.92043359375" />
                  <Point X="-3.099738525391" Y="27.917041015625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.985729492188" Y="27.954927734375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941466796875" Y="28.066615234375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.158269042969" Y="28.49118359375" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.073700927734" Y="28.928357421875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.460459960938" Y="29.336791015625" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.058466308594" Y="29.405697265625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247558594" Y="29.273662109375" />
                  <Point X="-1.908090332031" Y="29.2511953125" />
                  <Point X="-1.85603137207" Y="29.22409375" />
                  <Point X="-1.835125" Y="29.218490234375" />
                  <Point X="-1.813809570312" Y="29.22225" />
                  <Point X="-1.768858398438" Y="29.24087109375" />
                  <Point X="-1.714635620117" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.671452636719" Y="29.340890625" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.675360473633" Y="29.5964921875" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.383421875" Y="29.7868515625" />
                  <Point X="-0.968083007812" Y="29.903296875" />
                  <Point X="-0.61359765625" Y="29.94478515625" />
                  <Point X="-0.224199966431" Y="29.990359375" />
                  <Point X="-0.129729553223" Y="29.637791015625" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282131195" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594051361" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.160107513428" Y="29.705212890625" />
                  <Point X="0.236648422241" Y="29.9908671875" />
                  <Point X="0.497563232422" Y="29.96354296875" />
                  <Point X="0.860210021973" Y="29.925564453125" />
                  <Point X="1.153490478516" Y="29.8547578125" />
                  <Point X="1.508455810547" Y="29.769056640625" />
                  <Point X="1.699518188477" Y="29.6997578125" />
                  <Point X="1.931044311523" Y="29.61578125" />
                  <Point X="2.115625488281" Y="29.529458984375" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.517024658203" Y="29.321240234375" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.900706054688" Y="29.076087890625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.620391357422" Y="28.180029296875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.215120849609" Y="27.455123046875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.205839111328" Y="27.360857421875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.238153076172" Y="27.2721171875" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.303633544922" Y="27.204734375" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360335449219" Y="27.172978515625" />
                  <Point X="2.391802734375" Y="27.169185546875" />
                  <Point X="2.429760253906" Y="27.164607421875" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.4850546875" Y="27.175677734375" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.356985595703" Y="27.6635078125" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.074765136719" Y="27.919529296875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.296351074219" Y="27.58694140625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.802795654297" Y="26.987626953125" />
                  <Point X="3.288615966797" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.253180908203" Y="26.549666015625" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.203363769531" Y="26.456615234375" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.198788085938" Y="26.35215234375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.237428710938" Y="26.25484765625" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280947998047" Y="26.1988203125" />
                  <Point X="3.312513671875" Y="26.18105078125" />
                  <Point X="3.350590087891" Y="26.1596171875" />
                  <Point X="3.368566650391" Y="26.153619140625" />
                  <Point X="3.411245361328" Y="26.147978515625" />
                  <Point X="3.462727294922" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.250903320312" Y="26.24321484375" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.884287597656" Y="26.176900390625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.968734863281" Y="25.761615234375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.330428710938" Y="25.39571875" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087890625" Y="25.232818359375" />
                  <Point X="3.687157226562" Y="25.20858203125" />
                  <Point X="3.636577880859" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.597106445312" Y="25.134869140625" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.548598876953" Y="25.0309453125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.546869384766" Y="24.915525390625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.591917480469" Y="24.809181640625" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636576660156" Y="24.758091796875" />
                  <Point X="3.678507324219" Y="24.73385546875" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.451969238281" Y="24.50915234375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.9790859375" Y="24.236921875" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.910578125" Y="23.867720703125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.095359130859" Y="23.81240234375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.312541015625" Y="23.883771484375" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.135703125" Y="23.785478515625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.057228515625" Y="23.608455078125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.101903564453" Y="23.4125390625" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.828088867188" Y="22.80830859375" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.290542480469" Y="22.337685546875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.12584765625" Y="22.086626953125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.362093994141" Y="22.389384765625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340332031" Y="22.746685546875" />
                  <Point X="2.639395996094" Y="22.764375" />
                  <Point X="2.52125" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.407709960938" Y="22.737931640625" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.245776611328" Y="22.58394921875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.2068515625" Y="22.3556796875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.657968994141" Y="21.487244140625" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.937833251953" Y="20.883025390625" />
                  <Point X="2.835293212891" Y="20.809783203125" />
                  <Point X="2.747772949219" Y="20.7531328125" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="2.147625732422" Y="21.402630859375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.573949707031" Y="22.08163671875" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.320156860352" Y="22.154560546875" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.08375378418" Y="22.06366015625" />
                  <Point X="0.985348754883" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.944065185547" Y="21.84179296875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.034053100586" Y="20.7768515625" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.091351196289" Y="20.058015625" />
                  <Point X="0.994368469238" Y="20.0367578125" />
                  <Point X="0.91348815918" Y="20.022064453125" />
                  <Point X="0.860200378418" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#162" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.087589240046" Y="4.682011984302" Z="1.15" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="-0.625565786334" Y="5.025726147262" Z="1.15" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.15" />
                  <Point X="-1.403075624132" Y="4.866276683211" Z="1.15" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.15" />
                  <Point X="-1.731088682022" Y="4.621246401114" Z="1.15" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.15" />
                  <Point X="-1.725294383833" Y="4.38720683628" Z="1.15" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.15" />
                  <Point X="-1.794147891476" Y="4.318344114707" Z="1.15" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.15" />
                  <Point X="-1.891157919395" Y="4.326824904047" Z="1.15" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.15" />
                  <Point X="-2.02495484578" Y="4.467415259401" Z="1.15" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.15" />
                  <Point X="-2.490898777771" Y="4.411779151341" Z="1.15" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.15" />
                  <Point X="-3.1102790809" Y="3.999318244864" Z="1.15" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.15" />
                  <Point X="-3.207726253463" Y="3.497464479469" Z="1.15" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.15" />
                  <Point X="-2.997432485308" Y="3.093539306272" Z="1.15" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.15" />
                  <Point X="-3.02724025057" Y="3.021563317317" Z="1.15" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.15" />
                  <Point X="-3.101537159334" Y="2.998132213865" Z="1.15" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.15" />
                  <Point X="-3.436394868828" Y="3.172467695282" Z="1.15" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.15" />
                  <Point X="-4.019969030777" Y="3.087634909834" Z="1.15" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.15" />
                  <Point X="-4.393556798386" Y="2.527905477937" Z="1.15" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.15" />
                  <Point X="-4.161891733669" Y="1.967893921913" Z="1.15" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.15" />
                  <Point X="-3.680302199361" Y="1.579598773737" Z="1.15" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.15" />
                  <Point X="-3.680298294316" Y="1.521170772872" Z="1.15" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.15" />
                  <Point X="-3.725054299057" Y="1.483611002958" Z="1.15" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.15" />
                  <Point X="-4.234978979452" Y="1.538299972978" Z="1.15" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.15" />
                  <Point X="-4.901971065093" Y="1.299428578334" Z="1.15" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.15" />
                  <Point X="-5.020668846551" Y="0.714649400672" Z="1.15" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.15" />
                  <Point X="-4.387801197235" Y="0.266440261951" Z="1.15" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.15" />
                  <Point X="-3.561387014679" Y="0.038537603163" Z="1.15" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.15" />
                  <Point X="-3.54374986551" Y="0.013510176796" Z="1.15" />
                  <Point X="-3.539556741714" Y="0" Z="1.15" />
                  <Point X="-3.544614735625" Y="-0.016296774265" Z="1.15" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.15" />
                  <Point X="-3.563981580444" Y="-0.040338379971" Z="1.15" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.15" />
                  <Point X="-4.249086591538" Y="-0.229271794805" Z="1.15" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.15" />
                  <Point X="-5.017863993632" Y="-0.743540257442" Z="1.15" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.15" />
                  <Point X="-4.908436621557" Y="-1.28025932209" Z="1.15" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.15" />
                  <Point X="-4.109118534658" Y="-1.42402869573" Z="1.15" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.15" />
                  <Point X="-3.204679692677" Y="-1.31538513216" Z="1.15" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.15" />
                  <Point X="-3.196888693617" Y="-1.338714000332" Z="1.15" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.15" />
                  <Point X="-3.790755601627" Y="-1.805207667793" Z="1.15" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.15" />
                  <Point X="-4.342406211938" Y="-2.62078031245" Z="1.15" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.15" />
                  <Point X="-4.019571260284" Y="-3.093223691376" Z="1.15" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.15" />
                  <Point X="-3.277810989185" Y="-2.962506444866" Z="1.15" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.15" />
                  <Point X="-2.563354293456" Y="-2.564976175676" Z="1.15" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.15" />
                  <Point X="-2.892910568303" Y="-3.157267393307" Z="1.15" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.15" />
                  <Point X="-3.076061385528" Y="-4.034607213638" Z="1.15" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.15" />
                  <Point X="-2.65025925522" Y="-4.326238096227" Z="1.15" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.15" />
                  <Point X="-2.349182549246" Y="-4.316697069993" Z="1.15" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.15" />
                  <Point X="-2.085180738284" Y="-4.062211097272" Z="1.15" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.15" />
                  <Point X="-1.798989960659" Y="-3.995178661957" Z="1.15" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.15" />
                  <Point X="-1.531132847586" Y="-4.116220404554" Z="1.15" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.15" />
                  <Point X="-1.392312957069" Y="-4.375310207644" Z="1.15" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.15" />
                  <Point X="-1.386734768903" Y="-4.679246736002" Z="1.15" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.15" />
                  <Point X="-1.251428347995" Y="-4.92109971651" Z="1.15" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.15" />
                  <Point X="-0.95352417825" Y="-4.987392803613" Z="1.15" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="-0.636102237626" Y="-4.336149952577" Z="1.15" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="-0.327569627717" Y="-3.389795928804" Z="1.15" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="-0.114836500658" Y="-3.23988038038" Z="1.15" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.15" />
                  <Point X="0.138522578703" Y="-3.247231664908" Z="1.15" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.15" />
                  <Point X="0.342876231715" Y="-3.411849826964" Z="1.15" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.15" />
                  <Point X="0.598652622775" Y="-4.196386077239" Z="1.15" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.15" />
                  <Point X="0.916269045352" Y="-4.995850754123" Z="1.15" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.15" />
                  <Point X="1.095900759719" Y="-4.95954381364" Z="1.15" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.15" />
                  <Point X="1.077469391407" Y="-4.185342972699" Z="1.15" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.15" />
                  <Point X="0.986768397081" Y="-3.137546593711" Z="1.15" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.15" />
                  <Point X="1.109565059097" Y="-2.943505447159" Z="1.15" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.15" />
                  <Point X="1.318582529136" Y="-2.863948407564" Z="1.15" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.15" />
                  <Point X="1.540754437526" Y="-2.929140742709" Z="1.15" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.15" />
                  <Point X="2.101801955131" Y="-3.596525639874" Z="1.15" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.15" />
                  <Point X="2.768785828473" Y="-4.257560419713" Z="1.15" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.15" />
                  <Point X="2.960738896786" Y="-4.126381136638" Z="1.15" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.15" />
                  <Point X="2.69511430578" Y="-3.4564755239" Z="1.15" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.15" />
                  <Point X="2.249899780098" Y="-2.604152451245" Z="1.15" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.15" />
                  <Point X="2.283866993571" Y="-2.408057784782" Z="1.15" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.15" />
                  <Point X="2.42484059359" Y="-2.275034316396" Z="1.15" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.15" />
                  <Point X="2.624354352267" Y="-2.253548229393" Z="1.15" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.15" />
                  <Point X="3.330938073207" Y="-2.622635194495" Z="1.15" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.15" />
                  <Point X="4.160580471297" Y="-2.910869283072" Z="1.15" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.15" />
                  <Point X="4.326920440091" Y="-2.657319907355" Z="1.15" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.15" />
                  <Point X="3.852370824581" Y="-2.120743463248" Z="1.15" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.15" />
                  <Point X="3.137805804721" Y="-1.52914210515" Z="1.15" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.15" />
                  <Point X="3.100862217177" Y="-1.364847385313" Z="1.15" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.15" />
                  <Point X="3.167993379489" Y="-1.215208546291" Z="1.15" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.15" />
                  <Point X="3.317004753485" Y="-1.133807606309" Z="1.15" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.15" />
                  <Point X="4.082676917651" Y="-1.205888714104" Z="1.15" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.15" />
                  <Point X="4.953169245067" Y="-1.112123391744" Z="1.15" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.15" />
                  <Point X="5.022371380808" Y="-0.739250782891" Z="1.15" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.15" />
                  <Point X="4.45875414032" Y="-0.411269300136" Z="1.15" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.15" />
                  <Point X="3.697373412327" Y="-0.191574902187" Z="1.15" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.15" />
                  <Point X="3.62509514229" Y="-0.128668268591" Z="1.15" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.15" />
                  <Point X="3.589820866242" Y="-0.043789294484" Z="1.15" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.15" />
                  <Point X="3.591550584182" Y="0.05282123673" Z="1.15" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.15" />
                  <Point X="3.63028429611" Y="0.135280469984" Z="1.15" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.15" />
                  <Point X="3.706022002026" Y="0.196573942055" Z="1.15" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.15" />
                  <Point X="4.337213676987" Y="0.378702632633" Z="1.15" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.15" />
                  <Point X="5.011983461319" Y="0.800586742641" Z="1.15" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.15" />
                  <Point X="4.926712717109" Y="1.220007811904" Z="1.15" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.15" />
                  <Point X="4.238220852428" Y="1.324067891471" Z="1.15" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.15" />
                  <Point X="3.41163955611" Y="1.228828037834" Z="1.15" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.15" />
                  <Point X="3.33088423061" Y="1.255902287985" Z="1.15" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.15" />
                  <Point X="3.273043367322" Y="1.313608156799" Z="1.15" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.15" />
                  <Point X="3.241600595094" Y="1.393535631651" Z="1.15" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.15" />
                  <Point X="3.245360127151" Y="1.474429089023" Z="1.15" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.15" />
                  <Point X="3.286708047418" Y="1.550528036371" Z="1.15" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.15" />
                  <Point X="3.827077884251" Y="1.979239067042" Z="1.15" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.15" />
                  <Point X="4.332972433677" Y="2.644108468985" Z="1.15" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.15" />
                  <Point X="4.109194571834" Y="2.980013403414" Z="1.15" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.15" />
                  <Point X="3.32582963409" Y="2.73808868267" Z="1.15" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.15" />
                  <Point X="2.465981879757" Y="2.255260394789" Z="1.15" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.15" />
                  <Point X="2.391634019816" Y="2.250106162645" Z="1.15" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.15" />
                  <Point X="2.325553122027" Y="2.277387311336" Z="1.15" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.15" />
                  <Point X="2.273371288899" Y="2.331471738355" Z="1.15" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.15" />
                  <Point X="2.249323540114" Y="2.398124424016" Z="1.15" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.15" />
                  <Point X="2.257267547869" Y="2.473487696125" Z="1.15" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.15" />
                  <Point X="2.65753667019" Y="3.186309447865" Z="1.15" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.15" />
                  <Point X="2.923527160991" Y="4.148116541173" Z="1.15" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.15" />
                  <Point X="2.536038514268" Y="4.3957243076" Z="1.15" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.15" />
                  <Point X="2.130650966854" Y="4.606030640736" Z="1.15" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.15" />
                  <Point X="1.710410957197" Y="4.778042248186" Z="1.15" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.15" />
                  <Point X="1.159069124916" Y="4.934641203982" Z="1.15" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.15" />
                  <Point X="0.496615135178" Y="5.044552030988" Z="1.15" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.15" />
                  <Point X="0.105655321507" Y="4.749435521649" Z="1.15" />
                  <Point X="0" Y="4.355124473572" Z="1.15" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>