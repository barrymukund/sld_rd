<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#175" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2213" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004716796875" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.792180480957" Y="20.6332890625" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557721679688" Y="21.502857421875" />
                  <Point X="0.542363098145" Y="21.532625" />
                  <Point X="0.448814666748" Y="21.66741015625" />
                  <Point X="0.378635437012" Y="21.768525390625" />
                  <Point X="0.356752288818" Y="21.790978515625" />
                  <Point X="0.330497253418" Y="21.81022265625" />
                  <Point X="0.3024949646" Y="21.82433203125" />
                  <Point X="0.157734115601" Y="21.869259765625" />
                  <Point X="0.049135822296" Y="21.90296484375" />
                  <Point X="0.020976730347" Y="21.907234375" />
                  <Point X="-0.008664140701" Y="21.907234375" />
                  <Point X="-0.036824295044" Y="21.90296484375" />
                  <Point X="-0.181584976196" Y="21.85803515625" />
                  <Point X="-0.290183258057" Y="21.82433203125" />
                  <Point X="-0.31818359375" Y="21.810224609375" />
                  <Point X="-0.34443939209" Y="21.79098046875" />
                  <Point X="-0.366323608398" Y="21.7685234375" />
                  <Point X="-0.459872039795" Y="21.633736328125" />
                  <Point X="-0.530051269531" Y="21.532623046875" />
                  <Point X="-0.538188781738" Y="21.518427734375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.722692687988" Y="20.846671875" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-0.95514074707" Y="20.13054296875" />
                  <Point X="-1.079354858398" Y="20.154654296875" />
                  <Point X="-1.244067138672" Y="20.19703125" />
                  <Point X="-1.24641796875" Y="20.19763671875" />
                  <Point X="-1.242514526367" Y="20.22728515625" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.251092163086" Y="20.65763671875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868796875" />
                  <Point X="-1.456922119141" Y="20.985677734375" />
                  <Point X="-1.556905639648" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.61288659668" Y="21.102005859375" />
                  <Point X="-1.64302746582" Y="21.109033203125" />
                  <Point X="-1.819916259766" Y="21.120626953125" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657470703" Y="21.10519921875" />
                  <Point X="-2.19005078125" Y="21.00671484375" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.431500732422" Y="20.774908203125" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.619663085938" Y="20.79789453125" />
                  <Point X="-2.801714111328" Y="20.9106171875" />
                  <Point X="-3.029804443359" Y="21.08623828125" />
                  <Point X="-3.104721435547" Y="21.143921875" />
                  <Point X="-2.849106201172" Y="21.58666015625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414561279297" Y="22.444427734375" />
                  <Point X="-2.428779785156" Y="22.4732578125" />
                  <Point X="-2.446808105469" Y="22.498416015625" />
                  <Point X="-2.464156738281" Y="22.515763671875" />
                  <Point X="-2.489316894531" Y="22.533791015625" />
                  <Point X="-2.518145263672" Y="22.548005859375" />
                  <Point X="-2.54776171875" Y="22.55698828125" />
                  <Point X="-2.578694091797" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.191863769531" Y="22.2197109375" />
                  <Point X="-3.818022949219" Y="21.858197265625" />
                  <Point X="-3.939034179688" Y="22.017181640625" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.246384765625" Y="22.48034375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.850170410156" Y="22.930427734375" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577148438" Y="23.524404296875" />
                  <Point X="-3.066612548828" Y="23.55153515625" />
                  <Point X="-3.053856689453" Y="23.580166015625" />
                  <Point X="-3.046152099609" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64033984375" />
                  <Point X="-3.045555908203" Y="23.672009765625" />
                  <Point X="-3.052556396484" Y="23.701755859375" />
                  <Point X="-3.068637695312" Y="23.72773828125" />
                  <Point X="-3.089469482422" Y="23.7516953125" />
                  <Point X="-3.112974365234" Y="23.771234375" />
                  <Point X="-3.139461669922" Y="23.786822265625" />
                  <Point X="-3.168723144531" Y="23.79804296875" />
                  <Point X="-3.200607421875" Y="23.8045234375" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.929635498047" Y="23.71376171875" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.777825195312" Y="23.78712109375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.877341796875" Y="24.309845703125" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-4.380649902344" Y="24.5524296875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.509799804688" Y="24.78917578125" />
                  <Point X="-3.478375488281" Y="24.80740234375" />
                  <Point X="-3.465201660156" Y="24.816615234375" />
                  <Point X="-3.441487060547" Y="24.836388671875" />
                  <Point X="-3.425461914062" Y="24.8535234375" />
                  <Point X="-3.414123779297" Y="24.874060546875" />
                  <Point X="-3.401412353516" Y="24.905828125" />
                  <Point X="-3.396932861328" Y="24.9202578125" />
                  <Point X="-3.390720214844" Y="24.94785546875" />
                  <Point X="-3.38840234375" Y="24.96923046875" />
                  <Point X="-3.390950927734" Y="24.99058203125" />
                  <Point X="-3.398483398438" Y="25.02243359375" />
                  <Point X="-3.403157714844" Y="25.036908203125" />
                  <Point X="-3.414549560547" Y="25.06442578125" />
                  <Point X="-3.426132568359" Y="25.084830078125" />
                  <Point X="-3.442363525391" Y="25.1017734375" />
                  <Point X="-3.470034912109" Y="25.124291015625" />
                  <Point X="-3.483352783203" Y="25.1333671875" />
                  <Point X="-3.510820068359" Y="25.14884765625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.168868652344" Y="25.328263671875" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.860740722656" Y="25.731982421875" />
                  <Point X="-4.824488769531" Y="25.97697265625" />
                  <Point X="-4.737397949219" Y="26.29836328125" />
                  <Point X="-4.703551269531" Y="26.423267578125" />
                  <Point X="-4.37337109375" Y="26.379798828125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985839844" Y="26.299341796875" />
                  <Point X="-3.723422363281" Y="26.301228515625" />
                  <Point X="-3.703139404297" Y="26.305263671875" />
                  <Point X="-3.668042724609" Y="26.316328125" />
                  <Point X="-3.641713378906" Y="26.324630859375" />
                  <Point X="-3.622773925781" Y="26.332962890625" />
                  <Point X="-3.604030761719" Y="26.34378515625" />
                  <Point X="-3.5873515625" Y="26.356015625" />
                  <Point X="-3.573714599609" Y="26.37156640625" />
                  <Point X="-3.56130078125" Y="26.389294921875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.537268798828" Y="26.441427734375" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.528751953125" />
                  <Point X="-3.518951904297" Y="26.5491953125" />
                  <Point X="-3.524554443359" Y="26.5701015625" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.549042236328" Y="26.62201953125" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.57328125" Y="26.663705078125" />
                  <Point X="-3.587193115234" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.966942626953" Y="26.974517578125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.222017578125" Y="27.492326171875" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.850455810547" Y="28.0301875" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.579892578125" Y="28.06016015625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187727294922" Y="27.836341796875" />
                  <Point X="-3.167083496094" Y="27.82983203125" />
                  <Point X="-3.146794677734" Y="27.825794921875" />
                  <Point X="-3.097914550781" Y="27.821517578125" />
                  <Point X="-3.061245361328" Y="27.818310546875" />
                  <Point X="-3.04055859375" Y="27.81876171875" />
                  <Point X="-3.019101074219" Y="27.821587890625" />
                  <Point X="-2.999011474609" Y="27.826505859375" />
                  <Point X="-2.980462158203" Y="27.835654296875" />
                  <Point X="-2.962208740234" Y="27.847283203125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.911382568359" Y="27.894923828125" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.847712402344" Y="28.085" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.031451904297" Y="28.46153125" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-2.945961181641" Y="28.9065859375" />
                  <Point X="-2.700620849609" Y="29.094685546875" />
                  <Point X="-2.337286132812" Y="29.296546875" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.157454345703" Y="29.378646484375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028891479492" Y="29.21480078125" />
                  <Point X="-2.012311767578" Y="29.200888671875" />
                  <Point X="-1.995115112305" Y="29.189396484375" />
                  <Point X="-1.940711914062" Y="29.16107421875" />
                  <Point X="-1.899899047852" Y="29.139828125" />
                  <Point X="-1.880625488281" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.839269042969" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797307128906" Y="29.128693359375" />
                  <Point X="-1.777452636719" Y="29.134482421875" />
                  <Point X="-1.720787963867" Y="29.157955078125" />
                  <Point X="-1.678278686523" Y="29.1755625" />
                  <Point X="-1.660147705078" Y="29.185509765625" />
                  <Point X="-1.642417602539" Y="29.197923828125" />
                  <Point X="-1.626865112305" Y="29.2115625" />
                  <Point X="-1.614633422852" Y="29.228244140625" />
                  <Point X="-1.603811279297" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.577036743164" Y="29.324416015625" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165405273" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.576108886719" Y="29.57042578125" />
                  <Point X="-1.584201904297" Y="29.6318984375" />
                  <Point X="-1.267237915039" Y="29.720763671875" />
                  <Point X="-0.949624511719" Y="29.80980859375" />
                  <Point X="-0.509162475586" Y="29.861359375" />
                  <Point X="-0.294711212158" Y="29.886458984375" />
                  <Point X="-0.244316619873" Y="29.6983828125" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271453857" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155911446" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425918579" Y="29.208806640625" />
                  <Point X="0.115583435059" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.229046310425" Y="29.5954453125" />
                  <Point X="0.307419494629" Y="29.8879375" />
                  <Point X="0.566721191406" Y="29.86078125" />
                  <Point X="0.844031005859" Y="29.831740234375" />
                  <Point X="1.208460571289" Y="29.743755859375" />
                  <Point X="1.481038574219" Y="29.6779453125" />
                  <Point X="1.717594116211" Y="29.592146484375" />
                  <Point X="1.894645996094" Y="29.527927734375" />
                  <Point X="2.124010253906" Y="29.420662109375" />
                  <Point X="2.294557128906" Y="29.34090234375" />
                  <Point X="2.516178222656" Y="29.21178515625" />
                  <Point X="2.680993408203" Y="29.115765625" />
                  <Point X="2.889956054688" Y="28.967162109375" />
                  <Point X="2.943260742188" Y="28.92925390625" />
                  <Point X="2.639885986328" Y="28.403794921875" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.5160546875" />
                  <Point X="2.120809814453" Y="27.470181640625" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.112511230469" Y="27.34128515625" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.164615478516" Y="27.211296875" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.194469238281" Y="27.170322265625" />
                  <Point X="2.221597167969" Y="27.14559375" />
                  <Point X="2.25776953125" Y="27.121048828125" />
                  <Point X="2.284905761719" Y="27.102634765625" />
                  <Point X="2.304947998047" Y="27.0922734375" />
                  <Point X="2.327038085938" Y="27.084005859375" />
                  <Point X="2.348964599609" Y="27.078662109375" />
                  <Point X="2.388631591797" Y="27.07387890625" />
                  <Point X="2.418389404297" Y="27.070291015625" />
                  <Point X="2.436469482422" Y="27.06984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.519080810547" Y="27.0864375" />
                  <Point X="2.553494384766" Y="27.095640625" />
                  <Point X="2.5652890625" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.228219970703" Y="27.47946875" />
                  <Point X="3.967325927734" Y="27.90619140625" />
                  <Point X="4.025517578125" Y="27.825318359375" />
                  <Point X="4.123270507813" Y="27.68946484375" />
                  <Point X="4.239773925781" Y="27.496939453125" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.878949462891" Y="27.165806640625" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203973876953" Y="26.641626953125" />
                  <Point X="3.170958984375" Y="26.598556640625" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.136606689453" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.109331787109" Y="26.473111328125" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.107834960938" Y="26.32283984375" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679931641" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.235740234375" />
                  <Point X="3.163741455078" Y="26.194005859375" />
                  <Point X="3.184340576172" Y="26.1626953125" />
                  <Point X="3.198890869141" Y="26.145453125" />
                  <Point X="3.216133544922" Y="26.12936328125" />
                  <Point X="3.234346435547" Y="26.11603515625" />
                  <Point X="3.274137451172" Y="26.09363671875" />
                  <Point X="3.303988525391" Y="26.07683203125" />
                  <Point X="3.320521240234" Y="26.0695" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.409920410156" Y="26.052328125" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462697998047" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.095862548828" Y="26.126984375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.803951171875" Y="26.10526953125" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.882649414063" Y="25.697005859375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.459391601562" Y="25.528626953125" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788818359" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.628690673828" Y="25.284517578125" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574312988281" Y="25.25109765625" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.515816162109" Y="25.1851640625" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.453109375" Y="25.037404296875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.455750488281" Y="24.88624609375" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.823337890625" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.523738525391" Y="24.7421796875" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.560000732422" Y="24.69876171875" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.641892822266" Y="24.645291015625" />
                  <Point X="3.681545654297" Y="24.62237109375" />
                  <Point X="3.692708251953" Y="24.616861328125" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.273830566406" Y="24.458533203125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.878465332031" Y="24.206763671875" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.807986816406" Y="23.84515625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.285834960937" Y="23.883146484375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.270919189453" Y="23.97194140625" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163975341797" Y="23.943404296875" />
                  <Point X="3.136147949219" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.049693115234" Y="23.830625" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.960770507812" Y="23.596970703125" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.033861816406" Y="23.342703125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.627760253906" Y="22.842279296875" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.190892089844" Y="22.35714453125" />
                  <Point X="4.124815429687" Y="22.250220703125" />
                  <Point X="4.028980957031" Y="22.1140546875" />
                  <Point X="3.568194335938" Y="22.38008984375" />
                  <Point X="2.800954589844" Y="22.823056640625" />
                  <Point X="2.786137451172" Y="22.829984375" />
                  <Point X="2.754224365234" Y="22.84017578125" />
                  <Point X="2.630757568359" Y="22.862474609375" />
                  <Point X="2.538134033203" Y="22.879201171875" />
                  <Point X="2.50677734375" Y="22.879603515625" />
                  <Point X="2.474605712891" Y="22.87464453125" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.342262939453" Y="22.81083984375" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242386962891" Y="22.753453125" />
                  <Point X="2.221426757812" Y="22.732494140625" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.150549804688" Y="22.606990234375" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.117973388672" Y="22.313275390625" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.484128662109" Y="21.59834375" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.860264160156" Y="20.944365234375" />
                  <Point X="2.781857177734" Y="20.888361328125" />
                  <Point X="2.701764160156" Y="20.83651953125" />
                  <Point X="2.344020996094" Y="21.30273828125" />
                  <Point X="1.758546264648" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.60015222168" Y="22.17773046875" />
                  <Point X="1.508800292969" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365600586" Y="22.256564453125" />
                  <Point X="1.417100585938" Y="22.258880859375" />
                  <Point X="1.283922607422" Y="22.246626953125" />
                  <Point X="1.184013793945" Y="22.23743359375" />
                  <Point X="1.156362548828" Y="22.2306015625" />
                  <Point X="1.128977050781" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="1.001758728027" Y="22.11903125" />
                  <Point X="0.92461138916" Y="22.054884765625" />
                  <Point X="0.904140808105" Y="22.031134765625" />
                  <Point X="0.887248779297" Y="22.00330859375" />
                  <Point X="0.875624267578" Y="21.974189453125" />
                  <Point X="0.844876647949" Y="21.8327265625" />
                  <Point X="0.821809997559" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.91391607666" Y="20.96155859375" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.975721130371" Y="20.12992578125" />
                  <Point X="0.929315368652" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058456787109" Y="20.24737109375" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.157917602539" Y="20.676169921875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221737060547" Y="20.89237109375" />
                  <Point X="-1.230573242188" Y="20.905138671875" />
                  <Point X="-1.250208251953" Y="20.929064453125" />
                  <Point X="-1.261006835938" Y="20.94022265625" />
                  <Point X="-1.394284301758" Y="21.057103515625" />
                  <Point X="-1.494267822266" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.533023681641" Y="21.17037890625" />
                  <Point X="-1.546835083008" Y="21.177474609375" />
                  <Point X="-1.576532348633" Y="21.189775390625" />
                  <Point X="-1.591315917969" Y="21.194525390625" />
                  <Point X="-1.621456787109" Y="21.201552734375" />
                  <Point X="-1.636814086914" Y="21.203830078125" />
                  <Point X="-1.81370300293" Y="21.215423828125" />
                  <Point X="-1.946403564453" Y="21.22412109375" />
                  <Point X="-1.961927856445" Y="21.2238671875" />
                  <Point X="-1.992725952148" Y="21.220833984375" />
                  <Point X="-2.007999633789" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667724609" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095436279297" Y="21.184189453125" />
                  <Point X="-2.242829589844" Y="21.085705078125" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380450683594" Y="20.989861328125" />
                  <Point X="-2.402763427734" Y="20.96722265625" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.569652587891" Y="20.878666015625" />
                  <Point X="-2.747587890625" Y="20.98883984375" />
                  <Point X="-2.971847412109" Y="21.16151171875" />
                  <Point X="-2.980862060547" Y="21.168453125" />
                  <Point X="-2.766833740234" Y="21.53916015625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314667480469" Y="22.442390625" />
                  <Point X="-2.323652832031" Y="22.4720078125" />
                  <Point X="-2.329359619141" Y="22.486447265625" />
                  <Point X="-2.343578125" Y="22.51527734375" />
                  <Point X="-2.351559570312" Y="22.52859375" />
                  <Point X="-2.369587890625" Y="22.553751953125" />
                  <Point X="-2.379634765625" Y="22.56559375" />
                  <Point X="-2.396983398438" Y="22.58294140625" />
                  <Point X="-2.408825683594" Y="22.592986328125" />
                  <Point X="-2.433985839844" Y="22.611013671875" />
                  <Point X="-2.447303710938" Y="22.61899609375" />
                  <Point X="-2.476132080078" Y="22.6332109375" />
                  <Point X="-2.490572753906" Y="22.638916015625" />
                  <Point X="-2.520189208984" Y="22.6478984375" />
                  <Point X="-2.550873291016" Y="22.6519375" />
                  <Point X="-2.581805664062" Y="22.650923828125" />
                  <Point X="-2.597229736328" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.239363769531" Y="22.301982421875" />
                  <Point X="-3.793087890625" Y="21.9822890625" />
                  <Point X="-3.863440673828" Y="22.07471875" />
                  <Point X="-4.004017578125" Y="22.259408203125" />
                  <Point X="-4.164791992188" Y="22.529001953125" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.792338134766" Y="22.85505859375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036482421875" Y="23.4366875" />
                  <Point X="-3.015104980469" Y="23.459607421875" />
                  <Point X="-3.005367431641" Y="23.471955078125" />
                  <Point X="-2.987402832031" Y="23.4990859375" />
                  <Point X="-2.979835449219" Y="23.512873046875" />
                  <Point X="-2.967079589844" Y="23.54150390625" />
                  <Point X="-2.961891113281" Y="23.55634765625" />
                  <Point X="-2.954186523438" Y="23.586095703125" />
                  <Point X="-2.951553222656" Y="23.6011953125" />
                  <Point X="-2.948748779297" Y="23.63162109375" />
                  <Point X="-2.948577636719" Y="23.646947265625" />
                  <Point X="-2.950785888672" Y="23.6786171875" />
                  <Point X="-2.953082275391" Y="23.6937734375" />
                  <Point X="-2.960082763672" Y="23.72351953125" />
                  <Point X="-2.971776855469" Y="23.751751953125" />
                  <Point X="-2.987858154297" Y="23.777734375" />
                  <Point X="-2.996949462891" Y="23.79007421875" />
                  <Point X="-3.01778125" Y="23.81403125" />
                  <Point X="-3.028740722656" Y="23.82475" />
                  <Point X="-3.052245605469" Y="23.8442890625" />
                  <Point X="-3.064791015625" Y="23.853109375" />
                  <Point X="-3.091278320312" Y="23.868697265625" />
                  <Point X="-3.105447753906" Y="23.8755234375" />
                  <Point X="-3.134709228516" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891138671875" />
                  <Point X="-3.181685546875" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.942035400391" Y="23.80794921875" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.685780273438" Y="23.8106328125" />
                  <Point X="-4.740763183594" Y="24.025888671875" />
                  <Point X="-4.783298828125" Y="24.323294921875" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.356062011719" Y="24.460666015625" />
                  <Point X="-3.508287841797" Y="24.687826171875" />
                  <Point X="-3.496431884766" Y="24.691857421875" />
                  <Point X="-3.473355712891" Y="24.701443359375" />
                  <Point X="-3.462135742188" Y="24.706998046875" />
                  <Point X="-3.430711425781" Y="24.725224609375" />
                  <Point X="-3.423931396484" Y="24.72955078125" />
                  <Point X="-3.404363769531" Y="24.743650390625" />
                  <Point X="-3.380649169922" Y="24.763423828125" />
                  <Point X="-3.372102783203" Y="24.771498046875" />
                  <Point X="-3.356077636719" Y="24.7886328125" />
                  <Point X="-3.342294677734" Y="24.807607421875" />
                  <Point X="-3.330956542969" Y="24.82814453125" />
                  <Point X="-3.325922607422" Y="24.838767578125" />
                  <Point X="-3.313211181641" Y="24.87053515625" />
                  <Point X="-3.31068359375" Y="24.877662109375" />
                  <Point X="-3.304252197266" Y="24.89939453125" />
                  <Point X="-3.298039550781" Y="24.9269921875" />
                  <Point X="-3.296273925781" Y="24.93761328125" />
                  <Point X="-3.293956054688" Y="24.95898828125" />
                  <Point X="-3.294072021484" Y="24.980490234375" />
                  <Point X="-3.296620605469" Y="25.001841796875" />
                  <Point X="-3.298500976562" Y="25.0124453125" />
                  <Point X="-3.306033447266" Y="25.044296875" />
                  <Point X="-3.308080322266" Y="25.051626953125" />
                  <Point X="-3.315382080078" Y="25.07324609375" />
                  <Point X="-3.326773925781" Y="25.100763671875" />
                  <Point X="-3.331933105469" Y="25.11132421875" />
                  <Point X="-3.343516113281" Y="25.131728515625" />
                  <Point X="-3.357530761719" Y="25.150546875" />
                  <Point X="-3.37376171875" Y="25.167490234375" />
                  <Point X="-3.382401855469" Y="25.175458984375" />
                  <Point X="-3.410073242188" Y="25.1979765625" />
                  <Point X="-3.416534912109" Y="25.202794921875" />
                  <Point X="-3.436708984375" Y="25.21612890625" />
                  <Point X="-3.464176269531" Y="25.231609375" />
                  <Point X="-3.474921386719" Y="25.2368046875" />
                  <Point X="-3.496977294922" Y="25.245806640625" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.144280761719" Y="25.42002734375" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.766764160156" Y="25.718076171875" />
                  <Point X="-4.731331542969" Y="25.957529296875" />
                  <Point X="-4.645705078125" Y="26.273515625" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-4.385770996094" Y="26.285611328125" />
                  <Point X="-3.778066162109" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057861328" Y="26.204365234375" />
                  <Point X="-3.736705322266" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704885986328" Y="26.2080546875" />
                  <Point X="-3.684603027344" Y="26.21208984375" />
                  <Point X="-3.674575927734" Y="26.21466015625" />
                  <Point X="-3.639479248047" Y="26.225724609375" />
                  <Point X="-3.613149902344" Y="26.23402734375" />
                  <Point X="-3.603458251953" Y="26.237673828125" />
                  <Point X="-3.584518798828" Y="26.246005859375" />
                  <Point X="-3.575270996094" Y="26.25069140625" />
                  <Point X="-3.556527832031" Y="26.261513671875" />
                  <Point X="-3.547854003906" Y="26.267173828125" />
                  <Point X="-3.531174804688" Y="26.279404296875" />
                  <Point X="-3.515925292969" Y="26.29337890625" />
                  <Point X="-3.502288330078" Y="26.3089296875" />
                  <Point X="-3.495895507812" Y="26.317076171875" />
                  <Point X="-3.483481689453" Y="26.3348046875" />
                  <Point X="-3.478011962891" Y="26.343599609375" />
                  <Point X="-3.468062744141" Y="26.361734375" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.449500488281" Y="26.405072265625" />
                  <Point X="-3.438935791016" Y="26.430578125" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.501900390625" />
                  <Point X="-3.421008056641" Y="26.522541015625" />
                  <Point X="-3.421910888672" Y="26.54320703125" />
                  <Point X="-3.425058105469" Y="26.563650390625" />
                  <Point X="-3.427189697266" Y="26.57378515625" />
                  <Point X="-3.432792236328" Y="26.59469140625" />
                  <Point X="-3.436013427734" Y="26.604533203125" />
                  <Point X="-3.443509033203" Y="26.62380859375" />
                  <Point X="-3.447783447266" Y="26.6332421875" />
                  <Point X="-3.464775634766" Y="26.665884765625" />
                  <Point X="-3.477523193359" Y="26.69037109375" />
                  <Point X="-3.482798828125" Y="26.699283203125" />
                  <Point X="-3.494290283203" Y="26.716482421875" />
                  <Point X="-3.500506103516" Y="26.72476953125" />
                  <Point X="-3.51441796875" Y="26.741349609375" />
                  <Point X="-3.521498779297" Y="26.74891015625" />
                  <Point X="-3.53644140625" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.909110107422" Y="27.04988671875" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.139971191406" Y="27.4444375" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.775475097656" Y="27.971853515625" />
                  <Point X="-3.726337890625" Y="28.035013671875" />
                  <Point X="-3.627392333984" Y="27.97788671875" />
                  <Point X="-3.254156738281" Y="27.7623984375" />
                  <Point X="-3.244921142578" Y="27.75771875" />
                  <Point X="-3.225991455078" Y="27.749388671875" />
                  <Point X="-3.216297607422" Y="27.745740234375" />
                  <Point X="-3.195653808594" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165334472656" Y="27.73262109375" />
                  <Point X="-3.155076171875" Y="27.73115625" />
                  <Point X="-3.106196044922" Y="27.72687890625" />
                  <Point X="-3.069526855469" Y="27.723671875" />
                  <Point X="-3.059173828125" Y="27.723333984375" />
                  <Point X="-3.038487060547" Y="27.72378515625" />
                  <Point X="-3.028153320312" Y="27.72457421875" />
                  <Point X="-3.006695800781" Y="27.727400390625" />
                  <Point X="-2.996511962891" Y="27.7293125" />
                  <Point X="-2.976422363281" Y="27.73423046875" />
                  <Point X="-2.956990722656" Y="27.7413046875" />
                  <Point X="-2.93844140625" Y="27.750453125" />
                  <Point X="-2.92941796875" Y="27.755533203125" />
                  <Point X="-2.911164550781" Y="27.767162109375" />
                  <Point X="-2.902748535156" Y="27.77319140625" />
                  <Point X="-2.886617919922" Y="27.78613671875" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.844207763672" Y="27.827748046875" />
                  <Point X="-2.8181796875" Y="27.853775390625" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753073974609" Y="28.093279296875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.949179443359" Y="28.50903125" />
                  <Point X="-3.059386474609" Y="28.699916015625" />
                  <Point X="-2.888159179688" Y="28.831193359375" />
                  <Point X="-2.648370605469" Y="29.015037109375" />
                  <Point X="-2.2911484375" Y="29.213501953125" />
                  <Point X="-2.192523925781" Y="29.268296875" />
                  <Point X="-2.118565673828" Y="29.171912109375" />
                  <Point X="-2.111818359375" Y="29.164046875" />
                  <Point X="-2.097514404297" Y="29.14910546875" />
                  <Point X="-2.089956542969" Y="29.14202734375" />
                  <Point X="-2.073376708984" Y="29.128115234375" />
                  <Point X="-2.065096435547" Y="29.12190234375" />
                  <Point X="-2.047899780273" Y="29.11041015625" />
                  <Point X="-2.038983398437" Y="29.105130859375" />
                  <Point X="-1.984580078125" Y="29.07680859375" />
                  <Point X="-1.943767333984" Y="29.0555625" />
                  <Point X="-1.934334716797" Y="29.0512890625" />
                  <Point X="-1.915061157227" Y="29.04379296875" />
                  <Point X="-1.905220214844" Y="29.0405703125" />
                  <Point X="-1.884313964844" Y="29.034966796875" />
                  <Point X="-1.874174926758" Y="29.032833984375" />
                  <Point X="-1.853724609375" Y="29.029685546875" />
                  <Point X="-1.8330546875" Y="29.028783203125" />
                  <Point X="-1.812407836914" Y="29.03013671875" />
                  <Point X="-1.802119750977" Y="29.031376953125" />
                  <Point X="-1.78080480957" Y="29.03513671875" />
                  <Point X="-1.77071484375" Y="29.037490234375" />
                  <Point X="-1.750860351562" Y="29.043279296875" />
                  <Point X="-1.741095825195" Y="29.04671484375" />
                  <Point X="-1.684431152344" Y="29.0701875" />
                  <Point X="-1.641921875" Y="29.087794921875" />
                  <Point X="-1.632583862305" Y="29.0922734375" />
                  <Point X="-1.614452880859" Y="29.102220703125" />
                  <Point X="-1.605659912109" Y="29.107689453125" />
                  <Point X="-1.58792980957" Y="29.120103515625" />
                  <Point X="-1.579781005859" Y="29.126498046875" />
                  <Point X="-1.564228515625" Y="29.14013671875" />
                  <Point X="-1.550253295898" Y="29.15538671875" />
                  <Point X="-1.538021484375" Y="29.172068359375" />
                  <Point X="-1.532361328125" Y="29.180744140625" />
                  <Point X="-1.521539306641" Y="29.19948828125" />
                  <Point X="-1.516856689453" Y="29.2087265625" />
                  <Point X="-1.508525634766" Y="29.22766015625" />
                  <Point X="-1.504877319336" Y="29.237353515625" />
                  <Point X="-1.486433837891" Y="29.29584765625" />
                  <Point X="-1.472598022461" Y="29.33973046875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465990966797" Y="29.37005078125" />
                  <Point X="-1.464526977539" Y="29.380306640625" />
                  <Point X="-1.462640625" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.56265625" />
                  <Point X="-1.241592163086" Y="29.629291015625" />
                  <Point X="-0.931165893555" Y="29.7163203125" />
                  <Point X="-0.49811920166" Y="29.76700390625" />
                  <Point X="-0.365222564697" Y="29.78255859375" />
                  <Point X="-0.336079650879" Y="29.673794921875" />
                  <Point X="-0.225666366577" Y="29.2617265625" />
                  <Point X="-0.220435150146" Y="29.247107421875" />
                  <Point X="-0.207661453247" Y="29.218916015625" />
                  <Point X="-0.200119140625" Y="29.20534375" />
                  <Point X="-0.182260910034" Y="29.1786171875" />
                  <Point X="-0.172608886719" Y="29.166455078125" />
                  <Point X="-0.151451385498" Y="29.143865234375" />
                  <Point X="-0.126896499634" Y="29.1250234375" />
                  <Point X="-0.099600570679" Y="29.11043359375" />
                  <Point X="-0.085354026794" Y="29.1042578125" />
                  <Point X="-0.054916057587" Y="29.09392578125" />
                  <Point X="-0.039853721619" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629489899" Y="29.08511328125" />
                  <Point X="0.052165550232" Y="29.090154296875" />
                  <Point X="0.067227882385" Y="29.09392578125" />
                  <Point X="0.097665855408" Y="29.1042578125" />
                  <Point X="0.111912246704" Y="29.11043359375" />
                  <Point X="0.139208175659" Y="29.1250234375" />
                  <Point X="0.163763214111" Y="29.143865234375" />
                  <Point X="0.184920715332" Y="29.166455078125" />
                  <Point X="0.194572891235" Y="29.1786171875" />
                  <Point X="0.212431121826" Y="29.20534375" />
                  <Point X="0.219973724365" Y="29.218916015625" />
                  <Point X="0.232747116089" Y="29.247107421875" />
                  <Point X="0.23797819519" Y="29.2617265625" />
                  <Point X="0.320809295654" Y="29.570857421875" />
                  <Point X="0.378190612793" Y="29.7850078125" />
                  <Point X="0.556826171875" Y="29.766298828125" />
                  <Point X="0.827852661133" Y="29.737916015625" />
                  <Point X="1.186165161133" Y="29.651408203125" />
                  <Point X="1.453622314453" Y="29.586833984375" />
                  <Point X="1.685202392578" Y="29.50283984375" />
                  <Point X="1.858247558594" Y="29.44007421875" />
                  <Point X="2.083765625" Y="29.334607421875" />
                  <Point X="2.250427978516" Y="29.2566640625" />
                  <Point X="2.468355224609" Y="29.12969921875" />
                  <Point X="2.629450927734" Y="29.035845703125" />
                  <Point X="2.817780517578" Y="28.901916015625" />
                  <Point X="2.557613525391" Y="28.451294921875" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.0531796875" Y="27.57343359375" />
                  <Point X="2.044181640625" Y="27.549560546875" />
                  <Point X="2.041301635742" Y="27.540595703125" />
                  <Point X="2.029034423828" Y="27.49472265625" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017912719727" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.018194580078" Y="27.329912109375" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029143920898" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040735717773" Y="27.234216796875" />
                  <Point X="2.045319091797" Y="27.22388671875" />
                  <Point X="2.055680664062" Y="27.20384375" />
                  <Point X="2.061458740234" Y="27.194130859375" />
                  <Point X="2.086003417969" Y="27.15795703125" />
                  <Point X="2.104416503906" Y="27.130822265625" />
                  <Point X="2.109807373047" Y="27.123634765625" />
                  <Point X="2.130470703125" Y="27.10011328125" />
                  <Point X="2.157598632812" Y="27.075384765625" />
                  <Point X="2.168255371094" Y="27.066982421875" />
                  <Point X="2.204427734375" Y="27.0424375" />
                  <Point X="2.231563964844" Y="27.0240234375" />
                  <Point X="2.241278320312" Y="27.018244140625" />
                  <Point X="2.261320556641" Y="27.0078828125" />
                  <Point X="2.2716484375" Y="27.00330078125" />
                  <Point X="2.293738525391" Y="26.995033203125" />
                  <Point X="2.304543945312" Y="26.99170703125" />
                  <Point X="2.326470458984" Y="26.98636328125" />
                  <Point X="2.337591552734" Y="26.984345703125" />
                  <Point X="2.377258544922" Y="26.9795625" />
                  <Point X="2.407016357422" Y="26.975974609375" />
                  <Point X="2.416040039062" Y="26.9753203125" />
                  <Point X="2.447579589844" Y="26.97549609375" />
                  <Point X="2.484318115234" Y="26.979822265625" />
                  <Point X="2.497750976562" Y="26.98239453125" />
                  <Point X="2.543623779297" Y="26.994662109375" />
                  <Point X="2.578037353516" Y="27.003865234375" />
                  <Point X="2.584005371094" Y="27.005673828125" />
                  <Point X="2.604415039063" Y="27.013072265625" />
                  <Point X="2.627659912109" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.275719970703" Y="27.397197265625" />
                  <Point X="3.940403564453" Y="27.780953125" />
                  <Point X="3.948405029297" Y="27.76983203125" />
                  <Point X="4.043952880859" Y="27.63704296875" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.8211171875" Y="27.24117578125" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116943359" Y="26.725212890625" />
                  <Point X="3.134668457031" Y="26.7066015625" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.095561523438" Y="26.6563515625" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065634765625" Y="26.6166015625" />
                  <Point X="3.049740234375" Y="26.589373046875" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.017842285156" Y="26.498697265625" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.014794921875" Y="26.303642578125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034684814453" Y="26.22860546875" />
                  <Point X="3.050287841797" Y="26.195369140625" />
                  <Point X="3.056919677734" Y="26.183525390625" />
                  <Point X="3.084378173828" Y="26.141791015625" />
                  <Point X="3.104977294922" Y="26.11048046875" />
                  <Point X="3.111737548828" Y="26.101427734375" />
                  <Point X="3.126287841797" Y="26.084185546875" />
                  <Point X="3.134077880859" Y="26.07599609375" />
                  <Point X="3.151320556641" Y="26.05990625" />
                  <Point X="3.160030761719" Y="26.05269921875" />
                  <Point X="3.178243652344" Y="26.03937109375" />
                  <Point X="3.187746337891" Y="26.03325" />
                  <Point X="3.227537353516" Y="26.0108515625" />
                  <Point X="3.257388427734" Y="25.994046875" />
                  <Point X="3.265474853516" Y="25.98998828125" />
                  <Point X="3.294680664062" Y="25.97808203125" />
                  <Point X="3.330279541016" Y="25.96801953125" />
                  <Point X="3.343674560547" Y="25.965255859375" />
                  <Point X="3.397474853516" Y="25.958146484375" />
                  <Point X="3.437835205078" Y="25.9528125" />
                  <Point X="3.444033447266" Y="25.95219921875" />
                  <Point X="3.465708251953" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.108262695312" Y="26.032796875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.711646972656" Y="26.082798828125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.434803710938" Y="25.620390625" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686020263672" Y="25.419541015625" />
                  <Point X="3.665627441406" Y="25.41213671875" />
                  <Point X="3.642386230469" Y="25.40162109375" />
                  <Point X="3.634008056641" Y="25.397318359375" />
                  <Point X="3.581151123047" Y="25.366767578125" />
                  <Point X="3.541498046875" Y="25.34384765625" />
                  <Point X="3.533881103516" Y="25.3389453125" />
                  <Point X="3.508776611328" Y="25.319873046875" />
                  <Point X="3.481993896484" Y="25.2943515625" />
                  <Point X="3.472795654297" Y="25.284224609375" />
                  <Point X="3.441081542969" Y="25.2438125" />
                  <Point X="3.417289794922" Y="25.21349609375" />
                  <Point X="3.410854736328" Y="25.204208984375" />
                  <Point X="3.399130615234" Y="25.1849296875" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.38000390625" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.359805175781" Y="25.0552734375" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.362446289062" Y="24.868376953125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.816013671875" />
                  <Point X="3.380004394531" Y="24.794515625" />
                  <Point X="3.384067382812" Y="24.783970703125" />
                  <Point X="3.393841064453" Y="24.762505859375" />
                  <Point X="3.399128662109" Y="24.752515625" />
                  <Point X="3.410853027344" Y="24.733234375" />
                  <Point X="3.417289794922" Y="24.723943359375" />
                  <Point X="3.44900390625" Y="24.68353125" />
                  <Point X="3.472795654297" Y="24.65321484375" />
                  <Point X="3.478718261719" Y="24.646365234375" />
                  <Point X="3.501141357422" Y="24.624193359375" />
                  <Point X="3.530176269531" Y="24.601275390625" />
                  <Point X="3.541494140625" Y="24.593595703125" />
                  <Point X="3.594351318359" Y="24.56304296875" />
                  <Point X="3.634004150391" Y="24.540123046875" />
                  <Point X="3.639497558594" Y="24.53718359375" />
                  <Point X="3.659150146484" Y="24.527986328125" />
                  <Point X="3.683021972656" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.249242675781" Y="24.36676953125" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.784526855469" Y="24.22092578125" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.298234863281" Y="23.977333984375" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354480957031" Y="24.087322265625" />
                  <Point X="3.250741210938" Y="24.0647734375" />
                  <Point X="3.172916748047" Y="24.047859375" />
                  <Point X="3.157877197266" Y="24.0432578125" />
                  <Point X="3.1287578125" Y="24.031634765625" />
                  <Point X="3.114677978516" Y="24.02461328125" />
                  <Point X="3.086850585938" Y="24.007720703125" />
                  <Point X="3.074122314453" Y="23.99846875" />
                  <Point X="3.050371582031" Y="23.97799609375" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.976645019531" Y="23.891361328125" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.866170166016" Y="23.60567578125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.953951904297" Y="23.291328125" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486328125" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.569927734375" Y="22.76691015625" />
                  <Point X="4.087169677734" Y="22.370015625" />
                  <Point X="4.045491699219" Y="22.30257421875" />
                  <Point X="4.001273681641" Y="22.23974609375" />
                  <Point X="3.615694335938" Y="22.462361328125" />
                  <Point X="2.848454589844" Y="22.905328125" />
                  <Point X="2.841190917969" Y="22.909115234375" />
                  <Point X="2.815037597656" Y="22.920482421875" />
                  <Point X="2.783124511719" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.9336640625" />
                  <Point X="2.647641845703" Y="22.955962890625" />
                  <Point X="2.555018310547" Y="22.972689453125" />
                  <Point X="2.539352783203" Y="22.974193359375" />
                  <Point X="2.50799609375" Y="22.974595703125" />
                  <Point X="2.492304931641" Y="22.973494140625" />
                  <Point X="2.460133300781" Y="22.96853515625" />
                  <Point X="2.444841796875" Y="22.964861328125" />
                  <Point X="2.415069580078" Y="22.9550390625" />
                  <Point X="2.400588867188" Y="22.948890625" />
                  <Point X="2.298018310547" Y="22.894908203125" />
                  <Point X="2.221070800781" Y="22.854412109375" />
                  <Point X="2.208970214844" Y="22.846830078125" />
                  <Point X="2.186041748047" Y="22.829939453125" />
                  <Point X="2.175213867187" Y="22.820630859375" />
                  <Point X="2.154253662109" Y="22.799671875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128045898438" Y="22.76590625" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.066481933594" Y="22.651234375" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.024485717773" Y="22.296392578125" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.401856201172" Y="21.55084375" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723753173828" Y="20.963916015625" />
                  <Point X="2.419389404297" Y="21.3605703125" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828658569336" Y="22.12984765625" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251464844" Y="22.17199609375" />
                  <Point X="1.773298217773" Y="22.179353515625" />
                  <Point X="1.651526733398" Y="22.257640625" />
                  <Point X="1.560174926758" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470581055" Y="22.336126953125" />
                  <Point X="1.50254699707" Y="22.3411171875" />
                  <Point X="1.470926269531" Y="22.34884765625" />
                  <Point X="1.455384887695" Y="22.3513046875" />
                  <Point X="1.424119873047" Y="22.35362109375" />
                  <Point X="1.408396240234" Y="22.35348046875" />
                  <Point X="1.275218261719" Y="22.3412265625" />
                  <Point X="1.175309448242" Y="22.332033203125" />
                  <Point X="1.1612265625" Y="22.32966015625" />
                  <Point X="1.133575317383" Y="22.322828125" />
                  <Point X="1.120006713867" Y="22.318369140625" />
                  <Point X="1.092621337891" Y="22.307025390625" />
                  <Point X="1.079875488281" Y="22.300583984375" />
                  <Point X="1.055493530273" Y="22.28586328125" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.94102142334" Y="22.192078125" />
                  <Point X="0.863874084473" Y="22.127931640625" />
                  <Point X="0.852652160645" Y="22.116908203125" />
                  <Point X="0.83218145752" Y="22.093158203125" />
                  <Point X="0.822932800293" Y="22.080431640625" />
                  <Point X="0.806040771484" Y="22.05260546875" />
                  <Point X="0.799019348145" Y="22.038529296875" />
                  <Point X="0.787394836426" Y="22.00941015625" />
                  <Point X="0.782791809082" Y="21.9943671875" />
                  <Point X="0.752044128418" Y="21.852904296875" />
                  <Point X="0.728977539063" Y="21.74678125" />
                  <Point X="0.727584655762" Y="21.7387109375" />
                  <Point X="0.72472454834" Y="21.710322265625" />
                  <Point X="0.724742248535" Y="21.676830078125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.819728759766" Y="20.949158203125" />
                  <Point X="0.833091308594" Y="20.84766015625" />
                  <Point X="0.655064880371" Y="21.512064453125" />
                  <Point X="0.652606079102" Y="21.519876953125" />
                  <Point X="0.642146728516" Y="21.546416015625" />
                  <Point X="0.626788146973" Y="21.57618359375" />
                  <Point X="0.620407470703" Y="21.58679296875" />
                  <Point X="0.526859069824" Y="21.721578125" />
                  <Point X="0.456679748535" Y="21.822693359375" />
                  <Point X="0.446668518066" Y="21.83483203125" />
                  <Point X="0.424785430908" Y="21.85728515625" />
                  <Point X="0.412913726807" Y="21.867599609375" />
                  <Point X="0.386658599854" Y="21.88684375" />
                  <Point X="0.373244659424" Y="21.8950625" />
                  <Point X="0.34524230957" Y="21.909171875" />
                  <Point X="0.330653930664" Y="21.9150625" />
                  <Point X="0.185893157959" Y="21.959990234375" />
                  <Point X="0.07729486084" Y="21.9936953125" />
                  <Point X="0.063377067566" Y="21.996890625" />
                  <Point X="0.035218082428" Y="22.00116015625" />
                  <Point X="0.020976737976" Y="22.002234375" />
                  <Point X="-0.008664166451" Y="22.002234375" />
                  <Point X="-0.022904914856" Y="22.00116015625" />
                  <Point X="-0.051065090179" Y="21.996890625" />
                  <Point X="-0.064984512329" Y="21.9936953125" />
                  <Point X="-0.209745147705" Y="21.948765625" />
                  <Point X="-0.318343444824" Y="21.9150625" />
                  <Point X="-0.332928405762" Y="21.909171875" />
                  <Point X="-0.360928649902" Y="21.895064453125" />
                  <Point X="-0.374343963623" Y="21.88684765625" />
                  <Point X="-0.400599822998" Y="21.867603515625" />
                  <Point X="-0.412476593018" Y="21.857283203125" />
                  <Point X="-0.434360870361" Y="21.834826171875" />
                  <Point X="-0.444368377686" Y="21.822689453125" />
                  <Point X="-0.537916687012" Y="21.68790234375" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.612469482422" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777832031" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.814455749512" Y="20.871259765625" />
                  <Point X="-0.985424804688" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.918383517886" Y="22.287602944583" />
                  <Point X="4.019572315123" Y="22.421885013962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.835493354132" Y="22.335459795416" />
                  <Point X="3.944200935546" Y="22.479719628403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.703196136133" Y="20.990706449142" />
                  <Point X="2.715684288567" Y="21.007278787162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.752603190377" Y="22.383316646249" />
                  <Point X="3.868829555969" Y="22.537554242843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.643181058233" Y="21.068919564202" />
                  <Point X="2.664082104561" Y="21.096656189499" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.669713026623" Y="22.431173497082" />
                  <Point X="3.793458176392" Y="22.595388857284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.583165980333" Y="21.147132679262" />
                  <Point X="2.612479920555" Y="21.186033591836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.58682284195" Y="22.479030320155" />
                  <Point X="3.718086796815" Y="22.653223471725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.678182642897" Y="23.927313692526" />
                  <Point X="4.751493920228" Y="24.024601043475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523150902432" Y="21.225345794322" />
                  <Point X="2.560877736549" Y="21.275410994172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.503932618138" Y="22.52688709129" />
                  <Point X="3.642715417239" Y="22.711058086166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.569965513722" Y="23.941560525051" />
                  <Point X="4.782676571053" Y="24.223837632184" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.463135824532" Y="21.303558909382" />
                  <Point X="2.509275552542" Y="21.364788396509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.421042394326" Y="22.574743862424" />
                  <Point X="3.567344038418" Y="22.76889270161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.461748384547" Y="23.955807357576" />
                  <Point X="4.683706888446" Y="24.250356240789" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.403120757739" Y="21.381772039182" />
                  <Point X="2.457673368536" Y="21.454165798846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.338152170515" Y="22.622600633558" />
                  <Point X="3.491972680891" Y="22.826727345313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.353531255373" Y="23.970054190101" />
                  <Point X="4.584737205838" Y="24.276874849393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.343105720815" Y="21.459985208619" />
                  <Point X="2.40607118453" Y="21.543543201183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.255261946703" Y="22.670457404693" />
                  <Point X="3.416601323365" Y="22.884561989016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.245314187674" Y="23.984301104207" />
                  <Point X="4.48576752323" Y="24.303393457998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.283090683891" Y="21.538198378056" />
                  <Point X="2.354469019823" Y="21.632920629131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172371722892" Y="22.718314175827" />
                  <Point X="3.341229965839" Y="22.942396632719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.13709718421" Y="23.998548103556" />
                  <Point X="4.386797840622" Y="24.329912066602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.223075646966" Y="21.616411547493" />
                  <Point X="2.302866856833" Y="21.722298059358" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.08948149908" Y="22.766170946962" />
                  <Point X="3.265858608313" Y="23.000231276422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.028880180747" Y="24.012795102905" />
                  <Point X="4.287828158014" Y="24.356430675207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.163060610042" Y="21.694624716929" />
                  <Point X="2.251264693843" Y="21.811675489584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.006591275268" Y="22.814027718096" />
                  <Point X="3.190487250787" Y="23.058065920125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920663177284" Y="24.027042102254" />
                  <Point X="4.188858546053" Y="24.382949377562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103045573117" Y="21.772837886366" />
                  <Point X="2.199662530853" Y="21.901052919811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.923701051457" Y="22.861884489231" />
                  <Point X="3.11511589326" Y="23.115900563828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.81244617382" Y="24.041289101604" />
                  <Point X="4.089888979235" Y="24.409468139826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043030536193" Y="21.851051055803" />
                  <Point X="2.148060367864" Y="21.990430350037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.840549245224" Y="22.909394128768" />
                  <Point X="3.040554380224" Y="23.174809907467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.704229170357" Y="24.055536100953" />
                  <Point X="3.990919412417" Y="24.435986902089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.983015499268" Y="21.929264225239" />
                  <Point X="2.096458204874" Y="22.079807780264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.743625427942" Y="22.938627692358" />
                  <Point X="2.979481424764" Y="23.25161917159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.596012166893" Y="24.069783100302" />
                  <Point X="3.8919498456" Y="24.462505664352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.923000462344" Y="22.007477394676" />
                  <Point X="2.04925852579" Y="22.175027503961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.638922141523" Y="22.957537551716" />
                  <Point X="2.924717259092" Y="23.33680048253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.48779516343" Y="24.084030099651" />
                  <Point X="3.792980278782" Y="24.489024426615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.698838793437" Y="25.691139277608" />
                  <Point X="4.772250598735" Y="25.788560033675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.86298542542" Y="22.085690564113" />
                  <Point X="2.023958910642" Y="22.299309594095" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.532585735883" Y="22.974280188669" />
                  <Point X="2.874998220783" Y="23.428676903613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.373110355462" Y="24.089694032526" />
                  <Point X="3.694010711964" Y="24.515543188878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.549790695494" Y="25.651201584467" />
                  <Point X="4.75154032416" Y="25.918932384452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827325347892" Y="20.869179034678" />
                  <Point X="0.829822075377" Y="20.872492303958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.799022678559" Y="22.158664945522" />
                  <Point X="2.001470812353" Y="22.427322693119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390498798131" Y="22.943580267112" />
                  <Point X="2.862935674187" Y="23.570525177025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.232369661883" Y="24.060780637326" />
                  <Point X="3.6058451577" Y="24.556399360053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.400742622492" Y="25.611263924425" />
                  <Point X="4.722494186823" Y="26.038242671718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796122991578" Y="20.985627922715" />
                  <Point X="0.812130754347" Y="21.006870941404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.720997445529" Y="22.21297777748" />
                  <Point X="2.101762310856" Y="22.718269820268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.185671671823" Y="22.829621303224" />
                  <Point X="2.892585040626" Y="23.767727028628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052262183902" Y="23.979625754744" />
                  <Point X="3.524239793206" Y="24.605961197091" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.251694633691" Y="25.57132637612" />
                  <Point X="4.653532691497" Y="26.104583489862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764920635264" Y="21.102076810753" />
                  <Point X="0.794439506551" Y="21.141249676035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.640865434112" Y="22.26449482009" />
                  <Point X="3.456521587748" Y="24.673951916615" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.10264664489" Y="25.531388827815" />
                  <Point X="4.521479037519" Y="26.08719818558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733718278949" Y="21.21852569879" />
                  <Point X="0.776748258755" Y="21.275628410666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.560733509811" Y="22.316011978306" />
                  <Point X="3.398155802784" Y="24.754353717326" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953598656089" Y="25.49145127951" />
                  <Point X="4.389425383541" Y="26.069812881299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702515922635" Y="21.334974586827" />
                  <Point X="0.759057010959" Y="21.410007145297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.466992701764" Y="22.34946953782" />
                  <Point X="3.362989239364" Y="24.865541924852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.804550667287" Y="25.451513731205" />
                  <Point X="4.257371729562" Y="26.052427577017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.671313566321" Y="21.451423474865" />
                  <Point X="0.741365763163" Y="21.544385879928" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.346790815813" Y="22.347812060926" />
                  <Point X="3.350955220574" Y="25.00742805594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.650905691228" Y="25.405475774764" />
                  <Point X="4.125318075584" Y="26.035042272736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.634748270685" Y="21.560755502047" />
                  <Point X="0.724740478569" Y="21.680179195506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.218975518413" Y="22.336051245794" />
                  <Point X="3.993264478456" Y="26.017657043897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.579658368266" Y="21.645504545725" />
                  <Point X="0.758569998082" Y="21.8829282976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.067804134514" Y="22.29329585702" />
                  <Point X="3.861210889759" Y="26.000271826247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.522626482039" Y="21.727676489847" />
                  <Point X="3.729157301062" Y="25.982886608598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.465594705339" Y="21.809848579316" />
                  <Point X="3.597103712365" Y="25.965501390948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.39825615842" Y="21.878343122738" />
                  <Point X="3.467432211185" Y="25.951277310202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.311461805115" Y="21.921018939045" />
                  <Point X="3.357623877942" Y="25.963412543607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.067015769011" Y="20.249573225989" />
                  <Point X="-0.933594161659" Y="20.426629679117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.215055615761" Y="21.950939418098" />
                  <Point X="3.260557165541" Y="25.99245647897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.134825170618" Y="20.317442924135" />
                  <Point X="-0.867957692882" Y="20.671588028524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.118649681646" Y="21.980860235864" />
                  <Point X="3.177414313345" Y="26.039978000916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.121985044624" Y="20.492338160251" />
                  <Point X="-0.802321303496" Y="20.916546272575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.015803363235" Y="22.002234375" />
                  <Point X="3.108268108834" Y="26.106073701691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.146325612337" Y="20.617892949319" />
                  <Point X="-0.736685264156" Y="21.1615040521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.123199661438" Y="21.975626944325" />
                  <Point X="3.052980487421" Y="26.190560363401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.876972633283" Y="27.284034873624" />
                  <Point X="4.088059059223" Y="27.564156022082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.171167534384" Y="20.742782418714" />
                  <Point X="-0.671049224816" Y="21.406461831624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.278465048962" Y="21.927438629242" />
                  <Point X="3.01575066711" Y="26.299010536555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.594945327474" Y="27.067627811301" />
                  <Point X="4.034221617866" Y="27.650567137726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.202853810626" Y="20.858589123317" />
                  <Point X="3.004190826521" Y="26.441525923369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.312917400892" Y="26.851219925185" />
                  <Point X="3.976118245821" Y="27.731317172142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.260604610958" Y="20.939807036199" />
                  <Point X="3.859270586294" Y="27.734110904055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.332197892996" Y="21.002655355414" />
                  <Point X="3.648710158221" Y="27.61254359175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.403819676028" Y="21.065465852533" />
                  <Point X="3.438149730149" Y="27.490976279445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.475441274366" Y="21.128276594749" />
                  <Point X="3.227589424892" Y="27.369409130121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.554826473867" Y="21.180784690245" />
                  <Point X="3.017029534106" Y="27.247842530819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.655490967655" Y="21.205054208449" />
                  <Point X="2.80646964332" Y="27.126275931518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.768845301482" Y="21.212483740143" />
                  <Point X="2.601383606" Y="27.011973381112" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.88219973431" Y="21.219913140457" />
                  <Point X="2.45566014104" Y="26.976447624955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.001687543133" Y="21.219203275918" />
                  <Point X="2.342236786057" Y="26.983785562481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.199342624915" Y="21.114761936579" />
                  <Point X="2.247015338854" Y="27.015278247469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.519572585447" Y="20.847658239134" />
                  <Point X="2.167483150415" Y="27.067591282056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.600681714099" Y="20.897878603376" />
                  <Point X="2.100513010058" Y="27.136574717498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.68179047462" Y="20.948099456145" />
                  <Point X="2.046147260877" Y="27.22228474498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761798273485" Y="20.99978133438" />
                  <Point X="2.016829244632" Y="27.34123423675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.837074911606" Y="21.057741674979" />
                  <Point X="2.037558663951" Y="27.526598918719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.912351549726" Y="21.115702015579" />
                  <Point X="2.368223642777" Y="28.123261979968" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.945829660624" Y="21.229130875281" />
                  <Point X="2.757987793979" Y="28.798352291881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.556068096593" Y="21.904217753902" />
                  <Point X="2.752212679844" Y="28.94854426998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.31144482041" Y="22.386699619214" />
                  <Point X="2.674763663695" Y="29.003621767566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.339594374349" Y="22.507199712835" />
                  <Point X="2.595151342513" Y="29.055828462411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399721593398" Y="22.585264011566" />
                  <Point X="2.512488722131" Y="29.103987273497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.481072950638" Y="22.635162927614" />
                  <Point X="2.429826246905" Y="29.152146277214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.588751626464" Y="22.650124311868" />
                  <Point X="2.347163937952" Y="29.200305501581" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.762644825291" Y="22.577216056255" />
                  <Point X="2.264501628999" Y="29.248464725948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973204892694" Y="22.455649222575" />
                  <Point X="2.177447053964" Y="29.290795216356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.183764960096" Y="22.334082388895" />
                  <Point X="2.08949123346" Y="29.331929713632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.394325323855" Y="22.212515161937" />
                  <Point X="2.00153505808" Y="29.373063739972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.604885793944" Y="22.090947793874" />
                  <Point X="1.913578857991" Y="29.41419773352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.799435067368" Y="21.990628001433" />
                  <Point X="1.823596300067" Y="29.452642659398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.859209743032" Y="22.069160141036" />
                  <Point X="1.730177072309" Y="29.486526970368" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.91898455114" Y="22.14769210488" />
                  <Point X="1.636757495187" Y="29.520410817717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.978759369337" Y="22.226224055335" />
                  <Point X="-3.11084299771" Y="23.377987981902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.954272377875" Y="23.585764212172" />
                  <Point X="1.543337593724" Y="29.554294234651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.03436253086" Y="22.310291981176" />
                  <Point X="-3.392870318912" Y="23.161580899153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963394414758" Y="23.731514673771" />
                  <Point X="1.449631280357" Y="29.587797570151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.086913000447" Y="22.398410966045" />
                  <Point X="-3.674897640113" Y="22.945173816403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.019154507795" Y="23.815374344459" />
                  <Point X="1.348988895628" Y="29.612096428067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.139463470033" Y="22.486529950913" />
                  <Point X="-3.956924610415" Y="22.728767199313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.096155238335" Y="23.871046737142" />
                  <Point X="1.248346510898" Y="29.636395285983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.193988390193" Y="23.899073572993" />
                  <Point X="1.147703968426" Y="29.660693934567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.319887671989" Y="23.889855396447" />
                  <Point X="1.047061170925" Y="29.684992244717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451941033491" Y="23.872470480294" />
                  <Point X="0.946418373425" Y="29.709290554867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.583994394993" Y="23.855085564142" />
                  <Point X="0.845775575924" Y="29.733588865017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716047756495" Y="23.83770064799" />
                  <Point X="0.737234535544" Y="29.747405852854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.848101117997" Y="23.820315731837" />
                  <Point X="0.163094509532" Y="29.143352117856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.219798797112" Y="29.218601249053" />
                  <Point X="0.626982183088" Y="29.758951852863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980154520968" Y="23.802930760653" />
                  <Point X="0.000255510809" Y="29.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.295713889395" Y="29.477199792557" />
                  <Point X="0.516730091373" Y="29.770498198886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11220802613" Y="23.785545653859" />
                  <Point X="-3.357364510044" Y="24.787256833014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.349392565946" Y="24.797835960148" />
                  <Point X="-0.099612391499" Y="29.110439912052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.361349997514" Y="29.722157663354" />
                  <Point X="0.406478455871" Y="29.782045150324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.244261531291" Y="23.768160547064" />
                  <Point X="-3.562111530016" Y="24.673404173824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.300441695272" Y="25.020651772996" />
                  <Point X="-0.174530351254" Y="29.16887623492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.376315036452" Y="23.75077544027" />
                  <Point X="-3.711159232098" Y="24.633467006009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.340154539236" Y="25.125806862468" />
                  <Point X="-0.224973851609" Y="29.259791262396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.508368541614" Y="23.733390333475" />
                  <Point X="-3.86020693418" Y="24.593529838194" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.4067571183" Y="25.195278068222" />
                  <Point X="-0.256304532544" Y="29.37606985791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640422046775" Y="23.716005226681" />
                  <Point X="-4.009254636261" Y="24.553592670379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.489831681788" Y="25.242890212343" />
                  <Point X="-0.287506889586" Y="29.492518744982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.686359408376" Y="23.812900102256" />
                  <Point X="-4.158302338343" Y="24.513655502564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.587686645152" Y="25.270888103347" />
                  <Point X="-0.318709246628" Y="29.608967632054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.716473045756" Y="23.930793769118" />
                  <Point X="-4.307350040424" Y="24.473718334748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686656145552" Y="25.297406953751" />
                  <Point X="-0.349911534387" Y="29.725416611067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.744432723361" Y="24.051545837144" />
                  <Point X="-4.456398106818" Y="24.433780683475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.785625645951" Y="25.323925804154" />
                  <Point X="-0.431664977433" Y="29.774781941229" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.763408115928" Y="24.184220454107" />
                  <Point X="-4.605446350081" Y="24.393842797488" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.88459514635" Y="25.350444654557" />
                  <Point X="-0.562123976374" Y="29.759512815658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.782383508494" Y="24.31689507107" />
                  <Point X="-4.754494593344" Y="24.3539049115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.98356464675" Y="25.376963504961" />
                  <Point X="-0.692582758872" Y="29.744243977315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.082534147149" Y="25.403482355364" />
                  <Point X="-0.82304154137" Y="29.728975138973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.181503690051" Y="25.430001149365" />
                  <Point X="-3.553352767354" Y="26.263585578525" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.45287644835" Y="26.396922157356" />
                  <Point X="-0.956985153516" Y="29.709081775492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.280473303456" Y="25.456519849804" />
                  <Point X="-3.715263010409" Y="26.206579242318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.430170808486" Y="26.584909372565" />
                  <Point X="-1.107799352594" Y="29.666800386986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.379442916861" Y="25.483038550244" />
                  <Point X="-3.829815753145" Y="26.212418431675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.47430738742" Y="26.684193967453" />
                  <Point X="-1.672657447626" Y="29.07506419043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.472278948618" Y="29.340975439903" />
                  <Point X="-1.258613680601" Y="29.624518827384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.478412530267" Y="25.509557250683" />
                  <Point X="-3.93803272955" Y="26.226665466933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534856847277" Y="26.761697933705" />
                  <Point X="-1.826144184133" Y="29.029236224967" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.471074007933" Y="29.500430263606" />
                  <Point X="-1.409429022032" Y="29.582235922924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.577382143672" Y="25.536075951122" />
                  <Point X="-4.046249705954" Y="26.240912502191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.609744667965" Y="26.820174252465" />
                  <Point X="-2.869671585329" Y="27.802284404398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.76119854069" Y="27.946232996571" />
                  <Point X="-1.929806268293" Y="29.049527806391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.676351757077" Y="25.562594651561" />
                  <Point X="-4.154466682358" Y="26.255159537449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.685115950694" Y="26.878008995427" />
                  <Point X="-3.047933143484" Y="27.723579140161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754766366111" Y="28.112624593945" />
                  <Point X="-2.015910658521" Y="29.093119234627" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.775321370483" Y="25.589113352001" />
                  <Point X="-4.262683658762" Y="26.269406572706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.760487233423" Y="26.935843738389" />
                  <Point X="-3.160583640952" Y="27.731942694249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.786909938461" Y="28.227824446116" />
                  <Point X="-2.094677182614" Y="29.146448340119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.759353143337" Y="25.768159718552" />
                  <Point X="-4.370900635167" Y="26.283653607964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.835858516152" Y="26.993678481352" />
                  <Point X="-3.255849860235" Y="27.763375964681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.838463735562" Y="28.317266060045" />
                  <Point X="-2.15684717415" Y="29.221801788198" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.728931386079" Y="25.966386567392" />
                  <Point X="-4.479117745937" Y="26.297900464913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.911229810532" Y="27.051513208852" />
                  <Point X="-3.338740053969" Y="27.81123277573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890065859758" Y="28.406643541753" />
                  <Point X="-2.275505048186" Y="29.22219328432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662135126957" Y="26.21288401057" />
                  <Point X="-4.587334878111" Y="26.312147293457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.986601507552" Y="27.109347402033" />
                  <Point X="-3.421630247703" Y="27.859089586779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941667983953" Y="28.496021023461" />
                  <Point X="-2.480123124045" Y="29.108511739749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.061973204571" Y="27.167181595213" />
                  <Point X="-3.504520441437" Y="27.906946397827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993270025874" Y="28.585398614352" />
                  <Point X="-2.698443426327" Y="28.976646726557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.13734490159" Y="27.225015788394" />
                  <Point X="-3.587410635171" Y="27.954803208876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.044872053778" Y="28.674776223843" />
                  <Point X="-2.980149791882" Y="28.760665566336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.212716598609" Y="27.282849981575" />
                  <Point X="-3.670300704174" Y="28.002660185449" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.700417602539" Y="20.608701171875" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.370770324707" Y="21.6132421875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.7336015625" />
                  <Point X="0.129575042725" Y="21.778529296875" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664148331" Y="21.812234375" />
                  <Point X="-0.153424865723" Y="21.7673046875" />
                  <Point X="-0.262023132324" Y="21.7336015625" />
                  <Point X="-0.288278839111" Y="21.714357421875" />
                  <Point X="-0.381827331543" Y="21.5795703125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.63092980957" Y="20.822083984375" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.973242553711" Y="20.037283203125" />
                  <Point X="-1.10025793457" Y="20.0619375" />
                  <Point X="-1.26773815918" Y="20.10502734375" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.336701904297" Y="20.239685546875" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.344266601562" Y="20.639103515625" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.519559936523" Y="20.914251953125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.826129638672" Y="21.025830078125" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.137271728516" Y="20.927724609375" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.356131835938" Y="20.717076171875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.669674072266" Y="20.717123046875" />
                  <Point X="-2.855839111328" Y="20.832392578125" />
                  <Point X="-3.08776171875" Y="21.01096484375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.931378662109" Y="21.63416015625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513981445312" Y="22.43123828125" />
                  <Point X="-2.531330078125" Y="22.4485859375" />
                  <Point X="-2.560158447266" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.144363769531" Y="22.137439453125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.014627441406" Y="21.959642578125" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.327977539062" Y="22.431685546875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.908002685547" Y="23.005796875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822265625" Y="23.603984375" />
                  <Point X="-3.138117675781" Y="23.633732421875" />
                  <Point X="-3.140325927734" Y="23.66540234375" />
                  <Point X="-3.161157714844" Y="23.689359375" />
                  <Point X="-3.187645019531" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.917235595703" Y="23.61957421875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.869870117188" Y="23.763609375" />
                  <Point X="-4.927393066406" Y="23.98880859375" />
                  <Point X="-4.971384765625" Y="24.296396484375" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.405237792969" Y="24.644193359375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.526039550781" Y="24.889580078125" />
                  <Point X="-3.502324951172" Y="24.909353515625" />
                  <Point X="-3.489613525391" Y="24.94112109375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.490933349609" Y="25.0005703125" />
                  <Point X="-3.502325195312" Y="25.028087890625" />
                  <Point X="-3.529996582031" Y="25.05060546875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.193456542969" Y="25.2365" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.954717285156" Y="25.745888671875" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.829091308594" Y="26.3232109375" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.360971191406" Y="26.473986328125" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731702880859" Y="26.3958671875" />
                  <Point X="-3.696606201172" Y="26.406931640625" />
                  <Point X="-3.670276855469" Y="26.415234375" />
                  <Point X="-3.651533691406" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.625037109375" Y="26.477783203125" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316650391" Y="26.54551171875" />
                  <Point X="-3.633308837891" Y="26.578154296875" />
                  <Point X="-3.646056396484" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.024775146484" Y="26.8991484375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.304063964844" Y="27.54021484375" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.925436523438" Y="28.088521484375" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.532392578125" Y="28.142431640625" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138513183594" Y="27.92043359375" />
                  <Point X="-3.089633056641" Y="27.91615625" />
                  <Point X="-3.052963867188" Y="27.91294921875" />
                  <Point X="-3.031506347656" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.978557373047" Y="27.962099609375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.942350830078" Y="28.076720703125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.113724365234" Y="28.41403125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-3.003763427734" Y="28.981978515625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.383423583984" Y="29.379591796875" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.082085449219" Y="29.436478515625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246826172" Y="29.273662109375" />
                  <Point X="-1.89684362793" Y="29.24533984375" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124511719" Y="29.218490234375" />
                  <Point X="-1.813809448242" Y="29.22225" />
                  <Point X="-1.757144775391" Y="29.24572265625" />
                  <Point X="-1.714635498047" Y="29.263330078125" />
                  <Point X="-1.696905395508" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.667639892578" Y="29.352982421875" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.670296142578" Y="29.558025390625" />
                  <Point X="-1.689137573242" Y="29.701140625" />
                  <Point X="-1.292883666992" Y="29.812236328125" />
                  <Point X="-0.968083496094" Y="29.903296875" />
                  <Point X="-0.520205566406" Y="29.95571484375" />
                  <Point X="-0.224199981689" Y="29.990359375" />
                  <Point X="-0.152553695679" Y="29.722970703125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282114029" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594032288" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.13728338623" Y="29.620033203125" />
                  <Point X="0.236648376465" Y="29.9908671875" />
                  <Point X="0.576616088867" Y="29.955263671875" />
                  <Point X="0.860209777832" Y="29.925564453125" />
                  <Point X="1.230755737305" Y="29.836103515625" />
                  <Point X="1.508455322266" Y="29.769056640625" />
                  <Point X="1.749985839844" Y="29.681453125" />
                  <Point X="1.931044799805" Y="29.61578125" />
                  <Point X="2.164254882812" Y="29.506716796875" />
                  <Point X="2.338686767578" Y="29.425140625" />
                  <Point X="2.564001464844" Y="29.29387109375" />
                  <Point X="2.732533203125" Y="29.195685546875" />
                  <Point X="2.945012451172" Y="29.04458203125" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.722158447266" Y="28.356294921875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.212584960938" Y="27.445640625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.206827880859" Y="27.352658203125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682861328" Y="27.300810546875" />
                  <Point X="2.243227539062" Y="27.26463671875" />
                  <Point X="2.261640625" Y="27.237501953125" />
                  <Point X="2.274938964844" Y="27.224205078125" />
                  <Point X="2.311111328125" Y="27.19966015625" />
                  <Point X="2.338247558594" Y="27.18124609375" />
                  <Point X="2.360337646484" Y="27.172978515625" />
                  <Point X="2.400004638672" Y="27.1681953125" />
                  <Point X="2.429762451172" Y="27.164607421875" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.494537841797" Y="27.178212890625" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.180719970703" Y="27.561740234375" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.102629882812" Y="27.8808046875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.321051269531" Y="27.546123046875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.936781738281" Y="27.0904375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.246356445312" Y="26.54076171875" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.200821289062" Y="26.447525390625" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.200875" Y="26.342037109375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646240234" Y="26.287955078125" />
                  <Point X="3.243104736328" Y="26.246220703125" />
                  <Point X="3.263703857422" Y="26.21491015625" />
                  <Point X="3.280946533203" Y="26.1988203125" />
                  <Point X="3.320737548828" Y="26.176421875" />
                  <Point X="3.350588623047" Y="26.1596171875" />
                  <Point X="3.368565673828" Y="26.153619140625" />
                  <Point X="3.422365966797" Y="26.146509765625" />
                  <Point X="3.462726318359" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.083462646484" Y="26.221171875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.896255371094" Y="26.127740234375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.976518554688" Y="25.71162109375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.483979492188" Y="25.43686328125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.676230224609" Y="25.202267578125" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622264892578" Y="25.166927734375" />
                  <Point X="3.59055078125" Y="25.126515625" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.546413574219" Y="25.01953515625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.5490546875" Y="24.904115234375" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.598473144531" Y="24.800828125" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636577148438" Y="24.758091796875" />
                  <Point X="3.689434326172" Y="24.7275390625" />
                  <Point X="3.729087158203" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.298418457031" Y="24.550296875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.972403808594" Y="24.1926015625" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.900605957031" Y="23.824021484375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.273435058594" Y="23.788958984375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836914062" Y="23.901658203125" />
                  <Point X="3.291097167969" Y="23.879109375" />
                  <Point X="3.213272705078" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.122741210938" Y="23.769888671875" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.055370849609" Y="23.588265625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.113771728516" Y="23.394078125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.685592773438" Y="22.9176484375" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.271705566406" Y="22.307203125" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.105225097656" Y="22.057326171875" />
                  <Point X="4.056687744141" Y="21.988361328125" />
                  <Point X="3.520694335938" Y="22.29781640625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340087891" Y="22.7466875" />
                  <Point X="2.613873291016" Y="22.768986328125" />
                  <Point X="2.521249755859" Y="22.785712890625" />
                  <Point X="2.489078125" Y="22.78075390625" />
                  <Point X="2.386507568359" Y="22.726771484375" />
                  <Point X="2.309560058594" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.234617675781" Y="22.56274609375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.2114609375" Y="22.330158203125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.566401123047" Y="21.64584375" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.915482177734" Y="20.867060546875" />
                  <Point X="2.835297851563" Y="20.809787109375" />
                  <Point X="2.724714111328" Y="20.738208984375" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.26865234375" Y="21.24490625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.548777709961" Y="22.0978203125" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.292627075195" Y="22.15202734375" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.06249597168" Y="22.045984375" />
                  <Point X="0.985348754883" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.93770916748" Y="21.812548828125" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.008103393555" Y="20.973958984375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.070204101563" Y="20.053380859375" />
                  <Point X="0.994367370605" Y="20.0367578125" />
                  <Point X="0.892187744141" Y="20.0181953125" />
                  <Point X="0.860200317383" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#174" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.110413401202" Y="4.767192917724" Z="1.45" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="-0.532173715165" Y="5.036656325159" Z="1.45" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.45" />
                  <Point X="-1.31253717172" Y="4.891660490197" Z="1.45" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.45" />
                  <Point X="-1.726024460128" Y="4.582779778026" Z="1.45" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.45" />
                  <Point X="-1.721481873578" Y="4.399298544457" Z="1.45" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.45" />
                  <Point X="-1.782434438134" Y="4.32319602673" Z="1.45" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.45" />
                  <Point X="-1.879911916661" Y="4.320970563909" Z="1.45" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.45" />
                  <Point X="-2.048573885926" Y="4.498196206039" Z="1.45" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.45" />
                  <Point X="-2.413862410865" Y="4.454578872598" Z="1.45" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.45" />
                  <Point X="-3.040341486495" Y="4.052938631042" Z="1.45" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.45" />
                  <Point X="-3.163181617938" Y="3.420310911995" Z="1.45" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.45" />
                  <Point X="-2.998316501989" Y="3.103643560899" Z="1.45" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.45" />
                  <Point X="-3.020068178434" Y="3.028735389452" Z="1.45" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.45" />
                  <Point X="-3.091432904708" Y="2.997248197184" Z="1.45" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.45" />
                  <Point X="-3.513548436302" Y="3.217012330808" Z="1.45" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.45" />
                  <Point X="-3.971056153808" Y="3.150505524011" Z="1.45" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.45" />
                  <Point X="-4.353401833816" Y="2.596700549844" Z="1.45" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.45" />
                  <Point X="-4.061369071326" Y="1.89076017885" Z="1.45" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.45" />
                  <Point X="-3.683814780916" Y="1.586346381009" Z="1.45" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.45" />
                  <Point X="-3.677387175261" Y="1.528198836428" Z="1.45" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.45" />
                  <Point X="-3.717799240359" Y="1.485898511927" Z="1.45" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.45" />
                  <Point X="-4.360601007903" Y="1.554838431772" Z="1.45" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.45" />
                  <Point X="-4.883506315562" Y="1.367569163071" Z="1.45" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.45" />
                  <Point X="-5.010334640017" Y="0.784486982471" Z="1.45" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.45" />
                  <Point X="-4.212553204362" Y="0.219482703064" Z="1.45" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.45" />
                  <Point X="-3.564664913502" Y="0.040812652136" Z="1.45" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.45" />
                  <Point X="-3.544842517224" Y="0.01703068288" Z="1.45" />
                  <Point X="-3.539556741714" Y="0" Z="1.45" />
                  <Point X="-3.543522083911" Y="-0.012776268181" Z="1.45" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.45" />
                  <Point X="-3.560703681622" Y="-0.038063330998" Z="1.45" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.45" />
                  <Point X="-4.42433458441" Y="-0.276229353692" Z="1.45" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.45" />
                  <Point X="-5.027037064122" Y="-0.679403116116" Z="1.45" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.45" />
                  <Point X="-4.924471078486" Y="-1.217485021041" Z="1.45" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.45" />
                  <Point X="-3.916865251897" Y="-1.398718075741" Z="1.45" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.45" />
                  <Point X="-3.207807556751" Y="-1.313544207479" Z="1.45" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.45" />
                  <Point X="-3.195978713716" Y="-1.335200531747" Z="1.45" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.45" />
                  <Point X="-3.944596524994" Y="-1.923253955165" Z="1.45" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.45" />
                  <Point X="-4.377076967139" Y="-2.562642748307" Z="1.45" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.45" />
                  <Point X="-4.060569212888" Y="-3.039360822235" Z="1.45" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.45" />
                  <Point X="-3.1255197209" Y="-2.874581041347" Z="1.45" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.45" />
                  <Point X="-2.565403332216" Y="-2.562927136915" Z="1.45" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.45" />
                  <Point X="-2.980835971822" Y="-3.309558661592" Z="1.45" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.45" />
                  <Point X="-3.124421685192" Y="-3.99737139363" Z="1.45" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.45" />
                  <Point X="-2.702151904531" Y="-4.29410745636" Z="1.45" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.45" />
                  <Point X="-2.322620090899" Y="-4.28208021241" Z="1.45" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.45" />
                  <Point X="-2.115649197007" Y="-4.082569500476" Z="1.45" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.45" />
                  <Point X="-1.835555608559" Y="-3.992782020167" Z="1.45" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.45" />
                  <Point X="-1.558683344454" Y="-4.092059234343" Z="1.45" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.45" />
                  <Point X="-1.399461867872" Y="-4.339370239566" Z="1.45" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.45" />
                  <Point X="-1.392430105436" Y="-4.722507093224" Z="1.45" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.45" />
                  <Point X="-1.286353216104" Y="-4.912113851541" Z="1.45" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.45" />
                  <Point X="-0.988925803739" Y="-4.980521223371" Z="1.45" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="-0.588789484917" Y="-4.159576342999" Z="1.45" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="-0.346907545414" Y="-3.417658181435" Z="1.45" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="-0.14476080182" Y="-3.249167779151" Z="1.45" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.45" />
                  <Point X="0.108598277541" Y="-3.237944266137" Z="1.45" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.45" />
                  <Point X="0.323538314018" Y="-3.383987574333" Z="1.45" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.45" />
                  <Point X="0.645965375485" Y="-4.372959686817" Z="1.45" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.45" />
                  <Point X="0.894968799574" Y="-4.999720231401" Z="1.45" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.45" />
                  <Point X="1.07475397009" Y="-4.964179177762" Z="1.45" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.45" />
                  <Point X="1.05151972279" Y="-3.988235694099" Z="1.45" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.45" />
                  <Point X="0.980412385291" Y="-3.166789233616" Z="1.45" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.45" />
                  <Point X="1.088307137311" Y="-2.961180712038" Z="1.45" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.45" />
                  <Point X="1.29105260291" Y="-2.866481736417" Z="1.45" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.45" />
                  <Point X="1.515582377218" Y="-2.912957433737" Z="1.45" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.45" />
                  <Point X="2.222828662518" Y="-3.754250683549" Z="1.45" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.45" />
                  <Point X="2.745727531938" Y="-4.27248561072" Z="1.45" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.45" />
                  <Point X="2.938387830492" Y="-4.142345982851" Z="1.45" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.45" />
                  <Point X="2.603546307486" Y="-3.297875164883" Z="1.45" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.45" />
                  <Point X="2.254509124679" Y="-2.629674944631" Z="1.45" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.45" />
                  <Point X="2.272708046473" Y="-2.429260696643" Z="1.45" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.45" />
                  <Point X="2.40363768173" Y="-2.286193263494" Z="1.45" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.45" />
                  <Point X="2.598831858882" Y="-2.248938884812" Z="1.45" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.45" />
                  <Point X="3.489538432224" Y="-2.714203192789" Z="1.45" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.45" />
                  <Point X="4.139957564265" Y="-2.940171593329" Z="1.45" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.45" />
                  <Point X="4.308083475974" Y="-2.687800953453" Z="1.45" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.45" />
                  <Point X="3.709874871127" Y="-2.011402457843" Z="1.45" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.45" />
                  <Point X="3.149673559002" Y="-1.547601616981" Z="1.45" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.45" />
                  <Point X="3.09900446709" Y="-1.385035999756" Z="1.45" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.45" />
                  <Point X="3.155031481031" Y="-1.230797686118" Z="1.45" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.45" />
                  <Point X="3.295560147133" Y="-1.138468680214" Z="1.45" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.45" />
                  <Point X="4.260752536141" Y="-1.229332812268" Z="1.45" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.45" />
                  <Point X="4.943197002997" Y="-1.155823117338" Z="1.45" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.45" />
                  <Point X="5.015689089902" Y="-0.783573006263" Z="1.45" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.45" />
                  <Point X="4.305203458724" Y="-0.370125520894" Z="1.45" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.45" />
                  <Point X="3.708299788668" Y="-0.197890543563" Z="1.45" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.45" />
                  <Point X="3.631650968095" Y="-0.137021931255" Z="1.45" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.45" />
                  <Point X="3.592006141511" Y="-0.055199987314" Z="1.45" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.45" />
                  <Point X="3.589365308914" Y="0.041410543892" Z="1.45" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.45" />
                  <Point X="3.623728470305" Y="0.12692680732" Z="1.45" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.45" />
                  <Point X="3.695095625684" Y="0.190258300679" Z="1.45" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.45" />
                  <Point X="4.490764358582" Y="0.419846411875" Z="1.45" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.45" />
                  <Point X="5.019767189758" Y="0.750593125501" Z="1.45" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.45" />
                  <Point X="4.938680522715" Y="1.170847664828" Z="1.45" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.45" />
                  <Point X="4.070780213962" Y="1.302023906434" Z="1.45" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.45" />
                  <Point X="3.422760910043" Y="1.227358215432" Z="1.45" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.45" />
                  <Point X="3.339109671713" Y="1.251272077084" Z="1.45" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.45" />
                  <Point X="3.278719456717" Y="1.304980750414" Z="1.45" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.45" />
                  <Point X="3.243687480458" Y="1.383421521138" Z="1.45" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.45" />
                  <Point X="3.242817928" Y="1.465338793899" Z="1.45" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.45" />
                  <Point X="3.279883381194" Y="1.54162472149" Z="1.45" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.45" />
                  <Point X="3.961063758608" Y="2.082050040905" Z="1.45" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.45" />
                  <Point X="4.35767258007" Y="2.603291217016" Z="1.45" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.45" />
                  <Point X="4.137059471475" Y="2.941287514197" Z="1.45" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.45" />
                  <Point X="3.14956389424" Y="2.636321616123" Z="1.45" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.45" />
                  <Point X="2.475464509555" Y="2.257796173533" Z="1.45" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.45" />
                  <Point X="2.399833807454" Y="2.249117420132" Z="1.45" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.45" />
                  <Point X="2.33303052952" Y="2.272313585552" Z="1.45" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.45" />
                  <Point X="2.278445014683" Y="2.323994330862" Z="1.45" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.45" />
                  <Point X="2.250312282627" Y="2.389924636378" Z="1.45" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.45" />
                  <Point X="2.254731769124" Y="2.464005066327" Z="1.45" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.45" />
                  <Point X="2.759303736736" Y="3.362575187715" Z="1.45" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.45" />
                  <Point X="2.967833706464" Y="4.116608173807" Z="1.45" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.45" />
                  <Point X="2.583015103816" Y="4.368355637755" Z="1.45" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.45" />
                  <Point X="2.17928067013" Y="4.583288154958" Z="1.45" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.45" />
                  <Point X="1.760878712868" Y="4.759737220333" Z="1.45" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.45" />
                  <Point X="1.236334352542" Y="4.915986996156" Z="1.45" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.45" />
                  <Point X="0.57566798298" Y="5.036273086503" Z="1.45" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.45" />
                  <Point X="0.082831160351" Y="4.664254588227" Z="1.45" />
                  <Point X="0" Y="4.355124473572" Z="1.45" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>