<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#207" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3285" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004716796875" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.918347900391" Y="20.16242578125" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557720336914" Y="21.502861328125" />
                  <Point X="0.54236340332" Y="21.532623046875" />
                  <Point X="0.3972472229" Y="21.74170703125" />
                  <Point X="0.378635742188" Y="21.7685234375" />
                  <Point X="0.356754089355" Y="21.7909765625" />
                  <Point X="0.330499816895" Y="21.810220703125" />
                  <Point X="0.302496154785" Y="21.824330078125" />
                  <Point X="0.077937294006" Y="21.8940234375" />
                  <Point X="0.049137039185" Y="21.902962890625" />
                  <Point X="0.020984472275" Y="21.907232421875" />
                  <Point X="-0.008657006264" Y="21.907234375" />
                  <Point X="-0.036822929382" Y="21.90296484375" />
                  <Point X="-0.26138180542" Y="21.83326953125" />
                  <Point X="-0.290182067871" Y="21.82433203125" />
                  <Point X="-0.318182220459" Y="21.810224609375" />
                  <Point X="-0.344438323975" Y="21.79098046875" />
                  <Point X="-0.366323150635" Y="21.7685234375" />
                  <Point X="-0.511439331055" Y="21.5594375" />
                  <Point X="-0.530050964355" Y="21.532623046875" />
                  <Point X="-0.538187866211" Y="21.5184296875" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.59652545166" Y="21.31753515625" />
                  <Point X="-0.916584655762" Y="20.12305859375" />
                  <Point X="-1.049545410156" Y="20.1488671875" />
                  <Point X="-1.079324584961" Y="20.1546484375" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.270155761719" Y="20.7534765625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.2879375" Y="20.81703125" />
                  <Point X="-1.304009277344" Y="20.844869140625" />
                  <Point X="-1.32364453125" Y="20.868796875" />
                  <Point X="-1.530389892578" Y="21.050107421875" />
                  <Point X="-1.556905395508" Y="21.073361328125" />
                  <Point X="-1.583188964844" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.64302746582" Y="21.109033203125" />
                  <Point X="-1.917424682617" Y="21.127017578125" />
                  <Point X="-1.952616455078" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.271300048828" Y="20.95242578125" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.360667236328" Y="20.867220703125" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.758043457031" Y="20.883576171875" />
                  <Point X="-2.801728515625" Y="20.910625" />
                  <Point X="-3.104721679688" Y="21.143921875" />
                  <Point X="-3.083573730469" Y="21.18055078125" />
                  <Point X="-2.423760986328" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561035156" Y="22.444427734375" />
                  <Point X="-2.428779541016" Y="22.4732578125" />
                  <Point X="-2.446808837891" Y="22.498416015625" />
                  <Point X="-2.462174804688" Y="22.51378125" />
                  <Point X="-2.464144775391" Y="22.515751953125" />
                  <Point X="-2.489299072266" Y="22.533779296875" />
                  <Point X="-2.518129394531" Y="22.548" />
                  <Point X="-2.54775" Y="22.55698828125" />
                  <Point X="-2.578687744141" Y="22.555974609375" />
                  <Point X="-2.610217041016" Y="22.549703125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-2.78575390625" Y="22.454177734375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.048361816406" Y="22.160814453125" />
                  <Point X="-4.082860595703" Y="22.206138671875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.260412597656" Y="22.615638671875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053855957031" Y="23.58016796875" />
                  <Point X="-3.04703125" Y="23.60651953125" />
                  <Point X="-3.046155273438" Y="23.609900390625" />
                  <Point X="-3.043348388672" Y="23.64033203125" />
                  <Point X="-3.045555664062" Y="23.672005859375" />
                  <Point X="-3.052556640625" Y="23.701755859375" />
                  <Point X="-3.068640625" Y="23.7277421875" />
                  <Point X="-3.089476074219" Y="23.751701171875" />
                  <Point X="-3.112975830078" Y="23.771234375" />
                  <Point X="-3.136448242188" Y="23.785048828125" />
                  <Point X="-3.139458496094" Y="23.7868203125" />
                  <Point X="-3.16872265625" Y="23.79804296875" />
                  <Point X="-3.200607910156" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.416959960938" Y="23.781255859375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.820583984375" Y="23.95451953125" />
                  <Point X="-4.834076660156" Y="24.00734375" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-4.847977539062" Y="24.427208984375" />
                  <Point X="-3.532875488281" Y="24.77958984375" />
                  <Point X="-3.517489257812" Y="24.785173828125" />
                  <Point X="-3.487730224609" Y="24.800529296875" />
                  <Point X="-3.463132080078" Y="24.8176015625" />
                  <Point X="-3.459977294922" Y="24.819791015625" />
                  <Point X="-3.437523193359" Y="24.841673828125" />
                  <Point X="-3.418277832031" Y="24.8679296875" />
                  <Point X="-3.404167724609" Y="24.89593359375" />
                  <Point X="-3.395975585938" Y="24.922330078125" />
                  <Point X="-3.394923583984" Y="24.925716796875" />
                  <Point X="-3.390649414062" Y="24.953884765625" />
                  <Point X="-3.390647216797" Y="24.983529296875" />
                  <Point X="-3.394916259766" Y="25.0116953125" />
                  <Point X="-3.403115722656" Y="25.038115234375" />
                  <Point X="-3.404167236328" Y="25.04150390625" />
                  <Point X="-3.418275878906" Y="25.069505859375" />
                  <Point X="-3.437521728516" Y="25.095763671875" />
                  <Point X="-3.459977294922" Y="25.1176484375" />
                  <Point X="-3.484575439453" Y="25.134720703125" />
                  <Point X="-3.496078125" Y="25.141556640625" />
                  <Point X="-3.514799804688" Y="25.150966796875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.701540771484" Y="25.20304296875" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.833182617188" Y="25.91821484375" />
                  <Point X="-4.824487792969" Y="25.9769765625" />
                  <Point X="-4.703551269531" Y="26.423267578125" />
                  <Point X="-3.765667480469" Y="26.29979296875" />
                  <Point X="-3.744992675781" Y="26.299341796875" />
                  <Point X="-3.723433349609" Y="26.3012265625" />
                  <Point X="-3.703139892578" Y="26.30526171875" />
                  <Point X="-3.648696289062" Y="26.322427734375" />
                  <Point X="-3.641713867188" Y="26.32462890625" />
                  <Point X="-3.622781738281" Y="26.332958984375" />
                  <Point X="-3.604036621094" Y="26.34378125" />
                  <Point X="-3.587355957031" Y="26.35601171875" />
                  <Point X="-3.573717529297" Y="26.3715625" />
                  <Point X="-3.561302001953" Y="26.38929296875" />
                  <Point X="-3.551351806641" Y="26.4074296875" />
                  <Point X="-3.529506103516" Y="26.460169921875" />
                  <Point X="-3.526704345703" Y="26.46693359375" />
                  <Point X="-3.520916503906" Y="26.486791015625" />
                  <Point X="-3.517157714844" Y="26.50810546875" />
                  <Point X="-3.515804443359" Y="26.528744140625" />
                  <Point X="-3.518950683594" Y="26.549189453125" />
                  <Point X="-3.524552490234" Y="26.57009765625" />
                  <Point X="-3.532050048828" Y="26.58937890625" />
                  <Point X="-3.558409179688" Y="26.640013671875" />
                  <Point X="-3.561789794922" Y="26.6465078125" />
                  <Point X="-3.573281982422" Y="26.66370703125" />
                  <Point X="-3.587194580078" Y="26.680287109375" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.698882080078" Y="26.768826171875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.1149375" Y="27.675779296875" />
                  <Point X="-4.081156005859" Y="27.73365625" />
                  <Point X="-3.750505615234" Y="28.158662109375" />
                  <Point X="-3.206656494141" Y="27.844669921875" />
                  <Point X="-3.187729980469" Y="27.836341796875" />
                  <Point X="-3.167088378906" Y="27.82983203125" />
                  <Point X="-3.146796386719" Y="27.825794921875" />
                  <Point X="-3.070971679688" Y="27.81916015625" />
                  <Point X="-3.061222167969" Y="27.81830859375" />
                  <Point X="-3.040544433594" Y="27.818763671875" />
                  <Point X="-3.019089111328" Y="27.821591796875" />
                  <Point X="-2.999005859375" Y="27.8265078125" />
                  <Point X="-2.980461914062" Y="27.835654296875" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.892257080078" Y="27.914048828125" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.850069580078" Y="28.1119453125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795410156" Y="28.181533203125" />
                  <Point X="-2.912666748047" Y="28.255787109375" />
                  <Point X="-3.183333251953" Y="28.724595703125" />
                  <Point X="-2.759461425781" Y="29.04957421875" />
                  <Point X="-2.700622314453" Y="29.094685546875" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.04319519043" Y="29.2297421875" />
                  <Point X="-2.028893554688" Y="29.21480078125" />
                  <Point X="-2.012314697266" Y="29.200888671875" />
                  <Point X="-1.995115234375" Y="29.189396484375" />
                  <Point X="-1.91072277832" Y="29.145462890625" />
                  <Point X="-1.899899291992" Y="29.139828125" />
                  <Point X="-1.880625244141" Y="29.13233203125" />
                  <Point X="-1.859717529297" Y="29.126728515625" />
                  <Point X="-1.839268554688" Y="29.123580078125" />
                  <Point X="-1.818622924805" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.777452392578" Y="29.134482421875" />
                  <Point X="-1.689551879883" Y="29.170892578125" />
                  <Point X="-1.678278320312" Y="29.1755625" />
                  <Point X="-1.660138916016" Y="29.185513671875" />
                  <Point X="-1.642409423828" Y="29.1979296875" />
                  <Point X="-1.626860473633" Y="29.211568359375" />
                  <Point X="-1.614631225586" Y="29.228248046875" />
                  <Point X="-1.603809692383" Y="29.2469921875" />
                  <Point X="-1.595479736328" Y="29.265923828125" />
                  <Point X="-1.566869873047" Y="29.356662109375" />
                  <Point X="-1.563200439453" Y="29.36830078125" />
                  <Point X="-1.559165039062" Y="29.388587890625" />
                  <Point X="-1.557279052734" Y="29.410146484375" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.562604370117" Y="29.46784765625" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-1.025801757812" Y="29.788453125" />
                  <Point X="-0.949624816895" Y="29.80980859375" />
                  <Point X="-0.294711181641" Y="29.886458984375" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082114006042" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155910969" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.168182006836" Y="29.368294921875" />
                  <Point X="0.307419616699" Y="29.887939453125" />
                  <Point X="0.777528015137" Y="29.838705078125" />
                  <Point X="0.844029785156" Y="29.831740234375" />
                  <Point X="1.414500244141" Y="29.69401171875" />
                  <Point X="1.48103894043" Y="29.6779453125" />
                  <Point X="1.852174682617" Y="29.54333203125" />
                  <Point X="1.894646484375" Y="29.527927734375" />
                  <Point X="2.253689453125" Y="29.360015625" />
                  <Point X="2.294560546875" Y="29.340900390625" />
                  <Point X="2.641449707031" Y="29.138802734375" />
                  <Point X="2.6809765625" Y="29.115775390625" />
                  <Point X="2.943259277344" Y="28.929251953125" />
                  <Point X="2.911264648438" Y="28.8738359375" />
                  <Point X="2.147580810547" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.114047607422" Y="27.44489453125" />
                  <Point X="2.111674560547" Y="27.433044921875" />
                  <Point X="2.107896240234" Y="27.405015625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.115147949219" Y="27.31941796875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442138672" Y="27.289603515625" />
                  <Point X="2.129708007812" Y="27.267515625" />
                  <Point X="2.140070068359" Y="27.24747265625" />
                  <Point X="2.17814453125" Y="27.191359375" />
                  <Point X="2.185691894531" Y="27.18165234375" />
                  <Point X="2.203875488281" Y="27.16115625" />
                  <Point X="2.221599121094" Y="27.145591796875" />
                  <Point X="2.277711181641" Y="27.107517578125" />
                  <Point X="2.284901367188" Y="27.102638671875" />
                  <Point X="2.304937011719" Y="27.092279296875" />
                  <Point X="2.327033203125" Y="27.0840078125" />
                  <Point X="2.348968261719" Y="27.078662109375" />
                  <Point X="2.410501464844" Y="27.071244140625" />
                  <Point X="2.423242431641" Y="27.0705703125" />
                  <Point X="2.450039550781" Y="27.07095703125" />
                  <Point X="2.4732109375" Y="27.074171875" />
                  <Point X="2.544370849609" Y="27.093201171875" />
                  <Point X="2.551285400391" Y="27.0953359375" />
                  <Point X="2.572490722656" Y="27.10277734375" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="2.758177734375" Y="27.208087890625" />
                  <Point X="3.967326416016" Y="27.906189453125" />
                  <Point X="4.099823730469" Y="27.722048828125" />
                  <Point X="4.12327734375" Y="27.689453125" />
                  <Point X="4.262198730469" Y="27.459884765625" />
                  <Point X="4.236245117188" Y="27.43996875" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221426025391" Y="26.6602421875" />
                  <Point X="3.203974853516" Y="26.64162890625" />
                  <Point X="3.152760986328" Y="26.57481640625" />
                  <Point X="3.146309326172" Y="26.56524609375" />
                  <Point X="3.131269775391" Y="26.539720703125" />
                  <Point X="3.121629394531" Y="26.51708203125" />
                  <Point X="3.102552001953" Y="26.4488671875" />
                  <Point X="3.10010546875" Y="26.4401171875" />
                  <Point X="3.096651855469" Y="26.41781640625" />
                  <Point X="3.095836425781" Y="26.39425" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.113399902344" Y="26.295869140625" />
                  <Point X="3.116485351562" Y="26.284517578125" />
                  <Point X="3.125692871094" Y="26.257404296875" />
                  <Point X="3.136283447266" Y="26.23573828125" />
                  <Point X="3.178878173828" Y="26.17099609375" />
                  <Point X="3.184341064453" Y="26.162693359375" />
                  <Point X="3.198890380859" Y="26.145453125" />
                  <Point X="3.216130126953" Y="26.129365234375" />
                  <Point X="3.234343994141" Y="26.11603515625" />
                  <Point X="3.296069580078" Y="26.0812890625" />
                  <Point X="3.307106933594" Y="26.075982421875" />
                  <Point X="3.333004638672" Y="26.06552734375" />
                  <Point X="3.356121582031" Y="26.0594375" />
                  <Point X="3.439578613281" Y="26.048408203125" />
                  <Point X="3.446370117188" Y="26.0477578125" />
                  <Point X="3.470148681641" Y="26.04633984375" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.649353515625" Y="26.06819921875" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.835865234375" Y="25.974173828125" />
                  <Point X="4.845937011719" Y="25.932806640625" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.868859863281" Y="25.63834375" />
                  <Point X="3.716580078125" Y="25.32958984375" />
                  <Point X="3.704787597656" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.599553466797" Y="25.26767578125" />
                  <Point X="3.590337890625" Y="25.261609375" />
                  <Point X="3.565510742188" Y="25.24311328125" />
                  <Point X="3.547531494141" Y="25.225578125" />
                  <Point X="3.498335205078" Y="25.162888671875" />
                  <Point X="3.492025634766" Y="25.154849609375" />
                  <Point X="3.480302734375" Y="25.135572265625" />
                  <Point X="3.47052734375" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.447281738281" Y="25.006974609375" />
                  <Point X="3.445822021484" Y="24.995794921875" />
                  <Point X="3.44371875" Y="24.96600390625" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.461577636719" Y="24.855818359375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526123047" Y="24.823337890625" />
                  <Point X="3.480298095703" Y="24.801876953125" />
                  <Point X="3.492023193359" Y="24.78259375" />
                  <Point X="3.541219482422" Y="24.71990625" />
                  <Point X="3.549039306641" Y="24.71112109375" />
                  <Point X="3.569663574219" Y="24.69065625" />
                  <Point X="3.589036132812" Y="24.67584375" />
                  <Point X="3.671030273438" Y="24.62844921875" />
                  <Point X="3.676728027344" Y="24.625408203125" />
                  <Point X="3.699323974609" Y="24.614322265625" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="3.864362304688" Y="24.56825" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.860645507812" Y="24.0885703125" />
                  <Point X="4.855021484375" Y="24.051265625" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="4.760702636719" Y="23.82062890625" />
                  <Point X="3.424381347656" Y="23.99655859375" />
                  <Point X="3.408034912109" Y="23.9972890625" />
                  <Point X="3.374661621094" Y="23.994490234375" />
                  <Point X="3.213736328125" Y="23.95951171875" />
                  <Point X="3.193097412109" Y="23.95502734375" />
                  <Point X="3.163982177734" Y="23.94340625" />
                  <Point X="3.136151611328" Y="23.926513671875" />
                  <Point X="3.112396972656" Y="23.9060390625" />
                  <Point X="3.015127685547" Y="23.7890546875" />
                  <Point X="3.002652832031" Y="23.77405078125" />
                  <Point X="2.987931640625" Y="23.74966796875" />
                  <Point X="2.976588867188" Y="23.722283203125" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.955816650391" Y="23.543134765625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.95634765625" Y="23.49243359375" />
                  <Point X="2.964079589844" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.065509277344" Y="23.293478515625" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930419922" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.247771240234" Y="23.13385546875" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.14066015625" Y="22.275861328125" />
                  <Point X="4.124810546875" Y="22.250212890625" />
                  <Point X="4.028980957031" Y="22.1140546875" />
                  <Point X="3.991127929688" Y="22.135908203125" />
                  <Point X="2.800953857422" Y="22.823056640625" />
                  <Point X="2.78613671875" Y="22.829984375" />
                  <Point X="2.754223388672" Y="22.84017578125" />
                  <Point X="2.562696533203" Y="22.874765625" />
                  <Point X="2.538133056641" Y="22.879201171875" />
                  <Point X="2.506777099609" Y="22.879603515625" />
                  <Point X="2.474604492188" Y="22.87464453125" />
                  <Point X="2.444832763672" Y="22.864822265625" />
                  <Point X="2.285720947266" Y="22.781083984375" />
                  <Point X="2.265314697266" Y="22.77034375" />
                  <Point X="2.242383056641" Y="22.75344921875" />
                  <Point X="2.221424316406" Y="22.732490234375" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.120792480469" Y="22.55044921875" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229248047" Y="22.500267578125" />
                  <Point X="2.095271484375" Y="22.468095703125" />
                  <Point X="2.095675537109" Y="22.436744140625" />
                  <Point X="2.130265136719" Y="22.245216796875" />
                  <Point X="2.134701171875" Y="22.22065234375" />
                  <Point X="2.138984619141" Y="22.204861328125" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.239947509766" Y="22.021279296875" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.800661865234" Y="20.90179296875" />
                  <Point X="2.781867675781" Y="20.888369140625" />
                  <Point X="2.701763916016" Y="20.83651953125" />
                  <Point X="2.666758789062" Y="20.882138671875" />
                  <Point X="1.758546264648" Y="22.065744140625" />
                  <Point X="1.747503417969" Y="22.0778203125" />
                  <Point X="1.721922851562" Y="22.099443359375" />
                  <Point X="1.533025756836" Y="22.22088671875" />
                  <Point X="1.508799438477" Y="22.2364609375" />
                  <Point X="1.479986816406" Y="22.24883203125" />
                  <Point X="1.448368408203" Y="22.2565625" />
                  <Point X="1.417101318359" Y="22.258880859375" />
                  <Point X="1.210510131836" Y="22.23987109375" />
                  <Point X="1.184014404297" Y="22.23743359375" />
                  <Point X="1.156365112305" Y="22.230603515625" />
                  <Point X="1.128978149414" Y="22.219259765625" />
                  <Point X="1.104594848633" Y="22.204537109375" />
                  <Point X="0.945070495605" Y="22.071896484375" />
                  <Point X="0.92461126709" Y="22.054884765625" />
                  <Point X="0.904141723633" Y="22.03113671875" />
                  <Point X="0.88725" Y="22.0033125" />
                  <Point X="0.875624389648" Y="21.97419140625" />
                  <Point X="0.827927429199" Y="21.754748046875" />
                  <Point X="0.821810302734" Y="21.72660546875" />
                  <Point X="0.81972467041" Y="21.710375" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.844717163086" Y="21.487177734375" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.99347265625" Y="20.13381640625" />
                  <Point X="0.975699035645" Y="20.129919921875" />
                  <Point X="0.929315612793" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.031443115234" Y="20.242126953125" />
                  <Point X="-1.058421142578" Y="20.247365234375" />
                  <Point X="-1.14124597168" Y="20.268673828125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.176981201172" Y="20.772009765625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.18812487793" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.850494140625" />
                  <Point X="-1.205664428711" Y="20.86453125" />
                  <Point X="-1.221736206055" Y="20.892369140625" />
                  <Point X="-1.230570678711" Y="20.9051328125" />
                  <Point X="-1.250205810547" Y="20.929060546875" />
                  <Point X="-1.261006713867" Y="20.94022265625" />
                  <Point X="-1.467752075195" Y="21.121533203125" />
                  <Point X="-1.494267578125" Y="21.144787109375" />
                  <Point X="-1.506739868164" Y="21.15403515625" />
                  <Point X="-1.5330234375" Y="21.17037890625" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591315795898" Y="21.194525390625" />
                  <Point X="-1.62145690918" Y="21.201552734375" />
                  <Point X="-1.636814331055" Y="21.203830078125" />
                  <Point X="-1.911211547852" Y="21.221814453125" />
                  <Point X="-1.946403320312" Y="21.22412109375" />
                  <Point X="-1.961927734375" Y="21.2238671875" />
                  <Point X="-1.992725463867" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047729492" Y="21.209736328125" />
                  <Point X="-2.053666992188" Y="21.204505859375" />
                  <Point X="-2.081861328125" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.324079101562" Y="21.031416015625" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.436035888672" Y="20.925052734375" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.708032714844" Y="20.96434765625" />
                  <Point X="-2.747612548828" Y="20.98885546875" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849609375" Y="22.28991796875" />
                  <Point X="-2.323947509766" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314667236328" Y="22.442390625" />
                  <Point X="-2.323652587891" Y="22.4720078125" />
                  <Point X="-2.329359375" Y="22.486447265625" />
                  <Point X="-2.343577880859" Y="22.51527734375" />
                  <Point X="-2.351560791016" Y="22.528595703125" />
                  <Point X="-2.369590087891" Y="22.55375390625" />
                  <Point X="-2.379635253906" Y="22.565591796875" />
                  <Point X="-2.394987304688" Y="22.580943359375" />
                  <Point X="-2.408805175781" Y="22.59296875" />
                  <Point X="-2.433959472656" Y="22.61099609375" />
                  <Point X="-2.447274169922" Y="22.618978515625" />
                  <Point X="-2.476104492188" Y="22.63319921875" />
                  <Point X="-2.490543945312" Y="22.63890625" />
                  <Point X="-2.520164550781" Y="22.64789453125" />
                  <Point X="-2.550861083984" Y="22.6519375" />
                  <Point X="-2.581798828125" Y="22.650923828125" />
                  <Point X="-2.597221191406" Y="22.6491484375" />
                  <Point X="-2.628750488281" Y="22.642876953125" />
                  <Point X="-2.643680908203" Y="22.63861328125" />
                  <Point X="-2.672647705078" Y="22.6277109375" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-2.833254150391" Y="22.53644921875" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.972768554688" Y="22.218353515625" />
                  <Point X="-4.004018798828" Y="22.25941015625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481201172" Y="23.436689453125" />
                  <Point X="-3.015102783203" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833984375" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890136719" Y="23.556349609375" />
                  <Point X="-2.955068115234" Y="23.58269140625" />
                  <Point X="-2.951556884766" Y="23.60117578125" />
                  <Point X="-2.94875" Y="23.631607421875" />
                  <Point X="-2.948578125" Y="23.646935546875" />
                  <Point X="-2.950785400391" Y="23.678609375" />
                  <Point X="-2.953081787109" Y="23.693767578125" />
                  <Point X="-2.960082763672" Y="23.723517578125" />
                  <Point X="-2.971777587891" Y="23.75175390625" />
                  <Point X="-2.987861572266" Y="23.777740234375" />
                  <Point X="-2.996955322266" Y="23.79008203125" />
                  <Point X="-3.017790771484" Y="23.814041015625" />
                  <Point X="-3.028750244141" Y="23.8247578125" />
                  <Point X="-3.05225" Y="23.844291015625" />
                  <Point X="-3.064790283203" Y="23.853107421875" />
                  <Point X="-3.088262695312" Y="23.866921875" />
                  <Point X="-3.105442138672" Y="23.875521484375" />
                  <Point X="-3.134706298828" Y="23.886744140625" />
                  <Point X="-3.149801269531" Y="23.891138671875" />
                  <Point X="-3.181686523438" Y="23.897619140625" />
                  <Point X="-3.197298339844" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328613281" Y="23.899802734375" />
                  <Point X="-3.429359619141" Y="23.875443359375" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.7285390625" Y="23.97803125" />
                  <Point X="-4.740761230469" Y="24.025880859375" />
                  <Point X="-4.786451660156" Y="24.34534375" />
                  <Point X="-3.508286132812" Y="24.687828125" />
                  <Point X="-3.500466308594" Y="24.6902890625" />
                  <Point X="-3.473927246094" Y="24.70075" />
                  <Point X="-3.444168212891" Y="24.71610546875" />
                  <Point X="-3.433563476562" Y="24.722484375" />
                  <Point X="-3.408965332031" Y="24.739556640625" />
                  <Point X="-3.393673339844" Y="24.751755859375" />
                  <Point X="-3.371219238281" Y="24.773638671875" />
                  <Point X="-3.36090234375" Y="24.78551171875" />
                  <Point X="-3.341656982422" Y="24.811767578125" />
                  <Point X="-3.333438720703" Y="24.825181640625" />
                  <Point X="-3.319328613281" Y="24.853185546875" />
                  <Point X="-3.313436767578" Y="24.867775390625" />
                  <Point X="-3.305251953125" Y="24.8941484375" />
                  <Point X="-3.300998779297" Y="24.91146484375" />
                  <Point X="-3.296724609375" Y="24.9396328125" />
                  <Point X="-3.295649414062" Y="24.953876953125" />
                  <Point X="-3.295647216797" Y="24.983521484375" />
                  <Point X="-3.296719970703" Y="24.997765625" />
                  <Point X="-3.300989013672" Y="25.025931640625" />
                  <Point X="-3.304185302734" Y="25.039853515625" />
                  <Point X="-3.312384765625" Y="25.0662734375" />
                  <Point X="-3.319327636719" Y="25.08425" />
                  <Point X="-3.333436279297" Y="25.112251953125" />
                  <Point X="-3.341653564453" Y="25.125666015625" />
                  <Point X="-3.360899414062" Y="25.151923828125" />
                  <Point X="-3.371216796875" Y="25.163798828125" />
                  <Point X="-3.393672363281" Y="25.18568359375" />
                  <Point X="-3.405810546875" Y="25.195693359375" />
                  <Point X="-3.430408691406" Y="25.212765625" />
                  <Point X="-3.436041503906" Y="25.21638671875" />
                  <Point X="-3.4534140625" Y="25.2264375" />
                  <Point X="-3.472135742188" Y="25.23584765625" />
                  <Point X="-3.480995117188" Y="25.239748046875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.676953125" Y="25.294806640625" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.739206054688" Y="25.90430859375" />
                  <Point X="-4.731331054688" Y="25.95753125" />
                  <Point X="-4.633586425781" Y="26.318236328125" />
                  <Point X="-3.778067382812" Y="26.20560546875" />
                  <Point X="-3.767739990234" Y="26.20481640625" />
                  <Point X="-3.747065185547" Y="26.204365234375" />
                  <Point X="-3.736718994141" Y="26.204703125" />
                  <Point X="-3.715159667969" Y="26.206587890625" />
                  <Point X="-3.70490625" Y="26.20805078125" />
                  <Point X="-3.684612792969" Y="26.2120859375" />
                  <Point X="-3.674572753906" Y="26.214658203125" />
                  <Point X="-3.620129150391" Y="26.23182421875" />
                  <Point X="-3.603453857422" Y="26.237673828125" />
                  <Point X="-3.584521728516" Y="26.24600390625" />
                  <Point X="-3.575282470703" Y="26.250685546875" />
                  <Point X="-3.556537353516" Y="26.2615078125" />
                  <Point X="-3.547863037109" Y="26.26716796875" />
                  <Point X="-3.531182373047" Y="26.2793984375" />
                  <Point X="-3.515932861328" Y="26.29337109375" />
                  <Point X="-3.502294433594" Y="26.308921875" />
                  <Point X="-3.495899169922" Y="26.3170703125" />
                  <Point X="-3.483483642578" Y="26.33480078125" />
                  <Point X="-3.478012939453" Y="26.343599609375" />
                  <Point X="-3.468062744141" Y="26.361736328125" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.441737548828" Y="26.423814453125" />
                  <Point X="-3.435499511719" Y="26.440349609375" />
                  <Point X="-3.429711669922" Y="26.46020703125" />
                  <Point X="-3.427360107422" Y="26.47029296875" />
                  <Point X="-3.423601318359" Y="26.491607421875" />
                  <Point X="-3.422361328125" Y="26.501890625" />
                  <Point X="-3.421008056641" Y="26.522529296875" />
                  <Point X="-3.421909667969" Y="26.543193359375" />
                  <Point X="-3.425055908203" Y="26.563638671875" />
                  <Point X="-3.427187255859" Y="26.573775390625" />
                  <Point X="-3.4327890625" Y="26.59468359375" />
                  <Point X="-3.436010986328" Y="26.60452734375" />
                  <Point X="-3.443508544922" Y="26.62380859375" />
                  <Point X="-3.447784179688" Y="26.63324609375" />
                  <Point X="-3.474143310547" Y="26.683880859375" />
                  <Point X="-3.482800292969" Y="26.699287109375" />
                  <Point X="-3.494292480469" Y="26.716486328125" />
                  <Point X="-3.500508300781" Y="26.7247734375" />
                  <Point X="-3.514420898438" Y="26.741353515625" />
                  <Point X="-3.521501708984" Y="26.748912109375" />
                  <Point X="-3.536442871094" Y="26.76321484375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.641049560547" Y="26.8441953125" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.032891113281" Y="27.627890625" />
                  <Point X="-4.002295898437" Y="27.68030859375" />
                  <Point X="-3.726338867188" Y="28.035013671875" />
                  <Point X="-3.254156494141" Y="27.7623984375" />
                  <Point X="-3.244918457031" Y="27.75771484375" />
                  <Point X="-3.225991943359" Y="27.74938671875" />
                  <Point X="-3.216302978516" Y="27.745740234375" />
                  <Point X="-3.195661376953" Y="27.73923046875" />
                  <Point X="-3.185625488281" Y="27.736658203125" />
                  <Point X="-3.165333496094" Y="27.73262109375" />
                  <Point X="-3.155077392578" Y="27.73115625" />
                  <Point X="-3.079252685547" Y="27.724521484375" />
                  <Point X="-3.079237792969" Y="27.72451953125" />
                  <Point X="-3.059131835938" Y="27.72333203125" />
                  <Point X="-3.038454101562" Y="27.723787109375" />
                  <Point X="-3.028129394531" Y="27.724578125" />
                  <Point X="-3.006674072266" Y="27.72740625" />
                  <Point X="-2.996501708984" Y="27.72931640625" />
                  <Point X="-2.976418457031" Y="27.734232421875" />
                  <Point X="-2.956982421875" Y="27.74130859375" />
                  <Point X="-2.938438476562" Y="27.750455078125" />
                  <Point X="-2.929419677734" Y="27.75553125" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902749267578" Y="27.77319140625" />
                  <Point X="-2.886616699219" Y="27.786138671875" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.825082275391" Y="27.846873046875" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.755431152344" Y="28.120224609375" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774510009766" Y="28.200861328125" />
                  <Point X="-2.782841308594" Y="28.219794921875" />
                  <Point X="-2.7875234375" Y="28.229033203125" />
                  <Point X="-2.830394775391" Y="28.303287109375" />
                  <Point X="-3.059387695312" Y="28.6999140625" />
                  <Point X="-2.701659179688" Y="28.974181640625" />
                  <Point X="-2.648370361328" Y="29.015037109375" />
                  <Point X="-2.192523681641" Y="29.268296875" />
                  <Point X="-2.118563720703" Y="29.17191015625" />
                  <Point X="-2.111823486328" Y="29.164052734375" />
                  <Point X="-2.097521972656" Y="29.149111328125" />
                  <Point X="-2.089960449219" Y="29.14202734375" />
                  <Point X="-2.073381591797" Y="29.128115234375" />
                  <Point X="-2.065093505859" Y="29.1218984375" />
                  <Point X="-2.047894042969" Y="29.11040625" />
                  <Point X="-2.038982666016" Y="29.105130859375" />
                  <Point X="-1.954590209961" Y="29.061197265625" />
                  <Point X="-1.934334350586" Y="29.0512890625" />
                  <Point X="-1.915060180664" Y="29.04379296875" />
                  <Point X="-1.905218383789" Y="29.0405703125" />
                  <Point X="-1.884310668945" Y="29.034966796875" />
                  <Point X="-1.874173950195" Y="29.032833984375" />
                  <Point X="-1.853724975586" Y="29.029685546875" />
                  <Point X="-1.833053710938" Y="29.028783203125" />
                  <Point X="-1.812408081055" Y="29.03013671875" />
                  <Point X="-1.802121582031" Y="29.031376953125" />
                  <Point X="-1.780805175781" Y="29.03513671875" />
                  <Point X="-1.770713867188" Y="29.0374921875" />
                  <Point X="-1.750859741211" Y="29.04328125" />
                  <Point X="-1.741096923828" Y="29.04671484375" />
                  <Point X="-1.653196411133" Y="29.083125" />
                  <Point X="-1.6325859375" Y="29.0922734375" />
                  <Point X="-1.614446533203" Y="29.102224609375" />
                  <Point X="-1.605644165039" Y="29.107697265625" />
                  <Point X="-1.587914672852" Y="29.12011328125" />
                  <Point X="-1.579764770508" Y="29.12651171875" />
                  <Point X="-1.564215820312" Y="29.140150390625" />
                  <Point X="-1.550246459961" Y="29.155396484375" />
                  <Point X="-1.538017211914" Y="29.172076171875" />
                  <Point X="-1.532357910156" Y="29.18075" />
                  <Point X="-1.521536376953" Y="29.199494140625" />
                  <Point X="-1.516854858398" Y="29.208732421875" />
                  <Point X="-1.508524902344" Y="29.2276640625" />
                  <Point X="-1.504876708984" Y="29.237357421875" />
                  <Point X="-1.476266845703" Y="29.328095703125" />
                  <Point X="-1.470025878906" Y="29.349767578125" />
                  <Point X="-1.465990356445" Y="29.3700546875" />
                  <Point X="-1.464526489258" Y="29.38030859375" />
                  <Point X="-1.46264050293" Y="29.4018671875" />
                  <Point X="-1.462301635742" Y="29.41221875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.468417114258" Y="29.480248046875" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.000155883789" Y="29.69698046875" />
                  <Point X="-0.931166687012" Y="29.7163203125" />
                  <Point X="-0.365222412109" Y="29.78255859375" />
                  <Point X="-0.225666366577" Y="29.2617265625" />
                  <Point X="-0.220435150146" Y="29.247107421875" />
                  <Point X="-0.207661453247" Y="29.218916015625" />
                  <Point X="-0.200119293213" Y="29.20534375" />
                  <Point X="-0.182261199951" Y="29.1786171875" />
                  <Point X="-0.172608886719" Y="29.166455078125" />
                  <Point X="-0.15145123291" Y="29.143865234375" />
                  <Point X="-0.126896499634" Y="29.1250234375" />
                  <Point X="-0.099600570679" Y="29.11043359375" />
                  <Point X="-0.085354026794" Y="29.1042578125" />
                  <Point X="-0.054916057587" Y="29.09392578125" />
                  <Point X="-0.039853721619" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629489899" Y="29.08511328125" />
                  <Point X="0.052165550232" Y="29.090154296875" />
                  <Point X="0.067227882385" Y="29.09392578125" />
                  <Point X="0.097665855408" Y="29.1042578125" />
                  <Point X="0.111912246704" Y="29.11043359375" />
                  <Point X="0.139208175659" Y="29.1250234375" />
                  <Point X="0.163763061523" Y="29.143865234375" />
                  <Point X="0.184920715332" Y="29.166455078125" />
                  <Point X="0.194573181152" Y="29.1786171875" />
                  <Point X="0.212431259155" Y="29.20534375" />
                  <Point X="0.219973724365" Y="29.218916015625" />
                  <Point X="0.232747116089" Y="29.247107421875" />
                  <Point X="0.237978057861" Y="29.2617265625" />
                  <Point X="0.259944976807" Y="29.34370703125" />
                  <Point X="0.378190734863" Y="29.785009765625" />
                  <Point X="0.767632751465" Y="29.74422265625" />
                  <Point X="0.827851257324" Y="29.737916015625" />
                  <Point X="1.392204956055" Y="29.6016640625" />
                  <Point X="1.453619995117" Y="29.586833984375" />
                  <Point X="1.819782470703" Y="29.454025390625" />
                  <Point X="1.858249511719" Y="29.44007421875" />
                  <Point X="2.213444824219" Y="29.2739609375" />
                  <Point X="2.250431396484" Y="29.256662109375" />
                  <Point X="2.593626953125" Y="29.056716796875" />
                  <Point X="2.629431396484" Y="29.035857421875" />
                  <Point X="2.817779052734" Y="28.9019140625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.062370117188" Y="27.593099609375" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.022272338867" Y="27.469435546875" />
                  <Point X="2.017526123047" Y="27.445736328125" />
                  <Point X="2.013747924805" Y="27.41770703125" />
                  <Point X="2.0128984375" Y="27.4056796875" />
                  <Point X="2.01273034668" Y="27.381615234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.020831298828" Y="27.308044921875" />
                  <Point X="2.023800537109" Y="27.28903515625" />
                  <Point X="2.029143310547" Y="27.267111328125" />
                  <Point X="2.032468261719" Y="27.256306640625" />
                  <Point X="2.040734130859" Y="27.23421875" />
                  <Point X="2.045318847656" Y="27.22388671875" />
                  <Point X="2.055680908203" Y="27.20384375" />
                  <Point X="2.061458251953" Y="27.1941328125" />
                  <Point X="2.099532714844" Y="27.13801953125" />
                  <Point X="2.114627441406" Y="27.11860546875" />
                  <Point X="2.132811035156" Y="27.098109375" />
                  <Point X="2.141189208984" Y="27.0897734375" />
                  <Point X="2.158912841797" Y="27.074208984375" />
                  <Point X="2.168258300781" Y="27.06698046875" />
                  <Point X="2.224370361328" Y="27.02890625" />
                  <Point X="2.241269042969" Y="27.018251953125" />
                  <Point X="2.2613046875" Y="27.007892578125" />
                  <Point X="2.271631835938" Y="27.00330859375" />
                  <Point X="2.293728027344" Y="26.995037109375" />
                  <Point X="2.304539550781" Y="26.991708984375" />
                  <Point X="2.326474609375" Y="26.98636328125" />
                  <Point X="2.337598144531" Y="26.984345703125" />
                  <Point X="2.399131347656" Y="26.976927734375" />
                  <Point X="2.42461328125" Y="26.975580078125" />
                  <Point X="2.451410400391" Y="26.975966796875" />
                  <Point X="2.463094970703" Y="26.976857421875" />
                  <Point X="2.486266357422" Y="26.980072265625" />
                  <Point X="2.497753173828" Y="26.982396484375" />
                  <Point X="2.568913085938" Y="27.00142578125" />
                  <Point X="2.5827421875" Y="27.0056953125" />
                  <Point X="2.603947509766" Y="27.01313671875" />
                  <Point X="2.612135986328" Y="27.0164453125" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="2.805677490234" Y="27.125814453125" />
                  <Point X="3.940405273438" Y="27.78094921875" />
                  <Point X="4.022711425781" Y="27.6665625" />
                  <Point X="4.043959960938" Y="27.63703125" />
                  <Point X="4.136885253906" Y="27.48347265625" />
                  <Point X="3.172949462891" Y="26.743818359375" />
                  <Point X="3.168135986328" Y="26.7398671875" />
                  <Point X="3.152122314453" Y="26.72521875" />
                  <Point X="3.134671142578" Y="26.70660546875" />
                  <Point X="3.128577392578" Y="26.699423828125" />
                  <Point X="3.077363525391" Y="26.632611328125" />
                  <Point X="3.064460205078" Y="26.613470703125" />
                  <Point X="3.049420654297" Y="26.5879453125" />
                  <Point X="3.043864746094" Y="26.57694140625" />
                  <Point X="3.034224365234" Y="26.554302734375" />
                  <Point X="3.030139892578" Y="26.54266796875" />
                  <Point X="3.0110625" Y="26.474453125" />
                  <Point X="3.006224609375" Y="26.45465625" />
                  <Point X="3.002770996094" Y="26.43235546875" />
                  <Point X="3.001708740234" Y="26.4211015625" />
                  <Point X="3.000893310547" Y="26.39753515625" />
                  <Point X="3.001175048828" Y="26.386236328125" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.020359863281" Y="26.276671875" />
                  <Point X="3.026530761719" Y="26.25396875" />
                  <Point X="3.03573828125" Y="26.22685546875" />
                  <Point X="3.04034375" Y="26.215685546875" />
                  <Point X="3.050934326172" Y="26.19401953125" />
                  <Point X="3.056919433594" Y="26.1835234375" />
                  <Point X="3.099514160156" Y="26.11878125" />
                  <Point X="3.111739257813" Y="26.101423828125" />
                  <Point X="3.126288574219" Y="26.08418359375" />
                  <Point X="3.134075683594" Y="26.075998046875" />
                  <Point X="3.151315429688" Y="26.05991015625" />
                  <Point X="3.160023925781" Y="26.052703125" />
                  <Point X="3.178237792969" Y="26.039373046875" />
                  <Point X="3.187743164063" Y="26.03325" />
                  <Point X="3.24946875" Y="25.99850390625" />
                  <Point X="3.271543457031" Y="25.987890625" />
                  <Point X="3.297441162109" Y="25.977435546875" />
                  <Point X="3.308803955078" Y="25.973662109375" />
                  <Point X="3.331920898438" Y="25.967572265625" />
                  <Point X="3.343675048828" Y="25.965255859375" />
                  <Point X="3.427132080078" Y="25.9542265625" />
                  <Point X="3.440715087891" Y="25.95292578125" />
                  <Point X="3.464493652344" Y="25.9515078125" />
                  <Point X="3.473537841797" Y="25.951400390625" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.661752929688" Y="25.97401171875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.743561035156" Y="25.951703125" />
                  <Point X="4.752684570312" Y="25.91423046875" />
                  <Point X="4.78387109375" Y="25.713921875" />
                  <Point X="3.691991210938" Y="25.4213515625" />
                  <Point X="3.686023925781" Y="25.419541015625" />
                  <Point X="3.665624511719" Y="25.41213671875" />
                  <Point X="3.642384521484" Y="25.40162109375" />
                  <Point X="3.634007568359" Y="25.397318359375" />
                  <Point X="3.552013427734" Y="25.34992578125" />
                  <Point X="3.533582275391" Y="25.33779296875" />
                  <Point X="3.508755126953" Y="25.319296875" />
                  <Point X="3.499180664062" Y="25.311123046875" />
                  <Point X="3.481201416016" Y="25.293587890625" />
                  <Point X="3.472796630859" Y="25.2842265625" />
                  <Point X="3.423600341797" Y="25.221537109375" />
                  <Point X="3.410855957031" Y="25.2042109375" />
                  <Point X="3.399133056641" Y="25.18493359375" />
                  <Point X="3.393844970703" Y="25.174943359375" />
                  <Point X="3.384069580078" Y="25.1534765625" />
                  <Point X="3.380005615234" Y="25.1429296875" />
                  <Point X="3.373158935547" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.353977294922" Y="25.02484375" />
                  <Point X="3.351057861328" Y="25.002484375" />
                  <Point X="3.348954589844" Y="24.972693359375" />
                  <Point X="3.348886230469" Y="24.9603671875" />
                  <Point X="3.350346191406" Y="24.93580859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.3682734375" Y="24.83794921875" />
                  <Point X="3.373158935547" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794515625" />
                  <Point X="3.384067138672" Y="24.783970703125" />
                  <Point X="3.393839111328" Y="24.762509765625" />
                  <Point X="3.399125976562" Y="24.75251953125" />
                  <Point X="3.410851074219" Y="24.733236328125" />
                  <Point X="3.417289306641" Y="24.723943359375" />
                  <Point X="3.466485595703" Y="24.661255859375" />
                  <Point X="3.482125244141" Y="24.643685546875" />
                  <Point X="3.502749511719" Y="24.623220703125" />
                  <Point X="3.511960205078" Y="24.615189453125" />
                  <Point X="3.531332763672" Y="24.600376953125" />
                  <Point X="3.541494628906" Y="24.593595703125" />
                  <Point X="3.623488769531" Y="24.546201171875" />
                  <Point X="3.634884277344" Y="24.540119140625" />
                  <Point X="3.657480224609" Y="24.529033203125" />
                  <Point X="3.665951171875" Y="24.525376953125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="3.839774658203" Y="24.476486328125" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.76670703125" Y="24.102732421875" />
                  <Point X="4.761611816406" Y="24.068935546875" />
                  <Point X="4.727802734375" Y="23.920779296875" />
                  <Point X="3.43678125" Y="24.09074609375" />
                  <Point X="3.428622314453" Y="24.09146484375" />
                  <Point X="3.400095703125" Y="24.09195703125" />
                  <Point X="3.366722412109" Y="24.089158203125" />
                  <Point X="3.354483642578" Y="24.087322265625" />
                  <Point X="3.193558349609" Y="24.05234375" />
                  <Point X="3.172919433594" Y="24.047859375" />
                  <Point X="3.157880615234" Y="24.0432578125" />
                  <Point X="3.128765380859" Y="24.03163671875" />
                  <Point X="3.114688964844" Y="24.0246171875" />
                  <Point X="3.086858398438" Y="24.007724609375" />
                  <Point X="3.074128417969" Y="23.99847265625" />
                  <Point X="3.050373779297" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.942079833984" Y="23.849791015625" />
                  <Point X="2.921325927734" Y="23.82315234375" />
                  <Point X="2.906604736328" Y="23.79876953125" />
                  <Point X="2.900162597656" Y="23.786021484375" />
                  <Point X="2.888819824219" Y="23.75863671875" />
                  <Point X="2.884362304688" Y="23.7450703125" />
                  <Point X="2.877531005859" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.861216308594" Y="23.55183984375" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607910156" Y="23.485408203125" />
                  <Point X="2.86406640625" Y="23.469869140625" />
                  <Point X="2.871798339844" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.985599365234" Y="23.242103515625" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001739501953" Y="23.217650390625" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043487060547" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.189938720703" Y="23.058486328125" />
                  <Point X="4.087170166016" Y="22.370015625" />
                  <Point X="4.059846679688" Y="22.325802734375" />
                  <Point X="4.045493408203" Y="22.302576171875" />
                  <Point X="4.001274169922" Y="22.23974609375" />
                  <Point X="2.848452392578" Y="22.905330078125" />
                  <Point X="2.841190185547" Y="22.909115234375" />
                  <Point X="2.815036865234" Y="22.920482421875" />
                  <Point X="2.783123535156" Y="22.930673828125" />
                  <Point X="2.771107177734" Y="22.9336640625" />
                  <Point X="2.579580322266" Y="22.96825390625" />
                  <Point X="2.555016845703" Y="22.972689453125" />
                  <Point X="2.539352050781" Y="22.974193359375" />
                  <Point X="2.50799609375" Y="22.974595703125" />
                  <Point X="2.492304931641" Y="22.973494140625" />
                  <Point X="2.460132324219" Y="22.96853515625" />
                  <Point X="2.444840087891" Y="22.964861328125" />
                  <Point X="2.415068359375" Y="22.9550390625" />
                  <Point X="2.400588867188" Y="22.948890625" />
                  <Point X="2.241477050781" Y="22.86515234375" />
                  <Point X="2.221070800781" Y="22.854412109375" />
                  <Point X="2.208966064453" Y="22.846828125" />
                  <Point X="2.186034423828" Y="22.82993359375" />
                  <Point X="2.175207519531" Y="22.820623046875" />
                  <Point X="2.154248779297" Y="22.7996640625" />
                  <Point X="2.144939208984" Y="22.788837890625" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.036724487305" Y="22.594693359375" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.01983581543" Y="22.559806640625" />
                  <Point X="2.010012329102" Y="22.53003125" />
                  <Point X="2.006337524414" Y="22.514736328125" />
                  <Point X="2.001379760742" Y="22.482564453125" />
                  <Point X="2.000279418945" Y="22.46687109375" />
                  <Point X="2.00068347168" Y="22.43551953125" />
                  <Point X="2.002187866211" Y="22.419861328125" />
                  <Point X="2.03677746582" Y="22.228333984375" />
                  <Point X="2.041213500977" Y="22.20376953125" />
                  <Point X="2.043014526367" Y="22.19578125" />
                  <Point X="2.051234375" Y="22.168462890625" />
                  <Point X="2.064069091797" Y="22.137521484375" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.157675292969" Y="21.973779296875" />
                  <Point X="2.735893798828" Y="20.972275390625" />
                  <Point X="2.723752197266" Y="20.96391796875" />
                  <Point X="1.833914672852" Y="22.123576171875" />
                  <Point X="1.828653808594" Y="22.129853515625" />
                  <Point X="1.808831420898" Y="22.150373046875" />
                  <Point X="1.783250854492" Y="22.17199609375" />
                  <Point X="1.773297729492" Y="22.179353515625" />
                  <Point X="1.584400756836" Y="22.300796875" />
                  <Point X="1.560174316406" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.32375390625" />
                  <Point X="1.517467529297" Y="22.336125" />
                  <Point X="1.502549072266" Y="22.34111328125" />
                  <Point X="1.470930664062" Y="22.34884375" />
                  <Point X="1.455393066406" Y="22.351302734375" />
                  <Point X="1.424125976562" Y="22.35362109375" />
                  <Point X="1.408396362305" Y="22.35348046875" />
                  <Point X="1.201805297852" Y="22.334470703125" />
                  <Point X="1.175309448242" Y="22.332033203125" />
                  <Point X="1.161231811523" Y="22.329662109375" />
                  <Point X="1.133582641602" Y="22.32283203125" />
                  <Point X="1.120010986328" Y="22.318373046875" />
                  <Point X="1.092624023438" Y="22.307029296875" />
                  <Point X="1.079874023438" Y="22.300583984375" />
                  <Point X="1.055490722656" Y="22.285861328125" />
                  <Point X="1.043857421875" Y="22.277583984375" />
                  <Point X="0.884333068848" Y="22.144943359375" />
                  <Point X="0.863873779297" Y="22.127931640625" />
                  <Point X="0.852652893066" Y="22.116908203125" />
                  <Point X="0.832183410645" Y="22.09316015625" />
                  <Point X="0.822934875488" Y="22.080435546875" />
                  <Point X="0.806043151855" Y="22.052611328125" />
                  <Point X="0.799020874023" Y="22.03853515625" />
                  <Point X="0.787395263672" Y="22.0094140625" />
                  <Point X="0.782791992188" Y="21.994369140625" />
                  <Point X="0.735095031738" Y="21.77492578125" />
                  <Point X="0.728977844238" Y="21.746783203125" />
                  <Point X="0.727585144043" Y="21.738712890625" />
                  <Point X="0.724724731445" Y="21.71032421875" />
                  <Point X="0.724742248535" Y="21.676830078125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.750529968262" Y="21.47477734375" />
                  <Point X="0.83309173584" Y="20.847658203125" />
                  <Point X="0.655064880371" Y="21.512064453125" />
                  <Point X="0.652606201172" Y="21.519876953125" />
                  <Point X="0.642143920898" Y="21.546423828125" />
                  <Point X="0.626786987305" Y="21.576185546875" />
                  <Point X="0.620407653809" Y="21.586791015625" />
                  <Point X="0.475291503906" Y="21.795875" />
                  <Point X="0.456680053711" Y="21.82269140625" />
                  <Point X="0.446671051025" Y="21.834828125" />
                  <Point X="0.424789459229" Y="21.85728125" />
                  <Point X="0.412916564941" Y="21.86759765625" />
                  <Point X="0.386662322998" Y="21.886841796875" />
                  <Point X="0.373245544434" Y="21.895060546875" />
                  <Point X="0.345241882324" Y="21.909169921875" />
                  <Point X="0.33065512085" Y="21.915060546875" />
                  <Point X="0.106096191406" Y="21.98475390625" />
                  <Point X="0.077295898438" Y="21.993693359375" />
                  <Point X="0.063381523132" Y="21.996888671875" />
                  <Point X="0.035228931427" Y="22.001158203125" />
                  <Point X="0.020990709305" Y="22.002232421875" />
                  <Point X="-0.008650790215" Y="22.002234375" />
                  <Point X="-0.022894956589" Y="22.001162109375" />
                  <Point X="-0.051060779572" Y="21.996892578125" />
                  <Point X="-0.064982582092" Y="21.9936953125" />
                  <Point X="-0.289541381836" Y="21.924" />
                  <Point X="-0.318341674805" Y="21.9150625" />
                  <Point X="-0.332927368164" Y="21.909171875" />
                  <Point X="-0.360927459717" Y="21.895064453125" />
                  <Point X="-0.374342163086" Y="21.88684765625" />
                  <Point X="-0.400598205566" Y="21.867603515625" />
                  <Point X="-0.412474639893" Y="21.857283203125" />
                  <Point X="-0.434359527588" Y="21.834826171875" />
                  <Point X="-0.444367645264" Y="21.822689453125" />
                  <Point X="-0.589483947754" Y="21.613603515625" />
                  <Point X="-0.60809552002" Y="21.5867890625" />
                  <Point X="-0.612467834473" Y="21.57987109375" />
                  <Point X="-0.625975280762" Y="21.55473828125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.688288391113" Y="21.342123046875" />
                  <Point X="-0.985425109863" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.667063802452" Y="21.037795682674" />
                  <Point X="2.686718182631" Y="21.057450062854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.917424810658" Y="22.28815669088" />
                  <Point X="4.037441747076" Y="22.408173627299" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.608732415638" Y="21.113814584286" />
                  <Point X="2.637542566435" Y="21.142624735083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.832250235462" Y="22.337332404109" />
                  <Point X="3.961422865737" Y="22.466505034384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.550401028824" Y="21.189833485897" />
                  <Point X="2.588366950238" Y="21.227799407311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747075660265" Y="22.386508117338" />
                  <Point X="3.885403984397" Y="22.52483644147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49206964201" Y="21.265852387508" />
                  <Point X="2.539191334042" Y="21.31297407954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661901085069" Y="22.435683830567" />
                  <Point X="3.809385103057" Y="22.583167848556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433738255196" Y="21.34187128912" />
                  <Point X="2.490015717845" Y="21.398148751769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.576726509872" Y="22.484859543796" />
                  <Point X="3.733366221718" Y="22.641499255642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.375406868382" Y="21.417890190731" />
                  <Point X="2.440840101648" Y="21.483323423998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.491551934676" Y="22.534035257025" />
                  <Point X="3.657347340378" Y="22.699830662727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.317075481568" Y="21.493909092343" />
                  <Point X="2.391664485452" Y="21.568498096227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.406377359479" Y="22.583210970254" />
                  <Point X="3.581328459038" Y="22.758162069813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.258744094754" Y="21.569927993954" />
                  <Point X="2.342488869255" Y="21.653672768455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.321202784283" Y="22.632386683483" />
                  <Point X="3.505309577699" Y="22.816493476899" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.623347268681" Y="23.934531167882" />
                  <Point X="4.762297086489" Y="24.073480985689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.20041270794" Y="21.645946895565" />
                  <Point X="2.293313253058" Y="21.738847440684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.236028209086" Y="22.681562396712" />
                  <Point X="3.429290696359" Y="22.874824883985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.504626871663" Y="23.950161059289" />
                  <Point X="4.779227491271" Y="24.224761678897" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.142081321126" Y="21.721965797177" />
                  <Point X="2.244137636862" Y="21.824022112913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.15085363389" Y="22.730738109941" />
                  <Point X="3.353271815019" Y="22.933156291071" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.385906474645" Y="23.965790950696" />
                  <Point X="4.673268672404" Y="24.253153148456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.083749934311" Y="21.797984698788" />
                  <Point X="2.194962020665" Y="21.909196785142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.065679058693" Y="22.77991382317" />
                  <Point X="3.27725293368" Y="22.991487698156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267186077626" Y="23.981420842103" />
                  <Point X="4.567309853537" Y="24.281544618014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.025418547497" Y="21.8740036004" />
                  <Point X="2.145786312561" Y="21.994371365463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.980504483497" Y="22.829089536399" />
                  <Point X="3.20123405234" Y="23.049819105242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.148465680608" Y="23.99705073351" />
                  <Point X="4.461351034671" Y="24.309936087573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.967087160683" Y="21.950022502011" />
                  <Point X="2.096610316212" Y="22.079545657539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8953299083" Y="22.878265249628" />
                  <Point X="3.125215351663" Y="23.108150692991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.029745283589" Y="24.012680624917" />
                  <Point X="4.355392215804" Y="24.338327557132" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.811384647916" Y="20.928670277669" />
                  <Point X="0.821141751672" Y="20.938427381425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.908755773869" Y="22.026041403622" />
                  <Point X="2.051221164725" Y="22.168506794478" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.806062669114" Y="22.923348298868" />
                  <Point X="3.049406283916" Y="23.166691913669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.911024886571" Y="24.028310516324" />
                  <Point X="4.249433396937" Y="24.36671902669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782993124334" Y="21.034629042513" />
                  <Point X="0.805511914151" Y="21.057147832329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.850424387055" Y="22.102060305234" />
                  <Point X="2.027586989658" Y="22.279222907837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.6956548683" Y="22.947290786478" />
                  <Point X="2.987504438975" Y="23.239140357153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792304489553" Y="24.043940407731" />
                  <Point X="4.14347457807" Y="24.395110496249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.754601600753" Y="21.140587807357" />
                  <Point X="0.789882076629" Y="21.175868283233" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.78474602855" Y="22.170732235154" />
                  <Point X="2.007035134432" Y="22.393021341036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.58185660235" Y="22.967842808954" />
                  <Point X="2.93492985263" Y="23.320916059234" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.673584092534" Y="24.059570299138" />
                  <Point X="4.037515759204" Y="24.423501965808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.726210077171" Y="21.246546572201" />
                  <Point X="0.774252239108" Y="21.294588734138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.703737708717" Y="22.224074203746" />
                  <Point X="2.010168689371" Y="22.530505184401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.444369615065" Y="22.964706110095" />
                  <Point X="2.884659138069" Y="23.404995633098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.554863695516" Y="24.075200190545" />
                  <Point X="3.931556940337" Y="24.451893435366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.69781855359" Y="21.352505337045" />
                  <Point X="0.758622401587" Y="21.413309185042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.621961799691" Y="22.276648583146" />
                  <Point X="2.859475384312" Y="23.514162167767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.436117759556" Y="24.090804543011" />
                  <Point X="3.825598097407" Y="24.480284880862" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.669427030009" Y="21.458464101889" />
                  <Point X="0.742992554866" Y="21.532029626746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.538190315958" Y="22.327227387838" />
                  <Point X="2.871190023322" Y="23.660227095202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.282677503242" Y="24.071714575122" />
                  <Point X="3.719639098689" Y="24.508676170569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.635640360334" Y="21.55902772064" />
                  <Point X="0.727362698266" Y="21.650750058572" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.429812123022" Y="22.353199483328" />
                  <Point X="3.019422158392" Y="23.942809518698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.077630701515" Y="24.001018061821" />
                  <Point X="3.623061045858" Y="24.546448406163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.582979710928" Y="21.640717359659" />
                  <Point X="0.740068088927" Y="21.797805737658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.284326343808" Y="22.342063992539" />
                  <Point X="3.538114016204" Y="24.595851664935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.609447157042" Y="25.667184805773" />
                  <Point X="4.766669394061" Y="25.824407042792" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.527936263148" Y="21.720024200305" />
                  <Point X="0.777379499434" Y="21.969467436591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.12935514467" Y="22.321443081827" />
                  <Point X="3.467748790314" Y="24.659836727471" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.425920795504" Y="25.618008732661" />
                  <Point X="4.746704438555" Y="25.938792375712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.074847048752" Y="20.25159117683" />
                  <Point X="-0.945960977279" Y="20.380477248303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.472892865388" Y="21.79933109097" />
                  <Point X="3.409318533335" Y="24.735756758917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.242394433967" Y="25.568832659549" />
                  <Point X="4.72040098024" Y="26.046839205822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133533553997" Y="20.327254960011" />
                  <Point X="-0.896785261981" Y="20.564003252026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409392357175" Y="21.870180871183" />
                  <Point X="3.369892268045" Y="24.830680782052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058868072429" Y="25.519656586436" />
                  <Point X="4.642318558634" Y="26.103107072642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120586187702" Y="20.474552614731" />
                  <Point X="-0.847609546683" Y="20.74752925575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322463933875" Y="21.917602736308" />
                  <Point X="3.349803226345" Y="24.944942028778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.875341710891" Y="25.470480513324" />
                  <Point X="4.487598800437" Y="26.08273760287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.14056360436" Y="20.588925486499" />
                  <Point X="-0.798433831385" Y="20.931055259473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.219934376466" Y="21.949423467324" />
                  <Point X="3.367863594568" Y="25.097352685426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.691806396778" Y="25.421295487637" />
                  <Point X="4.332879042239" Y="26.062368133097" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.162853685547" Y="20.700985693737" />
                  <Point X="-0.749258116087" Y="21.114581263197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.117404819056" Y="21.98124419834" />
                  <Point X="4.178159284041" Y="26.041998663325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.185578230504" Y="20.812611437206" />
                  <Point X="-0.700082400789" Y="21.29810726692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.00404387082" Y="22.002233538529" />
                  <Point X="4.023439525843" Y="26.021629193553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.229276671067" Y="20.903263285068" />
                  <Point X="-0.650907258362" Y="21.481632697773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.172085787514" Y="21.960454168621" />
                  <Point X="3.868719767646" Y="26.00125972378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.295988995865" Y="20.970901248695" />
                  <Point X="-0.444746156266" Y="21.822144088295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.395687385813" Y="21.871202858747" />
                  <Point X="3.714000009448" Y="25.980890254008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.367567081234" Y="21.033673451752" />
                  <Point X="3.559281162421" Y="25.960521695406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.439145166602" Y="21.096445654809" />
                  <Point X="3.419627510448" Y="25.955218331859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512391582733" Y="21.157549527103" />
                  <Point X="3.304988172041" Y="25.974929281878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.606277673966" Y="21.198013724296" />
                  <Point X="3.214114085025" Y="26.018405483287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.728783801624" Y="21.209857885063" />
                  <Point X="3.135772714558" Y="26.074414401246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.854870211487" Y="21.218121763625" />
                  <Point X="3.078194404342" Y="26.151186379455" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.985829067718" Y="21.22151319582" />
                  <Point X="3.03162513668" Y="26.238967400218" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.503800085616" Y="20.837892466348" />
                  <Point X="-2.500001536281" Y="20.841691015683" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.282483084111" Y="21.059209467853" />
                  <Point X="3.005756210847" Y="26.347448762811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586774755547" Y="20.889268084842" />
                  <Point X="3.015974665247" Y="26.492017505636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.580500829272" Y="27.056543669661" />
                  <Point X="4.088080098052" Y="27.564122938442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.669749425479" Y="20.940643703336" />
                  <Point X="4.036711727811" Y="27.647104856625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752287997099" Y="20.992455420141" />
                  <Point X="3.980493014362" Y="27.725236431602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.828193433623" Y="21.050900272042" />
                  <Point X="3.849195680084" Y="27.72828938575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.904098870147" Y="21.109345123944" />
                  <Point X="3.531319899224" Y="27.544763893315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.980004306671" Y="21.167789975845" />
                  <Point X="3.213444118364" Y="27.36123840088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799413764707" Y="21.482730806235" />
                  <Point X="2.895568337504" Y="27.177712908446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.615888238445" Y="21.800606620922" />
                  <Point X="2.592692088666" Y="27.009186948034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.432362712183" Y="22.118482435609" />
                  <Point X="2.424736711599" Y="26.975581859392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.312575938392" Y="22.372619497826" />
                  <Point X="2.306126740631" Y="26.991322176849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.33059435611" Y="22.488951368534" />
                  <Point X="2.215428158375" Y="27.034973883019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.383969803982" Y="22.569926209087" />
                  <Point X="2.138526605305" Y="27.092422618374" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.462002809479" Y="22.626243492015" />
                  <Point X="2.079417935732" Y="27.167664237226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.571329743353" Y="22.651266846567" />
                  <Point X="2.032806422966" Y="27.255403012886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.803071589824" Y="22.553875288522" />
                  <Point X="2.013369494129" Y="27.370316372474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120948284986" Y="22.370348881785" />
                  <Point X="2.038382249689" Y="27.52967941646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438824613503" Y="22.186822841694" />
                  <Point X="2.191472655568" Y="27.817120110765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.75670094202" Y="22.003296801602" />
                  <Point X="2.374998782425" Y="28.134996526047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.84450574089" Y="22.049842291157" />
                  <Point X="2.558524909281" Y="28.452872941328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.902570611149" Y="22.126127709324" />
                  <Point X="2.742051036137" Y="28.77074935661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.960635481407" Y="22.202413127491" />
                  <Point X="2.771661752772" Y="28.93471036167" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.016709084344" Y="22.280689812979" />
                  <Point X="2.693147116264" Y="28.990546013588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.066898852316" Y="22.364850333433" />
                  <Point X="2.613430327602" Y="29.045179513351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.117088620287" Y="22.449010853887" />
                  <Point X="-3.442932326289" Y="23.123167147886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950198715652" Y="23.615900758523" />
                  <Point X="2.528538042565" Y="29.09463751674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.167278388258" Y="22.533171374341" />
                  <Point X="-4.020351521058" Y="22.680098241542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.965017532716" Y="23.735432229884" />
                  <Point X="2.443645844904" Y="29.144095607504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.01929151898" Y="23.815508532045" />
                  <Point X="2.358753647243" Y="29.193553698268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.097569636974" Y="23.871580702477" />
                  <Point X="2.273861449581" Y="29.243011789032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.203808839489" Y="23.899691788387" />
                  <Point X="2.184157184849" Y="29.287657812725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.352256976007" Y="23.885593940294" />
                  <Point X="2.092617177157" Y="29.330468093459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506976179239" Y="23.865225025488" />
                  <Point X="2.001077169465" Y="29.373278374192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.661695491926" Y="23.844856001226" />
                  <Point X="1.909537161773" Y="29.416088654926" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.816414804613" Y="23.824486976964" />
                  <Point X="1.814895964414" Y="29.455797745992" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.971134117301" Y="23.804117952703" />
                  <Point X="1.716305009489" Y="29.491557079493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.125853429988" Y="23.783748928441" />
                  <Point X="1.617714054565" Y="29.527316412993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.280572742675" Y="23.763379904179" />
                  <Point X="1.51912309964" Y="29.563075746494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.435292055363" Y="23.743010879917" />
                  <Point X="-3.47991192353" Y="24.698391011749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.315407415593" Y="24.862895519687" />
                  <Point X="1.417301087319" Y="29.595604022598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.59001136805" Y="23.722641855655" />
                  <Point X="-3.667481483091" Y="24.645171740614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.299111160605" Y="25.0135420631" />
                  <Point X="1.309079760578" Y="29.621732984283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.675727225431" Y="23.771276286699" />
                  <Point X="-3.851007577235" Y="24.595995934895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333935925856" Y="25.113067586275" />
                  <Point X="1.200857564542" Y="29.647861076673" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.70306232966" Y="23.878291470896" />
                  <Point X="-4.03453367138" Y="24.546820129177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.394767281427" Y="25.186586519129" />
                  <Point X="1.092635368507" Y="29.673989169063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.730397415212" Y="23.985306673769" />
                  <Point X="-4.218059765524" Y="24.497644323458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.477496380839" Y="25.238207708143" />
                  <Point X="0.984413172472" Y="29.700117261454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.751198334688" Y="24.098856042719" />
                  <Point X="-4.401585859668" Y="24.448468517739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.580967034907" Y="25.2690873425" />
                  <Point X="0.876190976437" Y="29.726245353844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76800914282" Y="24.216395523012" />
                  <Point X="-4.585111953812" Y="24.39929271202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686925843765" Y="25.297478822068" />
                  <Point X="0.14527092456" Y="29.129675590392" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.195299425328" Y="29.17970409116" />
                  <Point X="0.760558854981" Y="29.744963520813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.784819950953" Y="24.333935003305" />
                  <Point X="-4.768638047957" Y="24.350116906301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.792884579733" Y="25.325870374524" />
                  <Point X="-0.0301951356" Y="29.088559818658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.272753126045" Y="29.391508080303" />
                  <Point X="0.63894540741" Y="29.757700361668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.898843315702" Y="25.354261926981" />
                  <Point X="-0.127567172186" Y="29.125538070498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.321928203089" Y="29.575033445772" />
                  <Point X="0.517331959839" Y="29.770437202523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004802051671" Y="25.382653479438" />
                  <Point X="-0.192906501613" Y="29.194549029496" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.371103280133" Y="29.758558811241" />
                  <Point X="0.395718512268" Y="29.783174043377" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.11076078764" Y="25.411045031895" />
                  <Point X="-0.232938646433" Y="29.288867173102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216719523608" Y="25.439436584351" />
                  <Point X="-0.261330126576" Y="29.394825981384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.322678259577" Y="25.467828136808" />
                  <Point X="-0.289721606719" Y="29.500784789666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.428636995546" Y="25.496219689265" />
                  <Point X="-3.718566639426" Y="26.206290045385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.422318893634" Y="26.502537791176" />
                  <Point X="-0.318113086862" Y="29.606743597948" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.534595731514" Y="25.524611241721" />
                  <Point X="-3.844814153461" Y="26.214392819774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.441237852395" Y="26.617969120841" />
                  <Point X="-0.346504567005" Y="29.712702406231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640554467483" Y="25.553002794178" />
                  <Point X="-3.963534642818" Y="26.230022618844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.487394483045" Y="26.706162778616" />
                  <Point X="-0.417066516896" Y="29.776490744765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.746513203452" Y="25.581394346635" />
                  <Point X="-4.082255132174" Y="26.245652417913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.552024104548" Y="26.775883445539" />
                  <Point X="-0.569225541466" Y="29.758682008621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770685474615" Y="25.691572363897" />
                  <Point X="-4.20097562153" Y="26.261282216983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.628042912201" Y="26.834214926312" />
                  <Point X="-0.721384566036" Y="29.740873272476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.747352452543" Y="25.849255674395" />
                  <Point X="-4.319696110886" Y="26.276912016052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.704061872616" Y="26.892546254321" />
                  <Point X="-0.873543590606" Y="29.723064536332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.71568357337" Y="26.015274841993" />
                  <Point X="-4.438416600242" Y="26.292541815121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.780080864565" Y="26.950877550798" />
                  <Point X="-3.002830405432" Y="27.728128009932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753251604402" Y="27.977706810961" />
                  <Point X="-1.643552934038" Y="29.087405481325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.512634649906" Y="29.218323765458" />
                  <Point X="-1.047154855823" Y="29.683803559541" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665744431858" Y="26.199564271931" />
                  <Point X="-4.557137089598" Y="26.308171614191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.856099856514" Y="27.009208847275" />
                  <Point X="-3.135836093765" Y="27.729472610024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.754598745938" Y="28.110709957851" />
                  <Point X="-1.83638028862" Y="29.028928415168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462613393631" Y="29.402695310157" />
                  <Point X="-1.233847355368" Y="29.631461348421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.932118848462" Y="27.067540143752" />
                  <Point X="-3.242852999755" Y="27.756805992459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.781931557656" Y="28.217727434558" />
                  <Point X="-1.943759563687" Y="29.055899428527" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.474349563416" Y="29.525309428798" />
                  <Point X="-1.420539854913" Y="29.579119137301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008137840411" Y="27.125871440228" />
                  <Point X="-3.328619506216" Y="27.805389774424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.830514610678" Y="28.303494669962" />
                  <Point X="-2.032337701627" Y="29.101671579013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08415683236" Y="27.184202736705" />
                  <Point X="-3.413794105833" Y="27.854565463232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.879690262163" Y="28.388669306902" />
                  <Point X="-2.108147400067" Y="29.160212168998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160175824308" Y="27.242534033182" />
                  <Point X="-3.498968705451" Y="27.90374115204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.928865913647" Y="28.473843943843" />
                  <Point X="-2.16729357415" Y="29.23541628334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.206352927534" Y="27.330707218382" />
                  <Point X="-3.584143305068" Y="27.952916840847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.978041565132" Y="28.559018580784" />
                  <Point X="-2.364072588403" Y="29.172987557513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.017989820765" Y="27.653420613576" />
                  <Point X="-3.669317904686" Y="28.002092529655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027217216617" Y="28.644193217724" />
                  <Point X="-2.682670730018" Y="28.988739704323" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.826585021973" Y="20.137837890625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464319122314" Y="21.478455078125" />
                  <Point X="0.319202911377" Y="21.6875390625" />
                  <Point X="0.300591491699" Y="21.71435546875" />
                  <Point X="0.274337280273" Y="21.733599609375" />
                  <Point X="0.049778324127" Y="21.80329296875" />
                  <Point X="0.020978147507" Y="21.812232421875" />
                  <Point X="-0.008663235664" Y="21.812234375" />
                  <Point X="-0.233222183228" Y="21.7425390625" />
                  <Point X="-0.262022369385" Y="21.7336015625" />
                  <Point X="-0.288278564453" Y="21.714357421875" />
                  <Point X="-0.433394775391" Y="21.505271484375" />
                  <Point X="-0.452006347656" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.504762542725" Y="21.292947265625" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.067647216797" Y="20.055607421875" />
                  <Point X="-1.100228637695" Y="20.061931640625" />
                  <Point X="-1.351589477539" Y="20.126603515625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.363330322266" Y="20.734943359375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282348633" Y="20.79737109375" />
                  <Point X="-1.593027709961" Y="20.978681640625" />
                  <Point X="-1.619543212891" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.923637817383" Y="21.032220703125" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.218520996094" Y="20.873435546875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.285298583984" Y="20.809388671875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.808054443359" Y="20.8028046875" />
                  <Point X="-2.855842285156" Y="20.83239453125" />
                  <Point X="-3.216723144531" Y="21.11026171875" />
                  <Point X="-3.228580810547" Y="21.119390625" />
                  <Point X="-3.165846435547" Y="21.22805078125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.513981201172" Y="22.43123828125" />
                  <Point X="-2.529353515625" Y="22.446609375" />
                  <Point X="-2.531323974609" Y="22.448580078125" />
                  <Point X="-2.560154296875" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.738253662109" Y="22.37190625" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.123955078125" Y="22.103275390625" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.420432617188" Y="22.58671875" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.318245117188" Y="22.6910078125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.138994628906" Y="23.63034765625" />
                  <Point X="-3.138118652344" Y="23.633728515625" />
                  <Point X="-3.140325927734" Y="23.66540234375" />
                  <Point X="-3.161161376953" Y="23.689361328125" />
                  <Point X="-3.184633789062" Y="23.70317578125" />
                  <Point X="-3.187644042969" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.404560302734" Y="23.687068359375" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.91262890625" Y="23.9310078125" />
                  <Point X="-4.927392578125" Y="23.988806640625" />
                  <Point X="-4.995846191406" Y="24.467427734375" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.872565917969" Y="24.51897265625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541896972656" Y="24.87857421875" />
                  <Point X="-3.517298828125" Y="24.895646484375" />
                  <Point X="-3.514144042969" Y="24.8978359375" />
                  <Point X="-3.494898681641" Y="24.924091796875" />
                  <Point X="-3.486701171875" Y="24.950505859375" />
                  <Point X="-3.485649414063" Y="24.953892578125" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.493846679688" Y="25.00995703125" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514144042969" Y="25.039603515625" />
                  <Point X="-3.5387421875" Y="25.05667578125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.726128417969" Y="25.111279296875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.927159179688" Y="25.93212109375" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.779851074219" Y="26.504919921875" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.695963378906" Y="26.518087890625" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.73170703125" Y="26.395865234375" />
                  <Point X="-3.677263427734" Y="26.41303125" />
                  <Point X="-3.670281005859" Y="26.415232421875" />
                  <Point X="-3.651535888672" Y="26.4260546875" />
                  <Point X="-3.639120361328" Y="26.44378515625" />
                  <Point X="-3.617274658203" Y="26.496525390625" />
                  <Point X="-3.614472900391" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.524603515625" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.642675048828" Y="26.596146484375" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.756714599609" Y="26.69345703125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.196983886719" Y="27.72366796875" />
                  <Point X="-4.160016113281" Y="27.78700390625" />
                  <Point X="-3.795002685547" Y="28.256177734375" />
                  <Point X="-3.774671142578" Y="28.282310546875" />
                  <Point X="-3.738135253906" Y="28.261216796875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515380859" Y="27.92043359375" />
                  <Point X="-3.062690673828" Y="27.913798828125" />
                  <Point X="-3.052959472656" Y="27.91294921875" />
                  <Point X="-3.031504150391" Y="27.91577734375" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.959431884766" Y="27.981224609375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.944708007812" Y="28.103666015625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-2.994938720703" Y="28.208287109375" />
                  <Point X="-3.307278808594" Y="28.74927734375" />
                  <Point X="-2.817263427734" Y="29.124966796875" />
                  <Point X="-2.752873779297" Y="29.174333984375" />
                  <Point X="-2.177993408203" Y="29.493724609375" />
                  <Point X="-2.141548828125" Y="29.51397265625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247802734" Y="29.273662109375" />
                  <Point X="-1.86685534668" Y="29.229728515625" />
                  <Point X="-1.856031860352" Y="29.22409375" />
                  <Point X="-1.835124267578" Y="29.218490234375" />
                  <Point X="-1.813807861328" Y="29.22225" />
                  <Point X="-1.725907348633" Y="29.25866015625" />
                  <Point X="-1.714633789062" Y="29.263330078125" />
                  <Point X="-1.696904418945" Y="29.27574609375" />
                  <Point X="-1.686082885742" Y="29.294490234375" />
                  <Point X="-1.657472900391" Y="29.385228515625" />
                  <Point X="-1.653803588867" Y="29.3968671875" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.656791625977" Y="29.455447265625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.051447753906" Y="29.87992578125" />
                  <Point X="-0.968083618164" Y="29.903296875" />
                  <Point X="-0.27116015625" Y="29.98486328125" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.213418167114" Y="29.95012109375" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282123566" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594043732" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.076419059753" Y="29.3928828125" />
                  <Point X="0.236648422241" Y="29.990869140625" />
                  <Point X="0.787423095703" Y="29.9331875" />
                  <Point X="0.860208618164" Y="29.925564453125" />
                  <Point X="1.436795532227" Y="29.786359375" />
                  <Point X="1.508457763672" Y="29.769056640625" />
                  <Point X="1.884566894531" Y="29.632638671875" />
                  <Point X="1.931042602539" Y="29.61578125" />
                  <Point X="2.293934082031" Y="29.4460703125" />
                  <Point X="2.338688720703" Y="29.425138671875" />
                  <Point X="2.689272460938" Y="29.220888671875" />
                  <Point X="2.732519775391" Y="29.195693359375" />
                  <Point X="3.063163574219" Y="28.960556640625" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.993537353516" Y="28.8263359375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.205822998047" Y="27.420353515625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.209464599609" Y="27.330791015625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.256756347656" Y="27.24469921875" />
                  <Point X="2.256759033203" Y="27.2446953125" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.331052001953" Y="27.18612890625" />
                  <Point X="2.3382421875" Y="27.18125" />
                  <Point X="2.360338378906" Y="27.172978515625" />
                  <Point X="2.421871582031" Y="27.165560546875" />
                  <Point X="2.448668701172" Y="27.165947265625" />
                  <Point X="2.519828613281" Y="27.1849765625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.710677978516" Y="27.290361328125" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.176936035156" Y="27.77753515625" />
                  <Point X="4.20259375" Y="27.741876953125" />
                  <Point X="4.386918457031" Y="27.43727734375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.294077636719" Y="27.364599609375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279372314453" Y="26.583833984375" />
                  <Point X="3.228158447266" Y="26.517021484375" />
                  <Point X="3.213118896484" Y="26.49149609375" />
                  <Point X="3.194041503906" Y="26.42328125" />
                  <Point X="3.191594970703" Y="26.41453125" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.206439941406" Y="26.31506640625" />
                  <Point X="3.215647460938" Y="26.287953125" />
                  <Point X="3.2582421875" Y="26.2232109375" />
                  <Point X="3.263705078125" Y="26.214908203125" />
                  <Point X="3.280944824219" Y="26.1988203125" />
                  <Point X="3.342670410156" Y="26.16407421875" />
                  <Point X="3.368568115234" Y="26.153619140625" />
                  <Point X="3.452025146484" Y="26.14258984375" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.636954101562" Y="26.16238671875" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.928169433594" Y="25.99664453125" />
                  <Point X="4.939189453125" Y="25.951380859375" />
                  <Point X="4.997274902344" Y="25.5783046875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.893447753906" Y="25.546580078125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087646484" Y="25.232818359375" />
                  <Point X="3.647093505859" Y="25.18542578125" />
                  <Point X="3.622266357422" Y="25.1669296875" />
                  <Point X="3.573070068359" Y="25.104240234375" />
                  <Point X="3.566760498047" Y="25.096201171875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.540586181641" Y="24.98910546875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.554881835938" Y="24.8736875" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566757080078" Y="24.841244140625" />
                  <Point X="3.615953369141" Y="24.778556640625" />
                  <Point X="3.636577636719" Y="24.758091796875" />
                  <Point X="3.718571777344" Y="24.710697265625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.888949951172" Y="24.660013671875" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.954583984375" Y="24.074408203125" />
                  <Point X="4.948430664062" Y="24.03359375" />
                  <Point X="4.874546386719" Y="23.709822265625" />
                  <Point X="4.748303222656" Y="23.72644140625" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394839599609" Y="23.901658203125" />
                  <Point X="3.233914306641" Y="23.8666796875" />
                  <Point X="3.213275390625" Y="23.8621953125" />
                  <Point X="3.185444824219" Y="23.845302734375" />
                  <Point X="3.088175537109" Y="23.728318359375" />
                  <Point X="3.075700683594" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.050416992188" Y="23.5344296875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360839844" Y="23.483376953125" />
                  <Point X="3.145419189453" Y="23.344853515625" />
                  <Point X="3.156841064453" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.305603759766" Y="23.209224609375" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.221473632812" Y="22.225919921875" />
                  <Point X="4.204126953125" Y="22.197849609375" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.943628662109" Y="22.053634765625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737339599609" Y="22.7466875" />
                  <Point X="2.545812744141" Y="22.78127734375" />
                  <Point X="2.521249267578" Y="22.785712890625" />
                  <Point X="2.489076660156" Y="22.78075390625" />
                  <Point X="2.32996484375" Y="22.697015625" />
                  <Point X="2.30955859375" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.204860595703" Y="22.506205078125" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453626953125" />
                  <Point X="2.223752685547" Y="22.262099609375" />
                  <Point X="2.228188720703" Y="22.23753515625" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.322219726562" Y="22.068779296875" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.855879150391" Y="20.82448828125" />
                  <Point X="2.835303222656" Y="20.809791015625" />
                  <Point X="2.679774902344" Y="20.70912109375" />
                  <Point X="2.591390136719" Y="20.824306640625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670547973633" Y="22.019533203125" />
                  <Point X="1.481650878906" Y="22.1409765625" />
                  <Point X="1.457424560547" Y="22.15655078125" />
                  <Point X="1.425806152344" Y="22.16428125" />
                  <Point X="1.21921496582" Y="22.145271484375" />
                  <Point X="1.192719238281" Y="22.142833984375" />
                  <Point X="1.165332275391" Y="22.131490234375" />
                  <Point X="1.005807800293" Y="21.998849609375" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.96845690918" Y="21.954013671875" />
                  <Point X="0.920759887695" Y="21.7345703125" />
                  <Point X="0.914642700195" Y="21.706427734375" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.93890435791" Y="21.499578125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.013813537598" Y="20.04101953125" />
                  <Point X="0.994354248047" Y="20.03675390625" />
                  <Point X="0.860200805664" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#206" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.171277830952" Y="4.994342073515" Z="2.25" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="-0.283128192046" Y="5.06580346622" Z="2.25" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.25" />
                  <Point X="-1.071101298622" Y="4.959350642157" Z="2.25" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.25" />
                  <Point X="-1.712519868411" Y="4.480202116458" Z="2.25" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.25" />
                  <Point X="-1.711315179565" Y="4.431543099596" Z="2.25" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.25" />
                  <Point X="-1.751198562558" Y="4.336134458792" Z="2.25" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.25" />
                  <Point X="-1.849922576037" Y="4.305358990207" Z="2.25" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.25" />
                  <Point X="-2.111557992981" Y="4.580278730407" Z="2.25" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.25" />
                  <Point X="-2.208432099115" Y="4.568711462616" Z="2.25" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.25" />
                  <Point X="-2.853841234747" Y="4.195926327517" Z="2.25" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.25" />
                  <Point X="-3.044395923203" Y="3.214568065397" Z="2.25" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.25" />
                  <Point X="-3.000673879805" Y="3.130588239903" Z="2.25" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.25" />
                  <Point X="-3.00094265274" Y="3.047860915146" Z="2.25" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.25" />
                  <Point X="-3.064488225704" Y="2.994890819368" Z="2.25" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.25" />
                  <Point X="-3.7192912829" Y="3.335798025543" Z="2.25" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.25" />
                  <Point X="-3.840621815225" Y="3.318160495151" Z="2.25" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.25" />
                  <Point X="-4.246321928298" Y="2.780154074928" Z="2.25" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.25" />
                  <Point X="-3.793308638411" Y="1.685070197349" Z="2.25" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.25" />
                  <Point X="-3.693181665063" Y="1.604340000402" Z="2.25" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.25" />
                  <Point X="-3.669624191117" Y="1.546940339246" Z="2.25" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.25" />
                  <Point X="-3.698452417165" Y="1.491998535844" Z="2.25" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.25" />
                  <Point X="-4.69559308377" Y="1.598940988558" Z="2.25" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.25" />
                  <Point X="-4.834266983479" Y="1.549277389038" Z="2.25" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.25" />
                  <Point X="-4.982776755928" Y="0.970720533935" Z="2.25" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.25" />
                  <Point X="-3.74522522337" Y="0.09426254603" Z="2.25" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.25" />
                  <Point X="-3.573405977029" Y="0.046879449396" Z="2.25" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.25" />
                  <Point X="-3.547756255128" Y="0.026418699103" Z="2.25" />
                  <Point X="-3.539556741714" Y="0" Z="2.25" />
                  <Point X="-3.540608346007" Y="-0.003388251958" Z="2.25" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.25" />
                  <Point X="-3.551962618094" Y="-0.031996533738" Z="2.25" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.25" />
                  <Point X="-4.891662565403" Y="-0.401449510726" Z="2.25" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.25" />
                  <Point X="-5.051498585428" Y="-0.508370739248" Z="2.25" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.25" />
                  <Point X="-4.967229630298" Y="-1.050086884911" Z="2.25" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.25" />
                  <Point X="-3.404189831201" Y="-1.331223089101" Z="2.25" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.25" />
                  <Point X="-3.216148527614" Y="-1.308635074996" Z="2.25" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.25" />
                  <Point X="-3.193552100645" Y="-1.325831282188" Z="2.25" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.25" />
                  <Point X="-4.354838987306" Y="-2.238044054824" Z="2.25" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.25" />
                  <Point X="-4.469532314342" Y="-2.407609243926" Z="2.25" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.25" />
                  <Point X="-4.169897086498" Y="-2.895726504527" Z="2.25" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.25" />
                  <Point X="-2.719409672138" Y="-2.64011329863" Z="2.25" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.25" />
                  <Point X="-2.570867435576" Y="-2.557463033555" Z="2.25" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.25" />
                  <Point X="-3.215303714538" Y="-3.715668710354" Z="2.25" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.25" />
                  <Point X="-3.253382484296" Y="-3.898075873609" Z="2.25" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.25" />
                  <Point X="-2.840532302694" Y="-4.208425750049" Z="2.25" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.25" />
                  <Point X="-2.25178686864" Y="-4.189768592189" Z="2.25" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.25" />
                  <Point X="-2.196898420269" Y="-4.136858575686" Z="2.25" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.25" />
                  <Point X="-1.933064002958" Y="-3.986390975394" Z="2.25" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.25" />
                  <Point X="-1.632151336102" Y="-4.027629447113" Z="2.25" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.25" />
                  <Point X="-1.418525630012" Y="-4.243530324691" Z="2.25" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.25" />
                  <Point X="-1.407617669522" Y="-4.837868045817" Z="2.25" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.25" />
                  <Point X="-1.37948619773" Y="-4.888151544955" Z="2.25" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.25" />
                  <Point X="-1.083330138377" Y="-4.962197009392" Z="2.25" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="-0.462622144358" Y="-3.688713384123" Z="2.25" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="-0.39847532594" Y="-3.491957521785" Z="2.25" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="-0.22455893825" Y="-3.273934175873" Z="2.25" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.25" />
                  <Point X="0.028800141111" Y="-3.213177869415" Z="2.25" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.25" />
                  <Point X="0.271970533492" Y="-3.309688233982" Z="2.25" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.25" />
                  <Point X="0.772132716044" Y="-4.843822645693" Z="2.25" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.25" />
                  <Point X="0.838168144167" Y="-5.010038837474" Z="2.25" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.25" />
                  <Point X="1.018362531082" Y="-4.976540148752" Z="2.25" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.25" />
                  <Point X="0.982320606478" Y="-3.462616284498" Z="2.25" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.25" />
                  <Point X="0.963463020517" Y="-3.244769606698" Z="2.25" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.25" />
                  <Point X="1.031619345882" Y="-3.008314751716" Z="2.25" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.25" />
                  <Point X="1.217639466309" Y="-2.873237280024" Z="2.25" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.25" />
                  <Point X="1.448456883062" Y="-2.869801943144" Z="2.25" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.25" />
                  <Point X="2.545566548883" Y="-4.174850800018" Z="2.25" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.25" />
                  <Point X="2.684238741178" Y="-4.312286120072" Z="2.25" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.25" />
                  <Point X="2.87878498704" Y="-4.184918906084" Z="2.25" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.25" />
                  <Point X="2.359364978702" Y="-2.874940874171" Z="2.25" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.25" />
                  <Point X="2.26680071023" Y="-2.697734926991" Z="2.25" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.25" />
                  <Point X="2.24295085421" Y="-2.485801794938" Z="2.25" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.25" />
                  <Point X="2.347096583435" Y="-2.315950455757" Z="2.25" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.25" />
                  <Point X="2.530771876521" Y="-2.23664729926" Z="2.25" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.25" />
                  <Point X="3.912472722936" Y="-2.958384521573" Z="2.25" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.25" />
                  <Point X="4.084963145513" Y="-3.018311087349" Z="2.25" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.25" />
                  <Point X="4.257851571663" Y="-2.769083743048" Z="2.25" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.25" />
                  <Point X="3.329885661914" Y="-1.719826443428" Z="2.25" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.25" />
                  <Point X="3.181320903751" Y="-1.596826981865" Z="2.25" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.25" />
                  <Point X="3.094050466858" Y="-1.438872304936" Z="2.25" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.25" />
                  <Point X="3.120466418477" Y="-1.272368725657" Z="2.25" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.25" />
                  <Point X="3.238374530194" Y="-1.150898210627" Z="2.25" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.25" />
                  <Point X="4.735620852114" Y="-1.291850407371" Z="2.25" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.25" />
                  <Point X="4.916604357478" Y="-1.27235571892" Z="2.25" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.25" />
                  <Point X="4.997869647486" Y="-0.90176560192" Z="2.25" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.25" />
                  <Point X="3.89573497447" Y="-0.26040877625" Z="2.25" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.25" />
                  <Point X="3.737436792246" Y="-0.214732253899" Z="2.25" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.25" />
                  <Point X="3.649133170242" Y="-0.159298365023" Z="2.25" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.25" />
                  <Point X="3.597833542226" Y="-0.085628501526" Z="2.25" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.25" />
                  <Point X="3.583537908198" Y="0.010982029655" Z="2.25" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.25" />
                  <Point X="3.606246268158" Y="0.104650373552" Z="2.25" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.25" />
                  <Point X="3.665958622106" Y="0.173416590343" Z="2.25" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.25" />
                  <Point X="4.900232842837" Y="0.529563156519" Z="2.25" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.25" />
                  <Point X="5.040523798928" Y="0.617276813127" Z="2.25" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.25" />
                  <Point X="4.970594671" Y="1.039753939291" Z="2.25" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.25" />
                  <Point X="3.624271844719" Y="1.243239946335" Z="2.25" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.25" />
                  <Point X="3.452417853862" Y="1.223438689025" Z="2.25" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.25" />
                  <Point X="3.36104418132" Y="1.238924848015" Z="2.25" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.25" />
                  <Point X="3.293855695102" Y="1.281974333387" Z="2.25" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.25" />
                  <Point X="3.249252508093" Y="1.35645055977" Z="2.25" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.25" />
                  <Point X="3.236038730266" Y="1.4410980069" Z="2.25" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.25" />
                  <Point X="3.261684271265" Y="1.517882548471" Z="2.25" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.25" />
                  <Point X="4.318359423557" Y="2.356212637873" Z="2.25" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.25" />
                  <Point X="4.423539637117" Y="2.494445211766" Z="2.25" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.25" />
                  <Point X="4.211365870517" Y="2.838018476285" Z="2.25" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.25" />
                  <Point X="2.679521921305" Y="2.364942771999" Z="2.25" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.25" />
                  <Point X="2.50075152235" Y="2.264558250185" Z="2.25" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.25" />
                  <Point X="2.421699907824" Y="2.246480773431" Z="2.25" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.25" />
                  <Point X="2.352970282834" Y="2.258783650128" Z="2.25" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.25" />
                  <Point X="2.291974950107" Y="2.304054577548" Z="2.25" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.25" />
                  <Point X="2.252948929327" Y="2.368058536009" Z="2.25" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.25" />
                  <Point X="2.247969692472" Y="2.438718053532" Z="2.25" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.25" />
                  <Point X="3.030682580861" Y="3.83261716065" Z="2.25" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.25" />
                  <Point X="3.08598449439" Y="4.032585860832" Z="2.25" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.25" />
                  <Point X="2.708286009277" Y="4.295372518166" Z="2.25" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.25" />
                  <Point X="2.308959878866" Y="4.522641526216" Z="2.25" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.25" />
                  <Point X="1.895459394655" Y="4.710923812723" Z="2.25" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.25" />
                  <Point X="1.442374959544" Y="4.866242441956" Z="2.25" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.25" />
                  <Point X="0.786475577119" Y="5.014195901209" Z="2.25" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.25" />
                  <Point X="0.021966730601" Y="4.437105432436" Z="2.25" />
                  <Point X="0" Y="4.355124473572" Z="2.25" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>