<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#185" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2548" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004717773438" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.831607849121" Y="20.48614453125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.54236315918" Y="21.532625" />
                  <Point X="0.432699615479" Y="21.69062890625" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.356751739502" Y="21.79098046875" />
                  <Point X="0.33049609375" Y="21.810224609375" />
                  <Point X="0.302494873047" Y="21.82433203125" />
                  <Point X="0.132797286987" Y="21.877" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976488113" Y="21.907234375" />
                  <Point X="-0.008664384842" Y="21.907234375" />
                  <Point X="-0.036823932648" Y="21.90296484375" />
                  <Point X="-0.20652166748" Y="21.850296875" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318184753418" Y="21.81022265625" />
                  <Point X="-0.344439941406" Y="21.790978515625" />
                  <Point X="-0.366323577881" Y="21.7685234375" />
                  <Point X="-0.475986968994" Y="21.610517578125" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538188293457" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.683265441895" Y="20.99381640625" />
                  <Point X="-0.916584899902" Y="20.12305859375" />
                  <Point X="-0.984642272949" Y="20.136267578125" />
                  <Point X="-1.079352539062" Y="20.15465234375" />
                  <Point X="-1.246418212891" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.257049438477" Y="20.6875859375" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.479880737305" Y="21.005810546875" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189697266" Y="21.089705078125" />
                  <Point X="-1.612887573242" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.850387817383" Y="21.122625" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983415527344" Y="21.126291015625" />
                  <Point X="-2.014463745117" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.215441162109" Y="20.98975" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312791259766" Y="20.923173828125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.409365234375" Y="20.803755859375" />
                  <Point X="-2.480147705078" Y="20.711509765625" />
                  <Point X="-2.662906982422" Y="20.824669921875" />
                  <Point X="-2.8017265625" Y="20.910623046875" />
                  <Point X="-3.070104492188" Y="21.117267578125" />
                  <Point X="-3.104721923828" Y="21.143921875" />
                  <Point X="-2.922377441406" Y="21.459751953125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561279297" Y="22.4444296875" />
                  <Point X="-2.428779052734" Y="22.4732578125" />
                  <Point X="-2.446807128906" Y="22.4984140625" />
                  <Point X="-2.464155761719" Y="22.51576171875" />
                  <Point X="-2.489311523438" Y="22.533787109375" />
                  <Point X="-2.518140136719" Y="22.54800390625" />
                  <Point X="-2.547758544922" Y="22.55698828125" />
                  <Point X="-2.578693115234" Y="22.555974609375" />
                  <Point X="-2.610218994141" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.064954345703" Y="22.292982421875" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.973198974609" Y="22.06206640625" />
                  <Point X="-4.082862060547" Y="22.206142578125" />
                  <Point X="-4.27527734375" Y="22.52879296875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.97837109375" Y="22.832056640625" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577148438" Y="23.524404296875" />
                  <Point X="-3.066612548828" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.048696044922" Y="23.600091796875" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.043347900391" Y="23.640341796875" />
                  <Point X="-3.045556640625" Y="23.672013671875" />
                  <Point X="-3.052558105469" Y="23.701759765625" />
                  <Point X="-3.068641601562" Y="23.727744140625" />
                  <Point X="-3.089475585938" Y="23.751701171875" />
                  <Point X="-3.112974853516" Y="23.771234375" />
                  <Point X="-3.130712646484" Y="23.781673828125" />
                  <Point X="-3.139457519531" Y="23.7868203125" />
                  <Point X="-3.168722412109" Y="23.798044921875" />
                  <Point X="-3.200608642578" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.769424560547" Y="23.734853515625" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.7911875" Y="23.83943359375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.884986328125" Y="24.36329296875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.526689941406" Y="24.513298828125" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517485351562" Y="24.78517578125" />
                  <Point X="-3.4877265625" Y="24.800533203125" />
                  <Point X="-3.469151367188" Y="24.81342578125" />
                  <Point X="-3.459986816406" Y="24.81978515625" />
                  <Point X="-3.437527832031" Y="24.84166796875" />
                  <Point X="-3.418280761719" Y="24.867923828125" />
                  <Point X="-3.404168457031" Y="24.8959296875" />
                  <Point X="-3.397972167969" Y="24.91589453125" />
                  <Point X="-3.394917480469" Y="24.925736328125" />
                  <Point X="-3.390648193359" Y="24.953892578125" />
                  <Point X="-3.390647216797" Y="24.983533203125" />
                  <Point X="-3.394916503906" Y="25.011697265625" />
                  <Point X="-3.401112792969" Y="25.031662109375" />
                  <Point X="-3.404167480469" Y="25.041505859375" />
                  <Point X="-3.418272949219" Y="25.069501953125" />
                  <Point X="-3.437515136719" Y="25.0957578125" />
                  <Point X="-3.4599765625" Y="25.1176484375" />
                  <Point X="-3.478565185547" Y="25.130548828125" />
                  <Point X="-3.486999023438" Y="25.135771484375" />
                  <Point X="-3.511733886719" Y="25.14935546875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-4.022828613281" Y="25.289130859375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.85212890625" Y="25.7901796875" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.722010253906" Y="26.355146484375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.478056640625" Y="26.393580078125" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744986083984" Y="26.299341796875" />
                  <Point X="-3.723421875" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.661995605469" Y="26.318234375" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.622772216797" Y="26.33296484375" />
                  <Point X="-3.604029541016" Y="26.343787109375" />
                  <Point X="-3.587350341797" Y="26.356017578125" />
                  <Point X="-3.573713378906" Y="26.371568359375" />
                  <Point X="-3.561300048828" Y="26.389296875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.534842773438" Y="26.44728515625" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.5158046875" Y="26.528751953125" />
                  <Point X="-3.518951904297" Y="26.5491953125" />
                  <Point X="-3.524554443359" Y="26.5701015625" />
                  <Point X="-3.532050292969" Y="26.589376953125" />
                  <Point X="-3.551969726562" Y="26.627642578125" />
                  <Point X="-3.561790039062" Y="26.646505859375" />
                  <Point X="-3.57328125" Y="26.663705078125" />
                  <Point X="-3.587193115234" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-3.883174072266" Y="26.91023828125" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.1885546875" Y="27.54965625" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.8096953125" Y="28.082580078125" />
                  <Point X="-3.750504150391" Y="28.158662109375" />
                  <Point X="-3.644187255859" Y="28.097279296875" />
                  <Point X="-3.206657226562" Y="27.844671875" />
                  <Point X="-3.187727539062" Y="27.836341796875" />
                  <Point X="-3.167083984375" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.089493896484" Y="27.82078125" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040558837891" Y="27.81876171875" />
                  <Point X="-3.019101074219" Y="27.821587890625" />
                  <Point X="-2.999011474609" Y="27.826505859375" />
                  <Point X="-2.980462158203" Y="27.835654296875" />
                  <Point X="-2.962208740234" Y="27.847283203125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.905405761719" Y="27.900900390625" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848448974609" Y="28.093419921875" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-2.994331298828" Y="28.397236328125" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-2.887679931641" Y="28.95126953125" />
                  <Point X="-2.70062109375" Y="29.094685546875" />
                  <Point X="-2.273089111328" Y="29.332212890625" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.043195678711" Y="29.2297421875" />
                  <Point X="-2.028890869141" Y="29.214798828125" />
                  <Point X="-2.012310791016" Y="29.20088671875" />
                  <Point X="-1.995114868164" Y="29.189396484375" />
                  <Point X="-1.93133996582" Y="29.1561953125" />
                  <Point X="-1.899898803711" Y="29.139828125" />
                  <Point X="-1.880625244141" Y="29.13233203125" />
                  <Point X="-1.859718994141" Y="29.126728515625" />
                  <Point X="-1.839268798828" Y="29.123580078125" />
                  <Point X="-1.818621826172" Y="29.12493359375" />
                  <Point X="-1.797306884766" Y="29.128693359375" />
                  <Point X="-1.77745300293" Y="29.134482421875" />
                  <Point X="-1.711027099609" Y="29.161998046875" />
                  <Point X="-1.678279052734" Y="29.1755625" />
                  <Point X="-1.660147216797" Y="29.185509765625" />
                  <Point X="-1.642417236328" Y="29.197923828125" />
                  <Point X="-1.626864746094" Y="29.2115625" />
                  <Point X="-1.614633056641" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.573859863281" Y="29.3344921875" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.571888793945" Y="29.538369140625" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-1.191788818359" Y="29.741916015625" />
                  <Point X="-0.949623474121" Y="29.80980859375" />
                  <Point X="-0.431335693359" Y="29.87046875" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.26333682251" Y="29.7693671875" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113983154" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907154" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425941467" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.210026275635" Y="29.5244609375" />
                  <Point X="0.307419616699" Y="29.887939453125" />
                  <Point X="0.632598205566" Y="29.8538828125" />
                  <Point X="0.844031311035" Y="29.831740234375" />
                  <Point X="1.272848022461" Y="29.7282109375" />
                  <Point X="1.481038818359" Y="29.6779453125" />
                  <Point X="1.759650268555" Y="29.576892578125" />
                  <Point X="1.894647827148" Y="29.52792578125" />
                  <Point X="2.164535400391" Y="29.401708984375" />
                  <Point X="2.294557617188" Y="29.34090234375" />
                  <Point X="2.555325439453" Y="29.188978515625" />
                  <Point X="2.6809765625" Y="29.115775390625" />
                  <Point X="2.926878173828" Y="28.94090234375" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.724691894531" Y="28.55068359375" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.118696533203" Y="27.462279296875" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.113335205078" Y="27.334451171875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121443115234" Y="27.2896015625" />
                  <Point X="2.129709716797" Y="27.267513671875" />
                  <Point X="2.140071044922" Y="27.24747265625" />
                  <Point X="2.16884375" Y="27.205068359375" />
                  <Point X="2.183028808594" Y="27.1841640625" />
                  <Point X="2.194465332031" Y="27.170328125" />
                  <Point X="2.221597167969" Y="27.14559375" />
                  <Point X="2.264000732422" Y="27.1168203125" />
                  <Point X="2.284905761719" Y="27.102634765625" />
                  <Point X="2.304943359375" Y="27.092275390625" />
                  <Point X="2.327029785156" Y="27.0840078125" />
                  <Point X="2.348964599609" Y="27.078662109375" />
                  <Point X="2.39546484375" Y="27.073056640625" />
                  <Point X="2.418389404297" Y="27.070291015625" />
                  <Point X="2.436465087891" Y="27.06984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.526983154297" Y="27.08855078125" />
                  <Point X="2.553494384766" Y="27.095640625" />
                  <Point X="2.5652890625" Y="27.099640625" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="3.081331542969" Y="27.39466015625" />
                  <Point X="3.967326904297" Y="27.906189453125" />
                  <Point X="4.04873828125" Y="27.793046875" />
                  <Point X="4.123274414062" Y="27.689458984375" />
                  <Point X="4.260357910156" Y="27.46292578125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.990604248047" Y="27.251482421875" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221420654297" Y="26.660236328125" />
                  <Point X="3.203974121094" Y="26.641626953125" />
                  <Point X="3.165271972656" Y="26.59113671875" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.136607666016" Y="26.5509140625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.107213378906" Y="26.46553515625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.109573974609" Y="26.314412109375" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680175781" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.23573828125" />
                  <Point X="3.168471435547" Y="26.1868125" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198895996094" Y="26.145447265625" />
                  <Point X="3.216138671875" Y="26.129359375" />
                  <Point X="3.234346191406" Y="26.11603515625" />
                  <Point X="3.280991699219" Y="26.08977734375" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320520507812" Y="26.069501953125" />
                  <Point X="3.356120117188" Y="26.0594375" />
                  <Point X="3.419187988281" Y="26.051103515625" />
                  <Point X="3.450280761719" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="3.956328613281" Y="26.10861328125" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.813924316406" Y="26.064302734375" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.889135742188" Y="25.65534375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.587350585938" Y="25.562912109375" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704786865234" Y="25.325583984375" />
                  <Point X="3.681547607422" Y="25.315068359375" />
                  <Point X="3.619585205078" Y="25.27925390625" />
                  <Point X="3.589037597656" Y="25.26159765625" />
                  <Point X="3.574311279297" Y="25.251095703125" />
                  <Point X="3.547530273438" Y="25.225576171875" />
                  <Point X="3.510352783203" Y="25.178203125" />
                  <Point X="3.492024414062" Y="25.15484765625" />
                  <Point X="3.480300537109" Y="25.13556640625" />
                  <Point X="3.470527099609" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.451288085938" Y="25.02789453125" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.457571289062" Y="24.876736328125" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.47052734375" Y="24.8233359375" />
                  <Point X="3.48030078125" Y="24.801873046875" />
                  <Point X="3.492024414062" Y="24.782591796875" />
                  <Point X="3.529201660156" Y="24.73521875" />
                  <Point X="3.547530273438" Y="24.71186328125" />
                  <Point X="3.559997558594" Y="24.698765625" />
                  <Point X="3.589035400391" Y="24.67584375" />
                  <Point X="3.650997802734" Y="24.640029296875" />
                  <Point X="3.681545410156" Y="24.62237109375" />
                  <Point X="3.692698486328" Y="24.616865234375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.145872070313" Y="24.4928203125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.872896484375" Y="24.169826171875" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.434230957031" Y="23.863609375" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374659179688" Y="23.994490234375" />
                  <Point X="3.253049072266" Y="23.96805859375" />
                  <Point X="3.193094970703" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112397460938" Y="23.9060390625" />
                  <Point X="3.038891845703" Y="23.817634765625" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.959222412109" Y="23.580146484375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.95634765625" Y="23.49243359375" />
                  <Point X="2.964079589844" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.043751464844" Y="23.3273203125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930419922" Y="23.262763671875" />
                  <Point X="3.110628173828" Y="23.23908984375" />
                  <Point X="3.509013671875" Y="22.933396484375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.175194824219" Y="22.331744140625" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.028981445312" Y="22.1140546875" />
                  <Point X="3.700361083984" Y="22.303783203125" />
                  <Point X="2.800954345703" Y="22.823056640625" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.609488769531" Y="22.8663125" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611572266" Y="22.874646484375" />
                  <Point X="2.444833740234" Y="22.864822265625" />
                  <Point X="2.324593994141" Y="22.801541015625" />
                  <Point X="2.265315673828" Y="22.77034375" />
                  <Point X="2.242386474609" Y="22.753451171875" />
                  <Point X="2.221426513672" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.141250732422" Y="22.5893203125" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229492188" Y="22.500267578125" />
                  <Point X="2.095271484375" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.121814208984" Y="22.292005859375" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.407821777344" Y="21.73051171875" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.841638916016" Y="20.9310625" />
                  <Point X="2.781861328125" Y="20.88836328125" />
                  <Point X="2.701764160156" Y="20.83651953125" />
                  <Point X="2.444876708984" Y="21.17130078125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721923706055" Y="22.099443359375" />
                  <Point X="1.579175415039" Y="22.191216796875" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986450195" Y="22.24883203125" />
                  <Point X="1.448366088867" Y="22.2565625" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.260981079102" Y="22.244515625" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156368164062" Y="22.230603515625" />
                  <Point X="1.128982543945" Y="22.21926171875" />
                  <Point X="1.104595214844" Y="22.2045390625" />
                  <Point X="0.984043701172" Y="22.104302734375" />
                  <Point X="0.92461151123" Y="22.05488671875" />
                  <Point X="0.904139709473" Y="22.031134765625" />
                  <Point X="0.887247558594" Y="22.003306640625" />
                  <Point X="0.875624511719" Y="21.974189453125" />
                  <Point X="0.83958001709" Y="21.808357421875" />
                  <Point X="0.821810241699" Y="21.726603515625" />
                  <Point X="0.81972479248" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.892291381836" Y="21.125814453125" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="0.975713623047" Y="20.129923828125" />
                  <Point X="0.929315429688" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058441894531" Y="20.2473671875" />
                  <Point X="-1.141246459961" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.16387487793" Y="20.706119140625" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575561523" Y="20.905140625" />
                  <Point X="-1.250210327148" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94021875" />
                  <Point X="-1.417242919922" Y="21.077234375" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506736572266" Y="21.15403125" />
                  <Point X="-1.533020751953" Y="21.170376953125" />
                  <Point X="-1.5468359375" Y="21.177474609375" />
                  <Point X="-1.576533813477" Y="21.189775390625" />
                  <Point X="-1.59131640625" Y="21.1945234375" />
                  <Point X="-1.621456542969" Y="21.20155078125" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.844174194336" Y="21.217421875" />
                  <Point X="-1.946403198242" Y="21.22412109375" />
                  <Point X="-1.961927856445" Y="21.2238671875" />
                  <Point X="-1.99272644043" Y="21.220833984375" />
                  <Point X="-2.008000610352" Y="21.2180546875" />
                  <Point X="-2.039048828125" Y="21.209736328125" />
                  <Point X="-2.053668212891" Y="21.204505859375" />
                  <Point X="-2.081862060547" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.268219970703" Y="21.068740234375" />
                  <Point X="-2.353402832031" Y="21.011822265625" />
                  <Point X="-2.359687744141" Y="21.007240234375" />
                  <Point X="-2.380450683594" Y="20.989861328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.484733886719" Y="20.861587890625" />
                  <Point X="-2.503200927734" Y="20.837521484375" />
                  <Point X="-2.612895751953" Y="20.90544140625" />
                  <Point X="-2.747613037109" Y="20.98885546875" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.840104980469" Y="21.412251953125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314666992188" Y="22.442388671875" />
                  <Point X="-2.323652587891" Y="22.4720078125" />
                  <Point X="-2.329359863281" Y="22.486451171875" />
                  <Point X="-2.343577636719" Y="22.515279296875" />
                  <Point X="-2.351560546875" Y="22.528595703125" />
                  <Point X="-2.369588623047" Y="22.553751953125" />
                  <Point X="-2.379633789062" Y="22.565591796875" />
                  <Point X="-2.396982421875" Y="22.582939453125" />
                  <Point X="-2.408822265625" Y="22.592984375" />
                  <Point X="-2.433978027344" Y="22.611009765625" />
                  <Point X="-2.447293945312" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563964844" Y="22.6389140625" />
                  <Point X="-2.520182373047" Y="22.6478984375" />
                  <Point X="-2.550869873047" Y="22.6519375" />
                  <Point X="-2.581804443359" Y="22.650923828125" />
                  <Point X="-2.597228515625" Y="22.6491484375" />
                  <Point X="-2.628754394531" Y="22.642876953125" />
                  <Point X="-2.643685058594" Y="22.63861328125" />
                  <Point X="-2.672649658203" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.112454345703" Y="22.37525390625" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.897605712891" Y="22.11960546875" />
                  <Point X="-4.004021484375" Y="22.259416015625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.920538818359" Y="22.7566875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036482421875" Y="23.4366875" />
                  <Point X="-3.015104980469" Y="23.459607421875" />
                  <Point X="-3.005367431641" Y="23.471955078125" />
                  <Point X="-2.987402832031" Y="23.4990859375" />
                  <Point X="-2.979835693359" Y="23.512873046875" />
                  <Point X="-2.967079589844" Y="23.54150390625" />
                  <Point X="-2.961890625" Y="23.55634765625" />
                  <Point X="-2.956730224609" Y="23.5762734375" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748779297" Y="23.631625" />
                  <Point X="-2.948578125" Y="23.646951171875" />
                  <Point X="-2.950786865234" Y="23.678623046875" />
                  <Point X="-2.953083740234" Y="23.693779296875" />
                  <Point X="-2.960085205078" Y="23.723525390625" />
                  <Point X="-2.971780029297" Y="23.751759765625" />
                  <Point X="-2.987863525391" Y="23.777744140625" />
                  <Point X="-2.996956787109" Y="23.790083984375" />
                  <Point X="-3.017790771484" Y="23.814041015625" />
                  <Point X="-3.028749023438" Y="23.8247578125" />
                  <Point X="-3.052248291016" Y="23.844291015625" />
                  <Point X="-3.064789306641" Y="23.853107421875" />
                  <Point X="-3.082527099609" Y="23.863546875" />
                  <Point X="-3.105436767578" Y="23.87551953125" />
                  <Point X="-3.134701660156" Y="23.886744140625" />
                  <Point X="-3.149801757812" Y="23.891142578125" />
                  <Point X="-3.181687988281" Y="23.897623046875" />
                  <Point X="-3.197304931641" Y="23.89946875" />
                  <Point X="-3.228625488281" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.781824462891" Y="23.829041015625" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.699142578125" Y="23.8629453125" />
                  <Point X="-4.740762207031" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.5021015625" Y="24.42153515625" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.500464355469" Y="24.6902890625" />
                  <Point X="-3.473918701172" Y="24.70075390625" />
                  <Point X="-3.444159912109" Y="24.716111328125" />
                  <Point X="-3.433558349609" Y="24.722490234375" />
                  <Point X="-3.414983154297" Y="24.7353828125" />
                  <Point X="-3.393690185547" Y="24.7517421875" />
                  <Point X="-3.371231201172" Y="24.773625" />
                  <Point X="-3.360909423828" Y="24.785501953125" />
                  <Point X="-3.341662353516" Y="24.8117578125" />
                  <Point X="-3.333443115234" Y="24.825173828125" />
                  <Point X="-3.319330810547" Y="24.8531796875" />
                  <Point X="-3.313437744141" Y="24.86776953125" />
                  <Point X="-3.307241455078" Y="24.887734375" />
                  <Point X="-3.300990966797" Y="24.911494140625" />
                  <Point X="-3.296721679688" Y="24.939650390625" />
                  <Point X="-3.295648193359" Y="24.953888671875" />
                  <Point X="-3.295647216797" Y="24.983529296875" />
                  <Point X="-3.296720214844" Y="24.997771484375" />
                  <Point X="-3.300989501953" Y="25.025935546875" />
                  <Point X="-3.304185791016" Y="25.039857421875" />
                  <Point X="-3.310382080078" Y="25.059822265625" />
                  <Point X="-3.319327392578" Y="25.084251953125" />
                  <Point X="-3.333432861328" Y="25.112248046875" />
                  <Point X="-3.341647705078" Y="25.125658203125" />
                  <Point X="-3.360889892578" Y="25.1519140625" />
                  <Point X="-3.371209960938" Y="25.163791015625" />
                  <Point X="-3.393671386719" Y="25.185681640625" />
                  <Point X="-3.405812744141" Y="25.1956953125" />
                  <Point X="-3.424401367188" Y="25.208595703125" />
                  <Point X="-3.441269042969" Y="25.219041015625" />
                  <Point X="-3.46600390625" Y="25.232625" />
                  <Point X="-3.476318115234" Y="25.2375078125" />
                  <Point X="-3.497460449219" Y="25.246001953125" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.998240966797" Y="25.38089453125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.75815234375" Y="25.7762734375" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.49045703125" Y="26.299392578125" />
                  <Point X="-3.778066650391" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747058105469" Y="26.204365234375" />
                  <Point X="-3.736705810547" Y="26.204703125" />
                  <Point X="-3.715141601563" Y="26.20658984375" />
                  <Point X="-3.704886230469" Y="26.2080546875" />
                  <Point X="-3.684602539062" Y="26.21208984375" />
                  <Point X="-3.67457421875" Y="26.21466015625" />
                  <Point X="-3.633431640625" Y="26.227630859375" />
                  <Point X="-3.613148193359" Y="26.23402734375" />
                  <Point X="-3.603450439453" Y="26.23767578125" />
                  <Point X="-3.584510498047" Y="26.246009765625" />
                  <Point X="-3.575268310547" Y="26.2506953125" />
                  <Point X="-3.556525634766" Y="26.261517578125" />
                  <Point X="-3.547852783203" Y="26.26717578125" />
                  <Point X="-3.531173583984" Y="26.27940625" />
                  <Point X="-3.515924072266" Y="26.293380859375" />
                  <Point X="-3.502287109375" Y="26.308931640625" />
                  <Point X="-3.495893310547" Y="26.317080078125" />
                  <Point X="-3.483479980469" Y="26.33480859375" />
                  <Point X="-3.478011962891" Y="26.3436015625" />
                  <Point X="-3.468063476562" Y="26.361734375" />
                  <Point X="-3.463583007813" Y="26.37107421875" />
                  <Point X="-3.44707421875" Y="26.4109296875" />
                  <Point X="-3.438935546875" Y="26.430578125" />
                  <Point X="-3.435499267578" Y="26.440349609375" />
                  <Point X="-3.4297109375" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522541015625" />
                  <Point X="-3.421910888672" Y="26.54320703125" />
                  <Point X="-3.425058105469" Y="26.563650390625" />
                  <Point X="-3.427189697266" Y="26.57378515625" />
                  <Point X="-3.432792236328" Y="26.59469140625" />
                  <Point X="-3.436013671875" Y="26.604533203125" />
                  <Point X="-3.443509521484" Y="26.62380859375" />
                  <Point X="-3.447783935547" Y="26.6332421875" />
                  <Point X="-3.467703369141" Y="26.6715078125" />
                  <Point X="-3.477523681641" Y="26.69037109375" />
                  <Point X="-3.482798339844" Y="26.69928125" />
                  <Point X="-3.494289550781" Y="26.71648046875" />
                  <Point X="-3.500506103516" Y="26.72476953125" />
                  <Point X="-3.51441796875" Y="26.741349609375" />
                  <Point X="-3.521499267578" Y="26.74891015625" />
                  <Point X="-3.536442138672" Y="26.76321484375" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-3.825341796875" Y="26.985607421875" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.106508300781" Y="27.501767578125" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.734714599609" Y="28.02424609375" />
                  <Point X="-3.726337890625" Y="28.035013671875" />
                  <Point X="-3.691687744141" Y="28.0150078125" />
                  <Point X="-3.254157714844" Y="27.762400390625" />
                  <Point X="-3.244921386719" Y="27.75771875" />
                  <Point X="-3.225991699219" Y="27.749388671875" />
                  <Point X="-3.216298095703" Y="27.745740234375" />
                  <Point X="-3.195654541016" Y="27.73923046875" />
                  <Point X="-3.185623046875" Y="27.736658203125" />
                  <Point X="-3.165333251953" Y="27.73262109375" />
                  <Point X="-3.155074951172" Y="27.73115625" />
                  <Point X="-3.097774658203" Y="27.726142578125" />
                  <Point X="-3.069525634766" Y="27.723671875" />
                  <Point X="-3.059173339844" Y="27.723333984375" />
                  <Point X="-3.038487304688" Y="27.72378515625" />
                  <Point X="-3.028153564453" Y="27.72457421875" />
                  <Point X="-3.006695800781" Y="27.727400390625" />
                  <Point X="-2.996511962891" Y="27.7293125" />
                  <Point X="-2.976422363281" Y="27.73423046875" />
                  <Point X="-2.956990722656" Y="27.7413046875" />
                  <Point X="-2.93844140625" Y="27.750453125" />
                  <Point X="-2.92941796875" Y="27.755533203125" />
                  <Point X="-2.911164550781" Y="27.767162109375" />
                  <Point X="-2.902749023438" Y="27.77319140625" />
                  <Point X="-2.886618164062" Y="27.78613671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.838230712891" Y="27.833724609375" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.753810546875" Y="28.10169921875" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-2.912058837891" Y="28.444736328125" />
                  <Point X="-3.059386474609" Y="28.699916015625" />
                  <Point X="-2.829877929688" Y="28.875876953125" />
                  <Point X="-2.648369384766" Y="29.015037109375" />
                  <Point X="-2.226951660156" Y="29.24916796875" />
                  <Point X="-2.1925234375" Y="29.268296875" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111821044922" Y="29.164048828125" />
                  <Point X="-2.097516357422" Y="29.14910546875" />
                  <Point X="-2.089955078125" Y="29.1420234375" />
                  <Point X="-2.073375" Y="29.128111328125" />
                  <Point X="-2.065090820313" Y="29.1218984375" />
                  <Point X="-2.047894897461" Y="29.110408203125" />
                  <Point X="-2.038983154297" Y="29.105130859375" />
                  <Point X="-1.975208251953" Y="29.0719296875" />
                  <Point X="-1.943767089844" Y="29.0555625" />
                  <Point X="-1.934334472656" Y="29.0512890625" />
                  <Point X="-1.915060913086" Y="29.04379296875" />
                  <Point X="-1.905219970703" Y="29.0405703125" />
                  <Point X="-1.884313720703" Y="29.034966796875" />
                  <Point X="-1.874174560547" Y="29.032833984375" />
                  <Point X="-1.853724365234" Y="29.029685546875" />
                  <Point X="-1.83305456543" Y="29.028783203125" />
                  <Point X="-1.812407470703" Y="29.03013671875" />
                  <Point X="-1.802119384766" Y="29.031376953125" />
                  <Point X="-1.780804443359" Y="29.03513671875" />
                  <Point X="-1.770713867188" Y="29.0374921875" />
                  <Point X="-1.750859985352" Y="29.04328125" />
                  <Point X="-1.741096801758" Y="29.04671484375" />
                  <Point X="-1.674670898438" Y="29.07423046875" />
                  <Point X="-1.641922729492" Y="29.087794921875" />
                  <Point X="-1.63258581543" Y="29.0922734375" />
                  <Point X="-1.614453979492" Y="29.102220703125" />
                  <Point X="-1.605659301758" Y="29.107689453125" />
                  <Point X="-1.587929077148" Y="29.120103515625" />
                  <Point X="-1.579780639648" Y="29.126498046875" />
                  <Point X="-1.564228027344" Y="29.14013671875" />
                  <Point X="-1.550252929688" Y="29.15538671875" />
                  <Point X="-1.538021240234" Y="29.172068359375" />
                  <Point X="-1.532361083984" Y="29.180744140625" />
                  <Point X="-1.52153894043" Y="29.19948828125" />
                  <Point X="-1.516855834961" Y="29.208728515625" />
                  <Point X="-1.508525146484" Y="29.227662109375" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.483256835938" Y="29.30592578125" />
                  <Point X="-1.472597900391" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349765625" />
                  <Point X="-1.465990966797" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.477701416016" Y="29.55076953125" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.166142822266" Y="29.650443359375" />
                  <Point X="-0.931164611816" Y="29.7163203125" />
                  <Point X="-0.420292327881" Y="29.77611328125" />
                  <Point X="-0.365222686768" Y="29.78255859375" />
                  <Point X="-0.35509979248" Y="29.744779296875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451339722" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.301789215088" Y="29.499873046875" />
                  <Point X="0.378190704346" Y="29.7850078125" />
                  <Point X="0.622702758789" Y="29.759400390625" />
                  <Point X="0.827853088379" Y="29.737916015625" />
                  <Point X="1.250552734375" Y="29.63586328125" />
                  <Point X="1.453623168945" Y="29.586833984375" />
                  <Point X="1.727258300781" Y="29.4875859375" />
                  <Point X="1.858249389648" Y="29.440072265625" />
                  <Point X="2.124290771484" Y="29.315654296875" />
                  <Point X="2.250429931641" Y="29.2566640625" />
                  <Point X="2.507502441406" Y="29.106892578125" />
                  <Point X="2.629432373047" Y="29.035857421875" />
                  <Point X="2.817778564453" Y="28.9019140625" />
                  <Point X="2.642419433594" Y="28.59818359375" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.026921264648" Y="27.4868203125" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.019018432617" Y="27.323078125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.023801025391" Y="27.289033203125" />
                  <Point X="2.029144775391" Y="27.267107421875" />
                  <Point X="2.032470214844" Y="27.256302734375" />
                  <Point X="2.040736694336" Y="27.23421484375" />
                  <Point X="2.045320922852" Y="27.223884765625" />
                  <Point X="2.055682373047" Y="27.20384375" />
                  <Point X="2.061459472656" Y="27.1941328125" />
                  <Point X="2.090232177734" Y="27.151728515625" />
                  <Point X="2.104417236328" Y="27.13082421875" />
                  <Point X="2.109805175781" Y="27.123638671875" />
                  <Point X="2.130463623047" Y="27.100123046875" />
                  <Point X="2.157595458984" Y="27.075388671875" />
                  <Point X="2.168255126953" Y="27.066982421875" />
                  <Point X="2.210658691406" Y="27.038208984375" />
                  <Point X="2.231563720703" Y="27.0240234375" />
                  <Point X="2.241276855469" Y="27.01824609375" />
                  <Point X="2.261314453125" Y="27.00788671875" />
                  <Point X="2.271638916016" Y="27.0033046875" />
                  <Point X="2.293725341797" Y="26.995037109375" />
                  <Point X="2.304535888672" Y="26.991708984375" />
                  <Point X="2.326470703125" Y="26.98636328125" />
                  <Point X="2.337594970703" Y="26.984345703125" />
                  <Point X="2.384095214844" Y="26.978740234375" />
                  <Point X="2.407019775391" Y="26.975974609375" />
                  <Point X="2.416039550781" Y="26.9753203125" />
                  <Point X="2.447573730469" Y="26.97549609375" />
                  <Point X="2.484316650391" Y="26.979822265625" />
                  <Point X="2.497750976562" Y="26.98239453125" />
                  <Point X="2.551526123047" Y="26.996775390625" />
                  <Point X="2.578037353516" Y="27.003865234375" />
                  <Point X="2.584005371094" Y="27.005673828125" />
                  <Point X="2.604409423828" Y="27.013068359375" />
                  <Point X="2.627654052734" Y="27.023572265625" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="3.128831298828" Y="27.31238671875" />
                  <Point X="3.940405761719" Y="27.78094921875" />
                  <Point X="3.971625976562" Y="27.737560546875" />
                  <Point X="4.043956542969" Y="27.637037109375" />
                  <Point X="4.136885253906" Y="27.48347265625" />
                  <Point X="3.932772216797" Y="27.3268515625" />
                  <Point X="3.172951904297" Y="26.7438203125" />
                  <Point X="3.168131103516" Y="26.73986328125" />
                  <Point X="3.152114990234" Y="26.7252109375" />
                  <Point X="3.134668457031" Y="26.7066015625" />
                  <Point X="3.128576416016" Y="26.699421875" />
                  <Point X="3.089874267578" Y="26.648931640625" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065635986328" Y="26.6166015625" />
                  <Point X="3.049741455078" Y="26.589375" />
                  <Point X="3.034763671875" Y="26.555546875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.015723632812" Y="26.49112109375" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.016533935547" Y="26.29521484375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024597900391" Y="26.258234375" />
                  <Point X="3.034683837891" Y="26.228607421875" />
                  <Point X="3.050286621094" Y="26.195369140625" />
                  <Point X="3.056918701172" Y="26.1835234375" />
                  <Point X="3.089107177734" Y="26.13459765625" />
                  <Point X="3.104976318359" Y="26.110478515625" />
                  <Point X="3.111741210938" Y="26.101419921875" />
                  <Point X="3.126296630859" Y="26.084173828125" />
                  <Point X="3.134087158203" Y="26.075986328125" />
                  <Point X="3.151329833984" Y="26.0598984375" />
                  <Point X="3.160035644531" Y="26.0526953125" />
                  <Point X="3.178243164062" Y="26.03937109375" />
                  <Point X="3.187744873047" Y="26.03325" />
                  <Point X="3.234390380859" Y="26.0069921875" />
                  <Point X="3.257386962891" Y="25.994046875" />
                  <Point X="3.265482177734" Y="25.989986328125" />
                  <Point X="3.29467578125" Y="25.9780859375" />
                  <Point X="3.330275390625" Y="25.968021484375" />
                  <Point X="3.343674804688" Y="25.965255859375" />
                  <Point X="3.406742675781" Y="25.956921875" />
                  <Point X="3.437835449219" Y="25.9528125" />
                  <Point X="3.444033203125" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="3.968728515625" Y="26.01442578125" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.721620117188" Y="26.04183203125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.562762207031" Y="25.65467578125" />
                  <Point X="3.691991210938" Y="25.421353515625" />
                  <Point X="3.686023925781" Y="25.419541015625" />
                  <Point X="3.665622802734" Y="25.412134765625" />
                  <Point X="3.642383544922" Y="25.401619140625" />
                  <Point X="3.634007324219" Y="25.397318359375" />
                  <Point X="3.572044921875" Y="25.36150390625" />
                  <Point X="3.541497314453" Y="25.34384765625" />
                  <Point X="3.533878662109" Y="25.338943359375" />
                  <Point X="3.508775390625" Y="25.31987109375" />
                  <Point X="3.481994384766" Y="25.2943515625" />
                  <Point X="3.472796142578" Y="25.2842265625" />
                  <Point X="3.435618652344" Y="25.236853515625" />
                  <Point X="3.417290283203" Y="25.213498046875" />
                  <Point X="3.410852050781" Y="25.204205078125" />
                  <Point X="3.399128173828" Y="25.184923828125" />
                  <Point X="3.393842529297" Y="25.174935546875" />
                  <Point X="3.384069091797" Y="25.15347265625" />
                  <Point X="3.380005859375" Y="25.1429296875" />
                  <Point X="3.373159423828" Y="25.1214296875" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.357983642578" Y="25.045763671875" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.00496484375" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280029297" Y="24.937056640625" />
                  <Point X="3.351874267578" Y="24.923576171875" />
                  <Point X="3.364266845703" Y="24.8588671875" />
                  <Point X="3.370376464844" Y="24.826966796875" />
                  <Point X="3.373159667969" Y="24.816009765625" />
                  <Point X="3.380006103516" Y="24.794509765625" />
                  <Point X="3.384069335938" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.76250390625" />
                  <Point X="3.399127929688" Y="24.752517578125" />
                  <Point X="3.4108515625" Y="24.733236328125" />
                  <Point X="3.417290039063" Y="24.72394140625" />
                  <Point X="3.454467285156" Y="24.676568359375" />
                  <Point X="3.472795898438" Y="24.653212890625" />
                  <Point X="3.478719726563" Y="24.646365234375" />
                  <Point X="3.501135742188" Y="24.62419921875" />
                  <Point X="3.530173583984" Y="24.60127734375" />
                  <Point X="3.541495117188" Y="24.59359375" />
                  <Point X="3.603457519531" Y="24.557779296875" />
                  <Point X="3.634005126953" Y="24.54012109375" />
                  <Point X="3.639492431641" Y="24.537185546875" />
                  <Point X="3.659139648438" Y="24.527990234375" />
                  <Point X="3.683021240234" Y="24.51897265625" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.121284667969" Y="24.401056640625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.778958007812" Y="24.18398828125" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.920779296875" />
                  <Point X="4.446630859375" Y="23.957796875" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354482177734" Y="24.087322265625" />
                  <Point X="3.232872070312" Y="24.060890625" />
                  <Point X="3.17291796875" Y="24.047859375" />
                  <Point X="3.157874267578" Y="24.0432578125" />
                  <Point X="3.128752929688" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123046875" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.039349609375" Y="23.966775390625" />
                  <Point X="2.965843994141" Y="23.87837109375" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921327148438" Y="23.82315234375" />
                  <Point X="2.906606689453" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.864622070312" Y="23.5888515625" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607910156" Y="23.485408203125" />
                  <Point X="2.86406640625" Y="23.469869140625" />
                  <Point X="2.871798339844" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423330078125" />
                  <Point X="2.889157958984" Y="23.39451953125" />
                  <Point X="2.896540527344" Y="23.380626953125" />
                  <Point X="2.963841308594" Y="23.2759453125" />
                  <Point X="2.997020751953" Y="23.224337890625" />
                  <Point X="3.001739990234" Y="23.217650390625" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043487060547" Y="23.171880859375" />
                  <Point X="3.052795654297" Y="23.163720703125" />
                  <Point X="3.451181152344" Y="22.85802734375" />
                  <Point X="4.087170166016" Y="22.370015625" />
                  <Point X="4.045500976562" Y="22.302587890625" />
                  <Point X="4.001274658203" Y="22.239748046875" />
                  <Point X="3.747861083984" Y="22.386056640625" />
                  <Point X="2.848454345703" Y="22.905330078125" />
                  <Point X="2.841182617188" Y="22.909119140625" />
                  <Point X="2.815026123047" Y="22.920484375" />
                  <Point X="2.78312109375" Y="22.930671875" />
                  <Point X="2.771107910156" Y="22.933662109375" />
                  <Point X="2.626372314453" Y="22.95980078125" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317382813" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444847412109" Y="22.96486328125" />
                  <Point X="2.415069580078" Y="22.9550390625" />
                  <Point X="2.400589355469" Y="22.948890625" />
                  <Point X="2.280349609375" Y="22.885609375" />
                  <Point X="2.221071289062" Y="22.854412109375" />
                  <Point X="2.208967529297" Y="22.846828125" />
                  <Point X="2.186038330078" Y="22.829935546875" />
                  <Point X="2.175212890625" Y="22.820626953125" />
                  <Point X="2.154252929688" Y="22.79966796875" />
                  <Point X="2.144942382812" Y="22.78883984375" />
                  <Point X="2.128047851562" Y="22.765908203125" />
                  <Point X="2.120463867188" Y="22.7538046875" />
                  <Point X="2.057182617187" Y="22.633564453125" />
                  <Point X="2.025984863281" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.559806640625" />
                  <Point X="2.010012573242" Y="22.53003125" />
                  <Point X="2.006337890625" Y="22.514736328125" />
                  <Point X="2.001379760742" Y="22.4825625" />
                  <Point X="2.000279418945" Y="22.46687109375" />
                  <Point X="2.000683227539" Y="22.43551953125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.028326538086" Y="22.275123046875" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.325549316406" Y="21.68301171875" />
                  <Point X="2.735893798828" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.963916015625" />
                  <Point X="2.520245117188" Y="21.2291328125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657714844" Y="22.12984765625" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251831055" Y="22.17199609375" />
                  <Point X="1.773298339844" Y="22.179353515625" />
                  <Point X="1.630550048828" Y="22.271126953125" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546279663086" Y="22.323755859375" />
                  <Point X="1.517465942383" Y="22.336126953125" />
                  <Point X="1.502547363281" Y="22.34111328125" />
                  <Point X="1.470927001953" Y="22.34884375" />
                  <Point X="1.455391113281" Y="22.351302734375" />
                  <Point X="1.424125732422" Y="22.35362109375" />
                  <Point X="1.408396118164" Y="22.35348046875" />
                  <Point X="1.252276489258" Y="22.339115234375" />
                  <Point X="1.175308959961" Y="22.332033203125" />
                  <Point X="1.161228149414" Y="22.32966015625" />
                  <Point X="1.133582641602" Y="22.322830078125" />
                  <Point X="1.120017822266" Y="22.318373046875" />
                  <Point X="1.092632324219" Y="22.30703125" />
                  <Point X="1.079884399414" Y="22.30058984375" />
                  <Point X="1.055496948242" Y="22.2858671875" />
                  <Point X="1.043857543945" Y="22.2775859375" />
                  <Point X="0.923306091309" Y="22.177349609375" />
                  <Point X="0.863873901367" Y="22.12793359375" />
                  <Point X="0.852651550293" Y="22.116908203125" />
                  <Point X="0.832179748535" Y="22.09315625" />
                  <Point X="0.822930358887" Y="22.0804296875" />
                  <Point X="0.806038208008" Y="22.0526015625" />
                  <Point X="0.79901739502" Y="22.03852734375" />
                  <Point X="0.787394348145" Y="22.00941015625" />
                  <Point X="0.782792053223" Y="21.9943671875" />
                  <Point X="0.746747619629" Y="21.82853515625" />
                  <Point X="0.728977783203" Y="21.74678125" />
                  <Point X="0.727584899902" Y="21.7387109375" />
                  <Point X="0.72472479248" Y="21.71032421875" />
                  <Point X="0.7247421875" Y="21.67683203125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.798104064941" Y="21.1134140625" />
                  <Point X="0.833091308594" Y="20.84766015625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146972656" Y="21.546416015625" />
                  <Point X="0.626788391113" Y="21.57618359375" />
                  <Point X="0.620407592773" Y="21.58679296875" />
                  <Point X="0.51074395752" Y="21.744796875" />
                  <Point X="0.4566796875" Y="21.822693359375" />
                  <Point X="0.446670684814" Y="21.834830078125" />
                  <Point X="0.424786987305" Y="21.85728515625" />
                  <Point X="0.412912322998" Y="21.867603515625" />
                  <Point X="0.386656616211" Y="21.88684765625" />
                  <Point X="0.373240112305" Y="21.895064453125" />
                  <Point X="0.345238830566" Y="21.909171875" />
                  <Point X="0.330654296875" Y="21.9150625" />
                  <Point X="0.160956832886" Y="21.96773046875" />
                  <Point X="0.07729535675" Y="21.9936953125" />
                  <Point X="0.063376972198" Y="21.996890625" />
                  <Point X="0.035217689514" Y="22.00116015625" />
                  <Point X="0.020976495743" Y="22.002234375" />
                  <Point X="-0.008664410591" Y="22.002234375" />
                  <Point X="-0.022905456543" Y="22.00116015625" />
                  <Point X="-0.051065036774" Y="21.996890625" />
                  <Point X="-0.064983421326" Y="21.9936953125" />
                  <Point X="-0.23468119812" Y="21.94102734375" />
                  <Point X="-0.318342529297" Y="21.9150625" />
                  <Point X="-0.332931182861" Y="21.909169921875" />
                  <Point X="-0.360932922363" Y="21.895060546875" />
                  <Point X="-0.374346008301" Y="21.88684375" />
                  <Point X="-0.400601135254" Y="21.867599609375" />
                  <Point X="-0.412475219727" Y="21.857283203125" />
                  <Point X="-0.434358886719" Y="21.834828125" />
                  <Point X="-0.444368347168" Y="21.822689453125" />
                  <Point X="-0.554031677246" Y="21.66468359375" />
                  <Point X="-0.608095947266" Y="21.5867890625" />
                  <Point X="-0.61247052002" Y="21.5798671875" />
                  <Point X="-0.625975402832" Y="21.55473828125" />
                  <Point X="-0.63877722168" Y="21.523787109375" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.775028442383" Y="21.018404296875" />
                  <Point X="-0.98542578125" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.665117051724" Y="21.040332531658" />
                  <Point X="2.689206019964" Y="21.05314086327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606480441339" Y="21.116749047691" />
                  <Point X="2.641677007051" Y="21.135463393642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.547843830955" Y="21.193165563724" />
                  <Point X="2.594147994138" Y="21.217785924014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48920727775" Y="21.269582110161" />
                  <Point X="2.546618981225" Y="21.300108454386" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.43057077539" Y="21.345998683632" />
                  <Point X="2.499089968312" Y="21.382430984758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.371934273029" Y="21.422415257103" />
                  <Point X="2.451560955399" Y="21.46475351513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.957090539195" Y="22.26525779448" />
                  <Point X="4.054409342386" Y="22.317003120015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.313297770669" Y="21.498831830573" />
                  <Point X="2.404031942486" Y="21.547076045502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.860076754674" Y="22.321268805064" />
                  <Point X="4.031743796458" Y="22.412545790287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.254661268309" Y="21.575248404044" />
                  <Point X="2.356502929573" Y="21.629398575874" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763062970152" Y="22.377279815649" />
                  <Point X="3.948917688459" Y="22.476100522291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.814487973967" Y="20.917088835031" />
                  <Point X="0.823331755393" Y="20.921791157027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.196024765949" Y="21.651664977515" />
                  <Point X="2.308973895812" Y="21.711721095161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.666049186507" Y="22.433290826698" />
                  <Point X="3.86609158046" Y="22.539655254296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.789253432536" Y="21.011265546164" />
                  <Point X="0.810093384179" Y="21.022346345008" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.137388263588" Y="21.728081550986" />
                  <Point X="2.261444823119" Y="21.794043593748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.569035403024" Y="22.489301837835" />
                  <Point X="3.783265472462" Y="22.603209986301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764018891105" Y="21.105442257297" />
                  <Point X="0.796855018734" Y="21.122901536057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.078751761228" Y="21.804498124457" />
                  <Point X="2.213915750426" Y="21.876366092334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.472021619541" Y="22.545312848971" />
                  <Point X="3.700439364463" Y="22.666764718305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.053825978482" Y="20.246471149656" />
                  <Point X="-0.969912744454" Y="20.291088607629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.738784349673" Y="21.19961896843" />
                  <Point X="0.783616708659" Y="21.223456756546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.020115258868" Y="21.880914697928" />
                  <Point X="2.166386677733" Y="21.958688590921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.375007836058" Y="22.601323860108" />
                  <Point X="3.617613256464" Y="22.73031945031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.135738006416" Y="20.310511906652" />
                  <Point X="-0.936293026749" Y="20.416558683438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.713549808242" Y="21.293795679562" />
                  <Point X="0.770378398584" Y="21.324011977035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.961478756508" Y="21.957331271399" />
                  <Point X="2.11885760504" Y="22.041011089507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.277994052575" Y="22.657334871244" />
                  <Point X="3.534787148465" Y="22.793874182315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.120595488729" Y="20.426157480941" />
                  <Point X="-0.902673309044" Y="20.542028759248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.688315266811" Y="21.387972390695" />
                  <Point X="0.757140088509" Y="21.424567197524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.902842254148" Y="22.03374784487" />
                  <Point X="2.071328532346" Y="22.123333588094" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.180980269092" Y="22.713345882381" />
                  <Point X="3.451961040467" Y="22.857428914319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128728699948" Y="20.529427130641" />
                  <Point X="-0.869053591339" Y="20.667498835057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.663080725379" Y="21.482149101828" />
                  <Point X="0.743901778433" Y="21.525122418014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.844205751787" Y="22.11016441834" />
                  <Point X="2.039376400818" Y="22.213938493214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.083966485609" Y="22.769356893517" />
                  <Point X="3.369135138493" Y="22.920983755869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.148083453494" Y="20.626730180449" />
                  <Point X="-0.835433873634" Y="20.792968910866" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.62911130129" Y="21.571681393467" />
                  <Point X="0.730663468358" Y="21.625677638503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.772699914994" Y="22.179738245314" />
                  <Point X="2.021647556878" Y="22.312106054494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.986952702126" Y="22.825367904653" />
                  <Point X="3.286309238477" Y="22.984538598461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.167438173448" Y="20.724033248118" />
                  <Point X="-0.801814155929" Y="20.918438986675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.575868127126" Y="21.650965650408" />
                  <Point X="0.726831595421" Y="21.731234350337" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.681100280428" Y="22.238628010494" />
                  <Point X="2.003918877577" Y="22.410273703314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.889938918643" Y="22.88137891579" />
                  <Point X="3.203483338461" Y="23.048093441052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.187901246593" Y="20.820746993941" />
                  <Point X="-0.768194499265" Y="21.043909030027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.521321424228" Y="21.729556808826" />
                  <Point X="0.751879738749" Y="21.852146839205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.589500456908" Y="22.297517675205" />
                  <Point X="2.007554720608" Y="22.519801070161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.781192951422" Y="22.931151814179" />
                  <Point X="3.120657438446" Y="23.111648283643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.659358574049" Y="23.929790189952" />
                  <Point X="4.739593299406" Y="23.972451750171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.230867072266" Y="20.905495814007" />
                  <Point X="-0.734575081853" Y="21.169378946168" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.466774893518" Y="21.808148058799" />
                  <Point X="0.778321773514" Y="21.973800473298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.479660135084" Y="22.34670869493" />
                  <Point X="2.07195800548" Y="22.661639058973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.63110961523" Y="22.958945243606" />
                  <Point X="3.039352114379" Y="23.176011630808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.497163955029" Y="23.95114393607" />
                  <Point X="4.765351883939" Y="24.093741987329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.301360498548" Y="20.975607949198" />
                  <Point X="-0.70095566444" Y="21.294848862309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.397493587487" Y="21.87890468976" />
                  <Point X="0.861544193033" Y="22.125644773497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.265272797328" Y="22.340311080232" />
                  <Point X="2.185901973111" Y="22.829818296059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.4317708586" Y="22.960549101425" />
                  <Point X="2.979407948149" Y="23.251732907066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.334968910687" Y="23.972497456039" />
                  <Point X="4.782987118131" Y="24.210712962494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.37773946806" Y="21.042590685544" />
                  <Point X="-0.667336247027" Y="21.42031877845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.288024680786" Y="21.928293194409" />
                  <Point X="2.927856732566" Y="23.331916794343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.172773673861" Y="23.993850873664" />
                  <Point X="4.664745703069" Y="24.255437041708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.454118493141" Y="21.109573392342" />
                  <Point X="-0.628538169675" Y="21.548542236923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.160251772162" Y="21.967949288598" />
                  <Point X="2.880622975108" Y="23.414396314825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.010578437036" Y="24.015204291289" />
                  <Point X="4.530195487047" Y="24.291489577632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.537647888729" Y="21.172754179703" />
                  <Point X="-0.519857206981" Y="21.713923084646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.022203411905" Y="22.00214182824" />
                  <Point X="2.859719124203" Y="23.510875694956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.848383200211" Y="24.036557708914" />
                  <Point X="4.395645271024" Y="24.327542113556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.676647318878" Y="21.206441026513" />
                  <Point X="2.867739971967" Y="23.622734610177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.686187963386" Y="24.057911126539" />
                  <Point X="4.261095055002" Y="24.363594649481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.535373972324" Y="20.857442120472" />
                  <Point X="-2.455207825994" Y="20.900067216575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.856795041872" Y="21.21824893792" />
                  <Point X="2.88268363487" Y="23.738274451502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.52399272656" Y="24.079264544164" />
                  <Point X="4.12654483898" Y="24.399647185405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.628862385413" Y="20.915327604297" />
                  <Point X="2.98330307004" Y="23.899368909006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.32456102053" Y="24.080818979911" />
                  <Point X="3.991994287558" Y="24.435699542994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.722350327015" Y="20.973213338816" />
                  <Point X="3.857443722491" Y="24.471751893328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.807934764457" Y="21.03530144104" />
                  <Point X="3.722893157424" Y="24.507804243662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.890592660942" Y="21.098945612693" />
                  <Point X="3.608761305043" Y="24.554713416114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973250557427" Y="21.162589784346" />
                  <Point X="3.51560651341" Y="24.612776289613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.899482230439" Y="21.309407254379" />
                  <Point X="3.448213661478" Y="24.68453702943" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.809846186841" Y="21.464661738992" />
                  <Point X="3.393579639597" Y="24.763081759522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.720209935133" Y="21.61991633426" />
                  <Point X="3.364923395428" Y="24.855439119037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.630573683425" Y="21.775170929527" />
                  <Point X="3.349459120497" Y="24.954810773017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.770308299286" Y="25.710289682348" />
                  <Point X="4.78335626227" Y="25.717227407331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.540937431717" Y="21.930425524794" />
                  <Point X="3.362498430597" Y="25.069338051996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.362376380134" Y="25.600982588274" />
                  <Point X="4.767885368152" Y="25.816595541827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.451301180009" Y="22.085680120062" />
                  <Point X="3.409447801296" Y="25.201895630022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.954452505349" Y="25.491679771467" />
                  <Point X="4.752280408366" Y="25.915892392343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.361664928302" Y="22.240934715329" />
                  <Point X="4.729088467856" Y="26.011155173651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.312329612128" Y="22.374760923068" />
                  <Point X="4.705897403189" Y="26.106418420653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.325065443677" Y="22.475583316129" />
                  <Point X="4.449594673422" Y="26.077733996691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.372951709706" Y="22.55771589165" />
                  <Point X="4.180646190272" Y="26.042325706385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.453920563362" Y="22.622258143305" />
                  <Point X="3.911698278976" Y="26.006917720138" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.812331567916" Y="22.007572354927" />
                  <Point X="-3.014956507978" Y="22.431544194867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610768079889" Y="22.64645499425" />
                  <Point X="3.642752492602" Y="25.971510863733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.87063210679" Y="22.084167563352" />
                  <Point X="3.411721783405" Y="25.956263811465" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.928932393877" Y="22.160762905654" />
                  <Point X="3.26961839379" Y="25.988300253751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.987232464167" Y="22.23735836323" />
                  <Point X="3.171960457856" Y="26.043968762954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.038709190202" Y="22.317581857302" />
                  <Point X="3.102310915858" Y="26.114529599378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087426235035" Y="22.399272699897" />
                  <Point X="3.050678019139" Y="26.194670056024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.136143279868" Y="22.480963542492" />
                  <Point X="3.018598779226" Y="26.285207376417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.147561001099" Y="22.582486787241" />
                  <Point X="3.001393749567" Y="26.383653454691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.690914420916" Y="22.932884235675" />
                  <Point X="3.01826817153" Y="26.500219898818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.234264769258" Y="23.283283317242" />
                  <Point X="3.085935537473" Y="26.643793430321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.972078238653" Y="23.530284523235" />
                  <Point X="3.401434495023" Y="26.919141356545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948808286193" Y="23.650251531248" />
                  <Point X="3.858080362001" Y="27.269538425762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.969699774538" Y="23.746737484669" />
                  <Point X="4.117700662797" Y="27.515175143161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.027541336239" Y="23.823576735586" />
                  <Point X="4.068440604957" Y="27.59657726062" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120988196321" Y="23.881484313536" />
                  <Point X="4.015792998658" Y="27.676178186611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303555955248" Y="23.892005469013" />
                  <Point X="2.519619337087" Y="26.988242694166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.124004659949" Y="27.30960007069" />
                  <Point X="3.959797660733" Y="27.753999092123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.572501950976" Y="23.856598501292" />
                  <Point X="2.317733974053" Y="26.988492497342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.841448067851" Y="23.821191469157" />
                  <Point X="2.209878097681" Y="27.03873866543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.110394610041" Y="23.785784210877" />
                  <Point X="2.128077807194" Y="27.102838834281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.379341152231" Y="23.750376952597" />
                  <Point X="2.0709940694" Y="27.180081027317" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.648287694421" Y="23.714969694318" />
                  <Point X="2.02959015721" Y="27.265660331513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.683980200952" Y="23.803585806771" />
                  <Point X="2.013969476773" Y="27.364948823212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.708176892905" Y="23.89831435226" />
                  <Point X="2.024617281823" Y="27.478204516398" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.732373429002" Y="23.993042980619" />
                  <Point X="2.072670633665" Y="27.611349091611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750105127227" Y="24.091209024249" />
                  <Point X="-3.749676487503" Y="24.623146367694" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.320166763828" Y="24.851520738763" />
                  <Point X="2.162306846638" Y="27.766603666283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764406099244" Y="24.191199217361" />
                  <Point X="-4.157599588525" Y="24.513843962304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.295647591643" Y="24.972151968686" />
                  <Point X="2.251943059611" Y="27.921858240954" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.778707071261" Y="24.291189410473" />
                  <Point X="-4.565524406868" Y="24.404540643798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.314087397845" Y="25.069941504626" />
                  <Point X="2.341579272583" Y="28.077112815626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361327478739" Y="25.152417662877" />
                  <Point X="2.431215485556" Y="28.232367390297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.439935476176" Y="25.218215204051" />
                  <Point X="2.520851698528" Y="28.387621964969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558125093429" Y="25.262966824649" />
                  <Point X="2.610487911501" Y="28.54287653964" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.692675579205" Y="25.299019217143" />
                  <Point X="2.700124302758" Y="28.698131209107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.82722606498" Y="25.335071609637" />
                  <Point X="2.789760792671" Y="28.85338593103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.961776550755" Y="25.371124002131" />
                  <Point X="2.758268188657" Y="28.944235171264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.096326646192" Y="25.407176602172" />
                  <Point X="2.671698695464" Y="29.005799510054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.230876596518" Y="25.443229279369" />
                  <Point X="2.580017610531" Y="29.064645967306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.365426546843" Y="25.479281956567" />
                  <Point X="2.483460243977" Y="29.120899659628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.499976497169" Y="25.515334633765" />
                  <Point X="2.386903792901" Y="29.177153838719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.634526447494" Y="25.551387310963" />
                  <Point X="2.290347341825" Y="29.23340801781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76907639782" Y="25.587439988161" />
                  <Point X="2.187276459783" Y="29.286198412513" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.770267377899" Y="25.694400887635" />
                  <Point X="-3.802725788897" Y="26.208851876032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453966438157" Y="26.394290512201" />
                  <Point X="2.079614591528" Y="29.336547736547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.752986589321" Y="25.811183400724" />
                  <Point X="-3.964921336571" Y="26.230205128376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421220682884" Y="26.519295893942" />
                  <Point X="1.971952976097" Y="29.38689719501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.735705860114" Y="25.927965882245" />
                  <Point X="-4.127116884245" Y="26.25155838072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.440683479202" Y="26.616541496389" />
                  <Point X="1.864291360667" Y="29.437246653473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.70589013807" Y="26.051413337683" />
                  <Point X="-4.289312431918" Y="26.272911633064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.484011851477" Y="26.701097547007" />
                  <Point X="1.74470721429" Y="29.481256789783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.67182598247" Y="26.177119725312" />
                  <Point X="-4.451507979592" Y="26.294264885408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.549442105053" Y="26.77390181888" />
                  <Point X="1.624411626485" Y="29.524888645975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.63776182687" Y="26.302826112942" />
                  <Point X="-4.613702689357" Y="26.315618583275" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.632268156792" Y="26.837456580798" />
                  <Point X="1.504115603185" Y="29.568520270611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.715094208531" Y="26.901011342717" />
                  <Point X="1.3728714456" Y="29.606330668988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.79792026027" Y="26.964566104635" />
                  <Point X="1.233707757527" Y="29.639930178311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.880746434926" Y="27.028120801198" />
                  <Point X="-0.011445590193" Y="29.085464554278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.211558744907" Y="29.204038062552" />
                  <Point X="1.094542644136" Y="29.673528929778" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.963572670417" Y="27.091675465414" />
                  <Point X="-0.132014803812" Y="29.128950921045" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257961425794" Y="29.33630496045" />
                  <Point X="0.955377530746" Y="29.707127681245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.046398905908" Y="27.15523012963" />
                  <Point X="-0.197648968637" Y="29.201646771383" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.29158088081" Y="29.461774896585" />
                  <Point X="0.813712006832" Y="29.739396940854" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.129225141399" Y="27.218784793846" />
                  <Point X="-3.163367528283" Y="27.732340396382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761223615201" Y="27.946164107852" />
                  <Point X="-0.233316211728" Y="29.290276316646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.325200377268" Y="29.587244854754" />
                  <Point X="0.644654294185" Y="29.75710151536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.21205137689" Y="27.282339458062" />
                  <Point X="-3.280540606359" Y="27.777632520447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.750132202244" Y="28.059655671547" />
                  <Point X="-0.25855075859" Y="29.384453024891" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.358819891796" Y="29.712714822532" />
                  <Point X="0.475597421869" Y="29.774806536678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153662935402" Y="27.420979297916" />
                  <Point X="-3.377554591001" Y="27.833643424625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.761908417512" Y="28.160988301636" />
                  <Point X="-0.283785305452" Y="29.478629733136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.062599984327" Y="27.576992482693" />
                  <Point X="-3.474568575642" Y="27.889654328804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.798976873586" Y="28.248872808739" />
                  <Point X="-0.309019852314" Y="29.572806441381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.954072246441" Y="27.742291859339" />
                  <Point X="-3.571582560284" Y="27.945665232982" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.846505861885" Y="28.331195352199" />
                  <Point X="-0.334254399177" Y="29.666983149626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.811308861651" Y="27.925794652344" />
                  <Point X="-3.668596544926" Y="28.001676137161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.894034850183" Y="28.413517895659" />
                  <Point X="-0.35948893566" Y="29.76115986339" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941563751735" Y="28.495840485243" />
                  <Point X="-1.912557091894" Y="29.042973031523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.496397629259" Y="29.264248942881" />
                  <Point X="-0.56573621867" Y="29.759090392574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.989092600296" Y="28.578163103002" />
                  <Point X="-2.01828048273" Y="29.094353062284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.463748765694" Y="29.389202806387" />
                  <Point X="-0.825206506767" Y="29.728721747973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.036621448857" Y="28.660485720762" />
                  <Point X="-2.10431131905" Y="29.156203810014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.470147838466" Y="29.493394513856" />
                  <Point X="-1.184418554444" Y="29.645319469072" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.820811507658" Y="28.882828056759" />
                  <Point X="-2.164510627375" Y="29.231789424813" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.739844970703" Y="20.461556640625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.354655303955" Y="21.6364609375" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335449219" Y="21.7336015625" />
                  <Point X="0.10463785553" Y="21.78626953125" />
                  <Point X="0.020976472855" Y="21.812234375" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.178362197876" Y="21.75956640625" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278839111" Y="21.714357421875" />
                  <Point X="-0.397942230225" Y="21.5563515625" />
                  <Point X="-0.452006500244" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.591502441406" Y="20.969228515625" />
                  <Point X="-0.84774407959" Y="20.012923828125" />
                  <Point X="-1.002743103027" Y="20.0430078125" />
                  <Point X="-1.100259887695" Y="20.0619375" />
                  <Point X="-1.296842163086" Y="20.112515625" />
                  <Point X="-1.35158984375" Y="20.1266015625" />
                  <Point X="-1.341447998047" Y="20.20363671875" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.350223999023" Y="20.669052734375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.542518798828" Y="20.93438671875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649241333008" Y="21.014236328125" />
                  <Point X="-1.85660144043" Y="21.027828125" />
                  <Point X="-1.958830444336" Y="21.03452734375" />
                  <Point X="-1.989878662109" Y="21.026208984375" />
                  <Point X="-2.162662353516" Y="20.910759765625" />
                  <Point X="-2.247845214844" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.333996582031" Y="20.745923828125" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.71291796875" Y="20.7438984375" />
                  <Point X="-2.855840820312" Y="20.832392578125" />
                  <Point X="-3.128062255859" Y="21.04199609375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.004649902344" Y="21.507251953125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.51398046875" Y="22.431236328125" />
                  <Point X="-2.531329101562" Y="22.448583984375" />
                  <Point X="-2.560157714844" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.017454345703" Y="22.2107109375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.048792236328" Y="22.00452734375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.356870117188" Y="22.480134765625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.036203369141" Y="22.90742578125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822265625" Y="23.603984375" />
                  <Point X="-3.140661865234" Y="23.62391015625" />
                  <Point X="-3.138117675781" Y="23.633732421875" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161160400391" Y="23.689361328125" />
                  <Point X="-3.178898193359" Y="23.69980078125" />
                  <Point X="-3.187643066406" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.757024658203" Y="23.640666015625" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.883232421875" Y="23.815921875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.979029296875" Y="24.349841796875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.551277832031" Y="24.6050625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.54189453125" Y="24.878576171875" />
                  <Point X="-3.523310791016" Y="24.891474609375" />
                  <Point X="-3.514146240234" Y="24.897833984375" />
                  <Point X="-3.494899169922" Y="24.92408984375" />
                  <Point X="-3.488702880859" Y="24.9440546875" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.491843505859" Y="25.003501953125" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514140380859" Y="25.0396015625" />
                  <Point X="-3.532729003906" Y="25.052501953125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.047416259766" Y="25.1973671875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.94610546875" Y="25.8040859375" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.813703125" Y="26.379994140625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.46565625" Y="26.487767578125" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731702148438" Y="26.3958671875" />
                  <Point X="-3.690559570313" Y="26.408837890625" />
                  <Point X="-3.670276123047" Y="26.415234375" />
                  <Point X="-3.651533447266" Y="26.426056640625" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.622611328125" Y="26.483640625" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316650391" Y="26.54551171875" />
                  <Point X="-3.636236083984" Y="26.58377734375" />
                  <Point X="-3.646056396484" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.941006347656" Y="26.834869140625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.270601074219" Y="27.597544921875" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.884676025391" Y="28.1409140625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.596687011719" Y="28.17955078125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138513427734" Y="27.92043359375" />
                  <Point X="-3.081213134766" Y="27.915419921875" />
                  <Point X="-3.052964111328" Y="27.91294921875" />
                  <Point X="-3.031506347656" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.972580810547" Y="27.968076171875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.943087402344" Y="28.085140625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.076603759766" Y="28.349736328125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.945482177734" Y="29.026662109375" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.3192265625" Y="29.4152578125" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.101767822266" Y="29.46212890625" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246582031" Y="29.273662109375" />
                  <Point X="-1.887471679688" Y="29.2404609375" />
                  <Point X="-1.856030517578" Y="29.22409375" />
                  <Point X="-1.835124267578" Y="29.218490234375" />
                  <Point X="-1.813809204102" Y="29.22225" />
                  <Point X="-1.747383300781" Y="29.249765625" />
                  <Point X="-1.714635253906" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.664462890625" Y="29.36305859375" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.666076049805" Y="29.52596875" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.217434814453" Y="29.833388671875" />
                  <Point X="-0.968083007812" Y="29.903296875" />
                  <Point X="-0.44237890625" Y="29.96482421875" />
                  <Point X="-0.224200027466" Y="29.990359375" />
                  <Point X="-0.171573852539" Y="29.793955078125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282117844" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594036102" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.118263366699" Y="29.549048828125" />
                  <Point X="0.236648422241" Y="29.990869140625" />
                  <Point X="0.642493408203" Y="29.948365234375" />
                  <Point X="0.860209777832" Y="29.925564453125" />
                  <Point X="1.295143310547" Y="29.82055859375" />
                  <Point X="1.508455566406" Y="29.769056640625" />
                  <Point X="1.792042236328" Y="29.66619921875" />
                  <Point X="1.931046508789" Y="29.615779296875" />
                  <Point X="2.204780029297" Y="29.487763671875" />
                  <Point X="2.338686279297" Y="29.425140625" />
                  <Point X="2.6031484375" Y="29.271064453125" />
                  <Point X="2.73251953125" Y="29.195693359375" />
                  <Point X="2.981934814453" Y="29.018322265625" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.806964355469" Y="28.50318359375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.210471923828" Y="27.43773828125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.207651855469" Y="27.34582421875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682617188" Y="27.3008125" />
                  <Point X="2.247455322266" Y="27.258408203125" />
                  <Point X="2.261640380859" Y="27.23750390625" />
                  <Point X="2.274939208984" Y="27.224205078125" />
                  <Point X="2.317342773438" Y="27.195431640625" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360334228516" Y="27.172978515625" />
                  <Point X="2.406834472656" Y="27.167373046875" />
                  <Point X="2.429759033203" Y="27.164607421875" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.502440185547" Y="27.180326171875" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.033831787109" Y="27.47693359375" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.125850585938" Y="27.848533203125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.341634765625" Y="27.512109375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.048436523438" Y="27.17611328125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371826172" Y="26.58383203125" />
                  <Point X="3.240669677734" Y="26.533341796875" />
                  <Point X="3.221589599609" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.198703125" Y="26.43994921875" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.202614013672" Y="26.333609375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215647216797" Y="26.287953125" />
                  <Point X="3.247835693359" Y="26.23902734375" />
                  <Point X="3.263704833984" Y="26.214908203125" />
                  <Point X="3.280947509766" Y="26.1988203125" />
                  <Point X="3.327593017578" Y="26.1725625" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.431633300781" Y="26.14528515625" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.943928710938" Y="26.20280078125" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.906228515625" Y="26.0867734375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.983004882812" Y="25.669958984375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.611938476562" Y="25.4711484375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087890625" Y="25.232818359375" />
                  <Point X="3.667125488281" Y="25.19700390625" />
                  <Point X="3.636577880859" Y="25.17934765625" />
                  <Point X="3.622264404297" Y="25.16692578125" />
                  <Point X="3.585086914062" Y="25.119552734375" />
                  <Point X="3.566758544922" Y="25.096197265625" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.544592529297" Y="25.010025390625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.550875732422" Y="24.89460546875" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.8412421875" />
                  <Point X="3.603936035156" Y="24.793869140625" />
                  <Point X="3.622264648438" Y="24.770513671875" />
                  <Point X="3.636575683594" Y="24.75809375" />
                  <Point X="3.698538085938" Y="24.722279296875" />
                  <Point X="3.729085693359" Y="24.70462109375" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.170459472656" Y="24.584583984375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.966834960938" Y="24.1556640625" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.892295898438" Y="23.78760546875" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.421831054688" Y="23.769421875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.273226074219" Y="23.8752265625" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.111939697266" Y="23.7568984375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.053822753906" Y="23.57144140625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360839844" Y="23.483376953125" />
                  <Point X="3.123661621094" Y="23.3786953125" />
                  <Point X="3.156841064453" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.566846191406" Y="23.008765625" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.256008300781" Y="22.281802734375" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.088039306641" Y="22.03290625" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.652861328125" Y="22.221509765625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.592605224609" Y="22.77282421875" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489078125" Y="22.78075390625" />
                  <Point X="2.368838378906" Y="22.71747265625" />
                  <Point X="2.309560058594" Y="22.686275390625" />
                  <Point X="2.288600097656" Y="22.66531640625" />
                  <Point X="2.225318847656" Y="22.545076171875" />
                  <Point X="2.19412109375" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.215302001953" Y="22.308888671875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.490094238281" Y="21.77801171875" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.896855957031" Y="20.8537578125" />
                  <Point X="2.835295654297" Y="20.80978515625" />
                  <Point X="2.705498535156" Y="20.725771484375" />
                  <Point X="2.679774902344" Y="20.70912109375" />
                  <Point X="2.369508056641" Y="21.11346875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.52780078125" Y="22.111306640625" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805175781" Y="22.16428125" />
                  <Point X="1.269685668945" Y="22.149916015625" />
                  <Point X="1.192718383789" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.1314921875" />
                  <Point X="1.04478125" Y="22.031255859375" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.96845690918" Y="21.95401171875" />
                  <Point X="0.932412475586" Y="21.7881796875" />
                  <Point X="0.914642700195" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.98647869873" Y="21.13821484375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.05258215332" Y="20.049517578125" />
                  <Point X="0.99436932373" Y="20.0367578125" />
                  <Point X="0.874437438965" Y="20.014970703125" />
                  <Point X="0.860200256348" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#184" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.129433535499" Y="4.838177028909" Z="1.7" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="-0.45434698919" Y="5.045764806741" Z="1.7" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.7" />
                  <Point X="-1.237088461377" Y="4.912813662684" Z="1.7" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.7" />
                  <Point X="-1.721804275216" Y="4.550724258786" Z="1.7" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.7" />
                  <Point X="-1.718304781699" Y="4.409374967938" Z="1.7" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.7" />
                  <Point X="-1.772673227017" Y="4.327239286749" Z="1.7" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.7" />
                  <Point X="-1.870540247716" Y="4.316091947127" Z="1.7" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.7" />
                  <Point X="-2.06825641938" Y="4.523846994904" Z="1.7" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.7" />
                  <Point X="-2.349665438443" Y="4.490245306979" Z="1.7" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.7" />
                  <Point X="-2.982060157824" Y="4.097622286191" Z="1.7" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.7" />
                  <Point X="-3.126061088333" Y="3.356016272433" Z="1.7" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.7" />
                  <Point X="-2.999053182556" Y="3.112063773088" Z="1.7" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.7" />
                  <Point X="-3.014091451655" Y="3.034712116231" Z="1.7" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.7" />
                  <Point X="-3.083012692519" Y="2.996511516617" Z="1.7" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.7" />
                  <Point X="-3.577843075864" Y="3.254132860412" Z="1.7" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.7" />
                  <Point X="-3.930295423001" Y="3.202897702492" Z="1.7" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.7" />
                  <Point X="-4.319939363342" Y="2.654029776433" Z="1.7" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.7" />
                  <Point X="-3.97760018604" Y="1.826482059631" Z="1.7" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.7" />
                  <Point X="-3.686741932212" Y="1.59196938707" Z="1.7" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.7" />
                  <Point X="-3.674961242716" Y="1.534055556059" Z="1.7" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.7" />
                  <Point X="-3.711753358111" Y="1.487804769401" Z="1.7" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.7" />
                  <Point X="-4.465286031611" Y="1.568620480768" Z="1.7" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.7" />
                  <Point X="-4.868119024286" Y="1.424352983686" Z="1.7" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.7" />
                  <Point X="-5.00172280124" Y="0.842684967303" Z="1.7" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.7" />
                  <Point X="-4.066513210302" Y="0.180351403991" Z="1.7" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.7" />
                  <Point X="-3.567396495854" Y="0.04270852628" Z="1.7" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.7" />
                  <Point X="-3.545753060319" Y="0.01996443795" Z="1.7" />
                  <Point X="-3.539556741714" Y="0" Z="1.7" />
                  <Point X="-3.542611540816" Y="-0.009842513111" Z="1.7" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.7" />
                  <Point X="-3.557972099269" Y="-0.036167456854" Z="1.7" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.7" />
                  <Point X="-4.57037457847" Y="-0.315360652765" Z="1.7" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.7" />
                  <Point X="-5.03468128953" Y="-0.625955498345" Z="1.7" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.7" />
                  <Point X="-4.937833125927" Y="-1.165173103501" Z="1.7" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.7" />
                  <Point X="-3.75665418293" Y="-1.377625892416" Z="1.7" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.7" />
                  <Point X="-3.210414110146" Y="-1.312010103578" Z="1.7" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.7" />
                  <Point X="-3.195220397131" Y="-1.33227264126" Z="1.7" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.7" />
                  <Point X="-4.072797294467" Y="-2.021625861309" Z="1.7" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.7" />
                  <Point X="-4.40596926314" Y="-2.514194778188" Z="1.7" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.7" />
                  <Point X="-4.094734173391" Y="-2.994475097951" Z="1.7" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.7" />
                  <Point X="-2.998610330662" Y="-2.801309871748" Z="1.7" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.7" />
                  <Point X="-2.567110864516" Y="-2.561219604615" Z="1.7" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.7" />
                  <Point X="-3.054107141421" Y="-3.43646805183" Z="1.7" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.7" />
                  <Point X="-3.164721934912" Y="-3.966341543624" Z="1.7" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.7" />
                  <Point X="-2.745395778957" Y="-4.267331923138" Z="1.7" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.7" />
                  <Point X="-2.300484708943" Y="-4.253232831091" Z="1.7" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.7" />
                  <Point X="-2.141039579276" Y="-4.099534836479" Z="1.7" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.7" />
                  <Point X="-1.866026981808" Y="-3.990784818675" Z="1.7" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.7" />
                  <Point X="-1.581642091844" Y="-4.071924925834" Z="1.7" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.7" />
                  <Point X="-1.40541929354" Y="-4.309420266168" Z="1.7" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.7" />
                  <Point X="-1.397176219213" Y="-4.75855739091" Z="1.7" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.7" />
                  <Point X="-1.315457272862" Y="-4.904625630733" Z="1.7" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.7" />
                  <Point X="-1.018427158314" Y="-4.974794906503" Z="1.7" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="-0.549362190992" Y="-4.01243166835" Z="1.7" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="-0.363022476829" Y="-3.440876725295" Z="1.7" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="-0.169697719454" Y="-3.256907278127" Z="1.7" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.7" />
                  <Point X="0.083661359907" Y="-3.230204767161" Z="1.7" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.7" />
                  <Point X="0.307423382604" Y="-3.360769030473" Z="1.7" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.7" />
                  <Point X="0.685392669409" Y="-4.520104361466" Z="1.7" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.7" />
                  <Point X="0.87721859476" Y="-5.002944795799" Z="1.7" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.7" />
                  <Point X="1.0571316454" Y="-4.968041981196" Z="1.7" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.7" />
                  <Point X="1.029894998943" Y="-3.823979628599" Z="1.7" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.7" />
                  <Point X="0.975115708799" Y="-3.191158100205" Z="1.7" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.7" />
                  <Point X="1.07059220249" Y="-2.975910099437" Z="1.7" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.7" />
                  <Point X="1.268110997722" Y="-2.868592843794" Z="1.7" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.7" />
                  <Point X="1.494605660294" Y="-2.899471342927" Z="1.7" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.7" />
                  <Point X="2.323684252007" Y="-3.885688219946" Z="1.7" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.7" />
                  <Point X="2.726512284826" Y="-4.284923269893" Z="1.7" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.7" />
                  <Point X="2.919761941913" Y="-4.155650021361" Z="1.7" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.7" />
                  <Point X="2.527239642241" Y="-3.165708199036" Z="1.7" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.7" />
                  <Point X="2.258350245164" Y="-2.650943689118" Z="1.7" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.7" />
                  <Point X="2.263408923891" Y="-2.44692978986" Z="1.7" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.7" />
                  <Point X="2.385968588512" Y="-2.295492386076" Z="1.7" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.7" />
                  <Point X="2.577563114394" Y="-2.245097764327" Z="1.7" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.7" />
                  <Point X="3.621705398071" Y="-2.790509858034" Z="1.7" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.7" />
                  <Point X="4.122771808405" Y="-2.96459018521" Z="1.7" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.7" />
                  <Point X="4.292386005877" Y="-2.713201825202" Z="1.7" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.7" />
                  <Point X="3.591128243248" Y="-1.920284953338" Z="1.7" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.7" />
                  <Point X="3.159563354236" Y="-1.562984543507" Z="1.7" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.7" />
                  <Point X="3.097456342017" Y="-1.401859845125" Z="1.7" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.7" />
                  <Point X="3.144229898983" Y="-1.243788635974" Z="1.7" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.7" />
                  <Point X="3.27768964184" Y="-1.142352908468" Z="1.7" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.7" />
                  <Point X="4.409148884883" Y="-1.248869560737" Z="1.7" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.7" />
                  <Point X="4.934886801273" Y="-1.192239555332" Z="1.7" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.7" />
                  <Point X="5.010120514147" Y="-0.820508192406" Z="1.7" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.7" />
                  <Point X="4.177244557395" Y="-0.335839038193" Z="1.7" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.7" />
                  <Point X="3.717405102286" Y="-0.203153578043" Z="1.7" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.7" />
                  <Point X="3.637114156266" Y="-0.143983316807" Z="1.7" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.7" />
                  <Point X="3.593827204234" Y="-0.064708898005" Z="1.7" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.7" />
                  <Point X="3.58754424619" Y="0.031901633193" Z="1.7" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.7" />
                  <Point X="3.618265282134" Y="0.119965421768" Z="1.7" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.7" />
                  <Point X="3.685990312066" Y="0.184995266199" Z="1.7" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.7" />
                  <Point X="4.618723259912" Y="0.454132894576" Z="1.7" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.7" />
                  <Point X="5.026253630123" Y="0.708931777884" Z="1.7" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.7" />
                  <Point X="4.948653694054" Y="1.129880875598" Z="1.7" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.7" />
                  <Point X="3.931246348573" Y="1.283653918903" Z="1.7" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.7" />
                  <Point X="3.432028704986" Y="1.22613336343" Z="1.7" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.7" />
                  <Point X="3.345964205965" Y="1.247413568" Z="1.7" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.7" />
                  <Point X="3.283449531212" Y="1.297791245093" Z="1.7" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.7" />
                  <Point X="3.245426551594" Y="1.374993095711" Z="1.7" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.7" />
                  <Point X="3.240699428708" Y="1.457763547961" Z="1.7" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.7" />
                  <Point X="3.274196159341" Y="1.534205292421" Z="1.7" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.7" />
                  <Point X="4.072718653904" Y="2.167725852457" Z="1.7" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.7" />
                  <Point X="4.378256035397" Y="2.569276840375" Z="1.7" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.7" />
                  <Point X="4.160280221175" Y="2.909015939849" Z="1.7" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.7" />
                  <Point X="3.002675777698" Y="2.551515727334" Z="1.7" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.7" />
                  <Point X="2.483366701054" Y="2.259909322487" Z="1.7" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.7" />
                  <Point X="2.40666696382" Y="2.248293468038" Z="1.7" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.7" />
                  <Point X="2.33926170243" Y="2.268085480732" Z="1.7" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.7" />
                  <Point X="2.282673119503" Y="2.317763157952" Z="1.7" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.7" />
                  <Point X="2.251136234721" Y="2.383091480013" Z="1.7" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.7" />
                  <Point X="2.25261862017" Y="2.456102874828" Z="1.7" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.7" />
                  <Point X="2.844109625525" Y="3.509463304257" Z="1.7" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.7" />
                  <Point X="3.004755827691" Y="4.090351201002" Z="1.7" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.7" />
                  <Point X="2.622162261773" Y="4.345548412883" Z="1.7" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.7" />
                  <Point X="2.21980542286" Y="4.564336083476" Z="1.7" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.7" />
                  <Point X="1.802935175926" Y="4.744483030455" Z="1.7" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.7" />
                  <Point X="1.30072204223" Y="4.900441822969" Z="1.7" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.7" />
                  <Point X="0.641545356148" Y="5.029373966098" Z="1.7" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.7" />
                  <Point X="0.063811026054" Y="4.593270477042" Z="1.7" />
                  <Point X="0" Y="4.355124473572" Z="1.7" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>