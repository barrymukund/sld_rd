<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#213" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3486" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563302124023" Y="21.4874765625" />
                  <Point X="0.557720214844" Y="21.502861328125" />
                  <Point X="0.54236340332" Y="21.532623046875" />
                  <Point X="0.387578186035" Y="21.755638671875" />
                  <Point X="0.378635742188" Y="21.7685234375" />
                  <Point X="0.356752593994" Y="21.7909765625" />
                  <Point X="0.330497558594" Y="21.810220703125" />
                  <Point X="0.302495391846" Y="21.824330078125" />
                  <Point X="0.062974403381" Y="21.89866796875" />
                  <Point X="0.049136428833" Y="21.902962890625" />
                  <Point X="0.020977186203" Y="21.907232421875" />
                  <Point X="-0.008665203094" Y="21.907232421875" />
                  <Point X="-0.036824447632" Y="21.902962890625" />
                  <Point X="-0.276345458984" Y="21.828625" />
                  <Point X="-0.290183410645" Y="21.824330078125" />
                  <Point X="-0.318180847168" Y="21.810224609375" />
                  <Point X="-0.344434082031" Y="21.790984375" />
                  <Point X="-0.366321929932" Y="21.768525390625" />
                  <Point X="-0.521107177734" Y="21.5455078125" />
                  <Point X="-0.525663452148" Y="21.538267578125" />
                  <Point X="-0.541827941895" Y="21.509814453125" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.57286920166" Y="21.405822265625" />
                  <Point X="-0.916584533691" Y="20.12305859375" />
                  <Point X="-1.067245605469" Y="20.152302734375" />
                  <Point X="-1.079348510742" Y="20.15465234375" />
                  <Point X="-1.24641784668" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.273730102539" Y="20.7714453125" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.2879375" Y="20.81703125" />
                  <Point X="-1.304008056641" Y="20.8448671875" />
                  <Point X="-1.323643310547" Y="20.868794921875" />
                  <Point X="-1.5441640625" Y="21.062185546875" />
                  <Point X="-1.556904174805" Y="21.073359375" />
                  <Point X="-1.583186767578" Y="21.089701171875" />
                  <Point X="-1.612887817383" Y="21.10200390625" />
                  <Point X="-1.643029785156" Y="21.109033203125" />
                  <Point X="-1.935709838867" Y="21.128216796875" />
                  <Point X="-1.952618896484" Y="21.12932421875" />
                  <Point X="-1.983419433594" Y="21.126291015625" />
                  <Point X="-2.01446484375" Y="21.11797265625" />
                  <Point X="-2.042656860352" Y="21.10519921875" />
                  <Point X="-2.286533447266" Y="20.94224609375" />
                  <Point X="-2.29269140625" Y="20.937763671875" />
                  <Point X="-2.318671386719" Y="20.917212890625" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.347385986328" Y="20.884529296875" />
                  <Point X="-2.480148925781" Y="20.7115078125" />
                  <Point X="-2.783990966797" Y="20.899640625" />
                  <Point X="-2.801713134766" Y="20.91061328125" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406587890625" Y="22.383875" />
                  <Point X="-2.405575683594" Y="22.414810546875" />
                  <Point X="-2.414561279297" Y="22.4444296875" />
                  <Point X="-2.428779052734" Y="22.4732578125" />
                  <Point X="-2.446806884766" Y="22.498412109375" />
                  <Point X="-2.463208007813" Y="22.5148125" />
                  <Point X="-2.464159179688" Y="22.515763671875" />
                  <Point X="-2.489323242188" Y="22.533794921875" />
                  <Point X="-2.518150878906" Y="22.5480078125" />
                  <Point X="-2.547766357422" Y="22.55698828125" />
                  <Point X="-2.578697265625" Y="22.555974609375" />
                  <Point X="-2.610220458984" Y="22.549703125" />
                  <Point X="-2.639184082031" Y="22.53880078125" />
                  <Point X="-2.709608642578" Y="22.498140625" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.068860839844" Y="22.18774609375" />
                  <Point X="-4.082859619141" Y="22.206138671875" />
                  <Point X="-4.306142089844" Y="22.580548828125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046572753906" Y="23.6082890625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.640341796875" />
                  <Point X="-3.045556640625" Y="23.672013671875" />
                  <Point X="-3.052558105469" Y="23.701759765625" />
                  <Point X="-3.068640869141" Y="23.727744140625" />
                  <Point X="-3.089474365234" Y="23.751701171875" />
                  <Point X="-3.112975341797" Y="23.771234375" />
                  <Point X="-3.138011474609" Y="23.78596875" />
                  <Point X="-3.139473876953" Y="23.786830078125" />
                  <Point X="-3.168708984375" Y="23.7980390625" />
                  <Point X="-3.200601074219" Y="23.8045234375" />
                  <Point X="-3.231927978516" Y="23.805615234375" />
                  <Point X="-3.320832275391" Y="23.793912109375" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.8286015625" Y="23.98590625" />
                  <Point X="-4.834076660156" Y="24.00733984375" />
                  <Point X="-4.892424316406" Y="24.41530078125" />
                  <Point X="-3.532874023438" Y="24.779591796875" />
                  <Point X="-3.517493896484" Y="24.785171875" />
                  <Point X="-3.487725830078" Y="24.80053125" />
                  <Point X="-3.461488769531" Y="24.8187421875" />
                  <Point X="-3.459981689453" Y="24.8197890625" />
                  <Point X="-3.437561767578" Y="24.84163671875" />
                  <Point X="-3.418293701172" Y="24.867908203125" />
                  <Point X="-3.404168212891" Y="24.89593359375" />
                  <Point X="-3.395435302734" Y="24.924072265625" />
                  <Point X="-3.394929443359" Y="24.92569921875" />
                  <Point X="-3.390649414062" Y="24.953888671875" />
                  <Point X="-3.390647949219" Y="24.98353515625" />
                  <Point X="-3.394917236328" Y="25.011697265625" />
                  <Point X="-3.403653320312" Y="25.039845703125" />
                  <Point X="-3.404157958984" Y="25.041474609375" />
                  <Point X="-3.418266845703" Y="25.06948828125" />
                  <Point X="-3.437515380859" Y="25.095755859375" />
                  <Point X="-3.459977294922" Y="25.1176484375" />
                  <Point X="-3.486202148438" Y="25.135849609375" />
                  <Point X="-3.498952392578" Y="25.14330078125" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-3.613916503906" Y="25.179564453125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.828015625" Y="25.953134765625" />
                  <Point X="-4.824487792969" Y="25.9769765625" />
                  <Point X="-4.703551757812" Y="26.423267578125" />
                  <Point X="-3.765670166016" Y="26.29979296875" />
                  <Point X="-3.744984863281" Y="26.299341796875" />
                  <Point X="-3.723424072266" Y="26.301228515625" />
                  <Point X="-3.703140136719" Y="26.305263671875" />
                  <Point X="-3.645069091797" Y="26.323572265625" />
                  <Point X="-3.641718017578" Y="26.32462890625" />
                  <Point X="-3.622756591797" Y="26.332970703125" />
                  <Point X="-3.604018310547" Y="26.34379296875" />
                  <Point X="-3.587342041016" Y="26.356025390625" />
                  <Point X="-3.573707763672" Y="26.37157421875" />
                  <Point X="-3.561297119141" Y="26.38930078125" />
                  <Point X="-3.551351318359" Y="26.4074296875" />
                  <Point X="-3.528050048828" Y="26.46368359375" />
                  <Point X="-3.526703857422" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951171875" Y="26.549193359375" />
                  <Point X="-3.524552978516" Y="26.570099609375" />
                  <Point X="-3.532049804688" Y="26.589376953125" />
                  <Point X="-3.560165283203" Y="26.64338671875" />
                  <Point X="-3.561800292969" Y="26.64652734375" />
                  <Point X="-3.5732890625" Y="26.663716796875" />
                  <Point X="-3.587198486328" Y="26.680291015625" />
                  <Point X="-3.602135253906" Y="26.694587890625" />
                  <Point X="-3.648620361328" Y="26.7302578125" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.094859863281" Y="27.710177734375" />
                  <Point X="-4.081152099609" Y="27.733662109375" />
                  <Point X="-3.750504638672" Y="28.158662109375" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729492188" Y="27.836341796875" />
                  <Point X="-3.167087402344" Y="27.82983203125" />
                  <Point X="-3.146795166016" Y="27.825794921875" />
                  <Point X="-3.065918212891" Y="27.81871875" />
                  <Point X="-3.061217041016" Y="27.81830859375" />
                  <Point X="-3.040552734375" Y="27.818763671875" />
                  <Point X="-3.019097900391" Y="27.82158984375" />
                  <Point X="-2.999009765625" Y="27.826505859375" />
                  <Point X="-2.980461914062" Y="27.83565234375" />
                  <Point X="-2.9622109375" Y="27.847279296875" />
                  <Point X="-2.946078857422" Y="27.8602265625" />
                  <Point X="-2.888671875" Y="27.9176328125" />
                  <Point X="-2.885355224609" Y="27.92094921875" />
                  <Point X="-2.872409667969" Y="27.937078125" />
                  <Point X="-2.860779785156" Y="27.95533203125" />
                  <Point X="-2.851630371094" Y="27.9738828125" />
                  <Point X="-2.846713378906" Y="27.993974609375" />
                  <Point X="-2.843887451172" Y="28.01543359375" />
                  <Point X="-2.843435791016" Y="28.0361171875" />
                  <Point X="-2.85051171875" Y="28.116994140625" />
                  <Point X="-2.850920410156" Y="28.12166796875" />
                  <Point X="-2.854954833984" Y="28.141953125" />
                  <Point X="-2.861463378906" Y="28.16259765625" />
                  <Point X="-2.869795410156" Y="28.181533203125" />
                  <Point X="-2.890394287109" Y="28.2172109375" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.724491699219" Y="29.0763828125" />
                  <Point X="-2.700618896484" Y="29.0946875" />
                  <Point X="-2.167035400391" Y="29.3911328125" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028893554688" Y="29.21480078125" />
                  <Point X="-2.01231237793" Y="29.20088671875" />
                  <Point X="-1.99511328125" Y="29.18939453125" />
                  <Point X="-1.90509765625" Y="29.142533203125" />
                  <Point X="-1.899897216797" Y="29.139826171875" />
                  <Point X="-1.880614746094" Y="29.132328125" />
                  <Point X="-1.85970690918" Y="29.1267265625" />
                  <Point X="-1.83926184082" Y="29.123580078125" />
                  <Point X="-1.818620361328" Y="29.12493359375" />
                  <Point X="-1.797304199219" Y="29.128693359375" />
                  <Point X="-1.777450317383" Y="29.134482421875" />
                  <Point X="-1.683693237305" Y="29.173318359375" />
                  <Point X="-1.678276489258" Y="29.1755625" />
                  <Point X="-1.660133300781" Y="29.185517578125" />
                  <Point X="-1.642405517578" Y="29.19793359375" />
                  <Point X="-1.626859130859" Y="29.2115703125" />
                  <Point X="-1.614631591797" Y="29.22824609375" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.564963867188" Y="29.36270703125" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165405273" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773046875" Y="29.430826171875" />
                  <Point X="-1.560072265625" Y="29.44861328125" />
                  <Point X="-1.584201904297" Y="29.631896484375" />
                  <Point X="-0.980532531738" Y="29.80114453125" />
                  <Point X="-0.949624633789" Y="29.80980859375" />
                  <Point X="-0.294711242676" Y="29.886458984375" />
                  <Point X="-0.133903213501" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082113998413" Y="29.208806640625" />
                  <Point X="-0.054817985535" Y="29.194216796875" />
                  <Point X="-0.024379911423" Y="29.183884765625" />
                  <Point X="0.00615600872" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.09442591095" Y="29.208806640625" />
                  <Point X="0.115583435059" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.14621534729" Y="29.286314453125" />
                  <Point X="0.156769943237" Y="29.325705078125" />
                  <Point X="0.307419647217" Y="29.8879375" />
                  <Point X="0.817055603027" Y="29.834564453125" />
                  <Point X="0.844030212402" Y="29.831740234375" />
                  <Point X="1.45313293457" Y="29.68468359375" />
                  <Point X="1.481038452148" Y="29.6779453125" />
                  <Point X="1.877408569336" Y="29.5341796875" />
                  <Point X="1.894640625" Y="29.5279296875" />
                  <Point X="2.278003417969" Y="29.34864453125" />
                  <Point X="2.294561279297" Y="29.340900390625" />
                  <Point X="2.6649375" Y="29.125119140625" />
                  <Point X="2.680978759766" Y="29.1157734375" />
                  <Point X="2.943259765625" Y="28.92925390625" />
                  <Point X="2.147580566406" Y="27.55109765625" />
                  <Point X="2.142076416016" Y="27.539931640625" />
                  <Point X="2.133076904297" Y="27.516056640625" />
                  <Point X="2.112779785156" Y="27.440154296875" />
                  <Point X="2.110102539062" Y="27.425796875" />
                  <Point X="2.107591796875" Y="27.402509765625" />
                  <Point X="2.107727783203" Y="27.380953125" />
                  <Point X="2.115642089844" Y="27.3153203125" />
                  <Point X="2.116099121094" Y="27.311529296875" />
                  <Point X="2.121441162109" Y="27.289607421875" />
                  <Point X="2.129707275391" Y="27.267517578125" />
                  <Point X="2.1400703125" Y="27.24747265625" />
                  <Point X="2.180681640625" Y="27.18762109375" />
                  <Point X="2.18985546875" Y="27.176126953125" />
                  <Point X="2.205502197266" Y="27.159369140625" />
                  <Point X="2.221598876953" Y="27.145591796875" />
                  <Point X="2.281438232422" Y="27.10498828125" />
                  <Point X="2.28487109375" Y="27.102658203125" />
                  <Point X="2.304933105469" Y="27.09228125" />
                  <Point X="2.327035644531" Y="27.0840078125" />
                  <Point X="2.348966552734" Y="27.078662109375" />
                  <Point X="2.414599609375" Y="27.070748046875" />
                  <Point X="2.429657470703" Y="27.07013671875" />
                  <Point X="2.452349365234" Y="27.071017578125" />
                  <Point X="2.473206054688" Y="27.074169921875" />
                  <Point X="2.549107421875" Y="27.094466796875" />
                  <Point X="2.557923339844" Y="27.097291015625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="2.670044921875" Y="27.15720703125" />
                  <Point X="3.967325439453" Y="27.90619140625" />
                  <Point X="4.113756347656" Y="27.702685546875" />
                  <Point X="4.1232734375" Y="27.689458984375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.230785400391" Y="26.668451171875" />
                  <Point X="3.221421630859" Y="26.66023828125" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.149348144531" Y="26.57036328125" />
                  <Point X="3.141568115234" Y="26.55846484375" />
                  <Point X="3.129942626953" Y="26.537396484375" />
                  <Point X="3.121630126953" Y="26.5170859375" />
                  <Point X="3.101281738281" Y="26.44432421875" />
                  <Point X="3.100106201172" Y="26.44012109375" />
                  <Point X="3.096652832031" Y="26.41782421875" />
                  <Point X="3.095836181641" Y="26.39425390625" />
                  <Point X="3.097739257812" Y="26.371767578125" />
                  <Point X="3.114443115234" Y="26.2908125" />
                  <Point X="3.118389160156" Y="26.277037109375" />
                  <Point X="3.126551513672" Y="26.254982421875" />
                  <Point X="3.136281982422" Y="26.235740234375" />
                  <Point X="3.181714599609" Y="26.166685546875" />
                  <Point X="3.184339599609" Y="26.1626953125" />
                  <Point X="3.198890625" Y="26.145453125" />
                  <Point X="3.216135742188" Y="26.129361328125" />
                  <Point X="3.234348144531" Y="26.116033203125" />
                  <Point X="3.300186523438" Y="26.07897265625" />
                  <Point X="3.313531494141" Y="26.072767578125" />
                  <Point X="3.335310302734" Y="26.06462890625" />
                  <Point X="3.356119384766" Y="26.0594375" />
                  <Point X="3.445137207031" Y="26.047673828125" />
                  <Point X="3.454021484375" Y="26.046921875" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.565633300781" Y="26.057177734375" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.841849121094" Y="25.94959375" />
                  <Point X="4.8459375" Y="25.932802734375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.71658203125" Y="25.32958984375" />
                  <Point X="3.704790771484" Y="25.3255859375" />
                  <Point X="3.681550537109" Y="25.3150703125" />
                  <Point X="3.594093261719" Y="25.26451953125" />
                  <Point X="3.582646484375" Y="25.25673828125" />
                  <Point X="3.563275878906" Y="25.24139453125" />
                  <Point X="3.547529052734" Y="25.225576171875" />
                  <Point X="3.4950546875" Y="25.1587109375" />
                  <Point X="3.492023193359" Y="25.15484765625" />
                  <Point X="3.480298339844" Y="25.135564453125" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.09260546875" />
                  <Point X="3.446189453125" Y="25.001271484375" />
                  <Point X="3.444577392578" Y="24.987380859375" />
                  <Point X="3.443567382812" Y="24.9632890625" />
                  <Point X="3.445179443359" Y="24.94144140625" />
                  <Point X="3.462670898438" Y="24.850107421875" />
                  <Point X="3.463681640625" Y="24.84483203125" />
                  <Point X="3.47052734375" Y="24.82333203125" />
                  <Point X="3.480298583984" Y="24.801873046875" />
                  <Point X="3.492023193359" Y="24.782591796875" />
                  <Point X="3.544497558594" Y="24.7157265625" />
                  <Point X="3.554207275391" Y="24.7051171875" />
                  <Point X="3.571552978516" Y="24.68883203125" />
                  <Point X="3.589036865234" Y="24.675841796875" />
                  <Point X="3.676494140625" Y="24.625291015625" />
                  <Point X="3.684132324219" Y="24.621328125" />
                  <Point X="3.716581054688" Y="24.60784765625" />
                  <Point X="3.787588134766" Y="24.588822265625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.8573046875" Y="24.06641015625" />
                  <Point X="4.855021484375" Y="24.051265625" />
                  <Point X="4.801174804688" Y="23.81530078125" />
                  <Point X="3.4243828125" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658203125" Y="23.994490234375" />
                  <Point X="3.203010498047" Y="23.957181640625" />
                  <Point X="3.193099365234" Y="23.95502734375" />
                  <Point X="3.163986083984" Y="23.943408203125" />
                  <Point X="3.136153808594" Y="23.926515625" />
                  <Point X="3.112396972656" Y="23.9060390625" />
                  <Point X="3.008646728516" Y="23.781259765625" />
                  <Point X="3.002652832031" Y="23.77405078125" />
                  <Point X="2.987931640625" Y="23.74966796875" />
                  <Point X="2.976588867188" Y="23.722283203125" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.954887695312" Y="23.5330390625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956348144531" Y="23.492431640625" />
                  <Point X="2.964081298828" Y="23.46080859375" />
                  <Point X="2.976451904297" Y="23.432" />
                  <Point X="3.071444091797" Y="23.28424609375" />
                  <Point X="3.077473632812" Y="23.275900390625" />
                  <Point X="3.094580078125" Y="23.25473828125" />
                  <Point X="3.110628662109" Y="23.23908984375" />
                  <Point X="3.176523681641" Y="23.18852734375" />
                  <Point X="4.213123046875" Y="22.393115234375" />
                  <Point X="4.131241210938" Y="22.260619140625" />
                  <Point X="4.124819824219" Y="22.2502265625" />
                  <Point X="4.028980957031" Y="22.1140546875" />
                  <Point X="2.800954101562" Y="22.823056640625" />
                  <Point X="2.786134521484" Y="22.829986328125" />
                  <Point X="2.754222167969" Y="22.84017578125" />
                  <Point X="2.549934082031" Y="22.8770703125" />
                  <Point X="2.538131835938" Y="22.879201171875" />
                  <Point X="2.506774658203" Y="22.879603515625" />
                  <Point X="2.474604736328" Y="22.87464453125" />
                  <Point X="2.444833984375" Y="22.864822265625" />
                  <Point X="2.275120849609" Y="22.77550390625" />
                  <Point X="2.265315917969" Y="22.77034375" />
                  <Point X="2.242386962891" Y="22.753453125" />
                  <Point X="2.221426757812" Y="22.732494140625" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.115213134766" Y="22.53984765625" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.132569580078" Y="22.232453125" />
                  <Point X="2.134762939453" Y="22.223060546875" />
                  <Point X="2.142797363281" Y="22.19514453125" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.194163574219" Y="22.100578125" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.789486083984" Y="20.893810546875" />
                  <Point X="2.781818847656" Y="20.888333984375" />
                  <Point X="2.701765380859" Y="20.836517578125" />
                  <Point X="1.758546630859" Y="22.065744140625" />
                  <Point X="1.74750390625" Y="22.0778203125" />
                  <Point X="1.721920898438" Y="22.0994453125" />
                  <Point X="1.520437866211" Y="22.22898046875" />
                  <Point X="1.508797363281" Y="22.236462890625" />
                  <Point X="1.479979858398" Y="22.2488359375" />
                  <Point X="1.448362182617" Y="22.256564453125" />
                  <Point X="1.417099365234" Y="22.258880859375" />
                  <Point X="1.196776123047" Y="22.23860546875" />
                  <Point X="1.184045043945" Y="22.237439453125" />
                  <Point X="1.156370117188" Y="22.23060546875" />
                  <Point X="1.128978393555" Y="22.219259765625" />
                  <Point X="1.104594848633" Y="22.204537109375" />
                  <Point X="0.934441650391" Y="22.06305859375" />
                  <Point X="0.92461126709" Y="22.054884765625" />
                  <Point X="0.904141723633" Y="22.03113671875" />
                  <Point X="0.88725" Y="22.0033125" />
                  <Point X="0.875624572754" Y="21.97419140625" />
                  <Point X="0.824749389648" Y="21.740126953125" />
                  <Point X="0.823248657227" Y="21.73118359375" />
                  <Point X="0.819596130371" Y="21.700515625" />
                  <Point X="0.819742126465" Y="21.676880859375" />
                  <Point X="0.831742248535" Y="21.58573046875" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.982903503418" Y="20.1315" />
                  <Point X="0.975691589355" Y="20.129919921875" />
                  <Point X="0.929315490723" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.049143432617" Y="20.2455625" />
                  <Point X="-1.058444580078" Y="20.2473671875" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.458962890625" />
                  <Point X="-1.121759155273" Y="20.49066796875" />
                  <Point X="-1.123333984375" Y="20.502306640625" />
                  <Point X="-1.180555541992" Y="20.789978515625" />
                  <Point X="-1.18812487793" Y="20.821529296875" />
                  <Point X="-1.199026367188" Y="20.850494140625" />
                  <Point X="-1.205664428711" Y="20.864529296875" />
                  <Point X="-1.221734985352" Y="20.892365234375" />
                  <Point X="-1.230569580078" Y="20.905130859375" />
                  <Point X="-1.250204711914" Y="20.92905859375" />
                  <Point X="-1.261005615234" Y="20.940220703125" />
                  <Point X="-1.481526367188" Y="21.133611328125" />
                  <Point X="-1.506741699219" Y="21.15403515625" />
                  <Point X="-1.533024291992" Y="21.170376953125" />
                  <Point X="-1.546831420898" Y="21.17746875" />
                  <Point X="-1.576532470703" Y="21.189771484375" />
                  <Point X="-1.591312133789" Y="21.194521484375" />
                  <Point X="-1.621454101563" Y="21.20155078125" />
                  <Point X="-1.63681640625" Y="21.203830078125" />
                  <Point X="-1.929496459961" Y="21.223013671875" />
                  <Point X="-1.961929321289" Y="21.2238671875" />
                  <Point X="-1.992729858398" Y="21.220833984375" />
                  <Point X="-2.008006591797" Y="21.2180546875" />
                  <Point X="-2.039052001953" Y="21.209736328125" />
                  <Point X="-2.053671386719" Y="21.204505859375" />
                  <Point X="-2.081863525391" Y="21.191732421875" />
                  <Point X="-2.095436035156" Y="21.184189453125" />
                  <Point X="-2.3393125" Y="21.021236328125" />
                  <Point X="-2.351628662109" Y="21.012271484375" />
                  <Point X="-2.377608642578" Y="20.991720703125" />
                  <Point X="-2.386340576172" Y="20.983890625" />
                  <Point X="-2.402771972656" Y="20.96721484375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.422754638672" Y="20.942361328125" />
                  <Point X="-2.503203125" Y="20.837517578125" />
                  <Point X="-2.733979492188" Y="20.98041015625" />
                  <Point X="-2.747577880859" Y="20.988830078125" />
                  <Point X="-2.980862792969" Y="21.168451171875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413085938" Y="22.365341796875" />
                  <Point X="-2.311638671875" Y="22.380767578125" />
                  <Point X="-2.310626464844" Y="22.411703125" />
                  <Point X="-2.314666992188" Y="22.442388671875" />
                  <Point X="-2.323652587891" Y="22.4720078125" />
                  <Point X="-2.329359863281" Y="22.486451171875" />
                  <Point X="-2.343577636719" Y="22.515279296875" />
                  <Point X="-2.351562255859" Y="22.52859765625" />
                  <Point X="-2.369590087891" Y="22.553751953125" />
                  <Point X="-2.379633300781" Y="22.565587890625" />
                  <Point X="-2.396034423828" Y="22.58198828125" />
                  <Point X="-2.408825927734" Y="22.592986328125" />
                  <Point X="-2.433989990234" Y="22.611017578125" />
                  <Point X="-2.447313720703" Y="22.619001953125" />
                  <Point X="-2.476141357422" Y="22.63321484375" />
                  <Point X="-2.490583007812" Y="22.638919921875" />
                  <Point X="-2.520198486328" Y="22.647900390625" />
                  <Point X="-2.550877929688" Y="22.6519375" />
                  <Point X="-2.581808837891" Y="22.650923828125" />
                  <Point X="-2.597234130859" Y="22.6491484375" />
                  <Point X="-2.628757324219" Y="22.642876953125" />
                  <Point X="-2.6436875" Y="22.63861328125" />
                  <Point X="-2.672651123047" Y="22.6277109375" />
                  <Point X="-2.686684570312" Y="22.621072265625" />
                  <Point X="-2.757109130859" Y="22.580412109375" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.993267578125" Y="22.24528515625" />
                  <Point X="-4.004018310547" Y="22.25941015625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954607177734" Y="23.584470703125" />
                  <Point X="-2.951552734375" Y="23.6011953125" />
                  <Point X="-2.948748535156" Y="23.631623046875" />
                  <Point X="-2.948577880859" Y="23.646951171875" />
                  <Point X="-2.950786865234" Y="23.678623046875" />
                  <Point X="-2.953083740234" Y="23.693779296875" />
                  <Point X="-2.960085205078" Y="23.723525390625" />
                  <Point X="-2.971779052734" Y="23.7517578125" />
                  <Point X="-2.987861816406" Y="23.7777421875" />
                  <Point X="-2.996955322266" Y="23.790083984375" />
                  <Point X="-3.017788818359" Y="23.814041015625" />
                  <Point X="-3.028750244141" Y="23.824759765625" />
                  <Point X="-3.052251220703" Y="23.84429296875" />
                  <Point X="-3.064790771484" Y="23.853107421875" />
                  <Point X="-3.089799316406" Y="23.867826171875" />
                  <Point X="-3.105464111328" Y="23.875533203125" />
                  <Point X="-3.13469921875" Y="23.8867421875" />
                  <Point X="-3.149780761719" Y="23.891134765625" />
                  <Point X="-3.181672851562" Y="23.897619140625" />
                  <Point X="-3.197292236328" Y="23.89946484375" />
                  <Point X="-3.228619140625" Y="23.900556640625" />
                  <Point X="-3.244326660156" Y="23.899802734375" />
                  <Point X="-3.333230957031" Y="23.888099609375" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.736556640625" Y="24.00941796875" />
                  <Point X="-4.740761230469" Y="24.02587890625" />
                  <Point X="-4.786452148438" Y="24.34534375" />
                  <Point X="-3.508286132812" Y="24.687828125" />
                  <Point X="-3.500473632812" Y="24.690287109375" />
                  <Point X="-3.47393359375" Y="24.700748046875" />
                  <Point X="-3.444165527344" Y="24.716107421875" />
                  <Point X="-3.433556884766" Y="24.72248828125" />
                  <Point X="-3.407319824219" Y="24.74069921875" />
                  <Point X="-3.393680419922" Y="24.751751953125" />
                  <Point X="-3.371260498047" Y="24.773599609375" />
                  <Point X="-3.360956542969" Y="24.785453125" />
                  <Point X="-3.341688476562" Y="24.811724609375" />
                  <Point X="-3.333460205078" Y="24.825150390625" />
                  <Point X="-3.319334716797" Y="24.85317578125" />
                  <Point X="-3.313437255859" Y="24.867775390625" />
                  <Point X="-3.304704345703" Y="24.8959140625" />
                  <Point X="-3.301005859375" Y="24.911439453125" />
                  <Point X="-3.296725830078" Y="24.93962890625" />
                  <Point X="-3.295649414062" Y="24.953884765625" />
                  <Point X="-3.295647949219" Y="24.98353125" />
                  <Point X="-3.296721191406" Y="24.9977734375" />
                  <Point X="-3.300990478516" Y="25.025935546875" />
                  <Point X="-3.304186523438" Y="25.03985546875" />
                  <Point X="-3.312908691406" Y="25.067958984375" />
                  <Point X="-3.319311279297" Y="25.08420703125" />
                  <Point X="-3.333420166016" Y="25.112220703125" />
                  <Point X="-3.341638427734" Y="25.125640625" />
                  <Point X="-3.360886962891" Y="25.151908203125" />
                  <Point X="-3.371208007812" Y="25.163787109375" />
                  <Point X="-3.393669921875" Y="25.1856796875" />
                  <Point X="-3.405810791016" Y="25.195693359375" />
                  <Point X="-3.432035644531" Y="25.21389453125" />
                  <Point X="-3.43826953125" Y="25.21787109375" />
                  <Point X="-3.461508056641" Y="25.230609375" />
                  <Point X="-3.495431640625" Y="25.245158203125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-3.589328613281" Y="25.271328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.7340390625" Y="25.939228515625" />
                  <Point X="-4.731330566406" Y="25.957533203125" />
                  <Point X="-4.633586914062" Y="26.318236328125" />
                  <Point X="-3.778070068359" Y="26.20560546875" />
                  <Point X="-3.767741699219" Y="26.20481640625" />
                  <Point X="-3.747056396484" Y="26.204365234375" />
                  <Point X="-3.736703369141" Y="26.204703125" />
                  <Point X="-3.715142578125" Y="26.20658984375" />
                  <Point X="-3.704888671875" Y="26.2080546875" />
                  <Point X="-3.684604736328" Y="26.21208984375" />
                  <Point X="-3.674574707031" Y="26.21466015625" />
                  <Point X="-3.616503662109" Y="26.23296875" />
                  <Point X="-3.603462646484" Y="26.237671875" />
                  <Point X="-3.584501220703" Y="26.246013671875" />
                  <Point X="-3.575244384766" Y="26.250705078125" />
                  <Point X="-3.556506103516" Y="26.26152734375" />
                  <Point X="-3.547829345703" Y="26.26719140625" />
                  <Point X="-3.531153076172" Y="26.279423828125" />
                  <Point X="-3.515913330078" Y="26.293392578125" />
                  <Point X="-3.502279052734" Y="26.30894140625" />
                  <Point X="-3.495885009766" Y="26.31708984375" />
                  <Point X="-3.483474365234" Y="26.33481640625" />
                  <Point X="-3.478008056641" Y="26.343607421875" />
                  <Point X="-3.468062255859" Y="26.361736328125" />
                  <Point X="-3.463582763672" Y="26.37107421875" />
                  <Point X="-3.440281494141" Y="26.427328125" />
                  <Point X="-3.435498291016" Y="26.440353515625" />
                  <Point X="-3.429709960938" Y="26.46021484375" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543201171875" />
                  <Point X="-3.425056884766" Y="26.56364453125" />
                  <Point X="-3.427188232422" Y="26.57378125" />
                  <Point X="-3.432790039062" Y="26.5946875" />
                  <Point X="-3.436012695312" Y="26.604533203125" />
                  <Point X="-3.443509521484" Y="26.623810546875" />
                  <Point X="-3.447783691406" Y="26.6332421875" />
                  <Point X="-3.475899169922" Y="26.687251953125" />
                  <Point X="-3.482817382812" Y="26.69931640625" />
                  <Point X="-3.494306152344" Y="26.716505859375" />
                  <Point X="-3.500519287109" Y="26.724787109375" />
                  <Point X="-3.514428710938" Y="26.741361328125" />
                  <Point X="-3.521509521484" Y="26.748919921875" />
                  <Point X="-3.536446289062" Y="26.763216796875" />
                  <Point X="-3.544302246094" Y="26.769955078125" />
                  <Point X="-3.590787353516" Y="26.805625" />
                  <Point X="-4.227615234375" Y="27.29428125" />
                  <Point X="-4.012813476562" Y="27.6622890625" />
                  <Point X="-4.002292480469" Y="27.680314453125" />
                  <Point X="-3.726337890625" Y="28.035013671875" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244924804688" Y="27.757720703125" />
                  <Point X="-3.225997314453" Y="27.749390625" />
                  <Point X="-3.216302001953" Y="27.745740234375" />
                  <Point X="-3.195659912109" Y="27.73923046875" />
                  <Point X="-3.185624267578" Y="27.736658203125" />
                  <Point X="-3.16533203125" Y="27.73262109375" />
                  <Point X="-3.155075439453" Y="27.73115625" />
                  <Point X="-3.074198486328" Y="27.724080078125" />
                  <Point X="-3.059125488281" Y="27.72333203125" />
                  <Point X="-3.038461181641" Y="27.723787109375" />
                  <Point X="-3.028145996094" Y="27.724578125" />
                  <Point X="-3.006691162109" Y="27.727404296875" />
                  <Point X="-2.996515625" Y="27.7293125" />
                  <Point X="-2.976427490234" Y="27.734228515625" />
                  <Point X="-2.956993408203" Y="27.741302734375" />
                  <Point X="-2.938445556641" Y="27.75044921875" />
                  <Point X="-2.929419189453" Y="27.755529296875" />
                  <Point X="-2.911168212891" Y="27.76715625" />
                  <Point X="-2.902748535156" Y="27.773189453125" />
                  <Point X="-2.886616455078" Y="27.78613671875" />
                  <Point X="-2.878904052734" Y="27.79305078125" />
                  <Point X="-2.821497070312" Y="27.85045703125" />
                  <Point X="-2.811267822266" Y="27.861484375" />
                  <Point X="-2.798322265625" Y="27.87761328125" />
                  <Point X="-2.792289306641" Y="27.88603125" />
                  <Point X="-2.780659423828" Y="27.90428515625" />
                  <Point X="-2.775579101562" Y="27.913310546875" />
                  <Point X="-2.7664296875" Y="27.931861328125" />
                  <Point X="-2.759353515625" Y="27.95130078125" />
                  <Point X="-2.754436523438" Y="27.971392578125" />
                  <Point X="-2.752526611328" Y="27.9815703125" />
                  <Point X="-2.749700683594" Y="28.003029296875" />
                  <Point X="-2.74891015625" Y="28.013359375" />
                  <Point X="-2.748458496094" Y="28.03404296875" />
                  <Point X="-2.748797363281" Y="28.044396484375" />
                  <Point X="-2.755873291016" Y="28.1252734375" />
                  <Point X="-2.757745361328" Y="28.14019921875" />
                  <Point X="-2.761779785156" Y="28.160484375" />
                  <Point X="-2.764350830078" Y="28.170517578125" />
                  <Point X="-2.770859375" Y="28.191162109375" />
                  <Point X="-2.774509033203" Y="28.200859375" />
                  <Point X="-2.782841064453" Y="28.219794921875" />
                  <Point X="-2.7875234375" Y="28.229033203125" />
                  <Point X="-2.808122314453" Y="28.2647109375" />
                  <Point X="-3.059386962891" Y="28.699912109375" />
                  <Point X="-2.666689453125" Y="29.000990234375" />
                  <Point X="-2.648363769531" Y="29.015041015625" />
                  <Point X="-2.192522949219" Y="29.268294921875" />
                  <Point X="-2.118563720703" Y="29.17191015625" />
                  <Point X="-2.111823486328" Y="29.164052734375" />
                  <Point X="-2.097521728516" Y="29.149111328125" />
                  <Point X="-2.089960449219" Y="29.14202734375" />
                  <Point X="-2.073379150391" Y="29.12811328125" />
                  <Point X="-2.065092041016" Y="29.121896484375" />
                  <Point X="-2.047892822266" Y="29.110404296875" />
                  <Point X="-2.038980957031" Y="29.10512890625" />
                  <Point X="-1.948965332031" Y="29.058267578125" />
                  <Point X="-1.934326904297" Y="29.05128515625" />
                  <Point X="-1.915044433594" Y="29.043787109375" />
                  <Point X="-1.905199829102" Y="29.040564453125" />
                  <Point X="-1.884291992188" Y="29.034962890625" />
                  <Point X="-1.874157226562" Y="29.03283203125" />
                  <Point X="-1.853712158203" Y="29.029685546875" />
                  <Point X="-1.833045776367" Y="29.028783203125" />
                  <Point X="-1.812404296875" Y="29.03013671875" />
                  <Point X="-1.802118896484" Y="29.031376953125" />
                  <Point X="-1.780802734375" Y="29.03513671875" />
                  <Point X="-1.770711181641" Y="29.0374921875" />
                  <Point X="-1.750857299805" Y="29.04328125" />
                  <Point X="-1.741094970703" Y="29.04671484375" />
                  <Point X="-1.647337890625" Y="29.08555078125" />
                  <Point X="-1.632577636719" Y="29.092275390625" />
                  <Point X="-1.614434448242" Y="29.10223046875" />
                  <Point X="-1.605635009766" Y="29.107703125" />
                  <Point X="-1.587907226562" Y="29.120119140625" />
                  <Point X="-1.579760131836" Y="29.126515625" />
                  <Point X="-1.564213867188" Y="29.14015234375" />
                  <Point X="-1.550247680664" Y="29.15539453125" />
                  <Point X="-1.538020019531" Y="29.1720703125" />
                  <Point X="-1.532359130859" Y="29.18074609375" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.51685546875" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.474360839844" Y="29.334140625" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465990966797" Y="29.37005078125" />
                  <Point X="-1.464526733398" Y="29.380306640625" />
                  <Point X="-1.462640625" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462753173828" Y="29.4328984375" />
                  <Point X="-1.465885009766" Y="29.461013671875" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-0.954886657715" Y="29.709671875" />
                  <Point X="-0.931166748047" Y="29.7163203125" />
                  <Point X="-0.365222503662" Y="29.78255859375" />
                  <Point X="-0.225666183472" Y="29.2617265625" />
                  <Point X="-0.22043510437" Y="29.247107421875" />
                  <Point X="-0.207661712646" Y="29.218916015625" />
                  <Point X="-0.200119384766" Y="29.20534375" />
                  <Point X="-0.182261154175" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896308899" Y="29.1250234375" />
                  <Point X="-0.099600227356" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054915859222" Y="29.09392578125" />
                  <Point X="-0.03985351944" Y="29.090154296875" />
                  <Point X="-0.00931760788" Y="29.08511328125" />
                  <Point X="0.021629692078" Y="29.08511328125" />
                  <Point X="0.052165603638" Y="29.090154296875" />
                  <Point X="0.067227790833" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912307739" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572799683" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.219973495483" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978408813" Y="29.2617265625" />
                  <Point X="0.248532867432" Y="29.3011171875" />
                  <Point X="0.378190856934" Y="29.7850078125" />
                  <Point X="0.807160583496" Y="29.74008203125" />
                  <Point X="0.827852600098" Y="29.737916015625" />
                  <Point X="1.430837524414" Y="29.5923359375" />
                  <Point X="1.453619506836" Y="29.586833984375" />
                  <Point X="1.845016235352" Y="29.444873046875" />
                  <Point X="1.858244750977" Y="29.44007421875" />
                  <Point X="2.237758789062" Y="29.26258984375" />
                  <Point X="2.250431884766" Y="29.256662109375" />
                  <Point X="2.617114746094" Y="29.043033203125" />
                  <Point X="2.629436035156" Y="29.03585546875" />
                  <Point X="2.817779296875" Y="28.901916015625" />
                  <Point X="2.065308105469" Y="27.59859765625" />
                  <Point X="2.062370605469" Y="27.5931015625" />
                  <Point X="2.053182128906" Y="27.573439453125" />
                  <Point X="2.044182373047" Y="27.549564453125" />
                  <Point X="2.041301635742" Y="27.54059765625" />
                  <Point X="2.021004516602" Y="27.4646953125" />
                  <Point X="2.019389648438" Y="27.457568359375" />
                  <Point X="2.015649902344" Y="27.43598046875" />
                  <Point X="2.013139160156" Y="27.412693359375" />
                  <Point X="2.01259362793" Y="27.40191015625" />
                  <Point X="2.012729614258" Y="27.380353515625" />
                  <Point X="2.013410888672" Y="27.369580078125" />
                  <Point X="2.021325317383" Y="27.303947265625" />
                  <Point X="2.023799926758" Y="27.289037109375" />
                  <Point X="2.029142089844" Y="27.267115234375" />
                  <Point X="2.032466674805" Y="27.2563125" />
                  <Point X="2.040732788086" Y="27.23422265625" />
                  <Point X="2.045318115234" Y="27.223888671875" />
                  <Point X="2.055680908203" Y="27.20384375" />
                  <Point X="2.061458740234" Y="27.1941328125" />
                  <Point X="2.102070068359" Y="27.13428125" />
                  <Point X="2.106431396484" Y="27.128359375" />
                  <Point X="2.120417724609" Y="27.11129296875" />
                  <Point X="2.136064453125" Y="27.09453515625" />
                  <Point X="2.143728271484" Y="27.0871953125" />
                  <Point X="2.159824951172" Y="27.07341796875" />
                  <Point X="2.1682578125" Y="27.06698046875" />
                  <Point X="2.228085693359" Y="27.026384765625" />
                  <Point X="2.241225830078" Y="27.01827734375" />
                  <Point X="2.261287841797" Y="27.007900390625" />
                  <Point X="2.271629394531" Y="27.003310546875" />
                  <Point X="2.293731933594" Y="26.995037109375" />
                  <Point X="2.304537841797" Y="26.9917109375" />
                  <Point X="2.32646875" Y="26.986365234375" />
                  <Point X="2.33759375" Y="26.984345703125" />
                  <Point X="2.403226806641" Y="26.976431640625" />
                  <Point X="2.410745849609" Y="26.975826171875" />
                  <Point X="2.433342529297" Y="26.975208984375" />
                  <Point X="2.456034423828" Y="26.97608984375" />
                  <Point X="2.466546630859" Y="26.977083984375" />
                  <Point X="2.487403320312" Y="26.980236328125" />
                  <Point X="2.497747802734" Y="26.98239453125" />
                  <Point X="2.573649169922" Y="27.00269140625" />
                  <Point X="2.594708007812" Y="27.009701171875" />
                  <Point X="2.625318603516" Y="27.022556640625" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="2.717544921875" Y="27.074935546875" />
                  <Point X="3.940403076172" Y="27.780951171875" />
                  <Point X="4.036643798828" Y="27.64719921875" />
                  <Point X="4.043955810547" Y="27.637037109375" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.172953125" Y="26.7438203125" />
                  <Point X="3.168142822266" Y="26.73987109375" />
                  <Point X="3.152114013672" Y="26.7252109375" />
                  <Point X="3.134666748047" Y="26.706599609375" />
                  <Point X="3.128577148438" Y="26.699421875" />
                  <Point X="3.073950927734" Y="26.628158203125" />
                  <Point X="3.069836914062" Y="26.622353515625" />
                  <Point X="3.058390869141" Y="26.604361328125" />
                  <Point X="3.046765380859" Y="26.58329296875" />
                  <Point X="3.042021240234" Y="26.573380859375" />
                  <Point X="3.033708740234" Y="26.5530703125" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.009791992188" Y="26.46991015625" />
                  <Point X="3.006225585938" Y="26.454662109375" />
                  <Point X="3.002772216797" Y="26.432365234375" />
                  <Point X="3.001709716797" Y="26.42111328125" />
                  <Point X="3.000893066406" Y="26.39754296875" />
                  <Point X="3.001174560547" Y="26.3862421875" />
                  <Point X="3.003077636719" Y="26.363755859375" />
                  <Point X="3.00469921875" Y="26.3525703125" />
                  <Point X="3.021403076172" Y="26.271615234375" />
                  <Point X="3.023116210938" Y="26.264650390625" />
                  <Point X="3.029295166016" Y="26.244064453125" />
                  <Point X="3.037457519531" Y="26.222009765625" />
                  <Point X="3.041774658203" Y="26.212111328125" />
                  <Point X="3.051505126953" Y="26.192869140625" />
                  <Point X="3.056918457031" Y="26.183525390625" />
                  <Point X="3.102351074219" Y="26.114470703125" />
                  <Point X="3.111738037109" Y="26.10142578125" />
                  <Point X="3.1262890625" Y="26.08418359375" />
                  <Point X="3.134078125" Y="26.07599609375" />
                  <Point X="3.151323242188" Y="26.059904296875" />
                  <Point X="3.160031982422" Y="26.052697265625" />
                  <Point X="3.178244384766" Y="26.039369140625" />
                  <Point X="3.187748046875" Y="26.033248046875" />
                  <Point X="3.253586425781" Y="25.9961875" />
                  <Point X="3.260132080078" Y="25.992830078125" />
                  <Point X="3.280276367188" Y="25.98377734375" />
                  <Point X="3.302055175781" Y="25.975638671875" />
                  <Point X="3.312314697266" Y="25.972453125" />
                  <Point X="3.333123779297" Y="25.96726171875" />
                  <Point X="3.343673339844" Y="25.965255859375" />
                  <Point X="3.432691162109" Y="25.9534921875" />
                  <Point X="3.454195068359" Y="25.951921875" />
                  <Point X="3.488376708984" Y="25.951984375" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.578032714844" Y="25.962990234375" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.749544921875" Y="25.927123046875" />
                  <Point X="4.752685058594" Y="25.9142265625" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="3.691994140625" Y="25.421353515625" />
                  <Point X="3.686036376953" Y="25.419544921875" />
                  <Point X="3.665628173828" Y="25.412138671875" />
                  <Point X="3.642387939453" Y="25.401623046875" />
                  <Point X="3.634010009766" Y="25.3973203125" />
                  <Point X="3.546552734375" Y="25.34676953125" />
                  <Point X="3.540685791016" Y="25.3430859375" />
                  <Point X="3.523659179688" Y="25.33120703125" />
                  <Point X="3.504288574219" Y="25.31586328125" />
                  <Point X="3.495948730469" Y="25.30841796875" />
                  <Point X="3.480201904297" Y="25.292599609375" />
                  <Point X="3.472794921875" Y="25.2842265625" />
                  <Point X="3.420320556641" Y="25.217361328125" />
                  <Point X="3.410850585938" Y="25.204203125" />
                  <Point X="3.399125732422" Y="25.184919921875" />
                  <Point X="3.393839355469" Y="25.174931640625" />
                  <Point X="3.384067382812" Y="25.153470703125" />
                  <Point X="3.380004638672" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.1214296875" />
                  <Point X="3.370376464844" Y="25.110474609375" />
                  <Point X="3.352885009766" Y="25.019140625" />
                  <Point X="3.351822753906" Y="25.01222265625" />
                  <Point X="3.349660888672" Y="24.991359375" />
                  <Point X="3.348650878906" Y="24.967267578125" />
                  <Point X="3.348824951172" Y="24.956298828125" />
                  <Point X="3.350437011719" Y="24.934451171875" />
                  <Point X="3.351875" Y="24.923572265625" />
                  <Point X="3.369366455078" Y="24.83223828125" />
                  <Point X="3.373159423828" Y="24.816009765625" />
                  <Point X="3.380005126953" Y="24.794509765625" />
                  <Point X="3.384068603516" Y="24.783962890625" />
                  <Point X="3.39383984375" Y="24.76250390625" />
                  <Point X="3.399127685547" Y="24.752513671875" />
                  <Point X="3.410852294922" Y="24.733232421875" />
                  <Point X="3.4172890625" Y="24.72394140625" />
                  <Point X="3.469763427734" Y="24.657076171875" />
                  <Point X="3.474416748047" Y="24.651587890625" />
                  <Point X="3.489182861328" Y="24.635857421875" />
                  <Point X="3.506528564453" Y="24.619572265625" />
                  <Point X="3.514895996094" Y="24.612576171875" />
                  <Point X="3.532379882812" Y="24.5995859375" />
                  <Point X="3.541496337891" Y="24.593591796875" />
                  <Point X="3.628953613281" Y="24.543041015625" />
                  <Point X="3.647685546875" Y="24.53359765625" />
                  <Point X="3.680134277344" Y="24.5201171875" />
                  <Point X="3.691994384766" Y="24.516083984375" />
                  <Point X="3.763001464844" Y="24.49705859375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.763366210938" Y="24.080572265625" />
                  <Point X="4.761612304688" Y="24.0689375" />
                  <Point X="4.727803222656" Y="23.92078125" />
                  <Point X="3.436782714844" Y="24.09074609375" />
                  <Point X="3.428623535156" Y="24.09146484375" />
                  <Point X="3.400097412109" Y="24.09195703125" />
                  <Point X="3.366719970703" Y="24.089158203125" />
                  <Point X="3.35448046875" Y="24.087322265625" />
                  <Point X="3.182832763672" Y="24.050013671875" />
                  <Point X="3.157885742188" Y="24.043259765625" />
                  <Point X="3.128772460938" Y="24.031640625" />
                  <Point X="3.114695068359" Y="24.02462109375" />
                  <Point X="3.086862792969" Y="24.007728515625" />
                  <Point X="3.074130615234" Y="23.998474609375" />
                  <Point X="3.050373779297" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.935598876953" Y="23.84199609375" />
                  <Point X="2.921325927734" Y="23.82315234375" />
                  <Point X="2.906604736328" Y="23.79876953125" />
                  <Point X="2.900162597656" Y="23.786021484375" />
                  <Point X="2.888819824219" Y="23.75863671875" />
                  <Point X="2.884362304688" Y="23.7450703125" />
                  <Point X="2.877531005859" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.860287353516" Y="23.541744140625" />
                  <Point X="2.859288818359" Y="23.51667578125" />
                  <Point X="2.861608398438" Y="23.485404296875" />
                  <Point X="2.864067382812" Y="23.469865234375" />
                  <Point X="2.871800537109" Y="23.4382421875" />
                  <Point X="2.8767890625" Y="23.42332421875" />
                  <Point X="2.889159667969" Y="23.394515625" />
                  <Point X="2.896541748047" Y="23.380625" />
                  <Point X="2.991533935547" Y="23.23287109375" />
                  <Point X="3.003593017578" Y="23.2161796875" />
                  <Point X="3.020699462891" Y="23.195017578125" />
                  <Point X="3.028258300781" Y="23.186720703125" />
                  <Point X="3.044306884766" Y="23.171072265625" />
                  <Point X="3.052796630859" Y="23.163720703125" />
                  <Point X="3.118691650391" Y="23.113158203125" />
                  <Point X="4.087171142578" Y="22.370015625" />
                  <Point X="4.050427978516" Y="22.310560546875" />
                  <Point X="4.045501708984" Y="22.302587890625" />
                  <Point X="4.001273925781" Y="22.23974609375" />
                  <Point X="2.848454101562" Y="22.905328125" />
                  <Point X="2.841194335938" Y="22.90911328125" />
                  <Point X="2.815030273438" Y="22.920484375" />
                  <Point X="2.783117919922" Y="22.930673828125" />
                  <Point X="2.771105957031" Y="22.9336640625" />
                  <Point X="2.566817871094" Y="22.97055859375" />
                  <Point X="2.539350585938" Y="22.974193359375" />
                  <Point X="2.507993408203" Y="22.974595703125" />
                  <Point X="2.492301269531" Y="22.973494140625" />
                  <Point X="2.460131347656" Y="22.96853515625" />
                  <Point X="2.444839599609" Y="22.964861328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.40058984375" Y="22.948890625" />
                  <Point X="2.230876708984" Y="22.859572265625" />
                  <Point X="2.208971435547" Y="22.84683203125" />
                  <Point X="2.186042480469" Y="22.82994140625" />
                  <Point X="2.175213867187" Y="22.820630859375" />
                  <Point X="2.154253662109" Y="22.799671875" />
                  <Point X="2.144940673828" Y="22.78883984375" />
                  <Point X="2.128045898438" Y="22.76590625" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.031145019531" Y="22.584091796875" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.039081787109" Y="22.2155703125" />
                  <Point X="2.04346875" Y="22.19678515625" />
                  <Point X="2.051503173828" Y="22.168869140625" />
                  <Point X="2.055368164062" Y="22.15798046875" />
                  <Point X="2.064390136719" Y="22.136755859375" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.111891357422" Y="22.053078125" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723754394531" Y="20.963916015625" />
                  <Point X="1.833915161133" Y="22.123576171875" />
                  <Point X="1.828654541016" Y="22.1298515625" />
                  <Point X="1.808831787109" Y="22.150373046875" />
                  <Point X="1.783248779297" Y="22.171998046875" />
                  <Point X="1.773295776367" Y="22.17935546875" />
                  <Point X="1.571812744141" Y="22.308890625" />
                  <Point X="1.546277709961" Y="22.323755859375" />
                  <Point X="1.517460205078" Y="22.33612890625" />
                  <Point X="1.502537231445" Y="22.341119140625" />
                  <Point X="1.470919555664" Y="22.34884765625" />
                  <Point X="1.455381835938" Y="22.3513046875" />
                  <Point X="1.424119262695" Y="22.35362109375" />
                  <Point X="1.408393676758" Y="22.35348046875" />
                  <Point X="1.188111572266" Y="22.333208984375" />
                  <Point X="1.161269897461" Y="22.329669921875" />
                  <Point X="1.133594970703" Y="22.3228359375" />
                  <Point X="1.120016113281" Y="22.318375" />
                  <Point X="1.092624389648" Y="22.307029296875" />
                  <Point X="1.079874511719" Y="22.3005859375" />
                  <Point X="1.055490966797" Y="22.28586328125" />
                  <Point X="1.043857421875" Y="22.277583984375" />
                  <Point X="0.873704101562" Y="22.13610546875" />
                  <Point X="0.852653015137" Y="22.116908203125" />
                  <Point X="0.83218347168" Y="22.09316015625" />
                  <Point X="0.822934814453" Y="22.080435546875" />
                  <Point X="0.80604309082" Y="22.052611328125" />
                  <Point X="0.799020629883" Y="22.03853515625" />
                  <Point X="0.787395202637" Y="22.0094140625" />
                  <Point X="0.782792053223" Y="21.994369140625" />
                  <Point X="0.731916992188" Y="21.7603046875" />
                  <Point X="0.728915405273" Y="21.74241796875" />
                  <Point X="0.725262817383" Y="21.71175" />
                  <Point X="0.724598022461" Y="21.6999296875" />
                  <Point X="0.72474395752" Y="21.676294921875" />
                  <Point X="0.725554870605" Y="21.66448046875" />
                  <Point X="0.737554992676" Y="21.573330078125" />
                  <Point X="0.833091125488" Y="20.847662109375" />
                  <Point X="0.655065124512" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642143859863" Y="21.546423828125" />
                  <Point X="0.62678704834" Y="21.576185546875" />
                  <Point X="0.620407714844" Y="21.586791015625" />
                  <Point X="0.465622619629" Y="21.809806640625" />
                  <Point X="0.446668884277" Y="21.834830078125" />
                  <Point X="0.424785675049" Y="21.857283203125" />
                  <Point X="0.412913970947" Y="21.86759765625" />
                  <Point X="0.386658996582" Y="21.886841796875" />
                  <Point X="0.373245178223" Y="21.895060546875" />
                  <Point X="0.345242980957" Y="21.909169921875" />
                  <Point X="0.330654602051" Y="21.915060546875" />
                  <Point X="0.091133636475" Y="21.9893984375" />
                  <Point X="0.063377716064" Y="21.996888671875" />
                  <Point X="0.03521843338" Y="22.001158203125" />
                  <Point X="0.020977237701" Y="22.002232421875" />
                  <Point X="-0.008665153503" Y="22.002232421875" />
                  <Point X="-0.022906497955" Y="22.001158203125" />
                  <Point X="-0.051065631866" Y="21.996888671875" />
                  <Point X="-0.064983718872" Y="21.993693359375" />
                  <Point X="-0.304504699707" Y="21.91935546875" />
                  <Point X="-0.332927337646" Y="21.909169921875" />
                  <Point X="-0.36092477417" Y="21.895064453125" />
                  <Point X="-0.374337371826" Y="21.886849609375" />
                  <Point X="-0.400590576172" Y="21.867609375" />
                  <Point X="-0.412468658447" Y="21.8572890625" />
                  <Point X="-0.434356506348" Y="21.834830078125" />
                  <Point X="-0.444366577148" Y="21.82269140625" />
                  <Point X="-0.599151672363" Y="21.599673828125" />
                  <Point X="-0.608264343262" Y="21.585193359375" />
                  <Point X="-0.624428833008" Y="21.556740234375" />
                  <Point X="-0.629721740723" Y="21.545865234375" />
                  <Point X="-0.638883911133" Y="21.52352734375" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.664632263184" Y="21.43041015625" />
                  <Point X="-0.985425048828" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.88413034447" Y="27.832194577128" />
                  <Point X="-4.150105096226" Y="27.234805503753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.562362816527" Y="26.308859503631" />
                  <Point X="-4.737576430865" Y="25.915323282539" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.697302608985" Y="28.018250174532" />
                  <Point X="-4.072594958077" Y="27.175329757507" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.464130310822" Y="26.295926956959" />
                  <Point X="-4.778450684236" Y="25.589951839482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.614576985568" Y="27.970488599993" />
                  <Point X="-3.995084819928" Y="27.11585401126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.365897805116" Y="26.282994410286" />
                  <Point X="-4.685543875827" Y="25.565057580836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53185136215" Y="27.922727025453" />
                  <Point X="-3.917574681779" Y="27.056378265013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.267665299411" Y="26.270061863614" />
                  <Point X="-4.592637067418" Y="25.54016332219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.449125738732" Y="27.874965450914" />
                  <Point X="-3.84006454363" Y="26.996902518766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.169432793706" Y="26.257129316941" />
                  <Point X="-4.499730259008" Y="25.515269063544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.935585241428" Y="28.79482992587" />
                  <Point X="-3.013348317702" Y="28.620171196906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.366400115315" Y="27.827203876374" />
                  <Point X="-3.762554405482" Y="26.93742677252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.071200288" Y="26.244196770269" />
                  <Point X="-4.406823450599" Y="25.490374804898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.777700206436" Y="28.915879153631" />
                  <Point X="-2.954634973326" Y="28.518477160614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.283674491897" Y="27.779442301835" />
                  <Point X="-3.685044267333" Y="26.877951026273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.972967782295" Y="26.231264223596" />
                  <Point X="-4.31391664219" Y="25.465480546252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.623380156271" Y="29.028921294372" />
                  <Point X="-2.89592162895" Y="28.416783124321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.197350178473" Y="27.739763517388" />
                  <Point X="-3.607534129184" Y="26.818475280026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.874735276589" Y="26.218331676924" />
                  <Point X="-4.22100983378" Y="25.440586287606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.69810330424" Y="24.369016808364" />
                  <Point X="-4.767534254322" Y="24.213072341232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.48521293257" Y="29.105683592874" />
                  <Point X="-2.837208284574" Y="28.315089088029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.099362210798" Y="27.726281729306" />
                  <Point X="-3.530626483874" Y="26.757646312707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.776465482101" Y="26.205482882229" />
                  <Point X="-4.128103025371" Y="25.41569202896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.580026407582" Y="24.400655493527" />
                  <Point X="-4.7422507744" Y="24.03629360003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.347045708868" Y="29.182445891376" />
                  <Point X="-2.779264465639" Y="28.211666669297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.993717502054" Y="27.729997263234" />
                  <Point X="-3.466239434868" Y="26.668695625657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.667378975416" Y="26.216928820901" />
                  <Point X="-4.035196216961" Y="25.390797770314" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.461949510924" Y="24.432294178689" />
                  <Point X="-4.70508411624" Y="23.886204914141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.208878485167" Y="29.259208189877" />
                  <Point X="-2.748953407772" Y="28.046180053043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.847810059615" Y="27.82414437766" />
                  <Point X="-3.421572091609" Y="26.53545375433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53770310118" Y="26.274619236245" />
                  <Point X="-3.942289408552" Y="25.365903511668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.343872614266" Y="24.463932863852" />
                  <Point X="-4.667173511572" Y="23.737787159467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.134505658554" Y="29.192685926552" />
                  <Point X="-3.849382600143" Y="25.341009253022" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.225795717608" Y="24.495571549014" />
                  <Point X="-4.568675538505" Y="23.72545086225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.062734156965" Y="29.120320991557" />
                  <Point X="-3.756475791733" Y="25.316114994376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.10771882095" Y="24.527210234177" />
                  <Point X="-4.458210135282" Y="23.739993853254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.979332991374" Y="29.074076709582" />
                  <Point X="-3.663568983324" Y="25.29122073573" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.989641924292" Y="24.558848919339" />
                  <Point X="-4.347744732058" Y="23.754536844259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.891854992096" Y="29.036989145988" />
                  <Point X="-3.570662187714" Y="25.266326448335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.871565027635" Y="24.590487604502" />
                  <Point X="-4.237279328835" Y="23.769079835264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.789361411186" Y="29.033627130922" />
                  <Point X="-3.479196481732" Y="25.238195420642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.753488130977" Y="24.622126289665" />
                  <Point X="-4.126813925611" Y="23.783622826268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.44563481896" Y="29.57208333035" />
                  <Point X="-1.472549087637" Y="29.51163289316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.66562575094" Y="29.077975607198" />
                  <Point X="-3.397266726369" Y="25.188646297187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.635411234319" Y="24.653764974827" />
                  <Point X="-4.016348522388" Y="23.798165817273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.326812157626" Y="29.605397030402" />
                  <Point X="-3.330173369101" Y="25.105774078014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.517334337661" Y="24.68540365999" />
                  <Point X="-3.905883119164" Y="23.812708808277" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.207989496291" Y="29.638710730453" />
                  <Point X="-3.296025397296" Y="24.948905311564" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.376234617262" Y="24.768752453914" />
                  <Point X="-3.795417715941" Y="23.827251799282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.089166834956" Y="29.672024430504" />
                  <Point X="-3.684952312717" Y="23.841794790286" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.970344173622" Y="29.705338130555" />
                  <Point X="-3.574486909494" Y="23.856337781291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.138570458379" Y="22.589385386941" />
                  <Point X="-4.165171296996" Y="22.52963892519" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.857632286239" Y="29.724926807594" />
                  <Point X="-3.46402150627" Y="23.870880772295" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.980617691026" Y="22.710586744076" />
                  <Point X="-4.105631828214" Y="22.429800394693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.747925027828" Y="29.737766977469" />
                  <Point X="-3.353556103047" Y="23.8854237633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.822664923673" Y="22.831788101211" />
                  <Point X="-4.046092359432" Y="22.329961864196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.638217769417" Y="29.750607147344" />
                  <Point X="-3.243138332316" Y="23.899859769974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.66471215632" Y="22.952989458345" />
                  <Point X="-3.984771884129" Y="22.234123539841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.528510511007" Y="29.76344731722" />
                  <Point X="-3.143807138707" Y="23.889394916737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.506759388968" Y="23.07419081548" />
                  <Point X="-3.919160288621" Y="22.147923229267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.418803252596" Y="29.776287487095" />
                  <Point X="-3.058074795105" Y="23.848386546301" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.348806621615" Y="23.195392172615" />
                  <Point X="-3.853548693113" Y="22.061722918692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.345233998732" Y="29.707960369821" />
                  <Point X="-2.986509802495" Y="23.775557784546" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.190853854262" Y="23.31659352975" />
                  <Point X="-3.782100176325" Y="21.988632547961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.306163618336" Y="29.562147514082" />
                  <Point X="-2.949308509683" Y="23.62554688936" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.030574007354" Y="23.443021593142" />
                  <Point X="-3.64212992874" Y="22.06944450441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.26709323794" Y="29.416334658343" />
                  <Point X="-3.502159681154" Y="22.150256460859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.228022857544" Y="29.270521802603" />
                  <Point X="-3.362189433569" Y="22.231068417309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.171088582445" Y="29.164831911292" />
                  <Point X="-3.222219185984" Y="22.311880373758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.092657773575" Y="29.107424025341" />
                  <Point X="-3.082248938399" Y="22.392692330207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.001399289623" Y="29.08511328125" />
                  <Point X="-2.942278690814" Y="22.473504286656" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.415273736309" Y="29.781124141407" />
                  <Point X="0.319528563311" Y="29.566076961927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.118146713931" Y="29.113765922625" />
                  <Point X="-2.802308443229" Y="22.554316243105" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.514631282117" Y="29.770718476176" />
                  <Point X="-2.664229124106" Y="22.630881104691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.613988827925" Y="29.760312810946" />
                  <Point X="-2.550864550313" Y="22.651935739408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.713346373733" Y="29.749907145716" />
                  <Point X="-2.458976959664" Y="22.624752280191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.812704039061" Y="29.739501748931" />
                  <Point X="-2.380806069592" Y="22.566760607063" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.907434053021" Y="29.718702477" />
                  <Point X="-2.321768045766" Y="22.465795812756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.624807984029" Y="21.785156967454" />
                  <Point X="-2.920184303799" Y="21.121730891111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.001331244728" Y="29.69603265566" />
                  <Point X="-2.842741906407" Y="21.062102996634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.095228436435" Y="29.673362834321" />
                  <Point X="-2.765299509014" Y="21.002475102158" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.189125628142" Y="29.650693012982" />
                  <Point X="-2.684714315118" Y="20.9499060442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.283022819849" Y="29.628023191643" />
                  <Point X="-2.603196503267" Y="20.899431680466" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.376920011556" Y="29.605353370304" />
                  <Point X="-2.521678691416" Y="20.848957316733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.470017112808" Y="29.580886516379" />
                  <Point X="-2.341786437672" Y="21.019435567102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.559549357594" Y="29.548412863739" />
                  <Point X="-2.193665298906" Y="21.118554724884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.64908160238" Y="29.515939211099" />
                  <Point X="-2.050977888429" Y="21.205469529109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.738613847166" Y="29.483465558459" />
                  <Point X="-1.939064183168" Y="21.223265459751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.828146091952" Y="29.450991905819" />
                  <Point X="-1.837860005254" Y="21.217007398139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.915379306258" Y="29.413354546176" />
                  <Point X="-1.736818194372" Y="21.2103846542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.001448677985" Y="29.373103153303" />
                  <Point X="-1.635812358904" Y="21.203681108161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.087518049713" Y="29.33285176043" />
                  <Point X="-1.544113867557" Y="21.176072924958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.17358742144" Y="29.292600367558" />
                  <Point X="-1.465346157991" Y="21.11942173036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.259281600356" Y="29.251506277833" />
                  <Point X="-1.390557276552" Y="21.053833941472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.341853702969" Y="29.203399889921" />
                  <Point X="-1.315768395113" Y="20.988246152584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.424425805582" Y="29.155293502008" />
                  <Point X="-1.242376988052" Y="20.919519584851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506997908195" Y="29.107187114096" />
                  <Point X="-1.185985298865" Y="20.812611025629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.589570010807" Y="29.059080726183" />
                  <Point X="-1.153231426001" Y="20.652611061691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.67028591984" Y="29.006805259229" />
                  <Point X="-1.121700644737" Y="20.489863989038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.749268664019" Y="28.950637040281" />
                  <Point X="2.423608148977" Y="28.219191547688" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02021492506" Y="27.313155532425" />
                  <Point X="-0.406637686175" Y="21.862355322745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.763904664468" Y="21.059920551398" />
                  <Point X="-1.118766462078" Y="20.262887904313" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.067348441212" Y="27.185452776106" />
                  <Point X="-0.272902269357" Y="21.929163620014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.921081128325" Y="20.473330066703" />
                  <Point X="-1.02460985286" Y="20.240800744242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.132540251846" Y="27.098309613268" />
                  <Point X="-0.152238297905" Y="21.9666129703" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.209978106527" Y="27.038671515695" />
                  <Point X="-0.033588419408" Y="21.999538593744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.294443716573" Y="26.994818015109" />
                  <Point X="0.068595312817" Y="21.995480647137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.390909297848" Y="26.97791689119" />
                  <Point X="0.160317813201" Y="21.967926389113" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.496805817559" Y="26.982198001809" />
                  <Point X="0.251683248645" Y="21.939570150107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.617257879194" Y="27.019171394855" />
                  <Point X="0.342611690592" Y="21.910232407632" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.755947626346" Y="27.097107300241" />
                  <Point X="0.423518001955" Y="21.858384591316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89591768865" Y="27.177918840543" />
                  <Point X="0.490147626423" Y="21.774470811224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.035887750954" Y="27.258730380844" />
                  <Point X="0.55349891252" Y="21.683193762592" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.175857813259" Y="27.339541921146" />
                  <Point X="0.848388876378" Y="22.111961098793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.770161782024" Y="21.936260168156" />
                  <Point X="0.616850198617" Y="21.59191671396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.315827875563" Y="27.420353461447" />
                  <Point X="1.015692984983" Y="22.254165912265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.731852018586" Y="21.616648663797" />
                  <Point X="0.666420254304" Y="21.469686515037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.455797937867" Y="27.501165001749" />
                  <Point X="3.048679092959" Y="26.586761104735" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034047396445" Y="26.553897776301" />
                  <Point X="1.152315502392" Y="22.32745874363" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.755584118003" Y="21.436385464929" />
                  <Point X="0.705490660344" Y="21.323873716895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.595768000172" Y="27.58197654205" />
                  <Point X="3.248357731046" Y="26.801680301962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018538572822" Y="26.285498021244" />
                  <Point X="1.261888943492" Y="22.339998354903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779316243509" Y="21.256122324658" />
                  <Point X="0.744561066384" Y="21.178060918753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.735738062476" Y="27.662788082352" />
                  <Point X="3.406310542053" Y="26.922881757147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.068971412044" Y="26.165205665869" />
                  <Point X="1.370322138417" Y="22.349976931336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.803048369015" Y="21.075859184388" />
                  <Point X="0.783631472424" Y="21.032248120612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.87570812478" Y="27.743599622653" />
                  <Point X="3.564263353061" Y="27.044083212332" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.133509375054" Y="26.076593937224" />
                  <Point X="1.473526125761" Y="22.348210515245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.826780494521" Y="20.895596044118" />
                  <Point X="0.822701878465" Y="20.88643532247" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974951353689" Y="27.732937197463" />
                  <Point X="3.722216164068" Y="27.165284667516" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.212098320635" Y="26.01954123214" />
                  <Point X="1.562439644156" Y="22.314347180378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.039192024087" Y="27.643657738678" />
                  <Point X="3.880168975076" Y="27.286486122701" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.297328512711" Y="25.977405010912" />
                  <Point X="1.643485089522" Y="22.262811864148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.099424015209" Y="27.545374638824" />
                  <Point X="4.038121786083" Y="27.407687577886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.393007163867" Y="25.958736413006" />
                  <Point X="1.724333407452" Y="22.210833792447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.494162607428" Y="25.952368892244" />
                  <Point X="1.803435767615" Y="22.154934235396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.604429235346" Y="25.966465426602" />
                  <Point X="2.217752898866" Y="22.851939381367" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.008880066093" Y="22.382803317888" />
                  <Point X="1.8715991917" Y="22.074465425647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714894712932" Y="25.981008584629" />
                  <Point X="2.353989436185" Y="22.924365287255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.038888960608" Y="22.216638031633" />
                  <Point X="1.937406231179" Y="21.988704089419" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.825360190519" Y="25.995551742656" />
                  <Point X="3.532240369128" Y="25.337193844651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.351007200739" Y="24.9301374838" />
                  <Point X="2.478936186733" Y="22.971433916885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.088271985922" Y="22.093987755617" />
                  <Point X="2.003213270657" Y="21.902942753191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.935825668106" Y="26.010094900683" />
                  <Point X="3.670363520681" Y="25.413857155487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.38702540964" Y="24.77746933864" />
                  <Point X="3.010787001501" Y="23.932424038206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.866394581204" Y="23.608113352346" />
                  <Point X="2.581367037235" Y="22.967931007014" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.146985306472" Y="21.992293665811" />
                  <Point X="2.069020310136" Y="21.817181416962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.046291145692" Y="26.02463805871" />
                  <Point X="3.78930014839" Y="25.447426828207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.449123200247" Y="24.683376893043" />
                  <Point X="3.16497978614" Y="24.045180335895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.8823479228" Y="23.410378777357" />
                  <Point X="2.677618079375" Y="22.950548020308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205698553742" Y="21.890599411414" />
                  <Point X="2.134827349615" Y="21.731420080734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.156756623279" Y="26.039181216737" />
                  <Point X="3.907377157741" Y="25.479065766483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51992691846" Y="24.608838280998" />
                  <Point X="3.280581618212" Y="24.071259934981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.941769721149" Y="23.310275954742" />
                  <Point X="2.773793438256" Y="22.932995046219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.264411801011" Y="21.788905157017" />
                  <Point X="2.200634389094" Y="21.645658744506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.267222100866" Y="26.053724374764" />
                  <Point X="4.025454167093" Y="25.510704704759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.6016491828" Y="24.558823125075" />
                  <Point X="3.393542275133" Y="24.091407357549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003768923732" Y="23.215962076816" />
                  <Point X="2.861987080107" Y="22.897514842162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.323125048281" Y="21.68721090262" />
                  <Point X="2.266441428573" Y="21.559897408278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.377687578453" Y="26.068267532792" />
                  <Point X="4.143531176444" Y="25.542343643035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.687318845079" Y="24.517673970081" />
                  <Point X="3.493890908681" Y="24.083227711831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.076427083272" Y="23.145588608187" />
                  <Point X="2.944712642537" Y="22.849753130642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.38183829555" Y="21.585516648223" />
                  <Point X="2.332248468052" Y="21.47413607205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.488153056039" Y="26.082810690819" />
                  <Point X="4.261608185795" Y="25.573982581311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.780091872963" Y="24.492479235455" />
                  <Point X="3.592123450381" Y="24.070295246003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.153937349496" Y="23.086113149603" />
                  <Point X="3.027438204967" Y="22.801991419122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44055154282" Y="21.483822393826" />
                  <Point X="2.398055507531" Y="21.388374735822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.598618533626" Y="26.097353848846" />
                  <Point X="4.379685195147" Y="25.605621519587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872998669185" Y="24.467584949435" />
                  <Point X="3.69035599208" Y="24.057362780174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.231447472784" Y="23.026637369976" />
                  <Point X="3.110163767397" Y="22.754229707602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.49926479009" Y="21.382128139429" />
                  <Point X="2.46386254701" Y="21.302613399594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.706161299608" Y="26.10533248913" />
                  <Point X="4.497762204498" Y="25.637260457863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.965905465407" Y="24.442690663416" />
                  <Point X="3.78858853378" Y="24.044430314346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.308957596071" Y="22.967161590349" />
                  <Point X="3.192889329827" Y="22.706467996083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.557978037359" Y="21.280433885032" />
                  <Point X="2.529669586489" Y="21.216852063366" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.742921433914" Y="25.954330735714" />
                  <Point X="4.615839213849" Y="25.668899396139" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058812261628" Y="24.417796377396" />
                  <Point X="3.886821075479" Y="24.031497848518" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386467719358" Y="22.907685810722" />
                  <Point X="3.275614892257" Y="22.658706284563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.616691284629" Y="21.178739630636" />
                  <Point X="2.595476625968" Y="21.131090727137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.772472264256" Y="25.787136620482" />
                  <Point X="4.7339162232" Y="25.700538334416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.15171905785" Y="24.392902091376" />
                  <Point X="3.985053617179" Y="24.018565382689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463977842645" Y="22.848210031096" />
                  <Point X="3.358340454687" Y="22.610944573043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.675404531898" Y="21.077045376239" />
                  <Point X="2.661283665447" Y="21.045329390909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.244625854072" Y="24.368007805356" />
                  <Point X="4.083286158878" Y="24.005632916861" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.541487965932" Y="22.788734251469" />
                  <Point X="3.441066017117" Y="22.563182861524" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.734117779168" Y="20.975351121842" />
                  <Point X="2.731357692315" Y="20.969151865271" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.337532650294" Y="24.343113519336" />
                  <Point X="4.181518700578" Y="23.992700451032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.618998089219" Y="22.729258471842" />
                  <Point X="3.523791579547" Y="22.515421150004" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430439446516" Y="24.318219233317" />
                  <Point X="4.279751242277" Y="23.979767985204" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696508212506" Y="22.669782692215" />
                  <Point X="3.606517141978" Y="22.467659438484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523346242737" Y="24.293324947297" />
                  <Point X="4.377983783977" Y="23.966835519375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.774018335794" Y="22.610306912589" />
                  <Point X="3.689242704408" Y="22.419897726965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.616253038959" Y="24.268430661277" />
                  <Point X="4.476216325676" Y="23.953903053547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.851528459081" Y="22.550831132962" />
                  <Point X="3.771968266838" Y="22.372136015445" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.709159835181" Y="24.243536375257" />
                  <Point X="4.574448867376" Y="23.940970587718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.929038582368" Y="22.491355353335" />
                  <Point X="3.854693829268" Y="22.324374303925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.775025745851" Y="24.157907265889" />
                  <Point X="4.672681409075" Y="23.92803812189" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.006548705655" Y="22.431879573708" />
                  <Point X="3.937419391698" Y="22.276612592406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.084058828942" Y="22.372403794082" />
                  <Point X="4.072235033785" Y="22.34584711535" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.850241394043" Y="20.04955078125" />
                  <Point X="0.4715390625" Y="21.462888671875" />
                  <Point X="0.464319000244" Y="21.478455078125" />
                  <Point X="0.309533843994" Y="21.701470703125" />
                  <Point X="0.300591369629" Y="21.71435546875" />
                  <Point X="0.274336242676" Y="21.733599609375" />
                  <Point X="0.034815158844" Y="21.8079375" />
                  <Point X="0.020977140427" Y="21.812232421875" />
                  <Point X="-0.00866515255" Y="21.812232421875" />
                  <Point X="-0.248186248779" Y="21.73789453125" />
                  <Point X="-0.262024261475" Y="21.733599609375" />
                  <Point X="-0.288277404785" Y="21.714359375" />
                  <Point X="-0.443062591553" Y="21.491341796875" />
                  <Point X="-0.459227081299" Y="21.462888671875" />
                  <Point X="-0.481106109619" Y="21.381234375" />
                  <Point X="-0.84774395752" Y="20.012923828125" />
                  <Point X="-1.085347412109" Y="20.05904296875" />
                  <Point X="-1.100251586914" Y="20.061935546875" />
                  <Point X="-1.351589599609" Y="20.1266015625" />
                  <Point X="-1.309150268555" Y="20.4489609375" />
                  <Point X="-1.309683105469" Y="20.465240234375" />
                  <Point X="-1.366904663086" Y="20.752912109375" />
                  <Point X="-1.370210571289" Y="20.769533203125" />
                  <Point X="-1.38628112793" Y="20.797369140625" />
                  <Point X="-1.606801757812" Y="20.990759765625" />
                  <Point X="-1.619541992188" Y="21.00193359375" />
                  <Point X="-1.649243164062" Y="21.014236328125" />
                  <Point X="-1.941923217773" Y="21.033419921875" />
                  <Point X="-1.958832275391" Y="21.03452734375" />
                  <Point X="-1.989877685547" Y="21.026208984375" />
                  <Point X="-2.233754150391" Y="20.863255859375" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.272017333984" Y="20.826697265625" />
                  <Point X="-2.457094726562" Y="20.585498046875" />
                  <Point X="-2.834002197266" Y="20.81887109375" />
                  <Point X="-2.855835693359" Y="20.832388671875" />
                  <Point X="-3.228580810547" Y="21.119390625" />
                  <Point X="-3.209809082031" Y="21.151904296875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762695312" Y="22.402408203125" />
                  <Point X="-2.51398046875" Y="22.431236328125" />
                  <Point X="-2.530381591797" Y="22.44763671875" />
                  <Point X="-2.531332763672" Y="22.448587890625" />
                  <Point X="-2.560160400391" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.662108154297" Y="22.415869140625" />
                  <Point X="-3.842958740234" Y="21.73410546875" />
                  <Point X="-4.144454101563" Y="22.13020703125" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.395165527344" Y="22.631984375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138538330078" Y="23.632107421875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161159912109" Y="23.689361328125" />
                  <Point X="-3.186196044922" Y="23.704095703125" />
                  <Point X="-3.187637207031" Y="23.704943359375" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.30843359375" Y="23.699724609375" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.920646484375" Y="23.96239453125" />
                  <Point X="-4.927392089844" Y="23.988802734375" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.960189453125" Y="24.495494140625" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541894775391" Y="24.87857421875" />
                  <Point X="-3.515657714844" Y="24.89678515625" />
                  <Point X="-3.514166992188" Y="24.8978203125" />
                  <Point X="-3.494898925781" Y="24.924091796875" />
                  <Point X="-3.486155029297" Y="24.952265625" />
                  <Point X="-3.485649414063" Y="24.953892578125" />
                  <Point X="-3.485647949219" Y="24.9835390625" />
                  <Point X="-3.494390136719" Y="25.01170703125" />
                  <Point X="-3.494895263672" Y="25.0133359375" />
                  <Point X="-3.514143798828" Y="25.039603515625" />
                  <Point X="-3.540368652344" Y="25.0578046875" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.638504394531" Y="25.08780078125" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.9219921875" Y="25.967041015625" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.758774414062" Y="26.526357421875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731705566406" Y="26.3958671875" />
                  <Point X="-3.673634521484" Y="26.41417578125" />
                  <Point X="-3.670268798828" Y="26.415236328125" />
                  <Point X="-3.651530517578" Y="26.42605859375" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.615818603516" Y="26.5000390625" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616315917969" Y="26.54551171875" />
                  <Point X="-3.644431396484" Y="26.599521484375" />
                  <Point X="-3.646058837891" Y="26.602646484375" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.706453369141" Y="26.654890625" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.17690625" Y="27.75806640625" />
                  <Point X="-4.160012207031" Y="27.787009765625" />
                  <Point X="-3.774671386719" Y="28.282310546875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514892578" Y="27.92043359375" />
                  <Point X="-3.057637939453" Y="27.913357421875" />
                  <Point X="-3.052959472656" Y="27.91294921875" />
                  <Point X="-3.031504638672" Y="27.915775390625" />
                  <Point X="-3.013253662109" Y="27.92740234375" />
                  <Point X="-2.955846679688" Y="27.98480859375" />
                  <Point X="-2.952530029297" Y="27.988125" />
                  <Point X="-2.940900146484" Y="28.00637890625" />
                  <Point X="-2.93807421875" Y="28.027837890625" />
                  <Point X="-2.945150146484" Y="28.10871484375" />
                  <Point X="-2.945558837891" Y="28.113388671875" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-2.972666259766" Y="28.1697109375" />
                  <Point X="-3.307278320312" Y="28.749275390625" />
                  <Point X="-2.782293701172" Y="29.151775390625" />
                  <Point X="-2.752872314453" Y="29.174333984375" />
                  <Point X="-2.141547851563" Y="29.513970703125" />
                  <Point X="-1.967826904297" Y="29.28757421875" />
                  <Point X="-1.951245605469" Y="29.27366015625" />
                  <Point X="-1.861229980469" Y="29.226798828125" />
                  <Point X="-1.856029541016" Y="29.224091796875" />
                  <Point X="-1.835121826172" Y="29.218490234375" />
                  <Point X="-1.813805664062" Y="29.22225" />
                  <Point X="-1.720048461914" Y="29.2610859375" />
                  <Point X="-1.714631713867" Y="29.263330078125" />
                  <Point X="-1.696904052734" Y="29.27574609375" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.655566894531" Y="29.3912734375" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917724609" Y="29.41842578125" />
                  <Point X="-1.654259521484" Y="29.436212890625" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.006178405762" Y="29.8926171875" />
                  <Point X="-0.968083190918" Y="29.903296875" />
                  <Point X="-0.224463867188" Y="29.990328125" />
                  <Point X="-0.224200134277" Y="29.990359375" />
                  <Point X="-0.042140319824" Y="29.31090234375" />
                  <Point X="-0.024282102585" Y="29.28417578125" />
                  <Point X="0.006155994415" Y="29.27384375" />
                  <Point X="0.036594051361" Y="29.28417578125" />
                  <Point X="0.054452308655" Y="29.31090234375" />
                  <Point X="0.065006896973" Y="29.35029296875" />
                  <Point X="0.236648422241" Y="29.9908671875" />
                  <Point X="0.826950561523" Y="29.929046875" />
                  <Point X="0.860208496094" Y="29.925564453125" />
                  <Point X="1.475428344727" Y="29.77703125" />
                  <Point X="1.508455810547" Y="29.769056640625" />
                  <Point X="1.90980078125" Y="29.623486328125" />
                  <Point X="1.931037719727" Y="29.615783203125" />
                  <Point X="2.318248046875" Y="29.43469921875" />
                  <Point X="2.338689941406" Y="29.425138671875" />
                  <Point X="2.712760253906" Y="29.207205078125" />
                  <Point X="2.732522705078" Y="29.19569140625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="3.044420898438" Y="28.91446875" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852294922" Y="27.491515625" />
                  <Point X="2.204555175781" Y="27.41561328125" />
                  <Point X="2.202044433594" Y="27.392326171875" />
                  <Point X="2.209958740234" Y="27.326693359375" />
                  <Point X="2.210415771484" Y="27.32290234375" />
                  <Point X="2.218681884766" Y="27.3008125" />
                  <Point X="2.259293212891" Y="27.2409609375" />
                  <Point X="2.259297119141" Y="27.240955078125" />
                  <Point X="2.274939941406" Y="27.224203125" />
                  <Point X="2.334779296875" Y="27.183599609375" />
                  <Point X="2.338236816406" Y="27.181251953125" />
                  <Point X="2.360339355469" Y="27.172978515625" />
                  <Point X="2.425972412109" Y="27.165064453125" />
                  <Point X="2.448664306641" Y="27.1659453125" />
                  <Point X="2.524565673828" Y="27.1862421875" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.622544921875" Y="27.239478515625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.190868652344" Y="27.758171875" />
                  <Point X="4.202590332031" Y="27.741880859375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.3610703125" Y="27.416005859375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.224745361328" Y="26.512568359375" />
                  <Point X="3.213119873047" Y="26.4915" />
                  <Point X="3.192771484375" Y="26.41873828125" />
                  <Point X="3.191595947266" Y="26.41453515625" />
                  <Point X="3.190779296875" Y="26.39096484375" />
                  <Point X="3.207483154297" Y="26.310009765625" />
                  <Point X="3.215645507812" Y="26.287955078125" />
                  <Point X="3.261078125" Y="26.218900390625" />
                  <Point X="3.263703125" Y="26.21491015625" />
                  <Point X="3.280948242188" Y="26.198818359375" />
                  <Point X="3.346786621094" Y="26.1617578125" />
                  <Point X="3.346786865234" Y="26.1617578125" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.457583251953" Y="26.14185546875" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.553233886719" Y="26.151365234375" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.934153320312" Y="25.972064453125" />
                  <Point X="4.939189941406" Y="25.95137890625" />
                  <Point X="4.997858886719" Y="25.574556640625" />
                  <Point X="4.970223144531" Y="25.56715234375" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729091064453" Y="25.2328203125" />
                  <Point X="3.641633789062" Y="25.18226953125" />
                  <Point X="3.622263183594" Y="25.16692578125" />
                  <Point X="3.569788818359" Y="25.100060546875" />
                  <Point X="3.566757324219" Y="25.096197265625" />
                  <Point X="3.556985351562" Y="25.074736328125" />
                  <Point X="3.539493896484" Y="24.98340234375" />
                  <Point X="3.538483886719" Y="24.959310546875" />
                  <Point X="3.555975341797" Y="24.8679765625" />
                  <Point X="3.556986083984" Y="24.862701171875" />
                  <Point X="3.566757324219" Y="24.8412421875" />
                  <Point X="3.619231689453" Y="24.774376953125" />
                  <Point X="3.636577392578" Y="24.758091796875" />
                  <Point X="3.724034667969" Y="24.707541015625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.812174804688" Y="24.6805859375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.951243164062" Y="24.052248046875" />
                  <Point X="4.948430664062" Y="24.03359375" />
                  <Point X="4.874546386719" Y="23.7098203125" />
                  <Point X="4.837340820312" Y="23.71471875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.223188232422" Y="23.864349609375" />
                  <Point X="3.213277099609" Y="23.8621953125" />
                  <Point X="3.185444824219" Y="23.845302734375" />
                  <Point X="3.081694580078" Y="23.7205234375" />
                  <Point X="3.075700683594" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.049488037109" Y="23.524333984375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056362060547" Y="23.483375" />
                  <Point X="3.151354248047" Y="23.33562109375" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.234355712891" Y="23.263896484375" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.2120546875" Y="22.210677734375" />
                  <Point X="4.204137695313" Y="22.197865234375" />
                  <Point X="4.0566875" Y="21.988361328125" />
                  <Point X="4.022928955078" Y="22.0078515625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737338378906" Y="22.7466875" />
                  <Point X="2.533050292969" Y="22.78358203125" />
                  <Point X="2.521248046875" Y="22.785712890625" />
                  <Point X="2.489078125" Y="22.78075390625" />
                  <Point X="2.319364990234" Y="22.691435546875" />
                  <Point X="2.309560058594" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.199281005859" Y="22.495603515625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.226057128906" Y="22.2493359375" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.276435791016" Y="22.148078125" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.844703369141" Y="20.816505859375" />
                  <Point X="2.835274169922" Y="20.809771484375" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="2.651903564453" Y="20.745443359375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670546020508" Y="22.01953515625" />
                  <Point X="1.469062988281" Y="22.1490703125" />
                  <Point X="1.457422485352" Y="22.156552734375" />
                  <Point X="1.42580480957" Y="22.16428125" />
                  <Point X="1.205455078125" Y="22.14400390625" />
                  <Point X="1.192724121094" Y="22.1428359375" />
                  <Point X="1.165332397461" Y="22.131490234375" />
                  <Point X="0.995179016113" Y="21.99001171875" />
                  <Point X="0.985348632813" Y="21.981837890625" />
                  <Point X="0.96845703125" Y="21.954013671875" />
                  <Point X="0.91758190918" Y="21.71994921875" />
                  <Point X="0.913929443359" Y="21.68928125" />
                  <Point X="0.925929443359" Y="21.598130859375" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.00324407959" Y="20.038703125" />
                  <Point X="0.994339538574" Y="20.036751953125" />
                  <Point X="0.860200439453" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#212" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.18268991153" Y="5.036932540226" Z="2.4" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="-0.236432156461" Y="5.071268555169" Z="2.4" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.4" />
                  <Point X="-1.025832072416" Y="4.97204254565" Z="2.4" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.4" />
                  <Point X="-1.709987757464" Y="4.460968804913" Z="2.4" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.4" />
                  <Point X="-1.709408924438" Y="4.437588953684" Z="2.4" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.4" />
                  <Point X="-1.745341835888" Y="4.338560414803" Z="2.4" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.4" />
                  <Point X="-1.84429957467" Y="4.302431820138" Z="2.4" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.4" />
                  <Point X="-2.123367513053" Y="4.595669203726" Z="2.4" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.4" />
                  <Point X="-2.169913915662" Y="4.590111323244" Z="2.4" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.4" />
                  <Point X="-2.818872437545" Y="4.222736520606" Z="2.4" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.4" />
                  <Point X="-3.02212360544" Y="3.17599128166" Z="2.4" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.4" />
                  <Point X="-3.001115888145" Y="3.135640367216" Z="2.4" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.4" />
                  <Point X="-2.997356616673" Y="3.051446951214" Z="2.4" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.4" />
                  <Point X="-3.05943609839" Y="2.994448811028" Z="2.4" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.4" />
                  <Point X="-3.757868066637" Y="3.358070343305" Z="2.4" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.4" />
                  <Point X="-3.81616537674" Y="3.34959580224" Z="2.4" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.4" />
                  <Point X="-4.226244446013" Y="2.814551610882" Z="2.4" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.4" />
                  <Point X="-3.743047307239" Y="1.646503325818" Z="2.4" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.4" />
                  <Point X="-3.694937955841" Y="1.607713804039" Z="2.4" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.4" />
                  <Point X="-3.66816863159" Y="1.550454371025" Z="2.4" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.4" />
                  <Point X="-3.694824887816" Y="1.493142290329" Z="2.4" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.4" />
                  <Point X="-4.758404097995" Y="1.607210217955" Z="2.4" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.4" />
                  <Point X="-4.825034608714" Y="1.583347681407" Z="2.4" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.4" />
                  <Point X="-4.977609652661" Y="1.005639324835" Z="2.4" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.4" />
                  <Point X="-3.657601226934" Y="0.070783766587" Z="2.4" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.4" />
                  <Point X="-3.57504492644" Y="0.048016973882" Z="2.4" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.4" />
                  <Point X="-3.548302580985" Y="0.028178952145" Z="2.4" />
                  <Point X="-3.539556741714" Y="0" Z="2.4" />
                  <Point X="-3.54006202015" Y="-0.001627998916" Z="2.4" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.4" />
                  <Point X="-3.550323668683" Y="-0.030859009252" Z="2.4" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.4" />
                  <Point X="-4.979286561839" Y="-0.424928290169" Z="2.4" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.4" />
                  <Point X="-5.056085120672" Y="-0.476302168585" Z="2.4" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.4" />
                  <Point X="-4.975246858762" Y="-1.018699734387" Z="2.4" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.4" />
                  <Point X="-3.308063189821" Y="-1.318567779107" Z="2.4" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.4" />
                  <Point X="-3.217712459651" Y="-1.307714612656" Z="2.4" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.4" />
                  <Point X="-3.193097110694" Y="-1.324074547895" Z="2.4" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.4" />
                  <Point X="-4.43175944899" Y="-2.29706719851" Z="2.4" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.4" />
                  <Point X="-4.486867691943" Y="-2.378540461854" Z="2.4" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.4" />
                  <Point X="-4.1903960628" Y="-2.868795069957" Z="2.4" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.4" />
                  <Point X="-2.643264037996" Y="-2.596150596871" Z="2.4" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.4" />
                  <Point X="-2.571891954956" Y="-2.556438514175" Z="2.4" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.4" />
                  <Point X="-3.259266416298" Y="-3.791814344497" Z="2.4" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.4" />
                  <Point X="-3.277562634128" Y="-3.879457963605" Z="2.4" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.4" />
                  <Point X="-2.86647862735" Y="-4.192360430116" Z="2.4" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.4" />
                  <Point X="-2.238505639467" Y="-4.172460163397" Z="2.4" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.4" />
                  <Point X="-2.212132649631" Y="-4.147037777288" Z="2.4" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.4" />
                  <Point X="-1.951346826908" Y="-3.985192654499" Z="2.4" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.4" />
                  <Point X="-1.645926584536" Y="-4.015548862008" Z="2.4" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.4" />
                  <Point X="-1.422100085413" Y="-4.225560340652" Z="2.4" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.4" />
                  <Point X="-1.410465337788" Y="-4.859498224429" Z="2.4" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.4" />
                  <Point X="-1.396948631784" Y="-4.883658612471" Z="2.4" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.4" />
                  <Point X="-1.101030951122" Y="-4.958761219271" Z="2.4" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="-0.438965768003" Y="-3.600426579333" Z="2.4" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="-0.408144284789" Y="-3.505888648101" Z="2.4" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="-0.23952108883" Y="-3.278577875258" Z="2.4" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.4" />
                  <Point X="0.01383799053" Y="-3.20853417003" Z="2.4" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.4" />
                  <Point X="0.262301574644" Y="-3.295757107667" Z="2.4" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.4" />
                  <Point X="0.795789092398" Y="-4.932109450482" Z="2.4" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.4" />
                  <Point X="0.827518021279" Y="-5.011973576113" Z="2.4" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.4" />
                  <Point X="1.007789136268" Y="-4.978857830813" Z="2.4" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.4" />
                  <Point X="0.96934577217" Y="-3.364062645198" Z="2.4" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.4" />
                  <Point X="0.960285014622" Y="-3.259390926651" Z="2.4" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.4" />
                  <Point X="1.020990384989" Y="-3.017152384156" Z="2.4" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.4" />
                  <Point X="1.203874503196" Y="-2.874503944451" Z="2.4" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.4" />
                  <Point X="1.435870852908" Y="-2.861710288658" Z="2.4" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.4" />
                  <Point X="2.606079902576" Y="-4.253713321856" Z="2.4" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.4" />
                  <Point X="2.672709592911" Y="-4.319748715576" Z="2.4" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.4" />
                  <Point X="2.867609453893" Y="-4.19290132919" Z="2.4" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.4" />
                  <Point X="2.313580979555" Y="-2.795640694662" Z="2.4" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.4" />
                  <Point X="2.269105382521" Y="-2.710496173684" Z="2.4" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.4" />
                  <Point X="2.237371380661" Y="-2.496403250868" Z="2.4" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.4" />
                  <Point X="2.336495127504" Y="-2.321529929306" Z="2.4" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.4" />
                  <Point X="2.518010629829" Y="-2.23434262697" Z="2.4" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.4" />
                  <Point X="3.991772902444" Y="-3.00416852072" Z="2.4" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.4" />
                  <Point X="4.074651691996" Y="-3.032962242477" Z="2.4" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.4" />
                  <Point X="4.248433089604" Y="-2.784324266097" Z="2.4" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.4" />
                  <Point X="3.258637685187" Y="-1.665155940725" Z="2.4" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.4" />
                  <Point X="3.187254780891" Y="-1.60605673778" Z="2.4" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.4" />
                  <Point X="3.093121591814" Y="-1.448966612158" Z="2.4" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.4" />
                  <Point X="3.113985469248" Y="-1.280163295571" Z="2.4" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.4" />
                  <Point X="3.227652227018" Y="-1.15322874758" Z="2.4" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.4" />
                  <Point X="4.824658661359" Y="-1.303572456453" Z="2.4" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.4" />
                  <Point X="4.911618236444" Y="-1.294205581716" Z="2.4" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.4" />
                  <Point X="4.994528502033" Y="-0.923926713606" Z="2.4" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.4" />
                  <Point X="3.818959633672" Y="-0.239836886629" Z="2.4" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.4" />
                  <Point X="3.742899980417" Y="-0.217890074587" Z="2.4" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.4" />
                  <Point X="3.652411083145" Y="-0.163475196355" Z="2.4" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.4" />
                  <Point X="3.59892617986" Y="-0.09133384794" Z="2.4" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.4" />
                  <Point X="3.582445270564" Y="0.005276683236" Z="2.4" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.4" />
                  <Point X="3.602968355256" Y="0.10047354222" Z="2.4" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.4" />
                  <Point X="3.660495433935" Y="0.170258769655" Z="2.4" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.4" />
                  <Point X="4.977008183635" Y="0.550135046139" Z="2.4" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.4" />
                  <Point X="5.044415663147" Y="0.592280004557" Z="2.4" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.4" />
                  <Point X="4.976578573803" Y="1.015173865753" Z="2.4" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.4" />
                  <Point X="3.540551525485" Y="1.232217953817" Z="2.4" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.4" />
                  <Point X="3.457978530828" Y="1.222703777824" Z="2.4" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.4" />
                  <Point X="3.365156901871" Y="1.236609742565" Z="2.4" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.4" />
                  <Point X="3.2966937398" Y="1.277660630194" Z="2.4" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.4" />
                  <Point X="3.250295950775" Y="1.351393504514" Z="2.4" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.4" />
                  <Point X="3.23476763069" Y="1.436552859337" Z="2.4" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.4" />
                  <Point X="3.258271938154" Y="1.51343089103" Z="2.4" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.4" />
                  <Point X="4.385352360735" Y="2.407618124804" Z="2.4" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.4" />
                  <Point X="4.435889710313" Y="2.474036585782" Z="2.4" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.4" />
                  <Point X="4.225298320337" Y="2.818655531677" Z="2.4" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.4" />
                  <Point X="2.59138905138" Y="2.314059238726" Z="2.4" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.4" />
                  <Point X="2.50549283725" Y="2.265826139558" Z="2.4" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.4" />
                  <Point X="2.425799801643" Y="2.245986402175" Z="2.4" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.4" />
                  <Point X="2.35670898658" Y="2.256246787236" Z="2.4" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.4" />
                  <Point X="2.294511812999" Y="2.300315873802" Z="2.4" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.4" />
                  <Point X="2.253443300584" Y="2.363958642189" Z="2.4" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.4" />
                  <Point X="2.2467018031" Y="2.433976738632" Z="2.4" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.4" />
                  <Point X="3.081566114134" Y="3.920750030575" Z="2.4" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.4" />
                  <Point X="3.108137767126" Y="4.016831677149" Z="2.4" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.4" />
                  <Point X="2.731774304051" Y="4.281688183243" Z="2.4" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.4" />
                  <Point X="2.333274730504" Y="4.511270283326" Z="2.4" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.4" />
                  <Point X="1.92069327249" Y="4.701771298796" Z="2.4" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.4" />
                  <Point X="1.481007573357" Y="4.856915338043" Z="2.4" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.4" />
                  <Point X="0.82600200102" Y="5.010056428966" Z="2.4" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.4" />
                  <Point X="0.010554650023" Y="4.394514965725" Z="2.4" />
                  <Point X="0" Y="4.355124473572" Z="2.4" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>