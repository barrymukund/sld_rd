<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#205" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="3218" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004717773438" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.910462402344" Y="20.19185546875" />
                  <Point X="0.563302001953" Y="21.4874765625" />
                  <Point X="0.557721130371" Y="21.502859375" />
                  <Point X="0.542363708496" Y="21.532623046875" />
                  <Point X="0.400470489502" Y="21.7370625" />
                  <Point X="0.378636047363" Y="21.7685234375" />
                  <Point X="0.356755462646" Y="21.790974609375" />
                  <Point X="0.330499511719" Y="21.810220703125" />
                  <Point X="0.3024949646" Y="21.82433203125" />
                  <Point X="0.082923301697" Y="21.892478515625" />
                  <Point X="0.049135822296" Y="21.90296484375" />
                  <Point X="0.020976577759" Y="21.907234375" />
                  <Point X="-0.008664747238" Y="21.907234375" />
                  <Point X="-0.036823993683" Y="21.90296484375" />
                  <Point X="-0.256395477295" Y="21.834818359375" />
                  <Point X="-0.290182983398" Y="21.82433203125" />
                  <Point X="-0.318184814453" Y="21.81022265625" />
                  <Point X="-0.344440155029" Y="21.790978515625" />
                  <Point X="-0.366323455811" Y="21.768525390625" />
                  <Point X="-0.508216827393" Y="21.564083984375" />
                  <Point X="-0.530051147461" Y="21.532625" />
                  <Point X="-0.538188781738" Y="21.518427734375" />
                  <Point X="-0.55099017334" Y="21.4874765625" />
                  <Point X="-0.604410949707" Y="21.288107421875" />
                  <Point X="-0.916584655762" Y="20.12305859375" />
                  <Point X="-1.043643798828" Y="20.147720703125" />
                  <Point X="-1.079353881836" Y="20.15465234375" />
                  <Point X="-1.24641796875" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.268964233398" Y="20.747486328125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937255859" Y="20.81703125" />
                  <Point X="-1.304008666992" Y="20.844869140625" />
                  <Point X="-1.32364440918" Y="20.868796875" />
                  <Point X="-1.525797973633" Y="21.046080078125" />
                  <Point X="-1.556905151367" Y="21.073361328125" />
                  <Point X="-1.583190429688" Y="21.089705078125" />
                  <Point X="-1.612889282227" Y="21.102005859375" />
                  <Point X="-1.643028320312" Y="21.109033203125" />
                  <Point X="-1.911331176758" Y="21.126619140625" />
                  <Point X="-1.952617553711" Y="21.12932421875" />
                  <Point X="-1.983416870117" Y="21.126291015625" />
                  <Point X="-2.014464111328" Y="21.11797265625" />
                  <Point X="-2.042657470703" Y="21.10519921875" />
                  <Point X="-2.266221679688" Y="20.955818359375" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312788330078" Y="20.92317578125" />
                  <Point X="-2.335103027344" Y="20.900537109375" />
                  <Point X="-2.365094482422" Y="20.861451171875" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.749395507812" Y="20.87822265625" />
                  <Point X="-2.801723876953" Y="20.91062109375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-3.068919433594" Y="21.20593359375" />
                  <Point X="-2.423760986328" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414561523438" Y="22.4444296875" />
                  <Point X="-2.428780273438" Y="22.473259765625" />
                  <Point X="-2.446810302734" Y="22.49841796875" />
                  <Point X="-2.461845214844" Y="22.513451171875" />
                  <Point X="-2.464140380859" Y="22.51574609375" />
                  <Point X="-2.489298095703" Y="22.53377734375" />
                  <Point X="-2.518128662109" Y="22.547998046875" />
                  <Point X="-2.54774609375" Y="22.556984375" />
                  <Point X="-2.578680664062" Y="22.555974609375" />
                  <Point X="-2.6102109375" Y="22.549705078125" />
                  <Point X="-2.639183837891" Y="22.53880078125" />
                  <Point X="-2.811135742188" Y="22.4395234375" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.041528808594" Y="22.151837890625" />
                  <Point X="-4.082859619141" Y="22.206138671875" />
                  <Point X="-4.306142089844" Y="22.580548828125" />
                  <Point X="-4.234772949219" Y="22.6353125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084578613281" Y="23.52440234375" />
                  <Point X="-3.066614257812" Y="23.55153125" />
                  <Point X="-3.053856689453" Y="23.5801640625" />
                  <Point X="-3.0471796875" Y="23.6059453125" />
                  <Point X="-3.046152099609" Y="23.609912109375" />
                  <Point X="-3.043347900391" Y="23.640337890625" />
                  <Point X="-3.045555908203" Y="23.672009765625" />
                  <Point X="-3.052556884766" Y="23.701755859375" />
                  <Point X="-3.068639892578" Y="23.727740234375" />
                  <Point X="-3.089473144531" Y="23.751697265625" />
                  <Point X="-3.112972900391" Y="23.771232421875" />
                  <Point X="-3.135923828125" Y="23.784740234375" />
                  <Point X="-3.139445800781" Y="23.7868125" />
                  <Point X="-3.168729248047" Y="23.798046875" />
                  <Point X="-3.200612304688" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-3.449002441406" Y="23.777037109375" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.817911621094" Y="23.944056640625" />
                  <Point X="-4.834076660156" Y="24.00734375" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-4.81876953125" Y="24.43503515625" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517493408203" Y="24.785171875" />
                  <Point X="-3.487731933594" Y="24.80052734375" />
                  <Point X="-3.463680175781" Y="24.817220703125" />
                  <Point X="-3.459979003906" Y="24.8197890625" />
                  <Point X="-3.437527832031" Y="24.84166796875" />
                  <Point X="-3.418280761719" Y="24.867923828125" />
                  <Point X="-3.404168457031" Y="24.8959296875" />
                  <Point X="-3.396151123047" Y="24.92176171875" />
                  <Point X="-3.394917480469" Y="24.925736328125" />
                  <Point X="-3.390648193359" Y="24.953896484375" />
                  <Point X="-3.390647949219" Y="24.9835390625" />
                  <Point X="-3.394917236328" Y="25.011697265625" />
                  <Point X="-3.4029296875" Y="25.037513671875" />
                  <Point X="-3.404162841797" Y="25.041490234375" />
                  <Point X="-3.4182734375" Y="25.0695" />
                  <Point X="-3.437522705078" Y="25.095763671875" />
                  <Point X="-3.459979003906" Y="25.117650390625" />
                  <Point X="-3.484030761719" Y="25.13434375" />
                  <Point X="-3.495171386719" Y="25.14099609375" />
                  <Point X="-3.514437255859" Y="25.150783203125" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.730748779297" Y="25.210869140625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.834905273438" Y="25.906576171875" />
                  <Point X="-4.824487792969" Y="25.9769765625" />
                  <Point X="-4.703551757812" Y="26.423267578125" />
                  <Point X="-4.687426757812" Y="26.42114453125" />
                  <Point X="-3.765666748047" Y="26.29979296875" />
                  <Point X="-3.744984619141" Y="26.299341796875" />
                  <Point X="-3.723424316406" Y="26.301228515625" />
                  <Point X="-3.703141357422" Y="26.305263671875" />
                  <Point X="-3.649906982422" Y="26.322046875" />
                  <Point X="-3.641697998047" Y="26.324634765625" />
                  <Point X="-3.622764892578" Y="26.33296875" />
                  <Point X="-3.604024169922" Y="26.343791015625" />
                  <Point X="-3.587345458984" Y="26.356021484375" />
                  <Point X="-3.573709228516" Y="26.37157421875" />
                  <Point X="-3.561297119141" Y="26.389302734375" />
                  <Point X="-3.551351318359" Y="26.407431640625" />
                  <Point X="-3.529990722656" Y="26.459" />
                  <Point X="-3.526703857422" Y="26.466935546875" />
                  <Point X="-3.520915039062" Y="26.486796875" />
                  <Point X="-3.517156982422" Y="26.50811328125" />
                  <Point X="-3.5158046875" Y="26.528755859375" />
                  <Point X="-3.518952636719" Y="26.549201171875" />
                  <Point X="-3.524556396484" Y="26.570109375" />
                  <Point X="-3.532051513672" Y="26.589380859375" />
                  <Point X="-3.557825195312" Y="26.638892578125" />
                  <Point X="-3.561791259766" Y="26.646509765625" />
                  <Point X="-3.573286865234" Y="26.663712890625" />
                  <Point X="-3.587197509766" Y="26.6802890625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-3.715636230469" Y="26.781681640625" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.121629882812" Y="27.664314453125" />
                  <Point X="-4.08115625" Y="27.733654296875" />
                  <Point X="-3.750505126953" Y="28.158662109375" />
                  <Point X="-3.206656494141" Y="27.844669921875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167086181641" Y="27.82983203125" />
                  <Point X="-3.146793945312" Y="27.825794921875" />
                  <Point X="-3.072653320312" Y="27.81930859375" />
                  <Point X="-3.061244628906" Y="27.818310546875" />
                  <Point X="-3.040560302734" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946078369141" Y="27.860228515625" />
                  <Point X="-2.893452636719" Y="27.912853515625" />
                  <Point X="-2.885354736328" Y="27.920951171875" />
                  <Point X="-2.872408447266" Y="27.93708203125" />
                  <Point X="-2.860778808594" Y="27.9553359375" />
                  <Point X="-2.851629394531" Y="27.97388671875" />
                  <Point X="-2.846712646484" Y="27.99398046875" />
                  <Point X="-2.843887207031" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.849922363281" Y="28.110259765625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.920090576172" Y="28.268646484375" />
                  <Point X="-3.183332763672" Y="28.72459375" />
                  <Point X="-2.7711171875" Y="29.04063671875" />
                  <Point X="-2.700621826172" Y="29.09468359375" />
                  <Point X="-2.167035644531" Y="29.3911328125" />
                  <Point X="-2.043194946289" Y="29.2297421875" />
                  <Point X="-2.02889831543" Y="29.214806640625" />
                  <Point X="-2.012321533203" Y="29.20089453125" />
                  <Point X="-1.995116699219" Y="29.189396484375" />
                  <Point X="-1.912598510742" Y="29.146439453125" />
                  <Point X="-1.899900756836" Y="29.139828125" />
                  <Point X="-1.880626464844" Y="29.13233203125" />
                  <Point X="-1.859718017578" Y="29.126728515625" />
                  <Point X="-1.839269775391" Y="29.123580078125" />
                  <Point X="-1.818625" Y="29.12493359375" />
                  <Point X="-1.797307617188" Y="29.128693359375" />
                  <Point X="-1.777452758789" Y="29.134482421875" />
                  <Point X="-1.691504638672" Y="29.170083984375" />
                  <Point X="-1.678278930664" Y="29.1755625" />
                  <Point X="-1.660144287109" Y="29.18551171875" />
                  <Point X="-1.642415405273" Y="29.19792578125" />
                  <Point X="-1.626863525391" Y="29.211564453125" />
                  <Point X="-1.614632446289" Y="29.228244140625" />
                  <Point X="-1.603810668945" Y="29.24698828125" />
                  <Point X="-1.59548034668" Y="29.265921875" />
                  <Point X="-1.567505981445" Y="29.354646484375" />
                  <Point X="-1.563201049805" Y="29.368298828125" />
                  <Point X="-1.559166015625" Y="29.388583984375" />
                  <Point X="-1.557279296875" Y="29.41014453125" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.563448364258" Y="29.4742578125" />
                  <Point X="-1.584201904297" Y="29.6318984375" />
                  <Point X="-1.040891845703" Y="29.78422265625" />
                  <Point X="-0.949625366211" Y="29.80980859375" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.133903366089" Y="29.286314453125" />
                  <Point X="-0.121129745483" Y="29.258123046875" />
                  <Point X="-0.103271606445" Y="29.231396484375" />
                  <Point X="-0.082114006042" Y="29.208806640625" />
                  <Point X="-0.054818138123" Y="29.194216796875" />
                  <Point X="-0.024380064011" Y="29.183884765625" />
                  <Point X="0.006155910492" Y="29.17884375" />
                  <Point X="0.036691894531" Y="29.183884765625" />
                  <Point X="0.067129966736" Y="29.194216796875" />
                  <Point X="0.094425949097" Y="29.208806640625" />
                  <Point X="0.115583587646" Y="29.231396484375" />
                  <Point X="0.133441726685" Y="29.258123046875" />
                  <Point X="0.146215194702" Y="29.286314453125" />
                  <Point X="0.171986022949" Y="29.3824921875" />
                  <Point X="0.307419525146" Y="29.887939453125" />
                  <Point X="0.764352905273" Y="29.840083984375" />
                  <Point X="0.844031494141" Y="29.831740234375" />
                  <Point X="1.401623535156" Y="29.697119140625" />
                  <Point X="1.481039794922" Y="29.6779453125" />
                  <Point X="1.843763305664" Y="29.5463828125" />
                  <Point X="1.89464453125" Y="29.527927734375" />
                  <Point X="2.245583984375" Y="29.363806640625" />
                  <Point X="2.294558105469" Y="29.34090234375" />
                  <Point X="2.633619628906" Y="29.143365234375" />
                  <Point X="2.680991455078" Y="29.115765625" />
                  <Point X="2.943260742188" Y="28.92925390625" />
                  <Point X="2.894303955078" Y="28.844458984375" />
                  <Point X="2.147581298828" Y="27.55109765625" />
                  <Point X="2.142077148438" Y="27.53993359375" />
                  <Point X="2.133076660156" Y="27.516056640625" />
                  <Point X="2.114470214844" Y="27.4464765625" />
                  <Point X="2.1121875" Y="27.43528125" />
                  <Point X="2.107986572266" Y="27.405671875" />
                  <Point X="2.107728027344" Y="27.380953125" />
                  <Point X="2.114983154297" Y="27.320787109375" />
                  <Point X="2.116099365234" Y="27.311529296875" />
                  <Point X="2.121442138672" Y="27.28960546875" />
                  <Point X="2.129710205078" Y="27.26751171875" />
                  <Point X="2.140072265625" Y="27.24746875" />
                  <Point X="2.177301269531" Y="27.1926015625" />
                  <Point X="2.184428222656" Y="27.183373046875" />
                  <Point X="2.203455322266" Y="27.161634765625" />
                  <Point X="2.221599365234" Y="27.145591796875" />
                  <Point X="2.276447021484" Y="27.108375" />
                  <Point X="2.284888916016" Y="27.10264453125" />
                  <Point X="2.304943115234" Y="27.092275390625" />
                  <Point X="2.327038574219" Y="27.084005859375" />
                  <Point X="2.348965820312" Y="27.078662109375" />
                  <Point X="2.409127197266" Y="27.071408203125" />
                  <Point X="2.421243652344" Y="27.0707265625" />
                  <Point X="2.449409423828" Y="27.070947265625" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.542787597656" Y="27.09277734375" />
                  <Point X="2.549237792969" Y="27.09475" />
                  <Point X="2.572027099609" Y="27.102615234375" />
                  <Point X="2.588533691406" Y="27.11014453125" />
                  <Point X="2.787555175781" Y="27.225048828125" />
                  <Point X="3.967326171875" Y="27.906189453125" />
                  <Point X="4.095179931641" Y="27.728501953125" />
                  <Point X="4.123274414062" Y="27.689458984375" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="4.213914550781" Y="27.422833984375" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203974121094" Y="26.641626953125" />
                  <Point X="3.153897705078" Y="26.576298828125" />
                  <Point X="3.147797119141" Y="26.5673203125" />
                  <Point X="3.131621826172" Y="26.54031640625" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.102976318359" Y="26.450384765625" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836669922" Y="26.394251953125" />
                  <Point X="3.097739746094" Y="26.371767578125" />
                  <Point X="3.113052246094" Y="26.2975546875" />
                  <Point X="3.115925537109" Y="26.2868359375" />
                  <Point X="3.125481201172" Y="26.25803515625" />
                  <Point X="3.136283935547" Y="26.235736328125" />
                  <Point X="3.177932617188" Y="26.172431640625" />
                  <Point X="3.184341552734" Y="26.16269140625" />
                  <Point X="3.198896484375" Y="26.1454453125" />
                  <Point X="3.216136962891" Y="26.129359375" />
                  <Point X="3.234345214844" Y="26.11603515625" />
                  <Point X="3.294699951172" Y="26.082060546875" />
                  <Point X="3.305129638672" Y="26.077001953125" />
                  <Point X="3.332394287109" Y="26.065775390625" />
                  <Point X="3.356117919922" Y="26.0594375" />
                  <Point X="3.437721435547" Y="26.04865234375" />
                  <Point X="3.444022216797" Y="26.048033203125" />
                  <Point X="3.469656982422" Y="26.04637109375" />
                  <Point X="3.488203125" Y="26.046984375" />
                  <Point X="3.677260253906" Y="26.071873046875" />
                  <Point X="4.776839355469" Y="26.21663671875" />
                  <Point X="4.833870605469" Y="25.9823671875" />
                  <Point X="4.845937011719" Y="25.9328046875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.843268066406" Y="25.631486328125" />
                  <Point X="3.716580078125" Y="25.32958984375" />
                  <Point X="3.704788818359" Y="25.325583984375" />
                  <Point X="3.681547363281" Y="25.315068359375" />
                  <Point X="3.601374267578" Y="25.268728515625" />
                  <Point X="3.592718505859" Y="25.263076171875" />
                  <Point X="3.566070068359" Y="25.243525390625" />
                  <Point X="3.547531005859" Y="25.225576171875" />
                  <Point X="3.499427246094" Y="25.164279296875" />
                  <Point X="3.492025146484" Y="25.15484765625" />
                  <Point X="3.480302246094" Y="25.1355703125" />
                  <Point X="3.470527587891" Y="25.11410546875" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.447645996094" Y="25.008876953125" />
                  <Point X="3.446237060547" Y="24.998380859375" />
                  <Point X="3.443769775391" Y="24.966685546875" />
                  <Point X="3.445178710938" Y="24.941443359375" />
                  <Point X="3.461213378906" Y="24.857716796875" />
                  <Point X="3.463680908203" Y="24.844833984375" />
                  <Point X="3.470528808594" Y="24.823330078125" />
                  <Point X="3.480303222656" Y="24.8018671875" />
                  <Point X="3.492025146484" Y="24.782591796875" />
                  <Point X="3.54012890625" Y="24.721294921875" />
                  <Point X="3.547476074219" Y="24.71298046875" />
                  <Point X="3.569189697266" Y="24.69112890625" />
                  <Point X="3.589035644531" Y="24.67584375" />
                  <Point X="3.669208740234" Y="24.629501953125" />
                  <Point X="3.674460449219" Y="24.626681640625" />
                  <Point X="3.698877929688" Y="24.61454296875" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="3.889954101562" Y="24.561392578125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.861759277344" Y="24.09595703125" />
                  <Point X="4.855021972656" Y="24.05126953125" />
                  <Point X="4.801174316406" Y="23.81530078125" />
                  <Point X="4.731023925781" Y="23.824537109375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.40803515625" Y="23.9972890625" />
                  <Point X="3.374660400391" Y="23.994490234375" />
                  <Point X="3.217309082031" Y="23.9602890625" />
                  <Point X="3.193096191406" Y="23.95502734375" />
                  <Point X="3.163978271484" Y="23.943404296875" />
                  <Point X="3.136149658203" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.017288330078" Y="23.79165234375" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.956126220703" Y="23.5465" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.95634765625" Y="23.49243359375" />
                  <Point X="2.964079589844" Y="23.4608125" />
                  <Point X="2.976450927734" Y="23.432001953125" />
                  <Point X="3.063531494141" Y="23.2965546875" />
                  <Point X="3.076931152344" Y="23.275712890625" />
                  <Point X="3.086930419922" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.271520751953" Y="23.1156328125" />
                  <Point X="4.213123046875" Y="22.393115234375" />
                  <Point X="4.143799804688" Y="22.28094140625" />
                  <Point X="4.124823730469" Y="22.250232421875" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="3.964695068359" Y="22.151169921875" />
                  <Point X="2.800954345703" Y="22.823056640625" />
                  <Point X="2.786135009766" Y="22.829984375" />
                  <Point X="2.754222900391" Y="22.84017578125" />
                  <Point X="2.566949951172" Y="22.873998046875" />
                  <Point X="2.538132568359" Y="22.879201171875" />
                  <Point X="2.506775390625" Y="22.879603515625" />
                  <Point X="2.474604736328" Y="22.87464453125" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.289255615234" Y="22.782943359375" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242385253906" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.12265234375" Y="22.553982421875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229492188" Y="22.500267578125" />
                  <Point X="2.095271484375" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.129496826172" Y="22.24946875" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819335938" Y="22.173919921875" />
                  <Point X="2.255208740234" Y="21.994845703125" />
                  <Point X="2.861283691406" Y="20.94509375" />
                  <Point X="2.804387207031" Y="20.904453125" />
                  <Point X="2.781836914062" Y="20.88834765625" />
                  <Point X="2.701764892578" Y="20.836517578125" />
                  <Point X="2.646587890625" Y="20.90842578125" />
                  <Point X="1.758546386719" Y="22.065744140625" />
                  <Point X="1.74750378418" Y="22.0778203125" />
                  <Point X="1.721922973633" Y="22.099443359375" />
                  <Point X="1.537221435547" Y="22.218189453125" />
                  <Point X="1.508799560547" Y="22.2364609375" />
                  <Point X="1.479986938477" Y="22.24883203125" />
                  <Point X="1.448368774414" Y="22.2565625" />
                  <Point X="1.41710144043" Y="22.258880859375" />
                  <Point X="1.215098754883" Y="22.24029296875" />
                  <Point X="1.184014648438" Y="22.23743359375" />
                  <Point X="1.15636706543" Y="22.230603515625" />
                  <Point X="1.128978759766" Y="22.219259765625" />
                  <Point X="1.104594116211" Y="22.2045390625" />
                  <Point X="0.948612792969" Y="22.074845703125" />
                  <Point X="0.924610351562" Y="22.05488671875" />
                  <Point X="0.904137756348" Y="22.0311328125" />
                  <Point X="0.887247009277" Y="22.003306640625" />
                  <Point X="0.875624267578" Y="21.974189453125" />
                  <Point X="0.828986633301" Y="21.75962109375" />
                  <Point X="0.821809997559" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="0.849042114258" Y="21.454326171875" />
                  <Point X="1.022065490723" Y="20.1400859375" />
                  <Point X="0.996997680664" Y="20.13458984375" />
                  <Point X="0.975678833008" Y="20.12991796875" />
                  <Point X="0.929315307617" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.025542358398" Y="20.24098046875" />
                  <Point X="-1.05844543457" Y="20.2473671875" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.175789672852" Y="20.76601953125" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199025756836" Y="20.850494140625" />
                  <Point X="-1.20566394043" Y="20.864529296875" />
                  <Point X="-1.221735229492" Y="20.8923671875" />
                  <Point X="-1.230570800781" Y="20.905134765625" />
                  <Point X="-1.250206665039" Y="20.9290625" />
                  <Point X="-1.261006713867" Y="20.94022265625" />
                  <Point X="-1.46316027832" Y="21.117505859375" />
                  <Point X="-1.494267456055" Y="21.144787109375" />
                  <Point X="-1.506741943359" Y="21.154037109375" />
                  <Point X="-1.53302722168" Y="21.170380859375" />
                  <Point X="-1.546837646484" Y="21.177474609375" />
                  <Point X="-1.576536621094" Y="21.189775390625" />
                  <Point X="-1.591317260742" Y="21.1945234375" />
                  <Point X="-1.621456298828" Y="21.20155078125" />
                  <Point X="-1.636814941406" Y="21.203830078125" />
                  <Point X="-1.905117675781" Y="21.221416015625" />
                  <Point X="-1.946404052734" Y="21.22412109375" />
                  <Point X="-1.961928344727" Y="21.2238671875" />
                  <Point X="-1.992727661133" Y="21.220833984375" />
                  <Point X="-2.008002685547" Y="21.2180546875" />
                  <Point X="-2.039049926758" Y="21.209736328125" />
                  <Point X="-2.053669189453" Y="21.204505859375" />
                  <Point X="-2.081862548828" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.319000976562" Y="21.03480859375" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359688232422" Y="21.00723828125" />
                  <Point X="-2.380445800781" Y="20.989865234375" />
                  <Point X="-2.402760498047" Y="20.9672265625" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.440463378906" Y="20.919283203125" />
                  <Point X="-2.503200927734" Y="20.8375234375" />
                  <Point X="-2.699384521484" Y="20.958994140625" />
                  <Point X="-2.747601074219" Y="20.98884765625" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.341488037109" Y="22.275880859375" />
                  <Point X="-2.334849609375" Y="22.28991796875" />
                  <Point X="-2.323947509766" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314667236328" Y="22.442388671875" />
                  <Point X="-2.323652832031" Y="22.4720078125" />
                  <Point X="-2.329360107422" Y="22.486451171875" />
                  <Point X="-2.343578857422" Y="22.51528125" />
                  <Point X="-2.351562744141" Y="22.528599609375" />
                  <Point X="-2.369592773438" Y="22.5537578125" />
                  <Point X="-2.379638916016" Y="22.56559765625" />
                  <Point X="-2.394673828125" Y="22.580630859375" />
                  <Point X="-2.408797851562" Y="22.5929609375" />
                  <Point X="-2.433955566406" Y="22.6109921875" />
                  <Point X="-2.4472734375" Y="22.6189765625" />
                  <Point X="-2.476104003906" Y="22.633197265625" />
                  <Point X="-2.490546142578" Y="22.63890625" />
                  <Point X="-2.520163574219" Y="22.647892578125" />
                  <Point X="-2.550845458984" Y="22.65193359375" />
                  <Point X="-2.581780029297" Y="22.650923828125" />
                  <Point X="-2.597208007812" Y="22.649150390625" />
                  <Point X="-2.62873828125" Y="22.642880859375" />
                  <Point X="-2.643673828125" Y="22.6386171875" />
                  <Point X="-2.672646728516" Y="22.627712890625" />
                  <Point X="-2.686684082031" Y="22.621072265625" />
                  <Point X="-2.858635986328" Y="22.521794921875" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.965935546875" Y="22.209376953125" />
                  <Point X="-4.004018554688" Y="22.25941015625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-4.176940429688" Y="22.559943359375" />
                  <Point X="-3.048122070312" Y="23.426115234375" />
                  <Point X="-3.036482910156" Y="23.4366875" />
                  <Point X="-3.015106933594" Y="23.45960546875" />
                  <Point X="-3.005370361328" Y="23.471951171875" />
                  <Point X="-2.987406005859" Y="23.499080078125" />
                  <Point X="-2.979838134766" Y="23.5128671875" />
                  <Point X="-2.967080566406" Y="23.5415" />
                  <Point X="-2.961890869141" Y="23.556345703125" />
                  <Point X="-2.955213867188" Y="23.582126953125" />
                  <Point X="-2.951552978516" Y="23.601193359375" />
                  <Point X="-2.948748779297" Y="23.631619140625" />
                  <Point X="-2.948577880859" Y="23.6469453125" />
                  <Point X="-2.950785888672" Y="23.6786171875" />
                  <Point X="-2.953082519531" Y="23.6937734375" />
                  <Point X="-2.960083496094" Y="23.72351953125" />
                  <Point X="-2.971778076172" Y="23.75175390625" />
                  <Point X="-2.987861083984" Y="23.77773828125" />
                  <Point X="-2.996953857422" Y="23.790078125" />
                  <Point X="-3.017787109375" Y="23.81403515625" />
                  <Point X="-3.028743652344" Y="23.824751953125" />
                  <Point X="-3.052243408203" Y="23.844287109375" />
                  <Point X="-3.064786621094" Y="23.85310546875" />
                  <Point X="-3.087737548828" Y="23.86661328125" />
                  <Point X="-3.10541796875" Y="23.875509765625" />
                  <Point X="-3.134701416016" Y="23.886744140625" />
                  <Point X="-3.149812255859" Y="23.89114453125" />
                  <Point X="-3.1816953125" Y="23.897623046875" />
                  <Point X="-3.197308349609" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-3.46140234375" Y="23.871224609375" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.725866699219" Y="23.967568359375" />
                  <Point X="-4.740761230469" Y="24.025880859375" />
                  <Point X="-4.786451660156" Y="24.345341796875" />
                  <Point X="-3.508287109375" Y="24.687826171875" />
                  <Point X="-3.500469238281" Y="24.690287109375" />
                  <Point X="-3.473934082031" Y="24.70074609375" />
                  <Point X="-3.444172607422" Y="24.7161015625" />
                  <Point X="-3.433564697266" Y="24.722482421875" />
                  <Point X="-3.409512939453" Y="24.73917578125" />
                  <Point X="-3.393676513672" Y="24.751751953125" />
                  <Point X="-3.371225341797" Y="24.773630859375" />
                  <Point X="-3.360909423828" Y="24.785501953125" />
                  <Point X="-3.341662353516" Y="24.8117578125" />
                  <Point X="-3.333443115234" Y="24.825173828125" />
                  <Point X="-3.319330810547" Y="24.8531796875" />
                  <Point X="-3.313437744141" Y="24.86776953125" />
                  <Point X="-3.305420410156" Y="24.8936015625" />
                  <Point X="-3.300990722656" Y="24.91149609375" />
                  <Point X="-3.296721435547" Y="24.93965625" />
                  <Point X="-3.295648193359" Y="24.953896484375" />
                  <Point X="-3.295647949219" Y="24.9835390625" />
                  <Point X="-3.296721435547" Y="24.997779296875" />
                  <Point X="-3.300990722656" Y="25.0259375" />
                  <Point X="-3.304186523438" Y="25.039857421875" />
                  <Point X="-3.312192382812" Y="25.06565234375" />
                  <Point X="-3.319320800781" Y="25.084232421875" />
                  <Point X="-3.333431396484" Y="25.1122421875" />
                  <Point X="-3.341649902344" Y="25.125658203125" />
                  <Point X="-3.360899169922" Y="25.151921875" />
                  <Point X="-3.371215820312" Y="25.163796875" />
                  <Point X="-3.393672119141" Y="25.18568359375" />
                  <Point X="-3.405811767578" Y="25.1956953125" />
                  <Point X="-3.429863525391" Y="25.212388671875" />
                  <Point X="-3.452144775391" Y="25.225693359375" />
                  <Point X="-3.471410644531" Y="25.23548046875" />
                  <Point X="-3.480441162109" Y="25.2394921875" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.706161132812" Y="25.3026328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.740928710938" Y="25.892669921875" />
                  <Point X="-4.731331054688" Y="25.95753125" />
                  <Point X="-4.633586914062" Y="26.318236328125" />
                  <Point X="-3.778067626953" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747056396484" Y="26.204365234375" />
                  <Point X="-3.736702880859" Y="26.204703125" />
                  <Point X="-3.715142578125" Y="26.20658984375" />
                  <Point X="-3.704887939453" Y="26.2080546875" />
                  <Point X="-3.684604980469" Y="26.21208984375" />
                  <Point X="-3.674576660156" Y="26.21466015625" />
                  <Point X="-3.621342285156" Y="26.231443359375" />
                  <Point X="-3.603424560547" Y="26.237685546875" />
                  <Point X="-3.584491455078" Y="26.24601953125" />
                  <Point X="-3.575257324219" Y="26.250701171875" />
                  <Point X="-3.556516601562" Y="26.2615234375" />
                  <Point X="-3.547846435547" Y="26.267181640625" />
                  <Point X="-3.531167724609" Y="26.279412109375" />
                  <Point X="-3.515913574219" Y="26.293392578125" />
                  <Point X="-3.50227734375" Y="26.3089453125" />
                  <Point X="-3.49588671875" Y="26.31708984375" />
                  <Point X="-3.483474609375" Y="26.334818359375" />
                  <Point X="-3.478008056641" Y="26.343609375" />
                  <Point X="-3.468062255859" Y="26.36173828125" />
                  <Point X="-3.463583007813" Y="26.371076171875" />
                  <Point X="-3.442222412109" Y="26.42264453125" />
                  <Point X="-3.435498779297" Y="26.440353515625" />
                  <Point X="-3.429709960938" Y="26.46021484375" />
                  <Point X="-3.427357910156" Y="26.470302734375" />
                  <Point X="-3.423599853516" Y="26.491619140625" />
                  <Point X="-3.422360107422" Y="26.50190234375" />
                  <Point X="-3.4210078125" Y="26.522544921875" />
                  <Point X="-3.421911132813" Y="26.543212890625" />
                  <Point X="-3.425059082031" Y="26.563658203125" />
                  <Point X="-3.427191162109" Y="26.573794921875" />
                  <Point X="-3.432794921875" Y="26.594703125" />
                  <Point X="-3.436016845703" Y="26.604544921875" />
                  <Point X="-3.443511962891" Y="26.62381640625" />
                  <Point X="-3.44778515625" Y="26.63324609375" />
                  <Point X="-3.473558837891" Y="26.6827578125" />
                  <Point X="-3.482803466797" Y="26.699291015625" />
                  <Point X="-3.494299072266" Y="26.716494140625" />
                  <Point X="-3.500516113281" Y="26.72478125" />
                  <Point X="-3.514426757812" Y="26.741357421875" />
                  <Point X="-3.521502929688" Y="26.748912109375" />
                  <Point X="-3.53644140625" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-3.657803955078" Y="26.85705078125" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.039583496094" Y="27.61642578125" />
                  <Point X="-4.002296386719" Y="27.680306640625" />
                  <Point X="-3.726338378906" Y="28.035013671875" />
                  <Point X="-3.254156494141" Y="27.7623984375" />
                  <Point X="-3.244916259766" Y="27.75771484375" />
                  <Point X="-3.225988525391" Y="27.74938671875" />
                  <Point X="-3.216300537109" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.73923046875" />
                  <Point X="-3.185623046875" Y="27.736658203125" />
                  <Point X="-3.165330810547" Y="27.73262109375" />
                  <Point X="-3.155073486328" Y="27.73115625" />
                  <Point X="-3.080932861328" Y="27.724669921875" />
                  <Point X="-3.059172851562" Y="27.723333984375" />
                  <Point X="-3.038488525391" Y="27.72378515625" />
                  <Point X="-3.028155517578" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.902745605469" Y="27.773193359375" />
                  <Point X="-2.886614257812" Y="27.786140625" />
                  <Point X="-2.878903808594" Y="27.793052734375" />
                  <Point X="-2.826278076172" Y="27.845677734375" />
                  <Point X="-2.811265380859" Y="27.86148828125" />
                  <Point X="-2.798319091797" Y="27.877619140625" />
                  <Point X="-2.792287597656" Y="27.886037109375" />
                  <Point X="-2.780657958984" Y="27.904291015625" />
                  <Point X="-2.775578125" Y="27.913314453125" />
                  <Point X="-2.766428710938" Y="27.931865234375" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754435058594" Y="27.971400390625" />
                  <Point X="-2.752525634766" Y="27.981580078125" />
                  <Point X="-2.749700195312" Y="28.0030390625" />
                  <Point X="-2.748909912109" Y="28.013365234375" />
                  <Point X="-2.748458496094" Y="28.034044921875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.755283935547" Y="28.1185390625" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.837818359375" Y="28.316146484375" />
                  <Point X="-3.059386962891" Y="28.6999140625" />
                  <Point X="-2.713314941406" Y="28.965244140625" />
                  <Point X="-2.648369628906" Y="29.01503515625" />
                  <Point X="-2.192522460938" Y="29.268294921875" />
                  <Point X="-2.118563232422" Y="29.17191015625" />
                  <Point X="-2.111822021484" Y="29.16405078125" />
                  <Point X="-2.097525390625" Y="29.149115234375" />
                  <Point X="-2.089969726562" Y="29.142037109375" />
                  <Point X="-2.073392822266" Y="29.128125" />
                  <Point X="-2.065107421875" Y="29.12191015625" />
                  <Point X="-2.047902709961" Y="29.110412109375" />
                  <Point X="-2.038983398437" Y="29.105130859375" />
                  <Point X="-1.956465332031" Y="29.062173828125" />
                  <Point X="-1.934335327148" Y="29.0512890625" />
                  <Point X="-1.915061035156" Y="29.04379296875" />
                  <Point X="-1.90521887207" Y="29.0405703125" />
                  <Point X="-1.884310302734" Y="29.034966796875" />
                  <Point X="-1.874174926758" Y="29.0328359375" />
                  <Point X="-1.85372668457" Y="29.0296875" />
                  <Point X="-1.8330546875" Y="29.028783203125" />
                  <Point X="-1.812409912109" Y="29.03013671875" />
                  <Point X="-1.802124389648" Y="29.031376953125" />
                  <Point X="-1.780807006836" Y="29.03513671875" />
                  <Point X="-1.770715820312" Y="29.037490234375" />
                  <Point X="-1.750860961914" Y="29.043279296875" />
                  <Point X="-1.741097290039" Y="29.04671484375" />
                  <Point X="-1.655149169922" Y="29.08231640625" />
                  <Point X="-1.632584228516" Y="29.0922734375" />
                  <Point X="-1.614449462891" Y="29.10222265625" />
                  <Point X="-1.605654052734" Y="29.107693359375" />
                  <Point X="-1.587925292969" Y="29.120107421875" />
                  <Point X="-1.57977746582" Y="29.126501953125" />
                  <Point X="-1.564225585938" Y="29.140140625" />
                  <Point X="-1.550253540039" Y="29.15538671875" />
                  <Point X="-1.538022460938" Y="29.17206640625" />
                  <Point X="-1.532359741211" Y="29.180744140625" />
                  <Point X="-1.521537963867" Y="29.19948828125" />
                  <Point X="-1.516854980469" Y="29.20873046875" />
                  <Point X="-1.508524658203" Y="29.2276640625" />
                  <Point X="-1.504877197266" Y="29.23735546875" />
                  <Point X="-1.476902709961" Y="29.326080078125" />
                  <Point X="-1.470026611328" Y="29.349765625" />
                  <Point X="-1.465991455078" Y="29.37005078125" />
                  <Point X="-1.464527709961" Y="29.380302734375" />
                  <Point X="-1.462640991211" Y="29.40186328125" />
                  <Point X="-1.462301879883" Y="29.41221484375" />
                  <Point X="-1.462752807617" Y="29.432896484375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.469261352539" Y="29.486658203125" />
                  <Point X="-1.479266357422" Y="29.56265625" />
                  <Point X="-1.015246154785" Y="29.69275" />
                  <Point X="-0.931167724609" Y="29.7163203125" />
                  <Point X="-0.365222564697" Y="29.78255859375" />
                  <Point X="-0.225666366577" Y="29.2617265625" />
                  <Point X="-0.220435150146" Y="29.247107421875" />
                  <Point X="-0.207661453247" Y="29.218916015625" />
                  <Point X="-0.200119293213" Y="29.20534375" />
                  <Point X="-0.182261199951" Y="29.1786171875" />
                  <Point X="-0.172608886719" Y="29.166455078125" />
                  <Point X="-0.15145123291" Y="29.143865234375" />
                  <Point X="-0.126896499634" Y="29.1250234375" />
                  <Point X="-0.099600570679" Y="29.11043359375" />
                  <Point X="-0.085354026794" Y="29.1042578125" />
                  <Point X="-0.054916057587" Y="29.09392578125" />
                  <Point X="-0.039853721619" Y="29.090154296875" />
                  <Point X="-0.009317661285" Y="29.08511328125" />
                  <Point X="0.021629489899" Y="29.08511328125" />
                  <Point X="0.052165550232" Y="29.090154296875" />
                  <Point X="0.067227882385" Y="29.09392578125" />
                  <Point X="0.097665855408" Y="29.1042578125" />
                  <Point X="0.111912246704" Y="29.11043359375" />
                  <Point X="0.139208175659" Y="29.1250234375" />
                  <Point X="0.163763061523" Y="29.143865234375" />
                  <Point X="0.184920715332" Y="29.166455078125" />
                  <Point X="0.194573181152" Y="29.1786171875" />
                  <Point X="0.212431259155" Y="29.20534375" />
                  <Point X="0.219973724365" Y="29.218916015625" />
                  <Point X="0.232747116089" Y="29.247107421875" />
                  <Point X="0.23797819519" Y="29.2617265625" />
                  <Point X="0.263748962402" Y="29.357904296875" />
                  <Point X="0.378190673828" Y="29.7850078125" />
                  <Point X="0.754457458496" Y="29.7456015625" />
                  <Point X="0.827851989746" Y="29.737916015625" />
                  <Point X="1.37932800293" Y="29.604771484375" />
                  <Point X="1.453622070312" Y="29.586833984375" />
                  <Point X="1.811370849609" Y="29.457076171875" />
                  <Point X="1.858249023438" Y="29.440072265625" />
                  <Point X="2.205339599609" Y="29.277751953125" />
                  <Point X="2.250428466797" Y="29.2566640625" />
                  <Point X="2.585796875" Y="29.061279296875" />
                  <Point X="2.629447021484" Y="29.03584765625" />
                  <Point X="2.817780273438" Y="28.901916015625" />
                  <Point X="2.812031738281" Y="28.891958984375" />
                  <Point X="2.065309082031" Y="27.59859765625" />
                  <Point X="2.062374267578" Y="27.593107421875" />
                  <Point X="2.053183105469" Y="27.573443359375" />
                  <Point X="2.044182617188" Y="27.54956640625" />
                  <Point X="2.041301391602" Y="27.54059765625" />
                  <Point X="2.022694946289" Y="27.471017578125" />
                  <Point X="2.018129394531" Y="27.448626953125" />
                  <Point X="2.013928466797" Y="27.419017578125" />
                  <Point X="2.012991821289" Y="27.406666015625" />
                  <Point X="2.012733276367" Y="27.381947265625" />
                  <Point X="2.013411254883" Y="27.369580078125" />
                  <Point X="2.020666381836" Y="27.3094140625" />
                  <Point X="2.023800537109" Y="27.289037109375" />
                  <Point X="2.029143310547" Y="27.26711328125" />
                  <Point X="2.032468261719" Y="27.25630859375" />
                  <Point X="2.040736328125" Y="27.23421484375" />
                  <Point X="2.045320922852" Y="27.2238828125" />
                  <Point X="2.055683105469" Y="27.20383984375" />
                  <Point X="2.061460449219" Y="27.19412890625" />
                  <Point X="2.098689453125" Y="27.13926171875" />
                  <Point X="2.112943359375" Y="27.1208046875" />
                  <Point X="2.131970458984" Y="27.09906640625" />
                  <Point X="2.14052734375" Y="27.09046484375" />
                  <Point X="2.158671386719" Y="27.074421875" />
                  <Point X="2.168258056641" Y="27.06698046875" />
                  <Point X="2.223105712891" Y="27.029763671875" />
                  <Point X="2.241256103516" Y="27.0182578125" />
                  <Point X="2.261310302734" Y="27.007888671875" />
                  <Point X="2.271643798828" Y="27.003302734375" />
                  <Point X="2.293739257812" Y="26.995033203125" />
                  <Point X="2.304545166016" Y="26.99170703125" />
                  <Point X="2.326472412109" Y="26.98636328125" />
                  <Point X="2.33759375" Y="26.984345703125" />
                  <Point X="2.397755126953" Y="26.977091796875" />
                  <Point X="2.421988037109" Y="26.975728515625" />
                  <Point X="2.450153808594" Y="26.97594921875" />
                  <Point X="2.462157470703" Y="26.976806640625" />
                  <Point X="2.485956054688" Y="26.980029296875" />
                  <Point X="2.497750976562" Y="26.98239453125" />
                  <Point X="2.567330566406" Y="27.001001953125" />
                  <Point X="2.580230957031" Y="27.004947265625" />
                  <Point X="2.603020263672" Y="27.0128125" />
                  <Point X="2.611452392578" Y="27.016181640625" />
                  <Point X="2.636033447266" Y="27.02787109375" />
                  <Point X="2.835054931641" Y="27.142775390625" />
                  <Point X="3.940404785156" Y="27.78094921875" />
                  <Point X="4.018067382812" Y="27.673015625" />
                  <Point X="4.043958007812" Y="27.63703515625" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.172952392578" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116455078" Y="26.725212890625" />
                  <Point X="3.134668212891" Y="26.7066015625" />
                  <Point X="3.128576904297" Y="26.699421875" />
                  <Point X="3.078500488281" Y="26.63409375" />
                  <Point X="3.066299316406" Y="26.61613671875" />
                  <Point X="3.050124023438" Y="26.5891328125" />
                  <Point X="3.044352050781" Y="26.577853515625" />
                  <Point X="3.034360107422" Y="26.554623046875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.011486572266" Y="26.475970703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893554688" Y="26.3975390625" />
                  <Point X="3.001175048828" Y="26.386240234375" />
                  <Point X="3.003078125" Y="26.363755859375" />
                  <Point X="3.004699707031" Y="26.3525703125" />
                  <Point X="3.020012207031" Y="26.278357421875" />
                  <Point X="3.025758789062" Y="26.256919921875" />
                  <Point X="3.035314453125" Y="26.228119140625" />
                  <Point X="3.039985595703" Y="26.2166171875" />
                  <Point X="3.050788330078" Y="26.194318359375" />
                  <Point X="3.056919921875" Y="26.183521484375" />
                  <Point X="3.098568603516" Y="26.120216796875" />
                  <Point X="3.111741210938" Y="26.101419921875" />
                  <Point X="3.126296142578" Y="26.084173828125" />
                  <Point X="3.134087402344" Y="26.075984375" />
                  <Point X="3.151327880859" Y="26.0598984375" />
                  <Point X="3.160035400391" Y="26.052693359375" />
                  <Point X="3.178243652344" Y="26.039369140625" />
                  <Point X="3.187744384766" Y="26.03325" />
                  <Point X="3.248099121094" Y="25.999275390625" />
                  <Point X="3.268958496094" Y="25.989158203125" />
                  <Point X="3.296223144531" Y="25.977931640625" />
                  <Point X="3.307874511719" Y="25.973994140625" />
                  <Point X="3.331598144531" Y="25.96765625" />
                  <Point X="3.343670410156" Y="25.965255859375" />
                  <Point X="3.425273925781" Y="25.954470703125" />
                  <Point X="3.437875488281" Y="25.953232421875" />
                  <Point X="3.463510253906" Y="25.9515703125" />
                  <Point X="3.472796630859" Y="25.951423828125" />
                  <Point X="3.500602539062" Y="25.952796875" />
                  <Point X="3.689659667969" Y="25.977685546875" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.74156640625" Y="25.959896484375" />
                  <Point X="4.752684570312" Y="25.914228515625" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="3.691991455078" Y="25.421353515625" />
                  <Point X="3.686020996094" Y="25.419541015625" />
                  <Point X="3.665627929688" Y="25.41213671875" />
                  <Point X="3.642386474609" Y="25.40162109375" />
                  <Point X="3.634007568359" Y="25.397318359375" />
                  <Point X="3.553834472656" Y="25.350978515625" />
                  <Point X="3.536522949219" Y="25.339673828125" />
                  <Point X="3.509874511719" Y="25.320123046875" />
                  <Point X="3.499989501953" Y="25.31177734375" />
                  <Point X="3.481450439453" Y="25.293828125" />
                  <Point X="3.472796386719" Y="25.284224609375" />
                  <Point X="3.424692626953" Y="25.222927734375" />
                  <Point X="3.41085546875" Y="25.204208984375" />
                  <Point X="3.399132568359" Y="25.184931640625" />
                  <Point X="3.393844726562" Y="25.17494140625" />
                  <Point X="3.384070068359" Y="25.1534765625" />
                  <Point X="3.380006103516" Y="25.1429296875" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.354341552734" Y="25.02674609375" />
                  <Point X="3.351523681641" Y="25.00575390625" />
                  <Point X="3.349056396484" Y="24.97405859375" />
                  <Point X="3.348917480469" Y="24.961390625" />
                  <Point X="3.350326416016" Y="24.9361484375" />
                  <Point X="3.351874267578" Y="24.92357421875" />
                  <Point X="3.367908935547" Y="24.83984765625" />
                  <Point X="3.373159912109" Y="24.8160078125" />
                  <Point X="3.3800078125" Y="24.79450390625" />
                  <Point X="3.384072265625" Y="24.78395703125" />
                  <Point X="3.393846679688" Y="24.762494140625" />
                  <Point X="3.399134033203" Y="24.752505859375" />
                  <Point X="3.410855957031" Y="24.73323046875" />
                  <Point X="3.417290527344" Y="24.723943359375" />
                  <Point X="3.465394287109" Y="24.662646484375" />
                  <Point X="3.480088623047" Y="24.646017578125" />
                  <Point X="3.501802246094" Y="24.624166015625" />
                  <Point X="3.511221923828" Y="24.615865234375" />
                  <Point X="3.531067871094" Y="24.600580078125" />
                  <Point X="3.541494140625" Y="24.593595703125" />
                  <Point X="3.621667236328" Y="24.54725390625" />
                  <Point X="3.632170654297" Y="24.54161328125" />
                  <Point X="3.656588134766" Y="24.529474609375" />
                  <Point X="3.665270507812" Y="24.525685546875" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="3.865366455078" Y="24.46962890625" />
                  <Point X="4.784876464844" Y="24.223248046875" />
                  <Point X="4.767820800781" Y="24.110119140625" />
                  <Point X="4.761612792969" Y="24.06894140625" />
                  <Point X="4.727802734375" Y="23.92078125" />
                  <Point X="3.436783203125" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400096191406" Y="24.09195703125" />
                  <Point X="3.366721435547" Y="24.089158203125" />
                  <Point X="3.354482666016" Y="24.087322265625" />
                  <Point X="3.197131347656" Y="24.05312109375" />
                  <Point X="3.172918457031" Y="24.047859375" />
                  <Point X="3.157877197266" Y="24.0432578125" />
                  <Point X="3.128759277344" Y="24.031634765625" />
                  <Point X="3.114682617188" Y="24.02461328125" />
                  <Point X="3.086854003906" Y="24.007720703125" />
                  <Point X="3.074126708984" Y="23.998470703125" />
                  <Point X="3.050374267578" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.944240234375" Y="23.852388671875" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.861525878906" Y="23.555205078125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607910156" Y="23.485408203125" />
                  <Point X="2.86406640625" Y="23.469869140625" />
                  <Point X="2.871798339844" Y="23.438248046875" />
                  <Point X="2.876787109375" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.983621582031" Y="23.2451796875" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001739501953" Y="23.217650390625" />
                  <Point X="3.019789550781" Y="23.1955546875" />
                  <Point X="3.043487548828" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.213688476562" Y="23.040263671875" />
                  <Point X="4.087171142578" Y="22.370015625" />
                  <Point X="4.062986572266" Y="22.3308828125" />
                  <Point X="4.045506591797" Y="22.302595703125" />
                  <Point X="4.001274658203" Y="22.239748046875" />
                  <Point X="2.848454345703" Y="22.905330078125" />
                  <Point X="2.841185791016" Y="22.9091171875" />
                  <Point X="2.815036132812" Y="22.920482421875" />
                  <Point X="2.783124023438" Y="22.930673828125" />
                  <Point X="2.771107177734" Y="22.9336640625" />
                  <Point X="2.583834228516" Y="22.967486328125" />
                  <Point X="2.555016845703" Y="22.972689453125" />
                  <Point X="2.539351318359" Y="22.974193359375" />
                  <Point X="2.507994140625" Y="22.974595703125" />
                  <Point X="2.492302490234" Y="22.973494140625" />
                  <Point X="2.460131835938" Y="22.96853515625" />
                  <Point X="2.444840087891" Y="22.964861328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.400589355469" Y="22.948890625" />
                  <Point X="2.245011474609" Y="22.86701171875" />
                  <Point X="2.221071289062" Y="22.854412109375" />
                  <Point X="2.20896875" Y="22.846830078125" />
                  <Point X="2.186038574219" Y="22.8299375" />
                  <Point X="2.1752109375" Y="22.820626953125" />
                  <Point X="2.154251464844" Y="22.79966796875" />
                  <Point X="2.144940429687" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120463867188" Y="22.7538046875" />
                  <Point X="2.038584350586" Y="22.5982265625" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.559806640625" />
                  <Point X="2.010012573242" Y="22.53003125" />
                  <Point X="2.006337890625" Y="22.514736328125" />
                  <Point X="2.001379760742" Y="22.4825625" />
                  <Point X="2.000279418945" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.036009155273" Y="22.2325859375" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.195779296875" />
                  <Point X="2.051236816406" Y="22.168453125" />
                  <Point X="2.0640703125" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.172936523438" Y="21.947345703125" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.721956542969" Y="20.9662578125" />
                  <Point X="1.833915039062" Y="22.123576171875" />
                  <Point X="1.828654663086" Y="22.1298515625" />
                  <Point X="1.808831542969" Y="22.150373046875" />
                  <Point X="1.783250732422" Y="22.17199609375" />
                  <Point X="1.773297851562" Y="22.179353515625" />
                  <Point X="1.588596313477" Y="22.298099609375" />
                  <Point X="1.560174438477" Y="22.31637109375" />
                  <Point X="1.546280273438" Y="22.32375390625" />
                  <Point X="1.517467651367" Y="22.336125" />
                  <Point X="1.502549316406" Y="22.34111328125" />
                  <Point X="1.470931152344" Y="22.34884375" />
                  <Point X="1.455393310547" Y="22.351302734375" />
                  <Point X="1.424126098633" Y="22.35362109375" />
                  <Point X="1.408396362305" Y="22.35348046875" />
                  <Point X="1.206393798828" Y="22.334892578125" />
                  <Point X="1.175309692383" Y="22.332033203125" />
                  <Point X="1.161230712891" Y="22.32966015625" />
                  <Point X="1.133583129883" Y="22.322830078125" />
                  <Point X="1.120014526367" Y="22.318373046875" />
                  <Point X="1.092626220703" Y="22.307029296875" />
                  <Point X="1.079881469727" Y="22.30058984375" />
                  <Point X="1.055496704102" Y="22.285869140625" />
                  <Point X="1.043857055664" Y="22.277587890625" />
                  <Point X="0.887875793457" Y="22.14789453125" />
                  <Point X="0.863873352051" Y="22.127935546875" />
                  <Point X="0.852649047852" Y="22.116908203125" />
                  <Point X="0.832176391602" Y="22.093154296875" />
                  <Point X="0.822928161621" Y="22.080427734375" />
                  <Point X="0.806037353516" Y="22.0526015625" />
                  <Point X="0.799016540527" Y="22.038525390625" />
                  <Point X="0.787393798828" Y="22.009408203125" />
                  <Point X="0.782791809082" Y="21.9943671875" />
                  <Point X="0.73615411377" Y="21.779798828125" />
                  <Point X="0.728977539063" Y="21.74678125" />
                  <Point X="0.727584655762" Y="21.7387109375" />
                  <Point X="0.72472454834" Y="21.710322265625" />
                  <Point X="0.724742248535" Y="21.676830078125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.754854919434" Y="21.44192578125" />
                  <Point X="0.833091918945" Y="20.847658203125" />
                  <Point X="0.655064880371" Y="21.512064453125" />
                  <Point X="0.652606323242" Y="21.519876953125" />
                  <Point X="0.642145202637" Y="21.546419921875" />
                  <Point X="0.626787902832" Y="21.57618359375" />
                  <Point X="0.620407958984" Y="21.586791015625" />
                  <Point X="0.478514709473" Y="21.79123046875" />
                  <Point X="0.456680206299" Y="21.82269140625" />
                  <Point X="0.446670166016" Y="21.834828125" />
                  <Point X="0.424789611816" Y="21.857279296875" />
                  <Point X="0.412919250488" Y="21.86759375" />
                  <Point X="0.386663360596" Y="21.88683984375" />
                  <Point X="0.373248962402" Y="21.89505859375" />
                  <Point X="0.345244384766" Y="21.909169921875" />
                  <Point X="0.33065423584" Y="21.9150625" />
                  <Point X="0.111082633972" Y="21.983208984375" />
                  <Point X="0.077295150757" Y="21.9936953125" />
                  <Point X="0.063377067566" Y="21.996890625" />
                  <Point X="0.035217784882" Y="22.00116015625" />
                  <Point X="0.020976589203" Y="22.002234375" />
                  <Point X="-0.00866476059" Y="22.002234375" />
                  <Point X="-0.022905954361" Y="22.00116015625" />
                  <Point X="-0.051065238953" Y="21.996890625" />
                  <Point X="-0.064983322144" Y="21.9936953125" />
                  <Point X="-0.284554779053" Y="21.925548828125" />
                  <Point X="-0.318342254639" Y="21.9150625" />
                  <Point X="-0.332930938721" Y="21.909169921875" />
                  <Point X="-0.360932830811" Y="21.895060546875" />
                  <Point X="-0.374345733643" Y="21.886845703125" />
                  <Point X="-0.400601165771" Y="21.8676015625" />
                  <Point X="-0.412473022461" Y="21.85728515625" />
                  <Point X="-0.434356262207" Y="21.83483203125" />
                  <Point X="-0.444367919922" Y="21.822693359375" />
                  <Point X="-0.586261169434" Y="21.618251953125" />
                  <Point X="-0.60809552002" Y="21.58679296875" />
                  <Point X="-0.612471862793" Y="21.5798671875" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777832031" Y="21.52378515625" />
                  <Point X="-0.642753051758" Y="21.512064453125" />
                  <Point X="-0.69617388916" Y="21.3126953125" />
                  <Point X="-0.985425231934" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.833119537692" Y="21.054693868382" />
                  <Point X="-2.454071814995" Y="20.901548647569" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.128438168735" Y="20.36595788863" />
                  <Point X="-0.967296102938" Y="20.300852267963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.958206665026" Y="21.207693148893" />
                  <Point X="-2.391444838926" Y="20.978706507347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119888308877" Y="20.464964321575" />
                  <Point X="-0.942523669291" Y="20.393304355646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.91023984488" Y="21.290774096138" />
                  <Point X="-2.30306233252" Y="21.045458457408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.137719982917" Y="20.574629586092" />
                  <Point X="-0.917751235645" Y="20.48575644333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.862273024734" Y="21.373855043384" />
                  <Point X="-2.207501501858" Y="21.109310176212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.159881665086" Y="20.686044287451" />
                  <Point X="-0.892978801999" Y="20.578208531013" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.814306204587" Y="21.456935990629" />
                  <Point X="-2.111940671196" Y="21.173161895017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.182043285768" Y="20.797458963968" />
                  <Point X="-0.868206368353" Y="20.670660618697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.766339384441" Y="21.540016937875" />
                  <Point X="-1.979546746482" Y="21.222132077845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.248238769977" Y="20.926664476175" />
                  <Point X="-0.843433934707" Y="20.763112706381" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.875549260093" Y="22.090627618148" />
                  <Point X="-3.716641792003" Y="22.026424833558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.718372564295" Y="21.62309788512" />
                  <Point X="-1.689136437648" Y="21.207259497378" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.459890022337" Y="21.114637933413" />
                  <Point X="-0.818661501061" Y="20.855564794064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.988171891692" Y="22.238590915491" />
                  <Point X="-3.612236626325" Y="22.086703209066" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.670405744148" Y="21.706178832365" />
                  <Point X="-0.793889067415" Y="20.948016881748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073190842188" Y="22.375401601738" />
                  <Point X="-3.507831460648" Y="22.146981584574" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.622438924002" Y="21.789259779611" />
                  <Point X="-0.769116633768" Y="21.040468969431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153689749108" Y="22.510386071839" />
                  <Point X="-3.40342629497" Y="22.207259960082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.574472103856" Y="21.872340726856" />
                  <Point X="-0.744344200122" Y="21.132921057115" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12375626611" Y="22.600752960232" />
                  <Point X="-3.299021129292" Y="22.26753833559" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.526505283709" Y="21.955421674101" />
                  <Point X="-0.719571766476" Y="21.225373144798" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036284041354" Y="22.667872687953" />
                  <Point X="-3.194615963615" Y="22.327816711098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.478538463563" Y="22.038502621347" />
                  <Point X="-0.694799330536" Y="21.317825231555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.948811816598" Y="22.734992415674" />
                  <Point X="-3.090210797937" Y="22.388095086606" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.430571643417" Y="22.121583568592" />
                  <Point X="-0.670026855539" Y="21.410277302532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.861339591842" Y="22.802112143394" />
                  <Point X="-2.985805632259" Y="22.448373462114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.382604823271" Y="22.204664515837" />
                  <Point X="-0.645254380543" Y="21.502729373509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.815730128445" Y="20.912453316339" />
                  <Point X="0.82505759916" Y="20.908684773549" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.773867367086" Y="22.869231871115" />
                  <Point X="-2.881400466582" Y="22.508651837622" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.335678265419" Y="22.288165756331" />
                  <Point X="-0.60626117556" Y="21.589435896621" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.784942871113" Y="21.027352976277" />
                  <Point X="0.810810527655" Y="21.016901764633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.686395142329" Y="22.936351598836" />
                  <Point X="-2.776995699718" Y="22.568930374261" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311634186674" Y="22.380912118497" />
                  <Point X="-0.550721879986" Y="21.669457365199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.75415561378" Y="21.142252636215" />
                  <Point X="0.796563456151" Y="21.125118755717" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.598922917573" Y="23.003471326557" />
                  <Point X="-2.670700821004" Y="22.628445256143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.331875212818" Y="22.491550824451" />
                  <Point X="-0.495182681757" Y="21.749478873107" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.723368356448" Y="21.257152296154" />
                  <Point X="0.782316384646" Y="21.2333357468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.511450692817" Y="23.070591054277" />
                  <Point X="-0.438976069722" Y="21.829230728334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692581099116" Y="21.372051956092" />
                  <Point X="0.768069313141" Y="21.341552737884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423978468061" Y="23.137710781998" />
                  <Point X="-0.355316049689" Y="21.897890686741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.661793841783" Y="21.48695161603" />
                  <Point X="0.753822244814" Y="21.449769727684" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.668502519918" Y="23.742991938185" />
                  <Point X="-4.61122261125" Y="23.71984935287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.336506243305" Y="23.204830509719" />
                  <Point X="-0.219865450184" Y="21.94562589279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.601751596067" Y="21.613671058512" />
                  <Point X="0.739575217154" Y="21.557986701053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.697686163968" Y="23.857243696301" />
                  <Point X="-4.419949301591" Y="23.74503072002" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.249034018549" Y="23.27195023744" />
                  <Point X="-0.076440774043" Y="21.990139362752" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.502924792717" Y="21.756060479435" />
                  <Point X="0.725444737946" Y="21.666156585791" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726869778531" Y="23.971495442504" />
                  <Point X="-4.228675991932" Y="23.770212087169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.161561793793" Y="23.33906996516" />
                  <Point X="0.733055407557" Y="21.765542476226" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.748910786638" Y="24.082861388377" />
                  <Point X="-4.037402682273" Y="23.795393454319" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.074089569037" Y="23.406189692881" />
                  <Point X="0.75352801674" Y="21.859731805759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.612773579245" Y="21.10854783824" />
                  <Point X="2.670737582022" Y="21.085128860963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.764463828077" Y="24.191606025565" />
                  <Point X="-3.846129372614" Y="23.820574821469" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.000702582962" Y="23.479000226426" />
                  <Point X="0.774000622016" Y="21.953921136871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.498826871204" Y="21.25704609719" />
                  <Point X="2.593584806473" Y="21.218761406236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.780016869517" Y="24.300350662752" />
                  <Point X="-3.654856062955" Y="23.845756188619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.959678941855" Y="23.564886400094" />
                  <Point X="0.802233489902" Y="22.044975118369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.384880163163" Y="21.405544356141" />
                  <Point X="2.516432030924" Y="21.352393951508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.697059393799" Y="24.369294467487" />
                  <Point X="-3.463582753296" Y="23.870937555769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949719680661" Y="23.663323397936" />
                  <Point X="0.859975889075" Y="22.124106475315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.270933455123" Y="21.554042615091" />
                  <Point X="2.439279255375" Y="21.48602649678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.544582602641" Y="24.410150645583" />
                  <Point X="-3.272309711806" Y="23.896119031266" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.991487275628" Y="23.782659402247" />
                  <Point X="0.942429245983" Y="22.19325395727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156986747082" Y="21.702540874041" />
                  <Point X="2.362126479826" Y="21.619659042052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.392105811483" Y="24.451006823678" />
                  <Point X="1.025360298969" Y="22.262208437482" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043040039042" Y="21.851039132992" />
                  <Point X="2.284973704277" Y="21.753291587324" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.239629020325" Y="24.491863001773" />
                  <Point X="1.131008818785" Y="22.32198446531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.929093331001" Y="21.999537391942" />
                  <Point X="2.207820928728" Y="21.886924132596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.087152229166" Y="24.532719179868" />
                  <Point X="1.325526659647" Y="22.345854956763" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.80834642515" Y="22.150783109149" />
                  <Point X="2.130667689349" Y="22.020556865268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.934675438008" Y="24.573575357964" />
                  <Point X="2.05788683791" Y="22.152423038543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.78219864685" Y="24.614431536059" />
                  <Point X="2.029943242815" Y="22.266173784359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.629721855692" Y="24.655287714154" />
                  <Point X="2.009982518724" Y="22.376699240933" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.481375900383" Y="24.697812858267" />
                  <Point X="2.001390391806" Y="22.482631486098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.384181770629" Y="24.761004681404" />
                  <Point X="2.026361845973" Y="22.575003164272" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.32599420758" Y="24.839956180469" />
                  <Point X="2.070830691874" Y="22.659497384849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.298016610015" Y="24.931113297871" />
                  <Point X="2.115299358989" Y="22.743991677661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.779482586629" Y="25.63212520566" />
                  <Point X="-4.471591950774" Y="25.507729314086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.303227303342" Y="25.035679355184" />
                  <Point X="2.176492414562" Y="22.821728878925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.76517634439" Y="25.728805909157" />
                  <Point X="-3.718630696776" Y="25.305974020987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.37439211438" Y="25.166892605755" />
                  <Point X="2.276715210052" Y="22.883697041675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.75087010215" Y="25.825486612653" />
                  <Point X="2.386850799193" Y="22.941660175819" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.714861288347" Y="22.405109110016" />
                  <Point X="4.028477660159" Y="22.278399870953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.736563920781" Y="25.922167340743" />
                  <Point X="2.570625260052" Y="22.969871274547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.123712304388" Y="22.746409603466" />
                  <Point X="4.080807186236" Y="22.359718170587" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.715459652969" Y="26.016101463624" />
                  <Point X="3.840566112443" Y="22.559242665476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.690434575177" Y="26.108451476447" />
                  <Point X="3.55853971414" Y="22.775649527323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.665409497385" Y="26.20080148927" />
                  <Point X="3.276513315837" Y="22.99205638917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.640384419593" Y="26.293151502092" />
                  <Point X="3.016514820662" Y="23.199563400452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.359590302459" Y="26.282164115265" />
                  <Point X="2.922709356665" Y="23.339924068588" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.983413682689" Y="26.232639695886" />
                  <Point X="2.865094985483" Y="23.465662586085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.681145960111" Y="26.212976409295" />
                  <Point X="2.862799946488" Y="23.569050642583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553147176891" Y="26.263722344554" />
                  <Point X="2.871890410608" Y="23.667838657227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.481876713834" Y="26.337388008906" />
                  <Point X="2.890530993712" Y="23.762768173343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.44183302706" Y="26.423670109824" />
                  <Point X="2.938726079814" Y="23.845756895156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.421315935718" Y="26.517841467399" />
                  <Point X="3.002496340558" Y="23.922452837941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.446537255884" Y="26.63049234275" />
                  <Point X="3.072153784716" Y="23.996770204231" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551169567703" Y="26.775227341349" />
                  <Point X="3.190075107673" Y="24.051587697726" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.833197363262" Y="26.991634767724" />
                  <Point X="3.355026434382" Y="24.087403836303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115225332162" Y="27.208042264133" />
                  <Point X="3.679414743687" Y="24.058803252544" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.198506218389" Y="27.344150726834" />
                  <Point X="4.05559081662" Y="24.009279054102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.150113577875" Y="27.427059631483" />
                  <Point X="4.431766889554" Y="23.959754855659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.101720937361" Y="27.509968536132" />
                  <Point X="4.732363419136" Y="23.940766774867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.053328296846" Y="27.592877440781" />
                  <Point X="4.753771150605" Y="24.034578290473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.004935022104" Y="27.675786089186" />
                  <Point X="3.472361243112" Y="24.654762299699" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.058997573699" Y="24.417745837114" />
                  <Point X="4.770839654921" Y="24.130142967648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.944953768649" Y="27.754012890286" />
                  <Point X="3.379995970492" Y="24.794541092749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.884304263426" Y="27.831969700146" />
                  <Point X="3.355031548076" Y="24.907088174672" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.823654758203" Y="27.909926510006" />
                  <Point X="3.352187362041" Y="25.010698100976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.763005252979" Y="27.987883319867" />
                  <Point X="-3.368949690597" Y="27.828674538228" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.11998561826" Y="27.728086523713" />
                  <Point X="3.369547418088" Y="25.106144983605" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.931463389203" Y="27.754379399575" />
                  <Point X="3.404854456736" Y="25.194340814589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.848596370034" Y="27.823359751129" />
                  <Point X="3.463934201746" Y="25.272931848744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.783657188079" Y="27.899583419089" />
                  <Point X="3.542569130088" Y="25.343622075981" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.751539732612" Y="27.989067925328" />
                  <Point X="3.647212004767" Y="25.403804410818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.752971405117" Y="28.092107159121" />
                  <Point X="3.791428778798" Y="25.447997852459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.77579878412" Y="28.203790819459" />
                  <Point X="3.9439053608" Y="25.488854115059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.849235841068" Y="28.335922116969" />
                  <Point X="4.096381942801" Y="25.529710377659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.926388761468" Y="28.469554720765" />
                  <Point X="4.248858524803" Y="25.570566640259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.003541681868" Y="28.60318732456" />
                  <Point X="3.082834870152" Y="26.144131577236" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.542690954133" Y="25.958337659198" />
                  <Point X="4.401335106805" Y="25.611422902859" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.035216278541" Y="28.718445492863" />
                  <Point X="3.021931080449" Y="26.271199106083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.733965374141" Y="25.983518577738" />
                  <Point X="4.553811688806" Y="25.652279165459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.947696546643" Y="28.785546026453" />
                  <Point X="3.001542628097" Y="26.381897376092" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.925237636153" Y="26.008700368164" />
                  <Point X="4.706288270808" Y="25.693135428059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.860176814745" Y="28.852646560042" />
                  <Point X="3.012584578409" Y="26.479896939136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.116509898166" Y="26.033882158591" />
                  <Point X="4.775509434361" Y="25.767629063155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.772657082847" Y="28.919747093632" />
                  <Point X="3.041299345717" Y="26.570756220629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.307782160178" Y="26.059063949018" />
                  <Point X="4.758485889815" Y="25.876967822163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.685136636219" Y="28.986847338452" />
                  <Point X="3.0926015684" Y="26.652489577776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.499054422191" Y="26.084245739444" />
                  <Point X="4.7344471064" Y="25.989140921654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586450354805" Y="29.049436293185" />
                  <Point X="2.082559296748" Y="27.16303394528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.516949778398" Y="26.98752879844" />
                  <Point X="3.156432275046" Y="26.729161098832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.690326684203" Y="26.109427529871" />
                  <Point X="4.706782980036" Y="26.102778754774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.479676605452" Y="29.10875769877" />
                  <Point X="2.023768111576" Y="27.289247926492" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.650304869251" Y="27.036110644941" />
                  <Point X="3.242211660719" Y="26.796964777938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.3729028561" Y="29.168079104355" />
                  <Point X="2.012881382253" Y="27.396107251206" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754710493764" Y="27.096388835068" />
                  <Point X="3.329683868149" Y="26.864084512659" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.266129106747" Y="29.22740050994" />
                  <Point X="-2.113142002776" Y="29.165589707721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.787387096734" Y="29.033976182486" />
                  <Point X="2.028386993303" Y="27.492303378249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.859116023004" Y="27.156667063687" />
                  <Point X="3.417156075579" Y="26.93120424738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.654329346563" Y="29.082678162421" />
                  <Point X="2.057626836786" Y="27.582950515197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963521234112" Y="27.21694542084" />
                  <Point X="3.504628283009" Y="26.998323982101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.558477867708" Y="29.146412451733" />
                  <Point X="2.104497670667" Y="27.666474269637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.06792644522" Y="27.277223777993" />
                  <Point X="3.59210049044" Y="27.065443716822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.508189223752" Y="29.228555321267" />
                  <Point X="2.15246448793" Y="27.749555218047" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.172331656328" Y="27.337502135146" />
                  <Point X="3.67957269787" Y="27.132563451543" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.479057429729" Y="29.319246113031" />
                  <Point X="2.200431305192" Y="27.832636166457" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.276736867436" Y="27.397780492298" />
                  <Point X="3.7670449053" Y="27.199683186264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.462361764423" Y="29.414961426943" />
                  <Point X="2.248398122455" Y="27.915717114868" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.381142078545" Y="27.458058849451" />
                  <Point X="3.85451711273" Y="27.266802920985" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.473926500951" Y="29.52209468435" />
                  <Point X="2.296364939718" Y="27.998798063278" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.485547289653" Y="27.518337206604" />
                  <Point X="3.94198932016" Y="27.333922655706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.3856694036" Y="29.588897302959" />
                  <Point X="2.34433175698" Y="28.081879011689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.589952500761" Y="27.578615563757" />
                  <Point X="4.02946152759" Y="27.401042390427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.235957940674" Y="29.630870746182" />
                  <Point X="-0.205150500426" Y="29.214397506536" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.081248208527" Y="29.098684917074" />
                  <Point X="2.392298574243" Y="28.164959960099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.694357711869" Y="27.63889392091" />
                  <Point X="4.11693373502" Y="27.468162125148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.086246477747" Y="29.672844189406" />
                  <Point X="-0.244722874685" Y="29.33284658411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.180031345041" Y="29.161234739807" />
                  <Point X="2.440265391506" Y="28.248040908509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.798762922977" Y="27.699172278063" />
                  <Point X="4.073535654119" Y="27.588156888537" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.936532134347" Y="29.714816468843" />
                  <Point X="-0.27551006728" Y="29.447746217893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.23094308956" Y="29.243125860372" />
                  <Point X="2.488232208768" Y="28.33112185692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.903168134085" Y="27.759450635215" />
                  <Point X="3.97747623869" Y="27.729428212163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.74157651764" Y="29.738510087374" />
                  <Point X="-0.306297259876" Y="29.562645851676" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.257566271051" Y="29.334830197389" />
                  <Point X="2.536199026031" Y="28.41420280533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.544939747265" Y="29.761524475734" />
                  <Point X="-0.337084452472" Y="29.677545485459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.282338686562" Y="29.4272822924" />
                  <Point X="2.584165843294" Y="28.497283753741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.307111081417" Y="29.519734395756" />
                  <Point X="2.632132660556" Y="28.580364702151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.331883476272" Y="29.612186499112" />
                  <Point X="2.680099477819" Y="28.663445650561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.356655871127" Y="29.704638602468" />
                  <Point X="2.728066295082" Y="28.746526598972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.422932117422" Y="29.780322061369" />
                  <Point X="2.776033112344" Y="28.829607547382" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765270192864" Y="29.744469301344" />
                  <Point X="2.774521685661" Y="28.932679003955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.342814529431" Y="29.613587043342" />
                  <Point X="2.305178917697" Y="29.224766591673" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001626953125" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.818699462891" Y="20.167267578125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.46431942749" Y="21.478455078125" />
                  <Point X="0.322426269531" Y="21.68289453125" />
                  <Point X="0.300591796875" Y="21.71435546875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.054764087677" Y="21.801748046875" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.22823626709" Y="21.744087890625" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.430172332764" Y="21.509916015625" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.512647949219" Y="21.26351953125" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.061745239258" Y="20.0544609375" />
                  <Point X="-1.100260131836" Y="20.0619375" />
                  <Point X="-1.35158972168" Y="20.1266015625" />
                  <Point X="-1.350940185547" Y="20.13153515625" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.362138793945" Y="20.728953125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282104492" Y="20.79737109375" />
                  <Point X="-1.588435668945" Y="20.974654296875" />
                  <Point X="-1.61954284668" Y="21.001935546875" />
                  <Point X="-1.649241821289" Y="21.014236328125" />
                  <Point X="-1.917544677734" Y="21.031822265625" />
                  <Point X="-1.958831054688" Y="21.03452734375" />
                  <Point X="-1.989878295898" Y="21.026208984375" />
                  <Point X="-2.213442382812" Y="20.876828125" />
                  <Point X="-2.247844482422" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.289725585938" Y="20.803619140625" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.799406738281" Y="20.797451171875" />
                  <Point X="-2.855835205078" Y="20.832388671875" />
                  <Point X="-3.208662597656" Y="21.1040546875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.151192138672" Y="21.25343359375" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513981689453" Y="22.43123828125" />
                  <Point X="-2.529016601562" Y="22.446271484375" />
                  <Point X="-2.531322753906" Y="22.448578125" />
                  <Point X="-2.560153320312" Y="22.462798828125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.763635498047" Y="22.357251953125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.117122070312" Y="22.094298828125" />
                  <Point X="-4.161701660156" Y="22.1528671875" />
                  <Point X="-4.414653808594" Y="22.577029296875" />
                  <Point X="-4.43101953125" Y="22.60447265625" />
                  <Point X="-4.292604980469" Y="22.710681640625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822509766" Y="23.603982421875" />
                  <Point X="-3.139145507812" Y="23.629763671875" />
                  <Point X="-3.138117919922" Y="23.63373046875" />
                  <Point X="-3.140325927734" Y="23.66540234375" />
                  <Point X="-3.161159179688" Y="23.689359375" />
                  <Point X="-3.184110107422" Y="23.7028671875" />
                  <Point X="-3.187646240234" Y="23.70494921875" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-3.436602539062" Y="23.682849609375" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.909956542969" Y="23.920544921875" />
                  <Point X="-4.927392578125" Y="23.988806640625" />
                  <Point X="-4.994317382812" Y="24.45673828125" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-4.843357910156" Y="24.526798828125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541899169922" Y="24.878572265625" />
                  <Point X="-3.517847412109" Y="24.895265625" />
                  <Point X="-3.514146240234" Y="24.897833984375" />
                  <Point X="-3.494899169922" Y="24.92408984375" />
                  <Point X="-3.486881835938" Y="24.949921875" />
                  <Point X="-3.485648193359" Y="24.953896484375" />
                  <Point X="-3.485647949219" Y="24.9835390625" />
                  <Point X="-3.493663330078" Y="25.009365234375" />
                  <Point X="-3.494896972656" Y="25.013341796875" />
                  <Point X="-3.514146240234" Y="25.03960546875" />
                  <Point X="-3.538197998047" Y="25.056298828125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.755336425781" Y="25.11910546875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.928881835938" Y="25.920482421875" />
                  <Point X="-4.917645019531" Y="25.996419921875" />
                  <Point X="-4.782929199219" Y="26.4935625" />
                  <Point X="-4.773516113281" Y="26.528298828125" />
                  <Point X="-4.675026367188" Y="26.51533203125" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731706054688" Y="26.3958671875" />
                  <Point X="-3.678471679688" Y="26.412650390625" />
                  <Point X="-3.670272460938" Y="26.415236328125" />
                  <Point X="-3.651531738281" Y="26.42605859375" />
                  <Point X="-3.639119628906" Y="26.443787109375" />
                  <Point X="-3.617759033203" Y="26.49535546875" />
                  <Point X="-3.614472167969" Y="26.503291015625" />
                  <Point X="-3.610714111328" Y="26.524607421875" />
                  <Point X="-3.616317871094" Y="26.545515625" />
                  <Point X="-3.642091552734" Y="26.59502734375" />
                  <Point X="-3.646057617188" Y="26.60264453125" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.773468505859" Y="26.7063125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.203676269531" Y="27.712203125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.803154541016" Y="28.24569921875" />
                  <Point X="-3.774670898438" Y="28.282310546875" />
                  <Point X="-3.725276367188" Y="28.25379296875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514404297" Y="27.92043359375" />
                  <Point X="-3.064373779297" Y="27.913947265625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.960627197266" Y="27.980029296875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899658203" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.944560791016" Y="28.10198046875" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.002362792969" Y="28.221146484375" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-2.828919433594" Y="29.116029296875" />
                  <Point X="-2.752873779297" Y="29.17433203125" />
                  <Point X="-2.190832275391" Y="29.48658984375" />
                  <Point X="-2.141548828125" Y="29.513970703125" />
                  <Point X="-2.141133056641" Y="29.5134296875" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.95125" Y="29.273662109375" />
                  <Point X="-1.868731811523" Y="29.230705078125" />
                  <Point X="-1.856034057617" Y="29.22409375" />
                  <Point X="-1.835125610352" Y="29.218490234375" />
                  <Point X="-1.813808227539" Y="29.22225" />
                  <Point X="-1.727860107422" Y="29.2578515625" />
                  <Point X="-1.714634277344" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083618164" Y="29.29448828125" />
                  <Point X="-1.658109130859" Y="29.383212890625" />
                  <Point X="-1.653804321289" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.657635620117" Y="29.461857421875" />
                  <Point X="-1.689137573242" Y="29.701140625" />
                  <Point X="-1.066537597656" Y="29.8756953125" />
                  <Point X="-0.968083862305" Y="29.903296875" />
                  <Point X="-0.286725280762" Y="29.983041015625" />
                  <Point X="-0.224199981689" Y="29.990359375" />
                  <Point X="-0.209614196777" Y="29.935923828125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282119751" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594039917" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.08022303009" Y="29.407080078125" />
                  <Point X="0.236648406982" Y="29.990869140625" />
                  <Point X="0.774248168945" Y="29.93456640625" />
                  <Point X="0.860210205078" Y="29.925564453125" />
                  <Point X="1.423919067383" Y="29.789466796875" />
                  <Point X="1.508456420898" Y="29.769056640625" />
                  <Point X="1.876155639648" Y="29.635689453125" />
                  <Point X="1.931040527344" Y="29.615783203125" />
                  <Point X="2.285828613281" Y="29.449861328125" />
                  <Point X="2.338686523438" Y="29.425140625" />
                  <Point X="2.681442382812" Y="29.225451171875" />
                  <Point X="2.732534912109" Y="29.19568359375" />
                  <Point X="3.055778808594" Y="28.965810546875" />
                  <Point X="3.068740722656" Y="28.956591796875" />
                  <Point X="2.976576171875" Y="28.796958984375" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491515625" />
                  <Point X="2.206245605469" Y="27.421935546875" />
                  <Point X="2.202044677734" Y="27.392326171875" />
                  <Point X="2.209299804688" Y="27.33216015625" />
                  <Point X="2.210416015625" Y="27.32290234375" />
                  <Point X="2.218684082031" Y="27.30080859375" />
                  <Point X="2.255913085938" Y="27.24594140625" />
                  <Point X="2.274940185547" Y="27.224203125" />
                  <Point X="2.329800048828" Y="27.186978515625" />
                  <Point X="2.338242431641" Y="27.181248046875" />
                  <Point X="2.360337890625" Y="27.172978515625" />
                  <Point X="2.420499267578" Y="27.165724609375" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.518244628906" Y="27.184552734375" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.740055419922" Y="27.307322265625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.172292480469" Y="27.78398828125" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.382801757813" Y="27.444080078125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.271746582031" Y="27.34746484375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.229294921875" Y="26.51850390625" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.194466064453" Y="26.424798828125" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779785156" Y="26.39096484375" />
                  <Point X="3.206092285156" Y="26.316751953125" />
                  <Point X="3.215647949219" Y="26.287951171875" />
                  <Point X="3.257296630859" Y="26.224646484375" />
                  <Point X="3.263705566406" Y="26.21490625" />
                  <Point X="3.280946044922" Y="26.1988203125" />
                  <Point X="3.34130078125" Y="26.164845703125" />
                  <Point X="3.368565429688" Y="26.153619140625" />
                  <Point X="3.450168945312" Y="26.142833984375" />
                  <Point X="3.450174316406" Y="26.14283203125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.664860839844" Y="26.166060546875" />
                  <Point X="4.848975097656" Y="26.321953125" />
                  <Point X="4.926174804688" Y="26.004837890625" />
                  <Point X="4.939189453125" Y="25.951380859375" />
                  <Point X="4.995977539062" Y="25.58663671875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.867855957031" Y="25.53972265625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087158203" Y="25.232818359375" />
                  <Point X="3.6489140625" Y="25.186478515625" />
                  <Point X="3.622265625" Y="25.166927734375" />
                  <Point X="3.574161865234" Y="25.105630859375" />
                  <Point X="3.566759765625" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.540950439453" Y="24.9910078125" />
                  <Point X="3.538483154297" Y="24.9593125" />
                  <Point X="3.554517822266" Y="24.8755859375" />
                  <Point X="3.556985351562" Y="24.862703125" />
                  <Point X="3.566759765625" Y="24.841240234375" />
                  <Point X="3.614863525391" Y="24.779943359375" />
                  <Point X="3.636577148438" Y="24.758091796875" />
                  <Point X="3.716750244141" Y="24.71175" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="3.914541748047" Y="24.65315625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.955697753906" Y="24.081794921875" />
                  <Point X="4.948431152344" Y="24.03359765625" />
                  <Point X="4.87567578125" Y="23.714771484375" />
                  <Point X="4.874546386719" Y="23.709822265625" />
                  <Point X="4.718624023438" Y="23.730349609375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394838134766" Y="23.901658203125" />
                  <Point X="3.237486816406" Y="23.86745703125" />
                  <Point X="3.213273925781" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.090336425781" Y="23.730916015625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.0507265625" Y="23.537794921875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360839844" Y="23.483376953125" />
                  <Point X="3.14344140625" Y="23.3479296875" />
                  <Point X="3.156841064453" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.329353027344" Y="23.191001953125" />
                  <Point X="4.339074707031" Y="22.41621484375" />
                  <Point X="4.224612792969" Y="22.231" />
                  <Point X="4.204136230469" Y="22.19786328125" />
                  <Point X="4.056687255859" Y="21.988361328125" />
                  <Point X="3.9171953125" Y="22.068896484375" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737338623047" Y="22.7466875" />
                  <Point X="2.550065673828" Y="22.780509765625" />
                  <Point X="2.521248291016" Y="22.785712890625" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.333499755859" Y="22.698875" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288600097656" Y="22.66531640625" />
                  <Point X="2.206720458984" Y="22.50973828125" />
                  <Point X="2.19412109375" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.222984375" Y="22.2663515625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.337480957031" Y="22.042345703125" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.859604980469" Y="20.8271484375" />
                  <Point X="2.835293945312" Y="20.80978515625" />
                  <Point X="2.679775878906" Y="20.709119140625" />
                  <Point X="2.571219238281" Y="20.85059375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548095703" Y="22.019533203125" />
                  <Point X="1.485846435547" Y="22.138279296875" />
                  <Point X="1.457424682617" Y="22.15655078125" />
                  <Point X="1.425806396484" Y="22.16428125" />
                  <Point X="1.223803710938" Y="22.145693359375" />
                  <Point X="1.192719604492" Y="22.142833984375" />
                  <Point X="1.165331176758" Y="22.131490234375" />
                  <Point X="1.009349853516" Y="22.001796875" />
                  <Point X="0.985347412109" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.921819091797" Y="21.739443359375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.943229309082" Y="21.4667265625" />
                  <Point X="1.127642333984" Y="20.06597265625" />
                  <Point X="1.017339904785" Y="20.04179296875" />
                  <Point X="0.994335083008" Y="20.036751953125" />
                  <Point X="0.860200317383" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#204" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.167473804092" Y="4.980145251278" Z="2.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="-0.298693537241" Y="5.063981769904" Z="2.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="2.2" />
                  <Point X="-1.08619104069" Y="4.95512000766" Z="2.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="2.2" />
                  <Point X="-1.713363905394" Y="4.486613220306" Z="2.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="2.2" />
                  <Point X="-1.711950597941" Y="4.4295278149" Z="2.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="2.2" />
                  <Point X="-1.753150804782" Y="4.335325806788" Z="2.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="2.2" />
                  <Point X="-1.851796909826" Y="4.306334713563" Z="2.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="2.2" />
                  <Point X="-2.10762148629" Y="4.575148572634" Z="2.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="2.2" />
                  <Point X="-2.221271493599" Y="4.56157817574" Z="2.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="2.2" />
                  <Point X="-2.865497500482" Y="4.186989596487" Z="2.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="2.2" />
                  <Point X="-3.051820029124" Y="3.227426993309" Z="2.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="2.2" />
                  <Point X="-3.000526543691" Y="3.128904197465" Z="2.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="2.2" />
                  <Point X="-3.002137998096" Y="3.04666556979" Z="2.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="2.2" />
                  <Point X="-3.066172268141" Y="2.995038155482" Z="2.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="2.2" />
                  <Point X="-3.706432354988" Y="3.328373919622" Z="2.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="2.2" />
                  <Point X="-3.848773961386" Y="3.307682059455" Z="2.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="2.2" />
                  <Point X="-4.253014422393" Y="2.768688229611" Z="2.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="2.2" />
                  <Point X="-3.810062415468" Y="1.697925821193" Z="2.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="2.2" />
                  <Point X="-3.692596234804" Y="1.60321539919" Z="2.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="2.2" />
                  <Point X="-3.670109377626" Y="1.54576899532" Z="2.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="2.2" />
                  <Point X="-3.699661593614" Y="1.491617284349" Z="2.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="2.2" />
                  <Point X="-4.674656079028" Y="1.596184578759" Z="2.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="2.2" />
                  <Point X="-4.837344441734" Y="1.537920624915" Z="2.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="2.2" />
                  <Point X="-4.984499123684" Y="0.959080936969" Z="2.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="2.2" />
                  <Point X="-3.774433222182" Y="0.102088805845" Z="2.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="2.2" />
                  <Point X="-3.572859660558" Y="0.046500274567" Z="2.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="2.2" />
                  <Point X="-3.547574146509" Y="0.025831948089" Z="2.2" />
                  <Point X="-3.539556741714" Y="0" Z="2.2" />
                  <Point X="-3.540790454626" Y="-0.003975002972" Z="2.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="2.2" />
                  <Point X="-3.552508934565" Y="-0.032375708567" Z="2.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="2.2" />
                  <Point X="-4.862454566591" Y="-0.393623250911" Z="2.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="2.2" />
                  <Point X="-5.049969740346" Y="-0.519060262802" Z="2.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="2.2" />
                  <Point X="-4.964557220809" Y="-1.06054926842" Z="2.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="2.2" />
                  <Point X="-3.436232044995" Y="-1.335441525766" Z="2.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="2.2" />
                  <Point X="-3.215627216935" Y="-1.308941895776" Z="2.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="2.2" />
                  <Point X="-3.193703763962" Y="-1.326416860285" Z="2.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="2.2" />
                  <Point X="-4.329198833412" Y="-2.218369673595" Z="2.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="2.2" />
                  <Point X="-4.463753855142" Y="-2.417298837949" Z="2.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="2.2" />
                  <Point X="-4.163064094397" Y="-2.904703649384" Z="2.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="2.2" />
                  <Point X="-2.744791550186" Y="-2.65476753255" Z="2.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="2.2" />
                  <Point X="-2.570525929116" Y="-2.557804540015" Z="2.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="2.2" />
                  <Point X="-3.200649480618" Y="-3.690286832306" Z="2.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="2.2" />
                  <Point X="-3.245322434352" Y="-3.904281843611" Z="2.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="2.2" />
                  <Point X="-2.831883527809" Y="-4.213780856694" Z="2.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="2.2" />
                  <Point X="-2.256213945031" Y="-4.195538068452" Z="2.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="2.2" />
                  <Point X="-2.191820343815" Y="-4.133465508485" Z="2.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="2.2" />
                  <Point X="-1.926969728308" Y="-3.986790415692" Z="2.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="2.2" />
                  <Point X="-1.627559586624" Y="-4.031656308815" Z="2.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="2.2" />
                  <Point X="-1.417334144878" Y="-4.249520319371" Z="2.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="2.2" />
                  <Point X="-1.406668446767" Y="-4.83065798628" Z="2.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="2.2" />
                  <Point X="-1.373665386378" Y="-4.889649189117" Z="2.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="2.2" />
                  <Point X="-1.077429867462" Y="-4.963342272766" Z="2.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="-0.470507603143" Y="-3.718142319052" Z="2.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="-0.395252339657" Y="-3.487313813014" Z="2.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="-0.219571554723" Y="-3.272386276078" Z="2.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="2.2" />
                  <Point X="0.033787524638" Y="-3.21472576921" Z="2.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="2.2" />
                  <Point X="0.275193519775" Y="-3.314331942754" Z="2.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="2.2" />
                  <Point X="0.764247257259" Y="-4.814393710763" Z="2.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="2.2" />
                  <Point X="0.84171818513" Y="-5.009393924595" Z="2.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="2.2" />
                  <Point X="1.02188699602" Y="-4.975767588065" Z="2.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="2.2" />
                  <Point X="0.986645551248" Y="-3.495467497598" Z="2.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="2.2" />
                  <Point X="0.964522355815" Y="-3.239895833381" Z="2.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="2.2" />
                  <Point X="1.035162332847" Y="-3.005368874236" Z="2.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="2.2" />
                  <Point X="1.222227787347" Y="-2.872815058549" Z="2.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="2.2" />
                  <Point X="1.452652226447" Y="-2.872499161306" Z="2.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="2.2" />
                  <Point X="2.525395430985" Y="-4.148563292738" Z="2.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="2.2" />
                  <Point X="2.688081790601" Y="-4.309798588238" Z="2.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="2.2" />
                  <Point X="2.882510164756" Y="-4.182258098382" Z="2.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="2.2" />
                  <Point X="2.374626311751" Y="-2.901374267341" Z="2.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="2.2" />
                  <Point X="2.266032486134" Y="-2.693481178094" Z="2.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="2.2" />
                  <Point X="2.244810678727" Y="-2.482267976294" Z="2.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="2.2" />
                  <Point X="2.350630402078" Y="-2.31409063124" Z="2.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="2.2" />
                  <Point X="2.535025625419" Y="-2.237415523357" Z="2.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="2.2" />
                  <Point X="3.886039329766" Y="-2.943123188524" Z="2.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="2.2" />
                  <Point X="4.088400296685" Y="-3.013427368972" Z="2.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="2.2" />
                  <Point X="4.260991065682" Y="-2.764003568699" Z="2.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="2.2" />
                  <Point X="3.35363498749" Y="-1.738049944329" Z="2.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="2.2" />
                  <Point X="3.179342944704" Y="-1.593750396559" Z="2.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="2.2" />
                  <Point X="3.094360091872" Y="-1.435507535862" Z="2.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="2.2" />
                  <Point X="3.122626734887" Y="-1.269770535686" Z="2.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="2.2" />
                  <Point X="3.241948631253" Y="-1.150121364976" Z="2.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="2.2" />
                  <Point X="4.705941582365" Y="-1.287943057677" Z="2.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="2.2" />
                  <Point X="4.918266397823" Y="-1.265072431321" Z="2.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="2.2" />
                  <Point X="4.998983362637" Y="-0.894378564692" Z="2.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="2.2" />
                  <Point X="3.921326754736" Y="-0.26726607279" Z="2.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="2.2" />
                  <Point X="3.735615729523" Y="-0.213679647003" Z="2.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="2.2" />
                  <Point X="3.648040532608" Y="-0.157906087913" Z="2.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="2.2" />
                  <Point X="3.597469329681" Y="-0.083726719387" Z="2.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="2.2" />
                  <Point X="3.583902120743" Y="0.012883811795" Z="2.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="2.2" />
                  <Point X="3.607338905792" Y="0.106042650662" Z="2.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="2.2" />
                  <Point X="3.66777968483" Y="0.174469197239" Z="2.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="2.2" />
                  <Point X="4.874641062571" Y="0.522705859978" Z="2.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="2.2" />
                  <Point X="5.039226510854" Y="0.62560908265" Z="2.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="2.2" />
                  <Point X="4.968600036732" Y="1.047947297137" Z="2.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="2.2" />
                  <Point X="3.652178617796" Y="1.246913943841" Z="2.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="2.2" />
                  <Point X="3.450564294873" Y="1.223683659425" Z="2.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="2.2" />
                  <Point X="3.359673274469" Y="1.239696549832" Z="2.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="2.2" />
                  <Point X="3.292909680203" Y="1.283412234451" Z="2.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="2.2" />
                  <Point X="3.248904693866" Y="1.358136244856" Z="2.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="2.2" />
                  <Point X="3.236462430124" Y="1.442613056087" Z="2.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="2.2" />
                  <Point X="3.262821715636" Y="1.519366434285" Z="2.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="2.2" />
                  <Point X="4.296028444498" Y="2.339077475562" Z="2.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="2.2" />
                  <Point X="4.419422946051" Y="2.501248087094" Z="2.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="2.2" />
                  <Point X="4.206721720577" Y="2.844472791155" Z="2.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="2.2" />
                  <Point X="2.708899544614" Y="2.381903949757" Z="2.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="2.2" />
                  <Point X="2.499171084051" Y="2.264135620395" Z="2.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="2.2" />
                  <Point X="2.420333276551" Y="2.24664556385" Z="2.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="2.2" />
                  <Point X="2.351724048252" Y="2.259629271092" Z="2.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="2.2" />
                  <Point X="2.291129329143" Y="2.305300812131" Z="2.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="2.2" />
                  <Point X="2.252784138909" Y="2.369425167282" Z="2.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="2.2" />
                  <Point X="2.248392322263" Y="2.440298491831" Z="2.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="2.2" />
                  <Point X="3.013721403103" Y="3.803239537341" Z="2.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="2.2" />
                  <Point X="3.078600070145" Y="4.037837255393" Z="2.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="2.2" />
                  <Point X="2.700456577686" Y="4.29993396314" Z="2.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="2.2" />
                  <Point X="2.30085492832" Y="4.526431940512" Z="2.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="2.2" />
                  <Point X="1.887048102043" Y="4.713974650699" Z="2.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="2.2" />
                  <Point X="1.429497421607" Y="4.869351476594" Z="2.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="2.2" />
                  <Point X="0.773300102485" Y="5.015575725289" Z="2.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="2.2" />
                  <Point X="0.025770757461" Y="4.451302254673" Z="2.2" />
                  <Point X="0" Y="4.355124473572" Z="2.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>