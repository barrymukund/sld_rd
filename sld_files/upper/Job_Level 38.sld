<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#191" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="2749" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.855264221191" Y="20.397857421875" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557720458984" Y="21.502861328125" />
                  <Point X="0.542363342285" Y="21.532623046875" />
                  <Point X="0.423031036377" Y="21.704556640625" />
                  <Point X="0.378635528564" Y="21.7685234375" />
                  <Point X="0.356752655029" Y="21.7909765625" />
                  <Point X="0.330497467041" Y="21.810220703125" />
                  <Point X="0.302495178223" Y="21.824330078125" />
                  <Point X="0.11783531189" Y="21.881640625" />
                  <Point X="0.049136035919" Y="21.902962890625" />
                  <Point X="0.020983318329" Y="21.907232421875" />
                  <Point X="-0.008657554626" Y="21.907234375" />
                  <Point X="-0.036823631287" Y="21.90296484375" />
                  <Point X="-0.221483642578" Y="21.84565234375" />
                  <Point X="-0.290182769775" Y="21.82433203125" />
                  <Point X="-0.318183105469" Y="21.810224609375" />
                  <Point X="-0.344439025879" Y="21.79098046875" />
                  <Point X="-0.366323425293" Y="21.7685234375" />
                  <Point X="-0.485655883789" Y="21.596587890625" />
                  <Point X="-0.530051208496" Y="21.532623046875" />
                  <Point X="-0.538189147949" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.659609069824" Y="21.082103515625" />
                  <Point X="-0.916584655762" Y="20.12305859375" />
                  <Point X="-1.002342834473" Y="20.139705078125" />
                  <Point X="-1.079354125977" Y="20.154654296875" />
                  <Point X="-1.24641809082" Y="20.19763671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.260623901367" Y="20.705556640625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868796875" />
                  <Point X="-1.49365625" Y="21.017892578125" />
                  <Point X="-1.556905639648" Y="21.073361328125" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886474609" Y="21.102005859375" />
                  <Point X="-1.643027709961" Y="21.109033203125" />
                  <Point X="-1.868670654297" Y="21.123822265625" />
                  <Point X="-1.952616821289" Y="21.12932421875" />
                  <Point X="-1.983414794922" Y="21.126291015625" />
                  <Point X="-2.014463378906" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.230675537109" Y="20.9795703125" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.396083984375" Y="20.821064453125" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.688853515625" Y="20.840736328125" />
                  <Point X="-2.801728027344" Y="20.910625" />
                  <Point X="-3.09428515625" Y="21.135884765625" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.966340087891" Y="21.38360546875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405576171875" Y="22.4148125" />
                  <Point X="-2.4145625" Y="22.4444296875" />
                  <Point X="-2.428781982422" Y="22.473259765625" />
                  <Point X="-2.446808105469" Y="22.4984140625" />
                  <Point X="-2.464150390625" Y="22.515755859375" />
                  <Point X="-2.489299560547" Y="22.533779296875" />
                  <Point X="-2.518129638672" Y="22.548" />
                  <Point X="-2.547750488281" Y="22.55698828125" />
                  <Point X="-2.578688476562" Y="22.555974609375" />
                  <Point X="-2.610217285156" Y="22.549703125" />
                  <Point X="-2.639183349609" Y="22.538802734375" />
                  <Point X="-2.98880859375" Y="22.336947265625" />
                  <Point X="-3.818024169922" Y="21.85819921875" />
                  <Point X="-3.993697998047" Y="22.088998046875" />
                  <Point X="-4.082862060547" Y="22.206142578125" />
                  <Point X="-4.292612304688" Y="22.557861328125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-4.055291748047" Y="22.773033203125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084576171875" Y="23.52440625" />
                  <Point X="-3.066611083984" Y="23.5515390625" />
                  <Point X="-3.053856201172" Y="23.58016796875" />
                  <Point X="-3.048240722656" Y="23.601849609375" />
                  <Point X="-3.046151611328" Y="23.609916015625" />
                  <Point X="-3.043347412109" Y="23.64034375" />
                  <Point X="-3.045556396484" Y="23.672013671875" />
                  <Point X="-3.052558105469" Y="23.701759765625" />
                  <Point X="-3.068640869141" Y="23.727744140625" />
                  <Point X="-3.089472900391" Y="23.75169921875" />
                  <Point X="-3.112974365234" Y="23.771232421875" />
                  <Point X="-3.132276123047" Y="23.782591796875" />
                  <Point X="-3.13945703125" Y="23.786818359375" />
                  <Point X="-3.168715820312" Y="23.798041015625" />
                  <Point X="-3.200604003906" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-3.673297607422" Y="23.7475078125" />
                  <Point X="-4.7321015625" Y="23.608115234375" />
                  <Point X="-4.799204589844" Y="23.8708203125" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.889572753906" Y="24.395361328125" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.614313476562" Y="24.4898203125" />
                  <Point X="-3.532875732422" Y="24.77958984375" />
                  <Point X="-3.517488769531" Y="24.785173828125" />
                  <Point X="-3.487729980469" Y="24.800529296875" />
                  <Point X="-3.467502441406" Y="24.814568359375" />
                  <Point X="-3.459977050781" Y="24.819791015625" />
                  <Point X="-3.437523681641" Y="24.841673828125" />
                  <Point X="-3.418278076172" Y="24.8679296875" />
                  <Point X="-3.404168212891" Y="24.895931640625" />
                  <Point X="-3.397425537109" Y="24.91765625" />
                  <Point X="-3.394917236328" Y="24.92573828125" />
                  <Point X="-3.390647705078" Y="24.953896484375" />
                  <Point X="-3.390647216797" Y="24.98353515625" />
                  <Point X="-3.394916503906" Y="25.0116953125" />
                  <Point X="-3.401658935547" Y="25.033419921875" />
                  <Point X="-3.404167480469" Y="25.04150390625" />
                  <Point X="-3.418276367188" Y="25.0695078125" />
                  <Point X="-3.437522460938" Y="25.095765625" />
                  <Point X="-3.459977050781" Y="25.1176484375" />
                  <Point X="-3.480204589844" Y="25.1316875" />
                  <Point X="-3.489306640625" Y="25.1372734375" />
                  <Point X="-3.512398681641" Y="25.149716796875" />
                  <Point X="-3.532876220703" Y="25.157849609375" />
                  <Point X="-3.935204589844" Y="25.26565234375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.846961914062" Y="25.82509765625" />
                  <Point X="-4.82448828125" Y="25.976970703125" />
                  <Point X="-4.712777832031" Y="26.389216796875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.540867675781" Y="26.401849609375" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744985595703" Y="26.299341796875" />
                  <Point X="-3.723423095703" Y="26.301228515625" />
                  <Point X="-3.703138183594" Y="26.305263671875" />
                  <Point X="-3.658368164062" Y="26.31937890625" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.622776367188" Y="26.332962890625" />
                  <Point X="-3.604032470703" Y="26.34378515625" />
                  <Point X="-3.587352783203" Y="26.356015625" />
                  <Point X="-3.573715332031" Y="26.37156640625" />
                  <Point X="-3.561301269531" Y="26.389294921875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.533387207031" Y="26.450798828125" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532050292969" Y="26.58937890625" />
                  <Point X="-3.553726074219" Y="26.631017578125" />
                  <Point X="-3.561790039062" Y="26.6465078125" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-3.832912353516" Y="26.871671875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.168477539062" Y="27.584052734375" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.785239013672" Y="28.114015625" />
                  <Point X="-3.750505126953" Y="28.15866015625" />
                  <Point X="-3.682763916016" Y="28.11955078125" />
                  <Point X="-3.206656982422" Y="27.844669921875" />
                  <Point X="-3.187728759766" Y="27.836341796875" />
                  <Point X="-3.167086181641" Y="27.82983203125" />
                  <Point X="-3.146794189453" Y="27.825794921875" />
                  <Point X="-3.084441894531" Y="27.82033984375" />
                  <Point X="-3.061244873047" Y="27.818310546875" />
                  <Point X="-3.040560302734" Y="27.81876171875" />
                  <Point X="-3.019102050781" Y="27.821587890625" />
                  <Point X="-2.999012695312" Y="27.826505859375" />
                  <Point X="-2.980463623047" Y="27.83565234375" />
                  <Point X="-2.962209716797" Y="27.84728125" />
                  <Point X="-2.946078125" Y="27.860228515625" />
                  <Point X="-2.901819824219" Y="27.904486328125" />
                  <Point X="-2.885354492188" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.848890869141" Y="28.09847265625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-2.972059326172" Y="28.358658203125" />
                  <Point X="-3.183333007812" Y="28.72459375" />
                  <Point X="-2.8527109375" Y="28.978080078125" />
                  <Point X="-2.70062109375" Y="29.094685546875" />
                  <Point X="-2.234570800781" Y="29.35361328125" />
                  <Point X="-2.167036621094" Y="29.391134765625" />
                  <Point X="-2.043195800781" Y="29.2297421875" />
                  <Point X="-2.028894897461" Y="29.214802734375" />
                  <Point X="-2.012316650391" Y="29.200890625" />
                  <Point X="-1.995115112305" Y="29.189396484375" />
                  <Point X="-1.925717163086" Y="29.15326953125" />
                  <Point X="-1.899899169922" Y="29.139828125" />
                  <Point X="-1.880626342773" Y="29.13233203125" />
                  <Point X="-1.859719116211" Y="29.126728515625" />
                  <Point X="-1.83926965332" Y="29.123580078125" />
                  <Point X="-1.818623901367" Y="29.12493359375" />
                  <Point X="-1.797307617188" Y="29.128693359375" />
                  <Point X="-1.777453125" Y="29.134482421875" />
                  <Point X="-1.705170532227" Y="29.164423828125" />
                  <Point X="-1.678279052734" Y="29.1755625" />
                  <Point X="-1.660146362305" Y="29.185509765625" />
                  <Point X="-1.642416625977" Y="29.197923828125" />
                  <Point X="-1.626864379883" Y="29.2115625" />
                  <Point X="-1.6146328125" Y="29.228244140625" />
                  <Point X="-1.603810913086" Y="29.24698828125" />
                  <Point X="-1.59548046875" Y="29.265921875" />
                  <Point X="-1.571953857422" Y="29.3405390625" />
                  <Point X="-1.563200927734" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.569356689453" Y="29.51913671875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.14651965332" Y="29.754607421875" />
                  <Point X="-0.949623840332" Y="29.80980859375" />
                  <Point X="-0.384639526367" Y="29.87593359375" />
                  <Point X="-0.294711456299" Y="29.886458984375" />
                  <Point X="-0.27474887085" Y="29.81195703125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425926208" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.198614074707" Y="29.48187109375" />
                  <Point X="0.307419494629" Y="29.8879375" />
                  <Point X="0.672125183105" Y="29.8497421875" />
                  <Point X="0.844031188965" Y="29.831740234375" />
                  <Point X="1.311480834961" Y="29.7188828125" />
                  <Point X="1.481039794922" Y="29.6779453125" />
                  <Point X="1.784884155273" Y="29.567740234375" />
                  <Point X="1.894648193359" Y="29.52792578125" />
                  <Point X="2.188850341797" Y="29.390337890625" />
                  <Point X="2.294556152344" Y="29.340904296875" />
                  <Point X="2.578813964844" Y="29.175294921875" />
                  <Point X="2.680975097656" Y="29.115775390625" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.775575439453" Y="28.63881640625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.117428466797" Y="27.457537109375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.113829589844" Y="27.3303515625" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.140071044922" Y="27.247470703125" />
                  <Point X="2.171380615234" Y="27.201328125" />
                  <Point X="2.183028808594" Y="27.184162109375" />
                  <Point X="2.194465576172" Y="27.170328125" />
                  <Point X="2.221596923828" Y="27.14559375" />
                  <Point X="2.267739257812" Y="27.114283203125" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304949707031" Y="27.0922734375" />
                  <Point X="2.327040771484" Y="27.084005859375" />
                  <Point X="2.348968017578" Y="27.078662109375" />
                  <Point X="2.3995625" Y="27.0725625" />
                  <Point X="2.408583496094" Y="27.071908203125" />
                  <Point X="2.446315917969" Y="27.070974609375" />
                  <Point X="2.473208251953" Y="27.074169921875" />
                  <Point X="2.531724609375" Y="27.089818359375" />
                  <Point X="2.553494628906" Y="27.095640625" />
                  <Point X="2.565291748047" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.11014453125" />
                  <Point X="2.993198974609" Y="27.34377734375" />
                  <Point X="3.967326904297" Y="27.906189453125" />
                  <Point X="4.062670898438" Y="27.77368359375" />
                  <Point X="4.123274902344" Y="27.68945703125" />
                  <Point X="4.26219921875" Y="27.459884765625" />
                  <Point X="4.05759765625" Y="27.302888671875" />
                  <Point X="3.230784179688" Y="26.668451171875" />
                  <Point X="3.221421630859" Y="26.66023828125" />
                  <Point X="3.203974121094" Y="26.641626953125" />
                  <Point X="3.161859619141" Y="26.586685546875" />
                  <Point X="3.146191894531" Y="26.56624609375" />
                  <Point X="3.136607421875" Y="26.5509140625" />
                  <Point X="3.121629638672" Y="26.5170859375" />
                  <Point X="3.105941894531" Y="26.460990234375" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.110617431641" Y="26.30935546875" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120680419922" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.235740234375" />
                  <Point X="3.171309570313" Y="26.182501953125" />
                  <Point X="3.184340576172" Y="26.1626953125" />
                  <Point X="3.198890136719" Y="26.145455078125" />
                  <Point X="3.216132324219" Y="26.129365234375" />
                  <Point X="3.234345703125" Y="26.11603515625" />
                  <Point X="3.285104003906" Y="26.087462890625" />
                  <Point X="3.303987792969" Y="26.07683203125" />
                  <Point X="3.320525146484" Y="26.0695" />
                  <Point X="3.356120605469" Y="26.0594375" />
                  <Point X="3.424749267578" Y="26.0503671875" />
                  <Point X="3.45028125" Y="26.046994140625" />
                  <Point X="3.462700439453" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="3.872608154297" Y="26.097591796875" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.819908203125" Y="26.03972265625" />
                  <Point X="4.845936035156" Y="25.9328125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.664125976563" Y="25.583484375" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704784912109" Y="25.325583984375" />
                  <Point X="3.681547851562" Y="25.31506640625" />
                  <Point X="3.614122314453" Y="25.27609375" />
                  <Point X="3.589037841797" Y="25.261595703125" />
                  <Point X="3.574317626953" Y="25.2511015625" />
                  <Point X="3.547530517578" Y="25.225576171875" />
                  <Point X="3.507075195312" Y="25.174025390625" />
                  <Point X="3.492024658203" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.450195556641" Y="25.022189453125" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.458664306641" Y="24.87103125" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526611328" Y="24.8233359375" />
                  <Point X="3.480300537109" Y="24.80187109375" />
                  <Point X="3.492024658203" Y="24.782591796875" />
                  <Point X="3.532479980469" Y="24.731041015625" />
                  <Point X="3.547530517578" Y="24.71186328125" />
                  <Point X="3.559997314453" Y="24.698763671875" />
                  <Point X="3.58903515625" Y="24.67584375" />
                  <Point X="3.656460693359" Y="24.63687109375" />
                  <Point X="3.681545166016" Y="24.62237109375" />
                  <Point X="3.692710205078" Y="24.616859375" />
                  <Point X="3.716580322266" Y="24.60784765625" />
                  <Point X="4.069096679688" Y="24.513392578125" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.869555664062" Y="24.14766796875" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.523268554688" Y="23.85188671875" />
                  <Point X="3.424381591797" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374659423828" Y="23.994490234375" />
                  <Point X="3.242326904297" Y="23.9657265625" />
                  <Point X="3.193095214844" Y="23.95502734375" />
                  <Point X="3.1639765625" Y="23.943404296875" />
                  <Point X="3.136148681641" Y="23.92651171875" />
                  <Point X="3.112397216797" Y="23.9060390625" />
                  <Point X="3.032410644531" Y="23.80983984375" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.958293457031" Y="23.570052734375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347167969" Y="23.492435546875" />
                  <Point X="2.964078613281" Y="23.460814453125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.049685546875" Y="23.318091796875" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086931396484" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.437765869141" Y="22.988068359375" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.165776367188" Y="22.31650390625" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.028980224609" Y="22.1140546875" />
                  <Point X="3.779661621094" Y="22.257998046875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224365234" Y="22.840173828125" />
                  <Point X="2.596727539062" Y="22.8686171875" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474610839844" Y="22.874646484375" />
                  <Point X="2.444833496094" Y="22.864822265625" />
                  <Point X="2.313992431641" Y="22.795962890625" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242384033203" Y="22.753451171875" />
                  <Point X="2.221425048828" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.135671142578" Y="22.57871875" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229492188" Y="22.500267578125" />
                  <Point X="2.095271484375" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.124118896484" Y="22.279244140625" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.362037841797" Y="21.809810546875" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.830463378906" Y="20.923080078125" />
                  <Point X="2.781862548828" Y="20.88836328125" />
                  <Point X="2.701763916016" Y="20.836517578125" />
                  <Point X="2.505389648438" Y="21.0924375" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506225586" Y="22.077818359375" />
                  <Point X="1.721923583984" Y="22.099443359375" />
                  <Point X="1.566589111328" Y="22.19930859375" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986938477" Y="22.24883203125" />
                  <Point X="1.44836730957" Y="22.2565625" />
                  <Point X="1.417100708008" Y="22.258880859375" />
                  <Point X="1.247216186523" Y="22.243248046875" />
                  <Point X="1.184013671875" Y="22.23743359375" />
                  <Point X="1.156364746094" Y="22.230603515625" />
                  <Point X="1.128977905273" Y="22.219259765625" />
                  <Point X="1.104594970703" Y="22.2045390625" />
                  <Point X="0.973414550781" Y="22.095466796875" />
                  <Point X="0.924611206055" Y="22.05488671875" />
                  <Point X="0.904139526367" Y="22.031134765625" />
                  <Point X="0.887248168945" Y="22.00330859375" />
                  <Point X="0.875624511719" Y="21.974189453125" />
                  <Point X="0.836402160645" Y="21.793736328125" />
                  <Point X="0.821810241699" Y="21.726603515625" />
                  <Point X="0.81972479248" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.879316467285" Y="21.2243671875" />
                  <Point X="1.022065551758" Y="20.140083984375" />
                  <Point X="1.021667663574" Y="20.13999609375" />
                  <Point X="0.975712585449" Y="20.129923828125" />
                  <Point X="0.929315185547" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058452636719" Y="20.24737109375" />
                  <Point X="-1.14124621582" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.16744934082" Y="20.72408984375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573242188" Y="20.905138671875" />
                  <Point X="-1.250208251953" Y="20.929064453125" />
                  <Point X="-1.261006958008" Y="20.94022265625" />
                  <Point X="-1.431018432617" Y="21.089318359375" />
                  <Point X="-1.494267822266" Y="21.144787109375" />
                  <Point X="-1.506740112305" Y="21.15403515625" />
                  <Point X="-1.533023681641" Y="21.17037890625" />
                  <Point X="-1.546834838867" Y="21.177474609375" />
                  <Point X="-1.576532104492" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621457275391" Y="21.201552734375" />
                  <Point X="-1.636814575195" Y="21.203830078125" />
                  <Point X="-1.862457519531" Y="21.218619140625" />
                  <Point X="-1.946403686523" Y="21.22412109375" />
                  <Point X="-1.961928100586" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.007999511719" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667480469" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095436523437" Y="21.184189453125" />
                  <Point X="-2.283454589844" Y="21.058560546875" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.471452636719" Y="20.878896484375" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.638842529297" Y="20.9215078125" />
                  <Point X="-2.747615234375" Y="20.988857421875" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.884067626953" Y="21.33610546875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.38076953125" />
                  <Point X="-2.310626953125" Y="22.41170703125" />
                  <Point X="-2.314668701172" Y="22.44239453125" />
                  <Point X="-2.323655029297" Y="22.47201171875" />
                  <Point X="-2.329362060547" Y="22.486451171875" />
                  <Point X="-2.343581542969" Y="22.51528125" />
                  <Point X="-2.351562744141" Y="22.52859765625" />
                  <Point X="-2.369588867188" Y="22.553751953125" />
                  <Point X="-2.379633789062" Y="22.56558984375" />
                  <Point X="-2.396976074219" Y="22.582931640625" />
                  <Point X="-2.408811279297" Y="22.592974609375" />
                  <Point X="-2.433960449219" Y="22.610998046875" />
                  <Point X="-2.447274414062" Y="22.618978515625" />
                  <Point X="-2.476104492188" Y="22.63319921875" />
                  <Point X="-2.490544433594" Y="22.63890625" />
                  <Point X="-2.520165283203" Y="22.64789453125" />
                  <Point X="-2.550861572266" Y="22.6519375" />
                  <Point X="-2.581799560547" Y="22.650923828125" />
                  <Point X="-2.597222167969" Y="22.6491484375" />
                  <Point X="-2.628750976562" Y="22.642876953125" />
                  <Point X="-2.643676513672" Y="22.638615234375" />
                  <Point X="-2.672642578125" Y="22.62771484375" />
                  <Point X="-2.686683105469" Y="22.621076171875" />
                  <Point X="-3.036308349609" Y="22.419220703125" />
                  <Point X="-3.793089355469" Y="21.98229296875" />
                  <Point X="-3.918104736328" Y="22.146537109375" />
                  <Point X="-4.004021484375" Y="22.259416015625" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.997459472656" Y="22.6976640625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481201172" Y="23.436689453125" />
                  <Point X="-3.015102783203" Y="23.459611328125" />
                  <Point X="-3.005365478516" Y="23.471958984375" />
                  <Point X="-2.987400390625" Y="23.499091796875" />
                  <Point X="-2.979833740234" Y="23.512876953125" />
                  <Point X="-2.967078857422" Y="23.541505859375" />
                  <Point X="-2.961890625" Y="23.556349609375" />
                  <Point X="-2.956275146484" Y="23.57803125" />
                  <Point X="-2.951552490234" Y="23.601197265625" />
                  <Point X="-2.948748291016" Y="23.631625" />
                  <Point X="-2.948577636719" Y="23.646953125" />
                  <Point X="-2.950786621094" Y="23.678623046875" />
                  <Point X="-2.953083496094" Y="23.693779296875" />
                  <Point X="-2.960085205078" Y="23.723525390625" />
                  <Point X="-2.971779052734" Y="23.7517578125" />
                  <Point X="-2.987861816406" Y="23.7777421875" />
                  <Point X="-2.996955566406" Y="23.790083984375" />
                  <Point X="-3.017787597656" Y="23.8140390625" />
                  <Point X="-3.028749755859" Y="23.8247578125" />
                  <Point X="-3.052251220703" Y="23.844291015625" />
                  <Point X="-3.064790527344" Y="23.85310546875" />
                  <Point X="-3.084092285156" Y="23.86446484375" />
                  <Point X="-3.105435058594" Y="23.875517578125" />
                  <Point X="-3.134693847656" Y="23.886740234375" />
                  <Point X="-3.149790771484" Y="23.89113671875" />
                  <Point X="-3.181678955078" Y="23.897619140625" />
                  <Point X="-3.197294921875" Y="23.89946484375" />
                  <Point X="-3.228619873047" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-3.685697509766" Y="23.8416953125" />
                  <Point X="-4.660919921875" Y="23.713306640625" />
                  <Point X="-4.707159667969" Y="23.89433203125" />
                  <Point X="-4.740762695312" Y="24.025884765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.589725097656" Y="24.398056640625" />
                  <Point X="-3.508287353516" Y="24.687826171875" />
                  <Point X="-3.500468017578" Y="24.6902890625" />
                  <Point X="-3.473926513672" Y="24.70075" />
                  <Point X="-3.444167724609" Y="24.71610546875" />
                  <Point X="-3.433562744141" Y="24.722484375" />
                  <Point X="-3.413335205078" Y="24.7365234375" />
                  <Point X="-3.393671875" Y="24.7517578125" />
                  <Point X="-3.371218505859" Y="24.773640625" />
                  <Point X="-3.360903076172" Y="24.78551171875" />
                  <Point X="-3.341657470703" Y="24.811767578125" />
                  <Point X="-3.333439941406" Y="24.825181640625" />
                  <Point X="-3.319330078125" Y="24.85318359375" />
                  <Point X="-3.313437744141" Y="24.867771484375" />
                  <Point X="-3.306695068359" Y="24.88949609375" />
                  <Point X="-3.300990722656" Y="24.91149609375" />
                  <Point X="-3.296721191406" Y="24.939654296875" />
                  <Point X="-3.295647705078" Y="24.95389453125" />
                  <Point X="-3.295647216797" Y="24.983533203125" />
                  <Point X="-3.296720458984" Y="24.997775390625" />
                  <Point X="-3.300989746094" Y="25.025935546875" />
                  <Point X="-3.304185791016" Y="25.039853515625" />
                  <Point X="-3.310928222656" Y="25.061578125" />
                  <Point X="-3.319326904297" Y="25.084248046875" />
                  <Point X="-3.333435791016" Y="25.112251953125" />
                  <Point X="-3.341654541016" Y="25.125669921875" />
                  <Point X="-3.360900634766" Y="25.151927734375" />
                  <Point X="-3.371219238281" Y="25.16380078125" />
                  <Point X="-3.393673828125" Y="25.18568359375" />
                  <Point X="-3.405809814453" Y="25.195693359375" />
                  <Point X="-3.426037353516" Y="25.209732421875" />
                  <Point X="-3.444241455078" Y="25.220904296875" />
                  <Point X="-3.467333496094" Y="25.23334765625" />
                  <Point X="-3.477333007812" Y="25.2380078125" />
                  <Point X="-3.497810546875" Y="25.246140625" />
                  <Point X="-3.508288574219" Y="25.24961328125" />
                  <Point X="-3.910616943359" Y="25.357416015625" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.752985351562" Y="25.81119140625" />
                  <Point X="-4.731331542969" Y="25.957525390625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.553268066406" Y="26.307662109375" />
                  <Point X="-3.778066650391" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747057617188" Y="26.204365234375" />
                  <Point X="-3.736704833984" Y="26.204703125" />
                  <Point X="-3.715142333984" Y="26.20658984375" />
                  <Point X="-3.704888427734" Y="26.2080546875" />
                  <Point X="-3.684603515625" Y="26.21208984375" />
                  <Point X="-3.674572509766" Y="26.21466015625" />
                  <Point X="-3.629802490234" Y="26.228775390625" />
                  <Point X="-3.603450927734" Y="26.23767578125" />
                  <Point X="-3.584515136719" Y="26.2460078125" />
                  <Point X="-3.575274902344" Y="26.25069140625" />
                  <Point X="-3.556531005859" Y="26.261513671875" />
                  <Point X="-3.547856933594" Y="26.267173828125" />
                  <Point X="-3.531177246094" Y="26.279404296875" />
                  <Point X="-3.515927490234" Y="26.29337890625" />
                  <Point X="-3.502290039063" Y="26.3089296875" />
                  <Point X="-3.495896728516" Y="26.317076171875" />
                  <Point X="-3.483482666016" Y="26.3348046875" />
                  <Point X="-3.478013427734" Y="26.343599609375" />
                  <Point X="-3.468063720703" Y="26.361734375" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.445618896484" Y="26.414443359375" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443508789062" Y="26.62380859375" />
                  <Point X="-3.447784423828" Y="26.63324609375" />
                  <Point X="-3.469460205078" Y="26.674884765625" />
                  <Point X="-3.482802246094" Y="26.6992890625" />
                  <Point X="-3.494293945312" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521497558594" Y="26.74890625" />
                  <Point X="-3.536439208984" Y="26.7632109375" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.775079833984" Y="26.947041015625" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.086431152344" Y="27.5361640625" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.726339111328" Y="28.03501171875" />
                  <Point X="-3.254156005859" Y="27.762396484375" />
                  <Point X="-3.244916015625" Y="27.75771484375" />
                  <Point X="-3.225987792969" Y="27.74938671875" />
                  <Point X="-3.216300537109" Y="27.745740234375" />
                  <Point X="-3.195657958984" Y="27.73923046875" />
                  <Point X="-3.185623291016" Y="27.736658203125" />
                  <Point X="-3.165331298828" Y="27.73262109375" />
                  <Point X="-3.155073974609" Y="27.73115625" />
                  <Point X="-3.092721679688" Y="27.725701171875" />
                  <Point X="-3.069524658203" Y="27.723671875" />
                  <Point X="-3.059173339844" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028155517578" Y="27.72457421875" />
                  <Point X="-3.006697265625" Y="27.727400390625" />
                  <Point X="-2.996512695312" Y="27.7293125" />
                  <Point X="-2.976423339844" Y="27.73423046875" />
                  <Point X="-2.956998535156" Y="27.74130078125" />
                  <Point X="-2.938449462891" Y="27.750447265625" />
                  <Point X="-2.929420410156" Y="27.755529296875" />
                  <Point X="-2.911166503906" Y="27.767158203125" />
                  <Point X="-2.90274609375" Y="27.773193359375" />
                  <Point X="-2.886614501953" Y="27.786140625" />
                  <Point X="-2.878903320312" Y="27.793052734375" />
                  <Point X="-2.834645019531" Y="27.837310546875" />
                  <Point X="-2.8181796875" Y="27.853775390625" />
                  <Point X="-2.811267089844" Y="27.861486328125" />
                  <Point X="-2.798321533203" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.754252441406" Y="28.106751953125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.889787109375" Y="28.406158203125" />
                  <Point X="-3.059387939453" Y="28.6999140625" />
                  <Point X="-2.794908691406" Y="28.902689453125" />
                  <Point X="-2.648377441406" Y="29.015033203125" />
                  <Point X="-2.192523681641" Y="29.268294921875" />
                  <Point X="-2.118564453125" Y="29.17191015625" />
                  <Point X="-2.111821533203" Y="29.164048828125" />
                  <Point X="-2.097520751953" Y="29.149109375" />
                  <Point X="-2.089963134766" Y="29.14203125" />
                  <Point X="-2.073384765625" Y="29.128119140625" />
                  <Point X="-2.065097167969" Y="29.12190234375" />
                  <Point X="-2.047895629883" Y="29.110408203125" />
                  <Point X="-2.038981811523" Y="29.105130859375" />
                  <Point X="-1.969583862305" Y="29.06900390625" />
                  <Point X="-1.943765991211" Y="29.0555625" />
                  <Point X="-1.9343359375" Y="29.0512890625" />
                  <Point X="-1.915063110352" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884312866211" Y="29.034966796875" />
                  <Point X="-1.874175170898" Y="29.032833984375" />
                  <Point X="-1.853725708008" Y="29.029685546875" />
                  <Point X="-1.833054931641" Y="29.028783203125" />
                  <Point X="-1.812409179688" Y="29.03013671875" />
                  <Point X="-1.802122558594" Y="29.031376953125" />
                  <Point X="-1.780806274414" Y="29.03513671875" />
                  <Point X="-1.770715332031" Y="29.037490234375" />
                  <Point X="-1.750860839844" Y="29.043279296875" />
                  <Point X="-1.741097167969" Y="29.04671484375" />
                  <Point X="-1.668814575195" Y="29.07665625" />
                  <Point X="-1.641923095703" Y="29.087794921875" />
                  <Point X="-1.632587524414" Y="29.092271484375" />
                  <Point X="-1.614454956055" Y="29.10221875" />
                  <Point X="-1.605657958984" Y="29.107689453125" />
                  <Point X="-1.587928100586" Y="29.120103515625" />
                  <Point X="-1.579779541016" Y="29.126498046875" />
                  <Point X="-1.564227294922" Y="29.14013671875" />
                  <Point X="-1.550252319336" Y="29.155388671875" />
                  <Point X="-1.538020874023" Y="29.1720703125" />
                  <Point X="-1.532360351562" Y="29.180744140625" />
                  <Point X="-1.521538330078" Y="29.19948828125" />
                  <Point X="-1.51685546875" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504877319336" Y="29.23735546875" />
                  <Point X="-1.481350708008" Y="29.31197265625" />
                  <Point X="-1.47259777832" Y="29.339732421875" />
                  <Point X="-1.470026611328" Y="29.349763671875" />
                  <Point X="-1.465991333008" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.475169433594" Y="29.531537109375" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-1.120873779297" Y="29.663134765625" />
                  <Point X="-0.93116418457" Y="29.7163203125" />
                  <Point X="-0.373596191406" Y="29.781578125" />
                  <Point X="-0.365222839355" Y="29.78255859375" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.290377044678" Y="29.457283203125" />
                  <Point X="0.378190643311" Y="29.7850078125" />
                  <Point X="0.662230041504" Y="29.755259765625" />
                  <Point X="0.827852600098" Y="29.737916015625" />
                  <Point X="1.289185424805" Y="29.62653515625" />
                  <Point X="1.453622802734" Y="29.586833984375" />
                  <Point X="1.7524921875" Y="29.47843359375" />
                  <Point X="1.858250244141" Y="29.440072265625" />
                  <Point X="2.148605712891" Y="29.304283203125" />
                  <Point X="2.250428222656" Y="29.256666015625" />
                  <Point X="2.530990966797" Y="29.093208984375" />
                  <Point X="2.629434082031" Y="29.03585546875" />
                  <Point X="2.817779052734" Y="28.901916015625" />
                  <Point X="2.693302978516" Y="28.68631640625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.025653198242" Y="27.482078125" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.019512817383" Y="27.318978515625" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.045319580078" Y="27.22388671875" />
                  <Point X="2.055681396484" Y="27.20384375" />
                  <Point X="2.061459716797" Y="27.19412890625" />
                  <Point X="2.092769287109" Y="27.147986328125" />
                  <Point X="2.104417480469" Y="27.1308203125" />
                  <Point X="2.109810058594" Y="27.123630859375" />
                  <Point X="2.130463134766" Y="27.100123046875" />
                  <Point X="2.157594482422" Y="27.075388671875" />
                  <Point X="2.168254638672" Y="27.066982421875" />
                  <Point X="2.214396972656" Y="27.035671875" />
                  <Point X="2.231563232422" Y="27.0240234375" />
                  <Point X="2.241281494141" Y="27.018244140625" />
                  <Point X="2.261325683594" Y="27.0078828125" />
                  <Point X="2.271651611328" Y="27.00330078125" />
                  <Point X="2.293742675781" Y="26.995033203125" />
                  <Point X="2.304547363281" Y="26.99170703125" />
                  <Point X="2.326474609375" Y="26.98636328125" />
                  <Point X="2.337597167969" Y="26.984345703125" />
                  <Point X="2.388191650391" Y="26.97824609375" />
                  <Point X="2.406233642578" Y="26.9769375" />
                  <Point X="2.443966064453" Y="26.97600390625" />
                  <Point X="2.457524902344" Y="26.976638671875" />
                  <Point X="2.484417236328" Y="26.979833984375" />
                  <Point X="2.497750732422" Y="26.98239453125" />
                  <Point X="2.556267089844" Y="26.99804296875" />
                  <Point X="2.578037109375" Y="27.003865234375" />
                  <Point X="2.584" Y="27.005671875" />
                  <Point X="2.604415527344" Y="27.0130703125" />
                  <Point X="2.627657714844" Y="27.02357421875" />
                  <Point X="2.636033935547" Y="27.02787109375" />
                  <Point X="3.040698974609" Y="27.26150390625" />
                  <Point X="3.940405761719" Y="27.78094921875" />
                  <Point X="3.98555859375" Y="27.718197265625" />
                  <Point X="4.043957763672" Y="27.63703515625" />
                  <Point X="4.136884765625" Y="27.48347265625" />
                  <Point X="3.999765380859" Y="27.3782578125" />
                  <Point X="3.172951904297" Y="26.7438203125" />
                  <Point X="3.168136962891" Y="26.7398671875" />
                  <Point X="3.152114501953" Y="26.7252109375" />
                  <Point X="3.134666992188" Y="26.706599609375" />
                  <Point X="3.128576660156" Y="26.699421875" />
                  <Point X="3.086462158203" Y="26.64448046875" />
                  <Point X="3.070794433594" Y="26.624041015625" />
                  <Point X="3.06563671875" Y="26.616603515625" />
                  <Point X="3.049741210938" Y="26.589375" />
                  <Point X="3.034763427734" Y="26.555546875" />
                  <Point X="3.030139892578" Y="26.542671875" />
                  <Point X="3.014452148438" Y="26.486576171875" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.017577392578" Y="26.290158203125" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024598388672" Y="26.258232421875" />
                  <Point X="3.034684814453" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.19537109375" />
                  <Point X="3.056919433594" Y="26.183525390625" />
                  <Point X="3.091946044922" Y="26.130287109375" />
                  <Point X="3.104977050781" Y="26.11048046875" />
                  <Point X="3.111739257813" Y="26.10142578125" />
                  <Point X="3.126288818359" Y="26.084185546875" />
                  <Point X="3.134076171875" Y="26.076" />
                  <Point X="3.151318359375" Y="26.05991015625" />
                  <Point X="3.160025146484" Y="26.052703125" />
                  <Point X="3.178238525391" Y="26.039373046875" />
                  <Point X="3.187745117188" Y="26.03325" />
                  <Point X="3.238503417969" Y="26.004677734375" />
                  <Point X="3.257387207031" Y="25.994046875" />
                  <Point X="3.265483154297" Y="25.989984375" />
                  <Point X="3.294682373047" Y="25.97808203125" />
                  <Point X="3.330277832031" Y="25.96801953125" />
                  <Point X="3.343673095703" Y="25.965255859375" />
                  <Point X="3.412301757812" Y="25.956185546875" />
                  <Point X="3.437833740234" Y="25.9528125" />
                  <Point X="3.444034912109" Y="25.95219921875" />
                  <Point X="3.4657109375" Y="25.95122265625" />
                  <Point X="3.491214111328" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="3.885008056641" Y="26.003404296875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.727604003906" Y="26.017251953125" />
                  <Point X="4.752683105469" Y="25.91423828125" />
                  <Point X="4.78387109375" Y="25.713923828125" />
                  <Point X="4.639537597656" Y="25.675248046875" />
                  <Point X="3.691991210938" Y="25.421353515625" />
                  <Point X="3.686028320312" Y="25.41954296875" />
                  <Point X="3.665611816406" Y="25.412130859375" />
                  <Point X="3.642374755859" Y="25.40161328125" />
                  <Point X="3.634007080078" Y="25.397314453125" />
                  <Point X="3.566581542969" Y="25.358341796875" />
                  <Point X="3.541497070312" Y="25.34384375" />
                  <Point X="3.533890869141" Y="25.338951171875" />
                  <Point X="3.508781738281" Y="25.319876953125" />
                  <Point X="3.481994628906" Y="25.2943515625" />
                  <Point X="3.472795898438" Y="25.284224609375" />
                  <Point X="3.432340576172" Y="25.232673828125" />
                  <Point X="3.417290039063" Y="25.21349609375" />
                  <Point X="3.410854248047" Y="25.20420703125" />
                  <Point X="3.399130615234" Y="25.184927734375" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005371094" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.356891113281" Y="25.04005859375" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.00496484375" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.365360107422" Y="24.853162109375" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158691406" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067871094" Y="24.783966796875" />
                  <Point X="3.393841796875" Y="24.762501953125" />
                  <Point X="3.399130859375" Y="24.752509765625" />
                  <Point X="3.410854980469" Y="24.73323046875" />
                  <Point X="3.417290039063" Y="24.723943359375" />
                  <Point X="3.457745361328" Y="24.672392578125" />
                  <Point X="3.472795898438" Y="24.65321484375" />
                  <Point X="3.478713867188" Y="24.64637109375" />
                  <Point X="3.501138427734" Y="24.624193359375" />
                  <Point X="3.530176269531" Y="24.6012734375" />
                  <Point X="3.541494384766" Y="24.593595703125" />
                  <Point X="3.608919921875" Y="24.554623046875" />
                  <Point X="3.634004394531" Y="24.540123046875" />
                  <Point X="3.639492675781" Y="24.537185546875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683026367188" Y="24.518970703125" />
                  <Point X="3.691992919922" Y="24.516083984375" />
                  <Point X="4.044509277344" Y="24.42162890625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.7756171875" Y="24.161830078125" />
                  <Point X="4.76161328125" Y="24.0689453125" />
                  <Point X="4.727801757812" Y="23.920779296875" />
                  <Point X="4.535668457031" Y="23.94607421875" />
                  <Point X="3.43678125" Y="24.09074609375" />
                  <Point X="3.428622558594" Y="24.09146484375" />
                  <Point X="3.400096679688" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354481445312" Y="24.087322265625" />
                  <Point X="3.222148925781" Y="24.05855859375" />
                  <Point X="3.172917236328" Y="24.047859375" />
                  <Point X="3.157876953125" Y="24.0432578125" />
                  <Point X="3.128758300781" Y="24.031634765625" />
                  <Point X="3.114679931641" Y="24.02461328125" />
                  <Point X="3.086852050781" Y="24.007720703125" />
                  <Point X="3.074124267578" Y="23.998470703125" />
                  <Point X="3.050372802734" Y="23.977998046875" />
                  <Point X="3.039349121094" Y="23.966775390625" />
                  <Point X="2.959362548828" Y="23.870576171875" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.921326416016" Y="23.82315234375" />
                  <Point X="2.906606201172" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.863693115234" Y="23.5787578125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288574219" Y="23.516677734375" />
                  <Point X="2.861607177734" Y="23.48541015625" />
                  <Point X="2.864065429688" Y="23.469873046875" />
                  <Point X="2.871796875" Y="23.438251953125" />
                  <Point X="2.876786132812" Y="23.42333203125" />
                  <Point X="2.889158203125" Y="23.39451953125" />
                  <Point X="2.896541015625" Y="23.380626953125" />
                  <Point X="2.969775878906" Y="23.266716796875" />
                  <Point X="2.997021240234" Y="23.224337890625" />
                  <Point X="3.001742675781" Y="23.217646484375" />
                  <Point X="3.0197890625" Y="23.1955546875" />
                  <Point X="3.043486083984" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.37993359375" Y="22.91269921875" />
                  <Point X="4.087170654297" Y="22.370017578125" />
                  <Point X="4.084962890625" Y="22.3664453125" />
                  <Point X="4.045496337891" Y="22.302580078125" />
                  <Point X="4.001273925781" Y="22.239748046875" />
                  <Point X="3.827161376953" Y="22.340271484375" />
                  <Point X="2.848454345703" Y="22.905328125" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815026123047" Y="22.920484375" />
                  <Point X="2.78312109375" Y="22.930671875" />
                  <Point X="2.771107910156" Y="22.933662109375" />
                  <Point X="2.613611083984" Y="22.96210546875" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.539359130859" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.46014453125" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415068847656" Y="22.9550390625" />
                  <Point X="2.40058984375" Y="22.948890625" />
                  <Point X="2.269748779297" Y="22.88003125" />
                  <Point X="2.221071777344" Y="22.854412109375" />
                  <Point X="2.208970703125" Y="22.846830078125" />
                  <Point X="2.186039306641" Y="22.8299375" />
                  <Point X="2.175208984375" Y="22.820626953125" />
                  <Point X="2.15425" Y="22.79966796875" />
                  <Point X="2.144938476562" Y="22.788837890625" />
                  <Point X="2.128045410156" Y="22.76590625" />
                  <Point X="2.120463867188" Y="22.7538046875" />
                  <Point X="2.051603027344" Y="22.622962890625" />
                  <Point X="2.025984863281" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.559806640625" />
                  <Point X="2.010012573242" Y="22.53003125" />
                  <Point X="2.006337890625" Y="22.514736328125" />
                  <Point X="2.001379760742" Y="22.4825625" />
                  <Point X="2.000279418945" Y="22.46687109375" />
                  <Point X="2.000683227539" Y="22.43551953125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.030631347656" Y="22.262361328125" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.279765380859" Y="21.762310546875" />
                  <Point X="2.735893798828" Y="20.9722734375" />
                  <Point X="2.723752929688" Y="20.963916015625" />
                  <Point X="2.580758300781" Y="21.15026953125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657104492" Y="22.129849609375" />
                  <Point X="1.808834594727" Y="22.15037109375" />
                  <Point X="1.783251831055" Y="22.17199609375" />
                  <Point X="1.773298217773" Y="22.179353515625" />
                  <Point X="1.617963745117" Y="22.27921875" />
                  <Point X="1.560174804688" Y="22.31637109375" />
                  <Point X="1.546280273438" Y="22.323755859375" />
                  <Point X="1.517467041016" Y="22.336126953125" />
                  <Point X="1.502548461914" Y="22.34111328125" />
                  <Point X="1.470928710938" Y="22.34884375" />
                  <Point X="1.455392089844" Y="22.351302734375" />
                  <Point X="1.424125488281" Y="22.35362109375" />
                  <Point X="1.408395507812" Y="22.35348046875" />
                  <Point X="1.238511108398" Y="22.33784765625" />
                  <Point X="1.17530847168" Y="22.332033203125" />
                  <Point X="1.161230834961" Y="22.329662109375" />
                  <Point X="1.13358190918" Y="22.32283203125" />
                  <Point X="1.120010498047" Y="22.318373046875" />
                  <Point X="1.092623657227" Y="22.307029296875" />
                  <Point X="1.079877929688" Y="22.300587890625" />
                  <Point X="1.055494995117" Y="22.2858671875" />
                  <Point X="1.043857910156" Y="22.277587890625" />
                  <Point X="0.912677490234" Y="22.168515625" />
                  <Point X="0.863874206543" Y="22.127935546875" />
                  <Point X="0.852651062012" Y="22.116908203125" />
                  <Point X="0.832179321289" Y="22.09315625" />
                  <Point X="0.822930664062" Y="22.080431640625" />
                  <Point X="0.806039367676" Y="22.05260546875" />
                  <Point X="0.799017822266" Y="22.03852734375" />
                  <Point X="0.787394165039" Y="22.009408203125" />
                  <Point X="0.782792053223" Y="21.9943671875" />
                  <Point X="0.743569763184" Y="21.8139140625" />
                  <Point X="0.728977783203" Y="21.74678125" />
                  <Point X="0.727584899902" Y="21.7387109375" />
                  <Point X="0.72472479248" Y="21.71032421875" />
                  <Point X="0.7247421875" Y="21.67683203125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.785129150391" Y="21.211966796875" />
                  <Point X="0.833091064453" Y="20.847662109375" />
                  <Point X="0.655065124512" Y="21.512064453125" />
                  <Point X="0.652606445312" Y="21.519876953125" />
                  <Point X="0.642143859863" Y="21.546423828125" />
                  <Point X="0.626786621094" Y="21.576185546875" />
                  <Point X="0.620407592773" Y="21.586791015625" />
                  <Point X="0.501075286865" Y="21.758724609375" />
                  <Point X="0.4566796875" Y="21.82269140625" />
                  <Point X="0.446669036865" Y="21.834828125" />
                  <Point X="0.424786254883" Y="21.85728125" />
                  <Point X="0.412913818359" Y="21.86759765625" />
                  <Point X="0.386658691406" Y="21.886841796875" />
                  <Point X="0.373244873047" Y="21.895060546875" />
                  <Point X="0.345242523193" Y="21.909169921875" />
                  <Point X="0.330654144287" Y="21.915060546875" />
                  <Point X="0.145994216919" Y="21.97237109375" />
                  <Point X="0.077295059204" Y="21.993693359375" />
                  <Point X="0.06338053894" Y="21.996888671875" />
                  <Point X="0.035227794647" Y="22.001158203125" />
                  <Point X="0.020989574432" Y="22.002232421875" />
                  <Point X="-0.008651331902" Y="22.002234375" />
                  <Point X="-0.02289535141" Y="22.001162109375" />
                  <Point X="-0.051061470032" Y="21.996892578125" />
                  <Point X="-0.064983421326" Y="21.9936953125" />
                  <Point X="-0.249643508911" Y="21.9363828125" />
                  <Point X="-0.318342529297" Y="21.9150625" />
                  <Point X="-0.33292791748" Y="21.909171875" />
                  <Point X="-0.360928192139" Y="21.895064453125" />
                  <Point X="-0.374343322754" Y="21.88684765625" />
                  <Point X="-0.400599212646" Y="21.867603515625" />
                  <Point X="-0.412475952148" Y="21.857283203125" />
                  <Point X="-0.43436038208" Y="21.834826171875" />
                  <Point X="-0.444367889404" Y="21.82269140625" />
                  <Point X="-0.563700378418" Y="21.650755859375" />
                  <Point X="-0.60809564209" Y="21.586791015625" />
                  <Point X="-0.612468444824" Y="21.57987109375" />
                  <Point X="-0.625977233887" Y="21.554736328125" />
                  <Point X="-0.638778076172" Y="21.52378515625" />
                  <Point X="-0.642753173828" Y="21.512064453125" />
                  <Point X="-0.751372131348" Y="21.10669140625" />
                  <Point X="-0.985425048828" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.948702582889" Y="28.78477617013" />
                  <Point X="-3.04465484626" Y="28.674395717747" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642526849961" Y="27.986622653128" />
                  <Point X="-4.193370597969" Y="27.352949408104" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.598651595772" Y="29.042659723389" />
                  <Point X="-2.994417720515" Y="28.587382876837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.558714588593" Y="27.938233587506" />
                  <Point X="-4.162156027379" Y="27.244053620718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.355198237583" Y="29.177916732045" />
                  <Point X="-2.944180594769" Y="28.500370035927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.474902327226" Y="27.889844521884" />
                  <Point X="-4.086646476913" Y="27.186113378781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.172941479603" Y="29.242775105191" />
                  <Point X="-2.893943469024" Y="28.413357195017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.391090065858" Y="27.841455456261" />
                  <Point X="-4.011136926447" Y="27.128173136844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.113648935782" Y="29.166179331149" />
                  <Point X="-2.843706205733" Y="28.326344512335" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.307277804491" Y="27.793066390639" />
                  <Point X="-3.93562737598" Y="27.070232894907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.593872990944" Y="26.313007935261" />
                  <Point X="-4.653630904256" Y="26.244264319705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.04020949638" Y="29.105857698844" />
                  <Point X="-2.793468930036" Y="28.239331843925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.221002880611" Y="27.747510294168" />
                  <Point X="-3.860117825514" Y="27.01229265297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.480923187256" Y="26.298137777788" />
                  <Point X="-4.71064182982" Y="26.033876708833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.953602490675" Y="29.060683618813" />
                  <Point X="-2.757184028188" Y="28.136268805433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.112575129185" Y="27.727438110639" />
                  <Point X="-3.784608275048" Y="26.954352411032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.367973293405" Y="26.283267724036" />
                  <Point X="-4.747783298117" Y="25.846346293869" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.363044670715" Y="29.595238634296" />
                  <Point X="-1.467705160299" Y="29.474840513594" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.854560714385" Y="29.029814106016" />
                  <Point X="-2.750159672652" Y="27.999545358885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981976031153" Y="27.732871143789" />
                  <Point X="-3.709098984975" Y="26.896411869547" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.255023399555" Y="26.268397670283" />
                  <Point X="-4.773606358492" Y="25.671836217799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.196603980953" Y="29.641902702238" />
                  <Point X="-1.480179865241" Y="29.315685963903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.698741305023" Y="29.064259788541" />
                  <Point X="-3.633589732505" Y="26.838471284804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.142073505705" Y="26.25352761653" />
                  <Point X="-4.730159391042" Y="25.577012193306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.030164553592" Y="29.688565317953" />
                  <Point X="-3.558080480036" Y="26.78053070006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.029123611855" Y="26.238657562778" />
                  <Point X="-4.628063789622" Y="25.549655704459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.87438441726" Y="29.722965822024" />
                  <Point X="-3.491505113031" Y="26.712312855725" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.916173718004" Y="26.223787509025" />
                  <Point X="-4.525968188202" Y="25.522299215611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.734250790871" Y="29.739367075374" />
                  <Point X="-3.443216925046" Y="26.623058018388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.803223824154" Y="26.208917455272" />
                  <Point X="-4.423872586782" Y="25.494942726764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.594117164482" Y="29.755768328725" />
                  <Point X="-3.42233593031" Y="26.502274811806" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.671518526138" Y="26.215623025937" />
                  <Point X="-4.321776985362" Y="25.467586237917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.453983538093" Y="29.772169582075" />
                  <Point X="-4.219681383942" Y="25.44022974907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.354350043829" Y="29.741980762939" />
                  <Point X="-4.117585782522" Y="25.412873260222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324691790021" Y="29.631294637897" />
                  <Point X="-4.015490181102" Y="25.385516771375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.295033536213" Y="29.520608512854" />
                  <Point X="-3.913394579682" Y="25.358160282528" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.265375282404" Y="29.409922387812" />
                  <Point X="-3.811298796631" Y="25.330804002624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.631962125633" Y="24.386738835974" />
                  <Point X="-4.769709509944" Y="24.228278596886" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.235717028596" Y="29.299236262769" />
                  <Point X="-3.7092030085" Y="25.303447728563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.467862825226" Y="24.430709443572" />
                  <Point X="-4.751925316575" Y="24.103932927849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.196356063286" Y="29.199711830503" />
                  <Point X="-3.607107220369" Y="25.276091454502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.303764045978" Y="24.474679451646" />
                  <Point X="-4.730118703132" Y="23.984214523784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.433293516966" Y="29.779236772007" />
                  <Point X="0.351402023919" Y="29.685031385586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.132000721366" Y="29.128940139446" />
                  <Point X="-3.505151815369" Y="25.248573688122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.139665266729" Y="24.518649459721" />
                  <Point X="-4.701531134941" Y="23.872296715833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.548665983705" Y="29.76715356957" />
                  <Point X="0.295312950978" Y="29.475704244847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.039842157427" Y="29.090152396619" />
                  <Point X="-3.41793076352" Y="25.204105987376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.97556648748" Y="24.562619467795" />
                  <Point X="-4.672943672104" Y="23.760378786687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.664038470025" Y="29.755070389658" />
                  <Point X="0.239224587993" Y="29.26637792082" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.098676578217" Y="29.104695930675" />
                  <Point X="-3.350155708317" Y="25.137268226442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.811467708232" Y="24.606589475869" />
                  <Point X="-4.578561334516" Y="23.724149202811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.77941218602" Y="29.742988624325" />
                  <Point X="-3.305442070326" Y="25.043901339723" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.647368928983" Y="24.650559483943" />
                  <Point X="-4.436417896559" Y="23.742862479894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.888211158659" Y="29.72334348195" />
                  <Point X="-3.303680002868" Y="24.901124323221" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.479914236679" Y="24.698390028373" />
                  <Point X="-4.294274458601" Y="23.761575756976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.992251912572" Y="29.698224635078" />
                  <Point X="-4.152131020644" Y="23.780289034059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.096292666485" Y="29.673105788206" />
                  <Point X="-4.009987582687" Y="23.799002311142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.200333420398" Y="29.647986941334" />
                  <Point X="-3.86784414473" Y="23.817715588225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.30437413442" Y="29.622868048573" />
                  <Point X="-3.725700706772" Y="23.836428865307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.408414615085" Y="29.597748887365" />
                  <Point X="-3.583557053339" Y="23.855142390267" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.507739976076" Y="29.567205601447" />
                  <Point X="-3.441413315514" Y="23.873856012308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.603442122601" Y="29.532494284076" />
                  <Point X="-3.299269577689" Y="23.892569634349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.699144269127" Y="29.497782966705" />
                  <Point X="-3.170907363747" Y="23.895429426712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.794845696651" Y="29.463070822218" />
                  <Point X="-3.076058315703" Y="23.8597367318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.888451660879" Y="29.425948122957" />
                  <Point X="-3.003844284372" Y="23.798005428764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.88603012501" Y="22.783166708396" />
                  <Point X="-4.141265250426" Y="22.489552283704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977945518767" Y="29.384094986474" />
                  <Point X="-2.956383448745" Y="23.707798831413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.507993449701" Y="23.073244113206" />
                  <Point X="-4.090047278002" Y="22.403667777826" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.067439376655" Y="29.342241849991" />
                  <Point X="-2.961776300514" Y="23.556791021875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.129956774392" Y="23.363321518015" />
                  <Point X="-4.038829305578" Y="22.317783271947" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.15693331677" Y="29.300388808101" />
                  <Point X="-3.985193775996" Y="22.234679847446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24642805834" Y="29.258536688178" />
                  <Point X="-3.926430329175" Y="22.157475416932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.330251589731" Y="29.210160587235" />
                  <Point X="-3.86766635447" Y="22.080271593678" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.41380977362" Y="29.161479238908" />
                  <Point X="-3.80890229263" Y="22.003067870662" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.497367957508" Y="29.112797890581" />
                  <Point X="-3.608385879093" Y="22.088931574686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.580926099283" Y="29.064116493808" />
                  <Point X="-3.355681751112" Y="22.234830376653" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662064071718" Y="29.012651010686" />
                  <Point X="-3.102977623132" Y="22.380729178619" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.739852665568" Y="28.957332508256" />
                  <Point X="-2.850274208201" Y="22.526627160316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.817641259418" Y="28.902014005826" />
                  <Point X="-2.622213089752" Y="22.644177422658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.569281413722" Y="28.471504642479" />
                  <Point X="-2.498753600235" Y="22.641397275733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.320341755744" Y="28.040328281399" />
                  <Point X="-2.412606305947" Y="22.595694358212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.071402097765" Y="27.609151920319" />
                  <Point X="-2.348907482862" Y="22.524167428629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013093826607" Y="27.397271884061" />
                  <Point X="-2.311964373628" Y="22.421861571119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.02841634005" Y="27.270094376209" />
                  <Point X="-2.397854811704" Y="22.178251881437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.073207082852" Y="27.176816188427" />
                  <Point X="-2.646793877252" Y="21.74707620187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131552041221" Y="27.099130342018" />
                  <Point X="-2.895733044965" Y="21.315900404776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.206765956762" Y="27.040850011003" />
                  <Point X="-2.931243826061" Y="21.130245880851" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293042150032" Y="26.995295374799" />
                  <Point X="-2.855838333828" Y="21.07218593361" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.403154280339" Y="26.977160847518" />
                  <Point X="-2.780432841594" Y="21.014125986369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.544431774365" Y="26.99487797006" />
                  <Point X="-2.70139821293" Y="20.960240883024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.762418642683" Y="27.100839133326" />
                  <Point X="-2.619567020451" Y="20.90957285834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.015122045092" Y="27.246737100617" />
                  <Point X="-2.537735815457" Y="20.858904848052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.267825807394" Y="27.392635481918" />
                  <Point X="-2.175733050332" Y="21.130537349143" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.520529610223" Y="27.538533909841" />
                  <Point X="-1.969362948844" Y="21.223134950852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.773233413053" Y="27.684432337764" />
                  <Point X="-1.848223241914" Y="21.217686199327" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.959700449599" Y="27.754134082357" />
                  <Point X="-1.729132183217" Y="21.209880747597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.016706685324" Y="27.674908211713" />
                  <Point X="3.276181197094" Y="26.823031085311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029035358949" Y="26.538722321133" />
                  <Point X="-1.612342656083" Y="21.19942768667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.070923719469" Y="27.59247373169" />
                  <Point X="3.65422019524" Y="27.113111162239" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003054606431" Y="26.364030841003" />
                  <Point X="-1.519193793506" Y="21.16177915211" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122586399394" Y="27.507100803271" />
                  <Point X="4.032258722797" Y="27.403190697815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.028077873144" Y="26.248012773238" />
                  <Point X="-1.445367634134" Y="21.101902390241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.074572178868" Y="26.156694310422" />
                  <Point X="-1.373942171766" Y="21.039263942384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132103981365" Y="26.078073035188" />
                  <Point X="-1.302516635836" Y="20.976625579149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.208741844932" Y="26.021430768995" />
                  <Point X="-0.406836687354" Y="21.862183451626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.457047384539" Y="21.804422651881" />
                  <Point X="-1.234560025586" Y="20.909996673405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.296491117223" Y="25.977570716358" />
                  <Point X="-0.204199105224" Y="21.950487280988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.668347160721" Y="21.416546021671" />
                  <Point X="-1.18749716395" Y="20.819332259348" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.404655931398" Y="25.957196058122" />
                  <Point X="-0.035998517619" Y="21.999175879808" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.724435867831" Y="21.207219301773" />
                  <Point X="-1.163159860967" Y="20.702525080579" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.530081567178" Y="25.956677703741" />
                  <Point X="0.083450960603" Y="21.991782742576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.780524801109" Y="20.997892321698" />
                  <Point X="-1.13972017355" Y="20.584685313222" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.672225118117" Y="25.975391110794" />
                  <Point X="0.182581928013" Y="21.961015832424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.836613943362" Y="20.788565101225" />
                  <Point X="-1.119735913472" Y="20.46287053142" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.814368669056" Y="25.994104517848" />
                  <Point X="0.281713508757" Y="21.930249627834" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.892703085615" Y="20.579237880752" />
                  <Point X="-1.137421763755" Y="20.297721244762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.956512376573" Y="26.012818105024" />
                  <Point X="0.375695233797" Y="21.893559191938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.948792227868" Y="20.369910660279" />
                  <Point X="-1.055767365799" Y="20.246849841245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.098656238775" Y="26.031531870143" />
                  <Point X="0.448542682975" Y="21.832556552782" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.240800100976" Y="26.050245635263" />
                  <Point X="3.694756664964" Y="25.422094517505" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355261649783" Y="25.031550177631" />
                  <Point X="0.505143052325" Y="21.752863786283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.382943963177" Y="26.068959400383" />
                  <Point X="3.858855636002" Y="25.466064746207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358266298933" Y="24.890202587851" />
                  <Point X="1.12364785355" Y="22.319568126089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.763400509636" Y="21.905150962865" />
                  <Point X="0.561026809896" Y="21.672346652232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.525087825379" Y="26.087673165503" />
                  <Point X="4.02295460704" Y="25.51003497491" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.386723461798" Y="24.778134765733" />
                  <Point X="1.267753390313" Y="22.34053853965" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.725355171551" Y="21.716580764653" />
                  <Point X="0.616910567467" Y="21.591829518181" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.66723168758" Y="26.106386930623" />
                  <Point X="4.187053578078" Y="25.554005203612" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.440159402821" Y="24.694801740859" />
                  <Point X="1.404574197687" Y="22.353128830666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.736128060171" Y="21.58416951214" />
                  <Point X="0.659396413631" Y="21.495899850126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.724983067778" Y="26.028018250638" />
                  <Point X="4.351152549116" Y="25.597975432315" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.503225017681" Y="24.622546388539" />
                  <Point X="1.516075293502" Y="22.336592125426" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752684334573" Y="21.458411283916" />
                  <Point X="0.689054653758" Y="21.385213709344" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.752523123082" Y="25.914895416956" />
                  <Point X="4.515251520154" Y="25.641945661018" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.583053266423" Y="24.569574240658" />
                  <Point X="3.103344156982" Y="24.017732036501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.89856245963" Y="23.78215764149" />
                  <Point X="1.601357956724" Y="22.289894563643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.769240608975" Y="21.332653055692" />
                  <Point X="0.718712893884" Y="21.274527568562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.771692681594" Y="25.792143428212" />
                  <Point X="4.679351022813" Y="25.68591650128" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.669409380723" Y="24.524111543083" />
                  <Point X="3.274625728835" Y="24.069964902263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.865595311378" Y="23.599429232428" />
                  <Point X="2.214241795833" Y="22.850132726213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105056645387" Y="22.724529578603" />
                  <Point X="1.68210649416" Y="22.237981086801" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.785796890776" Y="21.20689483598" />
                  <Point X="0.74837113401" Y="21.163841427779" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770111814951" Y="24.495152398712" />
                  <Point X="3.41933093972" Y="24.091625161989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86731100351" Y="23.456598867216" />
                  <Point X="2.437843397403" Y="22.962552901227" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.000376867436" Y="22.459305225935" />
                  <Point X="1.762854963144" Y="22.186067531216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.80235334865" Y="21.081136818817" />
                  <Point X="0.778029374136" Y="21.053155286997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.872207739969" Y="24.467796282121" />
                  <Point X="3.533387033525" Y="24.078027645716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.909552746822" Y="23.360388390952" />
                  <Point X="2.570153290488" Y="22.969953978957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.017630569175" Y="22.334349296087" />
                  <Point X="1.834146039371" Y="22.123274489787" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818909806524" Y="20.955378801653" />
                  <Point X="0.807687614262" Y="20.942469146215" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974303664986" Y="24.44044016553" />
                  <Point X="3.646336813031" Y="24.063157460425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.963069362632" Y="23.277148171803" />
                  <Point X="2.678949446093" Y="22.950305595955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.039283328755" Y="22.214453903399" />
                  <Point X="1.893163131778" Y="22.046361845141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.076399502882" Y="24.413083948717" />
                  <Point X="3.759286592537" Y="24.048287275134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.018930792886" Y="23.196605353113" />
                  <Point X="2.78675084068" Y="22.929512871305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.077163885667" Y="22.113226456082" />
                  <Point X="1.952180224185" Y="21.969449200494" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.178495148982" Y="24.385727511269" />
                  <Point X="3.872236372043" Y="24.033417089844" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.090842724251" Y="23.13452652382" />
                  <Point X="2.877184450062" Y="22.888740795252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.127400883005" Y="22.026213467456" />
                  <Point X="2.011197316592" Y="21.892536555848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.280590795082" Y="24.35837107382" />
                  <Point X="3.985186151549" Y="24.018546904553" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.166352121095" Y="23.076586105161" />
                  <Point X="2.960996737397" Y="22.840351759502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.177637880343" Y="21.939200478831" />
                  <Point X="2.070214408999" Y="21.815623911202" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.382686441182" Y="24.331014636371" />
                  <Point X="4.098135931055" Y="24.003676719263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241861517939" Y="23.018645686501" />
                  <Point X="3.044809024731" Y="22.791962723751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.227874877681" Y="21.852187490205" />
                  <Point X="2.129231501406" Y="21.738711266556" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.484782087282" Y="24.303658198922" />
                  <Point X="4.211085710562" Y="23.988806533972" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.317370914783" Y="22.960705267842" />
                  <Point X="3.128621312066" Y="22.743573688001" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.278111875019" Y="21.765174501579" />
                  <Point X="2.188248593812" Y="21.66179862191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.586877733382" Y="24.276301761473" />
                  <Point X="4.324035490068" Y="23.973936348682" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.392880317697" Y="22.902764856166" />
                  <Point X="3.2124335994" Y="22.69518465225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.328348875803" Y="21.678161516917" />
                  <Point X="2.247265686219" Y="21.584885977264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.688973379481" Y="24.248945324024" />
                  <Point X="4.436985269574" Y="23.959066163391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.468389749946" Y="22.844824478235" />
                  <Point X="3.296245886735" Y="22.6467956165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.378585876703" Y="21.59114853239" />
                  <Point X="2.306282778626" Y="21.507973332617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.783275043445" Y="24.212622935759" />
                  <Point X="4.549935050072" Y="23.944195979242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.543899182194" Y="22.786884100303" />
                  <Point X="3.380058174069" Y="22.59840658075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.428822877604" Y="21.504135547862" />
                  <Point X="2.365299871033" Y="21.431060687971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.753554041897" Y="24.033628791309" />
                  <Point X="4.662884837433" Y="23.929325802987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.619408614442" Y="22.728943722372" />
                  <Point X="3.463870461404" Y="22.550017544999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479059878505" Y="21.417122563335" />
                  <Point X="2.42431696344" Y="21.354148043325" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.69491804669" Y="22.67100334444" />
                  <Point X="3.547682748739" Y="22.501628509249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.529296879405" Y="21.330109578807" />
                  <Point X="2.483334055847" Y="21.277235398679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.770427478939" Y="22.613062966509" />
                  <Point X="3.631495036073" Y="22.453239473498" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.579533880306" Y="21.24309659428" />
                  <Point X="2.542351148254" Y="21.200322754033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.845936911187" Y="22.555122588578" />
                  <Point X="3.715307323408" Y="22.404850437748" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.629770881207" Y="21.156083609753" />
                  <Point X="2.601368292844" Y="21.123410169417" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.921446343435" Y="22.497182210646" />
                  <Point X="3.799119610742" Y="22.356461401997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.680007882107" Y="21.069070625225" />
                  <Point X="2.660385534681" Y="21.04649769667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.996955775684" Y="22.439241832715" />
                  <Point X="3.882931984617" Y="22.3080724658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.730244883008" Y="20.982057640698" />
                  <Point X="2.719402776517" Y="20.969585223923" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.072465207932" Y="22.381301454783" />
                  <Point X="3.966744402005" Y="22.259683579659" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.763501281738" Y="20.37326953125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464319122314" Y="21.478455078125" />
                  <Point X="0.344986785889" Y="21.650388671875" />
                  <Point X="0.300591339111" Y="21.71435546875" />
                  <Point X="0.274336212158" Y="21.733599609375" />
                  <Point X="0.089676300049" Y="21.79091015625" />
                  <Point X="0.020977081299" Y="21.812232421875" />
                  <Point X="-0.008663844109" Y="21.812234375" />
                  <Point X="-0.193323745728" Y="21.754921875" />
                  <Point X="-0.262022979736" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.407611358643" Y="21.542421875" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.567846069336" Y="21.057515625" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-1.020444702148" Y="20.0464453125" />
                  <Point X="-1.100255615234" Y="20.0619375" />
                  <Point X="-1.314304199219" Y="20.1170078125" />
                  <Point X="-1.35158984375" Y="20.1266015625" />
                  <Point X="-1.344295654297" Y="20.182005859375" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.353798461914" Y="20.6870234375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.556294067383" Y="20.946466796875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.874883789062" Y="21.029025390625" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.177896484375" Y="20.900580078125" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.320715332031" Y="20.763232421875" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.738864501953" Y="20.75996484375" />
                  <Point X="-2.855839599609" Y="20.832392578125" />
                  <Point X="-3.152242431641" Y="21.06061328125" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-3.048612548828" Y="21.43110546875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513982421875" Y="22.43123828125" />
                  <Point X="-2.531324707031" Y="22.448580078125" />
                  <Point X="-2.560154785156" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-2.941308837891" Y="22.254673828125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-4.069291259766" Y="22.031458984375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.374205078125" Y="22.509203125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-4.113124023438" Y="22.84840234375" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145821777344" Y="23.603986328125" />
                  <Point X="-3.140206298828" Y="23.62566796875" />
                  <Point X="-3.1381171875" Y="23.633734375" />
                  <Point X="-3.140326171875" Y="23.665404296875" />
                  <Point X="-3.161158203125" Y="23.689359375" />
                  <Point X="-3.180459960938" Y="23.70071875" />
                  <Point X="-3.187640869141" Y="23.7049453125" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-3.660897705078" Y="23.6533203125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.891249511719" Y="23.84730859375" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.983615722656" Y="24.38191015625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.638901855469" Y="24.581583984375" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.541897216797" Y="24.87857421875" />
                  <Point X="-3.521669677734" Y="24.89261328125" />
                  <Point X="-3.514144287109" Y="24.8978359375" />
                  <Point X="-3.494898681641" Y="24.924091796875" />
                  <Point X="-3.488156005859" Y="24.94581640625" />
                  <Point X="-3.485647705078" Y="24.9538984375" />
                  <Point X="-3.485647216797" Y="24.983537109375" />
                  <Point X="-3.492389648438" Y="25.00526171875" />
                  <Point X="-3.494898193359" Y="25.013345703125" />
                  <Point X="-3.514144287109" Y="25.039603515625" />
                  <Point X="-3.534371826172" Y="25.053642578125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-3.959792236328" Y="25.173888671875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.940938476562" Y="25.83900390625" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.804470703125" Y="26.414064453125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.528467285156" Y="26.496037109375" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731703857422" Y="26.3958671875" />
                  <Point X="-3.686933837891" Y="26.409982421875" />
                  <Point X="-3.670277832031" Y="26.415234375" />
                  <Point X="-3.651533935547" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.621155517578" Y="26.487154296875" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.637991943359" Y="26.587150390625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-3.890744873047" Y="26.796302734375" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.250523925781" Y="27.63194140625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.860219726562" Y="28.172349609375" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.635263916016" Y="28.20182421875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514404297" Y="27.92043359375" />
                  <Point X="-3.076162109375" Y="27.914978515625" />
                  <Point X="-3.052965087891" Y="27.91294921875" />
                  <Point X="-3.031506835938" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.968994628906" Y="27.971662109375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.943529296875" Y="28.090193359375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.054331542969" Y="28.311158203125" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-2.910513183594" Y="29.05347265625" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.280708251953" Y="29.436658203125" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.113577636719" Y="29.47751953125" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951248413086" Y="29.273662109375" />
                  <Point X="-1.881850463867" Y="29.23753515625" />
                  <Point X="-1.856032348633" Y="29.22409375" />
                  <Point X="-1.835125244141" Y="29.218490234375" />
                  <Point X="-1.813809082031" Y="29.22225" />
                  <Point X="-1.741526489258" Y="29.25219140625" />
                  <Point X="-1.714635009766" Y="29.263330078125" />
                  <Point X="-1.696905395508" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.662556884766" Y="29.36910546875" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.663543945312" Y="29.506736328125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.172165405273" Y="29.846080078125" />
                  <Point X="-0.968083068848" Y="29.903296875" />
                  <Point X="-0.39568270874" Y="29.9702890625" />
                  <Point X="-0.224200149536" Y="29.990359375" />
                  <Point X="-0.182985931396" Y="29.836544921875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282110214" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594028473" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.106851142883" Y="29.506458984375" />
                  <Point X="0.236648376465" Y="29.9908671875" />
                  <Point X="0.682020080566" Y="29.944224609375" />
                  <Point X="0.860209960938" Y="29.925564453125" />
                  <Point X="1.333776245117" Y="29.81123046875" />
                  <Point X="1.508454956055" Y="29.769056640625" />
                  <Point X="1.817276000977" Y="29.657046875" />
                  <Point X="1.93104699707" Y="29.615779296875" />
                  <Point X="2.229094970703" Y="29.476392578125" />
                  <Point X="2.33868359375" Y="29.425142578125" />
                  <Point X="2.62663671875" Y="29.257380859375" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="3.004088134766" Y="29.002568359375" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.857847900391" Y="28.59131640625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.209203857422" Y="27.43299609375" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.208146240234" Y="27.341724609375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.249991943359" Y="27.254669921875" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274939208984" Y="27.224205078125" />
                  <Point X="2.321081542969" Y="27.19289453125" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360338867188" Y="27.172978515625" />
                  <Point X="2.410933349609" Y="27.16687890625" />
                  <Point X="2.410933105469" Y="27.16687890625" />
                  <Point X="2.448665771484" Y="27.1659453125" />
                  <Point X="2.507182128906" Y="27.18159375" />
                  <Point X="2.528952148438" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="2.945698974609" Y="27.42605078125" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.139783203125" Y="27.829169921875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.353984863281" Y="27.491701171875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="4.1154296875" Y="27.22751953125" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371582031" Y="26.58383203125" />
                  <Point X="3.237257080078" Y="26.528890625" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.197431640625" Y="26.435404296875" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.203657470703" Y="26.328552734375" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646484375" Y="26.287955078125" />
                  <Point X="3.250673095703" Y="26.234716796875" />
                  <Point X="3.263704101562" Y="26.21491015625" />
                  <Point X="3.280946289062" Y="26.1988203125" />
                  <Point X="3.331704589844" Y="26.170248046875" />
                  <Point X="3.350588378906" Y="26.1596171875" />
                  <Point X="3.368568115234" Y="26.153619140625" />
                  <Point X="3.437196777344" Y="26.144548828125" />
                  <Point X="3.462728759766" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="3.860208251953" Y="26.191779296875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.912212402344" Y="26.062193359375" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.986896484375" Y="25.644962890625" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.688713867188" Y="25.491720703125" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729088623047" Y="25.232818359375" />
                  <Point X="3.661663085938" Y="25.193845703125" />
                  <Point X="3.636578613281" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.581809814453" Y="25.115376953125" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.5435" Y="25.0043203125" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.551968505859" Y="24.888900390625" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566759277344" Y="24.841240234375" />
                  <Point X="3.607214599609" Y="24.789689453125" />
                  <Point X="3.622265136719" Y="24.77051171875" />
                  <Point X="3.636575927734" Y="24.758091796875" />
                  <Point X="3.704001464844" Y="24.719119140625" />
                  <Point X="3.7290859375" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.093684082031" Y="24.60515625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.963494140625" Y="24.133505859375" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.887309570312" Y="23.765755859375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.510869140625" Y="23.75769921875" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394837402344" Y="23.901658203125" />
                  <Point X="3.262504882813" Y="23.87289453125" />
                  <Point X="3.213273193359" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.105458740234" Y="23.749103515625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.052893798828" Y="23.56134765625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360351562" Y="23.483376953125" />
                  <Point X="3.129595214844" Y="23.369466796875" />
                  <Point X="3.156840576172" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.495598144531" Y="23.0634375" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.24658984375" Y="22.2665625" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.077727539062" Y="22.018255859375" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.732161621094" Y="22.1757265625" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.579843994141" Y="22.77512890625" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077148438" Y="22.78075390625" />
                  <Point X="2.358236083984" Y="22.71189453125" />
                  <Point X="2.309559082031" Y="22.686275390625" />
                  <Point X="2.288600097656" Y="22.66531640625" />
                  <Point X="2.219739257812" Y="22.534474609375" />
                  <Point X="2.19412109375" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.217606689453" Y="22.296126953125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.444310302734" Y="21.857310546875" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.885680419922" Y="20.845775390625" />
                  <Point X="2.835295654297" Y="20.80978515625" />
                  <Point X="2.693969482422" Y="20.71830859375" />
                  <Point X="2.679774902344" Y="20.70912109375" />
                  <Point X="2.430021240234" Y="21.03460546875" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670548950195" Y="22.019533203125" />
                  <Point X="1.515214599609" Y="22.1193984375" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805908203" Y="22.16428125" />
                  <Point X="1.255921386719" Y="22.1486484375" />
                  <Point X="1.192718994141" Y="22.142833984375" />
                  <Point X="1.16533203125" Y="22.131490234375" />
                  <Point X="1.034151611328" Y="22.02241796875" />
                  <Point X="0.985348327637" Y="21.981837890625" />
                  <Point X="0.96845690918" Y="21.95401171875" />
                  <Point X="0.929234619141" Y="21.77355859375" />
                  <Point X="0.914642700195" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="0.97350378418" Y="21.236767578125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.042009521484" Y="20.04719921875" />
                  <Point X="0.994369445801" Y="20.0367578125" />
                  <Point X="0.863788085938" Y="20.01303515625" />
                  <Point X="0.860200500488" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#190" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.140845616077" Y="4.88076749562" Z="1.85" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="-0.407650953605" Y="5.05122989569" Z="1.85" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.85" />
                  <Point X="-1.191819235171" Y="4.925505566177" Z="1.85" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.85" />
                  <Point X="-1.71927216427" Y="4.531490947242" Z="1.85" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.85" />
                  <Point X="-1.716398526572" Y="4.415420822026" Z="1.85" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.85" />
                  <Point X="-1.766816500346" Y="4.329665242761" Z="1.85" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.85" />
                  <Point X="-1.864917246349" Y="4.313164777058" Z="1.85" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.85" />
                  <Point X="-2.080065939453" Y="4.539237468223" Z="1.85" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.85" />
                  <Point X="-2.31114725499" Y="4.511645167607" Z="1.85" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.85" />
                  <Point X="-2.947091360621" Y="4.12443247928" Z="1.85" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.85" />
                  <Point X="-3.10378877057" Y="3.317439488696" Z="1.85" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.85" />
                  <Point X="-2.999495190897" Y="3.117115900401" Z="1.85" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.85" />
                  <Point X="-3.010505415587" Y="3.038298152299" Z="1.85" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.85" />
                  <Point X="-3.077960565206" Y="2.996069508276" Z="1.85" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.85" />
                  <Point X="-3.616419859601" Y="3.276405178175" Z="1.85" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.85" />
                  <Point X="-3.905838984517" Y="3.234333009581" Z="1.85" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.85" />
                  <Point X="-4.299861881057" Y="2.688427312386" Z="1.85" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.85" />
                  <Point X="-3.927338854868" Y="1.7879151881" Z="1.85" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.85" />
                  <Point X="-3.68849822299" Y="1.595343190706" Z="1.85" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.85" />
                  <Point X="-3.673505683189" Y="1.537569587837" Z="1.85" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.85" />
                  <Point X="-3.708125828762" Y="1.488948523886" Z="1.85" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.85" />
                  <Point X="-4.528097045836" Y="1.576889710165" Z="1.85" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.85" />
                  <Point X="-4.858886649521" Y="1.458423276055" Z="1.85" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.85" />
                  <Point X="-4.996555697973" Y="0.877603758203" Z="1.85" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.85" />
                  <Point X="-3.978889213866" Y="0.156872624547" Z="1.85" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.85" />
                  <Point X="-3.569035445265" Y="0.043846050766" Z="1.85" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.85" />
                  <Point X="-3.546299386176" Y="0.021724690992" Z="1.85" />
                  <Point X="-3.539556741714" Y="0" Z="1.85" />
                  <Point X="-3.542065214959" Y="-0.008082260069" Z="1.85" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.85" />
                  <Point X="-3.556333149858" Y="-0.035029932368" Z="1.85" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.85" />
                  <Point X="-4.657998574907" Y="-0.338839432209" Z="1.85" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.85" />
                  <Point X="-5.039267824775" Y="-0.593886927682" Z="1.85" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.85" />
                  <Point X="-4.945850354392" Y="-1.133785952976" Z="1.85" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.85" />
                  <Point X="-3.660527541549" Y="-1.364970582421" Z="1.85" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.85" />
                  <Point X="-3.211978042183" Y="-1.311089641238" Z="1.85" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.85" />
                  <Point X="-3.19476540718" Y="-1.330515906967" Z="1.85" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.85" />
                  <Point X="-4.14971775615" Y="-2.080649004995" Z="1.85" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.85" />
                  <Point X="-4.423304640741" Y="-2.485125996116" Z="1.85" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.85" />
                  <Point X="-4.115233149693" Y="-2.967543663381" Z="1.85" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.85" />
                  <Point X="-2.922464696519" Y="-2.757347169988" Z="1.85" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.85" />
                  <Point X="-2.568135383896" Y="-2.560195085235" Z="1.85" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.85" />
                  <Point X="-3.09806984318" Y="-3.512613685973" Z="1.85" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.85" />
                  <Point X="-3.188902084744" Y="-3.94772363362" Z="1.85" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.85" />
                  <Point X="-2.771342103613" Y="-4.251266603205" Z="1.85" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.85" />
                  <Point X="-2.28720347977" Y="-4.235924402299" Z="1.85" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.85" />
                  <Point X="-2.156273808638" Y="-4.109714038081" Z="1.85" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.85" />
                  <Point X="-1.884309805758" Y="-3.98958649778" Z="1.85" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.85" />
                  <Point X="-1.595417340278" Y="-4.059844340728" Z="1.85" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.85" />
                  <Point X="-1.408993748942" Y="-4.291450282129" Z="1.85" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.85" />
                  <Point X="-1.400023887479" Y="-4.780187569521" Z="1.85" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.85" />
                  <Point X="-1.332919706917" Y="-4.900132698248" Z="1.85" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.85" />
                  <Point X="-1.036127971058" Y="-4.971359116382" Z="1.85" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="-0.525705814637" Y="-3.924144863561" Z="1.85" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="-0.372691435677" Y="-3.45480785161" Z="1.85" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="-0.184659870035" Y="-3.261550977512" Z="1.85" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.85" />
                  <Point X="0.068699209326" Y="-3.225561067776" Z="1.85" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.85" />
                  <Point X="0.297754423755" Y="-3.346837904158" Z="1.85" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.85" />
                  <Point X="0.709049045764" Y="-4.608391166255" Z="1.85" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.85" />
                  <Point X="0.866568471871" Y="-5.004879534438" Z="1.85" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.85" />
                  <Point X="1.046558250586" Y="-4.970359663257" Z="1.85" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.85" />
                  <Point X="1.016920164634" Y="-3.725425989298" Z="1.85" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.85" />
                  <Point X="0.971937702904" Y="-3.205779420157" Z="1.85" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.85" />
                  <Point X="1.059963241597" Y="-2.984747731877" Z="1.85" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.85" />
                  <Point X="1.25434603461" Y="-2.869859508221" Z="1.85" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.85" />
                  <Point X="1.48201963014" Y="-2.891379688441" Z="1.85" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.85" />
                  <Point X="2.3841976057" Y="-3.964550741783" Z="1.85" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.85" />
                  <Point X="2.714983136558" Y="-4.292385865396" Z="1.85" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.85" />
                  <Point X="2.908586408766" Y="-4.163632444467" Z="1.85" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.85" />
                  <Point X="2.481455643094" Y="-3.086408019527" Z="1.85" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.85" />
                  <Point X="2.260654917455" Y="-2.663704935811" Z="1.85" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.85" />
                  <Point X="2.257829450342" Y="-2.45753124579" Z="1.85" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.85" />
                  <Point X="2.375367132582" Y="-2.301071859625" Z="1.85" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.85" />
                  <Point X="2.564801867702" Y="-2.242793092036" Z="1.85" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.85" />
                  <Point X="3.70100557758" Y="-2.836293857181" Z="1.85" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.85" />
                  <Point X="4.112460354889" Y="-2.979241340339" Z="1.85" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.85" />
                  <Point X="4.282967523818" Y="-2.728442348251" Z="1.85" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.85" />
                  <Point X="3.51988026652" Y="-1.865614450636" Z="1.85" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.85" />
                  <Point X="3.165497231376" Y="-1.572214299423" Z="1.85" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.85" />
                  <Point X="3.096527466974" Y="-1.411954152346" Z="1.85" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.85" />
                  <Point X="3.137748949754" Y="-1.251583205888" Z="1.85" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.85" />
                  <Point X="3.266967338664" Y="-1.144683445421" Z="1.85" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.85" />
                  <Point X="4.498186694127" Y="-1.260591609819" Z="1.85" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.85" />
                  <Point X="4.929900680238" Y="-1.214089418129" Z="1.85" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.85" />
                  <Point X="5.006779368694" Y="-0.842669304092" Z="1.85" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.85" />
                  <Point X="4.100469216597" Y="-0.315267148572" Z="1.85" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.85" />
                  <Point X="3.722868290457" Y="-0.206311398731" Z="1.85" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.85" />
                  <Point X="3.640392069169" Y="-0.148160148139" Z="1.85" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.85" />
                  <Point X="3.594919841868" Y="-0.07041424442" Z="1.85" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.85" />
                  <Point X="3.586451608556" Y="0.026196286773" Z="1.85" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.85" />
                  <Point X="3.614987369232" Y="0.115788590436" Z="1.85" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.85" />
                  <Point X="3.680527123895" Y="0.181837445511" Z="1.85" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.85" />
                  <Point X="4.695498600709" Y="0.474704784197" Z="1.85" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.85" />
                  <Point X="5.030145494343" Y="0.683934969314" Z="1.85" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.85" />
                  <Point X="4.954637596858" Y="1.10530080206" Z="1.85" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.85" />
                  <Point X="3.84752602934" Y="1.272631926384" Z="1.85" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.85" />
                  <Point X="3.437589381952" Y="1.225398452228" Z="1.85" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.85" />
                  <Point X="3.350076926516" Y="1.24509846255" Z="1.85" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.85" />
                  <Point X="3.28628757591" Y="1.2934775419" Z="1.85" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.85" />
                  <Point X="3.246469994276" Y="1.369936040454" Z="1.85" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.85" />
                  <Point X="3.239428329133" Y="1.453218400399" Z="1.85" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.85" />
                  <Point X="3.27078382623" Y="1.52975363498" Z="1.85" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.85" />
                  <Point X="4.139711591082" Y="2.219131339389" Z="1.85" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.85" />
                  <Point X="4.390606108593" Y="2.548868214391" Z="1.85" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.85" />
                  <Point X="4.174212670996" Y="2.889652995241" Z="1.85" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.85" />
                  <Point X="2.914542907773" Y="2.500632194061" Z="1.85" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.85" />
                  <Point X="2.488108015953" Y="2.261177211859" Z="1.85" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.85" />
                  <Point X="2.410766857639" Y="2.247799096782" Z="1.85" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.85" />
                  <Point X="2.343000406177" Y="2.26554861784" Z="1.85" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.85" />
                  <Point X="2.285209982395" Y="2.314024454205" Z="1.85" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.85" />
                  <Point X="2.251630605977" Y="2.378991586193" Z="1.85" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.85" />
                  <Point X="2.251350730798" Y="2.451361559929" Z="1.85" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.85" />
                  <Point X="2.894993158799" Y="3.597596174182" Z="1.85" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.85" />
                  <Point X="3.026909100427" Y="4.074597017319" Z="1.85" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.85" />
                  <Point X="2.645650556547" Y="4.33186407796" Z="1.85" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.85" />
                  <Point X="2.244120274498" Y="4.552964840587" Z="1.85" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.85" />
                  <Point X="1.828169053761" Y="4.735330516528" Z="1.85" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.85" />
                  <Point X="1.339354656043" Y="4.891114719056" Z="1.85" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.85" />
                  <Point X="0.681071780049" Y="5.025234493856" Z="1.85" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.85" />
                  <Point X="0.052398945476" Y="4.550680010331" Z="1.85" />
                  <Point X="0" Y="4.355124473572" Z="1.85" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>