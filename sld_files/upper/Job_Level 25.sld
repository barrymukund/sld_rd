<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#165" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1878" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004717773438" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.752753173828" Y="20.78043359375" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.464929443359" Y="21.64419140625" />
                  <Point X="0.378635223389" Y="21.768525390625" />
                  <Point X="0.35674887085" Y="21.790982421875" />
                  <Point X="0.330493835449" Y="21.810224609375" />
                  <Point X="0.302494567871" Y="21.824330078125" />
                  <Point X="0.182670761108" Y="21.861517578125" />
                  <Point X="0.04913558197" Y="21.902962890625" />
                  <Point X="0.020983318329" Y="21.907232421875" />
                  <Point X="-0.008658161163" Y="21.907234375" />
                  <Point X="-0.036824237823" Y="21.90296484375" />
                  <Point X="-0.156648040771" Y="21.865775390625" />
                  <Point X="-0.290183380127" Y="21.82433203125" />
                  <Point X="-0.318184448242" Y="21.810224609375" />
                  <Point X="-0.344439941406" Y="21.79098046875" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.443757324219" Y="21.656955078125" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.762120117188" Y="20.69952734375" />
                  <Point X="-0.916584594727" Y="20.12305859375" />
                  <Point X="-0.925639648438" Y="20.12481640625" />
                  <Point X="-1.079325561523" Y="20.154646484375" />
                  <Point X="-1.214963012695" Y="20.189544921875" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.237768554688" Y="20.2633359375" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.245134643555" Y="20.627685546875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937866211" Y="20.817033203125" />
                  <Point X="-1.304009765625" Y="20.84487109375" />
                  <Point X="-1.323645019531" Y="20.868796875" />
                  <Point X="-1.433963623047" Y="20.96554296875" />
                  <Point X="-1.556905883789" Y="21.073361328125" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886108398" Y="21.102005859375" />
                  <Point X="-1.643027954102" Y="21.109033203125" />
                  <Point X="-1.7894453125" Y="21.11862890625" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.98341418457" Y="21.126291015625" />
                  <Point X="-2.014463134766" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.16466015625" Y="21.0236796875" />
                  <Point X="-2.300623779297" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.453635986328" Y="20.746060546875" />
                  <Point X="-2.480147460938" Y="20.711509765625" />
                  <Point X="-2.576419677734" Y="20.771119140625" />
                  <Point X="-2.801723876953" Y="20.91062109375" />
                  <Point X="-2.989504150391" Y="21.05520703125" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.775835205078" Y="21.7135703125" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858398438" Y="22.352349609375" />
                  <Point X="-2.406587890625" Y="22.38387890625" />
                  <Point X="-2.405576660156" Y="22.41481640625" />
                  <Point X="-2.414563476562" Y="22.44443359375" />
                  <Point X="-2.428783691406" Y="22.473263671875" />
                  <Point X="-2.446811523438" Y="22.49841796875" />
                  <Point X="-2.464155029297" Y="22.515759765625" />
                  <Point X="-2.489305419922" Y="22.533783203125" />
                  <Point X="-2.518134765625" Y="22.548001953125" />
                  <Point X="-2.547754394531" Y="22.556986328125" />
                  <Point X="-2.578690673828" Y="22.555974609375" />
                  <Point X="-2.610218017578" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.318773193359" Y="22.146439453125" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-3.904869140625" Y="21.972294921875" />
                  <Point X="-4.082862792969" Y="22.206142578125" />
                  <Point X="-4.217492675781" Y="22.431896484375" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.721969482422" Y="23.02880078125" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577148438" Y="23.524404296875" />
                  <Point X="-3.066612548828" Y="23.55153515625" />
                  <Point X="-3.053856689453" Y="23.580166015625" />
                  <Point X="-3.046152099609" Y="23.6099140625" />
                  <Point X="-3.043347900391" Y="23.640341796875" />
                  <Point X="-3.045556640625" Y="23.672013671875" />
                  <Point X="-3.052557617188" Y="23.7017578125" />
                  <Point X="-3.068640136719" Y="23.7277421875" />
                  <Point X="-3.089473144531" Y="23.75169921875" />
                  <Point X="-3.112974365234" Y="23.771234375" />
                  <Point X="-3.13945703125" Y="23.7868203125" />
                  <Point X="-3.168721923828" Y="23.798044921875" />
                  <Point X="-3.200609130859" Y="23.804525390625" />
                  <Point X="-3.231929199219" Y="23.805615234375" />
                  <Point X="-4.089846679687" Y="23.692669921875" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.764463378906" Y="23.73480859375" />
                  <Point X="-4.834078125" Y="24.00734765625" />
                  <Point X="-4.869697753906" Y="24.2563984375" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.234609863281" Y="24.591560546875" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.510474853516" Y="24.7887890625" />
                  <Point X="-3.481781005859" Y="24.805119140625" />
                  <Point X="-3.468558105469" Y="24.814201171875" />
                  <Point X="-3.442112548828" Y="24.83587109375" />
                  <Point X="-3.425921386719" Y="24.85289453125" />
                  <Point X="-3.414402587891" Y="24.87337109375" />
                  <Point X="-3.402602050781" Y="24.902205078125" />
                  <Point X="-3.398008300781" Y="24.916603515625" />
                  <Point X="-3.390885253906" Y="24.947134765625" />
                  <Point X="-3.388401123047" Y="24.9685234375" />
                  <Point X="-3.390797851562" Y="24.989923828125" />
                  <Point X="-3.397419921875" Y="25.018841796875" />
                  <Point X="-3.401939941406" Y="25.03322265625" />
                  <Point X="-3.414240966797" Y="25.063669921875" />
                  <Point X="-3.425666748047" Y="25.084197265625" />
                  <Point X="-3.441780273438" Y="25.10129296875" />
                  <Point X="-3.466724365234" Y="25.121921875" />
                  <Point X="-3.479891845703" Y="25.131056640625" />
                  <Point X="-3.510087646484" Y="25.1484296875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.314908203125" Y="25.36739453125" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.869352539063" Y="25.673783203125" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.75278515625" Y="26.241578125" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.268687011719" Y="26.366015625" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744985351562" Y="26.299341796875" />
                  <Point X="-3.723422363281" Y="26.301228515625" />
                  <Point X="-3.703140136719" Y="26.305263671875" />
                  <Point X="-3.674089355469" Y="26.314421875" />
                  <Point X="-3.641714111328" Y="26.324630859375" />
                  <Point X="-3.622773681641" Y="26.33296484375" />
                  <Point X="-3.604030761719" Y="26.343787109375" />
                  <Point X="-3.587352294922" Y="26.356015625" />
                  <Point X="-3.573715576172" Y="26.37156640625" />
                  <Point X="-3.561301513672" Y="26.389294921875" />
                  <Point X="-3.551351318359" Y="26.407431640625" />
                  <Point X="-3.539694580078" Y="26.43557421875" />
                  <Point X="-3.526703857422" Y="26.466935546875" />
                  <Point X="-3.520915771484" Y="26.48679296875" />
                  <Point X="-3.517157226562" Y="26.508109375" />
                  <Point X="-3.515804443359" Y="26.52875" />
                  <Point X="-3.518951660156" Y="26.5491953125" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532050048828" Y="26.589376953125" />
                  <Point X="-3.546115234375" Y="26.616396484375" />
                  <Point X="-3.561789794922" Y="26.646505859375" />
                  <Point X="-3.573281494141" Y="26.663705078125" />
                  <Point X="-3.587193603516" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.050711425781" Y="27.038794921875" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.255479980469" Y="27.43499609375" />
                  <Point X="-4.081156005859" Y="27.733654296875" />
                  <Point X="-3.891216552734" Y="27.977794921875" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.515597900391" Y="28.0230390625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729980469" Y="27.836341796875" />
                  <Point X="-3.167088378906" Y="27.82983203125" />
                  <Point X="-3.146793457031" Y="27.825794921875" />
                  <Point X="-3.106333740234" Y="27.822255859375" />
                  <Point X="-3.061244140625" Y="27.818310546875" />
                  <Point X="-3.040561523438" Y="27.81876171875" />
                  <Point X="-3.019102783203" Y="27.821587890625" />
                  <Point X="-2.999013916016" Y="27.82650390625" />
                  <Point X="-2.980465087891" Y="27.83565234375" />
                  <Point X="-2.962210693359" Y="27.84728125" />
                  <Point X="-2.946077880859" Y="27.860228515625" />
                  <Point X="-2.917359130859" Y="27.888947265625" />
                  <Point X="-2.885354248047" Y="27.920951171875" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846975585938" Y="28.076580078125" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869795166016" Y="28.181533203125" />
                  <Point X="-3.068572753906" Y="28.52582421875" />
                  <Point X="-3.183333007812" Y="28.72459375" />
                  <Point X="-3.004242431641" Y="28.86190234375" />
                  <Point X="-2.70062109375" Y="29.094685546875" />
                  <Point X="-2.401482910156" Y="29.260880859375" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.137771728516" Y="29.35299609375" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028891967773" Y="29.21480078125" />
                  <Point X="-2.01231237793" Y="29.200888671875" />
                  <Point X="-1.995115234375" Y="29.189396484375" />
                  <Point X="-1.950083618164" Y="29.165953125" />
                  <Point X="-1.899899169922" Y="29.139828125" />
                  <Point X="-1.880625732422" Y="29.13233203125" />
                  <Point X="-1.859719238281" Y="29.126728515625" />
                  <Point X="-1.839269042969" Y="29.123580078125" />
                  <Point X="-1.818622314453" Y="29.12493359375" />
                  <Point X="-1.797307373047" Y="29.128693359375" />
                  <Point X="-1.777452026367" Y="29.134482421875" />
                  <Point X="-1.730548583984" Y="29.153912109375" />
                  <Point X="-1.678278076172" Y="29.1755625" />
                  <Point X="-1.660147827148" Y="29.185509765625" />
                  <Point X="-1.642417602539" Y="29.197923828125" />
                  <Point X="-1.626865356445" Y="29.2115625" />
                  <Point X="-1.614633789062" Y="29.2282421875" />
                  <Point X="-1.603811523438" Y="29.246986328125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.580213989258" Y="29.31433984375" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.580329101562" Y="29.60248046875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.342686035156" Y="29.699609375" />
                  <Point X="-0.949623474121" Y="29.80980859375" />
                  <Point X="-0.586988891602" Y="29.852251953125" />
                  <Point X="-0.294711364746" Y="29.886458984375" />
                  <Point X="-0.225296463013" Y="29.6273984375" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129844666" Y="29.258123046875" />
                  <Point X="-0.10327155304" Y="29.231396484375" />
                  <Point X="-0.082113975525" Y="29.208806640625" />
                  <Point X="-0.054818080902" Y="29.194216796875" />
                  <Point X="-0.024380004883" Y="29.183884765625" />
                  <Point X="0.006155907631" Y="29.17884375" />
                  <Point X="0.036691802979" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094425926208" Y="29.208806640625" />
                  <Point X="0.115583503723" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.248066482544" Y="29.6664296875" />
                  <Point X="0.307419586182" Y="29.887939453125" />
                  <Point X="0.500843505859" Y="29.867681640625" />
                  <Point X="0.84403112793" Y="29.831740234375" />
                  <Point X="1.144072631836" Y="29.75930078125" />
                  <Point X="1.481038696289" Y="29.6779453125" />
                  <Point X="1.675537475586" Y="29.607400390625" />
                  <Point X="1.894645141602" Y="29.527927734375" />
                  <Point X="2.083485351562" Y="29.43961328125" />
                  <Point X="2.294560546875" Y="29.340900390625" />
                  <Point X="2.477031494141" Y="29.23459375" />
                  <Point X="2.680973876953" Y="29.11577734375" />
                  <Point X="2.853033691406" Y="28.993416015625" />
                  <Point X="2.943259033203" Y="28.92925390625" />
                  <Point X="2.555080078125" Y="28.25690625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076660156" Y="27.5160546875" />
                  <Point X="2.122922851562" Y="27.478083984375" />
                  <Point X="2.111606933594" Y="27.435767578125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107727783203" Y="27.380951171875" />
                  <Point X="2.111687011719" Y="27.3481171875" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.160387207031" Y="27.217529296875" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.19446484375" Y="27.170328125" />
                  <Point X="2.22159765625" Y="27.14559375" />
                  <Point X="2.251538818359" Y="27.12527734375" />
                  <Point X="2.28490625" Y="27.102634765625" />
                  <Point X="2.304947509766" Y="27.0922734375" />
                  <Point X="2.327037353516" Y="27.084005859375" />
                  <Point X="2.34896484375" Y="27.078662109375" />
                  <Point X="2.381798828125" Y="27.074703125" />
                  <Point X="2.418389648438" Y="27.070291015625" />
                  <Point X="2.436468994141" Y="27.06984375" />
                  <Point X="2.473208007812" Y="27.074169921875" />
                  <Point X="2.511178710938" Y="27.08432421875" />
                  <Point X="2.553494384766" Y="27.095640625" />
                  <Point X="2.5652890625" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.375107910156" Y="27.564275390625" />
                  <Point X="3.967325927734" Y="27.90619140625" />
                  <Point X="4.002296875" Y="27.85758984375" />
                  <Point X="4.123274414062" Y="27.689458984375" />
                  <Point X="4.219190917969" Y="27.530955078125" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.767294677734" Y="27.080130859375" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221422363281" Y="26.66023828125" />
                  <Point X="3.203974365234" Y="26.641626953125" />
                  <Point X="3.176646728516" Y="26.6059765625" />
                  <Point X="3.146192138672" Y="26.56624609375" />
                  <Point X="3.136606201172" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.111450195312" Y="26.480685546875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.106095947266" Y="26.33126953125" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679931641" Y="26.2689765625" />
                  <Point X="3.136282958984" Y="26.235740234375" />
                  <Point X="3.159011230469" Y="26.2011953125" />
                  <Point X="3.184340576172" Y="26.1626953125" />
                  <Point X="3.198893310547" Y="26.145451171875" />
                  <Point X="3.216137207031" Y="26.129361328125" />
                  <Point X="3.234345703125" Y="26.11603515625" />
                  <Point X="3.267282226562" Y="26.097494140625" />
                  <Point X="3.303987792969" Y="26.07683203125" />
                  <Point X="3.320520996094" Y="26.069501953125" />
                  <Point X="3.356120361328" Y="26.0594375" />
                  <Point X="3.400652832031" Y="26.053552734375" />
                  <Point X="3.450281005859" Y="26.046994140625" />
                  <Point X="3.462698242188" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.235396484375" Y="26.145353515625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.793978027344" Y="26.146236328125" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.876162597656" Y="25.73866796875" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="4.331432617187" Y="25.49433984375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704786865234" Y="25.325583984375" />
                  <Point X="3.681547119141" Y="25.31506640625" />
                  <Point X="3.637795410156" Y="25.28977734375" />
                  <Point X="3.589037109375" Y="25.261595703125" />
                  <Point X="3.574314941406" Y="25.251099609375" />
                  <Point X="3.547530029297" Y="25.225576171875" />
                  <Point X="3.521279052734" Y="25.192125" />
                  <Point X="3.492024169922" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.454930175781" Y="25.046912109375" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978123046875" />
                  <Point X="3.445178710938" Y="24.9414453125" />
                  <Point X="3.453929199219" Y="24.895755859375" />
                  <Point X="3.463680908203" Y="24.8448359375" />
                  <Point X="3.470526855469" Y="24.8233359375" />
                  <Point X="3.480301025391" Y="24.80187109375" />
                  <Point X="3.492024169922" Y="24.782591796875" />
                  <Point X="3.518275146484" Y="24.749140625" />
                  <Point X="3.547530029297" Y="24.71186328125" />
                  <Point X="3.559999267578" Y="24.69876171875" />
                  <Point X="3.589035888672" Y="24.67584375" />
                  <Point X="3.632787597656" Y="24.6505546875" />
                  <Point X="3.681545898438" Y="24.62237109375" />
                  <Point X="3.692708984375" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.401789550781" Y="24.42424609375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.884033691406" Y="24.243697265625" />
                  <Point X="4.8550234375" Y="24.051279296875" />
                  <Point X="4.816296875" Y="23.881572265625" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="4.137438476562" Y="23.90268359375" />
                  <Point X="3.424382080078" Y="23.99655859375" />
                  <Point X="3.408035644531" Y="23.9972890625" />
                  <Point X="3.374658935547" Y="23.994490234375" />
                  <Point X="3.288789794922" Y="23.975826171875" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.163973876953" Y="23.94340234375" />
                  <Point X="3.136147216797" Y="23.926509765625" />
                  <Point X="3.112396972656" Y="23.9060390625" />
                  <Point X="3.060494384766" Y="23.843615234375" />
                  <Point X="3.002652832031" Y="23.77405078125" />
                  <Point X="2.987933349609" Y="23.749669921875" />
                  <Point X="2.97658984375" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.962318603516" Y="23.613794921875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450439453" Y="23.432001953125" />
                  <Point X="3.023971679688" Y="23.3580859375" />
                  <Point X="3.076930664062" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.746507324219" Y="22.751162109375" />
                  <Point X="4.213122070312" Y="22.3931171875" />
                  <Point X="4.20658984375" Y="22.382546875" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.04472265625" Y="22.136421875" />
                  <Point X="4.028981201172" Y="22.1140546875" />
                  <Point X="3.43602734375" Y="22.456396484375" />
                  <Point X="2.800954589844" Y="22.823056640625" />
                  <Point X="2.78612890625" Y="22.82998828125" />
                  <Point X="2.754224609375" Y="22.840173828125" />
                  <Point X="2.652026611328" Y="22.858630859375" />
                  <Point X="2.538134277344" Y="22.87919921875" />
                  <Point X="2.506783935547" Y="22.879603515625" />
                  <Point X="2.474611083984" Y="22.874646484375" />
                  <Point X="2.444834228516" Y="22.864822265625" />
                  <Point X="2.359932617188" Y="22.820140625" />
                  <Point X="2.265316162109" Y="22.77034375" />
                  <Point X="2.242385009766" Y="22.753451171875" />
                  <Point X="2.221425537109" Y="22.7324921875" />
                  <Point X="2.204531738281" Y="22.709560546875" />
                  <Point X="2.159848632812" Y="22.624658203125" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.114132080078" Y="22.33454296875" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.560435302734" Y="21.466177734375" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781837890625" Y="20.88834765625" />
                  <Point X="2.701764648438" Y="20.836517578125" />
                  <Point X="2.243165527344" Y="21.43417578125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506835938" Y="22.077818359375" />
                  <Point X="1.721923461914" Y="22.099443359375" />
                  <Point X="1.621128662109" Y="22.164244140625" />
                  <Point X="1.508800170898" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.248833984375" />
                  <Point X="1.448365478516" Y="22.256564453125" />
                  <Point X="1.417100952148" Y="22.258880859375" />
                  <Point X="1.306864501953" Y="22.24873828125" />
                  <Point X="1.184014038086" Y="22.23743359375" />
                  <Point X="1.156361938477" Y="22.2306015625" />
                  <Point X="1.12897668457" Y="22.2192578125" />
                  <Point X="1.104594848633" Y="22.204537109375" />
                  <Point X="1.019473388672" Y="22.133759765625" />
                  <Point X="0.924611206055" Y="22.054884765625" />
                  <Point X="0.904141479492" Y="22.03113671875" />
                  <Point X="0.887249206543" Y="22.003310546875" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.850173339844" Y="21.857095703125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.935540893555" Y="20.797302734375" />
                  <Point X="1.022065490723" Y="20.140083984375" />
                  <Point X="0.975699707031" Y="20.129921875" />
                  <Point X="0.929315612793" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058425415039" Y="20.24736328125" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123334106445" Y="20.502306640625" />
                  <Point X="-1.151960083008" Y="20.64621875" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124511719" Y="20.821529296875" />
                  <Point X="-1.199026245117" Y="20.85049609375" />
                  <Point X="-1.205665039062" Y="20.864533203125" />
                  <Point X="-1.221736816406" Y="20.89237109375" />
                  <Point X="-1.230573486328" Y="20.905138671875" />
                  <Point X="-1.250208862305" Y="20.929064453125" />
                  <Point X="-1.261007446289" Y="20.94022265625" />
                  <Point X="-1.371326171875" Y="21.03696875" />
                  <Point X="-1.494268310547" Y="21.144787109375" />
                  <Point X="-1.506739868164" Y="21.15403515625" />
                  <Point X="-1.533023071289" Y="21.17037890625" />
                  <Point X="-1.546834472656" Y="21.177474609375" />
                  <Point X="-1.576531494141" Y="21.189775390625" />
                  <Point X="-1.591316040039" Y="21.194525390625" />
                  <Point X="-1.621457885742" Y="21.201552734375" />
                  <Point X="-1.636815307617" Y="21.203830078125" />
                  <Point X="-1.783232666016" Y="21.21342578125" />
                  <Point X="-1.946404296875" Y="21.22412109375" />
                  <Point X="-1.961928344727" Y="21.2238671875" />
                  <Point X="-1.992725585938" Y="21.220833984375" />
                  <Point X="-2.007998779297" Y="21.2180546875" />
                  <Point X="-2.039047729492" Y="21.209736328125" />
                  <Point X="-2.053666992188" Y="21.204505859375" />
                  <Point X="-2.081861328125" Y="21.191732421875" />
                  <Point X="-2.095436767578" Y="21.184189453125" />
                  <Point X="-2.217439453125" Y="21.102669921875" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359685302734" Y="21.0072421875" />
                  <Point X="-2.380448730469" Y="20.98986328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410471435547" Y="20.958369140625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.526408447266" Y="20.851890625" />
                  <Point X="-2.747610595703" Y="20.988853515625" />
                  <Point X="-2.931546386719" Y="21.130478515625" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.693562744141" Y="21.6660703125" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947021484" Y="22.31888671875" />
                  <Point X="-2.319683105469" Y="22.333818359375" />
                  <Point X="-2.313412597656" Y="22.36534765625" />
                  <Point X="-2.311638671875" Y="22.380775390625" />
                  <Point X="-2.310627441406" Y="22.411712890625" />
                  <Point X="-2.314669433594" Y="22.442400390625" />
                  <Point X="-2.32365625" Y="22.472017578125" />
                  <Point X="-2.329363769531" Y="22.48645703125" />
                  <Point X="-2.343583984375" Y="22.515287109375" />
                  <Point X="-2.351566894531" Y="22.528603515625" />
                  <Point X="-2.369594726562" Y="22.5537578125" />
                  <Point X="-2.379639648438" Y="22.565595703125" />
                  <Point X="-2.396983154297" Y="22.5829375" />
                  <Point X="-2.408817871094" Y="22.592978515625" />
                  <Point X="-2.433968261719" Y="22.611001953125" />
                  <Point X="-2.447283935547" Y="22.618984375" />
                  <Point X="-2.47611328125" Y="22.633203125" />
                  <Point X="-2.490559570312" Y="22.638912109375" />
                  <Point X="-2.520179199219" Y="22.647896484375" />
                  <Point X="-2.550859619141" Y="22.651935546875" />
                  <Point X="-2.581795898438" Y="22.650923828125" />
                  <Point X="-2.597225097656" Y="22.6491484375" />
                  <Point X="-2.628752441406" Y="22.642876953125" />
                  <Point X="-2.643683105469" Y="22.63861328125" />
                  <Point X="-2.672648681641" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.366273193359" Y="22.2287109375" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-3.829275878906" Y="22.029833984375" />
                  <Point X="-4.004021484375" Y="22.2594140625" />
                  <Point X="-4.135899902344" Y="22.4805546875" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.664137207031" Y="22.953431640625" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036482421875" Y="23.4366875" />
                  <Point X="-3.015104980469" Y="23.459607421875" />
                  <Point X="-3.005367431641" Y="23.471955078125" />
                  <Point X="-2.987402832031" Y="23.4990859375" />
                  <Point X="-2.979835449219" Y="23.512873046875" />
                  <Point X="-2.967079589844" Y="23.54150390625" />
                  <Point X="-2.961891113281" Y="23.55634765625" />
                  <Point X="-2.954186523438" Y="23.586095703125" />
                  <Point X="-2.951552978516" Y="23.6011953125" />
                  <Point X="-2.948748779297" Y="23.631623046875" />
                  <Point X="-2.948578125" Y="23.646951171875" />
                  <Point X="-2.950786865234" Y="23.678623046875" />
                  <Point X="-2.953083740234" Y="23.693779296875" />
                  <Point X="-2.960084716797" Y="23.7235234375" />
                  <Point X="-2.971778076172" Y="23.75175390625" />
                  <Point X="-2.987860595703" Y="23.77773828125" />
                  <Point X="-2.996953857422" Y="23.790080078125" />
                  <Point X="-3.017786865234" Y="23.814037109375" />
                  <Point X="-3.028745849609" Y="23.824755859375" />
                  <Point X="-3.052247070312" Y="23.844291015625" />
                  <Point X="-3.064789306641" Y="23.853107421875" />
                  <Point X="-3.091271972656" Y="23.868693359375" />
                  <Point X="-3.105436279297" Y="23.87551953125" />
                  <Point X="-3.134701171875" Y="23.886744140625" />
                  <Point X="-3.149801757812" Y="23.891142578125" />
                  <Point X="-3.181688964844" Y="23.897623046875" />
                  <Point X="-3.197305419922" Y="23.89946875" />
                  <Point X="-3.228625488281" Y="23.90055859375" />
                  <Point X="-3.244329101562" Y="23.899802734375" />
                  <Point X="-4.102246582031" Y="23.786857421875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.672418457031" Y="23.7583203125" />
                  <Point X="-4.740763183594" Y="24.02588671875" />
                  <Point X="-4.775654785156" Y="24.26984765625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.210021972656" Y="24.499796875" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.496787841797" Y="24.6917109375" />
                  <Point X="-3.47438671875" Y="24.70091015625" />
                  <Point X="-3.463485839844" Y="24.706224609375" />
                  <Point X="-3.434791992188" Y="24.7225546875" />
                  <Point X="-3.427995849609" Y="24.726810546875" />
                  <Point X="-3.408346191406" Y="24.74071875" />
                  <Point X="-3.381900634766" Y="24.762388671875" />
                  <Point X="-3.373275878906" Y="24.770400390625" />
                  <Point X="-3.357084716797" Y="24.787423828125" />
                  <Point X="-3.343123046875" Y="24.806318359375" />
                  <Point X="-3.331604248047" Y="24.826794921875" />
                  <Point X="-3.326480712891" Y="24.837388671875" />
                  <Point X="-3.314680175781" Y="24.86622265625" />
                  <Point X="-3.312096679688" Y="24.873330078125" />
                  <Point X="-3.305492675781" Y="24.89501953125" />
                  <Point X="-3.298369628906" Y="24.92555078125" />
                  <Point X="-3.29651953125" Y="24.93617578125" />
                  <Point X="-3.294035400391" Y="24.957564453125" />
                  <Point X="-3.293991455078" Y="24.97909765625" />
                  <Point X="-3.296388183594" Y="25.000498046875" />
                  <Point X="-3.298194824219" Y="25.01112890625" />
                  <Point X="-3.304816894531" Y="25.040046875" />
                  <Point X="-3.306791015625" Y="25.047328125" />
                  <Point X="-3.313856933594" Y="25.06880859375" />
                  <Point X="-3.326157958984" Y="25.099255859375" />
                  <Point X="-3.331233398438" Y="25.109873046875" />
                  <Point X="-3.342659179688" Y="25.130400390625" />
                  <Point X="-3.35653515625" Y="25.149357421875" />
                  <Point X="-3.372648681641" Y="25.166453125" />
                  <Point X="-3.381236572266" Y="25.174501953125" />
                  <Point X="-3.406180664062" Y="25.195130859375" />
                  <Point X="-3.412573974609" Y="25.199978515625" />
                  <Point X="-3.432515625" Y="25.213400390625" />
                  <Point X="-3.462711425781" Y="25.2307734375" />
                  <Point X="-3.473796142578" Y="25.236224609375" />
                  <Point X="-3.496584472656" Y="25.24564453125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.2903203125" Y="25.459158203125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.775375976562" Y="25.659876953125" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.661092285156" Y="26.21673046875" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.281087402344" Y="26.271828125" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747057373047" Y="26.204365234375" />
                  <Point X="-3.736704589844" Y="26.204703125" />
                  <Point X="-3.715141601563" Y="26.20658984375" />
                  <Point X="-3.704885498047" Y="26.2080546875" />
                  <Point X="-3.684603271484" Y="26.21208984375" />
                  <Point X="-3.674577148438" Y="26.21466015625" />
                  <Point X="-3.645526367188" Y="26.223818359375" />
                  <Point X="-3.613151123047" Y="26.23402734375" />
                  <Point X="-3.603453125" Y="26.23767578125" />
                  <Point X="-3.584512695312" Y="26.246009765625" />
                  <Point X="-3.575270263672" Y="26.2506953125" />
                  <Point X="-3.55652734375" Y="26.261517578125" />
                  <Point X="-3.547858154297" Y="26.267173828125" />
                  <Point X="-3.5311796875" Y="26.27940234375" />
                  <Point X="-3.515925292969" Y="26.293380859375" />
                  <Point X="-3.502288574219" Y="26.308931640625" />
                  <Point X="-3.495896972656" Y="26.317076171875" />
                  <Point X="-3.483482910156" Y="26.3348046875" />
                  <Point X="-3.478012451172" Y="26.3436015625" />
                  <Point X="-3.468062255859" Y="26.36173828125" />
                  <Point X="-3.463582519531" Y="26.371078125" />
                  <Point X="-3.45192578125" Y="26.399220703125" />
                  <Point X="-3.438935058594" Y="26.43058203125" />
                  <Point X="-3.435499267578" Y="26.4403515625" />
                  <Point X="-3.429711181641" Y="26.460208984375" />
                  <Point X="-3.427358886719" Y="26.470296875" />
                  <Point X="-3.423600341797" Y="26.49161328125" />
                  <Point X="-3.422360595703" Y="26.501896484375" />
                  <Point X="-3.4210078125" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057617188" Y="26.5636484375" />
                  <Point X="-3.427189208984" Y="26.57378515625" />
                  <Point X="-3.432791503906" Y="26.59469140625" />
                  <Point X="-3.436013671875" Y="26.60453515625" />
                  <Point X="-3.443509765625" Y="26.623810546875" />
                  <Point X="-3.447783691406" Y="26.6332421875" />
                  <Point X="-3.461848876953" Y="26.66026171875" />
                  <Point X="-3.4775234375" Y="26.69037109375" />
                  <Point X="-3.482799316406" Y="26.699283203125" />
                  <Point X="-3.494291015625" Y="26.716482421875" />
                  <Point X="-3.500506835938" Y="26.72476953125" />
                  <Point X="-3.514418945312" Y="26.741349609375" />
                  <Point X="-3.521498291016" Y="26.748908203125" />
                  <Point X="-3.536440429688" Y="26.763212890625" />
                  <Point X="-3.544303222656" Y="26.769958984375" />
                  <Point X="-3.99287890625" Y="27.1141640625" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.17343359375" Y="27.387107421875" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.816235839844" Y="27.9194609375" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.563097900391" Y="27.940767578125" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244925537109" Y="27.757720703125" />
                  <Point X="-3.225998535156" Y="27.749390625" />
                  <Point X="-3.216302978516" Y="27.745740234375" />
                  <Point X="-3.195661376953" Y="27.73923046875" />
                  <Point X="-3.185622802734" Y="27.736658203125" />
                  <Point X="-3.165327880859" Y="27.73262109375" />
                  <Point X="-3.155071533203" Y="27.73115625" />
                  <Point X="-3.114611816406" Y="27.7276171875" />
                  <Point X="-3.069522216797" Y="27.723671875" />
                  <Point X="-3.059172363281" Y="27.723333984375" />
                  <Point X="-3.038489746094" Y="27.72378515625" />
                  <Point X="-3.028156982422" Y="27.72457421875" />
                  <Point X="-3.006698242188" Y="27.727400390625" />
                  <Point X="-2.996521240234" Y="27.729310546875" />
                  <Point X="-2.976432373047" Y="27.7342265625" />
                  <Point X="-2.9569921875" Y="27.741302734375" />
                  <Point X="-2.938443359375" Y="27.750451171875" />
                  <Point X="-2.929422851563" Y="27.755529296875" />
                  <Point X="-2.911168457031" Y="27.767158203125" />
                  <Point X="-2.902749755859" Y="27.77319140625" />
                  <Point X="-2.886616943359" Y="27.786138671875" />
                  <Point X="-2.878902832031" Y="27.793052734375" />
                  <Point X="-2.850184082031" Y="27.821771484375" />
                  <Point X="-2.818179199219" Y="27.853775390625" />
                  <Point X="-2.811266357422" Y="27.861486328125" />
                  <Point X="-2.798321044922" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.752337158203" Y="28.084859375" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509765625" Y="28.200861328125" />
                  <Point X="-2.782840820312" Y="28.219794921875" />
                  <Point X="-2.787522949219" Y="28.229033203125" />
                  <Point X="-2.986300537109" Y="28.57332421875" />
                  <Point X="-3.059387939453" Y="28.6999140625" />
                  <Point X="-2.946439941406" Y="28.78651171875" />
                  <Point X="-2.648378417969" Y="29.015033203125" />
                  <Point X="-2.355345214844" Y="29.1778359375" />
                  <Point X="-2.192524169922" Y="29.268296875" />
                  <Point X="-2.118564208984" Y="29.17191015625" />
                  <Point X="-2.111819824219" Y="29.164048828125" />
                  <Point X="-2.097516357422" Y="29.149107421875" />
                  <Point X="-2.089957275391" Y="29.14202734375" />
                  <Point X="-2.073377685547" Y="29.128115234375" />
                  <Point X="-2.065096191406" Y="29.12190234375" />
                  <Point X="-2.047898925781" Y="29.11041015625" />
                  <Point X="-2.038983398437" Y="29.105130859375" />
                  <Point X="-1.993951782227" Y="29.0816875" />
                  <Point X="-1.943767333984" Y="29.0555625" />
                  <Point X="-1.934335083008" Y="29.0512890625" />
                  <Point X="-1.915061645508" Y="29.04379296875" />
                  <Point X="-1.905220092773" Y="29.0405703125" />
                  <Point X="-1.884313720703" Y="29.034966796875" />
                  <Point X="-1.874174804688" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.83305456543" Y="29.028783203125" />
                  <Point X="-1.812407836914" Y="29.03013671875" />
                  <Point X="-1.802119873047" Y="29.031376953125" />
                  <Point X="-1.780804931641" Y="29.03513671875" />
                  <Point X="-1.770716064453" Y="29.037490234375" />
                  <Point X="-1.750860839844" Y="29.043279296875" />
                  <Point X="-1.741094482422" Y="29.04671484375" />
                  <Point X="-1.694191040039" Y="29.06614453125" />
                  <Point X="-1.641920532227" Y="29.087794921875" />
                  <Point X="-1.632581787109" Y="29.092275390625" />
                  <Point X="-1.614451538086" Y="29.10222265625" />
                  <Point X="-1.605660400391" Y="29.107689453125" />
                  <Point X="-1.587930175781" Y="29.120103515625" />
                  <Point X="-1.579780639648" Y="29.126498046875" />
                  <Point X="-1.564228271484" Y="29.14013671875" />
                  <Point X="-1.550256469727" Y="29.1553828125" />
                  <Point X="-1.538024780273" Y="29.1720625" />
                  <Point X="-1.532361938477" Y="29.180740234375" />
                  <Point X="-1.521539672852" Y="29.199484375" />
                  <Point X="-1.516856201172" Y="29.2087265625" />
                  <Point X="-1.508524780273" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.489610717773" Y="29.2857734375" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.317040161133" Y="29.60813671875" />
                  <Point X="-0.931164489746" Y="29.7163203125" />
                  <Point X="-0.575945251465" Y="29.757896484375" />
                  <Point X="-0.365222717285" Y="29.78255859375" />
                  <Point X="-0.317059448242" Y="29.602810546875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.220435256958" Y="29.247107421875" />
                  <Point X="-0.207661849976" Y="29.218916015625" />
                  <Point X="-0.200119247437" Y="29.20534375" />
                  <Point X="-0.182260864258" Y="29.1786171875" />
                  <Point X="-0.172608840942" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896453857" Y="29.1250234375" />
                  <Point X="-0.099600524902" Y="29.11043359375" />
                  <Point X="-0.085353973389" Y="29.1042578125" />
                  <Point X="-0.054916004181" Y="29.09392578125" />
                  <Point X="-0.039853668213" Y="29.090154296875" />
                  <Point X="-0.009317756653" Y="29.08511328125" />
                  <Point X="0.021629543304" Y="29.08511328125" />
                  <Point X="0.052165454865" Y="29.090154296875" />
                  <Point X="0.067227645874" Y="29.09392578125" />
                  <Point X="0.097665908813" Y="29.1042578125" />
                  <Point X="0.111912460327" Y="29.11043359375" />
                  <Point X="0.139208389282" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920776367" Y="29.166455078125" />
                  <Point X="0.194572952271" Y="29.1786171875" />
                  <Point X="0.212431182861" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.339829528809" Y="29.641841796875" />
                  <Point X="0.378190734863" Y="29.7850078125" />
                  <Point X="0.490947967529" Y="29.77319921875" />
                  <Point X="0.8278515625" Y="29.737916015625" />
                  <Point X="1.12177722168" Y="29.666953125" />
                  <Point X="1.453622314453" Y="29.586833984375" />
                  <Point X="1.643145629883" Y="29.51809375" />
                  <Point X="1.858245727539" Y="29.44007421875" />
                  <Point X="2.043240478516" Y="29.35355859375" />
                  <Point X="2.250434570312" Y="29.25666015625" />
                  <Point X="2.429208984375" Y="29.1525078125" />
                  <Point X="2.62942578125" Y="29.035861328125" />
                  <Point X="2.7979765625" Y="28.91599609375" />
                  <Point X="2.817778076172" Y="28.9019140625" />
                  <Point X="2.472807617188" Y="28.30440625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053179931641" Y="27.57343359375" />
                  <Point X="2.044181884766" Y="27.549560546875" />
                  <Point X="2.041301391602" Y="27.540595703125" />
                  <Point X="2.031147583008" Y="27.502625" />
                  <Point X="2.019831665039" Y="27.46030859375" />
                  <Point X="2.017912231445" Y="27.4514609375" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755371094" Y="27.383240234375" />
                  <Point X="2.013410888672" Y="27.369578125" />
                  <Point X="2.017370239258" Y="27.336744140625" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045317871094" Y="27.223888671875" />
                  <Point X="2.055680175781" Y="27.20384375" />
                  <Point X="2.061459472656" Y="27.19412890625" />
                  <Point X="2.081775878906" Y="27.1641875" />
                  <Point X="2.104417236328" Y="27.1308203125" />
                  <Point X="2.10980859375" Y="27.1236328125" />
                  <Point X="2.130464355469" Y="27.10012109375" />
                  <Point X="2.157597167969" Y="27.07538671875" />
                  <Point X="2.168256591797" Y="27.066982421875" />
                  <Point X="2.198197753906" Y="27.046666015625" />
                  <Point X="2.231565185547" Y="27.0240234375" />
                  <Point X="2.241277099609" Y="27.01824609375" />
                  <Point X="2.261318359375" Y="27.007884765625" />
                  <Point X="2.271647705078" Y="27.00330078125" />
                  <Point X="2.293737548828" Y="26.995033203125" />
                  <Point X="2.304544189453" Y="26.99170703125" />
                  <Point X="2.326471679688" Y="26.98636328125" />
                  <Point X="2.337592529297" Y="26.984345703125" />
                  <Point X="2.370426513672" Y="26.98038671875" />
                  <Point X="2.407017333984" Y="26.975974609375" />
                  <Point X="2.416040039062" Y="26.9753203125" />
                  <Point X="2.447578857422" Y="26.97549609375" />
                  <Point X="2.484317871094" Y="26.979822265625" />
                  <Point X="2.497750976562" Y="26.98239453125" />
                  <Point X="2.535721679688" Y="26.992548828125" />
                  <Point X="2.578037353516" Y="27.003865234375" />
                  <Point X="2.584005371094" Y="27.005673828125" />
                  <Point X="2.604415039063" Y="27.013072265625" />
                  <Point X="2.627659912109" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.422607910156" Y="27.48200390625" />
                  <Point X="3.940403564453" Y="27.780953125" />
                  <Point X="4.04395703125" Y="27.637037109375" />
                  <Point X="4.136885253906" Y="27.48347265625" />
                  <Point X="3.709462402344" Y="27.1555" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168133056641" Y="26.739865234375" />
                  <Point X="3.152116210938" Y="26.725212890625" />
                  <Point X="3.134668212891" Y="26.7066015625" />
                  <Point X="3.128577392578" Y="26.699421875" />
                  <Point X="3.101249755859" Y="26.663771484375" />
                  <Point X="3.070795166016" Y="26.624041015625" />
                  <Point X="3.065637451172" Y="26.616603515625" />
                  <Point X="3.049739257812" Y="26.58937109375" />
                  <Point X="3.034762939453" Y="26.555544921875" />
                  <Point X="3.030140136719" Y="26.542671875" />
                  <Point X="3.019960449219" Y="26.506271484375" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.013055908203" Y="26.312072265625" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034684814453" Y="26.22860546875" />
                  <Point X="3.050287841797" Y="26.195369140625" />
                  <Point X="3.056919677734" Y="26.183525390625" />
                  <Point X="3.079647949219" Y="26.14898046875" />
                  <Point X="3.104977294922" Y="26.11048046875" />
                  <Point X="3.111739013672" Y="26.10142578125" />
                  <Point X="3.126291748047" Y="26.084181640625" />
                  <Point X="3.134082763672" Y="26.0759921875" />
                  <Point X="3.151326660156" Y="26.05990234375" />
                  <Point X="3.160030761719" Y="26.05269921875" />
                  <Point X="3.178239257813" Y="26.039373046875" />
                  <Point X="3.187743652344" Y="26.03325" />
                  <Point X="3.220680175781" Y="26.014708984375" />
                  <Point X="3.257385742188" Y="25.994046875" />
                  <Point X="3.265483642578" Y="25.989984375" />
                  <Point X="3.294676025391" Y="25.9780859375" />
                  <Point X="3.330275390625" Y="25.968021484375" />
                  <Point X="3.343674804688" Y="25.965255859375" />
                  <Point X="3.388207275391" Y="25.95937109375" />
                  <Point X="3.437835449219" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465708496094" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.247796386719" Y="26.051166015625" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.782293457031" Y="25.724052734375" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.306844726562" Y="25.586103515625" />
                  <Point X="3.691991943359" Y="25.421353515625" />
                  <Point X="3.686024658203" Y="25.419541015625" />
                  <Point X="3.665617431641" Y="25.4121328125" />
                  <Point X="3.642377685547" Y="25.401615234375" />
                  <Point X="3.634006347656" Y="25.397314453125" />
                  <Point X="3.590254638672" Y="25.372025390625" />
                  <Point X="3.541496337891" Y="25.34384375" />
                  <Point X="3.533888183594" Y="25.33894921875" />
                  <Point X="3.508778808594" Y="25.319875" />
                  <Point X="3.481993896484" Y="25.2943515625" />
                  <Point X="3.472794921875" Y="25.284224609375" />
                  <Point X="3.446543945312" Y="25.2507734375" />
                  <Point X="3.4172890625" Y="25.21349609375" />
                  <Point X="3.410852783203" Y="25.204205078125" />
                  <Point X="3.399129638672" Y="25.18492578125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005371094" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376220703" Y="25.11047265625" />
                  <Point X="3.361625732422" Y="25.06478125" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350603271484" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973736328125" />
                  <Point X="3.350280029297" Y="24.93705859375" />
                  <Point X="3.351874511719" Y="24.923576171875" />
                  <Point X="3.360625" Y="24.87788671875" />
                  <Point X="3.370376708984" Y="24.826966796875" />
                  <Point X="3.373159179688" Y="24.81601171875" />
                  <Point X="3.380005126953" Y="24.79451171875" />
                  <Point X="3.384068603516" Y="24.783966796875" />
                  <Point X="3.393842773438" Y="24.762501953125" />
                  <Point X="3.399129638672" Y="24.752513671875" />
                  <Point X="3.410852783203" Y="24.733234375" />
                  <Point X="3.4172890625" Y="24.723943359375" />
                  <Point X="3.443540039062" Y="24.6904921875" />
                  <Point X="3.472794921875" Y="24.65321484375" />
                  <Point X="3.47871484375" Y="24.646369140625" />
                  <Point X="3.501142089844" Y="24.62419140625" />
                  <Point X="3.530178710938" Y="24.6012734375" />
                  <Point X="3.541495117188" Y="24.593595703125" />
                  <Point X="3.585246826172" Y="24.568306640625" />
                  <Point X="3.634005126953" Y="24.540123046875" />
                  <Point X="3.639487304688" Y="24.5371875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026855469" Y="24.518970703125" />
                  <Point X="3.691991943359" Y="24.516083984375" />
                  <Point X="4.377201660156" Y="24.332482421875" />
                  <Point X="4.784876953125" Y="24.22324609375" />
                  <Point X="4.761614257812" Y="24.068951171875" />
                  <Point X="4.727801757812" Y="23.92078125" />
                  <Point X="4.149838378906" Y="23.99687109375" />
                  <Point X="3.436782226562" Y="24.09074609375" />
                  <Point X="3.428623046875" Y="24.09146484375" />
                  <Point X="3.400097167969" Y="24.09195703125" />
                  <Point X="3.366720458984" Y="24.089158203125" />
                  <Point X="3.354481445312" Y="24.087322265625" />
                  <Point X="3.268612304688" Y="24.068658203125" />
                  <Point X="3.172917236328" Y="24.047859375" />
                  <Point X="3.157873535156" Y="24.0432578125" />
                  <Point X="3.128752685547" Y="24.0316328125" />
                  <Point X="3.114675537109" Y="24.024609375" />
                  <Point X="3.086848876953" Y="24.007716796875" />
                  <Point X="3.074124267578" Y="23.99846875" />
                  <Point X="3.050374023438" Y="23.977998046875" />
                  <Point X="3.039348388672" Y="23.966775390625" />
                  <Point X="2.987445800781" Y="23.9043515625" />
                  <Point X="2.929604248047" Y="23.834787109375" />
                  <Point X="2.921325195312" Y="23.823150390625" />
                  <Point X="2.906605712891" Y="23.79876953125" />
                  <Point X="2.900165283203" Y="23.786025390625" />
                  <Point X="2.888821777344" Y="23.758640625" />
                  <Point X="2.884363525391" Y="23.74507421875" />
                  <Point X="2.87753125" Y="23.717423828125" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.867718261719" Y="23.6225" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786621094" Y="23.423328125" />
                  <Point X="2.889157958984" Y="23.394517578125" />
                  <Point X="2.896540283203" Y="23.380626953125" />
                  <Point X="2.944061523438" Y="23.3067109375" />
                  <Point X="2.997020507812" Y="23.224337890625" />
                  <Point X="3.001741699219" Y="23.217646484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.688675048828" Y="22.67579296875" />
                  <Point X="4.087170410156" Y="22.370017578125" />
                  <Point X="4.045495117188" Y="22.302578125" />
                  <Point X="4.001273681641" Y="22.239748046875" />
                  <Point X="3.48352734375" Y="22.53866796875" />
                  <Point X="2.848454589844" Y="22.905328125" />
                  <Point X="2.841190673828" Y="22.909115234375" />
                  <Point X="2.815021240234" Y="22.92048828125" />
                  <Point X="2.783116943359" Y="22.930673828125" />
                  <Point X="2.771108642578" Y="22.933662109375" />
                  <Point X="2.668910644531" Y="22.952119140625" />
                  <Point X="2.555018310547" Y="22.9726875" />
                  <Point X="2.539359375" Y="22.97419140625" />
                  <Point X="2.508009033203" Y="22.974595703125" />
                  <Point X="2.492317626953" Y="22.97349609375" />
                  <Point X="2.460144775391" Y="22.9685390625" />
                  <Point X="2.444846191406" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400591064453" Y="22.948890625" />
                  <Point X="2.315689453125" Y="22.904208984375" />
                  <Point X="2.221072998047" Y="22.854412109375" />
                  <Point X="2.208970947266" Y="22.846830078125" />
                  <Point X="2.186039794922" Y="22.8299375" />
                  <Point X="2.175210693359" Y="22.820626953125" />
                  <Point X="2.154251220703" Y="22.79966796875" />
                  <Point X="2.144940185547" Y="22.78883984375" />
                  <Point X="2.128046386719" Y="22.765908203125" />
                  <Point X="2.120463623047" Y="22.7538046875" />
                  <Point X="2.075780517578" Y="22.66890234375" />
                  <Point X="2.02598449707" Y="22.574287109375" />
                  <Point X="2.01983605957" Y="22.55980859375" />
                  <Point X="2.010012329102" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.002187744141" Y="22.419859375" />
                  <Point X="2.02064440918" Y="22.31766015625" />
                  <Point X="2.041213256836" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.478162841797" Y="21.418677734375" />
                  <Point X="2.735893554688" Y="20.972275390625" />
                  <Point X="2.723753173828" Y="20.963916015625" />
                  <Point X="2.318534179688" Y="21.4920078125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828658813477" Y="22.12984765625" />
                  <Point X="1.808834228516" Y="22.15037109375" />
                  <Point X="1.783250854492" Y="22.17199609375" />
                  <Point X="1.773297729492" Y="22.179353515625" />
                  <Point X="1.672502807617" Y="22.244154296875" />
                  <Point X="1.560174438477" Y="22.31637109375" />
                  <Point X="1.546284545898" Y="22.32375390625" />
                  <Point X="1.517470703125" Y="22.336126953125" />
                  <Point X="1.502546875" Y="22.3411171875" />
                  <Point X="1.470926025391" Y="22.34884765625" />
                  <Point X="1.455384887695" Y="22.3513046875" />
                  <Point X="1.424120361328" Y="22.35362109375" />
                  <Point X="1.408396972656" Y="22.35348046875" />
                  <Point X="1.298160522461" Y="22.343337890625" />
                  <Point X="1.175310058594" Y="22.332033203125" />
                  <Point X="1.161227539062" Y="22.32966015625" />
                  <Point X="1.133575439453" Y="22.322828125" />
                  <Point X="1.120005859375" Y="22.318369140625" />
                  <Point X="1.092620605469" Y="22.307025390625" />
                  <Point X="1.07987512207" Y="22.300583984375" />
                  <Point X="1.055493286133" Y="22.28586328125" />
                  <Point X="1.043856933594" Y="22.277583984375" />
                  <Point X="0.958735595703" Y="22.206806640625" />
                  <Point X="0.863873291016" Y="22.127931640625" />
                  <Point X="0.852653137207" Y="22.11691015625" />
                  <Point X="0.83218347168" Y="22.093162109375" />
                  <Point X="0.822933776855" Y="22.080435546875" />
                  <Point X="0.806041625977" Y="22.052609375" />
                  <Point X="0.799019287109" Y="22.03853125" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.757340820312" Y="21.8772734375" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091796875" Y="20.847658203125" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407287598" Y="21.58679296875" />
                  <Point X="0.542973693848" Y="21.698359375" />
                  <Point X="0.456679534912" Y="21.822693359375" />
                  <Point X="0.446669189453" Y="21.834830078125" />
                  <Point X="0.424782836914" Y="21.857287109375" />
                  <Point X="0.412906524658" Y="21.867607421875" />
                  <Point X="0.386651550293" Y="21.886849609375" />
                  <Point X="0.37323550415" Y="21.89506640625" />
                  <Point X="0.345236297607" Y="21.909171875" />
                  <Point X="0.330653106689" Y="21.915060546875" />
                  <Point X="0.210829177856" Y="21.952248046875" />
                  <Point X="0.077294021606" Y="21.993693359375" />
                  <Point X="0.063380245209" Y="21.996888671875" />
                  <Point X="0.03522794342" Y="22.001158203125" />
                  <Point X="0.020989574432" Y="22.002232421875" />
                  <Point X="-0.008651926041" Y="22.002234375" />
                  <Point X="-0.022895944595" Y="22.001162109375" />
                  <Point X="-0.051062065125" Y="21.996892578125" />
                  <Point X="-0.064984016418" Y="21.9936953125" />
                  <Point X="-0.184807815552" Y="21.956505859375" />
                  <Point X="-0.318343261719" Y="21.9150625" />
                  <Point X="-0.332927490234" Y="21.909171875" />
                  <Point X="-0.360928619385" Y="21.895064453125" />
                  <Point X="-0.374345275879" Y="21.88684765625" />
                  <Point X="-0.40060067749" Y="21.867603515625" />
                  <Point X="-0.412477874756" Y="21.85728125" />
                  <Point X="-0.434361572266" Y="21.83482421875" />
                  <Point X="-0.444368499756" Y="21.822689453125" />
                  <Point X="-0.521802062988" Y="21.71112109375" />
                  <Point X="-0.608096069336" Y="21.5867890625" />
                  <Point X="-0.612470092773" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.853882995605" Y="20.724115234375" />
                  <Point X="-0.985424987793" Y="20.2331953125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.635578144832" Y="23.932922642617" />
                  <Point X="4.702008047487" Y="24.245450762818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.541099621117" Y="23.945360897998" />
                  <Point X="4.610119166305" Y="24.270072328434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.446621097401" Y="23.957799153378" />
                  <Point X="4.518230285124" Y="24.29469389405" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.986169301606" Y="22.248468533665" />
                  <Point X="4.022545643509" Y="22.419605767056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.352142573686" Y="23.970237408758" />
                  <Point X="4.426341403942" Y="24.319315459666" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.719084675949" Y="25.696564271499" />
                  <Point X="4.758039463665" Y="25.879832138696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.899662933151" Y="22.298412830722" />
                  <Point X="3.93904262371" Y="22.483679708625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.257664049971" Y="23.982675664138" />
                  <Point X="4.334452523601" Y="24.343937029238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.616096627081" Y="25.668968358631" />
                  <Point X="4.707596030852" Y="26.099439208608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.813156564697" Y="22.34835712778" />
                  <Point X="3.85553960391" Y="22.547753650195" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.163185526255" Y="23.995113919518" />
                  <Point X="4.242563644226" Y="24.368558603357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.513108578214" Y="25.641372445764" />
                  <Point X="4.610358918619" Y="26.098899325391" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.726650196242" Y="22.398301424838" />
                  <Point X="3.772036584111" Y="22.611827591765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.068707001766" Y="24.007552171256" />
                  <Point X="4.150674764852" Y="24.393180177477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.410120529346" Y="25.613776532896" />
                  <Point X="4.510440421297" Y="26.085744517146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.640143827788" Y="22.448245721895" />
                  <Point X="3.688533564395" Y="22.675901533727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.974228477149" Y="24.019990422394" />
                  <Point X="4.058785885477" Y="24.417801751596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.307132480479" Y="25.586180620028" />
                  <Point X="4.410521923975" Y="26.072589708901" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.553637459333" Y="22.498190018953" />
                  <Point X="3.605030593849" Y="22.739975707019" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879749952531" Y="24.032428673532" />
                  <Point X="3.966897006103" Y="24.442423325716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.204144483806" Y="25.558584952716" />
                  <Point X="4.310603426653" Y="26.059434900655" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.46713110084" Y="22.548134362876" />
                  <Point X="3.521527623304" Y="22.804049880311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.785271427914" Y="24.04486692467" />
                  <Point X="3.875008126728" Y="24.467044899835" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.101156487279" Y="25.530989286093" />
                  <Point X="4.210684960009" Y="26.04628023674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.380624784943" Y="22.598078907196" />
                  <Point X="3.438024652759" Y="22.868124053603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.690792903297" Y="24.057305175808" />
                  <Point X="3.783119247354" Y="24.491666473955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.998168490753" Y="25.503393619469" />
                  <Point X="4.110766545284" Y="26.033125817085" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.294118469045" Y="22.648023451515" />
                  <Point X="3.354521682213" Y="22.932198226895" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.59631437868" Y="24.069743426946" />
                  <Point X="3.691238556418" Y="24.516326571647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.895180494226" Y="25.475797952846" />
                  <Point X="4.010848130559" Y="26.019971397431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.207612153148" Y="22.697967995835" />
                  <Point X="3.271018711668" Y="22.996272400187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.501835854063" Y="24.082181678084" />
                  <Point X="3.602985474969" Y="24.558053230162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.792192497699" Y="25.448202286223" />
                  <Point X="3.910929715835" Y="26.006816977777" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.12110583725" Y="22.747912540154" />
                  <Point X="3.187515741122" Y="23.060346573479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.406766852171" Y="24.091841952055" />
                  <Point X="3.517223332247" Y="24.611498834009" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689181832995" Y="25.420499974207" />
                  <Point X="3.81101130111" Y="25.993662558122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.12588627831" Y="27.475032856578" />
                  <Point X="4.130073017895" Y="27.494729917691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.661913476566" Y="21.044507096788" />
                  <Point X="2.670598925021" Y="21.085368919102" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034599521353" Y="22.797857084474" />
                  <Point X="3.104012770577" Y="23.124420746771" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.306465459932" Y="24.076885764856" />
                  <Point X="3.438308524695" Y="24.697158617065" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.580565532158" Y="25.366425217662" />
                  <Point X="3.711092886385" Y="25.980508138468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009836121368" Y="27.385984556771" />
                  <Point X="4.058197137154" Y="27.613505247761" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.585858927456" Y="21.143623337834" />
                  <Point X="2.599611274111" Y="21.208323041983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.948093205455" Y="22.847801628793" />
                  <Point X="3.021621371643" Y="23.193724453336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.204638830183" Y="24.054753899344" />
                  <Point X="3.369618869772" Y="24.830923961055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.463435952798" Y="25.272298634643" />
                  <Point X="3.611174471661" Y="25.967353718814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.893785964426" Y="27.296936256963" />
                  <Point X="3.983836565724" Y="27.720591027207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.509804378347" Y="21.24273957888" />
                  <Point X="2.528623623202" Y="21.331277164865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.861586889558" Y="22.897746173113" />
                  <Point X="2.947408484649" Y="23.301505033421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.099099149526" Y="24.015153502729" />
                  <Point X="3.511256056936" Y="25.95419929916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.777735807484" Y="27.207887957156" />
                  <Point X="3.893828957173" Y="27.754063284685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433749829237" Y="21.341855819927" />
                  <Point X="2.457635962126" Y="21.454231239918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.772048962753" Y="22.933428109478" />
                  <Point X="2.876416009431" Y="23.424436459716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.975327150837" Y="23.889776793737" />
                  <Point X="3.414494580048" Y="25.955897104305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.661685626586" Y="27.118839544641" />
                  <Point X="3.783120517531" Y="27.690145788921" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.357695280127" Y="21.440972060973" />
                  <Point X="2.386648276059" Y="21.577185197393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.678530232112" Y="22.950381836255" />
                  <Point X="3.320534686278" Y="25.970775321744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.545635411452" Y="27.029790971064" />
                  <Point X="3.672412077888" Y="27.626228293157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281640726161" Y="21.540088279171" />
                  <Point X="2.315660589991" Y="21.700139154867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.584998262302" Y="22.967273277636" />
                  <Point X="3.23146080256" Y="26.008640409185" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.429585196319" Y="26.940742397487" />
                  <Point X="3.561703638246" Y="27.562310797393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205586167039" Y="21.639204473114" />
                  <Point X="2.244672903924" Y="21.823093112342" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.48909300082" Y="22.972999259564" />
                  <Point X="3.146242795367" Y="26.064645969425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.313534981186" Y="26.851693823911" />
                  <Point X="3.450995198604" Y="27.498393301629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129531607917" Y="21.738320667058" />
                  <Point X="2.173685217856" Y="21.946047069817" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.385114990911" Y="22.940745946171" />
                  <Point X="3.070123366478" Y="26.163456975109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.197484766053" Y="26.762645250334" />
                  <Point X="3.340286744321" Y="27.434475736988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053477048796" Y="21.837436861001" />
                  <Point X="2.102697531788" Y="22.069001027291" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.275759542422" Y="22.883193773328" />
                  <Point X="3.008885834029" Y="26.332281798872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.070941843127" Y="26.624232367747" />
                  <Point X="3.22957828499" Y="27.370558148597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.977422489674" Y="21.936553054945" />
                  <Point X="2.038001171222" Y="22.221553344147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.162673369327" Y="22.808089921168" />
                  <Point X="3.118869825659" Y="27.306640560205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.901367930552" Y="22.035669248888" />
                  <Point X="3.008161366328" Y="27.242722971814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.825056500007" Y="22.133576957797" />
                  <Point X="2.897452906997" Y="27.178805383422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.741948224856" Y="22.199508026928" />
                  <Point X="2.786744447666" Y="27.114887795031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.656502268377" Y="22.254441170094" />
                  <Point X="2.676035988336" Y="27.050970206639" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.571056449907" Y="22.309374962541" />
                  <Point X="2.568350526494" Y="27.001274703257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.481761227514" Y="22.346198733389" />
                  <Point X="2.466215214599" Y="26.977690602427" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.385743624778" Y="22.35139619127" />
                  <Point X="2.369684941197" Y="26.980476134454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.754979149151" Y="28.793142866201" />
                  <Point X="2.783309484778" Y="28.926426616205" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.286683948296" Y="22.342281817409" />
                  <Point X="2.276989179291" Y="27.001301624721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.601265702803" Y="28.526902721031" />
                  <Point X="2.698940367093" Y="28.986425887584" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.187624035995" Y="22.333166334104" />
                  <Point X="2.190604490869" Y="27.051818381323" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.44755230091" Y="28.260662785007" />
                  <Point X="2.614210083546" Y="29.044726007174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.800556529709" Y="20.969081652379" />
                  <Point X="0.810776231792" Y="21.017161570511" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.084021505904" Y="22.302679514368" />
                  <Point X="2.108981216907" Y="27.124735831757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.293839125133" Y="27.994423912775" />
                  <Point X="2.52778965617" Y="29.095074625217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.746397261505" Y="21.171207091234" />
                  <Point X="0.773629013414" Y="21.299322411196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.968192058922" Y="22.214669573287" />
                  <Point X="2.037160425316" Y="27.243770335903" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.140125949357" Y="27.728185040542" />
                  <Point X="2.441369228794" Y="29.145423243259" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.692237993302" Y="21.373332530088" />
                  <Point X="0.736481795036" Y="21.581483251881" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.849516904328" Y="22.113271630485" />
                  <Point X="2.354948660845" Y="29.195771199959" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.634893705656" Y="21.560473630572" />
                  <Point X="2.268528069877" Y="29.246119048362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.561428157533" Y="21.671770163613" />
                  <Point X="2.180589350145" Y="29.289324662474" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.487076345953" Y="21.778897154909" />
                  <Point X="2.092248619124" Y="29.330638962169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409362096921" Y="21.87020512173" />
                  <Point X="2.003907853191" Y="29.37195309762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.322323533815" Y="21.917645639805" />
                  <Point X="1.91556704376" Y="29.413267028424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.231211576535" Y="21.945922345004" />
                  <Point X="1.82658377193" Y="29.451558411282" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.140099885054" Y="21.974200300689" />
                  <Point X="1.736413285657" Y="29.484264389328" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.048286647541" Y="21.999177741785" />
                  <Point X="1.646242799384" Y="29.516970367373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.049263484963" Y="21.997165213975" />
                  <Point X="1.556072106952" Y="29.549675375514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.152924916272" Y="21.9664012858" />
                  <Point X="1.465901407186" Y="29.582380349156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.256906834157" Y="21.934129586824" />
                  <Point X="1.37382099757" Y="29.606100844331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.36254288006" Y="21.894075827377" />
                  <Point X="1.281439534922" Y="29.628404996353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.488290223219" Y="21.759405853312" />
                  <Point X="1.189058072275" Y="29.650709148375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.632024950168" Y="21.540111891879" />
                  <Point X="1.096676589638" Y="29.673013206353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.006088564366" Y="20.237205712414" />
                  <Point X="1.004295053419" Y="29.695317012249" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.098841590885" Y="20.257761793861" />
                  <Point X="0.9119135172" Y="29.717620818146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.133316857202" Y="20.552493180661" />
                  <Point X="0.819295501147" Y="29.73881207389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.180267617462" Y="20.788531983031" />
                  <Point X="0.724288067169" Y="29.748762002123" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.248072257965" Y="20.926460992509" />
                  <Point X="0.62928063319" Y="29.758711930356" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.329501764935" Y="21.000290044971" />
                  <Point X="0.534273199212" Y="29.768661858589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.411364351883" Y="21.072081616324" />
                  <Point X="0.439265742983" Y="29.77861168214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.493226821524" Y="21.143873739566" />
                  <Point X="0.226006444853" Y="29.23223032978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.580332882307" Y="21.190996706039" />
                  <Point X="0.102090367659" Y="29.106175784716" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.674206552179" Y="21.206280575021" />
                  <Point X="0.00049103783" Y="29.08511328125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.769994558306" Y="21.212558200023" />
                  <Point X="-0.091953894378" Y="29.107118832464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.86578239575" Y="21.21883661861" />
                  <Point X="-0.175649540911" Y="29.170286536503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.961835147836" Y="21.223868711798" />
                  <Point X="-0.241095884247" Y="29.319310461845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.06407553483" Y="21.199790271492" />
                  <Point X="-0.295255170501" Y="29.521435815774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.175948708806" Y="21.130393131516" />
                  <Point X="-0.349414444814" Y="29.723561225887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.289148278125" Y="21.054755792068" />
                  <Point X="-0.435751039565" Y="29.77430424542" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.405558772226" Y="20.964012239208" />
                  <Point X="-0.535351123103" Y="29.762647456252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.526501117267" Y="20.851948003969" />
                  <Point X="-0.634951306405" Y="29.750990197729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.612327839661" Y="20.90508978435" />
                  <Point X="-0.734551558342" Y="29.739332616306" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.698154562054" Y="20.95823156473" />
                  <Point X="-2.462707239898" Y="22.065924125744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.361435006104" Y="22.542372526102" />
                  <Point X="-0.834151810279" Y="29.727675034882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.78297947578" Y="21.016086484332" />
                  <Point X="-2.616420547381" Y="21.79968463388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.44283935136" Y="22.616319955122" />
                  <Point X="-0.933847578363" Y="29.715568085318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.866442152911" Y="21.080350223235" />
                  <Point X="-2.770133854842" Y="21.533445142125" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.532894108474" Y="22.649570396052" />
                  <Point X="-1.037124416143" Y="29.686613527437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.949904807879" Y="21.144614066407" />
                  <Point X="-2.923847162278" Y="21.26720565048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.631612821462" Y="22.642060129309" />
                  <Point X="-1.140401253923" Y="29.657658969555" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.739702780346" Y="22.590461616962" />
                  <Point X="-1.243678091703" Y="29.628704411674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850411236053" Y="22.52654404562" />
                  <Point X="-1.346954969834" Y="29.599749663956" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.96111969176" Y="22.462626474279" />
                  <Point X="-1.533516641675" Y="29.178970768089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468822929173" Y="29.48333075582" />
                  <Point X="-1.450231946921" Y="29.570794450689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.071828147466" Y="22.398708902938" />
                  <Point X="-1.65080085224" Y="29.08411670245" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.182536603173" Y="22.334791331597" />
                  <Point X="-1.756982872759" Y="29.041494334384" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.29324505888" Y="22.270873760256" />
                  <Point X="-3.047587687936" Y="23.426600824213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.976762490998" Y="23.759807178236" />
                  <Point X="-1.856523663629" Y="29.030116495283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.403953525712" Y="22.206956136574" />
                  <Point X="-3.163656953911" Y="23.337462623473" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.055448861299" Y="23.846541674066" />
                  <Point X="-1.947791968552" Y="29.057657642654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.514662014106" Y="22.143038411451" />
                  <Point X="-3.279707080507" Y="23.248414466431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.143482259649" Y="23.8893018603" />
                  <Point X="-2.03523803055" Y="29.103181029172" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.6253705025" Y="22.079120686328" />
                  <Point X="-3.395757207103" Y="23.159366309389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.238311015743" Y="23.900092401887" />
                  <Point X="-2.117912921871" Y="29.171151008918" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.736078990894" Y="22.015202961204" />
                  <Point X="-3.511807333699" Y="23.070318152347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.338119497388" Y="23.887455176705" />
                  <Point X="-2.194635637126" Y="29.267123775399" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.829913396765" Y="22.03067155279" />
                  <Point X="-3.627857460295" Y="22.981269995304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438037889175" Y="23.874300864966" />
                  <Point X="-2.304763330511" Y="29.205938475961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.905834369152" Y="22.130416222908" />
                  <Point X="-3.743907644886" Y="22.892221565421" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537956280962" Y="23.861146553226" />
                  <Point X="-3.333367719048" Y="24.82366006166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296178191896" Y="24.998623030857" />
                  <Point X="-2.414890919536" Y="29.144753667502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.981755341539" Y="22.230160893025" />
                  <Point X="-3.859957855852" Y="22.803173011449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.637874672748" Y="23.847992241487" />
                  <Point X="-3.45434613024" Y="24.711426148522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.360391345138" Y="25.153448659442" />
                  <Point X="-2.772390054235" Y="27.919777237034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.748499353704" Y="28.03217414609" />
                  <Point X="-2.52501841991" Y="29.08356927611" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.054623775173" Y="22.344266628871" />
                  <Point X="-3.976008066818" Y="22.714124457477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737793064535" Y="23.834837929747" />
                  <Point X="-3.559395617505" Y="24.6741319305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.443435175915" Y="25.219682915512" />
                  <Point X="-2.900242486815" Y="27.775203595898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.799403953698" Y="28.249611594997" />
                  <Point X="-2.635145920285" Y="29.022384884718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.12622544001" Y="22.464332043341" />
                  <Point X="-4.092058277784" Y="22.625075903504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.837711456322" Y="23.821683618008" />
                  <Point X="-3.662383596744" Y="24.646536345209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.53279959915" Y="25.256181121995" />
                  <Point X="-3.007549560291" Y="27.727288269818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.870391693347" Y="28.372565300392" />
                  <Point X="-2.750468003939" Y="28.936761900422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937629848108" Y="23.808529306268" />
                  <Point X="-3.765371575983" Y="24.618940759917" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.624688482737" Y="25.280802676297" />
                  <Point X="-3.104784772158" Y="27.726757327114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.941379432996" Y="28.495519005787" />
                  <Point X="-2.866499487555" Y="28.847801451705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.037548239895" Y="23.795374994529" />
                  <Point X="-3.868359555222" Y="24.591345174626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.716577366324" Y="25.305424230599" />
                  <Point X="-3.503590971203" Y="26.307446437991" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.438877528035" Y="26.611899251207" />
                  <Point X="-3.199030063349" Y="27.740292855253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.012367203918" Y="28.618472564054" />
                  <Point X="-2.98253107603" Y="28.758840509664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.137466646318" Y="23.782220613932" />
                  <Point X="-3.971347534461" Y="24.563749589334" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.80846624991" Y="25.330045784901" />
                  <Point X="-3.616546625246" Y="26.232956629696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.509683851642" Y="26.735706451974" />
                  <Point X="-3.287376616813" Y="27.781579762509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.237385079627" Y="23.769066106845" />
                  <Point X="-4.0743355137" Y="24.536154004043" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.900355133497" Y="25.354667339203" />
                  <Point X="-3.719351716" Y="26.206221467079" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.591781812221" Y="26.806390677461" />
                  <Point X="-3.373882948093" Y="27.831524234459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.337303512936" Y="23.755911599759" />
                  <Point X="-4.177323492939" Y="24.508558418751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.992244017084" Y="25.379288893505" />
                  <Point X="-3.815555935738" Y="26.210540960993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.675284770375" Y="26.870464909048" />
                  <Point X="-3.460389279373" Y="27.881468706408" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.437221946245" Y="23.742757092672" />
                  <Point X="-4.280311495742" Y="24.480962722598" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.08413290067" Y="25.403910447808" />
                  <Point X="-3.910034494316" Y="26.222979052357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.758787728529" Y="26.934539140636" />
                  <Point X="-3.546895610653" Y="27.931413178357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.537140379554" Y="23.729602585585" />
                  <Point X="-4.383299509507" Y="24.453366974873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.176021784257" Y="25.42853200211" />
                  <Point X="-4.004513052893" Y="26.235417143722" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.842290686684" Y="26.998613372223" />
                  <Point X="-3.63340194432" Y="27.98135763908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.637058812863" Y="23.716448078499" />
                  <Point X="-4.486287523273" Y="24.425771227148" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.267910667844" Y="25.453153556412" />
                  <Point X="-4.098991611471" Y="26.247855235087" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.925793644838" Y="27.062687603811" />
                  <Point X="-3.719908278536" Y="28.031302097216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.701271183791" Y="23.871277387579" />
                  <Point X="-4.589275537038" Y="24.398175479423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.359799566037" Y="25.477775041997" />
                  <Point X="-4.193470170049" Y="26.260293326451" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009296628908" Y="27.126761713474" />
                  <Point X="-3.850038476629" Y="27.876012411865" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.750725070461" Y="24.095539906069" />
                  <Point X="-4.692263550804" Y="24.370579731697" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.451688468941" Y="25.502396505419" />
                  <Point X="-4.287948721336" Y="26.272731452117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.092799718874" Y="27.190835324936" />
                  <Point X="-3.983670691476" Y="27.70424703305" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.543577371845" Y="25.527017968841" />
                  <Point X="-4.38242717952" Y="26.285170015802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.17630280884" Y="27.254908936397" />
                  <Point X="-4.133753106086" Y="27.455089549124" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.635466274749" Y="25.551639432263" />
                  <Point X="-4.476905637703" Y="26.297608579486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.727355177653" Y="25.576260895685" />
                  <Point X="-4.571384095886" Y="26.310047143171" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.66099029541" Y="20.755845703125" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.386885223389" Y="21.5900234375" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.27433605957" Y="21.733599609375" />
                  <Point X="0.15451222229" Y="21.770787109375" />
                  <Point X="0.020977081299" Y="21.812232421875" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.12848828125" Y="21.775044921875" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.365712585449" Y="21.6027890625" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.670357116699" Y="20.674939453125" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.943741149902" Y="20.031556640625" />
                  <Point X="-1.100229614258" Y="20.061931640625" />
                  <Point X="-1.238634155273" Y="20.097541015625" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.331955810547" Y="20.275736328125" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.338309204102" Y="20.60915234375" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282592773" Y="20.79737109375" />
                  <Point X="-1.496601196289" Y="20.8941171875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240600586" Y="21.014236328125" />
                  <Point X="-1.795657958984" Y="21.02383203125" />
                  <Point X="-1.958829589844" Y="21.03452734375" />
                  <Point X="-1.989878417969" Y="21.026208984375" />
                  <Point X="-2.111881103516" Y="20.944689453125" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.378267333984" Y="20.688228515625" />
                  <Point X="-2.457094482422" Y="20.5855" />
                  <Point X="-2.626430419922" Y="20.69034765625" />
                  <Point X="-2.855837890625" Y="20.832390625" />
                  <Point X="-3.047461669922" Y="20.979935546875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.858107666016" Y="21.7610703125" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.40241015625" />
                  <Point X="-2.513983398438" Y="22.431240234375" />
                  <Point X="-2.531326904297" Y="22.44858203125" />
                  <Point X="-2.56015625" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.271273193359" Y="22.06416796875" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.980462402344" Y="21.914755859375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.299085449219" Y="22.38323828125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.779801757813" Y="23.104169921875" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822265625" Y="23.603984375" />
                  <Point X="-3.138117675781" Y="23.633732421875" />
                  <Point X="-3.140326416016" Y="23.665404296875" />
                  <Point X="-3.161159423828" Y="23.689361328125" />
                  <Point X="-3.187642089844" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.077446777344" Y="23.598482421875" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.856508300781" Y="23.711296875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.963740722656" Y="24.242947265625" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.259197753906" Y="24.68332421875" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.528770019531" Y="24.88768359375" />
                  <Point X="-3.502324462891" Y="24.909353515625" />
                  <Point X="-3.490523925781" Y="24.9381875" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.490022949219" Y="24.99763671875" />
                  <Point X="-3.502323974609" Y="25.028083984375" />
                  <Point X="-3.527268066406" Y="25.048712890625" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.33949609375" Y="25.275630859375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.963329101563" Y="25.687689453125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.844478027344" Y="26.26642578125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.256286621094" Y="26.460203125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731703125" Y="26.3958671875" />
                  <Point X="-3.70265234375" Y="26.405025390625" />
                  <Point X="-3.670277099609" Y="26.415234375" />
                  <Point X="-3.651534179688" Y="26.426056640625" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.627463378906" Y="26.471927734375" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.61631640625" Y="26.54551171875" />
                  <Point X="-3.630381591797" Y="26.57253125" />
                  <Point X="-3.646056152344" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.108543945313" Y="26.96342578125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.337526367188" Y="27.482884765625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-3.966197265625" Y="28.03612890625" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.468097900391" Y="28.105310546875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138515380859" Y="27.92043359375" />
                  <Point X="-3.098055664062" Y="27.91689453125" />
                  <Point X="-3.052966064453" Y="27.91294921875" />
                  <Point X="-3.031507324219" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.984534179688" Y="27.956123046875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.941614013672" Y="28.06830078125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.150844970703" Y="28.47832421875" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.062044677734" Y="28.937294921875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.447620361328" Y="29.34392578125" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-2.062403076172" Y="29.410828125" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951247070312" Y="29.273662109375" />
                  <Point X="-1.906215454102" Y="29.25021875" />
                  <Point X="-1.856031005859" Y="29.22409375" />
                  <Point X="-1.835124633789" Y="29.218490234375" />
                  <Point X="-1.813809570312" Y="29.22225" />
                  <Point X="-1.76690612793" Y="29.2416796875" />
                  <Point X="-1.714635620117" Y="29.263330078125" />
                  <Point X="-1.696905517578" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.670817138672" Y="29.34290625" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.674516357422" Y="29.590080078125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.36833215332" Y="29.79108203125" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.598032348633" Y="29.946607421875" />
                  <Point X="-0.224200027466" Y="29.990359375" />
                  <Point X="-0.13353352356" Y="29.651986328125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282110214" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.036594028473" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.156303543091" Y="29.691017578125" />
                  <Point X="0.236648406982" Y="29.990869140625" />
                  <Point X="0.510738616943" Y="29.9621640625" />
                  <Point X="0.860210021973" Y="29.925564453125" />
                  <Point X="1.166368041992" Y="29.8516484375" />
                  <Point X="1.508455810547" Y="29.769056640625" />
                  <Point X="1.707929443359" Y="29.69670703125" />
                  <Point X="1.931044921875" Y="29.61578125" />
                  <Point X="2.12373046875" Y="29.52566796875" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.524854003906" Y="29.3166796875" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.908090576172" Y="29.0708359375" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.637352539062" Y="28.20940625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.214698242188" Y="27.45354296875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.20600390625" Y="27.359490234375" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.238998535156" Y="27.27087109375" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.304879882812" Y="27.203888671875" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360337158203" Y="27.172978515625" />
                  <Point X="2.393171142578" Y="27.16901953125" />
                  <Point X="2.429761962891" Y="27.164607421875" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.486635742188" Y="27.176099609375" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.327607910156" Y="27.646546875" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.079408935547" Y="27.913076171875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.300467773438" Y="27.580138671875" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.825126953125" Y="27.00476171875" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.279371337891" Y="26.58383203125" />
                  <Point X="3.252043701172" Y="26.548181640625" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213119628906" Y="26.4915" />
                  <Point X="3.202939941406" Y="26.455099609375" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.199135986328" Y="26.350466796875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646240234" Y="26.287955078125" />
                  <Point X="3.238374511719" Y="26.25341015625" />
                  <Point X="3.263703857422" Y="26.21491015625" />
                  <Point X="3.280947753906" Y="26.1988203125" />
                  <Point X="3.313884277344" Y="26.180279296875" />
                  <Point X="3.35058984375" Y="26.1596171875" />
                  <Point X="3.368565917969" Y="26.153619140625" />
                  <Point X="3.413098388672" Y="26.147734375" />
                  <Point X="3.4627265625" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.222996582031" Y="26.239541015625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.886282226562" Y="26.16870703125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.970031738281" Y="25.753283203125" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.356020507813" Y="25.402576171875" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087890625" Y="25.232818359375" />
                  <Point X="3.685336181641" Y="25.207529296875" />
                  <Point X="3.636577880859" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.596014160156" Y="25.1334765625" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.548234619141" Y="25.02904296875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538482910156" Y="24.959314453125" />
                  <Point X="3.547233398438" Y="24.913625" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759277344" Y="24.841240234375" />
                  <Point X="3.593010253906" Y="24.8077890625" />
                  <Point X="3.622265136719" Y="24.77051171875" />
                  <Point X="3.636576660156" Y="24.758091796875" />
                  <Point X="3.680328369141" Y="24.732802734375" />
                  <Point X="3.729086669922" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.426377441406" Y="24.516009765625" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.977972167969" Y="24.22953515625" />
                  <Point X="4.948432617188" Y="24.03360546875" />
                  <Point X="4.908916015625" Y="23.8604375" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="4.125038574219" Y="23.80849609375" />
                  <Point X="3.411982177734" Y="23.90237109375" />
                  <Point X="3.394836425781" Y="23.901658203125" />
                  <Point X="3.308967285156" Y="23.882994140625" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.185445556641" Y="23.845302734375" />
                  <Point X="3.13354296875" Y="23.78287890625" />
                  <Point X="3.075701416016" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.056918945312" Y="23.60508984375" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.103881835938" Y="23.4094609375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.804339599609" Y="22.82653125" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.287403320313" Y="22.33260546875" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.122410644531" Y="22.081744140625" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="3.38852734375" Y="22.374123046875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340576172" Y="22.746685546875" />
                  <Point X="2.635142578125" Y="22.765142578125" />
                  <Point X="2.521250244141" Y="22.7857109375" />
                  <Point X="2.489077392578" Y="22.78075390625" />
                  <Point X="2.40417578125" Y="22.736072265625" />
                  <Point X="2.309559326172" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.243916748047" Y="22.5804140625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.207619873047" Y="22.35142578125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.642707763672" Y="21.513677734375" />
                  <Point X="2.986673583984" Y="20.917912109375" />
                  <Point X="2.934107910156" Y="20.880365234375" />
                  <Point X="2.835295898438" Y="20.809787109375" />
                  <Point X="2.743929199219" Y="20.750646484375" />
                  <Point X="2.679775146484" Y="20.70912109375" />
                  <Point X="2.167796875" Y="21.37634375" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549194336" Y="22.019533203125" />
                  <Point X="1.569754394531" Y="22.084333984375" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425804931641" Y="22.16428125" />
                  <Point X="1.315568481445" Y="22.154138671875" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.131490234375" />
                  <Point X="1.080211303711" Y="22.060712890625" />
                  <Point X="0.985349060059" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.943005859375" Y="21.83691796875" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.029728149414" Y="20.809703125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.087827148438" Y="20.0572421875" />
                  <Point X="0.994371337891" Y="20.0367578125" />
                  <Point X="0.909937866211" Y="20.021419921875" />
                  <Point X="0.860200439453" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#164" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.091393266905" Y="4.696208806539" Z="1.2" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="-0.610000441139" Y="5.027547843578" Z="1.2" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="1.2" />
                  <Point X="-1.387985882063" Y="4.870507317709" Z="1.2" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="1.2" />
                  <Point X="-1.730244645039" Y="4.614835297266" Z="1.2" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="1.2" />
                  <Point X="-1.724658965457" Y="4.389222120976" Z="1.2" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="1.2" />
                  <Point X="-1.792195649252" Y="4.319152766711" Z="1.2" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="1.2" />
                  <Point X="-1.889283585606" Y="4.32584918069" Z="1.2" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="1.2" />
                  <Point X="-2.028891352471" Y="4.472545417174" Z="1.2" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="1.2" />
                  <Point X="-2.478059383287" Y="4.418912438217" Z="1.2" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="1.2" />
                  <Point X="-3.098622815166" Y="4.008254975894" Z="1.2" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="1.2" />
                  <Point X="-3.200302147543" Y="3.484605551556" Z="1.2" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="1.2" />
                  <Point X="-2.997579821421" Y="3.09522334871" Z="1.2" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="1.2" />
                  <Point X="-3.026044905214" Y="3.022758662673" Z="1.2" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="1.2" />
                  <Point X="-3.099853116897" Y="2.997984877752" Z="1.2" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="1.2" />
                  <Point X="-3.449253796741" Y="3.179891801203" Z="1.2" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="1.2" />
                  <Point X="-4.011816884616" Y="3.09811334553" Z="1.2" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="1.2" />
                  <Point X="-4.386864304291" Y="2.539371323255" Z="1.2" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="1.2" />
                  <Point X="-4.145137956612" Y="1.955038298069" Z="1.2" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="1.2" />
                  <Point X="-3.68088762962" Y="1.580723374949" Z="1.2" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="1.2" />
                  <Point X="-3.679813107807" Y="1.522342116798" Z="1.2" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="1.2" />
                  <Point X="-3.723845122607" Y="1.483992254453" Z="1.2" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="1.2" />
                  <Point X="-4.255915984194" Y="1.541056382777" Z="1.2" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="1.2" />
                  <Point X="-4.898893606838" Y="1.310785342457" Z="1.2" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="1.2" />
                  <Point X="-5.018946478795" Y="0.726288997638" Z="1.2" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="1.2" />
                  <Point X="-4.358593198422" Y="0.258614002137" Z="1.2" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="1.2" />
                  <Point X="-3.56193333115" Y="0.038916777992" Z="1.2" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="1.2" />
                  <Point X="-3.543931974129" Y="0.01409692781" Z="1.2" />
                  <Point X="-3.539556741714" Y="0" Z="1.2" />
                  <Point X="-3.544432627006" Y="-0.015710023251" Z="1.2" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="1.2" />
                  <Point X="-3.563435263974" Y="-0.039959205142" Z="1.2" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="1.2" />
                  <Point X="-4.27829459035" Y="-0.237098054619" Z="1.2" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="1.2" />
                  <Point X="-5.019392838714" Y="-0.732850733887" Z="1.2" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="1.2" />
                  <Point X="-4.911109031045" Y="-1.269796938582" Z="1.2" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="1.2" />
                  <Point X="-4.077076320865" Y="-1.419810259065" Z="1.2" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="1.2" />
                  <Point X="-3.205201003356" Y="-1.31507831138" Z="1.2" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="1.2" />
                  <Point X="-3.1967370303" Y="-1.338128422235" Z="1.2" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="1.2" />
                  <Point X="-3.816395755522" Y="-1.824882049022" Z="1.2" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="1.2" />
                  <Point X="-4.348184671138" Y="-2.611090718426" Z="1.2" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="1.2" />
                  <Point X="-4.026404252384" Y="-3.084246546519" Z="1.2" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="1.2" />
                  <Point X="-3.252429111138" Y="-2.947852210946" Z="1.2" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="1.2" />
                  <Point X="-2.563695799916" Y="-2.564634669216" Z="1.2" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="1.2" />
                  <Point X="-2.907564802223" Y="-3.182649271355" Z="1.2" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="1.2" />
                  <Point X="-3.084121435472" Y="-4.028401243637" Z="1.2" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="1.2" />
                  <Point X="-2.658908030106" Y="-4.320882989583" Z="1.2" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="1.2" />
                  <Point X="-2.344755472855" Y="-4.310927593729" Z="1.2" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="1.2" />
                  <Point X="-2.090258814738" Y="-4.065604164473" Z="1.2" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="1.2" />
                  <Point X="-1.805084235309" Y="-3.994779221658" Z="1.2" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="1.2" />
                  <Point X="-1.535724597064" Y="-4.112193542852" Z="1.2" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="1.2" />
                  <Point X="-1.393504442203" Y="-4.369320212965" Z="1.2" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="1.2" />
                  <Point X="-1.387683991658" Y="-4.686456795539" Z="1.2" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="1.2" />
                  <Point X="-1.257249159347" Y="-4.919602072349" Z="1.2" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="1.2" />
                  <Point X="-0.959424449165" Y="-4.986247540239" Z="1.2" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="-0.628216778841" Y="-4.306721017647" Z="1.2" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="-0.330792614" Y="-3.394439637576" Z="1.2" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="-0.119823884185" Y="-3.241428280176" Z="1.2" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="1.2" />
                  <Point X="0.133535195176" Y="-3.245683765113" Z="1.2" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="1.2" />
                  <Point X="0.339653245432" Y="-3.407206118192" Z="1.2" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="1.2" />
                  <Point X="0.60653808156" Y="-4.225815012168" Z="1.2" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="1.2" />
                  <Point X="0.912719004389" Y="-4.996495667003" Z="1.2" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="1.2" />
                  <Point X="1.092376294781" Y="-4.960316374327" Z="1.2" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="1.2" />
                  <Point X="1.073144446638" Y="-4.152491759599" Z="1.2" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="1.2" />
                  <Point X="0.985709061783" Y="-3.142420367028" Z="1.2" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="1.2" />
                  <Point X="1.106022072133" Y="-2.946451324639" Z="1.2" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="1.2" />
                  <Point X="1.313994208098" Y="-2.864370629039" Z="1.2" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="1.2" />
                  <Point X="1.536559094141" Y="-2.926443524547" Z="1.2" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="1.2" />
                  <Point X="2.121973073029" Y="-3.622813147153" Z="1.2" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="1.2" />
                  <Point X="2.76494277905" Y="-4.260047951547" Z="1.2" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="1.2" />
                  <Point X="2.95701371907" Y="-4.12904194434" Z="1.2" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="1.2" />
                  <Point X="2.679852972731" Y="-3.430042130731" Z="1.2" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="1.2" />
                  <Point X="2.250668004195" Y="-2.608406200143" Z="1.2" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="1.2" />
                  <Point X="2.282007169055" Y="-2.411591603426" Z="1.2" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="1.2" />
                  <Point X="2.421306774947" Y="-2.276894140912" Z="1.2" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="1.2" />
                  <Point X="2.62010060337" Y="-2.252780005296" Z="1.2" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="1.2" />
                  <Point X="3.357371466376" Y="-2.637896527544" Z="1.2" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="1.2" />
                  <Point X="4.157143320125" Y="-2.915753001448" Z="1.2" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="1.2" />
                  <Point X="4.323780946071" Y="-2.662400081705" Z="1.2" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="1.2" />
                  <Point X="3.828621499006" Y="-2.102519962348" Z="1.2" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="1.2" />
                  <Point X="3.139783763768" Y="-1.532218690455" Z="1.2" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="1.2" />
                  <Point X="3.100552592162" Y="-1.368212154387" Z="1.2" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="1.2" />
                  <Point X="3.165833063079" Y="-1.217806736262" Z="1.2" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="1.2" />
                  <Point X="3.313430652426" Y="-1.13458445196" Z="1.2" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="1.2" />
                  <Point X="4.1123561874" Y="-1.209796063798" Z="1.2" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="1.2" />
                  <Point X="4.951507204722" Y="-1.119406679343" Z="1.2" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="1.2" />
                  <Point X="5.021257665657" Y="-0.74663782012" Z="1.2" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="1.2" />
                  <Point X="4.433162360054" Y="-0.404412003595" Z="1.2" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="1.2" />
                  <Point X="3.69919447505" Y="-0.192627509083" Z="1.2" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="1.2" />
                  <Point X="3.626187779925" Y="-0.130060545702" Z="1.2" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="1.2" />
                  <Point X="3.590185078787" Y="-0.045691076622" Z="1.2" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="1.2" />
                  <Point X="3.591186371637" Y="0.050919454591" Z="1.2" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="1.2" />
                  <Point X="3.629191658476" Y="0.133888192873" Z="1.2" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="1.2" />
                  <Point X="3.704200939302" Y="0.195521335159" Z="1.2" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="1.2" />
                  <Point X="4.362805457252" Y="0.385559929173" Z="1.2" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="1.2" />
                  <Point X="5.013280749392" Y="0.792254473118" Z="1.2" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="1.2" />
                  <Point X="4.928707351377" Y="1.211814454058" Z="1.2" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="1.2" />
                  <Point X="4.21031407935" Y="1.320393893965" Z="1.2" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="1.2" />
                  <Point X="3.413493115099" Y="1.228583067434" Z="1.2" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="1.2" />
                  <Point X="3.332255137461" Y="1.255130586169" Z="1.2" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="1.2" />
                  <Point X="3.273989382221" Y="1.312170255735" Z="1.2" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="1.2" />
                  <Point X="3.241948409321" Y="1.391849946565" Z="1.2" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="1.2" />
                  <Point X="3.244936427292" Y="1.472914039836" Z="1.2" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="1.2" />
                  <Point X="3.285570603047" Y="1.549044150558" Z="1.2" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="1.2" />
                  <Point X="3.849408863311" Y="1.996374229352" Z="1.2" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="1.2" />
                  <Point X="4.337089124742" Y="2.637305593656" Z="1.2" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="1.2" />
                  <Point X="4.113838721774" Y="2.973559088544" Z="1.2" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="1.2" />
                  <Point X="3.296452010782" Y="2.721127504912" Z="1.2" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="1.2" />
                  <Point X="2.467562318056" Y="2.255683024579" Z="1.2" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="1.2" />
                  <Point X="2.393000651089" Y="2.249941372226" Z="1.2" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="1.2" />
                  <Point X="2.326799356609" Y="2.276541690372" Z="1.2" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="1.2" />
                  <Point X="2.274216909863" Y="2.330225503773" Z="1.2" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="1.2" />
                  <Point X="2.249488330533" Y="2.396757792743" Z="1.2" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="1.2" />
                  <Point X="2.256844918078" Y="2.471907257825" Z="1.2" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="1.2" />
                  <Point X="2.674497847948" Y="3.215687071173" Z="1.2" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="1.2" />
                  <Point X="2.930911585237" Y="4.142865146612" Z="1.2" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="1.2" />
                  <Point X="2.543867945859" Y="4.391162862626" Z="1.2" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="1.2" />
                  <Point X="2.1387559174" Y="4.60224022644" Z="1.2" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="1.2" />
                  <Point X="1.718822249809" Y="4.774991410211" Z="1.2" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="1.2" />
                  <Point X="1.171946662854" Y="4.931532169344" Z="1.2" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="1.2" />
                  <Point X="0.509790609812" Y="5.043172206907" Z="1.2" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="1.2" />
                  <Point X="0.101851294648" Y="4.735238699412" Z="1.2" />
                  <Point X="0" Y="4.355124473572" Z="1.2" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>