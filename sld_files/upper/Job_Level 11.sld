<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#137" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="940" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.00471484375" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.7664453125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.642356811523" Y="21.192439453125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.510051269531" Y="21.5791796875" />
                  <Point X="0.378635223389" Y="21.768525390625" />
                  <Point X="0.356752349854" Y="21.790978515625" />
                  <Point X="0.330497161865" Y="21.81022265625" />
                  <Point X="0.302494110107" Y="21.82433203125" />
                  <Point X="0.252493438721" Y="21.839849609375" />
                  <Point X="0.049134975433" Y="21.90296484375" />
                  <Point X="0.020976791382" Y="21.907234375" />
                  <Point X="-0.008664384842" Y="21.907234375" />
                  <Point X="-0.036825908661" Y="21.90296484375" />
                  <Point X="-0.086826431274" Y="21.8874453125" />
                  <Point X="-0.290184906006" Y="21.82433203125" />
                  <Point X="-0.318184143066" Y="21.810224609375" />
                  <Point X="-0.34443963623" Y="21.79098046875" />
                  <Point X="-0.366324188232" Y="21.7685234375" />
                  <Point X="-0.398635955811" Y="21.721966796875" />
                  <Point X="-0.530051818848" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.872516479492" Y="20.2875234375" />
                  <Point X="-0.916584777832" Y="20.12305859375" />
                  <Point X="-1.079326660156" Y="20.154646484375" />
                  <Point X="-1.133471679688" Y="20.168578125" />
                  <Point X="-1.246417602539" Y="20.197638671875" />
                  <Point X="-1.224479248047" Y="20.364275390625" />
                  <Point X="-1.214963012695" Y="20.43655859375" />
                  <Point X="-1.214201171875" Y="20.452068359375" />
                  <Point X="-1.216508666992" Y="20.4837734375" />
                  <Point X="-1.228453735352" Y="20.543826171875" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287937988281" Y="20.817033203125" />
                  <Point X="-1.304010009766" Y="20.84487109375" />
                  <Point X="-1.32364453125" Y="20.868794921875" />
                  <Point X="-1.369678833008" Y="20.909166015625" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189086914" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.643028442383" Y="21.109033203125" />
                  <Point X="-1.704125976562" Y="21.113037109375" />
                  <Point X="-1.952617553711" Y="21.12932421875" />
                  <Point X="-1.983414550781" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.04265637207" Y="21.10519921875" />
                  <Point X="-2.093566162109" Y="21.07118359375" />
                  <Point X="-2.300622802734" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480147705078" Y="20.71151171875" />
                  <Point X="-2.801724853516" Y="20.910623046875" />
                  <Point X="-2.876663330078" Y="20.96832421875" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.57067578125" Y="22.068916015625" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858886719" Y="22.35234765625" />
                  <Point X="-2.406588134766" Y="22.383875" />
                  <Point X="-2.405575927734" Y="22.414810546875" />
                  <Point X="-2.414560791016" Y="22.444427734375" />
                  <Point X="-2.428778808594" Y="22.4732578125" />
                  <Point X="-2.446807617188" Y="22.498416015625" />
                  <Point X="-2.46415625" Y="22.515763671875" />
                  <Point X="-2.489316650391" Y="22.533791015625" />
                  <Point X="-2.518145263672" Y="22.548005859375" />
                  <Point X="-2.54776171875" Y="22.55698828125" />
                  <Point X="-2.578693847656" Y="22.555974609375" />
                  <Point X="-2.610219238281" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.674119384766" Y="21.941279296875" />
                  <Point X="-3.818023193359" Y="21.858197265625" />
                  <Point X="-4.082862304687" Y="22.206142578125" />
                  <Point X="-4.136594238281" Y="22.2962421875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.363007324219" Y="23.3042421875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.083065429688" Y="23.5241171875" />
                  <Point X="-3.064125" Y="23.554150390625" />
                  <Point X="-3.055891113281" Y="23.57508984375" />
                  <Point X="-3.0523359375" Y="23.586037109375" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.042037597656" Y="23.641396484375" />
                  <Point X="-3.042736816406" Y="23.664263671875" />
                  <Point X="-3.048883300781" Y="23.686302734375" />
                  <Point X="-3.060888916016" Y="23.715287109375" />
                  <Point X="-3.073389892578" Y="23.73689453125" />
                  <Point X="-3.091088623047" Y="23.754501953125" />
                  <Point X="-3.108817626953" Y="23.7680078125" />
                  <Point X="-3.118202392578" Y="23.774310546875" />
                  <Point X="-3.139458740234" Y="23.7868203125" />
                  <Point X="-3.168722167969" Y="23.798044921875" />
                  <Point X="-3.200608398438" Y="23.804525390625" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.5384375" Y="23.633611328125" />
                  <Point X="-4.732102539062" Y="23.608115234375" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.848293945312" Y="24.106744140625" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-3.825697753906" Y="24.70112890625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.513390380859" Y="24.7871953125" />
                  <Point X="-3.492344970703" Y="24.798216796875" />
                  <Point X="-3.482250732422" Y="24.804330078125" />
                  <Point X="-3.459974853516" Y="24.819791015625" />
                  <Point X="-3.436020507812" Y="24.841318359375" />
                  <Point X="-3.415646972656" Y="24.87046875" />
                  <Point X="-3.406395996094" Y="24.89108984375" />
                  <Point X="-3.402342773438" Y="24.901814453125" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.389474365234" Y="24.9544765625" />
                  <Point X="-3.390187255859" Y="24.9870546875" />
                  <Point X="-3.394259765625" Y="25.0077578125" />
                  <Point X="-3.396742431641" Y="25.017580078125" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.417485351562" Y="25.07083203125" />
                  <Point X="-3.439332519531" Y="25.099197265625" />
                  <Point X="-3.456628173828" Y="25.114517578125" />
                  <Point X="-3.465451904297" Y="25.12144921875" />
                  <Point X="-3.487727783203" Y="25.13691015625" />
                  <Point X="-3.501923339844" Y="25.145046875" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.723819824219" Y="25.4769609375" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.795869628906" Y="26.082583984375" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.975568115234" Y="26.32742578125" />
                  <Point X="-3.765666259766" Y="26.29979296875" />
                  <Point X="-3.744984619141" Y="26.299341796875" />
                  <Point X="-3.723423583984" Y="26.301228515625" />
                  <Point X="-3.703137207031" Y="26.305263671875" />
                  <Point X="-3.691014892578" Y="26.3090859375" />
                  <Point X="-3.641711181641" Y="26.324630859375" />
                  <Point X="-3.622785400391" Y="26.332958984375" />
                  <Point X="-3.604040771484" Y="26.343779296875" />
                  <Point X="-3.58735546875" Y="26.35601171875" />
                  <Point X="-3.573714111328" Y="26.37156640625" />
                  <Point X="-3.561299560547" Y="26.389296875" />
                  <Point X="-3.551352539062" Y="26.407427734375" />
                  <Point X="-3.54648828125" Y="26.419169921875" />
                  <Point X="-3.526705078125" Y="26.466931640625" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.5158046875" Y="26.52875" />
                  <Point X="-3.518951416016" Y="26.549193359375" />
                  <Point X="-3.524553466797" Y="26.570099609375" />
                  <Point X="-3.532048828125" Y="26.589375" />
                  <Point X="-3.53791796875" Y="26.600650390625" />
                  <Point X="-3.561788574219" Y="26.64650390625" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.285264160156" Y="27.2187734375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.349174804688" Y="27.274474609375" />
                  <Point X="-4.081155517578" Y="27.733654296875" />
                  <Point X="-4.005346679688" Y="27.83109765625" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.335572753906" Y="27.9191015625" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729492188" Y="27.836341796875" />
                  <Point X="-3.167087402344" Y="27.82983203125" />
                  <Point X="-3.146791748047" Y="27.825794921875" />
                  <Point X="-3.129908447266" Y="27.824318359375" />
                  <Point X="-3.061242431641" Y="27.818310546875" />
                  <Point X="-3.040560791016" Y="27.81876171875" />
                  <Point X="-3.019102294922" Y="27.821587890625" />
                  <Point X="-2.999013427734" Y="27.826505859375" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962210205078" Y="27.84728125" />
                  <Point X="-2.946076416016" Y="27.86023046875" />
                  <Point X="-2.934092529297" Y="27.87221484375" />
                  <Point X="-2.885352783203" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435546875" Y="28.036119140625" />
                  <Point X="-2.844912597656" Y="28.05300390625" />
                  <Point X="-2.850920166016" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.172510009766" Y="28.705849609375" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-3.167430175781" Y="28.736787109375" />
                  <Point X="-2.700620117188" Y="29.0946875" />
                  <Point X="-2.581235107422" Y="29.161013671875" />
                  <Point X="-2.167036132812" Y="29.391134765625" />
                  <Point X="-2.082660400391" Y="29.281173828125" />
                  <Point X="-2.0431953125" Y="29.2297421875" />
                  <Point X="-2.028891723633" Y="29.21480078125" />
                  <Point X="-2.012312011719" Y="29.200888671875" />
                  <Point X="-1.995117553711" Y="29.1893984375" />
                  <Point X="-1.976326660156" Y="29.179615234375" />
                  <Point X="-1.899901367188" Y="29.139830078125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859718505859" Y="29.126728515625" />
                  <Point X="-1.839268676758" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797306762695" Y="29.128693359375" />
                  <Point X="-1.777451660156" Y="29.134482421875" />
                  <Point X="-1.757879882812" Y="29.14258984375" />
                  <Point X="-1.678277832031" Y="29.1755625" />
                  <Point X="-1.660145751953" Y="29.185509765625" />
                  <Point X="-1.642416259766" Y="29.197923828125" />
                  <Point X="-1.626864013672" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595480224609" Y="29.265921875" />
                  <Point X="-1.589109741211" Y="29.286126953125" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165649414" Y="29.388583984375" />
                  <Point X="-1.557279174805" Y="29.41014453125" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-1.553941894531" Y="29.640380859375" />
                  <Point X="-0.949622436523" Y="29.80980859375" />
                  <Point X="-0.804903442383" Y="29.826748046875" />
                  <Point X="-0.294711334229" Y="29.886458984375" />
                  <Point X="-0.172040039062" Y="29.428642578125" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129692078" Y="29.258123046875" />
                  <Point X="-0.103271400452" Y="29.231396484375" />
                  <Point X="-0.082113822937" Y="29.208806640625" />
                  <Point X="-0.054817928314" Y="29.194216796875" />
                  <Point X="-0.024379852295" Y="29.183884765625" />
                  <Point X="0.006156134129" Y="29.17884375" />
                  <Point X="0.036692108154" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094426101685" Y="29.208806640625" />
                  <Point X="0.115583656311" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.301322906494" Y="29.86518359375" />
                  <Point X="0.307419677734" Y="29.8879375" />
                  <Point X="0.316387481689" Y="29.886998046875" />
                  <Point X="0.844043457031" Y="29.83173828125" />
                  <Point X="0.963786621094" Y="29.802828125" />
                  <Point X="1.481024780273" Y="29.677951171875" />
                  <Point X="1.557779418945" Y="29.650111328125" />
                  <Point X="1.894647705078" Y="29.527927734375" />
                  <Point X="1.970016601562" Y="29.4926796875" />
                  <Point X="2.294558837891" Y="29.340900390625" />
                  <Point X="2.367419677734" Y="29.298453125" />
                  <Point X="2.680983642578" Y="29.115771484375" />
                  <Point X="2.749651123047" Y="29.0669375" />
                  <Point X="2.943260009766" Y="28.92925390625" />
                  <Point X="2.317623535156" Y="27.845619140625" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.139659667969" Y="27.533431640625" />
                  <Point X="2.130421142578" Y="27.505501953125" />
                  <Point X="2.12883984375" Y="27.5002109375" />
                  <Point X="2.111607177734" Y="27.43576953125" />
                  <Point X="2.10838671875" Y="27.410310546875" />
                  <Point X="2.108701171875" Y="27.37770703125" />
                  <Point X="2.109379882812" Y="27.36725" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121442382812" Y="27.289603515625" />
                  <Point X="2.129708496094" Y="27.267515625" />
                  <Point X="2.140070800781" Y="27.247470703125" />
                  <Point X="2.148548583984" Y="27.2349765625" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.200351074219" Y="27.16491796875" />
                  <Point X="2.226144775391" Y="27.143138671875" />
                  <Point X="2.234092041016" Y="27.13711328125" />
                  <Point X="2.284906738281" Y="27.1026328125" />
                  <Point X="2.304961914062" Y="27.092265625" />
                  <Point X="2.327056640625" Y="27.084" />
                  <Point X="2.348975585938" Y="27.07866015625" />
                  <Point X="2.362669189453" Y="27.077009765625" />
                  <Point X="2.418393066406" Y="27.0702890625" />
                  <Point X="2.444812988281" Y="27.070806640625" />
                  <Point X="2.479561279297" Y="27.0763828125" />
                  <Point X="2.489050537109" Y="27.078408203125" />
                  <Point X="2.553492431641" Y="27.095640625" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.786394775391" Y="27.80173046875" />
                  <Point X="3.967326171875" Y="27.90619140625" />
                  <Point X="4.12326953125" Y="27.689466796875" />
                  <Point X="4.161556640625" Y="27.6261953125" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.454660888672" Y="26.84023828125" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.216439208984" Y="26.6548515625" />
                  <Point X="3.195791259766" Y="26.630724609375" />
                  <Point X="3.192571289062" Y="26.62675" />
                  <Point X="3.146192382812" Y="26.56624609375" />
                  <Point X="3.133253173828" Y="26.543404296875" />
                  <Point X="3.120535644531" Y="26.511263671875" />
                  <Point X="3.117382080078" Y="26.501896484375" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.096652587891" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.101226318359" Y="26.354869140625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.124324951172" Y="26.261193359375" />
                  <Point X="3.141007324219" Y="26.2293984375" />
                  <Point X="3.145767333984" Y="26.221322265625" />
                  <Point X="3.184340820312" Y="26.162693359375" />
                  <Point X="3.198895996094" Y="26.145447265625" />
                  <Point X="3.216138916016" Y="26.129359375" />
                  <Point X="3.234347900391" Y="26.11603515625" />
                  <Point X="3.248091796875" Y="26.108298828125" />
                  <Point X="3.303989990234" Y="26.07683203125" />
                  <Point X="3.329188232422" Y="26.06705859375" />
                  <Point X="3.365747070312" Y="26.05860546875" />
                  <Point X="3.374701416016" Y="26.056982421875" />
                  <Point X="3.450279541016" Y="26.046994140625" />
                  <Point X="3.462697998047" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.626091308594" Y="26.1967890625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.858000976562" Y="25.855318359375" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.973147460938" Y="25.398337890625" />
                  <Point X="3.716579589844" Y="25.32958984375" />
                  <Point X="3.704786621094" Y="25.325583984375" />
                  <Point X="3.681546630859" Y="25.31506640625" />
                  <Point X="3.663289794922" Y="25.304513671875" />
                  <Point X="3.589036621094" Y="25.261595703125" />
                  <Point X="3.567971923828" Y="25.245060546875" />
                  <Point X="3.542705078125" Y="25.218681640625" />
                  <Point X="3.536576660156" Y="25.211619140625" />
                  <Point X="3.492024902344" Y="25.154849609375" />
                  <Point X="3.480300292969" Y="25.135568359375" />
                  <Point X="3.470526367188" Y="25.114103515625" />
                  <Point X="3.463680664062" Y="25.092603515625" />
                  <Point X="3.460029296875" Y="25.073537109375" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443921386719" Y="24.969005859375" />
                  <Point X="3.447572998047" Y="24.9311328125" />
                  <Point X="3.448830078125" Y="24.922380859375" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526367188" Y="24.8233359375" />
                  <Point X="3.480300292969" Y="24.80187109375" />
                  <Point X="3.492023193359" Y="24.782591796875" />
                  <Point X="3.502977294922" Y="24.7686328125" />
                  <Point X="3.547529052734" Y="24.71186328125" />
                  <Point X="3.560000244141" Y="24.69876171875" />
                  <Point X="3.589035888672" Y="24.67584375" />
                  <Point X="3.607292724609" Y="24.665291015625" />
                  <Point X="3.681545898438" Y="24.62237109375" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.76007421875" Y="24.328244140625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855021484375" Y="24.051267578125" />
                  <Point X="4.839565429688" Y="23.983537109375" />
                  <Point X="4.801174316406" Y="23.815302734375" />
                  <Point X="3.721928466797" Y="23.95738671875" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374661132812" Y="23.9944921875" />
                  <Point X="3.338829345703" Y="23.986705078125" />
                  <Point X="3.193096923828" Y="23.955029296875" />
                  <Point X="3.163973144531" Y="23.94340234375" />
                  <Point X="3.136146728516" Y="23.926509765625" />
                  <Point X="3.112396484375" Y="23.9060390625" />
                  <Point X="3.09073828125" Y="23.879990234375" />
                  <Point X="3.00265234375" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.966653320312" Y="23.660900390625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="2.996280517578" Y="23.401158203125" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.078997802734" Y="22.496033203125" />
                  <Point X="4.213121582031" Y="22.393115234375" />
                  <Point X="4.124810546875" Y="22.25021484375" />
                  <Point X="4.092842773438" Y="22.20479296875" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="3.065959960938" Y="22.6700546875" />
                  <Point X="2.800954589844" Y="22.8230546875" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754223388672" Y="22.840173828125" />
                  <Point X="2.711577880859" Y="22.847875" />
                  <Point X="2.538133056641" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444834960938" Y="22.864822265625" />
                  <Point X="2.409406982422" Y="22.846177734375" />
                  <Point X="2.265316894531" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.185886474609" Y="22.6741328125" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675292969" Y="22.4367421875" />
                  <Point X="2.103376953125" Y="22.394095703125" />
                  <Point X="2.134700927734" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.774093994141" Y="21.096109375" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.781859619141" Y="20.88836328125" />
                  <Point X="2.746108886719" Y="20.865220703125" />
                  <Point X="2.701764404297" Y="20.836517578125" />
                  <Point X="1.960769775391" Y="21.802201171875" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747506469727" Y="22.077818359375" />
                  <Point X="1.721924316406" Y="22.099443359375" />
                  <Point X="1.679864257812" Y="22.126484375" />
                  <Point X="1.50880078125" Y="22.2364609375" />
                  <Point X="1.479986328125" Y="22.24883203125" />
                  <Point X="1.448365844727" Y="22.2565625" />
                  <Point X="1.417100830078" Y="22.258880859375" />
                  <Point X="1.371101196289" Y="22.2546484375" />
                  <Point X="1.184014038086" Y="22.23743359375" />
                  <Point X="1.156367797852" Y="22.230603515625" />
                  <Point X="1.128982299805" Y="22.21926171875" />
                  <Point X="1.10459375" Y="22.2045390625" />
                  <Point X="1.069073974609" Y="22.17500390625" />
                  <Point X="0.924609985352" Y="22.05488671875" />
                  <Point X="0.904140014648" Y="22.031134765625" />
                  <Point X="0.887247680664" Y="22.003306640625" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.86500402832" Y="21.925328125" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.996090087891" Y="20.337384765625" />
                  <Point X="1.022065368652" Y="20.140083984375" />
                  <Point X="0.975685668945" Y="20.12991796875" />
                  <Point X="0.942653625488" Y="20.123916015625" />
                  <Point X="0.929315734863" Y="20.1214921875" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058415283203" Y="20.247361328125" />
                  <Point X="-1.109798950195" Y="20.26058203125" />
                  <Point X="-1.141245849609" Y="20.268673828125" />
                  <Point X="-1.130291992188" Y="20.351875" />
                  <Point X="-1.120775634766" Y="20.424158203125" />
                  <Point X="-1.120077514648" Y="20.4318984375" />
                  <Point X="-1.119451782227" Y="20.45896484375" />
                  <Point X="-1.121759277344" Y="20.490669921875" />
                  <Point X="-1.123333984375" Y="20.502306640625" />
                  <Point X="-1.135279052734" Y="20.562359375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.188124633789" Y="20.821529296875" />
                  <Point X="-1.199026489258" Y="20.85049609375" />
                  <Point X="-1.205665283203" Y="20.864533203125" />
                  <Point X="-1.221737304688" Y="20.89237109375" />
                  <Point X="-1.230575195312" Y="20.905140625" />
                  <Point X="-1.250209716797" Y="20.929064453125" />
                  <Point X="-1.261006591797" Y="20.94021875" />
                  <Point X="-1.307040893555" Y="20.98058984375" />
                  <Point X="-1.494267578125" Y="21.144783203125" />
                  <Point X="-1.506735717773" Y="21.15403125" />
                  <Point X="-1.53301940918" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531982422" Y="21.189775390625" />
                  <Point X="-1.59131640625" Y="21.194525390625" />
                  <Point X="-1.621458496094" Y="21.201552734375" />
                  <Point X="-1.636816162109" Y="21.203830078125" />
                  <Point X="-1.697913696289" Y="21.207833984375" />
                  <Point X="-1.946405395508" Y="21.22412109375" />
                  <Point X="-1.961929199219" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.007999267578" Y="21.2180546875" />
                  <Point X="-2.039047973633" Y="21.209736328125" />
                  <Point X="-2.053668701172" Y="21.204505859375" />
                  <Point X="-2.081861816406" Y="21.191732421875" />
                  <Point X="-2.095434326172" Y="21.184189453125" />
                  <Point X="-2.146343994141" Y="21.150173828125" />
                  <Point X="-2.353400634766" Y="21.011822265625" />
                  <Point X="-2.359678466797" Y="21.00724609375" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.747609619141" Y="20.988853515625" />
                  <Point X="-2.818705322266" Y="21.043595703125" />
                  <Point X="-2.980863037109" Y="21.168451171875" />
                  <Point X="-2.488403320312" Y="22.021416015625" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334850097656" Y="22.28991796875" />
                  <Point X="-2.323947753906" Y="22.318884765625" />
                  <Point X="-2.319684082031" Y="22.333814453125" />
                  <Point X="-2.313413330078" Y="22.365341796875" />
                  <Point X="-2.311638916016" Y="22.380767578125" />
                  <Point X="-2.310626708984" Y="22.411703125" />
                  <Point X="-2.314666992188" Y="22.442388671875" />
                  <Point X="-2.323651855469" Y="22.472005859375" />
                  <Point X="-2.329358642578" Y="22.486447265625" />
                  <Point X="-2.343576660156" Y="22.51527734375" />
                  <Point X="-2.351559326172" Y="22.52859375" />
                  <Point X="-2.369588134766" Y="22.553751953125" />
                  <Point X="-2.379634277344" Y="22.56559375" />
                  <Point X="-2.396982910156" Y="22.58294140625" />
                  <Point X="-2.408825683594" Y="22.59298828125" />
                  <Point X="-2.433986083984" Y="22.611015625" />
                  <Point X="-2.447303710938" Y="22.61899609375" />
                  <Point X="-2.476132324219" Y="22.6332109375" />
                  <Point X="-2.490572753906" Y="22.638916015625" />
                  <Point X="-2.520189208984" Y="22.6478984375" />
                  <Point X="-2.550873291016" Y="22.6519375" />
                  <Point X="-2.581805419922" Y="22.650923828125" />
                  <Point X="-2.597229492188" Y="22.6491484375" />
                  <Point X="-2.628754882813" Y="22.642876953125" />
                  <Point X="-2.643685546875" Y="22.63861328125" />
                  <Point X="-2.672649902344" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.721619384766" Y="22.02355078125" />
                  <Point X="-3.793087890625" Y="21.9822890625" />
                  <Point X="-4.004017089844" Y="22.259408203125" />
                  <Point X="-4.055001708984" Y="22.344900390625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.305175048828" Y="23.228873046875" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.039158691406" Y="23.433931640625" />
                  <Point X="-3.01626953125" Y="23.456564453125" />
                  <Point X="-3.002710205078" Y="23.47344140625" />
                  <Point X="-2.983769775391" Y="23.503474609375" />
                  <Point X="-2.975714599609" Y="23.519384765625" />
                  <Point X="-2.967480712891" Y="23.54032421875" />
                  <Point X="-2.960370361328" Y="23.56221875" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951952880859" Y="23.597603515625" />
                  <Point X="-2.947838623047" Y="23.6290859375" />
                  <Point X="-2.94708203125" Y="23.64430078125" />
                  <Point X="-2.94778125" Y="23.66716796875" />
                  <Point X="-2.951229003906" Y="23.68978515625" />
                  <Point X="-2.957375488281" Y="23.71182421875" />
                  <Point X="-2.961114746094" Y="23.722658203125" />
                  <Point X="-2.973120361328" Y="23.751642578125" />
                  <Point X="-2.978659179688" Y="23.762861328125" />
                  <Point X="-2.99116015625" Y="23.78446875" />
                  <Point X="-3.006388671875" Y="23.804244140625" />
                  <Point X="-3.024087402344" Y="23.8218515625" />
                  <Point X="-3.033519775391" Y="23.830072265625" />
                  <Point X="-3.051248779297" Y="23.843578125" />
                  <Point X="-3.070018310547" Y="23.85618359375" />
                  <Point X="-3.091274658203" Y="23.868693359375" />
                  <Point X="-3.105436523438" Y="23.87551953125" />
                  <Point X="-3.134699951172" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891142578125" />
                  <Point X="-3.181687744141" Y="23.897623046875" />
                  <Point X="-3.1973046875" Y="23.89946875" />
                  <Point X="-3.228625244141" Y="23.90055859375" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.550837402344" Y="23.727798828125" />
                  <Point X="-4.660920898438" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.754250976562" Y="24.120193359375" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-3.801109863281" Y="24.609365234375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.498334228516" Y="24.691091796875" />
                  <Point X="-3.478848632812" Y="24.698697265625" />
                  <Point X="-3.469316894531" Y="24.703037109375" />
                  <Point X="-3.448271484375" Y="24.71405859375" />
                  <Point X="-3.428083007812" Y="24.72628515625" />
                  <Point X="-3.405807128906" Y="24.74174609375" />
                  <Point X="-3.396474609375" Y="24.749130859375" />
                  <Point X="-3.372520263672" Y="24.770658203125" />
                  <Point X="-3.358153564453" Y="24.786896484375" />
                  <Point X="-3.337780029297" Y="24.816046875" />
                  <Point X="-3.328969726562" Y="24.831583984375" />
                  <Point X="-3.31971875" Y="24.852205078125" />
                  <Point X="-3.311612304688" Y="24.873654296875" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.301576904297" Y="24.90805859375" />
                  <Point X="-3.296133789062" Y="24.936796875" />
                  <Point X="-3.294497070312" Y="24.9565546875" />
                  <Point X="-3.295209960938" Y="24.9891328125" />
                  <Point X="-3.296973632812" Y="25.005390625" />
                  <Point X="-3.301046142578" Y="25.02609375" />
                  <Point X="-3.306011474609" Y="25.04573828125" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.317669189453" Y="25.080787109375" />
                  <Point X="-3.330986816406" Y="25.11011328125" />
                  <Point X="-3.342221679688" Y="25.12880078125" />
                  <Point X="-3.364068847656" Y="25.157166015625" />
                  <Point X="-3.376341064453" Y="25.170310546875" />
                  <Point X="-3.39363671875" Y="25.185630859375" />
                  <Point X="-3.411284179688" Y="25.199494140625" />
                  <Point X="-3.433560058594" Y="25.214955078125" />
                  <Point X="-3.440485351562" Y="25.219330078125" />
                  <Point X="-3.465612548828" Y="25.232833984375" />
                  <Point X="-3.496565185547" Y="25.24563671875" />
                  <Point X="-3.508288330078" Y="25.24961328125" />
                  <Point X="-4.699231933594" Y="25.568724609375" />
                  <Point X="-4.785446289062" Y="25.591826171875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.704176757812" Y="26.057736328125" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-3.987968261719" Y="26.23323828125" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.76773828125" Y="26.20481640625" />
                  <Point X="-3.747056640625" Y="26.204365234375" />
                  <Point X="-3.736703125" Y="26.204703125" />
                  <Point X="-3.715142089844" Y="26.20658984375" />
                  <Point X="-3.704890136719" Y="26.2080546875" />
                  <Point X="-3.684603759766" Y="26.21208984375" />
                  <Point X="-3.662447021484" Y="26.218482421875" />
                  <Point X="-3.613143310547" Y="26.23402734375" />
                  <Point X="-3.603447998047" Y="26.237677734375" />
                  <Point X="-3.584522216797" Y="26.246005859375" />
                  <Point X="-3.575291748047" Y="26.25068359375" />
                  <Point X="-3.556547119141" Y="26.26150390625" />
                  <Point X="-3.547871582031" Y="26.2671640625" />
                  <Point X="-3.531186279297" Y="26.279396484375" />
                  <Point X="-3.515931396484" Y="26.293373046875" />
                  <Point X="-3.502290039063" Y="26.308927734375" />
                  <Point X="-3.495893798828" Y="26.317078125" />
                  <Point X="-3.483479248047" Y="26.33480859375" />
                  <Point X="-3.478010742188" Y="26.343603515625" />
                  <Point X="-3.468063720703" Y="26.361734375" />
                  <Point X="-3.458720947266" Y="26.3828125" />
                  <Point X="-3.438937744141" Y="26.43057421875" />
                  <Point X="-3.435500244141" Y="26.44034765625" />
                  <Point X="-3.429710693359" Y="26.4602109375" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.421008056641" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057128906" Y="26.563646484375" />
                  <Point X="-3.427188720703" Y="26.57378125" />
                  <Point X="-3.432790771484" Y="26.5946875" />
                  <Point X="-3.436011962891" Y="26.604529296875" />
                  <Point X="-3.443507324219" Y="26.6238046875" />
                  <Point X="-3.453650634766" Y="26.644513671875" />
                  <Point X="-3.477521240234" Y="26.6903671875" />
                  <Point X="-3.482798339844" Y="26.69928125" />
                  <Point X="-3.494291503906" Y="26.716482421875" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.227431640625" Y="27.294142578125" />
                  <Point X="-4.227613769531" Y="27.29428125" />
                  <Point X="-4.002293457031" Y="27.68030859375" />
                  <Point X="-3.930365478516" Y="27.772763671875" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.383072753906" Y="27.836830078125" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244924804688" Y="27.757720703125" />
                  <Point X="-3.225997314453" Y="27.749390625" />
                  <Point X="-3.216302001953" Y="27.745740234375" />
                  <Point X="-3.195659912109" Y="27.73923046875" />
                  <Point X="-3.18562109375" Y="27.736658203125" />
                  <Point X="-3.165325439453" Y="27.73262109375" />
                  <Point X="-3.138185302734" Y="27.7296796875" />
                  <Point X="-3.069519287109" Y="27.723671875" />
                  <Point X="-3.059170410156" Y="27.723333984375" />
                  <Point X="-3.038488769531" Y="27.72378515625" />
                  <Point X="-3.028156005859" Y="27.72457421875" />
                  <Point X="-3.006697509766" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423583984" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938450195312" Y="27.750447265625" />
                  <Point X="-2.929421630859" Y="27.755529296875" />
                  <Point X="-2.911167480469" Y="27.767158203125" />
                  <Point X="-2.90274609375" Y="27.773193359375" />
                  <Point X="-2.886612304688" Y="27.786142578125" />
                  <Point X="-2.866916015625" Y="27.805041015625" />
                  <Point X="-2.818176269531" Y="27.853779296875" />
                  <Point X="-2.811265136719" Y="27.86148828125" />
                  <Point X="-2.798321289062" Y="27.877615234375" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.013365234375" />
                  <Point X="-2.748458251953" Y="28.034044921875" />
                  <Point X="-2.750273925781" Y="28.061283203125" />
                  <Point X="-2.756281494141" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761781005859" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.05938671875" Y="28.6999140625" />
                  <Point X="-2.6483671875" Y="29.015041015625" />
                  <Point X="-2.535098388672" Y="29.07796875" />
                  <Point X="-2.192523925781" Y="29.268296875" />
                  <Point X="-2.158029052734" Y="29.223341796875" />
                  <Point X="-2.118563964844" Y="29.17191015625" />
                  <Point X="-2.111819091797" Y="29.164046875" />
                  <Point X="-2.097515625" Y="29.14910546875" />
                  <Point X="-2.089956787109" Y="29.14202734375" />
                  <Point X="-2.073376953125" Y="29.128115234375" />
                  <Point X="-2.065095214844" Y="29.12190234375" />
                  <Point X="-2.047900756836" Y="29.110412109375" />
                  <Point X="-2.03898815918" Y="29.105134765625" />
                  <Point X="-2.020197143555" Y="29.0953515625" />
                  <Point X="-1.943772094727" Y="29.05556640625" />
                  <Point X="-1.934340698242" Y="29.05129296875" />
                  <Point X="-1.915064453125" Y="29.043794921875" />
                  <Point X="-1.905219604492" Y="29.0405703125" />
                  <Point X="-1.884312866211" Y="29.034966796875" />
                  <Point X="-1.874174316406" Y="29.032833984375" />
                  <Point X="-1.853724487305" Y="29.029685546875" />
                  <Point X="-1.833054077148" Y="29.028783203125" />
                  <Point X="-1.812407470703" Y="29.03013671875" />
                  <Point X="-1.802120117188" Y="29.031376953125" />
                  <Point X="-1.7808046875" Y="29.03513671875" />
                  <Point X="-1.770715209961" Y="29.037490234375" />
                  <Point X="-1.750860107422" Y="29.043279296875" />
                  <Point X="-1.741094726562" Y="29.04671484375" />
                  <Point X="-1.721522949219" Y="29.054822265625" />
                  <Point X="-1.641920898438" Y="29.087794921875" />
                  <Point X="-1.632585083008" Y="29.0922734375" />
                  <Point X="-1.61445300293" Y="29.102220703125" />
                  <Point X="-1.605656738281" Y="29.107689453125" />
                  <Point X="-1.587927246094" Y="29.120103515625" />
                  <Point X="-1.579779296875" Y="29.126498046875" />
                  <Point X="-1.564226928711" Y="29.14013671875" />
                  <Point X="-1.550251708984" Y="29.155388671875" />
                  <Point X="-1.538020263672" Y="29.1720703125" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.51685546875" Y="29.208728515625" />
                  <Point X="-1.508525024414" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.498506469727" Y="29.257560546875" />
                  <Point X="-1.472597412109" Y="29.339732421875" />
                  <Point X="-1.470026489258" Y="29.349763671875" />
                  <Point X="-1.465991210938" Y="29.370048828125" />
                  <Point X="-1.46452722168" Y="29.380302734375" />
                  <Point X="-1.46264074707" Y="29.40186328125" />
                  <Point X="-1.462301757812" Y="29.412216796875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.93116015625" Y="29.716322265625" />
                  <Point X="-0.793858947754" Y="29.732392578125" />
                  <Point X="-0.36522265625" Y="29.78255859375" />
                  <Point X="-0.263802978516" Y="29.4040546875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.22043510437" Y="29.247107421875" />
                  <Point X="-0.207661407471" Y="29.218916015625" />
                  <Point X="-0.200119094849" Y="29.20534375" />
                  <Point X="-0.18226071167" Y="29.1786171875" />
                  <Point X="-0.172608703613" Y="29.166455078125" />
                  <Point X="-0.151451034546" Y="29.143865234375" />
                  <Point X="-0.126896308899" Y="29.1250234375" />
                  <Point X="-0.099600372314" Y="29.11043359375" />
                  <Point X="-0.08535382843" Y="29.1042578125" />
                  <Point X="-0.054915859222" Y="29.09392578125" />
                  <Point X="-0.039853370667" Y="29.090154296875" />
                  <Point X="-0.009317459106" Y="29.08511328125" />
                  <Point X="0.021629692078" Y="29.08511328125" />
                  <Point X="0.052165752411" Y="29.090154296875" />
                  <Point X="0.067228240967" Y="29.09392578125" />
                  <Point X="0.097666061401" Y="29.1042578125" />
                  <Point X="0.111912162781" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763427734" Y="29.143865234375" />
                  <Point X="0.184920928955" Y="29.166455078125" />
                  <Point X="0.194573242188" Y="29.1786171875" />
                  <Point X="0.21243132019" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.378190795898" Y="29.785005859375" />
                  <Point X="0.827880249023" Y="29.737912109375" />
                  <Point X="0.941490844727" Y="29.710482421875" />
                  <Point X="1.45358972168" Y="29.586845703125" />
                  <Point X="1.52538671875" Y="29.5608046875" />
                  <Point X="1.858249633789" Y="29.44007421875" />
                  <Point X="1.929771362305" Y="29.406625" />
                  <Point X="2.250440429688" Y="29.25665625" />
                  <Point X="2.319598144531" Y="29.2163671875" />
                  <Point X="2.629443847656" Y="29.0358515625" />
                  <Point X="2.694593505859" Y="28.98951953125" />
                  <Point X="2.817780029297" Y="28.901916015625" />
                  <Point X="2.235351074219" Y="27.893119140625" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.060896484375" Y="27.589966796875" />
                  <Point X="2.049465820313" Y="27.563265625" />
                  <Point X="2.040227294922" Y="27.5353359375" />
                  <Point X="2.037064697266" Y="27.52475390625" />
                  <Point X="2.01983203125" Y="27.4603125" />
                  <Point X="2.017358276367" Y="27.44769140625" />
                  <Point X="2.014137817383" Y="27.422232421875" />
                  <Point X="2.013391235352" Y="27.40939453125" />
                  <Point X="2.013705566406" Y="27.376791015625" />
                  <Point X="2.015062988281" Y="27.355876953125" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144042969" Y="27.267109375" />
                  <Point X="2.03246887207" Y="27.256306640625" />
                  <Point X="2.040734985352" Y="27.23421875" />
                  <Point X="2.045317871094" Y="27.223888671875" />
                  <Point X="2.055680175781" Y="27.20384375" />
                  <Point X="2.069937255859" Y="27.181634765625" />
                  <Point X="2.104417236328" Y="27.1308203125" />
                  <Point X="2.112420654297" Y="27.12060546875" />
                  <Point X="2.129743164062" Y="27.101361328125" />
                  <Point X="2.139062255859" Y="27.09233203125" />
                  <Point X="2.164855957031" Y="27.070552734375" />
                  <Point X="2.180750488281" Y="27.058501953125" />
                  <Point X="2.231565185547" Y="27.024021484375" />
                  <Point X="2.241281982422" Y="27.0182421875" />
                  <Point X="2.261337158203" Y="27.007875" />
                  <Point X="2.271675537109" Y="27.003287109375" />
                  <Point X="2.293770263672" Y="26.995021484375" />
                  <Point X="2.304570556641" Y="26.99169921875" />
                  <Point X="2.326489501953" Y="26.986359375" />
                  <Point X="2.351301757812" Y="26.98269140625" />
                  <Point X="2.407025634766" Y="26.975970703125" />
                  <Point X="2.42025390625" Y="26.975306640625" />
                  <Point X="2.446673828125" Y="26.97582421875" />
                  <Point X="2.459865478516" Y="26.977005859375" />
                  <Point X="2.494613769531" Y="26.98258203125" />
                  <Point X="2.513592285156" Y="26.9866328125" />
                  <Point X="2.578034179688" Y="27.003865234375" />
                  <Point X="2.583991943359" Y="27.005669921875" />
                  <Point X="2.604414794922" Y="27.0130703125" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.833894775391" Y="27.719458984375" />
                  <Point X="3.940404296875" Y="27.780953125" />
                  <Point X="4.043947998047" Y="27.63705078125" />
                  <Point X="4.080279052734" Y="27.57701171875" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.396828613281" Y="26.915607421875" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.165423095703" Y="26.737392578125" />
                  <Point X="3.144262207031" Y="26.71662109375" />
                  <Point X="3.123614257812" Y="26.692494140625" />
                  <Point X="3.117174316406" Y="26.684544921875" />
                  <Point X="3.070795410156" Y="26.624041015625" />
                  <Point X="3.063533447266" Y="26.6130703125" />
                  <Point X="3.050594238281" Y="26.590228515625" />
                  <Point X="3.044916992188" Y="26.578357421875" />
                  <Point X="3.032199462891" Y="26.546216796875" />
                  <Point X="3.025892333984" Y="26.527482421875" />
                  <Point X="3.008616210938" Y="26.46570703125" />
                  <Point X="3.006225097656" Y="26.45466015625" />
                  <Point X="3.002771728516" Y="26.432361328125" />
                  <Point X="3.001709472656" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.008186279297" Y="26.335671875" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.025953125" Y="26.254154296875" />
                  <Point X="3.034869628906" Y="26.229212890625" />
                  <Point X="3.040201416016" Y="26.2170546875" />
                  <Point X="3.056883789063" Y="26.185259765625" />
                  <Point X="3.066403808594" Y="26.169107421875" />
                  <Point X="3.104977294922" Y="26.110478515625" />
                  <Point X="3.111740966797" Y="26.101421875" />
                  <Point X="3.126296142578" Y="26.08417578125" />
                  <Point X="3.134087646484" Y="26.075986328125" />
                  <Point X="3.151330566406" Y="26.0598984375" />
                  <Point X="3.160038818359" Y="26.052693359375" />
                  <Point X="3.178247802734" Y="26.039369140625" />
                  <Point X="3.201492431641" Y="26.025513671875" />
                  <Point X="3.257390625" Y="25.994046875" />
                  <Point X="3.26963671875" Y="25.98826171875" />
                  <Point X="3.294834960938" Y="25.97848828125" />
                  <Point X="3.307787109375" Y="25.9745" />
                  <Point X="3.344345947266" Y="25.966046875" />
                  <Point X="3.362254638672" Y="25.96280078125" />
                  <Point X="3.437832763672" Y="25.9528125" />
                  <Point X="3.444032714844" Y="25.95219921875" />
                  <Point X="3.465708251953" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.638491210938" Y="26.1026015625" />
                  <Point X="4.704704101562" Y="26.111318359375" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.764131835938" Y="25.840703125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.948559570313" Y="25.4901015625" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686024414062" Y="25.419541015625" />
                  <Point X="3.665617431641" Y="25.4121328125" />
                  <Point X="3.642377441406" Y="25.401615234375" />
                  <Point X="3.634005615234" Y="25.397314453125" />
                  <Point X="3.615748779297" Y="25.38676171875" />
                  <Point X="3.541495605469" Y="25.34384375" />
                  <Point X="3.530377929688" Y="25.336322265625" />
                  <Point X="3.509313232422" Y="25.319787109375" />
                  <Point X="3.499366210938" Y="25.3107734375" />
                  <Point X="3.474099365234" Y="25.28439453125" />
                  <Point X="3.461842529297" Y="25.27026953125" />
                  <Point X="3.417290771484" Y="25.2135" />
                  <Point X="3.410854003906" Y="25.204208984375" />
                  <Point X="3.399129394531" Y="25.184927734375" />
                  <Point X="3.393841552734" Y="25.1749375" />
                  <Point X="3.384067626953" Y="25.15347265625" />
                  <Point X="3.380004150391" Y="25.14292578125" />
                  <Point X="3.373158447266" Y="25.12142578125" />
                  <Point X="3.366724853516" Y="25.09140625" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.35028125" Y="25.000412109375" />
                  <Point X="3.349024169922" Y="24.97342578125" />
                  <Point X="3.349359863281" Y="24.959888671875" />
                  <Point X="3.353011474609" Y="24.922015625" />
                  <Point X="3.355525634766" Y="24.90451171875" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158447266" Y="24.816013671875" />
                  <Point X="3.380004150391" Y="24.794513671875" />
                  <Point X="3.384067626953" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.762501953125" />
                  <Point X="3.399128417969" Y="24.752513671875" />
                  <Point X="3.410851318359" Y="24.733234375" />
                  <Point X="3.428241455078" Y="24.709984375" />
                  <Point X="3.472793212891" Y="24.65321484375" />
                  <Point X="3.478718994141" Y="24.64636328125" />
                  <Point X="3.501141845703" Y="24.62419140625" />
                  <Point X="3.530177490234" Y="24.6012734375" />
                  <Point X="3.541494873047" Y="24.593595703125" />
                  <Point X="3.559751708984" Y="24.58304296875" />
                  <Point X="3.634004882812" Y="24.540123046875" />
                  <Point X="3.639488037109" Y="24.5371875" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.735486328125" Y="24.23648046875" />
                  <Point X="4.784876464844" Y="24.22324609375" />
                  <Point X="4.761612304688" Y="24.068939453125" />
                  <Point X="4.746946289063" Y="24.004671875" />
                  <Point X="4.727802734375" Y="23.92078125" />
                  <Point X="3.734328369141" Y="24.05157421875" />
                  <Point X="3.436781738281" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400101806641" Y="24.09195703125" />
                  <Point X="3.366727539062" Y="24.08916015625" />
                  <Point X="3.354486328125" Y="24.087326171875" />
                  <Point X="3.318654541016" Y="24.0795390625" />
                  <Point X="3.172922119141" Y="24.04786328125" />
                  <Point X="3.157873779297" Y="24.0432578125" />
                  <Point X="3.12875" Y="24.031630859375" />
                  <Point X="3.114674560547" Y="24.024609375" />
                  <Point X="3.086848144531" Y="24.007716796875" />
                  <Point X="3.074123779297" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.03934765625" Y="23.966775390625" />
                  <Point X="3.017689453125" Y="23.9407265625" />
                  <Point X="2.929603515625" Y="23.834787109375" />
                  <Point X="2.921324707031" Y="23.823150390625" />
                  <Point X="2.906605224609" Y="23.79876953125" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.872052978516" Y="23.66960546875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.916370605469" Y="23.349783203125" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="4.021165527344" Y="22.4206640625" />
                  <Point X="4.087169921875" Y="22.370015625" />
                  <Point X="4.045486572266" Y="22.30256640625" />
                  <Point X="4.015154541016" Y="22.25946875" />
                  <Point X="4.001274169922" Y="22.23974609375" />
                  <Point X="3.113459960938" Y="22.752326171875" />
                  <Point X="2.848454589844" Y="22.905326171875" />
                  <Point X="2.841191650391" Y="22.90911328125" />
                  <Point X="2.815025390625" Y="22.920484375" />
                  <Point X="2.783119384766" Y="22.930671875" />
                  <Point X="2.771105957031" Y="22.933662109375" />
                  <Point X="2.728460449219" Y="22.94136328125" />
                  <Point X="2.555015625" Y="22.9726875" />
                  <Point X="2.539358154297" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444845947266" Y="22.96486328125" />
                  <Point X="2.415069580078" Y="22.9550390625" />
                  <Point X="2.400592285156" Y="22.948890625" />
                  <Point X="2.365164306641" Y="22.93024609375" />
                  <Point X="2.22107421875" Y="22.854412109375" />
                  <Point X="2.208972167969" Y="22.846830078125" />
                  <Point X="2.186040771484" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940429687" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.101818603516" Y="22.718376953125" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.000683227539" Y="22.435517578125" />
                  <Point X="2.0021875" Y="22.419859375" />
                  <Point X="2.009889160156" Y="22.377212890625" />
                  <Point X="2.041213134766" Y="22.203767578125" />
                  <Point X="2.043014892578" Y="22.19577734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.691821533203" Y="21.048609375" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723752441406" Y="20.963916015625" />
                  <Point X="2.036138305664" Y="21.860033203125" />
                  <Point X="1.833914550781" Y="22.123576171875" />
                  <Point X="1.828657714844" Y="22.12984765625" />
                  <Point X="1.808835571289" Y="22.150369140625" />
                  <Point X="1.783253417969" Y="22.171994140625" />
                  <Point X="1.773299560547" Y="22.179353515625" />
                  <Point X="1.731239501953" Y="22.20639453125" />
                  <Point X="1.560176025391" Y="22.31637109375" />
                  <Point X="1.546279541016" Y="22.323755859375" />
                  <Point X="1.517465087891" Y="22.336126953125" />
                  <Point X="1.502547119141" Y="22.34111328125" />
                  <Point X="1.470926635742" Y="22.34884375" />
                  <Point X="1.455390991211" Y="22.351302734375" />
                  <Point X="1.424126098633" Y="22.35362109375" />
                  <Point X="1.408396484375" Y="22.35348046875" />
                  <Point X="1.362396972656" Y="22.349248046875" />
                  <Point X="1.175309814453" Y="22.332033203125" />
                  <Point X="1.161229125977" Y="22.32966015625" />
                  <Point X="1.133582885742" Y="22.322830078125" />
                  <Point X="1.120017333984" Y="22.318373046875" />
                  <Point X="1.092631835938" Y="22.30703125" />
                  <Point X="1.079885864258" Y="22.300591796875" />
                  <Point X="1.055497314453" Y="22.285869140625" />
                  <Point X="1.043854736328" Y="22.2775859375" />
                  <Point X="1.008334899902" Y="22.24805078125" />
                  <Point X="0.863870910645" Y="22.12793359375" />
                  <Point X="0.852647216797" Y="22.11690625" />
                  <Point X="0.832177368164" Y="22.093154296875" />
                  <Point X="0.822930969238" Y="22.0804296875" />
                  <Point X="0.806038635254" Y="22.0526015625" />
                  <Point X="0.799017822266" Y="22.03852734375" />
                  <Point X="0.787394470215" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.772171630859" Y="21.945505859375" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091369629" Y="20.84766015625" />
                  <Point X="0.734119689941" Y="21.21702734375" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407287598" Y="21.58679296875" />
                  <Point X="0.588095458984" Y="21.63334765625" />
                  <Point X="0.456679382324" Y="21.822693359375" />
                  <Point X="0.446668762207" Y="21.834830078125" />
                  <Point X="0.424785949707" Y="21.857283203125" />
                  <Point X="0.412913513184" Y="21.867599609375" />
                  <Point X="0.38665838623" Y="21.88684375" />
                  <Point X="0.373243682861" Y="21.8950625" />
                  <Point X="0.345240600586" Y="21.909171875" />
                  <Point X="0.33065222168" Y="21.9150625" />
                  <Point X="0.280651611328" Y="21.930580078125" />
                  <Point X="0.077293128967" Y="21.9936953125" />
                  <Point X="0.063376678467" Y="21.996890625" />
                  <Point X="0.035218582153" Y="22.00116015625" />
                  <Point X="0.020976791382" Y="22.002234375" />
                  <Point X="-0.008664410591" Y="22.002234375" />
                  <Point X="-0.022904417038" Y="22.00116015625" />
                  <Point X="-0.051066078186" Y="21.996890625" />
                  <Point X="-0.064987289429" Y="21.9936953125" />
                  <Point X="-0.11498789978" Y="21.97817578125" />
                  <Point X="-0.318346374512" Y="21.9150625" />
                  <Point X="-0.332931335449" Y="21.909171875" />
                  <Point X="-0.36093057251" Y="21.895064453125" />
                  <Point X="-0.374344970703" Y="21.88684765625" />
                  <Point X="-0.400600402832" Y="21.867603515625" />
                  <Point X="-0.412476409912" Y="21.857283203125" />
                  <Point X="-0.434360961914" Y="21.834826171875" />
                  <Point X="-0.444369384766" Y="21.822689453125" />
                  <Point X="-0.476681182861" Y="21.7761328125" />
                  <Point X="-0.608096984863" Y="21.5867890625" />
                  <Point X="-0.612471557617" Y="21.5798671875" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.96427935791" Y="20.312111328125" />
                  <Point X="-0.985425292969" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.315929711249" Y="29.199734870804" />
                  <Point X="-3.011153827175" Y="28.616372571545" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.713098911695" Y="28.027370710017" />
                  <Point X="-3.768354978219" Y="27.981005364977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.150968368055" Y="29.21414018055" />
                  <Point X="-2.9629209356" Y="28.53283108059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.625546427032" Y="27.97682227512" />
                  <Point X="-4.02473225052" Y="27.64186559786" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.08953606162" Y="29.141674313736" />
                  <Point X="-2.914688044024" Y="28.449289589634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.537993942369" Y="27.926273840223" />
                  <Point X="-4.166601870857" Y="27.398809159274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.420354125852" Y="29.579170936742" />
                  <Point X="-1.475363591221" Y="29.53301251464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.005849118502" Y="29.087882304353" />
                  <Point X="-2.866455152449" Y="28.365748098679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.450441457706" Y="27.875725405325" />
                  <Point X="-4.183614332368" Y="27.260520316607" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.198400827142" Y="29.641398175342" />
                  <Point X="-1.462467618629" Y="29.419819827998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.911851017161" Y="29.042742384033" />
                  <Point X="-2.818222260874" Y="28.282206607724" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.362888990202" Y="27.82517695603" />
                  <Point X="-4.106415916827" Y="27.201283786129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.976447528431" Y="29.703625413942" />
                  <Point X="-1.49507428457" Y="29.268445894147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.770104217825" Y="29.03766837859" />
                  <Point X="-2.77279675051" Y="28.19630944423" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.27533657997" Y="27.774628458678" />
                  <Point X="-4.029217501287" Y="27.14204725565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.794453298306" Y="29.73232301283" />
                  <Point X="-2.752711891287" Y="28.08914894971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.175252062747" Y="27.73459564768" />
                  <Point X="-3.952019085746" Y="27.082810725171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.622704234423" Y="29.752423896502" />
                  <Point X="-2.756809210554" Y="27.961697198137" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.040391201014" Y="27.723743654534" />
                  <Point X="-3.874820670205" Y="27.023574194692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.450955177038" Y="29.772524774723" />
                  <Point X="-3.797622254665" Y="26.964337664213" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.576562724666" Y="26.310729003126" />
                  <Point X="-4.653000862232" Y="26.246589790086" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.351635416749" Y="29.731850256463" />
                  <Point X="-3.720423839124" Y="26.905101133734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.448812790174" Y="26.293910233554" />
                  <Point X="-4.696496400969" Y="26.086079007088" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.324505801108" Y="29.630601014455" />
                  <Point X="-3.643225423583" Y="26.845864603255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.321062855682" Y="26.277091463983" />
                  <Point X="-4.735503376709" Y="25.929334575644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.297376185467" Y="29.529351772447" />
                  <Point X="-3.566027008042" Y="26.786628072776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.19331292119" Y="26.260272694411" />
                  <Point X="-4.756455938361" Y="25.787739596404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.270246569826" Y="29.428102530439" />
                  <Point X="-3.49720328578" Y="26.720364340257" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.065562986698" Y="26.24345392484" />
                  <Point X="-4.777408500012" Y="25.646144617163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.243116959366" Y="29.326853284083" />
                  <Point X="-3.449617638436" Y="26.636279746906" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.937812665013" Y="26.226635480162" />
                  <Point X="-4.716398425503" Y="25.573324455695" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.442638315409" Y="29.778256589738" />
                  <Point X="0.357164756759" Y="29.706535758199" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.212150547136" Y="29.228823496678" />
                  <Point X="-3.42158658456" Y="26.535786901388" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.810061744303" Y="26.209817538126" />
                  <Point X="-4.604376557371" Y="25.543308271442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.574033155039" Y="29.764496258724" />
                  <Point X="0.31429711714" Y="29.546551845119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.157680093429" Y="29.150515941806" />
                  <Point X="-3.460272632662" Y="26.379311760207" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.645618195079" Y="26.223788367142" />
                  <Point X="-4.492354586451" Y="25.513292173439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.705427994669" Y="29.750735927709" />
                  <Point X="0.271429477521" Y="29.386567932038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.070872735898" Y="29.099342271008" />
                  <Point X="-4.38033261553" Y="25.483276075435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.835691405327" Y="29.736026215061" />
                  <Point X="0.220311352757" Y="29.219661039916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.072665467944" Y="29.095771432425" />
                  <Point X="-4.268310644609" Y="25.453259977432" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.950462016278" Y="29.708316499894" />
                  <Point X="-4.156288673689" Y="25.423243879428" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.065233083136" Y="29.680607167278" />
                  <Point X="-4.044266702768" Y="25.393227781425" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.180004149995" Y="29.652897834662" />
                  <Point X="-3.932244731848" Y="25.363211683422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.294775216853" Y="29.625188502046" />
                  <Point X="-3.820222760927" Y="25.333195585418" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.409546283711" Y="29.59747916943" />
                  <Point X="-3.708200790007" Y="25.303179487415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.517180335985" Y="29.563781170508" />
                  <Point X="-3.596178819086" Y="25.273163389411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.662252748154" Y="24.378621148723" />
                  <Point X="-4.777393787667" Y="24.282006344934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.620370044492" Y="29.526353924372" />
                  <Point X="-3.486266613161" Y="25.241376988379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.445122420216" Y="24.436801434326" />
                  <Point X="-4.76155760134" Y="24.171280790554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.723559753931" Y="29.488926679017" />
                  <Point X="-3.399458307335" Y="25.190204113293" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.227992092278" Y="24.49498171993" />
                  <Point X="-4.74572152706" Y="24.060555142155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.82674946337" Y="29.451499433662" />
                  <Point X="-3.336394136861" Y="25.119107542993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.01086176434" Y="24.553162005534" />
                  <Point X="-4.722845231002" Y="23.955736941253" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.924180225633" Y="29.409239857855" />
                  <Point X="-3.300819995" Y="25.024944099821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.793731478166" Y="24.611342256093" />
                  <Point X="-4.696759292502" Y="23.853611950141" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01908083817" Y="29.364857234347" />
                  <Point X="-3.304021086038" Y="24.898244373025" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.576602379241" Y="24.669521510433" />
                  <Point X="-4.670673354002" Y="23.751486959029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.113981470399" Y="29.320474627363" />
                  <Point X="-4.551160733292" Y="23.727756262501" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.208882102629" Y="29.276092020378" />
                  <Point X="-4.375863494426" Y="23.750834418493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.299471827842" Y="29.228092132907" />
                  <Point X="-4.200566253575" Y="23.773912576151" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.386701535702" Y="29.177272856113" />
                  <Point X="-4.025269012723" Y="23.796990733809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.473930810692" Y="29.126453216098" />
                  <Point X="-3.849971771872" Y="23.820068891467" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.561160085681" Y="29.075633576083" />
                  <Point X="-3.674674531021" Y="23.843147049126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.646818192863" Y="29.02349556974" />
                  <Point X="-3.49937729017" Y="23.866225206784" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726813838195" Y="28.966606193748" />
                  <Point X="-3.324080049319" Y="23.889303364442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.806810031696" Y="28.909717277723" />
                  <Point X="-3.169357714572" Y="23.895117125976" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.697944316259" Y="28.694354403565" />
                  <Point X="-3.068876778522" Y="23.85541694987" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.559063696176" Y="28.453806033989" />
                  <Point X="-2.996842259819" Y="23.791847395458" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.420183076093" Y="28.213257664413" />
                  <Point X="-2.954917949458" Y="23.703012376333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.281302456009" Y="27.972709294837" />
                  <Point X="-2.956331618534" Y="23.577812474647" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088557957202" Y="22.627761771461" />
                  <Point X="-4.178620491853" Y="22.552190331852" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.142421328885" Y="27.732160499804" />
                  <Point X="-4.129329450169" Y="22.469536734263" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.035336720469" Y="27.51829215189" />
                  <Point X="-4.080038408485" Y="22.386883136674" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.013746391078" Y="27.376162021975" />
                  <Point X="-4.030747124301" Y="22.304229742566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.029643254565" Y="27.265487381777" />
                  <Point X="-3.977651278204" Y="22.224768754957" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.072682971758" Y="27.177588300113" />
                  <Point X="-3.920048223823" Y="22.149089764156" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.129695746477" Y="27.101414005866" />
                  <Point X="-3.862445169443" Y="22.073410773355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.205961542654" Y="27.041394914822" />
                  <Point X="-3.804842115062" Y="21.997731782554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.297223623186" Y="26.99395920045" />
                  <Point X="-3.415980312618" Y="22.200011885077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.422848712463" Y="26.975357474043" />
                  <Point X="-2.942190571059" Y="22.473554989988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.628814157372" Y="27.024169310415" />
                  <Point X="-2.583209459382" Y="22.650762215909" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.100832649311" Y="27.296226160423" />
                  <Point X="-2.463661021096" Y="22.627061573896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.574618639652" Y="27.569766117689" />
                  <Point X="-2.383963126439" Y="22.569922355422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953087873609" Y="27.763325819828" />
                  <Point X="-2.331200408257" Y="22.490181840302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.008727498375" Y="27.685999315961" />
                  <Point X="-2.311577866054" Y="22.382633415741" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.062213631944" Y="27.606865818425" />
                  <Point X="-2.388925763358" Y="22.193717131154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.111985574887" Y="27.524615744905" />
                  <Point X="3.272539802689" Y="26.82023710706" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.132052828802" Y="26.702354539087" />
                  <Point X="-2.527805464301" Y="21.953169532828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.0119261473" Y="26.477542592457" />
                  <Point X="-2.666686301667" Y="21.71262098093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.005863480394" Y="26.348441718406" />
                  <Point X="-2.805567139032" Y="21.472072429033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.029492160495" Y="26.244254842677" />
                  <Point X="-2.944447976398" Y="21.231523877135" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.073989020494" Y="26.157578449004" />
                  <Point X="-2.923999594014" Y="21.124668414765" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.129851443032" Y="26.080438894666" />
                  <Point X="-2.846927591531" Y="21.065325811136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.20789353708" Y="26.021910294511" />
                  <Point X="-2.769856064866" Y="21.005982808248" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.301494946244" Y="25.976437509932" />
                  <Point X="-2.687115263861" Y="20.951396891369" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.423405773259" Y="25.95471914743" />
                  <Point X="-2.602073910816" Y="20.898741366857" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.58161981216" Y="25.963462796633" />
                  <Point X="-2.187045819353" Y="21.122977592845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.459741402516" Y="20.894158829589" />
                  <Point X="-2.517032557771" Y="20.846085842345" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756917048981" Y="25.986540950909" />
                  <Point X="-1.920720247396" Y="21.222437589561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.932214285802" Y="26.009619105185" />
                  <Point X="-1.783634543069" Y="21.213452461015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.107511522623" Y="26.032697259462" />
                  <Point X="-1.64654822258" Y="21.20446784949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.282808759444" Y="26.055775413738" />
                  <Point X="-1.536966024694" Y="21.172404538833" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.458105996265" Y="26.078853568014" />
                  <Point X="3.66110583646" Y="25.410091027873" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.405758252019" Y="25.195828963948" />
                  <Point X="-1.458975371068" Y="21.113832775039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.633403233086" Y="26.101931722291" />
                  <Point X="3.88346582445" Y="25.472659519298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.354780980584" Y="25.029040261801" />
                  <Point X="-1.386709515903" Y="21.050457334969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.719576786218" Y="26.050226226454" />
                  <Point X="4.100596296653" Y="25.530839925955" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.355379543716" Y="24.905528823418" />
                  <Point X="-0.144054851798" Y="21.969154712813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.616152120614" Y="21.573018068669" />
                  <Point X="-1.314443660738" Y="20.987081894898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.744646400028" Y="25.94724843767" />
                  <Point X="4.317726556138" Y="25.589020154119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.378078414805" Y="24.800561745291" />
                  <Point X="0.040861776473" Y="22.000304494907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.672266389511" Y="21.401918913848" />
                  <Point X="-1.24433013639" Y="20.921900434832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.764285810654" Y="25.839714167395" />
                  <Point X="4.534856815622" Y="25.647200382283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.424263876831" Y="24.715302256955" />
                  <Point X="0.152837011078" Y="21.970249180479" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.715133908999" Y="21.241935101569" />
                  <Point X="-1.1948518769" Y="20.839403931635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.7813629837" Y="25.730029924514" />
                  <Point X="4.751987075107" Y="25.705380610448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.483851339096" Y="24.641288382078" />
                  <Point X="0.260725286471" Y="21.936764500083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.758001428487" Y="21.081951289291" />
                  <Point X="-1.169885800373" Y="20.736339264754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.561219690028" Y="24.582194444324" />
                  <Point X="0.364231844183" Y="21.899603121997" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.800868947976" Y="20.921967477012" />
                  <Point X="-1.148746285028" Y="20.630063731797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.649687430366" Y="24.532414000126" />
                  <Point X="0.441343680409" Y="21.840293942847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.843736467464" Y="20.761983664734" />
                  <Point X="-1.127606858732" Y="20.523788124118" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.757197770697" Y="24.498612194558" />
                  <Point X="3.236479754725" Y="24.061677899409" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.896006399918" Y="23.775986832965" />
                  <Point X="1.175134202619" Y="22.332003606908" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.78625883116" Y="22.005698426143" />
                  <Point X="0.497709470934" Y="21.7635767644" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.886603986952" Y="20.601999852455" />
                  <Point X="-1.123536157652" Y="20.403190155406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.869219642495" Y="24.468596013381" />
                  <Point X="3.419950570377" Y="24.091614500668" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.868290433136" Y="23.628716682974" />
                  <Point X="1.341149057359" Y="22.347292917804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.752062980483" Y="21.852991007966" />
                  <Point X="0.552103578067" Y="21.685205147147" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.92947150644" Y="20.442016040177" />
                  <Point X="-1.137271480539" Y="20.267651158551" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.981241514294" Y="24.438579832205" />
                  <Point X="3.549089080704" Y="24.075960884567" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.860652120215" Y="23.498293684932" />
                  <Point X="1.486309223339" Y="22.345083067052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.724726750694" Y="21.706039495145" />
                  <Point X="0.60649782116" Y="21.606833643979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.972338994399" Y="20.282032254354" />
                  <Point X="-1.022066691672" Y="20.240305761913" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.093263386092" Y="24.408563651028" />
                  <Point X="3.676839582133" Y="24.059142590713" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.887996131466" Y="23.397224342202" />
                  <Point X="2.274787401413" Y="22.882681122979" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.119883339761" Y="22.752701181979" />
                  <Point X="1.582659251021" Y="22.301916647258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735248065232" Y="21.590854233807" />
                  <Point X="0.652122434118" Y="21.521103547397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.205285257891" Y="24.378547469852" />
                  <Point X="3.804589899058" Y="24.042324142041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.938405722522" Y="23.315509318978" />
                  <Point X="2.531755720793" Y="22.974289452509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.012960740882" Y="22.538968776208" />
                  <Point X="1.666339274674" Y="22.248118831756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.749950588451" Y="21.47917742313" />
                  <Point X="0.679676973503" Y="21.420210858746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.317307129689" Y="24.348531288675" />
                  <Point X="3.932340065021" Y="24.025505566696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.990196335666" Y="23.234953110879" />
                  <Point X="2.655922118652" Y="22.95446373867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.004374328205" Y="22.407750228012" />
                  <Point X="1.75001910551" Y="22.19432085446" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.764653111669" Y="21.367500612454" />
                  <Point X="0.706806561442" Y="21.318961593493" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.429329001488" Y="24.318515107498" />
                  <Point X="4.060090230984" Y="24.008686991352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.052954139045" Y="23.163599468061" />
                  <Point X="2.777136566245" Y="22.932161044452" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.023823521365" Y="22.300056346332" />
                  <Point X="1.8252191739" Y="22.133407511624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.779355634887" Y="21.255823801777" />
                  <Point X="0.73393614938" Y="21.21771232824" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.541350873286" Y="24.288498926322" />
                  <Point X="4.187840396946" Y="23.991868416007" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.130152584968" Y="23.104362963076" />
                  <Point X="2.874813435534" Y="22.890107976961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.043887249842" Y="22.192878121011" />
                  <Point X="1.883807122814" Y="22.058554945463" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.794058158106" Y="21.1441469911" />
                  <Point X="0.76106580374" Y="21.11646311872" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.653372745085" Y="24.258482745145" />
                  <Point X="4.315590562909" Y="23.975049840663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.20735103089" Y="23.04512645809" />
                  <Point X="2.962366124251" Y="22.839559713285" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.083557768708" Y="22.102151946274" />
                  <Point X="1.941694575904" Y="21.983114593514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.808760681324" Y="21.032470180424" />
                  <Point X="0.788195458551" Y="21.01521390958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.765394440281" Y="24.228466415781" />
                  <Point X="4.443340728871" Y="23.958231265318" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.284549476813" Y="22.985889953104" />
                  <Point X="3.049918812967" Y="22.789011449609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.131790568956" Y="22.018610378686" />
                  <Point X="1.999582028995" Y="21.907674241565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.823463204543" Y="20.920793369747" />
                  <Point X="0.815325113363" Y="20.91396470044" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.767194169635" Y="24.105962875532" />
                  <Point X="4.571090894834" Y="23.941412689974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.361747922735" Y="22.926653448119" />
                  <Point X="3.137471436848" Y="22.73846313153" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.180023369204" Y="21.935068811098" />
                  <Point X="2.057469459759" Y="21.832233870883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.735737791031" Y="23.95555414736" />
                  <Point X="4.698841060797" Y="23.924594114629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.438946368657" Y="22.867416943133" />
                  <Point X="3.225023889159" Y="22.687914669485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.228256169452" Y="21.85152724351" />
                  <Point X="2.115356852263" Y="21.756793468096" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.51614481458" Y="22.808180438148" />
                  <Point X="3.312576341469" Y="22.637366207441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.2764889697" Y="21.767985675922" />
                  <Point X="2.173244244767" Y="21.68135306531" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.593343260502" Y="22.748943933162" />
                  <Point X="3.400128793779" Y="22.586817745397" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.324721769948" Y="21.684444108334" />
                  <Point X="2.231131637271" Y="21.605912662523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.670541706425" Y="22.689707428176" />
                  <Point X="3.48768124609" Y="22.536269283352" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.372954570195" Y="21.600902540746" />
                  <Point X="2.289019029774" Y="21.530472259736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.747740152347" Y="22.630470923191" />
                  <Point X="3.5752336984" Y="22.485720821308" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.421187370443" Y="21.517360973158" />
                  <Point X="2.346906422278" Y="21.455031856949" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.824938598269" Y="22.571234418205" />
                  <Point X="3.66278615071" Y="22.435172359264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.469420170691" Y="21.43381940557" />
                  <Point X="2.404793814782" Y="21.379591454163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.902137044192" Y="22.511997913219" />
                  <Point X="3.750338603021" Y="22.384623897219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.517652970939" Y="21.350277837982" />
                  <Point X="2.462681207286" Y="21.304151051376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.979335490114" Y="22.452761408234" />
                  <Point X="3.837891055331" Y="22.334075435175" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.565885771187" Y="21.266736270394" />
                  <Point X="2.52056859979" Y="21.228710648589" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.056533456348" Y="22.393524500742" />
                  <Point X="3.925443507641" Y="22.283526973131" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.614118571435" Y="21.183194702806" />
                  <Point X="2.578455992294" Y="21.153270245802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662351371682" Y="21.099653135218" />
                  <Point X="2.636343384798" Y="21.077829843015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.710584252759" Y="21.016111635454" />
                  <Point X="2.694230777301" Y="21.002389440229" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001625" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.55059387207" Y="21.1678515625" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.432007049561" Y="21.52501171875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.274335906982" Y="21.7336015625" />
                  <Point X="0.224335281372" Y="21.749119140625" />
                  <Point X="0.02097677803" Y="21.812234375" />
                  <Point X="-0.008664452553" Y="21.812234375" />
                  <Point X="-0.058664928436" Y="21.79671484375" />
                  <Point X="-0.2620234375" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.320590789795" Y="21.66780078125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.780753540039" Y="20.262935546875" />
                  <Point X="-0.84774432373" Y="20.012921875" />
                  <Point X="-0.861136962891" Y="20.015521484375" />
                  <Point X="-1.100231201172" Y="20.061931640625" />
                  <Point X="-1.157142944336" Y="20.07657421875" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.318666625977" Y="20.37667578125" />
                  <Point X="-1.309150146484" Y="20.448958984375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.321628417969" Y="20.52529296875" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282714844" Y="20.79737109375" />
                  <Point X="-1.432316894531" Y="20.8377421875" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649240722656" Y="21.014236328125" />
                  <Point X="-1.710338256836" Y="21.018240234375" />
                  <Point X="-1.958829833984" Y="21.03452734375" />
                  <Point X="-1.989878540039" Y="21.026208984375" />
                  <Point X="-2.040788330078" Y="20.992193359375" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.440246337891" Y="20.60745703125" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.50534765625" Y="20.615376953125" />
                  <Point X="-2.855839355469" Y="20.832392578125" />
                  <Point X="-2.93462109375" Y="20.893052734375" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.652948242188" Y="22.116416015625" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499762939453" Y="22.402408203125" />
                  <Point X="-2.513980957031" Y="22.43123828125" />
                  <Point X="-2.531329589844" Y="22.4485859375" />
                  <Point X="-2.560158203125" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.626619384766" Y="21.8590078125" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.884800537109" Y="21.789076171875" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.218187011719" Y="22.247583984375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.420839599609" Y="23.379611328125" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.152535400391" Y="23.588916015625" />
                  <Point X="-3.144301513672" Y="23.60985546875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136651855469" Y="23.649947265625" />
                  <Point X="-3.148657470703" Y="23.678931640625" />
                  <Point X="-3.166386474609" Y="23.6924375" />
                  <Point X="-3.187642822266" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.526037597656" Y="23.539423828125" />
                  <Point X="-4.803283203125" Y="23.502923828125" />
                  <Point X="-4.819094726562" Y="23.56482421875" />
                  <Point X="-4.927393554688" Y="23.988810546875" />
                  <Point X="-4.942336914062" Y="24.09329296875" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-3.850285644531" Y="24.792892578125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.536418457031" Y="24.882375" />
                  <Point X="-3.514142578125" Y="24.8978359375" />
                  <Point X="-3.50232421875" Y="24.909353515625" />
                  <Point X="-3.493073242188" Y="24.929974609375" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.487473388672" Y="24.989421875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.502323974609" Y="25.028083984375" />
                  <Point X="-3.519619628906" Y="25.043404296875" />
                  <Point X="-3.541895507813" Y="25.058865234375" />
                  <Point X="-3.557463623047" Y="25.0660859375" />
                  <Point X="-4.748407714844" Y="25.385197265625" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.987442382813" Y="25.524736328125" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.8875625" Y="26.107431640625" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.96316796875" Y="26.42161328125" />
                  <Point X="-3.753266113281" Y="26.39398046875" />
                  <Point X="-3.731705078125" Y="26.3958671875" />
                  <Point X="-3.719582763672" Y="26.399689453125" />
                  <Point X="-3.670279052734" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.634255615234" Y="26.45552734375" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.616316162109" Y="26.54551171875" />
                  <Point X="-3.622185302734" Y="26.556787109375" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.343096679688" Y="27.143404296875" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.431221191406" Y="27.32236328125" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.080327636719" Y="27.889431640625" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.288072753906" Y="28.001373046875" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514892578" Y="27.92043359375" />
                  <Point X="-3.121631591797" Y="27.91895703125" />
                  <Point X="-3.052965576172" Y="27.91294921875" />
                  <Point X="-3.031507080078" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-3.001269042969" Y="27.939388671875" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.939551269531" Y="28.044724609375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.254782470703" Y="28.658349609375" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.225232421875" Y="28.8121796875" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.627372070312" Y="29.24405859375" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.007291870117" Y="29.339005859375" />
                  <Point X="-1.967826660156" Y="29.28757421875" />
                  <Point X="-1.951246948242" Y="29.273662109375" />
                  <Point X="-1.932456054688" Y="29.26387890625" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835124145508" Y="29.218490234375" />
                  <Point X="-1.81380859375" Y="29.22225" />
                  <Point X="-1.794236816406" Y="29.230357421875" />
                  <Point X="-1.714634765625" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.679713012695" Y="29.314693359375" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.686332885742" Y="29.6798359375" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-1.579588500977" Y="29.731853515625" />
                  <Point X="-0.968082946777" Y="29.903296875" />
                  <Point X="-0.81594720459" Y="29.921103515625" />
                  <Point X="-0.224200027466" Y="29.990359375" />
                  <Point X="-0.080277153015" Y="29.45323046875" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282051086" Y="29.28417578125" />
                  <Point X="0.006156086445" Y="29.27384375" />
                  <Point X="0.036594120026" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.209559921265" Y="29.889771484375" />
                  <Point X="0.2366484375" Y="29.9908671875" />
                  <Point X="0.326282104492" Y="29.98148046875" />
                  <Point X="0.860210144043" Y="29.925564453125" />
                  <Point X="0.986082763672" Y="29.895173828125" />
                  <Point X="1.508455932617" Y="29.769056640625" />
                  <Point X="1.590171508789" Y="29.73941796875" />
                  <Point X="1.931043945312" Y="29.61578125" />
                  <Point X="2.010261108398" Y="29.578734375" />
                  <Point X="2.338684814453" Y="29.425140625" />
                  <Point X="2.415241943359" Y="29.3805390625" />
                  <Point X="2.732519775391" Y="29.195693359375" />
                  <Point X="2.804708496094" Y="29.14435546875" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.399895996094" Y="27.798119140625" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.220614990234" Y="27.47566796875" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.203696777344" Y="27.378623046875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682128906" Y="27.3008125" />
                  <Point X="2.227159912109" Y="27.288318359375" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.28743359375" Y="27.215724609375" />
                  <Point X="2.338248291016" Y="27.181244140625" />
                  <Point X="2.360343017578" Y="27.172978515625" />
                  <Point X="2.374036621094" Y="27.171328125" />
                  <Point X="2.429760498047" Y="27.164607421875" />
                  <Point X="2.464508789062" Y="27.17018359375" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.738894775391" Y="27.884001953125" />
                  <Point X="3.994247558594" Y="28.0314296875" />
                  <Point X="4.014391113281" Y="28.003435546875" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.242833984375" Y="27.67537890625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.512493164062" Y="26.764869140625" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.267968261719" Y="26.568955078125" />
                  <Point X="3.221589355469" Y="26.508451171875" />
                  <Point X="3.208871826172" Y="26.476310546875" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.194266357422" Y="26.37406640625" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.225130859375" Y="26.273537109375" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280947265625" Y="26.1988203125" />
                  <Point X="3.294691162109" Y="26.191083984375" />
                  <Point X="3.350589355469" Y="26.1596171875" />
                  <Point X="3.387148193359" Y="26.1511640625" />
                  <Point X="3.462726318359" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.61369140625" Y="26.2909765625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.858357421875" Y="26.2834140625" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.951870117188" Y="25.86993359375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.997735351562" Y="25.30657421875" />
                  <Point X="3.741167480469" Y="25.237826171875" />
                  <Point X="3.729087646484" Y="25.232818359375" />
                  <Point X="3.710830810547" Y="25.222265625" />
                  <Point X="3.636577636719" Y="25.17934765625" />
                  <Point X="3.611310791016" Y="25.15296875" />
                  <Point X="3.566759033203" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.553333740234" Y="25.05566796875" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.542134521484" Y="24.94025" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566759033203" Y="24.841240234375" />
                  <Point X="3.577713134766" Y="24.82728125" />
                  <Point X="3.622264892578" Y="24.77051171875" />
                  <Point X="3.636576904297" Y="24.758091796875" />
                  <Point X="3.654833740234" Y="24.7475390625" />
                  <Point X="3.729086914062" Y="24.704619140625" />
                  <Point X="3.74116796875" Y="24.699611328125" />
                  <Point X="4.784662109375" Y="24.4200078125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.993564453125" Y="24.332955078125" />
                  <Point X="4.948430664062" Y="24.033595703125" />
                  <Point X="4.932184570312" Y="23.96240234375" />
                  <Point X="4.874545898438" Y="23.709822265625" />
                  <Point X="3.709528564453" Y="23.86319921875" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.3948359375" Y="23.901658203125" />
                  <Point X="3.359004150391" Y="23.89387109375" />
                  <Point X="3.213271728516" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.163787109375" Y="23.81925390625" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.061253662109" Y="23.6521953125" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.076190429688" Y="23.452533203125" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.136830078125" Y="22.57140234375" />
                  <Point X="4.33907421875" Y="22.41621484375" />
                  <Point X="4.331355957031" Y="22.4037265625" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.170530761719" Y="22.150115234375" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.018459960938" Y="22.587783203125" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.6946953125" Y="22.75438671875" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.453649658203" Y="22.762109375" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.269954345703" Y="22.629888671875" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.196864746094" Y="22.410978515625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.856366455078" Y="21.143609375" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.986260498047" Y="20.9176171875" />
                  <Point X="2.835291259766" Y="20.809783203125" />
                  <Point X="2.797732421875" Y="20.785470703125" />
                  <Point X="2.679775634766" Y="20.709119140625" />
                  <Point X="1.885401123047" Y="21.744369140625" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549072266" Y="22.019533203125" />
                  <Point X="1.628489013672" Y="22.04657421875" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.425805053711" Y="22.16428125" />
                  <Point X="1.379805419922" Y="22.160048828125" />
                  <Point X="1.192718261719" Y="22.142833984375" />
                  <Point X="1.165332763672" Y="22.1314921875" />
                  <Point X="1.129812988281" Y="22.10195703125" />
                  <Point X="0.985349060059" Y="21.98183984375" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.957836547852" Y="21.905150390625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.09027734375" Y="20.34978515625" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="0.994365539551" Y="20.0367578125" />
                  <Point X="0.959638977051" Y="20.030447265625" />
                  <Point X="0.860200439453" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#136" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.038136890874" Y="4.497453295221" Z="0.5" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="-0.827915273868" Y="5.00204409515" Z="0.5" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.5" />
                  <Point X="-1.599242271024" Y="4.811278434743" Z="0.5" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.5" />
                  <Point X="-1.742061162791" Y="4.704590751138" Z="0.5" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.5" />
                  <Point X="-1.733554822718" Y="4.361008135229" Z="0.5" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.5" />
                  <Point X="-1.819527040381" Y="4.307831638657" Z="0.5" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.5" />
                  <Point X="-1.915524258653" Y="4.339509307679" Z="0.5" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.5" />
                  <Point X="-1.973780258798" Y="4.400723208351" Z="0.5" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.5" />
                  <Point X="-2.657810906068" Y="4.319046421952" Z="0.5" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.5" />
                  <Point X="-3.261810535445" Y="3.883140741479" Z="0.5" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.5" />
                  <Point X="-3.304239630436" Y="3.66463054233" Z="0.5" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.5" />
                  <Point X="-2.995517115832" Y="3.071646754582" Z="0.5" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.5" />
                  <Point X="-3.042779740196" Y="3.00602382769" Z="0.5" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.5" />
                  <Point X="-3.123429711025" Y="3.000047583341" Z="0.5" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.5" />
                  <Point X="-3.269228805968" Y="3.07595431831" Z="0.5" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.5" />
                  <Point X="-4.125946930877" Y="2.951415245782" Z="0.5" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.5" />
                  <Point X="-4.48055922162" Y="2.378849488806" Z="0.5" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.5" />
                  <Point X="-4.379690835413" Y="2.135017031883" Z="0.5" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.5" />
                  <Point X="-3.672691605992" Y="1.56497895798" Z="0.5" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.5" />
                  <Point X="-3.686605718933" Y="1.505943301832" Z="0.5" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.5" />
                  <Point X="-3.740773592902" Y="1.478654733526" Z="0.5" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.5" />
                  <Point X="-3.96279791781" Y="1.502466645589" Z="0.5" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.5" />
                  <Point X="-4.94197802241" Y="1.151790644736" Z="0.5" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.5" />
                  <Point X="-5.043059627374" Y="0.563334640107" Z="0.5" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.5" />
                  <Point X="-4.767505181791" Y="0.368181639541" Z="0.5" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.5" />
                  <Point X="-3.554284900563" Y="0.033608330389" Z="0.5" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.5" />
                  <Point X="-3.541382453463" Y="0.005882413615" Z="0.5" />
                  <Point X="-3.539556741714" Y="0" Z="0.5" />
                  <Point X="-3.546982147672" Y="-0.023924537446" Z="0.5" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.5" />
                  <Point X="-3.57108369456" Y="-0.045267652745" Z="0.5" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.5" />
                  <Point X="-3.869382606982" Y="-0.127530417215" Z="0.5" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.5" />
                  <Point X="-4.997989007571" Y="-0.882504063647" Z="0.5" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.5" />
                  <Point X="-4.87369529821" Y="-1.416270307696" Z="0.5" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.5" />
                  <Point X="-4.525667313974" Y="-1.478868372375" Z="0.5" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.5" />
                  <Point X="-3.197902653851" Y="-1.319373802303" Z="0.5" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.5" />
                  <Point X="-3.198860316737" Y="-1.346326515599" Z="0.5" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.5" />
                  <Point X="-3.457433600999" Y="-1.54944071182" Z="0.5" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.5" />
                  <Point X="-4.267286242335" Y="-2.74674503476" Z="0.5" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.5" />
                  <Point X="-3.930742362975" Y="-3.209926574513" Z="0.5" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.5" />
                  <Point X="-3.607775403804" Y="-3.153011485823" Z="0.5" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.5" />
                  <Point X="-2.558914709476" Y="-2.569415759656" Z="0.5" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.5" />
                  <Point X="-2.702405527345" Y="-2.827302978688" Z="0.5" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.5" />
                  <Point X="-2.971280736256" Y="-4.115284823655" Z="0.5" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.5" />
                  <Point X="-2.537825181713" Y="-4.395854482605" Z="0.5" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.5" />
                  <Point X="-2.406734542332" Y="-4.391700261423" Z="0.5" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.5" />
                  <Point X="-2.019165744383" Y="-4.018101223664" Z="0.5" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.5" />
                  <Point X="-1.71976439021" Y="-4.000371385834" Z="0.5" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.5" />
                  <Point X="-1.471440104372" Y="-4.168569606677" Z="0.5" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.5" />
                  <Point X="-1.37682365033" Y="-4.453180138481" Z="0.5" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.5" />
                  <Point X="-1.374394873083" Y="-4.58551596202" Z="0.5" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.5" />
                  <Point X="-1.175757800425" Y="-4.940569090611" Z="0.5" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.5" />
                  <Point X="-0.876820656357" Y="-5.002281227471" Z="0.5" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="-0.73861320183" Y="-4.718726106664" Z="0.5" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="-0.28567080604" Y="-3.329427714769" Z="0.5" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="-0.050000514809" Y="-3.219757683044" Z="0.5" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.5" />
                  <Point X="0.203358564552" Y="-3.267354362244" Z="0.5" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.5" />
                  <Point X="0.384775053392" Y="-3.472218040999" Z="0.5" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.5" />
                  <Point X="0.496141658571" Y="-3.813809923152" Z="0.5" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.5" />
                  <Point X="0.96241957787" Y="-4.987466886689" Z="0.5" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.5" />
                  <Point X="1.141718803913" Y="-4.94950052471" Z="0.5" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.5" />
                  <Point X="1.133693673411" Y="-4.612408742999" Z="0.5" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.5" />
                  <Point X="1.00053975596" Y="-3.074187540582" Z="0.5" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.5" />
                  <Point X="1.155623889634" Y="-2.90520903992" Z="0.5" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.5" />
                  <Point X="1.378230702624" Y="-2.858459528383" Z="0.5" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.5" />
                  <Point X="1.595293901527" Y="-2.964204578815" Z="0.5" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.5" />
                  <Point X="1.83957742246" Y="-3.254788045243" Z="0.5" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.5" />
                  <Point X="2.818745470965" Y="-4.225222505864" Z="0.5" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.5" />
                  <Point X="3.00916620709" Y="-4.091790636511" Z="0.5" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.5" />
                  <Point X="2.893511635416" Y="-3.800109635104" Z="0.5" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.5" />
                  <Point X="2.239912866837" Y="-2.548853715577" Z="0.5" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.5" />
                  <Point X="2.308044712285" Y="-2.362118142417" Z="0.5" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.5" />
                  <Point X="2.470780235955" Y="-2.250856597682" Z="0.5" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.5" />
                  <Point X="2.679653087935" Y="-2.263535142654" Z="0.5" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.5" />
                  <Point X="2.987303962003" Y="-2.424237864858" Z="0.5" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.5" />
                  <Point X="4.205263436533" Y="-2.847380944181" Z="0.5" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.5" />
                  <Point X="4.367733862343" Y="-2.591277640809" Z="0.5" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.5" />
                  <Point X="4.161112057066" Y="-2.35764897496" Z="0.5" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.5" />
                  <Point X="3.112092337112" Y="-1.489146496182" Z="0.5" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.5" />
                  <Point X="3.104887342365" Y="-1.321105387354" Z="0.5" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.5" />
                  <Point X="3.196077492814" Y="-1.181432076666" Z="0.5" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.5" />
                  <Point X="3.363468067248" Y="-1.123708612849" Z="0.5" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.5" />
                  <Point X="3.696846410924" Y="-1.155093168082" Z="0.5" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.5" />
                  <Point X="4.974775769551" Y="-1.017440652959" Z="0.5" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.5" />
                  <Point X="5.036849677771" Y="-0.64321929892" Z="0.5" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.5" />
                  <Point X="4.791447283777" Y="-0.500414155159" Z="0.5" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.5" />
                  <Point X="3.67369959692" Y="-0.177891012539" Z="0.5" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.5" />
                  <Point X="3.610890853046" Y="-0.110568666154" Z="0.5" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.5" />
                  <Point X="3.585086103161" Y="-0.019066126687" Z="0.5" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.5" />
                  <Point X="3.596285347263" Y="0.077544404547" Z="0.5" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.5" />
                  <Point X="3.644488585354" Y="0.153380072421" Z="0.5" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.5" />
                  <Point X="3.729695817433" Y="0.210257831703" Z="0.5" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.5" />
                  <Point X="4.004520533529" Y="0.28955777761" Z="0.5" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.5" />
                  <Point X="4.995118716369" Y="0.908906246445" Z="0.5" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.5" />
                  <Point X="4.900782471628" Y="1.326521463902" Z="0.5" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.5" />
                  <Point X="4.601008902438" Y="1.371829859051" Z="0.5" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.5" />
                  <Point X="3.387543289257" Y="1.23201265304" Z="0.5" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.5" />
                  <Point X="3.313062441555" Y="1.265934411604" Z="0.5" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.5" />
                  <Point X="3.260745173634" Y="1.332300870633" Z="0.5" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.5" />
                  <Point X="3.23707901014" Y="1.415449537762" Z="0.5" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.5" />
                  <Point X="3.25086822531" Y="1.49412472846" Z="0.5" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.5" />
                  <Point X="3.301494824235" Y="1.569818551949" Z="0.5" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.5" />
                  <Point X="3.53677515648" Y="1.756481957005" Z="0.5" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.5" />
                  <Point X="4.279455449826" Y="2.73254584825" Z="0.5" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.5" />
                  <Point X="4.048820622612" Y="3.063919496717" Z="0.5" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.5" />
                  <Point X="3.707738737099" Y="2.958583993521" Z="0.5" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.5" />
                  <Point X="2.445436181861" Y="2.249766207509" Z="0.5" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.5" />
                  <Point X="2.373867813266" Y="2.25224843809" Z="0.5" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.5" />
                  <Point X="2.30935207246" Y="2.288380383869" Z="0.5" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.5" />
                  <Point X="2.262378216366" Y="2.347672787922" Z="0.5" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.5" />
                  <Point X="2.247181264669" Y="2.415890630567" Z="0.5" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.5" />
                  <Point X="2.262761735149" Y="2.494033394021" Z="0.5" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.5" />
                  <Point X="2.437041359339" Y="2.804400344856" Z="0.5" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.5" />
                  <Point X="2.827529645801" Y="4.216384670466" Z="0.5" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.5" />
                  <Point X="2.434255903581" Y="4.455023092266" Z="0.5" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.5" />
                  <Point X="2.025286609756" Y="4.65530602659" Z="0.5" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.5" />
                  <Point X="1.601064153245" Y="4.817703141869" Z="0.5" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.5" />
                  <Point X="0.991661131727" Y="4.975058654269" Z="0.5" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.5" />
                  <Point X="0.32533396494" Y="5.062489744039" Z="0.5" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.5" />
                  <Point X="0.155107670679" Y="4.93399421073" Z="0.5" />
                  <Point X="0" Y="4.355124473572" Z="0.5" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>