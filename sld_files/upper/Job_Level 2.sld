<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#119" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="337" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475585938" Y="25.004715820312" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.766443359375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.571387817383" Y="21.457298828125" />
                  <Point X="0.563302124023" Y="21.4874765625" />
                  <Point X="0.555903381348" Y="21.50656640625" />
                  <Point X="0.545378234863" Y="21.526896484375" />
                  <Point X="0.539058288574" Y="21.53738671875" />
                  <Point X="0.378635406494" Y="21.768525390625" />
                  <Point X="0.357096191406" Y="21.792490234375" />
                  <Point X="0.328327056885" Y="21.8126875" />
                  <Point X="0.308468933105" Y="21.82169921875" />
                  <Point X="0.297370300293" Y="21.825919921875" />
                  <Point X="0.0491365242" Y="21.902962890625" />
                  <Point X="0.0203969841" Y="21.90840625" />
                  <Point X="-0.011795484543" Y="21.90776953125" />
                  <Point X="-0.031733154297" Y="21.90393359375" />
                  <Point X="-0.041943489075" Y="21.901375" />
                  <Point X="-0.29018258667" Y="21.82433203125" />
                  <Point X="-0.319508483887" Y="21.811015625" />
                  <Point X="-0.347585174561" Y="21.7894921875" />
                  <Point X="-0.362407806396" Y="21.772912109375" />
                  <Point X="-0.369628326416" Y="21.763763671875" />
                  <Point X="-0.530051208496" Y="21.532625" />
                  <Point X="-0.538189453125" Y="21.51842578125" />
                  <Point X="-0.550989929199" Y="21.4874765625" />
                  <Point X="-0.916584350586" Y="20.12305859375" />
                  <Point X="-1.07933190918" Y="20.1546484375" />
                  <Point X="-1.081078491211" Y="20.15509765625" />
                  <Point X="-1.246417724609" Y="20.197638671875" />
                  <Point X="-1.215936523438" Y="20.429166015625" />
                  <Point X="-1.214440063477" Y="20.456369140625" />
                  <Point X="-1.216194580078" Y="20.47879296875" />
                  <Point X="-1.21773046875" Y="20.489916015625" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.287037353516" Y="20.8182421875" />
                  <Point X="-1.305322631836" Y="20.84831640625" />
                  <Point X="-1.320364990234" Y="20.865037109375" />
                  <Point X="-1.328353149414" Y="20.872923828125" />
                  <Point X="-1.556905639648" Y="21.073359375" />
                  <Point X="-1.583204223633" Y="21.091208984375" />
                  <Point X="-1.616027587891" Y="21.10394140625" />
                  <Point X="-1.638140869141" Y="21.108048828125" />
                  <Point X="-1.649276977539" Y="21.109443359375" />
                  <Point X="-1.952616943359" Y="21.12932421875" />
                  <Point X="-1.984345947266" Y="21.127474609375" />
                  <Point X="-2.018122558594" Y="21.117599609375" />
                  <Point X="-2.038169799805" Y="21.1073984375" />
                  <Point X="-2.047864990234" Y="21.101720703125" />
                  <Point X="-2.300624267578" Y="20.93283203125" />
                  <Point X="-2.312788085938" Y="20.923177734375" />
                  <Point X="-2.335103515625" Y="20.900537109375" />
                  <Point X="-2.480148193359" Y="20.71151171875" />
                  <Point X="-2.801720947266" Y="20.910619140625" />
                  <Point X="-2.804124511719" Y="20.912470703125" />
                  <Point X="-3.104721923828" Y="21.143919921875" />
                  <Point X="-2.438787597656" Y="22.297353515625" />
                  <Point X="-2.423760986328" Y="22.323380859375" />
                  <Point X="-2.412853759766" Y="22.352373046875" />
                  <Point X="-2.406586425781" Y="22.38392578125" />
                  <Point X="-2.405616210938" Y="22.41511328125" />
                  <Point X="-2.414802978516" Y="22.44493359375" />
                  <Point X="-2.429404052734" Y="22.474123046875" />
                  <Point X="-2.447193115234" Y="22.498798828125" />
                  <Point X="-2.464154541016" Y="22.515759765625" />
                  <Point X="-2.489311767578" Y="22.533787109375" />
                  <Point X="-2.518141113281" Y="22.54800390625" />
                  <Point X="-2.547758789062" Y="22.55698828125" />
                  <Point X="-2.578692871094" Y="22.555974609375" />
                  <Point X="-2.610219482422" Y="22.549703125" />
                  <Point X="-2.639184570312" Y="22.53880078125" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-4.082861816406" Y="22.206140625" />
                  <Point X="-4.084587646484" Y="22.20903515625" />
                  <Point X="-4.306142089844" Y="22.580548828125" />
                  <Point X="-3.132246337891" Y="23.481310546875" />
                  <Point X="-3.105954833984" Y="23.501484375" />
                  <Point X="-3.084248046875" Y="23.52490625" />
                  <Point X="-3.066121826172" Y="23.55266015625" />
                  <Point X="-3.053695556641" Y="23.5807890625" />
                  <Point X="-3.046152099609" Y="23.6099140625" />
                  <Point X="-3.043348144531" Y="23.640349609375" />
                  <Point X="-3.045560058594" Y="23.672029296875" />
                  <Point X="-3.052677734375" Y="23.702046875" />
                  <Point X="-3.069039306641" Y="23.728203125" />
                  <Point X="-3.090417236328" Y="23.752474609375" />
                  <Point X="-3.113521728516" Y="23.771556640625" />
                  <Point X="-3.139456542969" Y="23.7868203125" />
                  <Point X="-3.168720703125" Y="23.79804296875" />
                  <Point X="-3.200605712891" Y="23.8045234375" />
                  <Point X="-3.231927001953" Y="23.805615234375" />
                  <Point X="-4.7321015625" Y="23.60811328125" />
                  <Point X="-4.834077148438" Y="24.00734375" />
                  <Point X="-4.834533691406" Y="24.010537109375" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-3.562825439453" Y="24.771564453125" />
                  <Point X="-3.532875488281" Y="24.77958984375" />
                  <Point X="-3.516926025391" Y="24.7854375" />
                  <Point X="-3.487168945312" Y="24.80091796875" />
                  <Point X="-3.459976318359" Y="24.819791015625" />
                  <Point X="-3.437174560547" Y="24.842150390625" />
                  <Point X="-3.417743652344" Y="24.8690078125" />
                  <Point X="-3.403981445312" Y="24.896533203125" />
                  <Point X="-3.394917236328" Y="24.92573828125" />
                  <Point X="-3.390649658203" Y="24.954484375" />
                  <Point X="-3.3908359375" Y="24.9847265625" />
                  <Point X="-3.395103271484" Y="25.012298828125" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.418660400391" Y="25.070029296875" />
                  <Point X="-3.438475585938" Y="25.096681640625" />
                  <Point X="-3.460548095703" Y="25.118044921875" />
                  <Point X="-3.487730224609" Y="25.13691015625" />
                  <Point X="-3.501921875" Y="25.145044921875" />
                  <Point X="-3.532875244141" Y="25.157849609375" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.824488769531" Y="25.976966796875" />
                  <Point X="-4.823565429688" Y="25.980375" />
                  <Point X="-4.703550292969" Y="26.423267578125" />
                  <Point X="-3.787134521484" Y="26.302619140625" />
                  <Point X="-3.765665527344" Y="26.29979296875" />
                  <Point X="-3.744997802734" Y="26.299341796875" />
                  <Point X="-3.723446044922" Y="26.301224609375" />
                  <Point X="-3.703094482422" Y="26.30527734375" />
                  <Point X="-3.701898193359" Y="26.305654296875" />
                  <Point X="-3.641711914063" Y="26.324630859375" />
                  <Point X="-3.622784667969" Y="26.332958984375" />
                  <Point X="-3.604040283203" Y="26.343779296875" />
                  <Point X="-3.587354980469" Y="26.35601171875" />
                  <Point X="-3.573713623047" Y="26.37156640625" />
                  <Point X="-3.561299316406" Y="26.389296875" />
                  <Point X="-3.551352050781" Y="26.4074296875" />
                  <Point X="-3.550854492188" Y="26.408630859375" />
                  <Point X="-3.526704589844" Y="26.46693359375" />
                  <Point X="-3.520914550781" Y="26.48680078125" />
                  <Point X="-3.517156494141" Y="26.508125" />
                  <Point X="-3.515805664062" Y="26.5287734375" />
                  <Point X="-3.518957763672" Y="26.54922265625" />
                  <Point X="-3.524566650391" Y="26.570134765625" />
                  <Point X="-3.532058105469" Y="26.589392578125" />
                  <Point X="-3.532650878906" Y="26.59053125" />
                  <Point X="-3.561790283203" Y="26.6465078125" />
                  <Point X="-3.573282958984" Y="26.66370703125" />
                  <Point X="-3.587196044922" Y="26.680287109375" />
                  <Point X="-3.602136230469" Y="26.69458984375" />
                  <Point X="-4.351859863281" Y="27.269875" />
                  <Point X="-4.081155517578" Y="27.733654296875" />
                  <Point X="-4.078716796875" Y="27.7367890625" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.219843994141" Y="27.85228515625" />
                  <Point X="-3.206658447266" Y="27.844671875" />
                  <Point X="-3.187704101563" Y="27.836333984375" />
                  <Point X="-3.167040771484" Y="27.82982421875" />
                  <Point X="-3.146833496094" Y="27.82580078125" />
                  <Point X="-3.145068603516" Y="27.82564453125" />
                  <Point X="-3.06124609375" Y="27.818310546875" />
                  <Point X="-3.040580078125" Y="27.818759765625" />
                  <Point X="-3.0191328125" Y="27.821580078125" />
                  <Point X="-2.999048095703" Y="27.826490234375" />
                  <Point X="-2.980501464844" Y="27.835630859375" />
                  <Point X="-2.962252929688" Y="27.847248046875" />
                  <Point X="-2.946134521484" Y="27.860171875" />
                  <Point X="-2.944851074219" Y="27.861453125" />
                  <Point X="-2.885353271484" Y="27.920951171875" />
                  <Point X="-2.872406738281" Y="27.937083984375" />
                  <Point X="-2.860777587891" Y="27.955337890625" />
                  <Point X="-2.851628662109" Y="27.973888671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843436035156" Y="28.036123046875" />
                  <Point X="-2.843587158203" Y="28.037849609375" />
                  <Point X="-2.850920654297" Y="28.121673828125" />
                  <Point X="-2.854955322266" Y="28.14195703125" />
                  <Point X="-2.861463623047" Y="28.162599609375" />
                  <Point X="-2.869794677734" Y="28.181533203125" />
                  <Point X="-3.183332763672" Y="28.724595703125" />
                  <Point X="-2.700613525391" Y="29.09469140625" />
                  <Point X="-2.696784423828" Y="29.096818359375" />
                  <Point X="-2.167037353516" Y="29.3911328125" />
                  <Point X="-2.047229736328" Y="29.234998046875" />
                  <Point X="-2.043193115234" Y="29.22973828125" />
                  <Point X="-2.028906005859" Y="29.214810546875" />
                  <Point X="-2.012354858398" Y="29.200916015625" />
                  <Point X="-1.995182006836" Y="29.189431640625" />
                  <Point X="-1.99319140625" Y="29.18839453125" />
                  <Point X="-1.899897216797" Y="29.139828125" />
                  <Point X="-1.880614746094" Y="29.132330078125" />
                  <Point X="-1.859698608398" Y="29.1267265625" />
                  <Point X="-1.839244140625" Y="29.123580078125" />
                  <Point X="-1.81859362793" Y="29.1249375" />
                  <Point X="-1.797270263672" Y="29.128703125" />
                  <Point X="-1.77732043457" Y="29.13453515625" />
                  <Point X="-1.775435668945" Y="29.135318359375" />
                  <Point X="-1.678280639648" Y="29.175560546875" />
                  <Point X="-1.660146484375" Y="29.185509765625" />
                  <Point X="-1.642416748047" Y="29.197923828125" />
                  <Point X="-1.626864257812" Y="29.2115625" />
                  <Point X="-1.614632568359" Y="29.228244140625" />
                  <Point X="-1.603810791016" Y="29.24698828125" />
                  <Point X="-1.595481323242" Y="29.26591796875" />
                  <Point X="-1.594829711914" Y="29.267984375" />
                  <Point X="-1.563201904297" Y="29.368294921875" />
                  <Point X="-1.559165527344" Y="29.388583984375" />
                  <Point X="-1.557278930664" Y="29.41014453125" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-0.949621643066" Y="29.809810546875" />
                  <Point X="-0.944988830566" Y="29.810353515625" />
                  <Point X="-0.294711181641" Y="29.886458984375" />
                  <Point X="-0.137803833008" Y="29.300873046875" />
                  <Point X="-0.133903427124" Y="29.28631640625" />
                  <Point X="-0.121132087708" Y="29.25812890625" />
                  <Point X="-0.103273948669" Y="29.231400390625" />
                  <Point X="-0.082115707397" Y="29.20880859375" />
                  <Point X="-0.05481804657" Y="29.194216796875" />
                  <Point X="-0.024379669189" Y="29.183884765625" />
                  <Point X="0.006155908585" Y="29.17884375" />
                  <Point X="0.036691532135" Y="29.183884765625" />
                  <Point X="0.067130065918" Y="29.194216796875" />
                  <Point X="0.09442755127" Y="29.20880859375" />
                  <Point X="0.11558581543" Y="29.2313984375" />
                  <Point X="0.133443954468" Y="29.258126953125" />
                  <Point X="0.146215454102" Y="29.28631640625" />
                  <Point X="0.307419219971" Y="29.8879375" />
                  <Point X="0.844041809082" Y="29.83173828125" />
                  <Point X="0.847887695312" Y="29.830810546875" />
                  <Point X="1.481030761719" Y="29.67794921875" />
                  <Point X="1.482095947266" Y="29.677564453125" />
                  <Point X="1.894659179688" Y="29.527921875" />
                  <Point X="1.897074462891" Y="29.52679296875" />
                  <Point X="2.294562011719" Y="29.340900390625" />
                  <Point X="2.296956787109" Y="29.339505859375" />
                  <Point X="2.680973388672" Y="29.11577734375" />
                  <Point X="2.683198242188" Y="29.1141953125" />
                  <Point X="2.943259765625" Y="28.92925390625" />
                  <Point X="2.164973144531" Y="27.581220703125" />
                  <Point X="2.147581298828" Y="27.55109765625" />
                  <Point X="2.141544433594" Y="27.538619140625" />
                  <Point X="2.132643554688" Y="27.51443359375" />
                  <Point X="2.111607177734" Y="27.435767578125" />
                  <Point X="2.108539306641" Y="27.416685546875" />
                  <Point X="2.107370605469" Y="27.3963828125" />
                  <Point X="2.107896728516" Y="27.37955078125" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121434570313" Y="27.289623046875" />
                  <Point X="2.1296875" Y="27.2675546875" />
                  <Point X="2.140038085938" Y="27.24751953125" />
                  <Point X="2.140938964844" Y="27.24619140625" />
                  <Point X="2.183029541016" Y="27.184162109375" />
                  <Point X="2.195432128906" Y="27.169375" />
                  <Point X="2.210009033203" Y="27.155208984375" />
                  <Point X="2.222874755859" Y="27.1447265625" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.304979980469" Y="27.09226171875" />
                  <Point X="2.327104492188" Y="27.083990234375" />
                  <Point X="2.349039550781" Y="27.07865234375" />
                  <Point X="2.350405029297" Y="27.07848828125" />
                  <Point X="2.418387451172" Y="27.070291015625" />
                  <Point X="2.437930419922" Y="27.069958984375" />
                  <Point X="2.458454589844" Y="27.07173046875" />
                  <Point X="2.474828125" Y="27.074603515625" />
                  <Point X="2.553493896484" Y="27.095640625" />
                  <Point X="2.565286865234" Y="27.099638671875" />
                  <Point X="2.588534423828" Y="27.110146484375" />
                  <Point X="3.967326171875" Y="27.90619140625" />
                  <Point X="4.12326953125" Y="27.68946484375" />
                  <Point X="4.124506835938" Y="27.687419921875" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.253682128906" Y="26.686021484375" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.220309082031" Y="26.65910546875" />
                  <Point X="3.202807373047" Y="26.64010546875" />
                  <Point X="3.146191650391" Y="26.56624609375" />
                  <Point X="3.135984863281" Y="26.54964453125" />
                  <Point X="3.127080566406" Y="26.531140625" />
                  <Point X="3.121195068359" Y="26.515533203125" />
                  <Point X="3.100105712891" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739746094" Y="26.371765625" />
                  <Point X="3.098096435547" Y="26.370037109375" />
                  <Point X="3.115408691406" Y="26.2861328125" />
                  <Point X="3.121277832031" Y="26.26756640625" />
                  <Point X="3.129446044922" Y="26.248712890625" />
                  <Point X="3.137252929688" Y="26.234263671875" />
                  <Point X="3.184340576172" Y="26.162693359375" />
                  <Point X="3.198896972656" Y="26.1454453125" />
                  <Point X="3.21614453125" Y="26.129353515625" />
                  <Point X="3.234458496094" Y="26.115970703125" />
                  <Point X="3.235755615234" Y="26.1152421875" />
                  <Point X="3.303987304688" Y="26.07683203125" />
                  <Point X="3.322101318359" Y="26.068990234375" />
                  <Point X="3.341977783203" Y="26.0627421875" />
                  <Point X="3.358018798828" Y="26.0591875" />
                  <Point X="3.450278808594" Y="26.046994140625" />
                  <Point X="3.462697753906" Y="26.04617578125" />
                  <Point X="3.488203857422" Y="26.046984375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845936035156" Y="25.93280859375" />
                  <Point X="4.846325683594" Y="25.930306640625" />
                  <Point X="4.890864746094" Y="25.64423828125" />
                  <Point X="3.742819091797" Y="25.33662109375" />
                  <Point X="3.716577148438" Y="25.329587890625" />
                  <Point X="3.703175537109" Y="25.3248984375" />
                  <Point X="3.679679199219" Y="25.31398828125" />
                  <Point X="3.589036376953" Y="25.261595703125" />
                  <Point X="3.573131591797" Y="25.250056640625" />
                  <Point X="3.557698730469" Y="25.236208984375" />
                  <Point X="3.546409912109" Y="25.224150390625" />
                  <Point X="3.492024414062" Y="25.154849609375" />
                  <Point X="3.480302001953" Y="25.135572265625" />
                  <Point X="3.470529541016" Y="25.11411328125" />
                  <Point X="3.463684814453" Y="25.092625" />
                  <Point X="3.463307128906" Y="25.090654296875" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443498291016" Y="24.9764140625" />
                  <Point X="3.443871826172" Y="24.95565625" />
                  <Point X="3.445552001953" Y="24.93949609375" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526123047" Y="24.823337890625" />
                  <Point X="3.480299804688" Y="24.801873046875" />
                  <Point X="3.492022216797" Y="24.78259375" />
                  <Point X="3.493142578125" Y="24.781166015625" />
                  <Point X="3.547528076172" Y="24.711865234375" />
                  <Point X="3.56140625" Y="24.697564453125" />
                  <Point X="3.577585449219" Y="24.68406640625" />
                  <Point X="3.590901611328" Y="24.674765625" />
                  <Point X="3.681544189453" Y="24.62237109375" />
                  <Point X="3.692709960938" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.89147265625" Y="24.293037109375" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.854522949219" Y="24.049083984375" />
                  <Point X="4.801173828125" Y="23.815302734375" />
                  <Point X="3.454813964844" Y="23.992552734375" />
                  <Point X="3.424380859375" Y="23.99655859375" />
                  <Point X="3.405106445312" Y="23.99712109375" />
                  <Point X="3.384297119141" Y="23.995611328125" />
                  <Point X="3.370994628906" Y="23.993693359375" />
                  <Point X="3.193095214844" Y="23.95502734375" />
                  <Point X="3.161443847656" Y="23.9418125" />
                  <Point X="3.131401611328" Y="23.922255859375" />
                  <Point X="3.110181884766" Y="23.903375" />
                  <Point X="3.002653076172" Y="23.77405078125" />
                  <Point X="2.986842529297" Y="23.74691796875" />
                  <Point X="2.975181640625" Y="23.71608203125" />
                  <Point X="2.969439941406" Y="23.69118359375" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.955109130859" Y="23.491515625" />
                  <Point X="2.963745849609" Y="23.458400390625" />
                  <Point X="2.972285644531" Y="23.44008203125" />
                  <Point X="2.978479003906" Y="23.42884765625" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086934326172" Y="23.262759765625" />
                  <Point X="3.110629394531" Y="23.23908984375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.124817871094" Y="22.250224609375" />
                  <Point X="4.12377734375" Y="22.24874609375" />
                  <Point X="4.028980957031" Y="22.114052734375" />
                  <Point X="2.828058837891" Y="22.80740625" />
                  <Point X="2.800954101562" Y="22.8230546875" />
                  <Point X="2.782955810547" Y="22.8310859375" />
                  <Point X="2.762473876953" Y="22.83777734375" />
                  <Point X="2.749855712891" Y="22.840962890625" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.505991455078" Y="22.88091796875" />
                  <Point X="2.472045166016" Y="22.8751171875" />
                  <Point X="2.452744628906" Y="22.8680390625" />
                  <Point X="2.441209228516" Y="22.862916015625" />
                  <Point X="2.265314941406" Y="22.77034375" />
                  <Point X="2.240001464844" Y="22.75098046875" />
                  <Point X="2.217134765625" Y="22.7263984375" />
                  <Point X="2.202624755859" Y="22.7059375" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.098733398438" Y="22.499890625" />
                  <Point X="2.094059326172" Y="22.465578125" />
                  <Point X="2.095059570312" Y="22.44471484375" />
                  <Point X="2.096463134766" Y="22.432380859375" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138986816406" Y="22.204853515625" />
                  <Point X="2.151819580078" Y="22.173919921875" />
                  <Point X="2.861283447266" Y="20.94509375" />
                  <Point X="2.78184375" Y="20.8883515625" />
                  <Point X="2.780694091797" Y="20.887607421875" />
                  <Point X="2.701764160156" Y="20.836517578125" />
                  <Point X="1.779229614258" Y="22.0387890625" />
                  <Point X="1.758546020508" Y="22.065744140625" />
                  <Point X="1.744694091797" Y="22.0803046875" />
                  <Point X="1.727763793945" Y="22.09469140625" />
                  <Point X="1.717621704102" Y="22.102208984375" />
                  <Point X="1.508800048828" Y="22.2364609375" />
                  <Point X="1.47974987793" Y="22.2503515625" />
                  <Point X="1.445816040039" Y="22.2579921875" />
                  <Point X="1.424647460938" Y="22.258783203125" />
                  <Point X="1.412394897461" Y="22.25844921875" />
                  <Point X="1.184013061523" Y="22.23743359375" />
                  <Point X="1.152791259766" Y="22.229037109375" />
                  <Point X="1.121768066406" Y="22.21466796875" />
                  <Point X="1.100957519531" Y="22.20151171875" />
                  <Point X="0.924611694336" Y="22.054884765625" />
                  <Point X="0.902615600586" Y="22.031361328125" />
                  <Point X="0.885034912109" Y="22.001080078125" />
                  <Point X="0.877830505371" Y="21.9807578125" />
                  <Point X="0.874538146973" Y="21.969193359375" />
                  <Point X="0.821810119629" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710373046875" />
                  <Point X="0.81974230957" Y="21.676880859375" />
                  <Point X="1.022065307617" Y="20.140083984375" />
                  <Point X="0.975671081543" Y="20.129916015625" />
                  <Point X="0.974598999023" Y="20.129720703125" />
                  <Point X="0.929315856934" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058428833008" Y="20.247365234375" />
                  <Point X="-1.14124609375" Y="20.268673828125" />
                  <Point X="-1.121749267578" Y="20.416765625" />
                  <Point X="-1.121079833984" Y="20.423947265625" />
                  <Point X="-1.119583496094" Y="20.451150390625" />
                  <Point X="-1.119729492188" Y="20.463779296875" />
                  <Point X="-1.121484008789" Y="20.486203125" />
                  <Point X="-1.124555908203" Y="20.50844921875" />
                  <Point X="-1.183861572266" Y="20.806599609375" />
                  <Point X="-1.186859985352" Y="20.817953125" />
                  <Point X="-1.196861083984" Y="20.84812890625" />
                  <Point X="-1.205863647461" Y="20.867595703125" />
                  <Point X="-1.224148925781" Y="20.897669921875" />
                  <Point X="-1.234696533203" Y="20.911853515625" />
                  <Point X="-1.249738891602" Y="20.92857421875" />
                  <Point X="-1.265715209961" Y="20.94434765625" />
                  <Point X="-1.494267700195" Y="21.144783203125" />
                  <Point X="-1.50355456543" Y="21.151962890625" />
                  <Point X="-1.529853149414" Y="21.1698125" />
                  <Point X="-1.548847412109" Y="21.179779296875" />
                  <Point X="-1.581670776367" Y="21.19251171875" />
                  <Point X="-1.598678588867" Y="21.19734375" />
                  <Point X="-1.620791870117" Y="21.201451171875" />
                  <Point X="-1.643063964844" Y="21.204240234375" />
                  <Point X="-1.946403930664" Y="21.22412109375" />
                  <Point X="-1.958145507812" Y="21.2241640625" />
                  <Point X="-1.989874511719" Y="21.222314453125" />
                  <Point X="-2.011004394531" Y="21.218658203125" />
                  <Point X="-2.044781005859" Y="21.208783203125" />
                  <Point X="-2.061206787109" Y="21.202267578125" />
                  <Point X="-2.08125390625" Y="21.19206640625" />
                  <Point X="-2.100644287109" Y="21.1807109375" />
                  <Point X="-2.353403564453" Y="21.011822265625" />
                  <Point X="-2.35968359375" Y="21.0072421875" />
                  <Point X="-2.380447265625" Y="20.989865234375" />
                  <Point X="-2.402762695312" Y="20.967224609375" />
                  <Point X="-2.410471923828" Y="20.958369140625" />
                  <Point X="-2.503201416016" Y="20.837521484375" />
                  <Point X="-2.747589599609" Y="20.98883984375" />
                  <Point X="-2.980862548828" Y="21.168451171875" />
                  <Point X="-2.356515136719" Y="22.249853515625" />
                  <Point X="-2.341488525391" Y="22.275880859375" />
                  <Point X="-2.334845214844" Y="22.2899296875" />
                  <Point X="-2.323937988281" Y="22.318921875" />
                  <Point X="-2.319674072266" Y="22.333865234375" />
                  <Point X="-2.313406738281" Y="22.36541796875" />
                  <Point X="-2.311632324219" Y="22.38097265625" />
                  <Point X="-2.310662109375" Y="22.41216015625" />
                  <Point X="-2.314826904297" Y="22.44308203125" />
                  <Point X="-2.324013671875" Y="22.47290234375" />
                  <Point X="-2.32983984375" Y="22.48743359375" />
                  <Point X="-2.344440917969" Y="22.516623046875" />
                  <Point X="-2.352341552734" Y="22.529677734375" />
                  <Point X="-2.370130615234" Y="22.554353515625" />
                  <Point X="-2.380019042969" Y="22.565974609375" />
                  <Point X="-2.39698046875" Y="22.582935546875" />
                  <Point X="-2.408819335938" Y="22.59298046875" />
                  <Point X="-2.4339765625" Y="22.6110078125" />
                  <Point X="-2.447294921875" Y="22.618990234375" />
                  <Point X="-2.476124267578" Y="22.63320703125" />
                  <Point X="-2.490564208984" Y="22.6389140625" />
                  <Point X="-2.520181884766" Y="22.6478984375" />
                  <Point X="-2.550870117188" Y="22.6519375" />
                  <Point X="-2.581804199219" Y="22.650923828125" />
                  <Point X="-2.597227783203" Y="22.6491484375" />
                  <Point X="-2.628754394531" Y="22.642876953125" />
                  <Point X="-2.643685058594" Y="22.63861328125" />
                  <Point X="-2.672650146484" Y="22.6277109375" />
                  <Point X="-2.686684570312" Y="22.621072265625" />
                  <Point X="-3.793087402344" Y="21.9822890625" />
                  <Point X="-4.004020751953" Y="22.259412109375" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.0744140625" Y="23.40594140625" />
                  <Point X="-3.048122558594" Y="23.426115234375" />
                  <Point X="-3.036277099609" Y="23.436908203125" />
                  <Point X="-3.0145703125" Y="23.460330078125" />
                  <Point X="-3.004708984375" Y="23.472958984375" />
                  <Point X="-2.986582763672" Y="23.500712890625" />
                  <Point X="-2.979223388672" Y="23.514271484375" />
                  <Point X="-2.966797119141" Y="23.542400390625" />
                  <Point X="-2.961730224609" Y="23.556970703125" />
                  <Point X="-2.954186767578" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.60119921875" />
                  <Point X="-2.948748779297" Y="23.631634765625" />
                  <Point X="-2.948578857422" Y="23.646966796875" />
                  <Point X="-2.950790771484" Y="23.678646484375" />
                  <Point X="-2.953123046875" Y="23.693947265625" />
                  <Point X="-2.960240722656" Y="23.72396484375" />
                  <Point X="-2.972137207031" Y="23.752427734375" />
                  <Point X="-2.988498779297" Y="23.778583984375" />
                  <Point X="-2.997749267578" Y="23.790994140625" />
                  <Point X="-3.019127197266" Y="23.815265625" />
                  <Point X="-3.029921630859" Y="23.82572265625" />
                  <Point X="-3.053026123047" Y="23.8448046875" />
                  <Point X="-3.065336181641" Y="23.8534296875" />
                  <Point X="-3.091270996094" Y="23.868693359375" />
                  <Point X="-3.105440185547" Y="23.875521484375" />
                  <Point X="-3.134704345703" Y="23.886744140625" />
                  <Point X="-3.149799316406" Y="23.891138671875" />
                  <Point X="-3.181684326172" Y="23.897619140625" />
                  <Point X="-3.197296142578" Y="23.89946484375" />
                  <Point X="-3.228617431641" Y="23.900556640625" />
                  <Point X="-3.244326904297" Y="23.899802734375" />
                  <Point X="-4.660919921875" Y="23.7133046875" />
                  <Point X="-4.740760742188" Y="24.025876953125" />
                  <Point X="-4.786451660156" Y="24.345341796875" />
                  <Point X="-3.538237548828" Y="24.67980078125" />
                  <Point X="-3.508287597656" Y="24.687826171875" />
                  <Point X="-3.500173583984" Y="24.690396484375" />
                  <Point X="-3.473082275391" Y="24.70116015625" />
                  <Point X="-3.443325195312" Y="24.716640625" />
                  <Point X="-3.433002197266" Y="24.722873046875" />
                  <Point X="-3.405809570312" Y="24.74174609375" />
                  <Point X="-3.393462402344" Y="24.7519609375" />
                  <Point X="-3.370660644531" Y="24.7743203125" />
                  <Point X="-3.360206054688" Y="24.78646484375" />
                  <Point X="-3.340775146484" Y="24.813322265625" />
                  <Point X="-3.332772460938" Y="24.8265234375" />
                  <Point X="-3.319010253906" Y="24.854048828125" />
                  <Point X="-3.313250732422" Y="24.868373046875" />
                  <Point X="-3.304186523438" Y="24.897578125" />
                  <Point X="-3.300947021484" Y="24.911787109375" />
                  <Point X="-3.296679443359" Y="24.940533203125" />
                  <Point X="-3.295651367188" Y="24.9550703125" />
                  <Point X="-3.295837646484" Y="24.9853125" />
                  <Point X="-3.296953613281" Y="24.999255859375" />
                  <Point X="-3.301220947266" Y="25.026828125" />
                  <Point X="-3.304372314453" Y="25.04045703125" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.319473144531" Y="25.0845390625" />
                  <Point X="-3.333965820312" Y="25.1130625" />
                  <Point X="-3.342422119141" Y="25.1267109375" />
                  <Point X="-3.362237304688" Y="25.15336328125" />
                  <Point X="-3.372406005859" Y="25.1649453125" />
                  <Point X="-3.394478515625" Y="25.18630859375" />
                  <Point X="-3.406382324219" Y="25.19608984375" />
                  <Point X="-3.433564453125" Y="25.214955078125" />
                  <Point X="-3.440486572266" Y="25.219330078125" />
                  <Point X="-3.465607177734" Y="25.232830078125" />
                  <Point X="-3.496560546875" Y="25.245634765625" />
                  <Point X="-3.508287353516" Y="25.24961328125" />
                  <Point X="-4.7854453125" Y="25.591826171875" />
                  <Point X="-4.731331054688" Y="25.957525390625" />
                  <Point X="-4.633585449219" Y="26.318236328125" />
                  <Point X="-3.799534423828" Y="26.208431640625" />
                  <Point X="-3.778065429688" Y="26.20560546875" />
                  <Point X="-3.767738769531" Y="26.20481640625" />
                  <Point X="-3.747071044922" Y="26.204365234375" />
                  <Point X="-3.736729980469" Y="26.204703125" />
                  <Point X="-3.715178222656" Y="26.2065859375" />
                  <Point X="-3.704892333984" Y="26.2080546875" />
                  <Point X="-3.684540771484" Y="26.212107421875" />
                  <Point X="-3.673331298828" Y="26.21505078125" />
                  <Point X="-3.613145019531" Y="26.23402734375" />
                  <Point X="-3.603451171875" Y="26.23767578125" />
                  <Point X="-3.584523925781" Y="26.24600390625" />
                  <Point X="-3.575290527344" Y="26.25068359375" />
                  <Point X="-3.556546142578" Y="26.26150390625" />
                  <Point X="-3.54787109375" Y="26.2671640625" />
                  <Point X="-3.531185791016" Y="26.279396484375" />
                  <Point X="-3.515930908203" Y="26.293373046875" />
                  <Point X="-3.502289550781" Y="26.308927734375" />
                  <Point X="-3.495892822266" Y="26.317078125" />
                  <Point X="-3.483478515625" Y="26.33480859375" />
                  <Point X="-3.478008789062" Y="26.34360546875" />
                  <Point X="-3.468061523438" Y="26.36173828125" />
                  <Point X="-3.463086425781" Y="26.372275390625" />
                  <Point X="-3.438936523438" Y="26.430578125" />
                  <Point X="-3.435499023438" Y="26.440353515625" />
                  <Point X="-3.429708984375" Y="26.460220703125" />
                  <Point X="-3.427356445313" Y="26.4703125" />
                  <Point X="-3.423598388672" Y="26.49163671875" />
                  <Point X="-3.422359130859" Y="26.501923828125" />
                  <Point X="-3.421008300781" Y="26.522572265625" />
                  <Point X="-3.421914550781" Y="26.54324609375" />
                  <Point X="-3.425066650391" Y="26.5636953125" />
                  <Point X="-3.427200927734" Y="26.57383203125" />
                  <Point X="-3.432809814453" Y="26.594744140625" />
                  <Point X="-3.436029785156" Y="26.604576171875" />
                  <Point X="-3.443521240234" Y="26.623833984375" />
                  <Point X="-3.448385498047" Y="26.6343984375" />
                  <Point X="-3.477524902344" Y="26.690375" />
                  <Point X="-3.482801757812" Y="26.6992890625" />
                  <Point X="-3.494294433594" Y="26.71648828125" />
                  <Point X="-3.500510253906" Y="26.7247734375" />
                  <Point X="-3.514423339844" Y="26.741353515625" />
                  <Point X="-3.521500732422" Y="26.74891015625" />
                  <Point X="-3.536440917969" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.22761328125" Y="27.294283203125" />
                  <Point X="-4.002290527344" Y="27.6803125" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.267343994141" Y="27.770013671875" />
                  <Point X="-3.244910888672" Y="27.757712890625" />
                  <Point X="-3.225956542969" Y="27.749375" />
                  <Point X="-3.216249755859" Y="27.745724609375" />
                  <Point X="-3.195586425781" Y="27.73921484375" />
                  <Point X="-3.185591796875" Y="27.73665234375" />
                  <Point X="-3.165384521484" Y="27.73262890625" />
                  <Point X="-3.153348876953" Y="27.731005859375" />
                  <Point X="-3.069526367188" Y="27.723671875" />
                  <Point X="-3.059181640625" Y="27.72333203125" />
                  <Point X="-3.038515625" Y="27.72378125" />
                  <Point X="-3.028194335938" Y="27.7245703125" />
                  <Point X="-3.006747070312" Y="27.727390625" />
                  <Point X="-2.996572265625" Y="27.729296875" />
                  <Point X="-2.976487548828" Y="27.73420703125" />
                  <Point X="-2.957051269531" Y="27.74127734375" />
                  <Point X="-2.938504638672" Y="27.75041796875" />
                  <Point X="-2.929484375" Y="27.7554921875" />
                  <Point X="-2.911235839844" Y="27.767109375" />
                  <Point X="-2.902825439453" Y="27.773130859375" />
                  <Point X="-2.88670703125" Y="27.7860546875" />
                  <Point X="-2.87767578125" Y="27.79427734375" />
                  <Point X="-2.818177978516" Y="27.853775390625" />
                  <Point X="-2.811260986328" Y="27.8614921875" />
                  <Point X="-2.798314453125" Y="27.877625" />
                  <Point X="-2.792284912109" Y="27.886041015625" />
                  <Point X="-2.780655761719" Y="27.904294921875" />
                  <Point X="-2.775575927734" Y="27.913318359375" />
                  <Point X="-2.766427001953" Y="27.931869140625" />
                  <Point X="-2.759351318359" Y="27.95130859375" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909423828" Y="28.013369140625" />
                  <Point X="-2.748458496094" Y="28.034052734375" />
                  <Point X="-2.748948974609" Y="28.0461328125" />
                  <Point X="-2.756282470703" Y="28.12995703125" />
                  <Point X="-2.75774609375" Y="28.14020703125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764351806641" Y="28.1705234375" />
                  <Point X="-2.770860107422" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200861328125" />
                  <Point X="-2.782840332031" Y="28.219794921875" />
                  <Point X="-2.787522216797" Y="28.229033203125" />
                  <Point X="-3.05938671875" Y="28.6999140625" />
                  <Point X="-2.648363769531" Y="29.015041015625" />
                  <Point X="-2.192524414062" Y="29.268294921875" />
                  <Point X="-2.122598144531" Y="29.177166015625" />
                  <Point X="-2.111824707031" Y="29.164052734375" />
                  <Point X="-2.097537597656" Y="29.149125" />
                  <Point X="-2.089987548828" Y="29.14205078125" />
                  <Point X="-2.073436279297" Y="29.12815625" />
                  <Point X="-2.065165283203" Y="29.121947265625" />
                  <Point X="-2.047992431641" Y="29.110462890625" />
                  <Point X="-2.037057861328" Y="29.10412890625" />
                  <Point X="-1.943763671875" Y="29.0555625" />
                  <Point X="-1.934326904297" Y="29.051287109375" />
                  <Point X="-1.915044433594" Y="29.0437890625" />
                  <Point X="-1.905198730469" Y="29.04056640625" />
                  <Point X="-1.884282592773" Y="29.034962890625" />
                  <Point X="-1.874142456055" Y="29.032830078125" />
                  <Point X="-1.853687988281" Y="29.02968359375" />
                  <Point X="-1.833013061523" Y="29.02878515625" />
                  <Point X="-1.812362426758" Y="29.030142578125" />
                  <Point X="-1.802072631836" Y="29.031384765625" />
                  <Point X="-1.780749267578" Y="29.035150390625" />
                  <Point X="-1.770614135742" Y="29.03751953125" />
                  <Point X="-1.750664306641" Y="29.0433515625" />
                  <Point X="-1.739081298828" Y="29.047548828125" />
                  <Point X="-1.641926269531" Y="29.087791015625" />
                  <Point X="-1.632584960938" Y="29.092271484375" />
                  <Point X="-1.614450805664" Y="29.102220703125" />
                  <Point X="-1.605657958984" Y="29.107689453125" />
                  <Point X="-1.587928222656" Y="29.120103515625" />
                  <Point X="-1.579780151367" Y="29.126498046875" />
                  <Point X="-1.564227539062" Y="29.14013671875" />
                  <Point X="-1.550252441406" Y="29.15538671875" />
                  <Point X="-1.538020751953" Y="29.172068359375" />
                  <Point X="-1.532359863281" Y="29.180744140625" />
                  <Point X="-1.521538085938" Y="29.19948828125" />
                  <Point X="-1.516856567383" Y="29.2087265625" />
                  <Point X="-1.508527099609" Y="29.22765625" />
                  <Point X="-1.504227539062" Y="29.2394140625" />
                  <Point X="-1.472599853516" Y="29.339724609375" />
                  <Point X="-1.470027832031" Y="29.3497578125" />
                  <Point X="-1.465991455078" Y="29.370046875" />
                  <Point X="-1.464527099609" Y="29.380302734375" />
                  <Point X="-1.462640625" Y="29.40186328125" />
                  <Point X="-1.462301513672" Y="29.412216796875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354296875" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.931157287598" Y="29.71632421875" />
                  <Point X="-0.365222473145" Y="29.78255859375" />
                  <Point X="-0.229566741943" Y="29.27628515625" />
                  <Point X="-0.220435806274" Y="29.247109375" />
                  <Point X="-0.207664489746" Y="29.218921875" />
                  <Point X="-0.200123504639" Y="29.2053515625" />
                  <Point X="-0.182265274048" Y="29.178623046875" />
                  <Point X="-0.172612960815" Y="29.1664609375" />
                  <Point X="-0.151454711914" Y="29.143869140625" />
                  <Point X="-0.12690057373" Y="29.12502734375" />
                  <Point X="-0.099602859497" Y="29.110435546875" />
                  <Point X="-0.085353782654" Y="29.1042578125" />
                  <Point X="-0.054915370941" Y="29.09392578125" />
                  <Point X="-0.039853477478" Y="29.090154296875" />
                  <Point X="-0.009317863464" Y="29.08511328125" />
                  <Point X="0.021629735947" Y="29.08511328125" />
                  <Point X="0.052165351868" Y="29.090154296875" />
                  <Point X="0.067227088928" Y="29.09392578125" />
                  <Point X="0.097665657043" Y="29.1042578125" />
                  <Point X="0.111915176392" Y="29.110435546875" />
                  <Point X="0.139212600708" Y="29.12502734375" />
                  <Point X="0.163763763428" Y="29.1438671875" />
                  <Point X="0.184922012329" Y="29.16645703125" />
                  <Point X="0.194577148438" Y="29.17862109375" />
                  <Point X="0.212435379028" Y="29.205349609375" />
                  <Point X="0.219977096558" Y="29.218921875" />
                  <Point X="0.232748718262" Y="29.247111328125" />
                  <Point X="0.23797845459" Y="29.261728515625" />
                  <Point X="0.378190368652" Y="29.7850078125" />
                  <Point X="0.827867675781" Y="29.7379140625" />
                  <Point X="1.453625244141" Y="29.5868359375" />
                  <Point X="1.858269165039" Y="29.440064453125" />
                  <Point X="2.250439208984" Y="29.25666015625" />
                  <Point X="2.629437988281" Y="29.035853515625" />
                  <Point X="2.817779785156" Y="28.901916015625" />
                  <Point X="2.082700683594" Y="27.628720703125" />
                  <Point X="2.065308837891" Y="27.59859765625" />
                  <Point X="2.062063232422" Y="27.59246875" />
                  <Point X="2.052390380859" Y="27.5714296875" />
                  <Point X="2.043489379883" Y="27.547244140625" />
                  <Point X="2.040868286133" Y="27.538974609375" />
                  <Point X="2.01983190918" Y="27.46030859375" />
                  <Point X="2.017811767578" Y="27.45084765625" />
                  <Point X="2.013696289062" Y="27.42214453125" />
                  <Point X="2.012527587891" Y="27.401841796875" />
                  <Point X="2.012416992188" Y="27.3934140625" />
                  <Point X="2.013579956055" Y="27.368177734375" />
                  <Point X="2.021782348633" Y="27.300154296875" />
                  <Point X="2.023797851563" Y="27.289044921875" />
                  <Point X="2.029133300781" Y="27.267140625" />
                  <Point X="2.03245324707" Y="27.256345703125" />
                  <Point X="2.040706176758" Y="27.23427734375" />
                  <Point X="2.045285522461" Y="27.223951171875" />
                  <Point X="2.055635986328" Y="27.203916015625" />
                  <Point X="2.062328369141" Y="27.192849609375" />
                  <Point X="2.104418945313" Y="27.1308203125" />
                  <Point X="2.110242431641" Y="27.12311328125" />
                  <Point X="2.129224121094" Y="27.10124609375" />
                  <Point X="2.143801025391" Y="27.087080078125" />
                  <Point X="2.150002685547" Y="27.08155859375" />
                  <Point X="2.169532470703" Y="27.066115234375" />
                  <Point X="2.231563232422" Y="27.0240234375" />
                  <Point X="2.241294433594" Y="27.018236328125" />
                  <Point X="2.261368896484" Y="27.00786328125" />
                  <Point X="2.271712158203" Y="27.00327734375" />
                  <Point X="2.293836669922" Y="26.995005859375" />
                  <Point X="2.304641845703" Y="26.99168359375" />
                  <Point X="2.326576904297" Y="26.986345703125" />
                  <Point X="2.339072509766" Y="26.98416796875" />
                  <Point X="2.407014892578" Y="26.975974609375" />
                  <Point X="2.416773681641" Y="26.9753046875" />
                  <Point X="2.446099609375" Y="26.975310546875" />
                  <Point X="2.466623779297" Y="26.97708203125" />
                  <Point X="2.474873291016" Y="26.97816015625" />
                  <Point X="2.499370849609" Y="26.982828125" />
                  <Point X="2.578036621094" Y="27.003865234375" />
                  <Point X="2.583995605469" Y="27.005669921875" />
                  <Point X="2.604415283203" Y="27.0130703125" />
                  <Point X="2.627662841797" Y="27.023578125" />
                  <Point X="2.636034423828" Y="27.027875" />
                  <Point X="3.940403564453" Y="27.780951171875" />
                  <Point X="4.043949462891" Y="27.637046875" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.195849853516" Y="26.761390625" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.167538330078" Y="26.739337890625" />
                  <Point X="3.150435546875" Y="26.72346875" />
                  <Point X="3.132933837891" Y="26.70446875" />
                  <Point X="3.127409912109" Y="26.697900390625" />
                  <Point X="3.070794189453" Y="26.624041015625" />
                  <Point X="3.065263427734" Y="26.616001953125" />
                  <Point X="3.050380615234" Y="26.590837890625" />
                  <Point X="3.041476318359" Y="26.572333984375" />
                  <Point X="3.038190673828" Y="26.56466015625" />
                  <Point X="3.029705322266" Y="26.541119140625" />
                  <Point X="3.008615966797" Y="26.46570703125" />
                  <Point X="3.006224853516" Y="26.45466015625" />
                  <Point X="3.002771484375" Y="26.432361328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003078125" Y="26.36375390625" />
                  <Point X="3.005056640625" Y="26.350837890625" />
                  <Point X="3.022368896484" Y="26.26693359375" />
                  <Point X="3.024826904297" Y="26.257498046875" />
                  <Point X="3.034107177734" Y="26.22980078125" />
                  <Point X="3.042275390625" Y="26.210947265625" />
                  <Point X="3.045865478516" Y="26.2035546875" />
                  <Point X="3.057889160156" Y="26.182048828125" />
                  <Point X="3.104976806641" Y="26.110478515625" />
                  <Point X="3.111739746094" Y="26.101421875" />
                  <Point X="3.126296142578" Y="26.084173828125" />
                  <Point X="3.134089599609" Y="26.075982421875" />
                  <Point X="3.151337158203" Y="26.059890625" />
                  <Point X="3.160094238281" Y="26.052650390625" />
                  <Point X="3.178408203125" Y="26.039267578125" />
                  <Point X="3.189153320312" Y="26.03245703125" />
                  <Point X="3.257385009766" Y="25.994046875" />
                  <Point X="3.266245361328" Y="25.989650390625" />
                  <Point X="3.293613037109" Y="25.97836328125" />
                  <Point X="3.313489501953" Y="25.972115234375" />
                  <Point X="3.321424316406" Y="25.9699921875" />
                  <Point X="3.345571533203" Y="25.965005859375" />
                  <Point X="3.437831542969" Y="25.9528125" />
                  <Point X="3.444032226562" Y="25.95219921875" />
                  <Point X="3.465708007812" Y="25.95122265625" />
                  <Point X="3.491214111328" Y="25.95203125" />
                  <Point X="3.500603759766" Y="25.952796875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752683105469" Y="25.914234375" />
                  <Point X="4.783870605469" Y="25.713921875" />
                  <Point X="3.718231445312" Y="25.428384765625" />
                  <Point X="3.691989501953" Y="25.4213515625" />
                  <Point X="3.685200439453" Y="25.419255859375" />
                  <Point X="3.663166503906" Y="25.4110625" />
                  <Point X="3.639670166016" Y="25.40015234375" />
                  <Point X="3.632138427734" Y="25.396236328125" />
                  <Point X="3.541495605469" Y="25.34384375" />
                  <Point X="3.533248779297" Y="25.338490234375" />
                  <Point X="3.509686035156" Y="25.320765625" />
                  <Point X="3.494253173828" Y="25.30691796875" />
                  <Point X="3.488346435547" Y="25.301134765625" />
                  <Point X="3.471675537109" Y="25.28280078125" />
                  <Point X="3.417290039063" Y="25.2135" />
                  <Point X="3.410853759766" Y="25.204208984375" />
                  <Point X="3.399131347656" Y="25.184931640625" />
                  <Point X="3.393845214844" Y="25.1749453125" />
                  <Point X="3.384072753906" Y="25.153486328125" />
                  <Point X="3.380010742188" Y="25.142947265625" />
                  <Point X="3.373166015625" Y="25.121458984375" />
                  <Point X="3.370002685547" Y="25.1085234375" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350526367188" Y="25.004115234375" />
                  <Point X="3.348513671875" Y="24.974705078125" />
                  <Point X="3.348887207031" Y="24.953947265625" />
                  <Point X="3.349381103516" Y="24.94583203125" />
                  <Point X="3.352247558594" Y="24.921626953125" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.81601171875" />
                  <Point X="3.380004394531" Y="24.794513671875" />
                  <Point X="3.384067138672" Y="24.783970703125" />
                  <Point X="3.393840820312" Y="24.762505859375" />
                  <Point X="3.399126953125" Y="24.752517578125" />
                  <Point X="3.410849365234" Y="24.73323828125" />
                  <Point X="3.418406005859" Y="24.72251953125" />
                  <Point X="3.472791503906" Y="24.65321875" />
                  <Point X="3.479353271484" Y="24.645705078125" />
                  <Point X="3.500547851562" Y="24.6246171875" />
                  <Point X="3.516727050781" Y="24.611119140625" />
                  <Point X="3.523187255859" Y="24.60618359375" />
                  <Point X="3.543359375" Y="24.592517578125" />
                  <Point X="3.634001953125" Y="24.540123046875" />
                  <Point X="3.639493896484" Y="24.53718359375" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026123047" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.76161328125" Y="24.068947265625" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.467213867188" Y="24.086740234375" />
                  <Point X="3.436780761719" Y="24.09074609375" />
                  <Point X="3.427152099609" Y="24.091517578125" />
                  <Point X="3.398231933594" Y="24.09187109375" />
                  <Point X="3.377422607422" Y="24.090361328125" />
                  <Point X="3.370739990234" Y="24.089638671875" />
                  <Point X="3.350817626953" Y="24.086525390625" />
                  <Point X="3.172918212891" Y="24.047859375" />
                  <Point X="3.156493652344" Y="24.042693359375" />
                  <Point X="3.124842285156" Y="24.029478515625" />
                  <Point X="3.109615478516" Y="24.0214296875" />
                  <Point X="3.079573242188" Y="24.001873046875" />
                  <Point X="3.068251708984" Y="23.993228515625" />
                  <Point X="3.047031982422" Y="23.97434765625" />
                  <Point X="3.037133789062" Y="23.964111328125" />
                  <Point X="2.929604980469" Y="23.834787109375" />
                  <Point X="2.920571777344" Y="23.821880859375" />
                  <Point X="2.904761230469" Y="23.794748046875" />
                  <Point X="2.897983886719" Y="23.780521484375" />
                  <Point X="2.886322998047" Y="23.749685546875" />
                  <Point X="2.882611083984" Y="23.7374296875" />
                  <Point X="2.876869384766" Y="23.71253125" />
                  <Point X="2.874839599609" Y="23.699888671875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.85908203125" Y="23.520515625" />
                  <Point X="2.860162597656" Y="23.488328125" />
                  <Point X="2.863184082031" Y="23.467541015625" />
                  <Point X="2.871820800781" Y="23.43442578125" />
                  <Point X="2.877642578125" Y="23.418259765625" />
                  <Point X="2.886182373047" Y="23.39994140625" />
                  <Point X="2.898569091797" Y="23.37747265625" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001742431641" Y="23.217646484375" />
                  <Point X="3.019794921875" Y="23.195548828125" />
                  <Point X="3.043489990234" Y="23.17187890625" />
                  <Point X="3.052796875" Y="23.163720703125" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045500732422" Y="22.302587890625" />
                  <Point X="4.001272949219" Y="22.23974609375" />
                  <Point X="2.875558837891" Y="22.889677734375" />
                  <Point X="2.848454101562" Y="22.905326171875" />
                  <Point X="2.839666015625" Y="22.90980859375" />
                  <Point X="2.812457519531" Y="22.921388671875" />
                  <Point X="2.791975585938" Y="22.928080078125" />
                  <Point X="2.766739257812" Y="22.934451171875" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.543206787109" Y="22.974064453125" />
                  <Point X="2.511064208984" Y="22.975783203125" />
                  <Point X="2.489989746094" Y="22.974560546875" />
                  <Point X="2.456043457031" Y="22.968759765625" />
                  <Point X="2.4393359375" Y="22.96430859375" />
                  <Point X="2.420035400391" Y="22.95723046875" />
                  <Point X="2.396964599609" Y="22.946984375" />
                  <Point X="2.2210703125" Y="22.854412109375" />
                  <Point X="2.207595947266" Y="22.845798828125" />
                  <Point X="2.182282470703" Y="22.826435546875" />
                  <Point X="2.170443359375" Y="22.815685546875" />
                  <Point X="2.147576660156" Y="22.791103515625" />
                  <Point X="2.139642578125" Y="22.781353515625" />
                  <Point X="2.125132568359" Y="22.760892578125" />
                  <Point X="2.118556640625" Y="22.750181640625" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.02111328125" Y="22.563431640625" />
                  <Point X="2.009793945312" Y="22.533279296875" />
                  <Point X="2.004602905273" Y="22.512712890625" />
                  <Point X="1.999928710938" Y="22.478400390625" />
                  <Point X="1.999168334961" Y="22.461029296875" />
                  <Point X="2.000168579102" Y="22.440166015625" />
                  <Point X="2.002975463867" Y="22.415498046875" />
                  <Point X="2.041213378906" Y="22.203767578125" />
                  <Point X="2.043015380859" Y="22.195775390625" />
                  <Point X="2.051238037109" Y="22.168451171875" />
                  <Point X="2.064070800781" Y="22.137517578125" />
                  <Point X="2.069547119141" Y="22.126419921875" />
                  <Point X="2.735893310547" Y="20.972275390625" />
                  <Point X="2.723752929688" Y="20.963916015625" />
                  <Point X="1.854598144531" Y="22.09662109375" />
                  <Point X="1.833914672852" Y="22.123576171875" />
                  <Point X="1.82737512207" Y="22.13122265625" />
                  <Point X="1.813523193359" Y="22.145783203125" />
                  <Point X="1.806210693359" Y="22.152697265625" />
                  <Point X="1.789280395508" Y="22.167083984375" />
                  <Point X="1.76899621582" Y="22.182119140625" />
                  <Point X="1.560174560547" Y="22.31637109375" />
                  <Point X="1.54978125" Y="22.32216796875" />
                  <Point X="1.520731079102" Y="22.33605859375" />
                  <Point X="1.500618041992" Y="22.34303125" />
                  <Point X="1.466684082031" Y="22.350671875" />
                  <Point X="1.449363647461" Y="22.35292578125" />
                  <Point X="1.428194946289" Y="22.353716796875" />
                  <Point X="1.403689819336" Y="22.353048828125" />
                  <Point X="1.175307983398" Y="22.332033203125" />
                  <Point X="1.159341308594" Y="22.329173828125" />
                  <Point X="1.128119506836" Y="22.32077734375" />
                  <Point X="1.112864501953" Y="22.315240234375" />
                  <Point X="1.081841308594" Y="22.30087109375" />
                  <Point X="1.071003540039" Y="22.294966796875" />
                  <Point X="1.050192993164" Y="22.281810546875" />
                  <Point X="1.040220214844" Y="22.27455859375" />
                  <Point X="0.863874389648" Y="22.127931640625" />
                  <Point X="0.855221557617" Y="22.11976953125" />
                  <Point X="0.833225524902" Y="22.09624609375" />
                  <Point X="0.820458374023" Y="22.079060546875" />
                  <Point X="0.802877624512" Y="22.048779296875" />
                  <Point X="0.795494934082" Y="22.032822265625" />
                  <Point X="0.788290588379" Y="22.0125" />
                  <Point X="0.781705688477" Y="21.98937109375" />
                  <Point X="0.728977722168" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.72472454834" Y="21.710322265625" />
                  <Point X="0.724742248535" Y="21.676830078125" />
                  <Point X="0.725555053711" Y="21.66448046875" />
                  <Point X="0.833091491699" Y="20.84766015625" />
                  <Point X="0.663150817871" Y="21.48188671875" />
                  <Point X="0.655065002441" Y="21.512064453125" />
                  <Point X="0.651881774902" Y="21.52180859375" />
                  <Point X="0.644483032227" Y="21.5408984375" />
                  <Point X="0.640267822266" Y="21.5502421875" />
                  <Point X="0.629742614746" Y="21.570572265625" />
                  <Point X="0.617102722168" Y="21.5915546875" />
                  <Point X="0.456679870605" Y="21.822693359375" />
                  <Point X="0.449291107178" Y="21.832029296875" />
                  <Point X="0.42775177002" Y="21.855994140625" />
                  <Point X="0.411681915283" Y="21.8702421875" />
                  <Point X="0.382912689209" Y="21.890439453125" />
                  <Point X="0.367585205078" Y="21.899197265625" />
                  <Point X="0.347727111816" Y="21.908208984375" />
                  <Point X="0.325529815674" Y="21.916650390625" />
                  <Point X="0.077296142578" Y="21.993693359375" />
                  <Point X="0.066815559387" Y="21.996302734375" />
                  <Point X="0.038075901031" Y="22.00174609375" />
                  <Point X="0.018518463135" Y="22.00338671875" />
                  <Point X="-0.01367414856" Y="22.00275" />
                  <Point X="-0.029743999481" Y="22.00105859375" />
                  <Point X="-0.049681613922" Y="21.99722265625" />
                  <Point X="-0.070102546692" Y="21.99210546875" />
                  <Point X="-0.318341583252" Y="21.9150625" />
                  <Point X="-0.32946081543" Y="21.91083203125" />
                  <Point X="-0.358786621094" Y="21.897515625" />
                  <Point X="-0.377306091309" Y="21.88641015625" />
                  <Point X="-0.405382720947" Y="21.86488671875" />
                  <Point X="-0.418408935547" Y="21.85280859375" />
                  <Point X="-0.433231536865" Y="21.836228515625" />
                  <Point X="-0.447672790527" Y="21.817931640625" />
                  <Point X="-0.608095581055" Y="21.58679296875" />
                  <Point X="-0.612473144531" Y="21.579865234375" />
                  <Point X="-0.625977172852" Y="21.554734375" />
                  <Point X="-0.638777587891" Y="21.52378515625" />
                  <Point X="-0.642752807617" Y="21.512064453125" />
                  <Point X="-0.985424865723" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.908586862305" Y="21.112801633036" />
                  <Point X="-2.443466463825" Y="20.915369737376" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.129533592871" Y="20.357638323466" />
                  <Point X="-0.970201966857" Y="20.290006060797" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.944584819338" Y="21.231286095085" />
                  <Point X="-2.378894445234" Y="20.991164777506" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.119647027474" Y="20.45664596129" />
                  <Point X="-0.945372546332" Y="20.382670832937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.896728134775" Y="21.314176373554" />
                  <Point X="-2.287759304743" Y="21.055684441349" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.13617574595" Y="20.566866221881" />
                  <Point X="-0.920543125808" Y="20.475335605077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.848871450211" Y="21.397066652023" />
                  <Point X="-2.193306562151" Y="21.11879586665" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.158597429984" Y="20.679587897943" />
                  <Point X="-0.895713705284" Y="20.568000377217" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.801014765647" Y="21.479956930492" />
                  <Point X="-2.098707483334" Y="21.18184517591" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.181019114017" Y="20.792309574006" />
                  <Point X="-0.87088428476" Y="20.660665149357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.817805124915" Y="22.014763067219" />
                  <Point X="-3.771145518579" Y="21.994957239396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.753158081084" Y="21.562847208961" />
                  <Point X="-1.955245588058" Y="21.224153450133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.244076549434" Y="20.922280103169" />
                  <Point X="-0.846054864235" Y="20.753329921497" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.93385366514" Y="22.167226985857" />
                  <Point X="-3.668129410465" Y="22.054433731691" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.70530139652" Y="21.64573748743" />
                  <Point X="-1.669240812224" Y="21.205955861329" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465319801859" Y="21.119396527933" />
                  <Point X="-0.821225443711" Y="20.845994693637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.036601368176" Y="22.314045034072" />
                  <Point X="-3.565113302351" Y="22.113910223986" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.657444711957" Y="21.728627765899" />
                  <Point X="-0.796396023187" Y="20.938659465776" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.119007932725" Y="22.452228781267" />
                  <Point X="-3.462097194236" Y="22.173386716281" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.609588027393" Y="21.811518044368" />
                  <Point X="-0.771566602662" Y="21.031324237916" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.160091617857" Y="22.572872006816" />
                  <Point X="-3.359081086122" Y="22.232863208576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.56173134283" Y="21.894408322837" />
                  <Point X="-0.746737182138" Y="21.123989010056" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.073496465421" Y="22.639318781255" />
                  <Point X="-3.256064978008" Y="22.292339700871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.513874658266" Y="21.977298601306" />
                  <Point X="-0.721907761614" Y="21.216653782196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.986901312985" Y="22.705765555693" />
                  <Point X="-3.153048869894" Y="22.351816193166" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.466017973703" Y="22.060188879775" />
                  <Point X="-0.69707834109" Y="21.309318554336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.900306160549" Y="22.772212330132" />
                  <Point X="-3.05003276178" Y="22.411292685461" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.418161289139" Y="22.143079158244" />
                  <Point X="-0.672248920565" Y="21.401983326476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.813711008113" Y="22.83865910457" />
                  <Point X="-2.947016653666" Y="22.470769177757" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.370304604575" Y="22.225969436713" />
                  <Point X="-0.647419500041" Y="21.494648098616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.827483658866" Y="20.868588851312" />
                  <Point X="0.830505019862" Y="20.867306359658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.727115855677" Y="22.905105879009" />
                  <Point X="-2.844000545552" Y="22.530245670052" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.326997038351" Y="22.310790701353" />
                  <Point X="-0.61090204058" Y="21.582351592576" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.796281244016" Y="20.985037726474" />
                  <Point X="0.816113690865" Y="20.976619352243" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.640520703241" Y="22.971552653447" />
                  <Point X="-2.740984437438" Y="22.589722162347" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.31081866683" Y="22.407127625928" />
                  <Point X="-0.555786348469" Y="21.66216060515" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.765078829167" Y="21.101486601636" />
                  <Point X="0.801722361868" Y="21.085932344827" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.553925550804" Y="23.037999427886" />
                  <Point X="-2.624887515444" Y="22.643646178575" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.350940400409" Y="22.527362527269" />
                  <Point X="-0.500457389784" Y="21.741879091435" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.733876414318" Y="21.217935476798" />
                  <Point X="0.787331032872" Y="21.195245337411" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.467330398368" Y="23.104446202325" />
                  <Point X="-0.444866960299" Y="21.82148658995" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.702673999469" Y="21.33438435196" />
                  <Point X="0.772939703875" Y="21.304558329996" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.66120482471" Y="23.714420065877" />
                  <Point X="-4.659131762535" Y="23.713540103191" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.380735245932" Y="23.170892976763" />
                  <Point X="-0.367929650035" Y="21.892032875169" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.671471584619" Y="21.450833227121" />
                  <Point X="0.758548374878" Y="21.41387132258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.690772271985" Y="23.830174938478" />
                  <Point X="-4.473555135909" Y="23.737971734565" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.294140093496" Y="23.237339751202" />
                  <Point X="-0.237880116529" Y="21.94003435919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.628789110049" Y="21.572155098523" />
                  <Point X="0.744157045882" Y="21.523184315165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.720339719261" Y="23.94592981108" />
                  <Point X="-4.287978509282" Y="23.762403365938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.20754494106" Y="23.30378652564" />
                  <Point X="-0.097434242585" Y="21.983622858514" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.529764094182" Y="21.717392959787" />
                  <Point X="0.729765716885" Y="21.632497307749" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.745621926126" Y="24.059865707046" />
                  <Point X="-4.102401882655" Y="23.786834997311" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.120949788624" Y="23.370233300079" />
                  <Point X="0.243496011658" Y="21.942110787357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.409960113403" Y="21.871450968363" />
                  <Point X="0.727383455804" Y="21.736712753437" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.761336569827" Y="24.169740413396" />
                  <Point X="-3.916825256029" Y="23.811266628685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.035886069338" Y="23.437330129322" />
                  <Point X="0.747376587898" Y="21.831430408219" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.69121538092" Y="21.00631979381" />
                  <Point X="2.724360701067" Y="20.992250440133" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.777051213529" Y="24.279615119747" />
                  <Point X="-3.731248629402" Y="23.835698260058" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.978400886744" Y="23.516133352859" />
                  <Point X="0.767913697359" Y="21.925917158309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.573771168485" Y="21.159376140152" />
                  <Point X="2.645432710326" Y="21.12895762035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.726563842774" Y="24.361388738178" />
                  <Point X="-3.545672002775" Y="23.860129891431" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.950954984844" Y="23.607687494548" />
                  <Point X="0.790747031433" Y="22.019429218878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.456326956051" Y="21.312432486493" />
                  <Point X="2.566504719586" Y="21.265664800566" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.57751627565" Y="24.40132603537" />
                  <Point X="-3.360095376149" Y="23.884561522805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.957833155201" Y="23.7138113405" />
                  <Point X="0.838819090287" Y="22.102228076385" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338882743616" Y="21.465488832835" />
                  <Point X="2.487576728846" Y="21.402371980783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.428468708527" Y="24.441263332563" />
                  <Point X="0.917113289075" Y="22.172198396597" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.221438531181" Y="21.618545179177" />
                  <Point X="2.408648738106" Y="21.539079161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.279421141403" Y="24.481200629755" />
                  <Point X="0.999285621068" Y="22.240522546931" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.103994318746" Y="21.771601525519" />
                  <Point X="2.329720747366" Y="21.675786341216" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.13037357428" Y="24.521137926947" />
                  <Point X="1.090643075068" Y="22.304947844288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.986550106311" Y="21.92465787186" />
                  <Point X="2.250792756626" Y="21.812493521433" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.981326007156" Y="24.561075224139" />
                  <Point X="1.253103110712" Y="22.33919188637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.869105893876" Y="22.077714218202" />
                  <Point X="2.171864765886" Y="21.94920070165" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.832278440033" Y="24.601012521332" />
                  <Point X="1.472024940713" Y="22.34946931867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.579948014785" Y="22.303658691638" />
                  <Point X="2.092936775145" Y="22.085907881867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.683230872909" Y="24.640949818524" />
                  <Point X="2.039785600637" Y="22.211673452751" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.534183364608" Y="24.680887140685" />
                  <Point X="2.01959978902" Y="22.32344605728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.41666865732" Y="24.73420934276" />
                  <Point X="2.00079838819" Y="22.434631014296" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.345271565123" Y="24.807107311026" />
                  <Point X="2.010032773798" Y="22.533915486015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.305477116643" Y="24.893419805674" />
                  <Point X="2.050101733023" Y="22.620111457766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.780849749331" Y="25.622882568629" />
                  <Point X="-4.574572450492" Y="25.535323050116" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.296435812283" Y="24.992786235521" />
                  <Point X="2.094499046164" Y="22.704470152284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.766480670561" Y="25.719987492412" />
                  <Point X="-3.915230242971" Y="25.358653123613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.333226350103" Y="25.111607128154" />
                  <Point X="2.143972319667" Y="22.78667422946" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.75211159179" Y="25.817092416195" />
                  <Point X="2.223952397998" Y="22.855928936264" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.73774251302" Y="25.914197339979" />
                  <Point X="2.332500487484" Y="22.913057241783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.717440954179" Y="26.008784075375" />
                  <Point X="2.448894486413" Y="22.966855156333" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.555397685684" Y="22.497172414187" />
                  <Point X="4.038211092566" Y="22.292230282037" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.692359593502" Y="26.101341905265" />
                  <Point X="2.770756983235" Y="22.933436868003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.880311812314" Y="22.886933602065" />
                  <Point X="4.073646024942" Y="22.380393281483" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.667278232826" Y="26.193899735155" />
                  <Point X="3.772631299705" Y="22.611370687508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.642196872149" Y="26.286457565045" />
                  <Point X="3.471616574467" Y="22.842348093533" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.402147894213" Y="26.287767055108" />
                  <Point X="3.17060184923" Y="23.073325499558" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.049701583817" Y="26.241366708132" />
                  <Point X="2.974144218196" Y="23.259921052238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.723016136587" Y="26.205901198814" />
                  <Point X="2.885879295719" Y="23.400591524837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.579890522553" Y="26.248352215956" />
                  <Point X="2.859264065586" Y="23.51509325561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.496585489464" Y="26.316195563199" />
                  <Point X="2.867028392339" Y="23.615001730292" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.451494791518" Y="26.400259933329" />
                  <Point X="2.877183918431" Y="23.713895201074" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.423600662918" Y="26.491623814072" />
                  <Point X="2.909734385701" Y="23.803282583316" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.434328640766" Y="26.599381806351" />
                  <Point X="2.968485491488" Y="23.881548454338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.50820696914" Y="26.733945532063" />
                  <Point X="3.031911151433" Y="23.957830094843" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.784967537811" Y="26.954627659437" />
                  <Point X="3.116562615669" Y="24.025101915974" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.085981244681" Y="27.185604633191" />
                  <Point X="3.260988123168" Y="24.067001161075" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.202050844322" Y="27.338077491019" />
                  <Point X="3.453307357571" Y="24.088570725252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.153772746334" Y="27.420788890102" />
                  <Point X="3.805752256159" Y="24.042170977554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.105494648346" Y="27.503500289185" />
                  <Point X="4.158198278161" Y="23.995770752994" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.057216550358" Y="27.586211688268" />
                  <Point X="4.510644300163" Y="23.949370528434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.008938452369" Y="27.668923087351" />
                  <Point X="4.736044036498" Y="23.956898252633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.950243033826" Y="27.747212596206" />
                  <Point X="4.757515154474" Y="24.05098853963" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.889884072686" Y="27.824795973123" />
                  <Point X="3.418314098561" Y="24.72264989756" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.269504994833" Y="24.361340798305" />
                  <Point X="4.773446068383" Y="24.14743050373" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.829525111546" Y="27.90237935004" />
                  <Point X="3.366366347971" Y="24.847904645298" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.769166150406" Y="27.979962726957" />
                  <Point X="-3.226535630281" Y="27.749629736657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.22577210054" Y="27.749305637511" />
                  <Point X="3.34880413651" Y="24.958563597633" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.960644949947" Y="27.739970074844" />
                  <Point X="3.360128029985" Y="25.056961125885" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.868073221236" Y="27.803879943167" />
                  <Point X="3.382918438387" Y="25.150491407321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.798405236421" Y="27.877511873971" />
                  <Point X="3.432363550275" Y="25.232707438393" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.756507183656" Y="27.962931441577" />
                  <Point X="3.496313397918" Y="25.308766574422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.75047494493" Y="28.063575144006" />
                  <Point X="3.590322565605" Y="25.372066286099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.765133066446" Y="28.173001383296" />
                  <Point X="3.707533302937" Y="25.425517515766" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.830866855213" Y="28.304107957055" />
                  <Point X="3.856580055939" Y="25.465455158532" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.9097952661" Y="28.440815315614" />
                  <Point X="4.005627807868" Y="25.505392377279" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.988723676988" Y="28.577522674173" />
                  <Point X="4.154675559797" Y="25.545329596026" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.050313638215" Y="28.706870297498" />
                  <Point X="3.125137377528" Y="26.08554686258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.437842490479" Y="25.952811417232" />
                  <Point X="4.303723311726" Y="25.585266814774" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.963672156437" Y="28.773297406298" />
                  <Point X="3.035386404053" Y="26.226848126404" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.638302676938" Y="25.970925352281" />
                  <Point X="4.452771063655" Y="25.625204033521" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.877030674659" Y="28.839724515098" />
                  <Point X="3.006845699008" Y="26.342167172785" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.823879140662" Y="25.995357052802" />
                  <Point X="4.601818815584" Y="25.665141252268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.790389192881" Y="28.906151623897" />
                  <Point X="3.004913353625" Y="26.44619164059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.009455604385" Y="26.019788753324" />
                  <Point X="4.750866567512" Y="25.705078471015" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.703747711103" Y="28.972578732697" />
                  <Point X="3.0291436585" Y="26.539110722235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.195032068109" Y="26.044220453845" />
                  <Point X="4.770475060146" Y="25.799959395562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.610373063154" Y="29.036147782024" />
                  <Point X="3.07113792824" Y="26.624489448159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.380608531833" Y="26.068652154367" />
                  <Point X="4.753269682552" Y="25.910466880907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.505068196789" Y="29.094652754081" />
                  <Point X="2.097536565431" Y="27.140962943552" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.479154186945" Y="26.978975873798" />
                  <Point X="3.131070942645" Y="26.702253628738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.566184995557" Y="26.093083854888" />
                  <Point X="4.725618707595" Y="26.025408259273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.399763330423" Y="29.153157726138" />
                  <Point X="2.027486439475" Y="27.273901693746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.622601409882" Y="27.021290376059" />
                  <Point X="3.20975336747" Y="26.772059156775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.294458464058" Y="29.211662698195" />
                  <Point X="-2.019094074003" Y="29.094777449336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.872594087865" Y="29.032591894645" />
                  <Point X="2.012882857404" Y="27.383304782415" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.726786547697" Y="27.080270644687" />
                  <Point X="3.296348551308" Y="26.838505917884" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.701433497073" Y="29.06314277018" />
                  <Point X="2.025415660186" Y="27.481189159111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.829802904369" Y="27.139747031476" />
                  <Point X="3.382943735146" Y="26.904952678993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.589650204058" Y="29.118897813276" />
                  <Point X="2.052972645059" Y="27.572696148875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.932819261041" Y="27.199223418264" />
                  <Point X="3.469538918984" Y="26.971399440103" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.524458541577" Y="29.194429830179" />
                  <Point X="2.098726858557" Y="27.656478873364" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.035835617713" Y="27.258699805053" />
                  <Point X="3.556134102822" Y="27.037846201212" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.490425719937" Y="29.283187990322" />
                  <Point X="2.146583546254" Y="27.739369150503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.138851974385" Y="27.318176191841" />
                  <Point X="3.64272928666" Y="27.104292962321" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.465187267366" Y="29.375679138659" />
                  <Point X="2.194440233951" Y="27.822259427642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.241868331056" Y="27.37765257863" />
                  <Point X="3.729324470498" Y="27.17073972343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.468417830746" Y="29.480254667309" />
                  <Point X="2.242296921648" Y="27.905149704781" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.344884687728" Y="27.437128965419" />
                  <Point X="3.815919654336" Y="27.23718648454" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.443216076848" Y="29.572761393309" />
                  <Point X="2.290153609345" Y="27.98803998192" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.4479010444" Y="27.496605352207" />
                  <Point X="3.902514838174" Y="27.303633245649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.296793603208" Y="29.613812976575" />
                  <Point X="2.338010297042" Y="28.070930259059" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.550917401072" Y="27.556081738996" />
                  <Point X="3.989110022012" Y="27.370080006758" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.150371129567" Y="29.654864559841" />
                  <Point X="-0.225135205062" Y="29.262125210836" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.120903764269" Y="29.115240382928" />
                  <Point X="2.385866984739" Y="28.153820536197" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.653933757744" Y="27.615558125785" />
                  <Point X="4.07570520585" Y="27.436526767867" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.003948655926" Y="29.695916143107" />
                  <Point X="-0.257056812291" Y="29.378879365051" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.19902807261" Y="29.185282817357" />
                  <Point X="2.433723672436" Y="28.236710813336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.756950114416" Y="27.675034512573" />
                  <Point X="4.112217398034" Y="27.524232497654" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.835317797505" Y="29.727540826325" />
                  <Point X="-0.288259234904" Y="29.495328243509" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.240438455874" Y="29.270909388385" />
                  <Point X="2.481580360132" Y="28.319601090475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.859966471088" Y="27.734510899362" />
                  <Point X="4.023885122925" Y="27.66493155975" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.64473183316" Y="29.749846119991" />
                  <Point X="-0.319461657517" Y="29.611777121966" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.265267847836" Y="29.363574172649" />
                  <Point X="2.529437047829" Y="28.402491367614" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.454145868815" Y="29.772151413657" />
                  <Point X="-0.35066408013" Y="29.728226000424" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.290097239799" Y="29.456238956912" />
                  <Point X="2.577293735526" Y="28.485381644753" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.314926631761" Y="29.548903741176" />
                  <Point X="2.625150423223" Y="28.568271921892" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.339756023723" Y="29.64156852544" />
                  <Point X="2.67300711092" Y="28.651162199031" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.364585415685" Y="29.734233309704" />
                  <Point X="2.720863798617" Y="28.73405247617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.524101924072" Y="29.769726804977" />
                  <Point X="2.768720486314" Y="28.816942753309" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.86106252439" Y="29.729899751941" />
                  <Point X="2.816577174011" Y="28.899833030448" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.424889281438" Y="29.593773728722" />
                  <Point X="2.334620282809" Y="29.207615829115" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000163818359" Y="25.001625976562" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978482421875" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.479624908447" Y="21.4327109375" />
                  <Point X="0.47153918457" Y="21.462888671875" />
                  <Point X="0.461013946533" Y="21.48321875" />
                  <Point X="0.300591033936" Y="21.714357421875" />
                  <Point X="0.289068786621" Y="21.726177734375" />
                  <Point X="0.269210662842" Y="21.735189453125" />
                  <Point X="0.020976930618" Y="21.812232421875" />
                  <Point X="0.006153041363" Y="21.81448046875" />
                  <Point X="-0.01378453064" Y="21.81064453125" />
                  <Point X="-0.262023590088" Y="21.7336015625" />
                  <Point X="-0.276761322021" Y="21.72617578125" />
                  <Point X="-0.291583892822" Y="21.709595703125" />
                  <Point X="-0.45200680542" Y="21.47845703125" />
                  <Point X="-0.459226989746" Y="21.462888671875" />
                  <Point X="-0.847744018555" Y="20.012921875" />
                  <Point X="-1.100232421875" Y="20.061931640625" />
                  <Point X="-1.104755615234" Y="20.063095703125" />
                  <Point X="-1.351589355469" Y="20.126603515625" />
                  <Point X="-1.310123779297" Y="20.44156640625" />
                  <Point X="-1.309150390625" Y="20.448958984375" />
                  <Point X="-1.310905029297" Y="20.4713828125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.375948730469" Y="20.784779296875" />
                  <Point X="-1.390991088867" Y="20.8015" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.633376708984" Y="21.0105390625" />
                  <Point X="-1.655489990234" Y="21.014646484375" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.975038452148" Y="21.032931640625" />
                  <Point X="-1.995085693359" Y="21.02273046875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734863281" Y="20.842705078125" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.855837402344" Y="20.832390625" />
                  <Point X="-2.862080322266" Y="20.837197265625" />
                  <Point X="-3.228580810547" Y="21.119390625" />
                  <Point X="-2.521060058594" Y="22.344853515625" />
                  <Point X="-2.506033447266" Y="22.370880859375" />
                  <Point X="-2.499766113281" Y="22.40243359375" />
                  <Point X="-2.5143671875" Y="22.431623046875" />
                  <Point X="-2.531328613281" Y="22.448583984375" />
                  <Point X="-2.560157958984" Y="22.46280078125" />
                  <Point X="-2.591684570312" Y="22.456529296875" />
                  <Point X="-3.842959716797" Y="21.73410546875" />
                  <Point X="-4.161704589844" Y="22.15287109375" />
                  <Point X="-4.166180664062" Y="22.160376953125" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.190078613281" Y="23.5566796875" />
                  <Point X="-3.163787109375" Y="23.576853515625" />
                  <Point X="-3.145660888672" Y="23.604607421875" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140329345703" Y="23.665412109375" />
                  <Point X="-3.161707275391" Y="23.68968359375" />
                  <Point X="-3.187642089844" Y="23.704947265625" />
                  <Point X="-3.219527099609" Y="23.711427734375" />
                  <Point X="-4.803283203125" Y="23.502921875" />
                  <Point X="-4.927393066406" Y="23.98880859375" />
                  <Point X="-4.928577148438" Y="23.99708984375" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-3.587413330078" Y="24.863328125" />
                  <Point X="-3.557463378906" Y="24.871353515625" />
                  <Point X="-3.541335693359" Y="24.878962890625" />
                  <Point X="-3.514143066406" Y="24.8978359375" />
                  <Point X="-3.494712158203" Y="24.924693359375" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.485834228516" Y="24.984140625" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.514713867188" Y="25.04" />
                  <Point X="-3.541895996094" Y="25.058865234375" />
                  <Point X="-3.557463134766" Y="25.0660859375" />
                  <Point X="-4.998186035156" Y="25.452126953125" />
                  <Point X="-4.917645507812" Y="25.9964140625" />
                  <Point X="-4.915259277344" Y="26.005220703125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.774734619141" Y="26.396806640625" />
                  <Point X="-3.753265625" Y="26.39398046875" />
                  <Point X="-3.731713867188" Y="26.39586328125" />
                  <Point X="-3.730465087891" Y="26.3962578125" />
                  <Point X="-3.670278808594" Y="26.415234375" />
                  <Point X="-3.651534423828" Y="26.4260546875" />
                  <Point X="-3.639120117188" Y="26.44378515625" />
                  <Point X="-3.638622558594" Y="26.444986328125" />
                  <Point X="-3.61447265625" Y="26.5032890625" />
                  <Point X="-3.610714599609" Y="26.52461328125" />
                  <Point X="-3.616323486328" Y="26.545525390625" />
                  <Point X="-3.616916259766" Y="26.5466640625" />
                  <Point X="-3.646055664062" Y="26.602640625" />
                  <Point X="-3.65996875" Y="26.619220703125" />
                  <Point X="-4.476105957031" Y="27.245466796875" />
                  <Point X="-4.160016601562" Y="27.787001953125" />
                  <Point X="-4.153696777344" Y="27.795125" />
                  <Point X="-3.774670898438" Y="28.282310546875" />
                  <Point X="-3.172343994141" Y="27.934556640625" />
                  <Point X="-3.159158447266" Y="27.926943359375" />
                  <Point X="-3.138495117188" Y="27.92043359375" />
                  <Point X="-3.136788330078" Y="27.920283203125" />
                  <Point X="-3.052965820312" Y="27.91294921875" />
                  <Point X="-3.031518554688" Y="27.91576953125" />
                  <Point X="-3.013270019531" Y="27.92738671875" />
                  <Point X="-3.012026367188" Y="27.92862890625" />
                  <Point X="-2.952528564453" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.938225341797" Y="28.02956640625" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067138672" Y="28.134033203125" />
                  <Point X="-3.307278808594" Y="28.74927734375" />
                  <Point X="-2.752874023438" Y="29.17433203125" />
                  <Point X="-2.742926513672" Y="29.179859375" />
                  <Point X="-2.141549316406" Y="29.513970703125" />
                  <Point X="-1.971861328125" Y="29.292830078125" />
                  <Point X="-1.967824829102" Y="29.2875703125" />
                  <Point X="-1.95127355957" Y="29.27367578125" />
                  <Point X="-1.949324951172" Y="29.27266015625" />
                  <Point X="-1.856030761719" Y="29.22409375" />
                  <Point X="-1.835114624023" Y="29.218490234375" />
                  <Point X="-1.813791259766" Y="29.222255859375" />
                  <Point X="-1.811790039063" Y="29.223087890625" />
                  <Point X="-1.714635009766" Y="29.263330078125" />
                  <Point X="-1.696905273438" Y="29.275744140625" />
                  <Point X="-1.686083496094" Y="29.29448828125" />
                  <Point X="-1.685431884766" Y="29.2965546875" />
                  <Point X="-1.653804077148" Y="29.396865234375" />
                  <Point X="-1.651917480469" Y="29.41842578125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-0.968080444336" Y="29.903298828125" />
                  <Point X="-0.956035339355" Y="29.904708984375" />
                  <Point X="-0.224199996948" Y="29.990359375" />
                  <Point X="-0.04604094696" Y="29.3254609375" />
                  <Point X="-0.042140533447" Y="29.310904296875" />
                  <Point X="-0.024282501221" Y="29.28417578125" />
                  <Point X="0.006155934334" Y="29.27384375" />
                  <Point X="0.03659444046" Y="29.28417578125" />
                  <Point X="0.05445255661" Y="29.310904296875" />
                  <Point X="0.236648086548" Y="29.9908671875" />
                  <Point X="0.860210998535" Y="29.925564453125" />
                  <Point X="0.870184875488" Y="29.92315625" />
                  <Point X="1.508461669922" Y="29.7690546875" />
                  <Point X="1.514469604492" Y="29.766876953125" />
                  <Point X="1.931043334961" Y="29.61578125" />
                  <Point X="1.937316650391" Y="29.61284765625" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.344777099609" Y="29.421591796875" />
                  <Point X="2.732521484375" Y="29.19569140625" />
                  <Point X="2.738248779297" Y="29.191619140625" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.247245605469" Y="27.533720703125" />
                  <Point X="2.229853759766" Y="27.50359765625" />
                  <Point X="2.224418701172" Y="27.489892578125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202213623047" Y="27.390923828125" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218668945312" Y="27.30083203125" />
                  <Point X="2.219549560547" Y="27.299533203125" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.276217041016" Y="27.223337890625" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360372314453" Y="27.172974609375" />
                  <Point X="2.361737792969" Y="27.172810546875" />
                  <Point X="2.429761230469" Y="27.164607421875" />
                  <Point X="2.450285400391" Y="27.16637890625" />
                  <Point X="2.528951171875" Y="27.187416015625" />
                  <Point X="2.541034423828" Y="27.19241796875" />
                  <Point X="3.994248535156" Y="28.0314296875" />
                  <Point X="4.20258984375" Y="27.7418828125" />
                  <Point X="4.205783691406" Y="27.736603515625" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.311514404297" Y="26.61065234375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.278204833984" Y="26.582310546875" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.212684814453" Y="26.489947265625" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.191136230469" Y="26.389236328125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.216616699219" Y="26.286478515625" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280951904297" Y="26.19881640625" />
                  <Point X="3.282357910156" Y="26.19802734375" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.370466064453" Y="26.153369140625" />
                  <Point X="3.462726074219" Y="26.14117578125" />
                  <Point X="3.475803955078" Y="26.141171875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.939187988281" Y="25.95138671875" />
                  <Point X="4.940194335938" Y="25.944923828125" />
                  <Point X="4.997858398438" Y="25.5745546875" />
                  <Point X="3.767406738281" Y="25.244857421875" />
                  <Point X="3.741164794922" Y="25.23782421875" />
                  <Point X="3.727219970703" Y="25.231740234375" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.621144287109" Y="25.1655" />
                  <Point X="3.566758789062" Y="25.09619921875" />
                  <Point X="3.556986328125" Y="25.074740234375" />
                  <Point X="3.556611572266" Y="25.07278515625" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538856445312" Y="24.957365234375" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.841240234375" />
                  <Point X="3.567879150391" Y="24.8398125" />
                  <Point X="3.622264648438" Y="24.77051171875" />
                  <Point X="3.638443847656" Y="24.757013671875" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.74116796875" Y="24.699611328125" />
                  <Point X="4.998068359375" Y="24.362826171875" />
                  <Point X="4.948432128906" Y="24.033603515625" />
                  <Point X="4.947143066406" Y="24.027953125" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.4424140625" Y="23.898365234375" />
                  <Point X="3.411980957031" Y="23.90237109375" />
                  <Point X="3.391171630859" Y="23.900861328125" />
                  <Point X="3.213272216797" Y="23.8621953125" />
                  <Point X="3.183229980469" Y="23.842638671875" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064040283203" Y="23.682478515625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.049849121094" Y="23.498541015625" />
                  <Point X="3.058388916016" Y="23.48022265625" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168461914062" Y="23.314458984375" />
                  <Point X="4.339073730469" Y="22.41621484375" />
                  <Point X="4.204133300781" Y="22.197859375" />
                  <Point X="4.201465332031" Y="22.1940703125" />
                  <Point X="4.056687988281" Y="21.988361328125" />
                  <Point X="2.780558837891" Y="22.725134765625" />
                  <Point X="2.753454101563" Y="22.740783203125" />
                  <Point X="2.732972167969" Y="22.747474609375" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.504754394531" Y="22.78592578125" />
                  <Point X="2.485453857422" Y="22.77884765625" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.286692871094" Y="22.661693359375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.188950439453" Y="22.470126953125" />
                  <Point X="2.189950683594" Y="22.449263671875" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234092041016" Y="22.221419921875" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.832319580078" Y="20.807859375" />
                  <Point X="2.679775146484" Y="20.709119140625" />
                  <Point X="1.703860961914" Y="21.98095703125" />
                  <Point X="1.683177490234" Y="22.007912109375" />
                  <Point X="1.666247192383" Y="22.022298828125" />
                  <Point X="1.457425537109" Y="22.15655078125" />
                  <Point X="1.442268798828" Y="22.16305859375" />
                  <Point X="1.421099975586" Y="22.163849609375" />
                  <Point X="1.192718017578" Y="22.142833984375" />
                  <Point X="1.161694824219" Y="22.12846484375" />
                  <Point X="0.985348937988" Y="21.981837890625" />
                  <Point X="0.974574951172" Y="21.969337890625" />
                  <Point X="0.967370605469" Y="21.949015625" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="0.99436517334" Y="20.0367578125" />
                  <Point X="0.99157824707" Y="20.03625" />
                  <Point X="0.860200561523" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#118" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.00390064914" Y="4.369681895088" Z="0.05" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="-0.968003380622" Y="4.985648828303" Z="0.05" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.05" />
                  <Point X="-1.735049949642" Y="4.773202724265" Z="0.05" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.05" />
                  <Point X="-1.749657495632" Y="4.76229068577" Z="0.05" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.05" />
                  <Point X="-1.739273588101" Y="4.342870572963" Z="0.05" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.05" />
                  <Point X="-1.837097220393" Y="4.300553770622" Z="0.05" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.05" />
                  <Point X="-1.932393262754" Y="4.348290817886" Z="0.05" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.05" />
                  <Point X="-1.93835169858" Y="4.354551788394" Z="0.05" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.05" />
                  <Point X="-2.773365456428" Y="4.254846840066" Z="0.05" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.05" />
                  <Point X="-3.366716927053" Y="3.802710162212" Z="0.05" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.05" />
                  <Point X="-3.371056583724" Y="3.780360893541" Z="0.05" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.05" />
                  <Point X="-2.99419109081" Y="3.056490372642" Z="0.05" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.05" />
                  <Point X="-3.053537848399" Y="2.995265719487" Z="0.05" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.05" />
                  <Point X="-3.138586092965" Y="3.001373608363" Z="0.05" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.05" />
                  <Point X="-3.153498454756" Y="3.009137365022" Z="0.05" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.05" />
                  <Point X="-4.19931624633" Y="2.857109324516" Z="0.05" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.05" />
                  <Point X="-4.540791668474" Y="2.275656880946" Z="0.05" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.05" />
                  <Point X="-4.530474828928" Y="2.250717646477" Z="0.05" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.05" />
                  <Point X="-3.667422733659" Y="1.554857547071" Z="0.05" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.05" />
                  <Point X="-3.690972397514" Y="1.495401206497" Z="0.05" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.05" />
                  <Point X="-3.751656180949" Y="1.475223470073" Z="0.05" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.05" />
                  <Point X="-3.774364875135" Y="1.477658957397" Z="0.05" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.05" />
                  <Point X="-4.969675146706" Y="1.04957976763" Z="0.05" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.05" />
                  <Point X="-5.058560937174" Y="0.458578267408" Z="0.05" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.05" />
                  <Point X="-5.030377171099" Y="0.438617977872" Z="0.05" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.05" />
                  <Point X="-3.549368052329" Y="0.03019575693" Z="0.05" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.05" />
                  <Point X="-3.539743475892" Y="0.00060165449" Z="0.05" />
                  <Point X="-3.539556741714" Y="0" Z="0.05" />
                  <Point X="-3.548621125243" Y="-0.029205296571" Z="0.05" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.05" />
                  <Point X="-3.576000542794" Y="-0.048680226204" Z="0.05" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.05" />
                  <Point X="-3.606510617673" Y="-0.057094078884" Z="0.05" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.05" />
                  <Point X="-4.984229401837" Y="-0.978709775635" Z="0.05" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.05" />
                  <Point X="-4.849643612815" Y="-1.510431759269" Z="0.05" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.05" />
                  <Point X="-4.814047238116" Y="-1.516834302359" Z="0.05" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.05" />
                  <Point X="-3.193210857741" Y="-1.322135189324" Z="0.05" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.05" />
                  <Point X="-3.200225286589" Y="-1.351596718477" Z="0.05" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.05" />
                  <Point X="-3.226672215948" Y="-1.372371280762" Z="0.05" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.05" />
                  <Point X="-4.215280109534" Y="-2.833951380975" Z="0.05" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.05" />
                  <Point X="-3.86924543407" Y="-3.290720878224" Z="0.05" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.05" />
                  <Point X="-3.836212306232" Y="-3.284899591101" Z="0.05" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.05" />
                  <Point X="-2.555841151336" Y="-2.572489317796" Z="0.05" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.05" />
                  <Point X="-2.570517422067" Y="-2.59886607626" Z="0.05" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.05" />
                  <Point X="-2.89874028676" Y="-4.171138553667" Z="0.05" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.05" />
                  <Point X="-2.459986207747" Y="-4.444050442405" Z="0.05" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.05" />
                  <Point X="-2.446578229853" Y="-4.443625547797" Z="0.05" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.05" />
                  <Point X="-1.973463056298" Y="-3.987563618858" Z="0.05" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.05" />
                  <Point X="-1.66491591836" Y="-4.003966348519" Z="0.05" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.05" />
                  <Point X="-1.430114359069" Y="-4.204811361994" Z="0.05" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.05" />
                  <Point X="-1.366100284126" Y="-4.507090090598" Z="0.05" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.05" />
                  <Point X="-1.365851868284" Y="-4.520625426187" Z="0.05" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.05" />
                  <Point X="-1.12337049826" Y="-4.954047888065" Z="0.05" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.05" />
                  <Point X="-0.823718218124" Y="-5.012588597834" Z="0.05" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="-0.809582330895" Y="-4.983586521031" Z="0.05" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="-0.256663929494" Y="-3.287634335822" Z="0.05" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="-0.005114063067" Y="-3.205826584888" Z="0.05" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.05" />
                  <Point X="0.248245016294" Y="-3.2812854604" Z="0.05" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.05" />
                  <Point X="0.413781929938" Y="-3.514011419946" Z="0.05" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.05" />
                  <Point X="0.425172529506" Y="-3.548949508784" Z="0.05" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.05" />
                  <Point X="0.994369946537" Y="-4.981662670772" Z="0.05" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.05" />
                  <Point X="1.173438988355" Y="-4.942547478528" Z="0.05" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.05" />
                  <Point X="1.172618176336" Y="-4.9080696609" Z="0.05" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.05" />
                  <Point X="1.010073773645" Y="-3.030323580723" Z="0.05" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.05" />
                  <Point X="1.187510772313" Y="-2.878696142601" Z="0.05" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.05" />
                  <Point X="1.419525591962" Y="-2.854659535104" Z="0.05" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.05" />
                  <Point X="1.63305199199" Y="-2.988479542273" Z="0.05" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.05" />
                  <Point X="1.658037361379" Y="-3.01820047973" Z="0.05" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.05" />
                  <Point X="2.853332915767" Y="-4.202834719354" Z="0.05" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.05" />
                  <Point X="3.042692806531" Y="-4.067843367192" Z="0.05" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.05" />
                  <Point X="3.030863632857" Y="-4.03801017363" Z="0.05" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.05" />
                  <Point X="2.232998849965" Y="-2.510569975499" Z="0.05" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.05" />
                  <Point X="2.324783132933" Y="-2.330313774627" Z="0.05" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.05" />
                  <Point X="2.502584603746" Y="-2.234118177034" Z="0.05" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.05" />
                  <Point X="2.717936828013" Y="-2.270449159526" Z="0.05" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.05" />
                  <Point X="2.749403423477" Y="-2.286885867417" Z="0.05" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.05" />
                  <Point X="4.236197797082" Y="-2.803427478795" Z="0.05" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.05" />
                  <Point X="4.395989308518" Y="-2.545556071662" Z="0.05" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.05" />
                  <Point X="4.374855987248" Y="-2.521660483069" Z="0.05" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.05" />
                  <Point X="3.094290705691" Y="-1.461457228435" Z="0.05" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.05" />
                  <Point X="3.107673967496" Y="-1.29082246569" Z="0.05" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.05" />
                  <Point X="3.2155203405" Y="-1.158048366925" Z="0.05" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.05" />
                  <Point X="3.395634976776" Y="-1.116717001991" Z="0.05" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.05" />
                  <Point X="3.429732983189" Y="-1.119927020836" Z="0.05" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.05" />
                  <Point X="4.989734132655" Y="-0.951891064569" Z="0.05" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.05" />
                  <Point X="5.04687311413" Y="-0.576735963862" Z="0.05" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.05" />
                  <Point X="5.02177330617" Y="-0.562129824021" Z="0.05" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.05" />
                  <Point X="3.657310032407" Y="-0.168417550475" Z="0.05" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.05" />
                  <Point X="3.601057114339" Y="-0.09803817216" Z="0.05" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.05" />
                  <Point X="3.581808190258" Y="-0.001950087443" Z="0.05" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.05" />
                  <Point X="3.599563260166" Y="0.094660443805" Z="0.05" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.05" />
                  <Point X="3.654322324062" Y="0.165910566415" Z="0.05" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.05" />
                  <Point X="3.746085381945" Y="0.219731293767" Z="0.05" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.05" />
                  <Point X="3.774194511136" Y="0.227842108748" Z="0.05" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.05" />
                  <Point X="4.983443123711" Y="0.983896672156" Z="0.05" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.05" />
                  <Point X="4.882830763218" Y="1.400261684516" Z="0.05" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.05" />
                  <Point X="4.852169860137" Y="1.404895836607" Z="0.05" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.05" />
                  <Point X="3.370861258359" Y="1.234217386644" Z="0.05" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.05" />
                  <Point X="3.300724279901" Y="1.272879727956" Z="0.05" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.05" />
                  <Point X="3.252231039542" Y="1.345241980211" Z="0.05" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.05" />
                  <Point X="3.233948682095" Y="1.430620703531" Z="0.05" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.05" />
                  <Point X="3.254681524036" Y="1.507760171147" Z="0.05" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.05" />
                  <Point X="3.31173182357" Y="1.583173524272" Z="0.05" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.05" />
                  <Point X="3.335796344946" Y="1.602265496211" Z="0.05" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.05" />
                  <Point X="4.242405230237" Y="2.793771726203" Z="0.05" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.05" />
                  <Point X="4.007023273151" Y="3.122008330542" Z="0.05" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.05" />
                  <Point X="3.972137346875" Y="3.11123459334" Z="0.05" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.05" />
                  <Point X="2.431212237163" Y="2.245962539392" Z="0.05" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.05" />
                  <Point X="2.361568131808" Y="2.253731551859" Z="0.05" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.05" />
                  <Point X="2.298135961221" Y="2.295990972545" Z="0.05" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.05" />
                  <Point X="2.25476762769" Y="2.358888899161" Z="0.05" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.05" />
                  <Point X="2.2456981509" Y="2.428190312024" Z="0.05" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.05" />
                  <Point X="2.266565403266" Y="2.508257338719" Z="0.05" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.05" />
                  <Point X="2.284390759519" Y="2.54000173508" Z="0.05" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.05" />
                  <Point X="2.761069827592" Y="4.263647221515" Z="0.05" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.05" />
                  <Point X="2.363791019259" Y="4.496076097035" Z="0.05" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.05" />
                  <Point X="1.952342054843" Y="4.689419755257" Z="0.05" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.05" />
                  <Point X="1.52536251974" Y="4.84516068365" Z="0.05" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.05" />
                  <Point X="0.875763290288" Y="5.003039966007" Z="0.05" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.05" />
                  <Point X="0.206754693237" Y="5.074908160767" Z="0.05" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.05" />
                  <Point X="0.189343912413" Y="5.061765610863" Z="0.05" />
                  <Point X="0" Y="4.355124473572" Z="0.05" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>