<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#117" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="270" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000475830078" Y="25.004717773438" />
                  <Width Value="9.783895996094" />
                  <Height Value="9.766447265625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.563502197266" Y="21.486728515625" />
                  <Point X="0.558131713867" Y="21.501654296875" />
                  <Point X="0.542240600586" Y="21.532798828125" />
                  <Point X="0.3786355896" Y="21.7685234375" />
                  <Point X="0.356650421143" Y="21.7910546875" />
                  <Point X="0.330268341064" Y="21.810337890625" />
                  <Point X="0.302368499756" Y="21.82437109375" />
                  <Point X="0.049136127472" Y="21.90296484375" />
                  <Point X="0.020852109909" Y="21.907234375" />
                  <Point X="-0.008916111946" Y="21.9071953125" />
                  <Point X="-0.036950737" Y="21.90292578125" />
                  <Point X="-0.290183105469" Y="21.82433203125" />
                  <Point X="-0.311769073486" Y="21.81453515625" />
                  <Point X="-0.332859436035" Y="21.801572265625" />
                  <Point X="-0.356658660889" Y="21.7807734375" />
                  <Point X="-0.366476318359" Y="21.768302734375" />
                  <Point X="-0.530050964355" Y="21.532625" />
                  <Point X="-0.538189025879" Y="21.518427734375" />
                  <Point X="-0.550989990234" Y="21.4874765625" />
                  <Point X="-0.916584472656" Y="20.12305859375" />
                  <Point X="-1.079336181641" Y="20.1546484375" />
                  <Point X="-1.24641809082" Y="20.197638671875" />
                  <Point X="-1.214994018555" Y="20.436326171875" />
                  <Point X="-1.214229858398" Y="20.451759765625" />
                  <Point X="-1.21653894043" Y="20.48392578125" />
                  <Point X="-1.277036132812" Y="20.78806640625" />
                  <Point X="-1.284377563477" Y="20.810248046875" />
                  <Point X="-1.294839111328" Y="20.832302734375" />
                  <Point X="-1.312682739258" Y="20.857939453125" />
                  <Point X="-1.323866699219" Y="20.86898828125" />
                  <Point X="-1.556905273438" Y="21.073359375" />
                  <Point X="-1.583358764648" Y="21.0897734375" />
                  <Point X="-1.613247314453" Y="21.1020859375" />
                  <Point X="-1.64321875" Y="21.109044921875" />
                  <Point X="-1.952616699219" Y="21.12932421875" />
                  <Point X="-1.983555786133" Y="21.12625390625" />
                  <Point X="-2.014733398438" Y="21.117849609375" />
                  <Point X="-2.042786621094" Y="21.10511328125" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312790283203" Y="20.92317578125" />
                  <Point X="-2.335102783203" Y="20.900537109375" />
                  <Point X="-2.480147460938" Y="20.71151171875" />
                  <Point X="-2.801709472656" Y="20.91061328125" />
                  <Point X="-3.104721435547" Y="21.143921875" />
                  <Point X="-2.424133544922" Y="22.322734375" />
                  <Point X="-2.422786621094" Y="22.32512109375" />
                  <Point X="-2.412240478516" Y="22.35422265625" />
                  <Point X="-2.406430664062" Y="22.385439453125" />
                  <Point X="-2.405744873047" Y="22.41599609375" />
                  <Point X="-2.414797363281" Y="22.445189453125" />
                  <Point X="-2.428958984375" Y="22.473611328125" />
                  <Point X="-2.446813476563" Y="22.498419921875" />
                  <Point X="-2.464153320312" Y="22.515759765625" />
                  <Point X="-2.489311035156" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.547758056641" Y="22.55698828125" />
                  <Point X="-2.578692138672" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.8180234375" Y="21.858197265625" />
                  <Point X="-4.082863037109" Y="22.206142578125" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.106666503906" Y="23.500939453125" />
                  <Point X="-3.106143798828" Y="23.50133984375" />
                  <Point X="-3.089136474609" Y="23.518126953125" />
                  <Point X="-3.073733398438" Y="23.53773828125" />
                  <Point X="-3.058676757812" Y="23.56532421875" />
                  <Point X="-3.053852539062" Y="23.580181640625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.0421171875" Y="23.63758984375" />
                  <Point X="-3.042044677734" Y="23.65771875" />
                  <Point X="-3.046214599609" Y="23.677412109375" />
                  <Point X="-3.054778076172" Y="23.703708984375" />
                  <Point X="-3.064922119141" Y="23.725234375" />
                  <Point X="-3.08009765625" Y="23.743564453125" />
                  <Point X="-3.096161621094" Y="23.758640625" />
                  <Point X="-3.112987792969" Y="23.7712421875" />
                  <Point X="-3.13945703125" Y="23.7868203125" />
                  <Point X="-3.168721923828" Y="23.798044921875" />
                  <Point X="-3.200608886719" Y="23.804525390625" />
                  <Point X="-3.231929443359" Y="23.805615234375" />
                  <Point X="-4.7321015625" Y="23.60811328125" />
                  <Point X="-4.834076660156" Y="24.00734375" />
                  <Point X="-4.892423828125" Y="24.415298828125" />
                  <Point X="-3.533617919922" Y="24.779390625" />
                  <Point X="-3.518851074219" Y="24.784689453125" />
                  <Point X="-3.487716064453" Y="24.8005390625" />
                  <Point X="-3.459977050781" Y="24.819791015625" />
                  <Point X="-3.442167236328" Y="24.83583203125" />
                  <Point X="-3.425830078125" Y="24.854796875" />
                  <Point X="-3.409555908203" Y="24.881630859375" />
                  <Point X="-3.404163818359" Y="24.895947265625" />
                  <Point X="-3.394917480469" Y="24.92573828125" />
                  <Point X="-3.390647949219" Y="24.953912109375" />
                  <Point X="-3.39065234375" Y="24.983568359375" />
                  <Point X="-3.394921386719" Y="25.011712890625" />
                  <Point X="-3.404167724609" Y="25.041505859375" />
                  <Point X="-3.418300292969" Y="25.069541015625" />
                  <Point X="-3.437576171875" Y="25.09581640625" />
                  <Point X="-3.460005126953" Y="25.117666015625" />
                  <Point X="-3.487724853516" Y="25.13690625" />
                  <Point X="-3.501916503906" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.891815917969" Y="25.5219765625" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-3.766198486328" Y="26.299861328125" />
                  <Point X="-3.765188964844" Y="26.299732421875" />
                  <Point X="-3.744488525391" Y="26.299353515625" />
                  <Point X="-3.7230625" Y="26.301306640625" />
                  <Point X="-3.702920410156" Y="26.30533203125" />
                  <Point X="-3.641712158203" Y="26.324630859375" />
                  <Point X="-3.622966308594" Y="26.332853515625" />
                  <Point X="-3.604391357422" Y="26.343521484375" />
                  <Point X="-3.587844970703" Y="26.35556640625" />
                  <Point X="-3.574262451172" Y="26.370876953125" />
                  <Point X="-3.561855712891" Y="26.388337890625" />
                  <Point X="-3.551740234375" Y="26.406501953125" />
                  <Point X="-3.551338867188" Y="26.407458984375" />
                  <Point X="-3.526703857422" Y="26.46693359375" />
                  <Point X="-3.520955566406" Y="26.48656640625" />
                  <Point X="-3.517187255859" Y="26.507640625" />
                  <Point X="-3.515775634766" Y="26.528048828125" />
                  <Point X="-3.518765136719" Y="26.548283203125" />
                  <Point X="-3.524155761719" Y="26.569001953125" />
                  <Point X="-3.531633300781" Y="26.5885703125" />
                  <Point X="-3.532065429688" Y="26.589408203125" />
                  <Point X="-3.561790283203" Y="26.6465078125" />
                  <Point X="-3.573281738281" Y="26.663705078125" />
                  <Point X="-3.587194091797" Y="26.68028515625" />
                  <Point X="-3.602135986328" Y="26.69458984375" />
                  <Point X="-4.351859863281" Y="27.269873046875" />
                  <Point X="-4.081156494141" Y="27.733654296875" />
                  <Point X="-3.750503662109" Y="28.158662109375" />
                  <Point X="-3.207255859375" Y="27.845017578125" />
                  <Point X="-3.206929931641" Y="27.844828125" />
                  <Point X="-3.188078125" Y="27.836474609375" />
                  <Point X="-3.167545654297" Y="27.82993359375" />
                  <Point X="-3.147266601562" Y="27.825837890625" />
                  <Point X="-3.146751220703" Y="27.825791015625" />
                  <Point X="-3.061244628906" Y="27.818310546875" />
                  <Point X="-3.041040527344" Y="27.818701171875" />
                  <Point X="-3.020062011719" Y="27.82135546875" />
                  <Point X="-3.000407226562" Y="27.826005859375" />
                  <Point X="-2.982179931641" Y="27.83470703125" />
                  <Point X="-2.964174072266" Y="27.84579296875" />
                  <Point X="-2.947485595703" Y="27.858841796875" />
                  <Point X="-2.946046386719" Y="27.8602578125" />
                  <Point X="-2.885353271484" Y="27.920951171875" />
                  <Point X="-2.872435058594" Y="27.9370390625" />
                  <Point X="-2.860825195312" Y="27.955240234375" />
                  <Point X="-2.851684326172" Y="27.973736328125" />
                  <Point X="-2.846752441406" Y="27.99376953125" />
                  <Point X="-2.8438984375" Y="28.015169921875" />
                  <Point X="-2.843409667969" Y="28.03581640625" />
                  <Point X="-2.843439453125" Y="28.036162109375" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.183332519531" Y="28.724595703125" />
                  <Point X="-2.700620117188" Y="29.094685546875" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.043295898438" Y="29.22987109375" />
                  <Point X="-2.042829833984" Y="29.229267578125" />
                  <Point X="-2.028868286133" Y="29.21469921875" />
                  <Point X="-2.012905883789" Y="29.201248046875" />
                  <Point X="-1.995984741211" Y="29.189853515625" />
                  <Point X="-1.99506652832" Y="29.18937109375" />
                  <Point X="-1.899898071289" Y="29.139828125" />
                  <Point X="-1.864206054688" Y="29.12748828125" />
                  <Point X="-1.843811523438" Y="29.123859375" />
                  <Point X="-1.823115356445" Y="29.12473828125" />
                  <Point X="-1.799407714844" Y="29.128361328125" />
                  <Point X="-1.777404418945" Y="29.134501953125" />
                  <Point X="-1.678280029297" Y="29.175560546875" />
                  <Point X="-1.660180541992" Y="29.185486328125" />
                  <Point X="-1.642481811523" Y="29.1978671875" />
                  <Point X="-1.626954467773" Y="29.21146484375" />
                  <Point X="-1.61473034668" Y="29.228095703125" />
                  <Point X="-1.603905029297" Y="29.24678515625" />
                  <Point X="-1.595503662109" Y="29.265845703125" />
                  <Point X="-1.595460327148" Y="29.265984375" />
                  <Point X="-1.563200683594" Y="29.368298828125" />
                  <Point X="-1.559165283203" Y="29.3885859375" />
                  <Point X="-1.557279052734" Y="29.410146484375" />
                  <Point X="-1.557730224609" Y="29.430826171875" />
                  <Point X="-1.584201782227" Y="29.631896484375" />
                  <Point X="-0.949618896484" Y="29.809810546875" />
                  <Point X="-0.294711364746" Y="29.886458984375" />
                  <Point X="-0.133999908447" Y="29.28667578125" />
                  <Point X="-0.121228111267" Y="29.258486328125" />
                  <Point X="-0.103372398376" Y="29.23176171875" />
                  <Point X="-0.082217926025" Y="29.209173828125" />
                  <Point X="-0.054925758362" Y="29.194583984375" />
                  <Point X="-0.024491477966" Y="29.18425" />
                  <Point X="0.006040857315" Y="29.179205078125" />
                  <Point X="0.036574562073" Y="29.1842421875" />
                  <Point X="0.067011421204" Y="29.194568359375" />
                  <Point X="0.094307868958" Y="29.209150390625" />
                  <Point X="0.115468383789" Y="29.231734375" />
                  <Point X="0.133330764771" Y="29.258455078125" />
                  <Point X="0.14610925293" Y="29.286640625" />
                  <Point X="0.307388641357" Y="29.88794140625" />
                  <Point X="0.844036437988" Y="29.83173828125" />
                  <Point X="1.481047241211" Y="29.677943359375" />
                  <Point X="1.894652587891" Y="29.527923828125" />
                  <Point X="2.294562744141" Y="29.340900390625" />
                  <Point X="2.680968994141" Y="29.115779296875" />
                  <Point X="2.943259277344" Y="28.92925390625" />
                  <Point X="2.148009277344" Y="27.55183984375" />
                  <Point X="2.142781494141" Y="27.5413359375" />
                  <Point X="2.133066162109" Y="27.516015625" />
                  <Point X="2.111607177734" Y="27.43576953125" />
                  <Point X="2.108616943359" Y="27.417900390625" />
                  <Point X="2.107731933594" Y="27.38091796875" />
                  <Point X="2.116099121094" Y="27.31152734375" />
                  <Point X="2.121544677734" Y="27.289330078125" />
                  <Point X="2.129885742188" Y="27.267248046875" />
                  <Point X="2.129905029297" Y="27.267255859375" />
                  <Point X="2.140092285156" Y="27.247439453125" />
                  <Point X="2.183028564453" Y="27.184162109375" />
                  <Point X="2.194491210938" Y="27.170302734375" />
                  <Point X="2.221628417969" Y="27.145572265625" />
                  <Point X="2.284905517578" Y="27.102634765625" />
                  <Point X="2.305296630859" Y="27.09214453125" />
                  <Point X="2.327430908203" Y="27.083958984375" />
                  <Point X="2.327438720703" Y="27.08398046875" />
                  <Point X="2.348998779297" Y="27.078658203125" />
                  <Point X="2.418388916016" Y="27.070291015625" />
                  <Point X="2.436512939453" Y="27.06984765625" />
                  <Point X="2.473246582031" Y="27.074181640625" />
                  <Point X="2.553492675781" Y="27.095640625" />
                  <Point X="2.565286376953" Y="27.099638671875" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.967324707031" Y="27.90619140625" />
                  <Point X="4.123278808594" Y="27.68944921875" />
                  <Point X="4.262198730469" Y="27.4598828125" />
                  <Point X="3.231351074219" Y="26.66888671875" />
                  <Point X="3.222416015625" Y="26.66109765625" />
                  <Point X="3.203944580078" Y="26.64158984375" />
                  <Point X="3.14619140625" Y="26.56624609375" />
                  <Point X="3.136588867188" Y="26.550876953125" />
                  <Point X="3.121619384766" Y="26.517046875" />
                  <Point X="3.100106201172" Y="26.44012109375" />
                  <Point X="3.096655517578" Y="26.417904296875" />
                  <Point X="3.095822021484" Y="26.39441796875" />
                  <Point X="3.097711669922" Y="26.37190234375" />
                  <Point X="3.097748291016" Y="26.371724609375" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120694580078" Y="26.26894140625" />
                  <Point X="3.136307128906" Y="26.235701171875" />
                  <Point X="3.184340820312" Y="26.162693359375" />
                  <Point X="3.1988828125" Y="26.145458984375" />
                  <Point X="3.216110839844" Y="26.12937890625" />
                  <Point X="3.234436523438" Y="26.116115234375" />
                  <Point X="3.234380859375" Y="26.116015625" />
                  <Point X="3.30398828125" Y="26.07683203125" />
                  <Point X="3.320565185547" Y="26.069486328125" />
                  <Point X="3.356166503906" Y="26.059431640625" />
                  <Point X="3.450280273438" Y="26.046994140625" />
                  <Point X="3.462698242188" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.845935058594" Y="25.932814453125" />
                  <Point X="4.890864746094" Y="25.644240234375" />
                  <Point X="3.717229980469" Y="25.329765625" />
                  <Point X="3.705953369141" Y="25.32597265625" />
                  <Point X="3.681500244141" Y="25.315041015625" />
                  <Point X="3.589036376953" Y="25.261595703125" />
                  <Point X="3.574274902344" Y="25.251064453125" />
                  <Point X="3.54750390625" Y="25.225541015625" />
                  <Point X="3.492025634766" Y="25.154849609375" />
                  <Point X="3.480273925781" Y="25.135509765625" />
                  <Point X="3.470495361328" Y="25.11399609375" />
                  <Point X="3.463657958984" Y="25.092484375" />
                  <Point X="3.445178466797" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.978076171875" />
                  <Point X="3.445187744141" Y="24.9413984375" />
                  <Point X="3.463680664062" Y="24.8448359375" />
                  <Point X="3.470526123047" Y="24.823337890625" />
                  <Point X="3.480299804688" Y="24.801873046875" />
                  <Point X="3.492022460938" Y="24.78259375" />
                  <Point X="3.492050048828" Y="24.78255859375" />
                  <Point X="3.547528320312" Y="24.711865234375" />
                  <Point X="3.560041748047" Y="24.698724609375" />
                  <Point X="3.589080566406" Y="24.675818359375" />
                  <Point X="3.681544189453" Y="24.62237109375" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716579833984" Y="24.60784765625" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.855022949219" Y="24.0512734375" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.425135986328" Y="23.996458984375" />
                  <Point X="3.409396484375" Y="23.997212890625" />
                  <Point X="3.374568115234" Y="23.994470703125" />
                  <Point X="3.193094726563" Y="23.95502734375" />
                  <Point X="3.169917480469" Y="23.947640625" />
                  <Point X="3.142415039062" Y="23.932427734375" />
                  <Point X="3.126290283203" Y="23.919615234375" />
                  <Point X="3.112342529297" Y="23.90597265625" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987883300781" Y="23.74955078125" />
                  <Point X="2.97652734375" Y="23.722029296875" />
                  <Point X="2.969744873047" Y="23.694498046875" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956375244141" Y="23.4923203125" />
                  <Point X="2.964173583984" Y="23.460595703125" />
                  <Point X="2.976517333984" Y="23.4318984375" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="4.213122070312" Y="22.393115234375" />
                  <Point X="4.124817871094" Y="22.250224609375" />
                  <Point X="4.028980712891" Y="22.1140546875" />
                  <Point X="2.801625976562" Y="22.82266796875" />
                  <Point X="2.787360839844" Y="22.829392578125" />
                  <Point X="2.754116210938" Y="22.840193359375" />
                  <Point X="2.538134033203" Y="22.87919921875" />
                  <Point X="2.506690185547" Y="22.879587890625" />
                  <Point X="2.474427734375" Y="22.874583984375" />
                  <Point X="2.444743652344" Y="22.864775390625" />
                  <Point X="2.265315429688" Y="22.77034375" />
                  <Point X="2.242318359375" Y="22.753384765625" />
                  <Point X="2.221311035156" Y="22.7323359375" />
                  <Point X="2.204484375" Y="22.709470703125" />
                  <Point X="2.110052734375" Y="22.53004296875" />
                  <Point X="2.100213378906" Y="22.5001640625" />
                  <Point X="2.095274902344" Y="22.467880859375" />
                  <Point X="2.095694824219" Y="22.436630859375" />
                  <Point X="2.134701171875" Y="22.2206484375" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.861282958984" Y="20.94509375" />
                  <Point X="2.781852294922" Y="20.888357421875" />
                  <Point X="2.701764160156" Y="20.836517578125" />
                  <Point X="1.759064697266" Y="22.065068359375" />
                  <Point X="1.748503295898" Y="22.07669921875" />
                  <Point X="1.721817382812" Y="22.09951171875" />
                  <Point X="1.508800415039" Y="22.2364609375" />
                  <Point X="1.479881347656" Y="22.248859375" />
                  <Point X="1.448143920898" Y="22.256580078125" />
                  <Point X="1.416983398438" Y="22.25887109375" />
                  <Point X="1.184013183594" Y="22.23743359375" />
                  <Point X="1.162787841797" Y="22.23299609375" />
                  <Point X="1.141259765625" Y="22.225849609375" />
                  <Point X="1.1155078125" Y="22.212658203125" />
                  <Point X="1.104229492188" Y="22.204232421875" />
                  <Point X="0.924611694336" Y="22.054884765625" />
                  <Point X="0.904079956055" Y="22.03103515625" />
                  <Point X="0.887160766602" Y="22.0030859375" />
                  <Point X="0.875597351074" Y="21.97406640625" />
                  <Point X="0.821810119629" Y="21.726603515625" />
                  <Point X="0.81972454834" Y="21.710369140625" />
                  <Point X="0.819742736816" Y="21.676876953125" />
                  <Point X="1.022065185547" Y="20.140083984375" />
                  <Point X="0.975691650391" Y="20.129919921875" />
                  <Point X="0.929315307617" Y="20.121494140625" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058425415039" Y="20.24736328125" />
                  <Point X="-1.141246337891" Y="20.268671875" />
                  <Point X="-1.120806762695" Y="20.42392578125" />
                  <Point X="-1.120110107422" Y="20.43162890625" />
                  <Point X="-1.119473754883" Y="20.4585625" />
                  <Point X="-1.121782958984" Y="20.490728515625" />
                  <Point X="-1.123364379883" Y="20.502458984375" />
                  <Point X="-1.183861450195" Y="20.806599609375" />
                  <Point X="-1.18684753418" Y="20.817916015625" />
                  <Point X="-1.194188842773" Y="20.84009765625" />
                  <Point X="-1.198544433594" Y="20.850962890625" />
                  <Point X="-1.209005981445" Y="20.873017578125" />
                  <Point X="-1.216866455078" Y="20.886572265625" />
                  <Point X="-1.234710205078" Y="20.912208984375" />
                  <Point X="-1.245917114258" Y="20.925521484375" />
                  <Point X="-1.261228515625" Y="20.940412109375" />
                  <Point X="-1.494267089844" Y="21.144783203125" />
                  <Point X="-1.506817504883" Y="21.15408203125" />
                  <Point X="-1.533271118164" Y="21.17049609375" />
                  <Point X="-1.547173828125" Y="21.177611328125" />
                  <Point X="-1.57706237793" Y="21.189923828125" />
                  <Point X="-1.591761108398" Y="21.194625" />
                  <Point X="-1.621732543945" Y="21.201583984375" />
                  <Point X="-1.637005371094" Y="21.203841796875" />
                  <Point X="-1.946403320312" Y="21.22412109375" />
                  <Point X="-1.961998168945" Y="21.223859375" />
                  <Point X="-1.992937133789" Y="21.2207890625" />
                  <Point X="-2.008281616211" Y="21.21798046875" />
                  <Point X="-2.039459228516" Y="21.209576171875" />
                  <Point X="-2.054006103516" Y="21.2043515625" />
                  <Point X="-2.082059326172" Y="21.191615234375" />
                  <Point X="-2.095565673828" Y="21.184103515625" />
                  <Point X="-2.353403076172" Y="21.011822265625" />
                  <Point X="-2.359683349609" Y="21.0072421875" />
                  <Point X="-2.380451171875" Y="20.989861328125" />
                  <Point X="-2.402763671875" Y="20.96722265625" />
                  <Point X="-2.410471191406" Y="20.958369140625" />
                  <Point X="-2.503200683594" Y="20.837521484375" />
                  <Point X="-2.747584960938" Y="20.988837890625" />
                  <Point X="-2.980861816406" Y="21.168453125" />
                  <Point X="-2.341861083984" Y="22.275234375" />
                  <Point X="-2.333470458984" Y="22.29275390625" />
                  <Point X="-2.322924316406" Y="22.32185546875" />
                  <Point X="-2.318844238281" Y="22.33683984375" />
                  <Point X="-2.313034423828" Y="22.368056640625" />
                  <Point X="-2.311454589844" Y="22.38330859375" />
                  <Point X="-2.310768798828" Y="22.413865234375" />
                  <Point X="-2.315007080078" Y="22.4441328125" />
                  <Point X="-2.324059570312" Y="22.473326171875" />
                  <Point X="-2.329767822266" Y="22.487556640625" />
                  <Point X="-2.343929443359" Y="22.515978515625" />
                  <Point X="-2.351851806641" Y="22.52910546875" />
                  <Point X="-2.369706298828" Y="22.5539140625" />
                  <Point X="-2.379638427734" Y="22.565595703125" />
                  <Point X="-2.396978271484" Y="22.582935546875" />
                  <Point X="-2.408818847656" Y="22.59298046875" />
                  <Point X="-2.4339765625" Y="22.6110078125" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122558594" Y="22.63320703125" />
                  <Point X="-2.490563476562" Y="22.6389140625" />
                  <Point X="-2.520181640625" Y="22.6478984375" />
                  <Point X="-2.550869384766" Y="22.6519375" />
                  <Point X="-2.581803466797" Y="22.650923828125" />
                  <Point X="-2.597227050781" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-4.004022460937" Y="22.259416015625" />
                  <Point X="-4.181265136719" Y="22.556625" />
                  <Point X="-3.048834228516" Y="23.4255703125" />
                  <Point X="-3.039407714844" Y="23.433728515625" />
                  <Point X="-3.022400390625" Y="23.450515625" />
                  <Point X="-3.014425537109" Y="23.459447265625" />
                  <Point X="-2.999022460938" Y="23.47905859375" />
                  <Point X="-2.990345703125" Y="23.492224609375" />
                  <Point X="-2.9752890625" Y="23.519810546875" />
                  <Point X="-2.968320556641" Y="23.535986328125" />
                  <Point X="-2.961886962891" Y="23.55636328125" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.952145507812" Y="23.596208984375" />
                  <Point X="-2.948110839844" Y="23.623884765625" />
                  <Point X="-2.947117919922" Y="23.637248046875" />
                  <Point X="-2.947045410156" Y="23.657376953125" />
                  <Point X="-2.949105224609" Y="23.6773984375" />
                  <Point X="-2.953275146484" Y="23.697091796875" />
                  <Point X="-2.955883544922" Y="23.706828125" />
                  <Point X="-2.964447021484" Y="23.733125" />
                  <Point X="-2.968842529297" Y="23.74420703125" />
                  <Point X="-2.978986572266" Y="23.765732421875" />
                  <Point X="-2.99174609375" Y="23.78581640625" />
                  <Point X="-3.006921630859" Y="23.804146484375" />
                  <Point X="-3.015086181641" Y="23.8128359375" />
                  <Point X="-3.031150146484" Y="23.827912109375" />
                  <Point X="-3.039213867188" Y="23.8346796875" />
                  <Point X="-3.056040039062" Y="23.84728125" />
                  <Point X="-3.064802490234" Y="23.853115234375" />
                  <Point X="-3.091271728516" Y="23.868693359375" />
                  <Point X="-3.105436279297" Y="23.87551953125" />
                  <Point X="-3.134701171875" Y="23.886744140625" />
                  <Point X="-3.149801513672" Y="23.891142578125" />
                  <Point X="-3.181688476562" Y="23.897623046875" />
                  <Point X="-3.197305175781" Y="23.89946875" />
                  <Point X="-3.228625732422" Y="23.90055859375" />
                  <Point X="-3.244329345703" Y="23.899802734375" />
                  <Point X="-4.660919921875" Y="23.7133046875" />
                  <Point X="-4.740761230469" Y="24.025880859375" />
                  <Point X="-4.786451660156" Y="24.345341796875" />
                  <Point X="-3.509030029297" Y="24.687626953125" />
                  <Point X="-3.501531982422" Y="24.68997265625" />
                  <Point X="-3.475753173828" Y="24.70002734375" />
                  <Point X="-3.444618164062" Y="24.715876953125" />
                  <Point X="-3.433549804688" Y="24.722494140625" />
                  <Point X="-3.405810791016" Y="24.74174609375" />
                  <Point X="-3.396398681641" Y="24.749201171875" />
                  <Point X="-3.378588867188" Y="24.7652421875" />
                  <Point X="-3.370191162109" Y="24.773828125" />
                  <Point X="-3.353854003906" Y="24.79279296875" />
                  <Point X="-3.344601318359" Y="24.805533203125" />
                  <Point X="-3.328327148438" Y="24.8323671875" />
                  <Point X="-3.320652587891" Y="24.848146484375" />
                  <Point X="-3.313433349609" Y="24.867787109375" />
                  <Point X="-3.304187011719" Y="24.897578125" />
                  <Point X="-3.300989990234" Y="24.91150390625" />
                  <Point X="-3.296720458984" Y="24.939677734375" />
                  <Point X="-3.295647949219" Y="24.95392578125" />
                  <Point X="-3.29565234375" Y="24.98358203125" />
                  <Point X="-3.296726806641" Y="24.997814453125" />
                  <Point X="-3.300995849609" Y="25.025958984375" />
                  <Point X="-3.304190429688" Y="25.03987109375" />
                  <Point X="-3.313436767578" Y="25.0696640625" />
                  <Point X="-3.319336669922" Y="25.08426953125" />
                  <Point X="-3.333469238281" Y="25.1123046875" />
                  <Point X="-3.341701904297" Y="25.125734375" />
                  <Point X="-3.360977783203" Y="25.152009765625" />
                  <Point X="-3.371285644531" Y="25.163865234375" />
                  <Point X="-3.393714599609" Y="25.18571484375" />
                  <Point X="-3.405835693359" Y="25.195708984375" />
                  <Point X="-3.433555419922" Y="25.21494921875" />
                  <Point X="-3.440481201172" Y="25.219326171875" />
                  <Point X="-3.465598388672" Y="25.23282421875" />
                  <Point X="-3.496557861328" Y="25.2456328125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.731331054688" Y="25.957529296875" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-3.778598632812" Y="26.205673828125" />
                  <Point X="-3.766927490234" Y="26.204748046875" />
                  <Point X="-3.746227050781" Y="26.204369140625" />
                  <Point X="-3.735864501953" Y="26.20474609375" />
                  <Point X="-3.714438476562" Y="26.20669921875" />
                  <Point X="-3.704444824219" Y="26.2081484375" />
                  <Point X="-3.684302734375" Y="26.212173828125" />
                  <Point X="-3.674353515625" Y="26.214728515625" />
                  <Point X="-3.613145263672" Y="26.23402734375" />
                  <Point X="-3.603551269531" Y="26.2376328125" />
                  <Point X="-3.584805419922" Y="26.24585546875" />
                  <Point X="-3.575653564453" Y="26.25047265625" />
                  <Point X="-3.557078613281" Y="26.261140625" />
                  <Point X="-3.548481201172" Y="26.266716796875" />
                  <Point X="-3.531934814453" Y="26.27876171875" />
                  <Point X="-3.516779296875" Y="26.292521484375" />
                  <Point X="-3.503196777344" Y="26.30783203125" />
                  <Point X="-3.496820800781" Y="26.3158515625" />
                  <Point X="-3.4844140625" Y="26.3333125" />
                  <Point X="-3.478857910156" Y="26.3421171875" />
                  <Point X="-3.468742431641" Y="26.36028125" />
                  <Point X="-3.4635703125" Y="26.371103515625" />
                  <Point X="-3.438935302734" Y="26.430578125" />
                  <Point X="-3.435531494141" Y="26.44023828125" />
                  <Point X="-3.429783203125" Y="26.45987109375" />
                  <Point X="-3.427438720703" Y="26.46984375" />
                  <Point X="-3.423670410156" Y="26.49091796875" />
                  <Point X="-3.422413818359" Y="26.5010859375" />
                  <Point X="-3.421002197266" Y="26.521494140625" />
                  <Point X="-3.421795898438" Y="26.54193359375" />
                  <Point X="-3.424785400391" Y="26.56216796875" />
                  <Point X="-3.426826171875" Y="26.572203125" />
                  <Point X="-3.432216796875" Y="26.592921875" />
                  <Point X="-3.4354140625" Y="26.602912109375" />
                  <Point X="-3.442891601562" Y="26.62248046875" />
                  <Point X="-3.447799804688" Y="26.633275390625" />
                  <Point X="-3.477524658203" Y="26.690375" />
                  <Point X="-3.482802001953" Y="26.6992890625" />
                  <Point X="-3.494293457031" Y="26.716486328125" />
                  <Point X="-3.500507568359" Y="26.72476953125" />
                  <Point X="-3.514419921875" Y="26.741349609375" />
                  <Point X="-3.521498046875" Y="26.748908203125" />
                  <Point X="-3.536439941406" Y="26.763212890625" />
                  <Point X="-3.544303710938" Y="26.769958984375" />
                  <Point X="-4.227614257812" Y="27.29428125" />
                  <Point X="-4.002296142578" Y="27.680306640625" />
                  <Point X="-3.726336914062" Y="28.035013671875" />
                  <Point X="-3.254997070312" Y="27.762884765625" />
                  <Point X="-3.245416748047" Y="27.75797265625" />
                  <Point X="-3.226564941406" Y="27.749619140625" />
                  <Point X="-3.216914306641" Y="27.74595703125" />
                  <Point X="-3.196381835938" Y="27.739416015625" />
                  <Point X="-3.186352783203" Y="27.736814453125" />
                  <Point X="-3.166073730469" Y="27.73271875" />
                  <Point X="-3.155030517578" Y="27.73115234375" />
                  <Point X="-3.069523925781" Y="27.723671875" />
                  <Point X="-3.059408203125" Y="27.723328125" />
                  <Point X="-3.039204101562" Y="27.72371875" />
                  <Point X="-3.029115722656" Y="27.724453125" />
                  <Point X="-3.008137207031" Y="27.727107421875" />
                  <Point X="-2.998188476562" Y="27.728908203125" />
                  <Point X="-2.978533691406" Y="27.73355859375" />
                  <Point X="-2.959481201172" Y="27.7402734375" />
                  <Point X="-2.94125390625" Y="27.748974609375" />
                  <Point X="-2.932373046875" Y="27.753810546875" />
                  <Point X="-2.9143671875" Y="27.764896484375" />
                  <Point X="-2.905657470703" Y="27.770955078125" />
                  <Point X="-2.888968994141" Y="27.78400390625" />
                  <Point X="-2.87887109375" Y="27.79308203125" />
                  <Point X="-2.818177978516" Y="27.853775390625" />
                  <Point X="-2.811278564453" Y="27.861470703125" />
                  <Point X="-2.798360351562" Y="27.87755859375" />
                  <Point X="-2.792341552734" Y="27.885951171875" />
                  <Point X="-2.780731689453" Y="27.90415234375" />
                  <Point X="-2.775658203125" Y="27.913150390625" />
                  <Point X="-2.766517333984" Y="27.931646484375" />
                  <Point X="-2.759438476562" Y="27.95102734375" />
                  <Point X="-2.754506591797" Y="27.971060546875" />
                  <Point X="-2.752586181641" Y="27.9812109375" />
                  <Point X="-2.749732177734" Y="28.002611328125" />
                  <Point X="-2.748925048828" Y="28.012921875" />
                  <Point X="-2.748436279297" Y="28.033568359375" />
                  <Point X="-2.748801025391" Y="28.04444140625" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.059386474609" Y="28.6999140625" />
                  <Point X="-2.648369140625" Y="29.015037109375" />
                  <Point X="-2.192524414062" Y="29.268296875" />
                  <Point X="-2.118664794922" Y="29.1720390625" />
                  <Point X="-2.111418212891" Y="29.16353515625" />
                  <Point X="-2.097456787109" Y="29.148966796875" />
                  <Point X="-2.090085449219" Y="29.142052734375" />
                  <Point X="-2.074123046875" Y="29.1286015625" />
                  <Point X="-2.065968505859" Y="29.12244921875" />
                  <Point X="-2.049047363281" Y="29.1110546875" />
                  <Point X="-2.03893359375" Y="29.10510546875" />
                  <Point X="-1.943765136719" Y="29.0555625" />
                  <Point X="-1.930939697266" Y="29.05004296875" />
                  <Point X="-1.895247680664" Y="29.037703125" />
                  <Point X="-1.880848510742" Y="29.03395703125" />
                  <Point X="-1.860453979492" Y="29.030328125" />
                  <Point X="-1.839780761719" Y="29.0289453125" />
                  <Point X="-1.819084472656" Y="29.02982421875" />
                  <Point X="-1.808763793945" Y="29.030828125" />
                  <Point X="-1.785056274414" Y="29.034451171875" />
                  <Point X="-1.77387121582" Y="29.036857421875" />
                  <Point X="-1.751867919922" Y="29.042998046875" />
                  <Point X="-1.741049682617" Y="29.046732421875" />
                  <Point X="-1.641925170898" Y="29.087791015625" />
                  <Point X="-1.632599975586" Y="29.092263671875" />
                  <Point X="-1.614500488281" Y="29.102189453125" />
                  <Point X="-1.605725952148" Y="29.107642578125" />
                  <Point X="-1.58802722168" Y="29.1200234375" />
                  <Point X="-1.57989465332" Y="29.1263984375" />
                  <Point X="-1.56436730957" Y="29.13999609375" />
                  <Point X="-1.550407714844" Y="29.155201171875" />
                  <Point X="-1.53818371582" Y="29.17183203125" />
                  <Point X="-1.532524536133" Y="29.18048046875" />
                  <Point X="-1.52169921875" Y="29.199169921875" />
                  <Point X="-1.516974853516" Y="29.20846875" />
                  <Point X="-1.508573486328" Y="29.227529296875" />
                  <Point X="-1.504808227539" Y="29.23757421875" />
                  <Point X="-1.47259753418" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990722656" Y="29.370052734375" />
                  <Point X="-1.464526733398" Y="29.380306640625" />
                  <Point X="-1.46264050293" Y="29.4018671875" />
                  <Point X="-1.462301635742" Y="29.41221875" />
                  <Point X="-1.462752807617" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479265991211" Y="29.562654296875" />
                  <Point X="-0.931160095215" Y="29.716322265625" />
                  <Point X="-0.36522253418" Y="29.78255859375" />
                  <Point X="-0.225762832642" Y="29.262087890625" />
                  <Point X="-0.220532791138" Y="29.247470703125" />
                  <Point X="-0.207761032104" Y="29.21928125" />
                  <Point X="-0.200219161987" Y="29.205708984375" />
                  <Point X="-0.182363464355" Y="29.178984375" />
                  <Point X="-0.172711593628" Y="29.166822265625" />
                  <Point X="-0.1515572052" Y="29.144234375" />
                  <Point X="-0.127005142212" Y="29.12539453125" />
                  <Point X="-0.099712928772" Y="29.1108046875" />
                  <Point X="-0.085470245361" Y="29.10462890625" />
                  <Point X="-0.055035995483" Y="29.094294921875" />
                  <Point X="-0.039978565216" Y="29.090521484375" />
                  <Point X="-0.009446219444" Y="29.0854765625" />
                  <Point X="0.021503904343" Y="29.08547265625" />
                  <Point X="0.052037586212" Y="29.090509765625" />
                  <Point X="0.067096054077" Y="29.094279296875" />
                  <Point X="0.097532981873" Y="29.10460546875" />
                  <Point X="0.111774475098" Y="29.110775390625" />
                  <Point X="0.139070861816" Y="29.125357421875" />
                  <Point X="0.163632278442" Y="29.1441953125" />
                  <Point X="0.184792755127" Y="29.166779296875" />
                  <Point X="0.194446853638" Y="29.178939453125" />
                  <Point X="0.212309249878" Y="29.20566015625" />
                  <Point X="0.219853790283" Y="29.219228515625" />
                  <Point X="0.232632385254" Y="29.2474140625" />
                  <Point X="0.237865997314" Y="29.262029296875" />
                  <Point X="0.378139434814" Y="29.785013671875" />
                  <Point X="0.827857543945" Y="29.7379140625" />
                  <Point X="1.45362902832" Y="29.58683203125" />
                  <Point X="1.858258300781" Y="29.440068359375" />
                  <Point X="2.250434326172" Y="29.256662109375" />
                  <Point X="2.629427978516" Y="29.035859375" />
                  <Point X="2.817778808594" Y="28.901916015625" />
                  <Point X="2.065736816406" Y="27.59933984375" />
                  <Point X="2.062960449219" Y="27.59416796875" />
                  <Point X="2.054086425781" Y="27.5753671875" />
                  <Point X="2.04437097168" Y="27.550046875" />
                  <Point X="2.041291015625" Y="27.54055859375" />
                  <Point X="2.01983203125" Y="27.4603125" />
                  <Point X="2.01791003418" Y="27.45144921875" />
                  <Point X="2.013644165039" Y="27.420173828125" />
                  <Point X="2.012759033203" Y="27.38319140625" />
                  <Point X="2.013415161133" Y="27.369544921875" />
                  <Point X="2.021782226562" Y="27.300154296875" />
                  <Point X="2.023834960938" Y="27.288892578125" />
                  <Point X="2.029280395508" Y="27.2666953125" />
                  <Point X="2.032673461914" Y="27.255759765625" />
                  <Point X="2.041014526367" Y="27.233677734375" />
                  <Point X="2.045415649414" Y="27.223822265625" />
                  <Point X="2.055603027344" Y="27.204005859375" />
                  <Point X="2.061480957031" Y="27.19409765625" />
                  <Point X="2.104417236328" Y="27.1308203125" />
                  <Point X="2.109822509766" Y="27.123615234375" />
                  <Point X="2.130501953125" Y="27.1000859375" />
                  <Point X="2.157639160156" Y="27.07535546875" />
                  <Point X="2.168286132813" Y="27.0669609375" />
                  <Point X="2.231563232422" Y="27.0240234375" />
                  <Point X="2.241446289062" Y="27.018158203125" />
                  <Point X="2.261837402344" Y="27.00766796875" />
                  <Point X="2.272345458984" Y="27.00304296875" />
                  <Point X="2.294479736328" Y="26.994857421875" />
                  <Point X="2.304670654297" Y="26.99175" />
                  <Point X="2.326230712891" Y="26.986427734375" />
                  <Point X="2.337625976563" Y="26.984341796875" />
                  <Point X="2.407016113281" Y="26.975974609375" />
                  <Point X="2.416065673828" Y="26.9753203125" />
                  <Point X="2.447644287109" Y="26.975501953125" />
                  <Point X="2.484377929688" Y="26.9798359375" />
                  <Point X="2.497788574219" Y="26.98240625" />
                  <Point X="2.578034667969" Y="27.003865234375" />
                  <Point X="2.583992675781" Y="27.005669921875" />
                  <Point X="2.604414794922" Y="27.0130703125" />
                  <Point X="2.627662353516" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.940401855469" Y="27.780951171875" />
                  <Point X="4.043964599609" Y="27.637021484375" />
                  <Point X="4.136884277344" Y="27.483470703125" />
                  <Point X="3.173518798828" Y="26.744255859375" />
                  <Point X="3.168925292969" Y="26.74049609375" />
                  <Point X="3.15343359375" Y="26.726416015625" />
                  <Point X="3.134962158203" Y="26.706908203125" />
                  <Point X="3.128546875" Y="26.699384765625" />
                  <Point X="3.070793701172" Y="26.624041015625" />
                  <Point X="3.065624023438" Y="26.616583984375" />
                  <Point X="3.049713867188" Y="26.589318359375" />
                  <Point X="3.034744384766" Y="26.55548828125" />
                  <Point X="3.030129882812" Y="26.5426328125" />
                  <Point X="3.008616699219" Y="26.46570703125" />
                  <Point X="3.006231689453" Y="26.454701171875" />
                  <Point X="3.002781005859" Y="26.432484375" />
                  <Point X="3.001715332031" Y="26.4212734375" />
                  <Point X="3.000881835938" Y="26.397787109375" />
                  <Point X="3.001154785156" Y="26.38647265625" />
                  <Point X="3.003044433594" Y="26.36395703125" />
                  <Point X="3.004708251953" Y="26.35252734375" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.024603271484" Y="26.258216796875" />
                  <Point X="3.03470703125" Y="26.2285546875" />
                  <Point X="3.050319580078" Y="26.195314453125" />
                  <Point X="3.056943603516" Y="26.183486328125" />
                  <Point X="3.104977294922" Y="26.110478515625" />
                  <Point X="3.111734130859" Y="26.1014296875" />
                  <Point X="3.126276123047" Y="26.0841953125" />
                  <Point X="3.134061279297" Y="26.076009765625" />
                  <Point X="3.151289306641" Y="26.0599296875" />
                  <Point X="3.160410644531" Y="26.052421875" />
                  <Point X="3.177148193359" Y="26.04030859375" />
                  <Point X="3.187779541016" Y="26.03323046875" />
                  <Point X="3.257386962891" Y="25.994046875" />
                  <Point X="3.265500488281" Y="25.989978515625" />
                  <Point X="3.294744873047" Y="25.9780625" />
                  <Point X="3.330346191406" Y="25.9680078125" />
                  <Point X="3.343720214844" Y="25.96525" />
                  <Point X="3.437833984375" Y="25.9528125" />
                  <Point X="3.444033203125" Y="25.95219921875" />
                  <Point X="3.465708496094" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.704704101562" Y="26.1113203125" />
                  <Point X="4.752682617188" Y="25.91423828125" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="3.692642089844" Y="25.421529296875" />
                  <Point X="3.686943359375" Y="25.41980859375" />
                  <Point X="3.667181884766" Y="25.412701171875" />
                  <Point X="3.642728759766" Y="25.40176953125" />
                  <Point X="3.633959472656" Y="25.3972890625" />
                  <Point X="3.541495605469" Y="25.34384375" />
                  <Point X="3.533862792969" Y="25.338931640625" />
                  <Point X="3.508720947266" Y="25.319822265625" />
                  <Point X="3.481949951172" Y="25.294298828125" />
                  <Point X="3.472770263672" Y="25.28419140625" />
                  <Point X="3.417291992188" Y="25.2135" />
                  <Point X="3.410838867188" Y="25.204181640625" />
                  <Point X="3.399087158203" Y="25.184841796875" />
                  <Point X="3.393788574219" Y="25.1748203125" />
                  <Point X="3.384010009766" Y="25.153306640625" />
                  <Point X="3.379958496094" Y="25.1427734375" />
                  <Point X="3.37312109375" Y="25.12126171875" />
                  <Point X="3.370353515625" Y="25.110353515625" />
                  <Point X="3.351874023438" Y="25.013861328125" />
                  <Point X="3.350601074219" Y="25.004943359375" />
                  <Point X="3.348585449219" Y="24.973666015625" />
                  <Point X="3.350290283203" Y="24.93698828125" />
                  <Point X="3.351883300781" Y="24.923529296875" />
                  <Point X="3.370376220703" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.81601171875" />
                  <Point X="3.380004394531" Y="24.794513671875" />
                  <Point X="3.384067138672" Y="24.783970703125" />
                  <Point X="3.393840820312" Y="24.762505859375" />
                  <Point X="3.399127441406" Y="24.752517578125" />
                  <Point X="3.410850097656" Y="24.73323828125" />
                  <Point X="3.417313720703" Y="24.723912109375" />
                  <Point X="3.472791992188" Y="24.65321875" />
                  <Point X="3.478731445312" Y="24.6463515625" />
                  <Point X="3.501205810547" Y="24.62413671875" />
                  <Point X="3.530244628906" Y="24.60123046875" />
                  <Point X="3.541538330078" Y="24.5935703125" />
                  <Point X="3.634001953125" Y="24.540123046875" />
                  <Point X="3.639491699219" Y="24.537185546875" />
                  <Point X="3.659156005859" Y="24.527982421875" />
                  <Point X="3.683026611328" Y="24.518970703125" />
                  <Point X="3.6919921875" Y="24.516083984375" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761613769531" Y="24.0689453125" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="3.437535888672" Y="24.090646484375" />
                  <Point X="3.429681152344" Y="24.091349609375" />
                  <Point X="3.401939697266" Y="24.091919921875" />
                  <Point X="3.367111328125" Y="24.089177734375" />
                  <Point X="3.354390869141" Y="24.087302734375" />
                  <Point X="3.172917480469" Y="24.047859375" />
                  <Point X="3.164247314453" Y="24.045541015625" />
                  <Point X="3.141070068359" Y="24.038154296875" />
                  <Point X="3.123934570312" Y="24.03076953125" />
                  <Point X="3.096432128906" Y="24.015556640625" />
                  <Point X="3.083314941406" Y="24.006806640625" />
                  <Point X="3.067190185547" Y="23.993994140625" />
                  <Point X="3.059862304688" Y="23.987529296875" />
                  <Point X="3.039294677734" Y="23.966708984375" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921294189453" Y="23.823099609375" />
                  <Point X="2.906524169922" Y="23.798599609375" />
                  <Point X="2.900065429688" Y="23.785787109375" />
                  <Point X="2.888709472656" Y="23.758265625" />
                  <Point X="2.88428515625" Y="23.74475390625" />
                  <Point X="2.877502685547" Y="23.71722265625" />
                  <Point X="2.87514453125" Y="23.703203125" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.85929296875" Y="23.516619140625" />
                  <Point X="2.861639648438" Y="23.485236328125" />
                  <Point X="2.864121582031" Y="23.469642578125" />
                  <Point X="2.871919921875" Y="23.43791796875" />
                  <Point X="2.876904296875" Y="23.42305859375" />
                  <Point X="2.889248046875" Y="23.394361328125" />
                  <Point X="2.896607421875" Y="23.3805234375" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052795898438" Y="23.163720703125" />
                  <Point X="4.087170410156" Y="22.370015625" />
                  <Point X="4.045499023438" Y="22.3025859375" />
                  <Point X="4.001273193359" Y="22.239748046875" />
                  <Point X="2.849125976562" Y="22.904939453125" />
                  <Point X="2.842134033203" Y="22.908599609375" />
                  <Point X="2.81671484375" Y="22.919744140625" />
                  <Point X="2.783470214844" Y="22.930544921875" />
                  <Point X="2.770999755859" Y="22.933681640625" />
                  <Point X="2.555017578125" Y="22.9726875" />
                  <Point X="2.539308105469" Y="22.97419140625" />
                  <Point X="2.507864257812" Y="22.974580078125" />
                  <Point X="2.492129882813" Y="22.97346484375" />
                  <Point X="2.459867431641" Y="22.9684609375" />
                  <Point X="2.444621582031" Y="22.964787109375" />
                  <Point X="2.4149375" Y="22.954978515625" />
                  <Point X="2.400499267578" Y="22.94884375" />
                  <Point X="2.221071044922" Y="22.854412109375" />
                  <Point X="2.208931884766" Y="22.846802734375" />
                  <Point X="2.185934814453" Y="22.82984375" />
                  <Point X="2.175076904297" Y="22.820494140625" />
                  <Point X="2.154069580078" Y="22.7994453125" />
                  <Point X="2.144796630859" Y="22.788642578125" />
                  <Point X="2.127969970703" Y="22.76577734375" />
                  <Point X="2.120416259766" Y="22.75371484375" />
                  <Point X="2.025984741211" Y="22.574287109375" />
                  <Point X="2.019819458008" Y="22.5597578125" />
                  <Point X="2.009980102539" Y="22.52987890625" />
                  <Point X="2.006305786133" Y="22.514529296875" />
                  <Point X="2.00136730957" Y="22.48224609375" />
                  <Point X="2.000283325195" Y="22.466603515625" />
                  <Point X="2.000703369141" Y="22.435353515625" />
                  <Point X="2.00220715332" Y="22.41974609375" />
                  <Point X="2.041213500977" Y="22.203763671875" />
                  <Point X="2.043015380859" Y="22.1957734375" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723753662109" Y="20.9639140625" />
                  <Point X="1.834433105469" Y="22.122900390625" />
                  <Point X="1.829395385742" Y="22.128931640625" />
                  <Point X="1.810233032227" Y="22.14891015625" />
                  <Point X="1.783547119141" Y="22.17172265625" />
                  <Point X="1.773191894531" Y="22.179421875" />
                  <Point X="1.560174926758" Y="22.31637109375" />
                  <Point X="1.546234375" Y="22.323775390625" />
                  <Point X="1.517315307617" Y="22.336173828125" />
                  <Point X="1.502336914062" Y="22.34116796875" />
                  <Point X="1.470599609375" Y="22.348888671875" />
                  <Point X="1.455109985352" Y="22.35132421875" />
                  <Point X="1.42394934082" Y="22.353615234375" />
                  <Point X="1.408278442383" Y="22.353470703125" />
                  <Point X="1.175308105469" Y="22.332033203125" />
                  <Point X="1.164572265625" Y="22.330423828125" />
                  <Point X="1.143346923828" Y="22.325986328125" />
                  <Point X="1.132857543945" Y="22.323158203125" />
                  <Point X="1.111329467773" Y="22.31601171875" />
                  <Point X="1.097947998047" Y="22.31040234375" />
                  <Point X="1.072195922852" Y="22.2972109375" />
                  <Point X="1.0434921875" Y="22.277279296875" />
                  <Point X="0.863874389648" Y="22.127931640625" />
                  <Point X="0.852615600586" Y="22.116865234375" />
                  <Point X="0.83208380127" Y="22.093015625" />
                  <Point X="0.822810791016" Y="22.080232421875" />
                  <Point X="0.805891540527" Y="22.052283203125" />
                  <Point X="0.798908935547" Y="22.038251953125" />
                  <Point X="0.78734564209" Y="22.009232421875" />
                  <Point X="0.782764953613" Y="21.994244140625" />
                  <Point X="0.728977722168" Y="21.74678125" />
                  <Point X="0.727584533691" Y="21.738708984375" />
                  <Point X="0.72472454834" Y="21.710318359375" />
                  <Point X="0.724742675781" Y="21.676826171875" />
                  <Point X="0.725555541992" Y="21.6644765625" />
                  <Point X="0.833091552734" Y="20.847658203125" />
                  <Point X="0.655265075684" Y="21.51131640625" />
                  <Point X="0.652891845703" Y="21.518892578125" />
                  <Point X="0.642752929688" Y="21.54483203125" />
                  <Point X="0.626861755371" Y="21.5759765625" />
                  <Point X="0.62028503418" Y="21.58696484375" />
                  <Point X="0.456680053711" Y="21.822689453125" />
                  <Point X="0.446629577637" Y="21.834869140625" />
                  <Point X="0.42464453125" Y="21.857400390625" />
                  <Point X="0.412709533691" Y="21.867751953125" />
                  <Point X="0.386327484131" Y="21.88703515625" />
                  <Point X="0.372956176758" Y="21.89520703125" />
                  <Point X="0.345056396484" Y="21.909240234375" />
                  <Point X="0.330527893066" Y="21.9151015625" />
                  <Point X="0.077295448303" Y="21.9936953125" />
                  <Point X="0.063315982819" Y="21.996900390625" />
                  <Point X="0.035031856537" Y="22.001169921875" />
                  <Point X="0.020727497101" Y="22.002234375" />
                  <Point X="-0.009040776253" Y="22.0021953125" />
                  <Point X="-0.023219251633" Y="22.00111328125" />
                  <Point X="-0.0512538414" Y="21.99684375" />
                  <Point X="-0.065110099792" Y="21.99365625" />
                  <Point X="-0.318342407227" Y="21.9150625" />
                  <Point X="-0.329444824219" Y="21.91083984375" />
                  <Point X="-0.351030822754" Y="21.90104296875" />
                  <Point X="-0.361514373779" Y="21.895470703125" />
                  <Point X="-0.382604705811" Y="21.8825078125" />
                  <Point X="-0.395373962402" Y="21.87310546875" />
                  <Point X="-0.419173217773" Y="21.852306640625" />
                  <Point X="-0.431302947998" Y="21.839537109375" />
                  <Point X="-0.444520568848" Y="21.822470703125" />
                  <Point X="-0.608095214844" Y="21.58679296875" />
                  <Point X="-0.612470703125" Y="21.579869140625" />
                  <Point X="-0.625977111816" Y="21.554736328125" />
                  <Point X="-0.638777954102" Y="21.52378515625" />
                  <Point X="-0.642752868652" Y="21.512064453125" />
                  <Point X="-0.985425048828" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691451660156" Y="26.104695502444" />
                  <Point X="-4.691451660156" Y="25.566640553692" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.691451660156" Y="24.370797049125" />
                  <Point X="-4.691451660156" Y="23.832835467252" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.596451660156" Y="26.313347451915" />
                  <Point X="-4.596451660156" Y="25.541185416687" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.596451660156" Y="24.396252301376" />
                  <Point X="-4.596451660156" Y="23.721792111529" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501451660156" Y="26.300840321761" />
                  <Point X="-4.501451660156" Y="25.515730279681" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.501451660156" Y="24.421707553626" />
                  <Point X="-4.501451660156" Y="23.734299122907" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.406451660156" Y="26.288333191607" />
                  <Point X="-4.406451660156" Y="25.490275142675" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.406451660156" Y="24.447162805877" />
                  <Point X="-4.406451660156" Y="23.746806134284" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.311451660156" Y="26.275826061452" />
                  <Point X="-4.311451660156" Y="25.464820005669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.311451660156" Y="24.472618058127" />
                  <Point X="-4.311451660156" Y="23.759313145661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216451660156" Y="27.313405527319" />
                  <Point X="-4.216451660156" Y="27.285715893413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216451660156" Y="26.263318931298" />
                  <Point X="-4.216451660156" Y="25.439364868663" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.216451660156" Y="24.498073310378" />
                  <Point X="-4.216451660156" Y="23.771820157038" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="27.476163910377" />
                  <Point X="-4.121451660156" Y="27.21281987935" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="26.250811801144" />
                  <Point X="-4.121451660156" Y="25.413909731657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="24.523528562628" />
                  <Point X="-4.121451660156" Y="23.784327168416" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.121451660156" Y="22.602521521992" />
                  <Point X="-4.121451660156" Y="22.456326905635" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="27.638922293436" />
                  <Point X="-4.026451660156" Y="27.139923865287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="26.238304670989" />
                  <Point X="-4.026451660156" Y="25.388454594652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="24.548983814879" />
                  <Point X="-4.026451660156" Y="23.796834179793" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.026451660156" Y="22.675417629022" />
                  <Point X="-4.026451660156" Y="22.297026368159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="27.771367324391" />
                  <Point X="-3.931451660156" Y="27.067027851224" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="26.225797540835" />
                  <Point X="-3.931451660156" Y="25.362999457646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="24.574439067129" />
                  <Point X="-3.931451660156" Y="23.80934119117" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.931451660156" Y="22.748313736051" />
                  <Point X="-3.931451660156" Y="22.164072445129" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="27.893476550997" />
                  <Point X="-3.836451660156" Y="26.994131837162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="26.213290410681" />
                  <Point X="-3.836451660156" Y="25.33754432064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="24.59989431938" />
                  <Point X="-3.836451660156" Y="23.821848202548" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.836451660156" Y="22.82120984308" />
                  <Point X="-3.836451660156" Y="22.039261376495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="28.015585777603" />
                  <Point X="-3.741451660156" Y="26.921235823099" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="26.204542852561" />
                  <Point X="-3.741451660156" Y="25.312089183634" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="24.62534957163" />
                  <Point X="-3.741451660156" Y="23.834355213925" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.741451660156" Y="22.894105950109" />
                  <Point X="-3.741451660156" Y="22.012100868876" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="27.9888917769" />
                  <Point X="-3.646451660156" Y="26.848339809036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="26.2235259097" />
                  <Point X="-3.646451660156" Y="25.286634046628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="24.650804823881" />
                  <Point X="-3.646451660156" Y="23.846862225302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.646451660156" Y="22.967002057138" />
                  <Point X="-3.646451660156" Y="22.066949213772" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="27.934043356092" />
                  <Point X="-3.551451660156" Y="26.775443794973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="26.264790195052" />
                  <Point X="-3.551451660156" Y="25.261178909623" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="24.676260076131" />
                  <Point X="-3.551451660156" Y="23.859369236679" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.551451660156" Y="23.039898164167" />
                  <Point X="-3.551451660156" Y="22.121797558667" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="27.879194935283" />
                  <Point X="-3.456451660156" Y="26.649895071101" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="26.388289588269" />
                  <Point X="-3.456451660156" Y="25.227908741241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="24.709852985507" />
                  <Point X="-3.456451660156" Y="23.871876248057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.456451660156" Y="23.112794271197" />
                  <Point X="-3.456451660156" Y="22.176645903563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361451660156" Y="27.824346514475" />
                  <Point X="-3.361451660156" Y="25.15255478978" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361451660156" Y="24.783973297553" />
                  <Point X="-3.361451660156" Y="23.884383259434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.361451660156" Y="23.185690378226" />
                  <Point X="-3.361451660156" Y="22.231494248459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.266451660156" Y="27.769498093666" />
                  <Point X="-3.266451660156" Y="23.896890270811" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.266451660156" Y="23.258586485255" />
                  <Point X="-3.266451660156" Y="22.286342593354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.171451660156" Y="27.733804915301" />
                  <Point X="-3.171451660156" Y="23.895542592583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.171451660156" Y="23.331482592284" />
                  <Point X="-3.171451660156" Y="22.34119093825" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076451660156" Y="27.72427794173" />
                  <Point X="-3.076451660156" Y="23.859971201413" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.076451660156" Y="23.404378699313" />
                  <Point X="-3.076451660156" Y="22.396039283146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981451660156" Y="28.759665937074" />
                  <Point X="-2.981451660156" Y="28.564927412707" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981451660156" Y="27.732868192181" />
                  <Point X="-2.981451660156" Y="23.769612566341" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.981451660156" Y="23.508519779033" />
                  <Point X="-2.981451660156" Y="22.450887628041" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886451660156" Y="28.832501525693" />
                  <Point X="-2.886451660156" Y="28.400383083958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886451660156" Y="27.78626701756" />
                  <Point X="-2.886451660156" Y="22.505735972937" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.886451660156" Y="21.331976240773" />
                  <Point X="-2.886451660156" Y="21.095760519863" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791451660156" Y="28.905337114311" />
                  <Point X="-2.791451660156" Y="28.235838755209" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791451660156" Y="27.887346286302" />
                  <Point X="-2.791451660156" Y="22.560584317832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.791451660156" Y="21.496520995443" />
                  <Point X="-2.791451660156" Y="21.022613755126" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696451660156" Y="28.97817270293" />
                  <Point X="-2.696451660156" Y="22.615432662728" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.696451660156" Y="21.661065750112" />
                  <Point X="-2.696451660156" Y="20.95717747696" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601451660156" Y="29.041103682958" />
                  <Point X="-2.601451660156" Y="22.648308043441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.601451660156" Y="21.825610504782" />
                  <Point X="-2.601451660156" Y="20.898355940853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.506451660156" Y="29.093884106493" />
                  <Point X="-2.506451660156" Y="22.643733584709" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.506451660156" Y="21.990155259451" />
                  <Point X="-2.506451660156" Y="20.839534404746" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.411451660156" Y="29.146664530029" />
                  <Point X="-2.411451660156" Y="22.594867071578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.411451660156" Y="22.154700014121" />
                  <Point X="-2.411451660156" Y="20.957091366449" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316451660156" Y="29.199444953564" />
                  <Point X="-2.316451660156" Y="22.44879143618" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.316451660156" Y="22.349695439387" />
                  <Point X="-2.316451660156" Y="21.036512384601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.221451660156" Y="29.2522253771" />
                  <Point X="-2.221451660156" Y="21.099989283613" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.126451660156" Y="29.182187322434" />
                  <Point X="-2.126451660156" Y="21.163466182625" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.031451660156" Y="29.101210510128" />
                  <Point X="-2.031451660156" Y="21.211734707336" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.936451660156" Y="29.05241508641" />
                  <Point X="-1.936451660156" Y="21.223468818351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.841451660156" Y="29.029057077358" />
                  <Point X="-1.841451660156" Y="21.217242102214" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.746451660156" Y="29.044867699434" />
                  <Point X="-1.746451660156" Y="21.211015386077" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.651451660156" Y="29.083845026429" />
                  <Point X="-1.651451660156" Y="21.204788669941" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.556451660156" Y="29.148617982144" />
                  <Point X="-1.556451660156" Y="21.181433303685" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.461451660156" Y="29.567648755223" />
                  <Point X="-1.461451660156" Y="21.116004597353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.366451660156" Y="29.594283128504" />
                  <Point X="-1.366451660156" Y="21.032691120297" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.271451660156" Y="29.620917501784" />
                  <Point X="-1.271451660156" Y="20.949377643242" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.176451660156" Y="29.647551875065" />
                  <Point X="-1.176451660156" Y="20.76934791831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.081451660156" Y="29.674186248345" />
                  <Point X="-1.081451660156" Y="20.253287592232" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.986451660156" Y="29.700820621626" />
                  <Point X="-0.986451660156" Y="20.23339263238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.891451660156" Y="29.720969670418" />
                  <Point X="-0.891451660156" Y="20.583907124036" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.796451660156" Y="29.732088301881" />
                  <Point X="-0.796451660156" Y="20.938452257651" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.701451660156" Y="29.743206933343" />
                  <Point X="-0.701451660156" Y="21.292997391265" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.606451660156" Y="29.754325564805" />
                  <Point X="-0.606451660156" Y="21.589160996159" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.511451660156" Y="29.765444196268" />
                  <Point X="-0.511451660156" Y="21.726036638648" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.416451660156" Y="29.77656282773" />
                  <Point X="-0.416451660156" Y="21.85468508517" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.321451660156" Y="29.619203464432" />
                  <Point X="-0.321451660156" Y="21.913879937111" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.226451660156" Y="29.264658629964" />
                  <Point X="-0.226451660156" Y="21.943581919484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.131451660156" Y="29.128806533872" />
                  <Point X="-0.131451660156" Y="21.973066333718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.036451660156" Y="29.089938726646" />
                  <Point X="-0.036451660156" Y="21.999098049988" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.058548339844" Y="29.092139578797" />
                  <Point X="0.058548339844" Y="21.997620073477" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.153548339844" Y="29.136461225526" />
                  <Point X="0.153548339844" Y="21.970029305958" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.248548339844" Y="29.301856496741" />
                  <Point X="0.248548339844" Y="21.940544907712" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.343548339844" Y="29.656046972932" />
                  <Point X="0.343548339844" Y="21.909848639422" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.438548339844" Y="29.778686962383" />
                  <Point X="0.438548339844" Y="21.843151151307" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.533548339844" Y="29.768737478918" />
                  <Point X="0.533548339844" Y="21.711936426719" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.628548339844" Y="29.758787995454" />
                  <Point X="0.628548339844" Y="21.572671087287" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.723548339844" Y="29.748838511989" />
                  <Point X="0.723548339844" Y="21.256479504864" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818548339844" Y="29.738889028524" />
                  <Point X="0.818548339844" Y="22.073191200971" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.818548339844" Y="20.958125043129" />
                  <Point X="0.818548339844" Y="20.901934274353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.913548339844" Y="29.717225456792" />
                  <Point X="0.913548339844" Y="22.169234273922" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.008548339844" Y="29.69428929986" />
                  <Point X="1.008548339844" Y="22.248224371304" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.103548339844" Y="29.671353142929" />
                  <Point X="1.103548339844" Y="22.312749949649" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.198548339844" Y="29.648416985998" />
                  <Point X="1.198548339844" Y="22.334171726557" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.293548339844" Y="29.625480829066" />
                  <Point X="1.293548339844" Y="22.342913451048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.388548339844" Y="29.602544672135" />
                  <Point X="1.388548339844" Y="22.35165517554" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.483548339844" Y="29.575979954255" />
                  <Point X="1.483548339844" Y="22.345738646856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.578548339844" Y="29.541522366095" />
                  <Point X="1.578548339844" Y="22.304558773848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.673548339844" Y="29.507064777934" />
                  <Point X="1.673548339844" Y="22.243483001729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.768548339844" Y="29.472607189774" />
                  <Point X="1.768548339844" Y="22.18240722961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.863548339844" Y="29.437594403386" />
                  <Point X="1.863548339844" Y="22.084956638702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.958548339844" Y="29.393166410286" />
                  <Point X="1.958548339844" Y="21.961150098586" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053548339844" Y="29.348738417186" />
                  <Point X="2.053548339844" Y="27.573964833689" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053548339844" Y="27.208002621103" />
                  <Point X="2.053548339844" Y="22.626660237856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.053548339844" Y="22.162880430367" />
                  <Point X="2.053548339844" Y="21.83734355847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148548339844" Y="29.304310424086" />
                  <Point X="2.148548339844" Y="27.742773745456" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148548339844" Y="27.083640044513" />
                  <Point X="2.148548339844" Y="22.793013217353" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.148548339844" Y="21.989585108151" />
                  <Point X="2.148548339844" Y="21.713537018354" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243548339844" Y="29.259882430986" />
                  <Point X="2.243548339844" Y="27.907318727802" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243548339844" Y="27.017076800405" />
                  <Point X="2.243548339844" Y="22.866241730183" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.243548339844" Y="21.825040473979" />
                  <Point X="2.243548339844" Y="21.589730478238" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338548339844" Y="29.205326644954" />
                  <Point X="2.338548339844" Y="28.071863710149" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338548339844" Y="26.984230576651" />
                  <Point X="2.338548339844" Y="22.916239471489" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.338548339844" Y="21.660495839807" />
                  <Point X="2.338548339844" Y="21.465923938122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433548339844" Y="29.149979386139" />
                  <Point X="2.433548339844" Y="28.236408692495" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433548339844" Y="26.975420873039" />
                  <Point X="2.433548339844" Y="22.961128147174" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.433548339844" Y="21.495951205636" />
                  <Point X="2.433548339844" Y="21.342117398005" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528548339844" Y="29.094632127324" />
                  <Point X="2.528548339844" Y="28.400953674841" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528548339844" Y="26.990631863224" />
                  <Point X="2.528548339844" Y="22.974324405822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.528548339844" Y="21.331406571464" />
                  <Point X="2.528548339844" Y="21.218310857889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623548339844" Y="29.039284868509" />
                  <Point X="2.623548339844" Y="28.565498657187" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623548339844" Y="27.021718605683" />
                  <Point X="2.623548339844" Y="22.960311010039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.623548339844" Y="21.166861937292" />
                  <Point X="2.623548339844" Y="21.094504317773" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718548339844" Y="28.972482534133" />
                  <Point X="2.718548339844" Y="28.730043639534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718548339844" Y="27.075514650411" />
                  <Point X="2.718548339844" Y="22.943154239608" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.718548339844" Y="21.00231730312" />
                  <Point X="2.718548339844" Y="20.970697777657" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813548339844" Y="28.904924461066" />
                  <Point X="2.813548339844" Y="28.89458862188" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.813548339844" Y="27.130362853311" />
                  <Point X="2.813548339844" Y="22.92077289977" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908548339844" Y="27.185211056211" />
                  <Point X="2.908548339844" Y="23.801957232768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.908548339844" Y="23.361950263329" />
                  <Point X="2.908548339844" Y="22.870631992396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003548339844" Y="27.240059259111" />
                  <Point X="3.003548339844" Y="26.437424759362" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003548339844" Y="26.360495420832" />
                  <Point X="3.003548339844" Y="23.92371729939" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.003548339844" Y="23.215436492208" />
                  <Point X="3.003548339844" Y="22.815783807484" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098548339844" Y="27.294907462011" />
                  <Point X="3.098548339844" Y="26.660249217163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098548339844" Y="26.120250072488" />
                  <Point X="3.098548339844" Y="24.016727216177" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.098548339844" Y="23.128613549314" />
                  <Point X="3.098548339844" Y="22.760935622571" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193548339844" Y="27.349755664911" />
                  <Point X="3.193548339844" Y="26.759625035832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193548339844" Y="26.029983081279" />
                  <Point X="3.193548339844" Y="24.052343505739" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.193548339844" Y="23.055717338544" />
                  <Point X="3.193548339844" Y="22.706087437658" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288548339844" Y="27.404603867811" />
                  <Point X="3.288548339844" Y="26.832520953033" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288548339844" Y="25.980587360313" />
                  <Point X="3.288548339844" Y="24.072991817693" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.288548339844" Y="22.982821127773" />
                  <Point X="3.288548339844" Y="22.651239252745" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="27.459452070711" />
                  <Point X="3.383548339844" Y="26.905416870235" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="25.959986558666" />
                  <Point X="3.383548339844" Y="25.152106382288" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="24.785317004945" />
                  <Point X="3.383548339844" Y="24.090471891098" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.383548339844" Y="22.909924917003" />
                  <Point X="3.383548339844" Y="22.596391067832" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="27.514300273611" />
                  <Point X="3.478548339844" Y="26.978312787436" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="25.951629716291" />
                  <Point X="3.478548339844" Y="25.290553436929" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="24.64656326879" />
                  <Point X="3.478548339844" Y="24.085247142146" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.478548339844" Y="22.837028706232" />
                  <Point X="3.478548339844" Y="22.541542882919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="27.569148476511" />
                  <Point X="3.573548339844" Y="27.051208704637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="25.962400278917" />
                  <Point X="3.573548339844" Y="25.362370645503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="24.575067387369" />
                  <Point X="3.573548339844" Y="24.072740269303" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.573548339844" Y="22.764132495462" />
                  <Point X="3.573548339844" Y="22.486694698006" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="27.623996679411" />
                  <Point X="3.668548339844" Y="27.124104621838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="25.97490731258" />
                  <Point X="3.668548339844" Y="25.413192631794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="24.524436593413" />
                  <Point X="3.668548339844" Y="24.06023339646" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.668548339844" Y="22.691236284691" />
                  <Point X="3.668548339844" Y="22.431846513093" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="27.678844882311" />
                  <Point X="3.763548339844" Y="27.197000539039" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="25.987414346244" />
                  <Point X="3.763548339844" Y="25.440528616847" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="24.496910679362" />
                  <Point X="3.763548339844" Y="24.047726523617" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.763548339844" Y="22.618340073921" />
                  <Point X="3.763548339844" Y="22.37699832818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="27.733693085211" />
                  <Point X="3.858548339844" Y="27.26989645624" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="25.999921379907" />
                  <Point X="3.858548339844" Y="25.465983853831" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="24.471455649758" />
                  <Point X="3.858548339844" Y="24.035219650775" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.858548339844" Y="22.54544386315" />
                  <Point X="3.858548339844" Y="22.322150143268" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="27.762680418203" />
                  <Point X="3.953548339844" Y="27.342792373441" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="26.01242841357" />
                  <Point X="3.953548339844" Y="25.491439090815" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="24.446000620154" />
                  <Point X="3.953548339844" Y="24.022712777932" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.953548339844" Y="22.47254765238" />
                  <Point X="3.953548339844" Y="22.267301958355" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="27.629446803627" />
                  <Point X="4.048548339844" Y="27.415688290642" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="26.024935447234" />
                  <Point X="4.048548339844" Y="25.516894327799" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="24.420545590551" />
                  <Point X="4.048548339844" Y="24.010205905089" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.048548339844" Y="22.399651441609" />
                  <Point X="4.048548339844" Y="22.307520125406" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.143548339844" Y="26.037442480897" />
                  <Point X="4.143548339844" Y="25.542349564783" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.143548339844" Y="24.395090560947" />
                  <Point X="4.143548339844" Y="23.997699032246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.238548339844" Y="26.049949514561" />
                  <Point X="4.238548339844" Y="25.567804801767" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.238548339844" Y="24.369635531343" />
                  <Point X="4.238548339844" Y="23.985192159403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333548339844" Y="26.062456548224" />
                  <Point X="4.333548339844" Y="25.59326003875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.333548339844" Y="24.344180501739" />
                  <Point X="4.333548339844" Y="23.972685286561" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428548339844" Y="26.074963581888" />
                  <Point X="4.428548339844" Y="25.618715275734" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.428548339844" Y="24.318725472135" />
                  <Point X="4.428548339844" Y="23.960178413718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523548339844" Y="26.087470615551" />
                  <Point X="4.523548339844" Y="25.644170512718" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.523548339844" Y="24.293270442532" />
                  <Point X="4.523548339844" Y="23.947671540875" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.618548339844" Y="26.099977649215" />
                  <Point X="4.618548339844" Y="25.669625749702" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.618548339844" Y="24.267815412928" />
                  <Point X="4.618548339844" Y="23.935164668032" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.713548339844" Y="26.07499070908" />
                  <Point X="4.713548339844" Y="25.695080986686" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.713548339844" Y="24.242360383324" />
                  <Point X="4.713548339844" Y="23.922657795189" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.0001640625" Y="25.001625976562" />
                  <Width Value="9.996463867188" />
                  <Height Value="9.978486328125" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.47173928833" Y="21.462140625" />
                  <Point X="0.464196136475" Y="21.4786328125" />
                  <Point X="0.300591094971" Y="21.714357421875" />
                  <Point X="0.274209106445" Y="21.733640625" />
                  <Point X="0.020976716995" Y="21.812234375" />
                  <Point X="-0.008791377068" Y="21.8121953125" />
                  <Point X="-0.262023773193" Y="21.7336015625" />
                  <Point X="-0.283114105225" Y="21.720638671875" />
                  <Point X="-0.288432128906" Y="21.714134765625" />
                  <Point X="-0.45200668335" Y="21.47845703125" />
                  <Point X="-0.459227050781" Y="21.462888671875" />
                  <Point X="-0.847743774414" Y="20.012923828125" />
                  <Point X="-1.100244140625" Y="20.06193359375" />
                  <Point X="-1.35158972168" Y="20.126603515625" />
                  <Point X="-1.309181274414" Y="20.4487265625" />
                  <Point X="-1.309713500977" Y="20.465392578125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.380672241211" Y="20.791587890625" />
                  <Point X="-1.386504760742" Y="20.797564453125" />
                  <Point X="-1.619543457031" Y="21.001935546875" />
                  <Point X="-1.649432128906" Y="21.014248046875" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.990007568359" Y="21.026123046875" />
                  <Point X="-2.247844970703" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.457094238281" Y="20.5855" />
                  <Point X="-2.855833251953" Y="20.832388671875" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.506406005859" Y="22.370234375" />
                  <Point X="-2.50563671875" Y="22.37160546875" />
                  <Point X="-2.499826904297" Y="22.402822265625" />
                  <Point X="-2.513988525391" Y="22.431244140625" />
                  <Point X="-2.531328369141" Y="22.448583984375" />
                  <Point X="-2.560157226562" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.842959228516" Y="21.73410546875" />
                  <Point X="-4.161705078125" Y="22.15287109375" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.164498779297" Y="23.57630859375" />
                  <Point X="-3.163847412109" Y="23.576806640625" />
                  <Point X="-3.148444335938" Y="23.59641796875" />
                  <Point X="-3.145818115234" Y="23.604" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.136545654297" Y="23.64799609375" />
                  <Point X="-3.145109130859" Y="23.67429296875" />
                  <Point X="-3.161173095703" Y="23.689369140625" />
                  <Point X="-3.187642333984" Y="23.704947265625" />
                  <Point X="-3.219529296875" Y="23.711427734375" />
                  <Point X="-4.803283203125" Y="23.502921875" />
                  <Point X="-4.927392578125" Y="23.988806640625" />
                  <Point X="-4.998395996094" Y="24.485255859375" />
                  <Point X="-3.558205810547" Y="24.871154296875" />
                  <Point X="-3.541882324219" Y="24.878583984375" />
                  <Point X="-3.514143310547" Y="24.8978359375" />
                  <Point X="-3.497806152344" Y="24.91680078125" />
                  <Point X="-3.494894287109" Y="24.924107421875" />
                  <Point X="-3.485647949219" Y="24.9538984375" />
                  <Point X="-3.48565234375" Y="24.9835546875" />
                  <Point X="-3.494898681641" Y="25.01334765625" />
                  <Point X="-3.514174560547" Y="25.039623046875" />
                  <Point X="-3.541894287109" Y="25.05886328125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.917645019531" Y="25.99641796875" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-3.753798339844" Y="26.394048828125" />
                  <Point X="-3.753112548828" Y="26.3939609375" />
                  <Point X="-3.731686523438" Y="26.3959140625" />
                  <Point X="-3.731487304688" Y="26.395935546875" />
                  <Point X="-3.670279052734" Y="26.415234375" />
                  <Point X="-3.651704101563" Y="26.42590234375" />
                  <Point X="-3.639297363281" Y="26.44336328125" />
                  <Point X="-3.639107421875" Y="26.443814453125" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610704101562" Y="26.52436328125" />
                  <Point X="-3.616094726562" Y="26.54508203125" />
                  <Point X="-3.616331054688" Y="26.545541015625" />
                  <Point X="-3.646055908203" Y="26.602640625" />
                  <Point X="-3.659968261719" Y="26.619220703125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.160016601562" Y="27.787001953125" />
                  <Point X="-3.774670410156" Y="28.282310546875" />
                  <Point X="-3.159568359375" Y="27.927181640625" />
                  <Point X="-3.159241943359" Y="27.9269921875" />
                  <Point X="-3.138709472656" Y="27.920451171875" />
                  <Point X="-3.138471923828" Y="27.9204296875" />
                  <Point X="-3.052965332031" Y="27.91294921875" />
                  <Point X="-3.031986816406" Y="27.915603515625" />
                  <Point X="-3.013980957031" Y="27.926689453125" />
                  <Point X="-3.013221679688" Y="27.92743359375" />
                  <Point X="-2.952528564453" Y="27.988126953125" />
                  <Point X="-2.940918701172" Y="28.006328125" />
                  <Point X="-2.938064697266" Y="28.027728515625" />
                  <Point X="-2.938077880859" Y="28.0278828125" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.307278564453" Y="28.749275390625" />
                  <Point X="-2.75287109375" Y="29.174333984375" />
                  <Point X="-2.141548583984" Y="29.51397265625" />
                  <Point X="-1.967927124023" Y="29.287703125" />
                  <Point X="-1.967651000977" Y="29.287345703125" />
                  <Point X="-1.951688598633" Y="29.27389453125" />
                  <Point X="-1.951199462891" Y="29.27363671875" />
                  <Point X="-1.856030883789" Y="29.22409375" />
                  <Point X="-1.837466796875" Y="29.2186484375" />
                  <Point X="-1.813759277344" Y="29.222271484375" />
                  <Point X="-1.714635009766" Y="29.263330078125" />
                  <Point X="-1.696936157227" Y="29.2757109375" />
                  <Point X="-1.686110839844" Y="29.294400390625" />
                  <Point X="-1.686067138672" Y="29.2945390625" />
                  <Point X="-1.653803833008" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.689137573242" Y="29.701138671875" />
                  <Point X="-0.968078491211" Y="29.903298828125" />
                  <Point X="-0.224200042725" Y="29.990359375" />
                  <Point X="-0.042236881256" Y="29.311263671875" />
                  <Point X="-0.02438120842" Y="29.2845390625" />
                  <Point X="0.006053043365" Y="29.274205078125" />
                  <Point X="0.036489959717" Y="29.28453125" />
                  <Point X="0.054352401733" Y="29.311251953125" />
                  <Point X="0.236637786865" Y="29.990869140625" />
                  <Point X="0.860216796875" Y="29.9255625" />
                  <Point X="1.508466064453" Y="29.7690546875" />
                  <Point X="1.931048217773" Y="29.615779296875" />
                  <Point X="2.33869140625" Y="29.425138671875" />
                  <Point X="2.732510253906" Y="29.19569921875" />
                  <Point X="3.068739746094" Y="28.956591796875" />
                  <Point X="2.230281738281" Y="27.50433984375" />
                  <Point X="2.224841308594" Y="27.49147265625" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202048828125" Y="27.392291015625" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218757080078" Y="27.300818359375" />
                  <Point X="2.218703613281" Y="27.30078125" />
                  <Point X="2.261639892578" Y="27.23750390625" />
                  <Point X="2.274970703125" Y="27.22418359375" />
                  <Point X="2.338247802734" Y="27.18124609375" />
                  <Point X="2.360382080078" Y="27.173060546875" />
                  <Point X="2.360371582031" Y="27.172974609375" />
                  <Point X="2.42976171875" Y="27.164607421875" />
                  <Point X="2.448704589844" Y="27.16595703125" />
                  <Point X="2.528950683594" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.994247070312" Y="28.031431640625" />
                  <Point X="4.202593261719" Y="27.741876953125" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.289183349609" Y="26.593517578125" />
                  <Point X="3.279342285156" Y="26.583794921875" />
                  <Point X="3.221589111328" Y="26.508451171875" />
                  <Point X="3.213108886719" Y="26.4914609375" />
                  <Point X="3.191595703125" Y="26.41453515625" />
                  <Point X="3.190762207031" Y="26.391048828125" />
                  <Point X="3.190788330078" Y="26.390921875" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215670654297" Y="26.287916015625" />
                  <Point X="3.263704345703" Y="26.214908203125" />
                  <Point X="3.280932373047" Y="26.198828125" />
                  <Point X="3.280982177734" Y="26.19880078125" />
                  <Point X="3.350589599609" Y="26.1596171875" />
                  <Point X="3.368612792969" Y="26.15361328125" />
                  <Point X="3.4627265625" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.939187988281" Y="25.951388671875" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="3.741817871094" Y="25.238001953125" />
                  <Point X="3.729041015625" Y="25.23279296875" />
                  <Point X="3.636577148438" Y="25.17934765625" />
                  <Point X="3.622237548828" Y="25.166890625" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556980712891" Y="25.074685546875" />
                  <Point X="3.556962402344" Y="25.074615234375" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.5384921875" Y="24.959267578125" />
                  <Point X="3.556985107422" Y="24.862705078125" />
                  <Point X="3.566758789062" Y="24.841240234375" />
                  <Point X="3.566786376953" Y="24.841205078125" />
                  <Point X="3.622264648438" Y="24.77051171875" />
                  <Point X="3.636622802734" Y="24.75806640625" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.948432128906" Y="24.0336015625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.412736083984" Y="23.902271484375" />
                  <Point X="3.394745361328" Y="23.901638671875" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.201515136719" Y="23.858048828125" />
                  <Point X="3.185390380859" Y="23.845236328125" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064345214844" Y="23.68579296875" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056427246094" Y="23.4832734375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="4.339073730469" Y="22.41621484375" />
                  <Point X="4.204134277344" Y="22.197861328125" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="2.754125976562" Y="22.740396484375" />
                  <Point X="2.737232666016" Y="22.746705078125" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.488988037109" Y="22.78070703125" />
                  <Point X="2.309559814453" Y="22.686275390625" />
                  <Point X="2.288552490234" Y="22.6652265625" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189182373047" Y="22.453515625" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.986673095703" Y="20.917912109375" />
                  <Point X="2.835301757812" Y="20.809791015625" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="1.683696044922" Y="22.007236328125" />
                  <Point X="1.670442626953" Y="22.0196015625" />
                  <Point X="1.45742578125" Y="22.15655078125" />
                  <Point X="1.425688354492" Y="22.164271484375" />
                  <Point X="1.192718139648" Y="22.142833984375" />
                  <Point X="1.171190063477" Y="22.1356875" />
                  <Point X="1.164966918945" Y="22.131185546875" />
                  <Point X="0.985349121094" Y="21.981837890625" />
                  <Point X="0.968429870605" Y="21.953888671875" />
                  <Point X="0.914642578125" Y="21.70642578125" />
                  <Point X="0.913930053711" Y="21.68927734375" />
                  <Point X="1.127642211914" Y="20.065970703125" />
                  <Point X="0.994338928223" Y="20.036751953125" />
                  <Point X="0.860200500488" Y="20.0123828125" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#116" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-9.6622281E-05" Y="4.355485072851" Z="0" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0" />
                  <Point X="-0.983568725817" Y="4.983827131987" Z="0" />
                  <Point X="-0.983964085579" Y="4.983780860901" />
                  <Point X="-1.75013969171" Y="4.768972089767" Z="0" />
                  <Point X="-1.750522971153" Y="4.768864631653" />
                  <Point X="-1.750501532614" Y="4.768701789618" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0" />
                  <Point X="-1.739909006476" Y="4.340855288267" Z="0" />
                  <Point X="-1.739925146103" Y="4.340804100037" />
                  <Point X="-1.839049462616" Y="4.299745118618" Z="0" />
                  <Point X="-1.839099049568" Y="4.299724578857" />
                  <Point X="-1.934267596543" Y="4.349266541243" Z="0" />
                  <Point X="-1.93431520462" Y="4.349291324615" />
                  <Point X="-1.934415191889" Y="4.349421630621" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0" />
                  <Point X="-2.786204850912" Y="4.24771355319" Z="0" />
                  <Point X="-2.786530971527" Y="4.247532367706" />
                  <Point X="-3.378373192787" Y="3.793773431182" Z="0" />
                  <Point X="-3.378669261932" Y="3.793546438217" />
                  <Point X="-3.378480689645" Y="3.793219821453" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0" />
                  <Point X="-2.994043754697" Y="3.054806330204" Z="0" />
                  <Point X="-2.99404001236" Y="3.054763555527" />
                  <Point X="-3.054733193755" Y="2.994070374131" Z="0" />
                  <Point X="-3.054763555527" Y="2.99404001236" />
                  <Point X="-3.140270135403" Y="3.001520944476" Z="0" />
                  <Point X="-3.14031291008" Y="3.001524686813" />
                  <Point X="-3.140639526844" Y="3.001713259101" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0" />
                  <Point X="-4.207468392491" Y="2.84663088882" Z="0" />
                  <Point X="-4.207675457001" Y="2.846364736557" />
                  <Point X="-4.547484162569" Y="2.264191035628" Z="0" />
                  <Point X="-4.547654151917" Y="2.263899803162" />
                  <Point X="-4.547228605986" Y="2.263573270321" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0" />
                  <Point X="-3.6668373034" Y="1.553732945859" Z="0" />
                  <Point X="-3.666822433472" Y="1.553704380989" />
                  <Point X="-3.691457584023" Y="1.494229862571" Z="0" />
                  <Point X="-3.691469907761" Y="1.494200110435" />
                  <Point X="-3.752865357399" Y="1.474842218578" Z="0" />
                  <Point X="-3.75289607048" Y="1.47483253479" />
                  <Point X="-3.753427870393" Y="1.474902547598" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0" />
                  <Point X="-4.972752604961" Y="1.038223003507" Z="0" />
                  <Point X="-4.9728307724" Y="1.037934541702" />
                  <Point X="-5.06028330493" Y="0.446938670442" Z="0" />
                  <Point X="-5.06032705307" Y="0.446643024683" />
                  <Point X="-5.059585169911" Y="0.446444237687" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0" />
                  <Point X="-3.548821735859" Y="0.029816582101" Z="0" />
                  <Point X="-3.548807859421" Y="0.029806951061" />
                  <Point X="-3.539561367273" Y="1.4903476E-05" Z="0" />
                  <Point X="-3.539556741714" Y="0" />
                  <Point X="-3.548803233862" Y="-0.029792047585" Z="0" />
                  <Point X="-3.548807859421" Y="-0.029806951061" />
                  <Point X="-3.576546859264" Y="-0.049059401033" Z="0" />
                  <Point X="-3.576560735703" Y="-0.049069032073" />
                  <Point X="-3.577302618861" Y="-0.049267819069" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0" />
                  <Point X="-4.982700556755" Y="-0.989399299189" Z="0" />
                  <Point X="-4.982661724091" Y="-0.989670813084" />
                  <Point X="-4.846971203327" Y="-1.520894142777" Z="0" />
                  <Point X="-4.846903324127" Y="-1.521159887314" />
                  <Point X="-4.846089451909" Y="-1.521052739024" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0" />
                  <Point X="-3.192689547062" Y="-1.322442010105" Z="0" />
                  <Point X="-3.192676305771" Y="-1.322449803352" />
                  <Point X="-3.200376949906" Y="-1.352182296574" Z="0" />
                  <Point X="-3.200380802155" Y="-1.352197170258" />
                  <Point X="-3.201032062054" Y="-1.352696899533" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0" />
                  <Point X="-4.209501650333" Y="-2.843640974998" Z="0" />
                  <Point X="-4.209354877472" Y="-2.843887090683" />
                  <Point X="-3.862412441969" Y="-3.299698023081" Z="0" />
                  <Point X="-3.862238883972" Y="-3.299926042557" />
                  <Point X="-3.861594184279" Y="-3.299553825021" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0" />
                  <Point X="-2.555499644876" Y="-2.572830824256" Z="0" />
                  <Point X="-2.555490970612" Y="-2.57283949852" />
                  <Point X="-2.555863188148" Y="-2.573484198213" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0" />
                  <Point X="-2.890680236816" Y="-4.177344523668" Z="0" />
                  <Point X="-2.890475511551" Y="-4.177502155304" />
                  <Point X="-2.451337432861" Y="-4.449405549049" Z="0" />
                  <Point X="-2.451117753983" Y="-4.449541568756" />
                  <Point X="-2.451005306244" Y="-4.449395024061" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0" />
                  <Point X="-1.968384979844" Y="-3.984170551658" Z="0" />
                  <Point X="-1.968255996704" Y="-3.984084367752" />
                  <Point X="-1.65882164371" Y="-4.004365788817" Z="0" />
                  <Point X="-1.658666849136" Y="-4.004375934601" />
                  <Point X="-1.425522609591" Y="-4.208838223696" Z="0" />
                  <Point X="-1.425405979156" Y="-4.208940505981" />
                  <Point X="-1.364908798993" Y="-4.513080085278" Z="0" />
                  <Point X="-1.364878535271" Y="-4.51323223114" />
                  <Point X="-1.364902645528" Y="-4.51341536665" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0" />
                  <Point X="-1.117549686909" Y="-4.955545532227" Z="0" />
                  <Point X="-1.117401838303" Y="-4.955583572388" />
                  <Point X="-0.817817947209" Y="-5.013733861208" Z="0" />
                  <Point X="-0.81766808033" Y="-5.013762950897" />
                  <Point X="-0.81746778968" Y="-5.013015455961" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0" />
                  <Point X="-0.253440943211" Y="-3.28299062705" Z="0" />
                  <Point X="-0.253359079361" Y="-3.282872676849" />
                  <Point X="-0.00012667954" Y="-3.204278685093" Z="0" />
                  <Point X="-1E-12" Y="-3.204239368439" />
                  <Point X="0.253232399821" Y="-3.282833360195" Z="0" />
                  <Point X="0.253359079361" Y="-3.282872676849" />
                  <Point X="0.417004916221" Y="-3.518655128717" Z="0" />
                  <Point X="0.417086780071" Y="-3.518773078918" />
                  <Point X="0.417287070721" Y="-3.519520573854" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0" />
                  <Point X="0.9979199875" Y="-4.981017757893" Z="0" />
                  <Point X="0.998010158539" Y="-4.981001377106" />
                  <Point X="1.176963453293" Y="-4.941774917841" Z="0" />
                  <Point X="1.177052974701" Y="-4.9417552948" />
                  <Point X="1.176943121105" Y="-4.940920874" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0" />
                  <Point X="1.011133108944" Y="-3.025449807405" Z="0" />
                  <Point X="1.01116001606" Y="-3.025326013565" />
                  <Point X="1.191053759277" Y="-2.875750265121" Z="0" />
                  <Point X="1.191143751144" Y="-2.875675439835" />
                  <Point X="1.424113913" Y="-2.854237313628" Z="0" />
                  <Point X="1.424230456352" Y="-2.854226589203" />
                  <Point X="1.637247335374" Y="-2.991176760435" Z="0" />
                  <Point X="1.637353897095" Y="-2.991245269775" />
                  <Point X="1.637866243482" Y="-2.99191297245" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0" />
                  <Point X="2.85717596519" Y="-4.200347187519" Z="0" />
                  <Point X="2.857273578644" Y="-4.200284004211" />
                  <Point X="3.046417984247" Y="-4.06518255949" Z="0" />
                  <Point X="3.04651260376" Y="-4.065114974976" />
                  <Point X="3.046124965906" Y="-4.064443566799" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0" />
                  <Point X="2.232230625868" Y="-2.506316226602" Z="0" />
                  <Point X="2.232211112976" Y="-2.506208181381" />
                  <Point X="2.326642957449" Y="-2.326779955983" Z="0" />
                  <Point X="2.326690196991" Y="-2.326690196991" />
                  <Point X="2.506118422389" Y="-2.232258352518" Z="0" />
                  <Point X="2.506208181381" Y="-2.232211112976" />
                  <Point X="2.722190576911" Y="-2.271217383623" Z="0" />
                  <Point X="2.722298622131" Y="-2.271236896515" />
                  <Point X="2.722970030308" Y="-2.271624534369" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0" />
                  <Point X="4.239634948254" Y="-2.798543760419" Z="0" />
                  <Point X="4.239722251892" Y="-2.798419713974" />
                  <Point X="4.399128802538" Y="-2.540475897312" Z="0" />
                  <Point X="4.399208545685" Y="-2.540346860886" />
                  <Point X="4.398605312824" Y="-2.53988398397" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0" />
                  <Point X="3.092312746644" Y="-1.458380643129" Z="0" />
                  <Point X="3.092262506485" Y="-1.458302497864" />
                  <Point X="3.10798359251" Y="-1.287457696617" Z="0" />
                  <Point X="3.107991456985" Y="-1.287372231483" />
                  <Point X="3.21768065691" Y="-1.155450176954" Z="0" />
                  <Point X="3.217735528946" Y="-1.15538418293" />
                  <Point X="3.399209077835" Y="-1.115940156341" Z="0" />
                  <Point X="3.399299860001" Y="-1.115920424461" />
                  <Point X="3.400053713441" Y="-1.116019671142" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0" />
                  <Point X="4.991396173" Y="-0.94460777697" Z="0" />
                  <Point X="4.991438388824" Y="-0.944422781467" />
                  <Point X="5.047986829281" Y="-0.569348926634" Z="0" />
                  <Point X="5.048015117645" Y="-0.569161295891" />
                  <Point X="5.047365086436" Y="-0.568987120561" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0" />
                  <Point X="3.655488969684" Y="-0.167364943579" Z="0" />
                  <Point X="3.655442714691" Y="-0.167338207364" />
                  <Point X="3.599964476705" Y="-0.096645895049" Z="0" />
                  <Point X="3.599936723709" Y="-0.096610531211" />
                  <Point X="3.581443977714" Y="-4.8305305E-05" Z="0" />
                  <Point X="3.581434726715" Y="-3.9E-11" />
                  <Point X="3.599927472711" Y="0.096562225945" Z="0" />
                  <Point X="3.599936723709" Y="0.096610531211" />
                  <Point X="3.655414961696" Y="0.167302843526" Z="0" />
                  <Point X="3.655442714691" Y="0.167338207364" />
                  <Point X="3.747906444669" Y="0.220783900663" Z="0" />
                  <Point X="3.747952699661" Y="0.220810636878" />
                  <Point X="3.74860273087" Y="0.220984812208" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0" />
                  <Point X="4.982145835638" Y="0.992228941679" Z="0" />
                  <Point X="4.982112884521" Y="0.992440581322" />
                  <Point X="4.88083612895" Y="1.408455042362" Z="0" />
                  <Point X="4.88078546524" Y="1.408663153648" />
                  <Point X="4.880076633215" Y="1.408569834113" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0" />
                  <Point X="3.36900769937" Y="1.234462357044" Z="0" />
                  <Point X="3.368960618973" Y="1.234468579292" />
                  <Point X="3.299353373051" Y="1.273651429772" Z="0" />
                  <Point X="3.299318552017" Y="1.273671030998" />
                  <Point X="3.251285024643" Y="1.346679881275" Z="0" />
                  <Point X="3.251260995865" Y="1.346716403961" />
                  <Point X="3.233600867867" Y="1.432306388617" Z="0" />
                  <Point X="3.233592033386" Y="1.432349205017" />
                  <Point X="3.255105223894" Y="1.509275220335" Z="0" />
                  <Point X="3.25511598587" Y="1.509313702583" />
                  <Point X="3.312869267941" Y="1.584657410085" Z="0" />
                  <Point X="3.312898159027" Y="1.584695100784" />
                  <Point X="3.313465365887" Y="1.5851303339" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0" />
                  <Point X="4.238288539171" Y="2.800574601531" Z="0" />
                  <Point X="4.23818397522" Y="2.800747394562" />
                  <Point X="4.002379123211" Y="3.128462645411" Z="0" />
                  <Point X="4.002261161804" Y="3.128626585007" />
                  <Point X="4.001514970183" Y="3.128195771098" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0" />
                  <Point X="2.429631798863" Y="2.245539909601" Z="0" />
                  <Point X="2.429591655731" Y="2.245529174805" />
                  <Point X="2.360201500535" Y="2.253896342278" Z="0" />
                  <Point X="2.360166788101" Y="2.253900527954" />
                  <Point X="2.296889726639" Y="2.296836593509" Z="0" />
                  <Point X="2.296858072281" Y="2.296858072281" />
                  <Point X="2.253922006726" Y="2.360135133743" Z="0" />
                  <Point X="2.253900527954" Y="2.360166788101" />
                  <Point X="2.245533360481" Y="2.429556943297" Z="0" />
                  <Point X="2.245529174805" Y="2.429591655731" />
                  <Point X="2.266988033056" Y="2.509837777019" Z="0" />
                  <Point X="2.266998767853" Y="2.509877920151" />
                  <Point X="2.267429581761" Y="2.510624111772" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0" />
                  <Point X="2.753685403347" Y="4.268898616076" Z="0" />
                  <Point X="2.753497838974" Y="4.269032001495" />
                  <Point X="2.355961587667" Y="4.500637542009" Z="0" />
                  <Point X="2.355762720108" Y="4.50075340271" />
                  <Point X="1.944237104297" Y="4.693210169554" Z="0" />
                  <Point X="1.944031238556" Y="4.693306446075" />
                  <Point X="1.516951227129" Y="4.848211521626" Z="0" />
                  <Point X="1.516737580299" Y="4.848289012909" />
                  <Point X="0.86288575235" Y="5.006149000645" Z="0" />
                  <Point X="0.862558662891" Y="5.006227970123" />
                  <Point X="0.193579218604" Y="5.076287984848" Z="0" />
                  <Point X="0.193244561553" Y="5.076323032379" />
                  <Point X="0.193147939272" Y="5.0759624331" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>