<?xml version="1.0" encoding="utf-16"?>
<ModelData>
  <Header>
    <ApplicationVersion Value="1.6.1.0" />
  </Header>
  <Definitions>
    <Layers />
    <HardwareParameter>
      <OpticParameter>
        <Lens Value="" />
        <UseVariablePolygonDelay Value="False" />
        <VariablePolygonDelayFile Value="" />
        <MarkingAreaWidth Value="101.6" />
        <MarkingAreaHeight Value="101.6" />
        <HomePositionEnabled Value="True" />
        <HomePositionX Value="0" />
        <HomePositionY Value="0" />
        <UseSecondScanHead Value="False" />
        <UseIntelliWeld Value="False" />
        <HPSActive Value="False" />
        <HPSPreviewTime Value="1200" />
        <HPSLimitOfAcceleration Value="0" />
        <HPSSpeedLimit Value="0" />
        <TimelagHead1 Value="0" />
        <TimelagHead2 Value="0" />
      </OpticParameter>
      <ScanHeads>
        <ScanHead>
          <LaserCorrectionFile Value="C:\Program Files\ScanLab\laserDesk\calibration\D3_2397.ct5" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="-0.65" />
          <OriginShiftY Value="0.35" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="True" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
        <ScanHead>
          <LaserCorrectionFile Value="" />
          <OpticCalibrationFactorX Value="0" />
          <OpticCalibrationFactorY Value="0" />
          <OpticCalibrationFactorZ Value="0" />
          <UsePilotLaserCorrectionFile Value="False" />
          <PilotLaserCorrectionFile Value="" />
          <UsePilotLaserCalibrationFactor Value="False" />
          <PilotLaserCalibrationFactorX Value="0" />
          <PilotLaserCalibrationFactorY Value="0" />
          <PilotLaserCalibrationFactorZ Value="0" />
          <OriginShiftX Value="0" />
          <OriginShiftY Value="0" />
          <OriginShiftZ Value="0" />
          <CoordinateSystemRotationAngle Value="0" />
          <CoordinateSystemMirrorX Value="False" />
          <CoordinateSystemMirrorY Value="False" />
          <ScanHeadMonitoringTemperature Value="False" />
          <ScanHeadMonitoringPower Value="False" />
        </ScanHead>
      </ScanHeads>
      <LaserParameter>
        <LaserType Value="GeneralType" />
        <CalibrationFactorOfPower Value="1" />
        <PowerChangeDelay Value="0.05" />
        <ShutterOpenDelay Value="0" />
        <ShutterCloseDelay Value="0" />
        <AutomaticShutterSwitch Value="False" />
        <StandbyPower Value="0" />
        <DelayToStandby Value="-1" />
        <QSwitchDelay Value="0" />
        <MarkingResolution Value="0.05" />
        <SerialPort Value="NoCOM" />
      </LaserParameter>
      <MOFParameter>
        <Type Value="X" />
        <CalibrationFactorX Value="1" />
        <CalibrationFactorY Value="1" />
        <CalibrationFactorRotation Value="1" />
        <RotationCenter X="0" Y="0" />
        <SimulateEncoder Value="False" />
        <CheckWorkingField Value="True" />
        <TrackingErrorCompensation Value="0" />
      </MOFParameter>
      <MiscParameter>
        <IODefault Value="0" />
        <ApplyIOInitialisation Value="True" />
        <MissedJobEndBreak Value="False" />
        <IOActivationOfAutomation Value="False" />
      </MiscParameter>
      <HardwareDevicesParameter>
        <RemoteConnectionType Value="2" />
        <RemoteSerialConnectionName Value="" />
        <RemoteTCPServerPort Value="3000" />
        <MotorControlAxisParameters />
      </HardwareDevicesParameter>
      <PCInterfacesParameter>
        <SerialPorts />
      </PCInterfacesParameter>
      <SSEIVisionParameter>
        <SSEIDLLPath Value="&lt;default&gt;" />
        <SSEIConfigPath Value="&lt;default&gt;" />
        <ServerIP Value="127.0.0.1" />
        <ServerPort Value="22222" />
        <Timeout Value="30000" />
        <LogSSEIConnection Value="False" />
      </SSEIVisionParameter>
    </HardwareParameter>
  </Definitions>
  <LibraryElement>
    <UID Value="G#31" />
    <Label Value="Library" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <HardwareParameters />
    <MarkingParameters>
      <MarkingParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="10%-power">
        <Label Value="10%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="10" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100%-power">
        <Label Value="100%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44 / 0.65m/s">
        <Label Value="100W - 27.44 / 0.65m/s" />
        <Speed Value="0.65" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="100W - 27.44% / 0.5 m/s">
        <Label Value="100W - 27.44% / 0.5 m/s" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="27.44" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="120W - 31.05% / 0.75m/s ">
        <Label Value="120W - 31.05% / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="31.05" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="160W - 38.26 / 0.75m/s">
        <Label Value="160W - 38.26 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="38.26" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="180W - 41.86 / 0.75m/s ">
        <Label Value="180W - 41.86 / 0.75m/s " />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="41.86" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="190W - 43.66 / 0.75m/s">
        <Label Value="190W - 43.66 / 0.75m/s" />
        <Speed Value="0.75" />
        <JumpSpeed Value="20" />
        <Power Value="43.66" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="300" />
        <DelayAfterMark Value="200" />
        <DelayInPolygon Value="50" />
        <DelayLaserOn Value="100" />
        <DelayLaserOff Value="1" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="400" />
        <SkyWritingLaserOnShift Value="100" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="20%-power">
        <Label Value="20%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="30%-power">
        <Label Value="30%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="30" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="40%-power">
        <Label Value="40%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="40" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="50%-power">
        <Label Value="50%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="60%-power">
        <Label Value="60%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="60" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="70%-power">
        <Label Value="70%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="70" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="80%-power">
        <Label Value="80%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="80" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="90%-power">
        <Label Value="90%-power" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="90" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="no-power">
        <Label Value="no-power" />
        <Speed Value="0.5" />
        <JumpSpeed Value="20" />
        <Power Value="0" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-100W-26.738% / 0.015m/s">
        <Label Value="powertest-100W-26.738% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="26.738" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-150W-39.2% / 0.015m/s">
        <Label Value="powertest-150W-39.2% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="39.2" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-200W-48.46% / 0.015m/s">
        <Label Value="powertest-200W-48.46% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="48.46" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-250W-57.72% / 0.015m/s">
        <Label Value="powertest-250W-57.72% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="57.72" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-300W-66.97% / 0.015m/s">
        <Label Value="powertest-300W-66.97% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="66.97" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-350W-76.23% / 0.015m/s">
        <Label Value="powertest-350W-76.23% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="76.23" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-400W-85.49% / 0.015m/s">
        <Label Value="powertest-400W-85.49% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="85.49" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="powertest-50W-20.68% / 0.015m/s">
        <Label Value="powertest-50W-20.68% / 0.015m/s" />
        <Speed Value="0.015" />
        <JumpSpeed Value="20" />
        <Power Value="20.68" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="300" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="90" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_1 (IPGBaseline+skywr)">
        <Label Value="Skywriting_test1_1 (IPGBaseline+skywr)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_10 (IPGBaseline)">
        <Label Value="Skywriting_test1_10 (IPGBaseline)" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode1" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_2">
        <Label Value="Skywriting_test1_2" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="392" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_3">
        <Label Value="Skywriting_test1_3" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="168" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_4">
        <Label Value="Skywriting_test1_4" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-413" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_5">
        <Label Value="Skywriting_test1_5" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-177" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_6">
        <Label Value="Skywriting_test1_6" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="530" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_7">
        <Label Value="Skywriting_test1_7" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="230" />
        <SkyWritingOverrun Value="250" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_8">
        <Label Value="Skywriting_test1_8" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="350" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
      <MarkingParameter Name="Skywriting_test1_9">
        <Label Value="Skywriting_test1_9" />
        <Speed Value="1.3" />
        <JumpSpeed Value="20" />
        <Power Value="50" />
        <Frequency Value="50" />
        <PulseLength Value="20" />
        <InitialImpulseSuppression Value="100" />
        <PulsedModeActive Value="False" />
        <DelayAfterJump Value="500" />
        <DelayAfterMark Value="250" />
        <DelayInPolygon Value="120" />
        <DelayLaserOn Value="150" />
        <DelayLaserOff Value="50" />
        <WobbleMode Value="Off" />
        <WobbleFrequency Value="0" />
        <WobbleAmplitudeLongitudinal Value="0" />
        <WobbleAmplitudeTransversal Value="0" />
        <SkyWritingMode Value="Mode3" />
        <SkyWritingTimeLag Value="280" />
        <SkyWritingLaserOnShift Value="-295" />
        <SkyWritingForerun Value="380" />
        <SkyWritingOverrun Value="150" />
        <SkyWritingAngleLimit Value="0" />
      </MarkingParameter>
    </MarkingParameters>
    <HatchParameters>
      <HatchParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="45-1-0.080">
        <Label Value="45-1-0.080" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="1" />
        <Angle Value="215" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="80um Hatch">
        <Label Value="80um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="85um Hatch">
        <Label Value="85um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.085" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch_Rev2">
        <Label Value="90um Hatch_Rev2" />
        <FillingType Value="2" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.045" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um Hatch-BAF">
        <Label Value="90um Hatch-BAF" />
        <FillingType Value="0" />
        <Distance Value="0.09" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.15" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Miter" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="True" />
        <SearchRadius Value="0.5" />
      </HatchParameter>
      <HatchParameter Name="90um-Borders--BAF">
        <Label Value="90um-Borders--BAF" />
        <FillingType Value="1" />
        <Distance Value="0.08" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <MinimalJump Value="0.04" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0.04" />
        <Beamcomp Value="0.08" />
        <NumberOfLoops Value="1" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="True" />
        <ReverseOrderOfLoops Value="True" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="True" />
        <Sort Value="True" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch">
        <Label Value="95um Hatch" />
        <FillingType Value="0" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="95um Hatch w/ 2 borders">
        <Label Value="95um Hatch w/ 2 borders" />
        <FillingType Value="2" />
        <Distance Value="0.095" />
        <HatchAngle Value="Relative" />
        <AngleCount Value="1" />
        <Angle Value="270" />
        <AngleStep Value="67" />
        <LineReduction Value="0" />
        <OutlineReduction Value="0.05" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="2" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
      <HatchParameter Name="IPGBaseline">
        <Label Value="IPGBaseline" />
        <FillingType Value="0" />
        <Distance Value="0.08" />
        <HatchAngle Value="Absolute" />
        <AngleCount Value="2" />
        <Angle Value="180" />
        <AngleStep Value="90" />
        <LineReduction Value="0.08" />
        <OutlineReduction Value="0" />
        <Beamcomp Value="NaN" />
        <NumberOfLoops Value="0" />
        <LineJoinType Value="Round" />
        <JoinFirstLinesRound Value="False" />
        <ReverseOrderOfLoops Value="False" />
        <Tolerance Value="0.05" />
        <SegmentCount Value="2" />
        <JumpReduction Value="False" />
        <Sort Value="False" />
        <SearchRadius Value="0.1" />
      </HatchParameter>
    </HatchParameters>
    <BitmapParameters>
      <BitmapParameter Name="&lt;default&gt;">
        <Label Value="&lt;default&gt;" />
        <PixelDistance Value="100" />
        <DitheringProcedure Value="None" />
        <BitmapCalibrationTable>
          <BitmapCalibrationValue>
            <GreyValue Value="0" />
            <Power Value="100" />
            <PulseLength Value="0" />
          </BitmapCalibrationValue>
          <BitmapCalibrationValue>
            <GreyValue Value="255" />
            <Power Value="100" />
            <PulseLength Value="10" />
          </BitmapCalibrationValue>
        </BitmapCalibrationTable>
        <DefaultPixelValue Value="0" />
        <BlackValue Value="255" />
        <Bidirectional Value="False" />
        <RowSynchronization Value="0" />
        <PixelMode Value="Standard" />
      </BitmapParameter>
    </BitmapParameters>
    <FontsElement />
    <ShapesElement />
  </LibraryElement>
  <JobElement>
    <Label Value="Job" />
    <Area Value="True" />
    <MarkingParameterReference Value="&lt;default&gt;" />
    <HatchParameterReference Value="&lt;default&gt;" />
    <BitmapParameterReference Value="&lt;default&gt;" />
    <GraphicResolution Value="0.02" />
    <JobMOFParameter>
      <UseMOF Value="False" />
      <DelayEncoder Value="X" />
      <EncoderDelay Value="0" />
      <NumberOfSimulatedStarts Value="0" />
      <DelaySimulatedStart Value="0" />
    </JobMOFParameter>
    <Use3D Value="True" />
    <ChildElements>
      <GroupElement>
        <UID Value="G#155" />
        <Label Value="Group" />
        <Area Value="True" />
        <HatchParameter>
          <FillingType Value="2" />
          <Distance Value="0.095" />
          <HatchAngle Value="Relative" />
          <AngleCount Value="1" />
          <Angle Value="1543" />
          <AngleStep Value="67" />
          <LineReduction Value="0" />
          <OutlineReduction Value="0.05" />
          <Beamcomp Value="NaN" />
          <NumberOfLoops Value="2" />
          <LineJoinType Value="Round" />
          <JoinFirstLinesRound Value="False" />
          <ReverseOrderOfLoops Value="False" />
          <Tolerance Value="0.05" />
          <SegmentCount Value="2" />
          <JumpReduction Value="False" />
          <Sort Value="False" />
          <SearchRadius Value="0.1" />
        </HatchParameter>
        <MarkMode Value="HatchlinesBeforeOutlines" />
        <GraphicResolution Value="0.02" />
        <HatchDistance Value="0.095" />
        <Box>
          <Center X="0" Y="24.999999046326" />
          <Width Value="10.108342170715" />
          <Height Value="10.090085983276" />
        </Box>
        <ChildElements>
          <FillHatchlinesElement>
            <Label Value="Fill-Hatchlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000476074219" Y="25.004716796875" />
                  <Width Value="9.783896484375" />
                  <Height Value="9.76644140625" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.71332611084" Y="20.927578125" />
                  <Point X="0.563302062988" Y="21.4874765625" />
                  <Point X="0.557721618652" Y="21.502857421875" />
                  <Point X="0.542363037109" Y="21.532625" />
                  <Point X="0.481044555664" Y="21.62097265625" />
                  <Point X="0.378635375977" Y="21.768525390625" />
                  <Point X="0.356752044678" Y="21.79098046875" />
                  <Point X="0.330496551514" Y="21.810224609375" />
                  <Point X="0.302495025635" Y="21.82433203125" />
                  <Point X="0.207608108521" Y="21.85378125" />
                  <Point X="0.049135883331" Y="21.90296484375" />
                  <Point X="0.020976638794" Y="21.907234375" />
                  <Point X="-0.00866468811" Y="21.907234375" />
                  <Point X="-0.036824085236" Y="21.90296484375" />
                  <Point X="-0.131710998535" Y="21.873515625" />
                  <Point X="-0.290183074951" Y="21.82433203125" />
                  <Point X="-0.318184906006" Y="21.81022265625" />
                  <Point X="-0.344440093994" Y="21.790978515625" />
                  <Point X="-0.366323730469" Y="21.7685234375" />
                  <Point X="-0.427642333984" Y="21.680173828125" />
                  <Point X="-0.530051391602" Y="21.532623046875" />
                  <Point X="-0.538188720703" Y="21.518427734375" />
                  <Point X="-0.550990112305" Y="21.4874765625" />
                  <Point X="-0.801547180176" Y="20.5523828125" />
                  <Point X="-0.916584655762" Y="20.12305859375" />
                  <Point X="-1.079324707031" Y="20.154646484375" />
                  <Point X="-1.185859130859" Y="20.182056640625" />
                  <Point X="-1.24641784668" Y="20.197638671875" />
                  <Point X="-1.233022338867" Y="20.29938671875" />
                  <Point X="-1.214963012695" Y="20.436560546875" />
                  <Point X="-1.214201171875" Y="20.4520703125" />
                  <Point X="-1.216508544922" Y="20.4837734375" />
                  <Point X="-1.239177001953" Y="20.597736328125" />
                  <Point X="-1.277036010742" Y="20.78806640625" />
                  <Point X="-1.287938354492" Y="20.817033203125" />
                  <Point X="-1.304010620117" Y="20.84487109375" />
                  <Point X="-1.323644775391" Y="20.868794921875" />
                  <Point X="-1.411004516602" Y="20.945408203125" />
                  <Point X="-1.556905517578" Y="21.073359375" />
                  <Point X="-1.583189208984" Y="21.089705078125" />
                  <Point X="-1.612886352539" Y="21.102005859375" />
                  <Point X="-1.64302734375" Y="21.109033203125" />
                  <Point X="-1.758973510742" Y="21.1166328125" />
                  <Point X="-1.952616577148" Y="21.12932421875" />
                  <Point X="-1.983414916992" Y="21.126291015625" />
                  <Point X="-2.014463256836" Y="21.11797265625" />
                  <Point X="-2.042657592773" Y="21.10519921875" />
                  <Point X="-2.139270019531" Y="21.04064453125" />
                  <Point X="-2.300624023438" Y="20.93283203125" />
                  <Point X="-2.312789306641" Y="20.92317578125" />
                  <Point X="-2.335102539062" Y="20.900537109375" />
                  <Point X="-2.475770996094" Y="20.71721484375" />
                  <Point X="-2.480147949219" Y="20.711509765625" />
                  <Point X="-2.533176025391" Y="20.74434375" />
                  <Point X="-2.801724609375" Y="20.910623046875" />
                  <Point X="-2.949204101562" Y="21.024177734375" />
                  <Point X="-3.104722167969" Y="21.143919921875" />
                  <Point X="-2.702563964844" Y="21.84048046875" />
                  <Point X="-2.423761230469" Y="22.323380859375" />
                  <Point X="-2.412858398438" Y="22.352349609375" />
                  <Point X="-2.406587890625" Y="22.38387890625" />
                  <Point X="-2.405576416016" Y="22.414814453125" />
                  <Point X="-2.414562988281" Y="22.44443359375" />
                  <Point X="-2.428782714844" Y="22.473263671875" />
                  <Point X="-2.446810058594" Y="22.49841796875" />
                  <Point X="-2.464155029297" Y="22.51576171875" />
                  <Point X="-2.489310791016" Y="22.533787109375" />
                  <Point X="-2.518139892578" Y="22.54800390625" />
                  <Point X="-2.5477578125" Y="22.55698828125" />
                  <Point X="-2.578691894531" Y="22.555974609375" />
                  <Point X="-2.610218505859" Y="22.549703125" />
                  <Point X="-2.63918359375" Y="22.53880078125" />
                  <Point X="-3.445682617188" Y="22.07316796875" />
                  <Point X="-3.818022705078" Y="21.858197265625" />
                  <Point X="-3.870703857422" Y="21.92741015625" />
                  <Point X="-4.08286328125" Y="22.20614453125" />
                  <Point X="-4.188600097656" Y="22.38344921875" />
                  <Point X="-4.306142578125" Y="22.580548828125" />
                  <Point X="-3.593768798828" Y="23.127171875" />
                  <Point X="-3.105954589844" Y="23.501484375" />
                  <Point X="-3.084577636719" Y="23.524404296875" />
                  <Point X="-3.066612792969" Y="23.55153515625" />
                  <Point X="-3.053856445312" Y="23.580166015625" />
                  <Point X="-3.046151855469" Y="23.6099140625" />
                  <Point X="-3.04334765625" Y="23.64034375" />
                  <Point X="-3.045556884766" Y="23.672015625" />
                  <Point X="-3.052558837891" Y="23.70176171875" />
                  <Point X="-3.068642578125" Y="23.72774609375" />
                  <Point X="-3.089477050781" Y="23.751703125" />
                  <Point X="-3.112976074219" Y="23.771234375" />
                  <Point X="-3.139458740234" Y="23.7868203125" />
                  <Point X="-3.168722412109" Y="23.79804296875" />
                  <Point X="-3.200607666016" Y="23.8045234375" />
                  <Point X="-3.231928955078" Y="23.805615234375" />
                  <Point X="-4.250057617188" Y="23.671576171875" />
                  <Point X="-4.732102050781" Y="23.608115234375" />
                  <Point X="-4.7511015625" Y="23.682498046875" />
                  <Point X="-4.834077636719" Y="24.00734375" />
                  <Point X="-4.862053710938" Y="24.202951171875" />
                  <Point X="-4.892424316406" Y="24.415298828125" />
                  <Point X="-4.088569824219" Y="24.63069140625" />
                  <Point X="-3.532875976562" Y="24.77958984375" />
                  <Point X="-3.511299560547" Y="24.78832421875" />
                  <Point X="-3.48533984375" Y="24.8027578125" />
                  <Point X="-3.477341064453" Y="24.807740234375" />
                  <Point X="-3.459981933594" Y="24.819787109375" />
                  <Point X="-3.436019287109" Y="24.8413203125" />
                  <Point X="-3.414752441406" Y="24.872529296875" />
                  <Point X="-3.403861572266" Y="24.8984296875" />
                  <Point X="-3.399059082031" Y="24.913078125" />
                  <Point X="-3.391025390625" Y="24.94654296875" />
                  <Point X="-3.388405273438" Y="24.9678046875" />
                  <Point X="-3.390615722656" Y="24.98911328125" />
                  <Point X="-3.396327148438" Y="25.01509765625" />
                  <Point X="-3.400779052734" Y="25.0296640625" />
                  <Point X="-3.4139921875" Y="25.063048828125" />
                  <Point X="-3.425205078125" Y="25.0835625" />
                  <Point X="-3.441088623047" Y="25.100716796875" />
                  <Point X="-3.463303710938" Y="25.119447265625" />
                  <Point X="-3.470372558594" Y="25.124861328125" />
                  <Point X="-3.4877265625" Y="25.13690625" />
                  <Point X="-3.501916015625" Y="25.145041015625" />
                  <Point X="-3.532875976562" Y="25.157849609375" />
                  <Point X="-4.460948242188" Y="25.406525390625" />
                  <Point X="-4.89181640625" Y="25.5219765625" />
                  <Point X="-4.877964355469" Y="25.6155859375" />
                  <Point X="-4.82448828125" Y="25.97697265625" />
                  <Point X="-4.768172363281" Y="26.184794921875" />
                  <Point X="-4.70355078125" Y="26.423267578125" />
                  <Point X="-4.164001464844" Y="26.352234375" />
                  <Point X="-3.765666503906" Y="26.29979296875" />
                  <Point X="-3.744985595703" Y="26.299341796875" />
                  <Point X="-3.723422607422" Y="26.301228515625" />
                  <Point X="-3.703141601563" Y="26.30526171875" />
                  <Point X="-3.68013671875" Y="26.312513671875" />
                  <Point X="-3.641715576172" Y="26.32462890625" />
                  <Point X="-3.622774902344" Y="26.332962890625" />
                  <Point X="-3.604031494141" Y="26.34378515625" />
                  <Point X="-3.587352294922" Y="26.356015625" />
                  <Point X="-3.573715332031" Y="26.37156640625" />
                  <Point X="-3.561301269531" Y="26.389294921875" />
                  <Point X="-3.5513515625" Y="26.4074296875" />
                  <Point X="-3.542120605469" Y="26.42971484375" />
                  <Point X="-3.526704101563" Y="26.46693359375" />
                  <Point X="-3.520915527344" Y="26.486794921875" />
                  <Point X="-3.517157226562" Y="26.508111328125" />
                  <Point X="-3.515804443359" Y="26.52875" />
                  <Point X="-3.518951660156" Y="26.5491953125" />
                  <Point X="-3.524553955078" Y="26.5701015625" />
                  <Point X="-3.532049804688" Y="26.589376953125" />
                  <Point X="-3.543187744141" Y="26.6107734375" />
                  <Point X="-3.561789550781" Y="26.646505859375" />
                  <Point X="-3.573281005859" Y="26.663705078125" />
                  <Point X="-3.587192871094" Y="26.68028515625" />
                  <Point X="-3.602135742188" Y="26.69458984375" />
                  <Point X="-4.13448046875" Y="27.103072265625" />
                  <Point X="-4.351860351563" Y="27.269873046875" />
                  <Point X="-4.288942382813" Y="27.37766796875" />
                  <Point X="-4.081156982422" Y="27.73365234375" />
                  <Point X="-3.931977294922" Y="27.925404296875" />
                  <Point X="-3.75050390625" Y="28.158662109375" />
                  <Point X="-3.451303222656" Y="27.98591796875" />
                  <Point X="-3.206656982422" Y="27.844671875" />
                  <Point X="-3.187729492188" Y="27.836341796875" />
                  <Point X="-3.167087402344" Y="27.82983203125" />
                  <Point X="-3.146793701172" Y="27.825794921875" />
                  <Point X="-3.114754150391" Y="27.8229921875" />
                  <Point X="-3.061244384766" Y="27.818310546875" />
                  <Point X="-3.040560791016" Y="27.81876171875" />
                  <Point X="-3.019102294922" Y="27.821587890625" />
                  <Point X="-2.999013427734" Y="27.826505859375" />
                  <Point X="-2.980464355469" Y="27.83565234375" />
                  <Point X="-2.962210205078" Y="27.84728125" />
                  <Point X="-2.946077392578" Y="27.86023046875" />
                  <Point X="-2.923335449219" Y="27.88297265625" />
                  <Point X="-2.885353759766" Y="27.920953125" />
                  <Point X="-2.872408935547" Y="27.937080078125" />
                  <Point X="-2.860779052734" Y="27.955333984375" />
                  <Point X="-2.851629150391" Y="27.97388671875" />
                  <Point X="-2.846712158203" Y="27.99398046875" />
                  <Point X="-2.843886962891" Y="28.015439453125" />
                  <Point X="-2.843435791016" Y="28.036119140625" />
                  <Point X="-2.846239013672" Y="28.06816015625" />
                  <Point X="-2.850920410156" Y="28.121669921875" />
                  <Point X="-2.854955566406" Y="28.14195703125" />
                  <Point X="-2.861464111328" Y="28.162599609375" />
                  <Point X="-2.869794921875" Y="28.181533203125" />
                  <Point X="-3.105692871094" Y="28.590119140625" />
                  <Point X="-3.183333496094" Y="28.724595703125" />
                  <Point X="-3.062523681641" Y="28.817220703125" />
                  <Point X="-2.700629150391" Y="29.094681640625" />
                  <Point X="-2.465680175781" Y="29.225212890625" />
                  <Point X="-2.167036376953" Y="29.391134765625" />
                  <Point X="-2.118089111328" Y="29.32734375" />
                  <Point X="-2.04319543457" Y="29.2297421875" />
                  <Point X="-2.028892333984" Y="29.21480078125" />
                  <Point X="-2.012312988281" Y="29.200888671875" />
                  <Point X="-1.995115356445" Y="29.189396484375" />
                  <Point X="-1.959455444336" Y="29.17083203125" />
                  <Point X="-1.899899291992" Y="29.139828125" />
                  <Point X="-1.88062512207" Y="29.13233203125" />
                  <Point X="-1.859717895508" Y="29.126728515625" />
                  <Point X="-1.839268310547" Y="29.123580078125" />
                  <Point X="-1.818622192383" Y="29.12493359375" />
                  <Point X="-1.797306518555" Y="29.128693359375" />
                  <Point X="-1.777453491211" Y="29.13448046875" />
                  <Point X="-1.740311279297" Y="29.149865234375" />
                  <Point X="-1.678279541016" Y="29.175560546875" />
                  <Point X="-1.660145141602" Y="29.185509765625" />
                  <Point X="-1.642415771484" Y="29.197923828125" />
                  <Point X="-1.626863647461" Y="29.2115625" />
                  <Point X="-1.614632324219" Y="29.228244140625" />
                  <Point X="-1.603810546875" Y="29.24698828125" />
                  <Point X="-1.595480102539" Y="29.265921875" />
                  <Point X="-1.583391113281" Y="29.304263671875" />
                  <Point X="-1.563200805664" Y="29.368298828125" />
                  <Point X="-1.559165527344" Y="29.3885859375" />
                  <Point X="-1.557279174805" Y="29.410146484375" />
                  <Point X="-1.55773034668" Y="29.430826171875" />
                  <Point X="-1.584202026367" Y="29.631896484375" />
                  <Point X="-1.418134521484" Y="29.67845703125" />
                  <Point X="-0.94962310791" Y="29.80980859375" />
                  <Point X="-0.664815917969" Y="29.843142578125" />
                  <Point X="-0.294711364746" Y="29.886458984375" />
                  <Point X="-0.206276519775" Y="29.5564140625" />
                  <Point X="-0.133903305054" Y="29.286314453125" />
                  <Point X="-0.121129692078" Y="29.258123046875" />
                  <Point X="-0.103271400452" Y="29.231396484375" />
                  <Point X="-0.082113853455" Y="29.208806640625" />
                  <Point X="-0.054817928314" Y="29.194216796875" />
                  <Point X="-0.024379852295" Y="29.183884765625" />
                  <Point X="0.006156154156" Y="29.17884375" />
                  <Point X="0.036692108154" Y="29.183884765625" />
                  <Point X="0.067130027771" Y="29.194216796875" />
                  <Point X="0.094426071167" Y="29.208806640625" />
                  <Point X="0.115583656311" Y="29.231396484375" />
                  <Point X="0.133441802979" Y="29.258123046875" />
                  <Point X="0.146215255737" Y="29.286314453125" />
                  <Point X="0.267086730957" Y="29.737412109375" />
                  <Point X="0.307419708252" Y="29.8879375" />
                  <Point X="0.43496585083" Y="29.874580078125" />
                  <Point X="0.84403125" Y="29.831740234375" />
                  <Point X="1.079684936523" Y="29.774845703125" />
                  <Point X="1.481039550781" Y="29.6779453125" />
                  <Point X="1.633481201172" Y="29.622654296875" />
                  <Point X="1.894647583008" Y="29.527927734375" />
                  <Point X="2.042961181641" Y="29.45856640625" />
                  <Point X="2.294556884766" Y="29.34090234375" />
                  <Point X="2.437884033203" Y="29.257400390625" />
                  <Point X="2.680976318359" Y="29.115775390625" />
                  <Point X="2.816111816406" Y="29.019673828125" />
                  <Point X="2.943259521484" Y="28.92925390625" />
                  <Point X="2.470274169922" Y="28.110017578125" />
                  <Point X="2.147581054688" Y="27.55109765625" />
                  <Point X="2.142074951172" Y="27.539927734375" />
                  <Point X="2.133076904297" Y="27.516056640625" />
                  <Point X="2.125036132813" Y="27.48598828125" />
                  <Point X="2.111607177734" Y="27.43576953125" />
                  <Point X="2.108619384766" Y="27.417931640625" />
                  <Point X="2.107728027344" Y="27.380951171875" />
                  <Point X="2.11086328125" Y="27.354951171875" />
                  <Point X="2.116099365234" Y="27.31152734375" />
                  <Point X="2.121442871094" Y="27.2896015625" />
                  <Point X="2.129709228516" Y="27.267513671875" />
                  <Point X="2.1400703125" Y="27.24747265625" />
                  <Point X="2.156158691406" Y="27.22376171875" />
                  <Point X="2.183028076172" Y="27.1841640625" />
                  <Point X="2.194464355469" Y="27.170330078125" />
                  <Point X="2.221598388672" Y="27.14559375" />
                  <Point X="2.245308349609" Y="27.129505859375" />
                  <Point X="2.284906982422" Y="27.102634765625" />
                  <Point X="2.304947265625" Y="27.0922734375" />
                  <Point X="2.327036865234" Y="27.084005859375" />
                  <Point X="2.348965576172" Y="27.078662109375" />
                  <Point X="2.374966308594" Y="27.07552734375" />
                  <Point X="2.418390380859" Y="27.070291015625" />
                  <Point X="2.436468505859" Y="27.06984375" />
                  <Point X="2.473207763672" Y="27.074169921875" />
                  <Point X="2.503276367188" Y="27.0822109375" />
                  <Point X="2.553494140625" Y="27.095640625" />
                  <Point X="2.5652890625" Y="27.099640625" />
                  <Point X="2.588533935547" Y="27.110146484375" />
                  <Point X="3.52199609375" Y="27.649080078125" />
                  <Point X="3.967326171875" Y="27.90619140625" />
                  <Point X="3.979076171875" Y="27.889861328125" />
                  <Point X="4.123271484375" Y="27.689462890625" />
                  <Point X="4.198606933594" Y="27.56496875" />
                  <Point X="4.262198242188" Y="27.4598828125" />
                  <Point X="3.655639648438" Y="26.994455078125" />
                  <Point X="3.230783935547" Y="26.668451171875" />
                  <Point X="3.221423339844" Y="26.660240234375" />
                  <Point X="3.203974609375" Y="26.64162890625" />
                  <Point X="3.182334228516" Y="26.6133984375" />
                  <Point X="3.146192382812" Y="26.566248046875" />
                  <Point X="3.136605957031" Y="26.550912109375" />
                  <Point X="3.121629882812" Y="26.5170859375" />
                  <Point X="3.113568847656" Y="26.48826171875" />
                  <Point X="3.100105957031" Y="26.44012109375" />
                  <Point X="3.09665234375" Y="26.417822265625" />
                  <Point X="3.095836425781" Y="26.394251953125" />
                  <Point X="3.097739501953" Y="26.371767578125" />
                  <Point X="3.104356689453" Y="26.339697265625" />
                  <Point X="3.115408447266" Y="26.286134765625" />
                  <Point X="3.120679931641" Y="26.2689765625" />
                  <Point X="3.136283203125" Y="26.23573828125" />
                  <Point X="3.154281494141" Y="26.2083828125" />
                  <Point X="3.184340820312" Y="26.162693359375" />
                  <Point X="3.198892333984" Y="26.145451171875" />
                  <Point X="3.216135742188" Y="26.129361328125" />
                  <Point X="3.234347167969" Y="26.11603515625" />
                  <Point X="3.260429199219" Y="26.101353515625" />
                  <Point X="3.303989257812" Y="26.07683203125" />
                  <Point X="3.320521728516" Y="26.0695" />
                  <Point X="3.356120361328" Y="26.0594375" />
                  <Point X="3.391385009766" Y="26.05477734375" />
                  <Point X="3.450281005859" Y="26.046994140625" />
                  <Point X="3.462698486328" Y="26.04617578125" />
                  <Point X="3.488203613281" Y="26.046984375" />
                  <Point X="4.374930175781" Y="26.16372265625" />
                  <Point X="4.77683984375" Y="26.21663671875" />
                  <Point X="4.784004882812" Y="26.187203125" />
                  <Point X="4.845935546875" Y="25.9328125" />
                  <Point X="4.869676269531" Y="25.780328125" />
                  <Point X="4.890864257812" Y="25.644240234375" />
                  <Point X="4.203473632812" Y="25.460052734375" />
                  <Point X="3.716579833984" Y="25.32958984375" />
                  <Point X="3.704788085938" Y="25.325583984375" />
                  <Point X="3.681547851562" Y="25.315068359375" />
                  <Point X="3.646901367188" Y="25.29504296875" />
                  <Point X="3.589037841797" Y="25.26159765625" />
                  <Point X="3.574313232422" Y="25.25109765625" />
                  <Point X="3.547529296875" Y="25.225576171875" />
                  <Point X="3.526741455078" Y="25.1990859375" />
                  <Point X="3.4920234375" Y="25.15484765625" />
                  <Point X="3.480301025391" Y="25.135568359375" />
                  <Point X="3.470526855469" Y="25.114103515625" />
                  <Point X="3.463680908203" Y="25.092603515625" />
                  <Point X="3.456751464844" Y="25.056421875" />
                  <Point X="3.445178710938" Y="24.9959921875" />
                  <Point X="3.443482910156" Y="24.97812109375" />
                  <Point X="3.445178955078" Y="24.9414453125" />
                  <Point X="3.452108398438" Y="24.905263671875" />
                  <Point X="3.463681152344" Y="24.8448359375" />
                  <Point X="3.470526855469" Y="24.8233359375" />
                  <Point X="3.480300048828" Y="24.801873046875" />
                  <Point X="3.492024902344" Y="24.782591796875" />
                  <Point X="3.512812744141" Y="24.756103515625" />
                  <Point X="3.547530761719" Y="24.71186328125" />
                  <Point X="3.559994140625" Y="24.698767578125" />
                  <Point X="3.589036865234" Y="24.675841796875" />
                  <Point X="3.623683349609" Y="24.65581640625" />
                  <Point X="3.681546875" Y="24.622369140625" />
                  <Point X="3.692709228516" Y="24.616859375" />
                  <Point X="3.716580078125" Y="24.60784765625" />
                  <Point X="4.529748535156" Y="24.3899609375" />
                  <Point X="4.891472167969" Y="24.293037109375" />
                  <Point X="4.889602539062" Y="24.280634765625" />
                  <Point X="4.855022460938" Y="24.0512734375" />
                  <Point X="4.824606933594" Y="23.91798828125" />
                  <Point X="4.801173828125" Y="23.81530078125" />
                  <Point X="3.989041748047" Y="23.922220703125" />
                  <Point X="3.424381835938" Y="23.99655859375" />
                  <Point X="3.408035400391" Y="23.9972890625" />
                  <Point X="3.374659423828" Y="23.994490234375" />
                  <Point X="3.306660644531" Y="23.9797109375" />
                  <Point X="3.193095214844" Y="23.95502734375" />
                  <Point X="3.163973632812" Y="23.94340234375" />
                  <Point X="3.136146972656" Y="23.926509765625" />
                  <Point X="3.112397460938" Y="23.9060390625" />
                  <Point X="3.071296386719" Y="23.856607421875" />
                  <Point X="3.002653320312" Y="23.77405078125" />
                  <Point X="2.987932861328" Y="23.749669921875" />
                  <Point X="2.976589599609" Y="23.72228515625" />
                  <Point X="2.969757568359" Y="23.694634765625" />
                  <Point X="2.963866699219" Y="23.630619140625" />
                  <Point X="2.954028564453" Y="23.523703125" />
                  <Point X="2.956347412109" Y="23.49243359375" />
                  <Point X="2.964079101562" Y="23.4608125" />
                  <Point X="2.976450683594" Y="23.432001953125" />
                  <Point X="3.014082275391" Y="23.37346875" />
                  <Point X="3.076930908203" Y="23.275712890625" />
                  <Point X="3.086930908203" Y="23.262763671875" />
                  <Point X="3.110628417969" Y="23.23908984375" />
                  <Point X="3.86525390625" Y="22.660044921875" />
                  <Point X="4.213122558594" Y="22.3931171875" />
                  <Point X="4.124813964844" Y="22.25021875" />
                  <Point X="4.061908203125" Y="22.16083984375" />
                  <Point X="4.028981201172" Y="22.1140546875" />
                  <Point X="3.303860351562" Y="22.532703125" />
                  <Point X="2.800954589844" Y="22.823056640625" />
                  <Point X="2.786129394531" Y="22.829986328125" />
                  <Point X="2.754224121094" Y="22.840173828125" />
                  <Point X="2.673294921875" Y="22.8547890625" />
                  <Point X="2.538133789062" Y="22.87919921875" />
                  <Point X="2.506784179688" Y="22.879603515625" />
                  <Point X="2.474611328125" Y="22.874646484375" />
                  <Point X="2.444834228516" Y="22.864822265625" />
                  <Point X="2.377601806641" Y="22.829439453125" />
                  <Point X="2.265316162109" Y="22.77034375" />
                  <Point X="2.242385498047" Y="22.753451171875" />
                  <Point X="2.22142578125" Y="22.7324921875" />
                  <Point X="2.204531982422" Y="22.709560546875" />
                  <Point X="2.169147949219" Y="22.642328125" />
                  <Point X="2.110052978516" Y="22.53004296875" />
                  <Point X="2.100229003906" Y="22.500267578125" />
                  <Point X="2.095271240234" Y="22.46809375" />
                  <Point X="2.095675537109" Y="22.4367421875" />
                  <Point X="2.110291259766" Y="22.3558125" />
                  <Point X="2.134701171875" Y="22.220650390625" />
                  <Point X="2.138985839844" Y="22.20485546875" />
                  <Point X="2.151819091797" Y="22.173919921875" />
                  <Point X="2.636741943359" Y="21.334009765625" />
                  <Point X="2.861283203125" Y="20.94509375" />
                  <Point X="2.781864257812" Y="20.8883671875" />
                  <Point X="2.7115234375" Y="20.8428359375" />
                  <Point X="2.701763427734" Y="20.836517578125" />
                  <Point X="2.142309814453" Y="21.56561328125" />
                  <Point X="1.758546142578" Y="22.065744140625" />
                  <Point X="1.747507202148" Y="22.07781640625" />
                  <Point X="1.721923217773" Y="22.099443359375" />
                  <Point X="1.642104980469" Y="22.1507578125" />
                  <Point X="1.508799804688" Y="22.2364609375" />
                  <Point X="1.479986816406" Y="22.24883203125" />
                  <Point X="1.448366455078" Y="22.2565625" />
                  <Point X="1.417100097656" Y="22.258880859375" />
                  <Point X="1.329805419922" Y="22.25084765625" />
                  <Point X="1.184013061523" Y="22.23743359375" />
                  <Point X="1.156363647461" Y="22.2306015625" />
                  <Point X="1.128977416992" Y="22.2192578125" />
                  <Point X="1.104595214844" Y="22.204537109375" />
                  <Point X="1.037188476563" Y="22.148490234375" />
                  <Point X="0.92461151123" Y="22.054884765625" />
                  <Point X="0.90414074707" Y="22.031134765625" />
                  <Point X="0.887248779297" Y="22.00330859375" />
                  <Point X="0.875624328613" Y="21.974189453125" />
                  <Point X="0.855470092773" Y="21.88146484375" />
                  <Point X="0.821810058594" Y="21.726603515625" />
                  <Point X="0.819724487305" Y="21.710373046875" />
                  <Point X="0.819742248535" Y="21.676880859375" />
                  <Point X="0.957165710449" Y="20.633044921875" />
                  <Point X="1.022065429688" Y="20.140083984375" />
                  <Point X="0.975700683594" Y="20.129921875" />
                  <Point X="0.929315551758" Y="20.12149609375" />
                </GraphicPath>
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="-1.058418457031" Y="20.247361328125" />
                  <Point X="-1.14124609375" Y="20.268671875" />
                  <Point X="-1.138834960938" Y="20.286986328125" />
                  <Point X="-1.120775634766" Y="20.42416015625" />
                  <Point X="-1.120077514648" Y="20.431900390625" />
                  <Point X="-1.119451782227" Y="20.458966796875" />
                  <Point X="-1.121759155273" Y="20.490669921875" />
                  <Point X="-1.123333740234" Y="20.502306640625" />
                  <Point X="-1.146002319336" Y="20.61626953125" />
                  <Point X="-1.183861328125" Y="20.806599609375" />
                  <Point X="-1.188125" Y="20.821529296875" />
                  <Point X="-1.19902734375" Y="20.85049609375" />
                  <Point X="-1.205666015625" Y="20.864533203125" />
                  <Point X="-1.22173840332" Y="20.89237109375" />
                  <Point X="-1.230575195312" Y="20.905138671875" />
                  <Point X="-1.250209350586" Y="20.9290625" />
                  <Point X="-1.261006591797" Y="20.94021875" />
                  <Point X="-1.348366455078" Y="21.01683203125" />
                  <Point X="-1.494267333984" Y="21.144783203125" />
                  <Point X="-1.506735839844" Y="21.15403125" />
                  <Point X="-1.53301953125" Y="21.170376953125" />
                  <Point X="-1.546834716797" Y="21.177474609375" />
                  <Point X="-1.576531860352" Y="21.189775390625" />
                  <Point X="-1.591315673828" Y="21.194525390625" />
                  <Point X="-1.621456665039" Y="21.201552734375" />
                  <Point X="-1.636813964844" Y="21.203830078125" />
                  <Point X="-1.752760131836" Y="21.2114296875" />
                  <Point X="-1.946403198242" Y="21.22412109375" />
                  <Point X="-1.961927612305" Y="21.2238671875" />
                  <Point X="-1.992726074219" Y="21.220833984375" />
                  <Point X="-2.00799987793" Y="21.2180546875" />
                  <Point X="-2.039048217773" Y="21.209736328125" />
                  <Point X="-2.053667236328" Y="21.204505859375" />
                  <Point X="-2.081861572266" Y="21.191732421875" />
                  <Point X="-2.095437011719" Y="21.184189453125" />
                  <Point X="-2.192049316406" Y="21.119634765625" />
                  <Point X="-2.353403320312" Y="21.011822265625" />
                  <Point X="-2.359686279297" Y="21.007240234375" />
                  <Point X="-2.380448974609" Y="20.98986328125" />
                  <Point X="-2.402762207031" Y="20.967224609375" />
                  <Point X="-2.410470947266" Y="20.958369140625" />
                  <Point X="-2.503202148438" Y="20.83751953125" />
                  <Point X="-2.747599853516" Y="20.988845703125" />
                  <Point X="-2.891246582031" Y="21.09944921875" />
                  <Point X="-2.980863037109" Y="21.16844921875" />
                  <Point X="-2.620291503906" Y="21.79298046875" />
                  <Point X="-2.341488769531" Y="22.275880859375" />
                  <Point X="-2.334849853516" Y="22.28991796875" />
                  <Point X="-2.323947021484" Y="22.31888671875" />
                  <Point X="-2.319683105469" Y="22.333818359375" />
                  <Point X="-2.313412597656" Y="22.36534765625" />
                  <Point X="-2.311638671875" Y="22.380775390625" />
                  <Point X="-2.310627197266" Y="22.4117109375" />
                  <Point X="-2.314668457031" Y="22.442396484375" />
                  <Point X="-2.323655029297" Y="22.472015625" />
                  <Point X="-2.329362792969" Y="22.48645703125" />
                  <Point X="-2.343582519531" Y="22.515287109375" />
                  <Point X="-2.351565185547" Y="22.528603515625" />
                  <Point X="-2.369592529297" Y="22.5537578125" />
                  <Point X="-2.379637207031" Y="22.565595703125" />
                  <Point X="-2.396982177734" Y="22.582939453125" />
                  <Point X="-2.408821533203" Y="22.592984375" />
                  <Point X="-2.433977294922" Y="22.611009765625" />
                  <Point X="-2.447293701172" Y="22.618990234375" />
                  <Point X="-2.476122802734" Y="22.63320703125" />
                  <Point X="-2.490563232422" Y="22.6389140625" />
                  <Point X="-2.520181152344" Y="22.6478984375" />
                  <Point X="-2.550869140625" Y="22.6519375" />
                  <Point X="-2.581803222656" Y="22.650923828125" />
                  <Point X="-2.597226806641" Y="22.6491484375" />
                  <Point X="-2.628753417969" Y="22.642876953125" />
                  <Point X="-2.643684082031" Y="22.63861328125" />
                  <Point X="-2.672649169922" Y="22.6277109375" />
                  <Point X="-2.68668359375" Y="22.621072265625" />
                  <Point X="-3.493182617188" Y="22.155439453125" />
                  <Point X="-3.793087158203" Y="21.9822890625" />
                  <Point X="-3.795110107422" Y="21.984947265625" />
                  <Point X="-4.004017822266" Y="22.25941015625" />
                  <Point X="-4.107007324219" Y="22.432107421875" />
                  <Point X="-4.181264648438" Y="22.556625" />
                  <Point X="-3.535936523438" Y="23.051802734375" />
                  <Point X="-3.048122314453" Y="23.426115234375" />
                  <Point X="-3.036481689453" Y="23.4366875" />
                  <Point X="-3.015104736328" Y="23.459607421875" />
                  <Point X="-3.005368408203" Y="23.471955078125" />
                  <Point X="-2.987403564453" Y="23.4990859375" />
                  <Point X="-2.979836181641" Y="23.512873046875" />
                  <Point X="-2.967079833984" Y="23.54150390625" />
                  <Point X="-2.961890869141" Y="23.55634765625" />
                  <Point X="-2.954186279297" Y="23.586095703125" />
                  <Point X="-2.951552734375" Y="23.601197265625" />
                  <Point X="-2.948748535156" Y="23.631626953125" />
                  <Point X="-2.948577880859" Y="23.646955078125" />
                  <Point X="-2.950787109375" Y="23.678626953125" />
                  <Point X="-2.953084228516" Y="23.693783203125" />
                  <Point X="-2.960086181641" Y="23.723529296875" />
                  <Point X="-2.971781005859" Y="23.75176171875" />
                  <Point X="-2.987864746094" Y="23.77774609375" />
                  <Point X="-2.996958496094" Y="23.790087890625" />
                  <Point X="-3.01779296875" Y="23.814044921875" />
                  <Point X="-3.028753662109" Y="23.82476171875" />
                  <Point X="-3.052252685547" Y="23.84429296875" />
                  <Point X="-3.064791015625" Y="23.853107421875" />
                  <Point X="-3.091273681641" Y="23.868693359375" />
                  <Point X="-3.105441894531" Y="23.875521484375" />
                  <Point X="-3.134705566406" Y="23.886744140625" />
                  <Point X="-3.149801025391" Y="23.891138671875" />
                  <Point X="-3.181686279297" Y="23.897619140625" />
                  <Point X="-3.197298095703" Y="23.89946484375" />
                  <Point X="-3.228619384766" Y="23.900556640625" />
                  <Point X="-3.244328857422" Y="23.899802734375" />
                  <Point X="-4.262457519531" Y="23.765763671875" />
                  <Point X="-4.660920410156" Y="23.713306640625" />
                  <Point X="-4.740762695312" Y="24.0258828125" />
                  <Point X="-4.768010742188" Y="24.216400390625" />
                  <Point X="-4.786452148438" Y="24.345341796875" />
                  <Point X="-4.063981933594" Y="24.538927734375" />
                  <Point X="-3.508288085938" Y="24.687826171875" />
                  <Point X="-3.497229003906" Y="24.69153125" />
                  <Point X="-3.475652587891" Y="24.700265625" />
                  <Point X="-3.465135253906" Y="24.705294921875" />
                  <Point X="-3.439175537109" Y="24.719728515625" />
                  <Point X="-3.423177978516" Y="24.729693359375" />
                  <Point X="-3.405818847656" Y="24.741740234375" />
                  <Point X="-3.396484375" Y="24.749125" />
                  <Point X="-3.372521728516" Y="24.770658203125" />
                  <Point X="-3.357513671875" Y="24.78782421875" />
                  <Point X="-3.336246826172" Y="24.819033203125" />
                  <Point X="-3.327179443359" Y="24.835705078125" />
                  <Point X="-3.316288574219" Y="24.86160546875" />
                  <Point X="-3.313589355469" Y="24.868833984375" />
                  <Point X="-3.30668359375" Y="24.89090234375" />
                  <Point X="-3.298649902344" Y="24.9243671875" />
                  <Point X="-3.296738525391" Y="24.934923828125" />
                  <Point X="-3.294118408203" Y="24.956185546875" />
                  <Point X="-3.293912353516" Y="24.977607421875" />
                  <Point X="-3.296122802734" Y="24.998916015625" />
                  <Point X="-3.297830566406" Y="25.0095078125" />
                  <Point X="-3.303541992188" Y="25.0354921875" />
                  <Point X="-3.305475585938" Y="25.042865234375" />
                  <Point X="-3.312445800781" Y="25.064625" />
                  <Point X="-3.325658935547" Y="25.098009765625" />
                  <Point X="-3.330632568359" Y="25.10861328125" />
                  <Point X="-3.341845458984" Y="25.129126953125" />
                  <Point X="-3.355497802734" Y="25.14810546875" />
                  <Point X="-3.371381347656" Y="25.165259765625" />
                  <Point X="-3.379851806641" Y="25.173345703125" />
                  <Point X="-3.402066894531" Y="25.192076171875" />
                  <Point X="-3.416204589844" Y="25.202904296875" />
                  <Point X="-3.43355859375" Y="25.21494921875" />
                  <Point X="-3.440477294922" Y="25.219322265625" />
                  <Point X="-3.465598388672" Y="25.23282421875" />
                  <Point X="-3.496558349609" Y="25.2456328125" />
                  <Point X="-3.508288085938" Y="25.24961328125" />
                  <Point X="-4.436360351563" Y="25.4982890625" />
                  <Point X="-4.785445800781" Y="25.591826171875" />
                  <Point X="-4.783987792969" Y="25.6016796875" />
                  <Point X="-4.731331542969" Y="25.95752734375" />
                  <Point X="-4.676479492188" Y="26.159947265625" />
                  <Point X="-4.6335859375" Y="26.318236328125" />
                  <Point X="-4.176401367188" Y="26.258046875" />
                  <Point X="-3.77806640625" Y="26.20560546875" />
                  <Point X="-3.767738525391" Y="26.20481640625" />
                  <Point X="-3.747057617188" Y="26.204365234375" />
                  <Point X="-3.736704833984" Y="26.204703125" />
                  <Point X="-3.715141845703" Y="26.20658984375" />
                  <Point X="-3.704893066406" Y="26.208052734375" />
                  <Point X="-3.684612060547" Y="26.2120859375" />
                  <Point X="-3.674579833984" Y="26.21465625" />
                  <Point X="-3.651574951172" Y="26.221908203125" />
                  <Point X="-3.613153808594" Y="26.2340234375" />
                  <Point X="-3.603455078125" Y="26.237673828125" />
                  <Point X="-3.584514404297" Y="26.2460078125" />
                  <Point X="-3.575272460938" Y="26.25069140625" />
                  <Point X="-3.556529052734" Y="26.261513671875" />
                  <Point X="-3.547854736328" Y="26.267173828125" />
                  <Point X="-3.531175537109" Y="26.279404296875" />
                  <Point X="-3.515926025391" Y="26.29337890625" />
                  <Point X="-3.5022890625" Y="26.3089296875" />
                  <Point X="-3.495896728516" Y="26.317076171875" />
                  <Point X="-3.483482666016" Y="26.3348046875" />
                  <Point X="-3.478013427734" Y="26.343599609375" />
                  <Point X="-3.468063720703" Y="26.361734375" />
                  <Point X="-3.463583251953" Y="26.37107421875" />
                  <Point X="-3.454352294922" Y="26.393359375" />
                  <Point X="-3.438935791016" Y="26.430578125" />
                  <Point X="-3.435498779297" Y="26.4403515625" />
                  <Point X="-3.429710205078" Y="26.460212890625" />
                  <Point X="-3.427358642578" Y="26.47030078125" />
                  <Point X="-3.423600341797" Y="26.4916171875" />
                  <Point X="-3.422360595703" Y="26.5018984375" />
                  <Point X="-3.4210078125" Y="26.522537109375" />
                  <Point X="-3.421910400391" Y="26.543203125" />
                  <Point X="-3.425057617188" Y="26.5636484375" />
                  <Point X="-3.427189208984" Y="26.57378515625" />
                  <Point X="-3.432791503906" Y="26.59469140625" />
                  <Point X="-3.436013183594" Y="26.604533203125" />
                  <Point X="-3.443509033203" Y="26.62380859375" />
                  <Point X="-3.447783203125" Y="26.6332421875" />
                  <Point X="-3.458921142578" Y="26.654638671875" />
                  <Point X="-3.477522949219" Y="26.69037109375" />
                  <Point X="-3.482798583984" Y="26.699283203125" />
                  <Point X="-3.494290039062" Y="26.716482421875" />
                  <Point X="-3.500505859375" Y="26.72476953125" />
                  <Point X="-3.514417724609" Y="26.741349609375" />
                  <Point X="-3.521499023438" Y="26.74891015625" />
                  <Point X="-3.536441894531" Y="26.76321484375" />
                  <Point X="-3.544303466797" Y="26.769958984375" />
                  <Point X="-4.076648193359" Y="27.17844140625" />
                  <Point X="-4.227615234375" Y="27.29428125" />
                  <Point X="-4.206895996094" Y="27.329779296875" />
                  <Point X="-4.002296386719" Y="27.6803046875" />
                  <Point X="-3.856996337891" Y="27.8670703125" />
                  <Point X="-3.726337158203" Y="28.035013671875" />
                  <Point X="-3.498803222656" Y="27.903646484375" />
                  <Point X="-3.254156982422" Y="27.762400390625" />
                  <Point X="-3.244924804688" Y="27.757720703125" />
                  <Point X="-3.225997314453" Y="27.749390625" />
                  <Point X="-3.216302001953" Y="27.745740234375" />
                  <Point X="-3.195659912109" Y="27.73923046875" />
                  <Point X="-3.185623046875" Y="27.736658203125" />
                  <Point X="-3.165329345703" Y="27.73262109375" />
                  <Point X="-3.155072509766" Y="27.73115625" />
                  <Point X="-3.123032958984" Y="27.728353515625" />
                  <Point X="-3.069523193359" Y="27.723671875" />
                  <Point X="-3.059172607422" Y="27.723333984375" />
                  <Point X="-3.038489013672" Y="27.72378515625" />
                  <Point X="-3.028156005859" Y="27.72457421875" />
                  <Point X="-3.006697509766" Y="27.727400390625" />
                  <Point X="-2.996512451172" Y="27.7293125" />
                  <Point X="-2.976423583984" Y="27.73423046875" />
                  <Point X="-2.956999267578" Y="27.74130078125" />
                  <Point X="-2.938450195312" Y="27.750447265625" />
                  <Point X="-2.929421630859" Y="27.755529296875" />
                  <Point X="-2.911167480469" Y="27.767158203125" />
                  <Point X="-2.902743896484" Y="27.7731953125" />
                  <Point X="-2.886611083984" Y="27.78614453125" />
                  <Point X="-2.878901855469" Y="27.793056640625" />
                  <Point X="-2.856159912109" Y="27.815798828125" />
                  <Point X="-2.818178222656" Y="27.853779296875" />
                  <Point X="-2.811268310547" Y="27.861486328125" />
                  <Point X="-2.798323486328" Y="27.87761328125" />
                  <Point X="-2.792288574219" Y="27.886033203125" />
                  <Point X="-2.780658691406" Y="27.904287109375" />
                  <Point X="-2.775577392578" Y="27.913314453125" />
                  <Point X="-2.766427490234" Y="27.9318671875" />
                  <Point X="-2.759351806641" Y="27.951306640625" />
                  <Point X="-2.754434814453" Y="27.971400390625" />
                  <Point X="-2.752524902344" Y="27.981580078125" />
                  <Point X="-2.749699707031" Y="28.0030390625" />
                  <Point X="-2.748909667969" Y="28.0133671875" />
                  <Point X="-2.748458496094" Y="28.034046875" />
                  <Point X="-2.748797363281" Y="28.0443984375" />
                  <Point X="-2.751600585938" Y="28.076439453125" />
                  <Point X="-2.756281982422" Y="28.12994921875" />
                  <Point X="-2.757745605469" Y="28.140203125" />
                  <Point X="-2.761780761719" Y="28.160490234375" />
                  <Point X="-2.764352294922" Y="28.1705234375" />
                  <Point X="-2.770860839844" Y="28.191166015625" />
                  <Point X="-2.774509277344" Y="28.200859375" />
                  <Point X="-2.782840087891" Y="28.21979296875" />
                  <Point X="-2.787522460938" Y="28.229033203125" />
                  <Point X="-3.023420410156" Y="28.637619140625" />
                  <Point X="-3.059388183594" Y="28.699916015625" />
                  <Point X="-3.004720947266" Y="28.741830078125" />
                  <Point X="-2.648384033203" Y="29.015029296875" />
                  <Point X="-2.41954296875" Y="29.14216796875" />
                  <Point X="-2.192525390625" Y="29.268294921875" />
                  <Point X="-2.118564697266" Y="29.17191015625" />
                  <Point X="-2.1118203125" Y="29.164048828125" />
                  <Point X="-2.097517333984" Y="29.149107421875" />
                  <Point X="-2.089958251953" Y="29.14202734375" />
                  <Point X="-2.07337890625" Y="29.128115234375" />
                  <Point X="-2.065095703125" Y="29.12190234375" />
                  <Point X="-2.047898193359" Y="29.11041015625" />
                  <Point X="-2.038983642578" Y="29.105130859375" />
                  <Point X="-2.003323608398" Y="29.08656640625" />
                  <Point X="-1.943767456055" Y="29.0555625" />
                  <Point X="-1.934333984375" Y="29.0512890625" />
                  <Point X="-1.915059936523" Y="29.04379296875" />
                  <Point X="-1.90521875" Y="29.0405703125" />
                  <Point X="-1.884311523438" Y="29.034966796875" />
                  <Point X="-1.874173828125" Y="29.032833984375" />
                  <Point X="-1.853724243164" Y="29.029685546875" />
                  <Point X="-1.833053710938" Y="29.028783203125" />
                  <Point X="-1.812407470703" Y="29.03013671875" />
                  <Point X="-1.802120239258" Y="29.031376953125" />
                  <Point X="-1.7808046875" Y="29.03513671875" />
                  <Point X="-1.770720703125" Y="29.03748828125" />
                  <Point X="-1.750867675781" Y="29.043275390625" />
                  <Point X="-1.741098754883" Y="29.0467109375" />
                  <Point X="-1.703956298828" Y="29.062095703125" />
                  <Point X="-1.641924682617" Y="29.087791015625" />
                  <Point X="-1.632584350586" Y="29.092271484375" />
                  <Point X="-1.614449951172" Y="29.102220703125" />
                  <Point X="-1.605655883789" Y="29.107689453125" />
                  <Point X="-1.587926513672" Y="29.120103515625" />
                  <Point X="-1.579778320312" Y="29.126498046875" />
                  <Point X="-1.564226196289" Y="29.14013671875" />
                  <Point X="-1.550251098633" Y="29.155388671875" />
                  <Point X="-1.538019775391" Y="29.1720703125" />
                  <Point X="-1.532359619141" Y="29.180744140625" />
                  <Point X="-1.521537841797" Y="29.19948828125" />
                  <Point X="-1.516855102539" Y="29.208728515625" />
                  <Point X="-1.508524658203" Y="29.227662109375" />
                  <Point X="-1.504877075195" Y="29.23735546875" />
                  <Point X="-1.492788085938" Y="29.275697265625" />
                  <Point X="-1.47259765625" Y="29.339732421875" />
                  <Point X="-1.470026123047" Y="29.349765625" />
                  <Point X="-1.465990844727" Y="29.370052734375" />
                  <Point X="-1.464527099609" Y="29.380306640625" />
                  <Point X="-1.46264074707" Y="29.4018671875" />
                  <Point X="-1.462301757812" Y="29.41221875" />
                  <Point X="-1.462752929688" Y="29.4328984375" />
                  <Point X="-1.46354309082" Y="29.4432265625" />
                  <Point X="-1.479266113281" Y="29.562654296875" />
                  <Point X="-1.39248828125" Y="29.586984375" />
                  <Point X="-0.931163513184" Y="29.7163203125" />
                  <Point X="-0.653772521973" Y="29.748787109375" />
                  <Point X="-0.365222686768" Y="29.78255859375" />
                  <Point X="-0.298039428711" Y="29.531826171875" />
                  <Point X="-0.22566633606" Y="29.2617265625" />
                  <Point X="-0.22043510437" Y="29.247107421875" />
                  <Point X="-0.207661407471" Y="29.218916015625" />
                  <Point X="-0.200119094849" Y="29.20534375" />
                  <Point X="-0.18226071167" Y="29.1786171875" />
                  <Point X="-0.172608703613" Y="29.166455078125" />
                  <Point X="-0.151451187134" Y="29.143865234375" />
                  <Point X="-0.126896156311" Y="29.1250234375" />
                  <Point X="-0.099600372314" Y="29.11043359375" />
                  <Point X="-0.08535382843" Y="29.1042578125" />
                  <Point X="-0.054915859222" Y="29.09392578125" />
                  <Point X="-0.039853370667" Y="29.090154296875" />
                  <Point X="-0.009317459106" Y="29.08511328125" />
                  <Point X="0.021629692078" Y="29.08511328125" />
                  <Point X="0.052165752411" Y="29.090154296875" />
                  <Point X="0.067228240967" Y="29.09392578125" />
                  <Point X="0.097666061401" Y="29.1042578125" />
                  <Point X="0.111912307739" Y="29.11043359375" />
                  <Point X="0.139208236694" Y="29.1250234375" />
                  <Point X="0.163763275146" Y="29.143865234375" />
                  <Point X="0.184920928955" Y="29.166455078125" />
                  <Point X="0.194573242188" Y="29.1786171875" />
                  <Point X="0.21243132019" Y="29.20534375" />
                  <Point X="0.2199737854" Y="29.218916015625" />
                  <Point X="0.232747192383" Y="29.247107421875" />
                  <Point X="0.237978271484" Y="29.2617265625" />
                  <Point X="0.358849700928" Y="29.71282421875" />
                  <Point X="0.378191009521" Y="29.7850078125" />
                  <Point X="0.425071014404" Y="29.78009765625" />
                  <Point X="0.827851013184" Y="29.737916015625" />
                  <Point X="1.057389404297" Y="29.682498046875" />
                  <Point X="1.453623046875" Y="29.586833984375" />
                  <Point X="1.601089233398" Y="29.53334765625" />
                  <Point X="1.858249633789" Y="29.44007421875" />
                  <Point X="2.002716430664" Y="29.37251171875" />
                  <Point X="2.250429931641" Y="29.2566640625" />
                  <Point X="2.390061279297" Y="29.175314453125" />
                  <Point X="2.629432128906" Y="29.035857421875" />
                  <Point X="2.761055175781" Y="28.94225390625" />
                  <Point X="2.817779052734" Y="28.901916015625" />
                  <Point X="2.388001708984" Y="28.157517578125" />
                  <Point X="2.06530859375" Y="27.59859765625" />
                  <Point X="2.06237109375" Y="27.5931015625" />
                  <Point X="2.053180664062" Y="27.573435546875" />
                  <Point X="2.044182373047" Y="27.549564453125" />
                  <Point X="2.041301757812" Y="27.540599609375" />
                  <Point X="2.033260986328" Y="27.51053125" />
                  <Point X="2.01983203125" Y="27.4603125" />
                  <Point X="2.017912231445" Y="27.451462890625" />
                  <Point X="2.013646972656" Y="27.420220703125" />
                  <Point X="2.012755615234" Y="27.383240234375" />
                  <Point X="2.013411254883" Y="27.369578125" />
                  <Point X="2.016546386719" Y="27.343578125" />
                  <Point X="2.021782592773" Y="27.300154296875" />
                  <Point X="2.02380078125" Y="27.289033203125" />
                  <Point X="2.029144287109" Y="27.267107421875" />
                  <Point X="2.032469604492" Y="27.256302734375" />
                  <Point X="2.040735961914" Y="27.23421484375" />
                  <Point X="2.04532019043" Y="27.223884765625" />
                  <Point X="2.055681152344" Y="27.20384375" />
                  <Point X="2.061458251953" Y="27.1941328125" />
                  <Point X="2.077546630859" Y="27.170421875" />
                  <Point X="2.104416015625" Y="27.13082421875" />
                  <Point X="2.109808105469" Y="27.123634765625" />
                  <Point X="2.130462646484" Y="27.100125" />
                  <Point X="2.157596679688" Y="27.075388671875" />
                  <Point X="2.168258056641" Y="27.066982421875" />
                  <Point X="2.191968017578" Y="27.05089453125" />
                  <Point X="2.231566650391" Y="27.0240234375" />
                  <Point X="2.241276123047" Y="27.01824609375" />
                  <Point X="2.26131640625" Y="27.007884765625" />
                  <Point X="2.271647216797" Y="27.00330078125" />
                  <Point X="2.293736816406" Y="26.995033203125" />
                  <Point X="2.304544677734" Y="26.99170703125" />
                  <Point X="2.326473388672" Y="26.98636328125" />
                  <Point X="2.337594238281" Y="26.984345703125" />
                  <Point X="2.363594970703" Y="26.9812109375" />
                  <Point X="2.407019042969" Y="26.975974609375" />
                  <Point X="2.416040771484" Y="26.9753203125" />
                  <Point X="2.447578369141" Y="26.97549609375" />
                  <Point X="2.484317626953" Y="26.979822265625" />
                  <Point X="2.497750488281" Y="26.98239453125" />
                  <Point X="2.527819091797" Y="26.990435546875" />
                  <Point X="2.578036865234" Y="27.003865234375" />
                  <Point X="2.584004638672" Y="27.005673828125" />
                  <Point X="2.604415039063" Y="27.013072265625" />
                  <Point X="2.627659912109" Y="27.023578125" />
                  <Point X="2.636033935547" Y="27.027875" />
                  <Point X="3.56949609375" Y="27.56680859375" />
                  <Point X="3.940404541016" Y="27.780953125" />
                  <Point X="4.043948486328" Y="27.63705078125" />
                  <Point X="4.117329589844" Y="27.51578515625" />
                  <Point X="4.136883789062" Y="27.483470703125" />
                  <Point X="3.597807373047" Y="27.06982421875" />
                  <Point X="3.172951660156" Y="26.7438203125" />
                  <Point X="3.168137939453" Y="26.739869140625" />
                  <Point X="3.152118408203" Y="26.725216796875" />
                  <Point X="3.134669677734" Y="26.70660546875" />
                  <Point X="3.128578125" Y="26.69942578125" />
                  <Point X="3.106937744141" Y="26.6711953125" />
                  <Point X="3.070795898438" Y="26.624044921875" />
                  <Point X="3.065635986328" Y="26.616603515625" />
                  <Point X="3.049738769531" Y="26.58937109375" />
                  <Point X="3.034762695312" Y="26.555544921875" />
                  <Point X="3.030140380859" Y="26.542671875" />
                  <Point X="3.022079345703" Y="26.51384765625" />
                  <Point X="3.008616455078" Y="26.46570703125" />
                  <Point X="3.006225341797" Y="26.454662109375" />
                  <Point X="3.002771728516" Y="26.43236328125" />
                  <Point X="3.001709228516" Y="26.421109375" />
                  <Point X="3.000893310547" Y="26.3975390625" />
                  <Point X="3.001174804688" Y="26.386240234375" />
                  <Point X="3.003077880859" Y="26.363755859375" />
                  <Point X="3.004699462891" Y="26.3525703125" />
                  <Point X="3.011316650391" Y="26.3205" />
                  <Point X="3.022368408203" Y="26.2669375" />
                  <Point X="3.02459765625" Y="26.258234375" />
                  <Point X="3.034684082031" Y="26.228607421875" />
                  <Point X="3.050287353516" Y="26.195369140625" />
                  <Point X="3.056920166016" Y="26.183521484375" />
                  <Point X="3.074918457031" Y="26.156166015625" />
                  <Point X="3.104977783203" Y="26.1104765625" />
                  <Point X="3.111740234375" Y="26.101421875" />
                  <Point X="3.126291748047" Y="26.0841796875" />
                  <Point X="3.134080810547" Y="26.0759921875" />
                  <Point X="3.15132421875" Y="26.05990234375" />
                  <Point X="3.16003515625" Y="26.0526953125" />
                  <Point X="3.178246582031" Y="26.039369140625" />
                  <Point X="3.187747070312" Y="26.03325" />
                  <Point X="3.213829101562" Y="26.018568359375" />
                  <Point X="3.257389160156" Y="25.994046875" />
                  <Point X="3.265475097656" Y="25.989990234375" />
                  <Point X="3.294680908203" Y="25.97808203125" />
                  <Point X="3.330279541016" Y="25.96801953125" />
                  <Point X="3.343674560547" Y="25.965255859375" />
                  <Point X="3.378939208984" Y="25.960595703125" />
                  <Point X="3.437835205078" Y="25.9528125" />
                  <Point X="3.444033691406" Y="25.95219921875" />
                  <Point X="3.465708740234" Y="25.95122265625" />
                  <Point X="3.491213867188" Y="25.95203125" />
                  <Point X="3.500603515625" Y="25.952796875" />
                  <Point X="4.387330078125" Y="26.06953515625" />
                  <Point X="4.704703613281" Y="26.1113203125" />
                  <Point X="4.75268359375" Y="25.914232421875" />
                  <Point X="4.775807128906" Y="25.765712890625" />
                  <Point X="4.783870605469" Y="25.713923828125" />
                  <Point X="4.178885742188" Y="25.55181640625" />
                  <Point X="3.691991699219" Y="25.421353515625" />
                  <Point X="3.686021972656" Y="25.419541015625" />
                  <Point X="3.665625488281" Y="25.41213671875" />
                  <Point X="3.642385253906" Y="25.40162109375" />
                  <Point X="3.634008300781" Y="25.397318359375" />
                  <Point X="3.599361816406" Y="25.37729296875" />
                  <Point X="3.541498291016" Y="25.34384765625" />
                  <Point X="3.533881347656" Y="25.3389453125" />
                  <Point X="3.508778564453" Y="25.319875" />
                  <Point X="3.481994628906" Y="25.294353515625" />
                  <Point X="3.472793457031" Y="25.284224609375" />
                  <Point X="3.452005615234" Y="25.257734375" />
                  <Point X="3.417287597656" Y="25.21349609375" />
                  <Point X="3.410850585938" Y="25.204203125" />
                  <Point X="3.399128173828" Y="25.184923828125" />
                  <Point X="3.393842773438" Y="25.1749375" />
                  <Point X="3.384068603516" Y="25.15347265625" />
                  <Point X="3.380005126953" Y="25.142927734375" />
                  <Point X="3.373159179688" Y="25.121427734375" />
                  <Point X="3.370376708984" Y="25.11047265625" />
                  <Point X="3.363447265625" Y="25.074291015625" />
                  <Point X="3.351874511719" Y="25.013861328125" />
                  <Point X="3.350603515625" Y="25.004966796875" />
                  <Point X="3.348584228516" Y="24.973732421875" />
                  <Point X="3.350280273438" Y="24.937056640625" />
                  <Point X="3.351874755859" Y="24.923576171875" />
                  <Point X="3.358804199219" Y="24.88739453125" />
                  <Point X="3.370376953125" Y="24.826966796875" />
                  <Point X="3.373158935547" Y="24.816013671875" />
                  <Point X="3.380004638672" Y="24.794513671875" />
                  <Point X="3.384068359375" Y="24.783966796875" />
                  <Point X="3.393841552734" Y="24.76250390625" />
                  <Point X="3.399129638672" Y="24.752513671875" />
                  <Point X="3.410854492188" Y="24.733232421875" />
                  <Point X="3.417291259766" Y="24.72394140625" />
                  <Point X="3.438079101562" Y="24.697453125" />
                  <Point X="3.472797119141" Y="24.653212890625" />
                  <Point X="3.47871484375" Y="24.646369140625" />
                  <Point X="3.501132080078" Y="24.624201171875" />
                  <Point X="3.530174804688" Y="24.601275390625" />
                  <Point X="3.541497314453" Y="24.593591796875" />
                  <Point X="3.576143798828" Y="24.57356640625" />
                  <Point X="3.634007324219" Y="24.540119140625" />
                  <Point X="3.639498046875" Y="24.537181640625" />
                  <Point X="3.65915625" Y="24.527982421875" />
                  <Point X="3.683027099609" Y="24.518970703125" />
                  <Point X="3.691992431641" Y="24.516083984375" />
                  <Point X="4.505160644531" Y="24.298197265625" />
                  <Point X="4.784876953125" Y="24.223248046875" />
                  <Point X="4.761614257812" Y="24.06894921875" />
                  <Point X="4.731987792969" Y="23.939123046875" />
                  <Point X="4.727802246094" Y="23.92078125" />
                  <Point X="4.001441894531" Y="24.016408203125" />
                  <Point X="3.436781982422" Y="24.09074609375" />
                  <Point X="3.428622802734" Y="24.09146484375" />
                  <Point X="3.400096679688" Y="24.09195703125" />
                  <Point X="3.366720703125" Y="24.089158203125" />
                  <Point X="3.354482666016" Y="24.087322265625" />
                  <Point X="3.286483886719" Y="24.07254296875" />
                  <Point X="3.172918457031" Y="24.047859375" />
                  <Point X="3.157874755859" Y="24.0432578125" />
                  <Point X="3.128753173828" Y="24.0316328125" />
                  <Point X="3.114675292969" Y="24.024609375" />
                  <Point X="3.086848632812" Y="24.007716796875" />
                  <Point X="3.074123046875" Y="23.99846875" />
                  <Point X="3.050373535156" Y="23.977998046875" />
                  <Point X="3.039349609375" Y="23.966775390625" />
                  <Point X="2.998248535156" Y="23.91734375" />
                  <Point X="2.92960546875" Y="23.834787109375" />
                  <Point X="2.921327148438" Y="23.82315234375" />
                  <Point X="2.906606689453" Y="23.798771484375" />
                  <Point X="2.900164550781" Y="23.786025390625" />
                  <Point X="2.888821289062" Y="23.758640625" />
                  <Point X="2.88436328125" Y="23.745072265625" />
                  <Point X="2.87753125" Y="23.717421875" />
                  <Point X="2.875157226562" Y="23.70333984375" />
                  <Point X="2.869266357422" Y="23.63932421875" />
                  <Point X="2.859428222656" Y="23.532408203125" />
                  <Point X="2.859288818359" Y="23.516677734375" />
                  <Point X="2.861607666016" Y="23.485408203125" />
                  <Point X="2.864065917969" Y="23.469869140625" />
                  <Point X="2.871797607422" Y="23.438248046875" />
                  <Point X="2.876786865234" Y="23.423328125" />
                  <Point X="2.889158447266" Y="23.394517578125" />
                  <Point X="2.896540771484" Y="23.380626953125" />
                  <Point X="2.934172363281" Y="23.32209375" />
                  <Point X="2.997020996094" Y="23.224337890625" />
                  <Point X="3.001741455078" Y="23.2176484375" />
                  <Point X="3.019789306641" Y="23.1955546875" />
                  <Point X="3.043486816406" Y="23.171880859375" />
                  <Point X="3.052796142578" Y="23.163720703125" />
                  <Point X="3.807421630859" Y="22.58467578125" />
                  <Point X="4.087170898438" Y="22.370017578125" />
                  <Point X="4.045495361328" Y="22.302580078125" />
                  <Point X="4.001273681641" Y="22.239748046875" />
                  <Point X="3.351360351562" Y="22.614974609375" />
                  <Point X="2.848454589844" Y="22.905328125" />
                  <Point X="2.841182373047" Y="22.909119140625" />
                  <Point X="2.815025878906" Y="22.920484375" />
                  <Point X="2.783120605469" Y="22.930671875" />
                  <Point X="2.771107421875" Y="22.933662109375" />
                  <Point X="2.690178222656" Y="22.94827734375" />
                  <Point X="2.555017089844" Y="22.9726875" />
                  <Point X="2.539358886719" Y="22.97419140625" />
                  <Point X="2.508009277344" Y="22.974595703125" />
                  <Point X="2.492317871094" Y="22.97349609375" />
                  <Point X="2.460145019531" Y="22.9685390625" />
                  <Point X="2.444846435547" Y="22.96486328125" />
                  <Point X="2.415069335938" Y="22.9550390625" />
                  <Point X="2.400590820312" Y="22.948890625" />
                  <Point X="2.333358398438" Y="22.9135078125" />
                  <Point X="2.221072753906" Y="22.854412109375" />
                  <Point X="2.208970214844" Y="22.846830078125" />
                  <Point X="2.186039550781" Y="22.8299375" />
                  <Point X="2.175211425781" Y="22.820626953125" />
                  <Point X="2.154251708984" Y="22.79966796875" />
                  <Point X="2.144940429687" Y="22.78883984375" />
                  <Point X="2.128046630859" Y="22.765908203125" />
                  <Point X="2.120464111328" Y="22.7538046875" />
                  <Point X="2.085080078125" Y="22.686572265625" />
                  <Point X="2.025984985352" Y="22.574287109375" />
                  <Point X="2.019836547852" Y="22.55980859375" />
                  <Point X="2.010012573242" Y="22.530033203125" />
                  <Point X="2.006337036133" Y="22.514736328125" />
                  <Point X="2.001379272461" Y="22.4825625" />
                  <Point X="2.000279174805" Y="22.466869140625" />
                  <Point X="2.00068347168" Y="22.435517578125" />
                  <Point X="2.002187866211" Y="22.419859375" />
                  <Point X="2.016803588867" Y="22.3389296875" />
                  <Point X="2.041213500977" Y="22.203767578125" />
                  <Point X="2.043014648438" Y="22.195779296875" />
                  <Point X="2.051236572266" Y="22.168453125" />
                  <Point X="2.064069824219" Y="22.137517578125" />
                  <Point X="2.069546630859" Y="22.126419921875" />
                  <Point X="2.554469482422" Y="21.286509765625" />
                  <Point X="2.735893066406" Y="20.972275390625" />
                  <Point X="2.723751953125" Y="20.963916015625" />
                  <Point X="2.217678466797" Y="21.6234453125" />
                  <Point X="1.833914794922" Y="22.123576171875" />
                  <Point X="1.828654663086" Y="22.1298515625" />
                  <Point X="1.808836914062" Y="22.1503671875" />
                  <Point X="1.783252929688" Y="22.171994140625" />
                  <Point X="1.773297119141" Y="22.179353515625" />
                  <Point X="1.693478881836" Y="22.23066796875" />
                  <Point X="1.560173706055" Y="22.31637109375" />
                  <Point X="1.546280151367" Y="22.32375390625" />
                  <Point X="1.517467163086" Y="22.336125" />
                  <Point X="1.502547607422" Y="22.34111328125" />
                  <Point X="1.470927368164" Y="22.34884375" />
                  <Point X="1.455391235352" Y="22.351302734375" />
                  <Point X="1.42412487793" Y="22.35362109375" />
                  <Point X="1.40839465332" Y="22.35348046875" />
                  <Point X="1.321099853516" Y="22.345447265625" />
                  <Point X="1.175307739258" Y="22.332033203125" />
                  <Point X="1.161224365234" Y="22.32966015625" />
                  <Point X="1.133574951172" Y="22.322828125" />
                  <Point X="1.120008666992" Y="22.318369140625" />
                  <Point X="1.092622314453" Y="22.307025390625" />
                  <Point X="1.079876342773" Y="22.300583984375" />
                  <Point X="1.055494140625" Y="22.28586328125" />
                  <Point X="1.043857910156" Y="22.277583984375" />
                  <Point X="0.97645111084" Y="22.221537109375" />
                  <Point X="0.863874206543" Y="22.127931640625" />
                  <Point X="0.852652587891" Y="22.116908203125" />
                  <Point X="0.83218170166" Y="22.093158203125" />
                  <Point X="0.822932739258" Y="22.080431640625" />
                  <Point X="0.806040710449" Y="22.05260546875" />
                  <Point X="0.799019287109" Y="22.038529296875" />
                  <Point X="0.787394775391" Y="22.00941015625" />
                  <Point X="0.782791931152" Y="21.9943671875" />
                  <Point X="0.762637634277" Y="21.901642578125" />
                  <Point X="0.728977661133" Y="21.74678125" />
                  <Point X="0.727584777832" Y="21.7387109375" />
                  <Point X="0.724724487305" Y="21.710322265625" />
                  <Point X="0.7247421875" Y="21.676830078125" />
                  <Point X="0.725554992676" Y="21.66448046875" />
                  <Point X="0.833091247559" Y="20.84766015625" />
                  <Point X="0.805089050293" Y="20.952166015625" />
                  <Point X="0.655064941406" Y="21.512064453125" />
                  <Point X="0.652605834961" Y="21.519876953125" />
                  <Point X="0.642146789551" Y="21.546416015625" />
                  <Point X="0.626788085938" Y="21.57618359375" />
                  <Point X="0.620407287598" Y="21.58679296875" />
                  <Point X="0.559088745117" Y="21.675140625" />
                  <Point X="0.4566796875" Y="21.822693359375" />
                  <Point X="0.446671112061" Y="21.834828125" />
                  <Point X="0.424787750244" Y="21.857283203125" />
                  <Point X="0.412912780762" Y="21.867603515625" />
                  <Point X="0.386657348633" Y="21.88684765625" />
                  <Point X="0.373240112305" Y="21.89506640625" />
                  <Point X="0.345238677979" Y="21.909173828125" />
                  <Point X="0.330654296875" Y="21.9150625" />
                  <Point X="0.235767349243" Y="21.94451171875" />
                  <Point X="0.077295211792" Y="21.9936953125" />
                  <Point X="0.063377120972" Y="21.996890625" />
                  <Point X="0.035217838287" Y="22.00116015625" />
                  <Point X="0.020976644516" Y="22.002234375" />
                  <Point X="-0.008664708138" Y="22.002234375" />
                  <Point X="-0.022905902863" Y="22.00116015625" />
                  <Point X="-0.051065185547" Y="21.996890625" />
                  <Point X="-0.064983421326" Y="21.9936953125" />
                  <Point X="-0.159870223999" Y="21.96424609375" />
                  <Point X="-0.318342376709" Y="21.9150625" />
                  <Point X="-0.332931030273" Y="21.909169921875" />
                  <Point X="-0.360932922363" Y="21.895060546875" />
                  <Point X="-0.374346160889" Y="21.88684375" />
                  <Point X="-0.400601287842" Y="21.867599609375" />
                  <Point X="-0.412475372314" Y="21.857283203125" />
                  <Point X="-0.434359039307" Y="21.834828125" />
                  <Point X="-0.444368499756" Y="21.822689453125" />
                  <Point X="-0.505687042236" Y="21.73433984375" />
                  <Point X="-0.608096069336" Y="21.5867890625" />
                  <Point X="-0.612470092773" Y="21.579869140625" />
                  <Point X="-0.625976318359" Y="21.554736328125" />
                  <Point X="-0.638777648926" Y="21.52378515625" />
                  <Point X="-0.642752990723" Y="21.512064453125" />
                  <Point X="-0.893310119629" Y="20.576970703125" />
                  <Point X="-0.985425109863" Y="20.233193359375" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.70623242323" Y="25.693120455469" />
                  <Point X="4.612486406381" Y="26.099179065759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.614413609281" Y="25.668517342049" />
                  <Point X="4.51786366235" Y="26.086721107845" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.522594795332" Y="25.643914228629" />
                  <Point X="4.423240918318" Y="26.07426314993" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.103766477636" Y="27.458058982195" />
                  <Point X="4.073851612235" Y="27.587634499962" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.772316556315" Y="24.139936354803" />
                  <Point X="4.750986027052" Y="24.23232902769" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.430775981383" Y="25.619311115208" />
                  <Point X="4.328618063646" Y="26.061805671256" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.020940309007" Y="27.394504442524" />
                  <Point X="3.932742844262" Y="27.776529633241" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.725338731881" Y="23.921105577116" />
                  <Point X="4.647058103555" Y="24.260176230134" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.338957167434" Y="25.594708001788" />
                  <Point X="4.2339951413" Y="26.049348485705" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.938114140379" Y="27.330949902853" />
                  <Point X="3.846711280587" Y="27.726859184882" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.624783535841" Y="23.934343891952" />
                  <Point X="4.543130180059" Y="24.288023432578" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.247138353485" Y="25.570104888368" />
                  <Point X="4.139372218954" Y="26.036891300155" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.85528797175" Y="27.267395363182" />
                  <Point X="3.760679716912" Y="27.677188736522" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.524228339801" Y="23.947582206788" />
                  <Point X="4.439202246115" Y="24.315870680273" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.155319521029" Y="25.545501855111" />
                  <Point X="4.044749296608" Y="26.024434114604" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.772461803121" Y="27.203840823511" />
                  <Point X="3.674648153236" Y="27.627518288163" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.423673143761" Y="23.960820521623" />
                  <Point X="4.335274306158" Y="24.343717954016" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.063500634972" Y="25.520899054026" />
                  <Point X="3.950126374262" Y="26.011976929054" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.689635634493" Y="27.140286283839" />
                  <Point X="3.588616589561" Y="27.577847839804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.323117947721" Y="23.974058836459" />
                  <Point X="4.231346366201" Y="24.371565227759" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.971681748915" Y="25.49629625294" />
                  <Point X="3.855503451916" Y="25.999519743504" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.606809465864" Y="27.076731744168" />
                  <Point X="3.502584994632" Y="27.528177526821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.222562751681" Y="23.987297151295" />
                  <Point X="4.127418426244" Y="24.399412501503" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.879862862858" Y="25.471693451854" />
                  <Point X="3.76088052957" Y="25.987062557953" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.523983357" Y="27.013176945626" />
                  <Point X="3.416553390771" Y="27.478507252523" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.122007555641" Y="24.000535466131" />
                  <Point X="4.023490486286" Y="24.427259775246" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.7880439768" Y="25.447090650768" />
                  <Point X="3.666257607225" Y="25.974605372403" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.441157255424" Y="26.949622115518" />
                  <Point X="3.330521786911" Y="27.428836978225" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.021452359601" Y="24.013773780966" />
                  <Point X="3.919562546329" Y="24.455107048989" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.696225090743" Y="25.422487849683" />
                  <Point X="3.571634684879" Y="25.962148186853" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.358331153848" Y="26.88606728541" />
                  <Point X="3.24449018305" Y="27.379166703927" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.920897192111" Y="24.027011972138" />
                  <Point X="3.815634606372" Y="24.482954322733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.608006689356" Y="25.382289636113" />
                  <Point X="3.476578592882" Y="25.951567265196" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.275505052272" Y="26.822512455302" />
                  <Point X="3.15845857919" Y="27.329496429629" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.801818975686" Y="28.87427226802" />
                  <Point X="2.791048357863" Y="28.920924939274" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.820342031714" Y="24.040250132587" />
                  <Point X="3.711706666414" Y="24.510801596476" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.522509169088" Y="25.330305991619" />
                  <Point X="3.376934151629" Y="25.960860667643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.192678950696" Y="26.758957625194" />
                  <Point X="3.072426975329" Y="27.279826155331" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.732170774294" Y="28.753637681192" />
                  <Point X="2.674397735777" Y="29.003880203711" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.719786871317" Y="24.053488293035" />
                  <Point X="3.603347756716" Y="24.557841508754" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.444092582138" Y="25.247651455292" />
                  <Point X="3.273462000719" Y="25.986733702122" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.113473260178" Y="26.679721071934" />
                  <Point X="2.986395371469" Y="27.230155881034" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.662522572902" Y="28.633003094365" />
                  <Point X="2.56020504504" Y="29.076188997812" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="4.031204715267" Y="22.282275324298" />
                  <Point X="3.994537813027" Y="22.441097126738" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.61923171092" Y="24.066726453484" />
                  <Point X="3.487391554518" Y="24.637788910201" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.374652209866" Y="25.126116661646" />
                  <Point X="3.160876869591" Y="26.052079390562" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.041256466833" Y="26.570212279179" />
                  <Point X="2.900363767608" Y="27.180485606736" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.592874371509" Y="28.512368507537" />
                  <Point X="2.447554238598" Y="29.14181915729" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.934642678946" Y="22.278217364152" />
                  <Point X="3.876048463169" Y="22.532016796162" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.518676550523" Y="24.079964613932" />
                  <Point X="2.814332163748" Y="27.130815332438" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.523226170117" Y="28.39173392071" />
                  <Point X="2.33490336529" Y="29.207449606396" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.822149398367" Y="22.343165204152" />
                  <Point X="3.757559057548" Y="22.622936707121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.418482229252" Y="24.091639808413" />
                  <Point X="2.728300559887" Y="27.08114505814" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.453577968725" Y="28.271099333882" />
                  <Point X="2.223090605213" Y="29.269449788258" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.709656117788" Y="22.408113044152" />
                  <Point X="3.63906957518" Y="22.713856950508" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.323533120779" Y="24.080595490204" />
                  <Point X="2.642268956027" Y="27.031474783842" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.383929761854" Y="28.150464770784" />
                  <Point X="2.11379063607" Y="29.320565876818" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.597162837209" Y="22.473060884152" />
                  <Point X="3.520580092812" Y="22.804777193896" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.230692867146" Y="24.060416718131" />
                  <Point X="2.552708054645" Y="26.99709157661" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.314281466757" Y="28.029830589835" />
                  <Point X="2.004490666926" Y="29.371681965379" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.484669556631" Y="22.538008724153" />
                  <Point X="3.402090610444" Y="22.895697437283" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.13890378555" Y="24.035684819741" />
                  <Point X="2.45986096581" Y="26.976942411053" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.24463317166" Y="27.909196408886" />
                  <Point X="1.895190694994" Y="29.422798066017" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.372176276052" Y="22.602956564153" />
                  <Point X="3.283601128076" Y="22.98661768067" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.054001074086" Y="23.981124775264" />
                  <Point X="2.361313093658" Y="26.981486050893" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.174984876564" Y="27.788562227937" />
                  <Point X="1.787804410959" Y="29.465625073698" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.259682919263" Y="22.667904734257" />
                  <Point X="3.165111645708" Y="23.077537924057" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.97709951377" Y="23.891907937622" />
                  <Point X="2.257232052508" Y="27.009996478762" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.105336581467" Y="27.667928046988" />
                  <Point X="1.681395113209" Y="29.504220288856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.147189545169" Y="22.732852979314" />
                  <Point X="3.046426968818" Y="23.169303647809" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.902828627355" Y="23.791296399445" />
                  <Point X="2.141181276557" Y="27.090353524136" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.03911857843" Y="27.532435638742" />
                  <Point X="1.574985834984" Y="29.542815419442" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="3.034696171076" Y="22.797801224371" />
                  <Point X="2.902157046109" Y="23.371891246563" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.86104424583" Y="23.549970349098" />
                  <Point X="1.468576616827" Y="29.581410289846" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.922202796982" Y="22.862749469427" />
                  <Point X="1.364879038416" Y="29.608259758109" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.811084182418" Y="22.92174297675" />
                  <Point X="1.261624830047" Y="29.63318877973" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.708211202483" Y="22.945020716609" />
                  <Point X="1.158370621679" Y="29.658117801351" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.606470266571" Y="22.963395035093" />
                  <Point X="1.055116413991" Y="29.683046820024" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.506411350565" Y="22.974483724935" />
                  <Point X="0.951862236535" Y="29.707975707744" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.41355051964" Y="22.954394082909" />
                  <Point X="0.84860805908" Y="29.732904595464" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.326343336917" Y="22.909815800091" />
                  <Point X="0.748022090766" Y="29.746276199664" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.658440205704" Y="21.049032134169" />
                  <Point X="2.636360216474" Y="21.144671074821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.239407611375" Y="22.864061707045" />
                  <Point X="0.648107468698" Y="29.756739883794" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.518981960437" Y="21.230778068167" />
                  <Point X="2.473895934643" Y="21.426067101161" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.156301883477" Y="22.801718071601" />
                  <Point X="0.54819284663" Y="29.767203567924" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.37952371517" Y="21.412524002165" />
                  <Point X="2.311431623596" Y="21.707463254048" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.085293066213" Y="22.686976959598" />
                  <Point X="0.448278224562" Y="29.777667252055" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.240065469903" Y="21.594269936163" />
                  <Point X="2.148967312549" Y="21.988859406934" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.01825792565" Y="22.555023962828" />
                  <Point X="0.362556053476" Y="29.726656677157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="2.100607307655" Y="21.77601551057" />
                  <Point X="0.310182667648" Y="29.531196643481" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.961149161281" Y="21.957761016213" />
                  <Point X="0.257809259173" Y="29.335736707898" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.822433094586" Y="22.136292221634" />
                  <Point X="0.19606785174" Y="29.180854033795" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.70483072236" Y="22.223369968843" />
                  <Point X="0.11450665714" Y="29.111820289636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.590338041009" Y="22.296978165051" />
                  <Point X="0.023116884763" Y="29.085358792998" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.481459494528" Y="22.346268871516" />
                  <Point X="-0.07818325002" Y="29.101823792027" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.382838623411" Y="22.351128704615" />
                  <Point X="-0.199297856861" Y="29.204114698737" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.287367922129" Y="22.342343653072" />
                  <Point X="-0.428627804672" Y="29.775137744083" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.191897001771" Y="22.333559550454" />
                  <Point X="-0.523561537751" Y="29.764026827727" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.099834331862" Y="22.310012693238" />
                  <Point X="-0.618495270831" Y="29.752915911372" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="1.015303514157" Y="22.253841799915" />
                  <Point X="-0.71342893968" Y="29.741804716805" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.933506497564" Y="22.185829513034" />
                  <Point X="-0.808362570548" Y="29.730693357721" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.85207425242" Y="22.116237227415" />
                  <Point X="-0.903296201416" Y="29.719581998637" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.782752820143" Y="21.99418724805" />
                  <Point X="-0.995854880117" Y="29.69818359154" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.735472981787" Y="21.776664636383" />
                  <Point X="-1.087426721708" Y="29.672510723312" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.807401372291" Y="21.042794457392" />
                  <Point X="-1.1789985633" Y="29.646837855084" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.566318847895" Y="21.66472350468" />
                  <Point X="-1.270570404891" Y="29.621164986856" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.424257696765" Y="21.857743862629" />
                  <Point X="-1.362142246483" Y="29.595492118628" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.312203726756" Y="21.920788839492" />
                  <Point X="-1.453713897263" Y="29.569818423904" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.207179593689" Y="21.953384247246" />
                  <Point X="-1.48731617104" Y="29.293051771244" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.102155460061" Y="21.985979657426" />
                  <Point X="-1.552473156766" Y="29.152963592114" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="0.000903872577" Y="22.002234375" />
                  <Point X="-1.635623588603" Y="29.090813590704" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.092641822859" Y="21.985111207081" />
                  <Point X="-1.724525448627" Y="29.053575761732" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.183621764651" Y="21.956874539149" />
                  <Point X="-1.816550307011" Y="29.029865124813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.274601713316" Y="21.928637900989" />
                  <Point X="-1.917482186296" Y="29.044735034035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.363925437459" Y="21.893227366272" />
                  <Point X="-2.027550402474" Y="29.09917876609" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.444946950387" Y="21.821856003972" />
                  <Point X="-2.151857203791" Y="29.21529658616" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.518109127966" Y="21.716442120218" />
                  <Point X="-2.253739958662" Y="29.234285190042" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.591271638496" Y="21.611029678634" />
                  <Point X="-2.340154745208" Y="29.186274662302" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.655198976704" Y="21.465615310947" />
                  <Point X="-2.426569540412" Y="29.138264172064" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.707572302692" Y="21.270155018073" />
                  <Point X="-2.512984433437" Y="29.090254105534" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.75994562868" Y="21.074694725199" />
                  <Point X="-2.599399326461" Y="29.042244039003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.812318954667" Y="20.879234432325" />
                  <Point X="-2.684264259791" Y="28.987520359447" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.864692280655" Y="20.68377413945" />
                  <Point X="-2.76710076824" Y="28.924010606459" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.91706564333" Y="20.488314005486" />
                  <Point X="-2.84993727669" Y="28.860500853472" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-0.969439050201" Y="20.292854062956" />
                  <Point X="-2.749643911649" Y="28.003768471602" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.81106404349" Y="28.269808290866" />
                  <Point X="-2.932773785139" Y="28.796991100485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.056342069218" Y="20.246958302386" />
                  <Point X="-1.173378359002" Y="20.753898167999" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.190368244541" Y="20.827489447322" />
                  <Point X="-2.813674726579" Y="27.858802310837" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.973528122077" Y="28.551203436858" />
                  <Point X="-3.015610240801" Y="28.733481118849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.327318849814" Y="20.998373599183" />
                  <Point X="-2.893181657307" Y="27.780870572276" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.449569253062" Y="21.105584180628" />
                  <Point X="-2.979726172248" Y="27.733421959951" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.565445009506" Y="21.185183133233" />
                  <Point X="-3.075086450042" Y="27.724158611733" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.667716506285" Y="21.205855563311" />
                  <Point X="-3.174982399381" Y="27.734541415389" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.766713413674" Y="21.212344188451" />
                  <Point X="-3.282720281882" Y="27.778891363343" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.865710239593" Y="21.218832460706" />
                  <Point X="-3.395213553301" Y="27.843839163669" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-1.964317156607" Y="21.223631851941" />
                  <Point X="-3.507706833621" Y="27.90878700255" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.057046961883" Y="21.202974675479" />
                  <Point X="-2.318961024494" Y="22.337449118813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.368570625243" Y="22.552331907592" />
                  <Point X="-3.620200217505" Y="27.973735290011" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.142888909082" Y="21.152482907924" />
                  <Point X="-2.384890485777" Y="22.200706898919" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.485606301241" Y="22.636955023762" />
                  <Point X="-3.730585617438" Y="28.029552895849" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.227357557118" Y="21.096042728182" />
                  <Point X="-2.454538751652" Y="22.080072591395" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.586212955574" Y="22.650416228952" />
                  <Point X="-3.451775619084" Y="26.399580023626" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.535524603345" Y="26.762336728448" />
                  <Point X="-3.805772878882" Y="27.932910614003" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.311826295479" Y="21.039602939679" />
                  <Point X="-2.524187017526" Y="21.959438283871" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.677896834254" Y="22.625228646667" />
                  <Point X="-3.52321412781" Y="26.286700109822" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.654262650022" Y="26.854333622143" />
                  <Point X="-3.880960035819" Y="27.836267879486" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.394541023784" Y="20.975565698933" />
                  <Point X="-2.593835283401" Y="21.838803976346" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.764115147656" Y="22.576367100245" />
                  <Point X="-2.980179974722" Y="23.512246685961" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.057727457444" Y="23.848141736485" />
                  <Point X="-3.302495441445" Y="24.908348353983" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358628282378" Y="25.151486400238" />
                  <Point X="-3.608919578166" Y="26.235617109487" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.77275211058" Y="26.945253771062" />
                  <Point X="-3.956146969369" Y="27.739624177371" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.470122287666" Y="20.88063002914" />
                  <Point X="-2.663483486497" Y="21.7181693969" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.850146723743" Y="22.526696705647" />
                  <Point X="-3.056338368996" Y="23.41981084254" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.165908788" Y="23.894412468989" />
                  <Point X="-3.369107603118" Y="24.774563234357" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.475888315447" Y="25.237081313649" />
                  <Point X="-3.700267116844" Y="26.208972678601" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.891241571139" Y="27.036173919981" />
                  <Point X="-4.029278612448" Y="27.634078034171" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.566752841138" Y="20.876868849376" />
                  <Point X="-2.73313165114" Y="21.597534650894" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.93617829983" Y="22.477026311049" />
                  <Point X="-3.139164512397" Y="23.356256193596" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.264052627934" Y="23.897206053025" />
                  <Point X="-3.452266509331" Y="24.712449939502" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.580763849847" Y="25.269033069859" />
                  <Point X="-3.797581778594" Y="26.208174697338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.009731031697" Y="27.127094068901" />
                  <Point X="-4.099143775811" Y="27.514383212887" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.680513733464" Y="20.94730731908" />
                  <Point X="-2.802779815784" Y="21.476899904889" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.022209875917" Y="22.42735591645" />
                  <Point X="-3.221990655799" Y="23.292701544652" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.358675517237" Y="23.884748724354" />
                  <Point X="-3.54199537974" Y="24.678794286061" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.684691810813" Y="25.2968804346" />
                  <Point X="-3.898136958054" Y="26.221412940359" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.128220374758" Y="27.218013708886" />
                  <Point X="-4.169008939175" Y="27.394688391603" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.796250766274" Y="21.02630539361" />
                  <Point X="-2.872427980427" Y="21.356265158883" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.108241452004" Y="22.377685521852" />
                  <Point X="-3.3048167992" Y="23.229146895708" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.453298406541" Y="23.872291395683" />
                  <Point X="-3.633814240965" Y="24.654191377414" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.788619771778" Y="25.324727799341" />
                  <Point X="-3.998692137514" Y="26.23465118338" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-2.914827986449" Y="21.117605671194" />
                  <Point X="-2.94207614507" Y="21.235630412878" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.194273028091" Y="22.328015127254" />
                  <Point X="-3.387642942601" Y="23.165592246764" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.547921295845" Y="23.859834067012" />
                  <Point X="-3.725633102189" Y="24.629588468768" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.892547732744" Y="25.352575164082" />
                  <Point X="-4.099247316975" Y="26.247889426401" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.280304604178" Y="22.278344732656" />
                  <Point X="-3.470469086002" Y="23.102037597821" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.642544185149" Y="23.847376738341" />
                  <Point X="-3.817451963414" Y="24.604985560121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.99647569371" Y="25.380422528823" />
                  <Point X="-4.199802501382" Y="26.261127690848" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.366336180265" Y="22.228674338058" />
                  <Point X="-3.553295225607" Y="23.038482932434" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.737167074453" Y="23.83491940967" />
                  <Point X="-3.909270824639" Y="24.580382651475" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.100403654676" Y="25.408269893564" />
                  <Point X="-4.300357702098" Y="26.274366025938" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.452367756352" Y="22.179003943459" />
                  <Point X="-3.636121350895" Y="22.974928205035" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.831789963756" Y="23.822462080999" />
                  <Point X="-4.001089685863" Y="24.555779742828" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.204331615641" Y="25.436117258305" />
                  <Point X="-4.400912902814" Y="26.287604361029" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.538399323831" Y="22.129333511576" />
                  <Point X="-3.718947476183" Y="22.911373477636" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.92641285306" Y="23.810004752328" />
                  <Point X="-4.092908549777" Y="24.53117684583" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.308259576607" Y="25.463964623046" />
                  <Point X="-4.50146810353" Y="26.300842696119" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.62443088354" Y="22.079663046036" />
                  <Point X="-3.801773601472" Y="22.847818750237" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.021035742364" Y="23.797547423657" />
                  <Point X="-4.184727419538" Y="24.506573974157" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.412187537573" Y="25.491811987787" />
                  <Point X="-4.602023304247" Y="26.31408103121" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.710462443249" Y="22.029992580497" />
                  <Point X="-3.88459972676" Y="22.784264022838" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.115658631668" Y="23.785090094986" />
                  <Point X="-4.276546289299" Y="24.481971102485" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.51611550461" Y="25.519659378824" />
                  <Point X="-4.669707298753" Y="26.184938529643" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.798629193016" Y="21.989570639181" />
                  <Point X="-3.967425852048" Y="22.720709295439" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.210281520972" Y="23.772632766315" />
                  <Point X="-4.36836515906" Y="24.457368230813" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.620043473486" Y="25.547506777831" />
                  <Point X="-4.722353341426" Y="25.990659502512" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-3.938575981473" Y="22.173432686221" />
                  <Point X="-4.050251977336" Y="22.65715456804" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.304904447439" Y="23.760175598616" />
                  <Point X="-4.460184028821" Y="24.43276535914" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.723971442363" Y="25.575354176838" />
                  <Point X="-4.762919603674" Y="25.744057197902" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.088711958902" Y="22.401428959469" />
                  <Point X="-4.133078102625" Y="22.593599840641" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.399527419588" Y="23.747718628786" />
                  <Point X="-4.552002898582" Y="24.408162487468" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.494150391737" Y="23.735261658956" />
                  <Point X="-4.643821768343" Y="24.383559615796" />
                </GraphicPath>
                <GraphicPath>
                  <Point X="-4.588773363885" Y="23.722804689126" />
                  <Point X="-4.735640638104" Y="24.358956744124" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillHatchlinesElement>
          <FillOutlinesElement>
            <Label Value="Fill-Outlines" />
            <Area Value="False" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="0" Y="0" />
              <Width Value="0" />
              <Height Value="0" />
            </Box>
            <ChildElements>
              <GraphicsPathsElement>
                <Label Value="Graphics Paths" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.000164306641" Y="25.001626953125" />
                  <Width Value="9.996464355469" />
                  <Height Value="9.978484375" />
                </Box>
                <IsBorder Value="True" />
                <GraphicPath>
                  <IsClosed Value="True" />
                  <Point X="0.621563110352" Y="20.902990234375" />
                  <Point X="0.471539031982" Y="21.462888671875" />
                  <Point X="0.464318817139" Y="21.47845703125" />
                  <Point X="0.403000274658" Y="21.5668046875" />
                  <Point X="0.300591186523" Y="21.714357421875" />
                  <Point X="0.274335754395" Y="21.7336015625" />
                  <Point X="0.179448806763" Y="21.76305078125" />
                  <Point X="0.020976625443" Y="21.812234375" />
                  <Point X="-0.008664756775" Y="21.812234375" />
                  <Point X="-0.103551712036" Y="21.78278515625" />
                  <Point X="-0.262023742676" Y="21.7336015625" />
                  <Point X="-0.288278991699" Y="21.714357421875" />
                  <Point X="-0.349597686768" Y="21.6260078125" />
                  <Point X="-0.452006652832" Y="21.47845703125" />
                  <Point X="-0.459227142334" Y="21.462888671875" />
                  <Point X="-0.709784301758" Y="20.527794921875" />
                  <Point X="-0.847744018555" Y="20.012923828125" />
                  <Point X="-0.914239685059" Y="20.025830078125" />
                  <Point X="-1.100230102539" Y="20.061931640625" />
                  <Point X="-1.209530151367" Y="20.090052734375" />
                  <Point X="-1.351589599609" Y="20.126603515625" />
                  <Point X="-1.327209594727" Y="20.311787109375" />
                  <Point X="-1.309150146484" Y="20.4489609375" />
                  <Point X="-1.309683227539" Y="20.465240234375" />
                  <Point X="-1.33235168457" Y="20.579203125" />
                  <Point X="-1.370210693359" Y="20.769533203125" />
                  <Point X="-1.386282958984" Y="20.79737109375" />
                  <Point X="-1.473642700195" Y="20.873984375" />
                  <Point X="-1.619543701172" Y="21.001935546875" />
                  <Point X="-1.649240844727" Y="21.014236328125" />
                  <Point X="-1.765187011719" Y="21.0218359375" />
                  <Point X="-1.958829956055" Y="21.03452734375" />
                  <Point X="-1.989878295898" Y="21.026208984375" />
                  <Point X="-2.086490722656" Y="20.961654296875" />
                  <Point X="-2.247844726562" Y="20.853841796875" />
                  <Point X="-2.259734130859" Y="20.842705078125" />
                  <Point X="-2.400402587891" Y="20.6593828125" />
                  <Point X="-2.457094726562" Y="20.5855" />
                  <Point X="-2.583186767578" Y="20.663572265625" />
                  <Point X="-2.855839355469" Y="20.832392578125" />
                  <Point X="-3.007161621094" Y="20.94890625" />
                  <Point X="-3.228581054688" Y="21.119390625" />
                  <Point X="-2.784836425781" Y="21.88798046875" />
                  <Point X="-2.506033691406" Y="22.370880859375" />
                  <Point X="-2.499763183594" Y="22.40241015625" />
                  <Point X="-2.513982910156" Y="22.431240234375" />
                  <Point X="-2.531327880859" Y="22.448583984375" />
                  <Point X="-2.560156982422" Y="22.46280078125" />
                  <Point X="-2.59168359375" Y="22.456529296875" />
                  <Point X="-3.398182617188" Y="21.990896484375" />
                  <Point X="-3.842958984375" Y="21.73410546875" />
                  <Point X="-3.946297363281" Y="21.86987109375" />
                  <Point X="-4.161704101562" Y="22.15287109375" />
                  <Point X="-4.270192871094" Y="22.334791015625" />
                  <Point X="-4.431020019531" Y="22.60447265625" />
                  <Point X="-3.651601074219" Y="23.202541015625" />
                  <Point X="-3.163786865234" Y="23.576853515625" />
                  <Point X="-3.145822021484" Y="23.603984375" />
                  <Point X="-3.138117431641" Y="23.633732421875" />
                  <Point X="-3.140326660156" Y="23.665404296875" />
                  <Point X="-3.161161132812" Y="23.689361328125" />
                  <Point X="-3.187643798828" Y="23.704947265625" />
                  <Point X="-3.219529052734" Y="23.711427734375" />
                  <Point X="-4.237657714844" Y="23.577388671875" />
                  <Point X="-4.803283691406" Y="23.502923828125" />
                  <Point X="-4.843146484375" Y="23.658986328125" />
                  <Point X="-4.927393066406" Y="23.988806640625" />
                  <Point X="-4.956096679688" Y="24.1895" />
                  <Point X="-4.998396484375" Y="24.485255859375" />
                  <Point X="-4.113157714844" Y="24.722455078125" />
                  <Point X="-3.557463867188" Y="24.871353515625" />
                  <Point X="-3.531504150391" Y="24.885787109375" />
                  <Point X="-3.514145019531" Y="24.897833984375" />
                  <Point X="-3.502325439453" Y="24.909353515625" />
                  <Point X="-3.491434570313" Y="24.93525390625" />
                  <Point X="-3.483400878906" Y="24.96871875" />
                  <Point X="-3.489112304688" Y="24.994703125" />
                  <Point X="-3.502325439453" Y="25.028087890625" />
                  <Point X="-3.524540527344" Y="25.046818359375" />
                  <Point X="-3.54189453125" Y="25.05886328125" />
                  <Point X="-3.557463867188" Y="25.0660859375" />
                  <Point X="-4.485536132812" Y="25.31476171875" />
                  <Point X="-4.998186523438" Y="25.452126953125" />
                  <Point X="-4.971940917969" Y="25.6294921875" />
                  <Point X="-4.917645507812" Y="25.996416015625" />
                  <Point X="-4.859865234375" Y="26.209642578125" />
                  <Point X="-4.773515625" Y="26.528298828125" />
                  <Point X="-4.1516015625" Y="26.446421875" />
                  <Point X="-3.753266357422" Y="26.39398046875" />
                  <Point X="-3.731703369141" Y="26.3958671875" />
                  <Point X="-3.708698486328" Y="26.403119140625" />
                  <Point X="-3.67027734375" Y="26.415234375" />
                  <Point X="-3.651533935547" Y="26.426056640625" />
                  <Point X="-3.639119873047" Y="26.44378515625" />
                  <Point X="-3.629888916016" Y="26.4660703125" />
                  <Point X="-3.614472412109" Y="26.5032890625" />
                  <Point X="-3.610714111328" Y="26.52460546875" />
                  <Point X="-3.61631640625" Y="26.54551171875" />
                  <Point X="-3.627454345703" Y="26.566908203125" />
                  <Point X="-3.646056152344" Y="26.602640625" />
                  <Point X="-3.659968017578" Y="26.619220703125" />
                  <Point X="-4.192312988281" Y="27.027703125" />
                  <Point X="-4.47610546875" Y="27.24546484375" />
                  <Point X="-4.370988769531" Y="27.425556640625" />
                  <Point X="-4.160016113281" Y="27.787001953125" />
                  <Point X="-4.006958251953" Y="27.98373828125" />
                  <Point X="-3.774670654297" Y="28.282310546875" />
                  <Point X="-3.403803222656" Y="28.068189453125" />
                  <Point X="-3.159156982422" Y="27.926943359375" />
                  <Point X="-3.138514892578" Y="27.92043359375" />
                  <Point X="-3.106475341797" Y="27.917630859375" />
                  <Point X="-3.052965576172" Y="27.91294921875" />
                  <Point X="-3.031507080078" Y="27.915775390625" />
                  <Point X="-3.013252929688" Y="27.927404296875" />
                  <Point X="-2.990510986328" Y="27.950146484375" />
                  <Point X="-2.952529296875" Y="27.988126953125" />
                  <Point X="-2.940899414062" Y="28.006380859375" />
                  <Point X="-2.93807421875" Y="28.02783984375" />
                  <Point X="-2.940877441406" Y="28.059880859375" />
                  <Point X="-2.945558837891" Y="28.113390625" />
                  <Point X="-2.952067382812" Y="28.134033203125" />
                  <Point X="-3.187965332031" Y="28.542619140625" />
                  <Point X="-3.307278808594" Y="28.749275390625" />
                  <Point X="-3.120326171875" Y="28.892611328125" />
                  <Point X="-2.752873291016" Y="29.174333984375" />
                  <Point X="-2.511817382812" Y="29.3082578125" />
                  <Point X="-2.141548339844" Y="29.51397265625" />
                  <Point X="-2.042720214844" Y="29.38517578125" />
                  <Point X="-1.967826538086" Y="29.28757421875" />
                  <Point X="-1.951247192383" Y="29.273662109375" />
                  <Point X="-1.915587280273" Y="29.25509765625" />
                  <Point X="-1.85603112793" Y="29.22409375" />
                  <Point X="-1.835124023438" Y="29.218490234375" />
                  <Point X="-1.813808349609" Y="29.22225" />
                  <Point X="-1.776666137695" Y="29.237634765625" />
                  <Point X="-1.714634399414" Y="29.263330078125" />
                  <Point X="-1.696905029297" Y="29.275744140625" />
                  <Point X="-1.686083251953" Y="29.29448828125" />
                  <Point X="-1.673994140625" Y="29.332830078125" />
                  <Point X="-1.653803955078" Y="29.396865234375" />
                  <Point X="-1.651917602539" Y="29.41842578125" />
                  <Point X="-1.678736572266" Y="29.622134765625" />
                  <Point X="-1.689137695313" Y="29.701138671875" />
                  <Point X="-1.443780883789" Y="29.7699296875" />
                  <Point X="-0.968083312988" Y="29.903296875" />
                  <Point X="-0.675859008789" Y="29.937498046875" />
                  <Point X="-0.224200012207" Y="29.990359375" />
                  <Point X="-0.114513511658" Y="29.581001953125" />
                  <Point X="-0.042140380859" Y="29.31090234375" />
                  <Point X="-0.024282073975" Y="29.28417578125" />
                  <Point X="0.006156086445" Y="29.27384375" />
                  <Point X="0.036594089508" Y="29.28417578125" />
                  <Point X="0.054452251434" Y="29.31090234375" />
                  <Point X="0.17532371521" Y="29.762" />
                  <Point X="0.236648452759" Y="29.990869140625" />
                  <Point X="0.444861328125" Y="29.9690625" />
                  <Point X="0.860209960938" Y="29.925564453125" />
                  <Point X="1.10198046875" Y="29.867193359375" />
                  <Point X="1.508456298828" Y="29.769056640625" />
                  <Point X="1.665873168945" Y="29.7119609375" />
                  <Point X="1.931043823242" Y="29.61578125" />
                  <Point X="2.083205566406" Y="29.54462109375" />
                  <Point X="2.338685302734" Y="29.425140625" />
                  <Point X="2.485706787109" Y="29.339486328125" />
                  <Point X="2.732520019531" Y="29.195693359375" />
                  <Point X="2.871168457031" Y="29.09709375" />
                  <Point X="3.068739990234" Y="28.956591796875" />
                  <Point X="2.552546630859" Y="28.062517578125" />
                  <Point X="2.229853515625" Y="27.50359765625" />
                  <Point X="2.224852050781" Y="27.491513671875" />
                  <Point X="2.216811279297" Y="27.4614453125" />
                  <Point X="2.203382324219" Y="27.4112265625" />
                  <Point X="2.202044677734" Y="27.39232421875" />
                  <Point X="2.205179931641" Y="27.36632421875" />
                  <Point X="2.210416015625" Y="27.322900390625" />
                  <Point X="2.218682373047" Y="27.3008125" />
                  <Point X="2.234770751953" Y="27.2771015625" />
                  <Point X="2.261640136719" Y="27.23750390625" />
                  <Point X="2.274938720703" Y="27.224205078125" />
                  <Point X="2.298648681641" Y="27.2081171875" />
                  <Point X="2.338247314453" Y="27.18124609375" />
                  <Point X="2.360336914062" Y="27.172978515625" />
                  <Point X="2.386337646484" Y="27.16984375" />
                  <Point X="2.42976171875" Y="27.164607421875" />
                  <Point X="2.448665039062" Y="27.1659453125" />
                  <Point X="2.478733642578" Y="27.173986328125" />
                  <Point X="2.528951416016" Y="27.187416015625" />
                  <Point X="2.541033935547" Y="27.19241796875" />
                  <Point X="3.47449609375" Y="27.7313515625" />
                  <Point X="3.994247802734" Y="28.0314296875" />
                  <Point X="4.056188232422" Y="27.94534765625" />
                  <Point X="4.202591308594" Y="27.741880859375" />
                  <Point X="4.279884277344" Y="27.61415234375" />
                  <Point X="4.387512695312" Y="27.436294921875" />
                  <Point X="3.713471923828" Y="26.9190859375" />
                  <Point X="3.288616210938" Y="26.59308203125" />
                  <Point X="3.27937109375" Y="26.58383203125" />
                  <Point X="3.257730712891" Y="26.5556015625" />
                  <Point X="3.221588867188" Y="26.508451171875" />
                  <Point X="3.213119384766" Y="26.4915" />
                  <Point X="3.205058349609" Y="26.46267578125" />
                  <Point X="3.191595458984" Y="26.41453515625" />
                  <Point X="3.190779541016" Y="26.39096484375" />
                  <Point X="3.197396728516" Y="26.35889453125" />
                  <Point X="3.208448486328" Y="26.30533203125" />
                  <Point X="3.215646240234" Y="26.287955078125" />
                  <Point X="3.23364453125" Y="26.260599609375" />
                  <Point X="3.263703857422" Y="26.21491015625" />
                  <Point X="3.280947265625" Y="26.1988203125" />
                  <Point X="3.307029296875" Y="26.184138671875" />
                  <Point X="3.350589355469" Y="26.1596171875" />
                  <Point X="3.368566162109" Y="26.153619140625" />
                  <Point X="3.403830810547" Y="26.148958984375" />
                  <Point X="3.462726806641" Y="26.14117578125" />
                  <Point X="3.475803710938" Y="26.141171875" />
                  <Point X="4.362530273438" Y="26.25791015625" />
                  <Point X="4.848975585938" Y="26.321953125" />
                  <Point X="4.876309082031" Y="26.209673828125" />
                  <Point X="4.939188476562" Y="25.95138671875" />
                  <Point X="4.963545410156" Y="25.794943359375" />
                  <Point X="4.997858398438" Y="25.574556640625" />
                  <Point X="4.228061523438" Y="25.3682890625" />
                  <Point X="3.741167724609" Y="25.237826171875" />
                  <Point X="3.729087402344" Y="25.232818359375" />
                  <Point X="3.694440917969" Y="25.21279296875" />
                  <Point X="3.636577392578" Y="25.17934765625" />
                  <Point X="3.622265136719" Y="25.166927734375" />
                  <Point X="3.601477294922" Y="25.1404375" />
                  <Point X="3.566759277344" Y="25.09619921875" />
                  <Point X="3.556985107422" Y="25.074734375" />
                  <Point X="3.550055664062" Y="25.038552734375" />
                  <Point X="3.538482910156" Y="24.978123046875" />
                  <Point X="3.538483154297" Y="24.959314453125" />
                  <Point X="3.545412597656" Y="24.9231328125" />
                  <Point X="3.556985351562" Y="24.862705078125" />
                  <Point X="3.566758544922" Y="24.8412421875" />
                  <Point X="3.587546386719" Y="24.81475390625" />
                  <Point X="3.622264404297" Y="24.770513671875" />
                  <Point X="3.636576416016" Y="24.758091796875" />
                  <Point X="3.671222900391" Y="24.73806640625" />
                  <Point X="3.729086425781" Y="24.704619140625" />
                  <Point X="3.741167724609" Y="24.699611328125" />
                  <Point X="4.554336425781" Y="24.481724609375" />
                  <Point X="4.998067871094" Y="24.362826171875" />
                  <Point X="4.983541015625" Y="24.26647265625" />
                  <Point X="4.948431640625" Y="24.0336015625" />
                  <Point X="4.917226074219" Y="23.896853515625" />
                  <Point X="4.874545410156" Y="23.709822265625" />
                  <Point X="3.976641845703" Y="23.828033203125" />
                  <Point X="3.411981933594" Y="23.90237109375" />
                  <Point X="3.394836181641" Y="23.901658203125" />
                  <Point X="3.326837402344" Y="23.88687890625" />
                  <Point X="3.213271972656" Y="23.8621953125" />
                  <Point X="3.1854453125" Y="23.845302734375" />
                  <Point X="3.144344238281" Y="23.79587109375" />
                  <Point X="3.075701171875" Y="23.713314453125" />
                  <Point X="3.064357910156" Y="23.6859296875" />
                  <Point X="3.058467041016" Y="23.6219140625" />
                  <Point X="3.04862890625" Y="23.514998046875" />
                  <Point X="3.056360595703" Y="23.483376953125" />
                  <Point X="3.0939921875" Y="23.42484375" />
                  <Point X="3.156840820312" Y="23.327087890625" />
                  <Point X="3.168460693359" Y="23.314458984375" />
                  <Point X="3.923086181641" Y="22.7354140625" />
                  <Point X="4.33907421875" Y="22.416216796875" />
                  <Point X="4.303100585938" Y="22.358005859375" />
                  <Point X="4.204130371094" Y="22.19785546875" />
                  <Point X="4.139596191406" Y="22.106162109375" />
                  <Point X="4.056688232422" Y="21.988361328125" />
                  <Point X="3.256360351562" Y="22.4504296875" />
                  <Point X="2.753454589844" Y="22.740783203125" />
                  <Point X="2.737340820313" Y="22.746685546875" />
                  <Point X="2.656411621094" Y="22.76130078125" />
                  <Point X="2.521250488281" Y="22.7857109375" />
                  <Point X="2.489077636719" Y="22.78075390625" />
                  <Point X="2.421845214844" Y="22.74537109375" />
                  <Point X="2.309559570312" Y="22.686275390625" />
                  <Point X="2.288599853516" Y="22.66531640625" />
                  <Point X="2.253215820312" Y="22.598083984375" />
                  <Point X="2.194120849609" Y="22.485798828125" />
                  <Point X="2.189163085938" Y="22.453625" />
                  <Point X="2.203778808594" Y="22.3726953125" />
                  <Point X="2.228188720703" Y="22.237533203125" />
                  <Point X="2.234091552734" Y="22.221419921875" />
                  <Point X="2.719014404297" Y="21.381509765625" />
                  <Point X="2.986673339844" Y="20.917912109375" />
                  <Point X="2.952733886719" Y="20.893669921875" />
                  <Point X="2.835294433594" Y="20.80978515625" />
                  <Point X="2.763144775391" Y="20.763083984375" />
                  <Point X="2.679775390625" Y="20.709119140625" />
                  <Point X="2.066941162109" Y="21.50778125" />
                  <Point X="1.683177612305" Y="22.007912109375" />
                  <Point X="1.670549316406" Y="22.019533203125" />
                  <Point X="1.590731201172" Y="22.07084765625" />
                  <Point X="1.45742590332" Y="22.15655078125" />
                  <Point X="1.425805541992" Y="22.16428125" />
                  <Point X="1.338510864258" Y="22.156248046875" />
                  <Point X="1.19271862793" Y="22.142833984375" />
                  <Point X="1.165332519531" Y="22.131490234375" />
                  <Point X="1.09792578125" Y="22.075443359375" />
                  <Point X="0.985348754883" Y="21.981837890625" />
                  <Point X="0.968456726074" Y="21.95401171875" />
                  <Point X="0.94830255127" Y="21.861287109375" />
                  <Point X="0.91464251709" Y="21.70642578125" />
                  <Point X="0.91392956543" Y="21.68928125" />
                  <Point X="1.051352905273" Y="20.6454453125" />
                  <Point X="1.127642456055" Y="20.065970703125" />
                  <Point X="1.10544921875" Y="20.06110546875" />
                  <Point X="0.994369445801" Y="20.0367578125" />
                  <Point X="0.927688049316" Y="20.02464453125" />
                  <Point X="0.860200439453" Y="20.012384765625" />
                </GraphicPath>
              </GraphicsPathsElement>
            </ChildElements>
          </FillOutlinesElement>
          <PolygonElement>
            <UID Value="G#154" />
            <Label Value="Polygon" />
            <Translation X="0.006155967712" Y="24.968719959259" Z="0" />
            <Area Value="True" />
            <OutlinesChanged Value="True" />
            <GraphicResolution Value="0.02" />
            <Box>
              <Center X="-0.006155967712" Y="0.031280040741" />
              <Width Value="10.108342170715" />
              <Height Value="10.090085983276" />
            </Box>
            <IsBorder Value="False" />
            <ChildElements>
              <OutlinePolygonElement>
                <Label Value="Outline" />
                <Area Value="True" />
                <GraphicResolution Value="0.02" />
                <Box>
                  <Center X="-0.006155967712" Y="0.031280040741" />
                  <Width Value="10.108342170715" />
                  <Height Value="10.090085983276" />
                </Box>
                <IsBorder Value="True" />
                <IsClosed Value="True" />
                <Points>
                  <Point X="-0.072373132609" Y="4.625224695354" Z="0.95" />
                  <Point X="-0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="-0.687827167114" Y="5.018439361996" Z="0.95" />
                  <Point X="-0.983964085579" Y="4.983780860901" Z="0.95" />
                  <Point X="-1.463434592406" Y="4.849354145221" Z="0.95" />
                  <Point X="-1.750522971153" Y="4.768864631653" Z="0.95" />
                  <Point X="-1.734464829951" Y="4.646890816506" Z="0.95" />
                  <Point X="-1.707645893097" Y="4.443180561066" Z="0.95" />
                  <Point X="-1.727836057336" Y="4.379145697495" Z="0.95" />
                  <Point X="-1.739925146103" Y="4.340804100037" Z="0.95" />
                  <Point X="-1.80195686037" Y="4.315109506691" Z="0.95" />
                  <Point X="-1.839099049568" Y="4.299724578857" Z="0.95" />
                  <Point X="-1.898655254551" Y="4.330727797472" Z="0.95" />
                  <Point X="-1.93431520462" Y="4.349291324615" Z="0.95" />
                  <Point X="-2.009208819016" Y="4.446894628308" Z="0.95" />
                  <Point X="-2.134289741516" Y="4.609903335571" Z="0.95" />
                  <Point X="-2.542256355709" Y="4.383246003837" Z="0.95" />
                  <Point X="-2.786530971527" Y="4.247532367706" Z="0.95" />
                  <Point X="-3.156904143837" Y="3.963571320746" Z="0.95" />
                  <Point X="-3.378669261932" Y="3.793546438217" Z="0.95" />
                  <Point X="-3.237422677147" Y="3.548900191118" Z="0.95" />
                  <Point X="-3.001524686813" Y="3.14031291008" Z="0.95" />
                  <Point X="-2.996843140853" Y="3.086803136521" Z="0.95" />
                  <Point X="-2.99404001236" Y="3.054763555527" Z="0.95" />
                  <Point X="-3.032021631993" Y="3.016781935893" Z="0.95" />
                  <Point X="-3.054763555527" Y="2.99404001236" Z="0.95" />
                  <Point X="-3.108273329085" Y="2.998721558319" Z="0.95" />
                  <Point X="-3.14031291008" Y="3.001524686813" Z="0.95" />
                  <Point X="-3.384959157179" Y="3.142771271599" Z="0.95" />
                  <Point X="-3.793546438217" Y="3.378669261932" Z="0.95" />
                  <Point X="-4.052577615423" Y="3.045721167049" Z="0.95" />
                  <Point X="-4.207675457001" Y="2.846364736557" Z="0.95" />
                  <Point X="-4.420326774766" Y="2.482042096666" Z="0.95" />
                  <Point X="-4.547654151917" Y="2.263899803162" Z="0.95" />
                  <Point X="-4.228906841898" Y="2.019316417288" Z="0.95" />
                  <Point X="-3.696562290192" Y="1.610834121704" Z="0.95" />
                  <Point X="-3.677960478325" Y="1.575100368889" Z="0.95" />
                  <Point X="-3.666822433472" Y="1.553704380989" Z="0.95" />
                  <Point X="-3.682239040352" Y="1.516485397167" Z="0.95" />
                  <Point X="-3.691469907761" Y="1.494200110435" Z="0.95" />
                  <Point X="-3.729891004856" Y="1.482085996979" Z="0.95" />
                  <Point X="-3.75289607048" Y="1.47483253479" Z="0.95" />
                  <Point X="-4.151230960485" Y="1.527274333781" Z="0.95" />
                  <Point X="-4.816495895386" Y="1.614858150482" Z="0.95" />
                  <Point X="-4.914280898113" Y="1.254001521842" Z="0.95" />
                  <Point X="-4.9728307724" Y="1.037934541702" Z="0.95" />
                  <Point X="-5.027558317573" Y="0.668091012806" Z="0.95" />
                  <Point X="-5.06032705307" Y="0.446643024683" Z="0.95" />
                  <Point X="-4.504633192483" Y="0.29774530121" Z="0.95" />
                  <Point X="-3.576560735703" Y="0.049069032073" Z="0.95" />
                  <Point X="-3.559201748797" Y="0.037020903848" Z="0.95" />
                  <Point X="-3.548807859421" Y="0.029806951061" Z="0.95" />
                  <Point X="-3.543021431034" Y="0.011163172741" Z="0.95" />
                  <Point X="-3.539556741714" Y="0" Z="0.95" />
                  <Point X="-3.545343170101" Y="-0.01864377832" Z="0.95" />
                  <Point X="-3.548807859421" Y="-0.029806951061" Z="0.95" />
                  <Point X="-3.566166846326" Y="-0.041855079286" Z="0.95" />
                  <Point X="-3.576560735703" Y="-0.049069032073" Z="0.95" />
                  <Point X="-4.13225459629" Y="-0.197966755546" Z="0.95" />
                  <Point X="-5.06032705307" Y="-0.446643024683" Z="0.95" />
                  <Point X="-5.011748613306" Y="-0.786298351659" Z="0.95" />
                  <Point X="-4.982661724091" Y="-0.989670813084" Z="0.95" />
                  <Point X="-4.897746983604" Y="-1.322108856122" Z="0.95" />
                  <Point X="-4.846903324127" Y="-1.521159887314" Z="0.95" />
                  <Point X="-4.237287389832" Y="-1.44090244239" Z="0.95" />
                  <Point X="-3.219158887863" Y="-1.306863307953" Z="0.95" />
                  <Point X="-3.202594449962" Y="-1.316612415281" Z="0.95" />
                  <Point X="-3.192676305771" Y="-1.322449803352" Z="0.95" />
                  <Point X="-3.197495346885" Y="-1.341056312722" Z="0.95" />
                  <Point X="-3.200380802155" Y="-1.352197170258" Z="0.95" />
                  <Point X="-3.688194986049" Y="-1.726510142878" Z="0.95" />
                  <Point X="-4.502900600433" Y="-2.351655721664" Z="0.95" />
                  <Point X="-4.319292375137" Y="-2.659538688545" Z="0.95" />
                  <Point X="-4.209354877472" Y="-2.843887090683" Z="0.95" />
                  <Point X="-3.992239291881" Y="-3.129132270803" Z="0.95" />
                  <Point X="-3.862238883972" Y="-3.299926042557" Z="0.95" />
                  <Point X="-3.379338501375" Y="-3.021123380545" Z="0.95" />
                  <Point X="-2.57283949852" Y="-2.555490970612" Z="0.95" />
                  <Point X="-2.561988267616" Y="-2.566342201516" Z="0.95" />
                  <Point X="-2.555490970612" Y="-2.57283949852" Z="0.95" />
                  <Point X="-2.834293632624" Y="-3.055739881117" Z="0.95" />
                  <Point X="-3.299926042557" Y="-3.862238883972" Z="0.95" />
                  <Point X="-3.043821185752" Y="-4.059431093643" Z="0.95" />
                  <Point X="-2.890475511551" Y="-4.177502155304" Z="0.95" />
                  <Point X="-2.61566415568" Y="-4.347658522805" Z="0.95" />
                  <Point X="-2.451117753983" Y="-4.449541568756" Z="0.95" />
                  <Point X="-2.366890854811" Y="-4.339774975048" Z="0.95" />
                  <Point X="-2.226222276688" Y="-4.156452178955" Z="0.95" />
                  <Point X="-2.064868432468" Y="-4.04863882847" Z="0.95" />
                  <Point X="-1.968255996704" Y="-3.984084367752" Z="0.95" />
                  <Point X="-1.774612862059" Y="-3.99677642315" Z="0.95" />
                  <Point X="-1.658666849136" Y="-4.004375934601" Z="0.95" />
                  <Point X="-1.512765849674" Y="-4.132327851361" Z="0.95" />
                  <Point X="-1.425405979156" Y="-4.208940505981" Z="0.95" />
                  <Point X="-1.387547016534" Y="-4.399270186363" Z="0.95" />
                  <Point X="-1.364878535271" Y="-4.51323223114" Z="0.95" />
                  <Point X="-1.382937877881" Y="-4.650406497854" Z="0.95" />
                  <Point X="-1.413099050522" Y="-4.879503250122" Z="0.95" />
                  <Point X="-1.228145102589" Y="-4.927090293157" Z="0.95" />
                  <Point X="-1.117401838303" Y="-4.955583572388" Z="0.95" />
                  <Point X="-0.929923094591" Y="-4.991973857108" Z="0.95" />
                  <Point X="-0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="-0.667644072766" Y="-4.453865692296" Z="0.95" />
                  <Point X="-0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="-0.314677682586" Y="-3.371221093716" Z="0.95" />
                  <Point X="-0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="-0.094886966551" Y="-3.2336887812" Z="0.95" />
                  <Point X="-1E-12" Y="-3.204239368439" Z="0.95" />
                  <Point X="0.15847211281" Y="-3.253423264088" Z="0.95" />
                  <Point X="0.253359079361" Y="-3.282872676849" Z="0.95" />
                  <Point X="0.355768176847" Y="-3.430424662052" Z="0.95" />
                  <Point X="0.417086780071" Y="-3.518773078918" Z="0.95" />
                  <Point X="0.567110787635" Y="-4.07867033752" Z="0.95" />
                  <Point X="0.81766808033" Y="-5.013762950897" Z="0.95" />
                  <Point X="0.930469209204" Y="-4.993271102605" Z="0.95" />
                  <Point X="0.998010158539" Y="-4.981001377106" Z="0.95" />
                  <Point X="1.109998619471" Y="-4.956453570892" Z="0.95" />
                  <Point X="1.177052974701" Y="-4.9417552948" Z="0.95" />
                  <Point X="1.094769170485" Y="-4.316747825099" Z="0.95" />
                  <Point X="0.95734578371" Y="-3.272913694382" Z="0.95" />
                  <Point X="0.991005738275" Y="-3.11805150044" Z="0.95" />
                  <Point X="1.01116001606" Y="-3.025326013565" Z="0.95" />
                  <Point X="1.123737006955" Y="-2.931721937239" Z="0.95" />
                  <Point X="1.191143751144" Y="-2.875675439835" Z="0.95" />
                  <Point X="1.336935813286" Y="-2.862259521662" Z="0.95" />
                  <Point X="1.424230456352" Y="-2.854226589203" Z="0.95" />
                  <Point X="1.557535811065" Y="-2.939929615357" Z="0.95" />
                  <Point X="1.637353897095" Y="-2.991245269775" Z="0.95" />
                  <Point X="2.02111748354" Y="-3.491375610757" Z="0.95" />
                  <Point X="2.662046670914" Y="-4.326650619507" Z="0.95" />
                  <Point X="2.784158026163" Y="-4.247610292375" Z="0.95" />
                  <Point X="2.857273578644" Y="-4.200284004211" Z="0.95" />
                  <Point X="2.975639607649" Y="-4.11573790583" Z="0.95" />
                  <Point X="3.04651260376" Y="-4.065114974976" Z="0.95" />
                  <Point X="2.756159637975" Y="-3.562209096578" Z="0.95" />
                  <Point X="2.271236896515" Y="-2.722298622131" Z="0.95" />
                  <Point X="2.24682688371" Y="-2.587137455655" Z="0.95" />
                  <Point X="2.232211112976" Y="-2.506208181381" Z="0.95" />
                  <Point X="2.291306291637" Y="-2.393922510208" Z="0.95" />
                  <Point X="2.326690196991" Y="-2.326690196991" Z="0.95" />
                  <Point X="2.438975868164" Y="-2.26759501833" Z="0.95" />
                  <Point X="2.506208181381" Y="-2.232211112976" Z="0.95" />
                  <Point X="2.641369347858" Y="-2.256621125781" Z="0.95" />
                  <Point X="2.722298622131" Y="-2.271236896515" Z="0.95" />
                  <Point X="3.225204500529" Y="-2.561589862299" Z="0.95" />
                  <Point X="4.065114974976" Y="-3.04651260376" Z="0.95" />
                  <Point X="4.174329075985" Y="-2.891334409567" Z="0.95" />
                  <Point X="4.239722251892" Y="-2.798419713974" Z="0.95" />
                  <Point X="4.339478416168" Y="-2.636999209956" Z="0.95" />
                  <Point X="4.399208545685" Y="-2.540346860886" Z="0.95" />
                  <Point X="3.947368126884" Y="-2.193637466852" Z="0.95" />
                  <Point X="3.192742824554" Y="-1.614593029022" Z="0.95" />
                  <Point X="3.129893968534" Y="-1.516835763929" Z="0.95" />
                  <Point X="3.092262506485" Y="-1.458302497864" Z="0.95" />
                  <Point X="3.102100717235" Y="-1.351388309018" Z="0.95" />
                  <Point X="3.107991456985" Y="-1.287372231483" Z="0.95" />
                  <Point X="3.176634645127" Y="-1.204815786407" Z="0.95" />
                  <Point X="3.217735528946" Y="-1.15538418293" Z="0.95" />
                  <Point X="3.33130115772" Y="-1.130700223706" Z="0.95" />
                  <Point X="3.399299860001" Y="-1.115920424461" Z="0.95" />
                  <Point X="3.963959838658" Y="-1.190259315328" Z="0.95" />
                  <Point X="4.90700674057" Y="-1.314413785934" Z="0.95" />
                  <Point X="4.959817406447" Y="-1.082990241349" Z="0.95" />
                  <Point X="4.991438388824" Y="-0.944422781467" Z="0.95" />
                  <Point X="5.026826241412" Y="-0.709702633977" Z="0.95" />
                  <Point X="5.048015117645" Y="-0.569161295891" Z="0.95" />
                  <Point X="4.561121261384" Y="-0.438698486297" Z="0.95" />
                  <Point X="3.747952699661" Y="-0.220810636878" Z="0.95" />
                  <Point X="3.690089161432" Y="-0.187364474603" Z="0.95" />
                  <Point X="3.655442714691" Y="-0.167338207364" Z="0.95" />
                  <Point X="3.620724591754" Y="-0.123099160149" Z="0.95" />
                  <Point X="3.599936723709" Y="-0.096610531211" Z="0.95" />
                  <Point X="3.588364016063" Y="-0.036182165931" Z="0.95" />
                  <Point X="3.581434726715" Y="-3.9E-11" Z="0.95" />
                  <Point X="3.593007434361" Y="0.060428365289" Z="0.95" />
                  <Point X="3.599936723709" Y="0.096610531211" Z="0.95" />
                  <Point X="3.634654846647" Y="0.140849578426" Z="0.95" />
                  <Point X="3.655442714691" Y="0.167338207364" Z="0.95" />
                  <Point X="3.71330625292" Y="0.200784369639" Z="0.95" />
                  <Point X="3.747952699661" Y="0.220810636878" Z="0.95" />
                  <Point X="4.234846555923" Y="0.351273446472" Z="0.95" />
                  <Point X="5.048015117645" Y="0.569161295891" Z="0.95" />
                  <Point X="5.006794309027" Y="0.833915820735" Z="0.95" />
                  <Point X="4.982112884521" Y="0.992440581322" Z="0.95" />
                  <Point X="4.918734180038" Y="1.252781243288" Z="0.95" />
                  <Point X="4.88078546524" Y="1.408663153648" Z="0.95" />
                  <Point X="4.349847944739" Y="1.338763881496" Z="0.95" />
                  <Point X="3.463121414185" Y="1.222024083138" Z="0.95" />
                  <Point X="3.404225320156" Y="1.229807919436" Z="0.95" />
                  <Point X="3.368960618973" Y="1.234468579292" Z="0.95" />
                  <Point X="3.325400603209" Y="1.258989095253" Z="0.95" />
                  <Point X="3.299318552017" Y="1.273671030998" Z="0.95" />
                  <Point X="3.269259307726" Y="1.319359761055" Z="0.95" />
                  <Point X="3.251260995865" Y="1.346716403961" Z="0.95" />
                  <Point X="3.240209338185" Y="1.400278371993" Z="0.95" />
                  <Point X="3.233592033386" Y="1.432349205017" Z="0.95" />
                  <Point X="3.247054926584" Y="1.480489285773" Z="0.95" />
                  <Point X="3.25511598587" Y="1.509313702583" Z="0.95" />
                  <Point X="3.2912578249" Y="1.556463579626" Z="0.95" />
                  <Point X="3.312898159027" Y="1.584695100784" Z="0.95" />
                  <Point X="3.737753968014" Y="1.9106984178" Z="0.95" />
                  <Point X="4.447311878204" Y="2.455161333084" Z="0.95" />
                  <Point X="4.316505669415" Y="2.671319970297" Z="0.95" />
                  <Point X="4.23818397522" Y="2.800747394562" Z="0.95" />
                  <Point X="4.090617972073" Y="3.005830662892" Z="0.95" />
                  <Point X="4.002261161804" Y="3.128626585007" Z="0.95" />
                  <Point X="3.443340127324" Y="2.805933393701" Z="0.95" />
                  <Point X="2.509877920151" Y="2.266998767853" Z="0.95" />
                  <Point X="2.459660126558" Y="2.253569875626" Z="0.95" />
                  <Point X="2.429591655731" Y="2.245529174805" Z="0.95" />
                  <Point X="2.386167494724" Y="2.25076532432" Z="0.95" />
                  <Point X="2.360166788101" Y="2.253900527954" Z="0.95" />
                  <Point X="2.320568183699" Y="2.280769795192" Z="0.95" />
                  <Point X="2.296858072281" Y="2.296858072281" Z="0.95" />
                  <Point X="2.269988805043" Y="2.336456676683" Z="0.95" />
                  <Point X="2.253900527954" Y="2.360166788101" Z="0.95" />
                  <Point X="2.248664378438" Y="2.403590949109" Z="0.95" />
                  <Point X="2.245529174805" Y="2.429591655731" Z="0.95" />
                  <Point X="2.258958067032" Y="2.479809449324" Z="0.95" />
                  <Point X="2.266998767853" Y="2.509877920151" Z="0.95" />
                  <Point X="2.589691959159" Y="3.068798954631" Z="0.95" />
                  <Point X="3.128626585007" Y="4.002261161804" Z="0.95" />
                  <Point X="2.89398946401" Y="4.169122119417" Z="0.95" />
                  <Point X="2.753497838974" Y="4.269032001495" Z="0.95" />
                  <Point X="2.504720787903" Y="4.413970087498" Z="0.95" />
                  <Point X="2.355762720108" Y="4.50075340271" Z="0.95" />
                  <Point X="2.09823116467" Y="4.621192297922" Z="0.95" />
                  <Point X="1.944031238556" Y="4.693306446075" Z="0.95" />
                  <Point X="1.676765786751" Y="4.790245600089" Z="0.95" />
                  <Point X="1.516737580299" Y="4.848289012909" Z="0.95" />
                  <Point X="1.107558973165" Y="4.947077342532" Z="0.95" />
                  <Point X="0.862558662891" Y="5.006227970123" Z="0.95" />
                  <Point X="0.443913236643" Y="5.050071327311" Z="0.95" />
                  <Point X="0.193244561553" Y="5.076323032379" Z="0.95" />
                  <Point X="0.120871428944" Y="4.806222810597" Z="0.95" />
                  <Point X="0" Y="4.355124473572" Z="0.95" />
                </Points>
              </OutlinePolygonElement>
            </ChildElements>
          </PolygonElement>
        </ChildElements>
      </GroupElement>
    </ChildElements>
  </JobElement>
</ModelData>